/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.classroutine.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.classroutine.NoSuchClassRoutineEventBatchException;
import info.diit.portal.classroutine.model.ClassRoutineEventBatch;
import info.diit.portal.classroutine.model.impl.ClassRoutineEventBatchImpl;
import info.diit.portal.classroutine.model.impl.ClassRoutineEventBatchModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the class routine event batch service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author saeid
 * @see ClassRoutineEventBatchPersistence
 * @see ClassRoutineEventBatchUtil
 * @generated
 */
public class ClassRoutineEventBatchPersistenceImpl extends BasePersistenceImpl<ClassRoutineEventBatch>
	implements ClassRoutineEventBatchPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ClassRoutineEventBatchUtil} to access the class routine event batch persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ClassRoutineEventBatchImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventBatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventBatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyId",
			new String[] { Long.class.getName() },
			ClassRoutineEventBatchModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHID = new FinderPath(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventBatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBybatchId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHID =
		new FinderPath(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventBatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBybatchId",
			new String[] { Long.class.getName() },
			ClassRoutineEventBatchModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCHID = new FinderPath(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBybatchId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CLASSROUTINEEVENTID =
		new FinderPath(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventBatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByClassRoutineEventId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLASSROUTINEEVENTID =
		new FinderPath(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventBatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByClassRoutineEventId", new String[] { Long.class.getName() },
			ClassRoutineEventBatchModelImpl.CLASSROUTINEEVENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CLASSROUTINEEVENTID = new FinderPath(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByClassRoutineEventId", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventBatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventBatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the class routine event batch in the entity cache if it is enabled.
	 *
	 * @param classRoutineEventBatch the class routine event batch
	 */
	public void cacheResult(ClassRoutineEventBatch classRoutineEventBatch) {
		EntityCacheUtil.putResult(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchImpl.class,
			classRoutineEventBatch.getPrimaryKey(), classRoutineEventBatch);

		classRoutineEventBatch.resetOriginalValues();
	}

	/**
	 * Caches the class routine event batchs in the entity cache if it is enabled.
	 *
	 * @param classRoutineEventBatchs the class routine event batchs
	 */
	public void cacheResult(
		List<ClassRoutineEventBatch> classRoutineEventBatchs) {
		for (ClassRoutineEventBatch classRoutineEventBatch : classRoutineEventBatchs) {
			if (EntityCacheUtil.getResult(
						ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
						ClassRoutineEventBatchImpl.class,
						classRoutineEventBatch.getPrimaryKey()) == null) {
				cacheResult(classRoutineEventBatch);
			}
			else {
				classRoutineEventBatch.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all class routine event batchs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ClassRoutineEventBatchImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ClassRoutineEventBatchImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the class routine event batch.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ClassRoutineEventBatch classRoutineEventBatch) {
		EntityCacheUtil.removeResult(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchImpl.class,
			classRoutineEventBatch.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ClassRoutineEventBatch> classRoutineEventBatchs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ClassRoutineEventBatch classRoutineEventBatch : classRoutineEventBatchs) {
			EntityCacheUtil.removeResult(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
				ClassRoutineEventBatchImpl.class,
				classRoutineEventBatch.getPrimaryKey());
		}
	}

	/**
	 * Creates a new class routine event batch with the primary key. Does not add the class routine event batch to the database.
	 *
	 * @param classRoutineEventBatchId the primary key for the new class routine event batch
	 * @return the new class routine event batch
	 */
	public ClassRoutineEventBatch create(long classRoutineEventBatchId) {
		ClassRoutineEventBatch classRoutineEventBatch = new ClassRoutineEventBatchImpl();

		classRoutineEventBatch.setNew(true);
		classRoutineEventBatch.setPrimaryKey(classRoutineEventBatchId);

		return classRoutineEventBatch;
	}

	/**
	 * Removes the class routine event batch with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param classRoutineEventBatchId the primary key of the class routine event batch
	 * @return the class routine event batch that was removed
	 * @throws info.diit.portal.classroutine.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch remove(long classRoutineEventBatchId)
		throws NoSuchClassRoutineEventBatchException, SystemException {
		return remove(Long.valueOf(classRoutineEventBatchId));
	}

	/**
	 * Removes the class routine event batch with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the class routine event batch
	 * @return the class routine event batch that was removed
	 * @throws info.diit.portal.classroutine.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClassRoutineEventBatch remove(Serializable primaryKey)
		throws NoSuchClassRoutineEventBatchException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ClassRoutineEventBatch classRoutineEventBatch = (ClassRoutineEventBatch)session.get(ClassRoutineEventBatchImpl.class,
					primaryKey);

			if (classRoutineEventBatch == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchClassRoutineEventBatchException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(classRoutineEventBatch);
		}
		catch (NoSuchClassRoutineEventBatchException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ClassRoutineEventBatch removeImpl(
		ClassRoutineEventBatch classRoutineEventBatch)
		throws SystemException {
		classRoutineEventBatch = toUnwrappedModel(classRoutineEventBatch);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, classRoutineEventBatch);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(classRoutineEventBatch);

		return classRoutineEventBatch;
	}

	@Override
	public ClassRoutineEventBatch updateImpl(
		info.diit.portal.classroutine.model.ClassRoutineEventBatch classRoutineEventBatch,
		boolean merge) throws SystemException {
		classRoutineEventBatch = toUnwrappedModel(classRoutineEventBatch);

		boolean isNew = classRoutineEventBatch.isNew();

		ClassRoutineEventBatchModelImpl classRoutineEventBatchModelImpl = (ClassRoutineEventBatchModelImpl)classRoutineEventBatch;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, classRoutineEventBatch, merge);

			classRoutineEventBatch.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ClassRoutineEventBatchModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((classRoutineEventBatchModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(classRoutineEventBatchModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] {
						Long.valueOf(classRoutineEventBatchModelImpl.getCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}

			if ((classRoutineEventBatchModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(classRoutineEventBatchModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHID,
					args);

				args = new Object[] {
						Long.valueOf(classRoutineEventBatchModelImpl.getBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHID,
					args);
			}

			if ((classRoutineEventBatchModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLASSROUTINEEVENTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(classRoutineEventBatchModelImpl.getOriginalClassRoutineEventId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CLASSROUTINEEVENTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLASSROUTINEEVENTID,
					args);

				args = new Object[] {
						Long.valueOf(classRoutineEventBatchModelImpl.getClassRoutineEventId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CLASSROUTINEEVENTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLASSROUTINEEVENTID,
					args);
			}
		}

		EntityCacheUtil.putResult(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventBatchImpl.class,
			classRoutineEventBatch.getPrimaryKey(), classRoutineEventBatch);

		return classRoutineEventBatch;
	}

	protected ClassRoutineEventBatch toUnwrappedModel(
		ClassRoutineEventBatch classRoutineEventBatch) {
		if (classRoutineEventBatch instanceof ClassRoutineEventBatchImpl) {
			return classRoutineEventBatch;
		}

		ClassRoutineEventBatchImpl classRoutineEventBatchImpl = new ClassRoutineEventBatchImpl();

		classRoutineEventBatchImpl.setNew(classRoutineEventBatch.isNew());
		classRoutineEventBatchImpl.setPrimaryKey(classRoutineEventBatch.getPrimaryKey());

		classRoutineEventBatchImpl.setClassRoutineEventBatchId(classRoutineEventBatch.getClassRoutineEventBatchId());
		classRoutineEventBatchImpl.setCompanyId(classRoutineEventBatch.getCompanyId());
		classRoutineEventBatchImpl.setUserId(classRoutineEventBatch.getUserId());
		classRoutineEventBatchImpl.setUserName(classRoutineEventBatch.getUserName());
		classRoutineEventBatchImpl.setCreateDate(classRoutineEventBatch.getCreateDate());
		classRoutineEventBatchImpl.setModifiedDate(classRoutineEventBatch.getModifiedDate());
		classRoutineEventBatchImpl.setOrganizationId(classRoutineEventBatch.getOrganizationId());
		classRoutineEventBatchImpl.setClassRoutineEventId(classRoutineEventBatch.getClassRoutineEventId());
		classRoutineEventBatchImpl.setBatchId(classRoutineEventBatch.getBatchId());

		return classRoutineEventBatchImpl;
	}

	/**
	 * Returns the class routine event batch with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the class routine event batch
	 * @return the class routine event batch
	 * @throws com.liferay.portal.NoSuchModelException if a class routine event batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClassRoutineEventBatch findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the class routine event batch with the primary key or throws a {@link info.diit.portal.classroutine.NoSuchClassRoutineEventBatchException} if it could not be found.
	 *
	 * @param classRoutineEventBatchId the primary key of the class routine event batch
	 * @return the class routine event batch
	 * @throws info.diit.portal.classroutine.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch findByPrimaryKey(
		long classRoutineEventBatchId)
		throws NoSuchClassRoutineEventBatchException, SystemException {
		ClassRoutineEventBatch classRoutineEventBatch = fetchByPrimaryKey(classRoutineEventBatchId);

		if (classRoutineEventBatch == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					classRoutineEventBatchId);
			}

			throw new NoSuchClassRoutineEventBatchException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				classRoutineEventBatchId);
		}

		return classRoutineEventBatch;
	}

	/**
	 * Returns the class routine event batch with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the class routine event batch
	 * @return the class routine event batch, or <code>null</code> if a class routine event batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClassRoutineEventBatch fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the class routine event batch with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param classRoutineEventBatchId the primary key of the class routine event batch
	 * @return the class routine event batch, or <code>null</code> if a class routine event batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch fetchByPrimaryKey(
		long classRoutineEventBatchId) throws SystemException {
		ClassRoutineEventBatch classRoutineEventBatch = (ClassRoutineEventBatch)EntityCacheUtil.getResult(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
				ClassRoutineEventBatchImpl.class, classRoutineEventBatchId);

		if (classRoutineEventBatch == _nullClassRoutineEventBatch) {
			return null;
		}

		if (classRoutineEventBatch == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				classRoutineEventBatch = (ClassRoutineEventBatch)session.get(ClassRoutineEventBatchImpl.class,
						Long.valueOf(classRoutineEventBatchId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (classRoutineEventBatch != null) {
					cacheResult(classRoutineEventBatch);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(ClassRoutineEventBatchModelImpl.ENTITY_CACHE_ENABLED,
						ClassRoutineEventBatchImpl.class,
						classRoutineEventBatchId, _nullClassRoutineEventBatch);
				}

				closeSession(session);
			}
		}

		return classRoutineEventBatch;
	}

	/**
	 * Returns all the class routine event batchs where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEventBatch> findByCompanyId(long companyId)
		throws SystemException {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the class routine event batchs where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of class routine event batchs
	 * @param end the upper bound of the range of class routine event batchs (not inclusive)
	 * @return the range of matching class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEventBatch> findByCompanyId(long companyId,
		int start, int end) throws SystemException {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the class routine event batchs where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of class routine event batchs
	 * @param end the upper bound of the range of class routine event batchs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEventBatch> findByCompanyId(long companyId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<ClassRoutineEventBatch> list = (List<ClassRoutineEventBatch>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ClassRoutineEventBatch classRoutineEventBatch : list) {
				if ((companyId != classRoutineEventBatch.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_CLASSROUTINEEVENTBATCH_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<ClassRoutineEventBatch>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first class routine event batch in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching class routine event batch
	 * @throws info.diit.portal.classroutine.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch findByCompanyId_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventBatchException, SystemException {
		ClassRoutineEventBatch classRoutineEventBatch = fetchByCompanyId_First(companyId,
				orderByComparator);

		if (classRoutineEventBatch != null) {
			return classRoutineEventBatch;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchClassRoutineEventBatchException(msg.toString());
	}

	/**
	 * Returns the first class routine event batch in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch fetchByCompanyId_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ClassRoutineEventBatch> list = findByCompanyId(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last class routine event batch in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching class routine event batch
	 * @throws info.diit.portal.classroutine.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch findByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventBatchException, SystemException {
		ClassRoutineEventBatch classRoutineEventBatch = fetchByCompanyId_Last(companyId,
				orderByComparator);

		if (classRoutineEventBatch != null) {
			return classRoutineEventBatch;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchClassRoutineEventBatchException(msg.toString());
	}

	/**
	 * Returns the last class routine event batch in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch fetchByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompanyId(companyId);

		List<ClassRoutineEventBatch> list = findByCompanyId(companyId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the class routine event batchs before and after the current class routine event batch in the ordered set where companyId = &#63;.
	 *
	 * @param classRoutineEventBatchId the primary key of the current class routine event batch
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next class routine event batch
	 * @throws info.diit.portal.classroutine.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch[] findByCompanyId_PrevAndNext(
		long classRoutineEventBatchId, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventBatchException, SystemException {
		ClassRoutineEventBatch classRoutineEventBatch = findByPrimaryKey(classRoutineEventBatchId);

		Session session = null;

		try {
			session = openSession();

			ClassRoutineEventBatch[] array = new ClassRoutineEventBatchImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session,
					classRoutineEventBatch, companyId, orderByComparator, true);

			array[1] = classRoutineEventBatch;

			array[2] = getByCompanyId_PrevAndNext(session,
					classRoutineEventBatch, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ClassRoutineEventBatch getByCompanyId_PrevAndNext(
		Session session, ClassRoutineEventBatch classRoutineEventBatch,
		long companyId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLASSROUTINEEVENTBATCH_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(classRoutineEventBatch);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ClassRoutineEventBatch> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the class routine event batchs where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the matching class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEventBatch> findBybatchId(long batchId)
		throws SystemException {
		return findBybatchId(batchId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the class routine event batchs where batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of class routine event batchs
	 * @param end the upper bound of the range of class routine event batchs (not inclusive)
	 * @return the range of matching class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEventBatch> findBybatchId(long batchId, int start,
		int end) throws SystemException {
		return findBybatchId(batchId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the class routine event batchs where batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of class routine event batchs
	 * @param end the upper bound of the range of class routine event batchs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEventBatch> findBybatchId(long batchId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHID;
			finderArgs = new Object[] { batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHID;
			finderArgs = new Object[] { batchId, start, end, orderByComparator };
		}

		List<ClassRoutineEventBatch> list = (List<ClassRoutineEventBatch>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ClassRoutineEventBatch classRoutineEventBatch : list) {
				if ((batchId != classRoutineEventBatch.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_CLASSROUTINEEVENTBATCH_WHERE);

			query.append(_FINDER_COLUMN_BATCHID_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				list = (List<ClassRoutineEventBatch>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first class routine event batch in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching class routine event batch
	 * @throws info.diit.portal.classroutine.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch findBybatchId_First(long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventBatchException, SystemException {
		ClassRoutineEventBatch classRoutineEventBatch = fetchBybatchId_First(batchId,
				orderByComparator);

		if (classRoutineEventBatch != null) {
			return classRoutineEventBatch;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchClassRoutineEventBatchException(msg.toString());
	}

	/**
	 * Returns the first class routine event batch in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch fetchBybatchId_First(long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ClassRoutineEventBatch> list = findBybatchId(batchId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last class routine event batch in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching class routine event batch
	 * @throws info.diit.portal.classroutine.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch findBybatchId_Last(long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventBatchException, SystemException {
		ClassRoutineEventBatch classRoutineEventBatch = fetchBybatchId_Last(batchId,
				orderByComparator);

		if (classRoutineEventBatch != null) {
			return classRoutineEventBatch;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchClassRoutineEventBatchException(msg.toString());
	}

	/**
	 * Returns the last class routine event batch in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch fetchBybatchId_Last(long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBybatchId(batchId);

		List<ClassRoutineEventBatch> list = findBybatchId(batchId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the class routine event batchs before and after the current class routine event batch in the ordered set where batchId = &#63;.
	 *
	 * @param classRoutineEventBatchId the primary key of the current class routine event batch
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next class routine event batch
	 * @throws info.diit.portal.classroutine.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch[] findBybatchId_PrevAndNext(
		long classRoutineEventBatchId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventBatchException, SystemException {
		ClassRoutineEventBatch classRoutineEventBatch = findByPrimaryKey(classRoutineEventBatchId);

		Session session = null;

		try {
			session = openSession();

			ClassRoutineEventBatch[] array = new ClassRoutineEventBatchImpl[3];

			array[0] = getBybatchId_PrevAndNext(session,
					classRoutineEventBatch, batchId, orderByComparator, true);

			array[1] = classRoutineEventBatch;

			array[2] = getBybatchId_PrevAndNext(session,
					classRoutineEventBatch, batchId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ClassRoutineEventBatch getBybatchId_PrevAndNext(Session session,
		ClassRoutineEventBatch classRoutineEventBatch, long batchId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLASSROUTINEEVENTBATCH_WHERE);

		query.append(_FINDER_COLUMN_BATCHID_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(classRoutineEventBatch);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ClassRoutineEventBatch> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the class routine event batchs where classRoutineEventId = &#63;.
	 *
	 * @param classRoutineEventId the class routine event ID
	 * @return the matching class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEventBatch> findByClassRoutineEventId(
		long classRoutineEventId) throws SystemException {
		return findByClassRoutineEventId(classRoutineEventId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the class routine event batchs where classRoutineEventId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param classRoutineEventId the class routine event ID
	 * @param start the lower bound of the range of class routine event batchs
	 * @param end the upper bound of the range of class routine event batchs (not inclusive)
	 * @return the range of matching class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEventBatch> findByClassRoutineEventId(
		long classRoutineEventId, int start, int end) throws SystemException {
		return findByClassRoutineEventId(classRoutineEventId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the class routine event batchs where classRoutineEventId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param classRoutineEventId the class routine event ID
	 * @param start the lower bound of the range of class routine event batchs
	 * @param end the upper bound of the range of class routine event batchs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEventBatch> findByClassRoutineEventId(
		long classRoutineEventId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CLASSROUTINEEVENTID;
			finderArgs = new Object[] { classRoutineEventId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CLASSROUTINEEVENTID;
			finderArgs = new Object[] {
					classRoutineEventId,
					
					start, end, orderByComparator
				};
		}

		List<ClassRoutineEventBatch> list = (List<ClassRoutineEventBatch>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ClassRoutineEventBatch classRoutineEventBatch : list) {
				if ((classRoutineEventId != classRoutineEventBatch.getClassRoutineEventId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_CLASSROUTINEEVENTBATCH_WHERE);

			query.append(_FINDER_COLUMN_CLASSROUTINEEVENTID_CLASSROUTINEEVENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(classRoutineEventId);

				list = (List<ClassRoutineEventBatch>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first class routine event batch in the ordered set where classRoutineEventId = &#63;.
	 *
	 * @param classRoutineEventId the class routine event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching class routine event batch
	 * @throws info.diit.portal.classroutine.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch findByClassRoutineEventId_First(
		long classRoutineEventId, OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventBatchException, SystemException {
		ClassRoutineEventBatch classRoutineEventBatch = fetchByClassRoutineEventId_First(classRoutineEventId,
				orderByComparator);

		if (classRoutineEventBatch != null) {
			return classRoutineEventBatch;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("classRoutineEventId=");
		msg.append(classRoutineEventId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchClassRoutineEventBatchException(msg.toString());
	}

	/**
	 * Returns the first class routine event batch in the ordered set where classRoutineEventId = &#63;.
	 *
	 * @param classRoutineEventId the class routine event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch fetchByClassRoutineEventId_First(
		long classRoutineEventId, OrderByComparator orderByComparator)
		throws SystemException {
		List<ClassRoutineEventBatch> list = findByClassRoutineEventId(classRoutineEventId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last class routine event batch in the ordered set where classRoutineEventId = &#63;.
	 *
	 * @param classRoutineEventId the class routine event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching class routine event batch
	 * @throws info.diit.portal.classroutine.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch findByClassRoutineEventId_Last(
		long classRoutineEventId, OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventBatchException, SystemException {
		ClassRoutineEventBatch classRoutineEventBatch = fetchByClassRoutineEventId_Last(classRoutineEventId,
				orderByComparator);

		if (classRoutineEventBatch != null) {
			return classRoutineEventBatch;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("classRoutineEventId=");
		msg.append(classRoutineEventId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchClassRoutineEventBatchException(msg.toString());
	}

	/**
	 * Returns the last class routine event batch in the ordered set where classRoutineEventId = &#63;.
	 *
	 * @param classRoutineEventId the class routine event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch fetchByClassRoutineEventId_Last(
		long classRoutineEventId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByClassRoutineEventId(classRoutineEventId);

		List<ClassRoutineEventBatch> list = findByClassRoutineEventId(classRoutineEventId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the class routine event batchs before and after the current class routine event batch in the ordered set where classRoutineEventId = &#63;.
	 *
	 * @param classRoutineEventBatchId the primary key of the current class routine event batch
	 * @param classRoutineEventId the class routine event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next class routine event batch
	 * @throws info.diit.portal.classroutine.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEventBatch[] findByClassRoutineEventId_PrevAndNext(
		long classRoutineEventBatchId, long classRoutineEventId,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventBatchException, SystemException {
		ClassRoutineEventBatch classRoutineEventBatch = findByPrimaryKey(classRoutineEventBatchId);

		Session session = null;

		try {
			session = openSession();

			ClassRoutineEventBatch[] array = new ClassRoutineEventBatchImpl[3];

			array[0] = getByClassRoutineEventId_PrevAndNext(session,
					classRoutineEventBatch, classRoutineEventId,
					orderByComparator, true);

			array[1] = classRoutineEventBatch;

			array[2] = getByClassRoutineEventId_PrevAndNext(session,
					classRoutineEventBatch, classRoutineEventId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ClassRoutineEventBatch getByClassRoutineEventId_PrevAndNext(
		Session session, ClassRoutineEventBatch classRoutineEventBatch,
		long classRoutineEventId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLASSROUTINEEVENTBATCH_WHERE);

		query.append(_FINDER_COLUMN_CLASSROUTINEEVENTID_CLASSROUTINEEVENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(classRoutineEventId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(classRoutineEventBatch);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ClassRoutineEventBatch> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the class routine event batchs.
	 *
	 * @return the class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEventBatch> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the class routine event batchs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of class routine event batchs
	 * @param end the upper bound of the range of class routine event batchs (not inclusive)
	 * @return the range of class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEventBatch> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the class routine event batchs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of class routine event batchs
	 * @param end the upper bound of the range of class routine event batchs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEventBatch> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ClassRoutineEventBatch> list = (List<ClassRoutineEventBatch>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLASSROUTINEEVENTBATCH);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLASSROUTINEEVENTBATCH;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<ClassRoutineEventBatch>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<ClassRoutineEventBatch>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the class routine event batchs where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCompanyId(long companyId) throws SystemException {
		for (ClassRoutineEventBatch classRoutineEventBatch : findByCompanyId(
				companyId)) {
			remove(classRoutineEventBatch);
		}
	}

	/**
	 * Removes all the class routine event batchs where batchId = &#63; from the database.
	 *
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBybatchId(long batchId) throws SystemException {
		for (ClassRoutineEventBatch classRoutineEventBatch : findBybatchId(
				batchId)) {
			remove(classRoutineEventBatch);
		}
	}

	/**
	 * Removes all the class routine event batchs where classRoutineEventId = &#63; from the database.
	 *
	 * @param classRoutineEventId the class routine event ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByClassRoutineEventId(long classRoutineEventId)
		throws SystemException {
		for (ClassRoutineEventBatch classRoutineEventBatch : findByClassRoutineEventId(
				classRoutineEventId)) {
			remove(classRoutineEventBatch);
		}
	}

	/**
	 * Removes all the class routine event batchs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (ClassRoutineEventBatch classRoutineEventBatch : findAll()) {
			remove(classRoutineEventBatch);
		}
	}

	/**
	 * Returns the number of class routine event batchs where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCompanyId(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANYID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLASSROUTINEEVENTBATCH_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANYID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of class routine event batchs where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the number of matching class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public int countBybatchId(long batchId) throws SystemException {
		Object[] finderArgs = new Object[] { batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCHID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLASSROUTINEEVENTBATCH_WHERE);

			query.append(_FINDER_COLUMN_BATCHID_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCHID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of class routine event batchs where classRoutineEventId = &#63;.
	 *
	 * @param classRoutineEventId the class routine event ID
	 * @return the number of matching class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByClassRoutineEventId(long classRoutineEventId)
		throws SystemException {
		Object[] finderArgs = new Object[] { classRoutineEventId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_CLASSROUTINEEVENTID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLASSROUTINEEVENTBATCH_WHERE);

			query.append(_FINDER_COLUMN_CLASSROUTINEEVENTID_CLASSROUTINEEVENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(classRoutineEventId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CLASSROUTINEEVENTID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of class routine event batchs.
	 *
	 * @return the number of class routine event batchs
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLASSROUTINEEVENTBATCH);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the class routine event batch persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.classroutine.model.ClassRoutineEventBatch")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ClassRoutineEventBatch>> listenersList = new ArrayList<ModelListener<ClassRoutineEventBatch>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ClassRoutineEventBatch>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ClassRoutineEventBatchImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_CLASSROUTINEEVENTBATCH = "SELECT classRoutineEventBatch FROM ClassRoutineEventBatch classRoutineEventBatch";
	private static final String _SQL_SELECT_CLASSROUTINEEVENTBATCH_WHERE = "SELECT classRoutineEventBatch FROM ClassRoutineEventBatch classRoutineEventBatch WHERE ";
	private static final String _SQL_COUNT_CLASSROUTINEEVENTBATCH = "SELECT COUNT(classRoutineEventBatch) FROM ClassRoutineEventBatch classRoutineEventBatch";
	private static final String _SQL_COUNT_CLASSROUTINEEVENTBATCH_WHERE = "SELECT COUNT(classRoutineEventBatch) FROM ClassRoutineEventBatch classRoutineEventBatch WHERE ";
	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "classRoutineEventBatch.companyId = ?";
	private static final String _FINDER_COLUMN_BATCHID_BATCHID_2 = "classRoutineEventBatch.batchId = ?";
	private static final String _FINDER_COLUMN_CLASSROUTINEEVENTID_CLASSROUTINEEVENTID_2 =
		"classRoutineEventBatch.classRoutineEventId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "classRoutineEventBatch.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ClassRoutineEventBatch exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ClassRoutineEventBatch exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ClassRoutineEventBatchPersistenceImpl.class);
	private static ClassRoutineEventBatch _nullClassRoutineEventBatch = new ClassRoutineEventBatchImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ClassRoutineEventBatch> toCacheModel() {
				return _nullClassRoutineEventBatchCacheModel;
			}
		};

	private static CacheModel<ClassRoutineEventBatch> _nullClassRoutineEventBatchCacheModel =
		new CacheModel<ClassRoutineEventBatch>() {
			public ClassRoutineEventBatch toEntityModel() {
				return _nullClassRoutineEventBatch;
			}
		};
}
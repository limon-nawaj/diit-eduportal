package info.diit.portal.classroutine.dto;

import java.io.Serializable;
import java.util.Date;

public class BatchSubjectDto implements Serializable {
	
	private long orgnizationId;	
	private long courseId;
	private long batchId;
	private long subjectId;
	private String subjcetName;
	private long teacherId;
	private String teacherName;
	private Date startDate;
	private int hours;
	
	
	public long getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(long subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjcetName() {
		return subjcetName;
	}
	public void setSubjcetName(String subjcetName) {
		this.subjcetName = subjcetName;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public int getHours() {
		return hours;
	}
	public void setHours(int hours) {
		this.hours = hours;
	}
	public long getOrgnizationId() {
		return orgnizationId;
	}
	public void setOrgnizationId(long orgnizationId) {
		this.orgnizationId = orgnizationId;
	}
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public long getBatchId() {
		return batchId;
	}
	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}
	public long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(long teacherId) {
		this.teacherId = teacherId;
	}
	
	public String toString(){
		return getSubjcetName();
	}
	
}

package info.diit.portal.classroutine.calendar.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class RoutineDto {

	int	day;
	String dayName;
	Date	start;
	Date 	end;
	long room;/*
	long companyId;
	long organizationId;
	long subjectId;*/
	
	List<Long> batch = new ArrayList<Long>();
	
	public List<Long> getBatch() {
		return batch;
	}
	public void setBatch(List<Long> batch) {
		this.batch = batch;
	}
	public String getDayName() {
		return dayName;
	}
	public void setDayName(String dayName) {
		this.dayName = dayName;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	public long getRoom() {
		return room;
	}
	public void setRoom(long room) {
		this.room = room;
	}
	
}

/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.classroutine.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.classroutine.model.Room;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Room in entity cache.
 *
 * @author saeid
 * @see Room
 * @generated
 */
public class RoomCacheModel implements CacheModel<Room>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{roomId=");
		sb.append(roomId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", label=");
		sb.append(label);
		sb.append(", description=");
		sb.append(description);
		sb.append(", roomType=");
		sb.append(roomType);
		sb.append("}");

		return sb.toString();
	}

	public Room toEntityModel() {
		RoomImpl roomImpl = new RoomImpl();

		roomImpl.setRoomId(roomId);
		roomImpl.setCompanyId(companyId);
		roomImpl.setOrganizationId(organizationId);
		roomImpl.setUserId(userId);

		if (userName == null) {
			roomImpl.setUserName(StringPool.BLANK);
		}
		else {
			roomImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			roomImpl.setCreateDate(null);
		}
		else {
			roomImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			roomImpl.setModifiedDate(null);
		}
		else {
			roomImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (label == null) {
			roomImpl.setLabel(StringPool.BLANK);
		}
		else {
			roomImpl.setLabel(label);
		}

		if (description == null) {
			roomImpl.setDescription(StringPool.BLANK);
		}
		else {
			roomImpl.setDescription(description);
		}

		roomImpl.setRoomType(roomType);

		roomImpl.resetOriginalValues();

		return roomImpl;
	}

	public long roomId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String label;
	public String description;
	public int roomType;
}
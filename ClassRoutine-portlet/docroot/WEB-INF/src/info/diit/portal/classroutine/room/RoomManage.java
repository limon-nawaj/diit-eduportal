package info.diit.portal.classroutine.room;


import info.diit.portal.classroutine.calendar.dto.CampusDto;
import info.diit.portal.classroutine.model.Room;
import info.diit.portal.classroutine.model.impl.RoomImpl;
import info.diit.portal.classroutine.room.dto.RoomDto;
import info.diit.portal.classroutine.room.enums.RoomType;
import info.diit.portal.classroutine.service.RoomLocalServiceUtil;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.vaadin.dialogs.ConfirmDialog;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.OrganizationLocalService;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

public class RoomManage extends Application implements PortletRequestListener {

	
	private Window window;
	private ThemeDisplay themeDisplay;
	
	private final static String LABEL = "label";
	private final static String DESCRIPTION = "description";
	private final static String TYPE = "type";
	private final static String CAMPUS = "campus";
	
	private Room room;
	
	private List<Organization> userOrganizationsList;
	private List<CampusDto> campusList;
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        try {
			userOrganizationsList	=	themeDisplay.getUser().getOrganizations();
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
        loadCampus();
        window.addComponent(mainLayout());
        loadRoom();
    }

	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}
	
	private ComboBox campusComboBox;
	private TextField labelField;
	private TextArea descriptionArea;
	private ComboBox typeComboBox;
	private Table roomTable;
	private BeanItemContainer<RoomDto> roomContainer;
	
	private GridLayout mainLayout(){
		GridLayout mainLayout = new GridLayout(8, 8);
		mainLayout.setWidth("100%");
		mainLayout.setSpacing(true);
		
		campusComboBox = new ComboBox("Campus");
		labelField = new TextField("Room Label");
		descriptionArea = new TextArea("Description");
		typeComboBox = new ComboBox("Type");
		
		if (campusList!=null) {
			for (CampusDto campus : campusList) {
				campusComboBox.addItem(campus);
			}
		}
		
		roomContainer = new BeanItemContainer<RoomDto>(RoomDto.class);
		roomTable = new Table("Room List", roomContainer);
		
		campusComboBox.setWidth("100%");
		labelField.setWidth("100%");
		descriptionArea.setWidth("100%");
		typeComboBox.setWidth("100%");
		roomTable.setWidth("100%");
		
		campusComboBox.setRequired(true);
		labelField.setRequired(true);
		typeComboBox.setRequired(true);
		typeComboBox.setImmediate(true);
		typeComboBox.setNullSelectionAllowed(false);
		
		roomTable.setSelectable(true);
		
		roomTable.setColumnHeader(LABEL, "Room Label");
		roomTable.setColumnHeader(DESCRIPTION, "Description");
		roomTable.setColumnHeader(TYPE, "Type");
		roomTable.setColumnHeader(CAMPUS, "Campu");
		
		if (userOrganizationsList.size()>1) {
			roomTable.setVisibleColumns(new String[]{CAMPUS, LABEL, TYPE, DESCRIPTION});
		}else{
			roomTable.setVisibleColumns(new String[]{LABEL, TYPE, DESCRIPTION});
		}
		
		for (RoomType roomType : RoomType.values()) {
			typeComboBox.addItem(roomType);
		}
		typeComboBox.select(getRoomType(1));
		
		Button saveButton = new Button("Save");
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (room==null) {
					room = new RoomImpl();
					room.setNew(true);
				}
				
				room.setCompanyId(themeDisplay.getCompany().getCompanyId());
				room.setUserId(themeDisplay.getUser().getUserId());
				room.setUserName(themeDisplay.getUser().getFullName());
				if (userOrganizationsList.size()>1) {
					CampusDto campus = (CampusDto) campusComboBox.getValue();
					if (campus!=null) {
						room.setOrganizationId(campus.getCampusId());
					} else {
						window.showNotification("Please select a campus!", Window.Notification.TYPE_ERROR_MESSAGE);
						return;
					}
				}else {
					Organization org = userOrganizationsList.get(0);
					room.setOrganizationId(org.getOrganizationId());
				}
						
				
				String roomLabel = (String) labelField.getValue();
				if (roomLabel!=null && !roomLabel.equals("")) {
					room.setLabel(roomLabel.trim());
				}else {
					window.showNotification("Room Label cannot be empty!", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				String description = (String) descriptionArea.getValue();
				if (description!=null) {
					room.setDescription(description);
				}
				
				RoomType roomType = (RoomType) typeComboBox.getValue();
				if (roomType!=null) {
					room.setRoomType(roomType.getKey());
				}else {
					window.showNotification("Please select a room", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				try {
					if (room.isNew()) {
						if (!checkLabel(roomLabel)) {
							RoomLocalServiceUtil.addRoom(room);
							window.showNotification("Room save successfully");
							loadRoom();
							clear();
						}else {
							window.showNotification("Room label already exist", Window.Notification.TYPE_ERROR_MESSAGE);
						}
						
					}else{
						if (!checkLabel(roomLabel)) {
							RoomLocalServiceUtil.updateRoom(room);
							window.showNotification("Room update successfully");
							loadRoom();
							clear();
						}else {
							window.showNotification("Room label already exist", Window.Notification.TYPE_ERROR_MESSAGE);
						}
					}
					
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
		
		Button clearButton = new Button("Clear");
		clearButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				clear();
			}
		});
		
		HorizontalLayout formLayout = new HorizontalLayout();
		formLayout.setSpacing(true);
		formLayout.addComponent(saveButton);
		formLayout.addComponent(clearButton);
		
		HorizontalLayout rowLayout = new HorizontalLayout();
		rowLayout.setSpacing(true);
		rowLayout.setWidth("100%");
		Label spacer = new Label();
		Button editButton = new Button("Edit");
		editButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				RoomDto roomDto = (RoomDto) roomTable.getValue();
				if (roomDto!=null) {
					editRoom(roomDto.getId());
				}else {
					window.showNotification("Please select a room!", Window.Notification.TYPE_ERROR_MESSAGE);
				}
			}
		});
		Button deleteButton = new Button("Delete");
		deleteButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				ConfirmDialog.show(window, "Are you sure you want to delete this record?", new ConfirmDialog.Listener() {
					
					@Override
					public void onClose(ConfirmDialog arg0) {
						RoomDto roomDto = (RoomDto) roomTable.getValue();
						if (roomDto!=null) {
							try {
								RoomLocalServiceUtil.deleteRoom(roomDto.getId());
								clear();
								loadRoom();
							} catch (PortalException e) {
								e.printStackTrace();
							} catch (SystemException e) {
								e.printStackTrace();
							}
						}else {
							window.showNotification("Please select a room!", Window.Notification.TYPE_ERROR_MESSAGE);
						}
					}
				});
			}
		});
		
		rowLayout.addComponent(spacer);
		rowLayout.addComponent(editButton);
		rowLayout.addComponent(deleteButton);
		
		rowLayout.setExpandRatio(spacer, 1);
		
		if (userOrganizationsList.size()>1) {
			mainLayout.addComponent(campusComboBox, 0, 0, 2, 0);
			mainLayout.addComponent(labelField, 0, 1, 2, 1);
			mainLayout.addComponent(descriptionArea, 0, 2, 2, 2);
			mainLayout.addComponent(typeComboBox, 0, 3, 2, 3);
			mainLayout.addComponent(formLayout, 0, 4, 2, 4);
			
			mainLayout.addComponent(roomTable, 3, 0, 7, 6);
			mainLayout.addComponent(rowLayout, 3, 7, 7, 7);
		}else{
			mainLayout.addComponent(labelField, 0, 0, 2, 0);
			mainLayout.addComponent(descriptionArea, 0, 1, 2, 1);
			mainLayout.addComponent(typeComboBox, 0, 2, 2, 2);
			mainLayout.addComponent(formLayout, 0, 3, 2, 3);
			
			mainLayout.addComponent(roomTable, 3, 0, 7, 6);
			mainLayout.addComponent(rowLayout, 3, 7, 7, 7);
		}
		
		return mainLayout;
	}
	
	private void editRoom(long roomId){
		try {
			room = RoomLocalServiceUtil.fetchRoom(roomId);
			if (room!=null) {
				campusComboBox.setValue(getCampus(room.getOrganizationId()));
				labelField.setValue(room.getLabel());
				descriptionArea.setValue(room.getDescription());
				typeComboBox.setValue(getRoomType(room.getRoomType()));
			} else {
				window.showNotification("Please select a room", Window.Notification.TYPE_ERROR_MESSAGE);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private CampusDto getCampus(long campusId){
		if (campusList!=null) {
			for (CampusDto campus : campusList) {
				if (campus.getCampusId()==campusId) {
					return campus;
				}
			}
		}
		return null;
	}
	
	private void loadCampus(){
		if (userOrganizationsList!=null) {
			campusList = new ArrayList<CampusDto>();
			for (Organization campus : userOrganizationsList) {
				CampusDto campusDto = new CampusDto();
				campusDto.setCampusId(campus.getOrganizationId());
				campusDto.setCampusName(campus.getName());
				campusList.add(campusDto);
			}
		}
	}
	
	private boolean checkLabel(String label){
		boolean status = true;
		try {
			List<Room> roomLabels = RoomLocalServiceUtil.findByOrganizationLabel(themeDisplay.getLayout().getGroup().getOrganizationId(), label.trim());
			if (roomLabels.size()>0) {
				for (Room r : roomLabels) {
					if (room.getRoomId()!=r.getRoomId()) {
						status = true;
					}else {
						status = false;
					}
				}
			}else {
				status = false;
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
		return status;
	}
	
	private void loadRoom(){
		try {
			if (roomContainer!=null) {
				roomContainer.removeAllItems();
			}
			
			if (userOrganizationsList!=null) {
				for (Organization organization : userOrganizationsList) {
					List<Room> roomList = RoomLocalServiceUtil.findByOrganizationId(organization.getOrganizationId());
					if (roomList!=null) {
						for (Room room : roomList) {
							RoomDto roomDto = new RoomDto();
							roomDto.setId(room.getRoomId());
							roomDto.setLabel(room.getLabel());
							roomDto.setType(getRoomType(room.getRoomType()));
							roomDto.setDescription(room.getDescription());
							
							CampusDto campus = new CampusDto();
							Organization org = OrganizationLocalServiceUtil.fetchOrganization(room.getOrganizationId());
							campus.setCampusId(org.getOrganizationId());
							campus.setCampusName(org.getName());
							roomDto.setCampus(campus);
							
							roomContainer.addBean(roomDto);
						}
					}
				}
				
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	}
	
	private RoomType getRoomType(long type){
		if (RoomType.values()!=null) {
			for (RoomType roomType : RoomType.values()) {
				if (roomType.getKey()==type) {
					return roomType;
				}
			}
		}
		return null;
	}
	
	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}
	
	private void clear(){
		room = null;
		campusComboBox.setValue(null);
		labelField.setValue("");
		descriptionArea.setValue("");
		typeComboBox.setValue(getRoomType(1));
	}

}

package info.diit.portal.classroutine.room.enums;


public enum RoomType {

	CLASS_ROOM(1, "Class Room"),
	LAB(2, "LAB"),
	OFFICE_ROOM(3, "Office Room"),
	CONFERENCE_ROOM(4, "Conference Room"),
	SEMINAR(5, "Seminar");
	private int key;
	private String value;
	private RoomType(int key, String value) {
		this.key = key;
		this.value = value;
	}
	public int getKey() {
		return key;
	}
	public String getValue() {
		return value;
	}
	public String toString(){
		return value;
	}
	
	public static RoomType getRoomType(int key){
		RoomType types[] = RoomType.values();
		for (RoomType roomType : types) {
			if (roomType.key==key) {
				return roomType;
			}
		}
		return null;
	}
}

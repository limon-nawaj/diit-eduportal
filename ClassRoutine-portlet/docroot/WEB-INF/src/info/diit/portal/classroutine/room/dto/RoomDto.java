package info.diit.portal.classroutine.room.dto;

import info.diit.portal.classroutine.calendar.dto.CampusDto;
import info.diit.portal.classroutine.room.enums.RoomType;

public class RoomDto {

	private long id;
	private String label;
	private String description;
	private RoomType type;
	private CampusDto campus;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public RoomType getType() {
		return type;
	}
	public void setType(RoomType type) {
		this.type = type;
	}
	public CampusDto getCampus() {
		return campus;
	}
	public void setCampus(CampusDto campus) {
		this.campus = campus;
	}
}

package info.diit.portal.classroutine.dto;

public class RoomDto {
	long roomId;
	String roomLabel;
	public long getRoomId() {
		return roomId;
	}
	public void setRoomId(long roomId) {
		this.roomId = roomId;
	}
	

	public String getRoomLabel() {
		return roomLabel;
	}
	public void setRoomLabel(String roomLabel) {
		this.roomLabel = roomLabel;
	}
	public String toString(){
		return getRoomLabel();
	}
}

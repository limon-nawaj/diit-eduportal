/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.classroutine.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.classroutine.NoSuchRoomException;
import info.diit.portal.classroutine.model.Room;
import info.diit.portal.classroutine.model.impl.RoomImpl;
import info.diit.portal.classroutine.model.impl.RoomModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the room service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author saeid
 * @see RoomPersistence
 * @see RoomUtil
 * @generated
 */
public class RoomPersistenceImpl extends BasePersistenceImpl<Room>
	implements RoomPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link RoomUtil} to access the room persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = RoomImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_FETCH_BY_ROOM = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByRoom",
			new String[] { Long.class.getName() },
			RoomModelImpl.ROOMID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ROOM = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByRoom",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOrganization",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByOrganization",
			new String[] { Long.class.getName() },
			RoomModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATION = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOrganization",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATIONLABEL =
		new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOrganizationLabel",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONLABEL =
		new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByOrganizationLabel",
			new String[] { Long.class.getName(), String.class.getName() },
			RoomModelImpl.ORGANIZATIONID_COLUMN_BITMASK |
			RoomModelImpl.LABEL_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATIONLABEL = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByOrganizationLabel",
			new String[] { Long.class.getName(), String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompany",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY =
		new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompany",
			new String[] { Long.class.getName() },
			RoomModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANY = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompany",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the room in the entity cache if it is enabled.
	 *
	 * @param room the room
	 */
	public void cacheResult(Room room) {
		EntityCacheUtil.putResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomImpl.class, room.getPrimaryKey(), room);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ROOM,
			new Object[] { Long.valueOf(room.getRoomId()) }, room);

		room.resetOriginalValues();
	}

	/**
	 * Caches the rooms in the entity cache if it is enabled.
	 *
	 * @param rooms the rooms
	 */
	public void cacheResult(List<Room> rooms) {
		for (Room room : rooms) {
			if (EntityCacheUtil.getResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
						RoomImpl.class, room.getPrimaryKey()) == null) {
				cacheResult(room);
			}
			else {
				room.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all rooms.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(RoomImpl.class.getName());
		}

		EntityCacheUtil.clearCache(RoomImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the room.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Room room) {
		EntityCacheUtil.removeResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomImpl.class, room.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(room);
	}

	@Override
	public void clearCache(List<Room> rooms) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Room room : rooms) {
			EntityCacheUtil.removeResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
				RoomImpl.class, room.getPrimaryKey());

			clearUniqueFindersCache(room);
		}
	}

	protected void clearUniqueFindersCache(Room room) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ROOM,
			new Object[] { Long.valueOf(room.getRoomId()) });
	}

	/**
	 * Creates a new room with the primary key. Does not add the room to the database.
	 *
	 * @param roomId the primary key for the new room
	 * @return the new room
	 */
	public Room create(long roomId) {
		Room room = new RoomImpl();

		room.setNew(true);
		room.setPrimaryKey(roomId);

		return room;
	}

	/**
	 * Removes the room with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param roomId the primary key of the room
	 * @return the room that was removed
	 * @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room remove(long roomId) throws NoSuchRoomException, SystemException {
		return remove(Long.valueOf(roomId));
	}

	/**
	 * Removes the room with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the room
	 * @return the room that was removed
	 * @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Room remove(Serializable primaryKey)
		throws NoSuchRoomException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Room room = (Room)session.get(RoomImpl.class, primaryKey);

			if (room == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchRoomException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(room);
		}
		catch (NoSuchRoomException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Room removeImpl(Room room) throws SystemException {
		room = toUnwrappedModel(room);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, room);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(room);

		return room;
	}

	@Override
	public Room updateImpl(info.diit.portal.classroutine.model.Room room,
		boolean merge) throws SystemException {
		room = toUnwrappedModel(room);

		boolean isNew = room.isNew();

		RoomModelImpl roomModelImpl = (RoomModelImpl)room;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, room, merge);

			room.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !RoomModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((roomModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(roomModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(roomModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);
			}

			if ((roomModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONLABEL.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(roomModelImpl.getOriginalOrganizationId()),
						
						roomModelImpl.getOriginalLabel()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATIONLABEL,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONLABEL,
					args);

				args = new Object[] {
						Long.valueOf(roomModelImpl.getOrganizationId()),
						
						roomModelImpl.getLabel()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATIONLABEL,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONLABEL,
					args);
			}

			if ((roomModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(roomModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);

				args = new Object[] { Long.valueOf(roomModelImpl.getCompanyId()) };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);
			}
		}

		EntityCacheUtil.putResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
			RoomImpl.class, room.getPrimaryKey(), room);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ROOM,
				new Object[] { Long.valueOf(room.getRoomId()) }, room);
		}
		else {
			if ((roomModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_ROOM.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(roomModelImpl.getOriginalRoomId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ROOM, args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ROOM, args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ROOM,
					new Object[] { Long.valueOf(room.getRoomId()) }, room);
			}
		}

		return room;
	}

	protected Room toUnwrappedModel(Room room) {
		if (room instanceof RoomImpl) {
			return room;
		}

		RoomImpl roomImpl = new RoomImpl();

		roomImpl.setNew(room.isNew());
		roomImpl.setPrimaryKey(room.getPrimaryKey());

		roomImpl.setRoomId(room.getRoomId());
		roomImpl.setCompanyId(room.getCompanyId());
		roomImpl.setOrganizationId(room.getOrganizationId());
		roomImpl.setUserId(room.getUserId());
		roomImpl.setUserName(room.getUserName());
		roomImpl.setCreateDate(room.getCreateDate());
		roomImpl.setModifiedDate(room.getModifiedDate());
		roomImpl.setLabel(room.getLabel());
		roomImpl.setDescription(room.getDescription());
		roomImpl.setRoomType(room.getRoomType());

		return roomImpl;
	}

	/**
	 * Returns the room with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the room
	 * @return the room
	 * @throws com.liferay.portal.NoSuchModelException if a room with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Room findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the room with the primary key or throws a {@link info.diit.portal.classroutine.NoSuchRoomException} if it could not be found.
	 *
	 * @param roomId the primary key of the room
	 * @return the room
	 * @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room findByPrimaryKey(long roomId)
		throws NoSuchRoomException, SystemException {
		Room room = fetchByPrimaryKey(roomId);

		if (room == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + roomId);
			}

			throw new NoSuchRoomException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				roomId);
		}

		return room;
	}

	/**
	 * Returns the room with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the room
	 * @return the room, or <code>null</code> if a room with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Room fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the room with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param roomId the primary key of the room
	 * @return the room, or <code>null</code> if a room with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room fetchByPrimaryKey(long roomId) throws SystemException {
		Room room = (Room)EntityCacheUtil.getResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
				RoomImpl.class, roomId);

		if (room == _nullRoom) {
			return null;
		}

		if (room == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				room = (Room)session.get(RoomImpl.class, Long.valueOf(roomId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (room != null) {
					cacheResult(room);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
						RoomImpl.class, roomId, _nullRoom);
				}

				closeSession(session);
			}
		}

		return room;
	}

	/**
	 * Returns the room where roomId = &#63; or throws a {@link info.diit.portal.classroutine.NoSuchRoomException} if it could not be found.
	 *
	 * @param roomId the room ID
	 * @return the matching room
	 * @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room findByRoom(long roomId)
		throws NoSuchRoomException, SystemException {
		Room room = fetchByRoom(roomId);

		if (room == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("roomId=");
			msg.append(roomId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchRoomException(msg.toString());
		}

		return room;
	}

	/**
	 * Returns the room where roomId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param roomId the room ID
	 * @return the matching room, or <code>null</code> if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room fetchByRoom(long roomId) throws SystemException {
		return fetchByRoom(roomId, true);
	}

	/**
	 * Returns the room where roomId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param roomId the room ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching room, or <code>null</code> if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room fetchByRoom(long roomId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { roomId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_ROOM,
					finderArgs, this);
		}

		if (result instanceof Room) {
			Room room = (Room)result;

			if ((roomId != room.getRoomId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_ROOM_WHERE);

			query.append(_FINDER_COLUMN_ROOM_ROOMID_2);

			query.append(RoomModelImpl.ORDER_BY_JPQL);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(roomId);

				List<Room> list = q.list();

				result = list;

				Room room = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ROOM,
						finderArgs, list);
				}
				else {
					room = list.get(0);

					cacheResult(room);

					if ((room.getRoomId() != roomId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ROOM,
							finderArgs, room);
					}
				}

				return room;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ROOM,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (Room)result;
			}
		}
	}

	/**
	 * Returns all the rooms where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching rooms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Room> findByOrganization(long organizationId)
		throws SystemException {
		return findByOrganization(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the rooms where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of rooms
	 * @param end the upper bound of the range of rooms (not inclusive)
	 * @return the range of matching rooms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Room> findByOrganization(long organizationId, int start, int end)
		throws SystemException {
		return findByOrganization(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the rooms where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of rooms
	 * @param end the upper bound of the range of rooms (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rooms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Room> findByOrganization(long organizationId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<Room> list = (List<Room>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Room room : list) {
				if ((organizationId != room.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ROOM_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(RoomModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				list = (List<Room>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first room in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching room
	 * @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room findByOrganization_First(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchRoomException, SystemException {
		Room room = fetchByOrganization_First(organizationId, orderByComparator);

		if (room != null) {
			return room;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchRoomException(msg.toString());
	}

	/**
	 * Returns the first room in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching room, or <code>null</code> if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room fetchByOrganization_First(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Room> list = findByOrganization(organizationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last room in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching room
	 * @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room findByOrganization_Last(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchRoomException, SystemException {
		Room room = fetchByOrganization_Last(organizationId, orderByComparator);

		if (room != null) {
			return room;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchRoomException(msg.toString());
	}

	/**
	 * Returns the last room in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching room, or <code>null</code> if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room fetchByOrganization_Last(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByOrganization(organizationId);

		List<Room> list = findByOrganization(organizationId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the rooms before and after the current room in the ordered set where organizationId = &#63;.
	 *
	 * @param roomId the primary key of the current room
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next room
	 * @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room[] findByOrganization_PrevAndNext(long roomId,
		long organizationId, OrderByComparator orderByComparator)
		throws NoSuchRoomException, SystemException {
		Room room = findByPrimaryKey(roomId);

		Session session = null;

		try {
			session = openSession();

			Room[] array = new RoomImpl[3];

			array[0] = getByOrganization_PrevAndNext(session, room,
					organizationId, orderByComparator, true);

			array[1] = room;

			array[2] = getByOrganization_PrevAndNext(session, room,
					organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Room getByOrganization_PrevAndNext(Session session, Room room,
		long organizationId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ROOM_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(RoomModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(room);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Room> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the rooms where organizationId = &#63; and label = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param label the label
	 * @return the matching rooms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Room> findByOrganizationLabel(long organizationId, String label)
		throws SystemException {
		return findByOrganizationLabel(organizationId, label,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the rooms where organizationId = &#63; and label = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param label the label
	 * @param start the lower bound of the range of rooms
	 * @param end the upper bound of the range of rooms (not inclusive)
	 * @return the range of matching rooms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Room> findByOrganizationLabel(long organizationId,
		String label, int start, int end) throws SystemException {
		return findByOrganizationLabel(organizationId, label, start, end, null);
	}

	/**
	 * Returns an ordered range of all the rooms where organizationId = &#63; and label = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param label the label
	 * @param start the lower bound of the range of rooms
	 * @param end the upper bound of the range of rooms (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rooms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Room> findByOrganizationLabel(long organizationId,
		String label, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONLABEL;
			finderArgs = new Object[] { organizationId, label };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATIONLABEL;
			finderArgs = new Object[] {
					organizationId, label,
					
					start, end, orderByComparator
				};
		}

		List<Room> list = (List<Room>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Room room : list) {
				if ((organizationId != room.getOrganizationId()) ||
						!Validator.equals(label, room.getLabel())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ROOM_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATIONLABEL_ORGANIZATIONID_2);

			if (label == null) {
				query.append(_FINDER_COLUMN_ORGANIZATIONLABEL_LABEL_1);
			}
			else {
				if (label.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_ORGANIZATIONLABEL_LABEL_3);
				}
				else {
					query.append(_FINDER_COLUMN_ORGANIZATIONLABEL_LABEL_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(RoomModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				if (label != null) {
					qPos.add(label);
				}

				list = (List<Room>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first room in the ordered set where organizationId = &#63; and label = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param label the label
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching room
	 * @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room findByOrganizationLabel_First(long organizationId,
		String label, OrderByComparator orderByComparator)
		throws NoSuchRoomException, SystemException {
		Room room = fetchByOrganizationLabel_First(organizationId, label,
				orderByComparator);

		if (room != null) {
			return room;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(", label=");
		msg.append(label);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchRoomException(msg.toString());
	}

	/**
	 * Returns the first room in the ordered set where organizationId = &#63; and label = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param label the label
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching room, or <code>null</code> if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room fetchByOrganizationLabel_First(long organizationId,
		String label, OrderByComparator orderByComparator)
		throws SystemException {
		List<Room> list = findByOrganizationLabel(organizationId, label, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last room in the ordered set where organizationId = &#63; and label = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param label the label
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching room
	 * @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room findByOrganizationLabel_Last(long organizationId, String label,
		OrderByComparator orderByComparator)
		throws NoSuchRoomException, SystemException {
		Room room = fetchByOrganizationLabel_Last(organizationId, label,
				orderByComparator);

		if (room != null) {
			return room;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(", label=");
		msg.append(label);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchRoomException(msg.toString());
	}

	/**
	 * Returns the last room in the ordered set where organizationId = &#63; and label = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param label the label
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching room, or <code>null</code> if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room fetchByOrganizationLabel_Last(long organizationId,
		String label, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByOrganizationLabel(organizationId, label);

		List<Room> list = findByOrganizationLabel(organizationId, label,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the rooms before and after the current room in the ordered set where organizationId = &#63; and label = &#63;.
	 *
	 * @param roomId the primary key of the current room
	 * @param organizationId the organization ID
	 * @param label the label
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next room
	 * @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room[] findByOrganizationLabel_PrevAndNext(long roomId,
		long organizationId, String label, OrderByComparator orderByComparator)
		throws NoSuchRoomException, SystemException {
		Room room = findByPrimaryKey(roomId);

		Session session = null;

		try {
			session = openSession();

			Room[] array = new RoomImpl[3];

			array[0] = getByOrganizationLabel_PrevAndNext(session, room,
					organizationId, label, orderByComparator, true);

			array[1] = room;

			array[2] = getByOrganizationLabel_PrevAndNext(session, room,
					organizationId, label, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Room getByOrganizationLabel_PrevAndNext(Session session,
		Room room, long organizationId, String label,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ROOM_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATIONLABEL_ORGANIZATIONID_2);

		if (label == null) {
			query.append(_FINDER_COLUMN_ORGANIZATIONLABEL_LABEL_1);
		}
		else {
			if (label.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ORGANIZATIONLABEL_LABEL_3);
			}
			else {
				query.append(_FINDER_COLUMN_ORGANIZATIONLABEL_LABEL_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(RoomModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (label != null) {
			qPos.add(label);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(room);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Room> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the rooms where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching rooms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Room> findByCompany(long companyId) throws SystemException {
		return findByCompany(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the rooms where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of rooms
	 * @param end the upper bound of the range of rooms (not inclusive)
	 * @return the range of matching rooms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Room> findByCompany(long companyId, int start, int end)
		throws SystemException {
		return findByCompany(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the rooms where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of rooms
	 * @param end the upper bound of the range of rooms (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rooms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Room> findByCompany(long companyId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<Room> list = (List<Room>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Room room : list) {
				if ((companyId != room.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ROOM_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(RoomModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<Room>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first room in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching room
	 * @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room findByCompany_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchRoomException, SystemException {
		Room room = fetchByCompany_First(companyId, orderByComparator);

		if (room != null) {
			return room;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchRoomException(msg.toString());
	}

	/**
	 * Returns the first room in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching room, or <code>null</code> if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room fetchByCompany_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Room> list = findByCompany(companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last room in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching room
	 * @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room findByCompany_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchRoomException, SystemException {
		Room room = fetchByCompany_Last(companyId, orderByComparator);

		if (room != null) {
			return room;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchRoomException(msg.toString());
	}

	/**
	 * Returns the last room in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching room, or <code>null</code> if a matching room could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room fetchByCompany_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompany(companyId);

		List<Room> list = findByCompany(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the rooms before and after the current room in the ordered set where companyId = &#63;.
	 *
	 * @param roomId the primary key of the current room
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next room
	 * @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Room[] findByCompany_PrevAndNext(long roomId, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchRoomException, SystemException {
		Room room = findByPrimaryKey(roomId);

		Session session = null;

		try {
			session = openSession();

			Room[] array = new RoomImpl[3];

			array[0] = getByCompany_PrevAndNext(session, room, companyId,
					orderByComparator, true);

			array[1] = room;

			array[2] = getByCompany_PrevAndNext(session, room, companyId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Room getByCompany_PrevAndNext(Session session, Room room,
		long companyId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ROOM_WHERE);

		query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(RoomModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(room);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Room> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the rooms.
	 *
	 * @return the rooms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Room> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the rooms.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of rooms
	 * @param end the upper bound of the range of rooms (not inclusive)
	 * @return the range of rooms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Room> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the rooms.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of rooms
	 * @param end the upper bound of the range of rooms (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of rooms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Room> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Room> list = (List<Room>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ROOM);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ROOM.concat(RoomModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Room>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Room>)QueryUtil.list(q, getDialect(), start,
							end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes the room where roomId = &#63; from the database.
	 *
	 * @param roomId the room ID
	 * @return the room that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public Room removeByRoom(long roomId)
		throws NoSuchRoomException, SystemException {
		Room room = findByRoom(roomId);

		return remove(room);
	}

	/**
	 * Removes all the rooms where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByOrganization(long organizationId)
		throws SystemException {
		for (Room room : findByOrganization(organizationId)) {
			remove(room);
		}
	}

	/**
	 * Removes all the rooms where organizationId = &#63; and label = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @param label the label
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByOrganizationLabel(long organizationId, String label)
		throws SystemException {
		for (Room room : findByOrganizationLabel(organizationId, label)) {
			remove(room);
		}
	}

	/**
	 * Removes all the rooms where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCompany(long companyId) throws SystemException {
		for (Room room : findByCompany(companyId)) {
			remove(room);
		}
	}

	/**
	 * Removes all the rooms from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Room room : findAll()) {
			remove(room);
		}
	}

	/**
	 * Returns the number of rooms where roomId = &#63;.
	 *
	 * @param roomId the room ID
	 * @return the number of matching rooms
	 * @throws SystemException if a system exception occurred
	 */
	public int countByRoom(long roomId) throws SystemException {
		Object[] finderArgs = new Object[] { roomId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ROOM,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ROOM_WHERE);

			query.append(_FINDER_COLUMN_ROOM_ROOMID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(roomId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ROOM,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of rooms where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching rooms
	 * @throws SystemException if a system exception occurred
	 */
	public int countByOrganization(long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ROOM_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of rooms where organizationId = &#63; and label = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param label the label
	 * @return the number of matching rooms
	 * @throws SystemException if a system exception occurred
	 */
	public int countByOrganizationLabel(long organizationId, String label)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId, label };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATIONLABEL,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ROOM_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATIONLABEL_ORGANIZATIONID_2);

			if (label == null) {
				query.append(_FINDER_COLUMN_ORGANIZATIONLABEL_LABEL_1);
			}
			else {
				if (label.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_ORGANIZATIONLABEL_LABEL_3);
				}
				else {
					query.append(_FINDER_COLUMN_ORGANIZATIONLABEL_LABEL_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				if (label != null) {
					qPos.add(label);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATIONLABEL,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of rooms where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching rooms
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCompany(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ROOM_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of rooms.
	 *
	 * @return the number of rooms
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ROOM);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the room persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.classroutine.model.Room")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Room>> listenersList = new ArrayList<ModelListener<Room>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Room>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(RoomImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_ROOM = "SELECT room FROM Room room";
	private static final String _SQL_SELECT_ROOM_WHERE = "SELECT room FROM Room room WHERE ";
	private static final String _SQL_COUNT_ROOM = "SELECT COUNT(room) FROM Room room";
	private static final String _SQL_COUNT_ROOM_WHERE = "SELECT COUNT(room) FROM Room room WHERE ";
	private static final String _FINDER_COLUMN_ROOM_ROOMID_2 = "room.roomId = ?";
	private static final String _FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2 = "room.organizationId = ?";
	private static final String _FINDER_COLUMN_ORGANIZATIONLABEL_ORGANIZATIONID_2 =
		"room.organizationId = ? AND ";
	private static final String _FINDER_COLUMN_ORGANIZATIONLABEL_LABEL_1 = "room.label IS NULL";
	private static final String _FINDER_COLUMN_ORGANIZATIONLABEL_LABEL_2 = "room.label = ?";
	private static final String _FINDER_COLUMN_ORGANIZATIONLABEL_LABEL_3 = "(room.label IS NULL OR room.label = ?)";
	private static final String _FINDER_COLUMN_COMPANY_COMPANYID_2 = "room.companyId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "room.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Room exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Room exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(RoomPersistenceImpl.class);
	private static Room _nullRoom = new RoomImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Room> toCacheModel() {
				return _nullRoomCacheModel;
			}
		};

	private static CacheModel<Room> _nullRoomCacheModel = new CacheModel<Room>() {
			public Room toEntityModel() {
				return _nullRoom;
			}
		};
}
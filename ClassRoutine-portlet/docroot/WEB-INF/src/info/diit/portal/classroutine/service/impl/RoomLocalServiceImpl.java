/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.classroutine.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import info.diit.portal.classroutine.model.Room;
import info.diit.portal.classroutine.service.base.RoomLocalServiceBaseImpl;
import info.diit.portal.classroutine.service.persistence.RoomUtil;

/**
 * The implementation of the room local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.classroutine.service.RoomLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author saeid
 * @see info.diit.portal.classroutine.service.base.RoomLocalServiceBaseImpl
 * @see info.diit.portal.classroutine.service.RoomLocalServiceUtil
 */
public class RoomLocalServiceImpl extends RoomLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.classroutine.service.RoomLocalServiceUtil} to access the room local service.
	 */
	public List<Room> findByOrganizationLabel(long organizationId, String label) throws SystemException{
		return RoomUtil.findByOrganizationLabel(organizationId, label);
	}
	
	public List<Room> findByOrganizationId(long organizationId) throws SystemException{
		return RoomUtil.findByOrganization(organizationId);
	}
	
	public List<Room> findByCompanyId(long companyId) throws SystemException{
		return RoomUtil.findByCompany(companyId);
	}
}
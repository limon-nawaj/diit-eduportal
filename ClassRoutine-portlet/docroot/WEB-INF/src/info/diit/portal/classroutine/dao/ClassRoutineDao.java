package info.diit.portal.classroutine.dao;

import info.diit.portal.batch.model.Batch;
import info.diit.portal.batch.service.BatchLocalServiceUtil;
import info.diit.portal.classroutine.dto.BatchDto;
import info.diit.portal.classroutine.dto.CourseDTO;
import info.diit.portal.coursesubject.model.Course;
import info.diit.portal.coursesubject.model.CourseOrganization;
import info.diit.portal.coursesubject.service.CourseLocalServiceUtil;
import info.diit.portal.coursesubject.service.CourseOrganizationLocalServiceUtil;


import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

public class ClassRoutineDao {
	public static ArrayList<CourseDTO> getCourseListByCompany(long companyId) throws SystemException
	{
		List<Course> courses = CourseLocalServiceUtil.findByCompany(companyId);
		ArrayList<CourseDTO> courseList = new ArrayList<CourseDTO>(courses.size());
		
		for(Course course:courses)
		{
			CourseDTO courseDto = new CourseDTO();
			courseDto.setCourseCode(course.getCourseCode());
			courseDto.setCourseId(course.getCourseId());
			courseDto.setCourseName(course.getCourseName());
			
			courseList.add(courseDto);
		}
		
		return courseList;
	}
	
	public static ArrayList<CourseDTO> getCourseListByOrganization(long organizationId) throws SystemException
	{
		List<CourseOrganization> courseOrganizations = CourseOrganizationLocalServiceUtil.findByOrganization(organizationId);
		
		ArrayList<CourseDTO> courseList = new ArrayList<CourseDTO>(courseOrganizations.size());
		
		for(CourseOrganization courseOrganization:courseOrganizations)
		{
			long courseId = courseOrganization.getCourseId();
			Course course = CourseLocalServiceUtil.fetchCourse(courseId);
			
			if(course!=null)
			{
				CourseDTO courseDto = new CourseDTO();
				courseDto.setCourseId(course.getCourseId());
				courseDto.setCourseCode(course.getCourseCode());
				courseDto.setCourseName(course.getCourseName());
				
				courseList.add(courseDto);
			}
			
		}
		
		return courseList;
		
	}
	
	public static ArrayList<BatchDto> getBatchListByCourse(long organizationId,long courseId) throws SystemException
	{
		List<Batch> batchList = BatchLocalServiceUtil.findBatchesByOrgCourseId(organizationId, courseId);
		ArrayList<BatchDto> batchDtoList = new ArrayList<BatchDto>(batchList.size());
		
		for(Batch batch:batchList)
		{
			BatchDto batchDto = new BatchDto();
			batchDto.setBatchId(batch.getBatchId());
			batchDto.setBatchName(batch.getBatchName());
			
			batchDtoList.add(batchDto);
		}
		
		return batchDtoList;
	}

}

/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.classroutine.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link ClassRoutineEventLocalService}.
 * </p>
 *
 * @author    saeid
 * @see       ClassRoutineEventLocalService
 * @generated
 */
public class ClassRoutineEventLocalServiceWrapper
	implements ClassRoutineEventLocalService,
		ServiceWrapper<ClassRoutineEventLocalService> {
	public ClassRoutineEventLocalServiceWrapper(
		ClassRoutineEventLocalService classRoutineEventLocalService) {
		_classRoutineEventLocalService = classRoutineEventLocalService;
	}

	/**
	* Adds the class routine event to the database. Also notifies the appropriate model listeners.
	*
	* @param classRoutineEvent the class routine event
	* @return the class routine event that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.ClassRoutineEvent addClassRoutineEvent(
		info.diit.portal.classroutine.model.ClassRoutineEvent classRoutineEvent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.addClassRoutineEvent(classRoutineEvent);
	}

	/**
	* Creates a new class routine event with the primary key. Does not add the class routine event to the database.
	*
	* @param classRoutineEventId the primary key for the new class routine event
	* @return the new class routine event
	*/
	public info.diit.portal.classroutine.model.ClassRoutineEvent createClassRoutineEvent(
		long classRoutineEventId) {
		return _classRoutineEventLocalService.createClassRoutineEvent(classRoutineEventId);
	}

	/**
	* Deletes the class routine event with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param classRoutineEventId the primary key of the class routine event
	* @return the class routine event that was removed
	* @throws PortalException if a class routine event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.ClassRoutineEvent deleteClassRoutineEvent(
		long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.deleteClassRoutineEvent(classRoutineEventId);
	}

	/**
	* Deletes the class routine event from the database. Also notifies the appropriate model listeners.
	*
	* @param classRoutineEvent the class routine event
	* @return the class routine event that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.ClassRoutineEvent deleteClassRoutineEvent(
		info.diit.portal.classroutine.model.ClassRoutineEvent classRoutineEvent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.deleteClassRoutineEvent(classRoutineEvent);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _classRoutineEventLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.classroutine.model.ClassRoutineEvent fetchClassRoutineEvent(
		long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.fetchClassRoutineEvent(classRoutineEventId);
	}

	/**
	* Returns the class routine event with the primary key.
	*
	* @param classRoutineEventId the primary key of the class routine event
	* @return the class routine event
	* @throws PortalException if a class routine event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.ClassRoutineEvent getClassRoutineEvent(
		long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.getClassRoutineEvent(classRoutineEventId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the class routine events.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @return the range of class routine events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.classroutine.model.ClassRoutineEvent> getClassRoutineEvents(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.getClassRoutineEvents(start, end);
	}

	/**
	* Returns the number of class routine events.
	*
	* @return the number of class routine events
	* @throws SystemException if a system exception occurred
	*/
	public int getClassRoutineEventsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.getClassRoutineEventsCount();
	}

	/**
	* Updates the class routine event in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param classRoutineEvent the class routine event
	* @return the class routine event that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.ClassRoutineEvent updateClassRoutineEvent(
		info.diit.portal.classroutine.model.ClassRoutineEvent classRoutineEvent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.updateClassRoutineEvent(classRoutineEvent);
	}

	/**
	* Updates the class routine event in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param classRoutineEvent the class routine event
	* @param merge whether to merge the class routine event with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the class routine event that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.ClassRoutineEvent updateClassRoutineEvent(
		info.diit.portal.classroutine.model.ClassRoutineEvent classRoutineEvent,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.updateClassRoutineEvent(classRoutineEvent,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _classRoutineEventLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_classRoutineEventLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _classRoutineEventLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	public boolean saveClassEventWithBatches(
		info.diit.portal.classroutine.model.ClassRoutineEvent classRoutineEvent,
		java.util.List<info.diit.portal.classroutine.model.ClassRoutineEventBatch> eventBatchs)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.saveClassEventWithBatches(classRoutineEvent,
			eventBatchs);
	}

	public info.diit.portal.classroutine.model.ClassRoutineEvent findEventsBySubjectId(
		long subjectId, long eventId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.findEventsBySubjectId(subjectId,
			eventId);
	}

	public void deleteEvent(long eventId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchClassRoutineEventException {
		_classRoutineEventLocalService.deleteEvent(eventId);
	}

	public java.util.List<info.diit.portal.classroutine.model.ClassRoutineEvent> findByRoom(
		long roomId) throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.findByRoom(roomId);
	}

	public java.util.List<info.diit.portal.classroutine.model.ClassRoutineEvent> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.findByCompany(companyId);
	}

	public java.util.List<info.diit.portal.classroutine.model.ClassRoutineEvent> findBySubujectDay(
		long subjectId, int day)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventLocalService.findBySubujectDay(subjectId, day);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public ClassRoutineEventLocalService getWrappedClassRoutineEventLocalService() {
		return _classRoutineEventLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedClassRoutineEventLocalService(
		ClassRoutineEventLocalService classRoutineEventLocalService) {
		_classRoutineEventLocalService = classRoutineEventLocalService;
	}

	public ClassRoutineEventLocalService getWrappedService() {
		return _classRoutineEventLocalService;
	}

	public void setWrappedService(
		ClassRoutineEventLocalService classRoutineEventLocalService) {
		_classRoutineEventLocalService = classRoutineEventLocalService;
	}

	private ClassRoutineEventLocalService _classRoutineEventLocalService;
}
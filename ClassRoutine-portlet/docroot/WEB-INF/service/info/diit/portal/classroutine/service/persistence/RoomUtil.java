/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.classroutine.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.classroutine.model.Room;

import java.util.List;

/**
 * The persistence utility for the room service. This utility wraps {@link RoomPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author saeid
 * @see RoomPersistence
 * @see RoomPersistenceImpl
 * @generated
 */
public class RoomUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Room room) {
		getPersistence().clearCache(room);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Room> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Room> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Room> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Room update(Room room, boolean merge)
		throws SystemException {
		return getPersistence().update(room, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Room update(Room room, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(room, merge, serviceContext);
	}

	/**
	* Caches the room in the entity cache if it is enabled.
	*
	* @param room the room
	*/
	public static void cacheResult(
		info.diit.portal.classroutine.model.Room room) {
		getPersistence().cacheResult(room);
	}

	/**
	* Caches the rooms in the entity cache if it is enabled.
	*
	* @param rooms the rooms
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.classroutine.model.Room> rooms) {
		getPersistence().cacheResult(rooms);
	}

	/**
	* Creates a new room with the primary key. Does not add the room to the database.
	*
	* @param roomId the primary key for the new room
	* @return the new room
	*/
	public static info.diit.portal.classroutine.model.Room create(long roomId) {
		return getPersistence().create(roomId);
	}

	/**
	* Removes the room with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param roomId the primary key of the room
	* @return the room that was removed
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room remove(long roomId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException {
		return getPersistence().remove(roomId);
	}

	public static info.diit.portal.classroutine.model.Room updateImpl(
		info.diit.portal.classroutine.model.Room room, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(room, merge);
	}

	/**
	* Returns the room with the primary key or throws a {@link info.diit.portal.classroutine.NoSuchRoomException} if it could not be found.
	*
	* @param roomId the primary key of the room
	* @return the room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room findByPrimaryKey(
		long roomId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException {
		return getPersistence().findByPrimaryKey(roomId);
	}

	/**
	* Returns the room with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param roomId the primary key of the room
	* @return the room, or <code>null</code> if a room with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room fetchByPrimaryKey(
		long roomId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(roomId);
	}

	/**
	* Returns the room where roomId = &#63; or throws a {@link info.diit.portal.classroutine.NoSuchRoomException} if it could not be found.
	*
	* @param roomId the room ID
	* @return the matching room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room findByRoom(
		long roomId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException {
		return getPersistence().findByRoom(roomId);
	}

	/**
	* Returns the room where roomId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param roomId the room ID
	* @return the matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room fetchByRoom(
		long roomId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByRoom(roomId);
	}

	/**
	* Returns the room where roomId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param roomId the room ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room fetchByRoom(
		long roomId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByRoom(roomId, retrieveFromCache);
	}

	/**
	* Returns all the rooms where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.classroutine.model.Room> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganization(organizationId);
	}

	/**
	* Returns a range of all the rooms where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @return the range of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.classroutine.model.Room> findByOrganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganization(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the rooms where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.classroutine.model.Room> findByOrganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByOrganization(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first room in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room findByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException {
		return getPersistence()
				   .findByOrganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first room in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room fetchByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the last room in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room findByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException {
		return getPersistence()
				   .findByOrganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last room in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room fetchByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the rooms before and after the current room in the ordered set where organizationId = &#63;.
	*
	* @param roomId the primary key of the current room
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room[] findByOrganization_PrevAndNext(
		long roomId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException {
		return getPersistence()
				   .findByOrganization_PrevAndNext(roomId, organizationId,
			orderByComparator);
	}

	/**
	* Returns all the rooms where organizationId = &#63; and label = &#63;.
	*
	* @param organizationId the organization ID
	* @param label the label
	* @return the matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.classroutine.model.Room> findByOrganizationLabel(
		long organizationId, java.lang.String label)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganizationLabel(organizationId, label);
	}

	/**
	* Returns a range of all the rooms where organizationId = &#63; and label = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param label the label
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @return the range of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.classroutine.model.Room> findByOrganizationLabel(
		long organizationId, java.lang.String label, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByOrganizationLabel(organizationId, label, start, end);
	}

	/**
	* Returns an ordered range of all the rooms where organizationId = &#63; and label = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param label the label
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.classroutine.model.Room> findByOrganizationLabel(
		long organizationId, java.lang.String label, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByOrganizationLabel(organizationId, label, start, end,
			orderByComparator);
	}

	/**
	* Returns the first room in the ordered set where organizationId = &#63; and label = &#63;.
	*
	* @param organizationId the organization ID
	* @param label the label
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room findByOrganizationLabel_First(
		long organizationId, java.lang.String label,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException {
		return getPersistence()
				   .findByOrganizationLabel_First(organizationId, label,
			orderByComparator);
	}

	/**
	* Returns the first room in the ordered set where organizationId = &#63; and label = &#63;.
	*
	* @param organizationId the organization ID
	* @param label the label
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room fetchByOrganizationLabel_First(
		long organizationId, java.lang.String label,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganizationLabel_First(organizationId, label,
			orderByComparator);
	}

	/**
	* Returns the last room in the ordered set where organizationId = &#63; and label = &#63;.
	*
	* @param organizationId the organization ID
	* @param label the label
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room findByOrganizationLabel_Last(
		long organizationId, java.lang.String label,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException {
		return getPersistence()
				   .findByOrganizationLabel_Last(organizationId, label,
			orderByComparator);
	}

	/**
	* Returns the last room in the ordered set where organizationId = &#63; and label = &#63;.
	*
	* @param organizationId the organization ID
	* @param label the label
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room fetchByOrganizationLabel_Last(
		long organizationId, java.lang.String label,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganizationLabel_Last(organizationId, label,
			orderByComparator);
	}

	/**
	* Returns the rooms before and after the current room in the ordered set where organizationId = &#63; and label = &#63;.
	*
	* @param roomId the primary key of the current room
	* @param organizationId the organization ID
	* @param label the label
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room[] findByOrganizationLabel_PrevAndNext(
		long roomId, long organizationId, java.lang.String label,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException {
		return getPersistence()
				   .findByOrganizationLabel_PrevAndNext(roomId, organizationId,
			label, orderByComparator);
	}

	/**
	* Returns all the rooms where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.classroutine.model.Room> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId);
	}

	/**
	* Returns a range of all the rooms where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @return the range of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.classroutine.model.Room> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the rooms where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.classroutine.model.Room> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first room in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException {
		return getPersistence().findByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first room in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last room in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException {
		return getPersistence().findByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last room in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the rooms before and after the current room in the ordered set where companyId = &#63;.
	*
	* @param roomId the primary key of the current room
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room[] findByCompany_PrevAndNext(
		long roomId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException {
		return getPersistence()
				   .findByCompany_PrevAndNext(roomId, companyId,
			orderByComparator);
	}

	/**
	* Returns all the rooms.
	*
	* @return the rooms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.classroutine.model.Room> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the rooms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @return the range of rooms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.classroutine.model.Room> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the rooms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of rooms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.classroutine.model.Room> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes the room where roomId = &#63; from the database.
	*
	* @param roomId the room ID
	* @return the room that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.classroutine.model.Room removeByRoom(
		long roomId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException {
		return getPersistence().removeByRoom(roomId);
	}

	/**
	* Removes all the rooms where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByOrganization(organizationId);
	}

	/**
	* Removes all the rooms where organizationId = &#63; and label = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param label the label
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByOrganizationLabel(long organizationId,
		java.lang.String label)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByOrganizationLabel(organizationId, label);
	}

	/**
	* Removes all the rooms where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCompany(companyId);
	}

	/**
	* Removes all the rooms from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of rooms where roomId = &#63;.
	*
	* @param roomId the room ID
	* @return the number of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public static int countByRoom(long roomId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByRoom(roomId);
	}

	/**
	* Returns the number of rooms where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public static int countByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByOrganization(organizationId);
	}

	/**
	* Returns the number of rooms where organizationId = &#63; and label = &#63;.
	*
	* @param organizationId the organization ID
	* @param label the label
	* @return the number of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public static int countByOrganizationLabel(long organizationId,
		java.lang.String label)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByOrganizationLabel(organizationId, label);
	}

	/**
	* Returns the number of rooms where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCompany(companyId);
	}

	/**
	* Returns the number of rooms.
	*
	* @return the number of rooms
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static RoomPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (RoomPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.classroutine.service.ClpSerializer.getServletContextName(),
					RoomPersistence.class.getName());

			ReferenceRegistry.registerReference(RoomUtil.class, "_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(RoomPersistence persistence) {
	}

	private static RoomPersistence _persistence;
}
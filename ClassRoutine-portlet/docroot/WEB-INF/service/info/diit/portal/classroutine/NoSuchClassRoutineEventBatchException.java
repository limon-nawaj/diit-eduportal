/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.classroutine;

import com.liferay.portal.NoSuchModelException;

/**
 * @author saeid
 */
public class NoSuchClassRoutineEventBatchException extends NoSuchModelException {

	public NoSuchClassRoutineEventBatchException() {
		super();
	}

	public NoSuchClassRoutineEventBatchException(String msg) {
		super(msg);
	}

	public NoSuchClassRoutineEventBatchException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public NoSuchClassRoutineEventBatchException(Throwable cause) {
		super(cause);
	}

}
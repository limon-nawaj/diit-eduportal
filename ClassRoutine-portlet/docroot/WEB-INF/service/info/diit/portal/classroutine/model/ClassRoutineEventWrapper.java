/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.classroutine.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ClassRoutineEvent}.
 * </p>
 *
 * @author    saeid
 * @see       ClassRoutineEvent
 * @generated
 */
public class ClassRoutineEventWrapper implements ClassRoutineEvent,
	ModelWrapper<ClassRoutineEvent> {
	public ClassRoutineEventWrapper(ClassRoutineEvent classRoutineEvent) {
		_classRoutineEvent = classRoutineEvent;
	}

	public Class<?> getModelClass() {
		return ClassRoutineEvent.class;
	}

	public String getModelClassName() {
		return ClassRoutineEvent.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("classRoutineEventId", getClassRoutineEventId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("subjectId", getSubjectId());
		attributes.put("day", getDay());
		attributes.put("startTime", getStartTime());
		attributes.put("endTime", getEndTime());
		attributes.put("roomId", getRoomId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long classRoutineEventId = (Long)attributes.get("classRoutineEventId");

		if (classRoutineEventId != null) {
			setClassRoutineEventId(classRoutineEventId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long subjectId = (Long)attributes.get("subjectId");

		if (subjectId != null) {
			setSubjectId(subjectId);
		}

		Integer day = (Integer)attributes.get("day");

		if (day != null) {
			setDay(day);
		}

		Date startTime = (Date)attributes.get("startTime");

		if (startTime != null) {
			setStartTime(startTime);
		}

		Date endTime = (Date)attributes.get("endTime");

		if (endTime != null) {
			setEndTime(endTime);
		}

		Long roomId = (Long)attributes.get("roomId");

		if (roomId != null) {
			setRoomId(roomId);
		}
	}

	/**
	* Returns the primary key of this class routine event.
	*
	* @return the primary key of this class routine event
	*/
	public long getPrimaryKey() {
		return _classRoutineEvent.getPrimaryKey();
	}

	/**
	* Sets the primary key of this class routine event.
	*
	* @param primaryKey the primary key of this class routine event
	*/
	public void setPrimaryKey(long primaryKey) {
		_classRoutineEvent.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the class routine event ID of this class routine event.
	*
	* @return the class routine event ID of this class routine event
	*/
	public long getClassRoutineEventId() {
		return _classRoutineEvent.getClassRoutineEventId();
	}

	/**
	* Sets the class routine event ID of this class routine event.
	*
	* @param classRoutineEventId the class routine event ID of this class routine event
	*/
	public void setClassRoutineEventId(long classRoutineEventId) {
		_classRoutineEvent.setClassRoutineEventId(classRoutineEventId);
	}

	/**
	* Returns the company ID of this class routine event.
	*
	* @return the company ID of this class routine event
	*/
	public long getCompanyId() {
		return _classRoutineEvent.getCompanyId();
	}

	/**
	* Sets the company ID of this class routine event.
	*
	* @param companyId the company ID of this class routine event
	*/
	public void setCompanyId(long companyId) {
		_classRoutineEvent.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this class routine event.
	*
	* @return the user ID of this class routine event
	*/
	public long getUserId() {
		return _classRoutineEvent.getUserId();
	}

	/**
	* Sets the user ID of this class routine event.
	*
	* @param userId the user ID of this class routine event
	*/
	public void setUserId(long userId) {
		_classRoutineEvent.setUserId(userId);
	}

	/**
	* Returns the user uuid of this class routine event.
	*
	* @return the user uuid of this class routine event
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEvent.getUserUuid();
	}

	/**
	* Sets the user uuid of this class routine event.
	*
	* @param userUuid the user uuid of this class routine event
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_classRoutineEvent.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this class routine event.
	*
	* @return the user name of this class routine event
	*/
	public java.lang.String getUserName() {
		return _classRoutineEvent.getUserName();
	}

	/**
	* Sets the user name of this class routine event.
	*
	* @param userName the user name of this class routine event
	*/
	public void setUserName(java.lang.String userName) {
		_classRoutineEvent.setUserName(userName);
	}

	/**
	* Returns the create date of this class routine event.
	*
	* @return the create date of this class routine event
	*/
	public java.util.Date getCreateDate() {
		return _classRoutineEvent.getCreateDate();
	}

	/**
	* Sets the create date of this class routine event.
	*
	* @param createDate the create date of this class routine event
	*/
	public void setCreateDate(java.util.Date createDate) {
		_classRoutineEvent.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this class routine event.
	*
	* @return the modified date of this class routine event
	*/
	public java.util.Date getModifiedDate() {
		return _classRoutineEvent.getModifiedDate();
	}

	/**
	* Sets the modified date of this class routine event.
	*
	* @param modifiedDate the modified date of this class routine event
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_classRoutineEvent.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the organization ID of this class routine event.
	*
	* @return the organization ID of this class routine event
	*/
	public long getOrganizationId() {
		return _classRoutineEvent.getOrganizationId();
	}

	/**
	* Sets the organization ID of this class routine event.
	*
	* @param organizationId the organization ID of this class routine event
	*/
	public void setOrganizationId(long organizationId) {
		_classRoutineEvent.setOrganizationId(organizationId);
	}

	/**
	* Returns the subject ID of this class routine event.
	*
	* @return the subject ID of this class routine event
	*/
	public long getSubjectId() {
		return _classRoutineEvent.getSubjectId();
	}

	/**
	* Sets the subject ID of this class routine event.
	*
	* @param subjectId the subject ID of this class routine event
	*/
	public void setSubjectId(long subjectId) {
		_classRoutineEvent.setSubjectId(subjectId);
	}

	/**
	* Returns the day of this class routine event.
	*
	* @return the day of this class routine event
	*/
	public int getDay() {
		return _classRoutineEvent.getDay();
	}

	/**
	* Sets the day of this class routine event.
	*
	* @param day the day of this class routine event
	*/
	public void setDay(int day) {
		_classRoutineEvent.setDay(day);
	}

	/**
	* Returns the start time of this class routine event.
	*
	* @return the start time of this class routine event
	*/
	public java.util.Date getStartTime() {
		return _classRoutineEvent.getStartTime();
	}

	/**
	* Sets the start time of this class routine event.
	*
	* @param startTime the start time of this class routine event
	*/
	public void setStartTime(java.util.Date startTime) {
		_classRoutineEvent.setStartTime(startTime);
	}

	/**
	* Returns the end time of this class routine event.
	*
	* @return the end time of this class routine event
	*/
	public java.util.Date getEndTime() {
		return _classRoutineEvent.getEndTime();
	}

	/**
	* Sets the end time of this class routine event.
	*
	* @param endTime the end time of this class routine event
	*/
	public void setEndTime(java.util.Date endTime) {
		_classRoutineEvent.setEndTime(endTime);
	}

	/**
	* Returns the room ID of this class routine event.
	*
	* @return the room ID of this class routine event
	*/
	public long getRoomId() {
		return _classRoutineEvent.getRoomId();
	}

	/**
	* Sets the room ID of this class routine event.
	*
	* @param roomId the room ID of this class routine event
	*/
	public void setRoomId(long roomId) {
		_classRoutineEvent.setRoomId(roomId);
	}

	public boolean isNew() {
		return _classRoutineEvent.isNew();
	}

	public void setNew(boolean n) {
		_classRoutineEvent.setNew(n);
	}

	public boolean isCachedModel() {
		return _classRoutineEvent.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_classRoutineEvent.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _classRoutineEvent.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _classRoutineEvent.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_classRoutineEvent.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _classRoutineEvent.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_classRoutineEvent.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ClassRoutineEventWrapper((ClassRoutineEvent)_classRoutineEvent.clone());
	}

	public int compareTo(
		info.diit.portal.classroutine.model.ClassRoutineEvent classRoutineEvent) {
		return _classRoutineEvent.compareTo(classRoutineEvent);
	}

	@Override
	public int hashCode() {
		return _classRoutineEvent.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.classroutine.model.ClassRoutineEvent> toCacheModel() {
		return _classRoutineEvent.toCacheModel();
	}

	public info.diit.portal.classroutine.model.ClassRoutineEvent toEscapedModel() {
		return new ClassRoutineEventWrapper(_classRoutineEvent.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _classRoutineEvent.toString();
	}

	public java.lang.String toXmlString() {
		return _classRoutineEvent.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_classRoutineEvent.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public ClassRoutineEvent getWrappedClassRoutineEvent() {
		return _classRoutineEvent;
	}

	public ClassRoutineEvent getWrappedModel() {
		return _classRoutineEvent;
	}

	public void resetOriginalValues() {
		_classRoutineEvent.resetOriginalValues();
	}

	private ClassRoutineEvent _classRoutineEvent;
}
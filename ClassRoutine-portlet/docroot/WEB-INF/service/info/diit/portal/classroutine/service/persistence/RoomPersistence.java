/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.classroutine.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.classroutine.model.Room;

/**
 * The persistence interface for the room service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author saeid
 * @see RoomPersistenceImpl
 * @see RoomUtil
 * @generated
 */
public interface RoomPersistence extends BasePersistence<Room> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link RoomUtil} to access the room persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the room in the entity cache if it is enabled.
	*
	* @param room the room
	*/
	public void cacheResult(info.diit.portal.classroutine.model.Room room);

	/**
	* Caches the rooms in the entity cache if it is enabled.
	*
	* @param rooms the rooms
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.classroutine.model.Room> rooms);

	/**
	* Creates a new room with the primary key. Does not add the room to the database.
	*
	* @param roomId the primary key for the new room
	* @return the new room
	*/
	public info.diit.portal.classroutine.model.Room create(long roomId);

	/**
	* Removes the room with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param roomId the primary key of the room
	* @return the room that was removed
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room remove(long roomId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException;

	public info.diit.portal.classroutine.model.Room updateImpl(
		info.diit.portal.classroutine.model.Room room, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the room with the primary key or throws a {@link info.diit.portal.classroutine.NoSuchRoomException} if it could not be found.
	*
	* @param roomId the primary key of the room
	* @return the room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room findByPrimaryKey(
		long roomId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException;

	/**
	* Returns the room with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param roomId the primary key of the room
	* @return the room, or <code>null</code> if a room with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room fetchByPrimaryKey(
		long roomId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the room where roomId = &#63; or throws a {@link info.diit.portal.classroutine.NoSuchRoomException} if it could not be found.
	*
	* @param roomId the room ID
	* @return the matching room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room findByRoom(long roomId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException;

	/**
	* Returns the room where roomId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param roomId the room ID
	* @return the matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room fetchByRoom(long roomId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the room where roomId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param roomId the room ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room fetchByRoom(long roomId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the rooms where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.classroutine.model.Room> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the rooms where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @return the range of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.classroutine.model.Room> findByOrganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the rooms where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.classroutine.model.Room> findByOrganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first room in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room findByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException;

	/**
	* Returns the first room in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room fetchByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last room in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room findByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException;

	/**
	* Returns the last room in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room fetchByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the rooms before and after the current room in the ordered set where organizationId = &#63;.
	*
	* @param roomId the primary key of the current room
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room[] findByOrganization_PrevAndNext(
		long roomId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException;

	/**
	* Returns all the rooms where organizationId = &#63; and label = &#63;.
	*
	* @param organizationId the organization ID
	* @param label the label
	* @return the matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.classroutine.model.Room> findByOrganizationLabel(
		long organizationId, java.lang.String label)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the rooms where organizationId = &#63; and label = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param label the label
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @return the range of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.classroutine.model.Room> findByOrganizationLabel(
		long organizationId, java.lang.String label, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the rooms where organizationId = &#63; and label = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param label the label
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.classroutine.model.Room> findByOrganizationLabel(
		long organizationId, java.lang.String label, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first room in the ordered set where organizationId = &#63; and label = &#63;.
	*
	* @param organizationId the organization ID
	* @param label the label
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room findByOrganizationLabel_First(
		long organizationId, java.lang.String label,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException;

	/**
	* Returns the first room in the ordered set where organizationId = &#63; and label = &#63;.
	*
	* @param organizationId the organization ID
	* @param label the label
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room fetchByOrganizationLabel_First(
		long organizationId, java.lang.String label,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last room in the ordered set where organizationId = &#63; and label = &#63;.
	*
	* @param organizationId the organization ID
	* @param label the label
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room findByOrganizationLabel_Last(
		long organizationId, java.lang.String label,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException;

	/**
	* Returns the last room in the ordered set where organizationId = &#63; and label = &#63;.
	*
	* @param organizationId the organization ID
	* @param label the label
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room fetchByOrganizationLabel_Last(
		long organizationId, java.lang.String label,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the rooms before and after the current room in the ordered set where organizationId = &#63; and label = &#63;.
	*
	* @param roomId the primary key of the current room
	* @param organizationId the organization ID
	* @param label the label
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room[] findByOrganizationLabel_PrevAndNext(
		long roomId, long organizationId, java.lang.String label,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException;

	/**
	* Returns all the rooms where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.classroutine.model.Room> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the rooms where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @return the range of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.classroutine.model.Room> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the rooms where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.classroutine.model.Room> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first room in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException;

	/**
	* Returns the first room in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last room in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException;

	/**
	* Returns the last room in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching room, or <code>null</code> if a matching room could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the rooms before and after the current room in the ordered set where companyId = &#63;.
	*
	* @param roomId the primary key of the current room
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next room
	* @throws info.diit.portal.classroutine.NoSuchRoomException if a room with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room[] findByCompany_PrevAndNext(
		long roomId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException;

	/**
	* Returns all the rooms.
	*
	* @return the rooms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.classroutine.model.Room> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the rooms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @return the range of rooms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.classroutine.model.Room> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the rooms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of rooms
	* @param end the upper bound of the range of rooms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of rooms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.classroutine.model.Room> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the room where roomId = &#63; from the database.
	*
	* @param roomId the room ID
	* @return the room that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.classroutine.model.Room removeByRoom(long roomId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.classroutine.NoSuchRoomException;

	/**
	* Removes all the rooms where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the rooms where organizationId = &#63; and label = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param label the label
	* @throws SystemException if a system exception occurred
	*/
	public void removeByOrganizationLabel(long organizationId,
		java.lang.String label)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the rooms where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the rooms from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of rooms where roomId = &#63;.
	*
	* @param roomId the room ID
	* @return the number of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public int countByRoom(long roomId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of rooms where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public int countByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of rooms where organizationId = &#63; and label = &#63;.
	*
	* @param organizationId the organization ID
	* @param label the label
	* @return the number of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public int countByOrganizationLabel(long organizationId,
		java.lang.String label)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of rooms where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching rooms
	* @throws SystemException if a system exception occurred
	*/
	public int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of rooms.
	*
	* @return the number of rooms
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
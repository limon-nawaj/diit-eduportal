create index IX_9ECF52EA on EduPortal_ClassRoutine_ClassRoutineEvent (companyId);
create index IX_BAEBDDC8 on EduPortal_ClassRoutine_ClassRoutineEvent (roomId);
create index IX_99287C19 on EduPortal_ClassRoutine_ClassRoutineEvent (subjectId);
create index IX_5799F500 on EduPortal_ClassRoutine_ClassRoutineEvent (subjectId, classRoutineEventId);
create index IX_19BD1653 on EduPortal_ClassRoutine_ClassRoutineEvent (subjectId, day);

create index IX_CB2D103D on EduPortal_ClassRoutine_ClassRoutineEventBatch (batchId);
create index IX_E29A83D1 on EduPortal_ClassRoutine_ClassRoutineEventBatch (classRoutineEventId);
create index IX_ABB1DE80 on EduPortal_ClassRoutine_ClassRoutineEventBatch (companyId);

create index IX_C6AF1E91 on EduPortal_ClassRoutine_Room (companyId);
create index IX_FFDB02D9 on EduPortal_ClassRoutine_Room (organizationId);
create index IX_1846EFAB on EduPortal_ClassRoutine_Room (organizationId, label);
create index IX_57350B81 on EduPortal_ClassRoutine_Room (roomId);
create table EduPortal_ClassRoutine_ClassRoutineEvent (
	classRoutineEventId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	subjectId LONG,
	day INTEGER,
	startTime DATE null,
	endTime DATE null,
	roomId LONG
);

create table EduPortal_ClassRoutine_ClassRoutineEventBatch (
	classRoutineEventBatchId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	classRoutineEventId LONG,
	batchId LONG
);

create table EduPortal_ClassRoutine_Room (
	roomId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	label VARCHAR(75) null,
	description VARCHAR(75) null,
	roomType INTEGER
);
package info.diit.portal.lessonplan.dto;

import java.util.Date;

public class LessonPlanDto {

	private long id;
	private String name;
	private SubjectDto subject;
	private Date planDate;
	private long totalClass;
	private long duration;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public SubjectDto getSubject() {
		return subject;
	}
	public void setSubject(SubjectDto subject) {
		this.subject = subject;
	}
	public Date getPlanDate() {
		return planDate;
	}
	public void setPlanDate(Date planDate) {
		this.planDate = planDate;
	}
	public long getTotalClass() {
		return totalClass;
	}
	public void setTotalClass(long totalClass) {
		this.totalClass = totalClass;
	}
	public long getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}
}

/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.lessonplan.model.LessonTopic;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing LessonTopic in entity cache.
 *
 * @author limon
 * @see LessonTopic
 * @generated
 */
public class LessonTopicCacheModel implements CacheModel<LessonTopic>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{lessonTopicId=");
		sb.append(lessonTopicId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", lessonPlanId=");
		sb.append(lessonPlanId);
		sb.append(", topic=");
		sb.append(topic);
		sb.append(", classSequence=");
		sb.append(classSequence);
		sb.append(", resource=");
		sb.append(resource);
		sb.append(", activities=");
		sb.append(activities);
		sb.append("}");

		return sb.toString();
	}

	public LessonTopic toEntityModel() {
		LessonTopicImpl lessonTopicImpl = new LessonTopicImpl();

		lessonTopicImpl.setLessonTopicId(lessonTopicId);
		lessonTopicImpl.setCompanyId(companyId);
		lessonTopicImpl.setUserId(userId);

		if (userName == null) {
			lessonTopicImpl.setUserName(StringPool.BLANK);
		}
		else {
			lessonTopicImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			lessonTopicImpl.setCreateDate(null);
		}
		else {
			lessonTopicImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			lessonTopicImpl.setModifiedDate(null);
		}
		else {
			lessonTopicImpl.setModifiedDate(new Date(modifiedDate));
		}

		lessonTopicImpl.setLessonPlanId(lessonPlanId);
		lessonTopicImpl.setTopic(topic);
		lessonTopicImpl.setClassSequence(classSequence);

		if (resource == null) {
			lessonTopicImpl.setResource(StringPool.BLANK);
		}
		else {
			lessonTopicImpl.setResource(resource);
		}

		if (activities == null) {
			lessonTopicImpl.setActivities(StringPool.BLANK);
		}
		else {
			lessonTopicImpl.setActivities(activities);
		}

		lessonTopicImpl.resetOriginalValues();

		return lessonTopicImpl;
	}

	public long lessonTopicId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long lessonPlanId;
	public long topic;
	public long classSequence;
	public String resource;
	public String activities;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.lessonplan.model.SubjectLesson;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing SubjectLesson in entity cache.
 *
 * @author limon
 * @see SubjectLesson
 * @generated
 */
public class SubjectLessonCacheModel implements CacheModel<SubjectLesson>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{SubjectLessonId=");
		sb.append(SubjectLessonId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", batchId=");
		sb.append(batchId);
		sb.append(", subjectId=");
		sb.append(subjectId);
		sb.append(", lessonPlanId=");
		sb.append(lessonPlanId);
		sb.append("}");

		return sb.toString();
	}

	public SubjectLesson toEntityModel() {
		SubjectLessonImpl subjectLessonImpl = new SubjectLessonImpl();

		subjectLessonImpl.setSubjectLessonId(SubjectLessonId);
		subjectLessonImpl.setCompanyId(companyId);
		subjectLessonImpl.setUserId(userId);

		if (userName == null) {
			subjectLessonImpl.setUserName(StringPool.BLANK);
		}
		else {
			subjectLessonImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			subjectLessonImpl.setCreateDate(null);
		}
		else {
			subjectLessonImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			subjectLessonImpl.setModifiedDate(null);
		}
		else {
			subjectLessonImpl.setModifiedDate(new Date(modifiedDate));
		}

		subjectLessonImpl.setOrganizationId(organizationId);
		subjectLessonImpl.setBatchId(batchId);
		subjectLessonImpl.setSubjectId(subjectId);
		subjectLessonImpl.setLessonPlanId(lessonPlanId);

		subjectLessonImpl.resetOriginalValues();

		return subjectLessonImpl;
	}

	public long SubjectLessonId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long organizationId;
	public long batchId;
	public long subjectId;
	public long lessonPlanId;
}
package info.diit.portal.lessonplan.dto;

public class LessonDto {

	private long id;
	private String lesson;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getLesson() {
		return lesson;
	}
	public void setLesson(String lesson) {
		this.lesson = lesson;
	}
	@Override
	public String toString() {
		return getLesson();
	}
}

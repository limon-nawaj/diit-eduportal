package info.diit.portal.lessonplan.dto;

public class AssessmentTypeDto {

	private long id;
	private String title;
	private Integer numberOfAssessment;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getNumberOfAssessment() {
		return numberOfAssessment;
	}
	public void setNumberOfAssessment(Integer numberOfAssessment) {
		this.numberOfAssessment = numberOfAssessment;
	}
}

/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.lessonplan.model.LessonPlan;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing LessonPlan in entity cache.
 *
 * @author limon
 * @see LessonPlan
 * @generated
 */
public class LessonPlanCacheModel implements CacheModel<LessonPlan>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{lessonPlanId=");
		sb.append(lessonPlanId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", title=");
		sb.append(title);
		sb.append(", organization=");
		sb.append(organization);
		sb.append(", subject=");
		sb.append(subject);
		sb.append(", planDate=");
		sb.append(planDate);
		sb.append(", totalClass=");
		sb.append(totalClass);
		sb.append(", totalDuration=");
		sb.append(totalDuration);
		sb.append(", objective=");
		sb.append(objective);
		sb.append("}");

		return sb.toString();
	}

	public LessonPlan toEntityModel() {
		LessonPlanImpl lessonPlanImpl = new LessonPlanImpl();

		lessonPlanImpl.setLessonPlanId(lessonPlanId);
		lessonPlanImpl.setCompanyId(companyId);
		lessonPlanImpl.setUserId(userId);

		if (userName == null) {
			lessonPlanImpl.setUserName(StringPool.BLANK);
		}
		else {
			lessonPlanImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			lessonPlanImpl.setCreateDate(null);
		}
		else {
			lessonPlanImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			lessonPlanImpl.setModifiedDate(null);
		}
		else {
			lessonPlanImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (title == null) {
			lessonPlanImpl.setTitle(StringPool.BLANK);
		}
		else {
			lessonPlanImpl.setTitle(title);
		}

		lessonPlanImpl.setOrganization(organization);
		lessonPlanImpl.setSubject(subject);

		if (planDate == Long.MIN_VALUE) {
			lessonPlanImpl.setPlanDate(null);
		}
		else {
			lessonPlanImpl.setPlanDate(new Date(planDate));
		}

		lessonPlanImpl.setTotalClass(totalClass);
		lessonPlanImpl.setTotalDuration(totalDuration);

		if (objective == null) {
			lessonPlanImpl.setObjective(StringPool.BLANK);
		}
		else {
			lessonPlanImpl.setObjective(objective);
		}

		lessonPlanImpl.resetOriginalValues();

		return lessonPlanImpl;
	}

	public long lessonPlanId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String title;
	public long organization;
	public long subject;
	public long planDate;
	public long totalClass;
	public long totalDuration;
	public String objective;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.lessonplan.NoSuchLessonPlanException;
import info.diit.portal.lessonplan.model.LessonPlan;
import info.diit.portal.lessonplan.model.impl.LessonPlanImpl;
import info.diit.portal.lessonplan.model.impl.LessonPlanModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the lesson plan service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see LessonPlanPersistence
 * @see LessonPlanUtil
 * @generated
 */
public class LessonPlanPersistenceImpl extends BasePersistenceImpl<LessonPlan>
	implements LessonPlanPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LessonPlanUtil} to access the lesson plan persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LessonPlanImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATIONUSER =
		new FinderPath(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
			LessonPlanModelImpl.FINDER_CACHE_ENABLED, LessonPlanImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOrganizationUser",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONUSER =
		new FinderPath(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
			LessonPlanModelImpl.FINDER_CACHE_ENABLED, LessonPlanImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByOrganizationUser", new String[] { Long.class.getName() },
			LessonPlanModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATIONUSER = new FinderPath(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
			LessonPlanModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByOrganizationUser", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SUBJECT = new FinderPath(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
			LessonPlanModelImpl.FINDER_CACHE_ENABLED, LessonPlanImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBySubject",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECT =
		new FinderPath(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
			LessonPlanModelImpl.FINDER_CACHE_ENABLED, LessonPlanImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBySubject",
			new String[] { Long.class.getName() },
			LessonPlanModelImpl.SUBJECT_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SUBJECT = new FinderPath(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
			LessonPlanModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBySubject",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
			LessonPlanModelImpl.FINDER_CACHE_ENABLED, LessonPlanImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
			LessonPlanModelImpl.FINDER_CACHE_ENABLED, LessonPlanImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
			LessonPlanModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the lesson plan in the entity cache if it is enabled.
	 *
	 * @param lessonPlan the lesson plan
	 */
	public void cacheResult(LessonPlan lessonPlan) {
		EntityCacheUtil.putResult(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
			LessonPlanImpl.class, lessonPlan.getPrimaryKey(), lessonPlan);

		lessonPlan.resetOriginalValues();
	}

	/**
	 * Caches the lesson plans in the entity cache if it is enabled.
	 *
	 * @param lessonPlans the lesson plans
	 */
	public void cacheResult(List<LessonPlan> lessonPlans) {
		for (LessonPlan lessonPlan : lessonPlans) {
			if (EntityCacheUtil.getResult(
						LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
						LessonPlanImpl.class, lessonPlan.getPrimaryKey()) == null) {
				cacheResult(lessonPlan);
			}
			else {
				lessonPlan.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all lesson plans.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(LessonPlanImpl.class.getName());
		}

		EntityCacheUtil.clearCache(LessonPlanImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the lesson plan.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LessonPlan lessonPlan) {
		EntityCacheUtil.removeResult(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
			LessonPlanImpl.class, lessonPlan.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<LessonPlan> lessonPlans) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LessonPlan lessonPlan : lessonPlans) {
			EntityCacheUtil.removeResult(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
				LessonPlanImpl.class, lessonPlan.getPrimaryKey());
		}
	}

	/**
	 * Creates a new lesson plan with the primary key. Does not add the lesson plan to the database.
	 *
	 * @param lessonPlanId the primary key for the new lesson plan
	 * @return the new lesson plan
	 */
	public LessonPlan create(long lessonPlanId) {
		LessonPlan lessonPlan = new LessonPlanImpl();

		lessonPlan.setNew(true);
		lessonPlan.setPrimaryKey(lessonPlanId);

		return lessonPlan;
	}

	/**
	 * Removes the lesson plan with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param lessonPlanId the primary key of the lesson plan
	 * @return the lesson plan that was removed
	 * @throws info.diit.portal.lessonplan.NoSuchLessonPlanException if a lesson plan with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonPlan remove(long lessonPlanId)
		throws NoSuchLessonPlanException, SystemException {
		return remove(Long.valueOf(lessonPlanId));
	}

	/**
	 * Removes the lesson plan with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the lesson plan
	 * @return the lesson plan that was removed
	 * @throws info.diit.portal.lessonplan.NoSuchLessonPlanException if a lesson plan with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LessonPlan remove(Serializable primaryKey)
		throws NoSuchLessonPlanException, SystemException {
		Session session = null;

		try {
			session = openSession();

			LessonPlan lessonPlan = (LessonPlan)session.get(LessonPlanImpl.class,
					primaryKey);

			if (lessonPlan == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLessonPlanException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(lessonPlan);
		}
		catch (NoSuchLessonPlanException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LessonPlan removeImpl(LessonPlan lessonPlan)
		throws SystemException {
		lessonPlan = toUnwrappedModel(lessonPlan);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, lessonPlan);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(lessonPlan);

		return lessonPlan;
	}

	@Override
	public LessonPlan updateImpl(
		info.diit.portal.lessonplan.model.LessonPlan lessonPlan, boolean merge)
		throws SystemException {
		lessonPlan = toUnwrappedModel(lessonPlan);

		boolean isNew = lessonPlan.isNew();

		LessonPlanModelImpl lessonPlanModelImpl = (LessonPlanModelImpl)lessonPlan;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, lessonPlan, merge);

			lessonPlan.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !LessonPlanModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((lessonPlanModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONUSER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(lessonPlanModelImpl.getOriginalUserId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATIONUSER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONUSER,
					args);

				args = new Object[] {
						Long.valueOf(lessonPlanModelImpl.getUserId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATIONUSER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONUSER,
					args);
			}

			if ((lessonPlanModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(lessonPlanModelImpl.getOriginalSubject())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBJECT, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECT,
					args);

				args = new Object[] {
						Long.valueOf(lessonPlanModelImpl.getSubject())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBJECT, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECT,
					args);
			}
		}

		EntityCacheUtil.putResult(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
			LessonPlanImpl.class, lessonPlan.getPrimaryKey(), lessonPlan);

		return lessonPlan;
	}

	protected LessonPlan toUnwrappedModel(LessonPlan lessonPlan) {
		if (lessonPlan instanceof LessonPlanImpl) {
			return lessonPlan;
		}

		LessonPlanImpl lessonPlanImpl = new LessonPlanImpl();

		lessonPlanImpl.setNew(lessonPlan.isNew());
		lessonPlanImpl.setPrimaryKey(lessonPlan.getPrimaryKey());

		lessonPlanImpl.setLessonPlanId(lessonPlan.getLessonPlanId());
		lessonPlanImpl.setCompanyId(lessonPlan.getCompanyId());
		lessonPlanImpl.setUserId(lessonPlan.getUserId());
		lessonPlanImpl.setUserName(lessonPlan.getUserName());
		lessonPlanImpl.setCreateDate(lessonPlan.getCreateDate());
		lessonPlanImpl.setModifiedDate(lessonPlan.getModifiedDate());
		lessonPlanImpl.setTitle(lessonPlan.getTitle());
		lessonPlanImpl.setOrganization(lessonPlan.getOrganization());
		lessonPlanImpl.setSubject(lessonPlan.getSubject());
		lessonPlanImpl.setPlanDate(lessonPlan.getPlanDate());
		lessonPlanImpl.setTotalClass(lessonPlan.getTotalClass());
		lessonPlanImpl.setTotalDuration(lessonPlan.getTotalDuration());
		lessonPlanImpl.setObjective(lessonPlan.getObjective());

		return lessonPlanImpl;
	}

	/**
	 * Returns the lesson plan with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the lesson plan
	 * @return the lesson plan
	 * @throws com.liferay.portal.NoSuchModelException if a lesson plan with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LessonPlan findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the lesson plan with the primary key or throws a {@link info.diit.portal.lessonplan.NoSuchLessonPlanException} if it could not be found.
	 *
	 * @param lessonPlanId the primary key of the lesson plan
	 * @return the lesson plan
	 * @throws info.diit.portal.lessonplan.NoSuchLessonPlanException if a lesson plan with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonPlan findByPrimaryKey(long lessonPlanId)
		throws NoSuchLessonPlanException, SystemException {
		LessonPlan lessonPlan = fetchByPrimaryKey(lessonPlanId);

		if (lessonPlan == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + lessonPlanId);
			}

			throw new NoSuchLessonPlanException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				lessonPlanId);
		}

		return lessonPlan;
	}

	/**
	 * Returns the lesson plan with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the lesson plan
	 * @return the lesson plan, or <code>null</code> if a lesson plan with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LessonPlan fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the lesson plan with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param lessonPlanId the primary key of the lesson plan
	 * @return the lesson plan, or <code>null</code> if a lesson plan with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonPlan fetchByPrimaryKey(long lessonPlanId)
		throws SystemException {
		LessonPlan lessonPlan = (LessonPlan)EntityCacheUtil.getResult(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
				LessonPlanImpl.class, lessonPlanId);

		if (lessonPlan == _nullLessonPlan) {
			return null;
		}

		if (lessonPlan == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				lessonPlan = (LessonPlan)session.get(LessonPlanImpl.class,
						Long.valueOf(lessonPlanId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (lessonPlan != null) {
					cacheResult(lessonPlan);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(LessonPlanModelImpl.ENTITY_CACHE_ENABLED,
						LessonPlanImpl.class, lessonPlanId, _nullLessonPlan);
				}

				closeSession(session);
			}
		}

		return lessonPlan;
	}

	/**
	 * Returns all the lesson plans where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching lesson plans
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonPlan> findByOrganizationUser(long userId)
		throws SystemException {
		return findByOrganizationUser(userId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lesson plans where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of lesson plans
	 * @param end the upper bound of the range of lesson plans (not inclusive)
	 * @return the range of matching lesson plans
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonPlan> findByOrganizationUser(long userId, int start,
		int end) throws SystemException {
		return findByOrganizationUser(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the lesson plans where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of lesson plans
	 * @param end the upper bound of the range of lesson plans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching lesson plans
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonPlan> findByOrganizationUser(long userId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONUSER;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATIONUSER;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<LessonPlan> list = (List<LessonPlan>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (LessonPlan lessonPlan : list) {
				if ((userId != lessonPlan.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_LESSONPLAN_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATIONUSER_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				list = (List<LessonPlan>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first lesson plan in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lesson plan
	 * @throws info.diit.portal.lessonplan.NoSuchLessonPlanException if a matching lesson plan could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonPlan findByOrganizationUser_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchLessonPlanException, SystemException {
		LessonPlan lessonPlan = fetchByOrganizationUser_First(userId,
				orderByComparator);

		if (lessonPlan != null) {
			return lessonPlan;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLessonPlanException(msg.toString());
	}

	/**
	 * Returns the first lesson plan in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lesson plan, or <code>null</code> if a matching lesson plan could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonPlan fetchByOrganizationUser_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<LessonPlan> list = findByOrganizationUser(userId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last lesson plan in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lesson plan
	 * @throws info.diit.portal.lessonplan.NoSuchLessonPlanException if a matching lesson plan could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonPlan findByOrganizationUser_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchLessonPlanException, SystemException {
		LessonPlan lessonPlan = fetchByOrganizationUser_Last(userId,
				orderByComparator);

		if (lessonPlan != null) {
			return lessonPlan;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLessonPlanException(msg.toString());
	}

	/**
	 * Returns the last lesson plan in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lesson plan, or <code>null</code> if a matching lesson plan could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonPlan fetchByOrganizationUser_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByOrganizationUser(userId);

		List<LessonPlan> list = findByOrganizationUser(userId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the lesson plans before and after the current lesson plan in the ordered set where userId = &#63;.
	 *
	 * @param lessonPlanId the primary key of the current lesson plan
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next lesson plan
	 * @throws info.diit.portal.lessonplan.NoSuchLessonPlanException if a lesson plan with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonPlan[] findByOrganizationUser_PrevAndNext(long lessonPlanId,
		long userId, OrderByComparator orderByComparator)
		throws NoSuchLessonPlanException, SystemException {
		LessonPlan lessonPlan = findByPrimaryKey(lessonPlanId);

		Session session = null;

		try {
			session = openSession();

			LessonPlan[] array = new LessonPlanImpl[3];

			array[0] = getByOrganizationUser_PrevAndNext(session, lessonPlan,
					userId, orderByComparator, true);

			array[1] = lessonPlan;

			array[2] = getByOrganizationUser_PrevAndNext(session, lessonPlan,
					userId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LessonPlan getByOrganizationUser_PrevAndNext(Session session,
		LessonPlan lessonPlan, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LESSONPLAN_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATIONUSER_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(lessonPlan);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LessonPlan> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the lesson plans where subject = &#63;.
	 *
	 * @param subject the subject
	 * @return the matching lesson plans
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonPlan> findBySubject(long subject)
		throws SystemException {
		return findBySubject(subject, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lesson plans where subject = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param subject the subject
	 * @param start the lower bound of the range of lesson plans
	 * @param end the upper bound of the range of lesson plans (not inclusive)
	 * @return the range of matching lesson plans
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonPlan> findBySubject(long subject, int start, int end)
		throws SystemException {
		return findBySubject(subject, start, end, null);
	}

	/**
	 * Returns an ordered range of all the lesson plans where subject = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param subject the subject
	 * @param start the lower bound of the range of lesson plans
	 * @param end the upper bound of the range of lesson plans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching lesson plans
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonPlan> findBySubject(long subject, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECT;
			finderArgs = new Object[] { subject };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SUBJECT;
			finderArgs = new Object[] { subject, start, end, orderByComparator };
		}

		List<LessonPlan> list = (List<LessonPlan>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (LessonPlan lessonPlan : list) {
				if ((subject != lessonPlan.getSubject())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_LESSONPLAN_WHERE);

			query.append(_FINDER_COLUMN_SUBJECT_SUBJECT_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subject);

				list = (List<LessonPlan>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first lesson plan in the ordered set where subject = &#63;.
	 *
	 * @param subject the subject
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lesson plan
	 * @throws info.diit.portal.lessonplan.NoSuchLessonPlanException if a matching lesson plan could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonPlan findBySubject_First(long subject,
		OrderByComparator orderByComparator)
		throws NoSuchLessonPlanException, SystemException {
		LessonPlan lessonPlan = fetchBySubject_First(subject, orderByComparator);

		if (lessonPlan != null) {
			return lessonPlan;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("subject=");
		msg.append(subject);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLessonPlanException(msg.toString());
	}

	/**
	 * Returns the first lesson plan in the ordered set where subject = &#63;.
	 *
	 * @param subject the subject
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lesson plan, or <code>null</code> if a matching lesson plan could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonPlan fetchBySubject_First(long subject,
		OrderByComparator orderByComparator) throws SystemException {
		List<LessonPlan> list = findBySubject(subject, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last lesson plan in the ordered set where subject = &#63;.
	 *
	 * @param subject the subject
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lesson plan
	 * @throws info.diit.portal.lessonplan.NoSuchLessonPlanException if a matching lesson plan could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonPlan findBySubject_Last(long subject,
		OrderByComparator orderByComparator)
		throws NoSuchLessonPlanException, SystemException {
		LessonPlan lessonPlan = fetchBySubject_Last(subject, orderByComparator);

		if (lessonPlan != null) {
			return lessonPlan;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("subject=");
		msg.append(subject);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLessonPlanException(msg.toString());
	}

	/**
	 * Returns the last lesson plan in the ordered set where subject = &#63;.
	 *
	 * @param subject the subject
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lesson plan, or <code>null</code> if a matching lesson plan could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonPlan fetchBySubject_Last(long subject,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBySubject(subject);

		List<LessonPlan> list = findBySubject(subject, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the lesson plans before and after the current lesson plan in the ordered set where subject = &#63;.
	 *
	 * @param lessonPlanId the primary key of the current lesson plan
	 * @param subject the subject
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next lesson plan
	 * @throws info.diit.portal.lessonplan.NoSuchLessonPlanException if a lesson plan with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonPlan[] findBySubject_PrevAndNext(long lessonPlanId,
		long subject, OrderByComparator orderByComparator)
		throws NoSuchLessonPlanException, SystemException {
		LessonPlan lessonPlan = findByPrimaryKey(lessonPlanId);

		Session session = null;

		try {
			session = openSession();

			LessonPlan[] array = new LessonPlanImpl[3];

			array[0] = getBySubject_PrevAndNext(session, lessonPlan, subject,
					orderByComparator, true);

			array[1] = lessonPlan;

			array[2] = getBySubject_PrevAndNext(session, lessonPlan, subject,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LessonPlan getBySubject_PrevAndNext(Session session,
		LessonPlan lessonPlan, long subject,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LESSONPLAN_WHERE);

		query.append(_FINDER_COLUMN_SUBJECT_SUBJECT_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(subject);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(lessonPlan);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LessonPlan> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the lesson plans.
	 *
	 * @return the lesson plans
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonPlan> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lesson plans.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of lesson plans
	 * @param end the upper bound of the range of lesson plans (not inclusive)
	 * @return the range of lesson plans
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonPlan> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the lesson plans.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of lesson plans
	 * @param end the upper bound of the range of lesson plans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of lesson plans
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonPlan> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LessonPlan> list = (List<LessonPlan>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LESSONPLAN);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LESSONPLAN;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<LessonPlan>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<LessonPlan>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the lesson plans where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByOrganizationUser(long userId) throws SystemException {
		for (LessonPlan lessonPlan : findByOrganizationUser(userId)) {
			remove(lessonPlan);
		}
	}

	/**
	 * Removes all the lesson plans where subject = &#63; from the database.
	 *
	 * @param subject the subject
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBySubject(long subject) throws SystemException {
		for (LessonPlan lessonPlan : findBySubject(subject)) {
			remove(lessonPlan);
		}
	}

	/**
	 * Removes all the lesson plans from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (LessonPlan lessonPlan : findAll()) {
			remove(lessonPlan);
		}
	}

	/**
	 * Returns the number of lesson plans where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching lesson plans
	 * @throws SystemException if a system exception occurred
	 */
	public int countByOrganizationUser(long userId) throws SystemException {
		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATIONUSER,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LESSONPLAN_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATIONUSER_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATIONUSER,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of lesson plans where subject = &#63;.
	 *
	 * @param subject the subject
	 * @return the number of matching lesson plans
	 * @throws SystemException if a system exception occurred
	 */
	public int countBySubject(long subject) throws SystemException {
		Object[] finderArgs = new Object[] { subject };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_SUBJECT,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LESSONPLAN_WHERE);

			query.append(_FINDER_COLUMN_SUBJECT_SUBJECT_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subject);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_SUBJECT,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of lesson plans.
	 *
	 * @return the number of lesson plans
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LESSONPLAN);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the lesson plan persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.lessonplan.model.LessonPlan")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<LessonPlan>> listenersList = new ArrayList<ModelListener<LessonPlan>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<LessonPlan>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(LessonPlanImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_LESSONPLAN = "SELECT lessonPlan FROM LessonPlan lessonPlan";
	private static final String _SQL_SELECT_LESSONPLAN_WHERE = "SELECT lessonPlan FROM LessonPlan lessonPlan WHERE ";
	private static final String _SQL_COUNT_LESSONPLAN = "SELECT COUNT(lessonPlan) FROM LessonPlan lessonPlan";
	private static final String _SQL_COUNT_LESSONPLAN_WHERE = "SELECT COUNT(lessonPlan) FROM LessonPlan lessonPlan WHERE ";
	private static final String _FINDER_COLUMN_ORGANIZATIONUSER_USERID_2 = "lessonPlan.userId = ?";
	private static final String _FINDER_COLUMN_SUBJECT_SUBJECT_2 = "lessonPlan.subject = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "lessonPlan.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LessonPlan exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No LessonPlan exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(LessonPlanPersistenceImpl.class);
	private static LessonPlan _nullLessonPlan = new LessonPlanImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<LessonPlan> toCacheModel() {
				return _nullLessonPlanCacheModel;
			}
		};

	private static CacheModel<LessonPlan> _nullLessonPlanCacheModel = new CacheModel<LessonPlan>() {
			public LessonPlan toEntityModel() {
				return _nullLessonPlan;
			}
		};
}
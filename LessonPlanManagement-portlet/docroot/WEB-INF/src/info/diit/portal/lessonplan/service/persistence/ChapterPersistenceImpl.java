/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.lessonplan.NoSuchChapterException;
import info.diit.portal.lessonplan.model.Chapter;
import info.diit.portal.lessonplan.model.impl.ChapterImpl;
import info.diit.portal.lessonplan.model.impl.ChapterModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the chapter service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see ChapterPersistence
 * @see ChapterUtil
 * @generated
 */
public class ChapterPersistenceImpl extends BasePersistenceImpl<Chapter>
	implements ChapterPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ChapterUtil} to access the chapter persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ChapterImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY = new FinderPath(ChapterModelImpl.ENTITY_CACHE_ENABLED,
			ChapterModelImpl.FINDER_CACHE_ENABLED, ChapterImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompany",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY =
		new FinderPath(ChapterModelImpl.ENTITY_CACHE_ENABLED,
			ChapterModelImpl.FINDER_CACHE_ENABLED, ChapterImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompany",
			new String[] { Long.class.getName() },
			ChapterModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANY = new FinderPath(ChapterModelImpl.ENTITY_CACHE_ENABLED,
			ChapterModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompany",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SUBJECT = new FinderPath(ChapterModelImpl.ENTITY_CACHE_ENABLED,
			ChapterModelImpl.FINDER_CACHE_ENABLED, ChapterImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBySubject",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECT =
		new FinderPath(ChapterModelImpl.ENTITY_CACHE_ENABLED,
			ChapterModelImpl.FINDER_CACHE_ENABLED, ChapterImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBySubject",
			new String[] { Long.class.getName() },
			ChapterModelImpl.SUBJECTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SUBJECT = new FinderPath(ChapterModelImpl.ENTITY_CACHE_ENABLED,
			ChapterModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBySubject",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FETCH_BY_CHAPTERSEQUENCE = new FinderPath(ChapterModelImpl.ENTITY_CACHE_ENABLED,
			ChapterModelImpl.FINDER_CACHE_ENABLED, ChapterImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByChapterSequence",
			new String[] { Long.class.getName(), Long.class.getName() },
			ChapterModelImpl.CHAPTERID_COLUMN_BITMASK |
			ChapterModelImpl.SEQUENCE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CHAPTERSEQUENCE = new FinderPath(ChapterModelImpl.ENTITY_CACHE_ENABLED,
			ChapterModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByChapterSequence",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ChapterModelImpl.ENTITY_CACHE_ENABLED,
			ChapterModelImpl.FINDER_CACHE_ENABLED, ChapterImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ChapterModelImpl.ENTITY_CACHE_ENABLED,
			ChapterModelImpl.FINDER_CACHE_ENABLED, ChapterImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ChapterModelImpl.ENTITY_CACHE_ENABLED,
			ChapterModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the chapter in the entity cache if it is enabled.
	 *
	 * @param chapter the chapter
	 */
	public void cacheResult(Chapter chapter) {
		EntityCacheUtil.putResult(ChapterModelImpl.ENTITY_CACHE_ENABLED,
			ChapterImpl.class, chapter.getPrimaryKey(), chapter);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CHAPTERSEQUENCE,
			new Object[] {
				Long.valueOf(chapter.getChapterId()),
				Long.valueOf(chapter.getSequence())
			}, chapter);

		chapter.resetOriginalValues();
	}

	/**
	 * Caches the chapters in the entity cache if it is enabled.
	 *
	 * @param chapters the chapters
	 */
	public void cacheResult(List<Chapter> chapters) {
		for (Chapter chapter : chapters) {
			if (EntityCacheUtil.getResult(
						ChapterModelImpl.ENTITY_CACHE_ENABLED,
						ChapterImpl.class, chapter.getPrimaryKey()) == null) {
				cacheResult(chapter);
			}
			else {
				chapter.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all chapters.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ChapterImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ChapterImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the chapter.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Chapter chapter) {
		EntityCacheUtil.removeResult(ChapterModelImpl.ENTITY_CACHE_ENABLED,
			ChapterImpl.class, chapter.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(chapter);
	}

	@Override
	public void clearCache(List<Chapter> chapters) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Chapter chapter : chapters) {
			EntityCacheUtil.removeResult(ChapterModelImpl.ENTITY_CACHE_ENABLED,
				ChapterImpl.class, chapter.getPrimaryKey());

			clearUniqueFindersCache(chapter);
		}
	}

	protected void clearUniqueFindersCache(Chapter chapter) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CHAPTERSEQUENCE,
			new Object[] {
				Long.valueOf(chapter.getChapterId()),
				Long.valueOf(chapter.getSequence())
			});
	}

	/**
	 * Creates a new chapter with the primary key. Does not add the chapter to the database.
	 *
	 * @param chapterId the primary key for the new chapter
	 * @return the new chapter
	 */
	public Chapter create(long chapterId) {
		Chapter chapter = new ChapterImpl();

		chapter.setNew(true);
		chapter.setPrimaryKey(chapterId);

		return chapter;
	}

	/**
	 * Removes the chapter with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param chapterId the primary key of the chapter
	 * @return the chapter that was removed
	 * @throws info.diit.portal.lessonplan.NoSuchChapterException if a chapter with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter remove(long chapterId)
		throws NoSuchChapterException, SystemException {
		return remove(Long.valueOf(chapterId));
	}

	/**
	 * Removes the chapter with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the chapter
	 * @return the chapter that was removed
	 * @throws info.diit.portal.lessonplan.NoSuchChapterException if a chapter with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Chapter remove(Serializable primaryKey)
		throws NoSuchChapterException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Chapter chapter = (Chapter)session.get(ChapterImpl.class, primaryKey);

			if (chapter == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchChapterException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(chapter);
		}
		catch (NoSuchChapterException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Chapter removeImpl(Chapter chapter) throws SystemException {
		chapter = toUnwrappedModel(chapter);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, chapter);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(chapter);

		return chapter;
	}

	@Override
	public Chapter updateImpl(
		info.diit.portal.lessonplan.model.Chapter chapter, boolean merge)
		throws SystemException {
		chapter = toUnwrappedModel(chapter);

		boolean isNew = chapter.isNew();

		ChapterModelImpl chapterModelImpl = (ChapterModelImpl)chapter;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, chapter, merge);

			chapter.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ChapterModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((chapterModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(chapterModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);

				args = new Object[] {
						Long.valueOf(chapterModelImpl.getCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);
			}

			if ((chapterModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(chapterModelImpl.getOriginalSubjectId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBJECT, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECT,
					args);

				args = new Object[] {
						Long.valueOf(chapterModelImpl.getSubjectId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBJECT, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECT,
					args);
			}
		}

		EntityCacheUtil.putResult(ChapterModelImpl.ENTITY_CACHE_ENABLED,
			ChapterImpl.class, chapter.getPrimaryKey(), chapter);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CHAPTERSEQUENCE,
				new Object[] {
					Long.valueOf(chapter.getChapterId()),
					Long.valueOf(chapter.getSequence())
				}, chapter);
		}
		else {
			if ((chapterModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_CHAPTERSEQUENCE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(chapterModelImpl.getOriginalChapterId()),
						Long.valueOf(chapterModelImpl.getOriginalSequence())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHAPTERSEQUENCE,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CHAPTERSEQUENCE,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CHAPTERSEQUENCE,
					new Object[] {
						Long.valueOf(chapter.getChapterId()),
						Long.valueOf(chapter.getSequence())
					}, chapter);
			}
		}

		return chapter;
	}

	protected Chapter toUnwrappedModel(Chapter chapter) {
		if (chapter instanceof ChapterImpl) {
			return chapter;
		}

		ChapterImpl chapterImpl = new ChapterImpl();

		chapterImpl.setNew(chapter.isNew());
		chapterImpl.setPrimaryKey(chapter.getPrimaryKey());

		chapterImpl.setChapterId(chapter.getChapterId());
		chapterImpl.setCompanyId(chapter.getCompanyId());
		chapterImpl.setOrganizationId(chapter.getOrganizationId());
		chapterImpl.setUserId(chapter.getUserId());
		chapterImpl.setUserName(chapter.getUserName());
		chapterImpl.setCreateDate(chapter.getCreateDate());
		chapterImpl.setModifiedDate(chapter.getModifiedDate());
		chapterImpl.setSequence(chapter.getSequence());
		chapterImpl.setName(chapter.getName());
		chapterImpl.setSubjectId(chapter.getSubjectId());
		chapterImpl.setNote(chapter.getNote());

		return chapterImpl;
	}

	/**
	 * Returns the chapter with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the chapter
	 * @return the chapter
	 * @throws com.liferay.portal.NoSuchModelException if a chapter with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Chapter findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the chapter with the primary key or throws a {@link info.diit.portal.lessonplan.NoSuchChapterException} if it could not be found.
	 *
	 * @param chapterId the primary key of the chapter
	 * @return the chapter
	 * @throws info.diit.portal.lessonplan.NoSuchChapterException if a chapter with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter findByPrimaryKey(long chapterId)
		throws NoSuchChapterException, SystemException {
		Chapter chapter = fetchByPrimaryKey(chapterId);

		if (chapter == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + chapterId);
			}

			throw new NoSuchChapterException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				chapterId);
		}

		return chapter;
	}

	/**
	 * Returns the chapter with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the chapter
	 * @return the chapter, or <code>null</code> if a chapter with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Chapter fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the chapter with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param chapterId the primary key of the chapter
	 * @return the chapter, or <code>null</code> if a chapter with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter fetchByPrimaryKey(long chapterId) throws SystemException {
		Chapter chapter = (Chapter)EntityCacheUtil.getResult(ChapterModelImpl.ENTITY_CACHE_ENABLED,
				ChapterImpl.class, chapterId);

		if (chapter == _nullChapter) {
			return null;
		}

		if (chapter == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				chapter = (Chapter)session.get(ChapterImpl.class,
						Long.valueOf(chapterId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (chapter != null) {
					cacheResult(chapter);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(ChapterModelImpl.ENTITY_CACHE_ENABLED,
						ChapterImpl.class, chapterId, _nullChapter);
				}

				closeSession(session);
			}
		}

		return chapter;
	}

	/**
	 * Returns all the chapters where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching chapters
	 * @throws SystemException if a system exception occurred
	 */
	public List<Chapter> findByCompany(long companyId)
		throws SystemException {
		return findByCompany(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the chapters where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chapters
	 * @param end the upper bound of the range of chapters (not inclusive)
	 * @return the range of matching chapters
	 * @throws SystemException if a system exception occurred
	 */
	public List<Chapter> findByCompany(long companyId, int start, int end)
		throws SystemException {
		return findByCompany(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the chapters where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chapters
	 * @param end the upper bound of the range of chapters (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chapters
	 * @throws SystemException if a system exception occurred
	 */
	public List<Chapter> findByCompany(long companyId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<Chapter> list = (List<Chapter>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Chapter chapter : list) {
				if ((companyId != chapter.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CHAPTER_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(ChapterModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<Chapter>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first chapter in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chapter
	 * @throws info.diit.portal.lessonplan.NoSuchChapterException if a matching chapter could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter findByCompany_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchChapterException, SystemException {
		Chapter chapter = fetchByCompany_First(companyId, orderByComparator);

		if (chapter != null) {
			return chapter;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchChapterException(msg.toString());
	}

	/**
	 * Returns the first chapter in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chapter, or <code>null</code> if a matching chapter could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter fetchByCompany_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Chapter> list = findByCompany(companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last chapter in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chapter
	 * @throws info.diit.portal.lessonplan.NoSuchChapterException if a matching chapter could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter findByCompany_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchChapterException, SystemException {
		Chapter chapter = fetchByCompany_Last(companyId, orderByComparator);

		if (chapter != null) {
			return chapter;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchChapterException(msg.toString());
	}

	/**
	 * Returns the last chapter in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chapter, or <code>null</code> if a matching chapter could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter fetchByCompany_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompany(companyId);

		List<Chapter> list = findByCompany(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the chapters before and after the current chapter in the ordered set where companyId = &#63;.
	 *
	 * @param chapterId the primary key of the current chapter
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chapter
	 * @throws info.diit.portal.lessonplan.NoSuchChapterException if a chapter with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter[] findByCompany_PrevAndNext(long chapterId, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchChapterException, SystemException {
		Chapter chapter = findByPrimaryKey(chapterId);

		Session session = null;

		try {
			session = openSession();

			Chapter[] array = new ChapterImpl[3];

			array[0] = getByCompany_PrevAndNext(session, chapter, companyId,
					orderByComparator, true);

			array[1] = chapter;

			array[2] = getByCompany_PrevAndNext(session, chapter, companyId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Chapter getByCompany_PrevAndNext(Session session,
		Chapter chapter, long companyId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CHAPTER_WHERE);

		query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(ChapterModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(chapter);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Chapter> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the chapters where subjectId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @return the matching chapters
	 * @throws SystemException if a system exception occurred
	 */
	public List<Chapter> findBySubject(long subjectId)
		throws SystemException {
		return findBySubject(subjectId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the chapters where subjectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param subjectId the subject ID
	 * @param start the lower bound of the range of chapters
	 * @param end the upper bound of the range of chapters (not inclusive)
	 * @return the range of matching chapters
	 * @throws SystemException if a system exception occurred
	 */
	public List<Chapter> findBySubject(long subjectId, int start, int end)
		throws SystemException {
		return findBySubject(subjectId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the chapters where subjectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param subjectId the subject ID
	 * @param start the lower bound of the range of chapters
	 * @param end the upper bound of the range of chapters (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chapters
	 * @throws SystemException if a system exception occurred
	 */
	public List<Chapter> findBySubject(long subjectId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECT;
			finderArgs = new Object[] { subjectId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SUBJECT;
			finderArgs = new Object[] { subjectId, start, end, orderByComparator };
		}

		List<Chapter> list = (List<Chapter>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Chapter chapter : list) {
				if ((subjectId != chapter.getSubjectId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CHAPTER_WHERE);

			query.append(_FINDER_COLUMN_SUBJECT_SUBJECTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(ChapterModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				list = (List<Chapter>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first chapter in the ordered set where subjectId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chapter
	 * @throws info.diit.portal.lessonplan.NoSuchChapterException if a matching chapter could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter findBySubject_First(long subjectId,
		OrderByComparator orderByComparator)
		throws NoSuchChapterException, SystemException {
		Chapter chapter = fetchBySubject_First(subjectId, orderByComparator);

		if (chapter != null) {
			return chapter;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("subjectId=");
		msg.append(subjectId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchChapterException(msg.toString());
	}

	/**
	 * Returns the first chapter in the ordered set where subjectId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chapter, or <code>null</code> if a matching chapter could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter fetchBySubject_First(long subjectId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Chapter> list = findBySubject(subjectId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last chapter in the ordered set where subjectId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chapter
	 * @throws info.diit.portal.lessonplan.NoSuchChapterException if a matching chapter could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter findBySubject_Last(long subjectId,
		OrderByComparator orderByComparator)
		throws NoSuchChapterException, SystemException {
		Chapter chapter = fetchBySubject_Last(subjectId, orderByComparator);

		if (chapter != null) {
			return chapter;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("subjectId=");
		msg.append(subjectId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchChapterException(msg.toString());
	}

	/**
	 * Returns the last chapter in the ordered set where subjectId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chapter, or <code>null</code> if a matching chapter could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter fetchBySubject_Last(long subjectId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBySubject(subjectId);

		List<Chapter> list = findBySubject(subjectId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the chapters before and after the current chapter in the ordered set where subjectId = &#63;.
	 *
	 * @param chapterId the primary key of the current chapter
	 * @param subjectId the subject ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chapter
	 * @throws info.diit.portal.lessonplan.NoSuchChapterException if a chapter with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter[] findBySubject_PrevAndNext(long chapterId, long subjectId,
		OrderByComparator orderByComparator)
		throws NoSuchChapterException, SystemException {
		Chapter chapter = findByPrimaryKey(chapterId);

		Session session = null;

		try {
			session = openSession();

			Chapter[] array = new ChapterImpl[3];

			array[0] = getBySubject_PrevAndNext(session, chapter, subjectId,
					orderByComparator, true);

			array[1] = chapter;

			array[2] = getBySubject_PrevAndNext(session, chapter, subjectId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Chapter getBySubject_PrevAndNext(Session session,
		Chapter chapter, long subjectId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CHAPTER_WHERE);

		query.append(_FINDER_COLUMN_SUBJECT_SUBJECTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(ChapterModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(subjectId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(chapter);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Chapter> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns the chapter where chapterId = &#63; and sequence = &#63; or throws a {@link info.diit.portal.lessonplan.NoSuchChapterException} if it could not be found.
	 *
	 * @param chapterId the chapter ID
	 * @param sequence the sequence
	 * @return the matching chapter
	 * @throws info.diit.portal.lessonplan.NoSuchChapterException if a matching chapter could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter findByChapterSequence(long chapterId, long sequence)
		throws NoSuchChapterException, SystemException {
		Chapter chapter = fetchByChapterSequence(chapterId, sequence);

		if (chapter == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("chapterId=");
			msg.append(chapterId);

			msg.append(", sequence=");
			msg.append(sequence);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchChapterException(msg.toString());
		}

		return chapter;
	}

	/**
	 * Returns the chapter where chapterId = &#63; and sequence = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param chapterId the chapter ID
	 * @param sequence the sequence
	 * @return the matching chapter, or <code>null</code> if a matching chapter could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter fetchByChapterSequence(long chapterId, long sequence)
		throws SystemException {
		return fetchByChapterSequence(chapterId, sequence, true);
	}

	/**
	 * Returns the chapter where chapterId = &#63; and sequence = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param chapterId the chapter ID
	 * @param sequence the sequence
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching chapter, or <code>null</code> if a matching chapter could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter fetchByChapterSequence(long chapterId, long sequence,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { chapterId, sequence };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_CHAPTERSEQUENCE,
					finderArgs, this);
		}

		if (result instanceof Chapter) {
			Chapter chapter = (Chapter)result;

			if ((chapterId != chapter.getChapterId()) ||
					(sequence != chapter.getSequence())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_CHAPTER_WHERE);

			query.append(_FINDER_COLUMN_CHAPTERSEQUENCE_CHAPTERID_2);

			query.append(_FINDER_COLUMN_CHAPTERSEQUENCE_SEQUENCE_2);

			query.append(ChapterModelImpl.ORDER_BY_JPQL);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(chapterId);

				qPos.add(sequence);

				List<Chapter> list = q.list();

				result = list;

				Chapter chapter = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CHAPTERSEQUENCE,
						finderArgs, list);
				}
				else {
					chapter = list.get(0);

					cacheResult(chapter);

					if ((chapter.getChapterId() != chapterId) ||
							(chapter.getSequence() != sequence)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CHAPTERSEQUENCE,
							finderArgs, chapter);
					}
				}

				return chapter;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CHAPTERSEQUENCE,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (Chapter)result;
			}
		}
	}

	/**
	 * Returns all the chapters.
	 *
	 * @return the chapters
	 * @throws SystemException if a system exception occurred
	 */
	public List<Chapter> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the chapters.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of chapters
	 * @param end the upper bound of the range of chapters (not inclusive)
	 * @return the range of chapters
	 * @throws SystemException if a system exception occurred
	 */
	public List<Chapter> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the chapters.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of chapters
	 * @param end the upper bound of the range of chapters (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of chapters
	 * @throws SystemException if a system exception occurred
	 */
	public List<Chapter> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Chapter> list = (List<Chapter>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CHAPTER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CHAPTER.concat(ChapterModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Chapter>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Chapter>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the chapters where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCompany(long companyId) throws SystemException {
		for (Chapter chapter : findByCompany(companyId)) {
			remove(chapter);
		}
	}

	/**
	 * Removes all the chapters where subjectId = &#63; from the database.
	 *
	 * @param subjectId the subject ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBySubject(long subjectId) throws SystemException {
		for (Chapter chapter : findBySubject(subjectId)) {
			remove(chapter);
		}
	}

	/**
	 * Removes the chapter where chapterId = &#63; and sequence = &#63; from the database.
	 *
	 * @param chapterId the chapter ID
	 * @param sequence the sequence
	 * @return the chapter that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public Chapter removeByChapterSequence(long chapterId, long sequence)
		throws NoSuchChapterException, SystemException {
		Chapter chapter = findByChapterSequence(chapterId, sequence);

		return remove(chapter);
	}

	/**
	 * Removes all the chapters from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Chapter chapter : findAll()) {
			remove(chapter);
		}
	}

	/**
	 * Returns the number of chapters where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching chapters
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCompany(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CHAPTER_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of chapters where subjectId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @return the number of matching chapters
	 * @throws SystemException if a system exception occurred
	 */
	public int countBySubject(long subjectId) throws SystemException {
		Object[] finderArgs = new Object[] { subjectId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_SUBJECT,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CHAPTER_WHERE);

			query.append(_FINDER_COLUMN_SUBJECT_SUBJECTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_SUBJECT,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of chapters where chapterId = &#63; and sequence = &#63;.
	 *
	 * @param chapterId the chapter ID
	 * @param sequence the sequence
	 * @return the number of matching chapters
	 * @throws SystemException if a system exception occurred
	 */
	public int countByChapterSequence(long chapterId, long sequence)
		throws SystemException {
		Object[] finderArgs = new Object[] { chapterId, sequence };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_CHAPTERSEQUENCE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CHAPTER_WHERE);

			query.append(_FINDER_COLUMN_CHAPTERSEQUENCE_CHAPTERID_2);

			query.append(_FINDER_COLUMN_CHAPTERSEQUENCE_SEQUENCE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(chapterId);

				qPos.add(sequence);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CHAPTERSEQUENCE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of chapters.
	 *
	 * @return the number of chapters
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CHAPTER);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the chapter persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.lessonplan.model.Chapter")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Chapter>> listenersList = new ArrayList<ModelListener<Chapter>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Chapter>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ChapterImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_CHAPTER = "SELECT chapter FROM Chapter chapter";
	private static final String _SQL_SELECT_CHAPTER_WHERE = "SELECT chapter FROM Chapter chapter WHERE ";
	private static final String _SQL_COUNT_CHAPTER = "SELECT COUNT(chapter) FROM Chapter chapter";
	private static final String _SQL_COUNT_CHAPTER_WHERE = "SELECT COUNT(chapter) FROM Chapter chapter WHERE ";
	private static final String _FINDER_COLUMN_COMPANY_COMPANYID_2 = "chapter.companyId = ?";
	private static final String _FINDER_COLUMN_SUBJECT_SUBJECTID_2 = "chapter.subjectId = ?";
	private static final String _FINDER_COLUMN_CHAPTERSEQUENCE_CHAPTERID_2 = "chapter.chapterId = ? AND ";
	private static final String _FINDER_COLUMN_CHAPTERSEQUENCE_SEQUENCE_2 = "chapter.sequence = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "chapter.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Chapter exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Chapter exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ChapterPersistenceImpl.class);
	private static Chapter _nullChapter = new ChapterImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Chapter> toCacheModel() {
				return _nullChapterCacheModel;
			}
		};

	private static CacheModel<Chapter> _nullChapterCacheModel = new CacheModel<Chapter>() {
			public Chapter toEntityModel() {
				return _nullChapter;
			}
		};
}
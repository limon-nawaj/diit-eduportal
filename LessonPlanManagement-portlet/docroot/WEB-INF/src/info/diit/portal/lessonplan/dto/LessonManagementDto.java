package info.diit.portal.lessonplan.dto;

public class LessonManagementDto {

	private long id;
	private CampusDto campus;
	private BatchDto batch;
	private SubjectDto subject;
	private LessonDto lesson;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public CampusDto getCampus() {
		return campus;
	}
	public void setCampus(CampusDto campus) {
		this.campus = campus;
	}
	public BatchDto getBatch() {
		return batch;
	}
	public void setBatch(BatchDto batch) {
		this.batch = batch;
	}
	public SubjectDto getSubject() {
		return subject;
	}
	public void setSubject(SubjectDto subject) {
		this.subject = subject;
	}
	public LessonDto getLesson() {
		return lesson;
	}
	public void setLesson(LessonDto lesson) {
		this.lesson = lesson;
	}
}

/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.service.impl;

import info.diit.portal.lessonplan.model.LessonTopic;
import info.diit.portal.lessonplan.service.base.LessonTopicLocalServiceBaseImpl;
import info.diit.portal.lessonplan.service.persistence.LessonTopicUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the lesson topic local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.lessonplan.service.LessonTopicLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see info.diit.portal.lessonplan.service.base.LessonTopicLocalServiceBaseImpl
 * @see info.diit.portal.lessonplan.service.LessonTopicLocalServiceUtil
 */
public class LessonTopicLocalServiceImpl extends LessonTopicLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.lessonplan.service.LessonTopicLocalServiceUtil} to access the lesson topic local service.
	 */
	public List<LessonTopic> findByLessonPlan(long lessonPlanId) throws SystemException{
		return LessonTopicUtil.findByLessonPlan(lessonPlanId);
	}
	
	public List<LessonTopic> findByLessonTopic(long lessonPlanId, long topicId) throws SystemException{
		return LessonTopicUtil.findByLessonTopic(lessonPlanId, topicId);
	}
}
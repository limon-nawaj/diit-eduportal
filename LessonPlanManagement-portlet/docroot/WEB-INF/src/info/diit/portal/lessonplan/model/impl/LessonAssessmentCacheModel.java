/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.lessonplan.model.LessonAssessment;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing LessonAssessment in entity cache.
 *
 * @author limon
 * @see LessonAssessment
 * @generated
 */
public class LessonAssessmentCacheModel implements CacheModel<LessonAssessment>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{lessonAssessmentId=");
		sb.append(lessonAssessmentId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", lessonPlanId=");
		sb.append(lessonPlanId);
		sb.append(", assessmentType=");
		sb.append(assessmentType);
		sb.append(", number=");
		sb.append(number);
		sb.append("}");

		return sb.toString();
	}

	public LessonAssessment toEntityModel() {
		LessonAssessmentImpl lessonAssessmentImpl = new LessonAssessmentImpl();

		lessonAssessmentImpl.setLessonAssessmentId(lessonAssessmentId);
		lessonAssessmentImpl.setCompanyId(companyId);
		lessonAssessmentImpl.setUserId(userId);

		if (userName == null) {
			lessonAssessmentImpl.setUserName(StringPool.BLANK);
		}
		else {
			lessonAssessmentImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			lessonAssessmentImpl.setCreateDate(null);
		}
		else {
			lessonAssessmentImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			lessonAssessmentImpl.setModifiedDate(null);
		}
		else {
			lessonAssessmentImpl.setModifiedDate(new Date(modifiedDate));
		}

		lessonAssessmentImpl.setLessonPlanId(lessonPlanId);
		lessonAssessmentImpl.setAssessmentType(assessmentType);
		lessonAssessmentImpl.setNumber(number);

		lessonAssessmentImpl.resetOriginalValues();

		return lessonAssessmentImpl;
	}

	public long lessonAssessmentId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long lessonPlanId;
	public long assessmentType;
	public long number;
}
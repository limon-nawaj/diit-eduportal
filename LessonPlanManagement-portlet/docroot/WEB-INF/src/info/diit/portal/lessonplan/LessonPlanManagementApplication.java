package info.diit.portal.lessonplan;


import info.diit.portal.assessment.model.AssessmentType;
import info.diit.portal.assessment.service.AssessmentTypeLocalServiceUtil;
import info.diit.portal.batch.subject.model.BatchSubject;
import info.diit.portal.batch.subject.service.BatchSubjectLocalServiceUtil;
import info.diit.portal.coursesubject.model.Subject;
import info.diit.portal.coursesubject.service.SubjectLocalServiceUtil;
import info.diit.portal.lessonplan.dto.AssessmentTypeDto;
import info.diit.portal.lessonplan.dto.CampusDto;
import info.diit.portal.lessonplan.dto.LessonContentDto;
import info.diit.portal.lessonplan.dto.LessonPlanDto;
import info.diit.portal.lessonplan.dto.SubjectDto;
import info.diit.portal.lessonplan.model.Chapter;
import info.diit.portal.lessonplan.model.LessonAssessment;
import info.diit.portal.lessonplan.model.LessonPlan;
import info.diit.portal.lessonplan.model.LessonTopic;
import info.diit.portal.lessonplan.model.Topic;
import info.diit.portal.lessonplan.model.impl.LessonAssessmentImpl;
import info.diit.portal.lessonplan.model.impl.LessonPlanImpl;
import info.diit.portal.lessonplan.model.impl.LessonTopicImpl;
import info.diit.portal.lessonplan.service.ChapterLocalServiceUtil;
import info.diit.portal.lessonplan.service.LessonAssessmentLocalServiceUtil;
import info.diit.portal.lessonplan.service.LessonPlanLocalServiceUtil;
import info.diit.portal.lessonplan.service.LessonTopicLocalServiceUtil;
import info.diit.portal.lessonplan.service.TopicLocalServiceUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import net.atontech.vaadin.ui.numericfield.NumericField;
import net.atontech.vaadin.ui.numericfield.widgetset.shared.NumericFieldType;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import com.liferay.portal.kernel.dao.jdbc.DataAccess;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ContainerHierarchicalWrapper;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;
import com.vaadin.terminal.StreamResource;
import com.vaadin.terminal.StreamResource.StreamSource;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Component.Event;
import com.vaadin.ui.Component.Listener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class LessonPlanManagementApplication extends Application implements PortletRequestListener {

	private ThemeDisplay themeDisplay;
	private final static String ASSESSMENT_TITLE = "title";
	private final static String ASSESSMENT_NUMBER = "numberOfAssessment";
	
	private Window window;
	
	private final static String LESSON_CONTENT = "content";
	private final static String LESSON_SEQUENCE = "sequence";
	private final static String LESSON_RESOURCE = "resource";
	private final static String LESSON_ACTIVITIES = "activities";
	
	private final static String DATE_FORMATE = "dd/MM/yyyy";
	
	private List<Organization> userOrganizationList;
	private List<CampusDto> campusList;
	private Set<SubjectDto> subjectList;
	
	private LessonPlan lessonPlan;
	private LessonAssessment assessment;
	private LessonTopic lessonTopic;
	
	private TabSheet tabSheet;
	
	private Connection connection;
	
	public void init() {
		window = new Window();

		setMainWindow(window);

		try {
			userOrganizationList = themeDisplay.getUser().getOrganizations();
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		try {
			connection = DataAccess.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		loadCampus();
		if (userOrganizationList.size()==1) {
			loadSubject();
		}
		
		window.addComponent(loadTabSheet());
		loadSubjectComboBox();
		loadAssessmentType();
		loadLessonPlan();
		
	}

	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}
	
	private TabSheet loadTabSheet(){
		tabSheet = new TabSheet();
		tabSheet.addTab(mainLayout(), "Lesson Plan");
		tabSheet.addTab(lessonPlanList(), "All Lesson Plan");
		return tabSheet;
	}
	
	private TextField lessonTitleField;
	private ComboBox campusComboBox;
	private ComboBox subjectComboBox;
	private DateField planDate;
	private NumericField totalClassField;
	private NumericField durationField;
	private Table assessmentTypeTable;
	private TextArea objectiveArea;
	
	private BeanItemContainer<AssessmentTypeDto> typeContainer;
	private BeanItemContainer<LessonContentDto> contentContainer;
	private ContainerHierarchicalWrapper contentWrapperContainer;
	
	private TreeTable contentTable;
	
	private GridLayout mainLayout;
	
	private List<LessonAssessment> lessonAssessmentList;
	private Set<Long> topicId;
	
	private GridLayout mainLayout(){
		mainLayout = new GridLayout(8, 20);
		mainLayout.setWidth("100%");
		mainLayout.setSpacing(true);
		
		lessonTitleField = new TextField("Lesson Plan Title");
		lessonTitleField.setWidth("100%");
		lessonTitleField.setRequired(true);
		
		campusComboBox = new ComboBox("Campus");
		campusComboBox.setWidth("100%");
		campusComboBox.setImmediate(true);
		
		if (campusList!=null) {
			for (CampusDto campus : campusList) {
				campusComboBox.addItem(campus);
			}
		}
		
		subjectComboBox = new ComboBox("Subject");
		subjectComboBox.setWidth("100%");
		subjectComboBox.setImmediate(true);
		subjectComboBox.setRequired(true);
		
		campusComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				loadSubject();
				loadSubjectComboBox();
			}
		});
		
		subjectComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				container();
			}
		});
		
		planDate = new DateField("Date");
		planDate.setWidth("100%");
		planDate.setDateFormat(DATE_FORMATE);
		planDate.setValue(new Date());
		planDate.setResolution(DateField.RESOLUTION_DAY);
		
		totalClassField = new NumericField("Total Class");
		totalClassField.setNullRepresentation("");
		totalClassField.setNumberType(NumericFieldType.INTEGER);
		totalClassField.setWidth("100%");
		totalClassField.setRequired(true);
		
		durationField = new NumericField("Total Duration (hours)");
		durationField.setNullRepresentation("");
		durationField.setNumberType(NumericFieldType.INTEGER);
		durationField.setWidth("100%");
		durationField.setRequired(true);
		
		typeContainer = new BeanItemContainer<AssessmentTypeDto>(AssessmentTypeDto.class);
		assessmentTypeTable = new Table("Assessment Type", typeContainer);
		assessmentTypeTable.setWidth("100%");
		assessmentTypeTable.setHeight("200px");
		assessmentTypeTable.setEditable(true);
		assessmentTypeTable.setSelectable(true);
		assessmentTypeTable.setEditable(true);
		assessmentTypeTable.setSelectable(true);
		assessmentTypeTable.setColumnHeader(ASSESSMENT_TITLE, "Title");
		assessmentTypeTable.setColumnHeader(ASSESSMENT_NUMBER, "Number");
		assessmentTypeTable.setVisibleColumns(new String[]{ASSESSMENT_TITLE, ASSESSMENT_NUMBER});
		
		assessmentTypeTable.setColumnExpandRatio(ASSESSMENT_TITLE, 3);
		assessmentTypeTable.setColumnExpandRatio(ASSESSMENT_NUMBER, 1);
		
		assessmentTypeTable.setTableFieldFactory(new DefaultFieldFactory(){
			
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				if (propertyId.equals(ASSESSMENT_TITLE)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					field.setNullRepresentation("");
					return field;
				}else if (propertyId.equals(ASSESSMENT_NUMBER)) {
					NumericField field = new NumericField();
					field.setNullSettingAllowed(true);
					field.setNumberType(NumericFieldType.INTEGER);
					field.setWidth("100%");
					field.setNullRepresentation("");
					field.setReadOnly(false);
					return field;
				}
				
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		contentContainer = new BeanItemContainer<LessonContentDto>(LessonContentDto.class);
        contentWrapperContainer = new ContainerHierarchicalWrapper(contentContainer);
		
		contentTable = new TreeTable("Table of Content", contentWrapperContainer);
		contentTable.setWidth("100%");
		contentTable.setEditable(true);
		contentTable.setSelectable(true);
		
		contentTable.setColumnHeader(LESSON_CONTENT, "Content");
		contentTable.setColumnHeader(LESSON_SEQUENCE, "Class Sequence");
		contentTable.setColumnHeader(LESSON_RESOURCE, "Resource");
		contentTable.setColumnHeader(LESSON_ACTIVITIES, "Activities");
		
		
		contentTable.setVisibleColumns(new String[]{LESSON_CONTENT, LESSON_SEQUENCE, LESSON_RESOURCE, LESSON_ACTIVITIES});
		
		contentTable.setColumnExpandRatio(LESSON_CONTENT, 4);
		contentTable.setColumnExpandRatio(LESSON_SEQUENCE, 2);
		contentTable.setColumnExpandRatio(LESSON_RESOURCE, 3);
		contentTable.setColumnExpandRatio(LESSON_ACTIVITIES, 3);
		
		contentTable.setTableFieldFactory(new DefaultFieldFactory(){
			
			@Override
			public Field createField(Container container, final Object itemId,
					Object propertyId, Component uiContext) {
				
				if (propertyId.equals(LESSON_CONTENT)) {
					TextField field = new TextField();
					field.setReadOnly(true);
					return field;
				}else if (propertyId.equals(LESSON_SEQUENCE)) {
					final TextField field = new TextField();
					field.setWidth("100%");
					field.setNullRepresentation("");
					field.addListener(new FocusListener() {
						
						@Override
						public void focus(FocusEvent event) {
							contentTable.select(itemId);
							LessonContentDto selected = (LessonContentDto) contentTable.getValue();
							if (selected!=null) {
								if (selected.getParent()==0) {
									field.setReadOnly(true);
								}else{
									field.setReadOnly(false);
								}
							}
						}
					});
					
					return field;
				}else if (propertyId.equals(LESSON_RESOURCE)) {
					final TextField field = new TextField();
					field.setReadOnly(false);
					field.setWidth("100%");
					field.setNullRepresentation("");
					field.addListener(new FocusListener() {
						
						@Override
						public void focus(FocusEvent event) {
							contentTable.select(itemId);
							LessonContentDto selected = (LessonContentDto) contentTable.getValue();
							if (selected!=null) {
								if (selected.getParent()==0) {
									field.setReadOnly(true);
								}else{
									field.setReadOnly(false);
								}
							}
						}
					});
					
					return field;
				}else if (propertyId.equals(LESSON_ACTIVITIES)) {
					final TextField field = new TextField();
					field.setReadOnly(false);
					field.setWidth("100%");
					field.setNullRepresentation("");
					field.addListener(new FocusListener() {
						
						@Override
						public void focus(FocusEvent event) {
							contentTable.select(itemId);
							LessonContentDto selected = (LessonContentDto) contentTable.getValue();
							if (selected!=null) {
								if (selected.getParent()==0) {
									field.setReadOnly(true);
								}else{
									field.setReadOnly(false);
								}
							}
						}
					});
					
					return field;
				}
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		objectiveArea = new TextArea("Objective");
		objectiveArea.setWidth("100%");
		
		Button saveButton = new Button("Save");
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (lessonPlan==null) {
					lessonPlan = new LessonPlanImpl();
					lessonPlan.setNew(true);
				}
				
				lessonPlan.setCompanyId(themeDisplay.getCompany().getCompanyId());
				lessonPlan.setUserId(themeDisplay.getUser().getUserId());
				lessonPlan.setUserName(themeDisplay.getUser().getFullName());
				
				String title = lessonTitleField.getValue().toString();
				if (title!=null) {
					lessonPlan.setTitle(title);
				} else {
					window.showNotification("Please insert a title", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				CampusDto campus = (CampusDto) campusComboBox.getValue();
				if (campus!=null) {
					lessonPlan.setOrganization(campus.getId());
				}
				
				SubjectDto subject = (SubjectDto) subjectComboBox.getValue();
				if (subject!=null) {
					lessonPlan.setSubject(subject.getId());
				} else {
					window.showNotification("Please select a subject", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				Date lessonDate = (Date) planDate.getValue();
				if (lessonDate!=null) {
					lessonDate.setHours(0);
					lessonDate.setMinutes(0);
					lessonDate.setSeconds(0);
					lessonPlan.setPlanDate(lessonDate);
				}
				
				if (totalClassField.getValue().toString()!=null && totalClassField.getValue().toString() != "") {
					long totalClass = new Long(totalClassField.getValue().toString());
					if (totalClass>0) {
						lessonPlan.setTotalClass(totalClass);
					}
				} else {
					window.showNotification("Please insert the number of total class", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				if (durationField.getValue().toString() !=null && durationField.getValue().toString()!="") {
					long duration = new Long(durationField.getValue().toString());
					if (duration>0) {
						lessonPlan.setTotalDuration(duration);
					}
				} else {
					window.showNotification("Please class duration", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				String objective = objectiveArea.getValue().toString();
				if (objective!=null) {
					lessonPlan.setObjective(objective);
				}
				
				try {
					if (lessonPlan.isNew()) {
						lessonPlan.setCreateDate(new Date());
						LessonPlanLocalServiceUtil.addLessonPlan(lessonPlan);
						saveAssessment(lessonPlan);
						saveTopic(lessonPlan);
						window.showNotification("Lesson plan save successfully");
					}else{
						lessonPlan.setModifiedDate(new Date());
						LessonPlanLocalServiceUtil.updateLessonPlan(lessonPlan);
						
						List<LessonTopic> topicList = LessonTopicLocalServiceUtil.findByLessonPlan(lessonPlan.getLessonPlanId());
						if (topicList!=null) {
							for (LessonTopic lessonTopic : topicList) {
								LessonTopicLocalServiceUtil.deleteLessonTopic(lessonTopic);
							}
						}
						
						saveAssessment(lessonPlan);
						saveTopic(lessonPlan);
						window.showNotification("Lesson plan update successfully");
					}
					loadLessonPlan();
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
		
		Button resetButton = new Button("Reset");
		resetButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				reset();
			}
		});
		
		Label spacer = new Label();
		HorizontalLayout rowLayout = new HorizontalLayout();
		rowLayout.setWidth("100%");
		rowLayout.setSpacing(true);
		
		rowLayout.addComponent(spacer);
		rowLayout.addComponent(saveButton);
		rowLayout.addComponent(resetButton);
		rowLayout.setExpandRatio(spacer, 1);
		
		if (userOrganizationList.size()==1) {
			mainLayout.addComponent(lessonTitleField, 0, 0, 2, 0);
			mainLayout.addComponent(subjectComboBox, 5, 0, 7, 0);
			mainLayout.addComponent(planDate, 0, 1, 2, 1);
			mainLayout.addComponent(totalClassField, 5, 1, 7, 1);
			mainLayout.addComponent(durationField, 0, 2, 2, 2);
			
			mainLayout.addComponent(assessmentTypeTable, 0, 3, 2, 3);
			mainLayout.addComponent(contentTable, 0, 5, 7, 5);
			mainLayout.addComponent(objectiveArea, 0, 6, 7, 6);
			mainLayout.addComponent(rowLayout, 0, 7, 7, 7);
		}else if (userOrganizationList.size()>1) {
			mainLayout.addComponent(lessonTitleField, 0, 0, 2, 0);
			mainLayout.addComponent(campusComboBox, 5, 0, 7, 0);
			mainLayout.addComponent(subjectComboBox, 0, 1, 2, 1);
			mainLayout.addComponent(planDate, 5, 1, 7, 1);
			mainLayout.addComponent(totalClassField, 0, 2, 2, 2);
			mainLayout.addComponent(durationField, 5, 2, 7, 2);
			
			mainLayout.addComponent(assessmentTypeTable, 0, 3, 2, 3);
			mainLayout.addComponent(contentTable, 0, 5, 7, 5);
			mainLayout.addComponent(objectiveArea, 0, 6, 7, 6);
			mainLayout.addComponent(rowLayout, 0, 7, 7, 7);
		}
		
		return mainLayout;
	}
	
	private void saveAssessment(LessonPlan lessonPlan){
		try {
			
			List<LessonAssessment> assessmentList = LessonAssessmentLocalServiceUtil.findByLessonPlan(lessonPlan.getLessonPlanId());
			if (assessmentList!=null) {
				for (LessonAssessment lessonAssessment : assessmentList) {
					LessonAssessmentLocalServiceUtil.deleteLessonAssessment(lessonAssessment);
				}
			}
			
			assessment = new LessonAssessmentImpl();
			
			assessment.setCompanyId(themeDisplay.getCompany().getCompanyId());
			assessment.setUserId(themeDisplay.getUser().getUserId());
			assessment.setUserName(themeDisplay.getUser().getFullName());
			assessment.setLessonPlanId(lessonPlan.getLessonPlanId());
			
			if (typeContainer!=null) {
				for (int i = 0; i < typeContainer.size(); i++) {
					AssessmentTypeDto content = typeContainer.getIdByIndex(i);
//					window.showNotification("Content: "+content.getTitle()+" Number:"+content.getNumberOfAssessment());
					if (content.getNumberOfAssessment()!=null && !content.getNumberOfAssessment().equals("")) {
						assessment.setAssessmentType(content.getId());
						assessment.setNumber(content.getNumberOfAssessment());
						LessonAssessmentLocalServiceUtil.addLessonAssessment(assessment);
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void saveTopic(LessonPlan lessonPlan){
		try{
			lessonTopic = new LessonTopicImpl();
			
			lessonTopic.setCompanyId(themeDisplay.getCompany().getCompanyId());
			lessonTopic.setUserId(themeDisplay.getUser().getUserId());
			lessonTopic.setUserName(themeDisplay.getUser().getFullName());
			lessonTopic.setLessonPlanId(lessonPlan.getLessonPlanId());
			
			if (contentContainer!=null) {
				for (int i = 0; i < contentContainer.size(); i++) {
					LessonContentDto content = (LessonContentDto) contentContainer.getIdByIndex(i);
					if (content.getSequence()!=null && !content.getSequence().equals("")) {
						String classSequence = content.getSequence();
						String[] sequenceArray = classSequence.split(",");
						int[] intArray = new int[sequenceArray.length];
						for (int j = 0; j < intArray.length; j++) {
							intArray[j] = Integer.parseInt(sequenceArray[j].trim());
							lessonTopic.setTopic(content.getId());
							lessonTopic.setResource(content.getResource());
							lessonTopic.setActivities(content.getActivities());
							lessonTopic.setClassSequence(intArray[j]);
							if (content.getParent()!=0) {
								LessonTopicLocalServiceUtil.addLessonTopic(lessonTopic);
							}
						}
						
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void loadSubjectComboBox(){
		subjectComboBox.removeAllItems();
		if (subjectList!=null) {
			for (SubjectDto subject : subjectList) {
				subjectComboBox.addItem(subject);
			}
		}
	}
	
	private void loadCampus(){
		if (userOrganizationList.size()>1) {
			campusList = new ArrayList<CampusDto>();
			
			for (Organization userOrg : userOrganizationList) {
				CampusDto campus = new CampusDto();
				campus.setId(userOrg.getOrganizationId());
				campus.setName(userOrg.getName());
				campusList.add(campus);
			}
		}
	}
	
	private void loadSubject(){
		subjectList = new HashSet<SubjectDto>();
		try {
			List<BatchSubject> batchSubjectList = BatchSubjectLocalServiceUtil.findByBatchSubjectTeacher(themeDisplay.getUser().getUserId());
//			window.showNotification("Batch Subject: "+batchSubjectList.size());
			Set<Long> subjectIds = new HashSet<Long>();
			for (BatchSubject batchSubject : batchSubjectList) {
				
				if (userOrganizationList.size()==1) {
					Organization org = userOrganizationList.get(0);
					if (batchSubject.getOrganizationId()==org.getOrganizationId()) {
						subjectIds.add(batchSubject.getSubjectId());
					}
//					subjectList.add(subjectDto);
				}else if (userOrganizationList.size()>1) {
					CampusDto campus = (CampusDto) campusComboBox.getValue();
					if (campus!=null && batchSubject.getOrganizationId()==campus.getId()) {
						/*SubjectDto subjectDto = new SubjectDto();
						subjectDto.setId(batchSubject.getSubjectId());
						Subject subject = SubjectLocalServiceUtil.fetchSubject(batchSubject.getSubjectId());
						subjectDto.setName(subject.getSubjectName());*/
//						subjectList.add(subjectDto);
						subjectIds.add(batchSubject.getSubjectId());
					}
				}
			}
			if (subjectIds!=null) {
				for (Long id : subjectIds) {
					SubjectDto subjectDto = new SubjectDto();
					Subject subject = SubjectLocalServiceUtil.fetchSubject(id);
					subjectDto.setId(subject.getSubjectId());
					subjectDto.setName(subject.getSubjectName());
					subjectList.add(subjectDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	}
	
	private void loadAssessmentType(){
		assessmentTypeTable.removeAllItems();
		try {
			List<AssessmentType> assessmentTypeList = AssessmentTypeLocalServiceUtil.findByCompany(themeDisplay.getCompany().getCompanyId());
			if (assessmentTypeList!=null) {
				for (AssessmentType assessmentType : assessmentTypeList) {
					AssessmentTypeDto typeDto = new AssessmentTypeDto();
					typeDto.setId(assessmentType.getAssessmentTypeId());
					typeDto.setTitle(assessmentType.getType());
					
					if (lessonAssessmentList!=null) {
						for (LessonAssessment lessonAssessment : lessonAssessmentList) {
							if (lessonAssessment.getAssessmentType()==assessmentType.getAssessmentTypeId()) {
								typeDto.setNumberOfAssessment((int) lessonAssessment.getNumber());
							}
						}
					}
					
					typeContainer.addBean(typeDto);
					assessmentTypeTable.refreshRowCache();
					assessmentTypeTable.requestRepaintAll();
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	}
	
	private void container() {
		contentTable.removeAllItems();
        try {
        	SubjectDto subject = (SubjectDto) subjectComboBox.getValue();
        	if (subject!=null) {
        		List<Chapter> chapters = ChapterLocalServiceUtil.findBySubject(subject.getId());
    			for (Chapter chp : chapters) {
    	            LessonContentDto chapterContent = new LessonContentDto();
    	            
    	            chapterContent.setId(chp.getChapterId());
    	            chapterContent.setContent(chp.getName());
    	            chapterContent.setParent(0);
    	            
    	            contentWrapperContainer.addItem(chapterContent);
    	            
    	            List<Topic> topics = TopicLocalServiceUtil.findByChapter(chp.getChapterId());
    	            
    	            if (topics.size()==0) {
    	            	contentWrapperContainer.setChildrenAllowed(chapterContent, false);
					}else{
						contentWrapperContainer.setChildrenAllowed(chapterContent, true);
					}
    	            
    	            if (topics!=null) {
    					for (Topic topic : topics) {
    						LessonContentDto parentTopic = new LessonContentDto();
    						parentTopic.setId(topic.getTopicId());
    						parentTopic.setContent(topic.getTopic());
    						parentTopic.setParent(chp.getChapterId());
    						
    						if (topic.getParentTopic()==0) {
    							contentWrapperContainer.addItem(parentTopic);
    							contentWrapperContainer.setParent(parentTopic, chapterContent);
    							
    							List<Topic> childList = TopicLocalServiceUtil.findByParentTopic(topic.getTopicId());
    							if (childList.size()==0) {
    								contentWrapperContainer.setChildrenAllowed(parentTopic, false);
    							}else{
    								contentWrapperContainer.setChildrenAllowed(parentTopic, true);
    							}
    							
    							if (topicId!=null) {
									for (Long id : topicId) {
										if (topic.getTopicId()==id) {
											if (lessonPlan!=null) {
												List<LessonTopic> lessonTopics = LessonTopicLocalServiceUtil.findByLessonTopic(lessonPlan.getLessonPlanId(), id);
												if (lessonTopics!=null) {
													String sequence = "";
													for (LessonTopic lessonTopic : lessonTopics) {
														sequence += lessonTopic.getClassSequence()+",";
														parentTopic.setActivities(lessonTopic.getActivities());
														parentTopic.setResource(lessonTopic.getResource());
													}
													sequence = sequence.substring(0,sequence.length()-1);
													parentTopic.setSequence(sequence);
													
												}
											}
										}
									}
								}
    							
    							recursiveTopic(parentTopic);
    						}
    					}
    				}
    	        }
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
	
	
	private void recursiveTopic(LessonContentDto topic){
    	try {
			List<Topic> allTopics = TopicLocalServiceUtil.findByParentTopic(topic.getId());
			if (allTopics!=null) {
				
				for (Topic topic2 : allTopics) {
					LessonContentDto childTopic = new LessonContentDto();
					childTopic.setId(topic2.getTopicId());
					childTopic.setParent(topic.getId());
					childTopic.setContent(topic2.getTopic());
					
					contentWrapperContainer.addItem(childTopic);
					contentWrapperContainer.setParent(childTopic, topic);
					
					List<Topic> childList = TopicLocalServiceUtil.findByParentTopic(topic2.getTopicId());
					
					if (childList.size()==0) {
						contentWrapperContainer.setChildrenAllowed(childTopic, false);
					}else{
						contentWrapperContainer.setChildrenAllowed(childTopic, true);
					}
					
					if (topicId!=null) {
						for (Long id : topicId) {
							if (topic2.getTopicId()==id) {
								if (lessonPlan!=null) {
									List<LessonTopic> lessonTopics = LessonTopicLocalServiceUtil.findByLessonTopic(lessonPlan.getLessonPlanId(), id);
									if (lessonTopics!=null) {
										String sequence = "";
										for (LessonTopic lessonTopic : lessonTopics) {
											sequence += lessonTopic.getClassSequence()+",";
										}
										sequence = sequence.substring(0,sequence.length()-1);
										childTopic.setSequence(sequence);
										childTopic.setResource(lessonTopic.getResource());
										childTopic.setActivities(lessonTopic.getActivities());
									}
								}
							}
						}
					}
					
					recursiveTopic(childTopic);
				}
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }

	private BeanItemContainer<LessonPlanDto> planContainer;
	private Table planTable;
	
	private final static String PLAN_NAME = "name";
	private final static String PLAN_SUBJECT = "subject";
	private final static String PLAN_DATE = "planDate";
	private final static String PLAN_CLASS = "totalClass";
	private final static String PLAN_DURATION = "duration";
	
	private final static String DATE_FORMAT = "dd/MM/yyyy";
	
	
	private GridLayout lessonPlanList(){
		GridLayout listLayout = new GridLayout(8, 5);
		listLayout.setSpacing(true);
		listLayout.setWidth("100%");
		
		final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		planContainer = new BeanItemContainer<LessonPlanDto>(LessonPlanDto.class);
		planTable = new Table("", planContainer){
			
			@Override
			protected String formatPropertyValue(Object rowId, Object colId, Property property){
				if (property.getValue()==null) {
					return null;
				}else if (property.getType()==Date.class) {
					return dateFormat.format(property.getValue());
				}
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		planTable.setWidth("100%");
		planTable.setSelectable(true);
		
		planTable.setColumnHeader(PLAN_NAME, "Name");
		planTable.setColumnHeader(PLAN_SUBJECT, "Subject");
		planTable.setColumnHeader(PLAN_DATE, "Date");
		planTable.setColumnHeader(PLAN_CLASS, "Class");
		planTable.setColumnHeader(PLAN_DURATION, "Duration");
		
		planTable.setVisibleColumns(new String[]{PLAN_NAME, PLAN_SUBJECT, PLAN_DATE, PLAN_CLASS, PLAN_DURATION});
		
		planTable.setColumnExpandRatio(PLAN_NAME, 2);
		planTable.setColumnExpandRatio(PLAN_SUBJECT, 2);
		planTable.setColumnExpandRatio(PLAN_DATE, 1);
		planTable.setColumnExpandRatio(PLAN_CLASS, 1);
		planTable.setColumnExpandRatio(PLAN_DURATION, 1);
		
		Button editButton = new Button("Edit");
		editButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				LessonPlanDto planDto = (LessonPlanDto) planTable.getValue();
				if (planDto!=null) {
					editPlan(planDto);
					tabSheet.setSelectedTab(mainLayout);
				}
			}
		});
		
		Button deleteButton = new Button("Delete");
		deleteButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				LessonPlanDto planDto = (LessonPlanDto) planTable.getValue();
				try {
					if (planDto!=null) {
						LessonPlan lessonPlan = LessonPlanLocalServiceUtil.fetchLessonPlan(planDto.getId());
						List<LessonAssessment> assessments = LessonAssessmentLocalServiceUtil.findByLessonPlan(lessonPlan.getLessonPlanId());
						if (assessments!=null) {
							for (LessonAssessment lessonAssessment : assessments) {
								LessonAssessmentLocalServiceUtil.deleteLessonAssessment(lessonAssessment);
							}
						}
						List<LessonTopic> lessonTopics = LessonTopicLocalServiceUtil.findByLessonPlan(lessonPlan.getLessonPlanId());
						if (lessonTopic!=null) {
							for (LessonTopic lessonTopic : lessonTopics) {
								LessonTopicLocalServiceUtil.deleteLessonTopic(lessonTopic);
							}
						}
						LessonPlanLocalServiceUtil.deleteLessonPlan(lessonPlan);
						loadLessonPlan();
						reset();
						window.showNotification("Lesson plan deleted successfully");
					}else{
						window.showNotification("Please select a row");
					}
				} catch (SystemException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		Button printButton = new Button("Print");
		printButton.addListener(new Listener() {
			
			@Override
			public void componentEvent(Event event) {
				LessonPlanDto planDto = (LessonPlanDto) planTable.getValue();
				if (planDto!=null) {
					loadReport(planDto.getId());
				}else{
					window.showNotification("Please select a lesson plan", Window.Notification.TYPE_ERROR_MESSAGE);
				}
			}
		});
		
		HorizontalLayout rowLayout = new HorizontalLayout();
		rowLayout.setWidth("100%");
		rowLayout.setSpacing(true);
		Label spacer = new Label();
		rowLayout.addComponent(spacer);
		rowLayout.addComponent(editButton);
		rowLayout.addComponent(deleteButton);
		rowLayout.addComponent(printButton);
		rowLayout.setExpandRatio(spacer, 1);
		
		listLayout.addComponent(planTable, 0, 0, 7, 0);
		listLayout.addComponent(rowLayout, 0, 1, 7, 1);
		
		return listLayout;
	}
	
	private void loadLessonPlan(){
		planTable.removeAllItems();
		try {
			List<LessonPlan> lessonPlanList = LessonPlanLocalServiceUtil.findByOrganizationUser(themeDisplay.getUser().getUserId());
			if (lessonPlanList!=null) {
				for (LessonPlan lessonPlan : lessonPlanList) {
					LessonPlanDto planDto = new LessonPlanDto();

					planDto.setId(lessonPlan.getLessonPlanId());
					planDto.setName(lessonPlan.getTitle());
					
					Subject subject = SubjectLocalServiceUtil.getSubject(lessonPlan.getSubject());
					SubjectDto subjectDto = new SubjectDto();
					subjectDto.setId(subject.getSubjectId());
					subjectDto.setName(subject.getSubjectName());
					planDto.setSubject(subjectDto);
					
					planDto.setPlanDate(lessonPlan.getPlanDate());
					planDto.setTotalClass(lessonPlan.getTotalClass());
					planDto.setDuration(lessonPlan.getTotalDuration());
					
					planContainer.addBean(planDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}
	
	private void editPlan(LessonPlanDto lessonPlanDto){
		try {
			lessonPlan = LessonPlanLocalServiceUtil.fetchLessonPlan(lessonPlanDto.getId());
			lessonTitleField.setValue(lessonPlan.getTitle());
			if (userOrganizationList.size()>1) {
				campusComboBox.setValue(getCampus(lessonPlan.getOrganization()));
			}
			subjectComboBox.setValue(getSubject(lessonPlan.getSubject()));
			planDate.setValue(lessonPlan.getPlanDate());
			
			totalClassField.setValue(lessonPlan.getTotalClass());
			durationField.setValue(lessonPlan.getTotalDuration());
			
			lessonAssessmentList = LessonAssessmentLocalServiceUtil.findByLessonPlan(lessonPlan.getLessonPlanId());
			loadAssessmentType();
			
			List<LessonTopic> lessonTopicList = LessonTopicLocalServiceUtil.findByLessonPlan(lessonPlan.getLessonPlanId());
			if (lessonTopicList!=null) {
				topicId = new HashSet<Long>();
				for (LessonTopic lessonTopic : lessonTopicList) {
					topicId.add(lessonTopic.getTopic());
				}
			}
			container();
			objectiveArea.setValue(lessonPlan.getObjective());
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private CampusDto getCampus(long campusId){
		if (campusList!=null) {
			for (CampusDto campus : campusList) {
				if (campus.getId()==campusId) {
					return campus;
				}
			}
		}
		return null;
	}
	
	private SubjectDto getSubject(long subjectId){
		if (subjectList!=null) {
			for (SubjectDto subject : subjectList) {
				if (subject.getId()==subjectId) {
					return subject;
				}
			}
		}
		return null;
	}
	
	private Window popWindow;
	private JasperPrint jasperPrint;
	SimpleDateFormat dateformat = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
	private final String timeStamp = dateformat.format(new Date());
	
	private void loadReport(long planId){
		popWindow = new Window("Report");
		popWindow.setWidth("60%");
		popWindow.setHeight("800px");
		popWindow.setPositionX(300);
		popWindow.setPositionY(300);
		
		final Map imagelocation = new HashMap();
		imagelocation.put("lessonPlanId", planId);
		
//		System.out.println("SSSSSSS------"+planId);
		
		StreamSource s = new StreamSource() {
			
			@Override
			public InputStream getStream() {
				try {
//					System.out.println("Before jasperprint");
					jasperPrint = JasperFillManager.fillReport(PropsUtil.get("dl.hook.file.system.root.dir.report")+"/"+"LessonPlan.jasper", imagelocation, connection);
					
//					System.err.println("before pdf export: "+PropsUtil.get("dl.hook.file.system.root.dir.pdf")+"/"+themeDisplay.getUserId()+"_"+timeStamp+".pdf");
					JasperExportManager.exportReportToPdfFile(jasperPrint, PropsUtil.get("dl.hook.file.system.root.dir.pdf")+"/"+themeDisplay.getUserId()+"_"+timeStamp+".pdf");
//					System.err.println("after pdf export");
				} catch (JRException e) {
					e.printStackTrace();
				}
				
				File file = new File(PropsUtil.get("dl.hook.file.system.root.dir.pdf")+"/"+themeDisplay.getUserId()+"_"+timeStamp+ ".pdf");
				FileInputStream fis = null;
				try {
					fis = new FileInputStream(file);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				return fis;
			}
		};
		
		StreamResource sr = new StreamResource(s, "repy.pdf", this);
		Embedded e = new Embedded();
		e.setSizeFull();
		e.setHeight("700px");
		e.setType(Embedded.TYPE_BROWSER);
		sr.setMIMEType("application/pdf");
		e.setSource(sr);
		
		popWindow.addComponent(e);
		window.addWindow(popWindow);
		
		popWindow.addListener(new CloseListener() {
			
			@Override
			public void windowClose(CloseEvent e) {
				File file = new File(PropsUtil.get("dl.hook.file.system.root.dir.pdf")+"/"+themeDisplay.getUserId()+"_"+timeStamp+ ".pdf");
				file.delete();
			}
		});
	}
	
	private void reset(){
		lessonPlan = null;
		assessment = null;
		lessonTopic = null;
		
		lessonTitleField.setValue("");
		campusComboBox.setValue(null);
		subjectComboBox.setValue(null);
		planDate.setValue(new Date());
		
		totalClassField.setValue(null);
		durationField.setValue(null);
		objectiveArea.setValue("");
		lessonAssessmentList = null;
		loadAssessmentType();
		assessmentTypeTable.refreshRowCache();
		contentTable.refreshRowCache();
	}
}
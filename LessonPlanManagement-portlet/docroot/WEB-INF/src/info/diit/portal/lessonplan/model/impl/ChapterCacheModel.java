/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.lessonplan.model.Chapter;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Chapter in entity cache.
 *
 * @author limon
 * @see Chapter
 * @generated
 */
public class ChapterCacheModel implements CacheModel<Chapter>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{chapterId=");
		sb.append(chapterId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", sequence=");
		sb.append(sequence);
		sb.append(", name=");
		sb.append(name);
		sb.append(", subjectId=");
		sb.append(subjectId);
		sb.append(", note=");
		sb.append(note);
		sb.append("}");

		return sb.toString();
	}

	public Chapter toEntityModel() {
		ChapterImpl chapterImpl = new ChapterImpl();

		chapterImpl.setChapterId(chapterId);
		chapterImpl.setCompanyId(companyId);
		chapterImpl.setOrganizationId(organizationId);
		chapterImpl.setUserId(userId);

		if (userName == null) {
			chapterImpl.setUserName(StringPool.BLANK);
		}
		else {
			chapterImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			chapterImpl.setCreateDate(null);
		}
		else {
			chapterImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			chapterImpl.setModifiedDate(null);
		}
		else {
			chapterImpl.setModifiedDate(new Date(modifiedDate));
		}

		chapterImpl.setSequence(sequence);

		if (name == null) {
			chapterImpl.setName(StringPool.BLANK);
		}
		else {
			chapterImpl.setName(name);
		}

		chapterImpl.setSubjectId(subjectId);

		if (note == null) {
			chapterImpl.setNote(StringPool.BLANK);
		}
		else {
			chapterImpl.setNote(note);
		}

		chapterImpl.resetOriginalValues();

		return chapterImpl;
	}

	public long chapterId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long sequence;
	public String name;
	public long subjectId;
	public String note;
}
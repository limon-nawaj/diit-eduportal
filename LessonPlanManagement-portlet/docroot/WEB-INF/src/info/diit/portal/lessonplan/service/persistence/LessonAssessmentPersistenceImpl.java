/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.lessonplan.NoSuchLessonAssessmentException;
import info.diit.portal.lessonplan.model.LessonAssessment;
import info.diit.portal.lessonplan.model.impl.LessonAssessmentImpl;
import info.diit.portal.lessonplan.model.impl.LessonAssessmentModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the lesson assessment service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see LessonAssessmentPersistence
 * @see LessonAssessmentUtil
 * @generated
 */
public class LessonAssessmentPersistenceImpl extends BasePersistenceImpl<LessonAssessment>
	implements LessonAssessmentPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LessonAssessmentUtil} to access the lesson assessment persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LessonAssessmentImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LESSONPLAN =
		new FinderPath(LessonAssessmentModelImpl.ENTITY_CACHE_ENABLED,
			LessonAssessmentModelImpl.FINDER_CACHE_ENABLED,
			LessonAssessmentImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByLessonPlan",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONPLAN =
		new FinderPath(LessonAssessmentModelImpl.ENTITY_CACHE_ENABLED,
			LessonAssessmentModelImpl.FINDER_CACHE_ENABLED,
			LessonAssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLessonPlan",
			new String[] { Long.class.getName() },
			LessonAssessmentModelImpl.LESSONPLANID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LESSONPLAN = new FinderPath(LessonAssessmentModelImpl.ENTITY_CACHE_ENABLED,
			LessonAssessmentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLessonPlan",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LessonAssessmentModelImpl.ENTITY_CACHE_ENABLED,
			LessonAssessmentModelImpl.FINDER_CACHE_ENABLED,
			LessonAssessmentImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LessonAssessmentModelImpl.ENTITY_CACHE_ENABLED,
			LessonAssessmentModelImpl.FINDER_CACHE_ENABLED,
			LessonAssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LessonAssessmentModelImpl.ENTITY_CACHE_ENABLED,
			LessonAssessmentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the lesson assessment in the entity cache if it is enabled.
	 *
	 * @param lessonAssessment the lesson assessment
	 */
	public void cacheResult(LessonAssessment lessonAssessment) {
		EntityCacheUtil.putResult(LessonAssessmentModelImpl.ENTITY_CACHE_ENABLED,
			LessonAssessmentImpl.class, lessonAssessment.getPrimaryKey(),
			lessonAssessment);

		lessonAssessment.resetOriginalValues();
	}

	/**
	 * Caches the lesson assessments in the entity cache if it is enabled.
	 *
	 * @param lessonAssessments the lesson assessments
	 */
	public void cacheResult(List<LessonAssessment> lessonAssessments) {
		for (LessonAssessment lessonAssessment : lessonAssessments) {
			if (EntityCacheUtil.getResult(
						LessonAssessmentModelImpl.ENTITY_CACHE_ENABLED,
						LessonAssessmentImpl.class,
						lessonAssessment.getPrimaryKey()) == null) {
				cacheResult(lessonAssessment);
			}
			else {
				lessonAssessment.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all lesson assessments.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(LessonAssessmentImpl.class.getName());
		}

		EntityCacheUtil.clearCache(LessonAssessmentImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the lesson assessment.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LessonAssessment lessonAssessment) {
		EntityCacheUtil.removeResult(LessonAssessmentModelImpl.ENTITY_CACHE_ENABLED,
			LessonAssessmentImpl.class, lessonAssessment.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<LessonAssessment> lessonAssessments) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LessonAssessment lessonAssessment : lessonAssessments) {
			EntityCacheUtil.removeResult(LessonAssessmentModelImpl.ENTITY_CACHE_ENABLED,
				LessonAssessmentImpl.class, lessonAssessment.getPrimaryKey());
		}
	}

	/**
	 * Creates a new lesson assessment with the primary key. Does not add the lesson assessment to the database.
	 *
	 * @param lessonAssessmentId the primary key for the new lesson assessment
	 * @return the new lesson assessment
	 */
	public LessonAssessment create(long lessonAssessmentId) {
		LessonAssessment lessonAssessment = new LessonAssessmentImpl();

		lessonAssessment.setNew(true);
		lessonAssessment.setPrimaryKey(lessonAssessmentId);

		return lessonAssessment;
	}

	/**
	 * Removes the lesson assessment with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param lessonAssessmentId the primary key of the lesson assessment
	 * @return the lesson assessment that was removed
	 * @throws info.diit.portal.lessonplan.NoSuchLessonAssessmentException if a lesson assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonAssessment remove(long lessonAssessmentId)
		throws NoSuchLessonAssessmentException, SystemException {
		return remove(Long.valueOf(lessonAssessmentId));
	}

	/**
	 * Removes the lesson assessment with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the lesson assessment
	 * @return the lesson assessment that was removed
	 * @throws info.diit.portal.lessonplan.NoSuchLessonAssessmentException if a lesson assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LessonAssessment remove(Serializable primaryKey)
		throws NoSuchLessonAssessmentException, SystemException {
		Session session = null;

		try {
			session = openSession();

			LessonAssessment lessonAssessment = (LessonAssessment)session.get(LessonAssessmentImpl.class,
					primaryKey);

			if (lessonAssessment == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLessonAssessmentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(lessonAssessment);
		}
		catch (NoSuchLessonAssessmentException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LessonAssessment removeImpl(LessonAssessment lessonAssessment)
		throws SystemException {
		lessonAssessment = toUnwrappedModel(lessonAssessment);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, lessonAssessment);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(lessonAssessment);

		return lessonAssessment;
	}

	@Override
	public LessonAssessment updateImpl(
		info.diit.portal.lessonplan.model.LessonAssessment lessonAssessment,
		boolean merge) throws SystemException {
		lessonAssessment = toUnwrappedModel(lessonAssessment);

		boolean isNew = lessonAssessment.isNew();

		LessonAssessmentModelImpl lessonAssessmentModelImpl = (LessonAssessmentModelImpl)lessonAssessment;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, lessonAssessment, merge);

			lessonAssessment.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !LessonAssessmentModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((lessonAssessmentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONPLAN.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(lessonAssessmentModelImpl.getOriginalLessonPlanId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LESSONPLAN,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONPLAN,
					args);

				args = new Object[] {
						Long.valueOf(lessonAssessmentModelImpl.getLessonPlanId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LESSONPLAN,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONPLAN,
					args);
			}
		}

		EntityCacheUtil.putResult(LessonAssessmentModelImpl.ENTITY_CACHE_ENABLED,
			LessonAssessmentImpl.class, lessonAssessment.getPrimaryKey(),
			lessonAssessment);

		return lessonAssessment;
	}

	protected LessonAssessment toUnwrappedModel(
		LessonAssessment lessonAssessment) {
		if (lessonAssessment instanceof LessonAssessmentImpl) {
			return lessonAssessment;
		}

		LessonAssessmentImpl lessonAssessmentImpl = new LessonAssessmentImpl();

		lessonAssessmentImpl.setNew(lessonAssessment.isNew());
		lessonAssessmentImpl.setPrimaryKey(lessonAssessment.getPrimaryKey());

		lessonAssessmentImpl.setLessonAssessmentId(lessonAssessment.getLessonAssessmentId());
		lessonAssessmentImpl.setCompanyId(lessonAssessment.getCompanyId());
		lessonAssessmentImpl.setUserId(lessonAssessment.getUserId());
		lessonAssessmentImpl.setUserName(lessonAssessment.getUserName());
		lessonAssessmentImpl.setCreateDate(lessonAssessment.getCreateDate());
		lessonAssessmentImpl.setModifiedDate(lessonAssessment.getModifiedDate());
		lessonAssessmentImpl.setLessonPlanId(lessonAssessment.getLessonPlanId());
		lessonAssessmentImpl.setAssessmentType(lessonAssessment.getAssessmentType());
		lessonAssessmentImpl.setNumber(lessonAssessment.getNumber());

		return lessonAssessmentImpl;
	}

	/**
	 * Returns the lesson assessment with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the lesson assessment
	 * @return the lesson assessment
	 * @throws com.liferay.portal.NoSuchModelException if a lesson assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LessonAssessment findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the lesson assessment with the primary key or throws a {@link info.diit.portal.lessonplan.NoSuchLessonAssessmentException} if it could not be found.
	 *
	 * @param lessonAssessmentId the primary key of the lesson assessment
	 * @return the lesson assessment
	 * @throws info.diit.portal.lessonplan.NoSuchLessonAssessmentException if a lesson assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonAssessment findByPrimaryKey(long lessonAssessmentId)
		throws NoSuchLessonAssessmentException, SystemException {
		LessonAssessment lessonAssessment = fetchByPrimaryKey(lessonAssessmentId);

		if (lessonAssessment == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					lessonAssessmentId);
			}

			throw new NoSuchLessonAssessmentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				lessonAssessmentId);
		}

		return lessonAssessment;
	}

	/**
	 * Returns the lesson assessment with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the lesson assessment
	 * @return the lesson assessment, or <code>null</code> if a lesson assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LessonAssessment fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the lesson assessment with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param lessonAssessmentId the primary key of the lesson assessment
	 * @return the lesson assessment, or <code>null</code> if a lesson assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonAssessment fetchByPrimaryKey(long lessonAssessmentId)
		throws SystemException {
		LessonAssessment lessonAssessment = (LessonAssessment)EntityCacheUtil.getResult(LessonAssessmentModelImpl.ENTITY_CACHE_ENABLED,
				LessonAssessmentImpl.class, lessonAssessmentId);

		if (lessonAssessment == _nullLessonAssessment) {
			return null;
		}

		if (lessonAssessment == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				lessonAssessment = (LessonAssessment)session.get(LessonAssessmentImpl.class,
						Long.valueOf(lessonAssessmentId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (lessonAssessment != null) {
					cacheResult(lessonAssessment);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(LessonAssessmentModelImpl.ENTITY_CACHE_ENABLED,
						LessonAssessmentImpl.class, lessonAssessmentId,
						_nullLessonAssessment);
				}

				closeSession(session);
			}
		}

		return lessonAssessment;
	}

	/**
	 * Returns all the lesson assessments where lessonPlanId = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @return the matching lesson assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonAssessment> findByLessonPlan(long lessonPlanId)
		throws SystemException {
		return findByLessonPlan(lessonPlanId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lesson assessments where lessonPlanId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param start the lower bound of the range of lesson assessments
	 * @param end the upper bound of the range of lesson assessments (not inclusive)
	 * @return the range of matching lesson assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonAssessment> findByLessonPlan(long lessonPlanId,
		int start, int end) throws SystemException {
		return findByLessonPlan(lessonPlanId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the lesson assessments where lessonPlanId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param start the lower bound of the range of lesson assessments
	 * @param end the upper bound of the range of lesson assessments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching lesson assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonAssessment> findByLessonPlan(long lessonPlanId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONPLAN;
			finderArgs = new Object[] { lessonPlanId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LESSONPLAN;
			finderArgs = new Object[] {
					lessonPlanId,
					
					start, end, orderByComparator
				};
		}

		List<LessonAssessment> list = (List<LessonAssessment>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (LessonAssessment lessonAssessment : list) {
				if ((lessonPlanId != lessonAssessment.getLessonPlanId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_LESSONASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_LESSONPLAN_LESSONPLANID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(lessonPlanId);

				list = (List<LessonAssessment>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first lesson assessment in the ordered set where lessonPlanId = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lesson assessment
	 * @throws info.diit.portal.lessonplan.NoSuchLessonAssessmentException if a matching lesson assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonAssessment findByLessonPlan_First(long lessonPlanId,
		OrderByComparator orderByComparator)
		throws NoSuchLessonAssessmentException, SystemException {
		LessonAssessment lessonAssessment = fetchByLessonPlan_First(lessonPlanId,
				orderByComparator);

		if (lessonAssessment != null) {
			return lessonAssessment;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("lessonPlanId=");
		msg.append(lessonPlanId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLessonAssessmentException(msg.toString());
	}

	/**
	 * Returns the first lesson assessment in the ordered set where lessonPlanId = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lesson assessment, or <code>null</code> if a matching lesson assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonAssessment fetchByLessonPlan_First(long lessonPlanId,
		OrderByComparator orderByComparator) throws SystemException {
		List<LessonAssessment> list = findByLessonPlan(lessonPlanId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last lesson assessment in the ordered set where lessonPlanId = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lesson assessment
	 * @throws info.diit.portal.lessonplan.NoSuchLessonAssessmentException if a matching lesson assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonAssessment findByLessonPlan_Last(long lessonPlanId,
		OrderByComparator orderByComparator)
		throws NoSuchLessonAssessmentException, SystemException {
		LessonAssessment lessonAssessment = fetchByLessonPlan_Last(lessonPlanId,
				orderByComparator);

		if (lessonAssessment != null) {
			return lessonAssessment;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("lessonPlanId=");
		msg.append(lessonPlanId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLessonAssessmentException(msg.toString());
	}

	/**
	 * Returns the last lesson assessment in the ordered set where lessonPlanId = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lesson assessment, or <code>null</code> if a matching lesson assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonAssessment fetchByLessonPlan_Last(long lessonPlanId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLessonPlan(lessonPlanId);

		List<LessonAssessment> list = findByLessonPlan(lessonPlanId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the lesson assessments before and after the current lesson assessment in the ordered set where lessonPlanId = &#63;.
	 *
	 * @param lessonAssessmentId the primary key of the current lesson assessment
	 * @param lessonPlanId the lesson plan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next lesson assessment
	 * @throws info.diit.portal.lessonplan.NoSuchLessonAssessmentException if a lesson assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonAssessment[] findByLessonPlan_PrevAndNext(
		long lessonAssessmentId, long lessonPlanId,
		OrderByComparator orderByComparator)
		throws NoSuchLessonAssessmentException, SystemException {
		LessonAssessment lessonAssessment = findByPrimaryKey(lessonAssessmentId);

		Session session = null;

		try {
			session = openSession();

			LessonAssessment[] array = new LessonAssessmentImpl[3];

			array[0] = getByLessonPlan_PrevAndNext(session, lessonAssessment,
					lessonPlanId, orderByComparator, true);

			array[1] = lessonAssessment;

			array[2] = getByLessonPlan_PrevAndNext(session, lessonAssessment,
					lessonPlanId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LessonAssessment getByLessonPlan_PrevAndNext(Session session,
		LessonAssessment lessonAssessment, long lessonPlanId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LESSONASSESSMENT_WHERE);

		query.append(_FINDER_COLUMN_LESSONPLAN_LESSONPLANID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(lessonPlanId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(lessonAssessment);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LessonAssessment> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the lesson assessments.
	 *
	 * @return the lesson assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonAssessment> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lesson assessments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of lesson assessments
	 * @param end the upper bound of the range of lesson assessments (not inclusive)
	 * @return the range of lesson assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonAssessment> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the lesson assessments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of lesson assessments
	 * @param end the upper bound of the range of lesson assessments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of lesson assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonAssessment> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LessonAssessment> list = (List<LessonAssessment>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LESSONASSESSMENT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LESSONASSESSMENT;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<LessonAssessment>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<LessonAssessment>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the lesson assessments where lessonPlanId = &#63; from the database.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByLessonPlan(long lessonPlanId) throws SystemException {
		for (LessonAssessment lessonAssessment : findByLessonPlan(lessonPlanId)) {
			remove(lessonAssessment);
		}
	}

	/**
	 * Removes all the lesson assessments from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (LessonAssessment lessonAssessment : findAll()) {
			remove(lessonAssessment);
		}
	}

	/**
	 * Returns the number of lesson assessments where lessonPlanId = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @return the number of matching lesson assessments
	 * @throws SystemException if a system exception occurred
	 */
	public int countByLessonPlan(long lessonPlanId) throws SystemException {
		Object[] finderArgs = new Object[] { lessonPlanId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_LESSONPLAN,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LESSONASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_LESSONPLAN_LESSONPLANID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(lessonPlanId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LESSONPLAN,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of lesson assessments.
	 *
	 * @return the number of lesson assessments
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LESSONASSESSMENT);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the lesson assessment persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.lessonplan.model.LessonAssessment")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<LessonAssessment>> listenersList = new ArrayList<ModelListener<LessonAssessment>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<LessonAssessment>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(LessonAssessmentImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_LESSONASSESSMENT = "SELECT lessonAssessment FROM LessonAssessment lessonAssessment";
	private static final String _SQL_SELECT_LESSONASSESSMENT_WHERE = "SELECT lessonAssessment FROM LessonAssessment lessonAssessment WHERE ";
	private static final String _SQL_COUNT_LESSONASSESSMENT = "SELECT COUNT(lessonAssessment) FROM LessonAssessment lessonAssessment";
	private static final String _SQL_COUNT_LESSONASSESSMENT_WHERE = "SELECT COUNT(lessonAssessment) FROM LessonAssessment lessonAssessment WHERE ";
	private static final String _FINDER_COLUMN_LESSONPLAN_LESSONPLANID_2 = "lessonAssessment.lessonPlanId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "lessonAssessment.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LessonAssessment exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No LessonAssessment exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(LessonAssessmentPersistenceImpl.class);
	private static LessonAssessment _nullLessonAssessment = new LessonAssessmentImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<LessonAssessment> toCacheModel() {
				return _nullLessonAssessmentCacheModel;
			}
		};

	private static CacheModel<LessonAssessment> _nullLessonAssessmentCacheModel = new CacheModel<LessonAssessment>() {
			public LessonAssessment toEntityModel() {
				return _nullLessonAssessment;
			}
		};
}
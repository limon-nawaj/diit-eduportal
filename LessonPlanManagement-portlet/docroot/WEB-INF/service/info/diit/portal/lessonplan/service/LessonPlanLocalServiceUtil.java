/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the lesson plan local service. This utility wraps {@link info.diit.portal.lessonplan.service.impl.LessonPlanLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see LessonPlanLocalService
 * @see info.diit.portal.lessonplan.service.base.LessonPlanLocalServiceBaseImpl
 * @see info.diit.portal.lessonplan.service.impl.LessonPlanLocalServiceImpl
 * @generated
 */
public class LessonPlanLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.lessonplan.service.impl.LessonPlanLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the lesson plan to the database. Also notifies the appropriate model listeners.
	*
	* @param lessonPlan the lesson plan
	* @return the lesson plan that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.LessonPlan addLessonPlan(
		info.diit.portal.lessonplan.model.LessonPlan lessonPlan)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addLessonPlan(lessonPlan);
	}

	/**
	* Creates a new lesson plan with the primary key. Does not add the lesson plan to the database.
	*
	* @param lessonPlanId the primary key for the new lesson plan
	* @return the new lesson plan
	*/
	public static info.diit.portal.lessonplan.model.LessonPlan createLessonPlan(
		long lessonPlanId) {
		return getService().createLessonPlan(lessonPlanId);
	}

	/**
	* Deletes the lesson plan with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonPlanId the primary key of the lesson plan
	* @return the lesson plan that was removed
	* @throws PortalException if a lesson plan with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.LessonPlan deleteLessonPlan(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteLessonPlan(lessonPlanId);
	}

	/**
	* Deletes the lesson plan from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonPlan the lesson plan
	* @return the lesson plan that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.LessonPlan deleteLessonPlan(
		info.diit.portal.lessonplan.model.LessonPlan lessonPlan)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteLessonPlan(lessonPlan);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.lessonplan.model.LessonPlan fetchLessonPlan(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchLessonPlan(lessonPlanId);
	}

	/**
	* Returns the lesson plan with the primary key.
	*
	* @param lessonPlanId the primary key of the lesson plan
	* @return the lesson plan
	* @throws PortalException if a lesson plan with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.LessonPlan getLessonPlan(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getLessonPlan(lessonPlanId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the lesson plans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson plans
	* @param end the upper bound of the range of lesson plans (not inclusive)
	* @return the range of lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.lessonplan.model.LessonPlan> getLessonPlans(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLessonPlans(start, end);
	}

	/**
	* Returns the number of lesson plans.
	*
	* @return the number of lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public static int getLessonPlansCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLessonPlansCount();
	}

	/**
	* Updates the lesson plan in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lessonPlan the lesson plan
	* @return the lesson plan that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.LessonPlan updateLessonPlan(
		info.diit.portal.lessonplan.model.LessonPlan lessonPlan)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateLessonPlan(lessonPlan);
	}

	/**
	* Updates the lesson plan in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lessonPlan the lesson plan
	* @param merge whether to merge the lesson plan with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the lesson plan that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.LessonPlan updateLessonPlan(
		info.diit.portal.lessonplan.model.LessonPlan lessonPlan, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateLessonPlan(lessonPlan, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<info.diit.portal.lessonplan.model.LessonPlan> findByOrganizationUser(
		long user) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByOrganizationUser(user);
	}

	public static java.util.List<info.diit.portal.lessonplan.model.LessonPlan> findBySubject(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findBySubject(subjectId);
	}

	public static void clearService() {
		_service = null;
	}

	public static LessonPlanLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					LessonPlanLocalService.class.getName());

			if (invokableLocalService instanceof LessonPlanLocalService) {
				_service = (LessonPlanLocalService)invokableLocalService;
			}
			else {
				_service = new LessonPlanLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(LessonPlanLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(LessonPlanLocalService service) {
	}

	private static LessonPlanLocalService _service;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import info.diit.portal.lessonplan.model.ChapterClp;
import info.diit.portal.lessonplan.model.LessonAssessmentClp;
import info.diit.portal.lessonplan.model.LessonPlanClp;
import info.diit.portal.lessonplan.model.LessonTopicClp;
import info.diit.portal.lessonplan.model.SubjectLessonClp;
import info.diit.portal.lessonplan.model.TopicClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Brian Wing Shun Chan
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"LessonPlanManagement-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"LessonPlanManagement-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "LessonPlanManagement-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(ChapterClp.class.getName())) {
			return translateInputChapter(oldModel);
		}

		if (oldModelClassName.equals(LessonAssessmentClp.class.getName())) {
			return translateInputLessonAssessment(oldModel);
		}

		if (oldModelClassName.equals(LessonPlanClp.class.getName())) {
			return translateInputLessonPlan(oldModel);
		}

		if (oldModelClassName.equals(LessonTopicClp.class.getName())) {
			return translateInputLessonTopic(oldModel);
		}

		if (oldModelClassName.equals(SubjectLessonClp.class.getName())) {
			return translateInputSubjectLesson(oldModel);
		}

		if (oldModelClassName.equals(TopicClp.class.getName())) {
			return translateInputTopic(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputChapter(BaseModel<?> oldModel) {
		ChapterClp oldClpModel = (ChapterClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getChapterRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLessonAssessment(BaseModel<?> oldModel) {
		LessonAssessmentClp oldClpModel = (LessonAssessmentClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLessonAssessmentRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLessonPlan(BaseModel<?> oldModel) {
		LessonPlanClp oldClpModel = (LessonPlanClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLessonPlanRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLessonTopic(BaseModel<?> oldModel) {
		LessonTopicClp oldClpModel = (LessonTopicClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLessonTopicRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputSubjectLesson(BaseModel<?> oldModel) {
		SubjectLessonClp oldClpModel = (SubjectLessonClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getSubjectLessonRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputTopic(BaseModel<?> oldModel) {
		TopicClp oldClpModel = (TopicClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getTopicRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(
					"info.diit.portal.lessonplan.model.impl.ChapterImpl")) {
			return translateOutputChapter(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.lessonplan.model.impl.LessonAssessmentImpl")) {
			return translateOutputLessonAssessment(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.lessonplan.model.impl.LessonPlanImpl")) {
			return translateOutputLessonPlan(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.lessonplan.model.impl.LessonTopicImpl")) {
			return translateOutputLessonTopic(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.lessonplan.model.impl.SubjectLessonImpl")) {
			return translateOutputSubjectLesson(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.lessonplan.model.impl.TopicImpl")) {
			return translateOutputTopic(oldModel);
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals(
					"info.diit.portal.lessonplan.NoSuchChapterException")) {
			return new info.diit.portal.lessonplan.NoSuchChapterException();
		}

		if (className.equals(
					"info.diit.portal.lessonplan.NoSuchLessonAssessmentException")) {
			return new info.diit.portal.lessonplan.NoSuchLessonAssessmentException();
		}

		if (className.equals(
					"info.diit.portal.lessonplan.NoSuchLessonPlanException")) {
			return new info.diit.portal.lessonplan.NoSuchLessonPlanException();
		}

		if (className.equals(
					"info.diit.portal.lessonplan.NoSuchLessonTopicException")) {
			return new info.diit.portal.lessonplan.NoSuchLessonTopicException();
		}

		if (className.equals(
					"info.diit.portal.lessonplan.NoSuchSubjectLessonException")) {
			return new info.diit.portal.lessonplan.NoSuchSubjectLessonException();
		}

		if (className.equals("info.diit.portal.lessonplan.NoSuchTopicException")) {
			return new info.diit.portal.lessonplan.NoSuchTopicException();
		}

		return throwable;
	}

	public static Object translateOutputChapter(BaseModel<?> oldModel) {
		ChapterClp newModel = new ChapterClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setChapterRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLessonAssessment(BaseModel<?> oldModel) {
		LessonAssessmentClp newModel = new LessonAssessmentClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLessonAssessmentRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLessonPlan(BaseModel<?> oldModel) {
		LessonPlanClp newModel = new LessonPlanClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLessonPlanRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLessonTopic(BaseModel<?> oldModel) {
		LessonTopicClp newModel = new LessonTopicClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLessonTopicRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputSubjectLesson(BaseModel<?> oldModel) {
		SubjectLessonClp newModel = new SubjectLessonClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setSubjectLessonRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputTopic(BaseModel<?> oldModel) {
		TopicClp newModel = new TopicClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setTopicRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}
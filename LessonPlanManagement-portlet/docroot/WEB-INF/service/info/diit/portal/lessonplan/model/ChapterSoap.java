/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    limon
 * @generated
 */
public class ChapterSoap implements Serializable {
	public static ChapterSoap toSoapModel(Chapter model) {
		ChapterSoap soapModel = new ChapterSoap();

		soapModel.setChapterId(model.getChapterId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setSequence(model.getSequence());
		soapModel.setName(model.getName());
		soapModel.setSubjectId(model.getSubjectId());
		soapModel.setNote(model.getNote());

		return soapModel;
	}

	public static ChapterSoap[] toSoapModels(Chapter[] models) {
		ChapterSoap[] soapModels = new ChapterSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ChapterSoap[][] toSoapModels(Chapter[][] models) {
		ChapterSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ChapterSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ChapterSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ChapterSoap[] toSoapModels(List<Chapter> models) {
		List<ChapterSoap> soapModels = new ArrayList<ChapterSoap>(models.size());

		for (Chapter model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ChapterSoap[soapModels.size()]);
	}

	public ChapterSoap() {
	}

	public long getPrimaryKey() {
		return _chapterId;
	}

	public void setPrimaryKey(long pk) {
		setChapterId(pk);
	}

	public long getChapterId() {
		return _chapterId;
	}

	public void setChapterId(long chapterId) {
		_chapterId = chapterId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getSequence() {
		return _sequence;
	}

	public void setSequence(long sequence) {
		_sequence = sequence;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public long getSubjectId() {
		return _subjectId;
	}

	public void setSubjectId(long subjectId) {
		_subjectId = subjectId;
	}

	public String getNote() {
		return _note;
	}

	public void setNote(String note) {
		_note = note;
	}

	private long _chapterId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _sequence;
	private String _name;
	private long _subjectId;
	private String _note;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    limon
 * @generated
 */
public class LessonPlanSoap implements Serializable {
	public static LessonPlanSoap toSoapModel(LessonPlan model) {
		LessonPlanSoap soapModel = new LessonPlanSoap();

		soapModel.setLessonPlanId(model.getLessonPlanId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setTitle(model.getTitle());
		soapModel.setOrganization(model.getOrganization());
		soapModel.setSubject(model.getSubject());
		soapModel.setPlanDate(model.getPlanDate());
		soapModel.setTotalClass(model.getTotalClass());
		soapModel.setTotalDuration(model.getTotalDuration());
		soapModel.setObjective(model.getObjective());

		return soapModel;
	}

	public static LessonPlanSoap[] toSoapModels(LessonPlan[] models) {
		LessonPlanSoap[] soapModels = new LessonPlanSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LessonPlanSoap[][] toSoapModels(LessonPlan[][] models) {
		LessonPlanSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LessonPlanSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LessonPlanSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LessonPlanSoap[] toSoapModels(List<LessonPlan> models) {
		List<LessonPlanSoap> soapModels = new ArrayList<LessonPlanSoap>(models.size());

		for (LessonPlan model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LessonPlanSoap[soapModels.size()]);
	}

	public LessonPlanSoap() {
	}

	public long getPrimaryKey() {
		return _lessonPlanId;
	}

	public void setPrimaryKey(long pk) {
		setLessonPlanId(pk);
	}

	public long getLessonPlanId() {
		return _lessonPlanId;
	}

	public void setLessonPlanId(long lessonPlanId) {
		_lessonPlanId = lessonPlanId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public long getOrganization() {
		return _organization;
	}

	public void setOrganization(long organization) {
		_organization = organization;
	}

	public long getSubject() {
		return _subject;
	}

	public void setSubject(long subject) {
		_subject = subject;
	}

	public Date getPlanDate() {
		return _planDate;
	}

	public void setPlanDate(Date planDate) {
		_planDate = planDate;
	}

	public long getTotalClass() {
		return _totalClass;
	}

	public void setTotalClass(long totalClass) {
		_totalClass = totalClass;
	}

	public long getTotalDuration() {
		return _totalDuration;
	}

	public void setTotalDuration(long totalDuration) {
		_totalDuration = totalDuration;
	}

	public String getObjective() {
		return _objective;
	}

	public void setObjective(String objective) {
		_objective = objective;
	}

	private long _lessonPlanId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _title;
	private long _organization;
	private long _subject;
	private Date _planDate;
	private long _totalClass;
	private long _totalDuration;
	private String _objective;
}
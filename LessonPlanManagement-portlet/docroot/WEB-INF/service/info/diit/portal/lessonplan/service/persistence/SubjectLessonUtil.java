/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.lessonplan.model.SubjectLesson;

import java.util.List;

/**
 * The persistence utility for the subject lesson service. This utility wraps {@link SubjectLessonPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see SubjectLessonPersistence
 * @see SubjectLessonPersistenceImpl
 * @generated
 */
public class SubjectLessonUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(SubjectLesson subjectLesson) {
		getPersistence().clearCache(subjectLesson);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<SubjectLesson> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<SubjectLesson> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<SubjectLesson> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static SubjectLesson update(SubjectLesson subjectLesson,
		boolean merge) throws SystemException {
		return getPersistence().update(subjectLesson, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static SubjectLesson update(SubjectLesson subjectLesson,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(subjectLesson, merge, serviceContext);
	}

	/**
	* Caches the subject lesson in the entity cache if it is enabled.
	*
	* @param subjectLesson the subject lesson
	*/
	public static void cacheResult(
		info.diit.portal.lessonplan.model.SubjectLesson subjectLesson) {
		getPersistence().cacheResult(subjectLesson);
	}

	/**
	* Caches the subject lessons in the entity cache if it is enabled.
	*
	* @param subjectLessons the subject lessons
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> subjectLessons) {
		getPersistence().cacheResult(subjectLessons);
	}

	/**
	* Creates a new subject lesson with the primary key. Does not add the subject lesson to the database.
	*
	* @param SubjectLessonId the primary key for the new subject lesson
	* @return the new subject lesson
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson create(
		long SubjectLessonId) {
		return getPersistence().create(SubjectLessonId);
	}

	/**
	* Removes the subject lesson with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param SubjectLessonId the primary key of the subject lesson
	* @return the subject lesson that was removed
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a subject lesson with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson remove(
		long SubjectLessonId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException {
		return getPersistence().remove(SubjectLessonId);
	}

	public static info.diit.portal.lessonplan.model.SubjectLesson updateImpl(
		info.diit.portal.lessonplan.model.SubjectLesson subjectLesson,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(subjectLesson, merge);
	}

	/**
	* Returns the subject lesson with the primary key or throws a {@link info.diit.portal.lessonplan.NoSuchSubjectLessonException} if it could not be found.
	*
	* @param SubjectLessonId the primary key of the subject lesson
	* @return the subject lesson
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a subject lesson with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson findByPrimaryKey(
		long SubjectLessonId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException {
		return getPersistence().findByPrimaryKey(SubjectLessonId);
	}

	/**
	* Returns the subject lesson with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param SubjectLessonId the primary key of the subject lesson
	* @return the subject lesson, or <code>null</code> if a subject lesson with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson fetchByPrimaryKey(
		long SubjectLessonId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(SubjectLessonId);
	}

	/**
	* Returns all the subject lessons where organizationId = &#63; and userId = &#63;.
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @return the matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findByUserOrganization(
		long organizationId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserOrganization(organizationId, userId);
	}

	/**
	* Returns a range of all the subject lessons where organizationId = &#63; and userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @param start the lower bound of the range of subject lessons
	* @param end the upper bound of the range of subject lessons (not inclusive)
	* @return the range of matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findByUserOrganization(
		long organizationId, long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserOrganization(organizationId, userId, start, end);
	}

	/**
	* Returns an ordered range of all the subject lessons where organizationId = &#63; and userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @param start the lower bound of the range of subject lessons
	* @param end the upper bound of the range of subject lessons (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findByUserOrganization(
		long organizationId, long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserOrganization(organizationId, userId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject lesson
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson findByUserOrganization_First(
		long organizationId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException {
		return getPersistence()
				   .findByUserOrganization_First(organizationId, userId,
			orderByComparator);
	}

	/**
	* Returns the first subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject lesson, or <code>null</code> if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson fetchByUserOrganization_First(
		long organizationId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUserOrganization_First(organizationId, userId,
			orderByComparator);
	}

	/**
	* Returns the last subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject lesson
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson findByUserOrganization_Last(
		long organizationId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException {
		return getPersistence()
				   .findByUserOrganization_Last(organizationId, userId,
			orderByComparator);
	}

	/**
	* Returns the last subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject lesson, or <code>null</code> if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson fetchByUserOrganization_Last(
		long organizationId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUserOrganization_Last(organizationId, userId,
			orderByComparator);
	}

	/**
	* Returns the subject lessons before and after the current subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	*
	* @param SubjectLessonId the primary key of the current subject lesson
	* @param organizationId the organization ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next subject lesson
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a subject lesson with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson[] findByUserOrganization_PrevAndNext(
		long SubjectLessonId, long organizationId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException {
		return getPersistence()
				   .findByUserOrganization_PrevAndNext(SubjectLessonId,
			organizationId, userId, orderByComparator);
	}

	/**
	* Returns all the subject lessons where batchId = &#63; and subjectId = &#63;.
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @return the matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findByBatchSubject(
		long batchId, long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBatchSubject(batchId, subjectId);
	}

	/**
	* Returns a range of all the subject lessons where batchId = &#63; and subjectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @param start the lower bound of the range of subject lessons
	* @param end the upper bound of the range of subject lessons (not inclusive)
	* @return the range of matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findByBatchSubject(
		long batchId, long subjectId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatchSubject(batchId, subjectId, start, end);
	}

	/**
	* Returns an ordered range of all the subject lessons where batchId = &#63; and subjectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @param start the lower bound of the range of subject lessons
	* @param end the upper bound of the range of subject lessons (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findByBatchSubject(
		long batchId, long subjectId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatchSubject(batchId, subjectId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject lesson
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson findByBatchSubject_First(
		long batchId, long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException {
		return getPersistence()
				   .findByBatchSubject_First(batchId, subjectId,
			orderByComparator);
	}

	/**
	* Returns the first subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject lesson, or <code>null</code> if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson fetchByBatchSubject_First(
		long batchId, long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBatchSubject_First(batchId, subjectId,
			orderByComparator);
	}

	/**
	* Returns the last subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject lesson
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson findByBatchSubject_Last(
		long batchId, long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException {
		return getPersistence()
				   .findByBatchSubject_Last(batchId, subjectId,
			orderByComparator);
	}

	/**
	* Returns the last subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject lesson, or <code>null</code> if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson fetchByBatchSubject_Last(
		long batchId, long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBatchSubject_Last(batchId, subjectId,
			orderByComparator);
	}

	/**
	* Returns the subject lessons before and after the current subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	*
	* @param SubjectLessonId the primary key of the current subject lesson
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next subject lesson
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a subject lesson with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson[] findByBatchSubject_PrevAndNext(
		long SubjectLessonId, long batchId, long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException {
		return getPersistence()
				   .findByBatchSubject_PrevAndNext(SubjectLessonId, batchId,
			subjectId, orderByComparator);
	}

	/**
	* Returns all the subject lessons.
	*
	* @return the subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the subject lessons.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of subject lessons
	* @param end the upper bound of the range of subject lessons (not inclusive)
	* @return the range of subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the subject lessons.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of subject lessons
	* @param end the upper bound of the range of subject lessons (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the subject lessons where organizationId = &#63; and userId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUserOrganization(long organizationId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUserOrganization(organizationId, userId);
	}

	/**
	* Removes all the subject lessons where batchId = &#63; and subjectId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBatchSubject(long batchId, long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBatchSubject(batchId, subjectId);
	}

	/**
	* Removes all the subject lessons from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of subject lessons where organizationId = &#63; and userId = &#63;.
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @return the number of matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUserOrganization(long organizationId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUserOrganization(organizationId, userId);
	}

	/**
	* Returns the number of subject lessons where batchId = &#63; and subjectId = &#63;.
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @return the number of matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBatchSubject(long batchId, long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBatchSubject(batchId, subjectId);
	}

	/**
	* Returns the number of subject lessons.
	*
	* @return the number of subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static SubjectLessonPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (SubjectLessonPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.lessonplan.service.ClpSerializer.getServletContextName(),
					SubjectLessonPersistence.class.getName());

			ReferenceRegistry.registerReference(SubjectLessonUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(SubjectLessonPersistence persistence) {
	}

	private static SubjectLessonPersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Topic}.
 * </p>
 *
 * @author    limon
 * @see       Topic
 * @generated
 */
public class TopicWrapper implements Topic, ModelWrapper<Topic> {
	public TopicWrapper(Topic topic) {
		_topic = topic;
	}

	public Class<?> getModelClass() {
		return Topic.class;
	}

	public String getModelClassName() {
		return Topic.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("topicId", getTopicId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("chapterId", getChapterId());
		attributes.put("topic", getTopic());
		attributes.put("parentTopic", getParentTopic());
		attributes.put("time", getTime());
		attributes.put("note", getNote());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long topicId = (Long)attributes.get("topicId");

		if (topicId != null) {
			setTopicId(topicId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long chapterId = (Long)attributes.get("chapterId");

		if (chapterId != null) {
			setChapterId(chapterId);
		}

		String topic = (String)attributes.get("topic");

		if (topic != null) {
			setTopic(topic);
		}

		Long parentTopic = (Long)attributes.get("parentTopic");

		if (parentTopic != null) {
			setParentTopic(parentTopic);
		}

		Long time = (Long)attributes.get("time");

		if (time != null) {
			setTime(time);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}
	}

	/**
	* Returns the primary key of this topic.
	*
	* @return the primary key of this topic
	*/
	public long getPrimaryKey() {
		return _topic.getPrimaryKey();
	}

	/**
	* Sets the primary key of this topic.
	*
	* @param primaryKey the primary key of this topic
	*/
	public void setPrimaryKey(long primaryKey) {
		_topic.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the topic ID of this topic.
	*
	* @return the topic ID of this topic
	*/
	public long getTopicId() {
		return _topic.getTopicId();
	}

	/**
	* Sets the topic ID of this topic.
	*
	* @param topicId the topic ID of this topic
	*/
	public void setTopicId(long topicId) {
		_topic.setTopicId(topicId);
	}

	/**
	* Returns the company ID of this topic.
	*
	* @return the company ID of this topic
	*/
	public long getCompanyId() {
		return _topic.getCompanyId();
	}

	/**
	* Sets the company ID of this topic.
	*
	* @param companyId the company ID of this topic
	*/
	public void setCompanyId(long companyId) {
		_topic.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this topic.
	*
	* @return the organization ID of this topic
	*/
	public long getOrganizationId() {
		return _topic.getOrganizationId();
	}

	/**
	* Sets the organization ID of this topic.
	*
	* @param organizationId the organization ID of this topic
	*/
	public void setOrganizationId(long organizationId) {
		_topic.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this topic.
	*
	* @return the user ID of this topic
	*/
	public long getUserId() {
		return _topic.getUserId();
	}

	/**
	* Sets the user ID of this topic.
	*
	* @param userId the user ID of this topic
	*/
	public void setUserId(long userId) {
		_topic.setUserId(userId);
	}

	/**
	* Returns the user uuid of this topic.
	*
	* @return the user uuid of this topic
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _topic.getUserUuid();
	}

	/**
	* Sets the user uuid of this topic.
	*
	* @param userUuid the user uuid of this topic
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_topic.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this topic.
	*
	* @return the user name of this topic
	*/
	public java.lang.String getUserName() {
		return _topic.getUserName();
	}

	/**
	* Sets the user name of this topic.
	*
	* @param userName the user name of this topic
	*/
	public void setUserName(java.lang.String userName) {
		_topic.setUserName(userName);
	}

	/**
	* Returns the create date of this topic.
	*
	* @return the create date of this topic
	*/
	public java.util.Date getCreateDate() {
		return _topic.getCreateDate();
	}

	/**
	* Sets the create date of this topic.
	*
	* @param createDate the create date of this topic
	*/
	public void setCreateDate(java.util.Date createDate) {
		_topic.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this topic.
	*
	* @return the modified date of this topic
	*/
	public java.util.Date getModifiedDate() {
		return _topic.getModifiedDate();
	}

	/**
	* Sets the modified date of this topic.
	*
	* @param modifiedDate the modified date of this topic
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_topic.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the chapter ID of this topic.
	*
	* @return the chapter ID of this topic
	*/
	public long getChapterId() {
		return _topic.getChapterId();
	}

	/**
	* Sets the chapter ID of this topic.
	*
	* @param chapterId the chapter ID of this topic
	*/
	public void setChapterId(long chapterId) {
		_topic.setChapterId(chapterId);
	}

	/**
	* Returns the topic of this topic.
	*
	* @return the topic of this topic
	*/
	public java.lang.String getTopic() {
		return _topic.getTopic();
	}

	/**
	* Sets the topic of this topic.
	*
	* @param topic the topic of this topic
	*/
	public void setTopic(java.lang.String topic) {
		_topic.setTopic(topic);
	}

	/**
	* Returns the parent topic of this topic.
	*
	* @return the parent topic of this topic
	*/
	public long getParentTopic() {
		return _topic.getParentTopic();
	}

	/**
	* Sets the parent topic of this topic.
	*
	* @param parentTopic the parent topic of this topic
	*/
	public void setParentTopic(long parentTopic) {
		_topic.setParentTopic(parentTopic);
	}

	/**
	* Returns the time of this topic.
	*
	* @return the time of this topic
	*/
	public long getTime() {
		return _topic.getTime();
	}

	/**
	* Sets the time of this topic.
	*
	* @param time the time of this topic
	*/
	public void setTime(long time) {
		_topic.setTime(time);
	}

	/**
	* Returns the note of this topic.
	*
	* @return the note of this topic
	*/
	public java.lang.String getNote() {
		return _topic.getNote();
	}

	/**
	* Sets the note of this topic.
	*
	* @param note the note of this topic
	*/
	public void setNote(java.lang.String note) {
		_topic.setNote(note);
	}

	public boolean isNew() {
		return _topic.isNew();
	}

	public void setNew(boolean n) {
		_topic.setNew(n);
	}

	public boolean isCachedModel() {
		return _topic.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_topic.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _topic.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _topic.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_topic.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _topic.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_topic.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new TopicWrapper((Topic)_topic.clone());
	}

	public int compareTo(info.diit.portal.lessonplan.model.Topic topic) {
		return _topic.compareTo(topic);
	}

	@Override
	public int hashCode() {
		return _topic.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.lessonplan.model.Topic> toCacheModel() {
		return _topic.toCacheModel();
	}

	public info.diit.portal.lessonplan.model.Topic toEscapedModel() {
		return new TopicWrapper(_topic.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _topic.toString();
	}

	public java.lang.String toXmlString() {
		return _topic.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_topic.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Topic getWrappedTopic() {
		return _topic;
	}

	public Topic getWrappedModel() {
		return _topic;
	}

	public void resetOriginalValues() {
		_topic.resetOriginalValues();
	}

	private Topic _topic;
}
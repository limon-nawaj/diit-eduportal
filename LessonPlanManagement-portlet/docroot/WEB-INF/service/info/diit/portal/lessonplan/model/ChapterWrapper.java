/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Chapter}.
 * </p>
 *
 * @author    limon
 * @see       Chapter
 * @generated
 */
public class ChapterWrapper implements Chapter, ModelWrapper<Chapter> {
	public ChapterWrapper(Chapter chapter) {
		_chapter = chapter;
	}

	public Class<?> getModelClass() {
		return Chapter.class;
	}

	public String getModelClassName() {
		return Chapter.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("chapterId", getChapterId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("sequence", getSequence());
		attributes.put("name", getName());
		attributes.put("subjectId", getSubjectId());
		attributes.put("note", getNote());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long chapterId = (Long)attributes.get("chapterId");

		if (chapterId != null) {
			setChapterId(chapterId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long sequence = (Long)attributes.get("sequence");

		if (sequence != null) {
			setSequence(sequence);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Long subjectId = (Long)attributes.get("subjectId");

		if (subjectId != null) {
			setSubjectId(subjectId);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}
	}

	/**
	* Returns the primary key of this chapter.
	*
	* @return the primary key of this chapter
	*/
	public long getPrimaryKey() {
		return _chapter.getPrimaryKey();
	}

	/**
	* Sets the primary key of this chapter.
	*
	* @param primaryKey the primary key of this chapter
	*/
	public void setPrimaryKey(long primaryKey) {
		_chapter.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the chapter ID of this chapter.
	*
	* @return the chapter ID of this chapter
	*/
	public long getChapterId() {
		return _chapter.getChapterId();
	}

	/**
	* Sets the chapter ID of this chapter.
	*
	* @param chapterId the chapter ID of this chapter
	*/
	public void setChapterId(long chapterId) {
		_chapter.setChapterId(chapterId);
	}

	/**
	* Returns the company ID of this chapter.
	*
	* @return the company ID of this chapter
	*/
	public long getCompanyId() {
		return _chapter.getCompanyId();
	}

	/**
	* Sets the company ID of this chapter.
	*
	* @param companyId the company ID of this chapter
	*/
	public void setCompanyId(long companyId) {
		_chapter.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this chapter.
	*
	* @return the organization ID of this chapter
	*/
	public long getOrganizationId() {
		return _chapter.getOrganizationId();
	}

	/**
	* Sets the organization ID of this chapter.
	*
	* @param organizationId the organization ID of this chapter
	*/
	public void setOrganizationId(long organizationId) {
		_chapter.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this chapter.
	*
	* @return the user ID of this chapter
	*/
	public long getUserId() {
		return _chapter.getUserId();
	}

	/**
	* Sets the user ID of this chapter.
	*
	* @param userId the user ID of this chapter
	*/
	public void setUserId(long userId) {
		_chapter.setUserId(userId);
	}

	/**
	* Returns the user uuid of this chapter.
	*
	* @return the user uuid of this chapter
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _chapter.getUserUuid();
	}

	/**
	* Sets the user uuid of this chapter.
	*
	* @param userUuid the user uuid of this chapter
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_chapter.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this chapter.
	*
	* @return the user name of this chapter
	*/
	public java.lang.String getUserName() {
		return _chapter.getUserName();
	}

	/**
	* Sets the user name of this chapter.
	*
	* @param userName the user name of this chapter
	*/
	public void setUserName(java.lang.String userName) {
		_chapter.setUserName(userName);
	}

	/**
	* Returns the create date of this chapter.
	*
	* @return the create date of this chapter
	*/
	public java.util.Date getCreateDate() {
		return _chapter.getCreateDate();
	}

	/**
	* Sets the create date of this chapter.
	*
	* @param createDate the create date of this chapter
	*/
	public void setCreateDate(java.util.Date createDate) {
		_chapter.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this chapter.
	*
	* @return the modified date of this chapter
	*/
	public java.util.Date getModifiedDate() {
		return _chapter.getModifiedDate();
	}

	/**
	* Sets the modified date of this chapter.
	*
	* @param modifiedDate the modified date of this chapter
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_chapter.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the sequence of this chapter.
	*
	* @return the sequence of this chapter
	*/
	public long getSequence() {
		return _chapter.getSequence();
	}

	/**
	* Sets the sequence of this chapter.
	*
	* @param sequence the sequence of this chapter
	*/
	public void setSequence(long sequence) {
		_chapter.setSequence(sequence);
	}

	/**
	* Returns the name of this chapter.
	*
	* @return the name of this chapter
	*/
	public java.lang.String getName() {
		return _chapter.getName();
	}

	/**
	* Sets the name of this chapter.
	*
	* @param name the name of this chapter
	*/
	public void setName(java.lang.String name) {
		_chapter.setName(name);
	}

	/**
	* Returns the subject ID of this chapter.
	*
	* @return the subject ID of this chapter
	*/
	public long getSubjectId() {
		return _chapter.getSubjectId();
	}

	/**
	* Sets the subject ID of this chapter.
	*
	* @param subjectId the subject ID of this chapter
	*/
	public void setSubjectId(long subjectId) {
		_chapter.setSubjectId(subjectId);
	}

	/**
	* Returns the note of this chapter.
	*
	* @return the note of this chapter
	*/
	public java.lang.String getNote() {
		return _chapter.getNote();
	}

	/**
	* Sets the note of this chapter.
	*
	* @param note the note of this chapter
	*/
	public void setNote(java.lang.String note) {
		_chapter.setNote(note);
	}

	public boolean isNew() {
		return _chapter.isNew();
	}

	public void setNew(boolean n) {
		_chapter.setNew(n);
	}

	public boolean isCachedModel() {
		return _chapter.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_chapter.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _chapter.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _chapter.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_chapter.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _chapter.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_chapter.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ChapterWrapper((Chapter)_chapter.clone());
	}

	public int compareTo(info.diit.portal.lessonplan.model.Chapter chapter) {
		return _chapter.compareTo(chapter);
	}

	@Override
	public int hashCode() {
		return _chapter.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.lessonplan.model.Chapter> toCacheModel() {
		return _chapter.toCacheModel();
	}

	public info.diit.portal.lessonplan.model.Chapter toEscapedModel() {
		return new ChapterWrapper(_chapter.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _chapter.toString();
	}

	public java.lang.String toXmlString() {
		return _chapter.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_chapter.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Chapter getWrappedChapter() {
		return _chapter;
	}

	public Chapter getWrappedModel() {
		return _chapter;
	}

	public void resetOriginalValues() {
		_chapter.resetOriginalValues();
	}

	private Chapter _chapter;
}
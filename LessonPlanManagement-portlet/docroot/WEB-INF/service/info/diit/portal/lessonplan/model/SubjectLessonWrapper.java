/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link SubjectLesson}.
 * </p>
 *
 * @author    limon
 * @see       SubjectLesson
 * @generated
 */
public class SubjectLessonWrapper implements SubjectLesson,
	ModelWrapper<SubjectLesson> {
	public SubjectLessonWrapper(SubjectLesson subjectLesson) {
		_subjectLesson = subjectLesson;
	}

	public Class<?> getModelClass() {
		return SubjectLesson.class;
	}

	public String getModelClassName() {
		return SubjectLesson.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("SubjectLessonId", getSubjectLessonId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("batchId", getBatchId());
		attributes.put("subjectId", getSubjectId());
		attributes.put("lessonPlanId", getLessonPlanId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long SubjectLessonId = (Long)attributes.get("SubjectLessonId");

		if (SubjectLessonId != null) {
			setSubjectLessonId(SubjectLessonId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Long subjectId = (Long)attributes.get("subjectId");

		if (subjectId != null) {
			setSubjectId(subjectId);
		}

		Long lessonPlanId = (Long)attributes.get("lessonPlanId");

		if (lessonPlanId != null) {
			setLessonPlanId(lessonPlanId);
		}
	}

	/**
	* Returns the primary key of this subject lesson.
	*
	* @return the primary key of this subject lesson
	*/
	public long getPrimaryKey() {
		return _subjectLesson.getPrimaryKey();
	}

	/**
	* Sets the primary key of this subject lesson.
	*
	* @param primaryKey the primary key of this subject lesson
	*/
	public void setPrimaryKey(long primaryKey) {
		_subjectLesson.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the subject lesson ID of this subject lesson.
	*
	* @return the subject lesson ID of this subject lesson
	*/
	public long getSubjectLessonId() {
		return _subjectLesson.getSubjectLessonId();
	}

	/**
	* Sets the subject lesson ID of this subject lesson.
	*
	* @param SubjectLessonId the subject lesson ID of this subject lesson
	*/
	public void setSubjectLessonId(long SubjectLessonId) {
		_subjectLesson.setSubjectLessonId(SubjectLessonId);
	}

	/**
	* Returns the company ID of this subject lesson.
	*
	* @return the company ID of this subject lesson
	*/
	public long getCompanyId() {
		return _subjectLesson.getCompanyId();
	}

	/**
	* Sets the company ID of this subject lesson.
	*
	* @param companyId the company ID of this subject lesson
	*/
	public void setCompanyId(long companyId) {
		_subjectLesson.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this subject lesson.
	*
	* @return the user ID of this subject lesson
	*/
	public long getUserId() {
		return _subjectLesson.getUserId();
	}

	/**
	* Sets the user ID of this subject lesson.
	*
	* @param userId the user ID of this subject lesson
	*/
	public void setUserId(long userId) {
		_subjectLesson.setUserId(userId);
	}

	/**
	* Returns the user uuid of this subject lesson.
	*
	* @return the user uuid of this subject lesson
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subjectLesson.getUserUuid();
	}

	/**
	* Sets the user uuid of this subject lesson.
	*
	* @param userUuid the user uuid of this subject lesson
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_subjectLesson.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this subject lesson.
	*
	* @return the user name of this subject lesson
	*/
	public java.lang.String getUserName() {
		return _subjectLesson.getUserName();
	}

	/**
	* Sets the user name of this subject lesson.
	*
	* @param userName the user name of this subject lesson
	*/
	public void setUserName(java.lang.String userName) {
		_subjectLesson.setUserName(userName);
	}

	/**
	* Returns the create date of this subject lesson.
	*
	* @return the create date of this subject lesson
	*/
	public java.util.Date getCreateDate() {
		return _subjectLesson.getCreateDate();
	}

	/**
	* Sets the create date of this subject lesson.
	*
	* @param createDate the create date of this subject lesson
	*/
	public void setCreateDate(java.util.Date createDate) {
		_subjectLesson.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this subject lesson.
	*
	* @return the modified date of this subject lesson
	*/
	public java.util.Date getModifiedDate() {
		return _subjectLesson.getModifiedDate();
	}

	/**
	* Sets the modified date of this subject lesson.
	*
	* @param modifiedDate the modified date of this subject lesson
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_subjectLesson.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the organization ID of this subject lesson.
	*
	* @return the organization ID of this subject lesson
	*/
	public long getOrganizationId() {
		return _subjectLesson.getOrganizationId();
	}

	/**
	* Sets the organization ID of this subject lesson.
	*
	* @param organizationId the organization ID of this subject lesson
	*/
	public void setOrganizationId(long organizationId) {
		_subjectLesson.setOrganizationId(organizationId);
	}

	/**
	* Returns the batch ID of this subject lesson.
	*
	* @return the batch ID of this subject lesson
	*/
	public long getBatchId() {
		return _subjectLesson.getBatchId();
	}

	/**
	* Sets the batch ID of this subject lesson.
	*
	* @param batchId the batch ID of this subject lesson
	*/
	public void setBatchId(long batchId) {
		_subjectLesson.setBatchId(batchId);
	}

	/**
	* Returns the subject ID of this subject lesson.
	*
	* @return the subject ID of this subject lesson
	*/
	public long getSubjectId() {
		return _subjectLesson.getSubjectId();
	}

	/**
	* Sets the subject ID of this subject lesson.
	*
	* @param subjectId the subject ID of this subject lesson
	*/
	public void setSubjectId(long subjectId) {
		_subjectLesson.setSubjectId(subjectId);
	}

	/**
	* Returns the lesson plan ID of this subject lesson.
	*
	* @return the lesson plan ID of this subject lesson
	*/
	public long getLessonPlanId() {
		return _subjectLesson.getLessonPlanId();
	}

	/**
	* Sets the lesson plan ID of this subject lesson.
	*
	* @param lessonPlanId the lesson plan ID of this subject lesson
	*/
	public void setLessonPlanId(long lessonPlanId) {
		_subjectLesson.setLessonPlanId(lessonPlanId);
	}

	public boolean isNew() {
		return _subjectLesson.isNew();
	}

	public void setNew(boolean n) {
		_subjectLesson.setNew(n);
	}

	public boolean isCachedModel() {
		return _subjectLesson.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_subjectLesson.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _subjectLesson.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _subjectLesson.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_subjectLesson.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _subjectLesson.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_subjectLesson.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new SubjectLessonWrapper((SubjectLesson)_subjectLesson.clone());
	}

	public int compareTo(
		info.diit.portal.lessonplan.model.SubjectLesson subjectLesson) {
		return _subjectLesson.compareTo(subjectLesson);
	}

	@Override
	public int hashCode() {
		return _subjectLesson.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.lessonplan.model.SubjectLesson> toCacheModel() {
		return _subjectLesson.toCacheModel();
	}

	public info.diit.portal.lessonplan.model.SubjectLesson toEscapedModel() {
		return new SubjectLessonWrapper(_subjectLesson.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _subjectLesson.toString();
	}

	public java.lang.String toXmlString() {
		return _subjectLesson.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_subjectLesson.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public SubjectLesson getWrappedSubjectLesson() {
		return _subjectLesson;
	}

	public SubjectLesson getWrappedModel() {
		return _subjectLesson;
	}

	public void resetOriginalValues() {
		_subjectLesson.resetOriginalValues();
	}

	private SubjectLesson _subjectLesson;
}
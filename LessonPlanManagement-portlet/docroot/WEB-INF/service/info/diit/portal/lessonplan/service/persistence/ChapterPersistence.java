/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.lessonplan.model.Chapter;

/**
 * The persistence interface for the chapter service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see ChapterPersistenceImpl
 * @see ChapterUtil
 * @generated
 */
public interface ChapterPersistence extends BasePersistence<Chapter> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ChapterUtil} to access the chapter persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the chapter in the entity cache if it is enabled.
	*
	* @param chapter the chapter
	*/
	public void cacheResult(info.diit.portal.lessonplan.model.Chapter chapter);

	/**
	* Caches the chapters in the entity cache if it is enabled.
	*
	* @param chapters the chapters
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.lessonplan.model.Chapter> chapters);

	/**
	* Creates a new chapter with the primary key. Does not add the chapter to the database.
	*
	* @param chapterId the primary key for the new chapter
	* @return the new chapter
	*/
	public info.diit.portal.lessonplan.model.Chapter create(long chapterId);

	/**
	* Removes the chapter with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param chapterId the primary key of the chapter
	* @return the chapter that was removed
	* @throws info.diit.portal.lessonplan.NoSuchChapterException if a chapter with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter remove(long chapterId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchChapterException;

	public info.diit.portal.lessonplan.model.Chapter updateImpl(
		info.diit.portal.lessonplan.model.Chapter chapter, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the chapter with the primary key or throws a {@link info.diit.portal.lessonplan.NoSuchChapterException} if it could not be found.
	*
	* @param chapterId the primary key of the chapter
	* @return the chapter
	* @throws info.diit.portal.lessonplan.NoSuchChapterException if a chapter with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter findByPrimaryKey(
		long chapterId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchChapterException;

	/**
	* Returns the chapter with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param chapterId the primary key of the chapter
	* @return the chapter, or <code>null</code> if a chapter with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter fetchByPrimaryKey(
		long chapterId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the chapters where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.Chapter> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the chapters where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of chapters
	* @param end the upper bound of the range of chapters (not inclusive)
	* @return the range of matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.Chapter> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the chapters where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of chapters
	* @param end the upper bound of the range of chapters (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.Chapter> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first chapter in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching chapter
	* @throws info.diit.portal.lessonplan.NoSuchChapterException if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchChapterException;

	/**
	* Returns the first chapter in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching chapter, or <code>null</code> if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last chapter in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching chapter
	* @throws info.diit.portal.lessonplan.NoSuchChapterException if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchChapterException;

	/**
	* Returns the last chapter in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching chapter, or <code>null</code> if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the chapters before and after the current chapter in the ordered set where companyId = &#63;.
	*
	* @param chapterId the primary key of the current chapter
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next chapter
	* @throws info.diit.portal.lessonplan.NoSuchChapterException if a chapter with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter[] findByCompany_PrevAndNext(
		long chapterId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchChapterException;

	/**
	* Returns all the chapters where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @return the matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.Chapter> findBySubject(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the chapters where subjectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject ID
	* @param start the lower bound of the range of chapters
	* @param end the upper bound of the range of chapters (not inclusive)
	* @return the range of matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.Chapter> findBySubject(
		long subjectId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the chapters where subjectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject ID
	* @param start the lower bound of the range of chapters
	* @param end the upper bound of the range of chapters (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.Chapter> findBySubject(
		long subjectId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first chapter in the ordered set where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching chapter
	* @throws info.diit.portal.lessonplan.NoSuchChapterException if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter findBySubject_First(
		long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchChapterException;

	/**
	* Returns the first chapter in the ordered set where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching chapter, or <code>null</code> if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter fetchBySubject_First(
		long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last chapter in the ordered set where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching chapter
	* @throws info.diit.portal.lessonplan.NoSuchChapterException if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter findBySubject_Last(
		long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchChapterException;

	/**
	* Returns the last chapter in the ordered set where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching chapter, or <code>null</code> if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter fetchBySubject_Last(
		long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the chapters before and after the current chapter in the ordered set where subjectId = &#63;.
	*
	* @param chapterId the primary key of the current chapter
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next chapter
	* @throws info.diit.portal.lessonplan.NoSuchChapterException if a chapter with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter[] findBySubject_PrevAndNext(
		long chapterId, long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchChapterException;

	/**
	* Returns the chapter where chapterId = &#63; and sequence = &#63; or throws a {@link info.diit.portal.lessonplan.NoSuchChapterException} if it could not be found.
	*
	* @param chapterId the chapter ID
	* @param sequence the sequence
	* @return the matching chapter
	* @throws info.diit.portal.lessonplan.NoSuchChapterException if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter findByChapterSequence(
		long chapterId, long sequence)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchChapterException;

	/**
	* Returns the chapter where chapterId = &#63; and sequence = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param chapterId the chapter ID
	* @param sequence the sequence
	* @return the matching chapter, or <code>null</code> if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter fetchByChapterSequence(
		long chapterId, long sequence)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the chapter where chapterId = &#63; and sequence = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param chapterId the chapter ID
	* @param sequence the sequence
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching chapter, or <code>null</code> if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter fetchByChapterSequence(
		long chapterId, long sequence, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the chapters.
	*
	* @return the chapters
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.Chapter> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the chapters.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of chapters
	* @param end the upper bound of the range of chapters (not inclusive)
	* @return the range of chapters
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.Chapter> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the chapters.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of chapters
	* @param end the upper bound of the range of chapters (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of chapters
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.Chapter> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the chapters where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the chapters where subjectId = &#63; from the database.
	*
	* @param subjectId the subject ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBySubject(long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the chapter where chapterId = &#63; and sequence = &#63; from the database.
	*
	* @param chapterId the chapter ID
	* @param sequence the sequence
	* @return the chapter that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.Chapter removeByChapterSequence(
		long chapterId, long sequence)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchChapterException;

	/**
	* Removes all the chapters from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of chapters where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of chapters where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @return the number of matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public int countBySubject(long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of chapters where chapterId = &#63; and sequence = &#63;.
	*
	* @param chapterId the chapter ID
	* @param sequence the sequence
	* @return the number of matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public int countByChapterSequence(long chapterId, long sequence)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of chapters.
	*
	* @return the number of chapters
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link LessonPlanLocalService}.
 * </p>
 *
 * @author    limon
 * @see       LessonPlanLocalService
 * @generated
 */
public class LessonPlanLocalServiceWrapper implements LessonPlanLocalService,
	ServiceWrapper<LessonPlanLocalService> {
	public LessonPlanLocalServiceWrapper(
		LessonPlanLocalService lessonPlanLocalService) {
		_lessonPlanLocalService = lessonPlanLocalService;
	}

	/**
	* Adds the lesson plan to the database. Also notifies the appropriate model listeners.
	*
	* @param lessonPlan the lesson plan
	* @return the lesson plan that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.LessonPlan addLessonPlan(
		info.diit.portal.lessonplan.model.LessonPlan lessonPlan)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.addLessonPlan(lessonPlan);
	}

	/**
	* Creates a new lesson plan with the primary key. Does not add the lesson plan to the database.
	*
	* @param lessonPlanId the primary key for the new lesson plan
	* @return the new lesson plan
	*/
	public info.diit.portal.lessonplan.model.LessonPlan createLessonPlan(
		long lessonPlanId) {
		return _lessonPlanLocalService.createLessonPlan(lessonPlanId);
	}

	/**
	* Deletes the lesson plan with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonPlanId the primary key of the lesson plan
	* @return the lesson plan that was removed
	* @throws PortalException if a lesson plan with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.LessonPlan deleteLessonPlan(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.deleteLessonPlan(lessonPlanId);
	}

	/**
	* Deletes the lesson plan from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonPlan the lesson plan
	* @return the lesson plan that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.LessonPlan deleteLessonPlan(
		info.diit.portal.lessonplan.model.LessonPlan lessonPlan)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.deleteLessonPlan(lessonPlan);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _lessonPlanLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.lessonplan.model.LessonPlan fetchLessonPlan(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.fetchLessonPlan(lessonPlanId);
	}

	/**
	* Returns the lesson plan with the primary key.
	*
	* @param lessonPlanId the primary key of the lesson plan
	* @return the lesson plan
	* @throws PortalException if a lesson plan with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.LessonPlan getLessonPlan(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.getLessonPlan(lessonPlanId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the lesson plans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson plans
	* @param end the upper bound of the range of lesson plans (not inclusive)
	* @return the range of lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.LessonPlan> getLessonPlans(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.getLessonPlans(start, end);
	}

	/**
	* Returns the number of lesson plans.
	*
	* @return the number of lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public int getLessonPlansCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.getLessonPlansCount();
	}

	/**
	* Updates the lesson plan in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lessonPlan the lesson plan
	* @return the lesson plan that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.LessonPlan updateLessonPlan(
		info.diit.portal.lessonplan.model.LessonPlan lessonPlan)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.updateLessonPlan(lessonPlan);
	}

	/**
	* Updates the lesson plan in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lessonPlan the lesson plan
	* @param merge whether to merge the lesson plan with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the lesson plan that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.LessonPlan updateLessonPlan(
		info.diit.portal.lessonplan.model.LessonPlan lessonPlan, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.updateLessonPlan(lessonPlan, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _lessonPlanLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_lessonPlanLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _lessonPlanLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.lessonplan.model.LessonPlan> findByOrganizationUser(
		long user) throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.findByOrganizationUser(user);
	}

	public java.util.List<info.diit.portal.lessonplan.model.LessonPlan> findBySubject(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlanLocalService.findBySubject(subjectId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public LessonPlanLocalService getWrappedLessonPlanLocalService() {
		return _lessonPlanLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedLessonPlanLocalService(
		LessonPlanLocalService lessonPlanLocalService) {
		_lessonPlanLocalService = lessonPlanLocalService;
	}

	public LessonPlanLocalService getWrappedService() {
		return _lessonPlanLocalService;
	}

	public void setWrappedService(LessonPlanLocalService lessonPlanLocalService) {
		_lessonPlanLocalService = lessonPlanLocalService;
	}

	private LessonPlanLocalService _lessonPlanLocalService;
}
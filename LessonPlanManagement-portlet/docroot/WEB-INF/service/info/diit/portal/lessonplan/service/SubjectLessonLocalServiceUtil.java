/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the subject lesson local service. This utility wraps {@link info.diit.portal.lessonplan.service.impl.SubjectLessonLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see SubjectLessonLocalService
 * @see info.diit.portal.lessonplan.service.base.SubjectLessonLocalServiceBaseImpl
 * @see info.diit.portal.lessonplan.service.impl.SubjectLessonLocalServiceImpl
 * @generated
 */
public class SubjectLessonLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.lessonplan.service.impl.SubjectLessonLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the subject lesson to the database. Also notifies the appropriate model listeners.
	*
	* @param subjectLesson the subject lesson
	* @return the subject lesson that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson addSubjectLesson(
		info.diit.portal.lessonplan.model.SubjectLesson subjectLesson)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addSubjectLesson(subjectLesson);
	}

	/**
	* Creates a new subject lesson with the primary key. Does not add the subject lesson to the database.
	*
	* @param SubjectLessonId the primary key for the new subject lesson
	* @return the new subject lesson
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson createSubjectLesson(
		long SubjectLessonId) {
		return getService().createSubjectLesson(SubjectLessonId);
	}

	/**
	* Deletes the subject lesson with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param SubjectLessonId the primary key of the subject lesson
	* @return the subject lesson that was removed
	* @throws PortalException if a subject lesson with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson deleteSubjectLesson(
		long SubjectLessonId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteSubjectLesson(SubjectLessonId);
	}

	/**
	* Deletes the subject lesson from the database. Also notifies the appropriate model listeners.
	*
	* @param subjectLesson the subject lesson
	* @return the subject lesson that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson deleteSubjectLesson(
		info.diit.portal.lessonplan.model.SubjectLesson subjectLesson)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteSubjectLesson(subjectLesson);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.lessonplan.model.SubjectLesson fetchSubjectLesson(
		long SubjectLessonId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchSubjectLesson(SubjectLessonId);
	}

	/**
	* Returns the subject lesson with the primary key.
	*
	* @param SubjectLessonId the primary key of the subject lesson
	* @return the subject lesson
	* @throws PortalException if a subject lesson with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson getSubjectLesson(
		long SubjectLessonId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getSubjectLesson(SubjectLessonId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the subject lessons.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of subject lessons
	* @param end the upper bound of the range of subject lessons (not inclusive)
	* @return the range of subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> getSubjectLessons(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getSubjectLessons(start, end);
	}

	/**
	* Returns the number of subject lessons.
	*
	* @return the number of subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public static int getSubjectLessonsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getSubjectLessonsCount();
	}

	/**
	* Updates the subject lesson in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param subjectLesson the subject lesson
	* @return the subject lesson that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson updateSubjectLesson(
		info.diit.portal.lessonplan.model.SubjectLesson subjectLesson)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateSubjectLesson(subjectLesson);
	}

	/**
	* Updates the subject lesson in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param subjectLesson the subject lesson
	* @param merge whether to merge the subject lesson with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the subject lesson that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.lessonplan.model.SubjectLesson updateSubjectLesson(
		info.diit.portal.lessonplan.model.SubjectLesson subjectLesson,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateSubjectLesson(subjectLesson, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findByUserOrganization(
		long organizationId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByUserOrganization(organizationId, userId);
	}

	public static java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findByBatchSubject(
		long batch, long subject)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByBatchSubject(batch, subject);
	}

	public static void clearService() {
		_service = null;
	}

	public static SubjectLessonLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					SubjectLessonLocalService.class.getName());

			if (invokableLocalService instanceof SubjectLessonLocalService) {
				_service = (SubjectLessonLocalService)invokableLocalService;
			}
			else {
				_service = new SubjectLessonLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(SubjectLessonLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(SubjectLessonLocalService service) {
	}

	private static SubjectLessonLocalService _service;
}
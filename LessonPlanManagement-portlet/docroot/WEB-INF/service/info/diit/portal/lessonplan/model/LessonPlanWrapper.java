/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link LessonPlan}.
 * </p>
 *
 * @author    limon
 * @see       LessonPlan
 * @generated
 */
public class LessonPlanWrapper implements LessonPlan, ModelWrapper<LessonPlan> {
	public LessonPlanWrapper(LessonPlan lessonPlan) {
		_lessonPlan = lessonPlan;
	}

	public Class<?> getModelClass() {
		return LessonPlan.class;
	}

	public String getModelClassName() {
		return LessonPlan.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("lessonPlanId", getLessonPlanId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("title", getTitle());
		attributes.put("organization", getOrganization());
		attributes.put("subject", getSubject());
		attributes.put("planDate", getPlanDate());
		attributes.put("totalClass", getTotalClass());
		attributes.put("totalDuration", getTotalDuration());
		attributes.put("objective", getObjective());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long lessonPlanId = (Long)attributes.get("lessonPlanId");

		if (lessonPlanId != null) {
			setLessonPlanId(lessonPlanId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		Long organization = (Long)attributes.get("organization");

		if (organization != null) {
			setOrganization(organization);
		}

		Long subject = (Long)attributes.get("subject");

		if (subject != null) {
			setSubject(subject);
		}

		Date planDate = (Date)attributes.get("planDate");

		if (planDate != null) {
			setPlanDate(planDate);
		}

		Long totalClass = (Long)attributes.get("totalClass");

		if (totalClass != null) {
			setTotalClass(totalClass);
		}

		Long totalDuration = (Long)attributes.get("totalDuration");

		if (totalDuration != null) {
			setTotalDuration(totalDuration);
		}

		String objective = (String)attributes.get("objective");

		if (objective != null) {
			setObjective(objective);
		}
	}

	/**
	* Returns the primary key of this lesson plan.
	*
	* @return the primary key of this lesson plan
	*/
	public long getPrimaryKey() {
		return _lessonPlan.getPrimaryKey();
	}

	/**
	* Sets the primary key of this lesson plan.
	*
	* @param primaryKey the primary key of this lesson plan
	*/
	public void setPrimaryKey(long primaryKey) {
		_lessonPlan.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the lesson plan ID of this lesson plan.
	*
	* @return the lesson plan ID of this lesson plan
	*/
	public long getLessonPlanId() {
		return _lessonPlan.getLessonPlanId();
	}

	/**
	* Sets the lesson plan ID of this lesson plan.
	*
	* @param lessonPlanId the lesson plan ID of this lesson plan
	*/
	public void setLessonPlanId(long lessonPlanId) {
		_lessonPlan.setLessonPlanId(lessonPlanId);
	}

	/**
	* Returns the company ID of this lesson plan.
	*
	* @return the company ID of this lesson plan
	*/
	public long getCompanyId() {
		return _lessonPlan.getCompanyId();
	}

	/**
	* Sets the company ID of this lesson plan.
	*
	* @param companyId the company ID of this lesson plan
	*/
	public void setCompanyId(long companyId) {
		_lessonPlan.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this lesson plan.
	*
	* @return the user ID of this lesson plan
	*/
	public long getUserId() {
		return _lessonPlan.getUserId();
	}

	/**
	* Sets the user ID of this lesson plan.
	*
	* @param userId the user ID of this lesson plan
	*/
	public void setUserId(long userId) {
		_lessonPlan.setUserId(userId);
	}

	/**
	* Returns the user uuid of this lesson plan.
	*
	* @return the user uuid of this lesson plan
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonPlan.getUserUuid();
	}

	/**
	* Sets the user uuid of this lesson plan.
	*
	* @param userUuid the user uuid of this lesson plan
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_lessonPlan.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this lesson plan.
	*
	* @return the user name of this lesson plan
	*/
	public java.lang.String getUserName() {
		return _lessonPlan.getUserName();
	}

	/**
	* Sets the user name of this lesson plan.
	*
	* @param userName the user name of this lesson plan
	*/
	public void setUserName(java.lang.String userName) {
		_lessonPlan.setUserName(userName);
	}

	/**
	* Returns the create date of this lesson plan.
	*
	* @return the create date of this lesson plan
	*/
	public java.util.Date getCreateDate() {
		return _lessonPlan.getCreateDate();
	}

	/**
	* Sets the create date of this lesson plan.
	*
	* @param createDate the create date of this lesson plan
	*/
	public void setCreateDate(java.util.Date createDate) {
		_lessonPlan.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this lesson plan.
	*
	* @return the modified date of this lesson plan
	*/
	public java.util.Date getModifiedDate() {
		return _lessonPlan.getModifiedDate();
	}

	/**
	* Sets the modified date of this lesson plan.
	*
	* @param modifiedDate the modified date of this lesson plan
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_lessonPlan.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the title of this lesson plan.
	*
	* @return the title of this lesson plan
	*/
	public java.lang.String getTitle() {
		return _lessonPlan.getTitle();
	}

	/**
	* Sets the title of this lesson plan.
	*
	* @param title the title of this lesson plan
	*/
	public void setTitle(java.lang.String title) {
		_lessonPlan.setTitle(title);
	}

	/**
	* Returns the organization of this lesson plan.
	*
	* @return the organization of this lesson plan
	*/
	public long getOrganization() {
		return _lessonPlan.getOrganization();
	}

	/**
	* Sets the organization of this lesson plan.
	*
	* @param organization the organization of this lesson plan
	*/
	public void setOrganization(long organization) {
		_lessonPlan.setOrganization(organization);
	}

	/**
	* Returns the subject of this lesson plan.
	*
	* @return the subject of this lesson plan
	*/
	public long getSubject() {
		return _lessonPlan.getSubject();
	}

	/**
	* Sets the subject of this lesson plan.
	*
	* @param subject the subject of this lesson plan
	*/
	public void setSubject(long subject) {
		_lessonPlan.setSubject(subject);
	}

	/**
	* Returns the plan date of this lesson plan.
	*
	* @return the plan date of this lesson plan
	*/
	public java.util.Date getPlanDate() {
		return _lessonPlan.getPlanDate();
	}

	/**
	* Sets the plan date of this lesson plan.
	*
	* @param planDate the plan date of this lesson plan
	*/
	public void setPlanDate(java.util.Date planDate) {
		_lessonPlan.setPlanDate(planDate);
	}

	/**
	* Returns the total class of this lesson plan.
	*
	* @return the total class of this lesson plan
	*/
	public long getTotalClass() {
		return _lessonPlan.getTotalClass();
	}

	/**
	* Sets the total class of this lesson plan.
	*
	* @param totalClass the total class of this lesson plan
	*/
	public void setTotalClass(long totalClass) {
		_lessonPlan.setTotalClass(totalClass);
	}

	/**
	* Returns the total duration of this lesson plan.
	*
	* @return the total duration of this lesson plan
	*/
	public long getTotalDuration() {
		return _lessonPlan.getTotalDuration();
	}

	/**
	* Sets the total duration of this lesson plan.
	*
	* @param totalDuration the total duration of this lesson plan
	*/
	public void setTotalDuration(long totalDuration) {
		_lessonPlan.setTotalDuration(totalDuration);
	}

	/**
	* Returns the objective of this lesson plan.
	*
	* @return the objective of this lesson plan
	*/
	public java.lang.String getObjective() {
		return _lessonPlan.getObjective();
	}

	/**
	* Sets the objective of this lesson plan.
	*
	* @param objective the objective of this lesson plan
	*/
	public void setObjective(java.lang.String objective) {
		_lessonPlan.setObjective(objective);
	}

	public boolean isNew() {
		return _lessonPlan.isNew();
	}

	public void setNew(boolean n) {
		_lessonPlan.setNew(n);
	}

	public boolean isCachedModel() {
		return _lessonPlan.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_lessonPlan.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _lessonPlan.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _lessonPlan.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_lessonPlan.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _lessonPlan.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_lessonPlan.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LessonPlanWrapper((LessonPlan)_lessonPlan.clone());
	}

	public int compareTo(
		info.diit.portal.lessonplan.model.LessonPlan lessonPlan) {
		return _lessonPlan.compareTo(lessonPlan);
	}

	@Override
	public int hashCode() {
		return _lessonPlan.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.lessonplan.model.LessonPlan> toCacheModel() {
		return _lessonPlan.toCacheModel();
	}

	public info.diit.portal.lessonplan.model.LessonPlan toEscapedModel() {
		return new LessonPlanWrapper(_lessonPlan.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _lessonPlan.toString();
	}

	public java.lang.String toXmlString() {
		return _lessonPlan.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_lessonPlan.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public LessonPlan getWrappedLessonPlan() {
		return _lessonPlan;
	}

	public LessonPlan getWrappedModel() {
		return _lessonPlan;
	}

	public void resetOriginalValues() {
		_lessonPlan.resetOriginalValues();
	}

	private LessonPlan _lessonPlan;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.lessonplan.model.SubjectLesson;

/**
 * The persistence interface for the subject lesson service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see SubjectLessonPersistenceImpl
 * @see SubjectLessonUtil
 * @generated
 */
public interface SubjectLessonPersistence extends BasePersistence<SubjectLesson> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SubjectLessonUtil} to access the subject lesson persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the subject lesson in the entity cache if it is enabled.
	*
	* @param subjectLesson the subject lesson
	*/
	public void cacheResult(
		info.diit.portal.lessonplan.model.SubjectLesson subjectLesson);

	/**
	* Caches the subject lessons in the entity cache if it is enabled.
	*
	* @param subjectLessons the subject lessons
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> subjectLessons);

	/**
	* Creates a new subject lesson with the primary key. Does not add the subject lesson to the database.
	*
	* @param SubjectLessonId the primary key for the new subject lesson
	* @return the new subject lesson
	*/
	public info.diit.portal.lessonplan.model.SubjectLesson create(
		long SubjectLessonId);

	/**
	* Removes the subject lesson with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param SubjectLessonId the primary key of the subject lesson
	* @return the subject lesson that was removed
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a subject lesson with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.SubjectLesson remove(
		long SubjectLessonId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException;

	public info.diit.portal.lessonplan.model.SubjectLesson updateImpl(
		info.diit.portal.lessonplan.model.SubjectLesson subjectLesson,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the subject lesson with the primary key or throws a {@link info.diit.portal.lessonplan.NoSuchSubjectLessonException} if it could not be found.
	*
	* @param SubjectLessonId the primary key of the subject lesson
	* @return the subject lesson
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a subject lesson with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.SubjectLesson findByPrimaryKey(
		long SubjectLessonId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException;

	/**
	* Returns the subject lesson with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param SubjectLessonId the primary key of the subject lesson
	* @return the subject lesson, or <code>null</code> if a subject lesson with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.SubjectLesson fetchByPrimaryKey(
		long SubjectLessonId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the subject lessons where organizationId = &#63; and userId = &#63;.
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @return the matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findByUserOrganization(
		long organizationId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the subject lessons where organizationId = &#63; and userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @param start the lower bound of the range of subject lessons
	* @param end the upper bound of the range of subject lessons (not inclusive)
	* @return the range of matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findByUserOrganization(
		long organizationId, long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the subject lessons where organizationId = &#63; and userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @param start the lower bound of the range of subject lessons
	* @param end the upper bound of the range of subject lessons (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findByUserOrganization(
		long organizationId, long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject lesson
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.SubjectLesson findByUserOrganization_First(
		long organizationId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException;

	/**
	* Returns the first subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject lesson, or <code>null</code> if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.SubjectLesson fetchByUserOrganization_First(
		long organizationId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject lesson
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.SubjectLesson findByUserOrganization_Last(
		long organizationId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException;

	/**
	* Returns the last subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject lesson, or <code>null</code> if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.SubjectLesson fetchByUserOrganization_Last(
		long organizationId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the subject lessons before and after the current subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	*
	* @param SubjectLessonId the primary key of the current subject lesson
	* @param organizationId the organization ID
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next subject lesson
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a subject lesson with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.SubjectLesson[] findByUserOrganization_PrevAndNext(
		long SubjectLessonId, long organizationId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException;

	/**
	* Returns all the subject lessons where batchId = &#63; and subjectId = &#63;.
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @return the matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findByBatchSubject(
		long batchId, long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the subject lessons where batchId = &#63; and subjectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @param start the lower bound of the range of subject lessons
	* @param end the upper bound of the range of subject lessons (not inclusive)
	* @return the range of matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findByBatchSubject(
		long batchId, long subjectId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the subject lessons where batchId = &#63; and subjectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @param start the lower bound of the range of subject lessons
	* @param end the upper bound of the range of subject lessons (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findByBatchSubject(
		long batchId, long subjectId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject lesson
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.SubjectLesson findByBatchSubject_First(
		long batchId, long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException;

	/**
	* Returns the first subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject lesson, or <code>null</code> if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.SubjectLesson fetchByBatchSubject_First(
		long batchId, long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject lesson
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.SubjectLesson findByBatchSubject_Last(
		long batchId, long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException;

	/**
	* Returns the last subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject lesson, or <code>null</code> if a matching subject lesson could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.SubjectLesson fetchByBatchSubject_Last(
		long batchId, long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the subject lessons before and after the current subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	*
	* @param SubjectLessonId the primary key of the current subject lesson
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next subject lesson
	* @throws info.diit.portal.lessonplan.NoSuchSubjectLessonException if a subject lesson with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.SubjectLesson[] findByBatchSubject_PrevAndNext(
		long SubjectLessonId, long batchId, long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchSubjectLessonException;

	/**
	* Returns all the subject lessons.
	*
	* @return the subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the subject lessons.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of subject lessons
	* @param end the upper bound of the range of subject lessons (not inclusive)
	* @return the range of subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the subject lessons.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of subject lessons
	* @param end the upper bound of the range of subject lessons (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.SubjectLesson> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the subject lessons where organizationId = &#63; and userId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUserOrganization(long organizationId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the subject lessons where batchId = &#63; and subjectId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBatchSubject(long batchId, long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the subject lessons from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of subject lessons where organizationId = &#63; and userId = &#63;.
	*
	* @param organizationId the organization ID
	* @param userId the user ID
	* @return the number of matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserOrganization(long organizationId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of subject lessons where batchId = &#63; and subjectId = &#63;.
	*
	* @param batchId the batch ID
	* @param subjectId the subject ID
	* @return the number of matching subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public int countByBatchSubject(long batchId, long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of subject lessons.
	*
	* @return the number of subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
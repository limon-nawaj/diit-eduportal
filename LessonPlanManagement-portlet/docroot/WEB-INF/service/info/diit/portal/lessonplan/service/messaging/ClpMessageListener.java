/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.service.messaging;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

import info.diit.portal.lessonplan.service.ChapterLocalServiceUtil;
import info.diit.portal.lessonplan.service.ClpSerializer;
import info.diit.portal.lessonplan.service.LessonAssessmentLocalServiceUtil;
import info.diit.portal.lessonplan.service.LessonPlanLocalServiceUtil;
import info.diit.portal.lessonplan.service.LessonTopicLocalServiceUtil;
import info.diit.portal.lessonplan.service.SubjectLessonLocalServiceUtil;
import info.diit.portal.lessonplan.service.TopicLocalServiceUtil;

/**
 * @author Brian Wing Shun Chan
 */
public class ClpMessageListener extends BaseMessageListener {
	public static String getServletContextName() {
		return ClpSerializer.getServletContextName();
	}

	@Override
	protected void doReceive(Message message) throws Exception {
		String command = message.getString("command");
		String servletContextName = message.getString("servletContextName");

		if (command.equals("undeploy") &&
				servletContextName.equals(getServletContextName())) {
			ChapterLocalServiceUtil.clearService();

			LessonAssessmentLocalServiceUtil.clearService();

			LessonPlanLocalServiceUtil.clearService();

			LessonTopicLocalServiceUtil.clearService();

			SubjectLessonLocalServiceUtil.clearService();

			TopicLocalServiceUtil.clearService();
		}
	}
}
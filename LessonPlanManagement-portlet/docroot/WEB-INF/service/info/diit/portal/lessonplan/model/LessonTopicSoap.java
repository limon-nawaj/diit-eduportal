/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    limon
 * @generated
 */
public class LessonTopicSoap implements Serializable {
	public static LessonTopicSoap toSoapModel(LessonTopic model) {
		LessonTopicSoap soapModel = new LessonTopicSoap();

		soapModel.setLessonTopicId(model.getLessonTopicId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setLessonPlanId(model.getLessonPlanId());
		soapModel.setTopic(model.getTopic());
		soapModel.setClassSequence(model.getClassSequence());
		soapModel.setResource(model.getResource());
		soapModel.setActivities(model.getActivities());

		return soapModel;
	}

	public static LessonTopicSoap[] toSoapModels(LessonTopic[] models) {
		LessonTopicSoap[] soapModels = new LessonTopicSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LessonTopicSoap[][] toSoapModels(LessonTopic[][] models) {
		LessonTopicSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LessonTopicSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LessonTopicSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LessonTopicSoap[] toSoapModels(List<LessonTopic> models) {
		List<LessonTopicSoap> soapModels = new ArrayList<LessonTopicSoap>(models.size());

		for (LessonTopic model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LessonTopicSoap[soapModels.size()]);
	}

	public LessonTopicSoap() {
	}

	public long getPrimaryKey() {
		return _lessonTopicId;
	}

	public void setPrimaryKey(long pk) {
		setLessonTopicId(pk);
	}

	public long getLessonTopicId() {
		return _lessonTopicId;
	}

	public void setLessonTopicId(long lessonTopicId) {
		_lessonTopicId = lessonTopicId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getLessonPlanId() {
		return _lessonPlanId;
	}

	public void setLessonPlanId(long lessonPlanId) {
		_lessonPlanId = lessonPlanId;
	}

	public long getTopic() {
		return _topic;
	}

	public void setTopic(long topic) {
		_topic = topic;
	}

	public long getClassSequence() {
		return _classSequence;
	}

	public void setClassSequence(long classSequence) {
		_classSequence = classSequence;
	}

	public String getResource() {
		return _resource;
	}

	public void setResource(String resource) {
		_resource = resource;
	}

	public String getActivities() {
		return _activities;
	}

	public void setActivities(String activities) {
		_activities = activities;
	}

	private long _lessonTopicId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _lessonPlanId;
	private long _topic;
	private long _classSequence;
	private String _resource;
	private String _activities;
}
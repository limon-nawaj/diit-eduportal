/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.lessonplan.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.lessonplan.model.LessonAssessment;

/**
 * The persistence interface for the lesson assessment service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see LessonAssessmentPersistenceImpl
 * @see LessonAssessmentUtil
 * @generated
 */
public interface LessonAssessmentPersistence extends BasePersistence<LessonAssessment> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LessonAssessmentUtil} to access the lesson assessment persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the lesson assessment in the entity cache if it is enabled.
	*
	* @param lessonAssessment the lesson assessment
	*/
	public void cacheResult(
		info.diit.portal.lessonplan.model.LessonAssessment lessonAssessment);

	/**
	* Caches the lesson assessments in the entity cache if it is enabled.
	*
	* @param lessonAssessments the lesson assessments
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.lessonplan.model.LessonAssessment> lessonAssessments);

	/**
	* Creates a new lesson assessment with the primary key. Does not add the lesson assessment to the database.
	*
	* @param lessonAssessmentId the primary key for the new lesson assessment
	* @return the new lesson assessment
	*/
	public info.diit.portal.lessonplan.model.LessonAssessment create(
		long lessonAssessmentId);

	/**
	* Removes the lesson assessment with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonAssessmentId the primary key of the lesson assessment
	* @return the lesson assessment that was removed
	* @throws info.diit.portal.lessonplan.NoSuchLessonAssessmentException if a lesson assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.LessonAssessment remove(
		long lessonAssessmentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchLessonAssessmentException;

	public info.diit.portal.lessonplan.model.LessonAssessment updateImpl(
		info.diit.portal.lessonplan.model.LessonAssessment lessonAssessment,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lesson assessment with the primary key or throws a {@link info.diit.portal.lessonplan.NoSuchLessonAssessmentException} if it could not be found.
	*
	* @param lessonAssessmentId the primary key of the lesson assessment
	* @return the lesson assessment
	* @throws info.diit.portal.lessonplan.NoSuchLessonAssessmentException if a lesson assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.LessonAssessment findByPrimaryKey(
		long lessonAssessmentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchLessonAssessmentException;

	/**
	* Returns the lesson assessment with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param lessonAssessmentId the primary key of the lesson assessment
	* @return the lesson assessment, or <code>null</code> if a lesson assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.LessonAssessment fetchByPrimaryKey(
		long lessonAssessmentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the lesson assessments where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @return the matching lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.LessonAssessment> findByLessonPlan(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lesson assessments where lessonPlanId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param lessonPlanId the lesson plan ID
	* @param start the lower bound of the range of lesson assessments
	* @param end the upper bound of the range of lesson assessments (not inclusive)
	* @return the range of matching lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.LessonAssessment> findByLessonPlan(
		long lessonPlanId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lesson assessments where lessonPlanId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param lessonPlanId the lesson plan ID
	* @param start the lower bound of the range of lesson assessments
	* @param end the upper bound of the range of lesson assessments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.LessonAssessment> findByLessonPlan(
		long lessonPlanId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first lesson assessment in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson assessment
	* @throws info.diit.portal.lessonplan.NoSuchLessonAssessmentException if a matching lesson assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.LessonAssessment findByLessonPlan_First(
		long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchLessonAssessmentException;

	/**
	* Returns the first lesson assessment in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson assessment, or <code>null</code> if a matching lesson assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.LessonAssessment fetchByLessonPlan_First(
		long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last lesson assessment in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson assessment
	* @throws info.diit.portal.lessonplan.NoSuchLessonAssessmentException if a matching lesson assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.LessonAssessment findByLessonPlan_Last(
		long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchLessonAssessmentException;

	/**
	* Returns the last lesson assessment in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson assessment, or <code>null</code> if a matching lesson assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.LessonAssessment fetchByLessonPlan_Last(
		long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lesson assessments before and after the current lesson assessment in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonAssessmentId the primary key of the current lesson assessment
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lesson assessment
	* @throws info.diit.portal.lessonplan.NoSuchLessonAssessmentException if a lesson assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.lessonplan.model.LessonAssessment[] findByLessonPlan_PrevAndNext(
		long lessonAssessmentId, long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.lessonplan.NoSuchLessonAssessmentException;

	/**
	* Returns all the lesson assessments.
	*
	* @return the lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.LessonAssessment> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lesson assessments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson assessments
	* @param end the upper bound of the range of lesson assessments (not inclusive)
	* @return the range of lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.LessonAssessment> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lesson assessments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson assessments
	* @param end the upper bound of the range of lesson assessments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.lessonplan.model.LessonAssessment> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the lesson assessments where lessonPlanId = &#63; from the database.
	*
	* @param lessonPlanId the lesson plan ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByLessonPlan(long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the lesson assessments from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lesson assessments where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @return the number of matching lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public int countByLessonPlan(long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lesson assessments.
	*
	* @return the number of lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
create index IX_298D8DB on EduPortal_lessonplan_Chapter (chapterId, sequence);
create index IX_30130216 on EduPortal_lessonplan_Chapter (companyId);
create index IX_2A6C2B45 on EduPortal_lessonplan_Chapter (subjectId);

create index IX_4A7AB6F1 on EduPortal_lessonplan_LessonAssessment (lessonPlanId);

create index IX_B6F88CB4 on EduPortal_lessonplan_LessonPlan (subject);
create index IX_B36FE9E2 on EduPortal_lessonplan_LessonPlan (userId);
create index IX_7FE7CCC9 on EduPortal_lessonplan_LessonPlan (userId, organization);

create index IX_70B536CC on EduPortal_lessonplan_LessonTopic (lessonPlanId);
create index IX_14B79FD3 on EduPortal_lessonplan_LessonTopic (lessonPlanId, topic);
create index IX_E5A50543 on EduPortal_lessonplan_LessonTopic (topic);

create index IX_18627CDB on EduPortal_lessonplan_SubjectLesson (batchId, subjectId);
create index IX_9E70F0AB on EduPortal_lessonplan_SubjectLesson (organizationId);
create index IX_89F46DE5 on EduPortal_lessonplan_SubjectLesson (organizationId, userId);

create index IX_F6253C84 on EduPortal_lessonplan_Topic (chapterId);
create index IX_BA0464B4 on EduPortal_lessonplan_Topic (companyId);
create index IX_F7D70721 on EduPortal_lessonplan_Topic (parentTopic);
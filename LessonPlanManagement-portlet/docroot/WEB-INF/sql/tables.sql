create table EduPortal_lessonplan_Chapter (
	chapterId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	sequence LONG,
	name VARCHAR(75) null,
	subjectId LONG,
	note VARCHAR(75) null
);

create table EduPortal_lessonplan_LessonAssessment (
	lessonAssessmentId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	lessonPlanId LONG,
	assessmentType LONG,
	number_ LONG
);

create table EduPortal_lessonplan_LessonPlan (
	lessonPlanId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	title VARCHAR(75) null,
	organization LONG,
	subject LONG,
	planDate DATE null,
	totalClass LONG,
	totalDuration LONG,
	objective VARCHAR(75) null
);

create table EduPortal_lessonplan_LessonTopic (
	lessonTopicId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	lessonPlanId LONG,
	topic LONG,
	classSequence LONG,
	resource VARCHAR(75) null,
	activities VARCHAR(75) null
);

create table EduPortal_lessonplan_SubjectLesson (
	SubjectLessonId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	batchId LONG,
	subjectId LONG,
	lessonPlanId LONG
);

create table EduPortal_lessonplan_Topic (
	topicId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	chapterId LONG,
	topic VARCHAR(75) null,
	parentTopic LONG,
	time_ LONG,
	note VARCHAR(75) null
);
package info.diit.portal.communicationRecord.enums;

public enum Media {

	PHONE(1, "Phone"),
	EMAIL(2, "Email"),
	LETTER(3, "Letter"),
	OTHER(4, "Other");
	private int key;
	private String value;
	
	private Media(int key, String value){
		this.key = key;
		this.value = value;
	}
	
	public int getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return value;
	}
	
	public static Media getMedia(int key){
		Media medias[] = Media.values();
		for (Media media : medias) {
			if (media.key==key) {
				return media;
			}
		}
		return null;
	}
}

package info.diit.portal.communicationRecord.enums;

public enum Subject {

//	(Attendance/Payment/Exam/Other)
	ATTENDANCE(1, "Attendance"),
	PAYMENT(2, "Payment"),
	EXAM(3, "Exam"),
	OTHER(4, "Other");
	private int key;
	private String value;
	
	private Subject(int key, String value){
		this.key = key;
		this.value = value;
	}
	public int getKey() {
		return key;
	}
	public String getValue() {
		return value;
	}
	@Override
	public String toString() {
		return value;
	}
	
	public static Subject getSubject(int key){
		Subject subjects[] = Subject.values();
		for (Subject subject : subjects) {
			if (subject.key==key) {
				return subject;
			}
		}
		return null;
	}
	
}

/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.communicationRecord.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.communicationRecord.model.CommunicationStudentRecord;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing CommunicationStudentRecord in entity cache.
 *
 * @author Limon
 * @see CommunicationStudentRecord
 * @generated
 */
public class CommunicationStudentRecordCacheModel implements CacheModel<CommunicationStudentRecord>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{CommunicationStudentRecorId=");
		sb.append(CommunicationStudentRecorId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", communicationBy=");
		sb.append(communicationBy);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", communicationRecordId=");
		sb.append(communicationRecordId);
		sb.append(", studentId=");
		sb.append(studentId);
		sb.append("}");

		return sb.toString();
	}

	public CommunicationStudentRecord toEntityModel() {
		CommunicationStudentRecordImpl communicationStudentRecordImpl = new CommunicationStudentRecordImpl();

		communicationStudentRecordImpl.setCommunicationStudentRecorId(CommunicationStudentRecorId);
		communicationStudentRecordImpl.setCompanyId(companyId);
		communicationStudentRecordImpl.setOrganizationId(organizationId);
		communicationStudentRecordImpl.setCommunicationBy(communicationBy);

		if (userName == null) {
			communicationStudentRecordImpl.setUserName(StringPool.BLANK);
		}
		else {
			communicationStudentRecordImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			communicationStudentRecordImpl.setCreateDate(null);
		}
		else {
			communicationStudentRecordImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			communicationStudentRecordImpl.setModifiedDate(null);
		}
		else {
			communicationStudentRecordImpl.setModifiedDate(new Date(
					modifiedDate));
		}

		communicationStudentRecordImpl.setCommunicationRecordId(communicationRecordId);
		communicationStudentRecordImpl.setStudentId(studentId);

		communicationStudentRecordImpl.resetOriginalValues();

		return communicationStudentRecordImpl;
	}

	public long CommunicationStudentRecorId;
	public long companyId;
	public long organizationId;
	public long communicationBy;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long communicationRecordId;
	public long studentId;
}
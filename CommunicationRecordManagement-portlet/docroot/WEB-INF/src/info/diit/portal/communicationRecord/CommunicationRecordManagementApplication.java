package info.diit.portal.communicationRecord;

import info.diit.portal.batch.model.Batch;
import info.diit.portal.batch.service.BatchLocalServiceUtil;
import info.diit.portal.batch.subject.model.BatchSubject;
import info.diit.portal.batch.subject.service.BatchSubjectLocalServiceUtil;
import info.diit.portal.communicationRecord.dto.BatchDto;
import info.diit.portal.communicationRecord.dto.CampusDto;
import info.diit.portal.communicationRecord.dto.CommunicationDto;
import info.diit.portal.communicationRecord.dto.StudentDto;
import info.diit.portal.communicationRecord.enums.CommunicationWith;
import info.diit.portal.communicationRecord.enums.Media;
import info.diit.portal.communicationRecord.enums.Subject;
import info.diit.portal.communicationRecord.model.CommunicationRecord;
import info.diit.portal.communicationRecord.model.CommunicationStudentRecord;
import info.diit.portal.communicationRecord.model.impl.CommunicationRecordImpl;
import info.diit.portal.communicationRecord.model.impl.CommunicationStudentRecordImpl;
import info.diit.portal.communicationRecord.service.CommunicationRecordLocalServiceUtil;
import info.diit.portal.communicationRecord.service.CommunicationStudentRecordLocalServiceUtil;
import info.diit.portal.student.service.model.BatchStudent;
import info.diit.portal.student.service.model.Student;
import info.diit.portal.student.service.service.BatchStudentLocalServiceUtil;
import info.diit.portal.student.service.service.StudentLocalServiceUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.client.ComputedStyle;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Component.Event;
import com.vaadin.ui.Component.Listener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class CommunicationRecordManagementApplication extends Application implements PortletRequestListener {

	private Window window;
	private ThemeDisplay themeDisplay;
	
	private final static String COLUMN_STUDENT_ID = "id";
	private final static String COLUMN_STUDENT_NAME = "name";
	private final static String COLUMN_CHECKBOX = "checkBox";
	
	
//	communication list
	private final static String COLUMN_BATCH = "batch";
	private final static String COLUMN_MEDIA = "media";
	private final static String COLUMN_SUBJECT = "subject";
	private final static String COLUMN_STUDENT = "student";
	private final static String COLUMN_DATE = "date";
	private final static String COLUMN_COMMUNICATION_WITH = "communicationWith";
	private final static String COLUMN_DETAILS = "details";
	
	private List<Organization> userOrganizationList;
	
	private List<CampusDto> campusDtoList;
	private List<BatchDto> batchList;
	private List<BatchDto> recordBatchList;
	
	private final static String DATE_FORMAT = "dd/MM/yyyy";
	SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
	
	private CommunicationRecord communicationRecord;
	private CommunicationStudentRecord studentRecord;
	
	public void init() {
		window = new Window();

		setMainWindow(window);
		
		try {
			userOrganizationList = themeDisplay.getUser().getOrganizations();
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		loadUserOrganization();

		window.addComponent(tabSheet());
		if (userOrganizationList.size()==1) {
			loadBatch();
			batchComboBox();
			loadRecodBatch();
			groupBatchBox();
//			individualBatchBox();
		}
		loadCommunication();
	}

	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}
	
	private TabSheet tab;
	
	private TabSheet tabSheet(){
		tab = new TabSheet();
		tab.addTab(mainLayout(), "New Communication");
		tab.addTab(communicationList(), "Communication List");
		return tab;
	}
	
	private GridLayout mainLayout;
	private ComboBox campusComboBox;
	private ComboBox batchComboBox;
	private DateField dateField;
	private ComboBox communicationWithComboBox;
	private TextField communicationByField;
	private ComboBox mediaComboBox;
	private ComboBox subjectComboBox;
	private TextArea detailsArea;
	
	private Table studentTable;
	private BeanItemContainer<StudentDto> studentContainer;
	
	private Table communicationTable;
	private BeanItemContainer<CommunicationDto> communicationContainer;
	
	/*private Table individualCommunicationTable;
	private BeanItemContainer<CommunicationDto> individualCommunicationContainer;*/
	
	private CheckBox studentCheckBox;
	
	private GridLayout mainLayout(){
		mainLayout = new GridLayout(8, 9);
		mainLayout.setSpacing(true);
		mainLayout.setWidth("100%");
		
		campusComboBox = new ComboBox("Campus");
		campusComboBox.setImmediate(true);
		campusComboBox.setWidth("100%");
		
		if (campusDtoList!=null) {
			for (CampusDto campus : campusDtoList) {
				campusComboBox.addItem(campus);
			}
		}
		
		batchComboBox = new ComboBox("Batch");
		batchComboBox.setImmediate(true);
		batchComboBox.setWidth("100%");
		
		campusComboBox.addListener(new Listener() {
			
			@Override
			public void componentEvent(Event event) {
				loadBatch();
				batchComboBox();
			}
		});
		
		batchComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				loadStudent();
			}
		});
		
		dateField = new DateField("Date");
		dateField.setImmediate(true);
		dateField.setWidth("100%");
		dateField.setDateFormat(DATE_FORMAT);
		dateField.setValue(new Date());
		dateField.setResolution(DateField.RESOLUTION_DAY);
		
		communicationWithComboBox = new ComboBox("Communication With");
		communicationWithComboBox.setImmediate(true);
		communicationWithComboBox.setWidth("100%");
		communicationWithComboBox.setRequired(true);
		
		for (CommunicationWith communicationWith : CommunicationWith.values()) {
			communicationWithComboBox.addItem(communicationWith);
		}
		
		communicationByField = new TextField("Communication By");
		communicationByField.setValue(themeDisplay.getUser().getFullName());
		communicationByField.setReadOnly(true);
		communicationByField.setWidth("100%");
		
		mediaComboBox = new ComboBox("Communication Media");
		mediaComboBox.setImmediate(true);
		mediaComboBox.setWidth("100%");
		mediaComboBox.setRequired(true);
		
		for (Media media : Media.values()) {
			mediaComboBox.addItem(media);
		}
		
		subjectComboBox = new ComboBox("Subject");
		subjectComboBox.setImmediate(true);
		subjectComboBox.setWidth("100%");
		subjectComboBox.setRequired(true);
		
		for (Subject subject : Subject.values()) {
			subjectComboBox.addItem(subject);
		}
		
		detailsArea = new TextArea("Details");
		detailsArea.setWidth("100%");
		detailsArea.setRequired(true);
		
		studentContainer = new BeanItemContainer<StudentDto>(StudentDto.class);
		studentTable = new Table("Student List", studentContainer);
		studentTable.setWidth("100%");
		studentTable.setHeight("100%");
		
		studentTable.setColumnHeader(COLUMN_CHECKBOX, "");
		studentTable.setColumnHeader(COLUMN_STUDENT_ID, "Student ID");
		studentTable.setColumnHeader(COLUMN_STUDENT_NAME, "Student Name");
		
		studentTable.setVisibleColumns(new String[]{COLUMN_CHECKBOX, COLUMN_STUDENT_ID, COLUMN_STUDENT_NAME});
		
		studentTable.setColumnExpandRatio(COLUMN_STUDENT_NAME, 7);
		studentTable.setColumnExpandRatio(COLUMN_CHECKBOX, 1);
		studentTable.setColumnExpandRatio(COLUMN_STUDENT_ID, 2);
		studentTable.setEditable(true);
		studentTable.setSelectable(true);
		
		
		studentTable.setTableFieldFactory(new DefaultFieldFactory(){
			
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				if (propertyId.equals(COLUMN_CHECKBOX)) {
					CheckBox field = new CheckBox();
					field.setWidth("100%");
					field.setReadOnly(false);
					return field;
				}
				if (propertyId.equals(COLUMN_STUDENT_ID)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				if (propertyId.equals(COLUMN_STUDENT_NAME)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		Label spacer = new Label();
		Button saveButton = new Button("Save");
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (communicationRecord==null) {
					communicationRecord = new CommunicationRecordImpl();
					communicationRecord.setNew(true);
				}
				
				communicationRecord.setCompanyId(themeDisplay.getCompany().getCompanyId());
				communicationRecord.setCommunicationBy(themeDisplay.getUser().getUserId());
				communicationRecord.setUserName(themeDisplay.getUser().getFullName());
				
				CampusDto campus = (CampusDto) campusComboBox.getValue();
				if (userOrganizationList.size()>1 && campus!=null) {
					communicationRecord.setOrganizationId(campus.getId());
				}else{
					try {
						communicationRecord.setOrganizationId(themeDisplay.getLayout().getGroup().getOrganizationId());
					} catch (PortalException e) {
						e.printStackTrace();
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}
				
				BatchDto batch = (BatchDto) batchComboBox.getValue();
				if (batch!=null) {
					communicationRecord.setBatch(batch.getId());
				}
				
				Date comDate = (Date) dateField.getValue();
				comDate.setHours(0);
				comDate.setMinutes(0);
				comDate.setSeconds(0);
				if (comDate!=null) {
					communicationRecord.setDate(comDate);
				}
				
				CommunicationWith comWith = (CommunicationWith) communicationWithComboBox.getValue();
				if (comWith!=null) {
					communicationRecord.setCommunicationWith(comWith.getKey());
				}else{
					window.showNotification("Please select a person type", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				Media media = (Media) mediaComboBox.getValue();
				if (media!=null) {
					communicationRecord.setMedia(media.getKey());
				}else{
					window.showNotification("Select a media", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				Subject subject = (Subject) subjectComboBox.getValue();
				if (subject!=null) {
					communicationRecord.setSubject(subject.getKey());
				}else{
					window.showNotification("Select a subject!", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				String details = detailsArea.getValue().toString();
				if (details.equals("") || details.equals(null)) {
					window.showNotification("Details cannot be empty", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}else{
					communicationRecord.setDetails(details);
				}
				
				int count = 0;
				
				if (studentContainer!=null) {
					for (int i = 0; i < studentContainer.size(); i++) {
						StudentDto student = studentContainer.getIdByIndex(i);
						if (student.getCheckBox()==true) {
							count++;
						}
					}
				}
				
				if (count==1) {
					communicationRecord.setStatus(0);
				}else if(count>1){
					communicationRecord.setStatus(1);
				}
				
				try {
					if (communicationRecord.isNew()) {
						if (count==0) {
							window.showNotification("No student selected", Window.Notification.TYPE_ERROR_MESSAGE);
						}else{
							CommunicationRecordLocalServiceUtil.addCommunicationRecord(communicationRecord);
							saveStudent(communicationRecord);
							window.showNotification("Record saved successfully");
						}
					}else{
						if (count==0) {
							window.showNotification("No student is selected", Window.Notification.TYPE_ERROR_MESSAGE);
						}else{
							CommunicationRecordLocalServiceUtil.updateCommunicationRecord(communicationRecord);
							saveStudent(communicationRecord);
							window.showNotification("Record updated successfully");
						}
					}
					loadCommunication();
				} catch (SystemException e) {
					e.printStackTrace();
				}
				
			}
		});
		
		Button resetButton = new Button("Reset");
		resetButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				clear();
			}
		});
		
		HorizontalLayout rowLayout = new HorizontalLayout();
		rowLayout.setSpacing(true);
		rowLayout.setWidth("100%");
		rowLayout.addComponent(spacer);
		rowLayout.addComponent(saveButton);
		rowLayout.addComponent(resetButton);
		rowLayout.setExpandRatio(spacer, 1);
		
		if (userOrganizationList.size()>1) {
			mainLayout.addComponent(campusComboBox, 0, 0, 2, 0);
			mainLayout.addComponent(batchComboBox, 0, 1, 2, 1);
			mainLayout.addComponent(dateField, 0, 2, 2, 2);
			mainLayout.addComponent(communicationWithComboBox, 0, 3, 2, 3);
			mainLayout.addComponent(mediaComboBox, 0, 4, 2, 4);
			mainLayout.addComponent(subjectComboBox, 0, 5, 2, 5);
			mainLayout.addComponent(communicationByField, 0, 6, 2, 6);
			mainLayout.addComponent(detailsArea, 0, 7, 2, 7);
			
			mainLayout.addComponent(studentTable, 4, 0, 7, 7);
			mainLayout.addComponent(rowLayout, 0, 8, 7, 8);
		}else{
			mainLayout.addComponent(batchComboBox, 0, 0, 2, 0);
			mainLayout.addComponent(dateField, 0, 1, 2, 1);
			mainLayout.addComponent(communicationWithComboBox, 0, 2, 2, 2);
			mainLayout.addComponent(mediaComboBox, 0, 3, 2, 3);
			mainLayout.addComponent(subjectComboBox, 0, 4, 2, 4);
			mainLayout.addComponent(communicationByField, 0, 5, 2, 5);
			mainLayout.addComponent(detailsArea, 0, 6, 2, 6);
			
			mainLayout.addComponent(studentTable, 4, 0, 7, 6);
			mainLayout.addComponent(rowLayout, 0, 7, 7, 7);
		}
		
		return mainLayout;
	}
	
	private void saveStudent(CommunicationRecord communicationRecord){
		
		if (studentRecord==null) {
			studentRecord = new CommunicationStudentRecordImpl();
			studentRecord.setNew(true);
		}
		
		try {
			List<CommunicationStudentRecord> studentRecordList = CommunicationStudentRecordLocalServiceUtil.findCommunicationRecord(communicationRecord.getCommunicationRecordId());
			if (studentRecordList!=null) {
				for (CommunicationStudentRecord communicationStudentRecord : studentRecordList) {
					CommunicationStudentRecordLocalServiceUtil.deleteCommunicationStudentRecord(communicationStudentRecord);
				}
			}
			
			studentRecord.setCompanyId(themeDisplay.getCompany().getCompanyId());
			studentRecord.setUserName(themeDisplay.getUser().getFullName());
			studentRecord.setCommunicationBy(themeDisplay.getUser().getUserId());
			CampusDto campus = (CampusDto) campusComboBox.getValue();
			if (userOrganizationList.size()>1 && campus!=null) {
				studentRecord.setOrganizationId(campus.getId());
			}else{
				try {
					studentRecord.setOrganizationId(themeDisplay.getLayout().getGroup().getOrganizationId());
				} catch (PortalException e) {
					e.printStackTrace();
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
			
			if (studentContainer!=null) {
				for (int i = 0; i < studentContainer.size(); i++) {
					StudentDto studentDto = studentContainer.getIdByIndex(i);
					
					if (studentDto.getCheckBox()==true) {
						studentRecord.setStudentId(studentDto.getId());
						studentRecord.setCommunicationRecordId(communicationRecord.getCommunicationRecordId());
						CommunicationStudentRecordLocalServiceUtil.addCommunicationStudentRecord(studentRecord);
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private ComboBox communicationCampusBox;
	private ComboBox communicationBatchBox;
	private DateField communicationDate;
	
	private GridLayout communicationList(){
		GridLayout communicationListLayout = new GridLayout(13, 6);
		communicationListLayout.setWidth("100%");
		communicationListLayout.setSpacing(true);
		
		communicationCampusBox = new ComboBox("Campus");
		communicationCampusBox.setImmediate(true);
		communicationCampusBox.setWidth("100%");
		
		if (campusDtoList!=null) {
			for (CampusDto campus : campusDtoList) {
				communicationCampusBox.addItem(campus);
			}
		}
		
		communicationDate = new DateField("Date");
		communicationDate.setImmediate(true);
		communicationDate.setWidth("100%");
		
		communicationBatchBox = new ComboBox("Batch");
		communicationBatchBox.setImmediate(true);
		communicationBatchBox.setWidth("100%");
		
				
		communicationContainer = new BeanItemContainer<CommunicationDto>(CommunicationDto.class);
		communicationTable = new Table("Communication List by Batch Student", communicationContainer){
			@Override
			protected String formatPropertyValue(Object rowId, Object colId,
					com.vaadin.data.Property property) {
				if (property.getType()==Date.class) {
					return dateFormat.format(property.getValue());
				}
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		communicationTable.setWidth("100%");
		communicationTable.setSelectable(true);
		
		communicationTable.setColumnHeader(COLUMN_BATCH, "Batch");
		communicationTable.setColumnHeader(COLUMN_STUDENT, "Student");
		communicationTable.setColumnHeader(COLUMN_MEDIA, "Media");
		communicationTable.setColumnHeader(COLUMN_DATE, "Date");
		communicationTable.setColumnHeader(COLUMN_SUBJECT, "Subject");
		communicationTable.setColumnHeader(COLUMN_COMMUNICATION_WITH, "Communication With");
		communicationTable.setColumnHeader(COLUMN_DETAILS, "Details");
		
		communicationTable.setVisibleColumns(new String[]{COLUMN_BATCH, COLUMN_STUDENT, COLUMN_MEDIA, COLUMN_DATE, COLUMN_SUBJECT, COLUMN_DETAILS});
		
		communicationTable.setColumnExpandRatio(COLUMN_BATCH, 1);
		communicationTable.setColumnExpandRatio(COLUMN_STUDENT, 2);
		communicationTable.setColumnExpandRatio(COLUMN_MEDIA, 1);
		communicationTable.setColumnExpandRatio(COLUMN_DATE, 1);
		communicationTable.setColumnExpandRatio(COLUMN_SUBJECT, 1);
		communicationTable.setColumnExpandRatio(COLUMN_DETAILS, 3);
		
		communicationCampusBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				loadRecodBatch();
				groupBatchBox();
//				individualBatchBox();
				if (communicationContainer!=null) {
					communicationContainer.removeAllItems();
				}
				loadCommunication();
			}
		});
		
		communicationBatchBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				BatchDto batch = (BatchDto) communicationBatchBox.getValue();
								
				if (batch!=null) {
					communicationTable.setVisibleColumns(new String[]{COLUMN_MEDIA, COLUMN_STUDENT, COLUMN_DATE, COLUMN_SUBJECT, COLUMN_DETAILS});
				}else{
					communicationTable.setColumnHeader(COLUMN_BATCH, "Batch");
					communicationTable.setVisibleColumns(new String[]{COLUMN_BATCH, COLUMN_STUDENT, COLUMN_MEDIA, COLUMN_DATE, COLUMN_SUBJECT, COLUMN_DETAILS});
				}
				loadCommunication();
			}
		});
		
		Label spacer = new Label();
		Button editButton = new Button("Edit");
		editButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				CommunicationDto communication = (CommunicationDto) communicationTable.getValue();
				if (communication!=null) {
					tab.setSelectedTab(mainLayout);
					editCommunication(communication.getId());
					communication.getCommunicationRecord().setNew(false);
				}else{
					window.showNotification("Please select a communication", Window.Notification.TYPE_ERROR_MESSAGE);
				}
			}
		});
		
		Button deleteButton = new Button("Delete");
		deleteButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				CommunicationDto communication = (CommunicationDto) communicationTable.getValue();
				if (communication!=null) {
					deleteCommunication(communication.getId());
				}else{
					window.showNotification("Please select a communication", Window.Notification.TYPE_ERROR_MESSAGE);
				}
			}
		});
		
		HorizontalLayout batchButtonLayout = new HorizontalLayout();
		batchButtonLayout.setWidth("100%");
		batchButtonLayout.setSpacing(true);
		batchButtonLayout.addComponent(spacer);
		batchButtonLayout.addComponent(editButton);
		batchButtonLayout.addComponent(deleteButton);
		batchButtonLayout.setExpandRatio(spacer, 1);
				
		if (userOrganizationList.size()>1) {
			communicationListLayout.addComponent(communicationCampusBox, 0, 0, 4, 0);
			communicationListLayout.addComponent(communicationBatchBox, 5, 0, 9, 0);
			communicationListLayout.addComponent(communicationTable, 0, 2, 12, 2);
			communicationListLayout.addComponent(batchButtonLayout, 0, 3, 12, 3);
		}else{
			communicationListLayout.addComponent(communicationBatchBox, 0, 0, 4, 0);
			communicationListLayout.addComponent(communicationTable, 0, 2, 12, 2);
			communicationListLayout.addComponent(batchButtonLayout, 0, 3, 12, 3);
		}
		return communicationListLayout;
	}
	
	private void batchComboBox(){
		if (batchList!=null) {
			for (BatchDto batchDto : batchList) {
				batchComboBox.addItem(batchDto);
			}
		}
	}
	
	private void groupBatchBox(){
		if (recordBatchList!=null) {
			for (BatchDto batchDto : recordBatchList) {
				communicationBatchBox.addItem(batchDto);
			}
		}
	}
	
	private void loadUserOrganization(){
		if (userOrganizationList.size()>1) {
			if (campusDtoList!=null) {
				campusDtoList.clear();
			}else{
				campusDtoList = new ArrayList<CampusDto>();
			}
			
			for (Organization organization : userOrganizationList) {
				CampusDto campus = new CampusDto();
				campus.setId(organization.getOrganizationId());
				campus.setName(organization.getName());
				campusDtoList.add(campus);
			}
		}
	}
	
	private void loadBatch(){
		try {
			List<BatchSubject> batchSubjects = BatchSubjectLocalServiceUtil.findByBatchSubjectTeacher(themeDisplay.getUser().getUserId());
			if (userOrganizationList.size()>0 && batchSubjects!=null) {
				List<Batch> batchs = new ArrayList<Batch>();
				
				List<Batch> recordBatchs = new ArrayList<Batch>();
				
				for (BatchSubject batchSubject : batchSubjects) {
					if (userOrganizationList.size()==1) {
						for (Organization organization : userOrganizationList) {
							if (organization.getOrganizationId()==batchSubject.getOrganizationId()) {
								Batch batch = BatchLocalServiceUtil.fetchBatch(batchSubject.getBatchId());
								batchs.add(batch);
							}
						}
					}else{
						CampusDto campus = (CampusDto) campusComboBox.getValue();
						if (campus!=null) {
							batchComboBox.removeAllItems();
							if (campus.getId()==batchSubject.getOrganizationId()) {
								Batch batch = BatchLocalServiceUtil.fetchBatch(batchSubject.getBatchId());
								batchs.add(batch);
							}
						}else{
							batchComboBox.removeAllItems();
						}
					}
				}
				
				if (batchList!=null) {
					batchList.clear();
				}else{
					batchList = new ArrayList<BatchDto>();
				}
				
				if (batchs!=null) {
					for (Batch batch : batchs) {
						BatchDto batchDto = new BatchDto();
						batchDto.setId(batch.getBatchId());
						batchDto.setTitle(batch.getBatchName());
						batchList.add(batchDto);
					}
				}
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	}
	
	private void loadRecodBatch(){
		try {
			List<BatchSubject> batchSubjects = BatchSubjectLocalServiceUtil.findByBatchSubjectTeacher(themeDisplay.getUser().getUserId());
			if (userOrganizationList.size()>0 && batchSubjects!=null) {
				List<Batch> recordBatchs = new ArrayList<Batch>();
				
				for (BatchSubject batchSubject : batchSubjects) {
					if (userOrganizationList.size()==1) {
						for (Organization organization : userOrganizationList) {
							if (organization.getOrganizationId()==batchSubject.getOrganizationId()) {
								Batch batch = BatchLocalServiceUtil.fetchBatch(batchSubject.getBatchId());
								recordBatchs.add(batch);
							}
						}
					}else{
						CampusDto campusDto = (CampusDto) communicationCampusBox.getValue();
						if (campusDto!=null) {
							communicationBatchBox.removeAllItems();
							if (campusDto.getId()==batchSubject.getOrganizationId()) {
								Batch batch = BatchLocalServiceUtil.fetchBatch(batchSubject.getBatchId());
								recordBatchs.add(batch);
							}
						}else{
							communicationBatchBox.removeAllItems();
						}
						
					}
					
				}
				
				if (recordBatchList!=null) {
					recordBatchList.clear();
				}else{
					recordBatchList = new ArrayList<BatchDto>();
				}
				
				if (recordBatchs!=null) {
					for (Batch batch : recordBatchs) {
						BatchDto batchDto = new BatchDto();
						batchDto.setId(batch.getBatchId());
						batchDto.setTitle(batch.getBatchName());
						recordBatchList.add(batchDto);
					}
				}
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	}
	
	private void loadStudent(){
		BatchDto batch = (BatchDto) batchComboBox.getValue();
		if (studentContainer!=null) {
			studentContainer.removeAllItems();
		}
		if (batch!=null) {
			try {
				List<BatchStudent> batchStudentListByBatch = BatchStudentLocalServiceUtil.findByBatch(batch.getId());
				for (BatchStudent batchStudent : batchStudentListByBatch) {
					Student student = StudentLocalServiceUtil.fetchStudent(batchStudent.getStudentId());
					StudentDto studentDto = new StudentDto();
					studentDto.setCheckBox(false);
					studentDto.setId(student.getStudentId());
					studentDto.setName(student.getName());
					studentContainer.addBean(studentDto);
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
	}

	private void loadCommunication(){
		
		CampusDto campus = (CampusDto) communicationCampusBox.getValue();
		BatchDto batch = (BatchDto) communicationBatchBox.getValue();
		
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(CommunicationRecord.class);
		List<CommunicationRecord> communicationList = null;
		
		if (communicationContainer!=null) {
			communicationContainer.removeAllItems();
		}
		
		try {
			if (userOrganizationList.size()==1) {
				if(batch!=null){
					dynamicQuery.add(PropertyFactoryUtil.forName("organizationId").eq(themeDisplay.getLayout().getGroup().getOrganizationId()))
								.add(PropertyFactoryUtil.forName("communicationBy").eq(themeDisplay.getUser().getUserId()))
								.add(PropertyFactoryUtil.forName("batch").eq(batch.getId()));
					communicationList = CommunicationRecordLocalServiceUtil.dynamicQuery(dynamicQuery);
				}else{
					dynamicQuery.add(PropertyFactoryUtil.forName("organizationId").eq(themeDisplay.getLayout().getGroup().getOrganizationId()))
					.add(PropertyFactoryUtil.forName("communicationBy").eq(themeDisplay.getUser().getUserId()));
					communicationList = CommunicationRecordLocalServiceUtil.dynamicQuery(dynamicQuery);
				}
			}else if(userOrganizationList.size()>1){
				if (campus!=null && batch==null) {
					dynamicQuery.add(PropertyFactoryUtil.forName("organizationId").eq(campus.getId()))
								.add(PropertyFactoryUtil.forName("communicationBy").eq(themeDisplay.getUser().getUserId()));
					communicationList = CommunicationRecordLocalServiceUtil.dynamicQuery(dynamicQuery);
				}else if(campus!=null && batch!=null){
					dynamicQuery.add(PropertyFactoryUtil.forName("organizationId").eq(campus.getId()))
								.add(PropertyFactoryUtil.forName("communicationBy").eq(themeDisplay.getUser().getUserId()))
								.add(PropertyFactoryUtil.forName("batch").eq(batch.getId()));
					communicationList = CommunicationRecordLocalServiceUtil.dynamicQuery(dynamicQuery);
				}else{
					dynamicQuery.add(PropertyFactoryUtil.forName("organizationId").eq(themeDisplay.getLayout().getGroup().getOrganizationId()))
					.add(PropertyFactoryUtil.forName("communicationBy").eq(themeDisplay.getUser().getUserId()));
					communicationList = CommunicationRecordLocalServiceUtil.dynamicQuery(dynamicQuery);
				}
			}
			
			if (communicationList!=null) {
				for (CommunicationRecord communicationRecord : communicationList) {
					CommunicationDto communication = new CommunicationDto();
					
					communication.setId(communicationRecord.getCommunicationRecordId());
					
					Batch b = BatchLocalServiceUtil.fetchBatch(communicationRecord.getBatch());
					BatchDto batchDto = new BatchDto();
					if (b!=null) {
						batchDto.setId(b.getBatchId());
						batchDto.setTitle(b.getBatchName());
						communication.setBatch(batchDto);
					}
					
					Media medias[] = Media.values();
					if (medias!=null) {
						for (Media media : medias) {
							if (media.getKey()==communicationRecord.getMedia()) {
								communication.setMedia(media);
							}
						}
					}
					
					communication.setDate(communicationRecord.getDate());
					communication.setDetails(communicationRecord.getDetails());
					
					Subject subjects[] = Subject.values();
					if (subjects!=null) {
						for (Subject subject : subjects) {
							if (subject.getKey()==communicationRecord.getSubject()) {
								communication.setSubject(subject);
							}
						}
					}
					
					if (communicationRecord.getStatus()==1) {
						communicationContainer.addBean(communication);
					}else if (communicationRecord.getStatus()==0) {
						List<CommunicationStudentRecord> studentCommunicationList = CommunicationStudentRecordLocalServiceUtil.findCommunicationRecord(communicationRecord.getCommunicationRecordId());
						for (CommunicationStudentRecord studentRecord : studentCommunicationList) {
							StudentDto studentDto = new StudentDto();
							Student student = StudentLocalServiceUtil.fetchStudent(studentRecord.getStudentId());
							if (student!=null) {
								studentDto.setId(student.getStudentId());
								studentDto.setName(student.getName());
								
								communication.setStudent(studentDto);
							}
							
							communicationContainer.addBean(communication);
						}
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}

	private void editCommunication(long communicationId){
		try {
			communicationRecord = CommunicationRecordLocalServiceUtil.fetchCommunicationRecord(communicationId);
			campusComboBox.setValue(getOrganization(communicationRecord.getOrganizationId()));
			batchComboBox.setValue(getBatch(communicationRecord.getBatch()));
			dateField.setValue(communicationRecord.getDate());
			communicationWithComboBox.setValue(getCommunicationWith(communicationRecord.getCommunicationWith()));
			mediaComboBox.setValue(getMedia(communicationRecord.getMedia()));
			subjectComboBox.setValue(getSubject(communicationRecord.getSubject()));
			detailsArea.setValue(communicationRecord.getDetails());
			
			List<CommunicationStudentRecord> studentList = CommunicationStudentRecordLocalServiceUtil.findCommunicationRecord(communicationId);
			
			if (studentContainer!=null) {
				studentContainer.removeAllItems();
			}
			
			if (communicationRecord.getBatch()>0) {
				List<BatchStudent> batchStudentListByBatch = BatchStudentLocalServiceUtil.findByBatch(communicationRecord.getBatch());
				for (BatchStudent batchStudent : batchStudentListByBatch) {
					Student student = StudentLocalServiceUtil.fetchStudent(batchStudent.getStudentId());
					StudentDto studentDto = new StudentDto();
					
					studentDto.setId(student.getStudentId());
					studentDto.setName(student.getName());
					
					for (CommunicationStudentRecord studentRecord : studentList) {
						if (studentRecord.getStudentId()==student.getStudentId()) {
							studentDto.setCheckBox(true);
							break;
						}else{
							studentDto.setCheckBox(false);
						}
					}
					studentContainer.addBean(studentDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void deleteCommunication(long communicationId){
		try {
			CommunicationRecord communicationRecord = CommunicationRecordLocalServiceUtil.fetchCommunicationRecord(communicationId);
			List<CommunicationStudentRecord> studentList = CommunicationStudentRecordLocalServiceUtil.findCommunicationRecord(communicationRecord.getCommunicationRecordId());
			if (studentList!=null) {
				for (CommunicationStudentRecord communicationStudentRecord : studentList) {
					CommunicationStudentRecordLocalServiceUtil.deleteCommunicationStudentRecord(communicationStudentRecord);
				}
			}
			CommunicationRecordLocalServiceUtil.deleteCommunicationRecord(communicationRecord);
			loadCommunication();
			clear();
			window.showNotification("Communication deleted successfully");
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private CampusDto getOrganization(long organizationId){
		if (campusDtoList!=null) {
			for (CampusDto campus : campusDtoList) {
				if (campus.getId()==organizationId) {
					return campus;
				}
			}
		}
		return null;
	}
	
	private BatchDto getBatch(long batchId){
		if (batchList!=null) {
			for (BatchDto batch : batchList) {
				if (batch.getId()==batchId) {
					return batch;
				}
			}
		}
		return null;
	}
	
	private CommunicationWith getCommunicationWith(long communicationWithId){
		if (CommunicationWith.values()!=null) {
			for (CommunicationWith communicationWith : CommunicationWith.values()) {
				if (communicationWith.getKey()==communicationWithId) {
					return communicationWith;
				}
			}
		}
		return null;
	}
	
	private Media getMedia(long mediaId){
		if (Media.values()!=null) {
			for (Media media : Media.values()) {
				if (media.getKey()==mediaId) {
					return media;
				}
			}
		}
		return null;
	}
	
	private Subject getSubject(long subjectId){
		if (Subject.values()!=null) {
			for (Subject subject : Subject.values()) {
				if (subject.getKey()==subjectId) {
					return subject;
				}
			}
		}
		return null;
	}
	
	private void clear(){
		communicationRecord = null;
		studentRecord = null;
		campusComboBox.setValue(null);
		batchComboBox.setValue(null);
		dateField.setValue(new Date());
		communicationWithComboBox.setValue(null);
		mediaComboBox.setValue(null);
		subjectComboBox.setValue(null);
		detailsArea.setValue("");
	}
}
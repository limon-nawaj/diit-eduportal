package info.diit.portal.communicationRecord.dto;

public class StudentDto {

	private long id;
	private String name;
	private Boolean checkBox;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getCheckBox() {
		return checkBox;
	}
	public void setCheckBox(Boolean checkBox) {
		this.checkBox = checkBox;
	}
	@Override
	public String toString() {
		return getName();
	}
}

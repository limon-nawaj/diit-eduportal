/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.communicationRecord.service.base;

import com.liferay.counter.service.CounterLocalService;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.bean.IdentifiableBean;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.model.PersistedModel;
import com.liferay.portal.service.BaseLocalServiceImpl;
import com.liferay.portal.service.PersistedModelLocalServiceRegistryUtil;
import com.liferay.portal.service.ResourceLocalService;
import com.liferay.portal.service.ResourceService;
import com.liferay.portal.service.UserLocalService;
import com.liferay.portal.service.UserService;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;

import info.diit.portal.communicationRecord.model.CommunicationStudentRecord;
import info.diit.portal.communicationRecord.service.CommunicationRecordLocalService;
import info.diit.portal.communicationRecord.service.CommunicationStudentRecordLocalService;
import info.diit.portal.communicationRecord.service.persistence.CommunicationRecordPersistence;
import info.diit.portal.communicationRecord.service.persistence.CommunicationStudentRecordPersistence;

import java.io.Serializable;

import java.util.List;

import javax.sql.DataSource;

/**
 * The base implementation of the communication student record local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link info.diit.portal.communicationRecord.service.impl.CommunicationStudentRecordLocalServiceImpl}.
 * </p>
 *
 * @author Limon
 * @see info.diit.portal.communicationRecord.service.impl.CommunicationStudentRecordLocalServiceImpl
 * @see info.diit.portal.communicationRecord.service.CommunicationStudentRecordLocalServiceUtil
 * @generated
 */
public abstract class CommunicationStudentRecordLocalServiceBaseImpl
	extends BaseLocalServiceImpl
	implements CommunicationStudentRecordLocalService, IdentifiableBean {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link info.diit.portal.communicationRecord.service.CommunicationStudentRecordLocalServiceUtil} to access the communication student record local service.
	 */

	/**
	 * Adds the communication student record to the database. Also notifies the appropriate model listeners.
	 *
	 * @param communicationStudentRecord the communication student record
	 * @return the communication student record that was added
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public CommunicationStudentRecord addCommunicationStudentRecord(
		CommunicationStudentRecord communicationStudentRecord)
		throws SystemException {
		communicationStudentRecord.setNew(true);

		return communicationStudentRecordPersistence.update(communicationStudentRecord,
			false);
	}

	/**
	 * Creates a new communication student record with the primary key. Does not add the communication student record to the database.
	 *
	 * @param CommunicationStudentRecorId the primary key for the new communication student record
	 * @return the new communication student record
	 */
	public CommunicationStudentRecord createCommunicationStudentRecord(
		long CommunicationStudentRecorId) {
		return communicationStudentRecordPersistence.create(CommunicationStudentRecorId);
	}

	/**
	 * Deletes the communication student record with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param CommunicationStudentRecorId the primary key of the communication student record
	 * @return the communication student record that was removed
	 * @throws PortalException if a communication student record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.DELETE)
	public CommunicationStudentRecord deleteCommunicationStudentRecord(
		long CommunicationStudentRecorId)
		throws PortalException, SystemException {
		return communicationStudentRecordPersistence.remove(CommunicationStudentRecorId);
	}

	/**
	 * Deletes the communication student record from the database. Also notifies the appropriate model listeners.
	 *
	 * @param communicationStudentRecord the communication student record
	 * @return the communication student record that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.DELETE)
	public CommunicationStudentRecord deleteCommunicationStudentRecord(
		CommunicationStudentRecord communicationStudentRecord)
		throws SystemException {
		return communicationStudentRecordPersistence.remove(communicationStudentRecord);
	}

	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(CommunicationStudentRecord.class,
			clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return communicationStudentRecordPersistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return communicationStudentRecordPersistence.findWithDynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return communicationStudentRecordPersistence.findWithDynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	public long dynamicQueryCount(DynamicQuery dynamicQuery)
		throws SystemException {
		return communicationStudentRecordPersistence.countWithDynamicQuery(dynamicQuery);
	}

	public CommunicationStudentRecord fetchCommunicationStudentRecord(
		long CommunicationStudentRecorId) throws SystemException {
		return communicationStudentRecordPersistence.fetchByPrimaryKey(CommunicationStudentRecorId);
	}

	/**
	 * Returns the communication student record with the primary key.
	 *
	 * @param CommunicationStudentRecorId the primary key of the communication student record
	 * @return the communication student record
	 * @throws PortalException if a communication student record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationStudentRecord getCommunicationStudentRecord(
		long CommunicationStudentRecorId)
		throws PortalException, SystemException {
		return communicationStudentRecordPersistence.findByPrimaryKey(CommunicationStudentRecorId);
	}

	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException, SystemException {
		return communicationStudentRecordPersistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns a range of all the communication student records.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of communication student records
	 * @param end the upper bound of the range of communication student records (not inclusive)
	 * @return the range of communication student records
	 * @throws SystemException if a system exception occurred
	 */
	public List<CommunicationStudentRecord> getCommunicationStudentRecords(
		int start, int end) throws SystemException {
		return communicationStudentRecordPersistence.findAll(start, end);
	}

	/**
	 * Returns the number of communication student records.
	 *
	 * @return the number of communication student records
	 * @throws SystemException if a system exception occurred
	 */
	public int getCommunicationStudentRecordsCount() throws SystemException {
		return communicationStudentRecordPersistence.countAll();
	}

	/**
	 * Updates the communication student record in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param communicationStudentRecord the communication student record
	 * @return the communication student record that was updated
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public CommunicationStudentRecord updateCommunicationStudentRecord(
		CommunicationStudentRecord communicationStudentRecord)
		throws SystemException {
		return updateCommunicationStudentRecord(communicationStudentRecord, true);
	}

	/**
	 * Updates the communication student record in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param communicationStudentRecord the communication student record
	 * @param merge whether to merge the communication student record with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	 * @return the communication student record that was updated
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public CommunicationStudentRecord updateCommunicationStudentRecord(
		CommunicationStudentRecord communicationStudentRecord, boolean merge)
		throws SystemException {
		communicationStudentRecord.setNew(false);

		return communicationStudentRecordPersistence.update(communicationStudentRecord,
			merge);
	}

	/**
	 * Returns the communication record local service.
	 *
	 * @return the communication record local service
	 */
	public CommunicationRecordLocalService getCommunicationRecordLocalService() {
		return communicationRecordLocalService;
	}

	/**
	 * Sets the communication record local service.
	 *
	 * @param communicationRecordLocalService the communication record local service
	 */
	public void setCommunicationRecordLocalService(
		CommunicationRecordLocalService communicationRecordLocalService) {
		this.communicationRecordLocalService = communicationRecordLocalService;
	}

	/**
	 * Returns the communication record persistence.
	 *
	 * @return the communication record persistence
	 */
	public CommunicationRecordPersistence getCommunicationRecordPersistence() {
		return communicationRecordPersistence;
	}

	/**
	 * Sets the communication record persistence.
	 *
	 * @param communicationRecordPersistence the communication record persistence
	 */
	public void setCommunicationRecordPersistence(
		CommunicationRecordPersistence communicationRecordPersistence) {
		this.communicationRecordPersistence = communicationRecordPersistence;
	}

	/**
	 * Returns the communication student record local service.
	 *
	 * @return the communication student record local service
	 */
	public CommunicationStudentRecordLocalService getCommunicationStudentRecordLocalService() {
		return communicationStudentRecordLocalService;
	}

	/**
	 * Sets the communication student record local service.
	 *
	 * @param communicationStudentRecordLocalService the communication student record local service
	 */
	public void setCommunicationStudentRecordLocalService(
		CommunicationStudentRecordLocalService communicationStudentRecordLocalService) {
		this.communicationStudentRecordLocalService = communicationStudentRecordLocalService;
	}

	/**
	 * Returns the communication student record persistence.
	 *
	 * @return the communication student record persistence
	 */
	public CommunicationStudentRecordPersistence getCommunicationStudentRecordPersistence() {
		return communicationStudentRecordPersistence;
	}

	/**
	 * Sets the communication student record persistence.
	 *
	 * @param communicationStudentRecordPersistence the communication student record persistence
	 */
	public void setCommunicationStudentRecordPersistence(
		CommunicationStudentRecordPersistence communicationStudentRecordPersistence) {
		this.communicationStudentRecordPersistence = communicationStudentRecordPersistence;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public CounterLocalService getCounterLocalService() {
		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public ResourceLocalService getResourceLocalService() {
		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the resource remote service.
	 *
	 * @return the resource remote service
	 */
	public ResourceService getResourceService() {
		return resourceService;
	}

	/**
	 * Sets the resource remote service.
	 *
	 * @param resourceService the resource remote service
	 */
	public void setResourceService(ResourceService resourceService) {
		this.resourceService = resourceService;
	}

	/**
	 * Returns the resource persistence.
	 *
	 * @return the resource persistence
	 */
	public ResourcePersistence getResourcePersistence() {
		return resourcePersistence;
	}

	/**
	 * Sets the resource persistence.
	 *
	 * @param resourcePersistence the resource persistence
	 */
	public void setResourcePersistence(ResourcePersistence resourcePersistence) {
		this.resourcePersistence = resourcePersistence;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public UserLocalService getUserLocalService() {
		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user remote service.
	 *
	 * @return the user remote service
	 */
	public UserService getUserService() {
		return userService;
	}

	/**
	 * Sets the user remote service.
	 *
	 * @param userService the user remote service
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	public void afterPropertiesSet() {
		PersistedModelLocalServiceRegistryUtil.register("info.diit.portal.communicationRecord.model.CommunicationStudentRecord",
			communicationStudentRecordLocalService);
	}

	public void destroy() {
		PersistedModelLocalServiceRegistryUtil.unregister(
			"info.diit.portal.communicationRecord.model.CommunicationStudentRecord");
	}

	/**
	 * Returns the Spring bean ID for this bean.
	 *
	 * @return the Spring bean ID for this bean
	 */
	public String getBeanIdentifier() {
		return _beanIdentifier;
	}

	/**
	 * Sets the Spring bean ID for this bean.
	 *
	 * @param beanIdentifier the Spring bean ID for this bean
	 */
	public void setBeanIdentifier(String beanIdentifier) {
		_beanIdentifier = beanIdentifier;
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		return _clpInvoker.invokeMethod(name, parameterTypes, arguments);
	}

	protected Class<?> getModelClass() {
		return CommunicationStudentRecord.class;
	}

	protected String getModelClassName() {
		return CommunicationStudentRecord.class.getName();
	}

	/**
	 * Performs an SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) throws SystemException {
		try {
			DataSource dataSource = communicationStudentRecordPersistence.getDataSource();

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(dataSource,
					sql, new int[0]);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(type = CommunicationRecordLocalService.class)
	protected CommunicationRecordLocalService communicationRecordLocalService;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordLocalService.class)
	protected CommunicationStudentRecordLocalService communicationStudentRecordLocalService;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounterLocalService.class)
	protected CounterLocalService counterLocalService;
	@BeanReference(type = ResourceLocalService.class)
	protected ResourceLocalService resourceLocalService;
	@BeanReference(type = ResourceService.class)
	protected ResourceService resourceService;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserLocalService.class)
	protected UserLocalService userLocalService;
	@BeanReference(type = UserService.class)
	protected UserService userService;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private String _beanIdentifier;
	private CommunicationStudentRecordLocalServiceClpInvoker _clpInvoker = new CommunicationStudentRecordLocalServiceClpInvoker();
}
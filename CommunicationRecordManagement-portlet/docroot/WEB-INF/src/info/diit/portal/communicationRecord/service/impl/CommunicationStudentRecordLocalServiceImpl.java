/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.communicationRecord.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import info.diit.portal.communicationRecord.model.CommunicationStudentRecord;
import info.diit.portal.communicationRecord.service.base.CommunicationStudentRecordLocalServiceBaseImpl;
import info.diit.portal.communicationRecord.service.persistence.CommunicationStudentRecordUtil;

/**
 * The implementation of the communication student record local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.communicationRecord.service.CommunicationStudentRecordLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Limon
 * @see info.diit.portal.communicationRecord.service.base.CommunicationStudentRecordLocalServiceBaseImpl
 * @see info.diit.portal.communicationRecord.service.CommunicationStudentRecordLocalServiceUtil
 */
public class CommunicationStudentRecordLocalServiceImpl
	extends CommunicationStudentRecordLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.communicationRecord.service.CommunicationStudentRecordLocalServiceUtil} to access the communication student record local service.
	 */
	public List<CommunicationStudentRecord> findCommunicationRecord(long communicationRecordId) throws SystemException{
		return CommunicationStudentRecordUtil.findByCommunicationRecord(communicationRecordId);
	}
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.communicationRecord.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.communicationRecord.NoSuchCommunicationRecordException;
import info.diit.portal.communicationRecord.model.CommunicationRecord;
import info.diit.portal.communicationRecord.model.impl.CommunicationRecordImpl;
import info.diit.portal.communicationRecord.model.impl.CommunicationRecordModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the communication record service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Limon
 * @see CommunicationRecordPersistence
 * @see CommunicationRecordUtil
 * @generated
 */
public class CommunicationRecordPersistenceImpl extends BasePersistenceImpl<CommunicationRecord>
	implements CommunicationRecordPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CommunicationRecordUtil} to access the communication record persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CommunicationRecordImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMMUNICATIONBY =
		new FinderPath(CommunicationRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationRecordModelImpl.FINDER_CACHE_ENABLED,
			CommunicationRecordImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCommunicationBy",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMMUNICATIONBY =
		new FinderPath(CommunicationRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationRecordModelImpl.FINDER_CACHE_ENABLED,
			CommunicationRecordImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCommunicationBy",
			new String[] { Long.class.getName() },
			CommunicationRecordModelImpl.COMMUNICATIONBY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMMUNICATIONBY = new FinderPath(CommunicationRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationRecordModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCommunicationBy", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CommunicationRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationRecordModelImpl.FINDER_CACHE_ENABLED,
			CommunicationRecordImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CommunicationRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationRecordModelImpl.FINDER_CACHE_ENABLED,
			CommunicationRecordImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CommunicationRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationRecordModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the communication record in the entity cache if it is enabled.
	 *
	 * @param communicationRecord the communication record
	 */
	public void cacheResult(CommunicationRecord communicationRecord) {
		EntityCacheUtil.putResult(CommunicationRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationRecordImpl.class, communicationRecord.getPrimaryKey(),
			communicationRecord);

		communicationRecord.resetOriginalValues();
	}

	/**
	 * Caches the communication records in the entity cache if it is enabled.
	 *
	 * @param communicationRecords the communication records
	 */
	public void cacheResult(List<CommunicationRecord> communicationRecords) {
		for (CommunicationRecord communicationRecord : communicationRecords) {
			if (EntityCacheUtil.getResult(
						CommunicationRecordModelImpl.ENTITY_CACHE_ENABLED,
						CommunicationRecordImpl.class,
						communicationRecord.getPrimaryKey()) == null) {
				cacheResult(communicationRecord);
			}
			else {
				communicationRecord.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all communication records.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CommunicationRecordImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CommunicationRecordImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the communication record.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CommunicationRecord communicationRecord) {
		EntityCacheUtil.removeResult(CommunicationRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationRecordImpl.class, communicationRecord.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CommunicationRecord> communicationRecords) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CommunicationRecord communicationRecord : communicationRecords) {
			EntityCacheUtil.removeResult(CommunicationRecordModelImpl.ENTITY_CACHE_ENABLED,
				CommunicationRecordImpl.class,
				communicationRecord.getPrimaryKey());
		}
	}

	/**
	 * Creates a new communication record with the primary key. Does not add the communication record to the database.
	 *
	 * @param communicationRecordId the primary key for the new communication record
	 * @return the new communication record
	 */
	public CommunicationRecord create(long communicationRecordId) {
		CommunicationRecord communicationRecord = new CommunicationRecordImpl();

		communicationRecord.setNew(true);
		communicationRecord.setPrimaryKey(communicationRecordId);

		return communicationRecord;
	}

	/**
	 * Removes the communication record with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param communicationRecordId the primary key of the communication record
	 * @return the communication record that was removed
	 * @throws info.diit.portal.communicationRecord.NoSuchCommunicationRecordException if a communication record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationRecord remove(long communicationRecordId)
		throws NoSuchCommunicationRecordException, SystemException {
		return remove(Long.valueOf(communicationRecordId));
	}

	/**
	 * Removes the communication record with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the communication record
	 * @return the communication record that was removed
	 * @throws info.diit.portal.communicationRecord.NoSuchCommunicationRecordException if a communication record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CommunicationRecord remove(Serializable primaryKey)
		throws NoSuchCommunicationRecordException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CommunicationRecord communicationRecord = (CommunicationRecord)session.get(CommunicationRecordImpl.class,
					primaryKey);

			if (communicationRecord == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCommunicationRecordException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(communicationRecord);
		}
		catch (NoSuchCommunicationRecordException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CommunicationRecord removeImpl(
		CommunicationRecord communicationRecord) throws SystemException {
		communicationRecord = toUnwrappedModel(communicationRecord);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, communicationRecord);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(communicationRecord);

		return communicationRecord;
	}

	@Override
	public CommunicationRecord updateImpl(
		info.diit.portal.communicationRecord.model.CommunicationRecord communicationRecord,
		boolean merge) throws SystemException {
		communicationRecord = toUnwrappedModel(communicationRecord);

		boolean isNew = communicationRecord.isNew();

		CommunicationRecordModelImpl communicationRecordModelImpl = (CommunicationRecordModelImpl)communicationRecord;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, communicationRecord, merge);

			communicationRecord.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CommunicationRecordModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((communicationRecordModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMMUNICATIONBY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(communicationRecordModelImpl.getOriginalCommunicationBy())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMMUNICATIONBY,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMMUNICATIONBY,
					args);

				args = new Object[] {
						Long.valueOf(communicationRecordModelImpl.getCommunicationBy())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMMUNICATIONBY,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMMUNICATIONBY,
					args);
			}
		}

		EntityCacheUtil.putResult(CommunicationRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationRecordImpl.class, communicationRecord.getPrimaryKey(),
			communicationRecord);

		return communicationRecord;
	}

	protected CommunicationRecord toUnwrappedModel(
		CommunicationRecord communicationRecord) {
		if (communicationRecord instanceof CommunicationRecordImpl) {
			return communicationRecord;
		}

		CommunicationRecordImpl communicationRecordImpl = new CommunicationRecordImpl();

		communicationRecordImpl.setNew(communicationRecord.isNew());
		communicationRecordImpl.setPrimaryKey(communicationRecord.getPrimaryKey());

		communicationRecordImpl.setCommunicationRecordId(communicationRecord.getCommunicationRecordId());
		communicationRecordImpl.setCompanyId(communicationRecord.getCompanyId());
		communicationRecordImpl.setOrganizationId(communicationRecord.getOrganizationId());
		communicationRecordImpl.setCommunicationBy(communicationRecord.getCommunicationBy());
		communicationRecordImpl.setUserName(communicationRecord.getUserName());
		communicationRecordImpl.setCreateDate(communicationRecord.getCreateDate());
		communicationRecordImpl.setModifiedDate(communicationRecord.getModifiedDate());
		communicationRecordImpl.setBatch(communicationRecord.getBatch());
		communicationRecordImpl.setDate(communicationRecord.getDate());
		communicationRecordImpl.setMedia(communicationRecord.getMedia());
		communicationRecordImpl.setSubject(communicationRecord.getSubject());
		communicationRecordImpl.setCommunicationWith(communicationRecord.getCommunicationWith());
		communicationRecordImpl.setDetails(communicationRecord.getDetails());
		communicationRecordImpl.setStatus(communicationRecord.getStatus());

		return communicationRecordImpl;
	}

	/**
	 * Returns the communication record with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the communication record
	 * @return the communication record
	 * @throws com.liferay.portal.NoSuchModelException if a communication record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CommunicationRecord findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the communication record with the primary key or throws a {@link info.diit.portal.communicationRecord.NoSuchCommunicationRecordException} if it could not be found.
	 *
	 * @param communicationRecordId the primary key of the communication record
	 * @return the communication record
	 * @throws info.diit.portal.communicationRecord.NoSuchCommunicationRecordException if a communication record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationRecord findByPrimaryKey(long communicationRecordId)
		throws NoSuchCommunicationRecordException, SystemException {
		CommunicationRecord communicationRecord = fetchByPrimaryKey(communicationRecordId);

		if (communicationRecord == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					communicationRecordId);
			}

			throw new NoSuchCommunicationRecordException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				communicationRecordId);
		}

		return communicationRecord;
	}

	/**
	 * Returns the communication record with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the communication record
	 * @return the communication record, or <code>null</code> if a communication record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CommunicationRecord fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the communication record with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param communicationRecordId the primary key of the communication record
	 * @return the communication record, or <code>null</code> if a communication record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationRecord fetchByPrimaryKey(long communicationRecordId)
		throws SystemException {
		CommunicationRecord communicationRecord = (CommunicationRecord)EntityCacheUtil.getResult(CommunicationRecordModelImpl.ENTITY_CACHE_ENABLED,
				CommunicationRecordImpl.class, communicationRecordId);

		if (communicationRecord == _nullCommunicationRecord) {
			return null;
		}

		if (communicationRecord == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				communicationRecord = (CommunicationRecord)session.get(CommunicationRecordImpl.class,
						Long.valueOf(communicationRecordId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (communicationRecord != null) {
					cacheResult(communicationRecord);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(CommunicationRecordModelImpl.ENTITY_CACHE_ENABLED,
						CommunicationRecordImpl.class, communicationRecordId,
						_nullCommunicationRecord);
				}

				closeSession(session);
			}
		}

		return communicationRecord;
	}

	/**
	 * Returns all the communication records where communicationBy = &#63;.
	 *
	 * @param communicationBy the communication by
	 * @return the matching communication records
	 * @throws SystemException if a system exception occurred
	 */
	public List<CommunicationRecord> findByCommunicationBy(long communicationBy)
		throws SystemException {
		return findByCommunicationBy(communicationBy, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the communication records where communicationBy = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param communicationBy the communication by
	 * @param start the lower bound of the range of communication records
	 * @param end the upper bound of the range of communication records (not inclusive)
	 * @return the range of matching communication records
	 * @throws SystemException if a system exception occurred
	 */
	public List<CommunicationRecord> findByCommunicationBy(
		long communicationBy, int start, int end) throws SystemException {
		return findByCommunicationBy(communicationBy, start, end, null);
	}

	/**
	 * Returns an ordered range of all the communication records where communicationBy = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param communicationBy the communication by
	 * @param start the lower bound of the range of communication records
	 * @param end the upper bound of the range of communication records (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching communication records
	 * @throws SystemException if a system exception occurred
	 */
	public List<CommunicationRecord> findByCommunicationBy(
		long communicationBy, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMMUNICATIONBY;
			finderArgs = new Object[] { communicationBy };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMMUNICATIONBY;
			finderArgs = new Object[] {
					communicationBy,
					
					start, end, orderByComparator
				};
		}

		List<CommunicationRecord> list = (List<CommunicationRecord>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CommunicationRecord communicationRecord : list) {
				if ((communicationBy != communicationRecord.getCommunicationBy())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COMMUNICATIONRECORD_WHERE);

			query.append(_FINDER_COLUMN_COMMUNICATIONBY_COMMUNICATIONBY_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CommunicationRecordModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(communicationBy);

				list = (List<CommunicationRecord>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first communication record in the ordered set where communicationBy = &#63;.
	 *
	 * @param communicationBy the communication by
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching communication record
	 * @throws info.diit.portal.communicationRecord.NoSuchCommunicationRecordException if a matching communication record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationRecord findByCommunicationBy_First(
		long communicationBy, OrderByComparator orderByComparator)
		throws NoSuchCommunicationRecordException, SystemException {
		CommunicationRecord communicationRecord = fetchByCommunicationBy_First(communicationBy,
				orderByComparator);

		if (communicationRecord != null) {
			return communicationRecord;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("communicationBy=");
		msg.append(communicationBy);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCommunicationRecordException(msg.toString());
	}

	/**
	 * Returns the first communication record in the ordered set where communicationBy = &#63;.
	 *
	 * @param communicationBy the communication by
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching communication record, or <code>null</code> if a matching communication record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationRecord fetchByCommunicationBy_First(
		long communicationBy, OrderByComparator orderByComparator)
		throws SystemException {
		List<CommunicationRecord> list = findByCommunicationBy(communicationBy,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last communication record in the ordered set where communicationBy = &#63;.
	 *
	 * @param communicationBy the communication by
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching communication record
	 * @throws info.diit.portal.communicationRecord.NoSuchCommunicationRecordException if a matching communication record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationRecord findByCommunicationBy_Last(
		long communicationBy, OrderByComparator orderByComparator)
		throws NoSuchCommunicationRecordException, SystemException {
		CommunicationRecord communicationRecord = fetchByCommunicationBy_Last(communicationBy,
				orderByComparator);

		if (communicationRecord != null) {
			return communicationRecord;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("communicationBy=");
		msg.append(communicationBy);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCommunicationRecordException(msg.toString());
	}

	/**
	 * Returns the last communication record in the ordered set where communicationBy = &#63;.
	 *
	 * @param communicationBy the communication by
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching communication record, or <code>null</code> if a matching communication record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationRecord fetchByCommunicationBy_Last(
		long communicationBy, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByCommunicationBy(communicationBy);

		List<CommunicationRecord> list = findByCommunicationBy(communicationBy,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the communication records before and after the current communication record in the ordered set where communicationBy = &#63;.
	 *
	 * @param communicationRecordId the primary key of the current communication record
	 * @param communicationBy the communication by
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next communication record
	 * @throws info.diit.portal.communicationRecord.NoSuchCommunicationRecordException if a communication record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationRecord[] findByCommunicationBy_PrevAndNext(
		long communicationRecordId, long communicationBy,
		OrderByComparator orderByComparator)
		throws NoSuchCommunicationRecordException, SystemException {
		CommunicationRecord communicationRecord = findByPrimaryKey(communicationRecordId);

		Session session = null;

		try {
			session = openSession();

			CommunicationRecord[] array = new CommunicationRecordImpl[3];

			array[0] = getByCommunicationBy_PrevAndNext(session,
					communicationRecord, communicationBy, orderByComparator,
					true);

			array[1] = communicationRecord;

			array[2] = getByCommunicationBy_PrevAndNext(session,
					communicationRecord, communicationBy, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CommunicationRecord getByCommunicationBy_PrevAndNext(
		Session session, CommunicationRecord communicationRecord,
		long communicationBy, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COMMUNICATIONRECORD_WHERE);

		query.append(_FINDER_COLUMN_COMMUNICATIONBY_COMMUNICATIONBY_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CommunicationRecordModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(communicationBy);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(communicationRecord);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CommunicationRecord> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the communication records.
	 *
	 * @return the communication records
	 * @throws SystemException if a system exception occurred
	 */
	public List<CommunicationRecord> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the communication records.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of communication records
	 * @param end the upper bound of the range of communication records (not inclusive)
	 * @return the range of communication records
	 * @throws SystemException if a system exception occurred
	 */
	public List<CommunicationRecord> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the communication records.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of communication records
	 * @param end the upper bound of the range of communication records (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of communication records
	 * @throws SystemException if a system exception occurred
	 */
	public List<CommunicationRecord> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CommunicationRecord> list = (List<CommunicationRecord>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_COMMUNICATIONRECORD);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COMMUNICATIONRECORD.concat(CommunicationRecordModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<CommunicationRecord>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<CommunicationRecord>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the communication records where communicationBy = &#63; from the database.
	 *
	 * @param communicationBy the communication by
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCommunicationBy(long communicationBy)
		throws SystemException {
		for (CommunicationRecord communicationRecord : findByCommunicationBy(
				communicationBy)) {
			remove(communicationRecord);
		}
	}

	/**
	 * Removes all the communication records from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (CommunicationRecord communicationRecord : findAll()) {
			remove(communicationRecord);
		}
	}

	/**
	 * Returns the number of communication records where communicationBy = &#63;.
	 *
	 * @param communicationBy the communication by
	 * @return the number of matching communication records
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCommunicationBy(long communicationBy)
		throws SystemException {
		Object[] finderArgs = new Object[] { communicationBy };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMMUNICATIONBY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COMMUNICATIONRECORD_WHERE);

			query.append(_FINDER_COLUMN_COMMUNICATIONBY_COMMUNICATIONBY_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(communicationBy);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMMUNICATIONBY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of communication records.
	 *
	 * @return the number of communication records
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COMMUNICATIONRECORD);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the communication record persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.communicationRecord.model.CommunicationRecord")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CommunicationRecord>> listenersList = new ArrayList<ModelListener<CommunicationRecord>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CommunicationRecord>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CommunicationRecordImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_COMMUNICATIONRECORD = "SELECT communicationRecord FROM CommunicationRecord communicationRecord";
	private static final String _SQL_SELECT_COMMUNICATIONRECORD_WHERE = "SELECT communicationRecord FROM CommunicationRecord communicationRecord WHERE ";
	private static final String _SQL_COUNT_COMMUNICATIONRECORD = "SELECT COUNT(communicationRecord) FROM CommunicationRecord communicationRecord";
	private static final String _SQL_COUNT_COMMUNICATIONRECORD_WHERE = "SELECT COUNT(communicationRecord) FROM CommunicationRecord communicationRecord WHERE ";
	private static final String _FINDER_COLUMN_COMMUNICATIONBY_COMMUNICATIONBY_2 =
		"communicationRecord.communicationBy = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "communicationRecord.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CommunicationRecord exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CommunicationRecord exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CommunicationRecordPersistenceImpl.class);
	private static CommunicationRecord _nullCommunicationRecord = new CommunicationRecordImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CommunicationRecord> toCacheModel() {
				return _nullCommunicationRecordCacheModel;
			}
		};

	private static CacheModel<CommunicationRecord> _nullCommunicationRecordCacheModel =
		new CacheModel<CommunicationRecord>() {
			public CommunicationRecord toEntityModel() {
				return _nullCommunicationRecord;
			}
		};
}
create table EduPortal_CommunicationRecord_CommunicationRecord (
	communicationRecordId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	communicationBy LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	batch LONG,
	date_ DATE null,
	media LONG,
	subject LONG,
	communicationWith LONG,
	details STRING null,
	status LONG
);

create table EduPortal_CommunicationRecord_CommunicationStudentRecor (
	CommunicationStudentRecorId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	communicationRecordId LONG,
	studentId LONG
);

create table EduPortal_CommunicationRecord_CommunicationStudentRecord (
	CommunicationStudentRecorId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	communicationBy LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	communicationRecordId LONG,
	studentId LONG
);
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.communicationRecord.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.communicationRecord.model.CommunicationRecord;

/**
 * The persistence interface for the communication record service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Limon
 * @see CommunicationRecordPersistenceImpl
 * @see CommunicationRecordUtil
 * @generated
 */
public interface CommunicationRecordPersistence extends BasePersistence<CommunicationRecord> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CommunicationRecordUtil} to access the communication record persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the communication record in the entity cache if it is enabled.
	*
	* @param communicationRecord the communication record
	*/
	public void cacheResult(
		info.diit.portal.communicationRecord.model.CommunicationRecord communicationRecord);

	/**
	* Caches the communication records in the entity cache if it is enabled.
	*
	* @param communicationRecords the communication records
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.communicationRecord.model.CommunicationRecord> communicationRecords);

	/**
	* Creates a new communication record with the primary key. Does not add the communication record to the database.
	*
	* @param communicationRecordId the primary key for the new communication record
	* @return the new communication record
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord create(
		long communicationRecordId);

	/**
	* Removes the communication record with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param communicationRecordId the primary key of the communication record
	* @return the communication record that was removed
	* @throws info.diit.portal.communicationRecord.NoSuchCommunicationRecordException if a communication record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord remove(
		long communicationRecordId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.communicationRecord.NoSuchCommunicationRecordException;

	public info.diit.portal.communicationRecord.model.CommunicationRecord updateImpl(
		info.diit.portal.communicationRecord.model.CommunicationRecord communicationRecord,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the communication record with the primary key or throws a {@link info.diit.portal.communicationRecord.NoSuchCommunicationRecordException} if it could not be found.
	*
	* @param communicationRecordId the primary key of the communication record
	* @return the communication record
	* @throws info.diit.portal.communicationRecord.NoSuchCommunicationRecordException if a communication record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord findByPrimaryKey(
		long communicationRecordId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.communicationRecord.NoSuchCommunicationRecordException;

	/**
	* Returns the communication record with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param communicationRecordId the primary key of the communication record
	* @return the communication record, or <code>null</code> if a communication record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord fetchByPrimaryKey(
		long communicationRecordId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the communication records where communicationBy = &#63;.
	*
	* @param communicationBy the communication by
	* @return the matching communication records
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.communicationRecord.model.CommunicationRecord> findByCommunicationBy(
		long communicationBy)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the communication records where communicationBy = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param communicationBy the communication by
	* @param start the lower bound of the range of communication records
	* @param end the upper bound of the range of communication records (not inclusive)
	* @return the range of matching communication records
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.communicationRecord.model.CommunicationRecord> findByCommunicationBy(
		long communicationBy, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the communication records where communicationBy = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param communicationBy the communication by
	* @param start the lower bound of the range of communication records
	* @param end the upper bound of the range of communication records (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching communication records
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.communicationRecord.model.CommunicationRecord> findByCommunicationBy(
		long communicationBy, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first communication record in the ordered set where communicationBy = &#63;.
	*
	* @param communicationBy the communication by
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching communication record
	* @throws info.diit.portal.communicationRecord.NoSuchCommunicationRecordException if a matching communication record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord findByCommunicationBy_First(
		long communicationBy,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.communicationRecord.NoSuchCommunicationRecordException;

	/**
	* Returns the first communication record in the ordered set where communicationBy = &#63;.
	*
	* @param communicationBy the communication by
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching communication record, or <code>null</code> if a matching communication record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord fetchByCommunicationBy_First(
		long communicationBy,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last communication record in the ordered set where communicationBy = &#63;.
	*
	* @param communicationBy the communication by
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching communication record
	* @throws info.diit.portal.communicationRecord.NoSuchCommunicationRecordException if a matching communication record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord findByCommunicationBy_Last(
		long communicationBy,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.communicationRecord.NoSuchCommunicationRecordException;

	/**
	* Returns the last communication record in the ordered set where communicationBy = &#63;.
	*
	* @param communicationBy the communication by
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching communication record, or <code>null</code> if a matching communication record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord fetchByCommunicationBy_Last(
		long communicationBy,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the communication records before and after the current communication record in the ordered set where communicationBy = &#63;.
	*
	* @param communicationRecordId the primary key of the current communication record
	* @param communicationBy the communication by
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next communication record
	* @throws info.diit.portal.communicationRecord.NoSuchCommunicationRecordException if a communication record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord[] findByCommunicationBy_PrevAndNext(
		long communicationRecordId, long communicationBy,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.communicationRecord.NoSuchCommunicationRecordException;

	/**
	* Returns all the communication records.
	*
	* @return the communication records
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.communicationRecord.model.CommunicationRecord> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the communication records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of communication records
	* @param end the upper bound of the range of communication records (not inclusive)
	* @return the range of communication records
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.communicationRecord.model.CommunicationRecord> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the communication records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of communication records
	* @param end the upper bound of the range of communication records (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of communication records
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.communicationRecord.model.CommunicationRecord> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the communication records where communicationBy = &#63; from the database.
	*
	* @param communicationBy the communication by
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCommunicationBy(long communicationBy)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the communication records from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of communication records where communicationBy = &#63;.
	*
	* @param communicationBy the communication by
	* @return the number of matching communication records
	* @throws SystemException if a system exception occurred
	*/
	public int countByCommunicationBy(long communicationBy)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of communication records.
	*
	* @return the number of communication records
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
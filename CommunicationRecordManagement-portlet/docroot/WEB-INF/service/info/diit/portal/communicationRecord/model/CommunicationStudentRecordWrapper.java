/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.communicationRecord.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CommunicationStudentRecord}.
 * </p>
 *
 * @author    Limon
 * @see       CommunicationStudentRecord
 * @generated
 */
public class CommunicationStudentRecordWrapper
	implements CommunicationStudentRecord,
		ModelWrapper<CommunicationStudentRecord> {
	public CommunicationStudentRecordWrapper(
		CommunicationStudentRecord communicationStudentRecord) {
		_communicationStudentRecord = communicationStudentRecord;
	}

	public Class<?> getModelClass() {
		return CommunicationStudentRecord.class;
	}

	public String getModelClassName() {
		return CommunicationStudentRecord.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("CommunicationStudentRecorId",
			getCommunicationStudentRecorId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("communicationBy", getCommunicationBy());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("communicationRecordId", getCommunicationRecordId());
		attributes.put("studentId", getStudentId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long CommunicationStudentRecorId = (Long)attributes.get(
				"CommunicationStudentRecorId");

		if (CommunicationStudentRecorId != null) {
			setCommunicationStudentRecorId(CommunicationStudentRecorId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long communicationBy = (Long)attributes.get("communicationBy");

		if (communicationBy != null) {
			setCommunicationBy(communicationBy);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long communicationRecordId = (Long)attributes.get(
				"communicationRecordId");

		if (communicationRecordId != null) {
			setCommunicationRecordId(communicationRecordId);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}
	}

	/**
	* Returns the primary key of this communication student record.
	*
	* @return the primary key of this communication student record
	*/
	public long getPrimaryKey() {
		return _communicationStudentRecord.getPrimaryKey();
	}

	/**
	* Sets the primary key of this communication student record.
	*
	* @param primaryKey the primary key of this communication student record
	*/
	public void setPrimaryKey(long primaryKey) {
		_communicationStudentRecord.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the communication student recor ID of this communication student record.
	*
	* @return the communication student recor ID of this communication student record
	*/
	public long getCommunicationStudentRecorId() {
		return _communicationStudentRecord.getCommunicationStudentRecorId();
	}

	/**
	* Sets the communication student recor ID of this communication student record.
	*
	* @param CommunicationStudentRecorId the communication student recor ID of this communication student record
	*/
	public void setCommunicationStudentRecorId(long CommunicationStudentRecorId) {
		_communicationStudentRecord.setCommunicationStudentRecorId(CommunicationStudentRecorId);
	}

	/**
	* Returns the company ID of this communication student record.
	*
	* @return the company ID of this communication student record
	*/
	public long getCompanyId() {
		return _communicationStudentRecord.getCompanyId();
	}

	/**
	* Sets the company ID of this communication student record.
	*
	* @param companyId the company ID of this communication student record
	*/
	public void setCompanyId(long companyId) {
		_communicationStudentRecord.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this communication student record.
	*
	* @return the organization ID of this communication student record
	*/
	public long getOrganizationId() {
		return _communicationStudentRecord.getOrganizationId();
	}

	/**
	* Sets the organization ID of this communication student record.
	*
	* @param organizationId the organization ID of this communication student record
	*/
	public void setOrganizationId(long organizationId) {
		_communicationStudentRecord.setOrganizationId(organizationId);
	}

	/**
	* Returns the communication by of this communication student record.
	*
	* @return the communication by of this communication student record
	*/
	public long getCommunicationBy() {
		return _communicationStudentRecord.getCommunicationBy();
	}

	/**
	* Sets the communication by of this communication student record.
	*
	* @param communicationBy the communication by of this communication student record
	*/
	public void setCommunicationBy(long communicationBy) {
		_communicationStudentRecord.setCommunicationBy(communicationBy);
	}

	/**
	* Returns the user name of this communication student record.
	*
	* @return the user name of this communication student record
	*/
	public java.lang.String getUserName() {
		return _communicationStudentRecord.getUserName();
	}

	/**
	* Sets the user name of this communication student record.
	*
	* @param userName the user name of this communication student record
	*/
	public void setUserName(java.lang.String userName) {
		_communicationStudentRecord.setUserName(userName);
	}

	/**
	* Returns the create date of this communication student record.
	*
	* @return the create date of this communication student record
	*/
	public java.util.Date getCreateDate() {
		return _communicationStudentRecord.getCreateDate();
	}

	/**
	* Sets the create date of this communication student record.
	*
	* @param createDate the create date of this communication student record
	*/
	public void setCreateDate(java.util.Date createDate) {
		_communicationStudentRecord.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this communication student record.
	*
	* @return the modified date of this communication student record
	*/
	public java.util.Date getModifiedDate() {
		return _communicationStudentRecord.getModifiedDate();
	}

	/**
	* Sets the modified date of this communication student record.
	*
	* @param modifiedDate the modified date of this communication student record
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_communicationStudentRecord.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the communication record ID of this communication student record.
	*
	* @return the communication record ID of this communication student record
	*/
	public long getCommunicationRecordId() {
		return _communicationStudentRecord.getCommunicationRecordId();
	}

	/**
	* Sets the communication record ID of this communication student record.
	*
	* @param communicationRecordId the communication record ID of this communication student record
	*/
	public void setCommunicationRecordId(long communicationRecordId) {
		_communicationStudentRecord.setCommunicationRecordId(communicationRecordId);
	}

	/**
	* Returns the student ID of this communication student record.
	*
	* @return the student ID of this communication student record
	*/
	public long getStudentId() {
		return _communicationStudentRecord.getStudentId();
	}

	/**
	* Sets the student ID of this communication student record.
	*
	* @param studentId the student ID of this communication student record
	*/
	public void setStudentId(long studentId) {
		_communicationStudentRecord.setStudentId(studentId);
	}

	public boolean isNew() {
		return _communicationStudentRecord.isNew();
	}

	public void setNew(boolean n) {
		_communicationStudentRecord.setNew(n);
	}

	public boolean isCachedModel() {
		return _communicationStudentRecord.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_communicationStudentRecord.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _communicationStudentRecord.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _communicationStudentRecord.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_communicationStudentRecord.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _communicationStudentRecord.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_communicationStudentRecord.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CommunicationStudentRecordWrapper((CommunicationStudentRecord)_communicationStudentRecord.clone());
	}

	public int compareTo(
		info.diit.portal.communicationRecord.model.CommunicationStudentRecord communicationStudentRecord) {
		return _communicationStudentRecord.compareTo(communicationStudentRecord);
	}

	@Override
	public int hashCode() {
		return _communicationStudentRecord.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.communicationRecord.model.CommunicationStudentRecord> toCacheModel() {
		return _communicationStudentRecord.toCacheModel();
	}

	public info.diit.portal.communicationRecord.model.CommunicationStudentRecord toEscapedModel() {
		return new CommunicationStudentRecordWrapper(_communicationStudentRecord.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _communicationStudentRecord.toString();
	}

	public java.lang.String toXmlString() {
		return _communicationStudentRecord.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_communicationStudentRecord.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public CommunicationStudentRecord getWrappedCommunicationStudentRecord() {
		return _communicationStudentRecord;
	}

	public CommunicationStudentRecord getWrappedModel() {
		return _communicationStudentRecord;
	}

	public void resetOriginalValues() {
		_communicationStudentRecord.resetOriginalValues();
	}

	private CommunicationStudentRecord _communicationStudentRecord;
}
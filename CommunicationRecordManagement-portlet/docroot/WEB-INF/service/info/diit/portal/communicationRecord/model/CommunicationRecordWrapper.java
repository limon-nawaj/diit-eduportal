/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.communicationRecord.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CommunicationRecord}.
 * </p>
 *
 * @author    Limon
 * @see       CommunicationRecord
 * @generated
 */
public class CommunicationRecordWrapper implements CommunicationRecord,
	ModelWrapper<CommunicationRecord> {
	public CommunicationRecordWrapper(CommunicationRecord communicationRecord) {
		_communicationRecord = communicationRecord;
	}

	public Class<?> getModelClass() {
		return CommunicationRecord.class;
	}

	public String getModelClassName() {
		return CommunicationRecord.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("communicationRecordId", getCommunicationRecordId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("communicationBy", getCommunicationBy());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("batch", getBatch());
		attributes.put("date", getDate());
		attributes.put("media", getMedia());
		attributes.put("subject", getSubject());
		attributes.put("communicationWith", getCommunicationWith());
		attributes.put("details", getDetails());
		attributes.put("status", getStatus());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long communicationRecordId = (Long)attributes.get(
				"communicationRecordId");

		if (communicationRecordId != null) {
			setCommunicationRecordId(communicationRecordId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long communicationBy = (Long)attributes.get("communicationBy");

		if (communicationBy != null) {
			setCommunicationBy(communicationBy);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long batch = (Long)attributes.get("batch");

		if (batch != null) {
			setBatch(batch);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		Long media = (Long)attributes.get("media");

		if (media != null) {
			setMedia(media);
		}

		Long subject = (Long)attributes.get("subject");

		if (subject != null) {
			setSubject(subject);
		}

		Long communicationWith = (Long)attributes.get("communicationWith");

		if (communicationWith != null) {
			setCommunicationWith(communicationWith);
		}

		String details = (String)attributes.get("details");

		if (details != null) {
			setDetails(details);
		}

		Long status = (Long)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	/**
	* Returns the primary key of this communication record.
	*
	* @return the primary key of this communication record
	*/
	public long getPrimaryKey() {
		return _communicationRecord.getPrimaryKey();
	}

	/**
	* Sets the primary key of this communication record.
	*
	* @param primaryKey the primary key of this communication record
	*/
	public void setPrimaryKey(long primaryKey) {
		_communicationRecord.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the communication record ID of this communication record.
	*
	* @return the communication record ID of this communication record
	*/
	public long getCommunicationRecordId() {
		return _communicationRecord.getCommunicationRecordId();
	}

	/**
	* Sets the communication record ID of this communication record.
	*
	* @param communicationRecordId the communication record ID of this communication record
	*/
	public void setCommunicationRecordId(long communicationRecordId) {
		_communicationRecord.setCommunicationRecordId(communicationRecordId);
	}

	/**
	* Returns the company ID of this communication record.
	*
	* @return the company ID of this communication record
	*/
	public long getCompanyId() {
		return _communicationRecord.getCompanyId();
	}

	/**
	* Sets the company ID of this communication record.
	*
	* @param companyId the company ID of this communication record
	*/
	public void setCompanyId(long companyId) {
		_communicationRecord.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this communication record.
	*
	* @return the organization ID of this communication record
	*/
	public long getOrganizationId() {
		return _communicationRecord.getOrganizationId();
	}

	/**
	* Sets the organization ID of this communication record.
	*
	* @param organizationId the organization ID of this communication record
	*/
	public void setOrganizationId(long organizationId) {
		_communicationRecord.setOrganizationId(organizationId);
	}

	/**
	* Returns the communication by of this communication record.
	*
	* @return the communication by of this communication record
	*/
	public long getCommunicationBy() {
		return _communicationRecord.getCommunicationBy();
	}

	/**
	* Sets the communication by of this communication record.
	*
	* @param communicationBy the communication by of this communication record
	*/
	public void setCommunicationBy(long communicationBy) {
		_communicationRecord.setCommunicationBy(communicationBy);
	}

	/**
	* Returns the user name of this communication record.
	*
	* @return the user name of this communication record
	*/
	public java.lang.String getUserName() {
		return _communicationRecord.getUserName();
	}

	/**
	* Sets the user name of this communication record.
	*
	* @param userName the user name of this communication record
	*/
	public void setUserName(java.lang.String userName) {
		_communicationRecord.setUserName(userName);
	}

	/**
	* Returns the create date of this communication record.
	*
	* @return the create date of this communication record
	*/
	public java.util.Date getCreateDate() {
		return _communicationRecord.getCreateDate();
	}

	/**
	* Sets the create date of this communication record.
	*
	* @param createDate the create date of this communication record
	*/
	public void setCreateDate(java.util.Date createDate) {
		_communicationRecord.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this communication record.
	*
	* @return the modified date of this communication record
	*/
	public java.util.Date getModifiedDate() {
		return _communicationRecord.getModifiedDate();
	}

	/**
	* Sets the modified date of this communication record.
	*
	* @param modifiedDate the modified date of this communication record
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_communicationRecord.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the batch of this communication record.
	*
	* @return the batch of this communication record
	*/
	public long getBatch() {
		return _communicationRecord.getBatch();
	}

	/**
	* Sets the batch of this communication record.
	*
	* @param batch the batch of this communication record
	*/
	public void setBatch(long batch) {
		_communicationRecord.setBatch(batch);
	}

	/**
	* Returns the date of this communication record.
	*
	* @return the date of this communication record
	*/
	public java.util.Date getDate() {
		return _communicationRecord.getDate();
	}

	/**
	* Sets the date of this communication record.
	*
	* @param date the date of this communication record
	*/
	public void setDate(java.util.Date date) {
		_communicationRecord.setDate(date);
	}

	/**
	* Returns the media of this communication record.
	*
	* @return the media of this communication record
	*/
	public long getMedia() {
		return _communicationRecord.getMedia();
	}

	/**
	* Sets the media of this communication record.
	*
	* @param media the media of this communication record
	*/
	public void setMedia(long media) {
		_communicationRecord.setMedia(media);
	}

	/**
	* Returns the subject of this communication record.
	*
	* @return the subject of this communication record
	*/
	public long getSubject() {
		return _communicationRecord.getSubject();
	}

	/**
	* Sets the subject of this communication record.
	*
	* @param subject the subject of this communication record
	*/
	public void setSubject(long subject) {
		_communicationRecord.setSubject(subject);
	}

	/**
	* Returns the communication with of this communication record.
	*
	* @return the communication with of this communication record
	*/
	public long getCommunicationWith() {
		return _communicationRecord.getCommunicationWith();
	}

	/**
	* Sets the communication with of this communication record.
	*
	* @param communicationWith the communication with of this communication record
	*/
	public void setCommunicationWith(long communicationWith) {
		_communicationRecord.setCommunicationWith(communicationWith);
	}

	/**
	* Returns the details of this communication record.
	*
	* @return the details of this communication record
	*/
	public java.lang.String getDetails() {
		return _communicationRecord.getDetails();
	}

	/**
	* Sets the details of this communication record.
	*
	* @param details the details of this communication record
	*/
	public void setDetails(java.lang.String details) {
		_communicationRecord.setDetails(details);
	}

	/**
	* Returns the status of this communication record.
	*
	* @return the status of this communication record
	*/
	public long getStatus() {
		return _communicationRecord.getStatus();
	}

	/**
	* Sets the status of this communication record.
	*
	* @param status the status of this communication record
	*/
	public void setStatus(long status) {
		_communicationRecord.setStatus(status);
	}

	public boolean isNew() {
		return _communicationRecord.isNew();
	}

	public void setNew(boolean n) {
		_communicationRecord.setNew(n);
	}

	public boolean isCachedModel() {
		return _communicationRecord.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_communicationRecord.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _communicationRecord.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _communicationRecord.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_communicationRecord.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _communicationRecord.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_communicationRecord.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CommunicationRecordWrapper((CommunicationRecord)_communicationRecord.clone());
	}

	public int compareTo(
		info.diit.portal.communicationRecord.model.CommunicationRecord communicationRecord) {
		return _communicationRecord.compareTo(communicationRecord);
	}

	@Override
	public int hashCode() {
		return _communicationRecord.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.communicationRecord.model.CommunicationRecord> toCacheModel() {
		return _communicationRecord.toCacheModel();
	}

	public info.diit.portal.communicationRecord.model.CommunicationRecord toEscapedModel() {
		return new CommunicationRecordWrapper(_communicationRecord.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _communicationRecord.toString();
	}

	public java.lang.String toXmlString() {
		return _communicationRecord.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_communicationRecord.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public CommunicationRecord getWrappedCommunicationRecord() {
		return _communicationRecord;
	}

	public CommunicationRecord getWrappedModel() {
		return _communicationRecord;
	}

	public void resetOriginalValues() {
		_communicationRecord.resetOriginalValues();
	}

	private CommunicationRecord _communicationRecord;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.communicationRecord.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.communicationRecord.model.CommunicationStudentRecord;

import java.util.List;

/**
 * The persistence utility for the communication student record service. This utility wraps {@link CommunicationStudentRecordPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Limon
 * @see CommunicationStudentRecordPersistence
 * @see CommunicationStudentRecordPersistenceImpl
 * @generated
 */
public class CommunicationStudentRecordUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(
		CommunicationStudentRecord communicationStudentRecord) {
		getPersistence().clearCache(communicationStudentRecord);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CommunicationStudentRecord> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CommunicationStudentRecord> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CommunicationStudentRecord> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static CommunicationStudentRecord update(
		CommunicationStudentRecord communicationStudentRecord, boolean merge)
		throws SystemException {
		return getPersistence().update(communicationStudentRecord, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static CommunicationStudentRecord update(
		CommunicationStudentRecord communicationStudentRecord, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence()
				   .update(communicationStudentRecord, merge, serviceContext);
	}

	/**
	* Caches the communication student record in the entity cache if it is enabled.
	*
	* @param communicationStudentRecord the communication student record
	*/
	public static void cacheResult(
		info.diit.portal.communicationRecord.model.CommunicationStudentRecord communicationStudentRecord) {
		getPersistence().cacheResult(communicationStudentRecord);
	}

	/**
	* Caches the communication student records in the entity cache if it is enabled.
	*
	* @param communicationStudentRecords the communication student records
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.communicationRecord.model.CommunicationStudentRecord> communicationStudentRecords) {
		getPersistence().cacheResult(communicationStudentRecords);
	}

	/**
	* Creates a new communication student record with the primary key. Does not add the communication student record to the database.
	*
	* @param CommunicationStudentRecorId the primary key for the new communication student record
	* @return the new communication student record
	*/
	public static info.diit.portal.communicationRecord.model.CommunicationStudentRecord create(
		long CommunicationStudentRecorId) {
		return getPersistence().create(CommunicationStudentRecorId);
	}

	/**
	* Removes the communication student record with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param CommunicationStudentRecorId the primary key of the communication student record
	* @return the communication student record that was removed
	* @throws info.diit.portal.communicationRecord.NoSuchCommunicationStudentRecordException if a communication student record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.communicationRecord.model.CommunicationStudentRecord remove(
		long CommunicationStudentRecorId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.communicationRecord.NoSuchCommunicationStudentRecordException {
		return getPersistence().remove(CommunicationStudentRecorId);
	}

	public static info.diit.portal.communicationRecord.model.CommunicationStudentRecord updateImpl(
		info.diit.portal.communicationRecord.model.CommunicationStudentRecord communicationStudentRecord,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(communicationStudentRecord, merge);
	}

	/**
	* Returns the communication student record with the primary key or throws a {@link info.diit.portal.communicationRecord.NoSuchCommunicationStudentRecordException} if it could not be found.
	*
	* @param CommunicationStudentRecorId the primary key of the communication student record
	* @return the communication student record
	* @throws info.diit.portal.communicationRecord.NoSuchCommunicationStudentRecordException if a communication student record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.communicationRecord.model.CommunicationStudentRecord findByPrimaryKey(
		long CommunicationStudentRecorId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.communicationRecord.NoSuchCommunicationStudentRecordException {
		return getPersistence().findByPrimaryKey(CommunicationStudentRecorId);
	}

	/**
	* Returns the communication student record with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param CommunicationStudentRecorId the primary key of the communication student record
	* @return the communication student record, or <code>null</code> if a communication student record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.communicationRecord.model.CommunicationStudentRecord fetchByPrimaryKey(
		long CommunicationStudentRecorId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(CommunicationStudentRecorId);
	}

	/**
	* Returns all the communication student records where communicationRecordId = &#63;.
	*
	* @param communicationRecordId the communication record ID
	* @return the matching communication student records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.communicationRecord.model.CommunicationStudentRecord> findByCommunicationRecord(
		long communicationRecordId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCommunicationRecord(communicationRecordId);
	}

	/**
	* Returns a range of all the communication student records where communicationRecordId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param communicationRecordId the communication record ID
	* @param start the lower bound of the range of communication student records
	* @param end the upper bound of the range of communication student records (not inclusive)
	* @return the range of matching communication student records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.communicationRecord.model.CommunicationStudentRecord> findByCommunicationRecord(
		long communicationRecordId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCommunicationRecord(communicationRecordId, start, end);
	}

	/**
	* Returns an ordered range of all the communication student records where communicationRecordId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param communicationRecordId the communication record ID
	* @param start the lower bound of the range of communication student records
	* @param end the upper bound of the range of communication student records (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching communication student records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.communicationRecord.model.CommunicationStudentRecord> findByCommunicationRecord(
		long communicationRecordId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCommunicationRecord(communicationRecordId, start,
			end, orderByComparator);
	}

	/**
	* Returns the first communication student record in the ordered set where communicationRecordId = &#63;.
	*
	* @param communicationRecordId the communication record ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching communication student record
	* @throws info.diit.portal.communicationRecord.NoSuchCommunicationStudentRecordException if a matching communication student record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.communicationRecord.model.CommunicationStudentRecord findByCommunicationRecord_First(
		long communicationRecordId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.communicationRecord.NoSuchCommunicationStudentRecordException {
		return getPersistence()
				   .findByCommunicationRecord_First(communicationRecordId,
			orderByComparator);
	}

	/**
	* Returns the first communication student record in the ordered set where communicationRecordId = &#63;.
	*
	* @param communicationRecordId the communication record ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching communication student record, or <code>null</code> if a matching communication student record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.communicationRecord.model.CommunicationStudentRecord fetchByCommunicationRecord_First(
		long communicationRecordId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCommunicationRecord_First(communicationRecordId,
			orderByComparator);
	}

	/**
	* Returns the last communication student record in the ordered set where communicationRecordId = &#63;.
	*
	* @param communicationRecordId the communication record ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching communication student record
	* @throws info.diit.portal.communicationRecord.NoSuchCommunicationStudentRecordException if a matching communication student record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.communicationRecord.model.CommunicationStudentRecord findByCommunicationRecord_Last(
		long communicationRecordId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.communicationRecord.NoSuchCommunicationStudentRecordException {
		return getPersistence()
				   .findByCommunicationRecord_Last(communicationRecordId,
			orderByComparator);
	}

	/**
	* Returns the last communication student record in the ordered set where communicationRecordId = &#63;.
	*
	* @param communicationRecordId the communication record ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching communication student record, or <code>null</code> if a matching communication student record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.communicationRecord.model.CommunicationStudentRecord fetchByCommunicationRecord_Last(
		long communicationRecordId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCommunicationRecord_Last(communicationRecordId,
			orderByComparator);
	}

	/**
	* Returns the communication student records before and after the current communication student record in the ordered set where communicationRecordId = &#63;.
	*
	* @param CommunicationStudentRecorId the primary key of the current communication student record
	* @param communicationRecordId the communication record ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next communication student record
	* @throws info.diit.portal.communicationRecord.NoSuchCommunicationStudentRecordException if a communication student record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.communicationRecord.model.CommunicationStudentRecord[] findByCommunicationRecord_PrevAndNext(
		long CommunicationStudentRecorId, long communicationRecordId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.communicationRecord.NoSuchCommunicationStudentRecordException {
		return getPersistence()
				   .findByCommunicationRecord_PrevAndNext(CommunicationStudentRecorId,
			communicationRecordId, orderByComparator);
	}

	/**
	* Returns all the communication student records.
	*
	* @return the communication student records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.communicationRecord.model.CommunicationStudentRecord> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the communication student records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of communication student records
	* @param end the upper bound of the range of communication student records (not inclusive)
	* @return the range of communication student records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.communicationRecord.model.CommunicationStudentRecord> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the communication student records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of communication student records
	* @param end the upper bound of the range of communication student records (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of communication student records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.communicationRecord.model.CommunicationStudentRecord> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the communication student records where communicationRecordId = &#63; from the database.
	*
	* @param communicationRecordId the communication record ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCommunicationRecord(long communicationRecordId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCommunicationRecord(communicationRecordId);
	}

	/**
	* Removes all the communication student records from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of communication student records where communicationRecordId = &#63;.
	*
	* @param communicationRecordId the communication record ID
	* @return the number of matching communication student records
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCommunicationRecord(long communicationRecordId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCommunicationRecord(communicationRecordId);
	}

	/**
	* Returns the number of communication student records.
	*
	* @return the number of communication student records
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CommunicationStudentRecordPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CommunicationStudentRecordPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.communicationRecord.service.ClpSerializer.getServletContextName(),
					CommunicationStudentRecordPersistence.class.getName());

			ReferenceRegistry.registerReference(CommunicationStudentRecordUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(
		CommunicationStudentRecordPersistence persistence) {
	}

	private static CommunicationStudentRecordPersistence _persistence;
}
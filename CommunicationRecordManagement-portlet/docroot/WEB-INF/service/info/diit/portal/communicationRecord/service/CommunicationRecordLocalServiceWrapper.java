/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.communicationRecord.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link CommunicationRecordLocalService}.
 * </p>
 *
 * @author    Limon
 * @see       CommunicationRecordLocalService
 * @generated
 */
public class CommunicationRecordLocalServiceWrapper
	implements CommunicationRecordLocalService,
		ServiceWrapper<CommunicationRecordLocalService> {
	public CommunicationRecordLocalServiceWrapper(
		CommunicationRecordLocalService communicationRecordLocalService) {
		_communicationRecordLocalService = communicationRecordLocalService;
	}

	/**
	* Adds the communication record to the database. Also notifies the appropriate model listeners.
	*
	* @param communicationRecord the communication record
	* @return the communication record that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord addCommunicationRecord(
		info.diit.portal.communicationRecord.model.CommunicationRecord communicationRecord)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.addCommunicationRecord(communicationRecord);
	}

	/**
	* Creates a new communication record with the primary key. Does not add the communication record to the database.
	*
	* @param communicationRecordId the primary key for the new communication record
	* @return the new communication record
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord createCommunicationRecord(
		long communicationRecordId) {
		return _communicationRecordLocalService.createCommunicationRecord(communicationRecordId);
	}

	/**
	* Deletes the communication record with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param communicationRecordId the primary key of the communication record
	* @return the communication record that was removed
	* @throws PortalException if a communication record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord deleteCommunicationRecord(
		long communicationRecordId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.deleteCommunicationRecord(communicationRecordId);
	}

	/**
	* Deletes the communication record from the database. Also notifies the appropriate model listeners.
	*
	* @param communicationRecord the communication record
	* @return the communication record that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord deleteCommunicationRecord(
		info.diit.portal.communicationRecord.model.CommunicationRecord communicationRecord)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.deleteCommunicationRecord(communicationRecord);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _communicationRecordLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.communicationRecord.model.CommunicationRecord fetchCommunicationRecord(
		long communicationRecordId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.fetchCommunicationRecord(communicationRecordId);
	}

	/**
	* Returns the communication record with the primary key.
	*
	* @param communicationRecordId the primary key of the communication record
	* @return the communication record
	* @throws PortalException if a communication record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord getCommunicationRecord(
		long communicationRecordId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.getCommunicationRecord(communicationRecordId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the communication records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of communication records
	* @param end the upper bound of the range of communication records (not inclusive)
	* @return the range of communication records
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.communicationRecord.model.CommunicationRecord> getCommunicationRecords(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.getCommunicationRecords(start,
			end);
	}

	/**
	* Returns the number of communication records.
	*
	* @return the number of communication records
	* @throws SystemException if a system exception occurred
	*/
	public int getCommunicationRecordsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.getCommunicationRecordsCount();
	}

	/**
	* Updates the communication record in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param communicationRecord the communication record
	* @return the communication record that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord updateCommunicationRecord(
		info.diit.portal.communicationRecord.model.CommunicationRecord communicationRecord)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.updateCommunicationRecord(communicationRecord);
	}

	/**
	* Updates the communication record in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param communicationRecord the communication record
	* @param merge whether to merge the communication record with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the communication record that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.communicationRecord.model.CommunicationRecord updateCommunicationRecord(
		info.diit.portal.communicationRecord.model.CommunicationRecord communicationRecord,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.updateCommunicationRecord(communicationRecord,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _communicationRecordLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_communicationRecordLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _communicationRecordLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	public java.util.List<info.diit.portal.communicationRecord.model.CommunicationRecord> findByCommunicationByUser(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return _communicationRecordLocalService.findByCommunicationByUser(userId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public CommunicationRecordLocalService getWrappedCommunicationRecordLocalService() {
		return _communicationRecordLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedCommunicationRecordLocalService(
		CommunicationRecordLocalService communicationRecordLocalService) {
		_communicationRecordLocalService = communicationRecordLocalService;
	}

	public CommunicationRecordLocalService getWrappedService() {
		return _communicationRecordLocalService;
	}

	public void setWrappedService(
		CommunicationRecordLocalService communicationRecordLocalService) {
		_communicationRecordLocalService = communicationRecordLocalService;
	}

	private CommunicationRecordLocalService _communicationRecordLocalService;
}
package info.diit.portal.absencel.dto;

public class AbsenceDto {
	private long id;
	private BatchDto batch;
	private long studentId;
	private String studentName;
	private CourseDto course;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public BatchDto getBatch() {
		return batch;
	}
	public void setBatch(BatchDto batch) {
		this.batch = batch;
	}
	public long getStudentId() {
		return studentId;
	}
	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public CourseDto getCourse() {
		return course;
	}
	public void setCourse(CourseDto course) {
		this.course = course;
	}
}

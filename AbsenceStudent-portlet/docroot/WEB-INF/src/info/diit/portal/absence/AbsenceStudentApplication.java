package info.diit.portal.absence;

import info.diit.portal.absencel.dto.AbsenceDto;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class AbsenceStudentApplication extends Application implements PortletRequestListener {

	private ThemeDisplay themeDisplay;
	private BeanItemContainer<AbsenceDto> container;
	
	private final static String COLUMN_COURSE = "course";
	private final static String COLUMN_BATCH = "batch";
	private final static String COLUMN_STUDENT_ID = "studentId";
	private final static String COLUMN_STUDENT_NAME = "studentName";
	
	public void init() {
		Window window = new Window();

		setMainWindow(window);

		Label label = new Label("Hello AbsenceStudent!");
		
		loadAbsenceStudent();
		window.addComponent(mainLayout());
	}
	
	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}
	
	private ComboBox courseComboBox;
	private ComboBox batchComboBox;
	private Table absenceTable;
	
	private GridLayout mainLayout(){
		GridLayout mainLayout = new GridLayout(8, 8);
		mainLayout.setWidth("100%");
		
		courseComboBox = new ComboBox("Course");
		batchComboBox = new ComboBox("Batch");
		
		courseComboBox.setImmediate(true);
		batchComboBox.setImmediate(true);
		
		courseComboBox.setWidth("100%");
		batchComboBox.setWidth("100%");
		
		container = new BeanItemContainer<AbsenceDto>(AbsenceDto.class);
		absenceTable = new Table("", container);
		absenceTable.setWidth("100%");
		
		absenceTable.setColumnHeader(COLUMN_COURSE, "Course");
		absenceTable.setColumnHeader(COLUMN_BATCH, "Batch");
		absenceTable.setColumnHeader(COLUMN_STUDENT_ID, "Student Id");
		absenceTable.setColumnHeader(COLUMN_STUDENT_NAME, "Student Name");
		
		absenceTable.setVisibleColumns(new String[]{COLUMN_COURSE, COLUMN_BATCH, COLUMN_STUDENT_ID, COLUMN_STUDENT_NAME});
		
		mainLayout.addComponent(courseComboBox, 0, 0, 1, 0);
		mainLayout.addComponent(batchComboBox, 3, 0, 4, 0);
		mainLayout.addComponent(absenceTable, 0, 1, 7, 1);
		
		return mainLayout;
	}

	private void loadAbsenceStudent(){
		
	}
}
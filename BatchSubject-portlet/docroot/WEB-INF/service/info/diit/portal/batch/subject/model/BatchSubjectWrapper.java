/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.batch.subject.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BatchSubject}.
 * </p>
 *
 * @author    saeid
 * @see       BatchSubject
 * @generated
 */
public class BatchSubjectWrapper implements BatchSubject,
	ModelWrapper<BatchSubject> {
	public BatchSubjectWrapper(BatchSubject batchSubject) {
		_batchSubject = batchSubject;
	}

	public Class<?> getModelClass() {
		return BatchSubject.class;
	}

	public String getModelClassName() {
		return BatchSubject.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("batchSubjectId", getBatchSubjectId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("courseId", getCourseId());
		attributes.put("batchId", getBatchId());
		attributes.put("subjectId", getSubjectId());
		attributes.put("teacherId", getTeacherId());
		attributes.put("startDate", getStartDate());
		attributes.put("hours", getHours());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long batchSubjectId = (Long)attributes.get("batchSubjectId");

		if (batchSubjectId != null) {
			setBatchSubjectId(batchSubjectId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long courseId = (Long)attributes.get("courseId");

		if (courseId != null) {
			setCourseId(courseId);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Long subjectId = (Long)attributes.get("subjectId");

		if (subjectId != null) {
			setSubjectId(subjectId);
		}

		Long teacherId = (Long)attributes.get("teacherId");

		if (teacherId != null) {
			setTeacherId(teacherId);
		}

		Date startDate = (Date)attributes.get("startDate");

		if (startDate != null) {
			setStartDate(startDate);
		}

		Integer hours = (Integer)attributes.get("hours");

		if (hours != null) {
			setHours(hours);
		}
	}

	/**
	* Returns the primary key of this batch subject.
	*
	* @return the primary key of this batch subject
	*/
	public long getPrimaryKey() {
		return _batchSubject.getPrimaryKey();
	}

	/**
	* Sets the primary key of this batch subject.
	*
	* @param primaryKey the primary key of this batch subject
	*/
	public void setPrimaryKey(long primaryKey) {
		_batchSubject.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the batch subject ID of this batch subject.
	*
	* @return the batch subject ID of this batch subject
	*/
	public long getBatchSubjectId() {
		return _batchSubject.getBatchSubjectId();
	}

	/**
	* Sets the batch subject ID of this batch subject.
	*
	* @param batchSubjectId the batch subject ID of this batch subject
	*/
	public void setBatchSubjectId(long batchSubjectId) {
		_batchSubject.setBatchSubjectId(batchSubjectId);
	}

	/**
	* Returns the company ID of this batch subject.
	*
	* @return the company ID of this batch subject
	*/
	public long getCompanyId() {
		return _batchSubject.getCompanyId();
	}

	/**
	* Sets the company ID of this batch subject.
	*
	* @param companyId the company ID of this batch subject
	*/
	public void setCompanyId(long companyId) {
		_batchSubject.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this batch subject.
	*
	* @return the organization ID of this batch subject
	*/
	public long getOrganizationId() {
		return _batchSubject.getOrganizationId();
	}

	/**
	* Sets the organization ID of this batch subject.
	*
	* @param organizationId the organization ID of this batch subject
	*/
	public void setOrganizationId(long organizationId) {
		_batchSubject.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this batch subject.
	*
	* @return the user ID of this batch subject
	*/
	public long getUserId() {
		return _batchSubject.getUserId();
	}

	/**
	* Sets the user ID of this batch subject.
	*
	* @param userId the user ID of this batch subject
	*/
	public void setUserId(long userId) {
		_batchSubject.setUserId(userId);
	}

	/**
	* Returns the user uuid of this batch subject.
	*
	* @return the user uuid of this batch subject
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchSubject.getUserUuid();
	}

	/**
	* Sets the user uuid of this batch subject.
	*
	* @param userUuid the user uuid of this batch subject
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_batchSubject.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this batch subject.
	*
	* @return the user name of this batch subject
	*/
	public java.lang.String getUserName() {
		return _batchSubject.getUserName();
	}

	/**
	* Sets the user name of this batch subject.
	*
	* @param userName the user name of this batch subject
	*/
	public void setUserName(java.lang.String userName) {
		_batchSubject.setUserName(userName);
	}

	/**
	* Returns the create date of this batch subject.
	*
	* @return the create date of this batch subject
	*/
	public java.util.Date getCreateDate() {
		return _batchSubject.getCreateDate();
	}

	/**
	* Sets the create date of this batch subject.
	*
	* @param createDate the create date of this batch subject
	*/
	public void setCreateDate(java.util.Date createDate) {
		_batchSubject.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this batch subject.
	*
	* @return the modified date of this batch subject
	*/
	public java.util.Date getModifiedDate() {
		return _batchSubject.getModifiedDate();
	}

	/**
	* Sets the modified date of this batch subject.
	*
	* @param modifiedDate the modified date of this batch subject
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_batchSubject.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the course ID of this batch subject.
	*
	* @return the course ID of this batch subject
	*/
	public long getCourseId() {
		return _batchSubject.getCourseId();
	}

	/**
	* Sets the course ID of this batch subject.
	*
	* @param courseId the course ID of this batch subject
	*/
	public void setCourseId(long courseId) {
		_batchSubject.setCourseId(courseId);
	}

	/**
	* Returns the batch ID of this batch subject.
	*
	* @return the batch ID of this batch subject
	*/
	public long getBatchId() {
		return _batchSubject.getBatchId();
	}

	/**
	* Sets the batch ID of this batch subject.
	*
	* @param batchId the batch ID of this batch subject
	*/
	public void setBatchId(long batchId) {
		_batchSubject.setBatchId(batchId);
	}

	/**
	* Returns the subject ID of this batch subject.
	*
	* @return the subject ID of this batch subject
	*/
	public long getSubjectId() {
		return _batchSubject.getSubjectId();
	}

	/**
	* Sets the subject ID of this batch subject.
	*
	* @param subjectId the subject ID of this batch subject
	*/
	public void setSubjectId(long subjectId) {
		_batchSubject.setSubjectId(subjectId);
	}

	/**
	* Returns the teacher ID of this batch subject.
	*
	* @return the teacher ID of this batch subject
	*/
	public long getTeacherId() {
		return _batchSubject.getTeacherId();
	}

	/**
	* Sets the teacher ID of this batch subject.
	*
	* @param teacherId the teacher ID of this batch subject
	*/
	public void setTeacherId(long teacherId) {
		_batchSubject.setTeacherId(teacherId);
	}

	/**
	* Returns the start date of this batch subject.
	*
	* @return the start date of this batch subject
	*/
	public java.util.Date getStartDate() {
		return _batchSubject.getStartDate();
	}

	/**
	* Sets the start date of this batch subject.
	*
	* @param startDate the start date of this batch subject
	*/
	public void setStartDate(java.util.Date startDate) {
		_batchSubject.setStartDate(startDate);
	}

	/**
	* Returns the hours of this batch subject.
	*
	* @return the hours of this batch subject
	*/
	public int getHours() {
		return _batchSubject.getHours();
	}

	/**
	* Sets the hours of this batch subject.
	*
	* @param hours the hours of this batch subject
	*/
	public void setHours(int hours) {
		_batchSubject.setHours(hours);
	}

	public boolean isNew() {
		return _batchSubject.isNew();
	}

	public void setNew(boolean n) {
		_batchSubject.setNew(n);
	}

	public boolean isCachedModel() {
		return _batchSubject.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_batchSubject.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _batchSubject.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _batchSubject.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_batchSubject.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _batchSubject.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_batchSubject.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new BatchSubjectWrapper((BatchSubject)_batchSubject.clone());
	}

	public int compareTo(
		info.diit.portal.batch.subject.model.BatchSubject batchSubject) {
		return _batchSubject.compareTo(batchSubject);
	}

	@Override
	public int hashCode() {
		return _batchSubject.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.batch.subject.model.BatchSubject> toCacheModel() {
		return _batchSubject.toCacheModel();
	}

	public info.diit.portal.batch.subject.model.BatchSubject toEscapedModel() {
		return new BatchSubjectWrapper(_batchSubject.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _batchSubject.toString();
	}

	public java.lang.String toXmlString() {
		return _batchSubject.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_batchSubject.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public BatchSubject getWrappedBatchSubject() {
		return _batchSubject;
	}

	public BatchSubject getWrappedModel() {
		return _batchSubject;
	}

	public void resetOriginalValues() {
		_batchSubject.resetOriginalValues();
	}

	private BatchSubject _batchSubject;
}
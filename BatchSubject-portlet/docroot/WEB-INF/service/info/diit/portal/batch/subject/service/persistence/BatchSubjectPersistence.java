/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.batch.subject.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.batch.subject.model.BatchSubject;

/**
 * The persistence interface for the batch subject service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author saeid
 * @see BatchSubjectPersistenceImpl
 * @see BatchSubjectUtil
 * @generated
 */
public interface BatchSubjectPersistence extends BasePersistence<BatchSubject> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BatchSubjectUtil} to access the batch subject persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the batch subject in the entity cache if it is enabled.
	*
	* @param batchSubject the batch subject
	*/
	public void cacheResult(
		info.diit.portal.batch.subject.model.BatchSubject batchSubject);

	/**
	* Caches the batch subjects in the entity cache if it is enabled.
	*
	* @param batchSubjects the batch subjects
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.batch.subject.model.BatchSubject> batchSubjects);

	/**
	* Creates a new batch subject with the primary key. Does not add the batch subject to the database.
	*
	* @param batchSubjectId the primary key for the new batch subject
	* @return the new batch subject
	*/
	public info.diit.portal.batch.subject.model.BatchSubject create(
		long batchSubjectId);

	/**
	* Removes the batch subject with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchSubjectId the primary key of the batch subject
	* @return the batch subject that was removed
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject remove(
		long batchSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	public info.diit.portal.batch.subject.model.BatchSubject updateImpl(
		info.diit.portal.batch.subject.model.BatchSubject batchSubject,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch subject with the primary key or throws a {@link info.diit.portal.batch.subject.NoSuchBatchSubjectException} if it could not be found.
	*
	* @param batchSubjectId the primary key of the batch subject
	* @return the batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject findByPrimaryKey(
		long batchSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns the batch subject with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param batchSubjectId the primary key of the batch subject
	* @return the batch subject, or <code>null</code> if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject fetchByPrimaryKey(
		long batchSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @return the matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findByBatchSubjectsByOrgCourseBatch(
		long organizationId, long courseId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @return the range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findByBatchSubjectsByOrgCourseBatch(
		long organizationId, long courseId, long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findByBatchSubjectsByOrgCourseBatch(
		long organizationId, long courseId, long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject findByBatchSubjectsByOrgCourseBatch_First(
		long organizationId, long courseId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns the first batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject fetchByBatchSubjectsByOrgCourseBatch_First(
		long organizationId, long courseId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject findByBatchSubjectsByOrgCourseBatch_Last(
		long organizationId, long courseId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns the last batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject fetchByBatchSubjectsByOrgCourseBatch_Last(
		long organizationId, long courseId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch subjects before and after the current batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* @param batchSubjectId the primary key of the current batch subject
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject[] findByBatchSubjectsByOrgCourseBatch_PrevAndNext(
		long batchSubjectId, long organizationId, long courseId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns all the batch subjects where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @return the matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findByteacherId(
		long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch subjects where teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param teacherId the teacher ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @return the range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findByteacherId(
		long teacherId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch subjects where teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param teacherId the teacher ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findByteacherId(
		long teacherId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch subject in the ordered set where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject findByteacherId_First(
		long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns the first batch subject in the ordered set where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject fetchByteacherId_First(
		long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch subject in the ordered set where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject findByteacherId_Last(
		long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns the last batch subject in the ordered set where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject fetchByteacherId_Last(
		long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch subjects before and after the current batch subject in the ordered set where teacherId = &#63;.
	*
	* @param batchSubjectId the primary key of the current batch subject
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject[] findByteacherId_PrevAndNext(
		long batchSubjectId, long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns all the batch subjects where batchId = &#63; and teacherId = &#63;.
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @return the matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findBybatch(
		long batchId, long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch subjects where batchId = &#63; and teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @return the range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findBybatch(
		long batchId, long teacherId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch subjects where batchId = &#63; and teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findBybatch(
		long batchId, long teacherId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject findBybatch_First(
		long batchId, long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns the first batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject fetchBybatch_First(
		long batchId, long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject findBybatch_Last(
		long batchId, long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns the last batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject fetchBybatch_Last(
		long batchId, long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch subjects before and after the current batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	*
	* @param batchSubjectId the primary key of the current batch subject
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject[] findBybatch_PrevAndNext(
		long batchSubjectId, long batchId, long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns all the batch subjects where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findBybatchSubject(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch subjects where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @return the range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findBybatchSubject(
		long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch subjects where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findBybatchSubject(
		long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch subject in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject findBybatchSubject_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns the first batch subject in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject fetchBybatchSubject_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch subject in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject findBybatchSubject_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns the last batch subject in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject fetchBybatchSubject_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch subjects before and after the current batch subject in the ordered set where batchId = &#63;.
	*
	* @param batchSubjectId the primary key of the current batch subject
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject[] findBybatchSubject_PrevAndNext(
		long batchSubjectId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns the batch subject where subjectId = &#63; or throws a {@link info.diit.portal.batch.subject.NoSuchBatchSubjectException} if it could not be found.
	*
	* @param subjectId the subject ID
	* @return the matching batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject findBysubject(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns the batch subject where subjectId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param subjectId the subject ID
	* @return the matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject fetchBysubject(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch subject where subjectId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param subjectId the subject ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject fetchBysubject(
		long subjectId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the batch subjects where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch subjects where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @return the range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch subjects where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns the first batch subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns the last batch subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch subjects before and after the current batch subject in the ordered set where organizationId = &#63;.
	*
	* @param batchSubjectId the primary key of the current batch subject
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch subject
	* @throws info.diit.portal.batch.subject.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject[] findByorganization_PrevAndNext(
		long batchSubjectId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Returns all the batch subjects.
	*
	* @return the batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch subjects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @return the range of batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch subjects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.subject.model.BatchSubject> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBatchSubjectsByOrgCourseBatch(long organizationId,
		long courseId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch subjects where teacherId = &#63; from the database.
	*
	* @param teacherId the teacher ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByteacherId(long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch subjects where batchId = &#63; and teacherId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBybatch(long batchId, long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch subjects where batchId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBybatchSubject(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the batch subject where subjectId = &#63; from the database.
	*
	* @param subjectId the subject ID
	* @return the batch subject that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.subject.model.BatchSubject removeBysubject(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.subject.NoSuchBatchSubjectException;

	/**
	* Removes all the batch subjects where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch subjects from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @return the number of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public int countByBatchSubjectsByOrgCourseBatch(long organizationId,
		long courseId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch subjects where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @return the number of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public int countByteacherId(long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch subjects where batchId = &#63; and teacherId = &#63;.
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @return the number of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public int countBybatch(long batchId, long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch subjects where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the number of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public int countBybatchSubject(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch subjects where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @return the number of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public int countBysubject(long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch subjects where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch subjects.
	*
	* @return the number of batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.batch.subject.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link info.diit.portal.batch.subject.service.http.BatchSubjectServiceSoap}.
 *
 * @author    saeid
 * @see       info.diit.portal.batch.subject.service.http.BatchSubjectServiceSoap
 * @generated
 */
public class BatchSubjectSoap implements Serializable {
	public static BatchSubjectSoap toSoapModel(BatchSubject model) {
		BatchSubjectSoap soapModel = new BatchSubjectSoap();

		soapModel.setBatchSubjectId(model.getBatchSubjectId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCourseId(model.getCourseId());
		soapModel.setBatchId(model.getBatchId());
		soapModel.setSubjectId(model.getSubjectId());
		soapModel.setTeacherId(model.getTeacherId());
		soapModel.setStartDate(model.getStartDate());
		soapModel.setHours(model.getHours());

		return soapModel;
	}

	public static BatchSubjectSoap[] toSoapModels(BatchSubject[] models) {
		BatchSubjectSoap[] soapModels = new BatchSubjectSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BatchSubjectSoap[][] toSoapModels(BatchSubject[][] models) {
		BatchSubjectSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BatchSubjectSoap[models.length][models[0].length];
		}
		else {
			soapModels = new BatchSubjectSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BatchSubjectSoap[] toSoapModels(List<BatchSubject> models) {
		List<BatchSubjectSoap> soapModels = new ArrayList<BatchSubjectSoap>(models.size());

		for (BatchSubject model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BatchSubjectSoap[soapModels.size()]);
	}

	public BatchSubjectSoap() {
	}

	public long getPrimaryKey() {
		return _batchSubjectId;
	}

	public void setPrimaryKey(long pk) {
		setBatchSubjectId(pk);
	}

	public long getBatchSubjectId() {
		return _batchSubjectId;
	}

	public void setBatchSubjectId(long batchSubjectId) {
		_batchSubjectId = batchSubjectId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getCourseId() {
		return _courseId;
	}

	public void setCourseId(long courseId) {
		_courseId = courseId;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public long getSubjectId() {
		return _subjectId;
	}

	public void setSubjectId(long subjectId) {
		_subjectId = subjectId;
	}

	public long getTeacherId() {
		return _teacherId;
	}

	public void setTeacherId(long teacherId) {
		_teacherId = teacherId;
	}

	public Date getStartDate() {
		return _startDate;
	}

	public void setStartDate(Date startDate) {
		_startDate = startDate;
	}

	public int getHours() {
		return _hours;
	}

	public void setHours(int hours) {
		_hours = hours;
	}

	private long _batchSubjectId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _courseId;
	private long _batchId;
	private long _subjectId;
	private long _teacherId;
	private Date _startDate;
	private int _hours;
}
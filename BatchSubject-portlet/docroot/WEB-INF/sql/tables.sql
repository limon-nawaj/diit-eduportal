create table EduPortal_BatchSubject_BatchSubject (
	batchSubjectId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	courseId LONG,
	batchId LONG,
	subjectId LONG,
	teacherId LONG,
	startDate DATE null,
	hours INTEGER
);
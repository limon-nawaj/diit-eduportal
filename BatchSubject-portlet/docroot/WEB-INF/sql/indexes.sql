create index IX_82CBAD03 on BatchSubject_EduPortal_BatchSubject (batchId);
create index IX_E2DF7C2A on BatchSubject_EduPortal_BatchSubject (batchId, teacherId);
create index IX_CC1CF7D7 on BatchSubject_EduPortal_BatchSubject (organizationId, courseId, batchId);
create index IX_7756B6AB on BatchSubject_EduPortal_BatchSubject (teacherId);

create index IX_6736E4DD on EduPortal_BatchSubject_BatchSubject (batchId);
create index IX_5562B810 on EduPortal_BatchSubject_BatchSubject (batchId, teacherId);
create index IX_4F4997AA on EduPortal_BatchSubject_BatchSubject (organizationId);
create index IX_EA69B331 on EduPortal_BatchSubject_BatchSubject (organizationId, courseId, batchId);
create index IX_66F2344F on EduPortal_BatchSubject_BatchSubject (subjectId);
create index IX_EDD36005 on EduPortal_BatchSubject_BatchSubject (teacherId);
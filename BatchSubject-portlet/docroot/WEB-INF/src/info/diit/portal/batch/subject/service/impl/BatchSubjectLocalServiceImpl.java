/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.batch.subject.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import info.diit.portal.batch.subject.NoSuchBatchSubjectException;
import info.diit.portal.batch.subject.model.BatchSubject;
import info.diit.portal.batch.subject.service.BatchSubjectLocalServiceUtil;
import info.diit.portal.batch.subject.service.base.BatchSubjectLocalServiceBaseImpl;
import info.diit.portal.batch.subject.service.persistence.BatchSubjectUtil;

/**
 * The implementation of the batch subject local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.batch.subject.service.BatchSubjectLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author saeid
 * @see info.diit.portal.batch.subject.service.base.BatchSubjectLocalServiceBaseImpl
 * @see info.diit.portal.batch.subject.service.BatchSubjectLocalServiceUtil
 */
public class BatchSubjectLocalServiceImpl
	extends BatchSubjectLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.batch.subject.service.BatchSubjectLocalServiceUtil} to access the batch subject local service.
	 */
	public List<BatchSubject> getBatchSubjectByOrgCourseBatch(long organizationId, long courseId, long batchId) throws SystemException{
		return BatchSubjectUtil.findByBatchSubjectsByOrgCourseBatch(organizationId, courseId, batchId);		
	}
	public void updateBatchSubject (long organizationId, long courseId, long batchId, List<BatchSubject> batchSubjects) throws SystemException{
		
		BatchSubjectUtil.removeByBatchSubjectsByOrgCourseBatch(organizationId, courseId, batchId);
		
		for(BatchSubject batchSubject :batchSubjects)
		{
			if(batchSubject.getBatchId()==batchId)
			{
				BatchSubjectLocalServiceUtil.addBatchSubject(batchSubject);
			}
		}
	}
	public List<BatchSubject> findByBatchSubjectTeacher(long teacherId) throws SystemException{
		return BatchSubjectUtil.findByteacherId(teacherId);
	}
	
	public List<BatchSubject> findByBatch(long batchId, long teacherId) throws SystemException{
		return BatchSubjectUtil.findBybatch(batchId, teacherId);
	}
	
	public List<BatchSubject> findSubjectByBatch(long batchId) throws SystemException{
		return BatchSubjectUtil.findBybatchSubject(batchId);
	}
	
	public BatchSubject findBySubject (long subjectId) throws NoSuchBatchSubjectException, SystemException{
		
		return BatchSubjectUtil.findBysubject(subjectId);
	}
	
	public List<BatchSubject> findBatchesByOrg (long organizationId) throws SystemException{
		return BatchSubjectUtil.findByorganization(organizationId);
	}
	
}
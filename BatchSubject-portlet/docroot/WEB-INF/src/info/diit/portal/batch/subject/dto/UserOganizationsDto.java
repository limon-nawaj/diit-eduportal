package info.diit.portal.batch.subject.dto;

import java.io.Serializable;

public class UserOganizationsDto implements Serializable{
	private long orgId;
	private String orgName;
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	
	public String toString(){
		return getOrgName();
	}
}

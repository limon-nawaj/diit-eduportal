package info.diit.portal.batch.subject.dto;

import java.io.Serializable;

public class SubjectDto implements Serializable {
	
	private long subjectId;
	private String subjcetName;
	
	public long getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(long subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjcetName() {
		return subjcetName;
	}
	public void setSubjcetName(String subjcetName) {
		this.subjcetName = subjcetName;
	}
	
	public String toString(){
		return getSubjcetName();
	}

}

package info.diit.portal.batch.subject.dto;

import java.io.Serializable;

public class BatchDto implements Serializable {

	private long batchId;
	private String batchName;
	public long getBatchId() {
		return batchId;
	}
	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public String toString(){
		return getBatchName();
	}
}

/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.studentattendence.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.studentattendence.model.StudentAttendance;

/**
 * The persistence interface for the student attendance service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see StudentAttendancePersistenceImpl
 * @see StudentAttendanceUtil
 * @generated
 */
public interface StudentAttendancePersistence extends BasePersistence<StudentAttendance> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link StudentAttendanceUtil} to access the student attendance persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the student attendance in the entity cache if it is enabled.
	*
	* @param studentAttendance the student attendance
	*/
	public void cacheResult(
		info.diit.portal.studentattendence.model.StudentAttendance studentAttendance);

	/**
	* Caches the student attendances in the entity cache if it is enabled.
	*
	* @param studentAttendances the student attendances
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> studentAttendances);

	/**
	* Creates a new student attendance with the primary key. Does not add the student attendance to the database.
	*
	* @param attendanceTopicId the primary key for the new student attendance
	* @return the new student attendance
	*/
	public info.diit.portal.studentattendence.model.StudentAttendance create(
		long attendanceTopicId);

	/**
	* Removes the student attendance with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceTopicId the primary key of the student attendance
	* @return the student attendance that was removed
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a student attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.StudentAttendance remove(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException;

	public info.diit.portal.studentattendence.model.StudentAttendance updateImpl(
		info.diit.portal.studentattendence.model.StudentAttendance studentAttendance,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student attendance with the primary key or throws a {@link info.diit.portal.studentattendence.NoSuchStudentAttendanceException} if it could not be found.
	*
	* @param attendanceTopicId the primary key of the student attendance
	* @return the student attendance
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a student attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.StudentAttendance findByPrimaryKey(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException;

	/**
	* Returns the student attendance with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param attendanceTopicId the primary key of the student attendance
	* @return the student attendance, or <code>null</code> if a student attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.StudentAttendance fetchByPrimaryKey(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the student attendances where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findByStudent(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the student attendances where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of student attendances
	* @param end the upper bound of the range of student attendances (not inclusive)
	* @return the range of matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findByStudent(
		long studentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the student attendances where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of student attendances
	* @param end the upper bound of the range of student attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findByStudent(
		long studentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first student attendance in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student attendance
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.StudentAttendance findByStudent_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException;

	/**
	* Returns the first student attendance in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student attendance, or <code>null</code> if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.StudentAttendance fetchByStudent_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last student attendance in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student attendance
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.StudentAttendance findByStudent_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException;

	/**
	* Returns the last student attendance in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student attendance, or <code>null</code> if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.StudentAttendance fetchByStudent_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student attendances before and after the current student attendance in the ordered set where studentId = &#63;.
	*
	* @param attendanceTopicId the primary key of the current student attendance
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next student attendance
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a student attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.StudentAttendance[] findByStudent_PrevAndNext(
		long attendanceTopicId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException;

	/**
	* Returns all the student attendances where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @return the matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findByAttendance(
		long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the student attendances where attendanceId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param attendanceId the attendance ID
	* @param start the lower bound of the range of student attendances
	* @param end the upper bound of the range of student attendances (not inclusive)
	* @return the range of matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findByAttendance(
		long attendanceId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the student attendances where attendanceId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param attendanceId the attendance ID
	* @param start the lower bound of the range of student attendances
	* @param end the upper bound of the range of student attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findByAttendance(
		long attendanceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first student attendance in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student attendance
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.StudentAttendance findByAttendance_First(
		long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException;

	/**
	* Returns the first student attendance in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student attendance, or <code>null</code> if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.StudentAttendance fetchByAttendance_First(
		long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last student attendance in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student attendance
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.StudentAttendance findByAttendance_Last(
		long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException;

	/**
	* Returns the last student attendance in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student attendance, or <code>null</code> if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.StudentAttendance fetchByAttendance_Last(
		long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student attendances before and after the current student attendance in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceTopicId the primary key of the current student attendance
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next student attendance
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a student attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.StudentAttendance[] findByAttendance_PrevAndNext(
		long attendanceTopicId, long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException;

	/**
	* Returns all the student attendances.
	*
	* @return the student attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the student attendances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student attendances
	* @param end the upper bound of the range of student attendances (not inclusive)
	* @return the range of student attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the student attendances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student attendances
	* @param end the upper bound of the range of student attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of student attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the student attendances where studentId = &#63; from the database.
	*
	* @param studentId the student ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByStudent(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the student attendances where attendanceId = &#63; from the database.
	*
	* @param attendanceId the attendance ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByAttendance(long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the student attendances from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of student attendances where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the number of matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public int countByStudent(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of student attendances where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @return the number of matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public int countByAttendance(long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of student attendances.
	*
	* @return the number of student attendances
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
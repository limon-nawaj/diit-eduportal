/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.studentattendence.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link StudentAttendance}.
 * </p>
 *
 * @author    limon
 * @see       StudentAttendance
 * @generated
 */
public class StudentAttendanceWrapper implements StudentAttendance,
	ModelWrapper<StudentAttendance> {
	public StudentAttendanceWrapper(StudentAttendance studentAttendance) {
		_studentAttendance = studentAttendance;
	}

	public Class<?> getModelClass() {
		return StudentAttendance.class;
	}

	public String getModelClassName() {
		return StudentAttendance.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("attendanceTopicId", getAttendanceTopicId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("attendanceId", getAttendanceId());
		attributes.put("studentId", getStudentId());
		attributes.put("status", getStatus());
		attributes.put("attendanceDate", getAttendanceDate());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long attendanceTopicId = (Long)attributes.get("attendanceTopicId");

		if (attendanceTopicId != null) {
			setAttendanceTopicId(attendanceTopicId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long attendanceId = (Long)attributes.get("attendanceId");

		if (attendanceId != null) {
			setAttendanceId(attendanceId);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Date attendanceDate = (Date)attributes.get("attendanceDate");

		if (attendanceDate != null) {
			setAttendanceDate(attendanceDate);
		}
	}

	/**
	* Returns the primary key of this student attendance.
	*
	* @return the primary key of this student attendance
	*/
	public long getPrimaryKey() {
		return _studentAttendance.getPrimaryKey();
	}

	/**
	* Sets the primary key of this student attendance.
	*
	* @param primaryKey the primary key of this student attendance
	*/
	public void setPrimaryKey(long primaryKey) {
		_studentAttendance.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the attendance topic ID of this student attendance.
	*
	* @return the attendance topic ID of this student attendance
	*/
	public long getAttendanceTopicId() {
		return _studentAttendance.getAttendanceTopicId();
	}

	/**
	* Sets the attendance topic ID of this student attendance.
	*
	* @param attendanceTopicId the attendance topic ID of this student attendance
	*/
	public void setAttendanceTopicId(long attendanceTopicId) {
		_studentAttendance.setAttendanceTopicId(attendanceTopicId);
	}

	/**
	* Returns the company ID of this student attendance.
	*
	* @return the company ID of this student attendance
	*/
	public long getCompanyId() {
		return _studentAttendance.getCompanyId();
	}

	/**
	* Sets the company ID of this student attendance.
	*
	* @param companyId the company ID of this student attendance
	*/
	public void setCompanyId(long companyId) {
		_studentAttendance.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this student attendance.
	*
	* @return the organization ID of this student attendance
	*/
	public long getOrganizationId() {
		return _studentAttendance.getOrganizationId();
	}

	/**
	* Sets the organization ID of this student attendance.
	*
	* @param organizationId the organization ID of this student attendance
	*/
	public void setOrganizationId(long organizationId) {
		_studentAttendance.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this student attendance.
	*
	* @return the user ID of this student attendance
	*/
	public long getUserId() {
		return _studentAttendance.getUserId();
	}

	/**
	* Sets the user ID of this student attendance.
	*
	* @param userId the user ID of this student attendance
	*/
	public void setUserId(long userId) {
		_studentAttendance.setUserId(userId);
	}

	/**
	* Returns the user uuid of this student attendance.
	*
	* @return the user uuid of this student attendance
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendance.getUserUuid();
	}

	/**
	* Sets the user uuid of this student attendance.
	*
	* @param userUuid the user uuid of this student attendance
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_studentAttendance.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this student attendance.
	*
	* @return the user name of this student attendance
	*/
	public java.lang.String getUserName() {
		return _studentAttendance.getUserName();
	}

	/**
	* Sets the user name of this student attendance.
	*
	* @param userName the user name of this student attendance
	*/
	public void setUserName(java.lang.String userName) {
		_studentAttendance.setUserName(userName);
	}

	/**
	* Returns the create date of this student attendance.
	*
	* @return the create date of this student attendance
	*/
	public java.util.Date getCreateDate() {
		return _studentAttendance.getCreateDate();
	}

	/**
	* Sets the create date of this student attendance.
	*
	* @param createDate the create date of this student attendance
	*/
	public void setCreateDate(java.util.Date createDate) {
		_studentAttendance.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this student attendance.
	*
	* @return the modified date of this student attendance
	*/
	public java.util.Date getModifiedDate() {
		return _studentAttendance.getModifiedDate();
	}

	/**
	* Sets the modified date of this student attendance.
	*
	* @param modifiedDate the modified date of this student attendance
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_studentAttendance.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the attendance ID of this student attendance.
	*
	* @return the attendance ID of this student attendance
	*/
	public long getAttendanceId() {
		return _studentAttendance.getAttendanceId();
	}

	/**
	* Sets the attendance ID of this student attendance.
	*
	* @param attendanceId the attendance ID of this student attendance
	*/
	public void setAttendanceId(long attendanceId) {
		_studentAttendance.setAttendanceId(attendanceId);
	}

	/**
	* Returns the student ID of this student attendance.
	*
	* @return the student ID of this student attendance
	*/
	public long getStudentId() {
		return _studentAttendance.getStudentId();
	}

	/**
	* Sets the student ID of this student attendance.
	*
	* @param studentId the student ID of this student attendance
	*/
	public void setStudentId(long studentId) {
		_studentAttendance.setStudentId(studentId);
	}

	/**
	* Returns the status of this student attendance.
	*
	* @return the status of this student attendance
	*/
	public int getStatus() {
		return _studentAttendance.getStatus();
	}

	/**
	* Sets the status of this student attendance.
	*
	* @param status the status of this student attendance
	*/
	public void setStatus(int status) {
		_studentAttendance.setStatus(status);
	}

	/**
	* Returns the attendance date of this student attendance.
	*
	* @return the attendance date of this student attendance
	*/
	public java.util.Date getAttendanceDate() {
		return _studentAttendance.getAttendanceDate();
	}

	/**
	* Sets the attendance date of this student attendance.
	*
	* @param attendanceDate the attendance date of this student attendance
	*/
	public void setAttendanceDate(java.util.Date attendanceDate) {
		_studentAttendance.setAttendanceDate(attendanceDate);
	}

	public boolean isNew() {
		return _studentAttendance.isNew();
	}

	public void setNew(boolean n) {
		_studentAttendance.setNew(n);
	}

	public boolean isCachedModel() {
		return _studentAttendance.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_studentAttendance.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _studentAttendance.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _studentAttendance.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_studentAttendance.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _studentAttendance.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_studentAttendance.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new StudentAttendanceWrapper((StudentAttendance)_studentAttendance.clone());
	}

	public int compareTo(
		info.diit.portal.studentattendence.model.StudentAttendance studentAttendance) {
		return _studentAttendance.compareTo(studentAttendance);
	}

	@Override
	public int hashCode() {
		return _studentAttendance.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.studentattendence.model.StudentAttendance> toCacheModel() {
		return _studentAttendance.toCacheModel();
	}

	public info.diit.portal.studentattendence.model.StudentAttendance toEscapedModel() {
		return new StudentAttendanceWrapper(_studentAttendance.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _studentAttendance.toString();
	}

	public java.lang.String toXmlString() {
		return _studentAttendance.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_studentAttendance.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public StudentAttendance getWrappedStudentAttendance() {
		return _studentAttendance;
	}

	public StudentAttendance getWrappedModel() {
		return _studentAttendance;
	}

	public void resetOriginalValues() {
		_studentAttendance.resetOriginalValues();
	}

	private StudentAttendance _studentAttendance;
}
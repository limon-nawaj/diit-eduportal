/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.studentattendence.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.studentattendence.model.StudentAttendance;

import java.util.List;

/**
 * The persistence utility for the student attendance service. This utility wraps {@link StudentAttendancePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see StudentAttendancePersistence
 * @see StudentAttendancePersistenceImpl
 * @generated
 */
public class StudentAttendanceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(StudentAttendance studentAttendance) {
		getPersistence().clearCache(studentAttendance);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<StudentAttendance> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<StudentAttendance> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<StudentAttendance> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static StudentAttendance update(
		StudentAttendance studentAttendance, boolean merge)
		throws SystemException {
		return getPersistence().update(studentAttendance, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static StudentAttendance update(
		StudentAttendance studentAttendance, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(studentAttendance, merge, serviceContext);
	}

	/**
	* Caches the student attendance in the entity cache if it is enabled.
	*
	* @param studentAttendance the student attendance
	*/
	public static void cacheResult(
		info.diit.portal.studentattendence.model.StudentAttendance studentAttendance) {
		getPersistence().cacheResult(studentAttendance);
	}

	/**
	* Caches the student attendances in the entity cache if it is enabled.
	*
	* @param studentAttendances the student attendances
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> studentAttendances) {
		getPersistence().cacheResult(studentAttendances);
	}

	/**
	* Creates a new student attendance with the primary key. Does not add the student attendance to the database.
	*
	* @param attendanceTopicId the primary key for the new student attendance
	* @return the new student attendance
	*/
	public static info.diit.portal.studentattendence.model.StudentAttendance create(
		long attendanceTopicId) {
		return getPersistence().create(attendanceTopicId);
	}

	/**
	* Removes the student attendance with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceTopicId the primary key of the student attendance
	* @return the student attendance that was removed
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a student attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.StudentAttendance remove(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException {
		return getPersistence().remove(attendanceTopicId);
	}

	public static info.diit.portal.studentattendence.model.StudentAttendance updateImpl(
		info.diit.portal.studentattendence.model.StudentAttendance studentAttendance,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(studentAttendance, merge);
	}

	/**
	* Returns the student attendance with the primary key or throws a {@link info.diit.portal.studentattendence.NoSuchStudentAttendanceException} if it could not be found.
	*
	* @param attendanceTopicId the primary key of the student attendance
	* @return the student attendance
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a student attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.StudentAttendance findByPrimaryKey(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException {
		return getPersistence().findByPrimaryKey(attendanceTopicId);
	}

	/**
	* Returns the student attendance with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param attendanceTopicId the primary key of the student attendance
	* @return the student attendance, or <code>null</code> if a student attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.StudentAttendance fetchByPrimaryKey(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(attendanceTopicId);
	}

	/**
	* Returns all the student attendances where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findByStudent(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStudent(studentId);
	}

	/**
	* Returns a range of all the student attendances where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of student attendances
	* @param end the upper bound of the range of student attendances (not inclusive)
	* @return the range of matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findByStudent(
		long studentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStudent(studentId, start, end);
	}

	/**
	* Returns an ordered range of all the student attendances where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of student attendances
	* @param end the upper bound of the range of student attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findByStudent(
		long studentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudent(studentId, start, end, orderByComparator);
	}

	/**
	* Returns the first student attendance in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student attendance
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.StudentAttendance findByStudent_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException {
		return getPersistence().findByStudent_First(studentId, orderByComparator);
	}

	/**
	* Returns the first student attendance in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student attendance, or <code>null</code> if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.StudentAttendance fetchByStudent_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudent_First(studentId, orderByComparator);
	}

	/**
	* Returns the last student attendance in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student attendance
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.StudentAttendance findByStudent_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException {
		return getPersistence().findByStudent_Last(studentId, orderByComparator);
	}

	/**
	* Returns the last student attendance in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student attendance, or <code>null</code> if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.StudentAttendance fetchByStudent_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByStudent_Last(studentId, orderByComparator);
	}

	/**
	* Returns the student attendances before and after the current student attendance in the ordered set where studentId = &#63;.
	*
	* @param attendanceTopicId the primary key of the current student attendance
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next student attendance
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a student attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.StudentAttendance[] findByStudent_PrevAndNext(
		long attendanceTopicId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException {
		return getPersistence()
				   .findByStudent_PrevAndNext(attendanceTopicId, studentId,
			orderByComparator);
	}

	/**
	* Returns all the student attendances where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @return the matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findByAttendance(
		long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByAttendance(attendanceId);
	}

	/**
	* Returns a range of all the student attendances where attendanceId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param attendanceId the attendance ID
	* @param start the lower bound of the range of student attendances
	* @param end the upper bound of the range of student attendances (not inclusive)
	* @return the range of matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findByAttendance(
		long attendanceId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByAttendance(attendanceId, start, end);
	}

	/**
	* Returns an ordered range of all the student attendances where attendanceId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param attendanceId the attendance ID
	* @param start the lower bound of the range of student attendances
	* @param end the upper bound of the range of student attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findByAttendance(
		long attendanceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByAttendance(attendanceId, start, end, orderByComparator);
	}

	/**
	* Returns the first student attendance in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student attendance
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.StudentAttendance findByAttendance_First(
		long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException {
		return getPersistence()
				   .findByAttendance_First(attendanceId, orderByComparator);
	}

	/**
	* Returns the first student attendance in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student attendance, or <code>null</code> if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.StudentAttendance fetchByAttendance_First(
		long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByAttendance_First(attendanceId, orderByComparator);
	}

	/**
	* Returns the last student attendance in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student attendance
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.StudentAttendance findByAttendance_Last(
		long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException {
		return getPersistence()
				   .findByAttendance_Last(attendanceId, orderByComparator);
	}

	/**
	* Returns the last student attendance in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student attendance, or <code>null</code> if a matching student attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.StudentAttendance fetchByAttendance_Last(
		long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByAttendance_Last(attendanceId, orderByComparator);
	}

	/**
	* Returns the student attendances before and after the current student attendance in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceTopicId the primary key of the current student attendance
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next student attendance
	* @throws info.diit.portal.studentattendence.NoSuchStudentAttendanceException if a student attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.StudentAttendance[] findByAttendance_PrevAndNext(
		long attendanceTopicId, long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchStudentAttendanceException {
		return getPersistence()
				   .findByAttendance_PrevAndNext(attendanceTopicId,
			attendanceId, orderByComparator);
	}

	/**
	* Returns all the student attendances.
	*
	* @return the student attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the student attendances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student attendances
	* @param end the upper bound of the range of student attendances (not inclusive)
	* @return the range of student attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the student attendances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student attendances
	* @param end the upper bound of the range of student attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of student attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.studentattendence.model.StudentAttendance> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the student attendances where studentId = &#63; from the database.
	*
	* @param studentId the student ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByStudent(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByStudent(studentId);
	}

	/**
	* Removes all the student attendances where attendanceId = &#63; from the database.
	*
	* @param attendanceId the attendance ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByAttendance(long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByAttendance(attendanceId);
	}

	/**
	* Removes all the student attendances from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of student attendances where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the number of matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStudent(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStudent(studentId);
	}

	/**
	* Returns the number of student attendances where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @return the number of matching student attendances
	* @throws SystemException if a system exception occurred
	*/
	public static int countByAttendance(long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByAttendance(attendanceId);
	}

	/**
	* Returns the number of student attendances.
	*
	* @return the number of student attendances
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static StudentAttendancePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (StudentAttendancePersistence)PortletBeanLocatorUtil.locate(info.diit.portal.studentattendence.service.ClpSerializer.getServletContextName(),
					StudentAttendancePersistence.class.getName());

			ReferenceRegistry.registerReference(StudentAttendanceUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(StudentAttendancePersistence persistence) {
	}

	private static StudentAttendancePersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.studentattendence.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.studentattendence.model.AttendanceTopic;

/**
 * The persistence interface for the attendance topic service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see AttendanceTopicPersistenceImpl
 * @see AttendanceTopicUtil
 * @generated
 */
public interface AttendanceTopicPersistence extends BasePersistence<AttendanceTopic> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AttendanceTopicUtil} to access the attendance topic persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the attendance topic in the entity cache if it is enabled.
	*
	* @param attendanceTopic the attendance topic
	*/
	public void cacheResult(
		info.diit.portal.studentattendence.model.AttendanceTopic attendanceTopic);

	/**
	* Caches the attendance topics in the entity cache if it is enabled.
	*
	* @param attendanceTopics the attendance topics
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.studentattendence.model.AttendanceTopic> attendanceTopics);

	/**
	* Creates a new attendance topic with the primary key. Does not add the attendance topic to the database.
	*
	* @param attendanceTopicId the primary key for the new attendance topic
	* @return the new attendance topic
	*/
	public info.diit.portal.studentattendence.model.AttendanceTopic create(
		long attendanceTopicId);

	/**
	* Removes the attendance topic with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceTopicId the primary key of the attendance topic
	* @return the attendance topic that was removed
	* @throws info.diit.portal.studentattendence.NoSuchAttendanceTopicException if a attendance topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.AttendanceTopic remove(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchAttendanceTopicException;

	public info.diit.portal.studentattendence.model.AttendanceTopic updateImpl(
		info.diit.portal.studentattendence.model.AttendanceTopic attendanceTopic,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the attendance topic with the primary key or throws a {@link info.diit.portal.studentattendence.NoSuchAttendanceTopicException} if it could not be found.
	*
	* @param attendanceTopicId the primary key of the attendance topic
	* @return the attendance topic
	* @throws info.diit.portal.studentattendence.NoSuchAttendanceTopicException if a attendance topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.AttendanceTopic findByPrimaryKey(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchAttendanceTopicException;

	/**
	* Returns the attendance topic with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param attendanceTopicId the primary key of the attendance topic
	* @return the attendance topic, or <code>null</code> if a attendance topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.AttendanceTopic fetchByPrimaryKey(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the attendance topics where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @return the matching attendance topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.AttendanceTopic> findByAttendance(
		long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the attendance topics where attendanceId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param attendanceId the attendance ID
	* @param start the lower bound of the range of attendance topics
	* @param end the upper bound of the range of attendance topics (not inclusive)
	* @return the range of matching attendance topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.AttendanceTopic> findByAttendance(
		long attendanceId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the attendance topics where attendanceId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param attendanceId the attendance ID
	* @param start the lower bound of the range of attendance topics
	* @param end the upper bound of the range of attendance topics (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching attendance topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.AttendanceTopic> findByAttendance(
		long attendanceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first attendance topic in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance topic
	* @throws info.diit.portal.studentattendence.NoSuchAttendanceTopicException if a matching attendance topic could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.AttendanceTopic findByAttendance_First(
		long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchAttendanceTopicException;

	/**
	* Returns the first attendance topic in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance topic, or <code>null</code> if a matching attendance topic could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.AttendanceTopic fetchByAttendance_First(
		long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last attendance topic in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance topic
	* @throws info.diit.portal.studentattendence.NoSuchAttendanceTopicException if a matching attendance topic could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.AttendanceTopic findByAttendance_Last(
		long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchAttendanceTopicException;

	/**
	* Returns the last attendance topic in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance topic, or <code>null</code> if a matching attendance topic could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.AttendanceTopic fetchByAttendance_Last(
		long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the attendance topics before and after the current attendance topic in the ordered set where attendanceId = &#63;.
	*
	* @param attendanceTopicId the primary key of the current attendance topic
	* @param attendanceId the attendance ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next attendance topic
	* @throws info.diit.portal.studentattendence.NoSuchAttendanceTopicException if a attendance topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.AttendanceTopic[] findByAttendance_PrevAndNext(
		long attendanceTopicId, long attendanceId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchAttendanceTopicException;

	/**
	* Returns all the attendance topics.
	*
	* @return the attendance topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.AttendanceTopic> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the attendance topics.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attendance topics
	* @param end the upper bound of the range of attendance topics (not inclusive)
	* @return the range of attendance topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.AttendanceTopic> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the attendance topics.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attendance topics
	* @param end the upper bound of the range of attendance topics (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of attendance topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.AttendanceTopic> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the attendance topics where attendanceId = &#63; from the database.
	*
	* @param attendanceId the attendance ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByAttendance(long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the attendance topics from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of attendance topics where attendanceId = &#63;.
	*
	* @param attendanceId the attendance ID
	* @return the number of matching attendance topics
	* @throws SystemException if a system exception occurred
	*/
	public int countByAttendance(long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of attendance topics.
	*
	* @return the number of attendance topics
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
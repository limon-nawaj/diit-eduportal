/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.studentattendence.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Attendance}.
 * </p>
 *
 * @author    limon
 * @see       Attendance
 * @generated
 */
public class AttendanceWrapper implements Attendance, ModelWrapper<Attendance> {
	public AttendanceWrapper(Attendance attendance) {
		_attendance = attendance;
	}

	public Class<?> getModelClass() {
		return Attendance.class;
	}

	public String getModelClassName() {
		return Attendance.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("attendanceId", getAttendanceId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("campus", getCampus());
		attributes.put("attendanceDate", getAttendanceDate());
		attributes.put("attendanceRate", getAttendanceRate());
		attributes.put("batch", getBatch());
		attributes.put("subject", getSubject());
		attributes.put("routineIn", getRoutineIn());
		attributes.put("routineOut", getRoutineOut());
		attributes.put("actualIn", getActualIn());
		attributes.put("actualOut", getActualOut());
		attributes.put("routineDuration", getRoutineDuration());
		attributes.put("actualDuration", getActualDuration());
		attributes.put("lateEntry", getLateEntry());
		attributes.put("lagTime", getLagTime());
		attributes.put("note", getNote());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long attendanceId = (Long)attributes.get("attendanceId");

		if (attendanceId != null) {
			setAttendanceId(attendanceId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long campus = (Long)attributes.get("campus");

		if (campus != null) {
			setCampus(campus);
		}

		Date attendanceDate = (Date)attributes.get("attendanceDate");

		if (attendanceDate != null) {
			setAttendanceDate(attendanceDate);
		}

		Long attendanceRate = (Long)attributes.get("attendanceRate");

		if (attendanceRate != null) {
			setAttendanceRate(attendanceRate);
		}

		Long batch = (Long)attributes.get("batch");

		if (batch != null) {
			setBatch(batch);
		}

		Long subject = (Long)attributes.get("subject");

		if (subject != null) {
			setSubject(subject);
		}

		Date routineIn = (Date)attributes.get("routineIn");

		if (routineIn != null) {
			setRoutineIn(routineIn);
		}

		Date routineOut = (Date)attributes.get("routineOut");

		if (routineOut != null) {
			setRoutineOut(routineOut);
		}

		Date actualIn = (Date)attributes.get("actualIn");

		if (actualIn != null) {
			setActualIn(actualIn);
		}

		Date actualOut = (Date)attributes.get("actualOut");

		if (actualOut != null) {
			setActualOut(actualOut);
		}

		Long routineDuration = (Long)attributes.get("routineDuration");

		if (routineDuration != null) {
			setRoutineDuration(routineDuration);
		}

		Long actualDuration = (Long)attributes.get("actualDuration");

		if (actualDuration != null) {
			setActualDuration(actualDuration);
		}

		Long lateEntry = (Long)attributes.get("lateEntry");

		if (lateEntry != null) {
			setLateEntry(lateEntry);
		}

		Long lagTime = (Long)attributes.get("lagTime");

		if (lagTime != null) {
			setLagTime(lagTime);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}
	}

	/**
	* Returns the primary key of this attendance.
	*
	* @return the primary key of this attendance
	*/
	public long getPrimaryKey() {
		return _attendance.getPrimaryKey();
	}

	/**
	* Sets the primary key of this attendance.
	*
	* @param primaryKey the primary key of this attendance
	*/
	public void setPrimaryKey(long primaryKey) {
		_attendance.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the attendance ID of this attendance.
	*
	* @return the attendance ID of this attendance
	*/
	public long getAttendanceId() {
		return _attendance.getAttendanceId();
	}

	/**
	* Sets the attendance ID of this attendance.
	*
	* @param attendanceId the attendance ID of this attendance
	*/
	public void setAttendanceId(long attendanceId) {
		_attendance.setAttendanceId(attendanceId);
	}

	/**
	* Returns the company ID of this attendance.
	*
	* @return the company ID of this attendance
	*/
	public long getCompanyId() {
		return _attendance.getCompanyId();
	}

	/**
	* Sets the company ID of this attendance.
	*
	* @param companyId the company ID of this attendance
	*/
	public void setCompanyId(long companyId) {
		_attendance.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this attendance.
	*
	* @return the user ID of this attendance
	*/
	public long getUserId() {
		return _attendance.getUserId();
	}

	/**
	* Sets the user ID of this attendance.
	*
	* @param userId the user ID of this attendance
	*/
	public void setUserId(long userId) {
		_attendance.setUserId(userId);
	}

	/**
	* Returns the user uuid of this attendance.
	*
	* @return the user uuid of this attendance
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendance.getUserUuid();
	}

	/**
	* Sets the user uuid of this attendance.
	*
	* @param userUuid the user uuid of this attendance
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_attendance.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this attendance.
	*
	* @return the user name of this attendance
	*/
	public java.lang.String getUserName() {
		return _attendance.getUserName();
	}

	/**
	* Sets the user name of this attendance.
	*
	* @param userName the user name of this attendance
	*/
	public void setUserName(java.lang.String userName) {
		_attendance.setUserName(userName);
	}

	/**
	* Returns the create date of this attendance.
	*
	* @return the create date of this attendance
	*/
	public java.util.Date getCreateDate() {
		return _attendance.getCreateDate();
	}

	/**
	* Sets the create date of this attendance.
	*
	* @param createDate the create date of this attendance
	*/
	public void setCreateDate(java.util.Date createDate) {
		_attendance.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this attendance.
	*
	* @return the modified date of this attendance
	*/
	public java.util.Date getModifiedDate() {
		return _attendance.getModifiedDate();
	}

	/**
	* Sets the modified date of this attendance.
	*
	* @param modifiedDate the modified date of this attendance
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_attendance.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the campus of this attendance.
	*
	* @return the campus of this attendance
	*/
	public long getCampus() {
		return _attendance.getCampus();
	}

	/**
	* Sets the campus of this attendance.
	*
	* @param campus the campus of this attendance
	*/
	public void setCampus(long campus) {
		_attendance.setCampus(campus);
	}

	/**
	* Returns the attendance date of this attendance.
	*
	* @return the attendance date of this attendance
	*/
	public java.util.Date getAttendanceDate() {
		return _attendance.getAttendanceDate();
	}

	/**
	* Sets the attendance date of this attendance.
	*
	* @param attendanceDate the attendance date of this attendance
	*/
	public void setAttendanceDate(java.util.Date attendanceDate) {
		_attendance.setAttendanceDate(attendanceDate);
	}

	/**
	* Returns the attendance rate of this attendance.
	*
	* @return the attendance rate of this attendance
	*/
	public long getAttendanceRate() {
		return _attendance.getAttendanceRate();
	}

	/**
	* Sets the attendance rate of this attendance.
	*
	* @param attendanceRate the attendance rate of this attendance
	*/
	public void setAttendanceRate(long attendanceRate) {
		_attendance.setAttendanceRate(attendanceRate);
	}

	/**
	* Returns the batch of this attendance.
	*
	* @return the batch of this attendance
	*/
	public long getBatch() {
		return _attendance.getBatch();
	}

	/**
	* Sets the batch of this attendance.
	*
	* @param batch the batch of this attendance
	*/
	public void setBatch(long batch) {
		_attendance.setBatch(batch);
	}

	/**
	* Returns the subject of this attendance.
	*
	* @return the subject of this attendance
	*/
	public long getSubject() {
		return _attendance.getSubject();
	}

	/**
	* Sets the subject of this attendance.
	*
	* @param subject the subject of this attendance
	*/
	public void setSubject(long subject) {
		_attendance.setSubject(subject);
	}

	/**
	* Returns the routine in of this attendance.
	*
	* @return the routine in of this attendance
	*/
	public java.util.Date getRoutineIn() {
		return _attendance.getRoutineIn();
	}

	/**
	* Sets the routine in of this attendance.
	*
	* @param routineIn the routine in of this attendance
	*/
	public void setRoutineIn(java.util.Date routineIn) {
		_attendance.setRoutineIn(routineIn);
	}

	/**
	* Returns the routine out of this attendance.
	*
	* @return the routine out of this attendance
	*/
	public java.util.Date getRoutineOut() {
		return _attendance.getRoutineOut();
	}

	/**
	* Sets the routine out of this attendance.
	*
	* @param routineOut the routine out of this attendance
	*/
	public void setRoutineOut(java.util.Date routineOut) {
		_attendance.setRoutineOut(routineOut);
	}

	/**
	* Returns the actual in of this attendance.
	*
	* @return the actual in of this attendance
	*/
	public java.util.Date getActualIn() {
		return _attendance.getActualIn();
	}

	/**
	* Sets the actual in of this attendance.
	*
	* @param actualIn the actual in of this attendance
	*/
	public void setActualIn(java.util.Date actualIn) {
		_attendance.setActualIn(actualIn);
	}

	/**
	* Returns the actual out of this attendance.
	*
	* @return the actual out of this attendance
	*/
	public java.util.Date getActualOut() {
		return _attendance.getActualOut();
	}

	/**
	* Sets the actual out of this attendance.
	*
	* @param actualOut the actual out of this attendance
	*/
	public void setActualOut(java.util.Date actualOut) {
		_attendance.setActualOut(actualOut);
	}

	/**
	* Returns the routine duration of this attendance.
	*
	* @return the routine duration of this attendance
	*/
	public long getRoutineDuration() {
		return _attendance.getRoutineDuration();
	}

	/**
	* Sets the routine duration of this attendance.
	*
	* @param routineDuration the routine duration of this attendance
	*/
	public void setRoutineDuration(long routineDuration) {
		_attendance.setRoutineDuration(routineDuration);
	}

	/**
	* Returns the actual duration of this attendance.
	*
	* @return the actual duration of this attendance
	*/
	public long getActualDuration() {
		return _attendance.getActualDuration();
	}

	/**
	* Sets the actual duration of this attendance.
	*
	* @param actualDuration the actual duration of this attendance
	*/
	public void setActualDuration(long actualDuration) {
		_attendance.setActualDuration(actualDuration);
	}

	/**
	* Returns the late entry of this attendance.
	*
	* @return the late entry of this attendance
	*/
	public long getLateEntry() {
		return _attendance.getLateEntry();
	}

	/**
	* Sets the late entry of this attendance.
	*
	* @param lateEntry the late entry of this attendance
	*/
	public void setLateEntry(long lateEntry) {
		_attendance.setLateEntry(lateEntry);
	}

	/**
	* Returns the lag time of this attendance.
	*
	* @return the lag time of this attendance
	*/
	public long getLagTime() {
		return _attendance.getLagTime();
	}

	/**
	* Sets the lag time of this attendance.
	*
	* @param lagTime the lag time of this attendance
	*/
	public void setLagTime(long lagTime) {
		_attendance.setLagTime(lagTime);
	}

	/**
	* Returns the note of this attendance.
	*
	* @return the note of this attendance
	*/
	public java.lang.String getNote() {
		return _attendance.getNote();
	}

	/**
	* Sets the note of this attendance.
	*
	* @param note the note of this attendance
	*/
	public void setNote(java.lang.String note) {
		_attendance.setNote(note);
	}

	public boolean isNew() {
		return _attendance.isNew();
	}

	public void setNew(boolean n) {
		_attendance.setNew(n);
	}

	public boolean isCachedModel() {
		return _attendance.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_attendance.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _attendance.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _attendance.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_attendance.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _attendance.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_attendance.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AttendanceWrapper((Attendance)_attendance.clone());
	}

	public int compareTo(
		info.diit.portal.studentattendence.model.Attendance attendance) {
		return _attendance.compareTo(attendance);
	}

	@Override
	public int hashCode() {
		return _attendance.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.studentattendence.model.Attendance> toCacheModel() {
		return _attendance.toCacheModel();
	}

	public info.diit.portal.studentattendence.model.Attendance toEscapedModel() {
		return new AttendanceWrapper(_attendance.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _attendance.toString();
	}

	public java.lang.String toXmlString() {
		return _attendance.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_attendance.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Attendance getWrappedAttendance() {
		return _attendance;
	}

	public Attendance getWrappedModel() {
		return _attendance;
	}

	public void resetOriginalValues() {
		_attendance.resetOriginalValues();
	}

	private Attendance _attendance;
}
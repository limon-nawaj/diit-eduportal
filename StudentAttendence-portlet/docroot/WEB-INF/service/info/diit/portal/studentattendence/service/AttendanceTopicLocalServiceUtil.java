/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.studentattendence.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the attendance topic local service. This utility wraps {@link info.diit.portal.studentattendence.service.impl.AttendanceTopicLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see AttendanceTopicLocalService
 * @see info.diit.portal.studentattendence.service.base.AttendanceTopicLocalServiceBaseImpl
 * @see info.diit.portal.studentattendence.service.impl.AttendanceTopicLocalServiceImpl
 * @generated
 */
public class AttendanceTopicLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.studentattendence.service.impl.AttendanceTopicLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the attendance topic to the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceTopic the attendance topic
	* @return the attendance topic that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.AttendanceTopic addAttendanceTopic(
		info.diit.portal.studentattendence.model.AttendanceTopic attendanceTopic)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addAttendanceTopic(attendanceTopic);
	}

	/**
	* Creates a new attendance topic with the primary key. Does not add the attendance topic to the database.
	*
	* @param attendanceTopicId the primary key for the new attendance topic
	* @return the new attendance topic
	*/
	public static info.diit.portal.studentattendence.model.AttendanceTopic createAttendanceTopic(
		long attendanceTopicId) {
		return getService().createAttendanceTopic(attendanceTopicId);
	}

	/**
	* Deletes the attendance topic with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceTopicId the primary key of the attendance topic
	* @return the attendance topic that was removed
	* @throws PortalException if a attendance topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.AttendanceTopic deleteAttendanceTopic(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteAttendanceTopic(attendanceTopicId);
	}

	/**
	* Deletes the attendance topic from the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceTopic the attendance topic
	* @return the attendance topic that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.AttendanceTopic deleteAttendanceTopic(
		info.diit.portal.studentattendence.model.AttendanceTopic attendanceTopic)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteAttendanceTopic(attendanceTopic);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.studentattendence.model.AttendanceTopic fetchAttendanceTopic(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchAttendanceTopic(attendanceTopicId);
	}

	/**
	* Returns the attendance topic with the primary key.
	*
	* @param attendanceTopicId the primary key of the attendance topic
	* @return the attendance topic
	* @throws PortalException if a attendance topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.AttendanceTopic getAttendanceTopic(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getAttendanceTopic(attendanceTopicId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the attendance topics.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attendance topics
	* @param end the upper bound of the range of attendance topics (not inclusive)
	* @return the range of attendance topics
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.studentattendence.model.AttendanceTopic> getAttendanceTopics(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getAttendanceTopics(start, end);
	}

	/**
	* Returns the number of attendance topics.
	*
	* @return the number of attendance topics
	* @throws SystemException if a system exception occurred
	*/
	public static int getAttendanceTopicsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getAttendanceTopicsCount();
	}

	/**
	* Updates the attendance topic in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param attendanceTopic the attendance topic
	* @return the attendance topic that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.AttendanceTopic updateAttendanceTopic(
		info.diit.portal.studentattendence.model.AttendanceTopic attendanceTopic)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateAttendanceTopic(attendanceTopic);
	}

	/**
	* Updates the attendance topic in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param attendanceTopic the attendance topic
	* @param merge whether to merge the attendance topic with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the attendance topic that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.studentattendence.model.AttendanceTopic updateAttendanceTopic(
		info.diit.portal.studentattendence.model.AttendanceTopic attendanceTopic,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateAttendanceTopic(attendanceTopic, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<info.diit.portal.studentattendence.model.AttendanceTopic> findByAttendence(
		long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByAttendence(attendanceId);
	}

	public static void clearService() {
		_service = null;
	}

	public static AttendanceTopicLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					AttendanceTopicLocalService.class.getName());

			if (invokableLocalService instanceof AttendanceTopicLocalService) {
				_service = (AttendanceTopicLocalService)invokableLocalService;
			}
			else {
				_service = new AttendanceTopicLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(AttendanceTopicLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(AttendanceTopicLocalService service) {
	}

	private static AttendanceTopicLocalService _service;
}
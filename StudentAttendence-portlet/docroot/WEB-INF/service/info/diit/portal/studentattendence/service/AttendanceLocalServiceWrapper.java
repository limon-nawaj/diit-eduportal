/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.studentattendence.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link AttendanceLocalService}.
 * </p>
 *
 * @author    limon
 * @see       AttendanceLocalService
 * @generated
 */
public class AttendanceLocalServiceWrapper implements AttendanceLocalService,
	ServiceWrapper<AttendanceLocalService> {
	public AttendanceLocalServiceWrapper(
		AttendanceLocalService attendanceLocalService) {
		_attendanceLocalService = attendanceLocalService;
	}

	/**
	* Adds the attendance to the database. Also notifies the appropriate model listeners.
	*
	* @param attendance the attendance
	* @return the attendance that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.Attendance addAttendance(
		info.diit.portal.studentattendence.model.Attendance attendance)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.addAttendance(attendance);
	}

	/**
	* Creates a new attendance with the primary key. Does not add the attendance to the database.
	*
	* @param attendanceId the primary key for the new attendance
	* @return the new attendance
	*/
	public info.diit.portal.studentattendence.model.Attendance createAttendance(
		long attendanceId) {
		return _attendanceLocalService.createAttendance(attendanceId);
	}

	/**
	* Deletes the attendance with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceId the primary key of the attendance
	* @return the attendance that was removed
	* @throws PortalException if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.Attendance deleteAttendance(
		long attendanceId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.deleteAttendance(attendanceId);
	}

	/**
	* Deletes the attendance from the database. Also notifies the appropriate model listeners.
	*
	* @param attendance the attendance
	* @return the attendance that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.Attendance deleteAttendance(
		info.diit.portal.studentattendence.model.Attendance attendance)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.deleteAttendance(attendance);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _attendanceLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.studentattendence.model.Attendance fetchAttendance(
		long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.fetchAttendance(attendanceId);
	}

	/**
	* Returns the attendance with the primary key.
	*
	* @param attendanceId the primary key of the attendance
	* @return the attendance
	* @throws PortalException if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.Attendance getAttendance(
		long attendanceId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.getAttendance(attendanceId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the attendances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @return the range of attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.studentattendence.model.Attendance> getAttendances(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.getAttendances(start, end);
	}

	/**
	* Returns the number of attendances.
	*
	* @return the number of attendances
	* @throws SystemException if a system exception occurred
	*/
	public int getAttendancesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.getAttendancesCount();
	}

	/**
	* Updates the attendance in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param attendance the attendance
	* @return the attendance that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.Attendance updateAttendance(
		info.diit.portal.studentattendence.model.Attendance attendance)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.updateAttendance(attendance);
	}

	/**
	* Updates the attendance in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param attendance the attendance
	* @param merge whether to merge the attendance with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the attendance that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.studentattendence.model.Attendance updateAttendance(
		info.diit.portal.studentattendence.model.Attendance attendance,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.updateAttendance(attendance, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _attendanceLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_attendanceLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _attendanceLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.studentattendence.model.Attendance> findByUserBatchSubject(
		long userId, long batchId, long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.findByUserBatchSubject(userId, batchId,
			subjectId);
	}

	public java.util.List<info.diit.portal.studentattendence.model.Attendance> findByBatch(
		long batch) throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.findByBatch(batch);
	}

	public info.diit.portal.studentattendence.model.Attendance findBySubjectDate(
		long subject, java.util.Date date)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.studentattendence.NoSuchAttendanceException {
		return _attendanceLocalService.findBySubjectDate(subject, date);
	}

	public java.util.List<info.diit.portal.studentattendence.model.Attendance> findByBatchDate(
		long batch, java.util.Date date)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.findByBatchDate(batch, date);
	}

	public java.util.List<info.diit.portal.studentattendence.model.Attendance> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceLocalService.findByCompany(companyId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public AttendanceLocalService getWrappedAttendanceLocalService() {
		return _attendanceLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedAttendanceLocalService(
		AttendanceLocalService attendanceLocalService) {
		_attendanceLocalService = attendanceLocalService;
	}

	public AttendanceLocalService getWrappedService() {
		return _attendanceLocalService;
	}

	public void setWrappedService(AttendanceLocalService attendanceLocalService) {
		_attendanceLocalService = attendanceLocalService;
	}

	private AttendanceLocalService _attendanceLocalService;
}
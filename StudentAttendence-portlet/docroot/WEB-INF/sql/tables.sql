create table EduPortal_Attendence_Attendance (
	attendanceId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	campus LONG,
	attendanceDate DATE null,
	attendanceRate LONG,
	batch LONG,
	subject LONG,
	routineIn DATE null,
	routineOut DATE null,
	actualIn DATE null,
	actualOut DATE null,
	routineDuration LONG,
	actualDuration LONG,
	lateEntry LONG,
	lagTime LONG,
	note STRING null
);

create table EduPortal_Attendence_AttendanceTopic (
	attendanceTopicId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	attendanceId LONG,
	topicId LONG,
	description VARCHAR(75) null
);



create table EduPortal_Attendence_StudentAttendance (
	attendanceTopicId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	attendanceId LONG,
	studentId LONG,
	status INTEGER,
	attendanceDate DATE null
);
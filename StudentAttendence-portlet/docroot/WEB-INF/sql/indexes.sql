create index IX_B9A7FD7E on EduPortal_Attendence_Attendance (batch);
create index IX_CEBD7B69 on EduPortal_Attendence_Attendance (batch, attendanceDate);
create index IX_B2C9503C on EduPortal_Attendence_Attendance (companyId);
create index IX_A945845B on EduPortal_Attendence_Attendance (subject, attendanceDate);
create index IX_D1CBB1D8 on EduPortal_Attendence_Attendance (userId, batch, subject);

create index IX_152C34C7 on EduPortal_Attendence_AttendanceTopic (attendanceId);

create index IX_C54FDBC5 on EduPortal_Attendence_StudentAttendance (attendanceId);
create index IX_F0D1A519 on EduPortal_Attendence_StudentAttendance (studentId);
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.studentattendence.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import info.diit.portal.studentattendence.model.StudentAttendance;
import info.diit.portal.studentattendence.service.base.StudentAttendanceLocalServiceBaseImpl;
import info.diit.portal.studentattendence.service.persistence.StudentAttendanceUtil;

/**
 * The implementation of the student attendance local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.studentattendence.service.StudentAttendanceLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see info.diit.portal.studentattendence.service.base.StudentAttendanceLocalServiceBaseImpl
 * @see info.diit.portal.studentattendence.service.StudentAttendanceLocalServiceUtil
 */
public class StudentAttendanceLocalServiceImpl
	extends StudentAttendanceLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.studentattendence.service.StudentAttendanceLocalServiceUtil} to access the student attendance local service.
	 */
	
	public List<StudentAttendance> findByAttendance(long attendanceId) throws SystemException{
		return StudentAttendanceUtil.findByAttendance(attendanceId);
	}
	
	public List<StudentAttendance> findByStudent(long student) throws SystemException{
		return StudentAttendanceUtil.findByStudent(student);
	}
}
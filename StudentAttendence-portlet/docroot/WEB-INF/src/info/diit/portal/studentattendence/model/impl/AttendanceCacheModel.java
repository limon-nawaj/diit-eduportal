/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.studentattendence.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.studentattendence.model.Attendance;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Attendance in entity cache.
 *
 * @author limon
 * @see Attendance
 * @generated
 */
public class AttendanceCacheModel implements CacheModel<Attendance>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(41);

		sb.append("{attendanceId=");
		sb.append(attendanceId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", campus=");
		sb.append(campus);
		sb.append(", attendanceDate=");
		sb.append(attendanceDate);
		sb.append(", attendanceRate=");
		sb.append(attendanceRate);
		sb.append(", batch=");
		sb.append(batch);
		sb.append(", subject=");
		sb.append(subject);
		sb.append(", routineIn=");
		sb.append(routineIn);
		sb.append(", routineOut=");
		sb.append(routineOut);
		sb.append(", actualIn=");
		sb.append(actualIn);
		sb.append(", actualOut=");
		sb.append(actualOut);
		sb.append(", routineDuration=");
		sb.append(routineDuration);
		sb.append(", actualDuration=");
		sb.append(actualDuration);
		sb.append(", lateEntry=");
		sb.append(lateEntry);
		sb.append(", lagTime=");
		sb.append(lagTime);
		sb.append(", note=");
		sb.append(note);
		sb.append("}");

		return sb.toString();
	}

	public Attendance toEntityModel() {
		AttendanceImpl attendanceImpl = new AttendanceImpl();

		attendanceImpl.setAttendanceId(attendanceId);
		attendanceImpl.setCompanyId(companyId);
		attendanceImpl.setUserId(userId);

		if (userName == null) {
			attendanceImpl.setUserName(StringPool.BLANK);
		}
		else {
			attendanceImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			attendanceImpl.setCreateDate(null);
		}
		else {
			attendanceImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			attendanceImpl.setModifiedDate(null);
		}
		else {
			attendanceImpl.setModifiedDate(new Date(modifiedDate));
		}

		attendanceImpl.setCampus(campus);

		if (attendanceDate == Long.MIN_VALUE) {
			attendanceImpl.setAttendanceDate(null);
		}
		else {
			attendanceImpl.setAttendanceDate(new Date(attendanceDate));
		}

		attendanceImpl.setAttendanceRate(attendanceRate);
		attendanceImpl.setBatch(batch);
		attendanceImpl.setSubject(subject);

		if (routineIn == Long.MIN_VALUE) {
			attendanceImpl.setRoutineIn(null);
		}
		else {
			attendanceImpl.setRoutineIn(new Date(routineIn));
		}

		if (routineOut == Long.MIN_VALUE) {
			attendanceImpl.setRoutineOut(null);
		}
		else {
			attendanceImpl.setRoutineOut(new Date(routineOut));
		}

		if (actualIn == Long.MIN_VALUE) {
			attendanceImpl.setActualIn(null);
		}
		else {
			attendanceImpl.setActualIn(new Date(actualIn));
		}

		if (actualOut == Long.MIN_VALUE) {
			attendanceImpl.setActualOut(null);
		}
		else {
			attendanceImpl.setActualOut(new Date(actualOut));
		}

		attendanceImpl.setRoutineDuration(routineDuration);
		attendanceImpl.setActualDuration(actualDuration);
		attendanceImpl.setLateEntry(lateEntry);
		attendanceImpl.setLagTime(lagTime);

		if (note == null) {
			attendanceImpl.setNote(StringPool.BLANK);
		}
		else {
			attendanceImpl.setNote(note);
		}

		attendanceImpl.resetOriginalValues();

		return attendanceImpl;
	}

	public long attendanceId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long campus;
	public long attendanceDate;
	public long attendanceRate;
	public long batch;
	public long subject;
	public long routineIn;
	public long routineOut;
	public long actualIn;
	public long actualOut;
	public long routineDuration;
	public long actualDuration;
	public long lateEntry;
	public long lagTime;
	public String note;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.studentattendence.service.base;

import com.liferay.counter.service.CounterLocalService;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.bean.IdentifiableBean;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.model.PersistedModel;
import com.liferay.portal.service.BaseLocalServiceImpl;
import com.liferay.portal.service.PersistedModelLocalServiceRegistryUtil;
import com.liferay.portal.service.ResourceLocalService;
import com.liferay.portal.service.ResourceService;
import com.liferay.portal.service.UserLocalService;
import com.liferay.portal.service.UserService;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;

import info.diit.portal.studentattendence.model.Attendance;
import info.diit.portal.studentattendence.service.AttendanceLocalService;
import info.diit.portal.studentattendence.service.AttendanceTopicLocalService;
import info.diit.portal.studentattendence.service.StudentAttendanceLocalService;
import info.diit.portal.studentattendence.service.persistence.AttendancePersistence;
import info.diit.portal.studentattendence.service.persistence.AttendanceTopicPersistence;
import info.diit.portal.studentattendence.service.persistence.StudentAttendancePersistence;

import java.io.Serializable;

import java.util.List;

import javax.sql.DataSource;

/**
 * The base implementation of the attendance local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link info.diit.portal.studentattendence.service.impl.AttendanceLocalServiceImpl}.
 * </p>
 *
 * @author limon
 * @see info.diit.portal.studentattendence.service.impl.AttendanceLocalServiceImpl
 * @see info.diit.portal.studentattendence.service.AttendanceLocalServiceUtil
 * @generated
 */
public abstract class AttendanceLocalServiceBaseImpl
	extends BaseLocalServiceImpl implements AttendanceLocalService,
		IdentifiableBean {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link info.diit.portal.studentattendence.service.AttendanceLocalServiceUtil} to access the attendance local service.
	 */

	/**
	 * Adds the attendance to the database. Also notifies the appropriate model listeners.
	 *
	 * @param attendance the attendance
	 * @return the attendance that was added
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Attendance addAttendance(Attendance attendance)
		throws SystemException {
		attendance.setNew(true);

		return attendancePersistence.update(attendance, false);
	}

	/**
	 * Creates a new attendance with the primary key. Does not add the attendance to the database.
	 *
	 * @param attendanceId the primary key for the new attendance
	 * @return the new attendance
	 */
	public Attendance createAttendance(long attendanceId) {
		return attendancePersistence.create(attendanceId);
	}

	/**
	 * Deletes the attendance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param attendanceId the primary key of the attendance
	 * @return the attendance that was removed
	 * @throws PortalException if a attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.DELETE)
	public Attendance deleteAttendance(long attendanceId)
		throws PortalException, SystemException {
		return attendancePersistence.remove(attendanceId);
	}

	/**
	 * Deletes the attendance from the database. Also notifies the appropriate model listeners.
	 *
	 * @param attendance the attendance
	 * @return the attendance that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.DELETE)
	public Attendance deleteAttendance(Attendance attendance)
		throws SystemException {
		return attendancePersistence.remove(attendance);
	}

	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(Attendance.class,
			clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return attendancePersistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return attendancePersistence.findWithDynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return attendancePersistence.findWithDynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	public long dynamicQueryCount(DynamicQuery dynamicQuery)
		throws SystemException {
		return attendancePersistence.countWithDynamicQuery(dynamicQuery);
	}

	public Attendance fetchAttendance(long attendanceId)
		throws SystemException {
		return attendancePersistence.fetchByPrimaryKey(attendanceId);
	}

	/**
	 * Returns the attendance with the primary key.
	 *
	 * @param attendanceId the primary key of the attendance
	 * @return the attendance
	 * @throws PortalException if a attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance getAttendance(long attendanceId)
		throws PortalException, SystemException {
		return attendancePersistence.findByPrimaryKey(attendanceId);
	}

	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException, SystemException {
		return attendancePersistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns a range of all the attendances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of attendances
	 * @param end the upper bound of the range of attendances (not inclusive)
	 * @return the range of attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> getAttendances(int start, int end)
		throws SystemException {
		return attendancePersistence.findAll(start, end);
	}

	/**
	 * Returns the number of attendances.
	 *
	 * @return the number of attendances
	 * @throws SystemException if a system exception occurred
	 */
	public int getAttendancesCount() throws SystemException {
		return attendancePersistence.countAll();
	}

	/**
	 * Updates the attendance in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param attendance the attendance
	 * @return the attendance that was updated
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Attendance updateAttendance(Attendance attendance)
		throws SystemException {
		return updateAttendance(attendance, true);
	}

	/**
	 * Updates the attendance in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param attendance the attendance
	 * @param merge whether to merge the attendance with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	 * @return the attendance that was updated
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Attendance updateAttendance(Attendance attendance, boolean merge)
		throws SystemException {
		attendance.setNew(false);

		return attendancePersistence.update(attendance, merge);
	}

	/**
	 * Returns the attendance local service.
	 *
	 * @return the attendance local service
	 */
	public AttendanceLocalService getAttendanceLocalService() {
		return attendanceLocalService;
	}

	/**
	 * Sets the attendance local service.
	 *
	 * @param attendanceLocalService the attendance local service
	 */
	public void setAttendanceLocalService(
		AttendanceLocalService attendanceLocalService) {
		this.attendanceLocalService = attendanceLocalService;
	}

	/**
	 * Returns the attendance persistence.
	 *
	 * @return the attendance persistence
	 */
	public AttendancePersistence getAttendancePersistence() {
		return attendancePersistence;
	}

	/**
	 * Sets the attendance persistence.
	 *
	 * @param attendancePersistence the attendance persistence
	 */
	public void setAttendancePersistence(
		AttendancePersistence attendancePersistence) {
		this.attendancePersistence = attendancePersistence;
	}

	/**
	 * Returns the attendance topic local service.
	 *
	 * @return the attendance topic local service
	 */
	public AttendanceTopicLocalService getAttendanceTopicLocalService() {
		return attendanceTopicLocalService;
	}

	/**
	 * Sets the attendance topic local service.
	 *
	 * @param attendanceTopicLocalService the attendance topic local service
	 */
	public void setAttendanceTopicLocalService(
		AttendanceTopicLocalService attendanceTopicLocalService) {
		this.attendanceTopicLocalService = attendanceTopicLocalService;
	}

	/**
	 * Returns the attendance topic persistence.
	 *
	 * @return the attendance topic persistence
	 */
	public AttendanceTopicPersistence getAttendanceTopicPersistence() {
		return attendanceTopicPersistence;
	}

	/**
	 * Sets the attendance topic persistence.
	 *
	 * @param attendanceTopicPersistence the attendance topic persistence
	 */
	public void setAttendanceTopicPersistence(
		AttendanceTopicPersistence attendanceTopicPersistence) {
		this.attendanceTopicPersistence = attendanceTopicPersistence;
	}

	/**
	 * Returns the student attendance local service.
	 *
	 * @return the student attendance local service
	 */
	public StudentAttendanceLocalService getStudentAttendanceLocalService() {
		return studentAttendanceLocalService;
	}

	/**
	 * Sets the student attendance local service.
	 *
	 * @param studentAttendanceLocalService the student attendance local service
	 */
	public void setStudentAttendanceLocalService(
		StudentAttendanceLocalService studentAttendanceLocalService) {
		this.studentAttendanceLocalService = studentAttendanceLocalService;
	}

	/**
	 * Returns the student attendance persistence.
	 *
	 * @return the student attendance persistence
	 */
	public StudentAttendancePersistence getStudentAttendancePersistence() {
		return studentAttendancePersistence;
	}

	/**
	 * Sets the student attendance persistence.
	 *
	 * @param studentAttendancePersistence the student attendance persistence
	 */
	public void setStudentAttendancePersistence(
		StudentAttendancePersistence studentAttendancePersistence) {
		this.studentAttendancePersistence = studentAttendancePersistence;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public CounterLocalService getCounterLocalService() {
		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public ResourceLocalService getResourceLocalService() {
		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the resource remote service.
	 *
	 * @return the resource remote service
	 */
	public ResourceService getResourceService() {
		return resourceService;
	}

	/**
	 * Sets the resource remote service.
	 *
	 * @param resourceService the resource remote service
	 */
	public void setResourceService(ResourceService resourceService) {
		this.resourceService = resourceService;
	}

	/**
	 * Returns the resource persistence.
	 *
	 * @return the resource persistence
	 */
	public ResourcePersistence getResourcePersistence() {
		return resourcePersistence;
	}

	/**
	 * Sets the resource persistence.
	 *
	 * @param resourcePersistence the resource persistence
	 */
	public void setResourcePersistence(ResourcePersistence resourcePersistence) {
		this.resourcePersistence = resourcePersistence;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public UserLocalService getUserLocalService() {
		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user remote service.
	 *
	 * @return the user remote service
	 */
	public UserService getUserService() {
		return userService;
	}

	/**
	 * Sets the user remote service.
	 *
	 * @param userService the user remote service
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	public void afterPropertiesSet() {
		PersistedModelLocalServiceRegistryUtil.register("info.diit.portal.studentattendence.model.Attendance",
			attendanceLocalService);
	}

	public void destroy() {
		PersistedModelLocalServiceRegistryUtil.unregister(
			"info.diit.portal.studentattendence.model.Attendance");
	}

	/**
	 * Returns the Spring bean ID for this bean.
	 *
	 * @return the Spring bean ID for this bean
	 */
	public String getBeanIdentifier() {
		return _beanIdentifier;
	}

	/**
	 * Sets the Spring bean ID for this bean.
	 *
	 * @param beanIdentifier the Spring bean ID for this bean
	 */
	public void setBeanIdentifier(String beanIdentifier) {
		_beanIdentifier = beanIdentifier;
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		return _clpInvoker.invokeMethod(name, parameterTypes, arguments);
	}

	protected Class<?> getModelClass() {
		return Attendance.class;
	}

	protected String getModelClassName() {
		return Attendance.class.getName();
	}

	/**
	 * Performs an SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) throws SystemException {
		try {
			DataSource dataSource = attendancePersistence.getDataSource();

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(dataSource,
					sql, new int[0]);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(type = AttendanceLocalService.class)
	protected AttendanceLocalService attendanceLocalService;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicLocalService.class)
	protected AttendanceTopicLocalService attendanceTopicLocalService;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = StudentAttendanceLocalService.class)
	protected StudentAttendanceLocalService studentAttendanceLocalService;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = CounterLocalService.class)
	protected CounterLocalService counterLocalService;
	@BeanReference(type = ResourceLocalService.class)
	protected ResourceLocalService resourceLocalService;
	@BeanReference(type = ResourceService.class)
	protected ResourceService resourceService;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserLocalService.class)
	protected UserLocalService userLocalService;
	@BeanReference(type = UserService.class)
	protected UserService userService;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private String _beanIdentifier;
	private AttendanceLocalServiceClpInvoker _clpInvoker = new AttendanceLocalServiceClpInvoker();
}
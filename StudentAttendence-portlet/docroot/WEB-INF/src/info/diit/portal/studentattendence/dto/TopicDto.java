package info.diit.portal.studentattendence.dto;

import java.io.Serializable;

public class TopicDto implements Serializable {

	private long id;
	private String topic;
	private String description;
	private boolean selection;
	private long classSequence;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public boolean isSelection() {
		return selection;
	}
	public void setSelection(boolean selection) {
		this.selection = selection;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getClassSequence() {
		return classSequence;
	}
	public void setClassSequence(long classSequence) {
		this.classSequence = classSequence;
	}
}

package info.diit.portal.studentattendence.dto;

import java.io.Serializable;

public class StudentDto implements Serializable {
	long studentId;
	String studentName;
	boolean present;
	
	/*public StudentDto() {
		present = false;
	}*/
	
	public long getStudentId() {
		return studentId;
	}
	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public boolean isPresent() {
		return present;
	}
	public void setPresent(boolean present) {
		this.present = present;
	}

	@Override
	public String toString() {
		return getStudentName();
	}
	
}

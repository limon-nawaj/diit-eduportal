/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.studentattendence.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.studentattendence.model.StudentAttendance;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing StudentAttendance in entity cache.
 *
 * @author limon
 * @see StudentAttendance
 * @generated
 */
public class StudentAttendanceCacheModel implements CacheModel<StudentAttendance>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{attendanceTopicId=");
		sb.append(attendanceTopicId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", attendanceId=");
		sb.append(attendanceId);
		sb.append(", studentId=");
		sb.append(studentId);
		sb.append(", status=");
		sb.append(status);
		sb.append(", attendanceDate=");
		sb.append(attendanceDate);
		sb.append("}");

		return sb.toString();
	}

	public StudentAttendance toEntityModel() {
		StudentAttendanceImpl studentAttendanceImpl = new StudentAttendanceImpl();

		studentAttendanceImpl.setAttendanceTopicId(attendanceTopicId);
		studentAttendanceImpl.setCompanyId(companyId);
		studentAttendanceImpl.setOrganizationId(organizationId);
		studentAttendanceImpl.setUserId(userId);

		if (userName == null) {
			studentAttendanceImpl.setUserName(StringPool.BLANK);
		}
		else {
			studentAttendanceImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			studentAttendanceImpl.setCreateDate(null);
		}
		else {
			studentAttendanceImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			studentAttendanceImpl.setModifiedDate(null);
		}
		else {
			studentAttendanceImpl.setModifiedDate(new Date(modifiedDate));
		}

		studentAttendanceImpl.setAttendanceId(attendanceId);
		studentAttendanceImpl.setStudentId(studentId);
		studentAttendanceImpl.setStatus(status);

		if (attendanceDate == Long.MIN_VALUE) {
			studentAttendanceImpl.setAttendanceDate(null);
		}
		else {
			studentAttendanceImpl.setAttendanceDate(new Date(attendanceDate));
		}

		studentAttendanceImpl.resetOriginalValues();

		return studentAttendanceImpl;
	}

	public long attendanceTopicId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long attendanceId;
	public long studentId;
	public int status;
	public long attendanceDate;
}
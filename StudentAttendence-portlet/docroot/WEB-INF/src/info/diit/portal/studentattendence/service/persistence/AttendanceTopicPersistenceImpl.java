/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.studentattendence.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.studentattendence.NoSuchAttendanceTopicException;
import info.diit.portal.studentattendence.model.AttendanceTopic;
import info.diit.portal.studentattendence.model.impl.AttendanceTopicImpl;
import info.diit.portal.studentattendence.model.impl.AttendanceTopicModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the attendance topic service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see AttendanceTopicPersistence
 * @see AttendanceTopicUtil
 * @generated
 */
public class AttendanceTopicPersistenceImpl extends BasePersistenceImpl<AttendanceTopic>
	implements AttendanceTopicPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AttendanceTopicUtil} to access the attendance topic persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AttendanceTopicImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ATTENDANCE =
		new FinderPath(AttendanceTopicModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceTopicModelImpl.FINDER_CACHE_ENABLED,
			AttendanceTopicImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByAttendance",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ATTENDANCE =
		new FinderPath(AttendanceTopicModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceTopicModelImpl.FINDER_CACHE_ENABLED,
			AttendanceTopicImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByAttendance",
			new String[] { Long.class.getName() },
			AttendanceTopicModelImpl.ATTENDANCEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ATTENDANCE = new FinderPath(AttendanceTopicModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceTopicModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAttendance",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AttendanceTopicModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceTopicModelImpl.FINDER_CACHE_ENABLED,
			AttendanceTopicImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AttendanceTopicModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceTopicModelImpl.FINDER_CACHE_ENABLED,
			AttendanceTopicImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AttendanceTopicModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceTopicModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the attendance topic in the entity cache if it is enabled.
	 *
	 * @param attendanceTopic the attendance topic
	 */
	public void cacheResult(AttendanceTopic attendanceTopic) {
		EntityCacheUtil.putResult(AttendanceTopicModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceTopicImpl.class, attendanceTopic.getPrimaryKey(),
			attendanceTopic);

		attendanceTopic.resetOriginalValues();
	}

	/**
	 * Caches the attendance topics in the entity cache if it is enabled.
	 *
	 * @param attendanceTopics the attendance topics
	 */
	public void cacheResult(List<AttendanceTopic> attendanceTopics) {
		for (AttendanceTopic attendanceTopic : attendanceTopics) {
			if (EntityCacheUtil.getResult(
						AttendanceTopicModelImpl.ENTITY_CACHE_ENABLED,
						AttendanceTopicImpl.class,
						attendanceTopic.getPrimaryKey()) == null) {
				cacheResult(attendanceTopic);
			}
			else {
				attendanceTopic.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all attendance topics.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AttendanceTopicImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AttendanceTopicImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the attendance topic.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AttendanceTopic attendanceTopic) {
		EntityCacheUtil.removeResult(AttendanceTopicModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceTopicImpl.class, attendanceTopic.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<AttendanceTopic> attendanceTopics) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AttendanceTopic attendanceTopic : attendanceTopics) {
			EntityCacheUtil.removeResult(AttendanceTopicModelImpl.ENTITY_CACHE_ENABLED,
				AttendanceTopicImpl.class, attendanceTopic.getPrimaryKey());
		}
	}

	/**
	 * Creates a new attendance topic with the primary key. Does not add the attendance topic to the database.
	 *
	 * @param attendanceTopicId the primary key for the new attendance topic
	 * @return the new attendance topic
	 */
	public AttendanceTopic create(long attendanceTopicId) {
		AttendanceTopic attendanceTopic = new AttendanceTopicImpl();

		attendanceTopic.setNew(true);
		attendanceTopic.setPrimaryKey(attendanceTopicId);

		return attendanceTopic;
	}

	/**
	 * Removes the attendance topic with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param attendanceTopicId the primary key of the attendance topic
	 * @return the attendance topic that was removed
	 * @throws info.diit.portal.studentattendence.NoSuchAttendanceTopicException if a attendance topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceTopic remove(long attendanceTopicId)
		throws NoSuchAttendanceTopicException, SystemException {
		return remove(Long.valueOf(attendanceTopicId));
	}

	/**
	 * Removes the attendance topic with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the attendance topic
	 * @return the attendance topic that was removed
	 * @throws info.diit.portal.studentattendence.NoSuchAttendanceTopicException if a attendance topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AttendanceTopic remove(Serializable primaryKey)
		throws NoSuchAttendanceTopicException, SystemException {
		Session session = null;

		try {
			session = openSession();

			AttendanceTopic attendanceTopic = (AttendanceTopic)session.get(AttendanceTopicImpl.class,
					primaryKey);

			if (attendanceTopic == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAttendanceTopicException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(attendanceTopic);
		}
		catch (NoSuchAttendanceTopicException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AttendanceTopic removeImpl(AttendanceTopic attendanceTopic)
		throws SystemException {
		attendanceTopic = toUnwrappedModel(attendanceTopic);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, attendanceTopic);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(attendanceTopic);

		return attendanceTopic;
	}

	@Override
	public AttendanceTopic updateImpl(
		info.diit.portal.studentattendence.model.AttendanceTopic attendanceTopic,
		boolean merge) throws SystemException {
		attendanceTopic = toUnwrappedModel(attendanceTopic);

		boolean isNew = attendanceTopic.isNew();

		AttendanceTopicModelImpl attendanceTopicModelImpl = (AttendanceTopicModelImpl)attendanceTopic;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, attendanceTopic, merge);

			attendanceTopic.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !AttendanceTopicModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((attendanceTopicModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ATTENDANCE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(attendanceTopicModelImpl.getOriginalAttendanceId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ATTENDANCE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ATTENDANCE,
					args);

				args = new Object[] {
						Long.valueOf(attendanceTopicModelImpl.getAttendanceId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ATTENDANCE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ATTENDANCE,
					args);
			}
		}

		EntityCacheUtil.putResult(AttendanceTopicModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceTopicImpl.class, attendanceTopic.getPrimaryKey(),
			attendanceTopic);

		return attendanceTopic;
	}

	protected AttendanceTopic toUnwrappedModel(AttendanceTopic attendanceTopic) {
		if (attendanceTopic instanceof AttendanceTopicImpl) {
			return attendanceTopic;
		}

		AttendanceTopicImpl attendanceTopicImpl = new AttendanceTopicImpl();

		attendanceTopicImpl.setNew(attendanceTopic.isNew());
		attendanceTopicImpl.setPrimaryKey(attendanceTopic.getPrimaryKey());

		attendanceTopicImpl.setAttendanceTopicId(attendanceTopic.getAttendanceTopicId());
		attendanceTopicImpl.setCompanyId(attendanceTopic.getCompanyId());
		attendanceTopicImpl.setUserId(attendanceTopic.getUserId());
		attendanceTopicImpl.setUserName(attendanceTopic.getUserName());
		attendanceTopicImpl.setCreateDate(attendanceTopic.getCreateDate());
		attendanceTopicImpl.setModifiedDate(attendanceTopic.getModifiedDate());
		attendanceTopicImpl.setAttendanceId(attendanceTopic.getAttendanceId());
		attendanceTopicImpl.setTopicId(attendanceTopic.getTopicId());
		attendanceTopicImpl.setDescription(attendanceTopic.getDescription());

		return attendanceTopicImpl;
	}

	/**
	 * Returns the attendance topic with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the attendance topic
	 * @return the attendance topic
	 * @throws com.liferay.portal.NoSuchModelException if a attendance topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AttendanceTopic findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the attendance topic with the primary key or throws a {@link info.diit.portal.studentattendence.NoSuchAttendanceTopicException} if it could not be found.
	 *
	 * @param attendanceTopicId the primary key of the attendance topic
	 * @return the attendance topic
	 * @throws info.diit.portal.studentattendence.NoSuchAttendanceTopicException if a attendance topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceTopic findByPrimaryKey(long attendanceTopicId)
		throws NoSuchAttendanceTopicException, SystemException {
		AttendanceTopic attendanceTopic = fetchByPrimaryKey(attendanceTopicId);

		if (attendanceTopic == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + attendanceTopicId);
			}

			throw new NoSuchAttendanceTopicException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				attendanceTopicId);
		}

		return attendanceTopic;
	}

	/**
	 * Returns the attendance topic with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the attendance topic
	 * @return the attendance topic, or <code>null</code> if a attendance topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AttendanceTopic fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the attendance topic with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param attendanceTopicId the primary key of the attendance topic
	 * @return the attendance topic, or <code>null</code> if a attendance topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceTopic fetchByPrimaryKey(long attendanceTopicId)
		throws SystemException {
		AttendanceTopic attendanceTopic = (AttendanceTopic)EntityCacheUtil.getResult(AttendanceTopicModelImpl.ENTITY_CACHE_ENABLED,
				AttendanceTopicImpl.class, attendanceTopicId);

		if (attendanceTopic == _nullAttendanceTopic) {
			return null;
		}

		if (attendanceTopic == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				attendanceTopic = (AttendanceTopic)session.get(AttendanceTopicImpl.class,
						Long.valueOf(attendanceTopicId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (attendanceTopic != null) {
					cacheResult(attendanceTopic);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(AttendanceTopicModelImpl.ENTITY_CACHE_ENABLED,
						AttendanceTopicImpl.class, attendanceTopicId,
						_nullAttendanceTopic);
				}

				closeSession(session);
			}
		}

		return attendanceTopic;
	}

	/**
	 * Returns all the attendance topics where attendanceId = &#63;.
	 *
	 * @param attendanceId the attendance ID
	 * @return the matching attendance topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceTopic> findByAttendance(long attendanceId)
		throws SystemException {
		return findByAttendance(attendanceId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the attendance topics where attendanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param attendanceId the attendance ID
	 * @param start the lower bound of the range of attendance topics
	 * @param end the upper bound of the range of attendance topics (not inclusive)
	 * @return the range of matching attendance topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceTopic> findByAttendance(long attendanceId, int start,
		int end) throws SystemException {
		return findByAttendance(attendanceId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the attendance topics where attendanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param attendanceId the attendance ID
	 * @param start the lower bound of the range of attendance topics
	 * @param end the upper bound of the range of attendance topics (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching attendance topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceTopic> findByAttendance(long attendanceId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ATTENDANCE;
			finderArgs = new Object[] { attendanceId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ATTENDANCE;
			finderArgs = new Object[] {
					attendanceId,
					
					start, end, orderByComparator
				};
		}

		List<AttendanceTopic> list = (List<AttendanceTopic>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AttendanceTopic attendanceTopic : list) {
				if ((attendanceId != attendanceTopic.getAttendanceId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_ATTENDANCETOPIC_WHERE);

			query.append(_FINDER_COLUMN_ATTENDANCE_ATTENDANCEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(attendanceId);

				list = (List<AttendanceTopic>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first attendance topic in the ordered set where attendanceId = &#63;.
	 *
	 * @param attendanceId the attendance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attendance topic
	 * @throws info.diit.portal.studentattendence.NoSuchAttendanceTopicException if a matching attendance topic could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceTopic findByAttendance_First(long attendanceId,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceTopicException, SystemException {
		AttendanceTopic attendanceTopic = fetchByAttendance_First(attendanceId,
				orderByComparator);

		if (attendanceTopic != null) {
			return attendanceTopic;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("attendanceId=");
		msg.append(attendanceId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAttendanceTopicException(msg.toString());
	}

	/**
	 * Returns the first attendance topic in the ordered set where attendanceId = &#63;.
	 *
	 * @param attendanceId the attendance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attendance topic, or <code>null</code> if a matching attendance topic could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceTopic fetchByAttendance_First(long attendanceId,
		OrderByComparator orderByComparator) throws SystemException {
		List<AttendanceTopic> list = findByAttendance(attendanceId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last attendance topic in the ordered set where attendanceId = &#63;.
	 *
	 * @param attendanceId the attendance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attendance topic
	 * @throws info.diit.portal.studentattendence.NoSuchAttendanceTopicException if a matching attendance topic could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceTopic findByAttendance_Last(long attendanceId,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceTopicException, SystemException {
		AttendanceTopic attendanceTopic = fetchByAttendance_Last(attendanceId,
				orderByComparator);

		if (attendanceTopic != null) {
			return attendanceTopic;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("attendanceId=");
		msg.append(attendanceId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAttendanceTopicException(msg.toString());
	}

	/**
	 * Returns the last attendance topic in the ordered set where attendanceId = &#63;.
	 *
	 * @param attendanceId the attendance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attendance topic, or <code>null</code> if a matching attendance topic could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceTopic fetchByAttendance_Last(long attendanceId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByAttendance(attendanceId);

		List<AttendanceTopic> list = findByAttendance(attendanceId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the attendance topics before and after the current attendance topic in the ordered set where attendanceId = &#63;.
	 *
	 * @param attendanceTopicId the primary key of the current attendance topic
	 * @param attendanceId the attendance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next attendance topic
	 * @throws info.diit.portal.studentattendence.NoSuchAttendanceTopicException if a attendance topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceTopic[] findByAttendance_PrevAndNext(
		long attendanceTopicId, long attendanceId,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceTopicException, SystemException {
		AttendanceTopic attendanceTopic = findByPrimaryKey(attendanceTopicId);

		Session session = null;

		try {
			session = openSession();

			AttendanceTopic[] array = new AttendanceTopicImpl[3];

			array[0] = getByAttendance_PrevAndNext(session, attendanceTopic,
					attendanceId, orderByComparator, true);

			array[1] = attendanceTopic;

			array[2] = getByAttendance_PrevAndNext(session, attendanceTopic,
					attendanceId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AttendanceTopic getByAttendance_PrevAndNext(Session session,
		AttendanceTopic attendanceTopic, long attendanceId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ATTENDANCETOPIC_WHERE);

		query.append(_FINDER_COLUMN_ATTENDANCE_ATTENDANCEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(attendanceId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(attendanceTopic);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AttendanceTopic> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the attendance topics.
	 *
	 * @return the attendance topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceTopic> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the attendance topics.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of attendance topics
	 * @param end the upper bound of the range of attendance topics (not inclusive)
	 * @return the range of attendance topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceTopic> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the attendance topics.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of attendance topics
	 * @param end the upper bound of the range of attendance topics (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of attendance topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceTopic> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<AttendanceTopic> list = (List<AttendanceTopic>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ATTENDANCETOPIC);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ATTENDANCETOPIC;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<AttendanceTopic>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<AttendanceTopic>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the attendance topics where attendanceId = &#63; from the database.
	 *
	 * @param attendanceId the attendance ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByAttendance(long attendanceId) throws SystemException {
		for (AttendanceTopic attendanceTopic : findByAttendance(attendanceId)) {
			remove(attendanceTopic);
		}
	}

	/**
	 * Removes all the attendance topics from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (AttendanceTopic attendanceTopic : findAll()) {
			remove(attendanceTopic);
		}
	}

	/**
	 * Returns the number of attendance topics where attendanceId = &#63;.
	 *
	 * @param attendanceId the attendance ID
	 * @return the number of matching attendance topics
	 * @throws SystemException if a system exception occurred
	 */
	public int countByAttendance(long attendanceId) throws SystemException {
		Object[] finderArgs = new Object[] { attendanceId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ATTENDANCE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ATTENDANCETOPIC_WHERE);

			query.append(_FINDER_COLUMN_ATTENDANCE_ATTENDANCEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(attendanceId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ATTENDANCE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of attendance topics.
	 *
	 * @return the number of attendance topics
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ATTENDANCETOPIC);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the attendance topic persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.studentattendence.model.AttendanceTopic")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<AttendanceTopic>> listenersList = new ArrayList<ModelListener<AttendanceTopic>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<AttendanceTopic>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AttendanceTopicImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_ATTENDANCETOPIC = "SELECT attendanceTopic FROM AttendanceTopic attendanceTopic";
	private static final String _SQL_SELECT_ATTENDANCETOPIC_WHERE = "SELECT attendanceTopic FROM AttendanceTopic attendanceTopic WHERE ";
	private static final String _SQL_COUNT_ATTENDANCETOPIC = "SELECT COUNT(attendanceTopic) FROM AttendanceTopic attendanceTopic";
	private static final String _SQL_COUNT_ATTENDANCETOPIC_WHERE = "SELECT COUNT(attendanceTopic) FROM AttendanceTopic attendanceTopic WHERE ";
	private static final String _FINDER_COLUMN_ATTENDANCE_ATTENDANCEID_2 = "attendanceTopic.attendanceId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "attendanceTopic.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AttendanceTopic exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No AttendanceTopic exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AttendanceTopicPersistenceImpl.class);
	private static AttendanceTopic _nullAttendanceTopic = new AttendanceTopicImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<AttendanceTopic> toCacheModel() {
				return _nullAttendanceTopicCacheModel;
			}
		};

	private static CacheModel<AttendanceTopic> _nullAttendanceTopicCacheModel = new CacheModel<AttendanceTopic>() {
			public AttendanceTopic toEntityModel() {
				return _nullAttendanceTopic;
			}
		};
}
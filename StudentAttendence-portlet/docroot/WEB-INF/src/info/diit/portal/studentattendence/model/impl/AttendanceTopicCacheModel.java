/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.studentattendence.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.studentattendence.model.AttendanceTopic;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing AttendanceTopic in entity cache.
 *
 * @author limon
 * @see AttendanceTopic
 * @generated
 */
public class AttendanceTopicCacheModel implements CacheModel<AttendanceTopic>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{attendanceTopicId=");
		sb.append(attendanceTopicId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", attendanceId=");
		sb.append(attendanceId);
		sb.append(", topicId=");
		sb.append(topicId);
		sb.append(", description=");
		sb.append(description);
		sb.append("}");

		return sb.toString();
	}

	public AttendanceTopic toEntityModel() {
		AttendanceTopicImpl attendanceTopicImpl = new AttendanceTopicImpl();

		attendanceTopicImpl.setAttendanceTopicId(attendanceTopicId);
		attendanceTopicImpl.setCompanyId(companyId);
		attendanceTopicImpl.setUserId(userId);

		if (userName == null) {
			attendanceTopicImpl.setUserName(StringPool.BLANK);
		}
		else {
			attendanceTopicImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			attendanceTopicImpl.setCreateDate(null);
		}
		else {
			attendanceTopicImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			attendanceTopicImpl.setModifiedDate(null);
		}
		else {
			attendanceTopicImpl.setModifiedDate(new Date(modifiedDate));
		}

		attendanceTopicImpl.setAttendanceId(attendanceId);
		attendanceTopicImpl.setTopicId(topicId);

		if (description == null) {
			attendanceTopicImpl.setDescription(StringPool.BLANK);
		}
		else {
			attendanceTopicImpl.setDescription(description);
		}

		attendanceTopicImpl.resetOriginalValues();

		return attendanceTopicImpl;
	}

	public long attendanceTopicId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long attendanceId;
	public long topicId;
	public String description;
}
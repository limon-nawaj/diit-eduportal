package info.diit.portal.studentattendence.absence;

import info.diit.portal.batch.model.Batch;
import info.diit.portal.batch.service.BatchLocalServiceUtil;
import info.diit.portal.coursesubject.model.Course;
import info.diit.portal.coursesubject.model.CourseOrganization;
import info.diit.portal.coursesubject.service.CourseLocalServiceUtil;
import info.diit.portal.coursesubject.service.CourseOrganizationLocalServiceUtil;
import info.diit.portal.student.service.model.BatchStudent;
import info.diit.portal.student.service.model.PhoneNumber;
import info.diit.portal.student.service.model.Student;
import info.diit.portal.student.service.service.BatchStudentLocalServiceUtil;
import info.diit.portal.student.service.service.PhoneNumberLocalServiceUtil;
import info.diit.portal.student.service.service.StudentLocalServiceUtil;
import info.diit.portal.studentattendence.dto.AbsenceDto;
import info.diit.portal.studentattendence.dto.BatchDto;
import info.diit.portal.studentattendence.dto.CampusDto;
import info.diit.portal.studentattendence.dto.CourseDto;
import info.diit.portal.studentattendence.model.Attendance;
import info.diit.portal.studentattendence.model.StudentAttendance;
import info.diit.portal.studentattendence.service.AttendanceLocalServiceUtil;
import info.diit.portal.studentattendence.service.StudentAttendanceLocalServiceUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import com.liferay.portal.kernel.dao.jdbc.DataAccess;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.FileNameException;
import com.vaadin.Application;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.StreamResource;
import com.vaadin.terminal.StreamResource.StreamSource;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

public class AbsenceStudentApplication extends Application implements PortletRequestListener {

	private ThemeDisplay themeDisplay;
	private BeanItemContainer<AbsenceDto> container;
	
	private final static String COLUMN_COURSE = "course";
	private final static String COLUMN_BATCH = "batch";
	private final static String COLUMN_STUDENT_ID = "studentId";
	private final static String COLUMN_STUDENT_NAME = "studentName";
	private final static String COLUMN_LAST_PRESENT = "lastPresent";
	private final static String COLUMN_PHONE_1 = "phone1";
	private final static String COLUMN_PHONE_2 = "phone2";
	
	private Window window;
	private Window myPopWindow;
	JasperPrint jasperPrint;
	private Connection connection;
	
	private List<Organization> userOrganizationList;
	
	User user;
	
	public void init() {
		window = new Window();

		setMainWindow(window);

		Label label = new Label("Hello AbsenceStudent!");
		
		try {
			connection = DataAccess.getConnection();
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
		
		try {
			userOrganizationList = OrganizationLocalServiceUtil.getUserOrganizations(themeDisplay.getUser().getUserId());
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		user = themeDisplay.getUser();
		
		loadCampus();
		window.addComponent(mainLayout());
		
		if (userOrganizationList.size()==1) {
			loadCourse();
			loadCourseCombobox();
			loadAbsenceStudent();
		}
	}
	
	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}
	
	private ComboBox courseComboBox;
	private ComboBox batchComboBox;
	private Table absenceTable;
	private ComboBox campusComboBox;
	
	
	private List<CourseDto> courseList;
	private List<CampusDto> campusList;
	private List<BatchDto> batchList;
	
	private GridLayout mainLayout(){
		GridLayout mainLayout = new GridLayout(8, 8);
		mainLayout.setWidth("100%");
		
		campusComboBox = new ComboBox("Campus");
		
		courseComboBox  = new ComboBox("Course");
		batchComboBox 	= new ComboBox("Batch");
		
		campusComboBox.setImmediate(true);
		courseComboBox.setImmediate(true);
		batchComboBox.setImmediate(true);
		
		campusComboBox.setWidth("100%");
		courseComboBox.setWidth("100%");
		batchComboBox.setWidth("100%");
		
		if (campusList!=null) {
			for (CampusDto	campus: campusList) {
				campusComboBox.addItem(campus);
			}
		}
		
		campusComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				courseComboBox.removeAllItems();
				container.removeAllItems();
				loadCourse();
				loadCourseCombobox();
				loadAbsenceStudent();
			}
		});
		
		loadCourseCombobox();
		
		courseComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				batchComboBox.removeAllItems();
				container.removeAllItems();
				loadBatch();
				loadAbsenceStudent();
				if (batchList!=null) {
					for (BatchDto batch : batchList) {
						batchComboBox.addItem(batch);
					}
				}
			}
		});
		
		batchComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				container.removeAllItems();
				loadAbsenceStudent();
			}
		});
		
		container = new BeanItemContainer<AbsenceDto>(AbsenceDto.class);
		absenceTable = new Table("", container)
		{
			@Override
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				if (property.getType()==Date.class) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					return sdf.format(property.getValue());
				}
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		absenceTable.setWidth("100%");
		absenceTable.setImmediate(true);
		
		absenceTable.setSelectable(true);
		
		absenceTable.setColumnHeader(COLUMN_COURSE, "Course");
		absenceTable.setColumnHeader(COLUMN_BATCH, "Batch");
		absenceTable.setColumnHeader(COLUMN_STUDENT_ID, "Student Id");
		absenceTable.setColumnHeader(COLUMN_STUDENT_NAME, "Student Name");
		absenceTable.setColumnHeader(COLUMN_LAST_PRESENT, "Last Present");
		absenceTable.setColumnHeader(COLUMN_PHONE_1, "Phone 1");
		absenceTable.setColumnHeader(COLUMN_PHONE_2, "Phone 2");
		
		absenceTable.setVisibleColumns(new String[]{COLUMN_COURSE, COLUMN_BATCH, COLUMN_STUDENT_ID, COLUMN_STUDENT_NAME, COLUMN_LAST_PRESENT, COLUMN_PHONE_1, COLUMN_PHONE_2});
		
		VerticalLayout buttonLayout = new VerticalLayout();
		buttonLayout.setWidth("100%");
		
		Label label = new Label();
		
		Button viewButton = new Button("Details");
		viewButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				AbsenceDto absenceDto = (AbsenceDto) absenceTable.getValue();
				if (absenceDto!=null) {
//					window.showNotification(""+absenceDto.getStudentId());
					reportWindow(absenceDto.getStudentId());
				}else{
					window.showNotification("Please select a student", Window.Notification.TYPE_ERROR_MESSAGE);
				}
			}
		});
		
		buttonLayout.addComponent(label);
		buttonLayout.addComponent(viewButton);
		
		buttonLayout.setExpandRatio(label, 1);
		
		if (userOrganizationList.size()>1) {
			mainLayout.addComponent(campusComboBox, 0, 0, 1, 0);
			mainLayout.addComponent(courseComboBox, 3, 0, 4, 0);
			mainLayout.addComponent(batchComboBox, 6, 0, 7, 0);
			
		}else{
			mainLayout.addComponent(courseComboBox, 0, 0, 1, 0);
			mainLayout.addComponent(batchComboBox, 3, 0, 4, 0);
		}
		mainLayout.addComponent(absenceTable, 0, 1, 7, 1);
		mainLayout.addComponent(buttonLayout, 0, 2, 7, 2);
		
		return mainLayout;
	}
	
	private void loadCampus(){
		if (userOrganizationList.size()>1) {
			campusList = new ArrayList<CampusDto>();
			for (Organization organization : userOrganizationList) {
				CampusDto campusDto = new CampusDto();
				campusDto.setId(organization.getOrganizationId());
				campusDto.setName(organization.getName());
				campusList.add(campusDto);
			}
			
		}
	}
	
	private void loadCourse(){
		try {
			List<CourseOrganization> courses = null;
			courseList = new ArrayList<CourseDto>();
			if (userOrganizationList.size()>1) {
				CampusDto campus = (CampusDto) campusComboBox.getValue();
				if (campus!=null) {
					courses = CourseOrganizationLocalServiceUtil.findByOrganization(campus.getId());
				}
			}else{
				Organization org = userOrganizationList.get(0);
				courses = CourseOrganizationLocalServiceUtil.findByOrganization(org.getOrganizationId());
//				window.showNotification("Course: "+courses.size());
			}
			
			if (courses!=null) {
//				window.showNotification("Course: "+courses.size());
				for (CourseOrganization course : courses) {
					Course c = CourseLocalServiceUtil.fetchCourse(course.getCourseId());
					CourseDto courseDto = new CourseDto();
					courseDto.setId(c.getCourseId());
					courseDto.setName(c.getCourseName());
					courseList.add(courseDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void loadBatch(){
		try {
			batchList = new ArrayList<BatchDto>();
			CourseDto courseDto = (CourseDto) courseComboBox.getValue();
			if (courseDto!=null) {
				List<Batch> batches = BatchLocalServiceUtil.findBatchByCourseId(courseDto.getId());
				if (batches!=null) {
					for (Batch batch : batches) {
						BatchDto batchDto = new BatchDto();
						batchDto.setId(batch.getBatchId());
						batchDto.setName(batch.getBatchName());
						batchList.add(batchDto);
					}
				}
			}
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void loadCourseCombobox(){
		if (courseList!=null) {
//			window.showNotification("Course List"+courseList.size());
			for (CourseDto course : courseList) {
				courseComboBox.addItem(course);
//				window.showNotification("Course"+course);
			}
		}
	}

	private void loadAbsenceStudent(){
		try {
			container.removeAllItems();
			List<CourseOrganization> courseList =null;
			Organization org = userOrganizationList.get(0);
			CampusDto campusDto = (CampusDto) campusComboBox.getValue();
			if (campusDto!=null) {
				courseList = CourseOrganizationLocalServiceUtil.findByOrganization(campusDto.getId());
			}else{
				courseList = CourseOrganizationLocalServiceUtil.findByOrganization(org.getOrganizationId());
			}
			
			if (courseList!=null) {
				List<Batch> batchList = null;
				
				CourseDto courseDto = (CourseDto) courseComboBox.getValue();
				if (courseDto!=null) {
					if (campusDto!=null) {
						if (campusDto.getId()==org.getOrganizationId()) {
							batchList = BatchLocalServiceUtil.findBatchByCourseId(courseDto.getId());
							loadAbsenceByCourse(batchList, campusDto.getId());
						}
					}else{
						batchList = BatchLocalServiceUtil.findBatchByCourseId(courseDto.getId());
						loadAbsenceByCourse(batchList, org.getOrganizationId());
					}
				}else{
					for (CourseOrganization c : courseList) {
						if (c.getOrganizationId()==org.getOrganizationId()) {
							Course course = CourseLocalServiceUtil.fetchCourse(c.getCourseId());
							batchList = BatchLocalServiceUtil.findBatchByCourseId(course.getCourseId());
							loadAbsenceByCourse(batchList, c.getOrganizationId());
						}
					}
				}
				
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void loadAbsenceByCourse(List<Batch> batchList, long organizationId){
		try{
			Organization org = userOrganizationList.get(0);
			List<BatchStudent> studentList = null;
			if (batchList!=null) {
				BatchDto btcDto = (BatchDto) batchComboBox.getValue();
				if (btcDto!=null) {
					studentList = BatchStudentLocalServiceUtil.findByBatch(btcDto.getId());
					absenceByBatch(studentList);
				}else{
					for (Batch batch : batchList) {
						if (batch.getStatus()==1) {
							studentList = BatchStudentLocalServiceUtil.findByBatch(batch.getBatchId());
							absenceByBatch(studentList);
						}
					}
				}
				
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void absenceByBatch(List<BatchStudent> studentList){
		try {
			Organization org = userOrganizationList.get(0);
			if (studentList!=null) {
				for (BatchStudent batchStudent : studentList) {
					if (batchStudent.getOrganizationId()==org.getOrganizationId()) {
						Batch batch = BatchLocalServiceUtil.fetchBatch(batchStudent.getBatchId());
						
						if (checkExistedStudent(batch.getBatchId(), batchStudent.getStudentId())) {
							DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(StudentAttendance.class);
							dynamicQuery.add(PropertyFactoryUtil.forName("studentId").eq(batchStudent.getStudentId()));
							dynamicQuery.addOrder(OrderFactoryUtil.desc("attendanceDate")).setLimit(0, 3);
							List<StudentAttendance> threeDaysAttendances = StudentAttendanceLocalServiceUtil.dynamicQuery(dynamicQuery);
							int count = 0;
							StudentAttendance studentAtt = null;
							if (threeDaysAttendances.size()==3) {
								for (StudentAttendance studentAttendance : threeDaysAttendances) {
									if (studentAttendance.getStatus()==0) {
										count++;
									}
								}
							}
							
							if (count==3) {
								
								AbsenceDto absenceDto = new AbsenceDto();
								Course course = CourseLocalServiceUtil.fetchCourse(batch.getCourseId());
								CourseDto courseDto = new CourseDto();
								courseDto.setId(course.getCourseId());
								courseDto.setName(course.getCourseName());
								
								absenceDto.setCourse(courseDto);
								
								BatchDto batchDto = new BatchDto();
								batchDto.setId(batch.getBatchId());
								batchDto.setName(batch.getBatchName());
								absenceDto.setBatch(batchDto);
								
								Student student = StudentLocalServiceUtil.fetchStudent(batchStudent.getStudentId());
								
								absenceDto.setStudentId(student.getStudentId());
								absenceDto.setStudentName(student.getName());
								
								/*student last present*/
								DynamicQuery lastPresentDynamicQuery = DynamicQueryFactoryUtil.forClass(StudentAttendance.class);
								lastPresentDynamicQuery.add(PropertyFactoryUtil.forName("studentId").eq(student.getStudentId()));
								lastPresentDynamicQuery.add(PropertyFactoryUtil.forName("status").eq(1));
								lastPresentDynamicQuery.addOrder(OrderFactoryUtil.desc("attendanceDate"));
								List<StudentAttendance> lastAttendance = StudentAttendanceLocalServiceUtil.dynamicQuery(lastPresentDynamicQuery);
								
								if (lastPresentDynamicQuery!=null) {
									StudentAttendance stuAtt = lastAttendance.get(0);
									Attendance attend = AttendanceLocalServiceUtil.fetchAttendance(stuAtt.getAttendanceId());
									Date lastPresent = attend.getAttendanceDate();
									
									absenceDto.setLastPresent(lastPresent);
								}
								/*student last present*/
								
								/*student phone number*/
								List<PhoneNumber> studentPhoneList = PhoneNumberLocalServiceUtil.findByPhoneNumberList(student.getStudentId());
								if (studentPhoneList!=null) {
									if (studentPhoneList.size()>1) {
										PhoneNumber phone1 = studentPhoneList.get(0);
										PhoneNumber phone2 = studentPhoneList.get(1);
										
										absenceDto.setPhone1(phone1.getPhoneNumber());
										absenceDto.setPhone2(phone2.getPhoneNumber());
									}else if (studentPhoneList.size()==1) {
										PhoneNumber phone = studentPhoneList.get(0);
										absenceDto.setPhone1(phone.getPhoneNumber());
									}
								}
								/*student phone number*/
								
								container.addBean(absenceDto);
							}
							
						}
					}
					
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 *private void loadAbsenceStudent(){
		try {
			container.removeAllItems();
			List<CourseOrganization> courseList =null;
			Organization org = userOrganizationList.get(0);
			CampusDto campusDto = (CampusDto) campusComboBox.getValue();
			if (campusDto!=null) {
				courseList = CourseOrganizationLocalServiceUtil.findByOrganization(campusDto.getId());
			}else{
				courseList = CourseOrganizationLocalServiceUtil.findByOrganization(org.getOrganizationId());
			}
			
			if (courseList!=null) {
				List<Batch> batchList = null;
				for (CourseOrganization c : courseList) {
					Course course = CourseLocalServiceUtil.fetchCourse(c.getCourseId());
					
					CourseDto crsDto = (CourseDto) courseComboBox.getValue();
					if (crsDto!=null) {
						batchList = BatchLocalServiceUtil.findBatchByCourseId(crsDto.getId());
					}else{
						batchList = BatchLocalServiceUtil.findBatchByCourseId(course.getCourseId());
					}
					
//					window.showNotification("BatchList: "+batchList.size());
					
					if (c.getOrganizationId()==org.getOrganizationId()) {
						if (batchList!=null) {
							for (Batch batch : batchList) {
								
								if (batch.getStatus()==1) {
									List<BatchStudent> studentList = null;
									
									BatchDto btcDto = (BatchDto) batchComboBox.getValue();
									if (btcDto!=null) {
										studentList = BatchStudentLocalServiceUtil.findByBatch(btcDto.getId());
									}else{
										studentList = BatchStudentLocalServiceUtil.findByBatch(batch.getBatchId());
									}
									
									if (studentList!=null) {
										for (BatchStudent batchStudent : studentList) {
											if (checkExistedStudent(batch.getBatchId(), batchStudent.getStudentId())) {
												DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(StudentAttendance.class);
												dynamicQuery.add(PropertyFactoryUtil.forName("studentId").eq(batchStudent.getStudentId()));
//												dynamicQuery.add(PropertyFactoryUtil.forName("status").eq(0));
												dynamicQuery.addOrder(OrderFactoryUtil.desc("attendanceDate")).setLimit(0, 3);
												List<StudentAttendance> threeDaysAttendances = StudentAttendanceLocalServiceUtil.dynamicQuery(dynamicQuery);
												int count = 0;
												if (threeDaysAttendances.size()==3) {
													for (StudentAttendance studentAttendance : threeDaysAttendances) {
														if (studentAttendance.getStatus()==0) {
															count++;
														}
													}
												}
												
												if (count==3) {
//													window.showNotification("Student absence: "+batchStudent.getStudentId());
													
													AbsenceDto absenceDto = new AbsenceDto();
													
													CourseDto courseDto = new CourseDto();
													courseDto.setId(course.getCourseId());
													courseDto.setName(course.getCourseName());
													
													absenceDto.setCourse(courseDto);
													
													BatchDto batchDto = new BatchDto();
													batchDto.setId(batch.getBatchId());
													batchDto.setName(batch.getBatchName());
													absenceDto.setBatch(batchDto);
													
													Student student = StudentLocalServiceUtil.fetchStudent(batchStudent.getStudentId());
													
													absenceDto.setStudentId(student.getStudentId());
													absenceDto.setStudentName(student.getName());
													container.addBean(absenceDto);
												}
												
											}
										}
									}
								}
							}
						}
					}
					
					
					
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	 */
	
	private boolean checkExistedStudent(long batchId, long studentId){
		boolean status = false;
		try {
			List<Attendance> attendanceList = AttendanceLocalServiceUtil.findByBatch(batchId);
			if (attendanceList!=null) {
				for (Attendance attendance : attendanceList) {
					List<StudentAttendance> studentAttendances = StudentAttendanceLocalServiceUtil.findByAttendance(attendance.getAttendanceId());
					if (studentAttendances!=null) {
						for (StudentAttendance studentAttendance : studentAttendances) {
							if (studentAttendance.getStudentId()==studentId) {
								if (studentAttendance.getStatus()==1) {
									status = true;
									break;
								}
							}
						}
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return status;
	}
	
	private boolean compareAttendance(long batch, long attendanceId){
		boolean status = false;
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Attendance.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("batch").eq(batch));
		dynamicQuery.addOrder(OrderFactoryUtil.desc("attendanceDate")).setLimit(0, 3);
		try {
			List<Attendance> threeDaysAttendances = AttendanceLocalServiceUtil.dynamicQuery(dynamicQuery);
//			window.showNotification("Dynamic query: "+threeDaysAttendances.size());
			if (threeDaysAttendances!=null) {
				for (Attendance attendance : threeDaysAttendances) {
					window.showNotification("Date: "+attendance.getAttendanceDate());
					if (attendance.getAttendanceId()==attendanceId) {
						status = true;
						break;
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return status;
	}
	
	SimpleDateFormat dateformat = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
	private final String timeStamp = dateformat.format(new Date());
	
	public void reportWindow(long studentId) {
		
		myPopWindow = new Window("report");
		myPopWindow.setWidth("60%");
		myPopWindow.setHeight("800px");
		myPopWindow.setPositionX(300);
		myPopWindow.setPositionY(300);
		
		final Map imagelocation = new HashMap();
		imagelocation.put("studentId", new Long(16));
		/*long batchid = 25;
		imagelocation.put("batchId",batchid );*/
		
		StreamSource s = new StreamResource.StreamSource() {
			public InputStream getStream() {
						
					try {
						jasperPrint = JasperFillManager.fillReport(PropsUtil.get("dl.hook.file.system.root.dir.report") + "/" 
					+"Student_Profile.jasper", imagelocation,connection);
						System.out.println("jasper print created--------------------------------------------" + jasperPrint.getPages().size());
						
						String path = PropsUtil.get("dl.hook.file.system.root.dir.pdf")	+"/"+user.getUserId()+"_"+timeStamp+ ".pdf";
						
						JasperExportManager.exportReportToPdfFile(jasperPrint,path);
						
						File file = new File(path);
						
//						System.err.println(file.getAbsolutePath());
						
						FileInputStream fis = new FileInputStream(file);
						return fis;
								
						
					} catch (JRException e2) {
						e2.printStackTrace();
					}
					catch(FileNotFoundException fnfe)
					{
						fnfe.printStackTrace();
					}
				return null;
			}
		};
		
		StreamResource r = new StreamResource(s, "repy.pdf", this);
		
		Embedded e = new Embedded();
		e.setSizeFull();
		e.setHeight("700px");
		e.setType(Embedded.TYPE_BROWSER);
		r.setMIMEType("application/pdf");
		e.setSource(r);
		
		myPopWindow.addComponent(e);	

		window.addWindow(myPopWindow);
		
		myPopWindow.addListener(new CloseListener() {
			public void windowClose(CloseEvent e) {
				File file = new File(PropsUtil.get("dl.hook.file.system.root.dir.pdf")
						+"/"+user.getUserId()+"_"+timeStamp+ ".pdf");
				file.delete();
			}
		});
	}

}

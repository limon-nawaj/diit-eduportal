/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.assessment.NoSuchAssessmentTypeException;
import info.diit.portal.assessment.model.AssessmentType;
import info.diit.portal.assessment.model.impl.AssessmentTypeImpl;
import info.diit.portal.assessment.model.impl.AssessmentTypeModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the assessment type service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see AssessmentTypePersistence
 * @see AssessmentTypeUtil
 * @generated
 */
public class AssessmentTypePersistenceImpl extends BasePersistenceImpl<AssessmentType>
	implements AssessmentTypePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AssessmentTypeUtil} to access the assessment type persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AssessmentTypeImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY = new FinderPath(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeModelImpl.FINDER_CACHE_ENABLED,
			AssessmentTypeImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBycompany",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY =
		new FinderPath(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeModelImpl.FINDER_CACHE_ENABLED,
			AssessmentTypeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBycompany",
			new String[] { Long.class.getName() },
			AssessmentTypeModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANY = new FinderPath(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBycompany",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYTYPENAME =
		new FinderPath(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeModelImpl.FINDER_CACHE_ENABLED,
			AssessmentTypeImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCompanyTypeName",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYTYPENAME =
		new FinderPath(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeModelImpl.FINDER_CACHE_ENABLED,
			AssessmentTypeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyTypeName",
			new String[] { Long.class.getName(), String.class.getName() },
			AssessmentTypeModelImpl.COMPANYID_COLUMN_BITMASK |
			AssessmentTypeModelImpl.TYPE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYTYPENAME = new FinderPath(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCompanyTypeName",
			new String[] { Long.class.getName(), String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeModelImpl.FINDER_CACHE_ENABLED,
			AssessmentTypeImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByorganization",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeModelImpl.FINDER_CACHE_ENABLED,
			AssessmentTypeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByorganization",
			new String[] { Long.class.getName() },
			AssessmentTypeModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATION = new FinderPath(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByorganization",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeModelImpl.FINDER_CACHE_ENABLED,
			AssessmentTypeImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeModelImpl.FINDER_CACHE_ENABLED,
			AssessmentTypeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the assessment type in the entity cache if it is enabled.
	 *
	 * @param assessmentType the assessment type
	 */
	public void cacheResult(AssessmentType assessmentType) {
		EntityCacheUtil.putResult(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeImpl.class, assessmentType.getPrimaryKey(),
			assessmentType);

		assessmentType.resetOriginalValues();
	}

	/**
	 * Caches the assessment types in the entity cache if it is enabled.
	 *
	 * @param assessmentTypes the assessment types
	 */
	public void cacheResult(List<AssessmentType> assessmentTypes) {
		for (AssessmentType assessmentType : assessmentTypes) {
			if (EntityCacheUtil.getResult(
						AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
						AssessmentTypeImpl.class, assessmentType.getPrimaryKey()) == null) {
				cacheResult(assessmentType);
			}
			else {
				assessmentType.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all assessment types.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AssessmentTypeImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AssessmentTypeImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the assessment type.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AssessmentType assessmentType) {
		EntityCacheUtil.removeResult(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeImpl.class, assessmentType.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<AssessmentType> assessmentTypes) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AssessmentType assessmentType : assessmentTypes) {
			EntityCacheUtil.removeResult(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
				AssessmentTypeImpl.class, assessmentType.getPrimaryKey());
		}
	}

	/**
	 * Creates a new assessment type with the primary key. Does not add the assessment type to the database.
	 *
	 * @param assessmentTypeId the primary key for the new assessment type
	 * @return the new assessment type
	 */
	public AssessmentType create(long assessmentTypeId) {
		AssessmentType assessmentType = new AssessmentTypeImpl();

		assessmentType.setNew(true);
		assessmentType.setPrimaryKey(assessmentTypeId);

		return assessmentType;
	}

	/**
	 * Removes the assessment type with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param assessmentTypeId the primary key of the assessment type
	 * @return the assessment type that was removed
	 * @throws info.diit.portal.assessment.NoSuchAssessmentTypeException if a assessment type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType remove(long assessmentTypeId)
		throws NoSuchAssessmentTypeException, SystemException {
		return remove(Long.valueOf(assessmentTypeId));
	}

	/**
	 * Removes the assessment type with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the assessment type
	 * @return the assessment type that was removed
	 * @throws info.diit.portal.assessment.NoSuchAssessmentTypeException if a assessment type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AssessmentType remove(Serializable primaryKey)
		throws NoSuchAssessmentTypeException, SystemException {
		Session session = null;

		try {
			session = openSession();

			AssessmentType assessmentType = (AssessmentType)session.get(AssessmentTypeImpl.class,
					primaryKey);

			if (assessmentType == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAssessmentTypeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(assessmentType);
		}
		catch (NoSuchAssessmentTypeException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AssessmentType removeImpl(AssessmentType assessmentType)
		throws SystemException {
		assessmentType = toUnwrappedModel(assessmentType);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, assessmentType);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(assessmentType);

		return assessmentType;
	}

	@Override
	public AssessmentType updateImpl(
		info.diit.portal.assessment.model.AssessmentType assessmentType,
		boolean merge) throws SystemException {
		assessmentType = toUnwrappedModel(assessmentType);

		boolean isNew = assessmentType.isNew();

		AssessmentTypeModelImpl assessmentTypeModelImpl = (AssessmentTypeModelImpl)assessmentType;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, assessmentType, merge);

			assessmentType.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !AssessmentTypeModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((assessmentTypeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentTypeModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);

				args = new Object[] {
						Long.valueOf(assessmentTypeModelImpl.getCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);
			}

			if ((assessmentTypeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYTYPENAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentTypeModelImpl.getOriginalCompanyId()),
						
						assessmentTypeModelImpl.getOriginalType()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYTYPENAME,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYTYPENAME,
					args);

				args = new Object[] {
						Long.valueOf(assessmentTypeModelImpl.getCompanyId()),
						
						assessmentTypeModelImpl.getType()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYTYPENAME,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYTYPENAME,
					args);
			}

			if ((assessmentTypeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentTypeModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(assessmentTypeModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);
			}
		}

		EntityCacheUtil.putResult(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentTypeImpl.class, assessmentType.getPrimaryKey(),
			assessmentType);

		return assessmentType;
	}

	protected AssessmentType toUnwrappedModel(AssessmentType assessmentType) {
		if (assessmentType instanceof AssessmentTypeImpl) {
			return assessmentType;
		}

		AssessmentTypeImpl assessmentTypeImpl = new AssessmentTypeImpl();

		assessmentTypeImpl.setNew(assessmentType.isNew());
		assessmentTypeImpl.setPrimaryKey(assessmentType.getPrimaryKey());

		assessmentTypeImpl.setAssessmentTypeId(assessmentType.getAssessmentTypeId());
		assessmentTypeImpl.setCompanyId(assessmentType.getCompanyId());
		assessmentTypeImpl.setOrganizationId(assessmentType.getOrganizationId());
		assessmentTypeImpl.setUserId(assessmentType.getUserId());
		assessmentTypeImpl.setUserName(assessmentType.getUserName());
		assessmentTypeImpl.setCreateDate(assessmentType.getCreateDate());
		assessmentTypeImpl.setModifiedDate(assessmentType.getModifiedDate());
		assessmentTypeImpl.setType(assessmentType.getType());

		return assessmentTypeImpl;
	}

	/**
	 * Returns the assessment type with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the assessment type
	 * @return the assessment type
	 * @throws com.liferay.portal.NoSuchModelException if a assessment type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AssessmentType findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the assessment type with the primary key or throws a {@link info.diit.portal.assessment.NoSuchAssessmentTypeException} if it could not be found.
	 *
	 * @param assessmentTypeId the primary key of the assessment type
	 * @return the assessment type
	 * @throws info.diit.portal.assessment.NoSuchAssessmentTypeException if a assessment type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType findByPrimaryKey(long assessmentTypeId)
		throws NoSuchAssessmentTypeException, SystemException {
		AssessmentType assessmentType = fetchByPrimaryKey(assessmentTypeId);

		if (assessmentType == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + assessmentTypeId);
			}

			throw new NoSuchAssessmentTypeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				assessmentTypeId);
		}

		return assessmentType;
	}

	/**
	 * Returns the assessment type with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the assessment type
	 * @return the assessment type, or <code>null</code> if a assessment type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AssessmentType fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the assessment type with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param assessmentTypeId the primary key of the assessment type
	 * @return the assessment type, or <code>null</code> if a assessment type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType fetchByPrimaryKey(long assessmentTypeId)
		throws SystemException {
		AssessmentType assessmentType = (AssessmentType)EntityCacheUtil.getResult(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
				AssessmentTypeImpl.class, assessmentTypeId);

		if (assessmentType == _nullAssessmentType) {
			return null;
		}

		if (assessmentType == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				assessmentType = (AssessmentType)session.get(AssessmentTypeImpl.class,
						Long.valueOf(assessmentTypeId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (assessmentType != null) {
					cacheResult(assessmentType);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(AssessmentTypeModelImpl.ENTITY_CACHE_ENABLED,
						AssessmentTypeImpl.class, assessmentTypeId,
						_nullAssessmentType);
				}

				closeSession(session);
			}
		}

		return assessmentType;
	}

	/**
	 * Returns all the assessment types where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentType> findBycompany(long companyId)
		throws SystemException {
		return findBycompany(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the assessment types where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of assessment types
	 * @param end the upper bound of the range of assessment types (not inclusive)
	 * @return the range of matching assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentType> findBycompany(long companyId, int start, int end)
		throws SystemException {
		return findBycompany(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessment types where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of assessment types
	 * @param end the upper bound of the range of assessment types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentType> findBycompany(long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<AssessmentType> list = (List<AssessmentType>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AssessmentType assessmentType : list) {
				if ((companyId != assessmentType.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ASSESSMENTTYPE_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentTypeModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<AssessmentType>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment type in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment type
	 * @throws info.diit.portal.assessment.NoSuchAssessmentTypeException if a matching assessment type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType findBycompany_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentTypeException, SystemException {
		AssessmentType assessmentType = fetchBycompany_First(companyId,
				orderByComparator);

		if (assessmentType != null) {
			return assessmentType;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentTypeException(msg.toString());
	}

	/**
	 * Returns the first assessment type in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment type, or <code>null</code> if a matching assessment type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType fetchBycompany_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<AssessmentType> list = findBycompany(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment type in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment type
	 * @throws info.diit.portal.assessment.NoSuchAssessmentTypeException if a matching assessment type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType findBycompany_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentTypeException, SystemException {
		AssessmentType assessmentType = fetchBycompany_Last(companyId,
				orderByComparator);

		if (assessmentType != null) {
			return assessmentType;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentTypeException(msg.toString());
	}

	/**
	 * Returns the last assessment type in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment type, or <code>null</code> if a matching assessment type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType fetchBycompany_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBycompany(companyId);

		List<AssessmentType> list = findBycompany(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessment types before and after the current assessment type in the ordered set where companyId = &#63;.
	 *
	 * @param assessmentTypeId the primary key of the current assessment type
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment type
	 * @throws info.diit.portal.assessment.NoSuchAssessmentTypeException if a assessment type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType[] findBycompany_PrevAndNext(long assessmentTypeId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchAssessmentTypeException, SystemException {
		AssessmentType assessmentType = findByPrimaryKey(assessmentTypeId);

		Session session = null;

		try {
			session = openSession();

			AssessmentType[] array = new AssessmentTypeImpl[3];

			array[0] = getBycompany_PrevAndNext(session, assessmentType,
					companyId, orderByComparator, true);

			array[1] = assessmentType;

			array[2] = getBycompany_PrevAndNext(session, assessmentType,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AssessmentType getBycompany_PrevAndNext(Session session,
		AssessmentType assessmentType, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENTTYPE_WHERE);

		query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentTypeModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessmentType);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AssessmentType> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessment types where companyId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param type the type
	 * @return the matching assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentType> findByCompanyTypeName(long companyId,
		String type) throws SystemException {
		return findByCompanyTypeName(companyId, type, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessment types where companyId = &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param type the type
	 * @param start the lower bound of the range of assessment types
	 * @param end the upper bound of the range of assessment types (not inclusive)
	 * @return the range of matching assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentType> findByCompanyTypeName(long companyId,
		String type, int start, int end) throws SystemException {
		return findByCompanyTypeName(companyId, type, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessment types where companyId = &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param type the type
	 * @param start the lower bound of the range of assessment types
	 * @param end the upper bound of the range of assessment types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentType> findByCompanyTypeName(long companyId,
		String type, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYTYPENAME;
			finderArgs = new Object[] { companyId, type };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYTYPENAME;
			finderArgs = new Object[] {
					companyId, type,
					
					start, end, orderByComparator
				};
		}

		List<AssessmentType> list = (List<AssessmentType>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AssessmentType assessmentType : list) {
				if ((companyId != assessmentType.getCompanyId()) ||
						!Validator.equals(type, assessmentType.getType())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ASSESSMENTTYPE_WHERE);

			query.append(_FINDER_COLUMN_COMPANYTYPENAME_COMPANYID_2);

			if (type == null) {
				query.append(_FINDER_COLUMN_COMPANYTYPENAME_TYPE_1);
			}
			else {
				if (type.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_COMPANYTYPENAME_TYPE_3);
				}
				else {
					query.append(_FINDER_COLUMN_COMPANYTYPENAME_TYPE_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentTypeModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (type != null) {
					qPos.add(type);
				}

				list = (List<AssessmentType>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment type in the ordered set where companyId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment type
	 * @throws info.diit.portal.assessment.NoSuchAssessmentTypeException if a matching assessment type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType findByCompanyTypeName_First(long companyId,
		String type, OrderByComparator orderByComparator)
		throws NoSuchAssessmentTypeException, SystemException {
		AssessmentType assessmentType = fetchByCompanyTypeName_First(companyId,
				type, orderByComparator);

		if (assessmentType != null) {
			return assessmentType;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", type=");
		msg.append(type);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentTypeException(msg.toString());
	}

	/**
	 * Returns the first assessment type in the ordered set where companyId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment type, or <code>null</code> if a matching assessment type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType fetchByCompanyTypeName_First(long companyId,
		String type, OrderByComparator orderByComparator)
		throws SystemException {
		List<AssessmentType> list = findByCompanyTypeName(companyId, type, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment type in the ordered set where companyId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment type
	 * @throws info.diit.portal.assessment.NoSuchAssessmentTypeException if a matching assessment type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType findByCompanyTypeName_Last(long companyId,
		String type, OrderByComparator orderByComparator)
		throws NoSuchAssessmentTypeException, SystemException {
		AssessmentType assessmentType = fetchByCompanyTypeName_Last(companyId,
				type, orderByComparator);

		if (assessmentType != null) {
			return assessmentType;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", type=");
		msg.append(type);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentTypeException(msg.toString());
	}

	/**
	 * Returns the last assessment type in the ordered set where companyId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment type, or <code>null</code> if a matching assessment type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType fetchByCompanyTypeName_Last(long companyId,
		String type, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByCompanyTypeName(companyId, type);

		List<AssessmentType> list = findByCompanyTypeName(companyId, type,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessment types before and after the current assessment type in the ordered set where companyId = &#63; and type = &#63;.
	 *
	 * @param assessmentTypeId the primary key of the current assessment type
	 * @param companyId the company ID
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment type
	 * @throws info.diit.portal.assessment.NoSuchAssessmentTypeException if a assessment type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType[] findByCompanyTypeName_PrevAndNext(
		long assessmentTypeId, long companyId, String type,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentTypeException, SystemException {
		AssessmentType assessmentType = findByPrimaryKey(assessmentTypeId);

		Session session = null;

		try {
			session = openSession();

			AssessmentType[] array = new AssessmentTypeImpl[3];

			array[0] = getByCompanyTypeName_PrevAndNext(session,
					assessmentType, companyId, type, orderByComparator, true);

			array[1] = assessmentType;

			array[2] = getByCompanyTypeName_PrevAndNext(session,
					assessmentType, companyId, type, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AssessmentType getByCompanyTypeName_PrevAndNext(Session session,
		AssessmentType assessmentType, long companyId, String type,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENTTYPE_WHERE);

		query.append(_FINDER_COLUMN_COMPANYTYPENAME_COMPANYID_2);

		if (type == null) {
			query.append(_FINDER_COLUMN_COMPANYTYPENAME_TYPE_1);
		}
		else {
			if (type.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPANYTYPENAME_TYPE_3);
			}
			else {
				query.append(_FINDER_COLUMN_COMPANYTYPENAME_TYPE_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentTypeModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (type != null) {
			qPos.add(type);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessmentType);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AssessmentType> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessment types where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentType> findByorganization(long organizationId)
		throws SystemException {
		return findByorganization(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessment types where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of assessment types
	 * @param end the upper bound of the range of assessment types (not inclusive)
	 * @return the range of matching assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentType> findByorganization(long organizationId,
		int start, int end) throws SystemException {
		return findByorganization(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessment types where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of assessment types
	 * @param end the upper bound of the range of assessment types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentType> findByorganization(long organizationId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<AssessmentType> list = (List<AssessmentType>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AssessmentType assessmentType : list) {
				if ((organizationId != assessmentType.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ASSESSMENTTYPE_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentTypeModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				list = (List<AssessmentType>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment type in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment type
	 * @throws info.diit.portal.assessment.NoSuchAssessmentTypeException if a matching assessment type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType findByorganization_First(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentTypeException, SystemException {
		AssessmentType assessmentType = fetchByorganization_First(organizationId,
				orderByComparator);

		if (assessmentType != null) {
			return assessmentType;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentTypeException(msg.toString());
	}

	/**
	 * Returns the first assessment type in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment type, or <code>null</code> if a matching assessment type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType fetchByorganization_First(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<AssessmentType> list = findByorganization(organizationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment type in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment type
	 * @throws info.diit.portal.assessment.NoSuchAssessmentTypeException if a matching assessment type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType findByorganization_Last(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentTypeException, SystemException {
		AssessmentType assessmentType = fetchByorganization_Last(organizationId,
				orderByComparator);

		if (assessmentType != null) {
			return assessmentType;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentTypeException(msg.toString());
	}

	/**
	 * Returns the last assessment type in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment type, or <code>null</code> if a matching assessment type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType fetchByorganization_Last(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByorganization(organizationId);

		List<AssessmentType> list = findByorganization(organizationId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessment types before and after the current assessment type in the ordered set where organizationId = &#63;.
	 *
	 * @param assessmentTypeId the primary key of the current assessment type
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment type
	 * @throws info.diit.portal.assessment.NoSuchAssessmentTypeException if a assessment type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType[] findByorganization_PrevAndNext(
		long assessmentTypeId, long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentTypeException, SystemException {
		AssessmentType assessmentType = findByPrimaryKey(assessmentTypeId);

		Session session = null;

		try {
			session = openSession();

			AssessmentType[] array = new AssessmentTypeImpl[3];

			array[0] = getByorganization_PrevAndNext(session, assessmentType,
					organizationId, orderByComparator, true);

			array[1] = assessmentType;

			array[2] = getByorganization_PrevAndNext(session, assessmentType,
					organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AssessmentType getByorganization_PrevAndNext(Session session,
		AssessmentType assessmentType, long organizationId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENTTYPE_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentTypeModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessmentType);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AssessmentType> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessment types.
	 *
	 * @return the assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentType> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessment types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of assessment types
	 * @param end the upper bound of the range of assessment types (not inclusive)
	 * @return the range of assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentType> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessment types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of assessment types
	 * @param end the upper bound of the range of assessment types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentType> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<AssessmentType> list = (List<AssessmentType>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ASSESSMENTTYPE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ASSESSMENTTYPE.concat(AssessmentTypeModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<AssessmentType>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<AssessmentType>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the assessment types where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBycompany(long companyId) throws SystemException {
		for (AssessmentType assessmentType : findBycompany(companyId)) {
			remove(assessmentType);
		}
	}

	/**
	 * Removes all the assessment types where companyId = &#63; and type = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param type the type
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCompanyTypeName(long companyId, String type)
		throws SystemException {
		for (AssessmentType assessmentType : findByCompanyTypeName(companyId,
				type)) {
			remove(assessmentType);
		}
	}

	/**
	 * Removes all the assessment types where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByorganization(long organizationId)
		throws SystemException {
		for (AssessmentType assessmentType : findByorganization(organizationId)) {
			remove(assessmentType);
		}
	}

	/**
	 * Removes all the assessment types from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (AssessmentType assessmentType : findAll()) {
			remove(assessmentType);
		}
	}

	/**
	 * Returns the number of assessment types where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public int countBycompany(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ASSESSMENTTYPE_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessment types where companyId = &#63; and type = &#63;.
	 *
	 * @param companyId the company ID
	 * @param type the type
	 * @return the number of matching assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCompanyTypeName(long companyId, String type)
		throws SystemException {
		Object[] finderArgs = new Object[] { companyId, type };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANYTYPENAME,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ASSESSMENTTYPE_WHERE);

			query.append(_FINDER_COLUMN_COMPANYTYPENAME_COMPANYID_2);

			if (type == null) {
				query.append(_FINDER_COLUMN_COMPANYTYPENAME_TYPE_1);
			}
			else {
				if (type.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_COMPANYTYPENAME_TYPE_3);
				}
				else {
					query.append(_FINDER_COLUMN_COMPANYTYPENAME_TYPE_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (type != null) {
					qPos.add(type);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANYTYPENAME,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessment types where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public int countByorganization(long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ASSESSMENTTYPE_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessment types.
	 *
	 * @return the number of assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ASSESSMENTTYPE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the assessment type persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.assessment.model.AssessmentType")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<AssessmentType>> listenersList = new ArrayList<ModelListener<AssessmentType>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<AssessmentType>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AssessmentTypeImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_ASSESSMENTTYPE = "SELECT assessmentType FROM AssessmentType assessmentType";
	private static final String _SQL_SELECT_ASSESSMENTTYPE_WHERE = "SELECT assessmentType FROM AssessmentType assessmentType WHERE ";
	private static final String _SQL_COUNT_ASSESSMENTTYPE = "SELECT COUNT(assessmentType) FROM AssessmentType assessmentType";
	private static final String _SQL_COUNT_ASSESSMENTTYPE_WHERE = "SELECT COUNT(assessmentType) FROM AssessmentType assessmentType WHERE ";
	private static final String _FINDER_COLUMN_COMPANY_COMPANYID_2 = "assessmentType.companyId = ?";
	private static final String _FINDER_COLUMN_COMPANYTYPENAME_COMPANYID_2 = "assessmentType.companyId = ? AND ";
	private static final String _FINDER_COLUMN_COMPANYTYPENAME_TYPE_1 = "assessmentType.type IS NULL";
	private static final String _FINDER_COLUMN_COMPANYTYPENAME_TYPE_2 = "assessmentType.type = ?";
	private static final String _FINDER_COLUMN_COMPANYTYPENAME_TYPE_3 = "(assessmentType.type IS NULL OR assessmentType.type = ?)";
	private static final String _FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2 = "assessmentType.organizationId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "assessmentType.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AssessmentType exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No AssessmentType exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AssessmentTypePersistenceImpl.class);
	private static AssessmentType _nullAssessmentType = new AssessmentTypeImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<AssessmentType> toCacheModel() {
				return _nullAssessmentTypeCacheModel;
			}
		};

	private static CacheModel<AssessmentType> _nullAssessmentTypeCacheModel = new CacheModel<AssessmentType>() {
			public AssessmentType toEntityModel() {
				return _nullAssessmentType;
			}
		};
}
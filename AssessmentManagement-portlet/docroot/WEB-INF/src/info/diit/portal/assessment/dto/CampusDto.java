package info.diit.portal.assessment.dto;

public class CampusDto {

	private long campusId;
	private String campusName;
	public long getCampusId() {
		return campusId;
	}
	public void setCampusId(long campusId) {
		this.campusId = campusId;
	}
	public String getCampusName() {
		return campusName;
	}
	public void setCampusName(String campusName) {
		this.campusName = campusName;
	}
	public String toString(){
		return getCampusName();
	}
}

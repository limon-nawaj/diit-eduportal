package info.diit.portal.assessment.dto;

import info.diit.portal.assessment.model.Assessment;

import java.io.Serializable;

public class AssessmentTypeDto implements Serializable,Comparable<AssessmentTypeDto>{

	private long assessmentTypeId;
	private String assessmentType;
	
	public long getAssessmentTypeId() {
		return assessmentTypeId;
	}
	public void setAssessmentTypeId(long assessmentTypeId) {
		this.assessmentTypeId = assessmentTypeId;
	}
	public String getAssessmentType() {
		return assessmentType;
	}
	public void setAssessmentType(String assessmentType) {
		this.assessmentType = assessmentType;
	}
	public String toString(){
		return getAssessmentType();
	}
	
	public int compareTo(AssessmentTypeDto obj) {
		// TODO Auto-generated method stub
		
		
			return obj.getAssessmentType().compareTo(getAssessmentType());
		
	}
}

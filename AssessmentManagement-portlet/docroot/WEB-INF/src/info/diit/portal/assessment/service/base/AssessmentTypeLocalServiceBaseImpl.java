/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.service.base;

import com.liferay.counter.service.CounterLocalService;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.bean.IdentifiableBean;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.model.PersistedModel;
import com.liferay.portal.service.BaseLocalServiceImpl;
import com.liferay.portal.service.PersistedModelLocalServiceRegistryUtil;
import com.liferay.portal.service.ResourceLocalService;
import com.liferay.portal.service.ResourceService;
import com.liferay.portal.service.UserLocalService;
import com.liferay.portal.service.UserService;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;

import info.diit.portal.assessment.model.AssessmentType;
import info.diit.portal.assessment.service.AssessmentLocalService;
import info.diit.portal.assessment.service.AssessmentStudentLocalService;
import info.diit.portal.assessment.service.AssessmentTypeLocalService;
import info.diit.portal.assessment.service.persistence.AssessmentPersistence;
import info.diit.portal.assessment.service.persistence.AssessmentStudentPersistence;
import info.diit.portal.assessment.service.persistence.AssessmentTypePersistence;

import java.io.Serializable;

import java.util.List;

import javax.sql.DataSource;

/**
 * The base implementation of the assessment type local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link info.diit.portal.assessment.service.impl.AssessmentTypeLocalServiceImpl}.
 * </p>
 *
 * @author limon
 * @see info.diit.portal.assessment.service.impl.AssessmentTypeLocalServiceImpl
 * @see info.diit.portal.assessment.service.AssessmentTypeLocalServiceUtil
 * @generated
 */
public abstract class AssessmentTypeLocalServiceBaseImpl
	extends BaseLocalServiceImpl implements AssessmentTypeLocalService,
		IdentifiableBean {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link info.diit.portal.assessment.service.AssessmentTypeLocalServiceUtil} to access the assessment type local service.
	 */

	/**
	 * Adds the assessment type to the database. Also notifies the appropriate model listeners.
	 *
	 * @param assessmentType the assessment type
	 * @return the assessment type that was added
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public AssessmentType addAssessmentType(AssessmentType assessmentType)
		throws SystemException {
		assessmentType.setNew(true);

		return assessmentTypePersistence.update(assessmentType, false);
	}

	/**
	 * Creates a new assessment type with the primary key. Does not add the assessment type to the database.
	 *
	 * @param assessmentTypeId the primary key for the new assessment type
	 * @return the new assessment type
	 */
	public AssessmentType createAssessmentType(long assessmentTypeId) {
		return assessmentTypePersistence.create(assessmentTypeId);
	}

	/**
	 * Deletes the assessment type with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param assessmentTypeId the primary key of the assessment type
	 * @return the assessment type that was removed
	 * @throws PortalException if a assessment type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.DELETE)
	public AssessmentType deleteAssessmentType(long assessmentTypeId)
		throws PortalException, SystemException {
		return assessmentTypePersistence.remove(assessmentTypeId);
	}

	/**
	 * Deletes the assessment type from the database. Also notifies the appropriate model listeners.
	 *
	 * @param assessmentType the assessment type
	 * @return the assessment type that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.DELETE)
	public AssessmentType deleteAssessmentType(AssessmentType assessmentType)
		throws SystemException {
		return assessmentTypePersistence.remove(assessmentType);
	}

	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(AssessmentType.class,
			clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return assessmentTypePersistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return assessmentTypePersistence.findWithDynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return assessmentTypePersistence.findWithDynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	public long dynamicQueryCount(DynamicQuery dynamicQuery)
		throws SystemException {
		return assessmentTypePersistence.countWithDynamicQuery(dynamicQuery);
	}

	public AssessmentType fetchAssessmentType(long assessmentTypeId)
		throws SystemException {
		return assessmentTypePersistence.fetchByPrimaryKey(assessmentTypeId);
	}

	/**
	 * Returns the assessment type with the primary key.
	 *
	 * @param assessmentTypeId the primary key of the assessment type
	 * @return the assessment type
	 * @throws PortalException if a assessment type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AssessmentType getAssessmentType(long assessmentTypeId)
		throws PortalException, SystemException {
		return assessmentTypePersistence.findByPrimaryKey(assessmentTypeId);
	}

	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException, SystemException {
		return assessmentTypePersistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns a range of all the assessment types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of assessment types
	 * @param end the upper bound of the range of assessment types (not inclusive)
	 * @return the range of assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public List<AssessmentType> getAssessmentTypes(int start, int end)
		throws SystemException {
		return assessmentTypePersistence.findAll(start, end);
	}

	/**
	 * Returns the number of assessment types.
	 *
	 * @return the number of assessment types
	 * @throws SystemException if a system exception occurred
	 */
	public int getAssessmentTypesCount() throws SystemException {
		return assessmentTypePersistence.countAll();
	}

	/**
	 * Updates the assessment type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param assessmentType the assessment type
	 * @return the assessment type that was updated
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public AssessmentType updateAssessmentType(AssessmentType assessmentType)
		throws SystemException {
		return updateAssessmentType(assessmentType, true);
	}

	/**
	 * Updates the assessment type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param assessmentType the assessment type
	 * @param merge whether to merge the assessment type with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	 * @return the assessment type that was updated
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public AssessmentType updateAssessmentType(AssessmentType assessmentType,
		boolean merge) throws SystemException {
		assessmentType.setNew(false);

		return assessmentTypePersistence.update(assessmentType, merge);
	}

	/**
	 * Returns the assessment local service.
	 *
	 * @return the assessment local service
	 */
	public AssessmentLocalService getAssessmentLocalService() {
		return assessmentLocalService;
	}

	/**
	 * Sets the assessment local service.
	 *
	 * @param assessmentLocalService the assessment local service
	 */
	public void setAssessmentLocalService(
		AssessmentLocalService assessmentLocalService) {
		this.assessmentLocalService = assessmentLocalService;
	}

	/**
	 * Returns the assessment persistence.
	 *
	 * @return the assessment persistence
	 */
	public AssessmentPersistence getAssessmentPersistence() {
		return assessmentPersistence;
	}

	/**
	 * Sets the assessment persistence.
	 *
	 * @param assessmentPersistence the assessment persistence
	 */
	public void setAssessmentPersistence(
		AssessmentPersistence assessmentPersistence) {
		this.assessmentPersistence = assessmentPersistence;
	}

	/**
	 * Returns the assessment student local service.
	 *
	 * @return the assessment student local service
	 */
	public AssessmentStudentLocalService getAssessmentStudentLocalService() {
		return assessmentStudentLocalService;
	}

	/**
	 * Sets the assessment student local service.
	 *
	 * @param assessmentStudentLocalService the assessment student local service
	 */
	public void setAssessmentStudentLocalService(
		AssessmentStudentLocalService assessmentStudentLocalService) {
		this.assessmentStudentLocalService = assessmentStudentLocalService;
	}

	/**
	 * Returns the assessment student persistence.
	 *
	 * @return the assessment student persistence
	 */
	public AssessmentStudentPersistence getAssessmentStudentPersistence() {
		return assessmentStudentPersistence;
	}

	/**
	 * Sets the assessment student persistence.
	 *
	 * @param assessmentStudentPersistence the assessment student persistence
	 */
	public void setAssessmentStudentPersistence(
		AssessmentStudentPersistence assessmentStudentPersistence) {
		this.assessmentStudentPersistence = assessmentStudentPersistence;
	}

	/**
	 * Returns the assessment type local service.
	 *
	 * @return the assessment type local service
	 */
	public AssessmentTypeLocalService getAssessmentTypeLocalService() {
		return assessmentTypeLocalService;
	}

	/**
	 * Sets the assessment type local service.
	 *
	 * @param assessmentTypeLocalService the assessment type local service
	 */
	public void setAssessmentTypeLocalService(
		AssessmentTypeLocalService assessmentTypeLocalService) {
		this.assessmentTypeLocalService = assessmentTypeLocalService;
	}

	/**
	 * Returns the assessment type persistence.
	 *
	 * @return the assessment type persistence
	 */
	public AssessmentTypePersistence getAssessmentTypePersistence() {
		return assessmentTypePersistence;
	}

	/**
	 * Sets the assessment type persistence.
	 *
	 * @param assessmentTypePersistence the assessment type persistence
	 */
	public void setAssessmentTypePersistence(
		AssessmentTypePersistence assessmentTypePersistence) {
		this.assessmentTypePersistence = assessmentTypePersistence;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public CounterLocalService getCounterLocalService() {
		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public ResourceLocalService getResourceLocalService() {
		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the resource remote service.
	 *
	 * @return the resource remote service
	 */
	public ResourceService getResourceService() {
		return resourceService;
	}

	/**
	 * Sets the resource remote service.
	 *
	 * @param resourceService the resource remote service
	 */
	public void setResourceService(ResourceService resourceService) {
		this.resourceService = resourceService;
	}

	/**
	 * Returns the resource persistence.
	 *
	 * @return the resource persistence
	 */
	public ResourcePersistence getResourcePersistence() {
		return resourcePersistence;
	}

	/**
	 * Sets the resource persistence.
	 *
	 * @param resourcePersistence the resource persistence
	 */
	public void setResourcePersistence(ResourcePersistence resourcePersistence) {
		this.resourcePersistence = resourcePersistence;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public UserLocalService getUserLocalService() {
		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user remote service.
	 *
	 * @return the user remote service
	 */
	public UserService getUserService() {
		return userService;
	}

	/**
	 * Sets the user remote service.
	 *
	 * @param userService the user remote service
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	public void afterPropertiesSet() {
		PersistedModelLocalServiceRegistryUtil.register("info.diit.portal.assessment.model.AssessmentType",
			assessmentTypeLocalService);
	}

	public void destroy() {
		PersistedModelLocalServiceRegistryUtil.unregister(
			"info.diit.portal.assessment.model.AssessmentType");
	}

	/**
	 * Returns the Spring bean ID for this bean.
	 *
	 * @return the Spring bean ID for this bean
	 */
	public String getBeanIdentifier() {
		return _beanIdentifier;
	}

	/**
	 * Sets the Spring bean ID for this bean.
	 *
	 * @param beanIdentifier the Spring bean ID for this bean
	 */
	public void setBeanIdentifier(String beanIdentifier) {
		_beanIdentifier = beanIdentifier;
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		return _clpInvoker.invokeMethod(name, parameterTypes, arguments);
	}

	protected Class<?> getModelClass() {
		return AssessmentType.class;
	}

	protected String getModelClassName() {
		return AssessmentType.class.getName();
	}

	/**
	 * Performs an SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) throws SystemException {
		try {
			DataSource dataSource = assessmentTypePersistence.getDataSource();

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(dataSource,
					sql, new int[0]);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(type = AssessmentLocalService.class)
	protected AssessmentLocalService assessmentLocalService;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentLocalService.class)
	protected AssessmentStudentLocalService assessmentStudentLocalService;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypeLocalService.class)
	protected AssessmentTypeLocalService assessmentTypeLocalService;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = CounterLocalService.class)
	protected CounterLocalService counterLocalService;
	@BeanReference(type = ResourceLocalService.class)
	protected ResourceLocalService resourceLocalService;
	@BeanReference(type = ResourceService.class)
	protected ResourceService resourceService;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserLocalService.class)
	protected UserLocalService userLocalService;
	@BeanReference(type = UserService.class)
	protected UserService userService;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private String _beanIdentifier;
	private AssessmentTypeLocalServiceClpInvoker _clpInvoker = new AssessmentTypeLocalServiceClpInvoker();
}
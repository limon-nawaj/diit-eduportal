package info.diit.portal.assessment.dto;

import info.diit.portal.assessment.model.Assessment;
import info.diit.portal.assessment.model.impl.AssessmentImpl;

import java.util.Date;

public class AssessmentDto {

	private long assessmentId;
	private long batchId;
	private String batchName;
	private long subjectId;
	private String subjectName;
	private long assessmentStudentId;
	private Date assessmentDate;
	private Date resultDate;
	private double totalMark;
	private String status;
	private long assessmentTypeId;
	private String assessmentType;
	
	private Assessment assessment;
	
	public AssessmentDto(){
		assessment = new AssessmentImpl();
		assessment.setNew(true);
	}
	
	public Assessment getAssessment() {
		return assessment;
	}
	public void setAssessment(Assessment assessment) {
		this.assessment = assessment;
	}
	public long getAssessmentId() {
		return assessmentId;
	}
	public void setAssessmentId(long assessmentId) {
		this.assessmentId = assessmentId;
	}
	public double getTotalMark() {
		return totalMark;
	}
	public void setTotalMark(double totalMark) {
		this.totalMark = totalMark;
	}
	
	public long getBatchId() {
		return batchId;
	}
	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public long getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(long subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public long getAssessmentStudentId() {
		return assessmentStudentId;
	}
	public void setAssessmentStudentId(long assessmentStudentId) {
		this.assessmentStudentId = assessmentStudentId;
	}
	public Date getAssessmentDate() {
		return assessmentDate;
	}
	public void setAssessmentDate(Date assessmentDate) {
		this.assessmentDate = assessmentDate;
	}
	public Date getResultDate() {
		return resultDate;
	}
	public void setResultDate(Date resultDate) {
		this.resultDate = resultDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public long getAssessmentTypeId() {
		return assessmentTypeId;
	}
	public void setAssessmentTypeId(long assessmentTypeId) {
		this.assessmentTypeId = assessmentTypeId;
	}
	
	public String getAssessmentType() {
		return assessmentType;
	}
	public void setAssessmentType(String assessmentType) {
		this.assessmentType = assessmentType;
	}
	
}

/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.assessment.NoSuchAssessmentException;
import info.diit.portal.assessment.model.Assessment;
import info.diit.portal.assessment.model.impl.AssessmentImpl;
import info.diit.portal.assessment.model.impl.AssessmentModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the assessment service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see AssessmentPersistence
 * @see AssessmentUtil
 * @generated
 */
public class AssessmentPersistenceImpl extends BasePersistenceImpl<Assessment>
	implements AssessmentPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AssessmentUtil} to access the assessment persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AssessmentImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByuserId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuserId",
			new String[] { Long.class.getName() },
			AssessmentModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuserId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY = new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBycompany",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY =
		new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBycompany",
			new String[] { Long.class.getName() },
			AssessmentModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANY = new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBycompany",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByorganization",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByorganization",
			new String[] { Long.class.getName() },
			AssessmentModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATION = new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByorganization",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCH = new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBybatch",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH = new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBybatch",
			new String[] { Long.class.getName() },
			AssessmentModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCH = new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBybatch",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SUBJECTASSESSMENTTYPE =
		new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBysubjectAssessmentType",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTASSESSMENTTYPE =
		new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBysubjectAssessmentType",
			new String[] { Long.class.getName(), Long.class.getName() },
			AssessmentModelImpl.SUBJECTID_COLUMN_BITMASK |
			AssessmentModelImpl.ASSESSMENTTYPEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SUBJECTASSESSMENTTYPE = new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countBysubjectAssessmentType",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHSTATUS =
		new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBatchStatus",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSTATUS =
		new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBatchStatus",
			new String[] { Long.class.getName(), Long.class.getName() },
			AssessmentModelImpl.BATCHID_COLUMN_BITMASK |
			AssessmentModelImpl.STATUS_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCHSTATUS = new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBatchStatus",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SUBJECTSTATUS =
		new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBySubjectStatus",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTSTATUS =
		new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBySubjectStatus",
			new String[] { Long.class.getName(), Long.class.getName() },
			AssessmentModelImpl.SUBJECTID_COLUMN_BITMASK |
			AssessmentModelImpl.STATUS_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SUBJECTSTATUS = new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBySubjectStatus",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, AssessmentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the assessment in the entity cache if it is enabled.
	 *
	 * @param assessment the assessment
	 */
	public void cacheResult(Assessment assessment) {
		EntityCacheUtil.putResult(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentImpl.class, assessment.getPrimaryKey(), assessment);

		assessment.resetOriginalValues();
	}

	/**
	 * Caches the assessments in the entity cache if it is enabled.
	 *
	 * @param assessments the assessments
	 */
	public void cacheResult(List<Assessment> assessments) {
		for (Assessment assessment : assessments) {
			if (EntityCacheUtil.getResult(
						AssessmentModelImpl.ENTITY_CACHE_ENABLED,
						AssessmentImpl.class, assessment.getPrimaryKey()) == null) {
				cacheResult(assessment);
			}
			else {
				assessment.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all assessments.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AssessmentImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AssessmentImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the assessment.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Assessment assessment) {
		EntityCacheUtil.removeResult(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentImpl.class, assessment.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Assessment> assessments) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Assessment assessment : assessments) {
			EntityCacheUtil.removeResult(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
				AssessmentImpl.class, assessment.getPrimaryKey());
		}
	}

	/**
	 * Creates a new assessment with the primary key. Does not add the assessment to the database.
	 *
	 * @param assessmentId the primary key for the new assessment
	 * @return the new assessment
	 */
	public Assessment create(long assessmentId) {
		Assessment assessment = new AssessmentImpl();

		assessment.setNew(true);
		assessment.setPrimaryKey(assessmentId);

		return assessment;
	}

	/**
	 * Removes the assessment with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param assessmentId the primary key of the assessment
	 * @return the assessment that was removed
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment remove(long assessmentId)
		throws NoSuchAssessmentException, SystemException {
		return remove(Long.valueOf(assessmentId));
	}

	/**
	 * Removes the assessment with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the assessment
	 * @return the assessment that was removed
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Assessment remove(Serializable primaryKey)
		throws NoSuchAssessmentException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Assessment assessment = (Assessment)session.get(AssessmentImpl.class,
					primaryKey);

			if (assessment == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAssessmentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(assessment);
		}
		catch (NoSuchAssessmentException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Assessment removeImpl(Assessment assessment)
		throws SystemException {
		assessment = toUnwrappedModel(assessment);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, assessment);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(assessment);

		return assessment;
	}

	@Override
	public Assessment updateImpl(
		info.diit.portal.assessment.model.Assessment assessment, boolean merge)
		throws SystemException {
		assessment = toUnwrappedModel(assessment);

		boolean isNew = assessment.isNew();

		AssessmentModelImpl assessmentModelImpl = (AssessmentModelImpl)assessment;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, assessment, merge);

			assessment.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !AssessmentModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((assessmentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentModelImpl.getOriginalUserId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] {
						Long.valueOf(assessmentModelImpl.getUserId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}

			if ((assessmentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);

				args = new Object[] {
						Long.valueOf(assessmentModelImpl.getCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);
			}

			if ((assessmentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(assessmentModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);
			}

			if ((assessmentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCH, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH,
					args);

				args = new Object[] {
						Long.valueOf(assessmentModelImpl.getBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCH, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH,
					args);
			}

			if ((assessmentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTASSESSMENTTYPE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentModelImpl.getOriginalSubjectId()),
						Long.valueOf(assessmentModelImpl.getOriginalAssessmentTypeId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBJECTASSESSMENTTYPE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTASSESSMENTTYPE,
					args);

				args = new Object[] {
						Long.valueOf(assessmentModelImpl.getSubjectId()),
						Long.valueOf(assessmentModelImpl.getAssessmentTypeId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBJECTASSESSMENTTYPE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTASSESSMENTTYPE,
					args);
			}

			if ((assessmentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSTATUS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentModelImpl.getOriginalBatchId()),
						Long.valueOf(assessmentModelImpl.getOriginalStatus())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHSTATUS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSTATUS,
					args);

				args = new Object[] {
						Long.valueOf(assessmentModelImpl.getBatchId()),
						Long.valueOf(assessmentModelImpl.getStatus())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHSTATUS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSTATUS,
					args);
			}

			if ((assessmentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTSTATUS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(assessmentModelImpl.getOriginalSubjectId()),
						Long.valueOf(assessmentModelImpl.getOriginalStatus())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBJECTSTATUS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTSTATUS,
					args);

				args = new Object[] {
						Long.valueOf(assessmentModelImpl.getSubjectId()),
						Long.valueOf(assessmentModelImpl.getStatus())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBJECTSTATUS,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTSTATUS,
					args);
			}
		}

		EntityCacheUtil.putResult(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
			AssessmentImpl.class, assessment.getPrimaryKey(), assessment);

		return assessment;
	}

	protected Assessment toUnwrappedModel(Assessment assessment) {
		if (assessment instanceof AssessmentImpl) {
			return assessment;
		}

		AssessmentImpl assessmentImpl = new AssessmentImpl();

		assessmentImpl.setNew(assessment.isNew());
		assessmentImpl.setPrimaryKey(assessment.getPrimaryKey());

		assessmentImpl.setAssessmentId(assessment.getAssessmentId());
		assessmentImpl.setCompanyId(assessment.getCompanyId());
		assessmentImpl.setOrganizationId(assessment.getOrganizationId());
		assessmentImpl.setUserId(assessment.getUserId());
		assessmentImpl.setUserName(assessment.getUserName());
		assessmentImpl.setCreateDate(assessment.getCreateDate());
		assessmentImpl.setModifiedDate(assessment.getModifiedDate());
		assessmentImpl.setAssessmentDate(assessment.getAssessmentDate());
		assessmentImpl.setResultDate(assessment.getResultDate());
		assessmentImpl.setTotalMark(assessment.getTotalMark());
		assessmentImpl.setAssessmentTypeId(assessment.getAssessmentTypeId());
		assessmentImpl.setBatchId(assessment.getBatchId());
		assessmentImpl.setSubjectId(assessment.getSubjectId());
		assessmentImpl.setStatus(assessment.getStatus());

		return assessmentImpl;
	}

	/**
	 * Returns the assessment with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the assessment
	 * @return the assessment
	 * @throws com.liferay.portal.NoSuchModelException if a assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Assessment findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the assessment with the primary key or throws a {@link info.diit.portal.assessment.NoSuchAssessmentException} if it could not be found.
	 *
	 * @param assessmentId the primary key of the assessment
	 * @return the assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findByPrimaryKey(long assessmentId)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchByPrimaryKey(assessmentId);

		if (assessment == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + assessmentId);
			}

			throw new NoSuchAssessmentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				assessmentId);
		}

		return assessment;
	}

	/**
	 * Returns the assessment with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the assessment
	 * @return the assessment, or <code>null</code> if a assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Assessment fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the assessment with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param assessmentId the primary key of the assessment
	 * @return the assessment, or <code>null</code> if a assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchByPrimaryKey(long assessmentId)
		throws SystemException {
		Assessment assessment = (Assessment)EntityCacheUtil.getResult(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
				AssessmentImpl.class, assessmentId);

		if (assessment == _nullAssessment) {
			return null;
		}

		if (assessment == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				assessment = (Assessment)session.get(AssessmentImpl.class,
						Long.valueOf(assessmentId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (assessment != null) {
					cacheResult(assessment);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(AssessmentModelImpl.ENTITY_CACHE_ENABLED,
						AssessmentImpl.class, assessmentId, _nullAssessment);
				}

				closeSession(session);
			}
		}

		return assessment;
	}

	/**
	 * Returns all the assessments where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findByuserId(long userId) throws SystemException {
		return findByuserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessments where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @return the range of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findByuserId(long userId, int start, int end)
		throws SystemException {
		return findByuserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessments where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findByuserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<Assessment> list = (List<Assessment>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Assessment assessment : list) {
				if ((userId != assessment.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				list = (List<Assessment>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findByuserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchByuserId_First(userId, orderByComparator);

		if (assessment != null) {
			return assessment;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentException(msg.toString());
	}

	/**
	 * Returns the first assessment in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment, or <code>null</code> if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchByuserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Assessment> list = findByuserId(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findByuserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchByuserId_Last(userId, orderByComparator);

		if (assessment != null) {
			return assessment;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentException(msg.toString());
	}

	/**
	 * Returns the last assessment in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment, or <code>null</code> if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchByuserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByuserId(userId);

		List<Assessment> list = findByuserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessments before and after the current assessment in the ordered set where userId = &#63;.
	 *
	 * @param assessmentId the primary key of the current assessment
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment[] findByuserId_PrevAndNext(long assessmentId,
		long userId, OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = findByPrimaryKey(assessmentId);

		Session session = null;

		try {
			session = openSession();

			Assessment[] array = new AssessmentImpl[3];

			array[0] = getByuserId_PrevAndNext(session, assessment, userId,
					orderByComparator, true);

			array[1] = assessment;

			array[2] = getByuserId_PrevAndNext(session, assessment, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Assessment getByuserId_PrevAndNext(Session session,
		Assessment assessment, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENT_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessment);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Assessment> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessments where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findBycompany(long companyId)
		throws SystemException {
		return findBycompany(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the assessments where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @return the range of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findBycompany(long companyId, int start, int end)
		throws SystemException {
		return findBycompany(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessments where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findBycompany(long companyId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<Assessment> list = (List<Assessment>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Assessment assessment : list) {
				if ((companyId != assessment.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<Assessment>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findBycompany_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchBycompany_First(companyId,
				orderByComparator);

		if (assessment != null) {
			return assessment;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentException(msg.toString());
	}

	/**
	 * Returns the first assessment in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment, or <code>null</code> if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchBycompany_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Assessment> list = findBycompany(companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findBycompany_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchBycompany_Last(companyId, orderByComparator);

		if (assessment != null) {
			return assessment;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentException(msg.toString());
	}

	/**
	 * Returns the last assessment in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment, or <code>null</code> if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchBycompany_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBycompany(companyId);

		List<Assessment> list = findBycompany(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessments before and after the current assessment in the ordered set where companyId = &#63;.
	 *
	 * @param assessmentId the primary key of the current assessment
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment[] findBycompany_PrevAndNext(long assessmentId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = findByPrimaryKey(assessmentId);

		Session session = null;

		try {
			session = openSession();

			Assessment[] array = new AssessmentImpl[3];

			array[0] = getBycompany_PrevAndNext(session, assessment, companyId,
					orderByComparator, true);

			array[1] = assessment;

			array[2] = getBycompany_PrevAndNext(session, assessment, companyId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Assessment getBycompany_PrevAndNext(Session session,
		Assessment assessment, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENT_WHERE);

		query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessment);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Assessment> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessments where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findByorganization(long organizationId)
		throws SystemException {
		return findByorganization(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessments where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @return the range of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findByorganization(long organizationId, int start,
		int end) throws SystemException {
		return findByorganization(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessments where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findByorganization(long organizationId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<Assessment> list = (List<Assessment>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Assessment assessment : list) {
				if ((organizationId != assessment.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				list = (List<Assessment>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findByorganization_First(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchByorganization_First(organizationId,
				orderByComparator);

		if (assessment != null) {
			return assessment;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentException(msg.toString());
	}

	/**
	 * Returns the first assessment in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment, or <code>null</code> if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchByorganization_First(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Assessment> list = findByorganization(organizationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findByorganization_Last(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchByorganization_Last(organizationId,
				orderByComparator);

		if (assessment != null) {
			return assessment;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentException(msg.toString());
	}

	/**
	 * Returns the last assessment in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment, or <code>null</code> if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchByorganization_Last(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByorganization(organizationId);

		List<Assessment> list = findByorganization(organizationId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessments before and after the current assessment in the ordered set where organizationId = &#63;.
	 *
	 * @param assessmentId the primary key of the current assessment
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment[] findByorganization_PrevAndNext(long assessmentId,
		long organizationId, OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = findByPrimaryKey(assessmentId);

		Session session = null;

		try {
			session = openSession();

			Assessment[] array = new AssessmentImpl[3];

			array[0] = getByorganization_PrevAndNext(session, assessment,
					organizationId, orderByComparator, true);

			array[1] = assessment;

			array[2] = getByorganization_PrevAndNext(session, assessment,
					organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Assessment getByorganization_PrevAndNext(Session session,
		Assessment assessment, long organizationId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENT_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessment);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Assessment> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessments where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findBybatch(long batchId) throws SystemException {
		return findBybatch(batchId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessments where batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @return the range of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findBybatch(long batchId, int start, int end)
		throws SystemException {
		return findBybatch(batchId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessments where batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findBybatch(long batchId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH;
			finderArgs = new Object[] { batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCH;
			finderArgs = new Object[] { batchId, start, end, orderByComparator };
		}

		List<Assessment> list = (List<Assessment>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Assessment assessment : list) {
				if ((batchId != assessment.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				list = (List<Assessment>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findBybatch_First(long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchBybatch_First(batchId, orderByComparator);

		if (assessment != null) {
			return assessment;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentException(msg.toString());
	}

	/**
	 * Returns the first assessment in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment, or <code>null</code> if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchBybatch_First(long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Assessment> list = findBybatch(batchId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findBybatch_Last(long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchBybatch_Last(batchId, orderByComparator);

		if (assessment != null) {
			return assessment;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentException(msg.toString());
	}

	/**
	 * Returns the last assessment in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment, or <code>null</code> if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchBybatch_Last(long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBybatch(batchId);

		List<Assessment> list = findBybatch(batchId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessments before and after the current assessment in the ordered set where batchId = &#63;.
	 *
	 * @param assessmentId the primary key of the current assessment
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment[] findBybatch_PrevAndNext(long assessmentId,
		long batchId, OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = findByPrimaryKey(assessmentId);

		Session session = null;

		try {
			session = openSession();

			Assessment[] array = new AssessmentImpl[3];

			array[0] = getBybatch_PrevAndNext(session, assessment, batchId,
					orderByComparator, true);

			array[1] = assessment;

			array[2] = getBybatch_PrevAndNext(session, assessment, batchId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Assessment getBybatch_PrevAndNext(Session session,
		Assessment assessment, long batchId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENT_WHERE);

		query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessment);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Assessment> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessments where subjectId = &#63; and assessmentTypeId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param assessmentTypeId the assessment type ID
	 * @return the matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findBysubjectAssessmentType(long subjectId,
		long assessmentTypeId) throws SystemException {
		return findBysubjectAssessmentType(subjectId, assessmentTypeId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessments where subjectId = &#63; and assessmentTypeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param subjectId the subject ID
	 * @param assessmentTypeId the assessment type ID
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @return the range of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findBysubjectAssessmentType(long subjectId,
		long assessmentTypeId, int start, int end) throws SystemException {
		return findBysubjectAssessmentType(subjectId, assessmentTypeId, start,
			end, null);
	}

	/**
	 * Returns an ordered range of all the assessments where subjectId = &#63; and assessmentTypeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param subjectId the subject ID
	 * @param assessmentTypeId the assessment type ID
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findBysubjectAssessmentType(long subjectId,
		long assessmentTypeId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTASSESSMENTTYPE;
			finderArgs = new Object[] { subjectId, assessmentTypeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SUBJECTASSESSMENTTYPE;
			finderArgs = new Object[] {
					subjectId, assessmentTypeId,
					
					start, end, orderByComparator
				};
		}

		List<Assessment> list = (List<Assessment>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Assessment assessment : list) {
				if ((subjectId != assessment.getSubjectId()) ||
						(assessmentTypeId != assessment.getAssessmentTypeId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_SUBJECTASSESSMENTTYPE_SUBJECTID_2);

			query.append(_FINDER_COLUMN_SUBJECTASSESSMENTTYPE_ASSESSMENTTYPEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				qPos.add(assessmentTypeId);

				list = (List<Assessment>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment in the ordered set where subjectId = &#63; and assessmentTypeId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param assessmentTypeId the assessment type ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findBysubjectAssessmentType_First(long subjectId,
		long assessmentTypeId, OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchBysubjectAssessmentType_First(subjectId,
				assessmentTypeId, orderByComparator);

		if (assessment != null) {
			return assessment;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("subjectId=");
		msg.append(subjectId);

		msg.append(", assessmentTypeId=");
		msg.append(assessmentTypeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentException(msg.toString());
	}

	/**
	 * Returns the first assessment in the ordered set where subjectId = &#63; and assessmentTypeId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param assessmentTypeId the assessment type ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment, or <code>null</code> if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchBysubjectAssessmentType_First(long subjectId,
		long assessmentTypeId, OrderByComparator orderByComparator)
		throws SystemException {
		List<Assessment> list = findBysubjectAssessmentType(subjectId,
				assessmentTypeId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment in the ordered set where subjectId = &#63; and assessmentTypeId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param assessmentTypeId the assessment type ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findBysubjectAssessmentType_Last(long subjectId,
		long assessmentTypeId, OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchBysubjectAssessmentType_Last(subjectId,
				assessmentTypeId, orderByComparator);

		if (assessment != null) {
			return assessment;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("subjectId=");
		msg.append(subjectId);

		msg.append(", assessmentTypeId=");
		msg.append(assessmentTypeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentException(msg.toString());
	}

	/**
	 * Returns the last assessment in the ordered set where subjectId = &#63; and assessmentTypeId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param assessmentTypeId the assessment type ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment, or <code>null</code> if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchBysubjectAssessmentType_Last(long subjectId,
		long assessmentTypeId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countBysubjectAssessmentType(subjectId, assessmentTypeId);

		List<Assessment> list = findBysubjectAssessmentType(subjectId,
				assessmentTypeId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessments before and after the current assessment in the ordered set where subjectId = &#63; and assessmentTypeId = &#63;.
	 *
	 * @param assessmentId the primary key of the current assessment
	 * @param subjectId the subject ID
	 * @param assessmentTypeId the assessment type ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment[] findBysubjectAssessmentType_PrevAndNext(
		long assessmentId, long subjectId, long assessmentTypeId,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = findByPrimaryKey(assessmentId);

		Session session = null;

		try {
			session = openSession();

			Assessment[] array = new AssessmentImpl[3];

			array[0] = getBysubjectAssessmentType_PrevAndNext(session,
					assessment, subjectId, assessmentTypeId, orderByComparator,
					true);

			array[1] = assessment;

			array[2] = getBysubjectAssessmentType_PrevAndNext(session,
					assessment, subjectId, assessmentTypeId, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Assessment getBysubjectAssessmentType_PrevAndNext(
		Session session, Assessment assessment, long subjectId,
		long assessmentTypeId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENT_WHERE);

		query.append(_FINDER_COLUMN_SUBJECTASSESSMENTTYPE_SUBJECTID_2);

		query.append(_FINDER_COLUMN_SUBJECTASSESSMENTTYPE_ASSESSMENTTYPEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(subjectId);

		qPos.add(assessmentTypeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessment);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Assessment> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessments where batchId = &#63; and status = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param status the status
	 * @return the matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findByBatchStatus(long batchId, long status)
		throws SystemException {
		return findByBatchStatus(batchId, status, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessments where batchId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param status the status
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @return the range of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findByBatchStatus(long batchId, long status,
		int start, int end) throws SystemException {
		return findByBatchStatus(batchId, status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessments where batchId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param status the status
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findByBatchStatus(long batchId, long status,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSTATUS;
			finderArgs = new Object[] { batchId, status };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHSTATUS;
			finderArgs = new Object[] {
					batchId, status,
					
					start, end, orderByComparator
				};
		}

		List<Assessment> list = (List<Assessment>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Assessment assessment : list) {
				if ((batchId != assessment.getBatchId()) ||
						(status != assessment.getStatus())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_BATCHSTATUS_BATCHID_2);

			query.append(_FINDER_COLUMN_BATCHSTATUS_STATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				qPos.add(status);

				list = (List<Assessment>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment in the ordered set where batchId = &#63; and status = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findByBatchStatus_First(long batchId, long status,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchByBatchStatus_First(batchId, status,
				orderByComparator);

		if (assessment != null) {
			return assessment;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentException(msg.toString());
	}

	/**
	 * Returns the first assessment in the ordered set where batchId = &#63; and status = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment, or <code>null</code> if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchByBatchStatus_First(long batchId, long status,
		OrderByComparator orderByComparator) throws SystemException {
		List<Assessment> list = findByBatchStatus(batchId, status, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment in the ordered set where batchId = &#63; and status = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findByBatchStatus_Last(long batchId, long status,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchByBatchStatus_Last(batchId, status,
				orderByComparator);

		if (assessment != null) {
			return assessment;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentException(msg.toString());
	}

	/**
	 * Returns the last assessment in the ordered set where batchId = &#63; and status = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment, or <code>null</code> if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchByBatchStatus_Last(long batchId, long status,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBatchStatus(batchId, status);

		List<Assessment> list = findByBatchStatus(batchId, status, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessments before and after the current assessment in the ordered set where batchId = &#63; and status = &#63;.
	 *
	 * @param assessmentId the primary key of the current assessment
	 * @param batchId the batch ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment[] findByBatchStatus_PrevAndNext(long assessmentId,
		long batchId, long status, OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = findByPrimaryKey(assessmentId);

		Session session = null;

		try {
			session = openSession();

			Assessment[] array = new AssessmentImpl[3];

			array[0] = getByBatchStatus_PrevAndNext(session, assessment,
					batchId, status, orderByComparator, true);

			array[1] = assessment;

			array[2] = getByBatchStatus_PrevAndNext(session, assessment,
					batchId, status, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Assessment getByBatchStatus_PrevAndNext(Session session,
		Assessment assessment, long batchId, long status,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENT_WHERE);

		query.append(_FINDER_COLUMN_BATCHSTATUS_BATCHID_2);

		query.append(_FINDER_COLUMN_BATCHSTATUS_STATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(batchId);

		qPos.add(status);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessment);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Assessment> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessments where subjectId = &#63; and status = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param status the status
	 * @return the matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findBySubjectStatus(long subjectId, long status)
		throws SystemException {
		return findBySubjectStatus(subjectId, status, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessments where subjectId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param subjectId the subject ID
	 * @param status the status
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @return the range of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findBySubjectStatus(long subjectId, long status,
		int start, int end) throws SystemException {
		return findBySubjectStatus(subjectId, status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessments where subjectId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param subjectId the subject ID
	 * @param status the status
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findBySubjectStatus(long subjectId, long status,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTSTATUS;
			finderArgs = new Object[] { subjectId, status };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SUBJECTSTATUS;
			finderArgs = new Object[] {
					subjectId, status,
					
					start, end, orderByComparator
				};
		}

		List<Assessment> list = (List<Assessment>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Assessment assessment : list) {
				if ((subjectId != assessment.getSubjectId()) ||
						(status != assessment.getStatus())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_SUBJECTSTATUS_SUBJECTID_2);

			query.append(_FINDER_COLUMN_SUBJECTSTATUS_STATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AssessmentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				qPos.add(status);

				list = (List<Assessment>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assessment in the ordered set where subjectId = &#63; and status = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findBySubjectStatus_First(long subjectId, long status,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchBySubjectStatus_First(subjectId, status,
				orderByComparator);

		if (assessment != null) {
			return assessment;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("subjectId=");
		msg.append(subjectId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentException(msg.toString());
	}

	/**
	 * Returns the first assessment in the ordered set where subjectId = &#63; and status = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assessment, or <code>null</code> if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchBySubjectStatus_First(long subjectId, long status,
		OrderByComparator orderByComparator) throws SystemException {
		List<Assessment> list = findBySubjectStatus(subjectId, status, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assessment in the ordered set where subjectId = &#63; and status = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment findBySubjectStatus_Last(long subjectId, long status,
		OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = fetchBySubjectStatus_Last(subjectId, status,
				orderByComparator);

		if (assessment != null) {
			return assessment;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("subjectId=");
		msg.append(subjectId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAssessmentException(msg.toString());
	}

	/**
	 * Returns the last assessment in the ordered set where subjectId = &#63; and status = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assessment, or <code>null</code> if a matching assessment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment fetchBySubjectStatus_Last(long subjectId, long status,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBySubjectStatus(subjectId, status);

		List<Assessment> list = findBySubjectStatus(subjectId, status,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assessments before and after the current assessment in the ordered set where subjectId = &#63; and status = &#63;.
	 *
	 * @param assessmentId the primary key of the current assessment
	 * @param subjectId the subject ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assessment
	 * @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Assessment[] findBySubjectStatus_PrevAndNext(long assessmentId,
		long subjectId, long status, OrderByComparator orderByComparator)
		throws NoSuchAssessmentException, SystemException {
		Assessment assessment = findByPrimaryKey(assessmentId);

		Session session = null;

		try {
			session = openSession();

			Assessment[] array = new AssessmentImpl[3];

			array[0] = getBySubjectStatus_PrevAndNext(session, assessment,
					subjectId, status, orderByComparator, true);

			array[1] = assessment;

			array[2] = getBySubjectStatus_PrevAndNext(session, assessment,
					subjectId, status, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Assessment getBySubjectStatus_PrevAndNext(Session session,
		Assessment assessment, long subjectId, long status,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSESSMENT_WHERE);

		query.append(_FINDER_COLUMN_SUBJECTSTATUS_SUBJECTID_2);

		query.append(_FINDER_COLUMN_SUBJECTSTATUS_STATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AssessmentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(subjectId);

		qPos.add(status);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assessment);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Assessment> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the assessments.
	 *
	 * @return the assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assessments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @return the range of assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the assessments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of assessments
	 * @param end the upper bound of the range of assessments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of assessments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Assessment> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Assessment> list = (List<Assessment>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ASSESSMENT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ASSESSMENT.concat(AssessmentModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Assessment>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Assessment>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the assessments where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByuserId(long userId) throws SystemException {
		for (Assessment assessment : findByuserId(userId)) {
			remove(assessment);
		}
	}

	/**
	 * Removes all the assessments where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBycompany(long companyId) throws SystemException {
		for (Assessment assessment : findBycompany(companyId)) {
			remove(assessment);
		}
	}

	/**
	 * Removes all the assessments where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByorganization(long organizationId)
		throws SystemException {
		for (Assessment assessment : findByorganization(organizationId)) {
			remove(assessment);
		}
	}

	/**
	 * Removes all the assessments where batchId = &#63; from the database.
	 *
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBybatch(long batchId) throws SystemException {
		for (Assessment assessment : findBybatch(batchId)) {
			remove(assessment);
		}
	}

	/**
	 * Removes all the assessments where subjectId = &#63; and assessmentTypeId = &#63; from the database.
	 *
	 * @param subjectId the subject ID
	 * @param assessmentTypeId the assessment type ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBysubjectAssessmentType(long subjectId,
		long assessmentTypeId) throws SystemException {
		for (Assessment assessment : findBysubjectAssessmentType(subjectId,
				assessmentTypeId)) {
			remove(assessment);
		}
	}

	/**
	 * Removes all the assessments where batchId = &#63; and status = &#63; from the database.
	 *
	 * @param batchId the batch ID
	 * @param status the status
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBatchStatus(long batchId, long status)
		throws SystemException {
		for (Assessment assessment : findByBatchStatus(batchId, status)) {
			remove(assessment);
		}
	}

	/**
	 * Removes all the assessments where subjectId = &#63; and status = &#63; from the database.
	 *
	 * @param subjectId the subject ID
	 * @param status the status
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBySubjectStatus(long subjectId, long status)
		throws SystemException {
		for (Assessment assessment : findBySubjectStatus(subjectId, status)) {
			remove(assessment);
		}
	}

	/**
	 * Removes all the assessments from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Assessment assessment : findAll()) {
			remove(assessment);
		}
	}

	/**
	 * Returns the number of assessments where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public int countByuserId(long userId) throws SystemException {
		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessments where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public int countBycompany(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessments where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public int countByorganization(long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessments where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the number of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public int countBybatch(long batchId) throws SystemException {
		Object[] finderArgs = new Object[] { batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCH,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCH,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessments where subjectId = &#63; and assessmentTypeId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param assessmentTypeId the assessment type ID
	 * @return the number of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public int countBysubjectAssessmentType(long subjectId,
		long assessmentTypeId) throws SystemException {
		Object[] finderArgs = new Object[] { subjectId, assessmentTypeId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_SUBJECTASSESSMENTTYPE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_SUBJECTASSESSMENTTYPE_SUBJECTID_2);

			query.append(_FINDER_COLUMN_SUBJECTASSESSMENTTYPE_ASSESSMENTTYPEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				qPos.add(assessmentTypeId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_SUBJECTASSESSMENTTYPE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessments where batchId = &#63; and status = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param status the status
	 * @return the number of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBatchStatus(long batchId, long status)
		throws SystemException {
		Object[] finderArgs = new Object[] { batchId, status };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCHSTATUS,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_BATCHSTATUS_BATCHID_2);

			query.append(_FINDER_COLUMN_BATCHSTATUS_STATUS_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				qPos.add(status);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCHSTATUS,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessments where subjectId = &#63; and status = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param status the status
	 * @return the number of matching assessments
	 * @throws SystemException if a system exception occurred
	 */
	public int countBySubjectStatus(long subjectId, long status)
		throws SystemException {
		Object[] finderArgs = new Object[] { subjectId, status };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_SUBJECTSTATUS,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ASSESSMENT_WHERE);

			query.append(_FINDER_COLUMN_SUBJECTSTATUS_SUBJECTID_2);

			query.append(_FINDER_COLUMN_SUBJECTSTATUS_STATUS_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				qPos.add(status);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_SUBJECTSTATUS,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of assessments.
	 *
	 * @return the number of assessments
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ASSESSMENT);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the assessment persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.assessment.model.Assessment")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Assessment>> listenersList = new ArrayList<ModelListener<Assessment>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Assessment>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AssessmentImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_ASSESSMENT = "SELECT assessment FROM Assessment assessment";
	private static final String _SQL_SELECT_ASSESSMENT_WHERE = "SELECT assessment FROM Assessment assessment WHERE ";
	private static final String _SQL_COUNT_ASSESSMENT = "SELECT COUNT(assessment) FROM Assessment assessment";
	private static final String _SQL_COUNT_ASSESSMENT_WHERE = "SELECT COUNT(assessment) FROM Assessment assessment WHERE ";
	private static final String _FINDER_COLUMN_USERID_USERID_2 = "assessment.userId = ?";
	private static final String _FINDER_COLUMN_COMPANY_COMPANYID_2 = "assessment.companyId = ?";
	private static final String _FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2 = "assessment.organizationId = ?";
	private static final String _FINDER_COLUMN_BATCH_BATCHID_2 = "assessment.batchId = ?";
	private static final String _FINDER_COLUMN_SUBJECTASSESSMENTTYPE_SUBJECTID_2 =
		"assessment.subjectId = ? AND ";
	private static final String _FINDER_COLUMN_SUBJECTASSESSMENTTYPE_ASSESSMENTTYPEID_2 =
		"assessment.assessmentTypeId = ?";
	private static final String _FINDER_COLUMN_BATCHSTATUS_BATCHID_2 = "assessment.batchId = ? AND ";
	private static final String _FINDER_COLUMN_BATCHSTATUS_STATUS_2 = "assessment.status = ?";
	private static final String _FINDER_COLUMN_SUBJECTSTATUS_SUBJECTID_2 = "assessment.subjectId = ? AND ";
	private static final String _FINDER_COLUMN_SUBJECTSTATUS_STATUS_2 = "assessment.status = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "assessment.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Assessment exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Assessment exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AssessmentPersistenceImpl.class);
	private static Assessment _nullAssessment = new AssessmentImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Assessment> toCacheModel() {
				return _nullAssessmentCacheModel;
			}
		};

	private static CacheModel<Assessment> _nullAssessmentCacheModel = new CacheModel<Assessment>() {
			public Assessment toEntityModel() {
				return _nullAssessment;
			}
		};
}
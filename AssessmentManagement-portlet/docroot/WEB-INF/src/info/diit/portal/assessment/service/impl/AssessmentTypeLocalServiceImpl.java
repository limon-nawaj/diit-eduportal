/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import info.diit.portal.assessment.NoSuchAssessmentTypeException;
import info.diit.portal.assessment.model.AssessmentType;
import info.diit.portal.assessment.service.base.AssessmentTypeLocalServiceBaseImpl;
import info.diit.portal.assessment.service.persistence.AssessmentTypeUtil;

/**
 * The implementation of the assessment type local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.assessment.service.AssessmentTypeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see info.diit.portal.assessment.service.base.AssessmentTypeLocalServiceBaseImpl
 * @see info.diit.portal.assessment.service.AssessmentTypeLocalServiceUtil
 */
public class AssessmentTypeLocalServiceImpl
	extends AssessmentTypeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.assessment.service.AssessmentTypeLocalServiceUtil} to access the assessment type local service.
	 */
	
	public List<AssessmentType> findByOrganization(long organizationId) throws SystemException{
		return AssessmentTypeUtil.findByorganization(organizationId);
	}
	
	public List<AssessmentType> findByCompany(long companyId) throws SystemException{
		return AssessmentTypeUtil.findBycompany(companyId);
	}
	
	public AssessmentType findByPrimaryKey(long assessmentTypeId) throws NoSuchAssessmentTypeException, SystemException{
		return AssessmentTypeUtil.findByPrimaryKey(assessmentTypeId);
	}
	
	public List<AssessmentType> findByCompanyTypeName(long companyId, String type) throws SystemException{
		return AssessmentTypeUtil.findByCompanyTypeName(companyId, type);
	}
}
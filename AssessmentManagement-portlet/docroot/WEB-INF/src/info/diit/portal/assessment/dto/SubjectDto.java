package info.diit.portal.assessment.dto;

public class SubjectDto {

	private long subjectId;
	private String subjectName;
	public long getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(long subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	
	public String toString(){
		return getSubjectName();
	}
}

package info.diit.portal.assessment.dto;

public class StudentDto {

	private long studentId;
	private String studentName;
	private Integer obtainMark;
	private String status;
		
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public long getStudentId() {
		return studentId;
	}
	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public Integer getObtainMark() {
		return obtainMark;
	}
	public void setObtainMark(Integer obtainMark) {
		this.obtainMark = obtainMark;
	}
}

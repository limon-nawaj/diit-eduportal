package info.diit.portal.assessment.dto;

public class AnalyticsDto {

	private String subjectName;
	private String teacher;
	private String type;
	private int count;
	private Double passRate;
	private Double averageObtainMark;
	private Double attendanceRate;
	
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getTeacher() {
		return teacher;
	}
	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Double getPassRate() {
		return passRate;
	}
	public void setPassRate(Double passRate) {
		this.passRate = passRate;
	}
	public Double getAverageObtainMark() {
		return averageObtainMark;
	}
	public void setAverageObtainMark(Double averageObtainMark) {
		this.averageObtainMark = averageObtainMark;
	}
	public Double getAttendanceRate() {
		return attendanceRate;
	}
	public void setAttendanceRate(Double attendanceRate) {
		this.attendanceRate = attendanceRate;
	}
}

/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.assessment.model.AssessmentStudent;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing AssessmentStudent in entity cache.
 *
 * @author limon
 * @see AssessmentStudent
 * @generated
 */
public class AssessmentStudentCacheModel implements CacheModel<AssessmentStudent>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{assessmentStudentId=");
		sb.append(assessmentStudentId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", obtainMark=");
		sb.append(obtainMark);
		sb.append(", assessmentId=");
		sb.append(assessmentId);
		sb.append(", studentId=");
		sb.append(studentId);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	public AssessmentStudent toEntityModel() {
		AssessmentStudentImpl assessmentStudentImpl = new AssessmentStudentImpl();

		assessmentStudentImpl.setAssessmentStudentId(assessmentStudentId);
		assessmentStudentImpl.setCompanyId(companyId);
		assessmentStudentImpl.setOrganizationId(organizationId);
		assessmentStudentImpl.setUserId(userId);

		if (userName == null) {
			assessmentStudentImpl.setUserName(StringPool.BLANK);
		}
		else {
			assessmentStudentImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			assessmentStudentImpl.setCreateDate(null);
		}
		else {
			assessmentStudentImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			assessmentStudentImpl.setModifiedDate(null);
		}
		else {
			assessmentStudentImpl.setModifiedDate(new Date(modifiedDate));
		}

		assessmentStudentImpl.setObtainMark(obtainMark);
		assessmentStudentImpl.setAssessmentId(assessmentId);
		assessmentStudentImpl.setStudentId(studentId);
		assessmentStudentImpl.setStatus(status);

		assessmentStudentImpl.resetOriginalValues();

		return assessmentStudentImpl;
	}

	public long assessmentStudentId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public double obtainMark;
	public long assessmentId;
	public long studentId;
	public long status;
}
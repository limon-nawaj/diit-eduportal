/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.assessment.model.AssessmentType;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing AssessmentType in entity cache.
 *
 * @author limon
 * @see AssessmentType
 * @generated
 */
public class AssessmentTypeCacheModel implements CacheModel<AssessmentType>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{assessmentTypeId=");
		sb.append(assessmentTypeId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", type=");
		sb.append(type);
		sb.append("}");

		return sb.toString();
	}

	public AssessmentType toEntityModel() {
		AssessmentTypeImpl assessmentTypeImpl = new AssessmentTypeImpl();

		assessmentTypeImpl.setAssessmentTypeId(assessmentTypeId);
		assessmentTypeImpl.setCompanyId(companyId);
		assessmentTypeImpl.setOrganizationId(organizationId);
		assessmentTypeImpl.setUserId(userId);

		if (userName == null) {
			assessmentTypeImpl.setUserName(StringPool.BLANK);
		}
		else {
			assessmentTypeImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			assessmentTypeImpl.setCreateDate(null);
		}
		else {
			assessmentTypeImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			assessmentTypeImpl.setModifiedDate(null);
		}
		else {
			assessmentTypeImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (type == null) {
			assessmentTypeImpl.setType(StringPool.BLANK);
		}
		else {
			assessmentTypeImpl.setType(type);
		}

		assessmentTypeImpl.resetOriginalValues();

		return assessmentTypeImpl;
	}

	public long assessmentTypeId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String type;
}
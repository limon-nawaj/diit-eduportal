package info.diit.portal.assessment.dto;

public class CourseDto {

	private long courseId;
	private String courseName;
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String toString(){
		return getCourseName();
	}
}

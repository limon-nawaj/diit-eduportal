/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.service.impl;

import info.diit.portal.assessment.NoSuchAssessmentException;
import info.diit.portal.assessment.model.Assessment;
import info.diit.portal.assessment.service.base.AssessmentLocalServiceBaseImpl;
import info.diit.portal.assessment.service.persistence.AssessmentUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.persistence.OrganizationUtil;

/**
 * The implementation of the assessment local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.assessment.service.AssessmentLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see info.diit.portal.assessment.service.base.AssessmentLocalServiceBaseImpl
 * @see info.diit.portal.assessment.service.AssessmentLocalServiceUtil
 */
public class AssessmentLocalServiceImpl extends AssessmentLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.assessment.service.AssessmentLocalServiceUtil} to access the assessment local service.
	 */
	
	public List<Assessment> findByUser(long userId) throws SystemException{
		return AssessmentUtil.findByuserId(userId);
	}
	
	public Assessment findByPrimaryKey(long assessmentId) throws NoSuchAssessmentException, SystemException{
		return AssessmentUtil.findByPrimaryKey(assessmentId);
	}
	
	public List<Assessment> findByBatch(long batchId) throws SystemException{
		return AssessmentUtil.findBybatch(batchId);
	}
	
	public List<Assessment> findBySubjectAssessmentType(long subjectId, long assessmentTypeId) throws SystemException{
		return AssessmentUtil.findBysubjectAssessmentType(subjectId, assessmentTypeId);
	}
	
	public List<Assessment> findByBatchStatus(long batchId, long status) throws SystemException{
		return AssessmentUtil.findByBatchStatus(batchId, status);
	}
	
	public List<Assessment> findBySubjectStatus(long subjectId, long status) throws SystemException{
		return AssessmentUtil.findBySubjectStatus(subjectId, status);
	}
}
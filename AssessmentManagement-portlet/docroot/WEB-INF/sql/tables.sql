create table EduPortal_Assessment_Assessment (
	assessmentId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	assessmentDate DATE null,
	resultDate DATE null,
	totalMark DOUBLE,
	assessmentTypeId LONG,
	batchId LONG,
	subjectId LONG,
	status LONG
);

create table EduPortal_Assessment_AssessmentStudent (
	assessmentStudentId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	obtainMark DOUBLE,
	assessmentId LONG,
	studentId LONG,
	status LONG
);

create table EduPortal_Assessment_AssessmentType (
	assessmentTypeId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	type_ VARCHAR(75) null
);
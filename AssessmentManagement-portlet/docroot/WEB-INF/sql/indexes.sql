create index IX_54619146 on Assessment_EduPortal_Assessment (companyId);
create index IX_EEBCF944 on Assessment_EduPortal_Assessment (organizationId);
create index IX_6D100FDC on Assessment_EduPortal_Assessment (userId);

create index IX_D3949736 on Assessment_EduPortal_AssessmentStudent (assessmentId);
create index IX_D80C25F0 on Assessment_EduPortal_AssessmentStudent (assessmentId, studentId);
create index IX_E53DA943 on Assessment_EduPortal_AssessmentStudent (companyId);
create index IX_76915767 on Assessment_EduPortal_AssessmentStudent (organizationId);
create index IX_704A40FF on Assessment_EduPortal_AssessmentStudent (userId);

create index IX_AB925E2C on Assessment_EduPortal_AssessmentType (companyId);
create index IX_3AA64C1E on Assessment_EduPortal_AssessmentType (organizationId);

create index IX_8E166A1D on EduPortal_Assessment_Assessment (batchId);
create index IX_C20B3C03 on EduPortal_Assessment_Assessment (batchId, status);
create index IX_59AC4060 on EduPortal_Assessment_Assessment (companyId);
create index IX_7425AA6A on EduPortal_Assessment_Assessment (organizationId);
create index IX_2915BE5A on EduPortal_Assessment_Assessment (subjectId, assessmentTypeId);
create index IX_EB04A975 on EduPortal_Assessment_Assessment (subjectId, status);
create index IX_93774702 on EduPortal_Assessment_Assessment (userId);

create index IX_B9CAADD0 on EduPortal_Assessment_AssessmentStudent (assessmentId);
create index IX_F929CB16 on EduPortal_Assessment_AssessmentStudent (assessmentId, studentId);
create index IX_B33EA8E9 on EduPortal_Assessment_AssessmentStudent (companyId);
create index IX_A79C2F81 on EduPortal_Assessment_AssessmentStudent (organizationId);
create index IX_6413D319 on EduPortal_Assessment_AssessmentStudent (userId);

create index IX_9F5BF046 on EduPortal_Assessment_AssessmentType (companyId);
create index IX_8B1CBECF on EduPortal_Assessment_AssessmentType (companyId, type_);
create index IX_8C65FA44 on EduPortal_Assessment_AssessmentType (organizationId);
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.assessment.model.AssessmentStudent;

import java.util.List;

/**
 * The persistence utility for the assessment student service. This utility wraps {@link AssessmentStudentPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see AssessmentStudentPersistence
 * @see AssessmentStudentPersistenceImpl
 * @generated
 */
public class AssessmentStudentUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(AssessmentStudent assessmentStudent) {
		getPersistence().clearCache(assessmentStudent);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<AssessmentStudent> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<AssessmentStudent> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<AssessmentStudent> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static AssessmentStudent update(
		AssessmentStudent assessmentStudent, boolean merge)
		throws SystemException {
		return getPersistence().update(assessmentStudent, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static AssessmentStudent update(
		AssessmentStudent assessmentStudent, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(assessmentStudent, merge, serviceContext);
	}

	/**
	* Caches the assessment student in the entity cache if it is enabled.
	*
	* @param assessmentStudent the assessment student
	*/
	public static void cacheResult(
		info.diit.portal.assessment.model.AssessmentStudent assessmentStudent) {
		getPersistence().cacheResult(assessmentStudent);
	}

	/**
	* Caches the assessment students in the entity cache if it is enabled.
	*
	* @param assessmentStudents the assessment students
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.assessment.model.AssessmentStudent> assessmentStudents) {
		getPersistence().cacheResult(assessmentStudents);
	}

	/**
	* Creates a new assessment student with the primary key. Does not add the assessment student to the database.
	*
	* @param assessmentStudentId the primary key for the new assessment student
	* @return the new assessment student
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent create(
		long assessmentStudentId) {
		return getPersistence().create(assessmentStudentId);
	}

	/**
	* Removes the assessment student with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param assessmentStudentId the primary key of the assessment student
	* @return the assessment student that was removed
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent remove(
		long assessmentStudentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence().remove(assessmentStudentId);
	}

	public static info.diit.portal.assessment.model.AssessmentStudent updateImpl(
		info.diit.portal.assessment.model.AssessmentStudent assessmentStudent,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(assessmentStudent, merge);
	}

	/**
	* Returns the assessment student with the primary key or throws a {@link info.diit.portal.assessment.NoSuchAssessmentStudentException} if it could not be found.
	*
	* @param assessmentStudentId the primary key of the assessment student
	* @return the assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent findByPrimaryKey(
		long assessmentStudentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence().findByPrimaryKey(assessmentStudentId);
	}

	/**
	* Returns the assessment student with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param assessmentStudentId the primary key of the assessment student
	* @return the assessment student, or <code>null</code> if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent fetchByPrimaryKey(
		long assessmentStudentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(assessmentStudentId);
	}

	/**
	* Returns all the assessment students where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByuserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuserId(userId);
	}

	/**
	* Returns a range of all the assessment students where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @return the range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByuserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuserId(userId, start, end);
	}

	/**
	* Returns an ordered range of all the assessment students where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByuserId(userId, start, end, orderByComparator);
	}

	/**
	* Returns the first assessment student in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent findByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence().findByuserId_First(userId, orderByComparator);
	}

	/**
	* Returns the first assessment student in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent fetchByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByuserId_First(userId, orderByComparator);
	}

	/**
	* Returns the last assessment student in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent findByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence().findByuserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the last assessment student in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent fetchByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByuserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the assessment students before and after the current assessment student in the ordered set where userId = &#63;.
	*
	* @param assessmentStudentId the primary key of the current assessment student
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent[] findByuserId_PrevAndNext(
		long assessmentStudentId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence()
				   .findByuserId_PrevAndNext(assessmentStudentId, userId,
			orderByComparator);
	}

	/**
	* Returns all the assessment students where assessmentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @return the matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByassessmentId(
		long assessmentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByassessmentId(assessmentId);
	}

	/**
	* Returns a range of all the assessment students where assessmentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assessmentId the assessment ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @return the range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByassessmentId(
		long assessmentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByassessmentId(assessmentId, start, end);
	}

	/**
	* Returns an ordered range of all the assessment students where assessmentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assessmentId the assessment ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByassessmentId(
		long assessmentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByassessmentId(assessmentId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first assessment student in the ordered set where assessmentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent findByassessmentId_First(
		long assessmentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence()
				   .findByassessmentId_First(assessmentId, orderByComparator);
	}

	/**
	* Returns the first assessment student in the ordered set where assessmentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent fetchByassessmentId_First(
		long assessmentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByassessmentId_First(assessmentId, orderByComparator);
	}

	/**
	* Returns the last assessment student in the ordered set where assessmentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent findByassessmentId_Last(
		long assessmentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence()
				   .findByassessmentId_Last(assessmentId, orderByComparator);
	}

	/**
	* Returns the last assessment student in the ordered set where assessmentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent fetchByassessmentId_Last(
		long assessmentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByassessmentId_Last(assessmentId, orderByComparator);
	}

	/**
	* Returns the assessment students before and after the current assessment student in the ordered set where assessmentId = &#63;.
	*
	* @param assessmentStudentId the primary key of the current assessment student
	* @param assessmentId the assessment ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent[] findByassessmentId_PrevAndNext(
		long assessmentStudentId, long assessmentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence()
				   .findByassessmentId_PrevAndNext(assessmentStudentId,
			assessmentId, orderByComparator);
	}

	/**
	* Returns all the assessment students where assessmentId = &#63; and studentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @return the matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByassessmentStudent(
		long assessmentId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByassessmentStudent(assessmentId, studentId);
	}

	/**
	* Returns a range of all the assessment students where assessmentId = &#63; and studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @return the range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByassessmentStudent(
		long assessmentId, long studentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByassessmentStudent(assessmentId, studentId, start, end);
	}

	/**
	* Returns an ordered range of all the assessment students where assessmentId = &#63; and studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByassessmentStudent(
		long assessmentId, long studentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByassessmentStudent(assessmentId, studentId, start,
			end, orderByComparator);
	}

	/**
	* Returns the first assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent findByassessmentStudent_First(
		long assessmentId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence()
				   .findByassessmentStudent_First(assessmentId, studentId,
			orderByComparator);
	}

	/**
	* Returns the first assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent fetchByassessmentStudent_First(
		long assessmentId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByassessmentStudent_First(assessmentId, studentId,
			orderByComparator);
	}

	/**
	* Returns the last assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent findByassessmentStudent_Last(
		long assessmentId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence()
				   .findByassessmentStudent_Last(assessmentId, studentId,
			orderByComparator);
	}

	/**
	* Returns the last assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent fetchByassessmentStudent_Last(
		long assessmentId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByassessmentStudent_Last(assessmentId, studentId,
			orderByComparator);
	}

	/**
	* Returns the assessment students before and after the current assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	*
	* @param assessmentStudentId the primary key of the current assessment student
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent[] findByassessmentStudent_PrevAndNext(
		long assessmentStudentId, long assessmentId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence()
				   .findByassessmentStudent_PrevAndNext(assessmentStudentId,
			assessmentId, studentId, orderByComparator);
	}

	/**
	* Returns all the assessment students where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findBycompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycompany(companyId);
	}

	/**
	* Returns a range of all the assessment students where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @return the range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findBycompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the assessment students where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findBycompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first assessment student in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent findBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence().findBycompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first assessment student in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent fetchBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBycompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last assessment student in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent findBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence().findBycompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last assessment student in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent fetchBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBycompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the assessment students before and after the current assessment student in the ordered set where companyId = &#63;.
	*
	* @param assessmentStudentId the primary key of the current assessment student
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent[] findBycompany_PrevAndNext(
		long assessmentStudentId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence()
				   .findBycompany_PrevAndNext(assessmentStudentId, companyId,
			orderByComparator);
	}

	/**
	* Returns all the assessment students where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId);
	}

	/**
	* Returns a range of all the assessment students where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @return the range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the assessment students where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByorganization(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first assessment student in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence()
				   .findByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first assessment student in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the last assessment student in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence()
				   .findByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last assessment student in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the assessment students before and after the current assessment student in the ordered set where organizationId = &#63;.
	*
	* @param assessmentStudentId the primary key of the current assessment student
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentStudent[] findByorganization_PrevAndNext(
		long assessmentStudentId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException {
		return getPersistence()
				   .findByorganization_PrevAndNext(assessmentStudentId,
			organizationId, orderByComparator);
	}

	/**
	* Returns all the assessment students.
	*
	* @return the assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the assessment students.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @return the range of assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the assessment students.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the assessment students where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByuserId(userId);
	}

	/**
	* Removes all the assessment students where assessmentId = &#63; from the database.
	*
	* @param assessmentId the assessment ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByassessmentId(long assessmentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByassessmentId(assessmentId);
	}

	/**
	* Removes all the assessment students where assessmentId = &#63; and studentId = &#63; from the database.
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByassessmentStudent(long assessmentId,
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByassessmentStudent(assessmentId, studentId);
	}

	/**
	* Removes all the assessment students where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBycompany(companyId);
	}

	/**
	* Removes all the assessment students where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByorganization(organizationId);
	}

	/**
	* Removes all the assessment students from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of assessment students where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static int countByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByuserId(userId);
	}

	/**
	* Returns the number of assessment students where assessmentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @return the number of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static int countByassessmentId(long assessmentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByassessmentId(assessmentId);
	}

	/**
	* Returns the number of assessment students where assessmentId = &#63; and studentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @return the number of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static int countByassessmentStudent(long assessmentId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByassessmentStudent(assessmentId, studentId);
	}

	/**
	* Returns the number of assessment students where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static int countBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBycompany(companyId);
	}

	/**
	* Returns the number of assessment students where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByorganization(organizationId);
	}

	/**
	* Returns the number of assessment students.
	*
	* @return the number of assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static AssessmentStudentPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (AssessmentStudentPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.assessment.service.ClpSerializer.getServletContextName(),
					AssessmentStudentPersistence.class.getName());

			ReferenceRegistry.registerReference(AssessmentStudentUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(AssessmentStudentPersistence persistence) {
	}

	private static AssessmentStudentPersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Assessment}.
 * </p>
 *
 * @author    limon
 * @see       Assessment
 * @generated
 */
public class AssessmentWrapper implements Assessment, ModelWrapper<Assessment> {
	public AssessmentWrapper(Assessment assessment) {
		_assessment = assessment;
	}

	public Class<?> getModelClass() {
		return Assessment.class;
	}

	public String getModelClassName() {
		return Assessment.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("assessmentId", getAssessmentId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("assessmentDate", getAssessmentDate());
		attributes.put("resultDate", getResultDate());
		attributes.put("totalMark", getTotalMark());
		attributes.put("assessmentTypeId", getAssessmentTypeId());
		attributes.put("batchId", getBatchId());
		attributes.put("subjectId", getSubjectId());
		attributes.put("status", getStatus());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long assessmentId = (Long)attributes.get("assessmentId");

		if (assessmentId != null) {
			setAssessmentId(assessmentId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date assessmentDate = (Date)attributes.get("assessmentDate");

		if (assessmentDate != null) {
			setAssessmentDate(assessmentDate);
		}

		Date resultDate = (Date)attributes.get("resultDate");

		if (resultDate != null) {
			setResultDate(resultDate);
		}

		Double totalMark = (Double)attributes.get("totalMark");

		if (totalMark != null) {
			setTotalMark(totalMark);
		}

		Long assessmentTypeId = (Long)attributes.get("assessmentTypeId");

		if (assessmentTypeId != null) {
			setAssessmentTypeId(assessmentTypeId);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Long subjectId = (Long)attributes.get("subjectId");

		if (subjectId != null) {
			setSubjectId(subjectId);
		}

		Long status = (Long)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	/**
	* Returns the primary key of this assessment.
	*
	* @return the primary key of this assessment
	*/
	public long getPrimaryKey() {
		return _assessment.getPrimaryKey();
	}

	/**
	* Sets the primary key of this assessment.
	*
	* @param primaryKey the primary key of this assessment
	*/
	public void setPrimaryKey(long primaryKey) {
		_assessment.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the assessment ID of this assessment.
	*
	* @return the assessment ID of this assessment
	*/
	public long getAssessmentId() {
		return _assessment.getAssessmentId();
	}

	/**
	* Sets the assessment ID of this assessment.
	*
	* @param assessmentId the assessment ID of this assessment
	*/
	public void setAssessmentId(long assessmentId) {
		_assessment.setAssessmentId(assessmentId);
	}

	/**
	* Returns the company ID of this assessment.
	*
	* @return the company ID of this assessment
	*/
	public long getCompanyId() {
		return _assessment.getCompanyId();
	}

	/**
	* Sets the company ID of this assessment.
	*
	* @param companyId the company ID of this assessment
	*/
	public void setCompanyId(long companyId) {
		_assessment.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this assessment.
	*
	* @return the organization ID of this assessment
	*/
	public long getOrganizationId() {
		return _assessment.getOrganizationId();
	}

	/**
	* Sets the organization ID of this assessment.
	*
	* @param organizationId the organization ID of this assessment
	*/
	public void setOrganizationId(long organizationId) {
		_assessment.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this assessment.
	*
	* @return the user ID of this assessment
	*/
	public long getUserId() {
		return _assessment.getUserId();
	}

	/**
	* Sets the user ID of this assessment.
	*
	* @param userId the user ID of this assessment
	*/
	public void setUserId(long userId) {
		_assessment.setUserId(userId);
	}

	/**
	* Returns the user uuid of this assessment.
	*
	* @return the user uuid of this assessment
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessment.getUserUuid();
	}

	/**
	* Sets the user uuid of this assessment.
	*
	* @param userUuid the user uuid of this assessment
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_assessment.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this assessment.
	*
	* @return the user name of this assessment
	*/
	public java.lang.String getUserName() {
		return _assessment.getUserName();
	}

	/**
	* Sets the user name of this assessment.
	*
	* @param userName the user name of this assessment
	*/
	public void setUserName(java.lang.String userName) {
		_assessment.setUserName(userName);
	}

	/**
	* Returns the create date of this assessment.
	*
	* @return the create date of this assessment
	*/
	public java.util.Date getCreateDate() {
		return _assessment.getCreateDate();
	}

	/**
	* Sets the create date of this assessment.
	*
	* @param createDate the create date of this assessment
	*/
	public void setCreateDate(java.util.Date createDate) {
		_assessment.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this assessment.
	*
	* @return the modified date of this assessment
	*/
	public java.util.Date getModifiedDate() {
		return _assessment.getModifiedDate();
	}

	/**
	* Sets the modified date of this assessment.
	*
	* @param modifiedDate the modified date of this assessment
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_assessment.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the assessment date of this assessment.
	*
	* @return the assessment date of this assessment
	*/
	public java.util.Date getAssessmentDate() {
		return _assessment.getAssessmentDate();
	}

	/**
	* Sets the assessment date of this assessment.
	*
	* @param assessmentDate the assessment date of this assessment
	*/
	public void setAssessmentDate(java.util.Date assessmentDate) {
		_assessment.setAssessmentDate(assessmentDate);
	}

	/**
	* Returns the result date of this assessment.
	*
	* @return the result date of this assessment
	*/
	public java.util.Date getResultDate() {
		return _assessment.getResultDate();
	}

	/**
	* Sets the result date of this assessment.
	*
	* @param resultDate the result date of this assessment
	*/
	public void setResultDate(java.util.Date resultDate) {
		_assessment.setResultDate(resultDate);
	}

	/**
	* Returns the total mark of this assessment.
	*
	* @return the total mark of this assessment
	*/
	public double getTotalMark() {
		return _assessment.getTotalMark();
	}

	/**
	* Sets the total mark of this assessment.
	*
	* @param totalMark the total mark of this assessment
	*/
	public void setTotalMark(double totalMark) {
		_assessment.setTotalMark(totalMark);
	}

	/**
	* Returns the assessment type ID of this assessment.
	*
	* @return the assessment type ID of this assessment
	*/
	public long getAssessmentTypeId() {
		return _assessment.getAssessmentTypeId();
	}

	/**
	* Sets the assessment type ID of this assessment.
	*
	* @param assessmentTypeId the assessment type ID of this assessment
	*/
	public void setAssessmentTypeId(long assessmentTypeId) {
		_assessment.setAssessmentTypeId(assessmentTypeId);
	}

	/**
	* Returns the batch ID of this assessment.
	*
	* @return the batch ID of this assessment
	*/
	public long getBatchId() {
		return _assessment.getBatchId();
	}

	/**
	* Sets the batch ID of this assessment.
	*
	* @param batchId the batch ID of this assessment
	*/
	public void setBatchId(long batchId) {
		_assessment.setBatchId(batchId);
	}

	/**
	* Returns the subject ID of this assessment.
	*
	* @return the subject ID of this assessment
	*/
	public long getSubjectId() {
		return _assessment.getSubjectId();
	}

	/**
	* Sets the subject ID of this assessment.
	*
	* @param subjectId the subject ID of this assessment
	*/
	public void setSubjectId(long subjectId) {
		_assessment.setSubjectId(subjectId);
	}

	/**
	* Returns the status of this assessment.
	*
	* @return the status of this assessment
	*/
	public long getStatus() {
		return _assessment.getStatus();
	}

	/**
	* Sets the status of this assessment.
	*
	* @param status the status of this assessment
	*/
	public void setStatus(long status) {
		_assessment.setStatus(status);
	}

	public boolean isNew() {
		return _assessment.isNew();
	}

	public void setNew(boolean n) {
		_assessment.setNew(n);
	}

	public boolean isCachedModel() {
		return _assessment.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_assessment.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _assessment.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _assessment.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_assessment.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _assessment.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_assessment.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AssessmentWrapper((Assessment)_assessment.clone());
	}

	public int compareTo(
		info.diit.portal.assessment.model.Assessment assessment) {
		return _assessment.compareTo(assessment);
	}

	@Override
	public int hashCode() {
		return _assessment.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.assessment.model.Assessment> toCacheModel() {
		return _assessment.toCacheModel();
	}

	public info.diit.portal.assessment.model.Assessment toEscapedModel() {
		return new AssessmentWrapper(_assessment.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _assessment.toString();
	}

	public java.lang.String toXmlString() {
		return _assessment.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_assessment.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Assessment getWrappedAssessment() {
		return _assessment;
	}

	public Assessment getWrappedModel() {
		return _assessment;
	}

	public void resetOriginalValues() {
		_assessment.resetOriginalValues();
	}

	private Assessment _assessment;
}
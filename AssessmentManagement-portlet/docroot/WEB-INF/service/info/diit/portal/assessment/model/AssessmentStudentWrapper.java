/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AssessmentStudent}.
 * </p>
 *
 * @author    limon
 * @see       AssessmentStudent
 * @generated
 */
public class AssessmentStudentWrapper implements AssessmentStudent,
	ModelWrapper<AssessmentStudent> {
	public AssessmentStudentWrapper(AssessmentStudent assessmentStudent) {
		_assessmentStudent = assessmentStudent;
	}

	public Class<?> getModelClass() {
		return AssessmentStudent.class;
	}

	public String getModelClassName() {
		return AssessmentStudent.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("assessmentStudentId", getAssessmentStudentId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("obtainMark", getObtainMark());
		attributes.put("assessmentId", getAssessmentId());
		attributes.put("studentId", getStudentId());
		attributes.put("status", getStatus());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long assessmentStudentId = (Long)attributes.get("assessmentStudentId");

		if (assessmentStudentId != null) {
			setAssessmentStudentId(assessmentStudentId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Double obtainMark = (Double)attributes.get("obtainMark");

		if (obtainMark != null) {
			setObtainMark(obtainMark);
		}

		Long assessmentId = (Long)attributes.get("assessmentId");

		if (assessmentId != null) {
			setAssessmentId(assessmentId);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Long status = (Long)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	/**
	* Returns the primary key of this assessment student.
	*
	* @return the primary key of this assessment student
	*/
	public long getPrimaryKey() {
		return _assessmentStudent.getPrimaryKey();
	}

	/**
	* Sets the primary key of this assessment student.
	*
	* @param primaryKey the primary key of this assessment student
	*/
	public void setPrimaryKey(long primaryKey) {
		_assessmentStudent.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the assessment student ID of this assessment student.
	*
	* @return the assessment student ID of this assessment student
	*/
	public long getAssessmentStudentId() {
		return _assessmentStudent.getAssessmentStudentId();
	}

	/**
	* Sets the assessment student ID of this assessment student.
	*
	* @param assessmentStudentId the assessment student ID of this assessment student
	*/
	public void setAssessmentStudentId(long assessmentStudentId) {
		_assessmentStudent.setAssessmentStudentId(assessmentStudentId);
	}

	/**
	* Returns the company ID of this assessment student.
	*
	* @return the company ID of this assessment student
	*/
	public long getCompanyId() {
		return _assessmentStudent.getCompanyId();
	}

	/**
	* Sets the company ID of this assessment student.
	*
	* @param companyId the company ID of this assessment student
	*/
	public void setCompanyId(long companyId) {
		_assessmentStudent.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this assessment student.
	*
	* @return the organization ID of this assessment student
	*/
	public long getOrganizationId() {
		return _assessmentStudent.getOrganizationId();
	}

	/**
	* Sets the organization ID of this assessment student.
	*
	* @param organizationId the organization ID of this assessment student
	*/
	public void setOrganizationId(long organizationId) {
		_assessmentStudent.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this assessment student.
	*
	* @return the user ID of this assessment student
	*/
	public long getUserId() {
		return _assessmentStudent.getUserId();
	}

	/**
	* Sets the user ID of this assessment student.
	*
	* @param userId the user ID of this assessment student
	*/
	public void setUserId(long userId) {
		_assessmentStudent.setUserId(userId);
	}

	/**
	* Returns the user uuid of this assessment student.
	*
	* @return the user uuid of this assessment student
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudent.getUserUuid();
	}

	/**
	* Sets the user uuid of this assessment student.
	*
	* @param userUuid the user uuid of this assessment student
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_assessmentStudent.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this assessment student.
	*
	* @return the user name of this assessment student
	*/
	public java.lang.String getUserName() {
		return _assessmentStudent.getUserName();
	}

	/**
	* Sets the user name of this assessment student.
	*
	* @param userName the user name of this assessment student
	*/
	public void setUserName(java.lang.String userName) {
		_assessmentStudent.setUserName(userName);
	}

	/**
	* Returns the create date of this assessment student.
	*
	* @return the create date of this assessment student
	*/
	public java.util.Date getCreateDate() {
		return _assessmentStudent.getCreateDate();
	}

	/**
	* Sets the create date of this assessment student.
	*
	* @param createDate the create date of this assessment student
	*/
	public void setCreateDate(java.util.Date createDate) {
		_assessmentStudent.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this assessment student.
	*
	* @return the modified date of this assessment student
	*/
	public java.util.Date getModifiedDate() {
		return _assessmentStudent.getModifiedDate();
	}

	/**
	* Sets the modified date of this assessment student.
	*
	* @param modifiedDate the modified date of this assessment student
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_assessmentStudent.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the obtain mark of this assessment student.
	*
	* @return the obtain mark of this assessment student
	*/
	public double getObtainMark() {
		return _assessmentStudent.getObtainMark();
	}

	/**
	* Sets the obtain mark of this assessment student.
	*
	* @param obtainMark the obtain mark of this assessment student
	*/
	public void setObtainMark(double obtainMark) {
		_assessmentStudent.setObtainMark(obtainMark);
	}

	/**
	* Returns the assessment ID of this assessment student.
	*
	* @return the assessment ID of this assessment student
	*/
	public long getAssessmentId() {
		return _assessmentStudent.getAssessmentId();
	}

	/**
	* Sets the assessment ID of this assessment student.
	*
	* @param assessmentId the assessment ID of this assessment student
	*/
	public void setAssessmentId(long assessmentId) {
		_assessmentStudent.setAssessmentId(assessmentId);
	}

	/**
	* Returns the student ID of this assessment student.
	*
	* @return the student ID of this assessment student
	*/
	public long getStudentId() {
		return _assessmentStudent.getStudentId();
	}

	/**
	* Sets the student ID of this assessment student.
	*
	* @param studentId the student ID of this assessment student
	*/
	public void setStudentId(long studentId) {
		_assessmentStudent.setStudentId(studentId);
	}

	/**
	* Returns the status of this assessment student.
	*
	* @return the status of this assessment student
	*/
	public long getStatus() {
		return _assessmentStudent.getStatus();
	}

	/**
	* Sets the status of this assessment student.
	*
	* @param status the status of this assessment student
	*/
	public void setStatus(long status) {
		_assessmentStudent.setStatus(status);
	}

	public boolean isNew() {
		return _assessmentStudent.isNew();
	}

	public void setNew(boolean n) {
		_assessmentStudent.setNew(n);
	}

	public boolean isCachedModel() {
		return _assessmentStudent.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_assessmentStudent.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _assessmentStudent.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _assessmentStudent.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_assessmentStudent.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _assessmentStudent.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_assessmentStudent.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AssessmentStudentWrapper((AssessmentStudent)_assessmentStudent.clone());
	}

	public int compareTo(
		info.diit.portal.assessment.model.AssessmentStudent assessmentStudent) {
		return _assessmentStudent.compareTo(assessmentStudent);
	}

	@Override
	public int hashCode() {
		return _assessmentStudent.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.assessment.model.AssessmentStudent> toCacheModel() {
		return _assessmentStudent.toCacheModel();
	}

	public info.diit.portal.assessment.model.AssessmentStudent toEscapedModel() {
		return new AssessmentStudentWrapper(_assessmentStudent.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _assessmentStudent.toString();
	}

	public java.lang.String toXmlString() {
		return _assessmentStudent.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_assessmentStudent.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public AssessmentStudent getWrappedAssessmentStudent() {
		return _assessmentStudent;
	}

	public AssessmentStudent getWrappedModel() {
		return _assessmentStudent;
	}

	public void resetOriginalValues() {
		_assessmentStudent.resetOriginalValues();
	}

	private AssessmentStudent _assessmentStudent;
}
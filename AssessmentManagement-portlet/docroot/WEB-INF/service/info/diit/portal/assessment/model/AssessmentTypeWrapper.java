/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AssessmentType}.
 * </p>
 *
 * @author    limon
 * @see       AssessmentType
 * @generated
 */
public class AssessmentTypeWrapper implements AssessmentType,
	ModelWrapper<AssessmentType> {
	public AssessmentTypeWrapper(AssessmentType assessmentType) {
		_assessmentType = assessmentType;
	}

	public Class<?> getModelClass() {
		return AssessmentType.class;
	}

	public String getModelClassName() {
		return AssessmentType.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("assessmentTypeId", getAssessmentTypeId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("type", getType());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long assessmentTypeId = (Long)attributes.get("assessmentTypeId");

		if (assessmentTypeId != null) {
			setAssessmentTypeId(assessmentTypeId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}
	}

	/**
	* Returns the primary key of this assessment type.
	*
	* @return the primary key of this assessment type
	*/
	public long getPrimaryKey() {
		return _assessmentType.getPrimaryKey();
	}

	/**
	* Sets the primary key of this assessment type.
	*
	* @param primaryKey the primary key of this assessment type
	*/
	public void setPrimaryKey(long primaryKey) {
		_assessmentType.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the assessment type ID of this assessment type.
	*
	* @return the assessment type ID of this assessment type
	*/
	public long getAssessmentTypeId() {
		return _assessmentType.getAssessmentTypeId();
	}

	/**
	* Sets the assessment type ID of this assessment type.
	*
	* @param assessmentTypeId the assessment type ID of this assessment type
	*/
	public void setAssessmentTypeId(long assessmentTypeId) {
		_assessmentType.setAssessmentTypeId(assessmentTypeId);
	}

	/**
	* Returns the company ID of this assessment type.
	*
	* @return the company ID of this assessment type
	*/
	public long getCompanyId() {
		return _assessmentType.getCompanyId();
	}

	/**
	* Sets the company ID of this assessment type.
	*
	* @param companyId the company ID of this assessment type
	*/
	public void setCompanyId(long companyId) {
		_assessmentType.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this assessment type.
	*
	* @return the organization ID of this assessment type
	*/
	public long getOrganizationId() {
		return _assessmentType.getOrganizationId();
	}

	/**
	* Sets the organization ID of this assessment type.
	*
	* @param organizationId the organization ID of this assessment type
	*/
	public void setOrganizationId(long organizationId) {
		_assessmentType.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this assessment type.
	*
	* @return the user ID of this assessment type
	*/
	public long getUserId() {
		return _assessmentType.getUserId();
	}

	/**
	* Sets the user ID of this assessment type.
	*
	* @param userId the user ID of this assessment type
	*/
	public void setUserId(long userId) {
		_assessmentType.setUserId(userId);
	}

	/**
	* Returns the user uuid of this assessment type.
	*
	* @return the user uuid of this assessment type
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentType.getUserUuid();
	}

	/**
	* Sets the user uuid of this assessment type.
	*
	* @param userUuid the user uuid of this assessment type
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_assessmentType.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this assessment type.
	*
	* @return the user name of this assessment type
	*/
	public java.lang.String getUserName() {
		return _assessmentType.getUserName();
	}

	/**
	* Sets the user name of this assessment type.
	*
	* @param userName the user name of this assessment type
	*/
	public void setUserName(java.lang.String userName) {
		_assessmentType.setUserName(userName);
	}

	/**
	* Returns the create date of this assessment type.
	*
	* @return the create date of this assessment type
	*/
	public java.util.Date getCreateDate() {
		return _assessmentType.getCreateDate();
	}

	/**
	* Sets the create date of this assessment type.
	*
	* @param createDate the create date of this assessment type
	*/
	public void setCreateDate(java.util.Date createDate) {
		_assessmentType.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this assessment type.
	*
	* @return the modified date of this assessment type
	*/
	public java.util.Date getModifiedDate() {
		return _assessmentType.getModifiedDate();
	}

	/**
	* Sets the modified date of this assessment type.
	*
	* @param modifiedDate the modified date of this assessment type
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_assessmentType.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the type of this assessment type.
	*
	* @return the type of this assessment type
	*/
	public java.lang.String getType() {
		return _assessmentType.getType();
	}

	/**
	* Sets the type of this assessment type.
	*
	* @param type the type of this assessment type
	*/
	public void setType(java.lang.String type) {
		_assessmentType.setType(type);
	}

	public boolean isNew() {
		return _assessmentType.isNew();
	}

	public void setNew(boolean n) {
		_assessmentType.setNew(n);
	}

	public boolean isCachedModel() {
		return _assessmentType.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_assessmentType.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _assessmentType.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _assessmentType.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_assessmentType.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _assessmentType.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_assessmentType.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AssessmentTypeWrapper((AssessmentType)_assessmentType.clone());
	}

	public int compareTo(
		info.diit.portal.assessment.model.AssessmentType assessmentType) {
		return _assessmentType.compareTo(assessmentType);
	}

	@Override
	public int hashCode() {
		return _assessmentType.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.assessment.model.AssessmentType> toCacheModel() {
		return _assessmentType.toCacheModel();
	}

	public info.diit.portal.assessment.model.AssessmentType toEscapedModel() {
		return new AssessmentTypeWrapper(_assessmentType.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _assessmentType.toString();
	}

	public java.lang.String toXmlString() {
		return _assessmentType.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_assessmentType.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public AssessmentType getWrappedAssessmentType() {
		return _assessmentType;
	}

	public AssessmentType getWrappedModel() {
		return _assessmentType;
	}

	public void resetOriginalValues() {
		_assessmentType.resetOriginalValues();
	}

	private AssessmentType _assessmentType;
}
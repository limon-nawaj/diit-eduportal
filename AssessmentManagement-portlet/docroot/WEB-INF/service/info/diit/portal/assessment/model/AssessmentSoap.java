/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    limon
 * @generated
 */
public class AssessmentSoap implements Serializable {
	public static AssessmentSoap toSoapModel(Assessment model) {
		AssessmentSoap soapModel = new AssessmentSoap();

		soapModel.setAssessmentId(model.getAssessmentId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setAssessmentDate(model.getAssessmentDate());
		soapModel.setResultDate(model.getResultDate());
		soapModel.setTotalMark(model.getTotalMark());
		soapModel.setAssessmentTypeId(model.getAssessmentTypeId());
		soapModel.setBatchId(model.getBatchId());
		soapModel.setSubjectId(model.getSubjectId());
		soapModel.setStatus(model.getStatus());

		return soapModel;
	}

	public static AssessmentSoap[] toSoapModels(Assessment[] models) {
		AssessmentSoap[] soapModels = new AssessmentSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AssessmentSoap[][] toSoapModels(Assessment[][] models) {
		AssessmentSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AssessmentSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AssessmentSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AssessmentSoap[] toSoapModels(List<Assessment> models) {
		List<AssessmentSoap> soapModels = new ArrayList<AssessmentSoap>(models.size());

		for (Assessment model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AssessmentSoap[soapModels.size()]);
	}

	public AssessmentSoap() {
	}

	public long getPrimaryKey() {
		return _assessmentId;
	}

	public void setPrimaryKey(long pk) {
		setAssessmentId(pk);
	}

	public long getAssessmentId() {
		return _assessmentId;
	}

	public void setAssessmentId(long assessmentId) {
		_assessmentId = assessmentId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getAssessmentDate() {
		return _assessmentDate;
	}

	public void setAssessmentDate(Date assessmentDate) {
		_assessmentDate = assessmentDate;
	}

	public Date getResultDate() {
		return _resultDate;
	}

	public void setResultDate(Date resultDate) {
		_resultDate = resultDate;
	}

	public double getTotalMark() {
		return _totalMark;
	}

	public void setTotalMark(double totalMark) {
		_totalMark = totalMark;
	}

	public long getAssessmentTypeId() {
		return _assessmentTypeId;
	}

	public void setAssessmentTypeId(long assessmentTypeId) {
		_assessmentTypeId = assessmentTypeId;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public long getSubjectId() {
		return _subjectId;
	}

	public void setSubjectId(long subjectId) {
		_subjectId = subjectId;
	}

	public long getStatus() {
		return _status;
	}

	public void setStatus(long status) {
		_status = status;
	}

	private long _assessmentId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _assessmentDate;
	private Date _resultDate;
	private double _totalMark;
	private long _assessmentTypeId;
	private long _batchId;
	private long _subjectId;
	private long _status;
}
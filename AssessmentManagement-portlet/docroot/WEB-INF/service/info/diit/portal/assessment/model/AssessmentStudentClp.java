/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.assessment.service.AssessmentStudentLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author limon
 */
public class AssessmentStudentClp extends BaseModelImpl<AssessmentStudent>
	implements AssessmentStudent {
	public AssessmentStudentClp() {
	}

	public Class<?> getModelClass() {
		return AssessmentStudent.class;
	}

	public String getModelClassName() {
		return AssessmentStudent.class.getName();
	}

	public long getPrimaryKey() {
		return _assessmentStudentId;
	}

	public void setPrimaryKey(long primaryKey) {
		setAssessmentStudentId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_assessmentStudentId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("assessmentStudentId", getAssessmentStudentId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("obtainMark", getObtainMark());
		attributes.put("assessmentId", getAssessmentId());
		attributes.put("studentId", getStudentId());
		attributes.put("status", getStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long assessmentStudentId = (Long)attributes.get("assessmentStudentId");

		if (assessmentStudentId != null) {
			setAssessmentStudentId(assessmentStudentId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Double obtainMark = (Double)attributes.get("obtainMark");

		if (obtainMark != null) {
			setObtainMark(obtainMark);
		}

		Long assessmentId = (Long)attributes.get("assessmentId");

		if (assessmentId != null) {
			setAssessmentId(assessmentId);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Long status = (Long)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	public long getAssessmentStudentId() {
		return _assessmentStudentId;
	}

	public void setAssessmentStudentId(long assessmentStudentId) {
		_assessmentStudentId = assessmentStudentId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public double getObtainMark() {
		return _obtainMark;
	}

	public void setObtainMark(double obtainMark) {
		_obtainMark = obtainMark;
	}

	public long getAssessmentId() {
		return _assessmentId;
	}

	public void setAssessmentId(long assessmentId) {
		_assessmentId = assessmentId;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	public long getStatus() {
		return _status;
	}

	public void setStatus(long status) {
		_status = status;
	}

	public BaseModel<?> getAssessmentStudentRemoteModel() {
		return _assessmentStudentRemoteModel;
	}

	public void setAssessmentStudentRemoteModel(
		BaseModel<?> assessmentStudentRemoteModel) {
		_assessmentStudentRemoteModel = assessmentStudentRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			AssessmentStudentLocalServiceUtil.addAssessmentStudent(this);
		}
		else {
			AssessmentStudentLocalServiceUtil.updateAssessmentStudent(this);
		}
	}

	@Override
	public AssessmentStudent toEscapedModel() {
		return (AssessmentStudent)Proxy.newProxyInstance(AssessmentStudent.class.getClassLoader(),
			new Class[] { AssessmentStudent.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		AssessmentStudentClp clone = new AssessmentStudentClp();

		clone.setAssessmentStudentId(getAssessmentStudentId());
		clone.setCompanyId(getCompanyId());
		clone.setOrganizationId(getOrganizationId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setObtainMark(getObtainMark());
		clone.setAssessmentId(getAssessmentId());
		clone.setStudentId(getStudentId());
		clone.setStatus(getStatus());

		return clone;
	}

	public int compareTo(AssessmentStudent assessmentStudent) {
		int value = 0;

		if (getStudentId() < assessmentStudent.getStudentId()) {
			value = -1;
		}
		else if (getStudentId() > assessmentStudent.getStudentId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		AssessmentStudentClp assessmentStudent = null;

		try {
			assessmentStudent = (AssessmentStudentClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = assessmentStudent.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{assessmentStudentId=");
		sb.append(getAssessmentStudentId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", obtainMark=");
		sb.append(getObtainMark());
		sb.append(", assessmentId=");
		sb.append(getAssessmentId());
		sb.append(", studentId=");
		sb.append(getStudentId());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(37);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.assessment.model.AssessmentStudent");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>assessmentStudentId</column-name><column-value><![CDATA[");
		sb.append(getAssessmentStudentId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>obtainMark</column-name><column-value><![CDATA[");
		sb.append(getObtainMark());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>assessmentId</column-name><column-value><![CDATA[");
		sb.append(getAssessmentId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>studentId</column-name><column-value><![CDATA[");
		sb.append(getStudentId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _assessmentStudentId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private double _obtainMark;
	private long _assessmentId;
	private long _studentId;
	private long _status;
	private BaseModel<?> _assessmentStudentRemoteModel;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.assessment.service.AssessmentLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author limon
 */
public class AssessmentClp extends BaseModelImpl<Assessment>
	implements Assessment {
	public AssessmentClp() {
	}

	public Class<?> getModelClass() {
		return Assessment.class;
	}

	public String getModelClassName() {
		return Assessment.class.getName();
	}

	public long getPrimaryKey() {
		return _assessmentId;
	}

	public void setPrimaryKey(long primaryKey) {
		setAssessmentId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_assessmentId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("assessmentId", getAssessmentId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("assessmentDate", getAssessmentDate());
		attributes.put("resultDate", getResultDate());
		attributes.put("totalMark", getTotalMark());
		attributes.put("assessmentTypeId", getAssessmentTypeId());
		attributes.put("batchId", getBatchId());
		attributes.put("subjectId", getSubjectId());
		attributes.put("status", getStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long assessmentId = (Long)attributes.get("assessmentId");

		if (assessmentId != null) {
			setAssessmentId(assessmentId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date assessmentDate = (Date)attributes.get("assessmentDate");

		if (assessmentDate != null) {
			setAssessmentDate(assessmentDate);
		}

		Date resultDate = (Date)attributes.get("resultDate");

		if (resultDate != null) {
			setResultDate(resultDate);
		}

		Double totalMark = (Double)attributes.get("totalMark");

		if (totalMark != null) {
			setTotalMark(totalMark);
		}

		Long assessmentTypeId = (Long)attributes.get("assessmentTypeId");

		if (assessmentTypeId != null) {
			setAssessmentTypeId(assessmentTypeId);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Long subjectId = (Long)attributes.get("subjectId");

		if (subjectId != null) {
			setSubjectId(subjectId);
		}

		Long status = (Long)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	public long getAssessmentId() {
		return _assessmentId;
	}

	public void setAssessmentId(long assessmentId) {
		_assessmentId = assessmentId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getAssessmentDate() {
		return _assessmentDate;
	}

	public void setAssessmentDate(Date assessmentDate) {
		_assessmentDate = assessmentDate;
	}

	public Date getResultDate() {
		return _resultDate;
	}

	public void setResultDate(Date resultDate) {
		_resultDate = resultDate;
	}

	public double getTotalMark() {
		return _totalMark;
	}

	public void setTotalMark(double totalMark) {
		_totalMark = totalMark;
	}

	public long getAssessmentTypeId() {
		return _assessmentTypeId;
	}

	public void setAssessmentTypeId(long assessmentTypeId) {
		_assessmentTypeId = assessmentTypeId;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public long getSubjectId() {
		return _subjectId;
	}

	public void setSubjectId(long subjectId) {
		_subjectId = subjectId;
	}

	public long getStatus() {
		return _status;
	}

	public void setStatus(long status) {
		_status = status;
	}

	public BaseModel<?> getAssessmentRemoteModel() {
		return _assessmentRemoteModel;
	}

	public void setAssessmentRemoteModel(BaseModel<?> assessmentRemoteModel) {
		_assessmentRemoteModel = assessmentRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			AssessmentLocalServiceUtil.addAssessment(this);
		}
		else {
			AssessmentLocalServiceUtil.updateAssessment(this);
		}
	}

	@Override
	public Assessment toEscapedModel() {
		return (Assessment)Proxy.newProxyInstance(Assessment.class.getClassLoader(),
			new Class[] { Assessment.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		AssessmentClp clone = new AssessmentClp();

		clone.setAssessmentId(getAssessmentId());
		clone.setCompanyId(getCompanyId());
		clone.setOrganizationId(getOrganizationId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setAssessmentDate(getAssessmentDate());
		clone.setResultDate(getResultDate());
		clone.setTotalMark(getTotalMark());
		clone.setAssessmentTypeId(getAssessmentTypeId());
		clone.setBatchId(getBatchId());
		clone.setSubjectId(getSubjectId());
		clone.setStatus(getStatus());

		return clone;
	}

	public int compareTo(Assessment assessment) {
		int value = 0;

		if (getAssessmentId() < assessment.getAssessmentId()) {
			value = -1;
		}
		else if (getAssessmentId() > assessment.getAssessmentId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		value = DateUtil.compareTo(getAssessmentDate(),
				assessment.getAssessmentDate());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		AssessmentClp assessment = null;

		try {
			assessment = (AssessmentClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = assessment.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{assessmentId=");
		sb.append(getAssessmentId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", assessmentDate=");
		sb.append(getAssessmentDate());
		sb.append(", resultDate=");
		sb.append(getResultDate());
		sb.append(", totalMark=");
		sb.append(getTotalMark());
		sb.append(", assessmentTypeId=");
		sb.append(getAssessmentTypeId());
		sb.append(", batchId=");
		sb.append(getBatchId());
		sb.append(", subjectId=");
		sb.append(getSubjectId());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(46);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.assessment.model.Assessment");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>assessmentId</column-name><column-value><![CDATA[");
		sb.append(getAssessmentId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>assessmentDate</column-name><column-value><![CDATA[");
		sb.append(getAssessmentDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>resultDate</column-name><column-value><![CDATA[");
		sb.append(getResultDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>totalMark</column-name><column-value><![CDATA[");
		sb.append(getTotalMark());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>assessmentTypeId</column-name><column-value><![CDATA[");
		sb.append(getAssessmentTypeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>batchId</column-name><column-value><![CDATA[");
		sb.append(getBatchId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>subjectId</column-name><column-value><![CDATA[");
		sb.append(getSubjectId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _assessmentId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _assessmentDate;
	private Date _resultDate;
	private double _totalMark;
	private long _assessmentTypeId;
	private long _batchId;
	private long _subjectId;
	private long _status;
	private BaseModel<?> _assessmentRemoteModel;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link AssessmentStudentLocalService}.
 * </p>
 *
 * @author    limon
 * @see       AssessmentStudentLocalService
 * @generated
 */
public class AssessmentStudentLocalServiceWrapper
	implements AssessmentStudentLocalService,
		ServiceWrapper<AssessmentStudentLocalService> {
	public AssessmentStudentLocalServiceWrapper(
		AssessmentStudentLocalService assessmentStudentLocalService) {
		_assessmentStudentLocalService = assessmentStudentLocalService;
	}

	/**
	* Adds the assessment student to the database. Also notifies the appropriate model listeners.
	*
	* @param assessmentStudent the assessment student
	* @return the assessment student that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent addAssessmentStudent(
		info.diit.portal.assessment.model.AssessmentStudent assessmentStudent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.addAssessmentStudent(assessmentStudent);
	}

	/**
	* Creates a new assessment student with the primary key. Does not add the assessment student to the database.
	*
	* @param assessmentStudentId the primary key for the new assessment student
	* @return the new assessment student
	*/
	public info.diit.portal.assessment.model.AssessmentStudent createAssessmentStudent(
		long assessmentStudentId) {
		return _assessmentStudentLocalService.createAssessmentStudent(assessmentStudentId);
	}

	/**
	* Deletes the assessment student with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param assessmentStudentId the primary key of the assessment student
	* @return the assessment student that was removed
	* @throws PortalException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent deleteAssessmentStudent(
		long assessmentStudentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.deleteAssessmentStudent(assessmentStudentId);
	}

	/**
	* Deletes the assessment student from the database. Also notifies the appropriate model listeners.
	*
	* @param assessmentStudent the assessment student
	* @return the assessment student that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent deleteAssessmentStudent(
		info.diit.portal.assessment.model.AssessmentStudent assessmentStudent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.deleteAssessmentStudent(assessmentStudent);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _assessmentStudentLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.assessment.model.AssessmentStudent fetchAssessmentStudent(
		long assessmentStudentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.fetchAssessmentStudent(assessmentStudentId);
	}

	/**
	* Returns the assessment student with the primary key.
	*
	* @param assessmentStudentId the primary key of the assessment student
	* @return the assessment student
	* @throws PortalException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent getAssessmentStudent(
		long assessmentStudentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.getAssessmentStudent(assessmentStudentId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the assessment students.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @return the range of assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> getAssessmentStudents(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.getAssessmentStudents(start, end);
	}

	/**
	* Returns the number of assessment students.
	*
	* @return the number of assessment students
	* @throws SystemException if a system exception occurred
	*/
	public int getAssessmentStudentsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.getAssessmentStudentsCount();
	}

	/**
	* Updates the assessment student in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param assessmentStudent the assessment student
	* @return the assessment student that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent updateAssessmentStudent(
		info.diit.portal.assessment.model.AssessmentStudent assessmentStudent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.updateAssessmentStudent(assessmentStudent);
	}

	/**
	* Updates the assessment student in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param assessmentStudent the assessment student
	* @param merge whether to merge the assessment student with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the assessment student that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent updateAssessmentStudent(
		info.diit.portal.assessment.model.AssessmentStudent assessmentStudent,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.updateAssessmentStudent(assessmentStudent,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _assessmentStudentLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_assessmentStudentLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _assessmentStudentLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByAssessmentId(
		long assessmentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.findByAssessmentId(assessmentId);
	}

	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByUser(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.findByUser(userId);
	}

	public info.diit.portal.assessment.model.AssessmentStudent findByAssessmentStudent(
		long assessmentId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _assessmentStudentLocalService.findByAssessmentStudent(assessmentId,
			studentId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public AssessmentStudentLocalService getWrappedAssessmentStudentLocalService() {
		return _assessmentStudentLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedAssessmentStudentLocalService(
		AssessmentStudentLocalService assessmentStudentLocalService) {
		_assessmentStudentLocalService = assessmentStudentLocalService;
	}

	public AssessmentStudentLocalService getWrappedService() {
		return _assessmentStudentLocalService;
	}

	public void setWrappedService(
		AssessmentStudentLocalService assessmentStudentLocalService) {
		_assessmentStudentLocalService = assessmentStudentLocalService;
	}

	private AssessmentStudentLocalService _assessmentStudentLocalService;
}
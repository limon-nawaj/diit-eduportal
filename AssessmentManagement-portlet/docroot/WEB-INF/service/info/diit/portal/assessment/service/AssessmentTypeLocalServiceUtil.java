/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the assessment type local service. This utility wraps {@link info.diit.portal.assessment.service.impl.AssessmentTypeLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see AssessmentTypeLocalService
 * @see info.diit.portal.assessment.service.base.AssessmentTypeLocalServiceBaseImpl
 * @see info.diit.portal.assessment.service.impl.AssessmentTypeLocalServiceImpl
 * @generated
 */
public class AssessmentTypeLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.assessment.service.impl.AssessmentTypeLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the assessment type to the database. Also notifies the appropriate model listeners.
	*
	* @param assessmentType the assessment type
	* @return the assessment type that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentType addAssessmentType(
		info.diit.portal.assessment.model.AssessmentType assessmentType)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addAssessmentType(assessmentType);
	}

	/**
	* Creates a new assessment type with the primary key. Does not add the assessment type to the database.
	*
	* @param assessmentTypeId the primary key for the new assessment type
	* @return the new assessment type
	*/
	public static info.diit.portal.assessment.model.AssessmentType createAssessmentType(
		long assessmentTypeId) {
		return getService().createAssessmentType(assessmentTypeId);
	}

	/**
	* Deletes the assessment type with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param assessmentTypeId the primary key of the assessment type
	* @return the assessment type that was removed
	* @throws PortalException if a assessment type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentType deleteAssessmentType(
		long assessmentTypeId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteAssessmentType(assessmentTypeId);
	}

	/**
	* Deletes the assessment type from the database. Also notifies the appropriate model listeners.
	*
	* @param assessmentType the assessment type
	* @return the assessment type that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentType deleteAssessmentType(
		info.diit.portal.assessment.model.AssessmentType assessmentType)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteAssessmentType(assessmentType);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.assessment.model.AssessmentType fetchAssessmentType(
		long assessmentTypeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchAssessmentType(assessmentTypeId);
	}

	/**
	* Returns the assessment type with the primary key.
	*
	* @param assessmentTypeId the primary key of the assessment type
	* @return the assessment type
	* @throws PortalException if a assessment type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentType getAssessmentType(
		long assessmentTypeId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getAssessmentType(assessmentTypeId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the assessment types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of assessment types
	* @param end the upper bound of the range of assessment types (not inclusive)
	* @return the range of assessment types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.AssessmentType> getAssessmentTypes(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getAssessmentTypes(start, end);
	}

	/**
	* Returns the number of assessment types.
	*
	* @return the number of assessment types
	* @throws SystemException if a system exception occurred
	*/
	public static int getAssessmentTypesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getAssessmentTypesCount();
	}

	/**
	* Updates the assessment type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param assessmentType the assessment type
	* @return the assessment type that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentType updateAssessmentType(
		info.diit.portal.assessment.model.AssessmentType assessmentType)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateAssessmentType(assessmentType);
	}

	/**
	* Updates the assessment type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param assessmentType the assessment type
	* @param merge whether to merge the assessment type with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the assessment type that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.AssessmentType updateAssessmentType(
		info.diit.portal.assessment.model.AssessmentType assessmentType,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateAssessmentType(assessmentType, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<info.diit.portal.assessment.model.AssessmentType> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByOrganization(organizationId);
	}

	public static java.util.List<info.diit.portal.assessment.model.AssessmentType> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByCompany(companyId);
	}

	public static info.diit.portal.assessment.model.AssessmentType findByPrimaryKey(
		long assessmentTypeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentTypeException {
		return getService().findByPrimaryKey(assessmentTypeId);
	}

	public static java.util.List<info.diit.portal.assessment.model.AssessmentType> findByCompanyTypeName(
		long companyId, java.lang.String type)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByCompanyTypeName(companyId, type);
	}

	public static void clearService() {
		_service = null;
	}

	public static AssessmentTypeLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					AssessmentTypeLocalService.class.getName());

			if (invokableLocalService instanceof AssessmentTypeLocalService) {
				_service = (AssessmentTypeLocalService)invokableLocalService;
			}
			else {
				_service = new AssessmentTypeLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(AssessmentTypeLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(AssessmentTypeLocalService service) {
	}

	private static AssessmentTypeLocalService _service;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.assessment.model.AssessmentStudent;

/**
 * The persistence interface for the assessment student service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see AssessmentStudentPersistenceImpl
 * @see AssessmentStudentUtil
 * @generated
 */
public interface AssessmentStudentPersistence extends BasePersistence<AssessmentStudent> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AssessmentStudentUtil} to access the assessment student persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the assessment student in the entity cache if it is enabled.
	*
	* @param assessmentStudent the assessment student
	*/
	public void cacheResult(
		info.diit.portal.assessment.model.AssessmentStudent assessmentStudent);

	/**
	* Caches the assessment students in the entity cache if it is enabled.
	*
	* @param assessmentStudents the assessment students
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.assessment.model.AssessmentStudent> assessmentStudents);

	/**
	* Creates a new assessment student with the primary key. Does not add the assessment student to the database.
	*
	* @param assessmentStudentId the primary key for the new assessment student
	* @return the new assessment student
	*/
	public info.diit.portal.assessment.model.AssessmentStudent create(
		long assessmentStudentId);

	/**
	* Removes the assessment student with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param assessmentStudentId the primary key of the assessment student
	* @return the assessment student that was removed
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent remove(
		long assessmentStudentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	public info.diit.portal.assessment.model.AssessmentStudent updateImpl(
		info.diit.portal.assessment.model.AssessmentStudent assessmentStudent,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the assessment student with the primary key or throws a {@link info.diit.portal.assessment.NoSuchAssessmentStudentException} if it could not be found.
	*
	* @param assessmentStudentId the primary key of the assessment student
	* @return the assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent findByPrimaryKey(
		long assessmentStudentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns the assessment student with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param assessmentStudentId the primary key of the assessment student
	* @return the assessment student, or <code>null</code> if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent fetchByPrimaryKey(
		long assessmentStudentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the assessment students where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByuserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the assessment students where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @return the range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByuserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the assessment students where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first assessment student in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent findByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns the first assessment student in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent fetchByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last assessment student in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent findByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns the last assessment student in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent fetchByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the assessment students before and after the current assessment student in the ordered set where userId = &#63;.
	*
	* @param assessmentStudentId the primary key of the current assessment student
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent[] findByuserId_PrevAndNext(
		long assessmentStudentId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns all the assessment students where assessmentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @return the matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByassessmentId(
		long assessmentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the assessment students where assessmentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assessmentId the assessment ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @return the range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByassessmentId(
		long assessmentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the assessment students where assessmentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assessmentId the assessment ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByassessmentId(
		long assessmentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first assessment student in the ordered set where assessmentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent findByassessmentId_First(
		long assessmentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns the first assessment student in the ordered set where assessmentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent fetchByassessmentId_First(
		long assessmentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last assessment student in the ordered set where assessmentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent findByassessmentId_Last(
		long assessmentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns the last assessment student in the ordered set where assessmentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent fetchByassessmentId_Last(
		long assessmentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the assessment students before and after the current assessment student in the ordered set where assessmentId = &#63;.
	*
	* @param assessmentStudentId the primary key of the current assessment student
	* @param assessmentId the assessment ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent[] findByassessmentId_PrevAndNext(
		long assessmentStudentId, long assessmentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns all the assessment students where assessmentId = &#63; and studentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @return the matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByassessmentStudent(
		long assessmentId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the assessment students where assessmentId = &#63; and studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @return the range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByassessmentStudent(
		long assessmentId, long studentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the assessment students where assessmentId = &#63; and studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByassessmentStudent(
		long assessmentId, long studentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent findByassessmentStudent_First(
		long assessmentId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns the first assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent fetchByassessmentStudent_First(
		long assessmentId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent findByassessmentStudent_Last(
		long assessmentId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns the last assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent fetchByassessmentStudent_Last(
		long assessmentId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the assessment students before and after the current assessment student in the ordered set where assessmentId = &#63; and studentId = &#63;.
	*
	* @param assessmentStudentId the primary key of the current assessment student
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent[] findByassessmentStudent_PrevAndNext(
		long assessmentStudentId, long assessmentId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns all the assessment students where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findBycompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the assessment students where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @return the range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findBycompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the assessment students where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findBycompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first assessment student in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent findBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns the first assessment student in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent fetchBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last assessment student in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent findBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns the last assessment student in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent fetchBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the assessment students before and after the current assessment student in the ordered set where companyId = &#63;.
	*
	* @param assessmentStudentId the primary key of the current assessment student
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent[] findBycompany_PrevAndNext(
		long assessmentStudentId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns all the assessment students where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the assessment students where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @return the range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the assessment students where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first assessment student in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns the first assessment student in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last assessment student in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns the last assessment student in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment student, or <code>null</code> if a matching assessment student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the assessment students before and after the current assessment student in the ordered set where organizationId = &#63;.
	*
	* @param assessmentStudentId the primary key of the current assessment student
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment student
	* @throws info.diit.portal.assessment.NoSuchAssessmentStudentException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.assessment.model.AssessmentStudent[] findByorganization_PrevAndNext(
		long assessmentStudentId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentStudentException;

	/**
	* Returns all the assessment students.
	*
	* @return the assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the assessment students.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @return the range of assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the assessment students.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of assessment students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.assessment.model.AssessmentStudent> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the assessment students where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the assessment students where assessmentId = &#63; from the database.
	*
	* @param assessmentId the assessment ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByassessmentId(long assessmentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the assessment students where assessmentId = &#63; and studentId = &#63; from the database.
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByassessmentStudent(long assessmentId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the assessment students where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the assessment students where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the assessment students from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of assessment students where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public int countByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of assessment students where assessmentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @return the number of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public int countByassessmentId(long assessmentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of assessment students where assessmentId = &#63; and studentId = &#63;.
	*
	* @param assessmentId the assessment ID
	* @param studentId the student ID
	* @return the number of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public int countByassessmentStudent(long assessmentId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of assessment students where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public int countBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of assessment students where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching assessment students
	* @throws SystemException if a system exception occurred
	*/
	public int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of assessment students.
	*
	* @return the number of assessment students
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
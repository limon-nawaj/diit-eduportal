/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.assessment.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.assessment.model.Assessment;

import java.util.List;

/**
 * The persistence utility for the assessment service. This utility wraps {@link AssessmentPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see AssessmentPersistence
 * @see AssessmentPersistenceImpl
 * @generated
 */
public class AssessmentUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Assessment assessment) {
		getPersistence().clearCache(assessment);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Assessment> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Assessment> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Assessment> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Assessment update(Assessment assessment, boolean merge)
		throws SystemException {
		return getPersistence().update(assessment, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Assessment update(Assessment assessment, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(assessment, merge, serviceContext);
	}

	/**
	* Caches the assessment in the entity cache if it is enabled.
	*
	* @param assessment the assessment
	*/
	public static void cacheResult(
		info.diit.portal.assessment.model.Assessment assessment) {
		getPersistence().cacheResult(assessment);
	}

	/**
	* Caches the assessments in the entity cache if it is enabled.
	*
	* @param assessments the assessments
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.assessment.model.Assessment> assessments) {
		getPersistence().cacheResult(assessments);
	}

	/**
	* Creates a new assessment with the primary key. Does not add the assessment to the database.
	*
	* @param assessmentId the primary key for the new assessment
	* @return the new assessment
	*/
	public static info.diit.portal.assessment.model.Assessment create(
		long assessmentId) {
		return getPersistence().create(assessmentId);
	}

	/**
	* Removes the assessment with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param assessmentId the primary key of the assessment
	* @return the assessment that was removed
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment remove(
		long assessmentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence().remove(assessmentId);
	}

	public static info.diit.portal.assessment.model.Assessment updateImpl(
		info.diit.portal.assessment.model.Assessment assessment, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(assessment, merge);
	}

	/**
	* Returns the assessment with the primary key or throws a {@link info.diit.portal.assessment.NoSuchAssessmentException} if it could not be found.
	*
	* @param assessmentId the primary key of the assessment
	* @return the assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findByPrimaryKey(
		long assessmentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence().findByPrimaryKey(assessmentId);
	}

	/**
	* Returns the assessment with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param assessmentId the primary key of the assessment
	* @return the assessment, or <code>null</code> if a assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchByPrimaryKey(
		long assessmentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(assessmentId);
	}

	/**
	* Returns all the assessments where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findByuserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuserId(userId);
	}

	/**
	* Returns a range of all the assessments where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @return the range of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findByuserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByuserId(userId, start, end);
	}

	/**
	* Returns an ordered range of all the assessments where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findByuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByuserId(userId, start, end, orderByComparator);
	}

	/**
	* Returns the first assessment in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence().findByuserId_First(userId, orderByComparator);
	}

	/**
	* Returns the first assessment in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment, or <code>null</code> if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchByuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByuserId_First(userId, orderByComparator);
	}

	/**
	* Returns the last assessment in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence().findByuserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the last assessment in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment, or <code>null</code> if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchByuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByuserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the assessments before and after the current assessment in the ordered set where userId = &#63;.
	*
	* @param assessmentId the primary key of the current assessment
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment[] findByuserId_PrevAndNext(
		long assessmentId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findByuserId_PrevAndNext(assessmentId, userId,
			orderByComparator);
	}

	/**
	* Returns all the assessments where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findBycompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycompany(companyId);
	}

	/**
	* Returns a range of all the assessments where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @return the range of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findBycompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the assessments where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findBycompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first assessment in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence().findBycompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first assessment in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment, or <code>null</code> if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBycompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last assessment in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence().findBycompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last assessment in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment, or <code>null</code> if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBycompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the assessments before and after the current assessment in the ordered set where companyId = &#63;.
	*
	* @param assessmentId the primary key of the current assessment
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment[] findBycompany_PrevAndNext(
		long assessmentId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findBycompany_PrevAndNext(assessmentId, companyId,
			orderByComparator);
	}

	/**
	* Returns all the assessments where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId);
	}

	/**
	* Returns a range of all the assessments where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @return the range of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the assessments where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByorganization(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first assessment in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first assessment in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment, or <code>null</code> if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the last assessment in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last assessment in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment, or <code>null</code> if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the assessments before and after the current assessment in the ordered set where organizationId = &#63;.
	*
	* @param assessmentId the primary key of the current assessment
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment[] findByorganization_PrevAndNext(
		long assessmentId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findByorganization_PrevAndNext(assessmentId,
			organizationId, orderByComparator);
	}

	/**
	* Returns all the assessments where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findBybatch(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybatch(batchId);
	}

	/**
	* Returns a range of all the assessments where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @return the range of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findBybatch(
		long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybatch(batchId, start, end);
	}

	/**
	* Returns an ordered range of all the assessments where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findBybatch(
		long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBybatch(batchId, start, end, orderByComparator);
	}

	/**
	* Returns the first assessment in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findBybatch_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence().findBybatch_First(batchId, orderByComparator);
	}

	/**
	* Returns the first assessment in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment, or <code>null</code> if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchBybatch_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBybatch_First(batchId, orderByComparator);
	}

	/**
	* Returns the last assessment in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findBybatch_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence().findBybatch_Last(batchId, orderByComparator);
	}

	/**
	* Returns the last assessment in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment, or <code>null</code> if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchBybatch_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBybatch_Last(batchId, orderByComparator);
	}

	/**
	* Returns the assessments before and after the current assessment in the ordered set where batchId = &#63;.
	*
	* @param assessmentId the primary key of the current assessment
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment[] findBybatch_PrevAndNext(
		long assessmentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findBybatch_PrevAndNext(assessmentId, batchId,
			orderByComparator);
	}

	/**
	* Returns all the assessments where subjectId = &#63; and assessmentTypeId = &#63;.
	*
	* @param subjectId the subject ID
	* @param assessmentTypeId the assessment type ID
	* @return the matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findBysubjectAssessmentType(
		long subjectId, long assessmentTypeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBysubjectAssessmentType(subjectId, assessmentTypeId);
	}

	/**
	* Returns a range of all the assessments where subjectId = &#63; and assessmentTypeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject ID
	* @param assessmentTypeId the assessment type ID
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @return the range of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findBysubjectAssessmentType(
		long subjectId, long assessmentTypeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBysubjectAssessmentType(subjectId, assessmentTypeId,
			start, end);
	}

	/**
	* Returns an ordered range of all the assessments where subjectId = &#63; and assessmentTypeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject ID
	* @param assessmentTypeId the assessment type ID
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findBysubjectAssessmentType(
		long subjectId, long assessmentTypeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBysubjectAssessmentType(subjectId, assessmentTypeId,
			start, end, orderByComparator);
	}

	/**
	* Returns the first assessment in the ordered set where subjectId = &#63; and assessmentTypeId = &#63;.
	*
	* @param subjectId the subject ID
	* @param assessmentTypeId the assessment type ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findBysubjectAssessmentType_First(
		long subjectId, long assessmentTypeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findBysubjectAssessmentType_First(subjectId,
			assessmentTypeId, orderByComparator);
	}

	/**
	* Returns the first assessment in the ordered set where subjectId = &#63; and assessmentTypeId = &#63;.
	*
	* @param subjectId the subject ID
	* @param assessmentTypeId the assessment type ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment, or <code>null</code> if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchBysubjectAssessmentType_First(
		long subjectId, long assessmentTypeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBysubjectAssessmentType_First(subjectId,
			assessmentTypeId, orderByComparator);
	}

	/**
	* Returns the last assessment in the ordered set where subjectId = &#63; and assessmentTypeId = &#63;.
	*
	* @param subjectId the subject ID
	* @param assessmentTypeId the assessment type ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findBysubjectAssessmentType_Last(
		long subjectId, long assessmentTypeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findBysubjectAssessmentType_Last(subjectId,
			assessmentTypeId, orderByComparator);
	}

	/**
	* Returns the last assessment in the ordered set where subjectId = &#63; and assessmentTypeId = &#63;.
	*
	* @param subjectId the subject ID
	* @param assessmentTypeId the assessment type ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment, or <code>null</code> if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchBysubjectAssessmentType_Last(
		long subjectId, long assessmentTypeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBysubjectAssessmentType_Last(subjectId,
			assessmentTypeId, orderByComparator);
	}

	/**
	* Returns the assessments before and after the current assessment in the ordered set where subjectId = &#63; and assessmentTypeId = &#63;.
	*
	* @param assessmentId the primary key of the current assessment
	* @param subjectId the subject ID
	* @param assessmentTypeId the assessment type ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment[] findBysubjectAssessmentType_PrevAndNext(
		long assessmentId, long subjectId, long assessmentTypeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findBysubjectAssessmentType_PrevAndNext(assessmentId,
			subjectId, assessmentTypeId, orderByComparator);
	}

	/**
	* Returns all the assessments where batchId = &#63; and status = &#63;.
	*
	* @param batchId the batch ID
	* @param status the status
	* @return the matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findByBatchStatus(
		long batchId, long status)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBatchStatus(batchId, status);
	}

	/**
	* Returns a range of all the assessments where batchId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param status the status
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @return the range of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findByBatchStatus(
		long batchId, long status, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBatchStatus(batchId, status, start, end);
	}

	/**
	* Returns an ordered range of all the assessments where batchId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param status the status
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findByBatchStatus(
		long batchId, long status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatchStatus(batchId, status, start, end,
			orderByComparator);
	}

	/**
	* Returns the first assessment in the ordered set where batchId = &#63; and status = &#63;.
	*
	* @param batchId the batch ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findByBatchStatus_First(
		long batchId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findByBatchStatus_First(batchId, status, orderByComparator);
	}

	/**
	* Returns the first assessment in the ordered set where batchId = &#63; and status = &#63;.
	*
	* @param batchId the batch ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment, or <code>null</code> if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchByBatchStatus_First(
		long batchId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBatchStatus_First(batchId, status, orderByComparator);
	}

	/**
	* Returns the last assessment in the ordered set where batchId = &#63; and status = &#63;.
	*
	* @param batchId the batch ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findByBatchStatus_Last(
		long batchId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findByBatchStatus_Last(batchId, status, orderByComparator);
	}

	/**
	* Returns the last assessment in the ordered set where batchId = &#63; and status = &#63;.
	*
	* @param batchId the batch ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment, or <code>null</code> if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchByBatchStatus_Last(
		long batchId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBatchStatus_Last(batchId, status, orderByComparator);
	}

	/**
	* Returns the assessments before and after the current assessment in the ordered set where batchId = &#63; and status = &#63;.
	*
	* @param assessmentId the primary key of the current assessment
	* @param batchId the batch ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment[] findByBatchStatus_PrevAndNext(
		long assessmentId, long batchId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findByBatchStatus_PrevAndNext(assessmentId, batchId,
			status, orderByComparator);
	}

	/**
	* Returns all the assessments where subjectId = &#63; and status = &#63;.
	*
	* @param subjectId the subject ID
	* @param status the status
	* @return the matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findBySubjectStatus(
		long subjectId, long status)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBySubjectStatus(subjectId, status);
	}

	/**
	* Returns a range of all the assessments where subjectId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject ID
	* @param status the status
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @return the range of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findBySubjectStatus(
		long subjectId, long status, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBySubjectStatus(subjectId, status, start, end);
	}

	/**
	* Returns an ordered range of all the assessments where subjectId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject ID
	* @param status the status
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findBySubjectStatus(
		long subjectId, long status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBySubjectStatus(subjectId, status, start, end,
			orderByComparator);
	}

	/**
	* Returns the first assessment in the ordered set where subjectId = &#63; and status = &#63;.
	*
	* @param subjectId the subject ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findBySubjectStatus_First(
		long subjectId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findBySubjectStatus_First(subjectId, status,
			orderByComparator);
	}

	/**
	* Returns the first assessment in the ordered set where subjectId = &#63; and status = &#63;.
	*
	* @param subjectId the subject ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment, or <code>null</code> if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchBySubjectStatus_First(
		long subjectId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBySubjectStatus_First(subjectId, status,
			orderByComparator);
	}

	/**
	* Returns the last assessment in the ordered set where subjectId = &#63; and status = &#63;.
	*
	* @param subjectId the subject ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment findBySubjectStatus_Last(
		long subjectId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findBySubjectStatus_Last(subjectId, status,
			orderByComparator);
	}

	/**
	* Returns the last assessment in the ordered set where subjectId = &#63; and status = &#63;.
	*
	* @param subjectId the subject ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment, or <code>null</code> if a matching assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment fetchBySubjectStatus_Last(
		long subjectId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBySubjectStatus_Last(subjectId, status,
			orderByComparator);
	}

	/**
	* Returns the assessments before and after the current assessment in the ordered set where subjectId = &#63; and status = &#63;.
	*
	* @param assessmentId the primary key of the current assessment
	* @param subjectId the subject ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment
	* @throws info.diit.portal.assessment.NoSuchAssessmentException if a assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.assessment.model.Assessment[] findBySubjectStatus_PrevAndNext(
		long assessmentId, long subjectId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.assessment.NoSuchAssessmentException {
		return getPersistence()
				   .findBySubjectStatus_PrevAndNext(assessmentId, subjectId,
			status, orderByComparator);
	}

	/**
	* Returns all the assessments.
	*
	* @return the assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the assessments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @return the range of assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the assessments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of assessments
	* @param end the upper bound of the range of assessments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.assessment.model.Assessment> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the assessments where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByuserId(userId);
	}

	/**
	* Removes all the assessments where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBycompany(companyId);
	}

	/**
	* Removes all the assessments where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByorganization(organizationId);
	}

	/**
	* Removes all the assessments where batchId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBybatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBybatch(batchId);
	}

	/**
	* Removes all the assessments where subjectId = &#63; and assessmentTypeId = &#63; from the database.
	*
	* @param subjectId the subject ID
	* @param assessmentTypeId the assessment type ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBysubjectAssessmentType(long subjectId,
		long assessmentTypeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeBysubjectAssessmentType(subjectId, assessmentTypeId);
	}

	/**
	* Removes all the assessments where batchId = &#63; and status = &#63; from the database.
	*
	* @param batchId the batch ID
	* @param status the status
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBatchStatus(long batchId, long status)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBatchStatus(batchId, status);
	}

	/**
	* Removes all the assessments where subjectId = &#63; and status = &#63; from the database.
	*
	* @param subjectId the subject ID
	* @param status the status
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBySubjectStatus(long subjectId, long status)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBySubjectStatus(subjectId, status);
	}

	/**
	* Removes all the assessments from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of assessments where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static int countByuserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByuserId(userId);
	}

	/**
	* Returns the number of assessments where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static int countBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBycompany(companyId);
	}

	/**
	* Returns the number of assessments where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByorganization(organizationId);
	}

	/**
	* Returns the number of assessments where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the number of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static int countBybatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBybatch(batchId);
	}

	/**
	* Returns the number of assessments where subjectId = &#63; and assessmentTypeId = &#63;.
	*
	* @param subjectId the subject ID
	* @param assessmentTypeId the assessment type ID
	* @return the number of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static int countBysubjectAssessmentType(long subjectId,
		long assessmentTypeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countBysubjectAssessmentType(subjectId, assessmentTypeId);
	}

	/**
	* Returns the number of assessments where batchId = &#63; and status = &#63;.
	*
	* @param batchId the batch ID
	* @param status the status
	* @return the number of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBatchStatus(long batchId, long status)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBatchStatus(batchId, status);
	}

	/**
	* Returns the number of assessments where subjectId = &#63; and status = &#63;.
	*
	* @param subjectId the subject ID
	* @param status the status
	* @return the number of matching assessments
	* @throws SystemException if a system exception occurred
	*/
	public static int countBySubjectStatus(long subjectId, long status)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBySubjectStatus(subjectId, status);
	}

	/**
	* Returns the number of assessments.
	*
	* @return the number of assessments
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static AssessmentPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (AssessmentPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.assessment.service.ClpSerializer.getServletContextName(),
					AssessmentPersistence.class.getName());

			ReferenceRegistry.registerReference(AssessmentUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(AssessmentPersistence persistence) {
	}

	private static AssessmentPersistence _persistence;
}
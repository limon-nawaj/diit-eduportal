/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchEmployeeRoleException;
import info.diit.portal.model.EmployeeRole;
import info.diit.portal.model.impl.EmployeeRoleImpl;
import info.diit.portal.model.impl.EmployeeRoleModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the employee role service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see EmployeeRolePersistence
 * @see EmployeeRoleUtil
 * @generated
 */
public class EmployeeRolePersistenceImpl extends BasePersistenceImpl<EmployeeRole>
	implements EmployeeRolePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EmployeeRoleUtil} to access the employee role persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EmployeeRoleImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeRoleModelImpl.FINDER_CACHE_ENABLED, EmployeeRoleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOrganization",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeRoleModelImpl.FINDER_CACHE_ENABLED, EmployeeRoleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByOrganization",
			new String[] { Long.class.getName() },
			EmployeeRoleModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATION = new FinderPath(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeRoleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOrganization",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY = new FinderPath(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeRoleModelImpl.FINDER_CACHE_ENABLED, EmployeeRoleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompany",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY =
		new FinderPath(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeRoleModelImpl.FINDER_CACHE_ENABLED, EmployeeRoleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompany",
			new String[] { Long.class.getName() },
			EmployeeRoleModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANY = new FinderPath(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeRoleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompany",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeRoleModelImpl.FINDER_CACHE_ENABLED, EmployeeRoleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeRoleModelImpl.FINDER_CACHE_ENABLED, EmployeeRoleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeRoleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the employee role in the entity cache if it is enabled.
	 *
	 * @param employeeRole the employee role
	 */
	public void cacheResult(EmployeeRole employeeRole) {
		EntityCacheUtil.putResult(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeRoleImpl.class, employeeRole.getPrimaryKey(), employeeRole);

		employeeRole.resetOriginalValues();
	}

	/**
	 * Caches the employee roles in the entity cache if it is enabled.
	 *
	 * @param employeeRoles the employee roles
	 */
	public void cacheResult(List<EmployeeRole> employeeRoles) {
		for (EmployeeRole employeeRole : employeeRoles) {
			if (EntityCacheUtil.getResult(
						EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
						EmployeeRoleImpl.class, employeeRole.getPrimaryKey()) == null) {
				cacheResult(employeeRole);
			}
			else {
				employeeRole.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all employee roles.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EmployeeRoleImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EmployeeRoleImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the employee role.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EmployeeRole employeeRole) {
		EntityCacheUtil.removeResult(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeRoleImpl.class, employeeRole.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<EmployeeRole> employeeRoles) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EmployeeRole employeeRole : employeeRoles) {
			EntityCacheUtil.removeResult(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
				EmployeeRoleImpl.class, employeeRole.getPrimaryKey());
		}
	}

	/**
	 * Creates a new employee role with the primary key. Does not add the employee role to the database.
	 *
	 * @param employeeRoleId the primary key for the new employee role
	 * @return the new employee role
	 */
	public EmployeeRole create(long employeeRoleId) {
		EmployeeRole employeeRole = new EmployeeRoleImpl();

		employeeRole.setNew(true);
		employeeRole.setPrimaryKey(employeeRoleId);

		return employeeRole;
	}

	/**
	 * Removes the employee role with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param employeeRoleId the primary key of the employee role
	 * @return the employee role that was removed
	 * @throws info.diit.portal.NoSuchEmployeeRoleException if a employee role with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeRole remove(long employeeRoleId)
		throws NoSuchEmployeeRoleException, SystemException {
		return remove(Long.valueOf(employeeRoleId));
	}

	/**
	 * Removes the employee role with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the employee role
	 * @return the employee role that was removed
	 * @throws info.diit.portal.NoSuchEmployeeRoleException if a employee role with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeRole remove(Serializable primaryKey)
		throws NoSuchEmployeeRoleException, SystemException {
		Session session = null;

		try {
			session = openSession();

			EmployeeRole employeeRole = (EmployeeRole)session.get(EmployeeRoleImpl.class,
					primaryKey);

			if (employeeRole == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEmployeeRoleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(employeeRole);
		}
		catch (NoSuchEmployeeRoleException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EmployeeRole removeImpl(EmployeeRole employeeRole)
		throws SystemException {
		employeeRole = toUnwrappedModel(employeeRole);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, employeeRole);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(employeeRole);

		return employeeRole;
	}

	@Override
	public EmployeeRole updateImpl(
		info.diit.portal.model.EmployeeRole employeeRole, boolean merge)
		throws SystemException {
		employeeRole = toUnwrappedModel(employeeRole);

		boolean isNew = employeeRole.isNew();

		EmployeeRoleModelImpl employeeRoleModelImpl = (EmployeeRoleModelImpl)employeeRole;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, employeeRole, merge);

			employeeRole.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !EmployeeRoleModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((employeeRoleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(employeeRoleModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(employeeRoleModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);
			}

			if ((employeeRoleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(employeeRoleModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);

				args = new Object[] {
						Long.valueOf(employeeRoleModelImpl.getCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);
			}
		}

		EntityCacheUtil.putResult(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeRoleImpl.class, employeeRole.getPrimaryKey(), employeeRole);

		return employeeRole;
	}

	protected EmployeeRole toUnwrappedModel(EmployeeRole employeeRole) {
		if (employeeRole instanceof EmployeeRoleImpl) {
			return employeeRole;
		}

		EmployeeRoleImpl employeeRoleImpl = new EmployeeRoleImpl();

		employeeRoleImpl.setNew(employeeRole.isNew());
		employeeRoleImpl.setPrimaryKey(employeeRole.getPrimaryKey());

		employeeRoleImpl.setEmployeeRoleId(employeeRole.getEmployeeRoleId());
		employeeRoleImpl.setOrganizationId(employeeRole.getOrganizationId());
		employeeRoleImpl.setCompanyId(employeeRole.getCompanyId());
		employeeRoleImpl.setRole(employeeRole.getRole());
		employeeRoleImpl.setStartTime(employeeRole.getStartTime());
		employeeRoleImpl.setEndTime(employeeRole.getEndTime());

		return employeeRoleImpl;
	}

	/**
	 * Returns the employee role with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the employee role
	 * @return the employee role
	 * @throws com.liferay.portal.NoSuchModelException if a employee role with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeRole findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the employee role with the primary key or throws a {@link info.diit.portal.NoSuchEmployeeRoleException} if it could not be found.
	 *
	 * @param employeeRoleId the primary key of the employee role
	 * @return the employee role
	 * @throws info.diit.portal.NoSuchEmployeeRoleException if a employee role with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeRole findByPrimaryKey(long employeeRoleId)
		throws NoSuchEmployeeRoleException, SystemException {
		EmployeeRole employeeRole = fetchByPrimaryKey(employeeRoleId);

		if (employeeRole == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + employeeRoleId);
			}

			throw new NoSuchEmployeeRoleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				employeeRoleId);
		}

		return employeeRole;
	}

	/**
	 * Returns the employee role with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the employee role
	 * @return the employee role, or <code>null</code> if a employee role with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeRole fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the employee role with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param employeeRoleId the primary key of the employee role
	 * @return the employee role, or <code>null</code> if a employee role with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeRole fetchByPrimaryKey(long employeeRoleId)
		throws SystemException {
		EmployeeRole employeeRole = (EmployeeRole)EntityCacheUtil.getResult(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
				EmployeeRoleImpl.class, employeeRoleId);

		if (employeeRole == _nullEmployeeRole) {
			return null;
		}

		if (employeeRole == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				employeeRole = (EmployeeRole)session.get(EmployeeRoleImpl.class,
						Long.valueOf(employeeRoleId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (employeeRole != null) {
					cacheResult(employeeRole);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(EmployeeRoleModelImpl.ENTITY_CACHE_ENABLED,
						EmployeeRoleImpl.class, employeeRoleId,
						_nullEmployeeRole);
				}

				closeSession(session);
			}
		}

		return employeeRole;
	}

	/**
	 * Returns all the employee roles where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching employee roles
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeRole> findByOrganization(long organizationId)
		throws SystemException {
		return findByOrganization(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the employee roles where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of employee roles
	 * @param end the upper bound of the range of employee roles (not inclusive)
	 * @return the range of matching employee roles
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeRole> findByOrganization(long organizationId,
		int start, int end) throws SystemException {
		return findByOrganization(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the employee roles where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of employee roles
	 * @param end the upper bound of the range of employee roles (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching employee roles
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeRole> findByOrganization(long organizationId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<EmployeeRole> list = (List<EmployeeRole>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EmployeeRole employeeRole : list) {
				if ((organizationId != employeeRole.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_EMPLOYEEROLE_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				list = (List<EmployeeRole>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first employee role in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching employee role
	 * @throws info.diit.portal.NoSuchEmployeeRoleException if a matching employee role could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeRole findByOrganization_First(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchEmployeeRoleException, SystemException {
		EmployeeRole employeeRole = fetchByOrganization_First(organizationId,
				orderByComparator);

		if (employeeRole != null) {
			return employeeRole;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEmployeeRoleException(msg.toString());
	}

	/**
	 * Returns the first employee role in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching employee role, or <code>null</code> if a matching employee role could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeRole fetchByOrganization_First(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<EmployeeRole> list = findByOrganization(organizationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last employee role in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching employee role
	 * @throws info.diit.portal.NoSuchEmployeeRoleException if a matching employee role could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeRole findByOrganization_Last(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchEmployeeRoleException, SystemException {
		EmployeeRole employeeRole = fetchByOrganization_Last(organizationId,
				orderByComparator);

		if (employeeRole != null) {
			return employeeRole;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEmployeeRoleException(msg.toString());
	}

	/**
	 * Returns the last employee role in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching employee role, or <code>null</code> if a matching employee role could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeRole fetchByOrganization_Last(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByOrganization(organizationId);

		List<EmployeeRole> list = findByOrganization(organizationId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the employee roles before and after the current employee role in the ordered set where organizationId = &#63;.
	 *
	 * @param employeeRoleId the primary key of the current employee role
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next employee role
	 * @throws info.diit.portal.NoSuchEmployeeRoleException if a employee role with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeRole[] findByOrganization_PrevAndNext(long employeeRoleId,
		long organizationId, OrderByComparator orderByComparator)
		throws NoSuchEmployeeRoleException, SystemException {
		EmployeeRole employeeRole = findByPrimaryKey(employeeRoleId);

		Session session = null;

		try {
			session = openSession();

			EmployeeRole[] array = new EmployeeRoleImpl[3];

			array[0] = getByOrganization_PrevAndNext(session, employeeRole,
					organizationId, orderByComparator, true);

			array[1] = employeeRole;

			array[2] = getByOrganization_PrevAndNext(session, employeeRole,
					organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EmployeeRole getByOrganization_PrevAndNext(Session session,
		EmployeeRole employeeRole, long organizationId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EMPLOYEEROLE_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(employeeRole);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EmployeeRole> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the employee roles where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching employee roles
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeRole> findByCompany(long companyId)
		throws SystemException {
		return findByCompany(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the employee roles where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of employee roles
	 * @param end the upper bound of the range of employee roles (not inclusive)
	 * @return the range of matching employee roles
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeRole> findByCompany(long companyId, int start, int end)
		throws SystemException {
		return findByCompany(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the employee roles where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of employee roles
	 * @param end the upper bound of the range of employee roles (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching employee roles
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeRole> findByCompany(long companyId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<EmployeeRole> list = (List<EmployeeRole>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EmployeeRole employeeRole : list) {
				if ((companyId != employeeRole.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_EMPLOYEEROLE_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<EmployeeRole>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first employee role in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching employee role
	 * @throws info.diit.portal.NoSuchEmployeeRoleException if a matching employee role could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeRole findByCompany_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchEmployeeRoleException, SystemException {
		EmployeeRole employeeRole = fetchByCompany_First(companyId,
				orderByComparator);

		if (employeeRole != null) {
			return employeeRole;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEmployeeRoleException(msg.toString());
	}

	/**
	 * Returns the first employee role in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching employee role, or <code>null</code> if a matching employee role could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeRole fetchByCompany_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<EmployeeRole> list = findByCompany(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last employee role in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching employee role
	 * @throws info.diit.portal.NoSuchEmployeeRoleException if a matching employee role could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeRole findByCompany_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchEmployeeRoleException, SystemException {
		EmployeeRole employeeRole = fetchByCompany_Last(companyId,
				orderByComparator);

		if (employeeRole != null) {
			return employeeRole;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEmployeeRoleException(msg.toString());
	}

	/**
	 * Returns the last employee role in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching employee role, or <code>null</code> if a matching employee role could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeRole fetchByCompany_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompany(companyId);

		List<EmployeeRole> list = findByCompany(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the employee roles before and after the current employee role in the ordered set where companyId = &#63;.
	 *
	 * @param employeeRoleId the primary key of the current employee role
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next employee role
	 * @throws info.diit.portal.NoSuchEmployeeRoleException if a employee role with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeRole[] findByCompany_PrevAndNext(long employeeRoleId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchEmployeeRoleException, SystemException {
		EmployeeRole employeeRole = findByPrimaryKey(employeeRoleId);

		Session session = null;

		try {
			session = openSession();

			EmployeeRole[] array = new EmployeeRoleImpl[3];

			array[0] = getByCompany_PrevAndNext(session, employeeRole,
					companyId, orderByComparator, true);

			array[1] = employeeRole;

			array[2] = getByCompany_PrevAndNext(session, employeeRole,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EmployeeRole getByCompany_PrevAndNext(Session session,
		EmployeeRole employeeRole, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EMPLOYEEROLE_WHERE);

		query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(employeeRole);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EmployeeRole> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the employee roles.
	 *
	 * @return the employee roles
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeRole> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the employee roles.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of employee roles
	 * @param end the upper bound of the range of employee roles (not inclusive)
	 * @return the range of employee roles
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeRole> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the employee roles.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of employee roles
	 * @param end the upper bound of the range of employee roles (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of employee roles
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeRole> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EmployeeRole> list = (List<EmployeeRole>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_EMPLOYEEROLE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_EMPLOYEEROLE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<EmployeeRole>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<EmployeeRole>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the employee roles where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByOrganization(long organizationId)
		throws SystemException {
		for (EmployeeRole employeeRole : findByOrganization(organizationId)) {
			remove(employeeRole);
		}
	}

	/**
	 * Removes all the employee roles where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCompany(long companyId) throws SystemException {
		for (EmployeeRole employeeRole : findByCompany(companyId)) {
			remove(employeeRole);
		}
	}

	/**
	 * Removes all the employee roles from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (EmployeeRole employeeRole : findAll()) {
			remove(employeeRole);
		}
	}

	/**
	 * Returns the number of employee roles where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching employee roles
	 * @throws SystemException if a system exception occurred
	 */
	public int countByOrganization(long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EMPLOYEEROLE_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of employee roles where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching employee roles
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCompany(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EMPLOYEEROLE_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of employee roles.
	 *
	 * @return the number of employee roles
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_EMPLOYEEROLE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the employee role persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.EmployeeRole")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<EmployeeRole>> listenersList = new ArrayList<ModelListener<EmployeeRole>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<EmployeeRole>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EmployeeRoleImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttendanceCorrectionPersistence.class)
	protected AttendanceCorrectionPersistence attendanceCorrectionPersistence;
	@BeanReference(type = DayTypePersistence.class)
	protected DayTypePersistence dayTypePersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = WorkingDayPersistence.class)
	protected WorkingDayPersistence workingDayPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_EMPLOYEEROLE = "SELECT employeeRole FROM EmployeeRole employeeRole";
	private static final String _SQL_SELECT_EMPLOYEEROLE_WHERE = "SELECT employeeRole FROM EmployeeRole employeeRole WHERE ";
	private static final String _SQL_COUNT_EMPLOYEEROLE = "SELECT COUNT(employeeRole) FROM EmployeeRole employeeRole";
	private static final String _SQL_COUNT_EMPLOYEEROLE_WHERE = "SELECT COUNT(employeeRole) FROM EmployeeRole employeeRole WHERE ";
	private static final String _FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2 = "employeeRole.organizationId = ?";
	private static final String _FINDER_COLUMN_COMPANY_COMPANYID_2 = "employeeRole.companyId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "employeeRole.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EmployeeRole exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No EmployeeRole exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EmployeeRolePersistenceImpl.class);
	private static EmployeeRole _nullEmployeeRole = new EmployeeRoleImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<EmployeeRole> toCacheModel() {
				return _nullEmployeeRoleCacheModel;
			}
		};

	private static CacheModel<EmployeeRole> _nullEmployeeRoleCacheModel = new CacheModel<EmployeeRole>() {
			public EmployeeRole toEntityModel() {
				return _nullEmployeeRole;
			}
		};
}
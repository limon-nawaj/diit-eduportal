/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchAttendanceCorrectionException;
import info.diit.portal.model.AttendanceCorrection;
import info.diit.portal.model.impl.AttendanceCorrectionImpl;
import info.diit.portal.model.impl.AttendanceCorrectionModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the attendance correction service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see AttendanceCorrectionPersistence
 * @see AttendanceCorrectionUtil
 * @generated
 */
public class AttendanceCorrectionPersistenceImpl extends BasePersistenceImpl<AttendanceCorrection>
	implements AttendanceCorrectionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AttendanceCorrectionUtil} to access the attendance correction persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AttendanceCorrectionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY = new FinderPath(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceCorrectionModelImpl.FINDER_CACHE_ENABLED,
			AttendanceCorrectionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompany",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY =
		new FinderPath(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceCorrectionModelImpl.FINDER_CACHE_ENABLED,
			AttendanceCorrectionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompany",
			new String[] { Long.class.getName() },
			AttendanceCorrectionModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANY = new FinderPath(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceCorrectionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompany",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEE = new FinderPath(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceCorrectionModelImpl.FINDER_CACHE_ENABLED,
			AttendanceCorrectionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByEmployee",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE =
		new FinderPath(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceCorrectionModelImpl.FINDER_CACHE_ENABLED,
			AttendanceCorrectionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByEmployee",
			new String[] { Long.class.getName() },
			AttendanceCorrectionModelImpl.EMPLOYEEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMPLOYEE = new FinderPath(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceCorrectionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEmployee",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceCorrectionModelImpl.FINDER_CACHE_ENABLED,
			AttendanceCorrectionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceCorrectionModelImpl.FINDER_CACHE_ENABLED,
			AttendanceCorrectionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceCorrectionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the attendance correction in the entity cache if it is enabled.
	 *
	 * @param attendanceCorrection the attendance correction
	 */
	public void cacheResult(AttendanceCorrection attendanceCorrection) {
		EntityCacheUtil.putResult(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceCorrectionImpl.class,
			attendanceCorrection.getPrimaryKey(), attendanceCorrection);

		attendanceCorrection.resetOriginalValues();
	}

	/**
	 * Caches the attendance corrections in the entity cache if it is enabled.
	 *
	 * @param attendanceCorrections the attendance corrections
	 */
	public void cacheResult(List<AttendanceCorrection> attendanceCorrections) {
		for (AttendanceCorrection attendanceCorrection : attendanceCorrections) {
			if (EntityCacheUtil.getResult(
						AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
						AttendanceCorrectionImpl.class,
						attendanceCorrection.getPrimaryKey()) == null) {
				cacheResult(attendanceCorrection);
			}
			else {
				attendanceCorrection.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all attendance corrections.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AttendanceCorrectionImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AttendanceCorrectionImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the attendance correction.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AttendanceCorrection attendanceCorrection) {
		EntityCacheUtil.removeResult(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceCorrectionImpl.class, attendanceCorrection.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<AttendanceCorrection> attendanceCorrections) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AttendanceCorrection attendanceCorrection : attendanceCorrections) {
			EntityCacheUtil.removeResult(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
				AttendanceCorrectionImpl.class,
				attendanceCorrection.getPrimaryKey());
		}
	}

	/**
	 * Creates a new attendance correction with the primary key. Does not add the attendance correction to the database.
	 *
	 * @param attendanceCorrectionId the primary key for the new attendance correction
	 * @return the new attendance correction
	 */
	public AttendanceCorrection create(long attendanceCorrectionId) {
		AttendanceCorrection attendanceCorrection = new AttendanceCorrectionImpl();

		attendanceCorrection.setNew(true);
		attendanceCorrection.setPrimaryKey(attendanceCorrectionId);

		return attendanceCorrection;
	}

	/**
	 * Removes the attendance correction with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param attendanceCorrectionId the primary key of the attendance correction
	 * @return the attendance correction that was removed
	 * @throws info.diit.portal.NoSuchAttendanceCorrectionException if a attendance correction with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceCorrection remove(long attendanceCorrectionId)
		throws NoSuchAttendanceCorrectionException, SystemException {
		return remove(Long.valueOf(attendanceCorrectionId));
	}

	/**
	 * Removes the attendance correction with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the attendance correction
	 * @return the attendance correction that was removed
	 * @throws info.diit.portal.NoSuchAttendanceCorrectionException if a attendance correction with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AttendanceCorrection remove(Serializable primaryKey)
		throws NoSuchAttendanceCorrectionException, SystemException {
		Session session = null;

		try {
			session = openSession();

			AttendanceCorrection attendanceCorrection = (AttendanceCorrection)session.get(AttendanceCorrectionImpl.class,
					primaryKey);

			if (attendanceCorrection == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAttendanceCorrectionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(attendanceCorrection);
		}
		catch (NoSuchAttendanceCorrectionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AttendanceCorrection removeImpl(
		AttendanceCorrection attendanceCorrection) throws SystemException {
		attendanceCorrection = toUnwrappedModel(attendanceCorrection);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, attendanceCorrection);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(attendanceCorrection);

		return attendanceCorrection;
	}

	@Override
	public AttendanceCorrection updateImpl(
		info.diit.portal.model.AttendanceCorrection attendanceCorrection,
		boolean merge) throws SystemException {
		attendanceCorrection = toUnwrappedModel(attendanceCorrection);

		boolean isNew = attendanceCorrection.isNew();

		AttendanceCorrectionModelImpl attendanceCorrectionModelImpl = (AttendanceCorrectionModelImpl)attendanceCorrection;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, attendanceCorrection, merge);

			attendanceCorrection.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !AttendanceCorrectionModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((attendanceCorrectionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(attendanceCorrectionModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);

				args = new Object[] {
						Long.valueOf(attendanceCorrectionModelImpl.getCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);
			}

			if ((attendanceCorrectionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(attendanceCorrectionModelImpl.getOriginalEmployeeId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE,
					args);

				args = new Object[] {
						Long.valueOf(attendanceCorrectionModelImpl.getEmployeeId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE,
					args);
			}
		}

		EntityCacheUtil.putResult(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceCorrectionImpl.class,
			attendanceCorrection.getPrimaryKey(), attendanceCorrection);

		return attendanceCorrection;
	}

	protected AttendanceCorrection toUnwrappedModel(
		AttendanceCorrection attendanceCorrection) {
		if (attendanceCorrection instanceof AttendanceCorrectionImpl) {
			return attendanceCorrection;
		}

		AttendanceCorrectionImpl attendanceCorrectionImpl = new AttendanceCorrectionImpl();

		attendanceCorrectionImpl.setNew(attendanceCorrection.isNew());
		attendanceCorrectionImpl.setPrimaryKey(attendanceCorrection.getPrimaryKey());

		attendanceCorrectionImpl.setAttendanceCorrectionId(attendanceCorrection.getAttendanceCorrectionId());
		attendanceCorrectionImpl.setOrganizationId(attendanceCorrection.getOrganizationId());
		attendanceCorrectionImpl.setCompanyId(attendanceCorrection.getCompanyId());
		attendanceCorrectionImpl.setEmployeeId(attendanceCorrection.getEmployeeId());
		attendanceCorrectionImpl.setCorrectionDate(attendanceCorrection.getCorrectionDate());
		attendanceCorrectionImpl.setRealIn(attendanceCorrection.getRealIn());
		attendanceCorrectionImpl.setRealOut(attendanceCorrection.getRealOut());
		attendanceCorrectionImpl.setExpectedIn(attendanceCorrection.getExpectedIn());
		attendanceCorrectionImpl.setExpectedOut(attendanceCorrection.getExpectedOut());
		attendanceCorrectionImpl.setStatus(attendanceCorrection.getStatus());

		return attendanceCorrectionImpl;
	}

	/**
	 * Returns the attendance correction with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the attendance correction
	 * @return the attendance correction
	 * @throws com.liferay.portal.NoSuchModelException if a attendance correction with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AttendanceCorrection findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the attendance correction with the primary key or throws a {@link info.diit.portal.NoSuchAttendanceCorrectionException} if it could not be found.
	 *
	 * @param attendanceCorrectionId the primary key of the attendance correction
	 * @return the attendance correction
	 * @throws info.diit.portal.NoSuchAttendanceCorrectionException if a attendance correction with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceCorrection findByPrimaryKey(long attendanceCorrectionId)
		throws NoSuchAttendanceCorrectionException, SystemException {
		AttendanceCorrection attendanceCorrection = fetchByPrimaryKey(attendanceCorrectionId);

		if (attendanceCorrection == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					attendanceCorrectionId);
			}

			throw new NoSuchAttendanceCorrectionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				attendanceCorrectionId);
		}

		return attendanceCorrection;
	}

	/**
	 * Returns the attendance correction with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the attendance correction
	 * @return the attendance correction, or <code>null</code> if a attendance correction with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AttendanceCorrection fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the attendance correction with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param attendanceCorrectionId the primary key of the attendance correction
	 * @return the attendance correction, or <code>null</code> if a attendance correction with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceCorrection fetchByPrimaryKey(long attendanceCorrectionId)
		throws SystemException {
		AttendanceCorrection attendanceCorrection = (AttendanceCorrection)EntityCacheUtil.getResult(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
				AttendanceCorrectionImpl.class, attendanceCorrectionId);

		if (attendanceCorrection == _nullAttendanceCorrection) {
			return null;
		}

		if (attendanceCorrection == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				attendanceCorrection = (AttendanceCorrection)session.get(AttendanceCorrectionImpl.class,
						Long.valueOf(attendanceCorrectionId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (attendanceCorrection != null) {
					cacheResult(attendanceCorrection);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(AttendanceCorrectionModelImpl.ENTITY_CACHE_ENABLED,
						AttendanceCorrectionImpl.class, attendanceCorrectionId,
						_nullAttendanceCorrection);
				}

				closeSession(session);
			}
		}

		return attendanceCorrection;
	}

	/**
	 * Returns all the attendance corrections where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching attendance corrections
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceCorrection> findByCompany(long companyId)
		throws SystemException {
		return findByCompany(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the attendance corrections where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of attendance corrections
	 * @param end the upper bound of the range of attendance corrections (not inclusive)
	 * @return the range of matching attendance corrections
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceCorrection> findByCompany(long companyId, int start,
		int end) throws SystemException {
		return findByCompany(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the attendance corrections where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of attendance corrections
	 * @param end the upper bound of the range of attendance corrections (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching attendance corrections
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceCorrection> findByCompany(long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<AttendanceCorrection> list = (List<AttendanceCorrection>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AttendanceCorrection attendanceCorrection : list) {
				if ((companyId != attendanceCorrection.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ATTENDANCECORRECTION_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AttendanceCorrectionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<AttendanceCorrection>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first attendance correction in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attendance correction
	 * @throws info.diit.portal.NoSuchAttendanceCorrectionException if a matching attendance correction could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceCorrection findByCompany_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceCorrectionException, SystemException {
		AttendanceCorrection attendanceCorrection = fetchByCompany_First(companyId,
				orderByComparator);

		if (attendanceCorrection != null) {
			return attendanceCorrection;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAttendanceCorrectionException(msg.toString());
	}

	/**
	 * Returns the first attendance correction in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attendance correction, or <code>null</code> if a matching attendance correction could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceCorrection fetchByCompany_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<AttendanceCorrection> list = findByCompany(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last attendance correction in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attendance correction
	 * @throws info.diit.portal.NoSuchAttendanceCorrectionException if a matching attendance correction could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceCorrection findByCompany_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceCorrectionException, SystemException {
		AttendanceCorrection attendanceCorrection = fetchByCompany_Last(companyId,
				orderByComparator);

		if (attendanceCorrection != null) {
			return attendanceCorrection;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAttendanceCorrectionException(msg.toString());
	}

	/**
	 * Returns the last attendance correction in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attendance correction, or <code>null</code> if a matching attendance correction could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceCorrection fetchByCompany_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompany(companyId);

		List<AttendanceCorrection> list = findByCompany(companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the attendance corrections before and after the current attendance correction in the ordered set where companyId = &#63;.
	 *
	 * @param attendanceCorrectionId the primary key of the current attendance correction
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next attendance correction
	 * @throws info.diit.portal.NoSuchAttendanceCorrectionException if a attendance correction with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceCorrection[] findByCompany_PrevAndNext(
		long attendanceCorrectionId, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceCorrectionException, SystemException {
		AttendanceCorrection attendanceCorrection = findByPrimaryKey(attendanceCorrectionId);

		Session session = null;

		try {
			session = openSession();

			AttendanceCorrection[] array = new AttendanceCorrectionImpl[3];

			array[0] = getByCompany_PrevAndNext(session, attendanceCorrection,
					companyId, orderByComparator, true);

			array[1] = attendanceCorrection;

			array[2] = getByCompany_PrevAndNext(session, attendanceCorrection,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AttendanceCorrection getByCompany_PrevAndNext(Session session,
		AttendanceCorrection attendanceCorrection, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ATTENDANCECORRECTION_WHERE);

		query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AttendanceCorrectionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(attendanceCorrection);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AttendanceCorrection> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the attendance corrections where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @return the matching attendance corrections
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceCorrection> findByEmployee(long employeeId)
		throws SystemException {
		return findByEmployee(employeeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the attendance corrections where employeeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employeeId the employee ID
	 * @param start the lower bound of the range of attendance corrections
	 * @param end the upper bound of the range of attendance corrections (not inclusive)
	 * @return the range of matching attendance corrections
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceCorrection> findByEmployee(long employeeId,
		int start, int end) throws SystemException {
		return findByEmployee(employeeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the attendance corrections where employeeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employeeId the employee ID
	 * @param start the lower bound of the range of attendance corrections
	 * @param end the upper bound of the range of attendance corrections (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching attendance corrections
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceCorrection> findByEmployee(long employeeId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE;
			finderArgs = new Object[] { employeeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEE;
			finderArgs = new Object[] { employeeId, start, end, orderByComparator };
		}

		List<AttendanceCorrection> list = (List<AttendanceCorrection>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AttendanceCorrection attendanceCorrection : list) {
				if ((employeeId != attendanceCorrection.getEmployeeId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ATTENDANCECORRECTION_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(AttendanceCorrectionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				list = (List<AttendanceCorrection>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first attendance correction in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attendance correction
	 * @throws info.diit.portal.NoSuchAttendanceCorrectionException if a matching attendance correction could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceCorrection findByEmployee_First(long employeeId,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceCorrectionException, SystemException {
		AttendanceCorrection attendanceCorrection = fetchByEmployee_First(employeeId,
				orderByComparator);

		if (attendanceCorrection != null) {
			return attendanceCorrection;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employeeId=");
		msg.append(employeeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAttendanceCorrectionException(msg.toString());
	}

	/**
	 * Returns the first attendance correction in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attendance correction, or <code>null</code> if a matching attendance correction could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceCorrection fetchByEmployee_First(long employeeId,
		OrderByComparator orderByComparator) throws SystemException {
		List<AttendanceCorrection> list = findByEmployee(employeeId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last attendance correction in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attendance correction
	 * @throws info.diit.portal.NoSuchAttendanceCorrectionException if a matching attendance correction could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceCorrection findByEmployee_Last(long employeeId,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceCorrectionException, SystemException {
		AttendanceCorrection attendanceCorrection = fetchByEmployee_Last(employeeId,
				orderByComparator);

		if (attendanceCorrection != null) {
			return attendanceCorrection;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employeeId=");
		msg.append(employeeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAttendanceCorrectionException(msg.toString());
	}

	/**
	 * Returns the last attendance correction in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attendance correction, or <code>null</code> if a matching attendance correction could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceCorrection fetchByEmployee_Last(long employeeId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByEmployee(employeeId);

		List<AttendanceCorrection> list = findByEmployee(employeeId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the attendance corrections before and after the current attendance correction in the ordered set where employeeId = &#63;.
	 *
	 * @param attendanceCorrectionId the primary key of the current attendance correction
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next attendance correction
	 * @throws info.diit.portal.NoSuchAttendanceCorrectionException if a attendance correction with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AttendanceCorrection[] findByEmployee_PrevAndNext(
		long attendanceCorrectionId, long employeeId,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceCorrectionException, SystemException {
		AttendanceCorrection attendanceCorrection = findByPrimaryKey(attendanceCorrectionId);

		Session session = null;

		try {
			session = openSession();

			AttendanceCorrection[] array = new AttendanceCorrectionImpl[3];

			array[0] = getByEmployee_PrevAndNext(session, attendanceCorrection,
					employeeId, orderByComparator, true);

			array[1] = attendanceCorrection;

			array[2] = getByEmployee_PrevAndNext(session, attendanceCorrection,
					employeeId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AttendanceCorrection getByEmployee_PrevAndNext(Session session,
		AttendanceCorrection attendanceCorrection, long employeeId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ATTENDANCECORRECTION_WHERE);

		query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(AttendanceCorrectionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(employeeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(attendanceCorrection);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AttendanceCorrection> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the attendance corrections.
	 *
	 * @return the attendance corrections
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceCorrection> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the attendance corrections.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of attendance corrections
	 * @param end the upper bound of the range of attendance corrections (not inclusive)
	 * @return the range of attendance corrections
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceCorrection> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the attendance corrections.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of attendance corrections
	 * @param end the upper bound of the range of attendance corrections (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of attendance corrections
	 * @throws SystemException if a system exception occurred
	 */
	public List<AttendanceCorrection> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<AttendanceCorrection> list = (List<AttendanceCorrection>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ATTENDANCECORRECTION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ATTENDANCECORRECTION.concat(AttendanceCorrectionModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<AttendanceCorrection>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<AttendanceCorrection>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the attendance corrections where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCompany(long companyId) throws SystemException {
		for (AttendanceCorrection attendanceCorrection : findByCompany(
				companyId)) {
			remove(attendanceCorrection);
		}
	}

	/**
	 * Removes all the attendance corrections where employeeId = &#63; from the database.
	 *
	 * @param employeeId the employee ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByEmployee(long employeeId) throws SystemException {
		for (AttendanceCorrection attendanceCorrection : findByEmployee(
				employeeId)) {
			remove(attendanceCorrection);
		}
	}

	/**
	 * Removes all the attendance corrections from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (AttendanceCorrection attendanceCorrection : findAll()) {
			remove(attendanceCorrection);
		}
	}

	/**
	 * Returns the number of attendance corrections where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching attendance corrections
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCompany(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ATTENDANCECORRECTION_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of attendance corrections where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @return the number of matching attendance corrections
	 * @throws SystemException if a system exception occurred
	 */
	public int countByEmployee(long employeeId) throws SystemException {
		Object[] finderArgs = new Object[] { employeeId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_EMPLOYEE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ATTENDANCECORRECTION_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EMPLOYEE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of attendance corrections.
	 *
	 * @return the number of attendance corrections
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ATTENDANCECORRECTION);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the attendance correction persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.AttendanceCorrection")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<AttendanceCorrection>> listenersList = new ArrayList<ModelListener<AttendanceCorrection>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<AttendanceCorrection>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AttendanceCorrectionImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttendanceCorrectionPersistence.class)
	protected AttendanceCorrectionPersistence attendanceCorrectionPersistence;
	@BeanReference(type = DayTypePersistence.class)
	protected DayTypePersistence dayTypePersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = WorkingDayPersistence.class)
	protected WorkingDayPersistence workingDayPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_ATTENDANCECORRECTION = "SELECT attendanceCorrection FROM AttendanceCorrection attendanceCorrection";
	private static final String _SQL_SELECT_ATTENDANCECORRECTION_WHERE = "SELECT attendanceCorrection FROM AttendanceCorrection attendanceCorrection WHERE ";
	private static final String _SQL_COUNT_ATTENDANCECORRECTION = "SELECT COUNT(attendanceCorrection) FROM AttendanceCorrection attendanceCorrection";
	private static final String _SQL_COUNT_ATTENDANCECORRECTION_WHERE = "SELECT COUNT(attendanceCorrection) FROM AttendanceCorrection attendanceCorrection WHERE ";
	private static final String _FINDER_COLUMN_COMPANY_COMPANYID_2 = "attendanceCorrection.companyId = ?";
	private static final String _FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2 = "attendanceCorrection.employeeId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "attendanceCorrection.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AttendanceCorrection exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No AttendanceCorrection exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AttendanceCorrectionPersistenceImpl.class);
	private static AttendanceCorrection _nullAttendanceCorrection = new AttendanceCorrectionImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<AttendanceCorrection> toCacheModel() {
				return _nullAttendanceCorrectionCacheModel;
			}
		};

	private static CacheModel<AttendanceCorrection> _nullAttendanceCorrectionCacheModel =
		new CacheModel<AttendanceCorrection>() {
			public AttendanceCorrection toEntityModel() {
				return _nullAttendanceCorrection;
			}
		};
}
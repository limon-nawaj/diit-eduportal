/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchWorkingDayException;
import info.diit.portal.model.WorkingDay;
import info.diit.portal.model.impl.WorkingDayImpl;
import info.diit.portal.model.impl.WorkingDayModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * The persistence implementation for the working day service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see WorkingDayPersistence
 * @see WorkingDayUtil
 * @generated
 */
public class WorkingDayPersistenceImpl extends BasePersistenceImpl<WorkingDay>
	implements WorkingDayPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link WorkingDayUtil} to access the working day persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = WorkingDayImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
			WorkingDayModelImpl.FINDER_CACHE_ENABLED, WorkingDayImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOrganization",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
			WorkingDayModelImpl.FINDER_CACHE_ENABLED, WorkingDayImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByOrganization",
			new String[] { Long.class.getName() },
			WorkingDayModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATION = new FinderPath(WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
			WorkingDayModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOrganization",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FETCH_BY_ORGANIZATIONDATE = new FinderPath(WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
			WorkingDayModelImpl.FINDER_CACHE_ENABLED, WorkingDayImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByOrganizationDate",
			new String[] { Long.class.getName(), Date.class.getName() },
			WorkingDayModelImpl.ORGANIZATIONID_COLUMN_BITMASK |
			WorkingDayModelImpl.DATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATIONDATE = new FinderPath(WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
			WorkingDayModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByOrganizationDate",
			new String[] { Long.class.getName(), Date.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
			WorkingDayModelImpl.FINDER_CACHE_ENABLED, WorkingDayImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
			WorkingDayModelImpl.FINDER_CACHE_ENABLED, WorkingDayImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
			WorkingDayModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the working day in the entity cache if it is enabled.
	 *
	 * @param workingDay the working day
	 */
	public void cacheResult(WorkingDay workingDay) {
		EntityCacheUtil.putResult(WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
			WorkingDayImpl.class, workingDay.getPrimaryKey(), workingDay);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATIONDATE,
			new Object[] {
				Long.valueOf(workingDay.getOrganizationId()),
				
			workingDay.getDate()
			}, workingDay);

		workingDay.resetOriginalValues();
	}

	/**
	 * Caches the working daies in the entity cache if it is enabled.
	 *
	 * @param workingDaies the working daies
	 */
	public void cacheResult(List<WorkingDay> workingDaies) {
		for (WorkingDay workingDay : workingDaies) {
			if (EntityCacheUtil.getResult(
						WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
						WorkingDayImpl.class, workingDay.getPrimaryKey()) == null) {
				cacheResult(workingDay);
			}
			else {
				workingDay.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all working daies.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(WorkingDayImpl.class.getName());
		}

		EntityCacheUtil.clearCache(WorkingDayImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the working day.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(WorkingDay workingDay) {
		EntityCacheUtil.removeResult(WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
			WorkingDayImpl.class, workingDay.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(workingDay);
	}

	@Override
	public void clearCache(List<WorkingDay> workingDaies) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (WorkingDay workingDay : workingDaies) {
			EntityCacheUtil.removeResult(WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
				WorkingDayImpl.class, workingDay.getPrimaryKey());

			clearUniqueFindersCache(workingDay);
		}
	}

	protected void clearUniqueFindersCache(WorkingDay workingDay) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ORGANIZATIONDATE,
			new Object[] {
				Long.valueOf(workingDay.getOrganizationId()),
				
			workingDay.getDate()
			});
	}

	/**
	 * Creates a new working day with the primary key. Does not add the working day to the database.
	 *
	 * @param workingDayId the primary key for the new working day
	 * @return the new working day
	 */
	public WorkingDay create(long workingDayId) {
		WorkingDay workingDay = new WorkingDayImpl();

		workingDay.setNew(true);
		workingDay.setPrimaryKey(workingDayId);

		return workingDay;
	}

	/**
	 * Removes the working day with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param workingDayId the primary key of the working day
	 * @return the working day that was removed
	 * @throws info.diit.portal.NoSuchWorkingDayException if a working day with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public WorkingDay remove(long workingDayId)
		throws NoSuchWorkingDayException, SystemException {
		return remove(Long.valueOf(workingDayId));
	}

	/**
	 * Removes the working day with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the working day
	 * @return the working day that was removed
	 * @throws info.diit.portal.NoSuchWorkingDayException if a working day with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WorkingDay remove(Serializable primaryKey)
		throws NoSuchWorkingDayException, SystemException {
		Session session = null;

		try {
			session = openSession();

			WorkingDay workingDay = (WorkingDay)session.get(WorkingDayImpl.class,
					primaryKey);

			if (workingDay == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchWorkingDayException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(workingDay);
		}
		catch (NoSuchWorkingDayException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected WorkingDay removeImpl(WorkingDay workingDay)
		throws SystemException {
		workingDay = toUnwrappedModel(workingDay);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, workingDay);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(workingDay);

		return workingDay;
	}

	@Override
	public WorkingDay updateImpl(info.diit.portal.model.WorkingDay workingDay,
		boolean merge) throws SystemException {
		workingDay = toUnwrappedModel(workingDay);

		boolean isNew = workingDay.isNew();

		WorkingDayModelImpl workingDayModelImpl = (WorkingDayModelImpl)workingDay;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, workingDay, merge);

			workingDay.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !WorkingDayModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((workingDayModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(workingDayModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(workingDayModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);
			}
		}

		EntityCacheUtil.putResult(WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
			WorkingDayImpl.class, workingDay.getPrimaryKey(), workingDay);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATIONDATE,
				new Object[] {
					Long.valueOf(workingDay.getOrganizationId()),
					
				workingDay.getDate()
				}, workingDay);
		}
		else {
			if ((workingDayModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_ORGANIZATIONDATE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(workingDayModelImpl.getOriginalOrganizationId()),
						
						workingDayModelImpl.getOriginalDate()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATIONDATE,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ORGANIZATIONDATE,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATIONDATE,
					new Object[] {
						Long.valueOf(workingDay.getOrganizationId()),
						
					workingDay.getDate()
					}, workingDay);
			}
		}

		return workingDay;
	}

	protected WorkingDay toUnwrappedModel(WorkingDay workingDay) {
		if (workingDay instanceof WorkingDayImpl) {
			return workingDay;
		}

		WorkingDayImpl workingDayImpl = new WorkingDayImpl();

		workingDayImpl.setNew(workingDay.isNew());
		workingDayImpl.setPrimaryKey(workingDay.getPrimaryKey());

		workingDayImpl.setWorkingDayId(workingDay.getWorkingDayId());
		workingDayImpl.setOrganizationId(workingDay.getOrganizationId());
		workingDayImpl.setDate(workingDay.getDate());
		workingDayImpl.setDayOfWeek(workingDay.getDayOfWeek());
		workingDayImpl.setWeek(workingDay.getWeek());
		workingDayImpl.setStatus(workingDay.getStatus());

		return workingDayImpl;
	}

	/**
	 * Returns the working day with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the working day
	 * @return the working day
	 * @throws com.liferay.portal.NoSuchModelException if a working day with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WorkingDay findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the working day with the primary key or throws a {@link info.diit.portal.NoSuchWorkingDayException} if it could not be found.
	 *
	 * @param workingDayId the primary key of the working day
	 * @return the working day
	 * @throws info.diit.portal.NoSuchWorkingDayException if a working day with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public WorkingDay findByPrimaryKey(long workingDayId)
		throws NoSuchWorkingDayException, SystemException {
		WorkingDay workingDay = fetchByPrimaryKey(workingDayId);

		if (workingDay == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + workingDayId);
			}

			throw new NoSuchWorkingDayException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				workingDayId);
		}

		return workingDay;
	}

	/**
	 * Returns the working day with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the working day
	 * @return the working day, or <code>null</code> if a working day with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WorkingDay fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the working day with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param workingDayId the primary key of the working day
	 * @return the working day, or <code>null</code> if a working day with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public WorkingDay fetchByPrimaryKey(long workingDayId)
		throws SystemException {
		WorkingDay workingDay = (WorkingDay)EntityCacheUtil.getResult(WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
				WorkingDayImpl.class, workingDayId);

		if (workingDay == _nullWorkingDay) {
			return null;
		}

		if (workingDay == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				workingDay = (WorkingDay)session.get(WorkingDayImpl.class,
						Long.valueOf(workingDayId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (workingDay != null) {
					cacheResult(workingDay);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(WorkingDayModelImpl.ENTITY_CACHE_ENABLED,
						WorkingDayImpl.class, workingDayId, _nullWorkingDay);
				}

				closeSession(session);
			}
		}

		return workingDay;
	}

	/**
	 * Returns all the working daies where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching working daies
	 * @throws SystemException if a system exception occurred
	 */
	public List<WorkingDay> findByOrganization(long organizationId)
		throws SystemException {
		return findByOrganization(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the working daies where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of working daies
	 * @param end the upper bound of the range of working daies (not inclusive)
	 * @return the range of matching working daies
	 * @throws SystemException if a system exception occurred
	 */
	public List<WorkingDay> findByOrganization(long organizationId, int start,
		int end) throws SystemException {
		return findByOrganization(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the working daies where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of working daies
	 * @param end the upper bound of the range of working daies (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching working daies
	 * @throws SystemException if a system exception occurred
	 */
	public List<WorkingDay> findByOrganization(long organizationId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<WorkingDay> list = (List<WorkingDay>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (WorkingDay workingDay : list) {
				if ((organizationId != workingDay.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_WORKINGDAY_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				list = (List<WorkingDay>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first working day in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching working day
	 * @throws info.diit.portal.NoSuchWorkingDayException if a matching working day could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public WorkingDay findByOrganization_First(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchWorkingDayException, SystemException {
		WorkingDay workingDay = fetchByOrganization_First(organizationId,
				orderByComparator);

		if (workingDay != null) {
			return workingDay;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWorkingDayException(msg.toString());
	}

	/**
	 * Returns the first working day in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching working day, or <code>null</code> if a matching working day could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public WorkingDay fetchByOrganization_First(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<WorkingDay> list = findByOrganization(organizationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last working day in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching working day
	 * @throws info.diit.portal.NoSuchWorkingDayException if a matching working day could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public WorkingDay findByOrganization_Last(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchWorkingDayException, SystemException {
		WorkingDay workingDay = fetchByOrganization_Last(organizationId,
				orderByComparator);

		if (workingDay != null) {
			return workingDay;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWorkingDayException(msg.toString());
	}

	/**
	 * Returns the last working day in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching working day, or <code>null</code> if a matching working day could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public WorkingDay fetchByOrganization_Last(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByOrganization(organizationId);

		List<WorkingDay> list = findByOrganization(organizationId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the working daies before and after the current working day in the ordered set where organizationId = &#63;.
	 *
	 * @param workingDayId the primary key of the current working day
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next working day
	 * @throws info.diit.portal.NoSuchWorkingDayException if a working day with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public WorkingDay[] findByOrganization_PrevAndNext(long workingDayId,
		long organizationId, OrderByComparator orderByComparator)
		throws NoSuchWorkingDayException, SystemException {
		WorkingDay workingDay = findByPrimaryKey(workingDayId);

		Session session = null;

		try {
			session = openSession();

			WorkingDay[] array = new WorkingDayImpl[3];

			array[0] = getByOrganization_PrevAndNext(session, workingDay,
					organizationId, orderByComparator, true);

			array[1] = workingDay;

			array[2] = getByOrganization_PrevAndNext(session, workingDay,
					organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected WorkingDay getByOrganization_PrevAndNext(Session session,
		WorkingDay workingDay, long organizationId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_WORKINGDAY_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(workingDay);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<WorkingDay> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns the working day where organizationId = &#63; and date = &#63; or throws a {@link info.diit.portal.NoSuchWorkingDayException} if it could not be found.
	 *
	 * @param organizationId the organization ID
	 * @param date the date
	 * @return the matching working day
	 * @throws info.diit.portal.NoSuchWorkingDayException if a matching working day could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public WorkingDay findByOrganizationDate(long organizationId, Date date)
		throws NoSuchWorkingDayException, SystemException {
		WorkingDay workingDay = fetchByOrganizationDate(organizationId, date);

		if (workingDay == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("organizationId=");
			msg.append(organizationId);

			msg.append(", date=");
			msg.append(date);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchWorkingDayException(msg.toString());
		}

		return workingDay;
	}

	/**
	 * Returns the working day where organizationId = &#63; and date = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param organizationId the organization ID
	 * @param date the date
	 * @return the matching working day, or <code>null</code> if a matching working day could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public WorkingDay fetchByOrganizationDate(long organizationId, Date date)
		throws SystemException {
		return fetchByOrganizationDate(organizationId, date, true);
	}

	/**
	 * Returns the working day where organizationId = &#63; and date = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param organizationId the organization ID
	 * @param date the date
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching working day, or <code>null</code> if a matching working day could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public WorkingDay fetchByOrganizationDate(long organizationId, Date date,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { organizationId, date };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_ORGANIZATIONDATE,
					finderArgs, this);
		}

		if (result instanceof WorkingDay) {
			WorkingDay workingDay = (WorkingDay)result;

			if ((organizationId != workingDay.getOrganizationId()) ||
					!Validator.equals(date, workingDay.getDate())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_WORKINGDAY_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATIONDATE_ORGANIZATIONID_2);

			if (date == null) {
				query.append(_FINDER_COLUMN_ORGANIZATIONDATE_DATE_1);
			}
			else {
				query.append(_FINDER_COLUMN_ORGANIZATIONDATE_DATE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				if (date != null) {
					qPos.add(CalendarUtil.getTimestamp(date));
				}

				List<WorkingDay> list = q.list();

				result = list;

				WorkingDay workingDay = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATIONDATE,
						finderArgs, list);
				}
				else {
					workingDay = list.get(0);

					cacheResult(workingDay);

					if ((workingDay.getOrganizationId() != organizationId) ||
							(workingDay.getDate() == null) ||
							!workingDay.getDate().equals(date)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATIONDATE,
							finderArgs, workingDay);
					}
				}

				return workingDay;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ORGANIZATIONDATE,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (WorkingDay)result;
			}
		}
	}

	/**
	 * Returns all the working daies.
	 *
	 * @return the working daies
	 * @throws SystemException if a system exception occurred
	 */
	public List<WorkingDay> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the working daies.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of working daies
	 * @param end the upper bound of the range of working daies (not inclusive)
	 * @return the range of working daies
	 * @throws SystemException if a system exception occurred
	 */
	public List<WorkingDay> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the working daies.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of working daies
	 * @param end the upper bound of the range of working daies (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of working daies
	 * @throws SystemException if a system exception occurred
	 */
	public List<WorkingDay> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<WorkingDay> list = (List<WorkingDay>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_WORKINGDAY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_WORKINGDAY;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<WorkingDay>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<WorkingDay>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the working daies where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByOrganization(long organizationId)
		throws SystemException {
		for (WorkingDay workingDay : findByOrganization(organizationId)) {
			remove(workingDay);
		}
	}

	/**
	 * Removes the working day where organizationId = &#63; and date = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @param date the date
	 * @return the working day that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public WorkingDay removeByOrganizationDate(long organizationId, Date date)
		throws NoSuchWorkingDayException, SystemException {
		WorkingDay workingDay = findByOrganizationDate(organizationId, date);

		return remove(workingDay);
	}

	/**
	 * Removes all the working daies from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (WorkingDay workingDay : findAll()) {
			remove(workingDay);
		}
	}

	/**
	 * Returns the number of working daies where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching working daies
	 * @throws SystemException if a system exception occurred
	 */
	public int countByOrganization(long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_WORKINGDAY_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of working daies where organizationId = &#63; and date = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param date the date
	 * @return the number of matching working daies
	 * @throws SystemException if a system exception occurred
	 */
	public int countByOrganizationDate(long organizationId, Date date)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId, date };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATIONDATE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_WORKINGDAY_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATIONDATE_ORGANIZATIONID_2);

			if (date == null) {
				query.append(_FINDER_COLUMN_ORGANIZATIONDATE_DATE_1);
			}
			else {
				query.append(_FINDER_COLUMN_ORGANIZATIONDATE_DATE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				if (date != null) {
					qPos.add(CalendarUtil.getTimestamp(date));
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATIONDATE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of working daies.
	 *
	 * @return the number of working daies
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_WORKINGDAY);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the working day persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.WorkingDay")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<WorkingDay>> listenersList = new ArrayList<ModelListener<WorkingDay>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<WorkingDay>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(WorkingDayImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttendanceCorrectionPersistence.class)
	protected AttendanceCorrectionPersistence attendanceCorrectionPersistence;
	@BeanReference(type = DayTypePersistence.class)
	protected DayTypePersistence dayTypePersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = WorkingDayPersistence.class)
	protected WorkingDayPersistence workingDayPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_WORKINGDAY = "SELECT workingDay FROM WorkingDay workingDay";
	private static final String _SQL_SELECT_WORKINGDAY_WHERE = "SELECT workingDay FROM WorkingDay workingDay WHERE ";
	private static final String _SQL_COUNT_WORKINGDAY = "SELECT COUNT(workingDay) FROM WorkingDay workingDay";
	private static final String _SQL_COUNT_WORKINGDAY_WHERE = "SELECT COUNT(workingDay) FROM WorkingDay workingDay WHERE ";
	private static final String _FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2 = "workingDay.organizationId = ?";
	private static final String _FINDER_COLUMN_ORGANIZATIONDATE_ORGANIZATIONID_2 =
		"workingDay.organizationId = ? AND ";
	private static final String _FINDER_COLUMN_ORGANIZATIONDATE_DATE_1 = "workingDay.date IS NULL";
	private static final String _FINDER_COLUMN_ORGANIZATIONDATE_DATE_2 = "workingDay.date = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "workingDay.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No WorkingDay exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No WorkingDay exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(WorkingDayPersistenceImpl.class);
	private static WorkingDay _nullWorkingDay = new WorkingDayImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<WorkingDay> toCacheModel() {
				return _nullWorkingDayCacheModel;
			}
		};

	private static CacheModel<WorkingDay> _nullWorkingDayCacheModel = new CacheModel<WorkingDay>() {
			public WorkingDay toEntityModel() {
				return _nullWorkingDay;
			}
		};
}
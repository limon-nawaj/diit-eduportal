/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchLeaveCategoryException;
import info.diit.portal.model.LeaveCategory;
import info.diit.portal.model.impl.LeaveCategoryImpl;
import info.diit.portal.model.impl.LeaveCategoryModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the leave category service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see LeaveCategoryPersistence
 * @see LeaveCategoryUtil
 * @generated
 */
public class LeaveCategoryPersistenceImpl extends BasePersistenceImpl<LeaveCategory>
	implements LeaveCategoryPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LeaveCategoryUtil} to access the leave category persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LeaveCategoryImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(LeaveCategoryModelImpl.ENTITY_CACHE_ENABLED,
			LeaveCategoryModelImpl.FINDER_CACHE_ENABLED,
			LeaveCategoryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByOrganization",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(LeaveCategoryModelImpl.ENTITY_CACHE_ENABLED,
			LeaveCategoryModelImpl.FINDER_CACHE_ENABLED,
			LeaveCategoryImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByOrganization", new String[] { Long.class.getName() },
			LeaveCategoryModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATION = new FinderPath(LeaveCategoryModelImpl.ENTITY_CACHE_ENABLED,
			LeaveCategoryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOrganization",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LeaveCategoryModelImpl.ENTITY_CACHE_ENABLED,
			LeaveCategoryModelImpl.FINDER_CACHE_ENABLED,
			LeaveCategoryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LeaveCategoryModelImpl.ENTITY_CACHE_ENABLED,
			LeaveCategoryModelImpl.FINDER_CACHE_ENABLED,
			LeaveCategoryImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LeaveCategoryModelImpl.ENTITY_CACHE_ENABLED,
			LeaveCategoryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the leave category in the entity cache if it is enabled.
	 *
	 * @param leaveCategory the leave category
	 */
	public void cacheResult(LeaveCategory leaveCategory) {
		EntityCacheUtil.putResult(LeaveCategoryModelImpl.ENTITY_CACHE_ENABLED,
			LeaveCategoryImpl.class, leaveCategory.getPrimaryKey(),
			leaveCategory);

		leaveCategory.resetOriginalValues();
	}

	/**
	 * Caches the leave categories in the entity cache if it is enabled.
	 *
	 * @param leaveCategories the leave categories
	 */
	public void cacheResult(List<LeaveCategory> leaveCategories) {
		for (LeaveCategory leaveCategory : leaveCategories) {
			if (EntityCacheUtil.getResult(
						LeaveCategoryModelImpl.ENTITY_CACHE_ENABLED,
						LeaveCategoryImpl.class, leaveCategory.getPrimaryKey()) == null) {
				cacheResult(leaveCategory);
			}
			else {
				leaveCategory.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all leave categories.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(LeaveCategoryImpl.class.getName());
		}

		EntityCacheUtil.clearCache(LeaveCategoryImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the leave category.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LeaveCategory leaveCategory) {
		EntityCacheUtil.removeResult(LeaveCategoryModelImpl.ENTITY_CACHE_ENABLED,
			LeaveCategoryImpl.class, leaveCategory.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<LeaveCategory> leaveCategories) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LeaveCategory leaveCategory : leaveCategories) {
			EntityCacheUtil.removeResult(LeaveCategoryModelImpl.ENTITY_CACHE_ENABLED,
				LeaveCategoryImpl.class, leaveCategory.getPrimaryKey());
		}
	}

	/**
	 * Creates a new leave category with the primary key. Does not add the leave category to the database.
	 *
	 * @param leaveCategoryId the primary key for the new leave category
	 * @return the new leave category
	 */
	public LeaveCategory create(long leaveCategoryId) {
		LeaveCategory leaveCategory = new LeaveCategoryImpl();

		leaveCategory.setNew(true);
		leaveCategory.setPrimaryKey(leaveCategoryId);

		return leaveCategory;
	}

	/**
	 * Removes the leave category with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param leaveCategoryId the primary key of the leave category
	 * @return the leave category that was removed
	 * @throws info.diit.portal.NoSuchLeaveCategoryException if a leave category with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveCategory remove(long leaveCategoryId)
		throws NoSuchLeaveCategoryException, SystemException {
		return remove(Long.valueOf(leaveCategoryId));
	}

	/**
	 * Removes the leave category with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the leave category
	 * @return the leave category that was removed
	 * @throws info.diit.portal.NoSuchLeaveCategoryException if a leave category with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LeaveCategory remove(Serializable primaryKey)
		throws NoSuchLeaveCategoryException, SystemException {
		Session session = null;

		try {
			session = openSession();

			LeaveCategory leaveCategory = (LeaveCategory)session.get(LeaveCategoryImpl.class,
					primaryKey);

			if (leaveCategory == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLeaveCategoryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(leaveCategory);
		}
		catch (NoSuchLeaveCategoryException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LeaveCategory removeImpl(LeaveCategory leaveCategory)
		throws SystemException {
		leaveCategory = toUnwrappedModel(leaveCategory);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, leaveCategory);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(leaveCategory);

		return leaveCategory;
	}

	@Override
	public LeaveCategory updateImpl(
		info.diit.portal.model.LeaveCategory leaveCategory, boolean merge)
		throws SystemException {
		leaveCategory = toUnwrappedModel(leaveCategory);

		boolean isNew = leaveCategory.isNew();

		LeaveCategoryModelImpl leaveCategoryModelImpl = (LeaveCategoryModelImpl)leaveCategory;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, leaveCategory, merge);

			leaveCategory.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !LeaveCategoryModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((leaveCategoryModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(leaveCategoryModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(leaveCategoryModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);
			}
		}

		EntityCacheUtil.putResult(LeaveCategoryModelImpl.ENTITY_CACHE_ENABLED,
			LeaveCategoryImpl.class, leaveCategory.getPrimaryKey(),
			leaveCategory);

		return leaveCategory;
	}

	protected LeaveCategory toUnwrappedModel(LeaveCategory leaveCategory) {
		if (leaveCategory instanceof LeaveCategoryImpl) {
			return leaveCategory;
		}

		LeaveCategoryImpl leaveCategoryImpl = new LeaveCategoryImpl();

		leaveCategoryImpl.setNew(leaveCategory.isNew());
		leaveCategoryImpl.setPrimaryKey(leaveCategory.getPrimaryKey());

		leaveCategoryImpl.setLeaveCategoryId(leaveCategory.getLeaveCategoryId());
		leaveCategoryImpl.setCompanyId(leaveCategory.getCompanyId());
		leaveCategoryImpl.setOrganizationId(leaveCategory.getOrganizationId());
		leaveCategoryImpl.setUserId(leaveCategory.getUserId());
		leaveCategoryImpl.setUserName(leaveCategory.getUserName());
		leaveCategoryImpl.setCreateDate(leaveCategory.getCreateDate());
		leaveCategoryImpl.setModifiedDate(leaveCategory.getModifiedDate());
		leaveCategoryImpl.setTitle(leaveCategory.getTitle());
		leaveCategoryImpl.setTotalLeave(leaveCategory.getTotalLeave());

		return leaveCategoryImpl;
	}

	/**
	 * Returns the leave category with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the leave category
	 * @return the leave category
	 * @throws com.liferay.portal.NoSuchModelException if a leave category with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LeaveCategory findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the leave category with the primary key or throws a {@link info.diit.portal.NoSuchLeaveCategoryException} if it could not be found.
	 *
	 * @param leaveCategoryId the primary key of the leave category
	 * @return the leave category
	 * @throws info.diit.portal.NoSuchLeaveCategoryException if a leave category with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveCategory findByPrimaryKey(long leaveCategoryId)
		throws NoSuchLeaveCategoryException, SystemException {
		LeaveCategory leaveCategory = fetchByPrimaryKey(leaveCategoryId);

		if (leaveCategory == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + leaveCategoryId);
			}

			throw new NoSuchLeaveCategoryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				leaveCategoryId);
		}

		return leaveCategory;
	}

	/**
	 * Returns the leave category with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the leave category
	 * @return the leave category, or <code>null</code> if a leave category with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LeaveCategory fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the leave category with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param leaveCategoryId the primary key of the leave category
	 * @return the leave category, or <code>null</code> if a leave category with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveCategory fetchByPrimaryKey(long leaveCategoryId)
		throws SystemException {
		LeaveCategory leaveCategory = (LeaveCategory)EntityCacheUtil.getResult(LeaveCategoryModelImpl.ENTITY_CACHE_ENABLED,
				LeaveCategoryImpl.class, leaveCategoryId);

		if (leaveCategory == _nullLeaveCategory) {
			return null;
		}

		if (leaveCategory == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				leaveCategory = (LeaveCategory)session.get(LeaveCategoryImpl.class,
						Long.valueOf(leaveCategoryId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (leaveCategory != null) {
					cacheResult(leaveCategory);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(LeaveCategoryModelImpl.ENTITY_CACHE_ENABLED,
						LeaveCategoryImpl.class, leaveCategoryId,
						_nullLeaveCategory);
				}

				closeSession(session);
			}
		}

		return leaveCategory;
	}

	/**
	 * Returns all the leave categories where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching leave categories
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveCategory> findByOrganization(long organizationId)
		throws SystemException {
		return findByOrganization(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the leave categories where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of leave categories
	 * @param end the upper bound of the range of leave categories (not inclusive)
	 * @return the range of matching leave categories
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveCategory> findByOrganization(long organizationId,
		int start, int end) throws SystemException {
		return findByOrganization(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the leave categories where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of leave categories
	 * @param end the upper bound of the range of leave categories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching leave categories
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveCategory> findByOrganization(long organizationId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<LeaveCategory> list = (List<LeaveCategory>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (LeaveCategory leaveCategory : list) {
				if ((organizationId != leaveCategory.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_LEAVECATEGORY_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				list = (List<LeaveCategory>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first leave category in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching leave category
	 * @throws info.diit.portal.NoSuchLeaveCategoryException if a matching leave category could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveCategory findByOrganization_First(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveCategoryException, SystemException {
		LeaveCategory leaveCategory = fetchByOrganization_First(organizationId,
				orderByComparator);

		if (leaveCategory != null) {
			return leaveCategory;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLeaveCategoryException(msg.toString());
	}

	/**
	 * Returns the first leave category in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching leave category, or <code>null</code> if a matching leave category could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveCategory fetchByOrganization_First(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<LeaveCategory> list = findByOrganization(organizationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last leave category in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching leave category
	 * @throws info.diit.portal.NoSuchLeaveCategoryException if a matching leave category could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveCategory findByOrganization_Last(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveCategoryException, SystemException {
		LeaveCategory leaveCategory = fetchByOrganization_Last(organizationId,
				orderByComparator);

		if (leaveCategory != null) {
			return leaveCategory;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLeaveCategoryException(msg.toString());
	}

	/**
	 * Returns the last leave category in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching leave category, or <code>null</code> if a matching leave category could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveCategory fetchByOrganization_Last(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByOrganization(organizationId);

		List<LeaveCategory> list = findByOrganization(organizationId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the leave categories before and after the current leave category in the ordered set where organizationId = &#63;.
	 *
	 * @param leaveCategoryId the primary key of the current leave category
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next leave category
	 * @throws info.diit.portal.NoSuchLeaveCategoryException if a leave category with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveCategory[] findByOrganization_PrevAndNext(
		long leaveCategoryId, long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveCategoryException, SystemException {
		LeaveCategory leaveCategory = findByPrimaryKey(leaveCategoryId);

		Session session = null;

		try {
			session = openSession();

			LeaveCategory[] array = new LeaveCategoryImpl[3];

			array[0] = getByOrganization_PrevAndNext(session, leaveCategory,
					organizationId, orderByComparator, true);

			array[1] = leaveCategory;

			array[2] = getByOrganization_PrevAndNext(session, leaveCategory,
					organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LeaveCategory getByOrganization_PrevAndNext(Session session,
		LeaveCategory leaveCategory, long organizationId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LEAVECATEGORY_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(leaveCategory);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LeaveCategory> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the leave categories.
	 *
	 * @return the leave categories
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveCategory> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the leave categories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of leave categories
	 * @param end the upper bound of the range of leave categories (not inclusive)
	 * @return the range of leave categories
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveCategory> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the leave categories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of leave categories
	 * @param end the upper bound of the range of leave categories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of leave categories
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveCategory> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LeaveCategory> list = (List<LeaveCategory>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LEAVECATEGORY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LEAVECATEGORY;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<LeaveCategory>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<LeaveCategory>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the leave categories where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByOrganization(long organizationId)
		throws SystemException {
		for (LeaveCategory leaveCategory : findByOrganization(organizationId)) {
			remove(leaveCategory);
		}
	}

	/**
	 * Removes all the leave categories from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (LeaveCategory leaveCategory : findAll()) {
			remove(leaveCategory);
		}
	}

	/**
	 * Returns the number of leave categories where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching leave categories
	 * @throws SystemException if a system exception occurred
	 */
	public int countByOrganization(long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LEAVECATEGORY_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of leave categories.
	 *
	 * @return the number of leave categories
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LEAVECATEGORY);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the leave category persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.LeaveCategory")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<LeaveCategory>> listenersList = new ArrayList<ModelListener<LeaveCategory>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<LeaveCategory>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(LeaveCategoryImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttendanceCorrectionPersistence.class)
	protected AttendanceCorrectionPersistence attendanceCorrectionPersistence;
	@BeanReference(type = DayTypePersistence.class)
	protected DayTypePersistence dayTypePersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = WorkingDayPersistence.class)
	protected WorkingDayPersistence workingDayPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_LEAVECATEGORY = "SELECT leaveCategory FROM LeaveCategory leaveCategory";
	private static final String _SQL_SELECT_LEAVECATEGORY_WHERE = "SELECT leaveCategory FROM LeaveCategory leaveCategory WHERE ";
	private static final String _SQL_COUNT_LEAVECATEGORY = "SELECT COUNT(leaveCategory) FROM LeaveCategory leaveCategory";
	private static final String _SQL_COUNT_LEAVECATEGORY_WHERE = "SELECT COUNT(leaveCategory) FROM LeaveCategory leaveCategory WHERE ";
	private static final String _FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2 = "leaveCategory.organizationId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "leaveCategory.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LeaveCategory exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No LeaveCategory exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(LeaveCategoryPersistenceImpl.class);
	private static LeaveCategory _nullLeaveCategory = new LeaveCategoryImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<LeaveCategory> toCacheModel() {
				return _nullLeaveCategoryCacheModel;
			}
		};

	private static CacheModel<LeaveCategory> _nullLeaveCategoryCacheModel = new CacheModel<LeaveCategory>() {
			public LeaveCategory toEntityModel() {
				return _nullLeaveCategory;
			}
		};
}
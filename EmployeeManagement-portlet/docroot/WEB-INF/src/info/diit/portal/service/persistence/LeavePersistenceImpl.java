/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchLeaveException;
import info.diit.portal.model.Leave;
import info.diit.portal.model.impl.LeaveImpl;
import info.diit.portal.model.impl.LeaveModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the leave service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see LeavePersistence
 * @see LeaveUtil
 * @generated
 */
public class LeavePersistenceImpl extends BasePersistenceImpl<Leave>
	implements LeavePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LeaveUtil} to access the leave persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LeaveImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveModelImpl.FINDER_CACHE_ENABLED, LeaveImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOrganization",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveModelImpl.FINDER_CACHE_ENABLED, LeaveImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByOrganization",
			new String[] { Long.class.getName() },
			LeaveModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATION = new FinderPath(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOrganization",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEE = new FinderPath(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveModelImpl.FINDER_CACHE_ENABLED, LeaveImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByEmployee",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE =
		new FinderPath(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveModelImpl.FINDER_CACHE_ENABLED, LeaveImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByEmployee",
			new String[] { Long.class.getName() },
			LeaveModelImpl.EMPLOYEE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMPLOYEE = new FinderPath(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEmployee",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_RESPONSIBLEEMPLOYEE =
		new FinderPath(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveModelImpl.FINDER_CACHE_ENABLED, LeaveImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByResponsibleEmployee",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESPONSIBLEEMPLOYEE =
		new FinderPath(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveModelImpl.FINDER_CACHE_ENABLED, LeaveImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByResponsibleEmployee", new String[] { Long.class.getName() },
			LeaveModelImpl.RESPONSIBLEEMPLOYEE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_RESPONSIBLEEMPLOYEE = new FinderPath(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByResponsibleEmployee", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveModelImpl.FINDER_CACHE_ENABLED, LeaveImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveModelImpl.FINDER_CACHE_ENABLED, LeaveImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the leave in the entity cache if it is enabled.
	 *
	 * @param leave the leave
	 */
	public void cacheResult(Leave leave) {
		EntityCacheUtil.putResult(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveImpl.class, leave.getPrimaryKey(), leave);

		leave.resetOriginalValues();
	}

	/**
	 * Caches the leaves in the entity cache if it is enabled.
	 *
	 * @param leaves the leaves
	 */
	public void cacheResult(List<Leave> leaves) {
		for (Leave leave : leaves) {
			if (EntityCacheUtil.getResult(LeaveModelImpl.ENTITY_CACHE_ENABLED,
						LeaveImpl.class, leave.getPrimaryKey()) == null) {
				cacheResult(leave);
			}
			else {
				leave.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all leaves.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(LeaveImpl.class.getName());
		}

		EntityCacheUtil.clearCache(LeaveImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the leave.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Leave leave) {
		EntityCacheUtil.removeResult(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveImpl.class, leave.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Leave> leaves) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Leave leave : leaves) {
			EntityCacheUtil.removeResult(LeaveModelImpl.ENTITY_CACHE_ENABLED,
				LeaveImpl.class, leave.getPrimaryKey());
		}
	}

	/**
	 * Creates a new leave with the primary key. Does not add the leave to the database.
	 *
	 * @param leaveId the primary key for the new leave
	 * @return the new leave
	 */
	public Leave create(long leaveId) {
		Leave leave = new LeaveImpl();

		leave.setNew(true);
		leave.setPrimaryKey(leaveId);

		return leave;
	}

	/**
	 * Removes the leave with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param leaveId the primary key of the leave
	 * @return the leave that was removed
	 * @throws info.diit.portal.NoSuchLeaveException if a leave with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave remove(long leaveId)
		throws NoSuchLeaveException, SystemException {
		return remove(Long.valueOf(leaveId));
	}

	/**
	 * Removes the leave with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the leave
	 * @return the leave that was removed
	 * @throws info.diit.portal.NoSuchLeaveException if a leave with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Leave remove(Serializable primaryKey)
		throws NoSuchLeaveException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Leave leave = (Leave)session.get(LeaveImpl.class, primaryKey);

			if (leave == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLeaveException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(leave);
		}
		catch (NoSuchLeaveException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Leave removeImpl(Leave leave) throws SystemException {
		leave = toUnwrappedModel(leave);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, leave);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(leave);

		return leave;
	}

	@Override
	public Leave updateImpl(info.diit.portal.model.Leave leave, boolean merge)
		throws SystemException {
		leave = toUnwrappedModel(leave);

		boolean isNew = leave.isNew();

		LeaveModelImpl leaveModelImpl = (LeaveModelImpl)leave;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, leave, merge);

			leave.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !LeaveModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((leaveModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(leaveModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(leaveModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);
			}

			if ((leaveModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(leaveModelImpl.getOriginalEmployee())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE,
					args);

				args = new Object[] { Long.valueOf(leaveModelImpl.getEmployee()) };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE,
					args);
			}

			if ((leaveModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESPONSIBLEEMPLOYEE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(leaveModelImpl.getOriginalResponsibleEmployee())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RESPONSIBLEEMPLOYEE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESPONSIBLEEMPLOYEE,
					args);

				args = new Object[] {
						Long.valueOf(leaveModelImpl.getResponsibleEmployee())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RESPONSIBLEEMPLOYEE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESPONSIBLEEMPLOYEE,
					args);
			}
		}

		EntityCacheUtil.putResult(LeaveModelImpl.ENTITY_CACHE_ENABLED,
			LeaveImpl.class, leave.getPrimaryKey(), leave);

		return leave;
	}

	protected Leave toUnwrappedModel(Leave leave) {
		if (leave instanceof LeaveImpl) {
			return leave;
		}

		LeaveImpl leaveImpl = new LeaveImpl();

		leaveImpl.setNew(leave.isNew());
		leaveImpl.setPrimaryKey(leave.getPrimaryKey());

		leaveImpl.setLeaveId(leave.getLeaveId());
		leaveImpl.setCompanyId(leave.getCompanyId());
		leaveImpl.setOrganizationId(leave.getOrganizationId());
		leaveImpl.setUserId(leave.getUserId());
		leaveImpl.setUserName(leave.getUserName());
		leaveImpl.setCreateDate(leave.getCreateDate());
		leaveImpl.setModifiedDate(leave.getModifiedDate());
		leaveImpl.setApplicationDate(leave.getApplicationDate());
		leaveImpl.setEmployee(leave.getEmployee());
		leaveImpl.setResponsibleEmployee(leave.getResponsibleEmployee());
		leaveImpl.setStartDate(leave.getStartDate());
		leaveImpl.setEndDate(leave.getEndDate());
		leaveImpl.setNumberOfDay(leave.getNumberOfDay());
		leaveImpl.setPhoneNumber(leave.getPhoneNumber());
		leaveImpl.setCauseOfLeave(leave.getCauseOfLeave());
		leaveImpl.setWhereEnjoy(leave.getWhereEnjoy());
		leaveImpl.setFirstRecommendation(leave.getFirstRecommendation());
		leaveImpl.setSecondRecommendation(leave.getSecondRecommendation());
		leaveImpl.setLeaveCategory(leave.getLeaveCategory());
		leaveImpl.setResponsibleEmployeeStatus(leave.getResponsibleEmployeeStatus());
		leaveImpl.setApplicationStatus(leave.getApplicationStatus());
		leaveImpl.setComments(leave.getComments());
		leaveImpl.setComplementedBy(leave.getComplementedBy());

		return leaveImpl;
	}

	/**
	 * Returns the leave with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the leave
	 * @return the leave
	 * @throws com.liferay.portal.NoSuchModelException if a leave with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Leave findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the leave with the primary key or throws a {@link info.diit.portal.NoSuchLeaveException} if it could not be found.
	 *
	 * @param leaveId the primary key of the leave
	 * @return the leave
	 * @throws info.diit.portal.NoSuchLeaveException if a leave with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave findByPrimaryKey(long leaveId)
		throws NoSuchLeaveException, SystemException {
		Leave leave = fetchByPrimaryKey(leaveId);

		if (leave == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + leaveId);
			}

			throw new NoSuchLeaveException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				leaveId);
		}

		return leave;
	}

	/**
	 * Returns the leave with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the leave
	 * @return the leave, or <code>null</code> if a leave with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Leave fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the leave with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param leaveId the primary key of the leave
	 * @return the leave, or <code>null</code> if a leave with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave fetchByPrimaryKey(long leaveId) throws SystemException {
		Leave leave = (Leave)EntityCacheUtil.getResult(LeaveModelImpl.ENTITY_CACHE_ENABLED,
				LeaveImpl.class, leaveId);

		if (leave == _nullLeave) {
			return null;
		}

		if (leave == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				leave = (Leave)session.get(LeaveImpl.class,
						Long.valueOf(leaveId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (leave != null) {
					cacheResult(leave);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(LeaveModelImpl.ENTITY_CACHE_ENABLED,
						LeaveImpl.class, leaveId, _nullLeave);
				}

				closeSession(session);
			}
		}

		return leave;
	}

	/**
	 * Returns all the leaves where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching leaves
	 * @throws SystemException if a system exception occurred
	 */
	public List<Leave> findByOrganization(long organizationId)
		throws SystemException {
		return findByOrganization(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the leaves where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of leaves
	 * @param end the upper bound of the range of leaves (not inclusive)
	 * @return the range of matching leaves
	 * @throws SystemException if a system exception occurred
	 */
	public List<Leave> findByOrganization(long organizationId, int start,
		int end) throws SystemException {
		return findByOrganization(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the leaves where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of leaves
	 * @param end the upper bound of the range of leaves (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching leaves
	 * @throws SystemException if a system exception occurred
	 */
	public List<Leave> findByOrganization(long organizationId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<Leave> list = (List<Leave>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Leave leave : list) {
				if ((organizationId != leave.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_LEAVE_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				list = (List<Leave>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first leave in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching leave
	 * @throws info.diit.portal.NoSuchLeaveException if a matching leave could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave findByOrganization_First(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveException, SystemException {
		Leave leave = fetchByOrganization_First(organizationId,
				orderByComparator);

		if (leave != null) {
			return leave;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLeaveException(msg.toString());
	}

	/**
	 * Returns the first leave in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching leave, or <code>null</code> if a matching leave could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave fetchByOrganization_First(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Leave> list = findByOrganization(organizationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last leave in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching leave
	 * @throws info.diit.portal.NoSuchLeaveException if a matching leave could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave findByOrganization_Last(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveException, SystemException {
		Leave leave = fetchByOrganization_Last(organizationId, orderByComparator);

		if (leave != null) {
			return leave;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLeaveException(msg.toString());
	}

	/**
	 * Returns the last leave in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching leave, or <code>null</code> if a matching leave could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave fetchByOrganization_Last(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByOrganization(organizationId);

		List<Leave> list = findByOrganization(organizationId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the leaves before and after the current leave in the ordered set where organizationId = &#63;.
	 *
	 * @param leaveId the primary key of the current leave
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next leave
	 * @throws info.diit.portal.NoSuchLeaveException if a leave with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave[] findByOrganization_PrevAndNext(long leaveId,
		long organizationId, OrderByComparator orderByComparator)
		throws NoSuchLeaveException, SystemException {
		Leave leave = findByPrimaryKey(leaveId);

		Session session = null;

		try {
			session = openSession();

			Leave[] array = new LeaveImpl[3];

			array[0] = getByOrganization_PrevAndNext(session, leave,
					organizationId, orderByComparator, true);

			array[1] = leave;

			array[2] = getByOrganization_PrevAndNext(session, leave,
					organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Leave getByOrganization_PrevAndNext(Session session, Leave leave,
		long organizationId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LEAVE_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(leave);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Leave> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the leaves where employee = &#63;.
	 *
	 * @param employee the employee
	 * @return the matching leaves
	 * @throws SystemException if a system exception occurred
	 */
	public List<Leave> findByEmployee(long employee) throws SystemException {
		return findByEmployee(employee, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the leaves where employee = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employee the employee
	 * @param start the lower bound of the range of leaves
	 * @param end the upper bound of the range of leaves (not inclusive)
	 * @return the range of matching leaves
	 * @throws SystemException if a system exception occurred
	 */
	public List<Leave> findByEmployee(long employee, int start, int end)
		throws SystemException {
		return findByEmployee(employee, start, end, null);
	}

	/**
	 * Returns an ordered range of all the leaves where employee = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employee the employee
	 * @param start the lower bound of the range of leaves
	 * @param end the upper bound of the range of leaves (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching leaves
	 * @throws SystemException if a system exception occurred
	 */
	public List<Leave> findByEmployee(long employee, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE;
			finderArgs = new Object[] { employee };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEE;
			finderArgs = new Object[] { employee, start, end, orderByComparator };
		}

		List<Leave> list = (List<Leave>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Leave leave : list) {
				if ((employee != leave.getEmployee())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_LEAVE_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employee);

				list = (List<Leave>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first leave in the ordered set where employee = &#63;.
	 *
	 * @param employee the employee
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching leave
	 * @throws info.diit.portal.NoSuchLeaveException if a matching leave could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave findByEmployee_First(long employee,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveException, SystemException {
		Leave leave = fetchByEmployee_First(employee, orderByComparator);

		if (leave != null) {
			return leave;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employee=");
		msg.append(employee);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLeaveException(msg.toString());
	}

	/**
	 * Returns the first leave in the ordered set where employee = &#63;.
	 *
	 * @param employee the employee
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching leave, or <code>null</code> if a matching leave could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave fetchByEmployee_First(long employee,
		OrderByComparator orderByComparator) throws SystemException {
		List<Leave> list = findByEmployee(employee, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last leave in the ordered set where employee = &#63;.
	 *
	 * @param employee the employee
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching leave
	 * @throws info.diit.portal.NoSuchLeaveException if a matching leave could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave findByEmployee_Last(long employee,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveException, SystemException {
		Leave leave = fetchByEmployee_Last(employee, orderByComparator);

		if (leave != null) {
			return leave;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employee=");
		msg.append(employee);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLeaveException(msg.toString());
	}

	/**
	 * Returns the last leave in the ordered set where employee = &#63;.
	 *
	 * @param employee the employee
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching leave, or <code>null</code> if a matching leave could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave fetchByEmployee_Last(long employee,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByEmployee(employee);

		List<Leave> list = findByEmployee(employee, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the leaves before and after the current leave in the ordered set where employee = &#63;.
	 *
	 * @param leaveId the primary key of the current leave
	 * @param employee the employee
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next leave
	 * @throws info.diit.portal.NoSuchLeaveException if a leave with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave[] findByEmployee_PrevAndNext(long leaveId, long employee,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveException, SystemException {
		Leave leave = findByPrimaryKey(leaveId);

		Session session = null;

		try {
			session = openSession();

			Leave[] array = new LeaveImpl[3];

			array[0] = getByEmployee_PrevAndNext(session, leave, employee,
					orderByComparator, true);

			array[1] = leave;

			array[2] = getByEmployee_PrevAndNext(session, leave, employee,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Leave getByEmployee_PrevAndNext(Session session, Leave leave,
		long employee, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LEAVE_WHERE);

		query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(employee);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(leave);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Leave> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the leaves where responsibleEmployee = &#63;.
	 *
	 * @param responsibleEmployee the responsible employee
	 * @return the matching leaves
	 * @throws SystemException if a system exception occurred
	 */
	public List<Leave> findByResponsibleEmployee(long responsibleEmployee)
		throws SystemException {
		return findByResponsibleEmployee(responsibleEmployee,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the leaves where responsibleEmployee = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param responsibleEmployee the responsible employee
	 * @param start the lower bound of the range of leaves
	 * @param end the upper bound of the range of leaves (not inclusive)
	 * @return the range of matching leaves
	 * @throws SystemException if a system exception occurred
	 */
	public List<Leave> findByResponsibleEmployee(long responsibleEmployee,
		int start, int end) throws SystemException {
		return findByResponsibleEmployee(responsibleEmployee, start, end, null);
	}

	/**
	 * Returns an ordered range of all the leaves where responsibleEmployee = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param responsibleEmployee the responsible employee
	 * @param start the lower bound of the range of leaves
	 * @param end the upper bound of the range of leaves (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching leaves
	 * @throws SystemException if a system exception occurred
	 */
	public List<Leave> findByResponsibleEmployee(long responsibleEmployee,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RESPONSIBLEEMPLOYEE;
			finderArgs = new Object[] { responsibleEmployee };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_RESPONSIBLEEMPLOYEE;
			finderArgs = new Object[] {
					responsibleEmployee,
					
					start, end, orderByComparator
				};
		}

		List<Leave> list = (List<Leave>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Leave leave : list) {
				if ((responsibleEmployee != leave.getResponsibleEmployee())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_LEAVE_WHERE);

			query.append(_FINDER_COLUMN_RESPONSIBLEEMPLOYEE_RESPONSIBLEEMPLOYEE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(responsibleEmployee);

				list = (List<Leave>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first leave in the ordered set where responsibleEmployee = &#63;.
	 *
	 * @param responsibleEmployee the responsible employee
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching leave
	 * @throws info.diit.portal.NoSuchLeaveException if a matching leave could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave findByResponsibleEmployee_First(long responsibleEmployee,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveException, SystemException {
		Leave leave = fetchByResponsibleEmployee_First(responsibleEmployee,
				orderByComparator);

		if (leave != null) {
			return leave;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("responsibleEmployee=");
		msg.append(responsibleEmployee);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLeaveException(msg.toString());
	}

	/**
	 * Returns the first leave in the ordered set where responsibleEmployee = &#63;.
	 *
	 * @param responsibleEmployee the responsible employee
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching leave, or <code>null</code> if a matching leave could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave fetchByResponsibleEmployee_First(long responsibleEmployee,
		OrderByComparator orderByComparator) throws SystemException {
		List<Leave> list = findByResponsibleEmployee(responsibleEmployee, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last leave in the ordered set where responsibleEmployee = &#63;.
	 *
	 * @param responsibleEmployee the responsible employee
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching leave
	 * @throws info.diit.portal.NoSuchLeaveException if a matching leave could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave findByResponsibleEmployee_Last(long responsibleEmployee,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveException, SystemException {
		Leave leave = fetchByResponsibleEmployee_Last(responsibleEmployee,
				orderByComparator);

		if (leave != null) {
			return leave;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("responsibleEmployee=");
		msg.append(responsibleEmployee);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLeaveException(msg.toString());
	}

	/**
	 * Returns the last leave in the ordered set where responsibleEmployee = &#63;.
	 *
	 * @param responsibleEmployee the responsible employee
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching leave, or <code>null</code> if a matching leave could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave fetchByResponsibleEmployee_Last(long responsibleEmployee,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByResponsibleEmployee(responsibleEmployee);

		List<Leave> list = findByResponsibleEmployee(responsibleEmployee,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the leaves before and after the current leave in the ordered set where responsibleEmployee = &#63;.
	 *
	 * @param leaveId the primary key of the current leave
	 * @param responsibleEmployee the responsible employee
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next leave
	 * @throws info.diit.portal.NoSuchLeaveException if a leave with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Leave[] findByResponsibleEmployee_PrevAndNext(long leaveId,
		long responsibleEmployee, OrderByComparator orderByComparator)
		throws NoSuchLeaveException, SystemException {
		Leave leave = findByPrimaryKey(leaveId);

		Session session = null;

		try {
			session = openSession();

			Leave[] array = new LeaveImpl[3];

			array[0] = getByResponsibleEmployee_PrevAndNext(session, leave,
					responsibleEmployee, orderByComparator, true);

			array[1] = leave;

			array[2] = getByResponsibleEmployee_PrevAndNext(session, leave,
					responsibleEmployee, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Leave getByResponsibleEmployee_PrevAndNext(Session session,
		Leave leave, long responsibleEmployee,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LEAVE_WHERE);

		query.append(_FINDER_COLUMN_RESPONSIBLEEMPLOYEE_RESPONSIBLEEMPLOYEE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(responsibleEmployee);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(leave);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Leave> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the leaves.
	 *
	 * @return the leaves
	 * @throws SystemException if a system exception occurred
	 */
	public List<Leave> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the leaves.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of leaves
	 * @param end the upper bound of the range of leaves (not inclusive)
	 * @return the range of leaves
	 * @throws SystemException if a system exception occurred
	 */
	public List<Leave> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the leaves.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of leaves
	 * @param end the upper bound of the range of leaves (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of leaves
	 * @throws SystemException if a system exception occurred
	 */
	public List<Leave> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Leave> list = (List<Leave>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LEAVE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LEAVE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Leave>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Leave>)QueryUtil.list(q, getDialect(), start,
							end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the leaves where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByOrganization(long organizationId)
		throws SystemException {
		for (Leave leave : findByOrganization(organizationId)) {
			remove(leave);
		}
	}

	/**
	 * Removes all the leaves where employee = &#63; from the database.
	 *
	 * @param employee the employee
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByEmployee(long employee) throws SystemException {
		for (Leave leave : findByEmployee(employee)) {
			remove(leave);
		}
	}

	/**
	 * Removes all the leaves where responsibleEmployee = &#63; from the database.
	 *
	 * @param responsibleEmployee the responsible employee
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByResponsibleEmployee(long responsibleEmployee)
		throws SystemException {
		for (Leave leave : findByResponsibleEmployee(responsibleEmployee)) {
			remove(leave);
		}
	}

	/**
	 * Removes all the leaves from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Leave leave : findAll()) {
			remove(leave);
		}
	}

	/**
	 * Returns the number of leaves where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching leaves
	 * @throws SystemException if a system exception occurred
	 */
	public int countByOrganization(long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LEAVE_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of leaves where employee = &#63;.
	 *
	 * @param employee the employee
	 * @return the number of matching leaves
	 * @throws SystemException if a system exception occurred
	 */
	public int countByEmployee(long employee) throws SystemException {
		Object[] finderArgs = new Object[] { employee };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_EMPLOYEE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LEAVE_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employee);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EMPLOYEE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of leaves where responsibleEmployee = &#63;.
	 *
	 * @param responsibleEmployee the responsible employee
	 * @return the number of matching leaves
	 * @throws SystemException if a system exception occurred
	 */
	public int countByResponsibleEmployee(long responsibleEmployee)
		throws SystemException {
		Object[] finderArgs = new Object[] { responsibleEmployee };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_RESPONSIBLEEMPLOYEE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LEAVE_WHERE);

			query.append(_FINDER_COLUMN_RESPONSIBLEEMPLOYEE_RESPONSIBLEEMPLOYEE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(responsibleEmployee);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_RESPONSIBLEEMPLOYEE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of leaves.
	 *
	 * @return the number of leaves
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LEAVE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the leave persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.Leave")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Leave>> listenersList = new ArrayList<ModelListener<Leave>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Leave>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(LeaveImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttendanceCorrectionPersistence.class)
	protected AttendanceCorrectionPersistence attendanceCorrectionPersistence;
	@BeanReference(type = DayTypePersistence.class)
	protected DayTypePersistence dayTypePersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = WorkingDayPersistence.class)
	protected WorkingDayPersistence workingDayPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_LEAVE = "SELECT leave FROM Leave leave";
	private static final String _SQL_SELECT_LEAVE_WHERE = "SELECT leave FROM Leave leave WHERE ";
	private static final String _SQL_COUNT_LEAVE = "SELECT COUNT(leave) FROM Leave leave";
	private static final String _SQL_COUNT_LEAVE_WHERE = "SELECT COUNT(leave) FROM Leave leave WHERE ";
	private static final String _FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2 = "leave.organizationId = ?";
	private static final String _FINDER_COLUMN_EMPLOYEE_EMPLOYEE_2 = "leave.employee = ?";
	private static final String _FINDER_COLUMN_RESPONSIBLEEMPLOYEE_RESPONSIBLEEMPLOYEE_2 =
		"leave.responsibleEmployee = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "leave.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Leave exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Leave exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(LeavePersistenceImpl.class);
	private static Leave _nullLeave = new LeaveImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Leave> toCacheModel() {
				return _nullLeaveCacheModel;
			}
		};

	private static CacheModel<Leave> _nullLeaveCacheModel = new CacheModel<Leave>() {
			public Leave toEntityModel() {
				return _nullLeave;
			}
		};
}
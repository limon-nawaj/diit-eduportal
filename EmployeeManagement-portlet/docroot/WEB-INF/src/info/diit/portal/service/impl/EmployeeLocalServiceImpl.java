/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.impl;

import info.diit.portal.NoSuchEmployeeException;
import info.diit.portal.model.Employee;
import info.diit.portal.service.base.EmployeeLocalServiceBaseImpl;
import info.diit.portal.service.persistence.EmployeeUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the employee local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.service.EmployeeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see info.diit.portal.service.base.EmployeeLocalServiceBaseImpl
 * @see info.diit.portal.service.EmployeeLocalServiceUtil
 */
public class EmployeeLocalServiceImpl extends EmployeeLocalServiceBaseImpl {
	public List<Employee> findByCompany(long companyId) throws SystemException{
		return EmployeeUtil.findByCompany(companyId);
	}
	
	public Employee findByUserId(long userId) throws NoSuchEmployeeException, SystemException{
		return EmployeeUtil.findByUserId(userId);
	}
	
	public List<Employee> findByCompanyOrganization(long companyId, long organizationId) throws SystemException{
		return EmployeeUtil.findByCompanyOrganization(companyId, organizationId);
	}
	
	public List<Employee> findBySupervisor(long employeeId) throws SystemException{
		return EmployeeUtil.findBySupervisor(employeeId);
	}
	
	public List<Employee> findByOrganization(long organizationId) throws SystemException{
		return EmployeeUtil.findByOrganization(organizationId);
	}
}
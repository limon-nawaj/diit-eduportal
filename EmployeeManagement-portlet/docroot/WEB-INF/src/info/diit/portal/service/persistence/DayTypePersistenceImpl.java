/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchDayTypeException;
import info.diit.portal.model.DayType;
import info.diit.portal.model.impl.DayTypeImpl;
import info.diit.portal.model.impl.DayTypeModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the day type service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see DayTypePersistence
 * @see DayTypeUtil
 * @generated
 */
public class DayTypePersistenceImpl extends BasePersistenceImpl<DayType>
	implements DayTypePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link DayTypeUtil} to access the day type persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = DayTypeImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(DayTypeModelImpl.ENTITY_CACHE_ENABLED,
			DayTypeModelImpl.FINDER_CACHE_ENABLED, DayTypeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOrganization",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(DayTypeModelImpl.ENTITY_CACHE_ENABLED,
			DayTypeModelImpl.FINDER_CACHE_ENABLED, DayTypeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByOrganization",
			new String[] { Long.class.getName() },
			DayTypeModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATION = new FinderPath(DayTypeModelImpl.ENTITY_CACHE_ENABLED,
			DayTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOrganization",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FETCH_BY_TYPEORGANIZATION = new FinderPath(DayTypeModelImpl.ENTITY_CACHE_ENABLED,
			DayTypeModelImpl.FINDER_CACHE_ENABLED, DayTypeImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByTypeOrganization",
			new String[] { String.class.getName(), Long.class.getName() },
			DayTypeModelImpl.TYPE_COLUMN_BITMASK |
			DayTypeModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TYPEORGANIZATION = new FinderPath(DayTypeModelImpl.ENTITY_CACHE_ENABLED,
			DayTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByTypeOrganization",
			new String[] { String.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DayTypeModelImpl.ENTITY_CACHE_ENABLED,
			DayTypeModelImpl.FINDER_CACHE_ENABLED, DayTypeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DayTypeModelImpl.ENTITY_CACHE_ENABLED,
			DayTypeModelImpl.FINDER_CACHE_ENABLED, DayTypeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DayTypeModelImpl.ENTITY_CACHE_ENABLED,
			DayTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the day type in the entity cache if it is enabled.
	 *
	 * @param dayType the day type
	 */
	public void cacheResult(DayType dayType) {
		EntityCacheUtil.putResult(DayTypeModelImpl.ENTITY_CACHE_ENABLED,
			DayTypeImpl.class, dayType.getPrimaryKey(), dayType);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TYPEORGANIZATION,
			new Object[] {
				dayType.getType(), Long.valueOf(dayType.getOrganizationId())
			}, dayType);

		dayType.resetOriginalValues();
	}

	/**
	 * Caches the day types in the entity cache if it is enabled.
	 *
	 * @param dayTypes the day types
	 */
	public void cacheResult(List<DayType> dayTypes) {
		for (DayType dayType : dayTypes) {
			if (EntityCacheUtil.getResult(
						DayTypeModelImpl.ENTITY_CACHE_ENABLED,
						DayTypeImpl.class, dayType.getPrimaryKey()) == null) {
				cacheResult(dayType);
			}
			else {
				dayType.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all day types.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(DayTypeImpl.class.getName());
		}

		EntityCacheUtil.clearCache(DayTypeImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the day type.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(DayType dayType) {
		EntityCacheUtil.removeResult(DayTypeModelImpl.ENTITY_CACHE_ENABLED,
			DayTypeImpl.class, dayType.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(dayType);
	}

	@Override
	public void clearCache(List<DayType> dayTypes) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (DayType dayType : dayTypes) {
			EntityCacheUtil.removeResult(DayTypeModelImpl.ENTITY_CACHE_ENABLED,
				DayTypeImpl.class, dayType.getPrimaryKey());

			clearUniqueFindersCache(dayType);
		}
	}

	protected void clearUniqueFindersCache(DayType dayType) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TYPEORGANIZATION,
			new Object[] {
				dayType.getType(), Long.valueOf(dayType.getOrganizationId())
			});
	}

	/**
	 * Creates a new day type with the primary key. Does not add the day type to the database.
	 *
	 * @param dayTypeId the primary key for the new day type
	 * @return the new day type
	 */
	public DayType create(long dayTypeId) {
		DayType dayType = new DayTypeImpl();

		dayType.setNew(true);
		dayType.setPrimaryKey(dayTypeId);

		return dayType;
	}

	/**
	 * Removes the day type with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param dayTypeId the primary key of the day type
	 * @return the day type that was removed
	 * @throws info.diit.portal.NoSuchDayTypeException if a day type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DayType remove(long dayTypeId)
		throws NoSuchDayTypeException, SystemException {
		return remove(Long.valueOf(dayTypeId));
	}

	/**
	 * Removes the day type with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the day type
	 * @return the day type that was removed
	 * @throws info.diit.portal.NoSuchDayTypeException if a day type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DayType remove(Serializable primaryKey)
		throws NoSuchDayTypeException, SystemException {
		Session session = null;

		try {
			session = openSession();

			DayType dayType = (DayType)session.get(DayTypeImpl.class, primaryKey);

			if (dayType == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDayTypeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(dayType);
		}
		catch (NoSuchDayTypeException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected DayType removeImpl(DayType dayType) throws SystemException {
		dayType = toUnwrappedModel(dayType);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, dayType);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(dayType);

		return dayType;
	}

	@Override
	public DayType updateImpl(info.diit.portal.model.DayType dayType,
		boolean merge) throws SystemException {
		dayType = toUnwrappedModel(dayType);

		boolean isNew = dayType.isNew();

		DayTypeModelImpl dayTypeModelImpl = (DayTypeModelImpl)dayType;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, dayType, merge);

			dayType.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !DayTypeModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((dayTypeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(dayTypeModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(dayTypeModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);
			}
		}

		EntityCacheUtil.putResult(DayTypeModelImpl.ENTITY_CACHE_ENABLED,
			DayTypeImpl.class, dayType.getPrimaryKey(), dayType);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TYPEORGANIZATION,
				new Object[] {
					dayType.getType(), Long.valueOf(dayType.getOrganizationId())
				}, dayType);
		}
		else {
			if ((dayTypeModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_TYPEORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						dayTypeModelImpl.getOriginalType(),
						Long.valueOf(dayTypeModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TYPEORGANIZATION,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TYPEORGANIZATION,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TYPEORGANIZATION,
					new Object[] {
						dayType.getType(),
						Long.valueOf(dayType.getOrganizationId())
					}, dayType);
			}
		}

		return dayType;
	}

	protected DayType toUnwrappedModel(DayType dayType) {
		if (dayType instanceof DayTypeImpl) {
			return dayType;
		}

		DayTypeImpl dayTypeImpl = new DayTypeImpl();

		dayTypeImpl.setNew(dayType.isNew());
		dayTypeImpl.setPrimaryKey(dayType.getPrimaryKey());

		dayTypeImpl.setDayTypeId(dayType.getDayTypeId());
		dayTypeImpl.setOrganizationId(dayType.getOrganizationId());
		dayTypeImpl.setType(dayType.getType());

		return dayTypeImpl;
	}

	/**
	 * Returns the day type with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the day type
	 * @return the day type
	 * @throws com.liferay.portal.NoSuchModelException if a day type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DayType findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the day type with the primary key or throws a {@link info.diit.portal.NoSuchDayTypeException} if it could not be found.
	 *
	 * @param dayTypeId the primary key of the day type
	 * @return the day type
	 * @throws info.diit.portal.NoSuchDayTypeException if a day type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DayType findByPrimaryKey(long dayTypeId)
		throws NoSuchDayTypeException, SystemException {
		DayType dayType = fetchByPrimaryKey(dayTypeId);

		if (dayType == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + dayTypeId);
			}

			throw new NoSuchDayTypeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				dayTypeId);
		}

		return dayType;
	}

	/**
	 * Returns the day type with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the day type
	 * @return the day type, or <code>null</code> if a day type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DayType fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the day type with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param dayTypeId the primary key of the day type
	 * @return the day type, or <code>null</code> if a day type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DayType fetchByPrimaryKey(long dayTypeId) throws SystemException {
		DayType dayType = (DayType)EntityCacheUtil.getResult(DayTypeModelImpl.ENTITY_CACHE_ENABLED,
				DayTypeImpl.class, dayTypeId);

		if (dayType == _nullDayType) {
			return null;
		}

		if (dayType == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				dayType = (DayType)session.get(DayTypeImpl.class,
						Long.valueOf(dayTypeId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (dayType != null) {
					cacheResult(dayType);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(DayTypeModelImpl.ENTITY_CACHE_ENABLED,
						DayTypeImpl.class, dayTypeId, _nullDayType);
				}

				closeSession(session);
			}
		}

		return dayType;
	}

	/**
	 * Returns all the day types where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching day types
	 * @throws SystemException if a system exception occurred
	 */
	public List<DayType> findByOrganization(long organizationId)
		throws SystemException {
		return findByOrganization(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the day types where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of day types
	 * @param end the upper bound of the range of day types (not inclusive)
	 * @return the range of matching day types
	 * @throws SystemException if a system exception occurred
	 */
	public List<DayType> findByOrganization(long organizationId, int start,
		int end) throws SystemException {
		return findByOrganization(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the day types where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of day types
	 * @param end the upper bound of the range of day types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching day types
	 * @throws SystemException if a system exception occurred
	 */
	public List<DayType> findByOrganization(long organizationId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<DayType> list = (List<DayType>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (DayType dayType : list) {
				if ((organizationId != dayType.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_DAYTYPE_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				list = (List<DayType>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first day type in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching day type
	 * @throws info.diit.portal.NoSuchDayTypeException if a matching day type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DayType findByOrganization_First(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchDayTypeException, SystemException {
		DayType dayType = fetchByOrganization_First(organizationId,
				orderByComparator);

		if (dayType != null) {
			return dayType;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDayTypeException(msg.toString());
	}

	/**
	 * Returns the first day type in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching day type, or <code>null</code> if a matching day type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DayType fetchByOrganization_First(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<DayType> list = findByOrganization(organizationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last day type in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching day type
	 * @throws info.diit.portal.NoSuchDayTypeException if a matching day type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DayType findByOrganization_Last(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchDayTypeException, SystemException {
		DayType dayType = fetchByOrganization_Last(organizationId,
				orderByComparator);

		if (dayType != null) {
			return dayType;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDayTypeException(msg.toString());
	}

	/**
	 * Returns the last day type in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching day type, or <code>null</code> if a matching day type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DayType fetchByOrganization_Last(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByOrganization(organizationId);

		List<DayType> list = findByOrganization(organizationId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the day types before and after the current day type in the ordered set where organizationId = &#63;.
	 *
	 * @param dayTypeId the primary key of the current day type
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next day type
	 * @throws info.diit.portal.NoSuchDayTypeException if a day type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DayType[] findByOrganization_PrevAndNext(long dayTypeId,
		long organizationId, OrderByComparator orderByComparator)
		throws NoSuchDayTypeException, SystemException {
		DayType dayType = findByPrimaryKey(dayTypeId);

		Session session = null;

		try {
			session = openSession();

			DayType[] array = new DayTypeImpl[3];

			array[0] = getByOrganization_PrevAndNext(session, dayType,
					organizationId, orderByComparator, true);

			array[1] = dayType;

			array[2] = getByOrganization_PrevAndNext(session, dayType,
					organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DayType getByOrganization_PrevAndNext(Session session,
		DayType dayType, long organizationId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DAYTYPE_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(dayType);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DayType> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns the day type where type = &#63; and organizationId = &#63; or throws a {@link info.diit.portal.NoSuchDayTypeException} if it could not be found.
	 *
	 * @param type the type
	 * @param organizationId the organization ID
	 * @return the matching day type
	 * @throws info.diit.portal.NoSuchDayTypeException if a matching day type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DayType findByTypeOrganization(String type, long organizationId)
		throws NoSuchDayTypeException, SystemException {
		DayType dayType = fetchByTypeOrganization(type, organizationId);

		if (dayType == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("type=");
			msg.append(type);

			msg.append(", organizationId=");
			msg.append(organizationId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchDayTypeException(msg.toString());
		}

		return dayType;
	}

	/**
	 * Returns the day type where type = &#63; and organizationId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param type the type
	 * @param organizationId the organization ID
	 * @return the matching day type, or <code>null</code> if a matching day type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DayType fetchByTypeOrganization(String type, long organizationId)
		throws SystemException {
		return fetchByTypeOrganization(type, organizationId, true);
	}

	/**
	 * Returns the day type where type = &#63; and organizationId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param type the type
	 * @param organizationId the organization ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching day type, or <code>null</code> if a matching day type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DayType fetchByTypeOrganization(String type, long organizationId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { type, organizationId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_TYPEORGANIZATION,
					finderArgs, this);
		}

		if (result instanceof DayType) {
			DayType dayType = (DayType)result;

			if (!Validator.equals(type, dayType.getType()) ||
					(organizationId != dayType.getOrganizationId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_DAYTYPE_WHERE);

			if (type == null) {
				query.append(_FINDER_COLUMN_TYPEORGANIZATION_TYPE_1);
			}
			else {
				if (type.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_TYPEORGANIZATION_TYPE_3);
				}
				else {
					query.append(_FINDER_COLUMN_TYPEORGANIZATION_TYPE_2);
				}
			}

			query.append(_FINDER_COLUMN_TYPEORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (type != null) {
					qPos.add(type);
				}

				qPos.add(organizationId);

				List<DayType> list = q.list();

				result = list;

				DayType dayType = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TYPEORGANIZATION,
						finderArgs, list);
				}
				else {
					dayType = list.get(0);

					cacheResult(dayType);

					if ((dayType.getType() == null) ||
							!dayType.getType().equals(type) ||
							(dayType.getOrganizationId() != organizationId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TYPEORGANIZATION,
							finderArgs, dayType);
					}
				}

				return dayType;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TYPEORGANIZATION,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (DayType)result;
			}
		}
	}

	/**
	 * Returns all the day types.
	 *
	 * @return the day types
	 * @throws SystemException if a system exception occurred
	 */
	public List<DayType> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the day types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of day types
	 * @param end the upper bound of the range of day types (not inclusive)
	 * @return the range of day types
	 * @throws SystemException if a system exception occurred
	 */
	public List<DayType> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the day types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of day types
	 * @param end the upper bound of the range of day types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of day types
	 * @throws SystemException if a system exception occurred
	 */
	public List<DayType> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<DayType> list = (List<DayType>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_DAYTYPE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DAYTYPE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<DayType>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<DayType>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the day types where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByOrganization(long organizationId)
		throws SystemException {
		for (DayType dayType : findByOrganization(organizationId)) {
			remove(dayType);
		}
	}

	/**
	 * Removes the day type where type = &#63; and organizationId = &#63; from the database.
	 *
	 * @param type the type
	 * @param organizationId the organization ID
	 * @return the day type that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public DayType removeByTypeOrganization(String type, long organizationId)
		throws NoSuchDayTypeException, SystemException {
		DayType dayType = findByTypeOrganization(type, organizationId);

		return remove(dayType);
	}

	/**
	 * Removes all the day types from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (DayType dayType : findAll()) {
			remove(dayType);
		}
	}

	/**
	 * Returns the number of day types where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching day types
	 * @throws SystemException if a system exception occurred
	 */
	public int countByOrganization(long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DAYTYPE_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of day types where type = &#63; and organizationId = &#63;.
	 *
	 * @param type the type
	 * @param organizationId the organization ID
	 * @return the number of matching day types
	 * @throws SystemException if a system exception occurred
	 */
	public int countByTypeOrganization(String type, long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { type, organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_TYPEORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_DAYTYPE_WHERE);

			if (type == null) {
				query.append(_FINDER_COLUMN_TYPEORGANIZATION_TYPE_1);
			}
			else {
				if (type.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_TYPEORGANIZATION_TYPE_3);
				}
				else {
					query.append(_FINDER_COLUMN_TYPEORGANIZATION_TYPE_2);
				}
			}

			query.append(_FINDER_COLUMN_TYPEORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (type != null) {
					qPos.add(type);
				}

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TYPEORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of day types.
	 *
	 * @return the number of day types
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DAYTYPE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the day type persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.DayType")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<DayType>> listenersList = new ArrayList<ModelListener<DayType>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<DayType>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(DayTypeImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttendanceCorrectionPersistence.class)
	protected AttendanceCorrectionPersistence attendanceCorrectionPersistence;
	@BeanReference(type = DayTypePersistence.class)
	protected DayTypePersistence dayTypePersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = WorkingDayPersistence.class)
	protected WorkingDayPersistence workingDayPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_DAYTYPE = "SELECT dayType FROM DayType dayType";
	private static final String _SQL_SELECT_DAYTYPE_WHERE = "SELECT dayType FROM DayType dayType WHERE ";
	private static final String _SQL_COUNT_DAYTYPE = "SELECT COUNT(dayType) FROM DayType dayType";
	private static final String _SQL_COUNT_DAYTYPE_WHERE = "SELECT COUNT(dayType) FROM DayType dayType WHERE ";
	private static final String _FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2 = "dayType.organizationId = ?";
	private static final String _FINDER_COLUMN_TYPEORGANIZATION_TYPE_1 = "dayType.type IS NULL AND ";
	private static final String _FINDER_COLUMN_TYPEORGANIZATION_TYPE_2 = "dayType.type = ? AND ";
	private static final String _FINDER_COLUMN_TYPEORGANIZATION_TYPE_3 = "(dayType.type IS NULL OR dayType.type = ?) AND ";
	private static final String _FINDER_COLUMN_TYPEORGANIZATION_ORGANIZATIONID_2 =
		"dayType.organizationId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "dayType.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No DayType exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No DayType exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(DayTypePersistenceImpl.class);
	private static DayType _nullDayType = new DayTypeImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<DayType> toCacheModel() {
				return _nullDayTypeCacheModel;
			}
		};

	private static CacheModel<DayType> _nullDayTypeCacheModel = new CacheModel<DayType>() {
			public DayType toEntityModel() {
				return _nullDayType;
			}
		};
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchEmployeeAttendanceException;
import info.diit.portal.model.EmployeeAttendance;
import info.diit.portal.model.impl.EmployeeAttendanceImpl;
import info.diit.portal.model.impl.EmployeeAttendanceModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * The persistence implementation for the employee attendance service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see EmployeeAttendancePersistence
 * @see EmployeeAttendanceUtil
 * @generated
 */
public class EmployeeAttendancePersistenceImpl extends BasePersistenceImpl<EmployeeAttendance>
	implements EmployeeAttendancePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EmployeeAttendanceUtil} to access the employee attendance persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EmployeeAttendanceImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEEID =
		new FinderPath(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceModelImpl.FINDER_CACHE_ENABLED,
			EmployeeAttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByEmployeeId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEID =
		new FinderPath(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceModelImpl.FINDER_CACHE_ENABLED,
			EmployeeAttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByEmployeeId",
			new String[] { Long.class.getName() },
			EmployeeAttendanceModelImpl.EMPLOYEEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMPLOYEEID = new FinderPath(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEmployeeId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEEATTENDANCE =
		new FinderPath(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceModelImpl.FINDER_CACHE_ENABLED,
			EmployeeAttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByEmployeeAttendance",
			new String[] {
				Long.class.getName(), Date.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEATTENDANCE =
		new FinderPath(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceModelImpl.FINDER_CACHE_ENABLED,
			EmployeeAttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByEmployeeAttendance",
			new String[] { Long.class.getName(), Date.class.getName() },
			EmployeeAttendanceModelImpl.EMPLOYEEID_COLUMN_BITMASK |
			EmployeeAttendanceModelImpl.REALTIME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMPLOYEEATTENDANCE = new FinderPath(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByEmployeeAttendance",
			new String[] { Long.class.getName(), Date.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceModelImpl.FINDER_CACHE_ENABLED,
			EmployeeAttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceModelImpl.FINDER_CACHE_ENABLED,
			EmployeeAttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the employee attendance in the entity cache if it is enabled.
	 *
	 * @param employeeAttendance the employee attendance
	 */
	public void cacheResult(EmployeeAttendance employeeAttendance) {
		EntityCacheUtil.putResult(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceImpl.class, employeeAttendance.getPrimaryKey(),
			employeeAttendance);

		employeeAttendance.resetOriginalValues();
	}

	/**
	 * Caches the employee attendances in the entity cache if it is enabled.
	 *
	 * @param employeeAttendances the employee attendances
	 */
	public void cacheResult(List<EmployeeAttendance> employeeAttendances) {
		for (EmployeeAttendance employeeAttendance : employeeAttendances) {
			if (EntityCacheUtil.getResult(
						EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
						EmployeeAttendanceImpl.class,
						employeeAttendance.getPrimaryKey()) == null) {
				cacheResult(employeeAttendance);
			}
			else {
				employeeAttendance.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all employee attendances.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EmployeeAttendanceImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EmployeeAttendanceImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the employee attendance.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EmployeeAttendance employeeAttendance) {
		EntityCacheUtil.removeResult(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceImpl.class, employeeAttendance.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<EmployeeAttendance> employeeAttendances) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EmployeeAttendance employeeAttendance : employeeAttendances) {
			EntityCacheUtil.removeResult(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
				EmployeeAttendanceImpl.class, employeeAttendance.getPrimaryKey());
		}
	}

	/**
	 * Creates a new employee attendance with the primary key. Does not add the employee attendance to the database.
	 *
	 * @param employeeAttendanceId the primary key for the new employee attendance
	 * @return the new employee attendance
	 */
	public EmployeeAttendance create(long employeeAttendanceId) {
		EmployeeAttendance employeeAttendance = new EmployeeAttendanceImpl();

		employeeAttendance.setNew(true);
		employeeAttendance.setPrimaryKey(employeeAttendanceId);

		return employeeAttendance;
	}

	/**
	 * Removes the employee attendance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param employeeAttendanceId the primary key of the employee attendance
	 * @return the employee attendance that was removed
	 * @throws info.diit.portal.NoSuchEmployeeAttendanceException if a employee attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance remove(long employeeAttendanceId)
		throws NoSuchEmployeeAttendanceException, SystemException {
		return remove(Long.valueOf(employeeAttendanceId));
	}

	/**
	 * Removes the employee attendance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the employee attendance
	 * @return the employee attendance that was removed
	 * @throws info.diit.portal.NoSuchEmployeeAttendanceException if a employee attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeAttendance remove(Serializable primaryKey)
		throws NoSuchEmployeeAttendanceException, SystemException {
		Session session = null;

		try {
			session = openSession();

			EmployeeAttendance employeeAttendance = (EmployeeAttendance)session.get(EmployeeAttendanceImpl.class,
					primaryKey);

			if (employeeAttendance == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEmployeeAttendanceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(employeeAttendance);
		}
		catch (NoSuchEmployeeAttendanceException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EmployeeAttendance removeImpl(
		EmployeeAttendance employeeAttendance) throws SystemException {
		employeeAttendance = toUnwrappedModel(employeeAttendance);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, employeeAttendance);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(employeeAttendance);

		return employeeAttendance;
	}

	@Override
	public EmployeeAttendance updateImpl(
		info.diit.portal.model.EmployeeAttendance employeeAttendance,
		boolean merge) throws SystemException {
		employeeAttendance = toUnwrappedModel(employeeAttendance);

		boolean isNew = employeeAttendance.isNew();

		EmployeeAttendanceModelImpl employeeAttendanceModelImpl = (EmployeeAttendanceModelImpl)employeeAttendance;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, employeeAttendance, merge);

			employeeAttendance.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !EmployeeAttendanceModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((employeeAttendanceModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(employeeAttendanceModelImpl.getOriginalEmployeeId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEID,
					args);

				args = new Object[] {
						Long.valueOf(employeeAttendanceModelImpl.getEmployeeId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEID,
					args);
			}

			if ((employeeAttendanceModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEATTENDANCE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(employeeAttendanceModelImpl.getOriginalEmployeeId()),
						
						employeeAttendanceModelImpl.getOriginalRealTime()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEEATTENDANCE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEATTENDANCE,
					args);

				args = new Object[] {
						Long.valueOf(employeeAttendanceModelImpl.getEmployeeId()),
						
						employeeAttendanceModelImpl.getRealTime()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEEATTENDANCE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEATTENDANCE,
					args);
			}
		}

		EntityCacheUtil.putResult(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceImpl.class, employeeAttendance.getPrimaryKey(),
			employeeAttendance);

		return employeeAttendance;
	}

	protected EmployeeAttendance toUnwrappedModel(
		EmployeeAttendance employeeAttendance) {
		if (employeeAttendance instanceof EmployeeAttendanceImpl) {
			return employeeAttendance;
		}

		EmployeeAttendanceImpl employeeAttendanceImpl = new EmployeeAttendanceImpl();

		employeeAttendanceImpl.setNew(employeeAttendance.isNew());
		employeeAttendanceImpl.setPrimaryKey(employeeAttendance.getPrimaryKey());

		employeeAttendanceImpl.setEmployeeAttendanceId(employeeAttendance.getEmployeeAttendanceId());
		employeeAttendanceImpl.setEmployeeId(employeeAttendance.getEmployeeId());
		employeeAttendanceImpl.setExpectedStart(employeeAttendance.getExpectedStart());
		employeeAttendanceImpl.setExpectedEnd(employeeAttendance.getExpectedEnd());
		employeeAttendanceImpl.setRealTime(employeeAttendance.getRealTime());
		employeeAttendanceImpl.setStatus(employeeAttendance.getStatus());

		return employeeAttendanceImpl;
	}

	/**
	 * Returns the employee attendance with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the employee attendance
	 * @return the employee attendance
	 * @throws com.liferay.portal.NoSuchModelException if a employee attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeAttendance findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the employee attendance with the primary key or throws a {@link info.diit.portal.NoSuchEmployeeAttendanceException} if it could not be found.
	 *
	 * @param employeeAttendanceId the primary key of the employee attendance
	 * @return the employee attendance
	 * @throws info.diit.portal.NoSuchEmployeeAttendanceException if a employee attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance findByPrimaryKey(long employeeAttendanceId)
		throws NoSuchEmployeeAttendanceException, SystemException {
		EmployeeAttendance employeeAttendance = fetchByPrimaryKey(employeeAttendanceId);

		if (employeeAttendance == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					employeeAttendanceId);
			}

			throw new NoSuchEmployeeAttendanceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				employeeAttendanceId);
		}

		return employeeAttendance;
	}

	/**
	 * Returns the employee attendance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the employee attendance
	 * @return the employee attendance, or <code>null</code> if a employee attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeAttendance fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the employee attendance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param employeeAttendanceId the primary key of the employee attendance
	 * @return the employee attendance, or <code>null</code> if a employee attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance fetchByPrimaryKey(long employeeAttendanceId)
		throws SystemException {
		EmployeeAttendance employeeAttendance = (EmployeeAttendance)EntityCacheUtil.getResult(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
				EmployeeAttendanceImpl.class, employeeAttendanceId);

		if (employeeAttendance == _nullEmployeeAttendance) {
			return null;
		}

		if (employeeAttendance == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				employeeAttendance = (EmployeeAttendance)session.get(EmployeeAttendanceImpl.class,
						Long.valueOf(employeeAttendanceId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (employeeAttendance != null) {
					cacheResult(employeeAttendance);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
						EmployeeAttendanceImpl.class, employeeAttendanceId,
						_nullEmployeeAttendance);
				}

				closeSession(session);
			}
		}

		return employeeAttendance;
	}

	/**
	 * Returns all the employee attendances where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @return the matching employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeAttendance> findByEmployeeId(long employeeId)
		throws SystemException {
		return findByEmployeeId(employeeId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the employee attendances where employeeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employeeId the employee ID
	 * @param start the lower bound of the range of employee attendances
	 * @param end the upper bound of the range of employee attendances (not inclusive)
	 * @return the range of matching employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeAttendance> findByEmployeeId(long employeeId,
		int start, int end) throws SystemException {
		return findByEmployeeId(employeeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the employee attendances where employeeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employeeId the employee ID
	 * @param start the lower bound of the range of employee attendances
	 * @param end the upper bound of the range of employee attendances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeAttendance> findByEmployeeId(long employeeId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEID;
			finderArgs = new Object[] { employeeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEEID;
			finderArgs = new Object[] { employeeId, start, end, orderByComparator };
		}

		List<EmployeeAttendance> list = (List<EmployeeAttendance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EmployeeAttendance employeeAttendance : list) {
				if ((employeeId != employeeAttendance.getEmployeeId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_EMPLOYEEATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEEID_EMPLOYEEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				list = (List<EmployeeAttendance>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first employee attendance in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching employee attendance
	 * @throws info.diit.portal.NoSuchEmployeeAttendanceException if a matching employee attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance findByEmployeeId_First(long employeeId,
		OrderByComparator orderByComparator)
		throws NoSuchEmployeeAttendanceException, SystemException {
		EmployeeAttendance employeeAttendance = fetchByEmployeeId_First(employeeId,
				orderByComparator);

		if (employeeAttendance != null) {
			return employeeAttendance;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employeeId=");
		msg.append(employeeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEmployeeAttendanceException(msg.toString());
	}

	/**
	 * Returns the first employee attendance in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching employee attendance, or <code>null</code> if a matching employee attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance fetchByEmployeeId_First(long employeeId,
		OrderByComparator orderByComparator) throws SystemException {
		List<EmployeeAttendance> list = findByEmployeeId(employeeId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last employee attendance in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching employee attendance
	 * @throws info.diit.portal.NoSuchEmployeeAttendanceException if a matching employee attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance findByEmployeeId_Last(long employeeId,
		OrderByComparator orderByComparator)
		throws NoSuchEmployeeAttendanceException, SystemException {
		EmployeeAttendance employeeAttendance = fetchByEmployeeId_Last(employeeId,
				orderByComparator);

		if (employeeAttendance != null) {
			return employeeAttendance;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employeeId=");
		msg.append(employeeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEmployeeAttendanceException(msg.toString());
	}

	/**
	 * Returns the last employee attendance in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching employee attendance, or <code>null</code> if a matching employee attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance fetchByEmployeeId_Last(long employeeId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByEmployeeId(employeeId);

		List<EmployeeAttendance> list = findByEmployeeId(employeeId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the employee attendances before and after the current employee attendance in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeAttendanceId the primary key of the current employee attendance
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next employee attendance
	 * @throws info.diit.portal.NoSuchEmployeeAttendanceException if a employee attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance[] findByEmployeeId_PrevAndNext(
		long employeeAttendanceId, long employeeId,
		OrderByComparator orderByComparator)
		throws NoSuchEmployeeAttendanceException, SystemException {
		EmployeeAttendance employeeAttendance = findByPrimaryKey(employeeAttendanceId);

		Session session = null;

		try {
			session = openSession();

			EmployeeAttendance[] array = new EmployeeAttendanceImpl[3];

			array[0] = getByEmployeeId_PrevAndNext(session, employeeAttendance,
					employeeId, orderByComparator, true);

			array[1] = employeeAttendance;

			array[2] = getByEmployeeId_PrevAndNext(session, employeeAttendance,
					employeeId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EmployeeAttendance getByEmployeeId_PrevAndNext(Session session,
		EmployeeAttendance employeeAttendance, long employeeId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EMPLOYEEATTENDANCE_WHERE);

		query.append(_FINDER_COLUMN_EMPLOYEEID_EMPLOYEEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(employeeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(employeeAttendance);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EmployeeAttendance> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the employee attendances where employeeId = &#63; and realTime = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param realTime the real time
	 * @return the matching employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeAttendance> findByEmployeeAttendance(long employeeId,
		Date realTime) throws SystemException {
		return findByEmployeeAttendance(employeeId, realTime,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the employee attendances where employeeId = &#63; and realTime = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employeeId the employee ID
	 * @param realTime the real time
	 * @param start the lower bound of the range of employee attendances
	 * @param end the upper bound of the range of employee attendances (not inclusive)
	 * @return the range of matching employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeAttendance> findByEmployeeAttendance(long employeeId,
		Date realTime, int start, int end) throws SystemException {
		return findByEmployeeAttendance(employeeId, realTime, start, end, null);
	}

	/**
	 * Returns an ordered range of all the employee attendances where employeeId = &#63; and realTime = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employeeId the employee ID
	 * @param realTime the real time
	 * @param start the lower bound of the range of employee attendances
	 * @param end the upper bound of the range of employee attendances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeAttendance> findByEmployeeAttendance(long employeeId,
		Date realTime, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEATTENDANCE;
			finderArgs = new Object[] { employeeId, realTime };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEEATTENDANCE;
			finderArgs = new Object[] {
					employeeId, realTime,
					
					start, end, orderByComparator
				};
		}

		List<EmployeeAttendance> list = (List<EmployeeAttendance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EmployeeAttendance employeeAttendance : list) {
				if ((employeeId != employeeAttendance.getEmployeeId()) ||
						!Validator.equals(realTime,
							employeeAttendance.getRealTime())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EMPLOYEEATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEEATTENDANCE_EMPLOYEEID_2);

			if (realTime == null) {
				query.append(_FINDER_COLUMN_EMPLOYEEATTENDANCE_REALTIME_1);
			}
			else {
				query.append(_FINDER_COLUMN_EMPLOYEEATTENDANCE_REALTIME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				if (realTime != null) {
					qPos.add(CalendarUtil.getTimestamp(realTime));
				}

				list = (List<EmployeeAttendance>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first employee attendance in the ordered set where employeeId = &#63; and realTime = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param realTime the real time
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching employee attendance
	 * @throws info.diit.portal.NoSuchEmployeeAttendanceException if a matching employee attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance findByEmployeeAttendance_First(long employeeId,
		Date realTime, OrderByComparator orderByComparator)
		throws NoSuchEmployeeAttendanceException, SystemException {
		EmployeeAttendance employeeAttendance = fetchByEmployeeAttendance_First(employeeId,
				realTime, orderByComparator);

		if (employeeAttendance != null) {
			return employeeAttendance;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employeeId=");
		msg.append(employeeId);

		msg.append(", realTime=");
		msg.append(realTime);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEmployeeAttendanceException(msg.toString());
	}

	/**
	 * Returns the first employee attendance in the ordered set where employeeId = &#63; and realTime = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param realTime the real time
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching employee attendance, or <code>null</code> if a matching employee attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance fetchByEmployeeAttendance_First(long employeeId,
		Date realTime, OrderByComparator orderByComparator)
		throws SystemException {
		List<EmployeeAttendance> list = findByEmployeeAttendance(employeeId,
				realTime, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last employee attendance in the ordered set where employeeId = &#63; and realTime = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param realTime the real time
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching employee attendance
	 * @throws info.diit.portal.NoSuchEmployeeAttendanceException if a matching employee attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance findByEmployeeAttendance_Last(long employeeId,
		Date realTime, OrderByComparator orderByComparator)
		throws NoSuchEmployeeAttendanceException, SystemException {
		EmployeeAttendance employeeAttendance = fetchByEmployeeAttendance_Last(employeeId,
				realTime, orderByComparator);

		if (employeeAttendance != null) {
			return employeeAttendance;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employeeId=");
		msg.append(employeeId);

		msg.append(", realTime=");
		msg.append(realTime);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEmployeeAttendanceException(msg.toString());
	}

	/**
	 * Returns the last employee attendance in the ordered set where employeeId = &#63; and realTime = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param realTime the real time
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching employee attendance, or <code>null</code> if a matching employee attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance fetchByEmployeeAttendance_Last(long employeeId,
		Date realTime, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByEmployeeAttendance(employeeId, realTime);

		List<EmployeeAttendance> list = findByEmployeeAttendance(employeeId,
				realTime, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the employee attendances before and after the current employee attendance in the ordered set where employeeId = &#63; and realTime = &#63;.
	 *
	 * @param employeeAttendanceId the primary key of the current employee attendance
	 * @param employeeId the employee ID
	 * @param realTime the real time
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next employee attendance
	 * @throws info.diit.portal.NoSuchEmployeeAttendanceException if a employee attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance[] findByEmployeeAttendance_PrevAndNext(
		long employeeAttendanceId, long employeeId, Date realTime,
		OrderByComparator orderByComparator)
		throws NoSuchEmployeeAttendanceException, SystemException {
		EmployeeAttendance employeeAttendance = findByPrimaryKey(employeeAttendanceId);

		Session session = null;

		try {
			session = openSession();

			EmployeeAttendance[] array = new EmployeeAttendanceImpl[3];

			array[0] = getByEmployeeAttendance_PrevAndNext(session,
					employeeAttendance, employeeId, realTime,
					orderByComparator, true);

			array[1] = employeeAttendance;

			array[2] = getByEmployeeAttendance_PrevAndNext(session,
					employeeAttendance, employeeId, realTime,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EmployeeAttendance getByEmployeeAttendance_PrevAndNext(
		Session session, EmployeeAttendance employeeAttendance,
		long employeeId, Date realTime, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EMPLOYEEATTENDANCE_WHERE);

		query.append(_FINDER_COLUMN_EMPLOYEEATTENDANCE_EMPLOYEEID_2);

		if (realTime == null) {
			query.append(_FINDER_COLUMN_EMPLOYEEATTENDANCE_REALTIME_1);
		}
		else {
			query.append(_FINDER_COLUMN_EMPLOYEEATTENDANCE_REALTIME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(employeeId);

		if (realTime != null) {
			qPos.add(CalendarUtil.getTimestamp(realTime));
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(employeeAttendance);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EmployeeAttendance> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the employee attendances.
	 *
	 * @return the employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeAttendance> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the employee attendances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of employee attendances
	 * @param end the upper bound of the range of employee attendances (not inclusive)
	 * @return the range of employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeAttendance> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the employee attendances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of employee attendances
	 * @param end the upper bound of the range of employee attendances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeAttendance> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EmployeeAttendance> list = (List<EmployeeAttendance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_EMPLOYEEATTENDANCE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_EMPLOYEEATTENDANCE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<EmployeeAttendance>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<EmployeeAttendance>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the employee attendances where employeeId = &#63; from the database.
	 *
	 * @param employeeId the employee ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByEmployeeId(long employeeId) throws SystemException {
		for (EmployeeAttendance employeeAttendance : findByEmployeeId(
				employeeId)) {
			remove(employeeAttendance);
		}
	}

	/**
	 * Removes all the employee attendances where employeeId = &#63; and realTime = &#63; from the database.
	 *
	 * @param employeeId the employee ID
	 * @param realTime the real time
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByEmployeeAttendance(long employeeId, Date realTime)
		throws SystemException {
		for (EmployeeAttendance employeeAttendance : findByEmployeeAttendance(
				employeeId, realTime)) {
			remove(employeeAttendance);
		}
	}

	/**
	 * Removes all the employee attendances from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (EmployeeAttendance employeeAttendance : findAll()) {
			remove(employeeAttendance);
		}
	}

	/**
	 * Returns the number of employee attendances where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @return the number of matching employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public int countByEmployeeId(long employeeId) throws SystemException {
		Object[] finderArgs = new Object[] { employeeId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_EMPLOYEEID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EMPLOYEEATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEEID_EMPLOYEEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EMPLOYEEID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of employee attendances where employeeId = &#63; and realTime = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param realTime the real time
	 * @return the number of matching employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public int countByEmployeeAttendance(long employeeId, Date realTime)
		throws SystemException {
		Object[] finderArgs = new Object[] { employeeId, realTime };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_EMPLOYEEATTENDANCE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_EMPLOYEEATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEEATTENDANCE_EMPLOYEEID_2);

			if (realTime == null) {
				query.append(_FINDER_COLUMN_EMPLOYEEATTENDANCE_REALTIME_1);
			}
			else {
				query.append(_FINDER_COLUMN_EMPLOYEEATTENDANCE_REALTIME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				if (realTime != null) {
					qPos.add(CalendarUtil.getTimestamp(realTime));
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EMPLOYEEATTENDANCE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of employee attendances.
	 *
	 * @return the number of employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_EMPLOYEEATTENDANCE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the employee attendance persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.EmployeeAttendance")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<EmployeeAttendance>> listenersList = new ArrayList<ModelListener<EmployeeAttendance>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<EmployeeAttendance>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EmployeeAttendanceImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttendanceCorrectionPersistence.class)
	protected AttendanceCorrectionPersistence attendanceCorrectionPersistence;
	@BeanReference(type = DayTypePersistence.class)
	protected DayTypePersistence dayTypePersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = WorkingDayPersistence.class)
	protected WorkingDayPersistence workingDayPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_EMPLOYEEATTENDANCE = "SELECT employeeAttendance FROM EmployeeAttendance employeeAttendance";
	private static final String _SQL_SELECT_EMPLOYEEATTENDANCE_WHERE = "SELECT employeeAttendance FROM EmployeeAttendance employeeAttendance WHERE ";
	private static final String _SQL_COUNT_EMPLOYEEATTENDANCE = "SELECT COUNT(employeeAttendance) FROM EmployeeAttendance employeeAttendance";
	private static final String _SQL_COUNT_EMPLOYEEATTENDANCE_WHERE = "SELECT COUNT(employeeAttendance) FROM EmployeeAttendance employeeAttendance WHERE ";
	private static final String _FINDER_COLUMN_EMPLOYEEID_EMPLOYEEID_2 = "employeeAttendance.employeeId = ?";
	private static final String _FINDER_COLUMN_EMPLOYEEATTENDANCE_EMPLOYEEID_2 = "employeeAttendance.employeeId = ? AND ";
	private static final String _FINDER_COLUMN_EMPLOYEEATTENDANCE_REALTIME_1 = "employeeAttendance.realTime IS NULL";
	private static final String _FINDER_COLUMN_EMPLOYEEATTENDANCE_REALTIME_2 = "employeeAttendance.realTime = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "employeeAttendance.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EmployeeAttendance exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No EmployeeAttendance exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EmployeeAttendancePersistenceImpl.class);
	private static EmployeeAttendance _nullEmployeeAttendance = new EmployeeAttendanceImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<EmployeeAttendance> toCacheModel() {
				return _nullEmployeeAttendanceCacheModel;
			}
		};

	private static CacheModel<EmployeeAttendance> _nullEmployeeAttendanceCacheModel =
		new CacheModel<EmployeeAttendance>() {
			public EmployeeAttendance toEntityModel() {
				return _nullEmployeeAttendance;
			}
		};
}
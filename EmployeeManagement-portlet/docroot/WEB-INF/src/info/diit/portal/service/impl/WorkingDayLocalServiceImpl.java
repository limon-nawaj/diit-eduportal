/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.impl;

import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import info.diit.portal.NoSuchWorkingDayException;
import info.diit.portal.model.WorkingDay;
import info.diit.portal.service.base.WorkingDayLocalServiceBaseImpl;
import info.diit.portal.service.persistence.WorkingDayUtil;

/**
 * The implementation of the working day local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.service.WorkingDayLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see info.diit.portal.service.base.WorkingDayLocalServiceBaseImpl
 * @see info.diit.portal.service.WorkingDayLocalServiceUtil
 */
public class WorkingDayLocalServiceImpl extends WorkingDayLocalServiceBaseImpl {
	
	public WorkingDay findWorkingDay(long organizationId, Date date) throws NoSuchWorkingDayException, SystemException{
		return WorkingDayUtil.findByOrganizationDate(organizationId, date);
	}
	
	public List<WorkingDay> findByOrganization(long organizationId) throws SystemException{
		return WorkingDayUtil.findByOrganization(organizationId);
	}
}
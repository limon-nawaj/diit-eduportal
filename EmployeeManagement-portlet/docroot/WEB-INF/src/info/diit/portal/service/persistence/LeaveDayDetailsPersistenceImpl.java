/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchLeaveDayDetailsException;
import info.diit.portal.model.LeaveDayDetails;
import info.diit.portal.model.impl.LeaveDayDetailsImpl;
import info.diit.portal.model.impl.LeaveDayDetailsModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * The persistence implementation for the leave day details service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see LeaveDayDetailsPersistence
 * @see LeaveDayDetailsUtil
 * @generated
 */
public class LeaveDayDetailsPersistenceImpl extends BasePersistenceImpl<LeaveDayDetails>
	implements LeaveDayDetailsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LeaveDayDetailsUtil} to access the leave day details persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LeaveDayDetailsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LEAVEID = new FinderPath(LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
			LeaveDayDetailsModelImpl.FINDER_CACHE_ENABLED,
			LeaveDayDetailsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByLeaveId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEAVEID =
		new FinderPath(LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
			LeaveDayDetailsModelImpl.FINDER_CACHE_ENABLED,
			LeaveDayDetailsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLeaveId",
			new String[] { Long.class.getName() },
			LeaveDayDetailsModelImpl.LEAVEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LEAVEID = new FinderPath(LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
			LeaveDayDetailsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLeaveId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FETCH_BY_LEAVEDAY = new FinderPath(LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
			LeaveDayDetailsModelImpl.FINDER_CACHE_ENABLED,
			LeaveDayDetailsImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByLeaveDay",
			new String[] { Long.class.getName(), Date.class.getName() },
			LeaveDayDetailsModelImpl.LEAVEID_COLUMN_BITMASK |
			LeaveDayDetailsModelImpl.LEAVEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LEAVEDAY = new FinderPath(LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
			LeaveDayDetailsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLeaveDay",
			new String[] { Long.class.getName(), Date.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
			LeaveDayDetailsModelImpl.FINDER_CACHE_ENABLED,
			LeaveDayDetailsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
			LeaveDayDetailsModelImpl.FINDER_CACHE_ENABLED,
			LeaveDayDetailsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
			LeaveDayDetailsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the leave day details in the entity cache if it is enabled.
	 *
	 * @param leaveDayDetails the leave day details
	 */
	public void cacheResult(LeaveDayDetails leaveDayDetails) {
		EntityCacheUtil.putResult(LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
			LeaveDayDetailsImpl.class, leaveDayDetails.getPrimaryKey(),
			leaveDayDetails);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LEAVEDAY,
			new Object[] {
				Long.valueOf(leaveDayDetails.getLeaveId()),
				
			leaveDayDetails.getLeaveDate()
			}, leaveDayDetails);

		leaveDayDetails.resetOriginalValues();
	}

	/**
	 * Caches the leave day detailses in the entity cache if it is enabled.
	 *
	 * @param leaveDayDetailses the leave day detailses
	 */
	public void cacheResult(List<LeaveDayDetails> leaveDayDetailses) {
		for (LeaveDayDetails leaveDayDetails : leaveDayDetailses) {
			if (EntityCacheUtil.getResult(
						LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
						LeaveDayDetailsImpl.class,
						leaveDayDetails.getPrimaryKey()) == null) {
				cacheResult(leaveDayDetails);
			}
			else {
				leaveDayDetails.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all leave day detailses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(LeaveDayDetailsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(LeaveDayDetailsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the leave day details.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LeaveDayDetails leaveDayDetails) {
		EntityCacheUtil.removeResult(LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
			LeaveDayDetailsImpl.class, leaveDayDetails.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(leaveDayDetails);
	}

	@Override
	public void clearCache(List<LeaveDayDetails> leaveDayDetailses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LeaveDayDetails leaveDayDetails : leaveDayDetailses) {
			EntityCacheUtil.removeResult(LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
				LeaveDayDetailsImpl.class, leaveDayDetails.getPrimaryKey());

			clearUniqueFindersCache(leaveDayDetails);
		}
	}

	protected void clearUniqueFindersCache(LeaveDayDetails leaveDayDetails) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LEAVEDAY,
			new Object[] {
				Long.valueOf(leaveDayDetails.getLeaveId()),
				
			leaveDayDetails.getLeaveDate()
			});
	}

	/**
	 * Creates a new leave day details with the primary key. Does not add the leave day details to the database.
	 *
	 * @param leaveDayDetailsId the primary key for the new leave day details
	 * @return the new leave day details
	 */
	public LeaveDayDetails create(long leaveDayDetailsId) {
		LeaveDayDetails leaveDayDetails = new LeaveDayDetailsImpl();

		leaveDayDetails.setNew(true);
		leaveDayDetails.setPrimaryKey(leaveDayDetailsId);

		return leaveDayDetails;
	}

	/**
	 * Removes the leave day details with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param leaveDayDetailsId the primary key of the leave day details
	 * @return the leave day details that was removed
	 * @throws info.diit.portal.NoSuchLeaveDayDetailsException if a leave day details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveDayDetails remove(long leaveDayDetailsId)
		throws NoSuchLeaveDayDetailsException, SystemException {
		return remove(Long.valueOf(leaveDayDetailsId));
	}

	/**
	 * Removes the leave day details with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the leave day details
	 * @return the leave day details that was removed
	 * @throws info.diit.portal.NoSuchLeaveDayDetailsException if a leave day details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LeaveDayDetails remove(Serializable primaryKey)
		throws NoSuchLeaveDayDetailsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			LeaveDayDetails leaveDayDetails = (LeaveDayDetails)session.get(LeaveDayDetailsImpl.class,
					primaryKey);

			if (leaveDayDetails == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLeaveDayDetailsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(leaveDayDetails);
		}
		catch (NoSuchLeaveDayDetailsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LeaveDayDetails removeImpl(LeaveDayDetails leaveDayDetails)
		throws SystemException {
		leaveDayDetails = toUnwrappedModel(leaveDayDetails);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, leaveDayDetails);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(leaveDayDetails);

		return leaveDayDetails;
	}

	@Override
	public LeaveDayDetails updateImpl(
		info.diit.portal.model.LeaveDayDetails leaveDayDetails, boolean merge)
		throws SystemException {
		leaveDayDetails = toUnwrappedModel(leaveDayDetails);

		boolean isNew = leaveDayDetails.isNew();

		LeaveDayDetailsModelImpl leaveDayDetailsModelImpl = (LeaveDayDetailsModelImpl)leaveDayDetails;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, leaveDayDetails, merge);

			leaveDayDetails.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !LeaveDayDetailsModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((leaveDayDetailsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEAVEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(leaveDayDetailsModelImpl.getOriginalLeaveId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LEAVEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEAVEID,
					args);

				args = new Object[] {
						Long.valueOf(leaveDayDetailsModelImpl.getLeaveId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LEAVEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEAVEID,
					args);
			}
		}

		EntityCacheUtil.putResult(LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
			LeaveDayDetailsImpl.class, leaveDayDetails.getPrimaryKey(),
			leaveDayDetails);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LEAVEDAY,
				new Object[] {
					Long.valueOf(leaveDayDetails.getLeaveId()),
					
				leaveDayDetails.getLeaveDate()
				}, leaveDayDetails);
		}
		else {
			if ((leaveDayDetailsModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_LEAVEDAY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(leaveDayDetailsModelImpl.getOriginalLeaveId()),
						
						leaveDayDetailsModelImpl.getOriginalLeaveDate()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LEAVEDAY, args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LEAVEDAY, args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LEAVEDAY,
					new Object[] {
						Long.valueOf(leaveDayDetails.getLeaveId()),
						
					leaveDayDetails.getLeaveDate()
					}, leaveDayDetails);
			}
		}

		return leaveDayDetails;
	}

	protected LeaveDayDetails toUnwrappedModel(LeaveDayDetails leaveDayDetails) {
		if (leaveDayDetails instanceof LeaveDayDetailsImpl) {
			return leaveDayDetails;
		}

		LeaveDayDetailsImpl leaveDayDetailsImpl = new LeaveDayDetailsImpl();

		leaveDayDetailsImpl.setNew(leaveDayDetails.isNew());
		leaveDayDetailsImpl.setPrimaryKey(leaveDayDetails.getPrimaryKey());

		leaveDayDetailsImpl.setLeaveDayDetailsId(leaveDayDetails.getLeaveDayDetailsId());
		leaveDayDetailsImpl.setLeaveId(leaveDayDetails.getLeaveId());
		leaveDayDetailsImpl.setLeaveDate(leaveDayDetails.getLeaveDate());
		leaveDayDetailsImpl.setDay(leaveDayDetails.getDay());

		return leaveDayDetailsImpl;
	}

	/**
	 * Returns the leave day details with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the leave day details
	 * @return the leave day details
	 * @throws com.liferay.portal.NoSuchModelException if a leave day details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LeaveDayDetails findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the leave day details with the primary key or throws a {@link info.diit.portal.NoSuchLeaveDayDetailsException} if it could not be found.
	 *
	 * @param leaveDayDetailsId the primary key of the leave day details
	 * @return the leave day details
	 * @throws info.diit.portal.NoSuchLeaveDayDetailsException if a leave day details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveDayDetails findByPrimaryKey(long leaveDayDetailsId)
		throws NoSuchLeaveDayDetailsException, SystemException {
		LeaveDayDetails leaveDayDetails = fetchByPrimaryKey(leaveDayDetailsId);

		if (leaveDayDetails == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + leaveDayDetailsId);
			}

			throw new NoSuchLeaveDayDetailsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				leaveDayDetailsId);
		}

		return leaveDayDetails;
	}

	/**
	 * Returns the leave day details with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the leave day details
	 * @return the leave day details, or <code>null</code> if a leave day details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LeaveDayDetails fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the leave day details with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param leaveDayDetailsId the primary key of the leave day details
	 * @return the leave day details, or <code>null</code> if a leave day details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveDayDetails fetchByPrimaryKey(long leaveDayDetailsId)
		throws SystemException {
		LeaveDayDetails leaveDayDetails = (LeaveDayDetails)EntityCacheUtil.getResult(LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
				LeaveDayDetailsImpl.class, leaveDayDetailsId);

		if (leaveDayDetails == _nullLeaveDayDetails) {
			return null;
		}

		if (leaveDayDetails == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				leaveDayDetails = (LeaveDayDetails)session.get(LeaveDayDetailsImpl.class,
						Long.valueOf(leaveDayDetailsId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (leaveDayDetails != null) {
					cacheResult(leaveDayDetails);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(LeaveDayDetailsModelImpl.ENTITY_CACHE_ENABLED,
						LeaveDayDetailsImpl.class, leaveDayDetailsId,
						_nullLeaveDayDetails);
				}

				closeSession(session);
			}
		}

		return leaveDayDetails;
	}

	/**
	 * Returns all the leave day detailses where leaveId = &#63;.
	 *
	 * @param leaveId the leave ID
	 * @return the matching leave day detailses
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveDayDetails> findByLeaveId(long leaveId)
		throws SystemException {
		return findByLeaveId(leaveId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the leave day detailses where leaveId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param leaveId the leave ID
	 * @param start the lower bound of the range of leave day detailses
	 * @param end the upper bound of the range of leave day detailses (not inclusive)
	 * @return the range of matching leave day detailses
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveDayDetails> findByLeaveId(long leaveId, int start, int end)
		throws SystemException {
		return findByLeaveId(leaveId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the leave day detailses where leaveId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param leaveId the leave ID
	 * @param start the lower bound of the range of leave day detailses
	 * @param end the upper bound of the range of leave day detailses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching leave day detailses
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveDayDetails> findByLeaveId(long leaveId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LEAVEID;
			finderArgs = new Object[] { leaveId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LEAVEID;
			finderArgs = new Object[] { leaveId, start, end, orderByComparator };
		}

		List<LeaveDayDetails> list = (List<LeaveDayDetails>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (LeaveDayDetails leaveDayDetails : list) {
				if ((leaveId != leaveDayDetails.getLeaveId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_LEAVEDAYDETAILS_WHERE);

			query.append(_FINDER_COLUMN_LEAVEID_LEAVEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(leaveId);

				list = (List<LeaveDayDetails>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first leave day details in the ordered set where leaveId = &#63;.
	 *
	 * @param leaveId the leave ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching leave day details
	 * @throws info.diit.portal.NoSuchLeaveDayDetailsException if a matching leave day details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveDayDetails findByLeaveId_First(long leaveId,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveDayDetailsException, SystemException {
		LeaveDayDetails leaveDayDetails = fetchByLeaveId_First(leaveId,
				orderByComparator);

		if (leaveDayDetails != null) {
			return leaveDayDetails;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("leaveId=");
		msg.append(leaveId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLeaveDayDetailsException(msg.toString());
	}

	/**
	 * Returns the first leave day details in the ordered set where leaveId = &#63;.
	 *
	 * @param leaveId the leave ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching leave day details, or <code>null</code> if a matching leave day details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveDayDetails fetchByLeaveId_First(long leaveId,
		OrderByComparator orderByComparator) throws SystemException {
		List<LeaveDayDetails> list = findByLeaveId(leaveId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last leave day details in the ordered set where leaveId = &#63;.
	 *
	 * @param leaveId the leave ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching leave day details
	 * @throws info.diit.portal.NoSuchLeaveDayDetailsException if a matching leave day details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveDayDetails findByLeaveId_Last(long leaveId,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveDayDetailsException, SystemException {
		LeaveDayDetails leaveDayDetails = fetchByLeaveId_Last(leaveId,
				orderByComparator);

		if (leaveDayDetails != null) {
			return leaveDayDetails;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("leaveId=");
		msg.append(leaveId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLeaveDayDetailsException(msg.toString());
	}

	/**
	 * Returns the last leave day details in the ordered set where leaveId = &#63;.
	 *
	 * @param leaveId the leave ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching leave day details, or <code>null</code> if a matching leave day details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveDayDetails fetchByLeaveId_Last(long leaveId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLeaveId(leaveId);

		List<LeaveDayDetails> list = findByLeaveId(leaveId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the leave day detailses before and after the current leave day details in the ordered set where leaveId = &#63;.
	 *
	 * @param leaveDayDetailsId the primary key of the current leave day details
	 * @param leaveId the leave ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next leave day details
	 * @throws info.diit.portal.NoSuchLeaveDayDetailsException if a leave day details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveDayDetails[] findByLeaveId_PrevAndNext(long leaveDayDetailsId,
		long leaveId, OrderByComparator orderByComparator)
		throws NoSuchLeaveDayDetailsException, SystemException {
		LeaveDayDetails leaveDayDetails = findByPrimaryKey(leaveDayDetailsId);

		Session session = null;

		try {
			session = openSession();

			LeaveDayDetails[] array = new LeaveDayDetailsImpl[3];

			array[0] = getByLeaveId_PrevAndNext(session, leaveDayDetails,
					leaveId, orderByComparator, true);

			array[1] = leaveDayDetails;

			array[2] = getByLeaveId_PrevAndNext(session, leaveDayDetails,
					leaveId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LeaveDayDetails getByLeaveId_PrevAndNext(Session session,
		LeaveDayDetails leaveDayDetails, long leaveId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LEAVEDAYDETAILS_WHERE);

		query.append(_FINDER_COLUMN_LEAVEID_LEAVEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(leaveId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(leaveDayDetails);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LeaveDayDetails> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns the leave day details where leaveId = &#63; and leaveDate = &#63; or throws a {@link info.diit.portal.NoSuchLeaveDayDetailsException} if it could not be found.
	 *
	 * @param leaveId the leave ID
	 * @param leaveDate the leave date
	 * @return the matching leave day details
	 * @throws info.diit.portal.NoSuchLeaveDayDetailsException if a matching leave day details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveDayDetails findByLeaveDay(long leaveId, Date leaveDate)
		throws NoSuchLeaveDayDetailsException, SystemException {
		LeaveDayDetails leaveDayDetails = fetchByLeaveDay(leaveId, leaveDate);

		if (leaveDayDetails == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("leaveId=");
			msg.append(leaveId);

			msg.append(", leaveDate=");
			msg.append(leaveDate);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchLeaveDayDetailsException(msg.toString());
		}

		return leaveDayDetails;
	}

	/**
	 * Returns the leave day details where leaveId = &#63; and leaveDate = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param leaveId the leave ID
	 * @param leaveDate the leave date
	 * @return the matching leave day details, or <code>null</code> if a matching leave day details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveDayDetails fetchByLeaveDay(long leaveId, Date leaveDate)
		throws SystemException {
		return fetchByLeaveDay(leaveId, leaveDate, true);
	}

	/**
	 * Returns the leave day details where leaveId = &#63; and leaveDate = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param leaveId the leave ID
	 * @param leaveDate the leave date
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching leave day details, or <code>null</code> if a matching leave day details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveDayDetails fetchByLeaveDay(long leaveId, Date leaveDate,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { leaveId, leaveDate };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_LEAVEDAY,
					finderArgs, this);
		}

		if (result instanceof LeaveDayDetails) {
			LeaveDayDetails leaveDayDetails = (LeaveDayDetails)result;

			if ((leaveId != leaveDayDetails.getLeaveId()) ||
					!Validator.equals(leaveDate, leaveDayDetails.getLeaveDate())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_LEAVEDAYDETAILS_WHERE);

			query.append(_FINDER_COLUMN_LEAVEDAY_LEAVEID_2);

			if (leaveDate == null) {
				query.append(_FINDER_COLUMN_LEAVEDAY_LEAVEDATE_1);
			}
			else {
				query.append(_FINDER_COLUMN_LEAVEDAY_LEAVEDATE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(leaveId);

				if (leaveDate != null) {
					qPos.add(CalendarUtil.getTimestamp(leaveDate));
				}

				List<LeaveDayDetails> list = q.list();

				result = list;

				LeaveDayDetails leaveDayDetails = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LEAVEDAY,
						finderArgs, list);
				}
				else {
					leaveDayDetails = list.get(0);

					cacheResult(leaveDayDetails);

					if ((leaveDayDetails.getLeaveId() != leaveId) ||
							(leaveDayDetails.getLeaveDate() == null) ||
							!leaveDayDetails.getLeaveDate().equals(leaveDate)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LEAVEDAY,
							finderArgs, leaveDayDetails);
					}
				}

				return leaveDayDetails;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LEAVEDAY,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (LeaveDayDetails)result;
			}
		}
	}

	/**
	 * Returns all the leave day detailses.
	 *
	 * @return the leave day detailses
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveDayDetails> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the leave day detailses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of leave day detailses
	 * @param end the upper bound of the range of leave day detailses (not inclusive)
	 * @return the range of leave day detailses
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveDayDetails> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the leave day detailses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of leave day detailses
	 * @param end the upper bound of the range of leave day detailses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of leave day detailses
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveDayDetails> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LeaveDayDetails> list = (List<LeaveDayDetails>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LEAVEDAYDETAILS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LEAVEDAYDETAILS;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<LeaveDayDetails>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<LeaveDayDetails>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the leave day detailses where leaveId = &#63; from the database.
	 *
	 * @param leaveId the leave ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByLeaveId(long leaveId) throws SystemException {
		for (LeaveDayDetails leaveDayDetails : findByLeaveId(leaveId)) {
			remove(leaveDayDetails);
		}
	}

	/**
	 * Removes the leave day details where leaveId = &#63; and leaveDate = &#63; from the database.
	 *
	 * @param leaveId the leave ID
	 * @param leaveDate the leave date
	 * @return the leave day details that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveDayDetails removeByLeaveDay(long leaveId, Date leaveDate)
		throws NoSuchLeaveDayDetailsException, SystemException {
		LeaveDayDetails leaveDayDetails = findByLeaveDay(leaveId, leaveDate);

		return remove(leaveDayDetails);
	}

	/**
	 * Removes all the leave day detailses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (LeaveDayDetails leaveDayDetails : findAll()) {
			remove(leaveDayDetails);
		}
	}

	/**
	 * Returns the number of leave day detailses where leaveId = &#63;.
	 *
	 * @param leaveId the leave ID
	 * @return the number of matching leave day detailses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByLeaveId(long leaveId) throws SystemException {
		Object[] finderArgs = new Object[] { leaveId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_LEAVEID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LEAVEDAYDETAILS_WHERE);

			query.append(_FINDER_COLUMN_LEAVEID_LEAVEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(leaveId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LEAVEID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of leave day detailses where leaveId = &#63; and leaveDate = &#63;.
	 *
	 * @param leaveId the leave ID
	 * @param leaveDate the leave date
	 * @return the number of matching leave day detailses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByLeaveDay(long leaveId, Date leaveDate)
		throws SystemException {
		Object[] finderArgs = new Object[] { leaveId, leaveDate };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_LEAVEDAY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_LEAVEDAYDETAILS_WHERE);

			query.append(_FINDER_COLUMN_LEAVEDAY_LEAVEID_2);

			if (leaveDate == null) {
				query.append(_FINDER_COLUMN_LEAVEDAY_LEAVEDATE_1);
			}
			else {
				query.append(_FINDER_COLUMN_LEAVEDAY_LEAVEDATE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(leaveId);

				if (leaveDate != null) {
					qPos.add(CalendarUtil.getTimestamp(leaveDate));
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LEAVEDAY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of leave day detailses.
	 *
	 * @return the number of leave day detailses
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LEAVEDAYDETAILS);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the leave day details persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.LeaveDayDetails")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<LeaveDayDetails>> listenersList = new ArrayList<ModelListener<LeaveDayDetails>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<LeaveDayDetails>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(LeaveDayDetailsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AttendanceCorrectionPersistence.class)
	protected AttendanceCorrectionPersistence attendanceCorrectionPersistence;
	@BeanReference(type = DayTypePersistence.class)
	protected DayTypePersistence dayTypePersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = WorkingDayPersistence.class)
	protected WorkingDayPersistence workingDayPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_LEAVEDAYDETAILS = "SELECT leaveDayDetails FROM LeaveDayDetails leaveDayDetails";
	private static final String _SQL_SELECT_LEAVEDAYDETAILS_WHERE = "SELECT leaveDayDetails FROM LeaveDayDetails leaveDayDetails WHERE ";
	private static final String _SQL_COUNT_LEAVEDAYDETAILS = "SELECT COUNT(leaveDayDetails) FROM LeaveDayDetails leaveDayDetails";
	private static final String _SQL_COUNT_LEAVEDAYDETAILS_WHERE = "SELECT COUNT(leaveDayDetails) FROM LeaveDayDetails leaveDayDetails WHERE ";
	private static final String _FINDER_COLUMN_LEAVEID_LEAVEID_2 = "leaveDayDetails.leaveId = ?";
	private static final String _FINDER_COLUMN_LEAVEDAY_LEAVEID_2 = "leaveDayDetails.leaveId = ? AND ";
	private static final String _FINDER_COLUMN_LEAVEDAY_LEAVEDATE_1 = "leaveDayDetails.leaveDate IS NULL";
	private static final String _FINDER_COLUMN_LEAVEDAY_LEAVEDATE_2 = "leaveDayDetails.leaveDate = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "leaveDayDetails.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LeaveDayDetails exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No LeaveDayDetails exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(LeaveDayDetailsPersistenceImpl.class);
	private static LeaveDayDetails _nullLeaveDayDetails = new LeaveDayDetailsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<LeaveDayDetails> toCacheModel() {
				return _nullLeaveDayDetailsCacheModel;
			}
		};

	private static CacheModel<LeaveDayDetails> _nullLeaveDayDetailsCacheModel = new CacheModel<LeaveDayDetails>() {
			public LeaveDayDetails toEntityModel() {
				return _nullLeaveDayDetails;
			}
		};
}
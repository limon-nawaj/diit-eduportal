package info.diit.portal.employee;

import info.diit.portal.constant.BloodGroup;
import info.diit.portal.constant.EmployeeType;
import info.diit.portal.constant.Gender;
import info.diit.portal.constant.Religion;
import info.diit.portal.constant.Status;
import info.diit.portal.dailyWork.model.Designation;
import info.diit.portal.dailyWork.service.DesignationLocalServiceUtil;
import info.diit.portal.employee.dto.DesignationDto;
import info.diit.portal.employee.dto.EmployeeDto;
import info.diit.portal.employee.dto.EmployeeRoleDto;
import info.diit.portal.employee.dto.EmployeeScheduleDto;
import info.diit.portal.employee.dto.OrganizationDto;
import info.diit.portal.employee.dto.UserDto;
import info.diit.portal.model.Employee;
import info.diit.portal.model.EmployeeEmail;
import info.diit.portal.model.EmployeeMobile;
import info.diit.portal.model.EmployeeRole;
import info.diit.portal.model.EmployeeTimeSchedule;
import info.diit.portal.model.impl.EmployeeEmailImpl;
import info.diit.portal.model.impl.EmployeeImpl;
import info.diit.portal.model.impl.EmployeeMobileImpl;
import info.diit.portal.model.impl.EmployeeTimeScheduleImpl;
import info.diit.portal.service.EmployeeEmailLocalServiceUtil;
import info.diit.portal.service.EmployeeLocalServiceUtil;
import info.diit.portal.service.EmployeeMobileLocalServiceUtil;
import info.diit.portal.service.EmployeeRoleLocalServiceUtil;
import info.diit.portal.service.EmployeeTimeScheduleLocalServiceUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import net.atontech.vaadin.ui.numericfield.NumericField;
import net.atontech.vaadin.ui.numericfield.widgetset.shared.NumericFieldType;

import org.vaadin.tokenfield.TokenField;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.OrganizationConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.terminal.FileResource;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.StartedListener;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

@SuppressWarnings("serial")
public class EmployeeManagementApplication extends Application implements Receiver, SucceededListener, FailedListener, StartedListener, PortletRequestListener {

	private ThemeDisplay 			themeDisplay;
	private Window 					window;
	
	private List<DesignationDto> 	designationList;
	private List<OrganizationDto> 	campusList;
	private Set<String> 			workEmailToken;
	private Set<String> 			personalEmailToken;
	private Set<String> 			mobileToken;
	private Set<String> 			homePhoneToken;
	
	private Employee 				employee;
	private EmployeeMobile 			employeeMobile;
	private EmployeeEmail 			employeeEmail;
	
	private List<UserDto> 			userList;
	private List<UserDto> 			supervisorList;
	
	private Panel 					photoPanel;
	private File 					file;
	private DLFileEntry 			fileEntry = null;
	
	private final static String 	DATE_FORMAT = "dd/MM/yyyy";
	
	private long organizationId;
	
	private List<EmployeeRoleDto> employeeRoleList;
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        try {
        	organizationId = themeDisplay.getLayout().getGroup().getOrganizationId();
//			window.showNotification("ORG ID: "+organizationId);
			if (organizationId>0) {
				loadDesignation();
		        loadCampus();
		        loadUser();
		        window.addComponent(tabLayout());
		        loadUserComboBox();
			}
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		} 
    }
    
    private TabSheet tabLayout(){
    	TabSheet tabSet = new TabSheet();
    	tabSet.addTab(mainLayout(), "Employee Form");
    	tabSet.addTab(employeesLayout(), "Employee List");
    	return tabSet;
    }

    private NumericField idField;
	private ComboBox userComboBox;
	private TextField nameField;
	private ComboBox genderComboBox;
	private ComboBox religionComboBox;
	private ComboBox bloodGroupComboBox;
	private DateField dobField;
	private Upload imageUpload;
	private TokenField mobileTokenField;
	private TokenField homePhoneTokenField;
	private ComboBox designationComboBox;
	private ComboBox branchComboBox;
	private TokenField workEmailTokenField;
	private TokenField personalEmailTokenField;
	private ComboBox statusComboBox;
	private TextArea presentArea;
	private TextArea permanentArea;
	private ComboBox supervisorComboBox;
	private ComboBox roleComboBox;
	
//	private ComboBox officeTiemComboBox;
	private Button setTimeButton;
	
	private ComboBox typeComBox;
	private NumericField durationField;
	private NumericField minimumDurationField;
	private VerticalLayout dynamicLayout;
	DecimalFormat decimalFormat = new DecimalFormat("0.00");
	
	private GridLayout mainLayout(){
		GridLayout mainGridLayout = new GridLayout(12, 14);
		mainGridLayout.setWidth("100%");
		mainGridLayout.setSpacing(true);
		
		idField = new NumericField("Employee Id");
		idField.setImmediate(true);
		idField.setNumberType(NumericFieldType.INTEGER);
		idField.setRequired(true);
//		idField.setNullRepresentation("");
		idField.setNullSettingAllowed(true);
		idField.setWidth("100%");
		
		userComboBox = new ComboBox("User");
		userComboBox.setImmediate(true);
		userComboBox.setRequired(true);
		userComboBox.setWidth("100%");
		
		nameField = new TextField("Name");
		nameField.setWidth("100%");
		nameField.setRequired(true);
		
		genderComboBox = new ComboBox("Gender");
		genderComboBox.setImmediate(true);
		genderComboBox.setWidth("100%");
		for (Gender gender : Gender.values()) {
			genderComboBox.addItem(gender);
		}
		
		userComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				UserDto user = (UserDto) userComboBox.getValue();
				if (user!=null) {
					nameField.setValue(user.getName());
					dobField.setValue(user.getDateOfBirth());
					genderComboBox.setValue(getGender(user.getGender()));
				}else{
					genderComboBox.setValue(null);
					nameField.setValue("");
					dobField.setValue(null);
				}
			}
		});
		
		religionComboBox = new ComboBox("Religion");
		religionComboBox.setImmediate(true);
		religionComboBox.setWidth("100%");
		for (Religion religion : Religion.values()) {
			religionComboBox.addItem(religion);
		}
		bloodGroupComboBox = new ComboBox("Blood Group");
		bloodGroupComboBox.setImmediate(true);
		bloodGroupComboBox.setWidth("100%");
		for (BloodGroup bloodGroup : BloodGroup.values()) {
			bloodGroupComboBox.addItem(bloodGroup);
		}
		dobField = new DateField("Date of Birth");
		dobField.setRequired(true);
		dobField.setWidth("100%");
//		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		dobField.setDateFormat(DATE_FORMAT);
		
		imageUpload = new Upload("Upload", null);
		imageUpload.setButtonCaption(null);
		
		mobileToken = new HashSet<String>();
		mobileTokenField = new TokenField("Mobile");
		mobileTokenField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Set mobile = (Set) event.getProperty().getValue();
				mobileToken.clear();
				if (mobile!=null) {
					mobileToken.addAll(mobile);
				}
//				window.showNotification("Mobile: "+mobileToken.size());
			}
		});
		
		homePhoneToken = new HashSet<String>();
		homePhoneTokenField = new TokenField("Home Phone");
		homePhoneTokenField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Set homePhone = (Set) event.getProperty().getValue();
				homePhoneToken.clear();
				if (homePhone!=null) {
					homePhoneToken.addAll(homePhone);
				}
//				window.showNotification("Home phone: "+homePhoneToken.size());
			}
		});
		
		designationComboBox = new ComboBox("Designation");
		designationComboBox.setImmediate(true);
		designationComboBox.setWidth("100%");
		
		if (designationList!=null) {
			for (DesignationDto designationDto : designationList) {
				designationComboBox.addItem(designationDto);
			}
		}
		
		branchComboBox = new ComboBox("Branch");
		branchComboBox.setImmediate(true);
		branchComboBox.setWidth("100%");
		if (campusList!=null) {
			for (OrganizationDto campusDto : campusList) {
				branchComboBox.addItem(campusDto);
			}
		}
		
		workEmailToken = new HashSet<String>();
		
		workEmailTokenField = new TokenField("Work Email");
		workEmailTokenField.addValidator(new EmailValidator("Invalid email address"));
		
		workEmailTokenField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Set emailToken = (Set) event.getProperty().getValue();
				workEmailToken.clear();
				if (emailToken!=null) {
					workEmailToken.addAll(emailToken);
				}
//				window.showNotification("Work Email:"+workEmailTokent.size());
			}
		});
		
		personalEmailToken = new HashSet<String>();
		personalEmailTokenField = new TokenField("Personal Email");
//		personalEmailTokenField.addValidator(new EmailValidator("Invalid Email Address"));
		personalEmailTokenField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Set emailToken = (Set) event.getProperty().getValue();
				personalEmailToken.clear();
				if (emailToken!=null) {
					personalEmailToken.addAll(emailToken);
				}
			}
		});
		
				
		statusComboBox = new ComboBox("Status");
		statusComboBox.setImmediate(true);
		statusComboBox.setWidth("100%");
		for (Status status : Status.values()) {
			statusComboBox.addItem(status);
		}
		
		supervisorComboBox = new ComboBox("Supervisor");
		supervisorComboBox.setWidth("100%");
		loadSupervisor();
		if (supervisorList!=null) {
			for (UserDto user : supervisorList) {
				supervisorComboBox.addItem(user);
			}
		}
		
		dynamicLayout = new VerticalLayout();
		dynamicLayout.setSpacing(true);
		
		typeComBox = new ComboBox("Type");
		typeComBox.setImmediate(true);
		typeComBox.setRequired(true);
    	typeComBox.setNullSelectionAllowed(false);
//    	Hourly employee value 1 and Fix employee value 2
    	for (EmployeeType durationType : EmployeeType.values()) {
			typeComBox.addItem(durationType);
		}
    	
    	typeComBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				dynamicLayout.removeAllComponents();
				EmployeeType type = (EmployeeType) typeComBox.getValue();
				if (type.getKey()==1) {
					durationField = new NumericField("Weekly duration"); 
					durationField.setNumberType(NumericFieldType.DOUBLE);
					durationField.setFormat(decimalFormat);
					
					
					minimumDurationField = new NumericField("Minimum Hours Per Day");
					minimumDurationField.setNumberType(NumericFieldType.DOUBLE);
					minimumDurationField.setFormat(decimalFormat);
					
					durationField.setRequired(true);
					minimumDurationField.setRequired(true);
					
					dynamicLayout.addComponent(durationField);
					dynamicLayout.addComponent(minimumDurationField);
				} else {
					roleComboBox = new ComboBox("Role");
					roleComboBox.setRequired(true);
					loadRole();
					if (employeeRoleList!=null) {
						for (EmployeeRoleDto roleDto : employeeRoleList) {
							roleComboBox.addItem(roleDto);
						}
					}
					dynamicLayout.addComponent(roleComboBox);
				}
			}
		});
    	
    	
		
		presentArea = new TextArea("Present Address");
		presentArea.setWidth("100%");
		permanentArea = new TextArea("Permanent Address");
		permanentArea.setWidth("100%");
		
		Label spacer = new Label();
		Button saveButton = new Button("Save");
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				if (employee==null) {
					employee = new EmployeeImpl();
					employee.setNew(true);
				}
				
				if (employeeEmail==null) {
					employeeEmail = new EmployeeEmailImpl();
					employeeEmail.setNew(true);
				}
				
				if (employeeMobile==null) {
					employeeMobile = new EmployeeMobileImpl();
					employeeMobile.setNew(true);
				}
				
				employee.setCompanyId(themeDisplay.getCompanyId());
				employee.setUserId(themeDisplay.getUser().getUserId());
				employee.setUserName(themeDisplay.getUser().getScreenName());
				employee.setOrganizationId(organizationId);
				
				String employeeId = idField.getValue().toString();
				if (!employeeId.equals(null) && !employeeId.equals("")) {
					try {
						Employee emp = EmployeeLocalServiceUtil.fetchEmployee(Long.parseLong(employeeId));
						if (emp!=null && employee.getEmployeeId()!=Long.parseLong(employeeId)) {
							window.showNotification("Employee id already exist!", Window.Notification.TYPE_ERROR_MESSAGE);
							return;
						}else{
							employee.setEmployeeId(Long.parseLong(employeeId));
						}
						
					} catch (SystemException e) {
						e.printStackTrace();
					}				
				}else{
					window.showNotification("Employee id cannot be empty", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				UserDto user = (UserDto) userComboBox.getValue();
				if (user!=null) {
					employee.setEmployeeUserId(user.getUserId());
				}else{
					window.showNotification("User cannot be empty", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				String name = nameField.getValue().toString();
				if (!name.equals(null)) {
					employee.setName(name);
				}else{
					window.showNotification("Employee name cannot be empty", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				
				Gender gender = (Gender) genderComboBox.getValue();
				if (gender!=null) {
					employee.setGender(gender.getKey());
				}
				
				Religion religion = (Religion) religionComboBox.getValue();
				if (religion!=null) {
					employee.setReligion(religion.getKey());
				}
				
				BloodGroup blood = (BloodGroup) bloodGroupComboBox.getValue();
				if (blood!=null) {
					employee.setBlood(blood.getKey());
				}
				
				Date birthDate = (Date) dobField.getValue();
				if (birthDate!=null) {
					birthDate.setHours(0);
					birthDate.setMinutes(0);
					birthDate.setSeconds(0);
					employee.setDob(birthDate);
				}else{
					window.showNotification("Select a date", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				DesignationDto designationDto = (DesignationDto) designationComboBox.getValue();
				if (designationDto!=null) {
					employee.setDesignation(designationDto.getId());
				}
				
				OrganizationDto campusDto = (OrganizationDto) branchComboBox.getValue();
				if (campusDto!=null) {
					employee.setBranch(campusDto.getOrganizationId());
				}
				
				Status status = (Status) statusComboBox.getValue();
				if (status!=null) {
					employee.setStatus(status.getKey());
				}
				
				UserDto supervisor = (UserDto) supervisorComboBox.getValue();
				if (supervisor!=null) {
					employee.setSupervisor(supervisor.getUserId());
				}
				
				employee.setPresentAddress(presentArea.getValue().toString());
				employee.setPermanentAddress(permanentArea.getValue().toString());
				
				EmployeeType type = (EmployeeType) typeComBox.getValue();
				if (type!=null) {
					employee.setType(type.getKey());
					if (type.getKey()==1) {
						Double duration = Double.parseDouble(durationField.getValue().toString());
						if (duration!=null) {
							employee.setDuration(duration);
						} else {
							window.showNotification("Duration can't be empty", Window.Notification.TYPE_WARNING_MESSAGE);
							return;
						}
						
						Double minimumDuration = Double.parseDouble(minimumDurationField.getValue().toString());
						if (minimumDuration!=null) {
							employee.setMinimumDuration(minimumDuration);
						} else {
							window.showNotification("Minimum duration can't be empty", Window.Notification.TYPE_WARNING_MESSAGE);
							return;
						}
					} else {
						EmployeeRoleDto roleDto = (EmployeeRoleDto) roleComboBox.getValue();
						if (roleDto!=null) {
							employee.setRole(roleDto.getId());
						}else{
							window.showNotification("Role can't be null", Window.Notification.TYPE_WARNING_MESSAGE);
							return;
						}
					}
				}else{
					window.showNotification("Type Can't be empty", Window.Notification.TYPE_WARNING_MESSAGE);
					return;
				}
				
				
				try {
					if (employee.isNew()) {
						employee.setCreateDate(new Date());
						EmployeeLocalServiceUtil.addEmployee(employee);
						contactInfoProcess(employee.getEmployeeId());	
						documentPhoto(employee);
						if (fileEntry!=null) {
							employee.setPhoto(fileEntry.getFileEntryId());
							EmployeeLocalServiceUtil.updateEmployee(employee);
						}else{
							EmployeeLocalServiceUtil.updateEmployee(employee);
						}
						if (type.getKey()==2) {
							saveDefaultSchedule(employee);
						}
						
						window.showNotification("Employee saved successfully");
					}else{
						employee.setModifiedDate(new Date());
						EmployeeLocalServiceUtil.updateEmployee(employee);
						contactInfoProcess(employee.getEmployeeId());
						documentPhoto(employee);
						if (fileEntry!=null) {
							employee.setPhoto(fileEntry.getFileEntryId());
							EmployeeLocalServiceUtil.updateEmployee(employee);
						}else{
							EmployeeLocalServiceUtil.updateEmployee(employee);
						}
						window.showNotification("Employee updated successfully");
					}
					loadEmployee();
				} catch (SystemException e) {
					e.printStackTrace();
				}
				loadUser();
			}
		});
		
		Button resetButton = new Button("Reset");
		resetButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				clear();
				loadUserComboBox();
			}
		});
		
		VerticalLayout photoLayout = new VerticalLayout();
		photoLayout.setWidth("100%");
		photoLayout.setHeight("100%");
		photoLayout.setSpacing(true);
		photoPanel = new Panel();
		photoPanel.setStyleName(Reindeer.LAYOUT_BLACK);
		photoPanel.setVisible(true);
		
		photoPanel.setWidth("150px");
		photoPanel.setHeight("100%");
		
		Button uploadButton = new Button("Upload");
		uploadButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				showPopWindow();
			}
		});
		
		
		photoLayout.addComponent(photoPanel);
		photoLayout.addComponent(uploadButton);
		
		HorizontalLayout rowLayout = new HorizontalLayout();
		rowLayout.setWidth("100%");
		rowLayout.setSpacing(true);
		rowLayout.addComponent(spacer);
		rowLayout.addComponent(saveButton);
		rowLayout.addComponent(resetButton);
		rowLayout.setExpandRatio(spacer, 1);
		
		mainGridLayout.addComponent(idField, 0, 0, 2, 0);
		mainGridLayout.addComponent(userComboBox, 0, 1, 2, 1);
		mainGridLayout.addComponent(nameField, 0, 2, 2, 2);
		mainGridLayout.addComponent(genderComboBox, 0, 3, 2, 3);
		mainGridLayout.addComponent(religionComboBox, 0, 4, 2, 4);
		mainGridLayout.addComponent(bloodGroupComboBox, 0, 5, 2, 5);
		mainGridLayout.addComponent(dobField, 0, 6, 2, 6);
		mainGridLayout.addComponent(supervisorComboBox, 0, 7, 2, 7);
		mainGridLayout.addComponent(presentArea, 0, 9, 2, 9);
		
		mainGridLayout.addComponent(designationComboBox, 4, 0, 6, 0);
		mainGridLayout.addComponent(branchComboBox, 4, 1, 6, 1);
		mainGridLayout.addComponent(statusComboBox, 4, 2, 6, 2);
		mainGridLayout.addComponent(mobileTokenField, 4, 3, 6, 3);
		mainGridLayout.addComponent(homePhoneTokenField, 4, 4, 6, 4);
		mainGridLayout.addComponent(workEmailTokenField, 4, 5, 6, 5);
		mainGridLayout.addComponent(personalEmailTokenField, 4, 6, 6, 6);
		mainGridLayout.addComponent(typeComBox, 4, 7, 6, 7);
		mainGridLayout.addComponent(dynamicLayout, 4, 8, 6, 8);
		mainGridLayout.addComponent(permanentArea, 4, 9, 6, 9);
		
		mainGridLayout.addComponent(rowLayout, 4, 10, 6, 10);
		
		mainGridLayout.addComponent(photoLayout, 8, 0, 11, 6);
		
		
		
//		mainGridLayout.addComponent(durationLayout, 4, 8, 6, 8);
		
		
		return mainGridLayout;
	}
	
	private Gender getGender(int id){
		if (Gender.values()!=null) {
			for (Gender gender : Gender.values()) {
				if (gender.getKey()==id) {
					return gender;
				}
			}
		}
		return null;
	}
	
	private void loadUserComboBox(){
		if (userList!=null) {
			userComboBox.removeAllItems();
			for (UserDto user : userList) {
//				window.showNotification("User: "+user.getName()+ " Id: "+user.getUserId());
				userComboBox.addItem(user);
			}
		}
	}
	
	
	private void saveDefaultSchedule(Employee employee){
		try {
			EmployeeTimeSchedule timeSchdule = new EmployeeTimeScheduleImpl();
			EmployeeRole employeeRole = EmployeeRoleLocalServiceUtil.fetchEmployeeRole(employee.getRole());
			
			for (int i = 1; i < 8; i++) {
				timeSchdule.setEmployeeId(employee.getEmployeeId());
				timeSchdule.setDay(i);
				timeSchdule.setCompanyId(themeDisplay.getCompanyId());
				timeSchdule.setOrganizationId(organizationId);
				timeSchdule.setStartTime(employeeRole.getStartTime());
				timeSchdule.setEndTime(employeeRole.getEndTime());
				timeSchdule.setDuration(8);
				EmployeeTimeScheduleLocalServiceUtil.addEmployeeTimeSchedule(timeSchdule);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void loadRole(){
		try {
			List<EmployeeRole> roleList = EmployeeRoleLocalServiceUtil.findByOrganization(organizationId);
			employeeRoleList = new ArrayList<EmployeeRoleDto>();
			if (roleList!=null) {
				for (EmployeeRole role: roleList) {
					EmployeeRoleDto roleDto = new EmployeeRoleDto();
					roleDto.setId(role.getEmployeeRoleId());
					roleDto.setRole(role.getRole());
					employeeRoleList.add(roleDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void documentPhoto(Employee employee){
		String fileTitle="Student Photo";
		long repositoryId = 0;
		long folderId = 0;
		
		if (file!=null) {
			if (file.exists()) {
				String fileName = file.getName();
				UserDto user = (UserDto) userComboBox.getValue();
				if (user!=null) {
					try {
//						window.showNotification("user id: "+user.getUserId()+" file name:"+fileName+"file name extension:"+FileUtil.getExtension(file.getName())+" employee id:"+employee.getEmployeeId(), Window.Notification.TYPE_ERROR_MESSAGE);
						fileEntry = DLFileEntryLocalServiceUtil.addFileEntry(user.getUserId(), themeDisplay.getUser().getGroupId(), repositoryId, 
								folderId, fileName, FileUtil.getExtension(file.getName()), "Employee Photo Id "+employee.getEmployeeId(), "Photo of "+employee.getName(), 
								fileTitle, 0, null, file, new FileInputStream(file), file.length(), new ServiceContext());
//						window.showNotification("Document store successfully");
					} catch (PortalException e) {
						e.printStackTrace();
					} catch (SystemException e) {
						e.printStackTrace();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}
				
			}
		}
	}
	
	private Upload upload;
	private Window popWindow;
	
	private void showPopWindow(){
		popWindow = new Window();
		popWindow.setWidth("370px");
		popWindow.setPositionX(300);
		popWindow.setPositionY(300);
		
		upload = new Upload("", this);
		upload.setButtonCaption("Start upload");
		
		upload.addListener((Upload.SucceededListener) this);
		upload.addListener((Upload.FailedListener) this);
		upload.addListener((Upload.StartedListener) this);
		
		popWindow.addComponent(upload);
		window.addWindow(popWindow);
	}
	
	private void contactInfoProcess(long employeeId){
		
		try {
			List<EmployeeMobile> employeeMobileList = EmployeeMobileLocalServiceUtil.findByEmployee(employeeId);
			if (employeeMobileList!=null) {
				for (EmployeeMobile employeeMobile : employeeMobileList) {
					EmployeeMobileLocalServiceUtil.deleteEmployeeMobile(employeeMobile);
				}
			}
			
			List<EmployeeEmail> employeeEmailList = EmployeeEmailLocalServiceUtil.findByEmployee(employeeId);
			if (employeeEmailList!=null) {
				for (EmployeeEmail employeeEmail : employeeEmailList) {
					EmployeeEmailLocalServiceUtil.deleteEmployeeEmail(employeeEmail);
				}
			}
			
			employeeMobile.setCompanyId(themeDisplay.getCompanyId());
			employeeMobile.setUserId(themeDisplay.getUser().getUserId());
			employeeMobile.setUserName(themeDisplay.getUser().getScreenName());
			employeeMobile.setCreateDate(new Date());
			
			Iterator<String> mobileIterator = mobileToken.iterator();
			while (mobileIterator.hasNext()) {
				String mobile = mobileIterator.next();
				employeeMobile.setEmployeeId(employeeId);
				employeeMobile.setMobile(mobile);
				employeeMobile.setHomePhone("");
				employeeMobile.setStatus(0);
				EmployeeMobileLocalServiceUtil.addEmployeeMobile(employeeMobile);
				
			}
			
			Iterator<String> phoneIterator = homePhoneToken.iterator();
			while(phoneIterator.hasNext()){
				String phone = phoneIterator.next();
				employeeMobile.setEmployeeId(employeeId);
				employeeMobile.setHomePhone(phone);
				employeeMobile.setMobile("");
				employeeMobile.setStatus(1);
				EmployeeMobileLocalServiceUtil.addEmployeeMobile(employeeMobile);
				
			}
			
			employeeEmail.setCompanyId(themeDisplay.getCompanyId());
			employeeEmail.setUserId(themeDisplay.getUser().getUserId());
			employeeEmail.setUserName(themeDisplay.getUser().getScreenName());
			employeeEmail.setCreateDate(new Date());
			
			Iterator<String> workEmail = workEmailToken.iterator();
			while(workEmail.hasNext()){
				String email = workEmail.next();
				employeeEmail.setEmployeeId(employeeId);
				employeeEmail.setWorkEmail(email);
				employeeEmail.setPersonalEmail("");
				employeeEmail.setStatus(0);
				EmployeeEmailLocalServiceUtil.addEmployeeEmail(employeeEmail);
			}
			
			Iterator<String> iterator = personalEmailToken.iterator();
			while(iterator.hasNext()){
				String email = iterator.next();
				employeeEmail.setEmployeeId(employeeId);
				employeeEmail.setWorkEmail("");
				employeeEmail.setPersonalEmail(email);
				employeeEmail.setStatus(1);
				EmployeeEmailLocalServiceUtil.addEmployeeEmail(employeeEmail);
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private List<UserDto> loadSupervisor(){
		if (supervisorList==null) {
			supervisorList = new ArrayList<UserDto>();
		}else{
			supervisorList.clear();
		}
		
		try {
			List<Employee> employees = EmployeeLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
			if (employees!=null) {
				for (Employee emp : employees) {
					UserDto userDto = new UserDto();
					userDto.setUserId(emp.getEmployeeId());
					userDto.setName(emp.getName());
					supervisorList.add(userDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return supervisorList;
	}
	
	private List<UserDto> loadUser(){
		if (userList==null) {
			userList = new ArrayList<UserDto>();
		}else{
			userList.clear();
		}
		try {
			List<User> users = UserLocalServiceUtil.getCompanyUsers(themeDisplay.getCompanyId(), 0, UserLocalServiceUtil.getUsersCount());
			for (User user : users) {
				if (!checkEmployee(user.getUserId())) {
					if (compareUser(user.getUserId())==false) {
						UserDto userDto = new UserDto();
						userDto.setUserId(user.getUserId());
						userDto.setName(user.getFullName());
						userDto.setDateOfBirth(user.getBirthday());
						
						if (user.getFemale()) {
							userDto.setGender(2);
						}else {
							userDto.setGender(1);
						}
						userList.add(userDto);
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
		return userList;
	}
	
	private final static String STUDENT_GROUP = "Student";
	
	private boolean checkEmployee(long employeeId){
		boolean status = false;
		try {
			UserGroup userGroup = UserGroupLocalServiceUtil.getUserGroup(themeDisplay.getCompanyId(), STUDENT_GROUP);
			List<User> users = UserLocalServiceUtil.getUserGroupUsers(userGroup.getUserGroupId());
			if (users!=null) {
				for (User user : users) {
					if (user.getUserId()==employeeId) {
						status = true;
					}
				}
			}
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return status;
		
	}
	
	private Boolean compareUser(long userId){
		boolean status = false;
		try {
			List<Employee> employeeList = EmployeeLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
			if (employeeList!=null) {
				for (Employee employee : employeeList) {
					if (employee.getEmployeeUserId()==userId) {
						status = true;
						break;
					}else{
						status = false;
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return status;
	}
	
	private List<DesignationDto> loadDesignation(){
		try {
			if (designationList==null) {
				designationList = new ArrayList<DesignationDto>();
			}else{
				designationList.clear();
			}
			List<Designation> designations = DesignationLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
			for (Designation designation : designations) {
				DesignationDto designationDto = new DesignationDto();
				designationDto.setId(designation.getDesignationId());
				designationDto.setTitle(designation.getDesignationTitle());
				designationList.add(designationDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return designationList;
	}
	
	private List<OrganizationDto> loadCampus(){
		if (campusList==null) {
			campusList = new ArrayList<OrganizationDto>();
		}else{
			campusList.clear();
		}
		
		try {
			List<Organization> organizationList = OrganizationLocalServiceUtil.getOrganizations(themeDisplay.getCompanyId(), OrganizationConstants.ANY_PARENT_ORGANIZATION_ID, 
						0, OrganizationLocalServiceUtil.getOrganizationsCount());
			for (Organization organization : organizationList) {
				OrganizationDto campusDto = new OrganizationDto();
				campusDto.setOrganizationId(organization.getOrganizationId());
				campusDto.setOrganizationName(organization.getName());
				campusList.add(campusDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return campusList;
	}
	
	private void clear(){
		employee = null;
		idField.setValue("");
		userComboBox.setValue(null);
		nameField.setValue("");
		genderComboBox.setValue(null);
		religionComboBox.setValue(null);
		bloodGroupComboBox.setValue(null);
		dobField.setValue(null);
		mobileTokenField.setValue(null);
		homePhoneTokenField.setValue(null);
		designationComboBox.setValue(null);
		branchComboBox.setValue(null);
		workEmailTokenField.setValue(null);
		personalEmailTokenField.setValue(null);
		statusComboBox.setValue(null);
		presentArea.setValue("");
		permanentArea.setValue("");
		loadUser();
	}
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

	public void uploadStarted(StartedEvent event) {
		System.out.println( event.getSource());
	}

	public void uploadFailed(FailedEvent event) {
		popWindow.addComponent(new Label("Uploading "+event.getFilename()+" of type '"+event.getMIMEType()+"' failed."));
	}

	public void uploadSucceeded(SucceededEvent event) {

		popWindow.addComponent(new Label("File " + event.getFilename()
				+ " of type '" + event.getMIMEType() + "' uploaded."));

		final FileResource imageResource = new FileResource(file, this);
		photoPanel.removeAllComponents();
		Embedded image = new Embedded("", imageResource);
		image.setWidth(photoPanel.getWidth()-20);
		image.setHeight(photoPanel.getHeight()-20);
		photoPanel.addComponent(image);
		popWindow.setVisible(false);
	}

	private void startUpload(){
		upload.addListener((Upload.SucceededListener) this);
		upload.addListener((Upload.FailedListener) this);
		upload.addListener((Upload.StartedListener) this);
	}
	public OutputStream receiveUpload(String filename, String mimeType) {
		FileOutputStream fos = null; // Output stream to write to
		if (mimeType.equalsIgnoreCase("image/jpeg") || mimeType.equalsIgnoreCase("image/gif") || mimeType.equalsIgnoreCase("image/png") ||mimeType.equalsIgnoreCase("image/bmp")) {
	
			File tmpdir = new File("/tmp/uploads/");
			if (!tmpdir.exists()) {
				tmpdir.mkdirs();
	//			System.out.println("Dir created ..");
			}
	//		System.out.println("Exists created ..");
			
			file = new File(tmpdir, filename);
			try {
				fos = new FileOutputStream(file);
			} catch (final java.io.FileNotFoundException e) {
				e.printStackTrace();
				return null;
			}
		}else {
			window.showNotification("Please upload an image");
//			upload.addListener((Upload.FailedListener) this);
		}

		return fos; 
	}
	
	private BeanItemContainer<EmployeeDto> employeeContainer;
	private Table employeeTable;
	private final static String EMPLOYEE_ID = "id";
	private final static String EMPLOYEE_NAME = "name";
	private final static String EMPLOYEE_DURATION = "duration";
	
	private GridLayout employeesLayout(){
		GridLayout employeesLayout = new GridLayout(8, 8);
		employeesLayout.setWidth("100%");
		employeesLayout.setSpacing(true);
		
		employeeContainer = new BeanItemContainer<EmployeeDto>(EmployeeDto.class);
		
		employeeTable = new Table("", employeeContainer);
		employeeTable.setWidth("100%");
		employeeTable.setColumnHeader(EMPLOYEE_ID, "Employee ID");
		employeeTable.setColumnHeader(EMPLOYEE_NAME, "Employee Name");
		employeeTable.setColumnHeader(EMPLOYEE_DURATION, "Weekly Duration(Hours)");
		
		employeeTable.setVisibleColumns(new String[]{EMPLOYEE_ID, EMPLOYEE_NAME, EMPLOYEE_DURATION});
		employeeTable.setImmediate(true);
		employeeTable.setSelectable(true);
		
		loadEmployee();
		
		Label spacerLabel = new Label();
		Button timeSchedule = new Button("Time Schedule");
		
		VerticalLayout buttonLayout = new VerticalLayout();
		buttonLayout.setWidth("100%");
		
		buttonLayout.addComponent(spacerLabel);
		buttonLayout.addComponent(timeSchedule);
		buttonLayout.setExpandRatio(spacerLabel, 1);
		
		employeesLayout.addComponent(employeeTable, 0, 0, 7, 0);
		employeesLayout.addComponent(buttonLayout, 0, 1, 7, 1);
		
		timeSchedule.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				VerticalLayout layout = new VerticalLayout();
				layout.setMargin(true);
				layout.setSpacing(true);
				final Window popUpWindow = new Window(null, layout);
				
				EmployeeDto employeeDto = (EmployeeDto) employeeTable.getValue();
				if (employeeDto!=null) {
					if (employeeDto.getType()==1) {
						layout.removeAllComponents();
						layout.addComponent(hourlyEmployeeLayout(employeeDto.getId()));
						popUpWindow.setWidth("30%");
						popUpWindow.center();
						popUpWindow.setModal(true);
						getMainWindow().addWindow(popUpWindow);
					}else{
						layout.removeAllComponents();
						layout.addComponent(fixEmployeelayout(employeeDto));
						popUpWindow.setWidth("50%");
						popUpWindow.center();
						popUpWindow.setModal(true);
						getMainWindow().addWindow(popUpWindow);
					}
				}
			}
		});
		
		return employeesLayout;
	}
	
	private void loadEmployee(){
		employeeContainer.removeAllItems();
		try {
			List<Employee> employeeList = EmployeeLocalServiceUtil.findByOrganization(organizationId);
			if (employeeList!=null) {
				for (Employee employee : employeeList) {
					EmployeeDto employeeDto = new EmployeeDto();
					employeeDto.setId(employee.getEmployeeId());
					employeeDto.setName(employee.getName());
					employeeDto.setType(employee.getType());
					
					double duration = 0;
					List<EmployeeTimeSchedule> result = EmployeeTimeScheduleLocalServiceUtil.findByEmployee(employee.getEmployeeId());
					if (result!=null) {
						for (EmployeeTimeSchedule employeeTimeSchedule : result) {
							duration += employeeTimeSchedule.getDuration();
						}
					}
					
					employeeDto.setDuration(duration);
					employeeContainer.addBean(employeeDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	}
	
	private VerticalLayout hourlyEmployeeLayout(final long employeeId){
		VerticalLayout hourlyEmployeeLayout = new VerticalLayout();
		hourlyEmployeeLayout.setSpacing(true);
		
		final NumericField weeklyDurationField = new NumericField("Weekly Duration");
		weeklyDurationField.setNumberType(NumericFieldType.INTEGER);
		final NumericField minimumPerDayField = new NumericField("Minimum Duration Per Day");
		minimumPerDayField.setNumberType(NumericFieldType.INTEGER);
		Button saveButton = new Button("Save");
		try {
			final Employee emp = EmployeeLocalServiceUtil.fetchEmployee(employeeId);
			weeklyDurationField.setValue(emp.getDuration());
			minimumPerDayField.setValue(emp.getMinimumDuration());
		
			saveButton.addListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					
						Double weeklyDuration = Double.parseDouble(weeklyDurationField.getValue().toString());
						if (weeklyDuration!=null) {
							emp.setDuration(weeklyDuration);
						}
						
						Double perDayDuration = Double.parseDouble(minimumPerDayField.getValue().toString());
						if (perDayDuration!=null) {
							emp.setMinimumDuration(perDayDuration);
						}
						
						try {
							EmployeeLocalServiceUtil.updateEmployee(emp);
						} catch (SystemException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
				}
			});
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
		
		hourlyEmployeeLayout.addComponent(weeklyDurationField);
		hourlyEmployeeLayout.addComponent(minimumPerDayField);
		hourlyEmployeeLayout.addComponent(saveButton);;
		window.showNotification("It is working");
		
		return hourlyEmployeeLayout;
	}
	
	private BeanItemContainer<EmployeeScheduleDto> scheduleContainer;
	private Table scheduleTable;
	
	private static final String DAY_NAME = "dayName";
	private static final String START_TIME = "startTime";
	private static final String END_TIME = "endTime";
	private static final String DURATION = "duration";
	
	private VerticalLayout fixEmployeelayout(final EmployeeDto employeeDto){
		VerticalLayout scheduleLayout = new VerticalLayout();
		scheduleLayout.setWidth("100%");
		scheduleLayout.setSpacing(true);
		
		scheduleContainer = new BeanItemContainer<EmployeeScheduleDto>(EmployeeScheduleDto.class);
		
//		set day as row
		loadDaySchedule(employeeDto);
//		set day as row
		
		scheduleTable = new Table("", scheduleContainer);
		scheduleTable.setWidth("100%");
		
		scheduleTable.setColumnHeader(DAY_NAME, "Day");
		scheduleTable.setColumnHeader(START_TIME, "Start Time");
		scheduleTable.setColumnHeader(END_TIME, "End TIme");
		scheduleTable.setColumnHeader(DURATION, "Duration (Hours)");
		
		scheduleTable.setVisibleColumns(new String[]{DAY_NAME, START_TIME, END_TIME, DURATION});
		
		scheduleTable.setSelectable(true);
		scheduleTable.setEditable(true);
		
		scheduleTable.setTableFieldFactory(new DefaultFieldFactory(){
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				if (propertyId.equals(DAY_NAME)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				
				if (propertyId.equals(START_TIME)) {
					CustomTimeField field = new CustomTimeField();
					field.setLocale(Locale.ROOT);
					field.setWidth("100%");
					field.setTabIndex(3);
					return field;
				}
				
				if (propertyId.equals(END_TIME)) {
					CustomTimeField field = new CustomTimeField();
					field.setLocale(Locale.ROOT);
					field.setWidth("100%");
					field.setTabIndex(3);
					return field;
				}
				
				if (propertyId.equals(DURATION)) {
					NumericField field = new NumericField();
					field.setNumberType(NumericFieldType.DOUBLE);
					field.setWidth("100%");
					return field;
				}
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		Button saveButton = new Button("Save");
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				saveSchedule(employeeDto);
			}
		});
		
		HorizontalLayout hLayout = new HorizontalLayout();
		hLayout.addComponent(saveButton);
		
		scheduleLayout.addComponent(scheduleTable);
		scheduleLayout.addComponent(hLayout);
		
		return scheduleLayout;
	}
	
	private void loadDaySchedule(EmployeeDto employeeDto){
		try {
			List<EmployeeTimeSchedule> scheduleList = EmployeeTimeScheduleLocalServiceUtil.findByEmployee(employeeDto.getId());
			
			EmployeeScheduleDto saturdaySchdule = new EmployeeScheduleDto();
			saturdaySchdule.setDay(Calendar.SATURDAY);
			saturdaySchdule.setDayName("Saturday");
//			saturdaySchdule.setEmployeeId(employeeDto.getId());
			
			EmployeeScheduleDto sundaySchedule = new EmployeeScheduleDto();
			sundaySchedule.setDay(Calendar.SUNDAY);
			sundaySchedule.setDayName("Sunday");
			
			EmployeeScheduleDto mondaySchedule = new EmployeeScheduleDto();
			mondaySchedule.setDay(Calendar.MONDAY);
			mondaySchedule.setDayName("Monday");
			
			EmployeeScheduleDto tuesdaySchedule = new EmployeeScheduleDto();
			tuesdaySchedule.setDay(Calendar.TUESDAY);
			tuesdaySchedule.setDayName("Tuesday");
			
			EmployeeScheduleDto wednesdaySchedule = new EmployeeScheduleDto();
			wednesdaySchedule.setDay(Calendar.WEDNESDAY);
			wednesdaySchedule.setDayName("Wednesday");
			
			EmployeeScheduleDto thursdaySchedule = new EmployeeScheduleDto();
			thursdaySchedule.setDay(Calendar.THURSDAY);
			thursdaySchedule.setDayName("Thursday");
			
			EmployeeScheduleDto fridaySchedule = new EmployeeScheduleDto();
			fridaySchedule.setDay(Calendar.FRIDAY);
			fridaySchedule.setDayName("Friday");
			
			
			if (scheduleList!=null) {
				
				for (EmployeeTimeSchedule employeeTimeSchedule : scheduleList) {
					if (employeeTimeSchedule.getDay()==saturdaySchdule.getDay()) {
						saturdaySchdule.setStartTime(employeeTimeSchedule.getStartTime());
						saturdaySchdule.setEndTime(employeeTimeSchedule.getEndTime());
						saturdaySchdule.setDuration(employeeTimeSchedule.getDuration());
					}else if (employeeTimeSchedule.getDay()==sundaySchedule.getDay()) {
						sundaySchedule.setStartTime(employeeTimeSchedule.getStartTime());
						sundaySchedule.setEndTime(employeeTimeSchedule.getEndTime());
						sundaySchedule.setDuration(employeeTimeSchedule.getDuration());
					}else if (employeeTimeSchedule.getDay()==mondaySchedule.getDay()) {
						mondaySchedule.setStartTime(employeeTimeSchedule.getStartTime());
						mondaySchedule.setEndTime(employeeTimeSchedule.getEndTime());
						mondaySchedule.setDuration(employeeTimeSchedule.getDuration());
					}else if (employeeTimeSchedule.getDay()==tuesdaySchedule.getDay()) {
						tuesdaySchedule.setStartTime(employeeTimeSchedule.getStartTime());
						tuesdaySchedule.setEndTime(employeeTimeSchedule.getEndTime());
						tuesdaySchedule.setDuration(employeeTimeSchedule.getDuration());
					}else if (employeeTimeSchedule.getDay()==wednesdaySchedule.getDay()) {
						wednesdaySchedule.setStartTime(employeeTimeSchedule.getStartTime());
						wednesdaySchedule.setEndTime(employeeTimeSchedule.getEndTime());
						wednesdaySchedule.setDuration(employeeTimeSchedule.getDuration());
					}else if (employeeTimeSchedule.getDay()==thursdaySchedule.getDay()) {
						thursdaySchedule.setStartTime(employeeTimeSchedule.getStartTime());
						thursdaySchedule.setEndTime(employeeTimeSchedule.getEndTime());
						thursdaySchedule.setDuration(employeeTimeSchedule.getDuration());
					}else if (employeeTimeSchedule.getDay()==fridaySchedule.getDay()) {
						fridaySchedule.setStartTime(employeeTimeSchedule.getStartTime());
						fridaySchedule.setEndTime(employeeTimeSchedule.getEndTime());
						fridaySchedule.setDuration(employeeTimeSchedule.getDuration());
					}
				}
			}
			
			scheduleContainer.addBean(saturdaySchdule);
			scheduleContainer.addBean(sundaySchedule);
			scheduleContainer.addBean(mondaySchedule);
			scheduleContainer.addBean(tuesdaySchedule);
			scheduleContainer.addBean(wednesdaySchedule);
			scheduleContainer.addBean(thursdaySchedule);
			scheduleContainer.addBean(fridaySchedule);
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	}
	
	private EmployeeTimeSchedule employeeSchedule;
	
	private void saveSchedule(EmployeeDto employeeDto){
		employeeSchedule = new EmployeeTimeScheduleImpl();
		
		try {
			List<EmployeeTimeSchedule> scheduleList = EmployeeTimeScheduleLocalServiceUtil.findByEmployee(employeeDto.getId());
			if (scheduleList!=null) {
				for (EmployeeTimeSchedule employeeTimeSchedule : scheduleList) {
					EmployeeTimeScheduleLocalServiceUtil.deleteEmployeeTimeSchedule(employeeTimeSchedule);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		if (scheduleContainer!=null) {
			for (int i = 0; i < scheduleContainer.size(); i++) {
				EmployeeScheduleDto scheduleDto = scheduleContainer.getIdByIndex(i);
				
				employeeSchedule.setOrganizationId(organizationId);
				employeeSchedule.setCompanyId(themeDisplay.getCompanyId());
				employeeSchedule.setEmployeeId(employeeDto.getId());
				employeeSchedule.setDay(scheduleDto.getDay());
				employeeSchedule.setStartTime(scheduleDto.getStartTime());
				employeeSchedule.setEndTime(scheduleDto.getEndTime());
				employeeSchedule.setDuration(scheduleDto.getDuration());
				
				try {
					EmployeeTimeScheduleLocalServiceUtil.addEmployeeTimeSchedule(employeeSchedule);
					window.showNotification("Schedule Saved");
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
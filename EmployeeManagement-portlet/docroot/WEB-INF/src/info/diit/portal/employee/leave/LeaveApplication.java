package info.diit.portal.employee.leave;

import info.diit.portal.constant.Day;
import info.diit.portal.employee.dto.DesignationDto;
import info.diit.portal.employee.dto.EmployeeDto;
import info.diit.portal.employee.dto.LeaveCategoryDto;
import info.diit.portal.employee.dto.LeaveDetailsDto;
import info.diit.portal.employee.dto.LeaveDto;
import info.diit.portal.model.Employee;
import info.diit.portal.model.Leave;
import info.diit.portal.model.LeaveDayDetails;
import info.diit.portal.model.impl.LeaveDayDetailsImpl;
import info.diit.portal.model.impl.LeaveImpl;
import info.diit.portal.service.EmployeeLocalServiceUtil;
import info.diit.portal.service.LeaveDayDetailsLocalServiceUtil;
import info.diit.portal.service.LeaveLocalServiceUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class LeaveApplication extends Application implements PortletRequestListener{

	private final static String COLUMN_DATE 			= "leaveDate";
	private final static String COLUMN_NUMBER_OF_DAY 	= "day";
	private final static String DATE_FORMAT 			= "dd/MM/yyyy";
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
	private LeaveDao leaveDao;
	private long organizationId;
	
	private ThemeDisplay themeDisplay;
	private User user;
	private Employee employee;
	private Window window;
	
	private Leave leave;
	private LeaveDayDetails leaveDay;
	
	private TabSheet tabSheet;

	public void init() {
		window = new Window();
		setMainWindow(window);
		leaveDao = new LeaveDao();
		user = themeDisplay.getUser();
		try {
        	organizationId = themeDisplay.getLayout().getGroup().getOrganizationId();
        	employee = EmployeeLocalServiceUtil.findByUserId(user.getUserId());
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}
		if (employee!=null) {
			window.addComponent(tabSheet());
			loadLeaveByEmployee();
		} else {
			window.addComponent(new Label("You have no employee Id"));
		}
	}
	
	private TabSheet tabSheet(){
		tabSheet = new TabSheet();
		tabSheet.addTab(mainLayout(), "Leave Form");
		tabSheet.addTab(listOfLeave(), "Leave List");
		return tabSheet;
	}
	
	private GridLayout  mainLayout;
	private TextField 	toTextField;
	private TextField 	throughTextField;
	private TextField   employeeField;
	private ComboBox    responsiblePersonComboBox;
	private TextField 	designationField;
	private TextField 	resDesignationField;
	private ComboBox    leaveCategoryComboBox;
	private TextField 	numberOfDayField;
	private TextField 	phoneNumberField;
	private DateField 	applicationDtaeField;
	private DateField   startDateField;
	private DateField 	endDateField;
	private TextArea	causeOfLeaveArea;
	private TextArea	whereLeaveEnjoyArea;
	private ComboBox 	recommendedBy1ComboBox;
	private ComboBox 	recommendedBy2ComboBox;
	/*private ComboBox 	approveBy1ComboBox;
	private ComboBox 	approveBy2ComboBox;*/
	private Button saveButton;
	
	private Table 		leaveDetailsTable;
	private BeanItemContainer<LeaveDetailsDto> leaveDetailsContainer;
	
	private BeanItemContainer<LeaveDto> leaveContainer;
	private List<LeaveCategoryDto> categoryList;
	private List<EmployeeDto> employeeList;
	
	
	private GridLayout mainLayout(){
		mainLayout 					= new GridLayout(8, 12);
		mainLayout.setWidth("100%");
		mainLayout.setSpacing(true);
		
		toTextField					= new TextField("To");
		throughTextField			= new TextField("Through");
		
		leaveCategoryComboBox		= new ComboBox("Leave Catagory");
		applicationDtaeField 		= new DateField("Application Date");
		
		employeeField		 		= new TextField("Employee");
		designationField			= new TextField("Designation");
		
		responsiblePersonComboBox	= new ComboBox("Responsible Person");
		resDesignationField			= new TextField("Responsible Designation Person");
		
		startDateField 				= new DateField("Start Date");
		endDateField 				= new DateField("End Date");
		
		numberOfDayField 			= new TextField("Number of Day");
		phoneNumberField			= new TextField("Phone Number");
		
		causeOfLeaveArea			= new TextArea("Specify cause of leave Clearly");
		whereLeaveEnjoyArea				= new TextArea("Specify where leave enjoy");
				
		recommendedBy1ComboBox		= new ComboBox("First Recommendation");
		recommendedBy2ComboBox		= new ComboBox("Second Recommendation");
		/*approveBy1ComboBox			= new ComboBox("First Approbation");
		approveBy2ComboBox			= new ComboBox("Second Approbation");*/
		
		leaveDetailsContainer		= new BeanItemContainer<LeaveDetailsDto>(LeaveDetailsDto.class);
		leaveDetailsTable			= new Table("Leave Details", leaveDetailsContainer);
		
		employeeField.setWidth("100%");
		applicationDtaeField.setWidth("100%");
		leaveCategoryComboBox.setWidth("100%");
		employeeField.setWidth("100%");
		designationField.setWidth("100%");
		responsiblePersonComboBox.setWidth("100%");
		resDesignationField.setWidth("100%");
		startDateField.setWidth("100%");
		endDateField.setWidth("100%");
		numberOfDayField.setWidth("100%");
		phoneNumberField.setWidth("100%");
		causeOfLeaveArea.setWidth("100%");
		whereLeaveEnjoyArea.setWidth("100%");
		recommendedBy1ComboBox.setWidth("100%");
		recommendedBy2ComboBox.setWidth("100%");
		toTextField.setWidth("100%");
		throughTextField.setWidth("100%");
		/*approveBy1ComboBox.setWidth("100%");
		approveBy2ComboBox.setWidth("100%");*/
		leaveDetailsTable.setWidth("70%");
		
		employeeField.setImmediate(true);
		applicationDtaeField.setImmediate(true);
		leaveCategoryComboBox.setImmediate(true);
		employeeField.setImmediate(true);
		designationField.setImmediate(true);
		responsiblePersonComboBox.setImmediate(true);
		resDesignationField.setImmediate(true);
		startDateField.setImmediate(true);
		endDateField.setImmediate(true);
		numberOfDayField.setImmediate(true);
		phoneNumberField.setImmediate(true);
		causeOfLeaveArea.setImmediate(true);
		whereLeaveEnjoyArea.setImmediate(true);
		recommendedBy1ComboBox.setImmediate(true);
		recommendedBy2ComboBox.setImmediate(true);
		/*approveBy1ComboBox.setImmediate(true);
		approveBy2ComboBox.setImmediate(true);*/
		leaveDetailsTable.setImmediate(true);
		
		toTextField.setValue("Executive Director");
		
		toTextField.setReadOnly(true);
		resDesignationField.setReadOnly(true);
		applicationDtaeField.setValue(new Date());
		applicationDtaeField.setReadOnly(true);
		numberOfDayField.setReadOnly(true);
		
		applicationDtaeField.setDateFormat(DATE_FORMAT);
		startDateField.setDateFormat(DATE_FORMAT);
		endDateField.setDateFormat(DATE_FORMAT);
		
		leaveDetailsTable.setEditable(true);
		
		leaveDetailsTable.setColumnHeader(COLUMN_DATE, "Leave Date");
		leaveDetailsTable.setColumnHeader(COLUMN_NUMBER_OF_DAY, "Day");
		
		leaveDetailsTable.setVisibleColumns(new String[]{COLUMN_DATE, COLUMN_NUMBER_OF_DAY});
		
//		default value setting
		categoryList = leaveDao.getCategory(organizationId);
		if (categoryList!=null) {
			for (LeaveCategoryDto leaveCategoryDto : categoryList) {
				leaveCategoryComboBox.addItem(leaveCategoryDto);
			}
		}
		
		employeeField.setValue(employee.getName());
		DesignationDto designationDto = leaveDao.employeeDesignation(employee.getDesignation());
		if (designationDto!=null) {
			designationField.setValue(designationDto);
		}
		
		try {
			Employee supervisor = EmployeeLocalServiceUtil.fetchEmployee(employee.getSupervisor());
			if (supervisor!=null) {
				throughTextField.setValue(supervisor.getName());
			}
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
		
		employeeField.setReadOnly(true);
		designationField.setReadOnly(true);
		throughTextField.setReadOnly(true);
//		default value setting
		
		employeeList = leaveDao.getEmployeeByOrganization(themeDisplay.getCompanyId(), organizationId);
		if (employeeList!=null) {
			for (EmployeeDto employeeDto : employeeList) {
				if (employee.getEmployeeId()!=employeeDto.getId()) {
					responsiblePersonComboBox.addItem(employeeDto);
					recommendedBy1ComboBox.addItem(employeeDto);
					recommendedBy2ComboBox.addItem(employeeDto);
				}
			}
		}
		
		responsiblePersonComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				resDesignationField.setReadOnly(false);
				EmployeeDto employeeDto = (EmployeeDto) responsiblePersonComboBox.getValue();
				try {
					Employee resEmployee = EmployeeLocalServiceUtil.fetchEmployee(employeeDto.getId());
					if (employeeDto!=null) {
						DesignationDto responsibleDesignation = leaveDao.employeeDesignation(resEmployee.getDesignation());
						resDesignationField.setValue(responsibleDesignation);
					}else{
						resDesignationField.setValue("");
					}
					
					resDesignationField.setReadOnly(true);
				} catch (SystemException e) {
					e.printStackTrace();
				}
				
			}
		});
		
		startDateField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Date startDate 	= (Date) startDateField.getValue();
				Date endDate 	= (Date) endDateField.getValue();
				if (endDate!=null) {
					if (startDate.before(endDate)) {
						detailsTable();
					}else{
						endDate = null;
//						endDateField.setValue(null);
						detailsTable();
						window.showNotification("Invalid date", Window.Notification.TYPE_ERROR_MESSAGE);
						leaveDetailsTable.removeAllItems();
					}
				}
				
			}
		});
		
		endDateField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Date startDate 	= (Date) startDateField.getValue();
				Date endDate 	= (Date) endDateField.getValue();
				if (startDate!=null) {
					if (endDate.after(startDate)) {
						detailsTable();
					} else {
//						endDateField.setValue(null);
						detailsTable();
						window.showNotification("Invalid date", Window.Notification.TYPE_ERROR_MESSAGE);
						leaveDetailsTable.removeAllItems();
					}
				}
			}
		});
		
		leaveDetailsTable.setTableFieldFactory(new DefaultFieldFactory(){
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				if (propertyId.equals(COLUMN_NUMBER_OF_DAY)) {
					ComboBox field = new ComboBox();
					field.setNullSelectionAllowed(false);
					field.setWidth("100%");
					field.setReadOnly(false);
					field.setImmediate(true);
					
					for (Day day : Day.values()) {
						field.addItem(day);
					}
					
					field.addListener(new ValueChangeListener() {
						
						@Override
						public void valueChange(ValueChangeEvent event) {
							numberOfDayField.setReadOnly(false);
							loadTotalLeave();
							numberOfDayField.setReadOnly(true);
						}
					});
					return field;
				}
				
				if (propertyId.equals(COLUMN_DATE)) {
					DateField field = new DateField();
					field.setWidth("100%");
					field.setDateFormat(DATE_FORMAT);
					field.setReadOnly(true);
					return field;
				}
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		VerticalLayout rightLayout = new VerticalLayout();
		rightLayout.setWidth("100%");
		rightLayout.setSpacing(true);
		rightLayout.addComponent(phoneNumberField);
		rightLayout.addComponent(causeOfLeaveArea);
		rightLayout.addComponent(whereLeaveEnjoyArea);
		rightLayout.addComponent(recommendedBy1ComboBox);
		rightLayout.addComponent(recommendedBy2ComboBox);
		
		HorizontalLayout rowButtonLayout = new HorizontalLayout();
		rowButtonLayout.setWidth("100%");
		rowButtonLayout.setSpacing(true);
		
		saveButton = new Button("Save");
		
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (leave==null) {
					leave = new LeaveImpl();
					leave.setNew(true);
				}
				leave.setCompanyId(themeDisplay.getCompanyId());
				leave.setOrganizationId(organizationId);
				leave.setUserId(themeDisplay.getUserId());
				
				Date appDate = (Date) applicationDtaeField.getValue();
				if (appDate!=null) {
					appDate.setHours(0);
					appDate.setMinutes(0);
					appDate.setSeconds(0);
					leave.setApplicationDate(appDate);
				}
				leave.setEmployee(employee.getEmployeeId());
				EmployeeDto responsibleEmployee = (EmployeeDto) responsiblePersonComboBox.getValue();
				if (responsibleEmployee!=null) {
					leave.setResponsibleEmployee(responsibleEmployee.getId());
				}
				
				Date startDate = (Date) startDateField.getValue();
				if (startDate!=null) {
					startDate.setHours(0);
					startDate.setMinutes(0);
					startDate.setSeconds(0);
					leave.setStartDate(startDate);
				}
				
				Date endDate = (Date) endDateField.getValue();
				if (endDate!=null) {
					endDate.setHours(0);
					endDate.setMinutes(0);
					endDate.setSeconds(0);
					leave.setEndDate(endDate);
				}
				
				String phoneNumber = (String) phoneNumberField.getValue();
				if (phoneNumber!=null) {
					leave.setPhoneNumber(phoneNumber);
				}
				
				String causeOfLeave = (String) causeOfLeaveArea.getValue();
				if (causeOfLeave!=null) {
					leave.setCauseOfLeave(causeOfLeave);
				}
				
				String whereEnjoy = (String) whereLeaveEnjoyArea.getValue();
				if (whereEnjoy!=null) {
					leave.setWhereEnjoy(whereEnjoy);
				}
				
				LeaveCategoryDto leaveCategory = (LeaveCategoryDto) leaveCategoryComboBox.getValue();
				if (leaveCategory!=null) {
					leave.setLeaveCategory(leaveCategory.getId());
				}
				
				EmployeeDto recomDto1 = (EmployeeDto) recommendedBy1ComboBox.getValue();
				if (recomDto1!=null) {
					leave.setFirstRecommendation(recomDto1.getId());
				}
				
				EmployeeDto recomDto2 = (EmployeeDto) recommendedBy2ComboBox.getValue();
				if (recomDto2!=null) {
					leave.setSecondRecommendation(recomDto2.getId());
				}
				
				double totalLeave = (double) numberOfDayField.getValue();
				if (totalLeave>0) {
					leave.setNumberOfDay(totalLeave);
				}
				try {
					if (checkLeaveDetails()==true) {
						if (leave.isNew()) {
							leave.setCreateDate(new Date());
							LeaveLocalServiceUtil.addLeave(leave);
							saveLeaveDetails(leave);
							loadLeaveByEmployee();
							window.showNotification("Leave application submitted successfully",Window.Notification.TYPE_WARNING_MESSAGE);
						}else{
							leave.setModifiedDate(new Date());
							LeaveLocalServiceUtil.updateLeave(leave);
							saveLeaveDetails(leave);
							loadLeaveByEmployee();
							window.showNotification("Leave application updated successfully", Window.Notification.TYPE_WARNING_MESSAGE);
						}
					}else{
						window.showNotification("Please provide leave details", Window.Notification.TYPE_ERROR_MESSAGE);
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
		
		Label spaceLabel = new Label();
		
		Button resetButton = new Button("Reset");
		resetButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				clear();
			}
		});
		
		rowButtonLayout.addComponent(spaceLabel);
		rowButtonLayout.addComponent(saveButton);
		rowButtonLayout.addComponent(resetButton);
		rowButtonLayout.setExpandRatio(spaceLabel, 1);
		
		mainLayout.addComponent(toTextField, 0, 0, 2, 0);
		mainLayout.addComponent(throughTextField, 0, 1, 2, 1);
		
		mainLayout.addComponent(employeeField, 0, 2, 2, 2);
		mainLayout.addComponent(responsiblePersonComboBox, 0, 3, 2, 3);
		mainLayout.addComponent(startDateField, 0, 4, 2, 4);
		mainLayout.addComponent(numberOfDayField, 0, 5, 2, 5);
				
		mainLayout.addComponent(applicationDtaeField, 4, 1, 7, 1);
		mainLayout.addComponent(designationField, 4, 2, 7, 2);
		mainLayout.addComponent(resDesignationField, 4, 3, 7, 3);
		mainLayout.addComponent(endDateField, 4, 4, 7, 4);
		mainLayout.addComponent(leaveCategoryComboBox, 4, 5, 7, 5);
		
		mainLayout.addComponent(leaveDetailsTable, 0, 6, 3, 6);
		mainLayout.addComponent(rightLayout, 4, 6, 7, 6);
		mainLayout.addComponent(new Label("<hr />",Label.CONTENT_XHTML), 0, 7, 7, 7);
		
		mainLayout.addComponent(rowButtonLayout, 0, 8, 7, 8);
		
		/*mainLayout.addComponent(recommendedBy1ComboBox, 0, 9, 2, 9);
		mainLayout.addComponent(recommendedBy2ComboBox, 4, 9, 7, 9);
		
		mainLayout.addComponent(approveBy1ComboBox, 0, 10, 2, 10);
		mainLayout.addComponent(approveBy2ComboBox, 4, 10, 7, 10);*/
		
		return mainLayout;
	}
	
	private void saveLeaveDetails(Leave lv){
		if (leaveDay==null) {
			leaveDay = new LeaveDayDetailsImpl();
		}
		try {
			if (lv!=null) {
				List<LeaveDayDetails> leaveDayList = LeaveDayDetailsLocalServiceUtil.findByLeave(lv.getLeaveId());
				if (leaveDayList!=null) {
					for (LeaveDayDetails leaveDay : leaveDayList) {
						LeaveDayDetailsLocalServiceUtil.deleteLeaveDayDetails(leaveDay);
					}
				}
				leaveDay.setLeaveId(lv.getLeaveId());
				if (leaveDetailsContainer!=null) {
					for (int i = 0; i < leaveDetailsContainer.size(); i++) {
						LeaveDetailsDto leaveDayDto = leaveDetailsContainer.getIdByIndex(i);
						Date lvDate = leaveDayDto.getLeaveDate();
						if (lvDate!=null) {
							lvDate.setHours(0);
							lvDate.setMinutes(0);
							lvDate.setSeconds(0);
							leaveDay.setLeaveDate(lvDate);
						}
						Day day = leaveDayDto.getDay();
						if (day.getKey()==1) {
							leaveDay.setDay(1);
						}else{
							leaveDay.setDay(0.5);
						}
						LeaveDayDetailsLocalServiceUtil.addLeaveDayDetails(leaveDay);
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private boolean checkLeaveDetails(){
		boolean status = false;
		if (leaveDetailsContainer!=null) {
			for (int i = 0; i < leaveDetailsContainer.size(); i++) {
				LeaveDetailsDto detailsDto = leaveDetailsContainer.getIdByIndex(i);
				if (detailsDto.getDay()!=null) {
					status = true;
				}else{
					status = false;
				}
			}
		}
		return status;
	}
	
	private void loadTotalLeave(){
		if (leaveDetailsContainer!=null) {
			numberOfDayField.setValue("");
			double totalLeave = 0;
			for (int i = 0; i < leaveDetailsContainer.size(); i++) {
				LeaveDetailsDto detailsDto = leaveDetailsContainer.getIdByIndex(i);
				if (detailsDto.getDay()!=null) {
					Day day = detailsDto.getDay();
					if (day.getKey()==1) {
						totalLeave = totalLeave+1;
					}else{
						totalLeave = totalLeave+0.5;
					}
				}
			}
			numberOfDayField.setValue(totalLeave);
		}
	}
	
	private void detailsTable(){
		
		final Date startDate 	= (Date) startDateField.getValue();
		Date endDate 	= (Date) endDateField.getValue();
		if (startDate!=null && endDate!=null) {
			leaveDetailsContainer.removeAllItems();
			Calendar cal = Calendar.getInstance();
			cal.setTime(startDate);
			int totalDay = daysBetween(startDate, endDate);
			for (int i = 0; i <= totalDay; i++) {
				LeaveDetailsDto detailsDto = new LeaveDetailsDto();
				detailsDto.setId(i);
				detailsDto.setLeaveDate(cal.getTime());
				leaveDetailsContainer.addBean(detailsDto);
				cal.add(Calendar.DAY_OF_MONTH, 1);
			}
			
		}
	}
	
	private Table leaveTable;
	private final static String COLUMN_EMPLOYEE 	= "employee";
	private final static String COLUMN_START_DATE 	= "startDate";
	private final static String COLUMN_END_DATE 	= "endDate";
	private final static String COLUMN_TOTAL_DATE 	= "totalDay";
	private final static String COLUMN_COMMENTS 	= "comments";
	private final static String COLUMN_STATUS	 	= "status";
	private final static String COLUMN_COPLEMENTED 	= "complementedBy";
	
	private VerticalLayout listOfLeave(){
		
		VerticalLayout listLayout = new VerticalLayout();
		listLayout.setWidth("100%");
		listLayout.setSpacing(true);
		
		leaveContainer = new BeanItemContainer<LeaveDto>(LeaveDto.class);
		leaveTable = new Table("List of Leave", leaveContainer)
		{
			@Override
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				if (property.getType()==Date.class) {
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
					return simpleDateFormat.format(property.getValue());
				}
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		leaveTable.setWidth("100%");
		leaveTable.setHeight("70%");
		
		leaveTable.setColumnHeader(COLUMN_EMPLOYEE, "Employee");
		leaveTable.setColumnHeader(COLUMN_START_DATE, "Start Date");
		leaveTable.setColumnHeader(COLUMN_END_DATE, "End Date");
		leaveTable.setColumnHeader(COLUMN_TOTAL_DATE, "Total Date");
		leaveTable.setColumnHeader(COLUMN_COMMENTS, "Comments");
		leaveTable.setColumnHeader(COLUMN_STATUS, "Status");
		leaveTable.setColumnHeader(COLUMN_COPLEMENTED, "Complemented By");
		
		leaveTable.setVisibleColumns(new String[]{COLUMN_EMPLOYEE, COLUMN_START_DATE, COLUMN_END_DATE, COLUMN_TOTAL_DATE, COLUMN_COMMENTS, COLUMN_STATUS, COLUMN_COPLEMENTED});
		
		leaveTable.setImmediate(true);
		leaveTable.setSelectable(true);
		
		final Button editButton = new Button("Edit");
		editButton.setVisible(false);
		final Button withdrawButton = new Button("Withdraw");
		withdrawButton.setVisible(false);
		final Button viewButton = new Button("View");
		viewButton.setVisible(false);
		
		leaveTable.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				LeaveDto leaveDto = (LeaveDto) leaveTable.getValue();
				Date newDate = new Date();
				newDate.setHours(0);
				newDate.setMinutes(0);
				newDate.setSeconds(0);
				Date startDate = leaveDto.getStartDate();
				if (leaveDto!=null) {
					if (newDate.before(startDate)) {
						editButton.setVisible(true);
						withdrawButton.setVisible(true);
					}else{
						editButton.setVisible(false);
						withdrawButton.setVisible(false);
					}
					viewButton.setVisible(true);
				}
			}
		});
		
		editButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				LeaveDto leave = (LeaveDto) leaveTable.getValue();
				editLeave(leave.getId());
				tabSheet.setSelectedTab(mainLayout);
			}
		});
		
		viewButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				LeaveDto leave = (LeaveDto) leaveTable.getValue();
				editLeave(leave.getId());
				tabSheet.setSelectedTab(mainLayout);
				saveButton.setVisible(false);
			}
		});
		
		withdrawButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				LeaveDto leaveDto = (LeaveDto) leaveTable.getValue();
				try {
					leave = LeaveLocalServiceUtil.fetchLeave(leaveDto.getId());
					leave.setApplicationStatus(3);
					LeaveLocalServiceUtil.updateLeave(leave);
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
		
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setWidth("100%");
		buttonLayout.setSpacing(true);
		Label spacingLabel = new Label();
		buttonLayout.addComponent(spacingLabel);
		buttonLayout.addComponent(editButton);
		buttonLayout.addComponent(withdrawButton);
		buttonLayout.addComponent(viewButton);
		buttonLayout.setExpandRatio(spacingLabel, 1);
		
		listLayout.addComponent(leaveTable);
		listLayout.addComponent(buttonLayout);
		
		return listLayout;
	}
	
	private void loadLeaveByEmployee(){
		leaveContainer.removeAllItems();
		List<LeaveDto> leaveList = leaveDao.getLeaveByEmployee(employee.getEmployeeId());
		if (leaveList!=null) {
			for (LeaveDto leaveDto : leaveList) {
				leaveContainer.addBean(leaveDto);
			}
		}
	}
	
	private void editLeave(long leaveId){
		try {
			applicationDtaeField.setReadOnly(false);
			leave = LeaveLocalServiceUtil.fetchLeave(leaveId);
			leaveCategoryComboBox.setValue(getCatagory(leave.getLeaveCategory()));
			applicationDtaeField.setValue(leave.getApplicationDate());
			responsiblePersonComboBox.setValue(getResponsiblePerson(leave.getResponsibleEmployee()));
			recommendedBy1ComboBox.setValue(getResponsiblePerson(leave.getFirstRecommendation()));
			recommendedBy2ComboBox.setValue(getResponsiblePerson(leave.getSecondRecommendation()));
			
			startDateField.setValue(leave.getStartDate());
			endDateField.setValue(leave.getEndDate());
			phoneNumberField.setValue(leave.getPhoneNumber());
			causeOfLeaveArea.setValue(leave.getCauseOfLeave());
			whereLeaveEnjoyArea.setValue(leave.getWhereEnjoy());
			
			List<LeaveDayDetails> leaveDayList = LeaveDayDetailsLocalServiceUtil.findByLeave(leave.getLeaveId());
			if (leaveDayList!=null) {
				leaveDetailsContainer.removeAllItems();
				for (LeaveDayDetails leaveDayDetails : leaveDayList) {
					LeaveDetailsDto detailsDto = new LeaveDetailsDto();
					detailsDto.setLeaveDate(leaveDayDetails.getLeaveDate());
					if (leaveDayDetails.getDay()==1) {
						detailsDto.setDay(getDay(1));
					}else{
						detailsDto.setDay(getDay(2));
					}
					leaveDetailsContainer.addBean(detailsDto);
				}
			}
			applicationDtaeField.setReadOnly(true);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private Day getDay(int d){
		for (Day day : Day.values()) {
			if (day.getKey()==d) {
				return day;
			}
		}
		return null;
	}
	
	private LeaveCategoryDto getCatagory(long l){
		if (categoryList!=null) {
			for (LeaveCategoryDto category : categoryList) {
				if (category.getId()==l) {
					return category;
				}
			}
		}
		return null;
	}
	
	private EmployeeDto getResponsiblePerson(long employeeId){
		if (employeeList!=null) {
			for (EmployeeDto employee : employeeList) {
				if (employee.getId() == employeeId) {
					return employee;
				}
			}
		}
		return null;
	}
	
	public int daysBetween(Date d1, Date d2){
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	}
	
	private void clear(){
		leave = null;
		resDesignationField.setReadOnly(false);
		applicationDtaeField.setReadOnly(false);
		numberOfDayField.setReadOnly(false);
		
		leaveDetailsContainer.removeAllItems();
		leaveCategoryComboBox.setValue(null);
		
		applicationDtaeField.setValue(new Date());
//		responsiblePersonComboBox.setValue(null);
		startDateField.setValue(new Date());
		endDateField.setValue(new Date());
//		resDesignationField.setValue("");
		phoneNumberField.setValue("");
		causeOfLeaveArea.setValue("");
		whereLeaveEnjoyArea.setValue("");
		numberOfDayField.setValue("");
		recommendedBy1ComboBox.setValue(null);
		recommendedBy2ComboBox.setValue(null);
		
		applicationDtaeField.setReadOnly(true);
		resDesignationField.setReadOnly(true);
		numberOfDayField.setReadOnly(true);
		saveButton.setVisible(true);
	}

	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}
	
}

package info.diit.portal.employee;

import info.diit.portal.NoSuchDayTypeException;
import info.diit.portal.employee.dto.DayTypeDto;
import info.diit.portal.model.DayType;
import info.diit.portal.model.impl.DayTypeImpl;
import info.diit.portal.service.DayTypeLocalServiceUtil;

import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class HolidayTypeApplication extends Application implements PortletRequestListener {
	
	private DayType dayType;
	private ThemeDisplay themeDisplay;
	
	private long organizationId;
	private Window window;

    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        
        try {
			organizationId = themeDisplay.getLayout().getGroup().getOrganizationId();
			List<DayType> dayTypes = DayTypeLocalServiceUtil.findByOrganization(organizationId);
			if (dayTypes.size()==0) {
				DayType workingDay = new DayTypeImpl();
				workingDay.setOrganizationId(organizationId);
				workingDay.setType("WorkingDay");
				DayTypeLocalServiceUtil.addDayType(workingDay);
				
				DayType hartal = new DayTypeImpl();
				hartal.setOrganizationId(organizationId);
				hartal.setType("Hartal");
				DayTypeLocalServiceUtil.addDayType(hartal);
				
				DayType weekend = new DayTypeImpl();
				weekend.setOrganizationId(organizationId);
				weekend.setType("Weekend");
				DayTypeLocalServiceUtil.addDayType(weekend);
			}
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
        
        window.addComponent(mainLayout());
    }
    
    private TextField typeField;
    private Table dayTable;
    private BeanItemContainer<DayTypeDto> typeContainer;
    
    private final static String TYPE = "type";
    
    private VerticalLayout mainLayout(){
    	VerticalLayout mainLayout = new VerticalLayout();
    	mainLayout.setWidth("100%");
    	mainLayout.setSpacing(true);
    	
    	typeField = new TextField("Holiday Type Title");
    	typeField.setWidth("50%");
    	typeField.setRequired(true);
    	Button saveButton = new Button("Save");
    	
    	typeContainer = new BeanItemContainer<DayTypeDto>(DayTypeDto.class);
    	dayTable = new Table("Holiday Type List", typeContainer);
    	loadType();
    	dayTable.setWidth("50%");
    	
    	dayTable.setColumnHeader("Type", TYPE);
    	dayTable.setVisibleColumns(new String[]{TYPE});
    	dayTable.setImmediate(true);
    	dayTable.setSelectable(true);
    	
    	Button editButton = new Button("Edit");
    	
    	mainLayout.addComponent(typeField);
    	mainLayout.addComponent(saveButton);
    	mainLayout.addComponent(dayTable);
    	mainLayout.addComponent(editButton);
    	
    	saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (dayType==null) {
					dayType = new DayTypeImpl();
					dayType.isNew();
				}
				
				dayType.setOrganizationId(organizationId);
				
				String type = (String) typeField.getValue();
				if (type!=null) {
					dayType.setType(type);
				}
				
				try {
					if (!checkType(dayType)) {
						if (dayType.isNew()) {
							DayTypeLocalServiceUtil.addDayType(dayType);
							window.showNotification("Day type added successfully");
						}else{
							DayTypeLocalServiceUtil.updateDayType(dayType);
							window.showNotification("Day type updated successfully");
						}
					
						loadType();
						dayType = null;
						typeField.setValue("");
					}else{
						window.showNotification("Type already exist", Window.Notification.TYPE_ERROR_MESSAGE);
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
    	
    	editButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				DayTypeDto dayType = (DayTypeDto) dayTable.getValue();
				if (dayType!=null) {
					if (dayType.getType().equalsIgnoreCase("WorkingDay") || dayType.getType().equalsIgnoreCase("Hartal") || dayType.getType().equalsIgnoreCase("Weekend")) {
						window.showNotification("WorkingDay/Hartal/Weekend is unchangable.", Window.Notification.TYPE_ERROR_MESSAGE);
					}else{
						editDayType(dayType.getId());
					}
					
				}
			}
		});
    	
    	return mainLayout;
    }
    
    private boolean checkType(DayType type){
    	boolean status = false;
    	
    	try {
			DayType dayType = DayTypeLocalServiceUtil.findByTypeOrganization(type.getType(), organizationId);
			if (dayType.getDayTypeId()!=type.getDayTypeId()) {
				if (dayType!=null) {
					status = true;
				}else{
					status = false;
				}
			}else{
				status = false;
			}
		} catch (NoSuchDayTypeException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
    	return status;
    }
    
    private void editDayType(long dayTypeId){
    	try {
			dayType = DayTypeLocalServiceUtil.fetchDayType(dayTypeId);
			typeField.setValue(dayType.getType());
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
    private void loadType(){
    	typeContainer.removeAllItems();
    	try {
			List<DayType> dayTypeList = DayTypeLocalServiceUtil.findByOrganization(organizationId);
			if (dayTypeList!=null) {
				for (DayType dayType : dayTypeList) {
					DayTypeDto dayTypeDto = new DayTypeDto();
					dayTypeDto.setId(dayType.getDayTypeId());
					dayTypeDto.setType(dayType.getType());
					typeContainer.addBean(dayTypeDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }

	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

package info.diit.portal.employee;

import info.diit.portal.NoSuchDayTypeException;
import info.diit.portal.NoSuchWorkingDayException;
import info.diit.portal.employee.dto.DayTypeDto;
import info.diit.portal.employee.dto.HolidayDto;
import info.diit.portal.model.DayType;
import info.diit.portal.model.WorkingDay;
import info.diit.portal.model.impl.WorkingDayImpl;
import info.diit.portal.service.DayTypeLocalService;
import info.diit.portal.service.DayTypeLocalServiceUtil;
import info.diit.portal.service.WorkingDayLocalServiceUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component.Event;
import com.vaadin.ui.Component.Listener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class HolidayApplication extends Application implements PortletRequestListener {
	
	private long organizationId;
	private ThemeDisplay themeDisplay;
	
	private Window window;

    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        try {
			organizationId = themeDisplay.getLayout().getGroup().getOrganizationId();
			Calendar cal = Calendar.getInstance();
			Date date = cal.getTime();
			date.setHours(0);
			date.setMinutes(0);
			date.setSeconds(0);
			WorkingDay wd = WorkingDayLocalServiceUtil.findWorkingDay(organizationId, date);
		} catch (PortalException e) {
			saveWorkingCalendar();
		} catch (SystemException e) {
			e.printStackTrace();
		}
        window.addComponent(mainLayout());
        loadHoliday();
        loadType();
    }
    
    List<DayTypeDto> typeList;
    
   /* private ComboBox durationComboBox;
    private DateField startDateField;
    private DateField endDateField;*/
    
    private final static String HOLIDAY_TYPE 	= "holidayType";
    private final static String HOLIDAY_DATE 	= "date";
    private final static String HOLIDAY_NUMBER  = "weekNumber";
    
    private BeanItemContainer<HolidayDto> holidayContainer;
    private Table holidayTable;
    
	private final static String ITEM_THIS_WEEK = "This Week";
	private final static String ITEM_LAST_WEEK = "Last Week";
	private final static String ITEM_THIS_MONTH = "This Month";
	private final static String ITEM_THIS_YEAR = "This Year";
	
	private final static String DATE_FORMAT = "dd/MM/yyyy";
    
    private GridLayout mainLayout(){
    	GridLayout mainLayout = new GridLayout(9, 9);
    	mainLayout.setSpacing(true);
    	mainLayout.setWidth("100%");
    	
		
		holidayContainer = new BeanItemContainer<HolidayDto>(HolidayDto.class);
		holidayTable = new Table("", holidayContainer);
		holidayTable.setWidth("100%");
		holidayTable.setSelectable(true);
		holidayTable.setColumnHeader(HOLIDAY_TYPE, "Holiday Type");
		holidayTable.setColumnHeader(HOLIDAY_DATE, "Date");
		holidayTable.setColumnHeader(HOLIDAY_NUMBER, "Number");
		
		holidayTable.setVisibleColumns(new String[]{HOLIDAY_DATE, HOLIDAY_NUMBER, HOLIDAY_TYPE});
		
		Button deleteButton = new Button("Delete");
		
		Button addHolidayButton = new Button("Add Holiday");
		addHolidayButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				VerticalLayout layout = new VerticalLayout();
				layout.setMargin(true);
				layout.setSpacing(true);
				final Window popUpWindow = new Window(null, layout);
				layout.addComponent(formLayout());
				popUpWindow.setWidth("30%");
				popUpWindow.center();
				popUpWindow.setModal(true);
				getMainWindow().addWindow(popUpWindow);
			}
		});
		
		deleteButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
								
				try {
					HolidayDto holidayDto = (HolidayDto) holidayTable.getValue();
					if (holidayDto!=null) {
						workingDay = WorkingDayLocalServiceUtil.fetchWorkingDay(holidayDto.getId());
						try {
							DayType dayType = DayTypeLocalServiceUtil.findByTypeOrganization("WorkingDay", organizationId);
							workingDay.setStatus(dayType.getDayTypeId());
						} catch (NoSuchDayTypeException e) {
							e.printStackTrace();
						}
						WorkingDayLocalServiceUtil.updateWorkingDay(workingDay);
						window.showNotification("Working day deleted successfully");
						loadHoliday();
					}else{
						window.showNotification("Select a row", Window.Notification.TYPE_WARNING_MESSAGE);
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
				
				
			}
		});
		
		/*mainLayout.addComponent(durationComboBox, 0, 0, 2, 0);
		mainLayout.addComponent(startDateField, 3, 0, 5, 0);
		mainLayout.addComponent(endDateField, 6, 0, 8, 0);*/
		
		mainLayout.addComponent(addHolidayButton, 0, 0, 2, 0);
		mainLayout.addComponent(holidayTable, 0, 1, 8, 5);
		mainLayout.addComponent(deleteButton, 0, 6, 8, 6);
    	
    	return mainLayout;
    }
    
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
    
    private void loadHoliday(){
    	try {
    		holidayContainer.removeAllItems();
			List<WorkingDay> workingDayList = WorkingDayLocalServiceUtil.findByOrganization(organizationId);
			if (workingDayList!=null) {
				for (WorkingDay workingDay : workingDayList) {
					
					HolidayDto holiday = new HolidayDto();
					holiday.setId(workingDay.getWorkingDayId());
					holiday.setDate(simpleDateFormat.format(workingDay.getDate()));
					
					DayType dayType = DayTypeLocalServiceUtil.fetchDayType(workingDay.getStatus());
					
					DayTypeDto typeDto = new DayTypeDto();
					typeDto.setId(dayType.getDayTypeId());
					typeDto.setType(dayType.getType());
					
					holiday.setHolidayType(typeDto);
					holiday.setWeekNumber(workingDay.getWeek());
					
					DayType dtWorkingDay = DayTypeLocalServiceUtil.findByTypeOrganization("WorkingDay", organizationId);
					DayType dtWeekend = DayTypeLocalServiceUtil.findByTypeOrganization("Weekend", organizationId);
					
					if (holiday.getHolidayType().getId()!=dtWorkingDay.getDayTypeId() && holiday.getHolidayType().getId()!=dtWeekend.getDayTypeId()) {
						holidayContainer.addBean(holiday);
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (NoSuchDayTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private ComboBox typeComboBox;
    private DateField holidayField;
    private TextArea commentArea;
    private Button saveButton;
    
    private WorkingDay workingDay;
    
    private VerticalLayout formLayout(){
    	VerticalLayout formLayout = new VerticalLayout();
    	
    	typeComboBox = new ComboBox("Holiday Type");
    	typeComboBox.setImmediate(true);
    	typeComboBox.setRequired(true);
    	
    	
    	if (typeList!=null) {
			for (DayTypeDto typeDto : typeList) {
				typeComboBox.addItem(typeDto);
			}
		}
    	
    	
    	holidayField = new DateField("Date");
    	holidayField.setDateFormat(DATE_FORMAT);
    	holidayField.setRequired(true);
    	commentArea = new TextArea("Comment");
    	saveButton = new Button("Save");
    	
    	saveButton.addListener(new Listener() {
			
			
			@Override
			public void componentEvent(Event event) {
				
				
				Date holiday = (Date) holidayField.getValue();
				holiday.setHours(0);
				holiday.setMinutes(0);
				holiday.setSeconds(0);
				if (holiday!=null) {
					try {
						workingDay = WorkingDayLocalServiceUtil.findWorkingDay(organizationId, holiday);
						
						DayTypeDto type = (DayTypeDto) typeComboBox.getValue();
						if (type!=null) {
							workingDay.setStatus(type.getId());
						}else{
							window.showNotification("Select a type", Window.Notification.TYPE_WARNING_MESSAGE);
							return;
						}
						
						WorkingDayLocalServiceUtil.updateWorkingDay(workingDay);
						window.showNotification("Working day updated successfully");
						loadHoliday();
					} catch (NoSuchWorkingDayException e) {
						e.printStackTrace();
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}else{
					window.showNotification("Holiday can't be null", Window.Notification.TYPE_WARNING_MESSAGE);
					return;
				}
			}
		});
    	
    	formLayout.addComponent(typeComboBox);
    	formLayout.addComponent(holidayField);
    	formLayout.addComponent(saveButton); 
    	
    	return formLayout;
    }
    
    private void loadType(){
    	try {
			List<DayType> dayTypeList = DayTypeLocalServiceUtil.findByOrganization(organizationId);
			typeList = new ArrayList<DayTypeDto>();
			if (dayTypeList!=null) {
				for (DayType dayType : dayTypeList) {
					DayTypeDto typeDto = new DayTypeDto();
					typeDto.setId(dayType.getDayTypeId());
					typeDto.setType(dayType.getType());
					typeList.add(typeDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
    private void saveWorkingCalendar(){
    	workingDay = new WorkingDayImpl();
    	
    	workingDay.setOrganizationId(organizationId);
		Calendar calendar = Calendar.getInstance();
		calendar.setFirstDayOfWeek(Calendar.SATURDAY);
		int totalDayOfYear = calendar.getActualMaximum(Calendar.DAY_OF_YEAR);
		calendar.set(Calendar.DAY_OF_YEAR, 3);
		
		
	    for (int i = 1; i <= totalDayOfYear; i++) {
			calendar.set(Calendar.DAY_OF_YEAR, i);
			Date date = calendar.getTime();
			date.setHours(0);
			date.setMinutes(0);
			date.setSeconds(0);
//			window.showNotification("It is working......>>>>>>>>>>>>"+totalDayOfYear+"Date::::::::::::::::"+calendar.getTime());
			int WEEK_OF_YEAR = calendar.get(Calendar.WEEK_OF_YEAR);
			int DAY_OF_WEEK = calendar.get(Calendar.DAY_OF_WEEK);
			
			workingDay.setDate(date);
			workingDay.setWeek(WEEK_OF_YEAR);
			workingDay.setDayOfWeek(DAY_OF_WEEK);
			try {
				if (Calendar.FRIDAY==DAY_OF_WEEK) {
					DayType dtWeekend = DayTypeLocalServiceUtil.findByTypeOrganization("Weekend", organizationId);
					workingDay.setStatus(dtWeekend.getDayTypeId());
				}else {
					DayType dtWorkingDay = DayTypeLocalServiceUtil.findByTypeOrganization("WorkingDay", organizationId);
					workingDay.setStatus(dtWorkingDay.getDayTypeId());
				}
			
			
				try {
					WorkingDay wd = WorkingDayLocalServiceUtil.findWorkingDay(organizationId, date);
				} catch (NoSuchWorkingDayException e) {
					WorkingDayLocalServiceUtil.addWorkingDay(workingDay);
				}
			} catch (NoSuchDayTypeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }

	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}

}

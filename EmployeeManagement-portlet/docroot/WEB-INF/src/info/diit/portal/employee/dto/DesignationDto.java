package info.diit.portal.employee.dto;


public class DesignationDto {

	private long id;
	private String title;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String toString(){
		return getTitle();
	}
}

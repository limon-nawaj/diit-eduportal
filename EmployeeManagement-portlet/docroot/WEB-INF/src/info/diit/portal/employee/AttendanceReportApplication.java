package info.diit.portal.employee;


import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;

public class AttendanceReportApplication extends Application implements PortletRequestListener {

	
	private Window window;
	private ThemeDisplay themeDisplay;
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        window.addComponent(maniLayout());
    }
    
    private DateField startDateField;
    private DateField endDateField;
    private Table attendanceTable;
    
    private GridLayout maniLayout(){
    	GridLayout gLayout = new GridLayout(8, 8);
    	
    	startDateField 	= new DateField("Start Date");
    	endDateField 	= new DateField("End Date");
    	
    	return gLayout;
    }

	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}

}

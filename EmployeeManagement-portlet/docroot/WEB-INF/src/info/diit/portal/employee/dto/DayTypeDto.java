package info.diit.portal.employee.dto;

public class DayTypeDto {
	private long id;
	private String type;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return getType();
	}
	
}

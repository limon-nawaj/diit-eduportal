package info.diit.portal.employee;

import info.diit.portal.NoSuchDayTypeException;
import info.diit.portal.NoSuchEmployeeException;
import info.diit.portal.NoSuchLeaveDayDetailsException;
import info.diit.portal.employee.dto.AttendanceCorrectionDto;
import info.diit.portal.employee.dto.EmployeeAttendanceDto;
import info.diit.portal.model.AttendanceCorrection;
import info.diit.portal.model.DayType;
import info.diit.portal.model.Employee;
import info.diit.portal.model.EmployeeAttendance;
import info.diit.portal.model.Leave;
import info.diit.portal.model.LeaveDayDetails;
import info.diit.portal.model.WorkingDay;
import info.diit.portal.model.impl.AttendanceCorrectionImpl;
import info.diit.portal.service.AttendanceCorrectionLocalService;
import info.diit.portal.service.AttendanceCorrectionLocalServiceUtil;
import info.diit.portal.service.DayTypeLocalServiceUtil;
import info.diit.portal.service.EmployeeAttendanceLocalServiceUtil;
import info.diit.portal.service.EmployeeLocalServiceUtil;
import info.diit.portal.service.LeaveDayDetailsLocalServiceUtil;
import info.diit.portal.service.LeaveLocalServiceUtil;
import info.diit.portal.service.WorkingDayLocalServiceUtil;
import info.diit.portal.service.impl.AttendanceCorrectionLocalServiceImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class EmployeeWeeklyAttendanceApp extends Application implements PortletRequestListener {

	private ThemeDisplay themeDisplay;
	private Window window;
	private BeanItemContainer<EmployeeAttendanceDto> attendanceContainer;
	private Table attendanceTable;
	private long employeeId;
	private long organizationId;
	
	private Window popUpWindow;
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
		try {
			try {
				organizationId = themeDisplay.getLayout().getGroup().getOrganizationId();
			} catch (PortalException e) {
				e.printStackTrace();
			}
			Employee emp = EmployeeLocalServiceUtil.findByUserId(themeDisplay.getUser().getUserId());
			if (emp!=null) {
				employeeId = emp.getEmployeeId();
			}
			
		} catch (NoSuchEmployeeException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
        window.addComponent(tabSheet());
    }
    
    private TabSheet tabSheet(){
    	TabSheet tabSheet = new TabSheet();
    	
    	tabSheet.addTab(mainLayout(), "Weekly Attendance");
    	tabSheet.addTab(requestList(), "Request");
    	return tabSheet;
    }
    
    private final static String DATE 			= "date";
    private final static String DAY_OF_WEEK 	= "dayName";
    private final static String EXPECTED_IN 	= "expectedIn";
    private final static String REAL_IN 		= "realIn";
    private final static String EXPECTED_OUT 	= "expectedOut";
    private final static String REAL_OUT 		= "realOut";
    private final static String REAL_DAILY_DURATION = "realDailyDuration";
    private final static String EXPECTED_DAILY_DURATION = "expectedDailyDuration";
    private final static String DAY_STATUS = "status";
    
    private final static String THIS_WEEK = "This Week";
	private final static String LAST_WEEK = "Last Week";
	
	private ComboBox durationComboBox;
	private Button requestButton;
	
    private GridLayout mainLayout(){
    	GridLayout mainLayout = new GridLayout(8, 8);
    	mainLayout.setWidth("100%");
    	mainLayout.setHeight("50%");
    	
    	durationComboBox = new ComboBox("Week");
    	durationComboBox.setWidth("30%");
    	durationComboBox.setImmediate(true);
    	durationComboBox.setNullSelectionAllowed(false);
    	durationComboBox.addItem(THIS_WEEK);
    	durationComboBox.addItem(LAST_WEEK);
    	durationComboBox.setValue(THIS_WEEK);
    	
    	requestButton = new Button("Request For Correction");
    	
//    	current week
    	Calendar calendar = Calendar.getInstance();
		
    	calendar.setFirstDayOfWeek(Calendar.SATURDAY);
		int firstDayOfWeek = calendar.getFirstDayOfWeek();
		
		int days = (calendar.get(Calendar.DAY_OF_WEEK)+14-firstDayOfWeek)%7;
		calendar.add(Calendar.DATE, -days);
		
		Date startDateOfWeek = calendar.getTime();
		startDateOfWeek.setHours(0);
		startDateOfWeek.setMinutes(0);
		startDateOfWeek.setSeconds(0);
		
		calendar.add(Calendar.DATE, 6);
		Date endDateOfWeek = calendar.getTime();
		endDateOfWeek.setHours(0);
		endDateOfWeek.setMinutes(0);
		endDateOfWeek.setSeconds(0);
    	
    	attendanceContainer = new BeanItemContainer<EmployeeAttendanceDto>(EmployeeAttendanceDto.class);
    	
    	Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());

    	attendanceTable = new Table("Attendance List", attendanceContainer);
    	
    	attendanceTable.setImmediate(true);
    	attendanceTable.setWidth("100%");
    	/*attendanceTable.setWidth("100%");
    	attendanceTable.setHeight("50%");*/
    	
    	
    	attendanceTable.setColumnHeader(DATE, "Date");
    	attendanceTable.setColumnHeader(EXPECTED_IN, "Expected In");
    	attendanceTable.setColumnHeader(REAL_IN, "Real In");
    	attendanceTable.setColumnHeader(EXPECTED_OUT, "Expected Out");
    	attendanceTable.setColumnHeader(REAL_OUT, "Real Out");
    	attendanceTable.setColumnHeader(REAL_DAILY_DURATION, "Real Duration");
    	attendanceTable.setColumnHeader(EXPECTED_DAILY_DURATION, "Minimum Duration");
    	attendanceTable.setColumnHeader(DAY_STATUS, "Status");
    	
    	try {
			Employee emp = EmployeeLocalServiceUtil.fetchEmployee(employeeId);
			if (emp.getDuration()>0) {
				attendanceTable.setVisibleColumns(new String[]{DATE, REAL_IN, REAL_OUT, REAL_DAILY_DURATION, EXPECTED_DAILY_DURATION, DAY_STATUS});
			}else{
				attendanceTable.setVisibleColumns(new String[]{DATE, EXPECTED_IN, REAL_IN, EXPECTED_OUT, REAL_OUT, REAL_DAILY_DURATION, EXPECTED_DAILY_DURATION, DAY_STATUS});
			}
		} catch (SystemException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	
    	attendanceTable.setFooterVisible(true);
    	
    	loadAttendance(startDateOfWeek, endDateOfWeek);
    	durationComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Object duration = durationComboBox.getValue();
				
				if (duration.equals(THIS_WEEK)) {
					Calendar cal = Calendar.getInstance();
					
					cal.setFirstDayOfWeek(Calendar.SATURDAY);
					int firstDayOfWeek = cal.getFirstDayOfWeek();
					
					int days = (cal.get(Calendar.DAY_OF_WEEK)+14-firstDayOfWeek)%7;
					cal.add(Calendar.DATE, -days);
					
					Date startDateOfWeek = cal.getTime();
					startDateOfWeek.setHours(0);
					startDateOfWeek.setMinutes(0);
					startDateOfWeek.setSeconds(0);
					
					cal.add(Calendar.DATE, 6);
					Date endDateOfWeek = cal.getTime();
					endDateOfWeek.setHours(0);
					endDateOfWeek.setMinutes(0);
					endDateOfWeek.setSeconds(0);
					
					loadAttendance(startDateOfWeek, endDateOfWeek);
				}else if (duration.equals(LAST_WEEK)) {
					Calendar cal = Calendar.getInstance();
					cal.setFirstDayOfWeek(Calendar.SATURDAY);
					int firstDayOfWeek = cal.getFirstDayOfWeek();
					
					int days = cal.get(Calendar.DAY_OF_WEEK)+firstDayOfWeek;
					cal.add(Calendar.DATE, -days);
					
					Date startDateOfWeek = cal.getTime();
					startDateOfWeek.setHours(0);
					startDateOfWeek.setMinutes(0);
					startDateOfWeek.setSeconds(0);
//					startDate.setValue(startDateOfWeek);
					
					cal.add(Calendar.DATE, 6);
					Date endDateOfWeek = cal.getTime();
					endDateOfWeek.setHours(0);
					endDateOfWeek.setMinutes(0);
					endDateOfWeek.setSeconds(0);
//					endDate.setValue(endDateOfWeek);
					loadAttendance(startDateOfWeek, endDateOfWeek);
				}
			}
		});
    	
    	HorizontalLayout buttonLayout = new HorizontalLayout();
    	buttonLayout.setWidth("100%");
    	buttonLayout.setSpacing(true);
    	
    	Label spacer = new Label();
    	buttonLayout.addComponent(spacer);
    	buttonLayout.addComponent(requestButton);
    	buttonLayout.setExpandRatio(spacer, 1);
    	
    	requestButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				VerticalLayout layout = new VerticalLayout();
				layout.setMargin(true);
				layout.setSpacing(true);
				popUpWindow = new Window(null, layout);
				
				layout.addComponent(currectionLayout(0));
				popUpWindow.setWidth("70%");
				popUpWindow.center();
				popUpWindow.setModal(true);
				try{
					getMainWindow().addWindow(popUpWindow);
				}catch(ClassCastException e){
					e.printStackTrace();
				}
				
			}
		});
    	
    	mainLayout.addComponent(durationComboBox, 0, 0, 5, 0);
    	mainLayout.addComponent(attendanceTable, 0, 1, 7, 1);
    	mainLayout.addComponent(buttonLayout, 0, 2, 7, 2);
    	return mainLayout;
    }
    
    private DateField correctionDateField;
    private CustomTimeField realInTimeField;
    private CustomTimeField realOutTimeField;
    private CustomTimeField expectedInTimeField;
    private CustomTimeField expectedOutTimeField;
    private AttendanceCorrection correction;
    
    private VerticalLayout currectionLayout(long correctionId){
    	VerticalLayout vLayout = new VerticalLayout();
    	vLayout.setSpacing(true);
    	
    	correctionDateField = new DateField("Currection Date");
    	realInTimeField = new CustomTimeField("Real Start Time");
    	realOutTimeField = new CustomTimeField("Real End Time");
    	expectedInTimeField = new CustomTimeField("Expected Start Time");
    	expectedOutTimeField = new CustomTimeField("Expected End Time");
    	
    	realInTimeField.setLocale(Locale.ROOT);
    	realOutTimeField.setLocale(Locale.ROOT);
    	expectedInTimeField.setLocale(Locale.ROOT);
    	expectedOutTimeField.setLocale(Locale.ROOT);
    	
    	realInTimeField.setTabIndex(3);
    	realOutTimeField.setTabIndex(3);
    	expectedInTimeField.setTabIndex(3);
    	expectedOutTimeField.setTabIndex(3);
    	
    	correctionDateField.setDateFormat("dd/mm/yyyy");
    	realInTimeField.setWidth("100%");
    	realOutTimeField.setWidth("100%");
    	expectedInTimeField.setWidth("100%");
    	expectedOutTimeField.setWidth("100%");
        
    	correctionDateField.setImmediate(true);
        realInTimeField.setImmediate(true);
        realOutTimeField.setImmediate(true);
        expectedInTimeField.setImmediate(true);
        expectedOutTimeField.setImmediate(true);
        
        correctionDateField.setRequired(true);
        expectedInTimeField.setRequired(true);
        expectedOutTimeField.setRequired(true);
        
        correctionDateField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Date currectionInDate = (Date) correctionDateField.getValue();
				currectionInDate.setHours(0);
				currectionInDate.setMinutes(0);
				currectionInDate.setSeconds(0);
				
				Date currectionOutDate = (Date) currectionInDate.clone();
				currectionOutDate.setHours(23);
				currectionOutDate.setMinutes(59);
				currectionOutDate.setSeconds(59);
				
				try {
					DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(EmployeeAttendance.class);
					dynamicQuery.add(PropertyFactoryUtil.forName("employeeId").eq(employeeId));
					dynamicQuery.add(RestrictionsFactoryUtil.between("realTime", currectionInDate, currectionOutDate));
					List<EmployeeAttendance> employeeAttendances = EmployeeAttendanceLocalServiceUtil.dynamicQuery(dynamicQuery);
					if (employeeAttendances.size()>0) {
						EmployeeAttendance realStart = employeeAttendances.get(0);
						EmployeeAttendance realEnd = employeeAttendances.get(employeeAttendances.size()-1);
						
						if (realStart!=null) {
							realInTimeField.setValue(realStart.getRealTime());
						}
						
						if (realEnd!=null) {
							realOutTimeField.setValue(realEnd.getRealTime());
						}
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
        
        try {
        	if (correctionId>0) {
        		correction = AttendanceCorrectionLocalServiceUtil.fetchAttendanceCorrection(correctionId);
        		correctionDateField.setValue(correction.getCorrectionDate());
        		realInTimeField.setValue(correction.getRealIn());
        		realOutTimeField.setValue(correction.getRealOut());
        		expectedInTimeField.setValue(correction.getExpectedIn());
        		expectedOutTimeField.setValue(correction.getExpectedOut());
			}
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
    	
    	Button sendButton = new Button("Send");
    	sendButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (correction==null) {
					correction = new AttendanceCorrectionImpl();
					correction.setNew(true);
				}
				
				correction.setCompanyId(themeDisplay.getCompanyId());
				
				Date correctionDate = (Date) correctionDateField.getValue();
				if (correctionDate!=null) {
					correctionDate.setHours(0); correctionDate.setMinutes(0); correctionDate.setSeconds(0);
					correction.setCorrectionDate(correctionDate);
				}else {
					window.showNotification("Please select a date");
					return ;
				}
				
				Date realIn = (Date) realInTimeField.getValue();
				if (realIn!=null) {
					correction.setRealIn(realIn);
				}
				
				Date realOut = (Date) realOutTimeField.getValue();
				if (realOut!=null) {
					correction.setRealOut(realOut);
				}
				
				Date expectedIn = (Date) expectedInTimeField.getValue();
				if (expectedIn!=null) {
					correction.setExpectedIn(expectedIn);
				}
				
				Date expectedOut = (Date) expectedOutTimeField.getValue();
				if (expectedOut!=null) {
					correction.setExpectedOut(expectedOut);
				}
				correction.setEmployeeId(employeeId);
				
				try {
					if (correction.isNew()) {
						correction.setStatus(0);
						AttendanceCorrectionLocalServiceUtil.addAttendanceCorrection(correction);
						window.showNotification("Your requested has been send", Window.Notification.TYPE_WARNING_MESSAGE);
					} else {
						correction.setStatus(1);
						AttendanceCorrectionLocalServiceUtil.updateAttendanceCorrection(correction);
						window.showNotification("Updated successfully", Window.Notification.TYPE_WARNING_MESSAGE);
					}
					popUpWindow.setVisible(false);
					popUpWindow.removeAllComponents();
					loadRequest();
					correction = null;
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
    	
    	vLayout.addComponent(correctionDateField);
    	vLayout.addComponent(realInTimeField);
    	vLayout.addComponent(realOutTimeField);
    	vLayout.addComponent(expectedInTimeField);
    	vLayout.addComponent(expectedOutTimeField);
    	vLayout.addComponent(sendButton);
    	
    	return vLayout;
    }
    
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm a");
    
    private void loadAttendance(Date startDateOfWeek, Date endDateOfWeek) {
    	attendanceContainer.removeAllItems();
    	long expectedHours = 0;
    	long realHours = 0;
    	long leaveHours = 0;
    	try {
    		DynamicQuery workingDayDynamicQuery = DynamicQueryFactoryUtil.forClass(WorkingDay.class);
//    		dynamicQuery.add(PropertyFactoryUtil.forName("employeeId").eq(employeeId));
    		workingDayDynamicQuery.add(RestrictionsFactoryUtil.between("date", startDateOfWeek, endDateOfWeek));
    		
//    		window.showNotification("Start Date "+startDateOfWeek+" End date "+endDateOfWeek);
    		
    		List<WorkingDay> workingDays = WorkingDayLocalServiceUtil.dynamicQuery(workingDayDynamicQuery);
    		if (workingDays!=null) {
				for (WorkingDay workingDay : workingDays) {
					EmployeeAttendanceDto attendanceDto = new EmployeeAttendanceDto();
					
					Calendar cal1 = Calendar.getInstance();
					Calendar cal2 = Calendar.getInstance();
					
					Date startDate = workingDay.getDate();
					Date endDate = (Date) startDate.clone();
					
					startDate.setHours(0);
					startDate.setMinutes(0);
					startDate.setSeconds(0);
					
					endDate.setHours(23);
					endDate.setMinutes(59);
					endDate.setSeconds(59);
					
					DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(EmployeeAttendance.class);
					dynamicQuery.add(PropertyFactoryUtil.forName("employeeId").eq(employeeId));
					dynamicQuery.add(RestrictionsFactoryUtil.between("realTime", startDate, endDate));
					
					DayType dtWorkingDay = DayTypeLocalServiceUtil.findByTypeOrganization("WorkingDay", organizationId);
					
					attendanceDto.setStatus(DayTypeLocalServiceUtil.fetchDayType(workingDay.getStatus()).getType());
					
					List<EmployeeAttendance> attendanceList = EmployeeAttendanceLocalServiceUtil.dynamicQuery(dynamicQuery);
					
					if (attendanceList.size()>0) {
						EmployeeAttendance startAtt = attendanceList.get(0);
						attendanceDto.setExpectedIn(timeFormat.format(startAtt.getExpectedStart()));
						attendanceDto.setRealIn(timeFormat.format(startAtt.getRealTime()));
						
						EmployeeAttendance endAtt = attendanceList.get(attendanceList.size()-1);
						attendanceDto.setExpectedOut(timeFormat.format(startAtt.getExpectedEnd()));
						attendanceDto.setRealOut(timeFormat.format(endAtt.getRealTime()));
						
						Employee emp = EmployeeLocalServiceUtil.fetchEmployee(employeeId);
						if (emp!=null) {
							if (emp.getMinimumDuration()>0) {
								String minDuration = ""+emp.getMinimumDuration();
								List<String> parts = new ArrayList<String>();
								
								StringTokenizer st = new StringTokenizer(minDuration, ".");
								
								while (st.hasMoreTokens()) {
									parts.add((String) st.nextElement());
								}
								
								String part1 = parts.get(0);
								String part2 = parts.get(1);
								
								int munites = Integer.parseInt(part2);
								if (munites==0) {
									attendanceDto.setExpectedDailyDuration(part1+":00");
								}else if (munites<10) {
									attendanceDto.setExpectedDailyDuration(part1+":0"+munites);
								}else{
									attendanceDto.setExpectedDailyDuration(part1+":"+munites);
								}
								long realDiff = endAtt.getRealTime().getTime()-startAtt.getRealTime().getTime();
								attendanceDto.setRealDailyDuration(timeFormat.format(new Date(realDiff)));
								realHours += endAtt.getRealTime().getTime()-startAtt.getRealTime().getTime();
							}else{
								long expectedDiff = endAtt.getExpectedEnd().getTime()-startAtt.getExpectedStart().getTime();
								attendanceDto.setExpectedDailyDuration(timeFormat.format(new Date(expectedDiff)));
								realHours += expectedDiff;
							}
						}
					} else {
						DayType workingDayDT = DayTypeLocalServiceUtil.findByTypeOrganization("WorkingDay", organizationId);
						DayType weekendDT = DayTypeLocalServiceUtil.findByTypeOrganization("Weekend", organizationId);
						DayType hartalDT = DayTypeLocalServiceUtil.findByTypeOrganization("Hartal", organizationId);
						
						Employee emp = EmployeeLocalServiceUtil.fetchEmployee(employeeId);
						
						List<Leave> leaveList = LeaveLocalServiceUtil.findByEmployee(employeeId);
						if (leaveList!=null) {
							for (Leave leave : leaveList) {
								try {
									
									if (emp!=null) {
										if (emp.getMinimumDuration()>0) {
											Date start = leave.getStartDate();
											Date end = leave.getEndDate();
											if (workingDay.getDate().after(start) && workingDay.getDate().before(end)) {
												attendanceDto.setStatus("Leave");
												LeaveDayDetails leaveDayDetails = LeaveDayDetailsLocalServiceUtil.findByLeaveDay(leave.getLeaveId(), workingDay.getDate());
												leaveHours += leaveDayDetails.getDay()*6;
											} else if(start.equals(workingDay.getDate())) {
												attendanceDto.setStatus("Leave");
												LeaveDayDetails leaveDayDetails = LeaveDayDetailsLocalServiceUtil.findByLeaveDay(leave.getLeaveId(), workingDay.getDate());
												leaveHours += leaveDayDetails.getDay()*6;
											} else if(end.equals(workingDay.getDate())){
												attendanceDto.setStatus("Leave");
												LeaveDayDetails leaveDayDetails = LeaveDayDetailsLocalServiceUtil.findByLeaveDay(leave.getLeaveId(), workingDay.getDate());
												leaveHours += leaveDayDetails.getDay()*6;
											} else {
												if (workingDay.getStatus()==workingDayDT.getDayTypeId()){
													attendanceDto.setStatus("Absence");
												}
											}
										} else {
											Date start = leave.getStartDate();
											Date end = leave.getEndDate();
											if (workingDay.getDate().after(start) && workingDay.getDate().before(end)) {
												attendanceDto.setStatus("Leave");
												LeaveDayDetails leaveDayDetails = LeaveDayDetailsLocalServiceUtil.findByLeaveDay(leave.getLeaveId(), workingDay.getDate());
												leaveHours += leaveDayDetails.getDay()*8;
											} else if(start.equals(workingDay.getDate())) {
												attendanceDto.setStatus("Leave");
												LeaveDayDetails leaveDayDetails = LeaveDayDetailsLocalServiceUtil.findByLeaveDay(leave.getLeaveId(), workingDay.getDate());
												leaveHours += leaveDayDetails.getDay()*8;
											} else if(end.equals(workingDay.getDate())){
												attendanceDto.setStatus("Leave");
												LeaveDayDetails leaveDayDetails = LeaveDayDetailsLocalServiceUtil.findByLeaveDay(leave.getLeaveId(), workingDay.getDate());
												leaveHours += leaveDayDetails.getDay()*8;
											} else if(workingDayDT!=null && weekendDT!=null && hartalDT!=null){
												leaveHours += 8;
											} else{
												if (workingDay.getStatus()==workingDayDT.getDayTypeId()){
													attendanceDto.setStatus("Absence");
												}
											}
										}
									}
									
								} catch (NoSuchLeaveDayDetailsException e) {
									e.printStackTrace();
								}
							}
						}else{
							if (workingDay.getStatus()==workingDayDT.getDayTypeId()){
								attendanceDto.setStatus("Absence");
							}
						}
					}
					
					attendanceDto.setDate(simpleDateFormat.format(workingDay.getDate()));
					attendanceContainer.addBean(attendanceDto);
				}
			}
    		totalHours(realHours, leaveHours);
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (NoSuchDayTypeException e) {
			e.printStackTrace();
		}
    }
    
    private void totalHours(long realHours, double leaveHours){
    	long expectedHours = 0;
    	try {
			Employee emp = EmployeeLocalServiceUtil.fetchEmployee(employeeId);
			String minDuration = ""+(emp.getDuration()-leaveHours);
			if (minDuration!=null) {
				List<String> parts = new ArrayList<String>();
				
				StringTokenizer st = new StringTokenizer(minDuration, ".");
				
				while (st.hasMoreTokens()) {
					parts.add((String) st.nextElement());
				}
				
				if (parts!=null) {
					String part1 = parts.get(0);
					String part2 = parts.get(1);
					int munites = Integer.parseInt(part2);
					
					if (munites==0) {
						attendanceTable.setColumnFooter(EXPECTED_DAILY_DURATION, "Weakly: "+part1+":00");
					}else if (munites<10) {
						attendanceTable.setColumnFooter(EXPECTED_DAILY_DURATION, "Weakly: "+part1+":0"+munites);
					}else{
						attendanceTable.setColumnFooter(EXPECTED_DAILY_DURATION, "Weakly: "+part1+":"+munites);
					}
				}
			}
			
			/*Date hour = new Date();
			hour.setHours(Integer.parseInt(part1));
			hour.setMinutes(Integer.parseInt(part2));
			attendanceTable.setColumnFooter(EXPECTED_DAILY_DURATION, "Weakly: "+timeFormat.format(hour));*/
			
	    	attendanceTable.setColumnFooter(DATE, "");
	    	attendanceTable.setColumnFooter(EXPECTED_IN, "");
	    	attendanceTable.setColumnFooter(REAL_IN, "");
	    	attendanceTable.setColumnFooter(EXPECTED_OUT, "");
	    	attendanceTable.setColumnFooter(REAL_OUT, "Total");
	    	attendanceTable.setColumnFooter(REAL_DAILY_DURATION, timeFormat.format(realHours));
	    	
	    	attendanceTable.setColumnFooter(DAY_STATUS, "");
    	} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
//    Attendance Request List Tab
    
    private BeanItemContainer<AttendanceCorrectionDto> requestContainer;
    private Table requestTable;
    
    private final static String REQUEST_DATE_COLUMN = "date";
    private final static String REAL_IN_COLUMN = "realIn";
    private final static String REAL_OUT_COLUMN = "realOut";
    private final static String EXPECTED_IN_COLUMN = "expectedIn";
    private final static String EXPECTED_OUT_COLUMN = "expectedOut";
    private final static String STATUS_COLUMN = "status";

    
    private VerticalLayout requestList(){
    	VerticalLayout requestLayout = new VerticalLayout();
    	requestLayout.setWidth("100%");
    	requestLayout.setSpacing(true);
    	
    	requestContainer = new BeanItemContainer<AttendanceCorrectionDto>(AttendanceCorrectionDto.class);
    	requestTable = new Table("", requestContainer);
    	requestTable.setWidth("100%");
    	requestTable.setImmediate(true);
    	requestTable.setSelectable(true);
    	
    	requestTable.setColumnHeader(REQUEST_DATE_COLUMN, "Requested Date");
    	requestTable.setColumnHeader(REAL_IN_COLUMN, "Real In");
    	requestTable.setColumnHeader(REAL_OUT_COLUMN, "Real Out");
    	requestTable.setColumnHeader(EXPECTED_IN_COLUMN, "Expected In");
    	requestTable.setColumnHeader(EXPECTED_OUT_COLUMN, "Expected Out");
    	requestTable.setColumnHeader(STATUS_COLUMN, "Status");
    	requestTable.setVisibleColumns(new String[]{REQUEST_DATE_COLUMN, REAL_IN_COLUMN, REAL_OUT_COLUMN, EXPECTED_IN_COLUMN, EXPECTED_OUT_COLUMN, STATUS_COLUMN});
    	
    	loadRequest();
    	
    	Label spacer = new Label();
    	Button editButton = new Button("Edit");
    	editButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				AttendanceCorrectionDto requestDto = (AttendanceCorrectionDto) requestTable.getValue();
				if (requestDto!=null) {
					VerticalLayout layout = new VerticalLayout();
					layout.setMargin(true);
					layout.setSpacing(true);
					popUpWindow = new Window(null, layout);
					layout.removeAllComponents();
					layout.addComponent(currectionLayout(requestDto.getId()));
					popUpWindow.setWidth("30%");
					popUpWindow.center();
					popUpWindow.setModal(true);
					try{
						getMainWindow().addWindow(popUpWindow);
					}catch(ClassCastException e){
						e.printStackTrace();
					}
					
				} else {
					window.showNotification("Please select a row", Window.Notification.TYPE_ERROR_MESSAGE);
				}
			}
		});
    	
    	HorizontalLayout hLayout = new HorizontalLayout();	
    	hLayout.addComponent(spacer);
    	hLayout.addComponent(editButton);
    	hLayout.setExpandRatio(spacer, 1);
    	
    	requestLayout.addComponent(requestTable);
    	requestLayout.addComponent(hLayout);
    	
    	return requestLayout;
    }
    
    private void loadRequest(){
    	requestContainer.removeAllItems();
    	try {
			List<AttendanceCorrection> correctionList = AttendanceCorrectionLocalServiceUtil.findByEmployee(employeeId);
			if (correctionList!=null) {
				for (AttendanceCorrection attendanceCorrection : correctionList) {
					AttendanceCorrectionDto requestDto = new AttendanceCorrectionDto();
					
					requestDto.setId(attendanceCorrection.getAttendanceCorrectionId());
					requestDto.setDate(simpleDateFormat.format(attendanceCorrection.getCorrectionDate()));
					requestDto.setRealIn(timeFormat.format(attendanceCorrection.getRealIn()));
					requestDto.setRealOut(timeFormat.format(attendanceCorrection.getRealOut()));
					requestDto.setExpectedIn(timeFormat.format(attendanceCorrection.getExpectedIn()));
					requestDto.setExpectedOut(timeFormat.format(attendanceCorrection.getExpectedOut()));
					if (attendanceCorrection.getStatus()==0) {
						requestDto.setStatus("Pending");
					} else {
						requestDto.setStatus("Completed");
					}
					requestContainer.addBean(requestDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
//  Attendance Request List Tab

	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

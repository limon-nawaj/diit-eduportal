package info.diit.portal.employee.dto;

import java.util.Date;


public class EmployeeAttendanceDto {
	private long id;
	private String name;
	private int    day;
	private String date;
	private String dayName;
	private String expectedIn;
	private String realIn;
	private String expectedOut;
	private String realOut;
	private String realDailyDuration;
	private String expectedDailyDuration;
	private String status;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDayName() {
		return dayName;
	}
	public String getExpectedIn() {
		return expectedIn;
	}
	public void setExpectedIn(String expectedIn) {
		this.expectedIn = expectedIn;
	}
	public String getRealIn() {
		return realIn;
	}
	public void setRealIn(String realIn) {
		this.realIn = realIn;
	}
	public String getExpectedOut() {
		return expectedOut;
	}
	public void setExpectedOut(String expectedOut) {
		this.expectedOut = expectedOut;
	}
	public String getRealOut() {
		return realOut;
	}
	public void setRealOut(String realOut) {
		this.realOut = realOut;
	}
	public String getRealDailyDuration() {
		return realDailyDuration;
	}
	public void setRealDailyDuration(String realDailyDuration) {
		this.realDailyDuration = realDailyDuration;
	}
	public String getExpectedDailyDuration() {
		return expectedDailyDuration;
	}
	public void setExpectedDailyDuration(String expectedDailyDuration) {
		this.expectedDailyDuration = expectedDailyDuration;
	}
	public void setDayName(String dayName) {
		this.dayName = dayName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}

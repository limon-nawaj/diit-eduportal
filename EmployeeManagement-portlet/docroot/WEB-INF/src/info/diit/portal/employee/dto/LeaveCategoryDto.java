package info.diit.portal.employee.dto;

import java.io.Serializable;

public class LeaveCategoryDto implements Serializable {
	private int id;
	private String categoryTitle;
	private Integer totalLeave;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCategoryTitle() {
		return categoryTitle;
	}
	public void setCategoryTitle(String categoryTitle) {
		this.categoryTitle = categoryTitle;
	}
	public Integer getTotalLeave() {
		return totalLeave;
	}
	public void setTotalLeave(Integer totalLeave) {
		this.totalLeave = totalLeave;
	}
	@Override
	public String toString() {
		return getCategoryTitle();
	}
}

package info.diit.portal.employee.dto;

public class AttendanceCorrectionDto {
	private long id;
	private String name;
	private String date;
	private String realIn;
	private String realOut;
	private String expectedIn;
	private String expectedOut;
	private String status;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getRealIn() {
		return realIn;
	}
	public void setRealIn(String realIn) {
		this.realIn = realIn;
	}
	public String getRealOut() {
		return realOut;
	}
	public void setRealOut(String realOut) {
		this.realOut = realOut;
	}
	public String getExpectedIn() {
		return expectedIn;
	}
	public void setExpectedIn(String expectedIn) {
		this.expectedIn = expectedIn;
	}
	public String getExpectedOut() {
		return expectedOut;
	}
	public void setExpectedOut(String expectedOut) {
		this.expectedOut = expectedOut;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}

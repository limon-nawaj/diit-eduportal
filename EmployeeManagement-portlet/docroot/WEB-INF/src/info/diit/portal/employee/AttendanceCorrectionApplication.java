package info.diit.portal.employee;

import info.diit.portal.NoSuchEmployeeTimeScheduleException;
import info.diit.portal.employee.dto.AttendanceCorrectionDto;
import info.diit.portal.model.AttendanceCorrection;
import info.diit.portal.model.EmployeeAttendance;
import info.diit.portal.model.EmployeeTimeSchedule;
import info.diit.portal.model.impl.EmployeeAttendanceImpl;
import info.diit.portal.service.AttendanceCorrectionLocalServiceUtil;
import info.diit.portal.service.EmployeeAttendanceLocalServiceUtil;
import info.diit.portal.service.EmployeeLocalServiceUtil;
import info.diit.portal.service.EmployeeTimeScheduleLocalServiceUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Role;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class AttendanceCorrectionApplication extends Application implements PortletRequestListener {
	
	private ThemeDisplay themeDisplay;
	private Window window;
	
	private long companyId;
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        companyId = themeDisplay.getCompanyId();
        try {
			List<Role> roles = themeDisplay.getUser().getRoles();
			
			window.addComponent(new Label("Only Office Admin is permitted to see the porlet"));
			if (roles!=null) {
				for (Role role : roles) {
					if (role.getName().equalsIgnoreCase("Office Admin")) {
						window.removeAllComponents();
						window.addComponent(mainLayout());
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
        
    }
    
    DateField startDateField;
    DateField endDateField;
    
    BeanItemContainer<AttendanceCorrectionDto> requestContainer;
    Table requestTable;
    
    private final static String REQUEST_NAME = "name";
    private final static String REQUEST_DATE_COLUMN = "date";
    private final static String REAL_IN_COLUMN = "realIn";
    private final static String REAL_OUT_COLUMN = "realOut";
    private final static String EXPECTED_IN_COLUMN = "expectedIn";
    private final static String EXPECTED_OUT_COLUMN = "expectedOut";
    private final static String STATUS_COLUMN = "status";
    
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
    
    private GridLayout mainLayout(){
    	GridLayout mainLayout = new GridLayout(8, 8);
    	mainLayout.setWidth("100%");
    	mainLayout.setSpacing(true);
    	
    	startDateField = new DateField("Start Date");
    	endDateField = new DateField("End Date");
    	
    	startDateField.setImmediate(true);
    	endDateField.setImmediate(true);
    	
    	startDateField.setWidth("100%");
    	endDateField.setWidth("100%");
    	
    	startDateField.setDateFormat("dd/MM/yyyy");
    	endDateField.setDateFormat("dd/MM/yyyy");
    	
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.MONTH, -1);
    	calendar.set(Calendar.DAY_OF_MONTH, 1);
    	
    	final Date startDate = calendar.getTime();
    	final Date endDate = new Date();
    	startDateField.setValue(startDate);
    	endDateField.setValue(endDate);
    	
    	startDateField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Date startDate = (Date) startDateField.getValue();
				Date endDate = (Date) endDateField.getValue();
				
				startDate.setHours(0);
	    		startDate.setMinutes(0);
	    		startDate.setSeconds(0);
	    		
	    		endDate.setHours(0);
	    		endDate.setMinutes(0);
	    		endDate.setSeconds(0);
	    		
	    		
	    		
				if (endDate!=null) {
					if (startDate.before(endDate)) {
						window.showNotification("start date");
						loadRequest(startDate, endDate);
					}
				}
			}
		});
    	
    	endDateField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Date startDate = (Date) startDateField.getValue();
				Date endDate = (Date) endDateField.getValue();
				
				startDate.setHours(0);
	    		startDate.setMinutes(0);
	    		startDate.setSeconds(0);
	    		
	    		endDate.setHours(0);
	    		endDate.setMinutes(0);
	    		endDate.setSeconds(0);
	    		
				if (startDate!=null) {
					if (endDate.after(startDate)) {
						window.showNotification("end date");
						loadRequest(startDate, endDate);
					}
				}
			}
		});
    	
    	requestContainer = new BeanItemContainer<AttendanceCorrectionDto>(AttendanceCorrectionDto.class);
    	requestTable = new Table("", requestContainer);
    	requestTable.setWidth("100%");
    	requestTable.setSelectable(true);
    	
    	requestTable.setColumnHeader(REQUEST_NAME, "Name");
    	requestTable.setColumnHeader(REQUEST_DATE_COLUMN, "Requested Date");
    	requestTable.setColumnHeader(REAL_IN_COLUMN, "Real In");
    	requestTable.setColumnHeader(REAL_OUT_COLUMN, "Real Out");
    	requestTable.setColumnHeader(EXPECTED_IN_COLUMN, "Expected In");
    	requestTable.setColumnHeader(EXPECTED_OUT_COLUMN, "Expected Out");
    	requestTable.setColumnHeader(STATUS_COLUMN, "Status");
    	
    	requestTable.setVisibleColumns(new String[]{REQUEST_NAME, REQUEST_DATE_COLUMN, REAL_IN_COLUMN, REAL_OUT_COLUMN, EXPECTED_IN_COLUMN, EXPECTED_OUT_COLUMN, STATUS_COLUMN});
    	loadRequest(startDate, endDate);
    	
    	HorizontalLayout hLayout = new HorizontalLayout();
    	hLayout.setWidth("100%");
    	
    	Label spacer = new Label();
    	Button updateButton = new Button("Update");
    	hLayout.addComponent(spacer);
    	hLayout.addComponent(updateButton);
    	hLayout.setExpandRatio(spacer, 1);
    	
    	updateButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				VerticalLayout layout = new VerticalLayout();
				layout.setMargin(true);
				layout.setSpacing(true);
				Window popUpWindow = new Window(null, layout);
				layout.removeAllComponents();
				AttendanceCorrectionDto correctionDto = (AttendanceCorrectionDto) requestTable.getValue();
				if (correctionDto!=null) {
					layout.addComponent(correctionLayout(correctionDto.getId(), startDate, endDate));
				}
				
				popUpWindow.setWidth("30%");
				popUpWindow.center();
				popUpWindow.setModal(true);
				getMainWindow().addWindow(popUpWindow);
			}
		});
    	
    	mainLayout.addComponent(startDateField, 0, 0, 2, 0);
    	mainLayout.addComponent(endDateField, 3, 0, 5, 0);
    	mainLayout.addComponent(requestTable, 0, 1, 7, 1);
    	mainLayout.addComponent(hLayout, 0, 2, 7, 2);
    	return mainLayout;
    }
    
    private DateField correctionDateField;
    private CustomTimeField realInTimeField;
    private CustomTimeField realOutTimeField;
    private CustomTimeField expectedInTimeField;
    private CustomTimeField expectedOutTimeField;
    private Button saveButton;
    private AttendanceCorrection correction;
    
    private VerticalLayout correctionLayout(long correctionId, final Date startDate, final Date endDate){
    	VerticalLayout vLayout = new VerticalLayout();
    	vLayout.setSpacing(true);
    	
    	correctionDateField = new DateField("Currection Date");
    	realInTimeField = new CustomTimeField("Real Start Time");
    	realOutTimeField = new CustomTimeField("Real End Time");
    	expectedInTimeField = new CustomTimeField("Expected Start Time");
    	expectedOutTimeField = new CustomTimeField("Expected End Time");
    	saveButton = new Button("Save");
    	
    	correctionDateField.setReadOnly(false);
    	realInTimeField.setReadOnly(false);
    	realOutTimeField.setReadOnly(false);
    	
    	realInTimeField.setLocale(Locale.ROOT);
    	realOutTimeField.setLocale(Locale.ROOT);
    	expectedInTimeField.setLocale(Locale.ROOT);
    	expectedOutTimeField.setLocale(Locale.ROOT);
    	
    	realInTimeField.setTabIndex(3);
    	realOutTimeField.setTabIndex(3);
    	expectedInTimeField.setTabIndex(3);
    	expectedOutTimeField.setTabIndex(3);
    	
    	correctionDateField.setDateFormat("dd/mm/yyyy");
    	realInTimeField.setWidth("100%");
    	realOutTimeField.setWidth("100%");
    	expectedInTimeField.setWidth("100%");
    	expectedOutTimeField.setWidth("100%");
        
    	correctionDateField.setImmediate(true);
        realInTimeField.setImmediate(true);
        realOutTimeField.setImmediate(true);
        expectedInTimeField.setImmediate(true);
        expectedOutTimeField.setImmediate(true);
        
        correctionDateField.setRequired(true);
        expectedInTimeField.setRequired(true);
        expectedOutTimeField.setRequired(true);
        
        try {
			correction = AttendanceCorrectionLocalServiceUtil.fetchAttendanceCorrection(correctionId);
			if (correction!=null) {
				correctionDateField.setValue(correction.getCorrectionDate());
				correctionDateField.setReadOnly(true);
				realInTimeField.setValue(correction.getRealIn());
				realInTimeField.setReadOnly(true);
				realOutTimeField.setValue(correction.getRealOut());
		    	realOutTimeField.setReadOnly(true);
				expectedInTimeField.setValue(correction.getExpectedIn());
				expectedOutTimeField.setValue(correction.getExpectedOut());
				
//				Need to save expected in and out time in EmployeeAttendance Table
				
				
				saveButton.addListener(new ClickListener() {
					
					@Override
					public void buttonClick(ClickEvent event) {
						Date start = correction.getCorrectionDate();
						Date end = (Date) start.clone();
						end.setHours(23);
						end.setMinutes(59);
						end.setSeconds(59);
						
						DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(EmployeeAttendance.class);
						dynamicQuery.add(PropertyFactoryUtil.forName("employeeId").eq(correction.getEmployeeId()));
						dynamicQuery.add(RestrictionsFactoryUtil.between("realTime", start, end));
						
						Date requestedDate = (Date) correctionDateField.getValue();
						List<EmployeeAttendance> attendanceList;
						try {
							attendanceList = EmployeeAttendanceLocalServiceUtil.dynamicQuery(dynamicQuery);
							
							Date expectedIn = (Date) expectedInTimeField.getValue();
							Date expectedOut = (Date) expectedOutTimeField.getValue();
							
							if (attendanceList.size()>0) {
								EmployeeAttendance inTime = attendanceList.get(0);
								EmployeeAttendance outTime = attendanceList.get(attendanceList.size()-1);
								
								if (expectedIn!=null) {
									inTime.setRealTime(expectedIn);
									inTime.setEmployeeId(correction.getEmployeeId());
									EmployeeAttendanceLocalServiceUtil.updateEmployeeAttendance(inTime);
									correction.setExpectedIn(expectedIn);
								}
								
								if (expectedOut!=null) {
									outTime.setEmployeeId(correction.getEmployeeId());
									outTime.setRealTime(expectedOut);
									EmployeeAttendanceLocalServiceUtil.updateEmployeeAttendance(outTime);
									correction.setExpectedOut(expectedOut);
								}
								correction.setStatus(1);
								AttendanceCorrectionLocalServiceUtil.updateAttendanceCorrection(correction);
								loadRequest(startDate, endDate);
								window.showNotification("Attendance updated successfully");
							}else{
								EmployeeAttendance inTime = new EmployeeAttendanceImpl();
								EmployeeAttendance outTime = new EmployeeAttendanceImpl();
								
								if (expectedIn!=null) {
									inTime.setRealTime(expectedIn);
									inTime.setEmployeeId(correction.getEmployeeId());
									Calendar cal = Calendar.getInstance();
									cal.setTime(requestedDate);
									int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
									
									try {
										EmployeeTimeSchedule schedule = EmployeeTimeScheduleLocalServiceUtil.findByEmployeeDay(correction.getEmployeeId(), dayOfWeek);
										if (schedule!=null) {
											inTime.setExpectedStart(schedule.getStartTime());
											inTime.setExpectedEnd(schedule.getEndTime());
										}
									} catch (NoSuchEmployeeTimeScheduleException e) {
										e.printStackTrace();
									}
									
									EmployeeAttendanceLocalServiceUtil.addEmployeeAttendance(inTime);
									correction.setExpectedIn(expectedIn);
								}
								
								if (expectedOut!=null) {
									outTime.setRealTime(expectedOut);
									outTime.setEmployeeId(correction.getEmployeeId());
									Calendar cal = Calendar.getInstance();
									cal.setTime(requestedDate);
									int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
									
									try {
										EmployeeTimeSchedule schedule = EmployeeTimeScheduleLocalServiceUtil.findByEmployeeDay(correction.getEmployeeId(), dayOfWeek);
										if (schedule!=null) {
											outTime.setExpectedStart(schedule.getStartTime());
											outTime.setExpectedEnd(schedule.getEndTime());
										}
									} catch (NoSuchEmployeeTimeScheduleException e) {
										e.printStackTrace();
									}
									EmployeeAttendanceLocalServiceUtil.addEmployeeAttendance(outTime);
									correction.setExpectedOut(expectedOut);
								}
								correction.setStatus(1);
								AttendanceCorrectionLocalServiceUtil.updateAttendanceCorrection(correction);
								loadRequest(startDate, endDate);
								window.showNotification("Attendance updated successfully");
							}
						} catch (SystemException e) {
							e.printStackTrace();
						}
					}
				});
			}
			
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
        
        vLayout.addComponent(correctionDateField);
    	vLayout.addComponent(realInTimeField);
    	vLayout.addComponent(realOutTimeField);
    	vLayout.addComponent(expectedInTimeField);
    	vLayout.addComponent(expectedOutTimeField);
    	vLayout.addComponent(saveButton);
        
        return vLayout;
    }
    
    private void loadRequest(Date startDate, Date endDate){
    	requestContainer.removeAllItems();
    	try {
    		startDate.setHours(0);
    		startDate.setMinutes(0);
    		startDate.setSeconds(0);
    		
    		endDate.setHours(0);
    		endDate.setMinutes(0);
    		endDate.setSeconds(0);
    		
    		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(AttendanceCorrection.class);
    		dynamicQuery.add(PropertyFactoryUtil.forName("companyId").eq(companyId));
    		dynamicQuery.add(RestrictionsFactoryUtil.between("correctionDate", startDate, endDate));
			List<AttendanceCorrection> correctionList = AttendanceCorrectionLocalServiceUtil.dynamicQuery(dynamicQuery);
			
			if (correctionList!=null) {
				for (AttendanceCorrection attendanceCorrection : correctionList) {
					AttendanceCorrectionDto correctionDto = new AttendanceCorrectionDto();
					correctionDto.setName(EmployeeLocalServiceUtil.fetchEmployee(attendanceCorrection.getEmployeeId()).getName());
					correctionDto.setId(attendanceCorrection.getAttendanceCorrectionId());
					correctionDto.setDate(simpleDateFormat.format(attendanceCorrection.getCorrectionDate()));
					correctionDto.setRealIn(timeFormat.format(attendanceCorrection.getRealIn()));
					correctionDto.setRealOut(timeFormat.format(attendanceCorrection.getRealOut()));
					correctionDto.setExpectedIn(timeFormat.format(attendanceCorrection.getExpectedIn()));
					correctionDto.setExpectedOut(timeFormat.format(attendanceCorrection.getExpectedOut()));
					if (attendanceCorrection.getStatus()==0) {
						correctionDto.setStatus("Pending");
					}else{
						correctionDto.setStatus("Completed");
					}
					requestContainer.addBean(correctionDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }

	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}

}

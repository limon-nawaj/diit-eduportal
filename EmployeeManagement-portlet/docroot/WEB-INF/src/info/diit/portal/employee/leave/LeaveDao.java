package info.diit.portal.employee.leave;

import info.diit.portal.dailyWork.model.Designation;
import info.diit.portal.dailyWork.service.DesignationLocalServiceUtil;
import info.diit.portal.employee.dto.DesignationDto;
import info.diit.portal.employee.dto.EmployeeDto;
import info.diit.portal.employee.dto.LeaveCategoryDto;
import info.diit.portal.employee.dto.LeaveDto;
import info.diit.portal.model.Employee;
import info.diit.portal.model.Leave;
import info.diit.portal.model.LeaveCategory;
import info.diit.portal.service.EmployeeLocalServiceUtil;
import info.diit.portal.service.LeaveCategoryLocalServiceUtil;
import info.diit.portal.service.LeaveLocalServiceUtil;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

public class LeaveDao {
	
	public List<LeaveCategoryDto> getCategory(long organizationId){
		List<LeaveCategoryDto> categoryList = new ArrayList<LeaveCategoryDto>();
		try {
			List<LeaveCategory> categories = LeaveCategoryLocalServiceUtil.findByOrganization(organizationId);
			if (categories.size()>0) {
				for (LeaveCategory leaveCategory : categories) {
					LeaveCategoryDto categoryDto = new LeaveCategoryDto();
					categoryDto.setId((int) leaveCategory.getLeaveCategoryId());
					categoryDto.setCategoryTitle(leaveCategory.getTitle());
					categoryDto.setTotalLeave(leaveCategory.getTotalLeave());
					categoryList.add(categoryDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return categoryList;
	}
	
	public DesignationDto employeeDesignation(long designationId){
		DesignationDto designationDto = new DesignationDto();
		try {
			Designation designation = DesignationLocalServiceUtil.fetchDesignation(designationId);
			if (designation!=null) {
				designationDto.setId(designation.getDesignationId());
				designationDto.setTitle(designation.getDesignationTitle());
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return designationDto;
	}
	
	public List<EmployeeDto> getEmployeeByOrganization(long companyId, long organizationId){
		List<EmployeeDto> employees = new ArrayList<EmployeeDto>();
		try {
			List<Employee> employeeList = EmployeeLocalServiceUtil.findByCompanyOrganization(companyId, organizationId);
			if (employeeList!=null) {
				for (Employee employee : employeeList) {
					EmployeeDto employeeDto = new EmployeeDto();
					employeeDto.setId(employee.getEmployeeId());
					employeeDto.setName(employee.getName());
					employees.add(employeeDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return employees;
	}
	
	public List<LeaveDto> getLeaveByEmployee(long employeeId){
		List<LeaveDto> leaves = new ArrayList<LeaveDto>();
		try {
			List<Leave> leaveList = LeaveLocalServiceUtil.findByEmployee(employeeId);
			Employee emp = EmployeeLocalServiceUtil.fetchEmployee(employeeId);
			EmployeeDto empDto = new EmployeeDto();
			empDto.setId(emp.getEmployeeId());
			empDto.setName(emp.getName());
			if (leaveList!=null) {
				for (Leave leave : leaveList) {
					LeaveDto leaveDto = new LeaveDto();
					
					leaveDto.setId(leave.getLeaveId());
					leaveDto.setEmployee(empDto);
					leaveDto.setStartDate(leave.getStartDate());
					leaveDto.setEndDate(leave.getEndDate());
					leaveDto.setTotalDay(leave.getNumberOfDay());
					leaveDto.setComments(leave.getComments());
					if (leave.getApplicationStatus()==0) {
						leaveDto.setStatus("Pending");
					}else if (leave.getApplicationStatus()==1) {
						leaveDto.setStatus("Accepted");
					}else if (leave.getApplicationStatus()==2) {
						leaveDto.setStatus("Decline");
					}else if (leave.getApplicationStatus()==3) {
						leaveDto.setStatus("Withdraw");
					}
					
					Employee employee = EmployeeLocalServiceUtil.fetchEmployee(leave.getComplementedBy());
					if (employee!=null) {
						EmployeeDto employeeDto = new EmployeeDto();
						employeeDto.setId(employee.getEmployeeId());
						employeeDto.setName(employee.getName());
						
						leaveDto.setComplementedBy(employeeDto);
					}
					leaves.add(leaveDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return leaves;
	}
	
	public List<Employee> countAssistantEmployee(long employeeId){
		List<Employee> employeeList = null;
		try {
			employeeList = EmployeeLocalServiceUtil.findBySupervisor(employeeId);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return employeeList;
	}
}

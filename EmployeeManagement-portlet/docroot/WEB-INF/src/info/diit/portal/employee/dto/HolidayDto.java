package info.diit.portal.employee.dto;

import java.util.Date;

public class HolidayDto {
	private long id;
	private DayTypeDto holidayType;
	private String date;
	private int weekNumber;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public DayTypeDto getHolidayType() {
		return holidayType;
	}
	public void setHolidayType(DayTypeDto holidayType) {
		this.holidayType = holidayType;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getWeekNumber() {
		return weekNumber;
	}
	public void setWeekNumber(int weekNumber) {
		this.weekNumber = weekNumber;
	}
}

package info.diit.portal.employee;

import info.diit.portal.employee.dto.EmployeeAttendanceDto;
import info.diit.portal.employee.dto.EmployeeDto;
import info.diit.portal.model.Employee;
import info.diit.portal.model.EmployeeAttendance;
import info.diit.portal.model.WorkingDay;
import info.diit.portal.service.EmployeeAttendanceLocalServiceUtil;
import info.diit.portal.service.EmployeeLocalServiceUtil;
import info.diit.portal.service.WorkingDayLocalServiceUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component.Event;
import com.vaadin.ui.Component.Listener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;

public class AttendanceViewApplication extends Application implements PortletRequestListener {

	private ThemeDisplay themeDisplay;
	
	private Window window;
	
	long organizationId;
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        try {
			organizationId = themeDisplay.getLayout().getGroup().getOrganizationId();
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
        loadEmployee();
        window.addComponent(mainLayout());
        
    }
    
    private ComboBox employeeComboBox;
    private DateField startDateField;
    private DateField endDateField;
    private List<EmployeeDto> employeeList;
    
    private Table attendanceTable;
    BeanItemContainer<EmployeeAttendanceDto> attendanceContainer;
    
    private final static String DATE_COLUMN = "date";
    private final static String REAL_IN_COLUMN = "realIn";
    private final static String REAL_OUT_COLUMN = "realOut";
    
    private final static String DATE_FORMAT = "dd/MM/yyyy";
    
    private GridLayout mainLayout(){
    	GridLayout mainLayout = new GridLayout(8, 8);
    	
    	employeeComboBox = new ComboBox("Employee");
    	startDateField = new DateField("Start Date");
    	endDateField = new DateField("End Date");
    	
    	employeeComboBox.setImmediate(true);
    	startDateField.setImmediate(true);
    	endDateField.setImmediate(true);
    	
    	startDateField.setDateFormat(DATE_FORMAT);
    	endDateField.setDateFormat(DATE_FORMAT);
    	
    	Calendar startCalendar = Calendar.getInstance();
    	startCalendar.set(Calendar.DAY_OF_MONTH, 1);
    	startDateField.setValue(startCalendar.getTime());
    	
    	
    	endDateField.setValue(new Date());
    	
    	if (employeeList!=null) {
			for (EmployeeDto employeeDto : employeeList) {
				employeeComboBox.addItem(employeeDto);
			}
		}
    	
    	employeeComboBox.addListener(new Listener() {
			
			@Override
			public void componentEvent(Event event) {
				EmployeeDto employee = (EmployeeDto) employeeComboBox.getValue();
				Date startDate = (Date) startDateField.getValue();
				Date endDate = (Date) endDateField.getValue();
				loadAttendance(employee.getId(), startDate, endDate);
			}
		});
    	
    	attendanceContainer = new BeanItemContainer<EmployeeAttendanceDto>(EmployeeAttendanceDto.class);
    	attendanceTable = new Table("", attendanceContainer);
    	attendanceTable.setWidth("100%");
    	
    	attendanceTable.setColumnHeader(DATE_COLUMN, "Date");
    	attendanceTable.setColumnHeader(REAL_IN_COLUMN, "In Time");
    	attendanceTable.setColumnHeader(REAL_OUT_COLUMN, "Out Time");
    	
    	attendanceTable.setVisibleColumns(new String[]{DATE_COLUMN, REAL_IN_COLUMN, REAL_OUT_COLUMN});
    	
    	
    	mainLayout.addComponent(employeeComboBox, 	0, 0, 1, 0);
    	mainLayout.addComponent(startDateField, 	3, 0, 4, 0);
    	mainLayout.addComponent(endDateField, 		6, 0, 7, 0);
    	mainLayout.addComponent(attendanceTable, 	0, 1, 7, 1);
    	
    	return mainLayout;
    }
    
    private void loadAttendance(long employeeId, Date startDate, Date endDate){
    	startDate.setHours(0);
    	startDate.setMinutes(0);
    	startDate.setSeconds(0);
    	
    	endDate.setHours(0);
    	endDate.setMinutes(0);
    	endDate.setSeconds(0);
    	
    	attendanceContainer.removeAllItems();
    	
    	DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(WorkingDay.class);
    	dynamicQuery.add(PropertyFactoryUtil.forName("organizationId").eq(organizationId));
		dynamicQuery.add(RestrictionsFactoryUtil.between("date", startDate, endDate));
		
		try {
			List<WorkingDay> workingDays = WorkingDayLocalServiceUtil.dynamicQuery(dynamicQuery);
			if (workingDays!=null) {
				for (WorkingDay workingDay : workingDays) {
					EmployeeAttendanceDto attendanceDto = new EmployeeAttendanceDto();
					attendanceDto.setDate(""+workingDay.getDate());
					
					List<EmployeeAttendance> attendanceList = EmployeeAttendanceLocalServiceUtil.findByEmployeeAttendance(employeeId, workingDay.getDate());
					if (attendanceList!=null && attendanceList.size()>0) {
						EmployeeAttendance realStart = attendanceList.get(0);
						EmployeeAttendance realEnd = attendanceList.get(attendanceList.size()-1);
						
						if (realStart!=null && realStart.getRealTime()!=null) {
							attendanceDto.setRealIn(""+realStart.getRealTime());
						}
						
						if (realEnd!=null && realEnd.getRealTime()!=null) {
							attendanceDto.setRealOut(""+realEnd.getRealTime());
						}
						
					}
					attendanceContainer.addBean(attendanceDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
    private void loadEmployee(){
    	employeeList = new ArrayList<EmployeeDto>();
    	try {
			List<Employee> employees = EmployeeLocalServiceUtil.findByOrganization(themeDisplay.getLayout().getGroup().getOrganizationId());
			if (employees!=null) {
				for (Employee employee : employees) {
					EmployeeDto employeeDto = new EmployeeDto();
					employeeDto.setId(employee.getEmployeeId());
					employeeDto.setName(employee.getName());
					employeeList.add(employeeDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
    }

	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

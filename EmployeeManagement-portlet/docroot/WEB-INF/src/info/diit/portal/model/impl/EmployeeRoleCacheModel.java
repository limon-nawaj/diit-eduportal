/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.EmployeeRole;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing EmployeeRole in entity cache.
 *
 * @author limon
 * @see EmployeeRole
 * @generated
 */
public class EmployeeRoleCacheModel implements CacheModel<EmployeeRole>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{employeeRoleId=");
		sb.append(employeeRoleId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", role=");
		sb.append(role);
		sb.append(", startTime=");
		sb.append(startTime);
		sb.append(", endTime=");
		sb.append(endTime);
		sb.append("}");

		return sb.toString();
	}

	public EmployeeRole toEntityModel() {
		EmployeeRoleImpl employeeRoleImpl = new EmployeeRoleImpl();

		employeeRoleImpl.setEmployeeRoleId(employeeRoleId);
		employeeRoleImpl.setOrganizationId(organizationId);
		employeeRoleImpl.setCompanyId(companyId);

		if (role == null) {
			employeeRoleImpl.setRole(StringPool.BLANK);
		}
		else {
			employeeRoleImpl.setRole(role);
		}

		if (startTime == Long.MIN_VALUE) {
			employeeRoleImpl.setStartTime(null);
		}
		else {
			employeeRoleImpl.setStartTime(new Date(startTime));
		}

		if (endTime == Long.MIN_VALUE) {
			employeeRoleImpl.setEndTime(null);
		}
		else {
			employeeRoleImpl.setEndTime(new Date(endTime));
		}

		employeeRoleImpl.resetOriginalValues();

		return employeeRoleImpl;
	}

	public long employeeRoleId;
	public long organizationId;
	public long companyId;
	public String role;
	public long startTime;
	public long endTime;
}
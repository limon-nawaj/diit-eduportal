/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.WorkingDay;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing WorkingDay in entity cache.
 *
 * @author limon
 * @see WorkingDay
 * @generated
 */
public class WorkingDayCacheModel implements CacheModel<WorkingDay>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{workingDayId=");
		sb.append(workingDayId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", date=");
		sb.append(date);
		sb.append(", dayOfWeek=");
		sb.append(dayOfWeek);
		sb.append(", week=");
		sb.append(week);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	public WorkingDay toEntityModel() {
		WorkingDayImpl workingDayImpl = new WorkingDayImpl();

		workingDayImpl.setWorkingDayId(workingDayId);
		workingDayImpl.setOrganizationId(organizationId);

		if (date == Long.MIN_VALUE) {
			workingDayImpl.setDate(null);
		}
		else {
			workingDayImpl.setDate(new Date(date));
		}

		workingDayImpl.setDayOfWeek(dayOfWeek);
		workingDayImpl.setWeek(week);
		workingDayImpl.setStatus(status);

		workingDayImpl.resetOriginalValues();

		return workingDayImpl;
	}

	public long workingDayId;
	public long organizationId;
	public long date;
	public int dayOfWeek;
	public int week;
	public long status;
}
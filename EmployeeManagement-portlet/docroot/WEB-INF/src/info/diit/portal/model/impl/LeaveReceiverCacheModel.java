/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.LeaveReceiver;

import java.io.Serializable;

/**
 * The cache model class for representing LeaveReceiver in entity cache.
 *
 * @author limon
 * @see LeaveReceiver
 * @generated
 */
public class LeaveReceiverCacheModel implements CacheModel<LeaveReceiver>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{leaveReceiverId=");
		sb.append(leaveReceiverId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", employeeId=");
		sb.append(employeeId);
		sb.append("}");

		return sb.toString();
	}

	public LeaveReceiver toEntityModel() {
		LeaveReceiverImpl leaveReceiverImpl = new LeaveReceiverImpl();

		leaveReceiverImpl.setLeaveReceiverId(leaveReceiverId);
		leaveReceiverImpl.setOrganizationId(organizationId);
		leaveReceiverImpl.setCompanyId(companyId);
		leaveReceiverImpl.setEmployeeId(employeeId);

		leaveReceiverImpl.resetOriginalValues();

		return leaveReceiverImpl;
	}

	public long leaveReceiverId;
	public long organizationId;
	public long companyId;
	public long employeeId;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.EmployeeTimeSchedule;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing EmployeeTimeSchedule in entity cache.
 *
 * @author limon
 * @see EmployeeTimeSchedule
 * @generated
 */
public class EmployeeTimeScheduleCacheModel implements CacheModel<EmployeeTimeSchedule>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{employeeTimeScheduleId=");
		sb.append(employeeTimeScheduleId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", employeeId=");
		sb.append(employeeId);
		sb.append(", day=");
		sb.append(day);
		sb.append(", startTime=");
		sb.append(startTime);
		sb.append(", endTime=");
		sb.append(endTime);
		sb.append(", duration=");
		sb.append(duration);
		sb.append("}");

		return sb.toString();
	}

	public EmployeeTimeSchedule toEntityModel() {
		EmployeeTimeScheduleImpl employeeTimeScheduleImpl = new EmployeeTimeScheduleImpl();

		employeeTimeScheduleImpl.setEmployeeTimeScheduleId(employeeTimeScheduleId);
		employeeTimeScheduleImpl.setOrganizationId(organizationId);
		employeeTimeScheduleImpl.setCompanyId(companyId);
		employeeTimeScheduleImpl.setEmployeeId(employeeId);
		employeeTimeScheduleImpl.setDay(day);

		if (startTime == Long.MIN_VALUE) {
			employeeTimeScheduleImpl.setStartTime(null);
		}
		else {
			employeeTimeScheduleImpl.setStartTime(new Date(startTime));
		}

		if (endTime == Long.MIN_VALUE) {
			employeeTimeScheduleImpl.setEndTime(null);
		}
		else {
			employeeTimeScheduleImpl.setEndTime(new Date(endTime));
		}

		employeeTimeScheduleImpl.setDuration(duration);

		employeeTimeScheduleImpl.resetOriginalValues();

		return employeeTimeScheduleImpl;
	}

	public long employeeTimeScheduleId;
	public long organizationId;
	public long companyId;
	public long employeeId;
	public int day;
	public long startTime;
	public long endTime;
	public double duration;
}
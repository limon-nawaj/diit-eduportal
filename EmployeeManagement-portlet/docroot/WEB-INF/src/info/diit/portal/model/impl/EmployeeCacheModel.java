/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.Employee;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Employee in entity cache.
 *
 * @author limon
 * @see Employee
 * @generated
 */
public class EmployeeCacheModel implements CacheModel<Employee>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(49);

		sb.append("{employeeId=");
		sb.append(employeeId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", employeeUserId=");
		sb.append(employeeUserId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", gender=");
		sb.append(gender);
		sb.append(", religion=");
		sb.append(religion);
		sb.append(", blood=");
		sb.append(blood);
		sb.append(", dob=");
		sb.append(dob);
		sb.append(", designation=");
		sb.append(designation);
		sb.append(", branch=");
		sb.append(branch);
		sb.append(", status=");
		sb.append(status);
		sb.append(", presentAddress=");
		sb.append(presentAddress);
		sb.append(", permanentAddress=");
		sb.append(permanentAddress);
		sb.append(", photo=");
		sb.append(photo);
		sb.append(", supervisor=");
		sb.append(supervisor);
		sb.append(", role=");
		sb.append(role);
		sb.append(", duration=");
		sb.append(duration);
		sb.append(", minimumDuration=");
		sb.append(minimumDuration);
		sb.append(", type=");
		sb.append(type);
		sb.append("}");

		return sb.toString();
	}

	public Employee toEntityModel() {
		EmployeeImpl employeeImpl = new EmployeeImpl();

		employeeImpl.setEmployeeId(employeeId);
		employeeImpl.setCompanyId(companyId);
		employeeImpl.setOrganizationId(organizationId);
		employeeImpl.setUserId(userId);

		if (userName == null) {
			employeeImpl.setUserName(StringPool.BLANK);
		}
		else {
			employeeImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			employeeImpl.setCreateDate(null);
		}
		else {
			employeeImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			employeeImpl.setModifiedDate(null);
		}
		else {
			employeeImpl.setModifiedDate(new Date(modifiedDate));
		}

		employeeImpl.setEmployeeUserId(employeeUserId);

		if (name == null) {
			employeeImpl.setName(StringPool.BLANK);
		}
		else {
			employeeImpl.setName(name);
		}

		employeeImpl.setGender(gender);
		employeeImpl.setReligion(religion);
		employeeImpl.setBlood(blood);

		if (dob == Long.MIN_VALUE) {
			employeeImpl.setDob(null);
		}
		else {
			employeeImpl.setDob(new Date(dob));
		}

		employeeImpl.setDesignation(designation);
		employeeImpl.setBranch(branch);
		employeeImpl.setStatus(status);

		if (presentAddress == null) {
			employeeImpl.setPresentAddress(StringPool.BLANK);
		}
		else {
			employeeImpl.setPresentAddress(presentAddress);
		}

		if (permanentAddress == null) {
			employeeImpl.setPermanentAddress(StringPool.BLANK);
		}
		else {
			employeeImpl.setPermanentAddress(permanentAddress);
		}

		employeeImpl.setPhoto(photo);
		employeeImpl.setSupervisor(supervisor);
		employeeImpl.setRole(role);
		employeeImpl.setDuration(duration);
		employeeImpl.setMinimumDuration(minimumDuration);
		employeeImpl.setType(type);

		employeeImpl.resetOriginalValues();

		return employeeImpl;
	}

	public long employeeId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long employeeUserId;
	public String name;
	public long gender;
	public long religion;
	public long blood;
	public long dob;
	public long designation;
	public long branch;
	public long status;
	public String presentAddress;
	public String permanentAddress;
	public long photo;
	public long supervisor;
	public long role;
	public Double duration;
	public Double minimumDuration;
	public int type;
}
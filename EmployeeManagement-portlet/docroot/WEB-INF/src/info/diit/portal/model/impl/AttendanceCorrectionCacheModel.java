/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.AttendanceCorrection;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing AttendanceCorrection in entity cache.
 *
 * @author limon
 * @see AttendanceCorrection
 * @generated
 */
public class AttendanceCorrectionCacheModel implements CacheModel<AttendanceCorrection>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{attendanceCorrectionId=");
		sb.append(attendanceCorrectionId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", employeeId=");
		sb.append(employeeId);
		sb.append(", correctionDate=");
		sb.append(correctionDate);
		sb.append(", realIn=");
		sb.append(realIn);
		sb.append(", realOut=");
		sb.append(realOut);
		sb.append(", expectedIn=");
		sb.append(expectedIn);
		sb.append(", expectedOut=");
		sb.append(expectedOut);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	public AttendanceCorrection toEntityModel() {
		AttendanceCorrectionImpl attendanceCorrectionImpl = new AttendanceCorrectionImpl();

		attendanceCorrectionImpl.setAttendanceCorrectionId(attendanceCorrectionId);
		attendanceCorrectionImpl.setOrganizationId(organizationId);
		attendanceCorrectionImpl.setCompanyId(companyId);
		attendanceCorrectionImpl.setEmployeeId(employeeId);

		if (correctionDate == Long.MIN_VALUE) {
			attendanceCorrectionImpl.setCorrectionDate(null);
		}
		else {
			attendanceCorrectionImpl.setCorrectionDate(new Date(correctionDate));
		}

		if (realIn == Long.MIN_VALUE) {
			attendanceCorrectionImpl.setRealIn(null);
		}
		else {
			attendanceCorrectionImpl.setRealIn(new Date(realIn));
		}

		if (realOut == Long.MIN_VALUE) {
			attendanceCorrectionImpl.setRealOut(null);
		}
		else {
			attendanceCorrectionImpl.setRealOut(new Date(realOut));
		}

		if (expectedIn == Long.MIN_VALUE) {
			attendanceCorrectionImpl.setExpectedIn(null);
		}
		else {
			attendanceCorrectionImpl.setExpectedIn(new Date(expectedIn));
		}

		if (expectedOut == Long.MIN_VALUE) {
			attendanceCorrectionImpl.setExpectedOut(null);
		}
		else {
			attendanceCorrectionImpl.setExpectedOut(new Date(expectedOut));
		}

		attendanceCorrectionImpl.setStatus(status);

		attendanceCorrectionImpl.resetOriginalValues();

		return attendanceCorrectionImpl;
	}

	public long attendanceCorrectionId;
	public long organizationId;
	public long companyId;
	public long employeeId;
	public long correctionDate;
	public long realIn;
	public long realOut;
	public long expectedIn;
	public long expectedOut;
	public int status;
}
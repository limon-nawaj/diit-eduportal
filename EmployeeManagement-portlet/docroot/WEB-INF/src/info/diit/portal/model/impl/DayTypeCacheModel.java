/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.DayType;

import java.io.Serializable;

/**
 * The cache model class for representing DayType in entity cache.
 *
 * @author limon
 * @see DayType
 * @generated
 */
public class DayTypeCacheModel implements CacheModel<DayType>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{dayTypeId=");
		sb.append(dayTypeId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", type=");
		sb.append(type);
		sb.append("}");

		return sb.toString();
	}

	public DayType toEntityModel() {
		DayTypeImpl dayTypeImpl = new DayTypeImpl();

		dayTypeImpl.setDayTypeId(dayTypeId);
		dayTypeImpl.setOrganizationId(organizationId);

		if (type == null) {
			dayTypeImpl.setType(StringPool.BLANK);
		}
		else {
			dayTypeImpl.setType(type);
		}

		dayTypeImpl.resetOriginalValues();

		return dayTypeImpl;
	}

	public long dayTypeId;
	public long organizationId;
	public String type;
}
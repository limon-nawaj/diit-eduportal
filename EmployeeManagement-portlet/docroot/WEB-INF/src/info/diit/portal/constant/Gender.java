package info.diit.portal.constant;


public enum Gender {

	MALE(1, "Male"), 
	FEMALE(2, "Female");
	private int key;
	private String value;
	
	private Gender(int key, String value){
		this.key = key;
		this.value = value;
	}
	
	public int getKey() {
		return key;
	}
	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return value;
	}
	
	public static Gender getGender(int key){
		Gender genders[] = Gender.values();
		for (Gender gender : genders) {
			if (gender.key==key) {
				return gender;
			}
		}
		return null;
	}
}

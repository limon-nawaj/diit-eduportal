package info.diit.portal.constant;

public enum Day {

	FULL_DAY(1, "Full Day"), 
	HAFL_DAY(2, "Half Day");
	private int key;
	private String value;
	
	private Day(int key, String value){
		this.key = key;
		this.value = value;
	}
	
	public int getKey() {
		return key;
	}
	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return value;
	}
	
	public static Day getDay(int key){
		Day days[] = Day.values();
		for (Day day : days) {
			if (day.key==key) {
				return day;
			}
		}
		return null;
	}
}

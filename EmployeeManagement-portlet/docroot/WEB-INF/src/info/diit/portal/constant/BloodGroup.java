package info.diit.portal.constant;

public enum BloodGroup {

	O_POSITIVE(1, "O+"), O_NEGATIVE(2, "O-"),
	A_POSITIVE(3, "A+"), A_NEGATIVE(4, "A-"),
	B_POSITIVE(5, "B+"), B_NEGATIVE(6, "B-"),
	AB_POSITIVE(7, "AB+"), AB_NEGATIVE(8, "AB-");
	
	private int key;
	private String value;
	
	private BloodGroup(int key, String value){
		this.key = key;
		this.value = value;
	}
	
	public int getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value;
	}
	
	public static BloodGroup getBloodGroup(int key){
		BloodGroup bloodGroups[] = BloodGroup.values();
		for (BloodGroup bloodGroup : bloodGroups) {
			if (bloodGroup.key==key) {
				return bloodGroup;
			}
		}
		return null;
	}
}

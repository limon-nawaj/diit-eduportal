package info.diit.portal.constant;

public enum EmployeeType {
	HOURLY(1, "Hourly"),
	FIX(2, "Fix");
	private int key;
	private String value;
	
	private EmployeeType(int key, String value){
		this.key = key;
		this.value = value;
	}

	public int getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return value;
	}
	
	public static EmployeeType getDurationType(int key){
		EmployeeType durationTypes[] = EmployeeType.values();
		for (EmployeeType durationType : durationTypes) {
			if (durationType.key==key) {
				return durationType;
			}
		}
		return null;
	}
}

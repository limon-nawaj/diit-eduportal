package info.diit.portal.constant;

public enum Religion {

	ISLAM(1, "Islam"), HINDUISM(2, "Hinduism"), BUDDHISM(3, "Buddhism"), CHRISTIANITY(4, "Christianity");
	
	private int key;
	private String value;
	
	private Religion(int key, String value){
		this.key = key;
		this.value = value;
	}

	public int getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value;
	}
	
	public static Religion getReligion(int key){
		Religion religions[] = Religion.values();
		for (Religion religion : religions) {
			if (religion.key==key) {
				return religion;
			}
		}
		return null;
	}
}

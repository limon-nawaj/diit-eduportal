/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link EmployeeAttendance}.
 * </p>
 *
 * @author    limon
 * @see       EmployeeAttendance
 * @generated
 */
public class EmployeeAttendanceWrapper implements EmployeeAttendance,
	ModelWrapper<EmployeeAttendance> {
	public EmployeeAttendanceWrapper(EmployeeAttendance employeeAttendance) {
		_employeeAttendance = employeeAttendance;
	}

	public Class<?> getModelClass() {
		return EmployeeAttendance.class;
	}

	public String getModelClassName() {
		return EmployeeAttendance.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("employeeAttendanceId", getEmployeeAttendanceId());
		attributes.put("employeeId", getEmployeeId());
		attributes.put("expectedStart", getExpectedStart());
		attributes.put("expectedEnd", getExpectedEnd());
		attributes.put("realTime", getRealTime());
		attributes.put("status", getStatus());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long employeeAttendanceId = (Long)attributes.get("employeeAttendanceId");

		if (employeeAttendanceId != null) {
			setEmployeeAttendanceId(employeeAttendanceId);
		}

		Long employeeId = (Long)attributes.get("employeeId");

		if (employeeId != null) {
			setEmployeeId(employeeId);
		}

		Date expectedStart = (Date)attributes.get("expectedStart");

		if (expectedStart != null) {
			setExpectedStart(expectedStart);
		}

		Date expectedEnd = (Date)attributes.get("expectedEnd");

		if (expectedEnd != null) {
			setExpectedEnd(expectedEnd);
		}

		Date realTime = (Date)attributes.get("realTime");

		if (realTime != null) {
			setRealTime(realTime);
		}

		Long status = (Long)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	/**
	* Returns the primary key of this employee attendance.
	*
	* @return the primary key of this employee attendance
	*/
	public long getPrimaryKey() {
		return _employeeAttendance.getPrimaryKey();
	}

	/**
	* Sets the primary key of this employee attendance.
	*
	* @param primaryKey the primary key of this employee attendance
	*/
	public void setPrimaryKey(long primaryKey) {
		_employeeAttendance.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the employee attendance ID of this employee attendance.
	*
	* @return the employee attendance ID of this employee attendance
	*/
	public long getEmployeeAttendanceId() {
		return _employeeAttendance.getEmployeeAttendanceId();
	}

	/**
	* Sets the employee attendance ID of this employee attendance.
	*
	* @param employeeAttendanceId the employee attendance ID of this employee attendance
	*/
	public void setEmployeeAttendanceId(long employeeAttendanceId) {
		_employeeAttendance.setEmployeeAttendanceId(employeeAttendanceId);
	}

	/**
	* Returns the employee ID of this employee attendance.
	*
	* @return the employee ID of this employee attendance
	*/
	public long getEmployeeId() {
		return _employeeAttendance.getEmployeeId();
	}

	/**
	* Sets the employee ID of this employee attendance.
	*
	* @param employeeId the employee ID of this employee attendance
	*/
	public void setEmployeeId(long employeeId) {
		_employeeAttendance.setEmployeeId(employeeId);
	}

	/**
	* Returns the expected start of this employee attendance.
	*
	* @return the expected start of this employee attendance
	*/
	public java.util.Date getExpectedStart() {
		return _employeeAttendance.getExpectedStart();
	}

	/**
	* Sets the expected start of this employee attendance.
	*
	* @param expectedStart the expected start of this employee attendance
	*/
	public void setExpectedStart(java.util.Date expectedStart) {
		_employeeAttendance.setExpectedStart(expectedStart);
	}

	/**
	* Returns the expected end of this employee attendance.
	*
	* @return the expected end of this employee attendance
	*/
	public java.util.Date getExpectedEnd() {
		return _employeeAttendance.getExpectedEnd();
	}

	/**
	* Sets the expected end of this employee attendance.
	*
	* @param expectedEnd the expected end of this employee attendance
	*/
	public void setExpectedEnd(java.util.Date expectedEnd) {
		_employeeAttendance.setExpectedEnd(expectedEnd);
	}

	/**
	* Returns the real time of this employee attendance.
	*
	* @return the real time of this employee attendance
	*/
	public java.util.Date getRealTime() {
		return _employeeAttendance.getRealTime();
	}

	/**
	* Sets the real time of this employee attendance.
	*
	* @param realTime the real time of this employee attendance
	*/
	public void setRealTime(java.util.Date realTime) {
		_employeeAttendance.setRealTime(realTime);
	}

	/**
	* Returns the status of this employee attendance.
	*
	* @return the status of this employee attendance
	*/
	public long getStatus() {
		return _employeeAttendance.getStatus();
	}

	/**
	* Sets the status of this employee attendance.
	*
	* @param status the status of this employee attendance
	*/
	public void setStatus(long status) {
		_employeeAttendance.setStatus(status);
	}

	public boolean isNew() {
		return _employeeAttendance.isNew();
	}

	public void setNew(boolean n) {
		_employeeAttendance.setNew(n);
	}

	public boolean isCachedModel() {
		return _employeeAttendance.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_employeeAttendance.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _employeeAttendance.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _employeeAttendance.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_employeeAttendance.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _employeeAttendance.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_employeeAttendance.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new EmployeeAttendanceWrapper((EmployeeAttendance)_employeeAttendance.clone());
	}

	public int compareTo(
		info.diit.portal.model.EmployeeAttendance employeeAttendance) {
		return _employeeAttendance.compareTo(employeeAttendance);
	}

	@Override
	public int hashCode() {
		return _employeeAttendance.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.EmployeeAttendance> toCacheModel() {
		return _employeeAttendance.toCacheModel();
	}

	public info.diit.portal.model.EmployeeAttendance toEscapedModel() {
		return new EmployeeAttendanceWrapper(_employeeAttendance.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _employeeAttendance.toString();
	}

	public java.lang.String toXmlString() {
		return _employeeAttendance.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_employeeAttendance.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public EmployeeAttendance getWrappedEmployeeAttendance() {
		return _employeeAttendance;
	}

	public EmployeeAttendance getWrappedModel() {
		return _employeeAttendance;
	}

	public void resetOriginalValues() {
		_employeeAttendance.resetOriginalValues();
	}

	private EmployeeAttendance _employeeAttendance;
}
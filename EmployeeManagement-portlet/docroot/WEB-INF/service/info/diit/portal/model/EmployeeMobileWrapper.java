/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link EmployeeMobile}.
 * </p>
 *
 * @author    limon
 * @see       EmployeeMobile
 * @generated
 */
public class EmployeeMobileWrapper implements EmployeeMobile,
	ModelWrapper<EmployeeMobile> {
	public EmployeeMobileWrapper(EmployeeMobile employeeMobile) {
		_employeeMobile = employeeMobile;
	}

	public Class<?> getModelClass() {
		return EmployeeMobile.class;
	}

	public String getModelClassName() {
		return EmployeeMobile.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("mobileId", getMobileId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("employeeId", getEmployeeId());
		attributes.put("mobile", getMobile());
		attributes.put("homePhone", getHomePhone());
		attributes.put("status", getStatus());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long mobileId = (Long)attributes.get("mobileId");

		if (mobileId != null) {
			setMobileId(mobileId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long employeeId = (Long)attributes.get("employeeId");

		if (employeeId != null) {
			setEmployeeId(employeeId);
		}

		String mobile = (String)attributes.get("mobile");

		if (mobile != null) {
			setMobile(mobile);
		}

		String homePhone = (String)attributes.get("homePhone");

		if (homePhone != null) {
			setHomePhone(homePhone);
		}

		Long status = (Long)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	/**
	* Returns the primary key of this employee mobile.
	*
	* @return the primary key of this employee mobile
	*/
	public long getPrimaryKey() {
		return _employeeMobile.getPrimaryKey();
	}

	/**
	* Sets the primary key of this employee mobile.
	*
	* @param primaryKey the primary key of this employee mobile
	*/
	public void setPrimaryKey(long primaryKey) {
		_employeeMobile.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the mobile ID of this employee mobile.
	*
	* @return the mobile ID of this employee mobile
	*/
	public long getMobileId() {
		return _employeeMobile.getMobileId();
	}

	/**
	* Sets the mobile ID of this employee mobile.
	*
	* @param mobileId the mobile ID of this employee mobile
	*/
	public void setMobileId(long mobileId) {
		_employeeMobile.setMobileId(mobileId);
	}

	/**
	* Returns the company ID of this employee mobile.
	*
	* @return the company ID of this employee mobile
	*/
	public long getCompanyId() {
		return _employeeMobile.getCompanyId();
	}

	/**
	* Sets the company ID of this employee mobile.
	*
	* @param companyId the company ID of this employee mobile
	*/
	public void setCompanyId(long companyId) {
		_employeeMobile.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this employee mobile.
	*
	* @return the user ID of this employee mobile
	*/
	public long getUserId() {
		return _employeeMobile.getUserId();
	}

	/**
	* Sets the user ID of this employee mobile.
	*
	* @param userId the user ID of this employee mobile
	*/
	public void setUserId(long userId) {
		_employeeMobile.setUserId(userId);
	}

	/**
	* Returns the user uuid of this employee mobile.
	*
	* @return the user uuid of this employee mobile
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobile.getUserUuid();
	}

	/**
	* Sets the user uuid of this employee mobile.
	*
	* @param userUuid the user uuid of this employee mobile
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_employeeMobile.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this employee mobile.
	*
	* @return the user name of this employee mobile
	*/
	public java.lang.String getUserName() {
		return _employeeMobile.getUserName();
	}

	/**
	* Sets the user name of this employee mobile.
	*
	* @param userName the user name of this employee mobile
	*/
	public void setUserName(java.lang.String userName) {
		_employeeMobile.setUserName(userName);
	}

	/**
	* Returns the create date of this employee mobile.
	*
	* @return the create date of this employee mobile
	*/
	public java.util.Date getCreateDate() {
		return _employeeMobile.getCreateDate();
	}

	/**
	* Sets the create date of this employee mobile.
	*
	* @param createDate the create date of this employee mobile
	*/
	public void setCreateDate(java.util.Date createDate) {
		_employeeMobile.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this employee mobile.
	*
	* @return the modified date of this employee mobile
	*/
	public java.util.Date getModifiedDate() {
		return _employeeMobile.getModifiedDate();
	}

	/**
	* Sets the modified date of this employee mobile.
	*
	* @param modifiedDate the modified date of this employee mobile
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_employeeMobile.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the employee ID of this employee mobile.
	*
	* @return the employee ID of this employee mobile
	*/
	public long getEmployeeId() {
		return _employeeMobile.getEmployeeId();
	}

	/**
	* Sets the employee ID of this employee mobile.
	*
	* @param employeeId the employee ID of this employee mobile
	*/
	public void setEmployeeId(long employeeId) {
		_employeeMobile.setEmployeeId(employeeId);
	}

	/**
	* Returns the mobile of this employee mobile.
	*
	* @return the mobile of this employee mobile
	*/
	public java.lang.String getMobile() {
		return _employeeMobile.getMobile();
	}

	/**
	* Sets the mobile of this employee mobile.
	*
	* @param mobile the mobile of this employee mobile
	*/
	public void setMobile(java.lang.String mobile) {
		_employeeMobile.setMobile(mobile);
	}

	/**
	* Returns the home phone of this employee mobile.
	*
	* @return the home phone of this employee mobile
	*/
	public java.lang.String getHomePhone() {
		return _employeeMobile.getHomePhone();
	}

	/**
	* Sets the home phone of this employee mobile.
	*
	* @param homePhone the home phone of this employee mobile
	*/
	public void setHomePhone(java.lang.String homePhone) {
		_employeeMobile.setHomePhone(homePhone);
	}

	/**
	* Returns the status of this employee mobile.
	*
	* @return the status of this employee mobile
	*/
	public long getStatus() {
		return _employeeMobile.getStatus();
	}

	/**
	* Sets the status of this employee mobile.
	*
	* @param status the status of this employee mobile
	*/
	public void setStatus(long status) {
		_employeeMobile.setStatus(status);
	}

	public boolean isNew() {
		return _employeeMobile.isNew();
	}

	public void setNew(boolean n) {
		_employeeMobile.setNew(n);
	}

	public boolean isCachedModel() {
		return _employeeMobile.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_employeeMobile.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _employeeMobile.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _employeeMobile.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_employeeMobile.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _employeeMobile.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_employeeMobile.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new EmployeeMobileWrapper((EmployeeMobile)_employeeMobile.clone());
	}

	public int compareTo(info.diit.portal.model.EmployeeMobile employeeMobile) {
		return _employeeMobile.compareTo(employeeMobile);
	}

	@Override
	public int hashCode() {
		return _employeeMobile.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.EmployeeMobile> toCacheModel() {
		return _employeeMobile.toCacheModel();
	}

	public info.diit.portal.model.EmployeeMobile toEscapedModel() {
		return new EmployeeMobileWrapper(_employeeMobile.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _employeeMobile.toString();
	}

	public java.lang.String toXmlString() {
		return _employeeMobile.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_employeeMobile.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public EmployeeMobile getWrappedEmployeeMobile() {
		return _employeeMobile;
	}

	public EmployeeMobile getWrappedModel() {
		return _employeeMobile;
	}

	public void resetOriginalValues() {
		_employeeMobile.resetOriginalValues();
	}

	private EmployeeMobile _employeeMobile;
}
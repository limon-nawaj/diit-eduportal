/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Leave}.
 * </p>
 *
 * @author    limon
 * @see       Leave
 * @generated
 */
public class LeaveWrapper implements Leave, ModelWrapper<Leave> {
	public LeaveWrapper(Leave leave) {
		_leave = leave;
	}

	public Class<?> getModelClass() {
		return Leave.class;
	}

	public String getModelClassName() {
		return Leave.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("leaveId", getLeaveId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("applicationDate", getApplicationDate());
		attributes.put("employee", getEmployee());
		attributes.put("responsibleEmployee", getResponsibleEmployee());
		attributes.put("startDate", getStartDate());
		attributes.put("endDate", getEndDate());
		attributes.put("numberOfDay", getNumberOfDay());
		attributes.put("phoneNumber", getPhoneNumber());
		attributes.put("causeOfLeave", getCauseOfLeave());
		attributes.put("whereEnjoy", getWhereEnjoy());
		attributes.put("firstRecommendation", getFirstRecommendation());
		attributes.put("secondRecommendation", getSecondRecommendation());
		attributes.put("leaveCategory", getLeaveCategory());
		attributes.put("responsibleEmployeeStatus",
			getResponsibleEmployeeStatus());
		attributes.put("applicationStatus", getApplicationStatus());
		attributes.put("comments", getComments());
		attributes.put("complementedBy", getComplementedBy());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long leaveId = (Long)attributes.get("leaveId");

		if (leaveId != null) {
			setLeaveId(leaveId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date applicationDate = (Date)attributes.get("applicationDate");

		if (applicationDate != null) {
			setApplicationDate(applicationDate);
		}

		Long employee = (Long)attributes.get("employee");

		if (employee != null) {
			setEmployee(employee);
		}

		Long responsibleEmployee = (Long)attributes.get("responsibleEmployee");

		if (responsibleEmployee != null) {
			setResponsibleEmployee(responsibleEmployee);
		}

		Date startDate = (Date)attributes.get("startDate");

		if (startDate != null) {
			setStartDate(startDate);
		}

		Date endDate = (Date)attributes.get("endDate");

		if (endDate != null) {
			setEndDate(endDate);
		}

		Double numberOfDay = (Double)attributes.get("numberOfDay");

		if (numberOfDay != null) {
			setNumberOfDay(numberOfDay);
		}

		String phoneNumber = (String)attributes.get("phoneNumber");

		if (phoneNumber != null) {
			setPhoneNumber(phoneNumber);
		}

		String causeOfLeave = (String)attributes.get("causeOfLeave");

		if (causeOfLeave != null) {
			setCauseOfLeave(causeOfLeave);
		}

		String whereEnjoy = (String)attributes.get("whereEnjoy");

		if (whereEnjoy != null) {
			setWhereEnjoy(whereEnjoy);
		}

		Long firstRecommendation = (Long)attributes.get("firstRecommendation");

		if (firstRecommendation != null) {
			setFirstRecommendation(firstRecommendation);
		}

		Long secondRecommendation = (Long)attributes.get("secondRecommendation");

		if (secondRecommendation != null) {
			setSecondRecommendation(secondRecommendation);
		}

		Long leaveCategory = (Long)attributes.get("leaveCategory");

		if (leaveCategory != null) {
			setLeaveCategory(leaveCategory);
		}

		Integer responsibleEmployeeStatus = (Integer)attributes.get(
				"responsibleEmployeeStatus");

		if (responsibleEmployeeStatus != null) {
			setResponsibleEmployeeStatus(responsibleEmployeeStatus);
		}

		Integer applicationStatus = (Integer)attributes.get("applicationStatus");

		if (applicationStatus != null) {
			setApplicationStatus(applicationStatus);
		}

		String comments = (String)attributes.get("comments");

		if (comments != null) {
			setComments(comments);
		}

		Long complementedBy = (Long)attributes.get("complementedBy");

		if (complementedBy != null) {
			setComplementedBy(complementedBy);
		}
	}

	/**
	* Returns the primary key of this leave.
	*
	* @return the primary key of this leave
	*/
	public long getPrimaryKey() {
		return _leave.getPrimaryKey();
	}

	/**
	* Sets the primary key of this leave.
	*
	* @param primaryKey the primary key of this leave
	*/
	public void setPrimaryKey(long primaryKey) {
		_leave.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the leave ID of this leave.
	*
	* @return the leave ID of this leave
	*/
	public long getLeaveId() {
		return _leave.getLeaveId();
	}

	/**
	* Sets the leave ID of this leave.
	*
	* @param leaveId the leave ID of this leave
	*/
	public void setLeaveId(long leaveId) {
		_leave.setLeaveId(leaveId);
	}

	/**
	* Returns the company ID of this leave.
	*
	* @return the company ID of this leave
	*/
	public long getCompanyId() {
		return _leave.getCompanyId();
	}

	/**
	* Sets the company ID of this leave.
	*
	* @param companyId the company ID of this leave
	*/
	public void setCompanyId(long companyId) {
		_leave.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this leave.
	*
	* @return the organization ID of this leave
	*/
	public long getOrganizationId() {
		return _leave.getOrganizationId();
	}

	/**
	* Sets the organization ID of this leave.
	*
	* @param organizationId the organization ID of this leave
	*/
	public void setOrganizationId(long organizationId) {
		_leave.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this leave.
	*
	* @return the user ID of this leave
	*/
	public long getUserId() {
		return _leave.getUserId();
	}

	/**
	* Sets the user ID of this leave.
	*
	* @param userId the user ID of this leave
	*/
	public void setUserId(long userId) {
		_leave.setUserId(userId);
	}

	/**
	* Returns the user uuid of this leave.
	*
	* @return the user uuid of this leave
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leave.getUserUuid();
	}

	/**
	* Sets the user uuid of this leave.
	*
	* @param userUuid the user uuid of this leave
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_leave.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this leave.
	*
	* @return the user name of this leave
	*/
	public java.lang.String getUserName() {
		return _leave.getUserName();
	}

	/**
	* Sets the user name of this leave.
	*
	* @param userName the user name of this leave
	*/
	public void setUserName(java.lang.String userName) {
		_leave.setUserName(userName);
	}

	/**
	* Returns the create date of this leave.
	*
	* @return the create date of this leave
	*/
	public java.util.Date getCreateDate() {
		return _leave.getCreateDate();
	}

	/**
	* Sets the create date of this leave.
	*
	* @param createDate the create date of this leave
	*/
	public void setCreateDate(java.util.Date createDate) {
		_leave.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this leave.
	*
	* @return the modified date of this leave
	*/
	public java.util.Date getModifiedDate() {
		return _leave.getModifiedDate();
	}

	/**
	* Sets the modified date of this leave.
	*
	* @param modifiedDate the modified date of this leave
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_leave.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the application date of this leave.
	*
	* @return the application date of this leave
	*/
	public java.util.Date getApplicationDate() {
		return _leave.getApplicationDate();
	}

	/**
	* Sets the application date of this leave.
	*
	* @param applicationDate the application date of this leave
	*/
	public void setApplicationDate(java.util.Date applicationDate) {
		_leave.setApplicationDate(applicationDate);
	}

	/**
	* Returns the employee of this leave.
	*
	* @return the employee of this leave
	*/
	public long getEmployee() {
		return _leave.getEmployee();
	}

	/**
	* Sets the employee of this leave.
	*
	* @param employee the employee of this leave
	*/
	public void setEmployee(long employee) {
		_leave.setEmployee(employee);
	}

	/**
	* Returns the responsible employee of this leave.
	*
	* @return the responsible employee of this leave
	*/
	public long getResponsibleEmployee() {
		return _leave.getResponsibleEmployee();
	}

	/**
	* Sets the responsible employee of this leave.
	*
	* @param responsibleEmployee the responsible employee of this leave
	*/
	public void setResponsibleEmployee(long responsibleEmployee) {
		_leave.setResponsibleEmployee(responsibleEmployee);
	}

	/**
	* Returns the start date of this leave.
	*
	* @return the start date of this leave
	*/
	public java.util.Date getStartDate() {
		return _leave.getStartDate();
	}

	/**
	* Sets the start date of this leave.
	*
	* @param startDate the start date of this leave
	*/
	public void setStartDate(java.util.Date startDate) {
		_leave.setStartDate(startDate);
	}

	/**
	* Returns the end date of this leave.
	*
	* @return the end date of this leave
	*/
	public java.util.Date getEndDate() {
		return _leave.getEndDate();
	}

	/**
	* Sets the end date of this leave.
	*
	* @param endDate the end date of this leave
	*/
	public void setEndDate(java.util.Date endDate) {
		_leave.setEndDate(endDate);
	}

	/**
	* Returns the number of day of this leave.
	*
	* @return the number of day of this leave
	*/
	public double getNumberOfDay() {
		return _leave.getNumberOfDay();
	}

	/**
	* Sets the number of day of this leave.
	*
	* @param numberOfDay the number of day of this leave
	*/
	public void setNumberOfDay(double numberOfDay) {
		_leave.setNumberOfDay(numberOfDay);
	}

	/**
	* Returns the phone number of this leave.
	*
	* @return the phone number of this leave
	*/
	public java.lang.String getPhoneNumber() {
		return _leave.getPhoneNumber();
	}

	/**
	* Sets the phone number of this leave.
	*
	* @param phoneNumber the phone number of this leave
	*/
	public void setPhoneNumber(java.lang.String phoneNumber) {
		_leave.setPhoneNumber(phoneNumber);
	}

	/**
	* Returns the cause of leave of this leave.
	*
	* @return the cause of leave of this leave
	*/
	public java.lang.String getCauseOfLeave() {
		return _leave.getCauseOfLeave();
	}

	/**
	* Sets the cause of leave of this leave.
	*
	* @param causeOfLeave the cause of leave of this leave
	*/
	public void setCauseOfLeave(java.lang.String causeOfLeave) {
		_leave.setCauseOfLeave(causeOfLeave);
	}

	/**
	* Returns the where enjoy of this leave.
	*
	* @return the where enjoy of this leave
	*/
	public java.lang.String getWhereEnjoy() {
		return _leave.getWhereEnjoy();
	}

	/**
	* Sets the where enjoy of this leave.
	*
	* @param whereEnjoy the where enjoy of this leave
	*/
	public void setWhereEnjoy(java.lang.String whereEnjoy) {
		_leave.setWhereEnjoy(whereEnjoy);
	}

	/**
	* Returns the first recommendation of this leave.
	*
	* @return the first recommendation of this leave
	*/
	public long getFirstRecommendation() {
		return _leave.getFirstRecommendation();
	}

	/**
	* Sets the first recommendation of this leave.
	*
	* @param firstRecommendation the first recommendation of this leave
	*/
	public void setFirstRecommendation(long firstRecommendation) {
		_leave.setFirstRecommendation(firstRecommendation);
	}

	/**
	* Returns the second recommendation of this leave.
	*
	* @return the second recommendation of this leave
	*/
	public long getSecondRecommendation() {
		return _leave.getSecondRecommendation();
	}

	/**
	* Sets the second recommendation of this leave.
	*
	* @param secondRecommendation the second recommendation of this leave
	*/
	public void setSecondRecommendation(long secondRecommendation) {
		_leave.setSecondRecommendation(secondRecommendation);
	}

	/**
	* Returns the leave category of this leave.
	*
	* @return the leave category of this leave
	*/
	public long getLeaveCategory() {
		return _leave.getLeaveCategory();
	}

	/**
	* Sets the leave category of this leave.
	*
	* @param leaveCategory the leave category of this leave
	*/
	public void setLeaveCategory(long leaveCategory) {
		_leave.setLeaveCategory(leaveCategory);
	}

	/**
	* Returns the responsible employee status of this leave.
	*
	* @return the responsible employee status of this leave
	*/
	public int getResponsibleEmployeeStatus() {
		return _leave.getResponsibleEmployeeStatus();
	}

	/**
	* Sets the responsible employee status of this leave.
	*
	* @param responsibleEmployeeStatus the responsible employee status of this leave
	*/
	public void setResponsibleEmployeeStatus(int responsibleEmployeeStatus) {
		_leave.setResponsibleEmployeeStatus(responsibleEmployeeStatus);
	}

	/**
	* Returns the application status of this leave.
	*
	* @return the application status of this leave
	*/
	public int getApplicationStatus() {
		return _leave.getApplicationStatus();
	}

	/**
	* Sets the application status of this leave.
	*
	* @param applicationStatus the application status of this leave
	*/
	public void setApplicationStatus(int applicationStatus) {
		_leave.setApplicationStatus(applicationStatus);
	}

	/**
	* Returns the comments of this leave.
	*
	* @return the comments of this leave
	*/
	public java.lang.String getComments() {
		return _leave.getComments();
	}

	/**
	* Sets the comments of this leave.
	*
	* @param comments the comments of this leave
	*/
	public void setComments(java.lang.String comments) {
		_leave.setComments(comments);
	}

	/**
	* Returns the complemented by of this leave.
	*
	* @return the complemented by of this leave
	*/
	public long getComplementedBy() {
		return _leave.getComplementedBy();
	}

	/**
	* Sets the complemented by of this leave.
	*
	* @param complementedBy the complemented by of this leave
	*/
	public void setComplementedBy(long complementedBy) {
		_leave.setComplementedBy(complementedBy);
	}

	public boolean isNew() {
		return _leave.isNew();
	}

	public void setNew(boolean n) {
		_leave.setNew(n);
	}

	public boolean isCachedModel() {
		return _leave.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_leave.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _leave.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _leave.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_leave.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _leave.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_leave.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LeaveWrapper((Leave)_leave.clone());
	}

	public int compareTo(info.diit.portal.model.Leave leave) {
		return _leave.compareTo(leave);
	}

	@Override
	public int hashCode() {
		return _leave.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.Leave> toCacheModel() {
		return _leave.toCacheModel();
	}

	public info.diit.portal.model.Leave toEscapedModel() {
		return new LeaveWrapper(_leave.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _leave.toString();
	}

	public java.lang.String toXmlString() {
		return _leave.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_leave.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Leave getWrappedLeave() {
		return _leave;
	}

	public Leave getWrappedModel() {
		return _leave;
	}

	public void resetOriginalValues() {
		_leave.resetOriginalValues();
	}

	private Leave _leave;
}
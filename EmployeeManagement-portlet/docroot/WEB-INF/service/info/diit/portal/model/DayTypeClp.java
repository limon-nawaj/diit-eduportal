/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import info.diit.portal.service.DayTypeLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author limon
 */
public class DayTypeClp extends BaseModelImpl<DayType> implements DayType {
	public DayTypeClp() {
	}

	public Class<?> getModelClass() {
		return DayType.class;
	}

	public String getModelClassName() {
		return DayType.class.getName();
	}

	public long getPrimaryKey() {
		return _dayTypeId;
	}

	public void setPrimaryKey(long primaryKey) {
		setDayTypeId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_dayTypeId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("dayTypeId", getDayTypeId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("type", getType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long dayTypeId = (Long)attributes.get("dayTypeId");

		if (dayTypeId != null) {
			setDayTypeId(dayTypeId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}
	}

	public long getDayTypeId() {
		return _dayTypeId;
	}

	public void setDayTypeId(long dayTypeId) {
		_dayTypeId = dayTypeId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public String getType() {
		return _type;
	}

	public void setType(String type) {
		_type = type;
	}

	public BaseModel<?> getDayTypeRemoteModel() {
		return _dayTypeRemoteModel;
	}

	public void setDayTypeRemoteModel(BaseModel<?> dayTypeRemoteModel) {
		_dayTypeRemoteModel = dayTypeRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			DayTypeLocalServiceUtil.addDayType(this);
		}
		else {
			DayTypeLocalServiceUtil.updateDayType(this);
		}
	}

	@Override
	public DayType toEscapedModel() {
		return (DayType)Proxy.newProxyInstance(DayType.class.getClassLoader(),
			new Class[] { DayType.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		DayTypeClp clone = new DayTypeClp();

		clone.setDayTypeId(getDayTypeId());
		clone.setOrganizationId(getOrganizationId());
		clone.setType(getType());

		return clone;
	}

	public int compareTo(DayType dayType) {
		long primaryKey = dayType.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		DayTypeClp dayType = null;

		try {
			dayType = (DayTypeClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = dayType.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{dayTypeId=");
		sb.append(getDayTypeId());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", type=");
		sb.append(getType());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.DayType");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>dayTypeId</column-name><column-value><![CDATA[");
		sb.append(getDayTypeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>type</column-name><column-value><![CDATA[");
		sb.append(getType());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _dayTypeId;
	private long _organizationId;
	private String _type;
	private BaseModel<?> _dayTypeRemoteModel;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AttendanceCorrection}.
 * </p>
 *
 * @author    limon
 * @see       AttendanceCorrection
 * @generated
 */
public class AttendanceCorrectionWrapper implements AttendanceCorrection,
	ModelWrapper<AttendanceCorrection> {
	public AttendanceCorrectionWrapper(
		AttendanceCorrection attendanceCorrection) {
		_attendanceCorrection = attendanceCorrection;
	}

	public Class<?> getModelClass() {
		return AttendanceCorrection.class;
	}

	public String getModelClassName() {
		return AttendanceCorrection.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("attendanceCorrectionId", getAttendanceCorrectionId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("companyId", getCompanyId());
		attributes.put("employeeId", getEmployeeId());
		attributes.put("correctionDate", getCorrectionDate());
		attributes.put("realIn", getRealIn());
		attributes.put("realOut", getRealOut());
		attributes.put("expectedIn", getExpectedIn());
		attributes.put("expectedOut", getExpectedOut());
		attributes.put("status", getStatus());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long attendanceCorrectionId = (Long)attributes.get(
				"attendanceCorrectionId");

		if (attendanceCorrectionId != null) {
			setAttendanceCorrectionId(attendanceCorrectionId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long employeeId = (Long)attributes.get("employeeId");

		if (employeeId != null) {
			setEmployeeId(employeeId);
		}

		Date correctionDate = (Date)attributes.get("correctionDate");

		if (correctionDate != null) {
			setCorrectionDate(correctionDate);
		}

		Date realIn = (Date)attributes.get("realIn");

		if (realIn != null) {
			setRealIn(realIn);
		}

		Date realOut = (Date)attributes.get("realOut");

		if (realOut != null) {
			setRealOut(realOut);
		}

		Date expectedIn = (Date)attributes.get("expectedIn");

		if (expectedIn != null) {
			setExpectedIn(expectedIn);
		}

		Date expectedOut = (Date)attributes.get("expectedOut");

		if (expectedOut != null) {
			setExpectedOut(expectedOut);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	/**
	* Returns the primary key of this attendance correction.
	*
	* @return the primary key of this attendance correction
	*/
	public long getPrimaryKey() {
		return _attendanceCorrection.getPrimaryKey();
	}

	/**
	* Sets the primary key of this attendance correction.
	*
	* @param primaryKey the primary key of this attendance correction
	*/
	public void setPrimaryKey(long primaryKey) {
		_attendanceCorrection.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the attendance correction ID of this attendance correction.
	*
	* @return the attendance correction ID of this attendance correction
	*/
	public long getAttendanceCorrectionId() {
		return _attendanceCorrection.getAttendanceCorrectionId();
	}

	/**
	* Sets the attendance correction ID of this attendance correction.
	*
	* @param attendanceCorrectionId the attendance correction ID of this attendance correction
	*/
	public void setAttendanceCorrectionId(long attendanceCorrectionId) {
		_attendanceCorrection.setAttendanceCorrectionId(attendanceCorrectionId);
	}

	/**
	* Returns the organization ID of this attendance correction.
	*
	* @return the organization ID of this attendance correction
	*/
	public long getOrganizationId() {
		return _attendanceCorrection.getOrganizationId();
	}

	/**
	* Sets the organization ID of this attendance correction.
	*
	* @param organizationId the organization ID of this attendance correction
	*/
	public void setOrganizationId(long organizationId) {
		_attendanceCorrection.setOrganizationId(organizationId);
	}

	/**
	* Returns the company ID of this attendance correction.
	*
	* @return the company ID of this attendance correction
	*/
	public long getCompanyId() {
		return _attendanceCorrection.getCompanyId();
	}

	/**
	* Sets the company ID of this attendance correction.
	*
	* @param companyId the company ID of this attendance correction
	*/
	public void setCompanyId(long companyId) {
		_attendanceCorrection.setCompanyId(companyId);
	}

	/**
	* Returns the employee ID of this attendance correction.
	*
	* @return the employee ID of this attendance correction
	*/
	public long getEmployeeId() {
		return _attendanceCorrection.getEmployeeId();
	}

	/**
	* Sets the employee ID of this attendance correction.
	*
	* @param employeeId the employee ID of this attendance correction
	*/
	public void setEmployeeId(long employeeId) {
		_attendanceCorrection.setEmployeeId(employeeId);
	}

	/**
	* Returns the correction date of this attendance correction.
	*
	* @return the correction date of this attendance correction
	*/
	public java.util.Date getCorrectionDate() {
		return _attendanceCorrection.getCorrectionDate();
	}

	/**
	* Sets the correction date of this attendance correction.
	*
	* @param correctionDate the correction date of this attendance correction
	*/
	public void setCorrectionDate(java.util.Date correctionDate) {
		_attendanceCorrection.setCorrectionDate(correctionDate);
	}

	/**
	* Returns the real in of this attendance correction.
	*
	* @return the real in of this attendance correction
	*/
	public java.util.Date getRealIn() {
		return _attendanceCorrection.getRealIn();
	}

	/**
	* Sets the real in of this attendance correction.
	*
	* @param realIn the real in of this attendance correction
	*/
	public void setRealIn(java.util.Date realIn) {
		_attendanceCorrection.setRealIn(realIn);
	}

	/**
	* Returns the real out of this attendance correction.
	*
	* @return the real out of this attendance correction
	*/
	public java.util.Date getRealOut() {
		return _attendanceCorrection.getRealOut();
	}

	/**
	* Sets the real out of this attendance correction.
	*
	* @param realOut the real out of this attendance correction
	*/
	public void setRealOut(java.util.Date realOut) {
		_attendanceCorrection.setRealOut(realOut);
	}

	/**
	* Returns the expected in of this attendance correction.
	*
	* @return the expected in of this attendance correction
	*/
	public java.util.Date getExpectedIn() {
		return _attendanceCorrection.getExpectedIn();
	}

	/**
	* Sets the expected in of this attendance correction.
	*
	* @param expectedIn the expected in of this attendance correction
	*/
	public void setExpectedIn(java.util.Date expectedIn) {
		_attendanceCorrection.setExpectedIn(expectedIn);
	}

	/**
	* Returns the expected out of this attendance correction.
	*
	* @return the expected out of this attendance correction
	*/
	public java.util.Date getExpectedOut() {
		return _attendanceCorrection.getExpectedOut();
	}

	/**
	* Sets the expected out of this attendance correction.
	*
	* @param expectedOut the expected out of this attendance correction
	*/
	public void setExpectedOut(java.util.Date expectedOut) {
		_attendanceCorrection.setExpectedOut(expectedOut);
	}

	/**
	* Returns the status of this attendance correction.
	*
	* @return the status of this attendance correction
	*/
	public int getStatus() {
		return _attendanceCorrection.getStatus();
	}

	/**
	* Sets the status of this attendance correction.
	*
	* @param status the status of this attendance correction
	*/
	public void setStatus(int status) {
		_attendanceCorrection.setStatus(status);
	}

	public boolean isNew() {
		return _attendanceCorrection.isNew();
	}

	public void setNew(boolean n) {
		_attendanceCorrection.setNew(n);
	}

	public boolean isCachedModel() {
		return _attendanceCorrection.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_attendanceCorrection.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _attendanceCorrection.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _attendanceCorrection.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_attendanceCorrection.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _attendanceCorrection.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_attendanceCorrection.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AttendanceCorrectionWrapper((AttendanceCorrection)_attendanceCorrection.clone());
	}

	public int compareTo(
		info.diit.portal.model.AttendanceCorrection attendanceCorrection) {
		return _attendanceCorrection.compareTo(attendanceCorrection);
	}

	@Override
	public int hashCode() {
		return _attendanceCorrection.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.AttendanceCorrection> toCacheModel() {
		return _attendanceCorrection.toCacheModel();
	}

	public info.diit.portal.model.AttendanceCorrection toEscapedModel() {
		return new AttendanceCorrectionWrapper(_attendanceCorrection.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _attendanceCorrection.toString();
	}

	public java.lang.String toXmlString() {
		return _attendanceCorrection.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_attendanceCorrection.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public AttendanceCorrection getWrappedAttendanceCorrection() {
		return _attendanceCorrection;
	}

	public AttendanceCorrection getWrappedModel() {
		return _attendanceCorrection;
	}

	public void resetOriginalValues() {
		_attendanceCorrection.resetOriginalValues();
	}

	private AttendanceCorrection _attendanceCorrection;
}
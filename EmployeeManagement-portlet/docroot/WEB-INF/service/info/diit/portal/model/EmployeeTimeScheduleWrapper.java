/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link EmployeeTimeSchedule}.
 * </p>
 *
 * @author    limon
 * @see       EmployeeTimeSchedule
 * @generated
 */
public class EmployeeTimeScheduleWrapper implements EmployeeTimeSchedule,
	ModelWrapper<EmployeeTimeSchedule> {
	public EmployeeTimeScheduleWrapper(
		EmployeeTimeSchedule employeeTimeSchedule) {
		_employeeTimeSchedule = employeeTimeSchedule;
	}

	public Class<?> getModelClass() {
		return EmployeeTimeSchedule.class;
	}

	public String getModelClassName() {
		return EmployeeTimeSchedule.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("employeeTimeScheduleId", getEmployeeTimeScheduleId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("companyId", getCompanyId());
		attributes.put("employeeId", getEmployeeId());
		attributes.put("day", getDay());
		attributes.put("startTime", getStartTime());
		attributes.put("endTime", getEndTime());
		attributes.put("duration", getDuration());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long employeeTimeScheduleId = (Long)attributes.get(
				"employeeTimeScheduleId");

		if (employeeTimeScheduleId != null) {
			setEmployeeTimeScheduleId(employeeTimeScheduleId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long employeeId = (Long)attributes.get("employeeId");

		if (employeeId != null) {
			setEmployeeId(employeeId);
		}

		Integer day = (Integer)attributes.get("day");

		if (day != null) {
			setDay(day);
		}

		Date startTime = (Date)attributes.get("startTime");

		if (startTime != null) {
			setStartTime(startTime);
		}

		Date endTime = (Date)attributes.get("endTime");

		if (endTime != null) {
			setEndTime(endTime);
		}

		Double duration = (Double)attributes.get("duration");

		if (duration != null) {
			setDuration(duration);
		}
	}

	/**
	* Returns the primary key of this employee time schedule.
	*
	* @return the primary key of this employee time schedule
	*/
	public long getPrimaryKey() {
		return _employeeTimeSchedule.getPrimaryKey();
	}

	/**
	* Sets the primary key of this employee time schedule.
	*
	* @param primaryKey the primary key of this employee time schedule
	*/
	public void setPrimaryKey(long primaryKey) {
		_employeeTimeSchedule.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the employee time schedule ID of this employee time schedule.
	*
	* @return the employee time schedule ID of this employee time schedule
	*/
	public long getEmployeeTimeScheduleId() {
		return _employeeTimeSchedule.getEmployeeTimeScheduleId();
	}

	/**
	* Sets the employee time schedule ID of this employee time schedule.
	*
	* @param employeeTimeScheduleId the employee time schedule ID of this employee time schedule
	*/
	public void setEmployeeTimeScheduleId(long employeeTimeScheduleId) {
		_employeeTimeSchedule.setEmployeeTimeScheduleId(employeeTimeScheduleId);
	}

	/**
	* Returns the organization ID of this employee time schedule.
	*
	* @return the organization ID of this employee time schedule
	*/
	public long getOrganizationId() {
		return _employeeTimeSchedule.getOrganizationId();
	}

	/**
	* Sets the organization ID of this employee time schedule.
	*
	* @param organizationId the organization ID of this employee time schedule
	*/
	public void setOrganizationId(long organizationId) {
		_employeeTimeSchedule.setOrganizationId(organizationId);
	}

	/**
	* Returns the company ID of this employee time schedule.
	*
	* @return the company ID of this employee time schedule
	*/
	public long getCompanyId() {
		return _employeeTimeSchedule.getCompanyId();
	}

	/**
	* Sets the company ID of this employee time schedule.
	*
	* @param companyId the company ID of this employee time schedule
	*/
	public void setCompanyId(long companyId) {
		_employeeTimeSchedule.setCompanyId(companyId);
	}

	/**
	* Returns the employee ID of this employee time schedule.
	*
	* @return the employee ID of this employee time schedule
	*/
	public long getEmployeeId() {
		return _employeeTimeSchedule.getEmployeeId();
	}

	/**
	* Sets the employee ID of this employee time schedule.
	*
	* @param employeeId the employee ID of this employee time schedule
	*/
	public void setEmployeeId(long employeeId) {
		_employeeTimeSchedule.setEmployeeId(employeeId);
	}

	/**
	* Returns the day of this employee time schedule.
	*
	* @return the day of this employee time schedule
	*/
	public int getDay() {
		return _employeeTimeSchedule.getDay();
	}

	/**
	* Sets the day of this employee time schedule.
	*
	* @param day the day of this employee time schedule
	*/
	public void setDay(int day) {
		_employeeTimeSchedule.setDay(day);
	}

	/**
	* Returns the start time of this employee time schedule.
	*
	* @return the start time of this employee time schedule
	*/
	public java.util.Date getStartTime() {
		return _employeeTimeSchedule.getStartTime();
	}

	/**
	* Sets the start time of this employee time schedule.
	*
	* @param startTime the start time of this employee time schedule
	*/
	public void setStartTime(java.util.Date startTime) {
		_employeeTimeSchedule.setStartTime(startTime);
	}

	/**
	* Returns the end time of this employee time schedule.
	*
	* @return the end time of this employee time schedule
	*/
	public java.util.Date getEndTime() {
		return _employeeTimeSchedule.getEndTime();
	}

	/**
	* Sets the end time of this employee time schedule.
	*
	* @param endTime the end time of this employee time schedule
	*/
	public void setEndTime(java.util.Date endTime) {
		_employeeTimeSchedule.setEndTime(endTime);
	}

	/**
	* Returns the duration of this employee time schedule.
	*
	* @return the duration of this employee time schedule
	*/
	public double getDuration() {
		return _employeeTimeSchedule.getDuration();
	}

	/**
	* Sets the duration of this employee time schedule.
	*
	* @param duration the duration of this employee time schedule
	*/
	public void setDuration(double duration) {
		_employeeTimeSchedule.setDuration(duration);
	}

	public boolean isNew() {
		return _employeeTimeSchedule.isNew();
	}

	public void setNew(boolean n) {
		_employeeTimeSchedule.setNew(n);
	}

	public boolean isCachedModel() {
		return _employeeTimeSchedule.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_employeeTimeSchedule.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _employeeTimeSchedule.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _employeeTimeSchedule.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_employeeTimeSchedule.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _employeeTimeSchedule.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_employeeTimeSchedule.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new EmployeeTimeScheduleWrapper((EmployeeTimeSchedule)_employeeTimeSchedule.clone());
	}

	public int compareTo(
		info.diit.portal.model.EmployeeTimeSchedule employeeTimeSchedule) {
		return _employeeTimeSchedule.compareTo(employeeTimeSchedule);
	}

	@Override
	public int hashCode() {
		return _employeeTimeSchedule.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.EmployeeTimeSchedule> toCacheModel() {
		return _employeeTimeSchedule.toCacheModel();
	}

	public info.diit.portal.model.EmployeeTimeSchedule toEscapedModel() {
		return new EmployeeTimeScheduleWrapper(_employeeTimeSchedule.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _employeeTimeSchedule.toString();
	}

	public java.lang.String toXmlString() {
		return _employeeTimeSchedule.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_employeeTimeSchedule.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public EmployeeTimeSchedule getWrappedEmployeeTimeSchedule() {
		return _employeeTimeSchedule;
	}

	public EmployeeTimeSchedule getWrappedModel() {
		return _employeeTimeSchedule;
	}

	public void resetOriginalValues() {
		_employeeTimeSchedule.resetOriginalValues();
	}

	private EmployeeTimeSchedule _employeeTimeSchedule;
}
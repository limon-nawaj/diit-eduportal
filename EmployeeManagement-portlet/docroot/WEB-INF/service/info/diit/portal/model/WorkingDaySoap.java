/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    limon
 * @generated
 */
public class WorkingDaySoap implements Serializable {
	public static WorkingDaySoap toSoapModel(WorkingDay model) {
		WorkingDaySoap soapModel = new WorkingDaySoap();

		soapModel.setWorkingDayId(model.getWorkingDayId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setDate(model.getDate());
		soapModel.setDayOfWeek(model.getDayOfWeek());
		soapModel.setWeek(model.getWeek());
		soapModel.setStatus(model.getStatus());

		return soapModel;
	}

	public static WorkingDaySoap[] toSoapModels(WorkingDay[] models) {
		WorkingDaySoap[] soapModels = new WorkingDaySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static WorkingDaySoap[][] toSoapModels(WorkingDay[][] models) {
		WorkingDaySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new WorkingDaySoap[models.length][models[0].length];
		}
		else {
			soapModels = new WorkingDaySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static WorkingDaySoap[] toSoapModels(List<WorkingDay> models) {
		List<WorkingDaySoap> soapModels = new ArrayList<WorkingDaySoap>(models.size());

		for (WorkingDay model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new WorkingDaySoap[soapModels.size()]);
	}

	public WorkingDaySoap() {
	}

	public long getPrimaryKey() {
		return _workingDayId;
	}

	public void setPrimaryKey(long pk) {
		setWorkingDayId(pk);
	}

	public long getWorkingDayId() {
		return _workingDayId;
	}

	public void setWorkingDayId(long workingDayId) {
		_workingDayId = workingDayId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public Date getDate() {
		return _date;
	}

	public void setDate(Date date) {
		_date = date;
	}

	public int getDayOfWeek() {
		return _dayOfWeek;
	}

	public void setDayOfWeek(int dayOfWeek) {
		_dayOfWeek = dayOfWeek;
	}

	public int getWeek() {
		return _week;
	}

	public void setWeek(int week) {
		_week = week;
	}

	public long getStatus() {
		return _status;
	}

	public void setStatus(long status) {
		_status = status;
	}

	private long _workingDayId;
	private long _organizationId;
	private Date _date;
	private int _dayOfWeek;
	private int _week;
	private long _status;
}
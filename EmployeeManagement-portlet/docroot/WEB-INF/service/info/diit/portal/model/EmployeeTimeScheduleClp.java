/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import info.diit.portal.service.EmployeeTimeScheduleLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author limon
 */
public class EmployeeTimeScheduleClp extends BaseModelImpl<EmployeeTimeSchedule>
	implements EmployeeTimeSchedule {
	public EmployeeTimeScheduleClp() {
	}

	public Class<?> getModelClass() {
		return EmployeeTimeSchedule.class;
	}

	public String getModelClassName() {
		return EmployeeTimeSchedule.class.getName();
	}

	public long getPrimaryKey() {
		return _employeeTimeScheduleId;
	}

	public void setPrimaryKey(long primaryKey) {
		setEmployeeTimeScheduleId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_employeeTimeScheduleId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("employeeTimeScheduleId", getEmployeeTimeScheduleId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("companyId", getCompanyId());
		attributes.put("employeeId", getEmployeeId());
		attributes.put("day", getDay());
		attributes.put("startTime", getStartTime());
		attributes.put("endTime", getEndTime());
		attributes.put("duration", getDuration());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long employeeTimeScheduleId = (Long)attributes.get(
				"employeeTimeScheduleId");

		if (employeeTimeScheduleId != null) {
			setEmployeeTimeScheduleId(employeeTimeScheduleId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long employeeId = (Long)attributes.get("employeeId");

		if (employeeId != null) {
			setEmployeeId(employeeId);
		}

		Integer day = (Integer)attributes.get("day");

		if (day != null) {
			setDay(day);
		}

		Date startTime = (Date)attributes.get("startTime");

		if (startTime != null) {
			setStartTime(startTime);
		}

		Date endTime = (Date)attributes.get("endTime");

		if (endTime != null) {
			setEndTime(endTime);
		}

		Double duration = (Double)attributes.get("duration");

		if (duration != null) {
			setDuration(duration);
		}
	}

	public long getEmployeeTimeScheduleId() {
		return _employeeTimeScheduleId;
	}

	public void setEmployeeTimeScheduleId(long employeeTimeScheduleId) {
		_employeeTimeScheduleId = employeeTimeScheduleId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getEmployeeId() {
		return _employeeId;
	}

	public void setEmployeeId(long employeeId) {
		_employeeId = employeeId;
	}

	public int getDay() {
		return _day;
	}

	public void setDay(int day) {
		_day = day;
	}

	public Date getStartTime() {
		return _startTime;
	}

	public void setStartTime(Date startTime) {
		_startTime = startTime;
	}

	public Date getEndTime() {
		return _endTime;
	}

	public void setEndTime(Date endTime) {
		_endTime = endTime;
	}

	public double getDuration() {
		return _duration;
	}

	public void setDuration(double duration) {
		_duration = duration;
	}

	public BaseModel<?> getEmployeeTimeScheduleRemoteModel() {
		return _employeeTimeScheduleRemoteModel;
	}

	public void setEmployeeTimeScheduleRemoteModel(
		BaseModel<?> employeeTimeScheduleRemoteModel) {
		_employeeTimeScheduleRemoteModel = employeeTimeScheduleRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			EmployeeTimeScheduleLocalServiceUtil.addEmployeeTimeSchedule(this);
		}
		else {
			EmployeeTimeScheduleLocalServiceUtil.updateEmployeeTimeSchedule(this);
		}
	}

	@Override
	public EmployeeTimeSchedule toEscapedModel() {
		return (EmployeeTimeSchedule)Proxy.newProxyInstance(EmployeeTimeSchedule.class.getClassLoader(),
			new Class[] { EmployeeTimeSchedule.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		EmployeeTimeScheduleClp clone = new EmployeeTimeScheduleClp();

		clone.setEmployeeTimeScheduleId(getEmployeeTimeScheduleId());
		clone.setOrganizationId(getOrganizationId());
		clone.setCompanyId(getCompanyId());
		clone.setEmployeeId(getEmployeeId());
		clone.setDay(getDay());
		clone.setStartTime(getStartTime());
		clone.setEndTime(getEndTime());
		clone.setDuration(getDuration());

		return clone;
	}

	public int compareTo(EmployeeTimeSchedule employeeTimeSchedule) {
		long primaryKey = employeeTimeSchedule.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		EmployeeTimeScheduleClp employeeTimeSchedule = null;

		try {
			employeeTimeSchedule = (EmployeeTimeScheduleClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = employeeTimeSchedule.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{employeeTimeScheduleId=");
		sb.append(getEmployeeTimeScheduleId());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", employeeId=");
		sb.append(getEmployeeId());
		sb.append(", day=");
		sb.append(getDay());
		sb.append(", startTime=");
		sb.append(getStartTime());
		sb.append(", endTime=");
		sb.append(getEndTime());
		sb.append(", duration=");
		sb.append(getDuration());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(28);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.EmployeeTimeSchedule");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>employeeTimeScheduleId</column-name><column-value><![CDATA[");
		sb.append(getEmployeeTimeScheduleId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>employeeId</column-name><column-value><![CDATA[");
		sb.append(getEmployeeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>day</column-name><column-value><![CDATA[");
		sb.append(getDay());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>startTime</column-name><column-value><![CDATA[");
		sb.append(getStartTime());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>endTime</column-name><column-value><![CDATA[");
		sb.append(getEndTime());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>duration</column-name><column-value><![CDATA[");
		sb.append(getDuration());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _employeeTimeScheduleId;
	private long _organizationId;
	private long _companyId;
	private long _employeeId;
	private int _day;
	private Date _startTime;
	private Date _endTime;
	private double _duration;
	private BaseModel<?> _employeeTimeScheduleRemoteModel;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link WorkingDay}.
 * </p>
 *
 * @author    limon
 * @see       WorkingDay
 * @generated
 */
public class WorkingDayWrapper implements WorkingDay, ModelWrapper<WorkingDay> {
	public WorkingDayWrapper(WorkingDay workingDay) {
		_workingDay = workingDay;
	}

	public Class<?> getModelClass() {
		return WorkingDay.class;
	}

	public String getModelClassName() {
		return WorkingDay.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("workingDayId", getWorkingDayId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("date", getDate());
		attributes.put("dayOfWeek", getDayOfWeek());
		attributes.put("week", getWeek());
		attributes.put("status", getStatus());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long workingDayId = (Long)attributes.get("workingDayId");

		if (workingDayId != null) {
			setWorkingDayId(workingDayId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		Integer dayOfWeek = (Integer)attributes.get("dayOfWeek");

		if (dayOfWeek != null) {
			setDayOfWeek(dayOfWeek);
		}

		Integer week = (Integer)attributes.get("week");

		if (week != null) {
			setWeek(week);
		}

		Long status = (Long)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	/**
	* Returns the primary key of this working day.
	*
	* @return the primary key of this working day
	*/
	public long getPrimaryKey() {
		return _workingDay.getPrimaryKey();
	}

	/**
	* Sets the primary key of this working day.
	*
	* @param primaryKey the primary key of this working day
	*/
	public void setPrimaryKey(long primaryKey) {
		_workingDay.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the working day ID of this working day.
	*
	* @return the working day ID of this working day
	*/
	public long getWorkingDayId() {
		return _workingDay.getWorkingDayId();
	}

	/**
	* Sets the working day ID of this working day.
	*
	* @param workingDayId the working day ID of this working day
	*/
	public void setWorkingDayId(long workingDayId) {
		_workingDay.setWorkingDayId(workingDayId);
	}

	/**
	* Returns the organization ID of this working day.
	*
	* @return the organization ID of this working day
	*/
	public long getOrganizationId() {
		return _workingDay.getOrganizationId();
	}

	/**
	* Sets the organization ID of this working day.
	*
	* @param organizationId the organization ID of this working day
	*/
	public void setOrganizationId(long organizationId) {
		_workingDay.setOrganizationId(organizationId);
	}

	/**
	* Returns the date of this working day.
	*
	* @return the date of this working day
	*/
	public java.util.Date getDate() {
		return _workingDay.getDate();
	}

	/**
	* Sets the date of this working day.
	*
	* @param date the date of this working day
	*/
	public void setDate(java.util.Date date) {
		_workingDay.setDate(date);
	}

	/**
	* Returns the day of week of this working day.
	*
	* @return the day of week of this working day
	*/
	public int getDayOfWeek() {
		return _workingDay.getDayOfWeek();
	}

	/**
	* Sets the day of week of this working day.
	*
	* @param dayOfWeek the day of week of this working day
	*/
	public void setDayOfWeek(int dayOfWeek) {
		_workingDay.setDayOfWeek(dayOfWeek);
	}

	/**
	* Returns the week of this working day.
	*
	* @return the week of this working day
	*/
	public int getWeek() {
		return _workingDay.getWeek();
	}

	/**
	* Sets the week of this working day.
	*
	* @param week the week of this working day
	*/
	public void setWeek(int week) {
		_workingDay.setWeek(week);
	}

	/**
	* Returns the status of this working day.
	*
	* @return the status of this working day
	*/
	public long getStatus() {
		return _workingDay.getStatus();
	}

	/**
	* Sets the status of this working day.
	*
	* @param status the status of this working day
	*/
	public void setStatus(long status) {
		_workingDay.setStatus(status);
	}

	public boolean isNew() {
		return _workingDay.isNew();
	}

	public void setNew(boolean n) {
		_workingDay.setNew(n);
	}

	public boolean isCachedModel() {
		return _workingDay.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_workingDay.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _workingDay.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _workingDay.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_workingDay.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _workingDay.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_workingDay.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new WorkingDayWrapper((WorkingDay)_workingDay.clone());
	}

	public int compareTo(info.diit.portal.model.WorkingDay workingDay) {
		return _workingDay.compareTo(workingDay);
	}

	@Override
	public int hashCode() {
		return _workingDay.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.WorkingDay> toCacheModel() {
		return _workingDay.toCacheModel();
	}

	public info.diit.portal.model.WorkingDay toEscapedModel() {
		return new WorkingDayWrapper(_workingDay.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _workingDay.toString();
	}

	public java.lang.String toXmlString() {
		return _workingDay.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_workingDay.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public WorkingDay getWrappedWorkingDay() {
		return _workingDay;
	}

	public WorkingDay getWrappedModel() {
		return _workingDay;
	}

	public void resetOriginalValues() {
		_workingDay.resetOriginalValues();
	}

	private WorkingDay _workingDay;
}
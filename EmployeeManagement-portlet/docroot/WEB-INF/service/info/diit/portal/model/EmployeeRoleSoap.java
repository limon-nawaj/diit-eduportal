/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    limon
 * @generated
 */
public class EmployeeRoleSoap implements Serializable {
	public static EmployeeRoleSoap toSoapModel(EmployeeRole model) {
		EmployeeRoleSoap soapModel = new EmployeeRoleSoap();

		soapModel.setEmployeeRoleId(model.getEmployeeRoleId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setRole(model.getRole());
		soapModel.setStartTime(model.getStartTime());
		soapModel.setEndTime(model.getEndTime());

		return soapModel;
	}

	public static EmployeeRoleSoap[] toSoapModels(EmployeeRole[] models) {
		EmployeeRoleSoap[] soapModels = new EmployeeRoleSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EmployeeRoleSoap[][] toSoapModels(EmployeeRole[][] models) {
		EmployeeRoleSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EmployeeRoleSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EmployeeRoleSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EmployeeRoleSoap[] toSoapModels(List<EmployeeRole> models) {
		List<EmployeeRoleSoap> soapModels = new ArrayList<EmployeeRoleSoap>(models.size());

		for (EmployeeRole model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EmployeeRoleSoap[soapModels.size()]);
	}

	public EmployeeRoleSoap() {
	}

	public long getPrimaryKey() {
		return _employeeRoleId;
	}

	public void setPrimaryKey(long pk) {
		setEmployeeRoleId(pk);
	}

	public long getEmployeeRoleId() {
		return _employeeRoleId;
	}

	public void setEmployeeRoleId(long employeeRoleId) {
		_employeeRoleId = employeeRoleId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public String getRole() {
		return _role;
	}

	public void setRole(String role) {
		_role = role;
	}

	public Date getStartTime() {
		return _startTime;
	}

	public void setStartTime(Date startTime) {
		_startTime = startTime;
	}

	public Date getEndTime() {
		return _endTime;
	}

	public void setEndTime(Date endTime) {
		_endTime = endTime;
	}

	private long _employeeRoleId;
	private long _organizationId;
	private long _companyId;
	private String _role;
	private Date _startTime;
	private Date _endTime;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    limon
 * @generated
 */
public class DayTypeSoap implements Serializable {
	public static DayTypeSoap toSoapModel(DayType model) {
		DayTypeSoap soapModel = new DayTypeSoap();

		soapModel.setDayTypeId(model.getDayTypeId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setType(model.getType());

		return soapModel;
	}

	public static DayTypeSoap[] toSoapModels(DayType[] models) {
		DayTypeSoap[] soapModels = new DayTypeSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DayTypeSoap[][] toSoapModels(DayType[][] models) {
		DayTypeSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DayTypeSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DayTypeSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DayTypeSoap[] toSoapModels(List<DayType> models) {
		List<DayTypeSoap> soapModels = new ArrayList<DayTypeSoap>(models.size());

		for (DayType model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DayTypeSoap[soapModels.size()]);
	}

	public DayTypeSoap() {
	}

	public long getPrimaryKey() {
		return _dayTypeId;
	}

	public void setPrimaryKey(long pk) {
		setDayTypeId(pk);
	}

	public long getDayTypeId() {
		return _dayTypeId;
	}

	public void setDayTypeId(long dayTypeId) {
		_dayTypeId = dayTypeId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public String getType() {
		return _type;
	}

	public void setType(String type) {
		_type = type;
	}

	private long _dayTypeId;
	private long _organizationId;
	private String _type;
}
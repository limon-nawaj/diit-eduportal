/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link DayType}.
 * </p>
 *
 * @author    limon
 * @see       DayType
 * @generated
 */
public class DayTypeWrapper implements DayType, ModelWrapper<DayType> {
	public DayTypeWrapper(DayType dayType) {
		_dayType = dayType;
	}

	public Class<?> getModelClass() {
		return DayType.class;
	}

	public String getModelClassName() {
		return DayType.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("dayTypeId", getDayTypeId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("type", getType());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long dayTypeId = (Long)attributes.get("dayTypeId");

		if (dayTypeId != null) {
			setDayTypeId(dayTypeId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}
	}

	/**
	* Returns the primary key of this day type.
	*
	* @return the primary key of this day type
	*/
	public long getPrimaryKey() {
		return _dayType.getPrimaryKey();
	}

	/**
	* Sets the primary key of this day type.
	*
	* @param primaryKey the primary key of this day type
	*/
	public void setPrimaryKey(long primaryKey) {
		_dayType.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the day type ID of this day type.
	*
	* @return the day type ID of this day type
	*/
	public long getDayTypeId() {
		return _dayType.getDayTypeId();
	}

	/**
	* Sets the day type ID of this day type.
	*
	* @param dayTypeId the day type ID of this day type
	*/
	public void setDayTypeId(long dayTypeId) {
		_dayType.setDayTypeId(dayTypeId);
	}

	/**
	* Returns the organization ID of this day type.
	*
	* @return the organization ID of this day type
	*/
	public long getOrganizationId() {
		return _dayType.getOrganizationId();
	}

	/**
	* Sets the organization ID of this day type.
	*
	* @param organizationId the organization ID of this day type
	*/
	public void setOrganizationId(long organizationId) {
		_dayType.setOrganizationId(organizationId);
	}

	/**
	* Returns the type of this day type.
	*
	* @return the type of this day type
	*/
	public java.lang.String getType() {
		return _dayType.getType();
	}

	/**
	* Sets the type of this day type.
	*
	* @param type the type of this day type
	*/
	public void setType(java.lang.String type) {
		_dayType.setType(type);
	}

	public boolean isNew() {
		return _dayType.isNew();
	}

	public void setNew(boolean n) {
		_dayType.setNew(n);
	}

	public boolean isCachedModel() {
		return _dayType.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_dayType.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _dayType.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _dayType.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_dayType.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _dayType.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_dayType.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new DayTypeWrapper((DayType)_dayType.clone());
	}

	public int compareTo(info.diit.portal.model.DayType dayType) {
		return _dayType.compareTo(dayType);
	}

	@Override
	public int hashCode() {
		return _dayType.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.DayType> toCacheModel() {
		return _dayType.toCacheModel();
	}

	public info.diit.portal.model.DayType toEscapedModel() {
		return new DayTypeWrapper(_dayType.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _dayType.toString();
	}

	public java.lang.String toXmlString() {
		return _dayType.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_dayType.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public DayType getWrappedDayType() {
		return _dayType;
	}

	public DayType getWrappedModel() {
		return _dayType;
	}

	public void resetOriginalValues() {
		_dayType.resetOriginalValues();
	}

	private DayType _dayType;
}
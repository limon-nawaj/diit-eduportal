/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Employee}.
 * </p>
 *
 * @author    limon
 * @see       Employee
 * @generated
 */
public class EmployeeWrapper implements Employee, ModelWrapper<Employee> {
	public EmployeeWrapper(Employee employee) {
		_employee = employee;
	}

	public Class<?> getModelClass() {
		return Employee.class;
	}

	public String getModelClassName() {
		return Employee.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("employeeId", getEmployeeId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("employeeUserId", getEmployeeUserId());
		attributes.put("name", getName());
		attributes.put("gender", getGender());
		attributes.put("religion", getReligion());
		attributes.put("blood", getBlood());
		attributes.put("dob", getDob());
		attributes.put("designation", getDesignation());
		attributes.put("branch", getBranch());
		attributes.put("status", getStatus());
		attributes.put("presentAddress", getPresentAddress());
		attributes.put("permanentAddress", getPermanentAddress());
		attributes.put("photo", getPhoto());
		attributes.put("supervisor", getSupervisor());
		attributes.put("role", getRole());
		attributes.put("duration", getDuration());
		attributes.put("minimumDuration", getMinimumDuration());
		attributes.put("type", getType());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long employeeId = (Long)attributes.get("employeeId");

		if (employeeId != null) {
			setEmployeeId(employeeId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long employeeUserId = (Long)attributes.get("employeeUserId");

		if (employeeUserId != null) {
			setEmployeeUserId(employeeUserId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Long gender = (Long)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		Long religion = (Long)attributes.get("religion");

		if (religion != null) {
			setReligion(religion);
		}

		Long blood = (Long)attributes.get("blood");

		if (blood != null) {
			setBlood(blood);
		}

		Date dob = (Date)attributes.get("dob");

		if (dob != null) {
			setDob(dob);
		}

		Long designation = (Long)attributes.get("designation");

		if (designation != null) {
			setDesignation(designation);
		}

		Long branch = (Long)attributes.get("branch");

		if (branch != null) {
			setBranch(branch);
		}

		Long status = (Long)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		String presentAddress = (String)attributes.get("presentAddress");

		if (presentAddress != null) {
			setPresentAddress(presentAddress);
		}

		String permanentAddress = (String)attributes.get("permanentAddress");

		if (permanentAddress != null) {
			setPermanentAddress(permanentAddress);
		}

		Long photo = (Long)attributes.get("photo");

		if (photo != null) {
			setPhoto(photo);
		}

		Long supervisor = (Long)attributes.get("supervisor");

		if (supervisor != null) {
			setSupervisor(supervisor);
		}

		Long role = (Long)attributes.get("role");

		if (role != null) {
			setRole(role);
		}

		Double duration = (Double)attributes.get("duration");

		if (duration != null) {
			setDuration(duration);
		}

		Double minimumDuration = (Double)attributes.get("minimumDuration");

		if (minimumDuration != null) {
			setMinimumDuration(minimumDuration);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}
	}

	/**
	* Returns the primary key of this employee.
	*
	* @return the primary key of this employee
	*/
	public long getPrimaryKey() {
		return _employee.getPrimaryKey();
	}

	/**
	* Sets the primary key of this employee.
	*
	* @param primaryKey the primary key of this employee
	*/
	public void setPrimaryKey(long primaryKey) {
		_employee.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the employee ID of this employee.
	*
	* @return the employee ID of this employee
	*/
	public long getEmployeeId() {
		return _employee.getEmployeeId();
	}

	/**
	* Sets the employee ID of this employee.
	*
	* @param employeeId the employee ID of this employee
	*/
	public void setEmployeeId(long employeeId) {
		_employee.setEmployeeId(employeeId);
	}

	/**
	* Returns the company ID of this employee.
	*
	* @return the company ID of this employee
	*/
	public long getCompanyId() {
		return _employee.getCompanyId();
	}

	/**
	* Sets the company ID of this employee.
	*
	* @param companyId the company ID of this employee
	*/
	public void setCompanyId(long companyId) {
		_employee.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this employee.
	*
	* @return the organization ID of this employee
	*/
	public long getOrganizationId() {
		return _employee.getOrganizationId();
	}

	/**
	* Sets the organization ID of this employee.
	*
	* @param organizationId the organization ID of this employee
	*/
	public void setOrganizationId(long organizationId) {
		_employee.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this employee.
	*
	* @return the user ID of this employee
	*/
	public long getUserId() {
		return _employee.getUserId();
	}

	/**
	* Sets the user ID of this employee.
	*
	* @param userId the user ID of this employee
	*/
	public void setUserId(long userId) {
		_employee.setUserId(userId);
	}

	/**
	* Returns the user uuid of this employee.
	*
	* @return the user uuid of this employee
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employee.getUserUuid();
	}

	/**
	* Sets the user uuid of this employee.
	*
	* @param userUuid the user uuid of this employee
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_employee.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this employee.
	*
	* @return the user name of this employee
	*/
	public java.lang.String getUserName() {
		return _employee.getUserName();
	}

	/**
	* Sets the user name of this employee.
	*
	* @param userName the user name of this employee
	*/
	public void setUserName(java.lang.String userName) {
		_employee.setUserName(userName);
	}

	/**
	* Returns the create date of this employee.
	*
	* @return the create date of this employee
	*/
	public java.util.Date getCreateDate() {
		return _employee.getCreateDate();
	}

	/**
	* Sets the create date of this employee.
	*
	* @param createDate the create date of this employee
	*/
	public void setCreateDate(java.util.Date createDate) {
		_employee.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this employee.
	*
	* @return the modified date of this employee
	*/
	public java.util.Date getModifiedDate() {
		return _employee.getModifiedDate();
	}

	/**
	* Sets the modified date of this employee.
	*
	* @param modifiedDate the modified date of this employee
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_employee.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the employee user ID of this employee.
	*
	* @return the employee user ID of this employee
	*/
	public long getEmployeeUserId() {
		return _employee.getEmployeeUserId();
	}

	/**
	* Sets the employee user ID of this employee.
	*
	* @param employeeUserId the employee user ID of this employee
	*/
	public void setEmployeeUserId(long employeeUserId) {
		_employee.setEmployeeUserId(employeeUserId);
	}

	/**
	* Returns the employee user uuid of this employee.
	*
	* @return the employee user uuid of this employee
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getEmployeeUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employee.getEmployeeUserUuid();
	}

	/**
	* Sets the employee user uuid of this employee.
	*
	* @param employeeUserUuid the employee user uuid of this employee
	*/
	public void setEmployeeUserUuid(java.lang.String employeeUserUuid) {
		_employee.setEmployeeUserUuid(employeeUserUuid);
	}

	/**
	* Returns the name of this employee.
	*
	* @return the name of this employee
	*/
	public java.lang.String getName() {
		return _employee.getName();
	}

	/**
	* Sets the name of this employee.
	*
	* @param name the name of this employee
	*/
	public void setName(java.lang.String name) {
		_employee.setName(name);
	}

	/**
	* Returns the gender of this employee.
	*
	* @return the gender of this employee
	*/
	public long getGender() {
		return _employee.getGender();
	}

	/**
	* Sets the gender of this employee.
	*
	* @param gender the gender of this employee
	*/
	public void setGender(long gender) {
		_employee.setGender(gender);
	}

	/**
	* Returns the religion of this employee.
	*
	* @return the religion of this employee
	*/
	public long getReligion() {
		return _employee.getReligion();
	}

	/**
	* Sets the religion of this employee.
	*
	* @param religion the religion of this employee
	*/
	public void setReligion(long religion) {
		_employee.setReligion(religion);
	}

	/**
	* Returns the blood of this employee.
	*
	* @return the blood of this employee
	*/
	public long getBlood() {
		return _employee.getBlood();
	}

	/**
	* Sets the blood of this employee.
	*
	* @param blood the blood of this employee
	*/
	public void setBlood(long blood) {
		_employee.setBlood(blood);
	}

	/**
	* Returns the dob of this employee.
	*
	* @return the dob of this employee
	*/
	public java.util.Date getDob() {
		return _employee.getDob();
	}

	/**
	* Sets the dob of this employee.
	*
	* @param dob the dob of this employee
	*/
	public void setDob(java.util.Date dob) {
		_employee.setDob(dob);
	}

	/**
	* Returns the designation of this employee.
	*
	* @return the designation of this employee
	*/
	public long getDesignation() {
		return _employee.getDesignation();
	}

	/**
	* Sets the designation of this employee.
	*
	* @param designation the designation of this employee
	*/
	public void setDesignation(long designation) {
		_employee.setDesignation(designation);
	}

	/**
	* Returns the branch of this employee.
	*
	* @return the branch of this employee
	*/
	public long getBranch() {
		return _employee.getBranch();
	}

	/**
	* Sets the branch of this employee.
	*
	* @param branch the branch of this employee
	*/
	public void setBranch(long branch) {
		_employee.setBranch(branch);
	}

	/**
	* Returns the status of this employee.
	*
	* @return the status of this employee
	*/
	public long getStatus() {
		return _employee.getStatus();
	}

	/**
	* Sets the status of this employee.
	*
	* @param status the status of this employee
	*/
	public void setStatus(long status) {
		_employee.setStatus(status);
	}

	/**
	* Returns the present address of this employee.
	*
	* @return the present address of this employee
	*/
	public java.lang.String getPresentAddress() {
		return _employee.getPresentAddress();
	}

	/**
	* Sets the present address of this employee.
	*
	* @param presentAddress the present address of this employee
	*/
	public void setPresentAddress(java.lang.String presentAddress) {
		_employee.setPresentAddress(presentAddress);
	}

	/**
	* Returns the permanent address of this employee.
	*
	* @return the permanent address of this employee
	*/
	public java.lang.String getPermanentAddress() {
		return _employee.getPermanentAddress();
	}

	/**
	* Sets the permanent address of this employee.
	*
	* @param permanentAddress the permanent address of this employee
	*/
	public void setPermanentAddress(java.lang.String permanentAddress) {
		_employee.setPermanentAddress(permanentAddress);
	}

	/**
	* Returns the photo of this employee.
	*
	* @return the photo of this employee
	*/
	public long getPhoto() {
		return _employee.getPhoto();
	}

	/**
	* Sets the photo of this employee.
	*
	* @param photo the photo of this employee
	*/
	public void setPhoto(long photo) {
		_employee.setPhoto(photo);
	}

	/**
	* Returns the supervisor of this employee.
	*
	* @return the supervisor of this employee
	*/
	public long getSupervisor() {
		return _employee.getSupervisor();
	}

	/**
	* Sets the supervisor of this employee.
	*
	* @param supervisor the supervisor of this employee
	*/
	public void setSupervisor(long supervisor) {
		_employee.setSupervisor(supervisor);
	}

	/**
	* Returns the role of this employee.
	*
	* @return the role of this employee
	*/
	public long getRole() {
		return _employee.getRole();
	}

	/**
	* Sets the role of this employee.
	*
	* @param role the role of this employee
	*/
	public void setRole(long role) {
		_employee.setRole(role);
	}

	/**
	* Returns the duration of this employee.
	*
	* @return the duration of this employee
	*/
	public java.lang.Double getDuration() {
		return _employee.getDuration();
	}

	/**
	* Sets the duration of this employee.
	*
	* @param duration the duration of this employee
	*/
	public void setDuration(java.lang.Double duration) {
		_employee.setDuration(duration);
	}

	/**
	* Returns the minimum duration of this employee.
	*
	* @return the minimum duration of this employee
	*/
	public java.lang.Double getMinimumDuration() {
		return _employee.getMinimumDuration();
	}

	/**
	* Sets the minimum duration of this employee.
	*
	* @param minimumDuration the minimum duration of this employee
	*/
	public void setMinimumDuration(java.lang.Double minimumDuration) {
		_employee.setMinimumDuration(minimumDuration);
	}

	/**
	* Returns the type of this employee.
	*
	* @return the type of this employee
	*/
	public int getType() {
		return _employee.getType();
	}

	/**
	* Sets the type of this employee.
	*
	* @param type the type of this employee
	*/
	public void setType(int type) {
		_employee.setType(type);
	}

	public boolean isNew() {
		return _employee.isNew();
	}

	public void setNew(boolean n) {
		_employee.setNew(n);
	}

	public boolean isCachedModel() {
		return _employee.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_employee.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _employee.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _employee.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_employee.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _employee.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_employee.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new EmployeeWrapper((Employee)_employee.clone());
	}

	public int compareTo(info.diit.portal.model.Employee employee) {
		return _employee.compareTo(employee);
	}

	@Override
	public int hashCode() {
		return _employee.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.Employee> toCacheModel() {
		return _employee.toCacheModel();
	}

	public info.diit.portal.model.Employee toEscapedModel() {
		return new EmployeeWrapper(_employee.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _employee.toString();
	}

	public java.lang.String toXmlString() {
		return _employee.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_employee.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Employee getWrappedEmployee() {
		return _employee;
	}

	public Employee getWrappedModel() {
		return _employee;
	}

	public void resetOriginalValues() {
		_employee.resetOriginalValues();
	}

	private Employee _employee;
}
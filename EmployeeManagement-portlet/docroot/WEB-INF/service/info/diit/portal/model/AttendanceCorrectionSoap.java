/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    limon
 * @generated
 */
public class AttendanceCorrectionSoap implements Serializable {
	public static AttendanceCorrectionSoap toSoapModel(
		AttendanceCorrection model) {
		AttendanceCorrectionSoap soapModel = new AttendanceCorrectionSoap();

		soapModel.setAttendanceCorrectionId(model.getAttendanceCorrectionId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setEmployeeId(model.getEmployeeId());
		soapModel.setCorrectionDate(model.getCorrectionDate());
		soapModel.setRealIn(model.getRealIn());
		soapModel.setRealOut(model.getRealOut());
		soapModel.setExpectedIn(model.getExpectedIn());
		soapModel.setExpectedOut(model.getExpectedOut());
		soapModel.setStatus(model.getStatus());

		return soapModel;
	}

	public static AttendanceCorrectionSoap[] toSoapModels(
		AttendanceCorrection[] models) {
		AttendanceCorrectionSoap[] soapModels = new AttendanceCorrectionSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AttendanceCorrectionSoap[][] toSoapModels(
		AttendanceCorrection[][] models) {
		AttendanceCorrectionSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AttendanceCorrectionSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AttendanceCorrectionSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AttendanceCorrectionSoap[] toSoapModels(
		List<AttendanceCorrection> models) {
		List<AttendanceCorrectionSoap> soapModels = new ArrayList<AttendanceCorrectionSoap>(models.size());

		for (AttendanceCorrection model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AttendanceCorrectionSoap[soapModels.size()]);
	}

	public AttendanceCorrectionSoap() {
	}

	public long getPrimaryKey() {
		return _attendanceCorrectionId;
	}

	public void setPrimaryKey(long pk) {
		setAttendanceCorrectionId(pk);
	}

	public long getAttendanceCorrectionId() {
		return _attendanceCorrectionId;
	}

	public void setAttendanceCorrectionId(long attendanceCorrectionId) {
		_attendanceCorrectionId = attendanceCorrectionId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getEmployeeId() {
		return _employeeId;
	}

	public void setEmployeeId(long employeeId) {
		_employeeId = employeeId;
	}

	public Date getCorrectionDate() {
		return _correctionDate;
	}

	public void setCorrectionDate(Date correctionDate) {
		_correctionDate = correctionDate;
	}

	public Date getRealIn() {
		return _realIn;
	}

	public void setRealIn(Date realIn) {
		_realIn = realIn;
	}

	public Date getRealOut() {
		return _realOut;
	}

	public void setRealOut(Date realOut) {
		_realOut = realOut;
	}

	public Date getExpectedIn() {
		return _expectedIn;
	}

	public void setExpectedIn(Date expectedIn) {
		_expectedIn = expectedIn;
	}

	public Date getExpectedOut() {
		return _expectedOut;
	}

	public void setExpectedOut(Date expectedOut) {
		_expectedOut = expectedOut;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	private long _attendanceCorrectionId;
	private long _organizationId;
	private long _companyId;
	private long _employeeId;
	private Date _correctionDate;
	private Date _realIn;
	private Date _realOut;
	private Date _expectedIn;
	private Date _expectedOut;
	private int _status;
}
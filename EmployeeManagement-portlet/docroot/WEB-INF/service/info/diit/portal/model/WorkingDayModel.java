/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the WorkingDay service. Represents a row in the &quot;DiiTPortal_WorkingDay&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link info.diit.portal.model.impl.WorkingDayModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link info.diit.portal.model.impl.WorkingDayImpl}.
 * </p>
 *
 * @author limon
 * @see WorkingDay
 * @see info.diit.portal.model.impl.WorkingDayImpl
 * @see info.diit.portal.model.impl.WorkingDayModelImpl
 * @generated
 */
public interface WorkingDayModel extends BaseModel<WorkingDay> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a working day model instance should use the {@link WorkingDay} interface instead.
	 */

	/**
	 * Returns the primary key of this working day.
	 *
	 * @return the primary key of this working day
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this working day.
	 *
	 * @param primaryKey the primary key of this working day
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the working day ID of this working day.
	 *
	 * @return the working day ID of this working day
	 */
	public long getWorkingDayId();

	/**
	 * Sets the working day ID of this working day.
	 *
	 * @param workingDayId the working day ID of this working day
	 */
	public void setWorkingDayId(long workingDayId);

	/**
	 * Returns the organization ID of this working day.
	 *
	 * @return the organization ID of this working day
	 */
	public long getOrganizationId();

	/**
	 * Sets the organization ID of this working day.
	 *
	 * @param organizationId the organization ID of this working day
	 */
	public void setOrganizationId(long organizationId);

	/**
	 * Returns the date of this working day.
	 *
	 * @return the date of this working day
	 */
	public Date getDate();

	/**
	 * Sets the date of this working day.
	 *
	 * @param date the date of this working day
	 */
	public void setDate(Date date);

	/**
	 * Returns the day of week of this working day.
	 *
	 * @return the day of week of this working day
	 */
	public int getDayOfWeek();

	/**
	 * Sets the day of week of this working day.
	 *
	 * @param dayOfWeek the day of week of this working day
	 */
	public void setDayOfWeek(int dayOfWeek);

	/**
	 * Returns the week of this working day.
	 *
	 * @return the week of this working day
	 */
	public int getWeek();

	/**
	 * Sets the week of this working day.
	 *
	 * @param week the week of this working day
	 */
	public void setWeek(int week);

	/**
	 * Returns the status of this working day.
	 *
	 * @return the status of this working day
	 */
	public long getStatus();

	/**
	 * Sets the status of this working day.
	 *
	 * @param status the status of this working day
	 */
	public void setStatus(long status);

	public boolean isNew();

	public void setNew(boolean n);

	public boolean isCachedModel();

	public void setCachedModel(boolean cachedModel);

	public boolean isEscapedModel();

	public Serializable getPrimaryKeyObj();

	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	public ExpandoBridge getExpandoBridge();

	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	public Object clone();

	public int compareTo(WorkingDay workingDay);

	public int hashCode();

	public CacheModel<WorkingDay> toCacheModel();

	public WorkingDay toEscapedModel();

	public String toString();

	public String toXmlString();
}
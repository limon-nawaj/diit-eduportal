/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    limon
 * @generated
 */
public class LeaveCategorySoap implements Serializable {
	public static LeaveCategorySoap toSoapModel(LeaveCategory model) {
		LeaveCategorySoap soapModel = new LeaveCategorySoap();

		soapModel.setLeaveCategoryId(model.getLeaveCategoryId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setTitle(model.getTitle());
		soapModel.setTotalLeave(model.getTotalLeave());

		return soapModel;
	}

	public static LeaveCategorySoap[] toSoapModels(LeaveCategory[] models) {
		LeaveCategorySoap[] soapModels = new LeaveCategorySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LeaveCategorySoap[][] toSoapModels(LeaveCategory[][] models) {
		LeaveCategorySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LeaveCategorySoap[models.length][models[0].length];
		}
		else {
			soapModels = new LeaveCategorySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LeaveCategorySoap[] toSoapModels(List<LeaveCategory> models) {
		List<LeaveCategorySoap> soapModels = new ArrayList<LeaveCategorySoap>(models.size());

		for (LeaveCategory model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LeaveCategorySoap[soapModels.size()]);
	}

	public LeaveCategorySoap() {
	}

	public long getPrimaryKey() {
		return _leaveCategoryId;
	}

	public void setPrimaryKey(long pk) {
		setLeaveCategoryId(pk);
	}

	public long getLeaveCategoryId() {
		return _leaveCategoryId;
	}

	public void setLeaveCategoryId(long leaveCategoryId) {
		_leaveCategoryId = leaveCategoryId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public int getTotalLeave() {
		return _totalLeave;
	}

	public void setTotalLeave(int totalLeave) {
		_totalLeave = totalLeave;
	}

	private long _leaveCategoryId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _title;
	private int _totalLeave;
}
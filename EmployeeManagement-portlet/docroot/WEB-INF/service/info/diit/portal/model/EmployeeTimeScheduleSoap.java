/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    limon
 * @generated
 */
public class EmployeeTimeScheduleSoap implements Serializable {
	public static EmployeeTimeScheduleSoap toSoapModel(
		EmployeeTimeSchedule model) {
		EmployeeTimeScheduleSoap soapModel = new EmployeeTimeScheduleSoap();

		soapModel.setEmployeeTimeScheduleId(model.getEmployeeTimeScheduleId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setEmployeeId(model.getEmployeeId());
		soapModel.setDay(model.getDay());
		soapModel.setStartTime(model.getStartTime());
		soapModel.setEndTime(model.getEndTime());
		soapModel.setDuration(model.getDuration());

		return soapModel;
	}

	public static EmployeeTimeScheduleSoap[] toSoapModels(
		EmployeeTimeSchedule[] models) {
		EmployeeTimeScheduleSoap[] soapModels = new EmployeeTimeScheduleSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EmployeeTimeScheduleSoap[][] toSoapModels(
		EmployeeTimeSchedule[][] models) {
		EmployeeTimeScheduleSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EmployeeTimeScheduleSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EmployeeTimeScheduleSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EmployeeTimeScheduleSoap[] toSoapModels(
		List<EmployeeTimeSchedule> models) {
		List<EmployeeTimeScheduleSoap> soapModels = new ArrayList<EmployeeTimeScheduleSoap>(models.size());

		for (EmployeeTimeSchedule model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EmployeeTimeScheduleSoap[soapModels.size()]);
	}

	public EmployeeTimeScheduleSoap() {
	}

	public long getPrimaryKey() {
		return _employeeTimeScheduleId;
	}

	public void setPrimaryKey(long pk) {
		setEmployeeTimeScheduleId(pk);
	}

	public long getEmployeeTimeScheduleId() {
		return _employeeTimeScheduleId;
	}

	public void setEmployeeTimeScheduleId(long employeeTimeScheduleId) {
		_employeeTimeScheduleId = employeeTimeScheduleId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getEmployeeId() {
		return _employeeId;
	}

	public void setEmployeeId(long employeeId) {
		_employeeId = employeeId;
	}

	public int getDay() {
		return _day;
	}

	public void setDay(int day) {
		_day = day;
	}

	public Date getStartTime() {
		return _startTime;
	}

	public void setStartTime(Date startTime) {
		_startTime = startTime;
	}

	public Date getEndTime() {
		return _endTime;
	}

	public void setEndTime(Date endTime) {
		_endTime = endTime;
	}

	public double getDuration() {
		return _duration;
	}

	public void setDuration(double duration) {
		_duration = duration;
	}

	private long _employeeTimeScheduleId;
	private long _organizationId;
	private long _companyId;
	private long _employeeId;
	private int _day;
	private Date _startTime;
	private Date _endTime;
	private double _duration;
}
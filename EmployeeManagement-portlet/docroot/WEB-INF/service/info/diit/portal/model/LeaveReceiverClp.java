/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import info.diit.portal.service.LeaveReceiverLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author limon
 */
public class LeaveReceiverClp extends BaseModelImpl<LeaveReceiver>
	implements LeaveReceiver {
	public LeaveReceiverClp() {
	}

	public Class<?> getModelClass() {
		return LeaveReceiver.class;
	}

	public String getModelClassName() {
		return LeaveReceiver.class.getName();
	}

	public long getPrimaryKey() {
		return _leaveReceiverId;
	}

	public void setPrimaryKey(long primaryKey) {
		setLeaveReceiverId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_leaveReceiverId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("leaveReceiverId", getLeaveReceiverId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("companyId", getCompanyId());
		attributes.put("employeeId", getEmployeeId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long leaveReceiverId = (Long)attributes.get("leaveReceiverId");

		if (leaveReceiverId != null) {
			setLeaveReceiverId(leaveReceiverId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long employeeId = (Long)attributes.get("employeeId");

		if (employeeId != null) {
			setEmployeeId(employeeId);
		}
	}

	public long getLeaveReceiverId() {
		return _leaveReceiverId;
	}

	public void setLeaveReceiverId(long leaveReceiverId) {
		_leaveReceiverId = leaveReceiverId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getEmployeeId() {
		return _employeeId;
	}

	public void setEmployeeId(long employeeId) {
		_employeeId = employeeId;
	}

	public BaseModel<?> getLeaveReceiverRemoteModel() {
		return _leaveReceiverRemoteModel;
	}

	public void setLeaveReceiverRemoteModel(
		BaseModel<?> leaveReceiverRemoteModel) {
		_leaveReceiverRemoteModel = leaveReceiverRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			LeaveReceiverLocalServiceUtil.addLeaveReceiver(this);
		}
		else {
			LeaveReceiverLocalServiceUtil.updateLeaveReceiver(this);
		}
	}

	@Override
	public LeaveReceiver toEscapedModel() {
		return (LeaveReceiver)Proxy.newProxyInstance(LeaveReceiver.class.getClassLoader(),
			new Class[] { LeaveReceiver.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		LeaveReceiverClp clone = new LeaveReceiverClp();

		clone.setLeaveReceiverId(getLeaveReceiverId());
		clone.setOrganizationId(getOrganizationId());
		clone.setCompanyId(getCompanyId());
		clone.setEmployeeId(getEmployeeId());

		return clone;
	}

	public int compareTo(LeaveReceiver leaveReceiver) {
		long primaryKey = leaveReceiver.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		LeaveReceiverClp leaveReceiver = null;

		try {
			leaveReceiver = (LeaveReceiverClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = leaveReceiver.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{leaveReceiverId=");
		sb.append(getLeaveReceiverId());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", employeeId=");
		sb.append(getEmployeeId());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.LeaveReceiver");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>leaveReceiverId</column-name><column-value><![CDATA[");
		sb.append(getLeaveReceiverId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>employeeId</column-name><column-value><![CDATA[");
		sb.append(getEmployeeId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _leaveReceiverId;
	private long _organizationId;
	private long _companyId;
	private long _employeeId;
	private BaseModel<?> _leaveReceiverRemoteModel;
}
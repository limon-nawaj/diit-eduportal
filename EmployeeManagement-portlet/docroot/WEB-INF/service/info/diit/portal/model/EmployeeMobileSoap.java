/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    limon
 * @generated
 */
public class EmployeeMobileSoap implements Serializable {
	public static EmployeeMobileSoap toSoapModel(EmployeeMobile model) {
		EmployeeMobileSoap soapModel = new EmployeeMobileSoap();

		soapModel.setMobileId(model.getMobileId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setEmployeeId(model.getEmployeeId());
		soapModel.setMobile(model.getMobile());
		soapModel.setHomePhone(model.getHomePhone());
		soapModel.setStatus(model.getStatus());

		return soapModel;
	}

	public static EmployeeMobileSoap[] toSoapModels(EmployeeMobile[] models) {
		EmployeeMobileSoap[] soapModels = new EmployeeMobileSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EmployeeMobileSoap[][] toSoapModels(EmployeeMobile[][] models) {
		EmployeeMobileSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EmployeeMobileSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EmployeeMobileSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EmployeeMobileSoap[] toSoapModels(List<EmployeeMobile> models) {
		List<EmployeeMobileSoap> soapModels = new ArrayList<EmployeeMobileSoap>(models.size());

		for (EmployeeMobile model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EmployeeMobileSoap[soapModels.size()]);
	}

	public EmployeeMobileSoap() {
	}

	public long getPrimaryKey() {
		return _mobileId;
	}

	public void setPrimaryKey(long pk) {
		setMobileId(pk);
	}

	public long getMobileId() {
		return _mobileId;
	}

	public void setMobileId(long mobileId) {
		_mobileId = mobileId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getEmployeeId() {
		return _employeeId;
	}

	public void setEmployeeId(long employeeId) {
		_employeeId = employeeId;
	}

	public String getMobile() {
		return _mobile;
	}

	public void setMobile(String mobile) {
		_mobile = mobile;
	}

	public String getHomePhone() {
		return _homePhone;
	}

	public void setHomePhone(String homePhone) {
		_homePhone = homePhone;
	}

	public long getStatus() {
		return _status;
	}

	public void setStatus(long status) {
		_status = status;
	}

	private long _mobileId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _employeeId;
	private String _mobile;
	private String _homePhone;
	private long _status;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import info.diit.portal.service.AttendanceCorrectionLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author limon
 */
public class AttendanceCorrectionClp extends BaseModelImpl<AttendanceCorrection>
	implements AttendanceCorrection {
	public AttendanceCorrectionClp() {
	}

	public Class<?> getModelClass() {
		return AttendanceCorrection.class;
	}

	public String getModelClassName() {
		return AttendanceCorrection.class.getName();
	}

	public long getPrimaryKey() {
		return _attendanceCorrectionId;
	}

	public void setPrimaryKey(long primaryKey) {
		setAttendanceCorrectionId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_attendanceCorrectionId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("attendanceCorrectionId", getAttendanceCorrectionId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("companyId", getCompanyId());
		attributes.put("employeeId", getEmployeeId());
		attributes.put("correctionDate", getCorrectionDate());
		attributes.put("realIn", getRealIn());
		attributes.put("realOut", getRealOut());
		attributes.put("expectedIn", getExpectedIn());
		attributes.put("expectedOut", getExpectedOut());
		attributes.put("status", getStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long attendanceCorrectionId = (Long)attributes.get(
				"attendanceCorrectionId");

		if (attendanceCorrectionId != null) {
			setAttendanceCorrectionId(attendanceCorrectionId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long employeeId = (Long)attributes.get("employeeId");

		if (employeeId != null) {
			setEmployeeId(employeeId);
		}

		Date correctionDate = (Date)attributes.get("correctionDate");

		if (correctionDate != null) {
			setCorrectionDate(correctionDate);
		}

		Date realIn = (Date)attributes.get("realIn");

		if (realIn != null) {
			setRealIn(realIn);
		}

		Date realOut = (Date)attributes.get("realOut");

		if (realOut != null) {
			setRealOut(realOut);
		}

		Date expectedIn = (Date)attributes.get("expectedIn");

		if (expectedIn != null) {
			setExpectedIn(expectedIn);
		}

		Date expectedOut = (Date)attributes.get("expectedOut");

		if (expectedOut != null) {
			setExpectedOut(expectedOut);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	public long getAttendanceCorrectionId() {
		return _attendanceCorrectionId;
	}

	public void setAttendanceCorrectionId(long attendanceCorrectionId) {
		_attendanceCorrectionId = attendanceCorrectionId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getEmployeeId() {
		return _employeeId;
	}

	public void setEmployeeId(long employeeId) {
		_employeeId = employeeId;
	}

	public Date getCorrectionDate() {
		return _correctionDate;
	}

	public void setCorrectionDate(Date correctionDate) {
		_correctionDate = correctionDate;
	}

	public Date getRealIn() {
		return _realIn;
	}

	public void setRealIn(Date realIn) {
		_realIn = realIn;
	}

	public Date getRealOut() {
		return _realOut;
	}

	public void setRealOut(Date realOut) {
		_realOut = realOut;
	}

	public Date getExpectedIn() {
		return _expectedIn;
	}

	public void setExpectedIn(Date expectedIn) {
		_expectedIn = expectedIn;
	}

	public Date getExpectedOut() {
		return _expectedOut;
	}

	public void setExpectedOut(Date expectedOut) {
		_expectedOut = expectedOut;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public BaseModel<?> getAttendanceCorrectionRemoteModel() {
		return _attendanceCorrectionRemoteModel;
	}

	public void setAttendanceCorrectionRemoteModel(
		BaseModel<?> attendanceCorrectionRemoteModel) {
		_attendanceCorrectionRemoteModel = attendanceCorrectionRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			AttendanceCorrectionLocalServiceUtil.addAttendanceCorrection(this);
		}
		else {
			AttendanceCorrectionLocalServiceUtil.updateAttendanceCorrection(this);
		}
	}

	@Override
	public AttendanceCorrection toEscapedModel() {
		return (AttendanceCorrection)Proxy.newProxyInstance(AttendanceCorrection.class.getClassLoader(),
			new Class[] { AttendanceCorrection.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		AttendanceCorrectionClp clone = new AttendanceCorrectionClp();

		clone.setAttendanceCorrectionId(getAttendanceCorrectionId());
		clone.setOrganizationId(getOrganizationId());
		clone.setCompanyId(getCompanyId());
		clone.setEmployeeId(getEmployeeId());
		clone.setCorrectionDate(getCorrectionDate());
		clone.setRealIn(getRealIn());
		clone.setRealOut(getRealOut());
		clone.setExpectedIn(getExpectedIn());
		clone.setExpectedOut(getExpectedOut());
		clone.setStatus(getStatus());

		return clone;
	}

	public int compareTo(AttendanceCorrection attendanceCorrection) {
		int value = 0;

		if (getAttendanceCorrectionId() < attendanceCorrection.getAttendanceCorrectionId()) {
			value = -1;
		}
		else if (getAttendanceCorrectionId() > attendanceCorrection.getAttendanceCorrectionId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		if (getStatus() < attendanceCorrection.getStatus()) {
			value = -1;
		}
		else if (getStatus() > attendanceCorrection.getStatus()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		AttendanceCorrectionClp attendanceCorrection = null;

		try {
			attendanceCorrection = (AttendanceCorrectionClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = attendanceCorrection.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{attendanceCorrectionId=");
		sb.append(getAttendanceCorrectionId());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", employeeId=");
		sb.append(getEmployeeId());
		sb.append(", correctionDate=");
		sb.append(getCorrectionDate());
		sb.append(", realIn=");
		sb.append(getRealIn());
		sb.append(", realOut=");
		sb.append(getRealOut());
		sb.append(", expectedIn=");
		sb.append(getExpectedIn());
		sb.append(", expectedOut=");
		sb.append(getExpectedOut());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.AttendanceCorrection");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>attendanceCorrectionId</column-name><column-value><![CDATA[");
		sb.append(getAttendanceCorrectionId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>employeeId</column-name><column-value><![CDATA[");
		sb.append(getEmployeeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>correctionDate</column-name><column-value><![CDATA[");
		sb.append(getCorrectionDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>realIn</column-name><column-value><![CDATA[");
		sb.append(getRealIn());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>realOut</column-name><column-value><![CDATA[");
		sb.append(getRealOut());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>expectedIn</column-name><column-value><![CDATA[");
		sb.append(getExpectedIn());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>expectedOut</column-name><column-value><![CDATA[");
		sb.append(getExpectedOut());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _attendanceCorrectionId;
	private long _organizationId;
	private long _companyId;
	private long _employeeId;
	private Date _correctionDate;
	private Date _realIn;
	private Date _realOut;
	private Date _expectedIn;
	private Date _expectedOut;
	private int _status;
	private BaseModel<?> _attendanceCorrectionRemoteModel;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.service.EmployeeLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author limon
 */
public class EmployeeClp extends BaseModelImpl<Employee> implements Employee {
	public EmployeeClp() {
	}

	public Class<?> getModelClass() {
		return Employee.class;
	}

	public String getModelClassName() {
		return Employee.class.getName();
	}

	public long getPrimaryKey() {
		return _employeeId;
	}

	public void setPrimaryKey(long primaryKey) {
		setEmployeeId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_employeeId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("employeeId", getEmployeeId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("employeeUserId", getEmployeeUserId());
		attributes.put("name", getName());
		attributes.put("gender", getGender());
		attributes.put("religion", getReligion());
		attributes.put("blood", getBlood());
		attributes.put("dob", getDob());
		attributes.put("designation", getDesignation());
		attributes.put("branch", getBranch());
		attributes.put("status", getStatus());
		attributes.put("presentAddress", getPresentAddress());
		attributes.put("permanentAddress", getPermanentAddress());
		attributes.put("photo", getPhoto());
		attributes.put("supervisor", getSupervisor());
		attributes.put("role", getRole());
		attributes.put("duration", getDuration());
		attributes.put("minimumDuration", getMinimumDuration());
		attributes.put("type", getType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long employeeId = (Long)attributes.get("employeeId");

		if (employeeId != null) {
			setEmployeeId(employeeId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long employeeUserId = (Long)attributes.get("employeeUserId");

		if (employeeUserId != null) {
			setEmployeeUserId(employeeUserId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Long gender = (Long)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		Long religion = (Long)attributes.get("religion");

		if (religion != null) {
			setReligion(religion);
		}

		Long blood = (Long)attributes.get("blood");

		if (blood != null) {
			setBlood(blood);
		}

		Date dob = (Date)attributes.get("dob");

		if (dob != null) {
			setDob(dob);
		}

		Long designation = (Long)attributes.get("designation");

		if (designation != null) {
			setDesignation(designation);
		}

		Long branch = (Long)attributes.get("branch");

		if (branch != null) {
			setBranch(branch);
		}

		Long status = (Long)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		String presentAddress = (String)attributes.get("presentAddress");

		if (presentAddress != null) {
			setPresentAddress(presentAddress);
		}

		String permanentAddress = (String)attributes.get("permanentAddress");

		if (permanentAddress != null) {
			setPermanentAddress(permanentAddress);
		}

		Long photo = (Long)attributes.get("photo");

		if (photo != null) {
			setPhoto(photo);
		}

		Long supervisor = (Long)attributes.get("supervisor");

		if (supervisor != null) {
			setSupervisor(supervisor);
		}

		Long role = (Long)attributes.get("role");

		if (role != null) {
			setRole(role);
		}

		Double duration = (Double)attributes.get("duration");

		if (duration != null) {
			setDuration(duration);
		}

		Double minimumDuration = (Double)attributes.get("minimumDuration");

		if (minimumDuration != null) {
			setMinimumDuration(minimumDuration);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}
	}

	public long getEmployeeId() {
		return _employeeId;
	}

	public void setEmployeeId(long employeeId) {
		_employeeId = employeeId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getEmployeeUserId() {
		return _employeeUserId;
	}

	public void setEmployeeUserId(long employeeUserId) {
		_employeeUserId = employeeUserId;
	}

	public String getEmployeeUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getEmployeeUserId(), "uuid",
			_employeeUserUuid);
	}

	public void setEmployeeUserUuid(String employeeUserUuid) {
		_employeeUserUuid = employeeUserUuid;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public long getGender() {
		return _gender;
	}

	public void setGender(long gender) {
		_gender = gender;
	}

	public long getReligion() {
		return _religion;
	}

	public void setReligion(long religion) {
		_religion = religion;
	}

	public long getBlood() {
		return _blood;
	}

	public void setBlood(long blood) {
		_blood = blood;
	}

	public Date getDob() {
		return _dob;
	}

	public void setDob(Date dob) {
		_dob = dob;
	}

	public long getDesignation() {
		return _designation;
	}

	public void setDesignation(long designation) {
		_designation = designation;
	}

	public long getBranch() {
		return _branch;
	}

	public void setBranch(long branch) {
		_branch = branch;
	}

	public long getStatus() {
		return _status;
	}

	public void setStatus(long status) {
		_status = status;
	}

	public String getPresentAddress() {
		return _presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		_presentAddress = presentAddress;
	}

	public String getPermanentAddress() {
		return _permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		_permanentAddress = permanentAddress;
	}

	public long getPhoto() {
		return _photo;
	}

	public void setPhoto(long photo) {
		_photo = photo;
	}

	public long getSupervisor() {
		return _supervisor;
	}

	public void setSupervisor(long supervisor) {
		_supervisor = supervisor;
	}

	public long getRole() {
		return _role;
	}

	public void setRole(long role) {
		_role = role;
	}

	public Double getDuration() {
		return _duration;
	}

	public void setDuration(Double duration) {
		_duration = duration;
	}

	public Double getMinimumDuration() {
		return _minimumDuration;
	}

	public void setMinimumDuration(Double minimumDuration) {
		_minimumDuration = minimumDuration;
	}

	public int getType() {
		return _type;
	}

	public void setType(int type) {
		_type = type;
	}

	public BaseModel<?> getEmployeeRemoteModel() {
		return _employeeRemoteModel;
	}

	public void setEmployeeRemoteModel(BaseModel<?> employeeRemoteModel) {
		_employeeRemoteModel = employeeRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			EmployeeLocalServiceUtil.addEmployee(this);
		}
		else {
			EmployeeLocalServiceUtil.updateEmployee(this);
		}
	}

	@Override
	public Employee toEscapedModel() {
		return (Employee)Proxy.newProxyInstance(Employee.class.getClassLoader(),
			new Class[] { Employee.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		EmployeeClp clone = new EmployeeClp();

		clone.setEmployeeId(getEmployeeId());
		clone.setCompanyId(getCompanyId());
		clone.setOrganizationId(getOrganizationId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setEmployeeUserId(getEmployeeUserId());
		clone.setName(getName());
		clone.setGender(getGender());
		clone.setReligion(getReligion());
		clone.setBlood(getBlood());
		clone.setDob(getDob());
		clone.setDesignation(getDesignation());
		clone.setBranch(getBranch());
		clone.setStatus(getStatus());
		clone.setPresentAddress(getPresentAddress());
		clone.setPermanentAddress(getPermanentAddress());
		clone.setPhoto(getPhoto());
		clone.setSupervisor(getSupervisor());
		clone.setRole(getRole());
		clone.setDuration(getDuration());
		clone.setMinimumDuration(getMinimumDuration());
		clone.setType(getType());

		return clone;
	}

	public int compareTo(Employee employee) {
		long primaryKey = employee.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		EmployeeClp employee = null;

		try {
			employee = (EmployeeClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = employee.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(49);

		sb.append("{employeeId=");
		sb.append(getEmployeeId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", employeeUserId=");
		sb.append(getEmployeeUserId());
		sb.append(", name=");
		sb.append(getName());
		sb.append(", gender=");
		sb.append(getGender());
		sb.append(", religion=");
		sb.append(getReligion());
		sb.append(", blood=");
		sb.append(getBlood());
		sb.append(", dob=");
		sb.append(getDob());
		sb.append(", designation=");
		sb.append(getDesignation());
		sb.append(", branch=");
		sb.append(getBranch());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append(", presentAddress=");
		sb.append(getPresentAddress());
		sb.append(", permanentAddress=");
		sb.append(getPermanentAddress());
		sb.append(", photo=");
		sb.append(getPhoto());
		sb.append(", supervisor=");
		sb.append(getSupervisor());
		sb.append(", role=");
		sb.append(getRole());
		sb.append(", duration=");
		sb.append(getDuration());
		sb.append(", minimumDuration=");
		sb.append(getMinimumDuration());
		sb.append(", type=");
		sb.append(getType());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(76);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.Employee");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>employeeId</column-name><column-value><![CDATA[");
		sb.append(getEmployeeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>employeeUserId</column-name><column-value><![CDATA[");
		sb.append(getEmployeeUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>gender</column-name><column-value><![CDATA[");
		sb.append(getGender());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>religion</column-name><column-value><![CDATA[");
		sb.append(getReligion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>blood</column-name><column-value><![CDATA[");
		sb.append(getBlood());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dob</column-name><column-value><![CDATA[");
		sb.append(getDob());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>designation</column-name><column-value><![CDATA[");
		sb.append(getDesignation());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>branch</column-name><column-value><![CDATA[");
		sb.append(getBranch());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>presentAddress</column-name><column-value><![CDATA[");
		sb.append(getPresentAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>permanentAddress</column-name><column-value><![CDATA[");
		sb.append(getPermanentAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>photo</column-name><column-value><![CDATA[");
		sb.append(getPhoto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>supervisor</column-name><column-value><![CDATA[");
		sb.append(getSupervisor());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>role</column-name><column-value><![CDATA[");
		sb.append(getRole());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>duration</column-name><column-value><![CDATA[");
		sb.append(getDuration());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>minimumDuration</column-name><column-value><![CDATA[");
		sb.append(getMinimumDuration());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>type</column-name><column-value><![CDATA[");
		sb.append(getType());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _employeeId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _employeeUserId;
	private String _employeeUserUuid;
	private String _name;
	private long _gender;
	private long _religion;
	private long _blood;
	private Date _dob;
	private long _designation;
	private long _branch;
	private long _status;
	private String _presentAddress;
	private String _permanentAddress;
	private long _photo;
	private long _supervisor;
	private long _role;
	private Double _duration;
	private Double _minimumDuration;
	private int _type;
	private BaseModel<?> _employeeRemoteModel;
}
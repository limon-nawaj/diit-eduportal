/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import info.diit.portal.service.WorkingDayLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author limon
 */
public class WorkingDayClp extends BaseModelImpl<WorkingDay>
	implements WorkingDay {
	public WorkingDayClp() {
	}

	public Class<?> getModelClass() {
		return WorkingDay.class;
	}

	public String getModelClassName() {
		return WorkingDay.class.getName();
	}

	public long getPrimaryKey() {
		return _workingDayId;
	}

	public void setPrimaryKey(long primaryKey) {
		setWorkingDayId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_workingDayId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("workingDayId", getWorkingDayId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("date", getDate());
		attributes.put("dayOfWeek", getDayOfWeek());
		attributes.put("week", getWeek());
		attributes.put("status", getStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long workingDayId = (Long)attributes.get("workingDayId");

		if (workingDayId != null) {
			setWorkingDayId(workingDayId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		Integer dayOfWeek = (Integer)attributes.get("dayOfWeek");

		if (dayOfWeek != null) {
			setDayOfWeek(dayOfWeek);
		}

		Integer week = (Integer)attributes.get("week");

		if (week != null) {
			setWeek(week);
		}

		Long status = (Long)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	public long getWorkingDayId() {
		return _workingDayId;
	}

	public void setWorkingDayId(long workingDayId) {
		_workingDayId = workingDayId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public Date getDate() {
		return _date;
	}

	public void setDate(Date date) {
		_date = date;
	}

	public int getDayOfWeek() {
		return _dayOfWeek;
	}

	public void setDayOfWeek(int dayOfWeek) {
		_dayOfWeek = dayOfWeek;
	}

	public int getWeek() {
		return _week;
	}

	public void setWeek(int week) {
		_week = week;
	}

	public long getStatus() {
		return _status;
	}

	public void setStatus(long status) {
		_status = status;
	}

	public BaseModel<?> getWorkingDayRemoteModel() {
		return _workingDayRemoteModel;
	}

	public void setWorkingDayRemoteModel(BaseModel<?> workingDayRemoteModel) {
		_workingDayRemoteModel = workingDayRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			WorkingDayLocalServiceUtil.addWorkingDay(this);
		}
		else {
			WorkingDayLocalServiceUtil.updateWorkingDay(this);
		}
	}

	@Override
	public WorkingDay toEscapedModel() {
		return (WorkingDay)Proxy.newProxyInstance(WorkingDay.class.getClassLoader(),
			new Class[] { WorkingDay.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		WorkingDayClp clone = new WorkingDayClp();

		clone.setWorkingDayId(getWorkingDayId());
		clone.setOrganizationId(getOrganizationId());
		clone.setDate(getDate());
		clone.setDayOfWeek(getDayOfWeek());
		clone.setWeek(getWeek());
		clone.setStatus(getStatus());

		return clone;
	}

	public int compareTo(WorkingDay workingDay) {
		long primaryKey = workingDay.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		WorkingDayClp workingDay = null;

		try {
			workingDay = (WorkingDayClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = workingDay.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{workingDayId=");
		sb.append(getWorkingDayId());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", date=");
		sb.append(getDate());
		sb.append(", dayOfWeek=");
		sb.append(getDayOfWeek());
		sb.append(", week=");
		sb.append(getWeek());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.WorkingDay");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>workingDayId</column-name><column-value><![CDATA[");
		sb.append(getWorkingDayId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>date</column-name><column-value><![CDATA[");
		sb.append(getDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dayOfWeek</column-name><column-value><![CDATA[");
		sb.append(getDayOfWeek());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>week</column-name><column-value><![CDATA[");
		sb.append(getWeek());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _workingDayId;
	private long _organizationId;
	private Date _date;
	private int _dayOfWeek;
	private int _week;
	private long _status;
	private BaseModel<?> _workingDayRemoteModel;
}
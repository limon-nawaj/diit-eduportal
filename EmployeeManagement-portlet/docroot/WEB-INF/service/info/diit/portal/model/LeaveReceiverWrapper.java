/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link LeaveReceiver}.
 * </p>
 *
 * @author    limon
 * @see       LeaveReceiver
 * @generated
 */
public class LeaveReceiverWrapper implements LeaveReceiver,
	ModelWrapper<LeaveReceiver> {
	public LeaveReceiverWrapper(LeaveReceiver leaveReceiver) {
		_leaveReceiver = leaveReceiver;
	}

	public Class<?> getModelClass() {
		return LeaveReceiver.class;
	}

	public String getModelClassName() {
		return LeaveReceiver.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("leaveReceiverId", getLeaveReceiverId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("companyId", getCompanyId());
		attributes.put("employeeId", getEmployeeId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long leaveReceiverId = (Long)attributes.get("leaveReceiverId");

		if (leaveReceiverId != null) {
			setLeaveReceiverId(leaveReceiverId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long employeeId = (Long)attributes.get("employeeId");

		if (employeeId != null) {
			setEmployeeId(employeeId);
		}
	}

	/**
	* Returns the primary key of this leave receiver.
	*
	* @return the primary key of this leave receiver
	*/
	public long getPrimaryKey() {
		return _leaveReceiver.getPrimaryKey();
	}

	/**
	* Sets the primary key of this leave receiver.
	*
	* @param primaryKey the primary key of this leave receiver
	*/
	public void setPrimaryKey(long primaryKey) {
		_leaveReceiver.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the leave receiver ID of this leave receiver.
	*
	* @return the leave receiver ID of this leave receiver
	*/
	public long getLeaveReceiverId() {
		return _leaveReceiver.getLeaveReceiverId();
	}

	/**
	* Sets the leave receiver ID of this leave receiver.
	*
	* @param leaveReceiverId the leave receiver ID of this leave receiver
	*/
	public void setLeaveReceiverId(long leaveReceiverId) {
		_leaveReceiver.setLeaveReceiverId(leaveReceiverId);
	}

	/**
	* Returns the organization ID of this leave receiver.
	*
	* @return the organization ID of this leave receiver
	*/
	public long getOrganizationId() {
		return _leaveReceiver.getOrganizationId();
	}

	/**
	* Sets the organization ID of this leave receiver.
	*
	* @param organizationId the organization ID of this leave receiver
	*/
	public void setOrganizationId(long organizationId) {
		_leaveReceiver.setOrganizationId(organizationId);
	}

	/**
	* Returns the company ID of this leave receiver.
	*
	* @return the company ID of this leave receiver
	*/
	public long getCompanyId() {
		return _leaveReceiver.getCompanyId();
	}

	/**
	* Sets the company ID of this leave receiver.
	*
	* @param companyId the company ID of this leave receiver
	*/
	public void setCompanyId(long companyId) {
		_leaveReceiver.setCompanyId(companyId);
	}

	/**
	* Returns the employee ID of this leave receiver.
	*
	* @return the employee ID of this leave receiver
	*/
	public long getEmployeeId() {
		return _leaveReceiver.getEmployeeId();
	}

	/**
	* Sets the employee ID of this leave receiver.
	*
	* @param employeeId the employee ID of this leave receiver
	*/
	public void setEmployeeId(long employeeId) {
		_leaveReceiver.setEmployeeId(employeeId);
	}

	public boolean isNew() {
		return _leaveReceiver.isNew();
	}

	public void setNew(boolean n) {
		_leaveReceiver.setNew(n);
	}

	public boolean isCachedModel() {
		return _leaveReceiver.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_leaveReceiver.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _leaveReceiver.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _leaveReceiver.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_leaveReceiver.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _leaveReceiver.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_leaveReceiver.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LeaveReceiverWrapper((LeaveReceiver)_leaveReceiver.clone());
	}

	public int compareTo(info.diit.portal.model.LeaveReceiver leaveReceiver) {
		return _leaveReceiver.compareTo(leaveReceiver);
	}

	@Override
	public int hashCode() {
		return _leaveReceiver.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.LeaveReceiver> toCacheModel() {
		return _leaveReceiver.toCacheModel();
	}

	public info.diit.portal.model.LeaveReceiver toEscapedModel() {
		return new LeaveReceiverWrapper(_leaveReceiver.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _leaveReceiver.toString();
	}

	public java.lang.String toXmlString() {
		return _leaveReceiver.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_leaveReceiver.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public LeaveReceiver getWrappedLeaveReceiver() {
		return _leaveReceiver;
	}

	public LeaveReceiver getWrappedModel() {
		return _leaveReceiver;
	}

	public void resetOriginalValues() {
		_leaveReceiver.resetOriginalValues();
	}

	private LeaveReceiver _leaveReceiver;
}
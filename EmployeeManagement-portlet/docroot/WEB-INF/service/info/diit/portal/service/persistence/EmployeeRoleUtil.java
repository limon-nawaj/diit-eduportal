/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.EmployeeRole;

import java.util.List;

/**
 * The persistence utility for the employee role service. This utility wraps {@link EmployeeRolePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see EmployeeRolePersistence
 * @see EmployeeRolePersistenceImpl
 * @generated
 */
public class EmployeeRoleUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(EmployeeRole employeeRole) {
		getPersistence().clearCache(employeeRole);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<EmployeeRole> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<EmployeeRole> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<EmployeeRole> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static EmployeeRole update(EmployeeRole employeeRole, boolean merge)
		throws SystemException {
		return getPersistence().update(employeeRole, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static EmployeeRole update(EmployeeRole employeeRole, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(employeeRole, merge, serviceContext);
	}

	/**
	* Caches the employee role in the entity cache if it is enabled.
	*
	* @param employeeRole the employee role
	*/
	public static void cacheResult(
		info.diit.portal.model.EmployeeRole employeeRole) {
		getPersistence().cacheResult(employeeRole);
	}

	/**
	* Caches the employee roles in the entity cache if it is enabled.
	*
	* @param employeeRoles the employee roles
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.EmployeeRole> employeeRoles) {
		getPersistence().cacheResult(employeeRoles);
	}

	/**
	* Creates a new employee role with the primary key. Does not add the employee role to the database.
	*
	* @param employeeRoleId the primary key for the new employee role
	* @return the new employee role
	*/
	public static info.diit.portal.model.EmployeeRole create(
		long employeeRoleId) {
		return getPersistence().create(employeeRoleId);
	}

	/**
	* Removes the employee role with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param employeeRoleId the primary key of the employee role
	* @return the employee role that was removed
	* @throws info.diit.portal.NoSuchEmployeeRoleException if a employee role with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeRole remove(
		long employeeRoleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeRoleException {
		return getPersistence().remove(employeeRoleId);
	}

	public static info.diit.portal.model.EmployeeRole updateImpl(
		info.diit.portal.model.EmployeeRole employeeRole, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(employeeRole, merge);
	}

	/**
	* Returns the employee role with the primary key or throws a {@link info.diit.portal.NoSuchEmployeeRoleException} if it could not be found.
	*
	* @param employeeRoleId the primary key of the employee role
	* @return the employee role
	* @throws info.diit.portal.NoSuchEmployeeRoleException if a employee role with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeRole findByPrimaryKey(
		long employeeRoleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeRoleException {
		return getPersistence().findByPrimaryKey(employeeRoleId);
	}

	/**
	* Returns the employee role with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param employeeRoleId the primary key of the employee role
	* @return the employee role, or <code>null</code> if a employee role with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeRole fetchByPrimaryKey(
		long employeeRoleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(employeeRoleId);
	}

	/**
	* Returns all the employee roles where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching employee roles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeRole> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganization(organizationId);
	}

	/**
	* Returns a range of all the employee roles where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of employee roles
	* @param end the upper bound of the range of employee roles (not inclusive)
	* @return the range of matching employee roles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeRole> findByOrganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganization(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the employee roles where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of employee roles
	* @param end the upper bound of the range of employee roles (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching employee roles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeRole> findByOrganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByOrganization(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first employee role in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee role
	* @throws info.diit.portal.NoSuchEmployeeRoleException if a matching employee role could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeRole findByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeRoleException {
		return getPersistence()
				   .findByOrganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first employee role in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee role, or <code>null</code> if a matching employee role could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeRole fetchByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the last employee role in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee role
	* @throws info.diit.portal.NoSuchEmployeeRoleException if a matching employee role could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeRole findByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeRoleException {
		return getPersistence()
				   .findByOrganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last employee role in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee role, or <code>null</code> if a matching employee role could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeRole fetchByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the employee roles before and after the current employee role in the ordered set where organizationId = &#63;.
	*
	* @param employeeRoleId the primary key of the current employee role
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next employee role
	* @throws info.diit.portal.NoSuchEmployeeRoleException if a employee role with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeRole[] findByOrganization_PrevAndNext(
		long employeeRoleId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeRoleException {
		return getPersistence()
				   .findByOrganization_PrevAndNext(employeeRoleId,
			organizationId, orderByComparator);
	}

	/**
	* Returns all the employee roles where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching employee roles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeRole> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId);
	}

	/**
	* Returns a range of all the employee roles where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of employee roles
	* @param end the upper bound of the range of employee roles (not inclusive)
	* @return the range of matching employee roles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeRole> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the employee roles where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of employee roles
	* @param end the upper bound of the range of employee roles (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching employee roles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeRole> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first employee role in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee role
	* @throws info.diit.portal.NoSuchEmployeeRoleException if a matching employee role could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeRole findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeRoleException {
		return getPersistence().findByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first employee role in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee role, or <code>null</code> if a matching employee role could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeRole fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last employee role in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee role
	* @throws info.diit.portal.NoSuchEmployeeRoleException if a matching employee role could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeRole findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeRoleException {
		return getPersistence().findByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last employee role in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee role, or <code>null</code> if a matching employee role could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeRole fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the employee roles before and after the current employee role in the ordered set where companyId = &#63;.
	*
	* @param employeeRoleId the primary key of the current employee role
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next employee role
	* @throws info.diit.portal.NoSuchEmployeeRoleException if a employee role with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeRole[] findByCompany_PrevAndNext(
		long employeeRoleId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeRoleException {
		return getPersistence()
				   .findByCompany_PrevAndNext(employeeRoleId, companyId,
			orderByComparator);
	}

	/**
	* Returns all the employee roles.
	*
	* @return the employee roles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeRole> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the employee roles.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee roles
	* @param end the upper bound of the range of employee roles (not inclusive)
	* @return the range of employee roles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeRole> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the employee roles.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee roles
	* @param end the upper bound of the range of employee roles (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of employee roles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeRole> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the employee roles where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByOrganization(organizationId);
	}

	/**
	* Removes all the employee roles where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCompany(companyId);
	}

	/**
	* Removes all the employee roles from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of employee roles where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching employee roles
	* @throws SystemException if a system exception occurred
	*/
	public static int countByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByOrganization(organizationId);
	}

	/**
	* Returns the number of employee roles where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching employee roles
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCompany(companyId);
	}

	/**
	* Returns the number of employee roles.
	*
	* @return the number of employee roles
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static EmployeeRolePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (EmployeeRolePersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					EmployeeRolePersistence.class.getName());

			ReferenceRegistry.registerReference(EmployeeRoleUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(EmployeeRolePersistence persistence) {
	}

	private static EmployeeRolePersistence _persistence;
}
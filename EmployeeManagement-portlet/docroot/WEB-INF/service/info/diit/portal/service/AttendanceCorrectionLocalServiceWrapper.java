/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link AttendanceCorrectionLocalService}.
 * </p>
 *
 * @author    limon
 * @see       AttendanceCorrectionLocalService
 * @generated
 */
public class AttendanceCorrectionLocalServiceWrapper
	implements AttendanceCorrectionLocalService,
		ServiceWrapper<AttendanceCorrectionLocalService> {
	public AttendanceCorrectionLocalServiceWrapper(
		AttendanceCorrectionLocalService attendanceCorrectionLocalService) {
		_attendanceCorrectionLocalService = attendanceCorrectionLocalService;
	}

	/**
	* Adds the attendance correction to the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceCorrection the attendance correction
	* @return the attendance correction that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection addAttendanceCorrection(
		info.diit.portal.model.AttendanceCorrection attendanceCorrection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.addAttendanceCorrection(attendanceCorrection);
	}

	/**
	* Creates a new attendance correction with the primary key. Does not add the attendance correction to the database.
	*
	* @param attendanceCorrectionId the primary key for the new attendance correction
	* @return the new attendance correction
	*/
	public info.diit.portal.model.AttendanceCorrection createAttendanceCorrection(
		long attendanceCorrectionId) {
		return _attendanceCorrectionLocalService.createAttendanceCorrection(attendanceCorrectionId);
	}

	/**
	* Deletes the attendance correction with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceCorrectionId the primary key of the attendance correction
	* @return the attendance correction that was removed
	* @throws PortalException if a attendance correction with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection deleteAttendanceCorrection(
		long attendanceCorrectionId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.deleteAttendanceCorrection(attendanceCorrectionId);
	}

	/**
	* Deletes the attendance correction from the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceCorrection the attendance correction
	* @return the attendance correction that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection deleteAttendanceCorrection(
		info.diit.portal.model.AttendanceCorrection attendanceCorrection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.deleteAttendanceCorrection(attendanceCorrection);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _attendanceCorrectionLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.AttendanceCorrection fetchAttendanceCorrection(
		long attendanceCorrectionId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.fetchAttendanceCorrection(attendanceCorrectionId);
	}

	/**
	* Returns the attendance correction with the primary key.
	*
	* @param attendanceCorrectionId the primary key of the attendance correction
	* @return the attendance correction
	* @throws PortalException if a attendance correction with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection getAttendanceCorrection(
		long attendanceCorrectionId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.getAttendanceCorrection(attendanceCorrectionId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the attendance corrections.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attendance corrections
	* @param end the upper bound of the range of attendance corrections (not inclusive)
	* @return the range of attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AttendanceCorrection> getAttendanceCorrections(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.getAttendanceCorrections(start,
			end);
	}

	/**
	* Returns the number of attendance corrections.
	*
	* @return the number of attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public int getAttendanceCorrectionsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.getAttendanceCorrectionsCount();
	}

	/**
	* Updates the attendance correction in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param attendanceCorrection the attendance correction
	* @return the attendance correction that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection updateAttendanceCorrection(
		info.diit.portal.model.AttendanceCorrection attendanceCorrection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.updateAttendanceCorrection(attendanceCorrection);
	}

	/**
	* Updates the attendance correction in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param attendanceCorrection the attendance correction
	* @param merge whether to merge the attendance correction with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the attendance correction that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection updateAttendanceCorrection(
		info.diit.portal.model.AttendanceCorrection attendanceCorrection,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.updateAttendanceCorrection(attendanceCorrection,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _attendanceCorrectionLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_attendanceCorrectionLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _attendanceCorrectionLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	public java.util.List<info.diit.portal.model.AttendanceCorrection> findByEmployee(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.findByEmployee(employeeId);
	}

	public java.util.List<info.diit.portal.model.AttendanceCorrection> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceCorrectionLocalService.findByCompany(companyId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public AttendanceCorrectionLocalService getWrappedAttendanceCorrectionLocalService() {
		return _attendanceCorrectionLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedAttendanceCorrectionLocalService(
		AttendanceCorrectionLocalService attendanceCorrectionLocalService) {
		_attendanceCorrectionLocalService = attendanceCorrectionLocalService;
	}

	public AttendanceCorrectionLocalService getWrappedService() {
		return _attendanceCorrectionLocalService;
	}

	public void setWrappedService(
		AttendanceCorrectionLocalService attendanceCorrectionLocalService) {
		_attendanceCorrectionLocalService = attendanceCorrectionLocalService;
	}

	private AttendanceCorrectionLocalService _attendanceCorrectionLocalService;
}
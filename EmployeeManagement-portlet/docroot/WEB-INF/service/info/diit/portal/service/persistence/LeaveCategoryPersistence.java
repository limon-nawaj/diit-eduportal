/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.LeaveCategory;

/**
 * The persistence interface for the leave category service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see LeaveCategoryPersistenceImpl
 * @see LeaveCategoryUtil
 * @generated
 */
public interface LeaveCategoryPersistence extends BasePersistence<LeaveCategory> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LeaveCategoryUtil} to access the leave category persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the leave category in the entity cache if it is enabled.
	*
	* @param leaveCategory the leave category
	*/
	public void cacheResult(info.diit.portal.model.LeaveCategory leaveCategory);

	/**
	* Caches the leave categories in the entity cache if it is enabled.
	*
	* @param leaveCategories the leave categories
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.LeaveCategory> leaveCategories);

	/**
	* Creates a new leave category with the primary key. Does not add the leave category to the database.
	*
	* @param leaveCategoryId the primary key for the new leave category
	* @return the new leave category
	*/
	public info.diit.portal.model.LeaveCategory create(long leaveCategoryId);

	/**
	* Removes the leave category with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param leaveCategoryId the primary key of the leave category
	* @return the leave category that was removed
	* @throws info.diit.portal.NoSuchLeaveCategoryException if a leave category with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveCategory remove(long leaveCategoryId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveCategoryException;

	public info.diit.portal.model.LeaveCategory updateImpl(
		info.diit.portal.model.LeaveCategory leaveCategory, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the leave category with the primary key or throws a {@link info.diit.portal.NoSuchLeaveCategoryException} if it could not be found.
	*
	* @param leaveCategoryId the primary key of the leave category
	* @return the leave category
	* @throws info.diit.portal.NoSuchLeaveCategoryException if a leave category with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveCategory findByPrimaryKey(
		long leaveCategoryId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveCategoryException;

	/**
	* Returns the leave category with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param leaveCategoryId the primary key of the leave category
	* @return the leave category, or <code>null</code> if a leave category with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveCategory fetchByPrimaryKey(
		long leaveCategoryId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the leave categories where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching leave categories
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveCategory> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the leave categories where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of leave categories
	* @param end the upper bound of the range of leave categories (not inclusive)
	* @return the range of matching leave categories
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveCategory> findByOrganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the leave categories where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of leave categories
	* @param end the upper bound of the range of leave categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching leave categories
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveCategory> findByOrganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first leave category in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave category
	* @throws info.diit.portal.NoSuchLeaveCategoryException if a matching leave category could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveCategory findByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveCategoryException;

	/**
	* Returns the first leave category in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave category, or <code>null</code> if a matching leave category could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveCategory fetchByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last leave category in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave category
	* @throws info.diit.portal.NoSuchLeaveCategoryException if a matching leave category could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveCategory findByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveCategoryException;

	/**
	* Returns the last leave category in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave category, or <code>null</code> if a matching leave category could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveCategory fetchByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the leave categories before and after the current leave category in the ordered set where organizationId = &#63;.
	*
	* @param leaveCategoryId the primary key of the current leave category
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next leave category
	* @throws info.diit.portal.NoSuchLeaveCategoryException if a leave category with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveCategory[] findByOrganization_PrevAndNext(
		long leaveCategoryId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveCategoryException;

	/**
	* Returns all the leave categories.
	*
	* @return the leave categories
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveCategory> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the leave categories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leave categories
	* @param end the upper bound of the range of leave categories (not inclusive)
	* @return the range of leave categories
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveCategory> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the leave categories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leave categories
	* @param end the upper bound of the range of leave categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of leave categories
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveCategory> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the leave categories where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the leave categories from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of leave categories where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching leave categories
	* @throws SystemException if a system exception occurred
	*/
	public int countByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of leave categories.
	*
	* @return the number of leave categories
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
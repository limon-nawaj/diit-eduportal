/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.LeaveCategory;

import java.util.List;

/**
 * The persistence utility for the leave category service. This utility wraps {@link LeaveCategoryPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see LeaveCategoryPersistence
 * @see LeaveCategoryPersistenceImpl
 * @generated
 */
public class LeaveCategoryUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(LeaveCategory leaveCategory) {
		getPersistence().clearCache(leaveCategory);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<LeaveCategory> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<LeaveCategory> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<LeaveCategory> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static LeaveCategory update(LeaveCategory leaveCategory,
		boolean merge) throws SystemException {
		return getPersistence().update(leaveCategory, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static LeaveCategory update(LeaveCategory leaveCategory,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(leaveCategory, merge, serviceContext);
	}

	/**
	* Caches the leave category in the entity cache if it is enabled.
	*
	* @param leaveCategory the leave category
	*/
	public static void cacheResult(
		info.diit.portal.model.LeaveCategory leaveCategory) {
		getPersistence().cacheResult(leaveCategory);
	}

	/**
	* Caches the leave categories in the entity cache if it is enabled.
	*
	* @param leaveCategories the leave categories
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.LeaveCategory> leaveCategories) {
		getPersistence().cacheResult(leaveCategories);
	}

	/**
	* Creates a new leave category with the primary key. Does not add the leave category to the database.
	*
	* @param leaveCategoryId the primary key for the new leave category
	* @return the new leave category
	*/
	public static info.diit.portal.model.LeaveCategory create(
		long leaveCategoryId) {
		return getPersistence().create(leaveCategoryId);
	}

	/**
	* Removes the leave category with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param leaveCategoryId the primary key of the leave category
	* @return the leave category that was removed
	* @throws info.diit.portal.NoSuchLeaveCategoryException if a leave category with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveCategory remove(
		long leaveCategoryId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveCategoryException {
		return getPersistence().remove(leaveCategoryId);
	}

	public static info.diit.portal.model.LeaveCategory updateImpl(
		info.diit.portal.model.LeaveCategory leaveCategory, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(leaveCategory, merge);
	}

	/**
	* Returns the leave category with the primary key or throws a {@link info.diit.portal.NoSuchLeaveCategoryException} if it could not be found.
	*
	* @param leaveCategoryId the primary key of the leave category
	* @return the leave category
	* @throws info.diit.portal.NoSuchLeaveCategoryException if a leave category with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveCategory findByPrimaryKey(
		long leaveCategoryId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveCategoryException {
		return getPersistence().findByPrimaryKey(leaveCategoryId);
	}

	/**
	* Returns the leave category with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param leaveCategoryId the primary key of the leave category
	* @return the leave category, or <code>null</code> if a leave category with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveCategory fetchByPrimaryKey(
		long leaveCategoryId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(leaveCategoryId);
	}

	/**
	* Returns all the leave categories where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching leave categories
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveCategory> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganization(organizationId);
	}

	/**
	* Returns a range of all the leave categories where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of leave categories
	* @param end the upper bound of the range of leave categories (not inclusive)
	* @return the range of matching leave categories
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveCategory> findByOrganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganization(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the leave categories where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of leave categories
	* @param end the upper bound of the range of leave categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching leave categories
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveCategory> findByOrganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByOrganization(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first leave category in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave category
	* @throws info.diit.portal.NoSuchLeaveCategoryException if a matching leave category could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveCategory findByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveCategoryException {
		return getPersistence()
				   .findByOrganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first leave category in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave category, or <code>null</code> if a matching leave category could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveCategory fetchByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the last leave category in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave category
	* @throws info.diit.portal.NoSuchLeaveCategoryException if a matching leave category could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveCategory findByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveCategoryException {
		return getPersistence()
				   .findByOrganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last leave category in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave category, or <code>null</code> if a matching leave category could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveCategory fetchByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the leave categories before and after the current leave category in the ordered set where organizationId = &#63;.
	*
	* @param leaveCategoryId the primary key of the current leave category
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next leave category
	* @throws info.diit.portal.NoSuchLeaveCategoryException if a leave category with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveCategory[] findByOrganization_PrevAndNext(
		long leaveCategoryId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveCategoryException {
		return getPersistence()
				   .findByOrganization_PrevAndNext(leaveCategoryId,
			organizationId, orderByComparator);
	}

	/**
	* Returns all the leave categories.
	*
	* @return the leave categories
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveCategory> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the leave categories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leave categories
	* @param end the upper bound of the range of leave categories (not inclusive)
	* @return the range of leave categories
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveCategory> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the leave categories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leave categories
	* @param end the upper bound of the range of leave categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of leave categories
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveCategory> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the leave categories where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByOrganization(organizationId);
	}

	/**
	* Removes all the leave categories from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of leave categories where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching leave categories
	* @throws SystemException if a system exception occurred
	*/
	public static int countByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByOrganization(organizationId);
	}

	/**
	* Returns the number of leave categories.
	*
	* @return the number of leave categories
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static LeaveCategoryPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (LeaveCategoryPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					LeaveCategoryPersistence.class.getName());

			ReferenceRegistry.registerReference(LeaveCategoryUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(LeaveCategoryPersistence persistence) {
	}

	private static LeaveCategoryPersistence _persistence;
}
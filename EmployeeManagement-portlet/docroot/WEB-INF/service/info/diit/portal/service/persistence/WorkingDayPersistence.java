/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.WorkingDay;

/**
 * The persistence interface for the working day service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see WorkingDayPersistenceImpl
 * @see WorkingDayUtil
 * @generated
 */
public interface WorkingDayPersistence extends BasePersistence<WorkingDay> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link WorkingDayUtil} to access the working day persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the working day in the entity cache if it is enabled.
	*
	* @param workingDay the working day
	*/
	public void cacheResult(info.diit.portal.model.WorkingDay workingDay);

	/**
	* Caches the working daies in the entity cache if it is enabled.
	*
	* @param workingDaies the working daies
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.WorkingDay> workingDaies);

	/**
	* Creates a new working day with the primary key. Does not add the working day to the database.
	*
	* @param workingDayId the primary key for the new working day
	* @return the new working day
	*/
	public info.diit.portal.model.WorkingDay create(long workingDayId);

	/**
	* Removes the working day with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param workingDayId the primary key of the working day
	* @return the working day that was removed
	* @throws info.diit.portal.NoSuchWorkingDayException if a working day with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.WorkingDay remove(long workingDayId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchWorkingDayException;

	public info.diit.portal.model.WorkingDay updateImpl(
		info.diit.portal.model.WorkingDay workingDay, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the working day with the primary key or throws a {@link info.diit.portal.NoSuchWorkingDayException} if it could not be found.
	*
	* @param workingDayId the primary key of the working day
	* @return the working day
	* @throws info.diit.portal.NoSuchWorkingDayException if a working day with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.WorkingDay findByPrimaryKey(long workingDayId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchWorkingDayException;

	/**
	* Returns the working day with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param workingDayId the primary key of the working day
	* @return the working day, or <code>null</code> if a working day with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.WorkingDay fetchByPrimaryKey(
		long workingDayId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the working daies where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching working daies
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.WorkingDay> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the working daies where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of working daies
	* @param end the upper bound of the range of working daies (not inclusive)
	* @return the range of matching working daies
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.WorkingDay> findByOrganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the working daies where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of working daies
	* @param end the upper bound of the range of working daies (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching working daies
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.WorkingDay> findByOrganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first working day in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching working day
	* @throws info.diit.portal.NoSuchWorkingDayException if a matching working day could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.WorkingDay findByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchWorkingDayException;

	/**
	* Returns the first working day in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching working day, or <code>null</code> if a matching working day could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.WorkingDay fetchByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last working day in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching working day
	* @throws info.diit.portal.NoSuchWorkingDayException if a matching working day could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.WorkingDay findByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchWorkingDayException;

	/**
	* Returns the last working day in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching working day, or <code>null</code> if a matching working day could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.WorkingDay fetchByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the working daies before and after the current working day in the ordered set where organizationId = &#63;.
	*
	* @param workingDayId the primary key of the current working day
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next working day
	* @throws info.diit.portal.NoSuchWorkingDayException if a working day with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.WorkingDay[] findByOrganization_PrevAndNext(
		long workingDayId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchWorkingDayException;

	/**
	* Returns the working day where organizationId = &#63; and date = &#63; or throws a {@link info.diit.portal.NoSuchWorkingDayException} if it could not be found.
	*
	* @param organizationId the organization ID
	* @param date the date
	* @return the matching working day
	* @throws info.diit.portal.NoSuchWorkingDayException if a matching working day could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.WorkingDay findByOrganizationDate(
		long organizationId, java.util.Date date)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchWorkingDayException;

	/**
	* Returns the working day where organizationId = &#63; and date = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param organizationId the organization ID
	* @param date the date
	* @return the matching working day, or <code>null</code> if a matching working day could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.WorkingDay fetchByOrganizationDate(
		long organizationId, java.util.Date date)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the working day where organizationId = &#63; and date = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param organizationId the organization ID
	* @param date the date
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching working day, or <code>null</code> if a matching working day could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.WorkingDay fetchByOrganizationDate(
		long organizationId, java.util.Date date, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the working daies.
	*
	* @return the working daies
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.WorkingDay> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the working daies.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of working daies
	* @param end the upper bound of the range of working daies (not inclusive)
	* @return the range of working daies
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.WorkingDay> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the working daies.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of working daies
	* @param end the upper bound of the range of working daies (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of working daies
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.WorkingDay> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the working daies where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the working day where organizationId = &#63; and date = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param date the date
	* @return the working day that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.WorkingDay removeByOrganizationDate(
		long organizationId, java.util.Date date)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchWorkingDayException;

	/**
	* Removes all the working daies from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of working daies where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching working daies
	* @throws SystemException if a system exception occurred
	*/
	public int countByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of working daies where organizationId = &#63; and date = &#63;.
	*
	* @param organizationId the organization ID
	* @param date the date
	* @return the number of matching working daies
	* @throws SystemException if a system exception occurred
	*/
	public int countByOrganizationDate(long organizationId, java.util.Date date)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of working daies.
	*
	* @return the number of working daies
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
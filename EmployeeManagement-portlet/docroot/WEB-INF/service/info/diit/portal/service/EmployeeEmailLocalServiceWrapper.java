/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link EmployeeEmailLocalService}.
 * </p>
 *
 * @author    limon
 * @see       EmployeeEmailLocalService
 * @generated
 */
public class EmployeeEmailLocalServiceWrapper
	implements EmployeeEmailLocalService,
		ServiceWrapper<EmployeeEmailLocalService> {
	public EmployeeEmailLocalServiceWrapper(
		EmployeeEmailLocalService employeeEmailLocalService) {
		_employeeEmailLocalService = employeeEmailLocalService;
	}

	/**
	* Adds the employee email to the database. Also notifies the appropriate model listeners.
	*
	* @param employeeEmail the employee email
	* @return the employee email that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeEmail addEmployeeEmail(
		info.diit.portal.model.EmployeeEmail employeeEmail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.addEmployeeEmail(employeeEmail);
	}

	/**
	* Creates a new employee email with the primary key. Does not add the employee email to the database.
	*
	* @param emailId the primary key for the new employee email
	* @return the new employee email
	*/
	public info.diit.portal.model.EmployeeEmail createEmployeeEmail(
		long emailId) {
		return _employeeEmailLocalService.createEmployeeEmail(emailId);
	}

	/**
	* Deletes the employee email with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param emailId the primary key of the employee email
	* @return the employee email that was removed
	* @throws PortalException if a employee email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeEmail deleteEmployeeEmail(
		long emailId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.deleteEmployeeEmail(emailId);
	}

	/**
	* Deletes the employee email from the database. Also notifies the appropriate model listeners.
	*
	* @param employeeEmail the employee email
	* @return the employee email that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeEmail deleteEmployeeEmail(
		info.diit.portal.model.EmployeeEmail employeeEmail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.deleteEmployeeEmail(employeeEmail);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _employeeEmailLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.EmployeeEmail fetchEmployeeEmail(long emailId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.fetchEmployeeEmail(emailId);
	}

	/**
	* Returns the employee email with the primary key.
	*
	* @param emailId the primary key of the employee email
	* @return the employee email
	* @throws PortalException if a employee email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeEmail getEmployeeEmail(long emailId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.getEmployeeEmail(emailId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the employee emails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee emails
	* @param end the upper bound of the range of employee emails (not inclusive)
	* @return the range of employee emails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeEmail> getEmployeeEmails(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.getEmployeeEmails(start, end);
	}

	/**
	* Returns the number of employee emails.
	*
	* @return the number of employee emails
	* @throws SystemException if a system exception occurred
	*/
	public int getEmployeeEmailsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.getEmployeeEmailsCount();
	}

	/**
	* Updates the employee email in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param employeeEmail the employee email
	* @return the employee email that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeEmail updateEmployeeEmail(
		info.diit.portal.model.EmployeeEmail employeeEmail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.updateEmployeeEmail(employeeEmail);
	}

	/**
	* Updates the employee email in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param employeeEmail the employee email
	* @param merge whether to merge the employee email with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the employee email that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeEmail updateEmployeeEmail(
		info.diit.portal.model.EmployeeEmail employeeEmail, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.updateEmployeeEmail(employeeEmail,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _employeeEmailLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_employeeEmailLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _employeeEmailLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.model.EmployeeEmail> findByEmployee(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmailLocalService.findByEmployee(employeeId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public EmployeeEmailLocalService getWrappedEmployeeEmailLocalService() {
		return _employeeEmailLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedEmployeeEmailLocalService(
		EmployeeEmailLocalService employeeEmailLocalService) {
		_employeeEmailLocalService = employeeEmailLocalService;
	}

	public EmployeeEmailLocalService getWrappedService() {
		return _employeeEmailLocalService;
	}

	public void setWrappedService(
		EmployeeEmailLocalService employeeEmailLocalService) {
		_employeeEmailLocalService = employeeEmailLocalService;
	}

	private EmployeeEmailLocalService _employeeEmailLocalService;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.AttendanceCorrection;

/**
 * The persistence interface for the attendance correction service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see AttendanceCorrectionPersistenceImpl
 * @see AttendanceCorrectionUtil
 * @generated
 */
public interface AttendanceCorrectionPersistence extends BasePersistence<AttendanceCorrection> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AttendanceCorrectionUtil} to access the attendance correction persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the attendance correction in the entity cache if it is enabled.
	*
	* @param attendanceCorrection the attendance correction
	*/
	public void cacheResult(
		info.diit.portal.model.AttendanceCorrection attendanceCorrection);

	/**
	* Caches the attendance corrections in the entity cache if it is enabled.
	*
	* @param attendanceCorrections the attendance corrections
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.AttendanceCorrection> attendanceCorrections);

	/**
	* Creates a new attendance correction with the primary key. Does not add the attendance correction to the database.
	*
	* @param attendanceCorrectionId the primary key for the new attendance correction
	* @return the new attendance correction
	*/
	public info.diit.portal.model.AttendanceCorrection create(
		long attendanceCorrectionId);

	/**
	* Removes the attendance correction with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceCorrectionId the primary key of the attendance correction
	* @return the attendance correction that was removed
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a attendance correction with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection remove(
		long attendanceCorrectionId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException;

	public info.diit.portal.model.AttendanceCorrection updateImpl(
		info.diit.portal.model.AttendanceCorrection attendanceCorrection,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the attendance correction with the primary key or throws a {@link info.diit.portal.NoSuchAttendanceCorrectionException} if it could not be found.
	*
	* @param attendanceCorrectionId the primary key of the attendance correction
	* @return the attendance correction
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a attendance correction with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection findByPrimaryKey(
		long attendanceCorrectionId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException;

	/**
	* Returns the attendance correction with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param attendanceCorrectionId the primary key of the attendance correction
	* @return the attendance correction, or <code>null</code> if a attendance correction with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection fetchByPrimaryKey(
		long attendanceCorrectionId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the attendance corrections where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AttendanceCorrection> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the attendance corrections where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of attendance corrections
	* @param end the upper bound of the range of attendance corrections (not inclusive)
	* @return the range of matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AttendanceCorrection> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the attendance corrections where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of attendance corrections
	* @param end the upper bound of the range of attendance corrections (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AttendanceCorrection> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first attendance correction in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance correction
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException;

	/**
	* Returns the first attendance correction in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance correction, or <code>null</code> if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last attendance correction in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance correction
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException;

	/**
	* Returns the last attendance correction in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance correction, or <code>null</code> if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the attendance corrections before and after the current attendance correction in the ordered set where companyId = &#63;.
	*
	* @param attendanceCorrectionId the primary key of the current attendance correction
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next attendance correction
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a attendance correction with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection[] findByCompany_PrevAndNext(
		long attendanceCorrectionId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException;

	/**
	* Returns all the attendance corrections where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AttendanceCorrection> findByEmployee(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the attendance corrections where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of attendance corrections
	* @param end the upper bound of the range of attendance corrections (not inclusive)
	* @return the range of matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AttendanceCorrection> findByEmployee(
		long employeeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the attendance corrections where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of attendance corrections
	* @param end the upper bound of the range of attendance corrections (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AttendanceCorrection> findByEmployee(
		long employeeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first attendance correction in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance correction
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection findByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException;

	/**
	* Returns the first attendance correction in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance correction, or <code>null</code> if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection fetchByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last attendance correction in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance correction
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection findByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException;

	/**
	* Returns the last attendance correction in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance correction, or <code>null</code> if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection fetchByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the attendance corrections before and after the current attendance correction in the ordered set where employeeId = &#63;.
	*
	* @param attendanceCorrectionId the primary key of the current attendance correction
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next attendance correction
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a attendance correction with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AttendanceCorrection[] findByEmployee_PrevAndNext(
		long attendanceCorrectionId, long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException;

	/**
	* Returns all the attendance corrections.
	*
	* @return the attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AttendanceCorrection> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the attendance corrections.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attendance corrections
	* @param end the upper bound of the range of attendance corrections (not inclusive)
	* @return the range of attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AttendanceCorrection> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the attendance corrections.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attendance corrections
	* @param end the upper bound of the range of attendance corrections (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AttendanceCorrection> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the attendance corrections where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the attendance corrections where employeeId = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the attendance corrections from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of attendance corrections where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of attendance corrections where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the number of matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public int countByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of attendance corrections.
	*
	* @return the number of attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
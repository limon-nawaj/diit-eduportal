/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.DayType;

/**
 * The persistence interface for the day type service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see DayTypePersistenceImpl
 * @see DayTypeUtil
 * @generated
 */
public interface DayTypePersistence extends BasePersistence<DayType> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DayTypeUtil} to access the day type persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the day type in the entity cache if it is enabled.
	*
	* @param dayType the day type
	*/
	public void cacheResult(info.diit.portal.model.DayType dayType);

	/**
	* Caches the day types in the entity cache if it is enabled.
	*
	* @param dayTypes the day types
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.DayType> dayTypes);

	/**
	* Creates a new day type with the primary key. Does not add the day type to the database.
	*
	* @param dayTypeId the primary key for the new day type
	* @return the new day type
	*/
	public info.diit.portal.model.DayType create(long dayTypeId);

	/**
	* Removes the day type with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dayTypeId the primary key of the day type
	* @return the day type that was removed
	* @throws info.diit.portal.NoSuchDayTypeException if a day type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType remove(long dayTypeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException;

	public info.diit.portal.model.DayType updateImpl(
		info.diit.portal.model.DayType dayType, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the day type with the primary key or throws a {@link info.diit.portal.NoSuchDayTypeException} if it could not be found.
	*
	* @param dayTypeId the primary key of the day type
	* @return the day type
	* @throws info.diit.portal.NoSuchDayTypeException if a day type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType findByPrimaryKey(long dayTypeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException;

	/**
	* Returns the day type with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param dayTypeId the primary key of the day type
	* @return the day type, or <code>null</code> if a day type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType fetchByPrimaryKey(long dayTypeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the day types where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching day types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.DayType> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the day types where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of day types
	* @param end the upper bound of the range of day types (not inclusive)
	* @return the range of matching day types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.DayType> findByOrganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the day types where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of day types
	* @param end the upper bound of the range of day types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching day types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.DayType> findByOrganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first day type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching day type
	* @throws info.diit.portal.NoSuchDayTypeException if a matching day type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType findByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException;

	/**
	* Returns the first day type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching day type, or <code>null</code> if a matching day type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType fetchByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last day type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching day type
	* @throws info.diit.portal.NoSuchDayTypeException if a matching day type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType findByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException;

	/**
	* Returns the last day type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching day type, or <code>null</code> if a matching day type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType fetchByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the day types before and after the current day type in the ordered set where organizationId = &#63;.
	*
	* @param dayTypeId the primary key of the current day type
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next day type
	* @throws info.diit.portal.NoSuchDayTypeException if a day type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType[] findByOrganization_PrevAndNext(
		long dayTypeId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException;

	/**
	* Returns the day type where type = &#63; and organizationId = &#63; or throws a {@link info.diit.portal.NoSuchDayTypeException} if it could not be found.
	*
	* @param type the type
	* @param organizationId the organization ID
	* @return the matching day type
	* @throws info.diit.portal.NoSuchDayTypeException if a matching day type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType findByTypeOrganization(
		java.lang.String type, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException;

	/**
	* Returns the day type where type = &#63; and organizationId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param type the type
	* @param organizationId the organization ID
	* @return the matching day type, or <code>null</code> if a matching day type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType fetchByTypeOrganization(
		java.lang.String type, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the day type where type = &#63; and organizationId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param type the type
	* @param organizationId the organization ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching day type, or <code>null</code> if a matching day type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType fetchByTypeOrganization(
		java.lang.String type, long organizationId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the day types.
	*
	* @return the day types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.DayType> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the day types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of day types
	* @param end the upper bound of the range of day types (not inclusive)
	* @return the range of day types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.DayType> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the day types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of day types
	* @param end the upper bound of the range of day types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of day types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.DayType> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the day types where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the day type where type = &#63; and organizationId = &#63; from the database.
	*
	* @param type the type
	* @param organizationId the organization ID
	* @return the day type that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType removeByTypeOrganization(
		java.lang.String type, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException;

	/**
	* Removes all the day types from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of day types where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching day types
	* @throws SystemException if a system exception occurred
	*/
	public int countByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of day types where type = &#63; and organizationId = &#63;.
	*
	* @param type the type
	* @param organizationId the organization ID
	* @return the number of matching day types
	* @throws SystemException if a system exception occurred
	*/
	public int countByTypeOrganization(java.lang.String type,
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of day types.
	*
	* @return the number of day types
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
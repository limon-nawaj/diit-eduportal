/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.Leave;

/**
 * The persistence interface for the leave service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see LeavePersistenceImpl
 * @see LeaveUtil
 * @generated
 */
public interface LeavePersistence extends BasePersistence<Leave> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LeaveUtil} to access the leave persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the leave in the entity cache if it is enabled.
	*
	* @param leave the leave
	*/
	public void cacheResult(info.diit.portal.model.Leave leave);

	/**
	* Caches the leaves in the entity cache if it is enabled.
	*
	* @param leaves the leaves
	*/
	public void cacheResult(java.util.List<info.diit.portal.model.Leave> leaves);

	/**
	* Creates a new leave with the primary key. Does not add the leave to the database.
	*
	* @param leaveId the primary key for the new leave
	* @return the new leave
	*/
	public info.diit.portal.model.Leave create(long leaveId);

	/**
	* Removes the leave with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param leaveId the primary key of the leave
	* @return the leave that was removed
	* @throws info.diit.portal.NoSuchLeaveException if a leave with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave remove(long leaveId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveException;

	public info.diit.portal.model.Leave updateImpl(
		info.diit.portal.model.Leave leave, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the leave with the primary key or throws a {@link info.diit.portal.NoSuchLeaveException} if it could not be found.
	*
	* @param leaveId the primary key of the leave
	* @return the leave
	* @throws info.diit.portal.NoSuchLeaveException if a leave with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave findByPrimaryKey(long leaveId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveException;

	/**
	* Returns the leave with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param leaveId the primary key of the leave
	* @return the leave, or <code>null</code> if a leave with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave fetchByPrimaryKey(long leaveId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the leaves where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching leaves
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Leave> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the leaves where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of leaves
	* @param end the upper bound of the range of leaves (not inclusive)
	* @return the range of matching leaves
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Leave> findByOrganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the leaves where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of leaves
	* @param end the upper bound of the range of leaves (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching leaves
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Leave> findByOrganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first leave in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave
	* @throws info.diit.portal.NoSuchLeaveException if a matching leave could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave findByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveException;

	/**
	* Returns the first leave in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave, or <code>null</code> if a matching leave could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave fetchByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last leave in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave
	* @throws info.diit.portal.NoSuchLeaveException if a matching leave could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave findByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveException;

	/**
	* Returns the last leave in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave, or <code>null</code> if a matching leave could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave fetchByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the leaves before and after the current leave in the ordered set where organizationId = &#63;.
	*
	* @param leaveId the primary key of the current leave
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next leave
	* @throws info.diit.portal.NoSuchLeaveException if a leave with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave[] findByOrganization_PrevAndNext(
		long leaveId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveException;

	/**
	* Returns all the leaves where employee = &#63;.
	*
	* @param employee the employee
	* @return the matching leaves
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Leave> findByEmployee(
		long employee)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the leaves where employee = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employee the employee
	* @param start the lower bound of the range of leaves
	* @param end the upper bound of the range of leaves (not inclusive)
	* @return the range of matching leaves
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Leave> findByEmployee(
		long employee, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the leaves where employee = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employee the employee
	* @param start the lower bound of the range of leaves
	* @param end the upper bound of the range of leaves (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching leaves
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Leave> findByEmployee(
		long employee, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first leave in the ordered set where employee = &#63;.
	*
	* @param employee the employee
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave
	* @throws info.diit.portal.NoSuchLeaveException if a matching leave could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave findByEmployee_First(long employee,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveException;

	/**
	* Returns the first leave in the ordered set where employee = &#63;.
	*
	* @param employee the employee
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave, or <code>null</code> if a matching leave could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave fetchByEmployee_First(long employee,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last leave in the ordered set where employee = &#63;.
	*
	* @param employee the employee
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave
	* @throws info.diit.portal.NoSuchLeaveException if a matching leave could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave findByEmployee_Last(long employee,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveException;

	/**
	* Returns the last leave in the ordered set where employee = &#63;.
	*
	* @param employee the employee
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave, or <code>null</code> if a matching leave could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave fetchByEmployee_Last(long employee,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the leaves before and after the current leave in the ordered set where employee = &#63;.
	*
	* @param leaveId the primary key of the current leave
	* @param employee the employee
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next leave
	* @throws info.diit.portal.NoSuchLeaveException if a leave with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave[] findByEmployee_PrevAndNext(
		long leaveId, long employee,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveException;

	/**
	* Returns all the leaves where responsibleEmployee = &#63;.
	*
	* @param responsibleEmployee the responsible employee
	* @return the matching leaves
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Leave> findByResponsibleEmployee(
		long responsibleEmployee)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the leaves where responsibleEmployee = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param responsibleEmployee the responsible employee
	* @param start the lower bound of the range of leaves
	* @param end the upper bound of the range of leaves (not inclusive)
	* @return the range of matching leaves
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Leave> findByResponsibleEmployee(
		long responsibleEmployee, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the leaves where responsibleEmployee = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param responsibleEmployee the responsible employee
	* @param start the lower bound of the range of leaves
	* @param end the upper bound of the range of leaves (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching leaves
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Leave> findByResponsibleEmployee(
		long responsibleEmployee, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first leave in the ordered set where responsibleEmployee = &#63;.
	*
	* @param responsibleEmployee the responsible employee
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave
	* @throws info.diit.portal.NoSuchLeaveException if a matching leave could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave findByResponsibleEmployee_First(
		long responsibleEmployee,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveException;

	/**
	* Returns the first leave in the ordered set where responsibleEmployee = &#63;.
	*
	* @param responsibleEmployee the responsible employee
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave, or <code>null</code> if a matching leave could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave fetchByResponsibleEmployee_First(
		long responsibleEmployee,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last leave in the ordered set where responsibleEmployee = &#63;.
	*
	* @param responsibleEmployee the responsible employee
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave
	* @throws info.diit.portal.NoSuchLeaveException if a matching leave could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave findByResponsibleEmployee_Last(
		long responsibleEmployee,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveException;

	/**
	* Returns the last leave in the ordered set where responsibleEmployee = &#63;.
	*
	* @param responsibleEmployee the responsible employee
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave, or <code>null</code> if a matching leave could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave fetchByResponsibleEmployee_Last(
		long responsibleEmployee,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the leaves before and after the current leave in the ordered set where responsibleEmployee = &#63;.
	*
	* @param leaveId the primary key of the current leave
	* @param responsibleEmployee the responsible employee
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next leave
	* @throws info.diit.portal.NoSuchLeaveException if a leave with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave[] findByResponsibleEmployee_PrevAndNext(
		long leaveId, long responsibleEmployee,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveException;

	/**
	* Returns all the leaves.
	*
	* @return the leaves
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Leave> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the leaves.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leaves
	* @param end the upper bound of the range of leaves (not inclusive)
	* @return the range of leaves
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Leave> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the leaves.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leaves
	* @param end the upper bound of the range of leaves (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of leaves
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Leave> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the leaves where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the leaves where employee = &#63; from the database.
	*
	* @param employee the employee
	* @throws SystemException if a system exception occurred
	*/
	public void removeByEmployee(long employee)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the leaves where responsibleEmployee = &#63; from the database.
	*
	* @param responsibleEmployee the responsible employee
	* @throws SystemException if a system exception occurred
	*/
	public void removeByResponsibleEmployee(long responsibleEmployee)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the leaves from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of leaves where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching leaves
	* @throws SystemException if a system exception occurred
	*/
	public int countByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of leaves where employee = &#63;.
	*
	* @param employee the employee
	* @return the number of matching leaves
	* @throws SystemException if a system exception occurred
	*/
	public int countByEmployee(long employee)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of leaves where responsibleEmployee = &#63;.
	*
	* @param responsibleEmployee the responsible employee
	* @return the number of matching leaves
	* @throws SystemException if a system exception occurred
	*/
	public int countByResponsibleEmployee(long responsibleEmployee)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of leaves.
	*
	* @return the number of leaves
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link LeaveLocalService}.
 * </p>
 *
 * @author    limon
 * @see       LeaveLocalService
 * @generated
 */
public class LeaveLocalServiceWrapper implements LeaveLocalService,
	ServiceWrapper<LeaveLocalService> {
	public LeaveLocalServiceWrapper(LeaveLocalService leaveLocalService) {
		_leaveLocalService = leaveLocalService;
	}

	/**
	* Adds the leave to the database. Also notifies the appropriate model listeners.
	*
	* @param leave the leave
	* @return the leave that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave addLeave(
		info.diit.portal.model.Leave leave)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.addLeave(leave);
	}

	/**
	* Creates a new leave with the primary key. Does not add the leave to the database.
	*
	* @param leaveId the primary key for the new leave
	* @return the new leave
	*/
	public info.diit.portal.model.Leave createLeave(long leaveId) {
		return _leaveLocalService.createLeave(leaveId);
	}

	/**
	* Deletes the leave with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param leaveId the primary key of the leave
	* @return the leave that was removed
	* @throws PortalException if a leave with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave deleteLeave(long leaveId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.deleteLeave(leaveId);
	}

	/**
	* Deletes the leave from the database. Also notifies the appropriate model listeners.
	*
	* @param leave the leave
	* @return the leave that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave deleteLeave(
		info.diit.portal.model.Leave leave)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.deleteLeave(leave);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _leaveLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.Leave fetchLeave(long leaveId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.fetchLeave(leaveId);
	}

	/**
	* Returns the leave with the primary key.
	*
	* @param leaveId the primary key of the leave
	* @return the leave
	* @throws PortalException if a leave with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave getLeave(long leaveId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.getLeave(leaveId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the leaves.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leaves
	* @param end the upper bound of the range of leaves (not inclusive)
	* @return the range of leaves
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Leave> getLeaves(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.getLeaves(start, end);
	}

	/**
	* Returns the number of leaves.
	*
	* @return the number of leaves
	* @throws SystemException if a system exception occurred
	*/
	public int getLeavesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.getLeavesCount();
	}

	/**
	* Updates the leave in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param leave the leave
	* @return the leave that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave updateLeave(
		info.diit.portal.model.Leave leave)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.updateLeave(leave);
	}

	/**
	* Updates the leave in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param leave the leave
	* @param merge whether to merge the leave with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the leave that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Leave updateLeave(
		info.diit.portal.model.Leave leave, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.updateLeave(leave, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _leaveLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_leaveLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _leaveLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	public java.util.List<info.diit.portal.model.Leave> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.findByOrganization(organizationId);
	}

	public java.util.List<info.diit.portal.model.Leave> findByEmployee(
		long employee)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.findByEmployee(employee);
	}

	public java.util.List<info.diit.portal.model.Leave> findByResponsiblePerson(
		long employee)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveLocalService.findByResponsiblePerson(employee);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public LeaveLocalService getWrappedLeaveLocalService() {
		return _leaveLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedLeaveLocalService(LeaveLocalService leaveLocalService) {
		_leaveLocalService = leaveLocalService;
	}

	public LeaveLocalService getWrappedService() {
		return _leaveLocalService;
	}

	public void setWrappedService(LeaveLocalService leaveLocalService) {
		_leaveLocalService = leaveLocalService;
	}

	private LeaveLocalService _leaveLocalService;
}
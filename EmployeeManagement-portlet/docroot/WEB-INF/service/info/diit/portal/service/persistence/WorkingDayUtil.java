/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.WorkingDay;

import java.util.List;

/**
 * The persistence utility for the working day service. This utility wraps {@link WorkingDayPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see WorkingDayPersistence
 * @see WorkingDayPersistenceImpl
 * @generated
 */
public class WorkingDayUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(WorkingDay workingDay) {
		getPersistence().clearCache(workingDay);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<WorkingDay> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<WorkingDay> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<WorkingDay> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static WorkingDay update(WorkingDay workingDay, boolean merge)
		throws SystemException {
		return getPersistence().update(workingDay, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static WorkingDay update(WorkingDay workingDay, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(workingDay, merge, serviceContext);
	}

	/**
	* Caches the working day in the entity cache if it is enabled.
	*
	* @param workingDay the working day
	*/
	public static void cacheResult(info.diit.portal.model.WorkingDay workingDay) {
		getPersistence().cacheResult(workingDay);
	}

	/**
	* Caches the working daies in the entity cache if it is enabled.
	*
	* @param workingDaies the working daies
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.WorkingDay> workingDaies) {
		getPersistence().cacheResult(workingDaies);
	}

	/**
	* Creates a new working day with the primary key. Does not add the working day to the database.
	*
	* @param workingDayId the primary key for the new working day
	* @return the new working day
	*/
	public static info.diit.portal.model.WorkingDay create(long workingDayId) {
		return getPersistence().create(workingDayId);
	}

	/**
	* Removes the working day with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param workingDayId the primary key of the working day
	* @return the working day that was removed
	* @throws info.diit.portal.NoSuchWorkingDayException if a working day with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.WorkingDay remove(long workingDayId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchWorkingDayException {
		return getPersistence().remove(workingDayId);
	}

	public static info.diit.portal.model.WorkingDay updateImpl(
		info.diit.portal.model.WorkingDay workingDay, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(workingDay, merge);
	}

	/**
	* Returns the working day with the primary key or throws a {@link info.diit.portal.NoSuchWorkingDayException} if it could not be found.
	*
	* @param workingDayId the primary key of the working day
	* @return the working day
	* @throws info.diit.portal.NoSuchWorkingDayException if a working day with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.WorkingDay findByPrimaryKey(
		long workingDayId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchWorkingDayException {
		return getPersistence().findByPrimaryKey(workingDayId);
	}

	/**
	* Returns the working day with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param workingDayId the primary key of the working day
	* @return the working day, or <code>null</code> if a working day with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.WorkingDay fetchByPrimaryKey(
		long workingDayId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(workingDayId);
	}

	/**
	* Returns all the working daies where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching working daies
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.WorkingDay> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganization(organizationId);
	}

	/**
	* Returns a range of all the working daies where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of working daies
	* @param end the upper bound of the range of working daies (not inclusive)
	* @return the range of matching working daies
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.WorkingDay> findByOrganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganization(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the working daies where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of working daies
	* @param end the upper bound of the range of working daies (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching working daies
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.WorkingDay> findByOrganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByOrganization(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first working day in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching working day
	* @throws info.diit.portal.NoSuchWorkingDayException if a matching working day could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.WorkingDay findByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchWorkingDayException {
		return getPersistence()
				   .findByOrganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first working day in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching working day, or <code>null</code> if a matching working day could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.WorkingDay fetchByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the last working day in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching working day
	* @throws info.diit.portal.NoSuchWorkingDayException if a matching working day could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.WorkingDay findByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchWorkingDayException {
		return getPersistence()
				   .findByOrganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last working day in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching working day, or <code>null</code> if a matching working day could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.WorkingDay fetchByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the working daies before and after the current working day in the ordered set where organizationId = &#63;.
	*
	* @param workingDayId the primary key of the current working day
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next working day
	* @throws info.diit.portal.NoSuchWorkingDayException if a working day with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.WorkingDay[] findByOrganization_PrevAndNext(
		long workingDayId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchWorkingDayException {
		return getPersistence()
				   .findByOrganization_PrevAndNext(workingDayId,
			organizationId, orderByComparator);
	}

	/**
	* Returns the working day where organizationId = &#63; and date = &#63; or throws a {@link info.diit.portal.NoSuchWorkingDayException} if it could not be found.
	*
	* @param organizationId the organization ID
	* @param date the date
	* @return the matching working day
	* @throws info.diit.portal.NoSuchWorkingDayException if a matching working day could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.WorkingDay findByOrganizationDate(
		long organizationId, java.util.Date date)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchWorkingDayException {
		return getPersistence().findByOrganizationDate(organizationId, date);
	}

	/**
	* Returns the working day where organizationId = &#63; and date = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param organizationId the organization ID
	* @param date the date
	* @return the matching working day, or <code>null</code> if a matching working day could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.WorkingDay fetchByOrganizationDate(
		long organizationId, java.util.Date date)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByOrganizationDate(organizationId, date);
	}

	/**
	* Returns the working day where organizationId = &#63; and date = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param organizationId the organization ID
	* @param date the date
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching working day, or <code>null</code> if a matching working day could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.WorkingDay fetchByOrganizationDate(
		long organizationId, java.util.Date date, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganizationDate(organizationId, date,
			retrieveFromCache);
	}

	/**
	* Returns all the working daies.
	*
	* @return the working daies
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.WorkingDay> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the working daies.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of working daies
	* @param end the upper bound of the range of working daies (not inclusive)
	* @return the range of working daies
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.WorkingDay> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the working daies.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of working daies
	* @param end the upper bound of the range of working daies (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of working daies
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.WorkingDay> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the working daies where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByOrganization(organizationId);
	}

	/**
	* Removes the working day where organizationId = &#63; and date = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param date the date
	* @return the working day that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.WorkingDay removeByOrganizationDate(
		long organizationId, java.util.Date date)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchWorkingDayException {
		return getPersistence().removeByOrganizationDate(organizationId, date);
	}

	/**
	* Removes all the working daies from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of working daies where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching working daies
	* @throws SystemException if a system exception occurred
	*/
	public static int countByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByOrganization(organizationId);
	}

	/**
	* Returns the number of working daies where organizationId = &#63; and date = &#63;.
	*
	* @param organizationId the organization ID
	* @param date the date
	* @return the number of matching working daies
	* @throws SystemException if a system exception occurred
	*/
	public static int countByOrganizationDate(long organizationId,
		java.util.Date date)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByOrganizationDate(organizationId, date);
	}

	/**
	* Returns the number of working daies.
	*
	* @return the number of working daies
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static WorkingDayPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (WorkingDayPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					WorkingDayPersistence.class.getName());

			ReferenceRegistry.registerReference(WorkingDayUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(WorkingDayPersistence persistence) {
	}

	private static WorkingDayPersistence _persistence;
}
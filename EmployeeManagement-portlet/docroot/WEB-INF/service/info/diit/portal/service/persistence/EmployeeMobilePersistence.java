/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.EmployeeMobile;

/**
 * The persistence interface for the employee mobile service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see EmployeeMobilePersistenceImpl
 * @see EmployeeMobileUtil
 * @generated
 */
public interface EmployeeMobilePersistence extends BasePersistence<EmployeeMobile> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EmployeeMobileUtil} to access the employee mobile persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the employee mobile in the entity cache if it is enabled.
	*
	* @param employeeMobile the employee mobile
	*/
	public void cacheResult(
		info.diit.portal.model.EmployeeMobile employeeMobile);

	/**
	* Caches the employee mobiles in the entity cache if it is enabled.
	*
	* @param employeeMobiles the employee mobiles
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.EmployeeMobile> employeeMobiles);

	/**
	* Creates a new employee mobile with the primary key. Does not add the employee mobile to the database.
	*
	* @param mobileId the primary key for the new employee mobile
	* @return the new employee mobile
	*/
	public info.diit.portal.model.EmployeeMobile create(long mobileId);

	/**
	* Removes the employee mobile with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param mobileId the primary key of the employee mobile
	* @return the employee mobile that was removed
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a employee mobile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile remove(long mobileId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException;

	public info.diit.portal.model.EmployeeMobile updateImpl(
		info.diit.portal.model.EmployeeMobile employeeMobile, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the employee mobile with the primary key or throws a {@link info.diit.portal.NoSuchEmployeeMobileException} if it could not be found.
	*
	* @param mobileId the primary key of the employee mobile
	* @return the employee mobile
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a employee mobile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile findByPrimaryKey(long mobileId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException;

	/**
	* Returns the employee mobile with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param mobileId the primary key of the employee mobile
	* @return the employee mobile, or <code>null</code> if a employee mobile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile fetchByPrimaryKey(
		long mobileId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the employee mobiles where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployee(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the employee mobiles where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of employee mobiles
	* @param end the upper bound of the range of employee mobiles (not inclusive)
	* @return the range of matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployee(
		long employeeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the employee mobiles where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of employee mobiles
	* @param end the upper bound of the range of employee mobiles (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployee(
		long employeeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first employee mobile in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee mobile
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile findByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException;

	/**
	* Returns the first employee mobile in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee mobile, or <code>null</code> if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile fetchByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last employee mobile in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee mobile
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile findByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException;

	/**
	* Returns the last employee mobile in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee mobile, or <code>null</code> if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile fetchByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the employee mobiles before and after the current employee mobile in the ordered set where employeeId = &#63;.
	*
	* @param mobileId the primary key of the current employee mobile
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next employee mobile
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a employee mobile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile[] findByEmployee_PrevAndNext(
		long mobileId, long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException;

	/**
	* Returns all the employee mobiles where employeeId = &#63; and status = &#63;.
	*
	* @param employeeId the employee ID
	* @param status the status
	* @return the matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployeeStatus(
		long employeeId, long status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the employee mobiles where employeeId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param status the status
	* @param start the lower bound of the range of employee mobiles
	* @param end the upper bound of the range of employee mobiles (not inclusive)
	* @return the range of matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployeeStatus(
		long employeeId, long status, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the employee mobiles where employeeId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param status the status
	* @param start the lower bound of the range of employee mobiles
	* @param end the upper bound of the range of employee mobiles (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployeeStatus(
		long employeeId, long status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first employee mobile in the ordered set where employeeId = &#63; and status = &#63;.
	*
	* @param employeeId the employee ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee mobile
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile findByEmployeeStatus_First(
		long employeeId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException;

	/**
	* Returns the first employee mobile in the ordered set where employeeId = &#63; and status = &#63;.
	*
	* @param employeeId the employee ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee mobile, or <code>null</code> if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile fetchByEmployeeStatus_First(
		long employeeId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last employee mobile in the ordered set where employeeId = &#63; and status = &#63;.
	*
	* @param employeeId the employee ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee mobile
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile findByEmployeeStatus_Last(
		long employeeId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException;

	/**
	* Returns the last employee mobile in the ordered set where employeeId = &#63; and status = &#63;.
	*
	* @param employeeId the employee ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee mobile, or <code>null</code> if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile fetchByEmployeeStatus_Last(
		long employeeId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the employee mobiles before and after the current employee mobile in the ordered set where employeeId = &#63; and status = &#63;.
	*
	* @param mobileId the primary key of the current employee mobile
	* @param employeeId the employee ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next employee mobile
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a employee mobile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile[] findByEmployeeStatus_PrevAndNext(
		long mobileId, long employeeId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException;

	/**
	* Returns all the employee mobiles.
	*
	* @return the employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeMobile> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the employee mobiles.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee mobiles
	* @param end the upper bound of the range of employee mobiles (not inclusive)
	* @return the range of employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeMobile> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the employee mobiles.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee mobiles
	* @param end the upper bound of the range of employee mobiles (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeMobile> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the employee mobiles where employeeId = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the employee mobiles where employeeId = &#63; and status = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @param status the status
	* @throws SystemException if a system exception occurred
	*/
	public void removeByEmployeeStatus(long employeeId, long status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the employee mobiles from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of employee mobiles where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the number of matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public int countByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of employee mobiles where employeeId = &#63; and status = &#63;.
	*
	* @param employeeId the employee ID
	* @param status the status
	* @return the number of matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public int countByEmployeeStatus(long employeeId, long status)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of employee mobiles.
	*
	* @return the number of employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
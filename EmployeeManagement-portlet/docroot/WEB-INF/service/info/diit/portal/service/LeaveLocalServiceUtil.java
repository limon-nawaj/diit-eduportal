/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the leave local service. This utility wraps {@link info.diit.portal.service.impl.LeaveLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see LeaveLocalService
 * @see info.diit.portal.service.base.LeaveLocalServiceBaseImpl
 * @see info.diit.portal.service.impl.LeaveLocalServiceImpl
 * @generated
 */
public class LeaveLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.service.impl.LeaveLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the leave to the database. Also notifies the appropriate model listeners.
	*
	* @param leave the leave
	* @return the leave that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Leave addLeave(
		info.diit.portal.model.Leave leave)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addLeave(leave);
	}

	/**
	* Creates a new leave with the primary key. Does not add the leave to the database.
	*
	* @param leaveId the primary key for the new leave
	* @return the new leave
	*/
	public static info.diit.portal.model.Leave createLeave(long leaveId) {
		return getService().createLeave(leaveId);
	}

	/**
	* Deletes the leave with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param leaveId the primary key of the leave
	* @return the leave that was removed
	* @throws PortalException if a leave with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Leave deleteLeave(long leaveId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteLeave(leaveId);
	}

	/**
	* Deletes the leave from the database. Also notifies the appropriate model listeners.
	*
	* @param leave the leave
	* @return the leave that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Leave deleteLeave(
		info.diit.portal.model.Leave leave)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteLeave(leave);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.model.Leave fetchLeave(long leaveId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchLeave(leaveId);
	}

	/**
	* Returns the leave with the primary key.
	*
	* @param leaveId the primary key of the leave
	* @return the leave
	* @throws PortalException if a leave with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Leave getLeave(long leaveId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getLeave(leaveId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the leaves.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leaves
	* @param end the upper bound of the range of leaves (not inclusive)
	* @return the range of leaves
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Leave> getLeaves(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLeaves(start, end);
	}

	/**
	* Returns the number of leaves.
	*
	* @return the number of leaves
	* @throws SystemException if a system exception occurred
	*/
	public static int getLeavesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLeavesCount();
	}

	/**
	* Updates the leave in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param leave the leave
	* @return the leave that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Leave updateLeave(
		info.diit.portal.model.Leave leave)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateLeave(leave);
	}

	/**
	* Updates the leave in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param leave the leave
	* @param merge whether to merge the leave with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the leave that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Leave updateLeave(
		info.diit.portal.model.Leave leave, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateLeave(leave, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<info.diit.portal.model.Leave> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByOrganization(organizationId);
	}

	public static java.util.List<info.diit.portal.model.Leave> findByEmployee(
		long employee)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByEmployee(employee);
	}

	public static java.util.List<info.diit.portal.model.Leave> findByResponsiblePerson(
		long employee)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByResponsiblePerson(employee);
	}

	public static void clearService() {
		_service = null;
	}

	public static LeaveLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					LeaveLocalService.class.getName());

			if (invokableLocalService instanceof LeaveLocalService) {
				_service = (LeaveLocalService)invokableLocalService;
			}
			else {
				_service = new LeaveLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(LeaveLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(LeaveLocalService service) {
	}

	private static LeaveLocalService _service;
}
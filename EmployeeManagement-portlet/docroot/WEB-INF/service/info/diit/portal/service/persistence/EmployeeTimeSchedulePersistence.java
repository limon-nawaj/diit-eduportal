/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.EmployeeTimeSchedule;

/**
 * The persistence interface for the employee time schedule service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see EmployeeTimeSchedulePersistenceImpl
 * @see EmployeeTimeScheduleUtil
 * @generated
 */
public interface EmployeeTimeSchedulePersistence extends BasePersistence<EmployeeTimeSchedule> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EmployeeTimeScheduleUtil} to access the employee time schedule persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the employee time schedule in the entity cache if it is enabled.
	*
	* @param employeeTimeSchedule the employee time schedule
	*/
	public void cacheResult(
		info.diit.portal.model.EmployeeTimeSchedule employeeTimeSchedule);

	/**
	* Caches the employee time schedules in the entity cache if it is enabled.
	*
	* @param employeeTimeSchedules the employee time schedules
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.EmployeeTimeSchedule> employeeTimeSchedules);

	/**
	* Creates a new employee time schedule with the primary key. Does not add the employee time schedule to the database.
	*
	* @param employeeTimeScheduleId the primary key for the new employee time schedule
	* @return the new employee time schedule
	*/
	public info.diit.portal.model.EmployeeTimeSchedule create(
		long employeeTimeScheduleId);

	/**
	* Removes the employee time schedule with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param employeeTimeScheduleId the primary key of the employee time schedule
	* @return the employee time schedule that was removed
	* @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a employee time schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule remove(
		long employeeTimeScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException;

	public info.diit.portal.model.EmployeeTimeSchedule updateImpl(
		info.diit.portal.model.EmployeeTimeSchedule employeeTimeSchedule,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the employee time schedule with the primary key or throws a {@link info.diit.portal.NoSuchEmployeeTimeScheduleException} if it could not be found.
	*
	* @param employeeTimeScheduleId the primary key of the employee time schedule
	* @return the employee time schedule
	* @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a employee time schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule findByPrimaryKey(
		long employeeTimeScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException;

	/**
	* Returns the employee time schedule with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param employeeTimeScheduleId the primary key of the employee time schedule
	* @return the employee time schedule, or <code>null</code> if a employee time schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule fetchByPrimaryKey(
		long employeeTimeScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the employee time schedules where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the matching employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeTimeSchedule> findByEmployee(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the employee time schedules where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of employee time schedules
	* @param end the upper bound of the range of employee time schedules (not inclusive)
	* @return the range of matching employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeTimeSchedule> findByEmployee(
		long employeeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the employee time schedules where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of employee time schedules
	* @param end the upper bound of the range of employee time schedules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeTimeSchedule> findByEmployee(
		long employeeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first employee time schedule in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee time schedule
	* @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a matching employee time schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule findByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException;

	/**
	* Returns the first employee time schedule in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee time schedule, or <code>null</code> if a matching employee time schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule fetchByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last employee time schedule in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee time schedule
	* @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a matching employee time schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule findByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException;

	/**
	* Returns the last employee time schedule in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee time schedule, or <code>null</code> if a matching employee time schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule fetchByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the employee time schedules before and after the current employee time schedule in the ordered set where employeeId = &#63;.
	*
	* @param employeeTimeScheduleId the primary key of the current employee time schedule
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next employee time schedule
	* @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a employee time schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule[] findByEmployee_PrevAndNext(
		long employeeTimeScheduleId, long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException;

	/**
	* Returns the employee time schedule where employeeId = &#63; and day = &#63; or throws a {@link info.diit.portal.NoSuchEmployeeTimeScheduleException} if it could not be found.
	*
	* @param employeeId the employee ID
	* @param day the day
	* @return the matching employee time schedule
	* @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a matching employee time schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule findByEmployeeDay(
		long employeeId, int day)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException;

	/**
	* Returns the employee time schedule where employeeId = &#63; and day = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param employeeId the employee ID
	* @param day the day
	* @return the matching employee time schedule, or <code>null</code> if a matching employee time schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule fetchByEmployeeDay(
		long employeeId, int day)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the employee time schedule where employeeId = &#63; and day = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param employeeId the employee ID
	* @param day the day
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching employee time schedule, or <code>null</code> if a matching employee time schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule fetchByEmployeeDay(
		long employeeId, int day, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the employee time schedules.
	*
	* @return the employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeTimeSchedule> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the employee time schedules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee time schedules
	* @param end the upper bound of the range of employee time schedules (not inclusive)
	* @return the range of employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeTimeSchedule> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the employee time schedules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee time schedules
	* @param end the upper bound of the range of employee time schedules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeTimeSchedule> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the employee time schedules where employeeId = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the employee time schedule where employeeId = &#63; and day = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @param day the day
	* @return the employee time schedule that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule removeByEmployeeDay(
		long employeeId, int day)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException;

	/**
	* Removes all the employee time schedules from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of employee time schedules where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the number of matching employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public int countByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of employee time schedules where employeeId = &#63; and day = &#63;.
	*
	* @param employeeId the employee ID
	* @param day the day
	* @return the number of matching employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public int countByEmployeeDay(long employeeId, int day)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of employee time schedules.
	*
	* @return the number of employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
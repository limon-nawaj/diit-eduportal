/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.AttendanceCorrection;

import java.util.List;

/**
 * The persistence utility for the attendance correction service. This utility wraps {@link AttendanceCorrectionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see AttendanceCorrectionPersistence
 * @see AttendanceCorrectionPersistenceImpl
 * @generated
 */
public class AttendanceCorrectionUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(AttendanceCorrection attendanceCorrection) {
		getPersistence().clearCache(attendanceCorrection);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<AttendanceCorrection> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<AttendanceCorrection> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<AttendanceCorrection> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static AttendanceCorrection update(
		AttendanceCorrection attendanceCorrection, boolean merge)
		throws SystemException {
		return getPersistence().update(attendanceCorrection, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static AttendanceCorrection update(
		AttendanceCorrection attendanceCorrection, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence()
				   .update(attendanceCorrection, merge, serviceContext);
	}

	/**
	* Caches the attendance correction in the entity cache if it is enabled.
	*
	* @param attendanceCorrection the attendance correction
	*/
	public static void cacheResult(
		info.diit.portal.model.AttendanceCorrection attendanceCorrection) {
		getPersistence().cacheResult(attendanceCorrection);
	}

	/**
	* Caches the attendance corrections in the entity cache if it is enabled.
	*
	* @param attendanceCorrections the attendance corrections
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.AttendanceCorrection> attendanceCorrections) {
		getPersistence().cacheResult(attendanceCorrections);
	}

	/**
	* Creates a new attendance correction with the primary key. Does not add the attendance correction to the database.
	*
	* @param attendanceCorrectionId the primary key for the new attendance correction
	* @return the new attendance correction
	*/
	public static info.diit.portal.model.AttendanceCorrection create(
		long attendanceCorrectionId) {
		return getPersistence().create(attendanceCorrectionId);
	}

	/**
	* Removes the attendance correction with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceCorrectionId the primary key of the attendance correction
	* @return the attendance correction that was removed
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a attendance correction with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AttendanceCorrection remove(
		long attendanceCorrectionId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException {
		return getPersistence().remove(attendanceCorrectionId);
	}

	public static info.diit.portal.model.AttendanceCorrection updateImpl(
		info.diit.portal.model.AttendanceCorrection attendanceCorrection,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(attendanceCorrection, merge);
	}

	/**
	* Returns the attendance correction with the primary key or throws a {@link info.diit.portal.NoSuchAttendanceCorrectionException} if it could not be found.
	*
	* @param attendanceCorrectionId the primary key of the attendance correction
	* @return the attendance correction
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a attendance correction with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AttendanceCorrection findByPrimaryKey(
		long attendanceCorrectionId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException {
		return getPersistence().findByPrimaryKey(attendanceCorrectionId);
	}

	/**
	* Returns the attendance correction with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param attendanceCorrectionId the primary key of the attendance correction
	* @return the attendance correction, or <code>null</code> if a attendance correction with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AttendanceCorrection fetchByPrimaryKey(
		long attendanceCorrectionId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(attendanceCorrectionId);
	}

	/**
	* Returns all the attendance corrections where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AttendanceCorrection> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId);
	}

	/**
	* Returns a range of all the attendance corrections where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of attendance corrections
	* @param end the upper bound of the range of attendance corrections (not inclusive)
	* @return the range of matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AttendanceCorrection> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the attendance corrections where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of attendance corrections
	* @param end the upper bound of the range of attendance corrections (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AttendanceCorrection> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first attendance correction in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance correction
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AttendanceCorrection findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException {
		return getPersistence().findByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first attendance correction in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance correction, or <code>null</code> if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AttendanceCorrection fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last attendance correction in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance correction
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AttendanceCorrection findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException {
		return getPersistence().findByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last attendance correction in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance correction, or <code>null</code> if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AttendanceCorrection fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the attendance corrections before and after the current attendance correction in the ordered set where companyId = &#63;.
	*
	* @param attendanceCorrectionId the primary key of the current attendance correction
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next attendance correction
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a attendance correction with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AttendanceCorrection[] findByCompany_PrevAndNext(
		long attendanceCorrectionId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException {
		return getPersistence()
				   .findByCompany_PrevAndNext(attendanceCorrectionId,
			companyId, orderByComparator);
	}

	/**
	* Returns all the attendance corrections where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AttendanceCorrection> findByEmployee(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByEmployee(employeeId);
	}

	/**
	* Returns a range of all the attendance corrections where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of attendance corrections
	* @param end the upper bound of the range of attendance corrections (not inclusive)
	* @return the range of matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AttendanceCorrection> findByEmployee(
		long employeeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByEmployee(employeeId, start, end);
	}

	/**
	* Returns an ordered range of all the attendance corrections where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of attendance corrections
	* @param end the upper bound of the range of attendance corrections (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AttendanceCorrection> findByEmployee(
		long employeeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByEmployee(employeeId, start, end, orderByComparator);
	}

	/**
	* Returns the first attendance correction in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance correction
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AttendanceCorrection findByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException {
		return getPersistence()
				   .findByEmployee_First(employeeId, orderByComparator);
	}

	/**
	* Returns the first attendance correction in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance correction, or <code>null</code> if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AttendanceCorrection fetchByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployee_First(employeeId, orderByComparator);
	}

	/**
	* Returns the last attendance correction in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance correction
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AttendanceCorrection findByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException {
		return getPersistence()
				   .findByEmployee_Last(employeeId, orderByComparator);
	}

	/**
	* Returns the last attendance correction in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance correction, or <code>null</code> if a matching attendance correction could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AttendanceCorrection fetchByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployee_Last(employeeId, orderByComparator);
	}

	/**
	* Returns the attendance corrections before and after the current attendance correction in the ordered set where employeeId = &#63;.
	*
	* @param attendanceCorrectionId the primary key of the current attendance correction
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next attendance correction
	* @throws info.diit.portal.NoSuchAttendanceCorrectionException if a attendance correction with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AttendanceCorrection[] findByEmployee_PrevAndNext(
		long attendanceCorrectionId, long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceCorrectionException {
		return getPersistence()
				   .findByEmployee_PrevAndNext(attendanceCorrectionId,
			employeeId, orderByComparator);
	}

	/**
	* Returns all the attendance corrections.
	*
	* @return the attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AttendanceCorrection> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the attendance corrections.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attendance corrections
	* @param end the upper bound of the range of attendance corrections (not inclusive)
	* @return the range of attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AttendanceCorrection> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the attendance corrections.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attendance corrections
	* @param end the upper bound of the range of attendance corrections (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AttendanceCorrection> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the attendance corrections where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCompany(companyId);
	}

	/**
	* Removes all the attendance corrections where employeeId = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByEmployee(employeeId);
	}

	/**
	* Removes all the attendance corrections from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of attendance corrections where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCompany(companyId);
	}

	/**
	* Returns the number of attendance corrections where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the number of matching attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public static int countByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByEmployee(employeeId);
	}

	/**
	* Returns the number of attendance corrections.
	*
	* @return the number of attendance corrections
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static AttendanceCorrectionPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (AttendanceCorrectionPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					AttendanceCorrectionPersistence.class.getName());

			ReferenceRegistry.registerReference(AttendanceCorrectionUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(AttendanceCorrectionPersistence persistence) {
	}

	private static AttendanceCorrectionPersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the employee mobile local service. This utility wraps {@link info.diit.portal.service.impl.EmployeeMobileLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see EmployeeMobileLocalService
 * @see info.diit.portal.service.base.EmployeeMobileLocalServiceBaseImpl
 * @see info.diit.portal.service.impl.EmployeeMobileLocalServiceImpl
 * @generated
 */
public class EmployeeMobileLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.service.impl.EmployeeMobileLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the employee mobile to the database. Also notifies the appropriate model listeners.
	*
	* @param employeeMobile the employee mobile
	* @return the employee mobile that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile addEmployeeMobile(
		info.diit.portal.model.EmployeeMobile employeeMobile)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addEmployeeMobile(employeeMobile);
	}

	/**
	* Creates a new employee mobile with the primary key. Does not add the employee mobile to the database.
	*
	* @param mobileId the primary key for the new employee mobile
	* @return the new employee mobile
	*/
	public static info.diit.portal.model.EmployeeMobile createEmployeeMobile(
		long mobileId) {
		return getService().createEmployeeMobile(mobileId);
	}

	/**
	* Deletes the employee mobile with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param mobileId the primary key of the employee mobile
	* @return the employee mobile that was removed
	* @throws PortalException if a employee mobile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile deleteEmployeeMobile(
		long mobileId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteEmployeeMobile(mobileId);
	}

	/**
	* Deletes the employee mobile from the database. Also notifies the appropriate model listeners.
	*
	* @param employeeMobile the employee mobile
	* @return the employee mobile that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile deleteEmployeeMobile(
		info.diit.portal.model.EmployeeMobile employeeMobile)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteEmployeeMobile(employeeMobile);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.model.EmployeeMobile fetchEmployeeMobile(
		long mobileId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchEmployeeMobile(mobileId);
	}

	/**
	* Returns the employee mobile with the primary key.
	*
	* @param mobileId the primary key of the employee mobile
	* @return the employee mobile
	* @throws PortalException if a employee mobile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile getEmployeeMobile(
		long mobileId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getEmployeeMobile(mobileId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the employee mobiles.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee mobiles
	* @param end the upper bound of the range of employee mobiles (not inclusive)
	* @return the range of employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeMobile> getEmployeeMobiles(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getEmployeeMobiles(start, end);
	}

	/**
	* Returns the number of employee mobiles.
	*
	* @return the number of employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public static int getEmployeeMobilesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getEmployeeMobilesCount();
	}

	/**
	* Updates the employee mobile in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param employeeMobile the employee mobile
	* @return the employee mobile that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile updateEmployeeMobile(
		info.diit.portal.model.EmployeeMobile employeeMobile)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateEmployeeMobile(employeeMobile);
	}

	/**
	* Updates the employee mobile in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param employeeMobile the employee mobile
	* @param merge whether to merge the employee mobile with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the employee mobile that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile updateEmployeeMobile(
		info.diit.portal.model.EmployeeMobile employeeMobile, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateEmployeeMobile(employeeMobile, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployee(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByEmployee(employeeId);
	}

	public static java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployeeStatus(
		long employeeId, long status)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByEmployeeStatus(employeeId, status);
	}

	public static void clearService() {
		_service = null;
	}

	public static EmployeeMobileLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					EmployeeMobileLocalService.class.getName());

			if (invokableLocalService instanceof EmployeeMobileLocalService) {
				_service = (EmployeeMobileLocalService)invokableLocalService;
			}
			else {
				_service = new EmployeeMobileLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(EmployeeMobileLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(EmployeeMobileLocalService service) {
	}

	private static EmployeeMobileLocalService _service;
}
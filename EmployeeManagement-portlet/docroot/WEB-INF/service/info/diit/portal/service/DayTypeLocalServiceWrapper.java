/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link DayTypeLocalService}.
 * </p>
 *
 * @author    limon
 * @see       DayTypeLocalService
 * @generated
 */
public class DayTypeLocalServiceWrapper implements DayTypeLocalService,
	ServiceWrapper<DayTypeLocalService> {
	public DayTypeLocalServiceWrapper(DayTypeLocalService dayTypeLocalService) {
		_dayTypeLocalService = dayTypeLocalService;
	}

	/**
	* Adds the day type to the database. Also notifies the appropriate model listeners.
	*
	* @param dayType the day type
	* @return the day type that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType addDayType(
		info.diit.portal.model.DayType dayType)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.addDayType(dayType);
	}

	/**
	* Creates a new day type with the primary key. Does not add the day type to the database.
	*
	* @param dayTypeId the primary key for the new day type
	* @return the new day type
	*/
	public info.diit.portal.model.DayType createDayType(long dayTypeId) {
		return _dayTypeLocalService.createDayType(dayTypeId);
	}

	/**
	* Deletes the day type with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dayTypeId the primary key of the day type
	* @return the day type that was removed
	* @throws PortalException if a day type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType deleteDayType(long dayTypeId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.deleteDayType(dayTypeId);
	}

	/**
	* Deletes the day type from the database. Also notifies the appropriate model listeners.
	*
	* @param dayType the day type
	* @return the day type that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType deleteDayType(
		info.diit.portal.model.DayType dayType)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.deleteDayType(dayType);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _dayTypeLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.DayType fetchDayType(long dayTypeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.fetchDayType(dayTypeId);
	}

	/**
	* Returns the day type with the primary key.
	*
	* @param dayTypeId the primary key of the day type
	* @return the day type
	* @throws PortalException if a day type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType getDayType(long dayTypeId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.getDayType(dayTypeId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the day types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of day types
	* @param end the upper bound of the range of day types (not inclusive)
	* @return the range of day types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.DayType> getDayTypes(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.getDayTypes(start, end);
	}

	/**
	* Returns the number of day types.
	*
	* @return the number of day types
	* @throws SystemException if a system exception occurred
	*/
	public int getDayTypesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.getDayTypesCount();
	}

	/**
	* Updates the day type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param dayType the day type
	* @return the day type that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType updateDayType(
		info.diit.portal.model.DayType dayType)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.updateDayType(dayType);
	}

	/**
	* Updates the day type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param dayType the day type
	* @param merge whether to merge the day type with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the day type that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.DayType updateDayType(
		info.diit.portal.model.DayType dayType, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.updateDayType(dayType, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _dayTypeLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_dayTypeLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _dayTypeLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	public java.util.List<info.diit.portal.model.DayType> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dayTypeLocalService.findByOrganization(organizationId);
	}

	public info.diit.portal.model.DayType findByTypeOrganization(
		java.lang.String type, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException {
		return _dayTypeLocalService.findByTypeOrganization(type, organizationId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public DayTypeLocalService getWrappedDayTypeLocalService() {
		return _dayTypeLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedDayTypeLocalService(
		DayTypeLocalService dayTypeLocalService) {
		_dayTypeLocalService = dayTypeLocalService;
	}

	public DayTypeLocalService getWrappedService() {
		return _dayTypeLocalService;
	}

	public void setWrappedService(DayTypeLocalService dayTypeLocalService) {
		_dayTypeLocalService = dayTypeLocalService;
	}

	private DayTypeLocalService _dayTypeLocalService;
}
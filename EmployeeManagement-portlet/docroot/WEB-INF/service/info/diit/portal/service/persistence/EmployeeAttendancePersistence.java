/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.EmployeeAttendance;

/**
 * The persistence interface for the employee attendance service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see EmployeeAttendancePersistenceImpl
 * @see EmployeeAttendanceUtil
 * @generated
 */
public interface EmployeeAttendancePersistence extends BasePersistence<EmployeeAttendance> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EmployeeAttendanceUtil} to access the employee attendance persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the employee attendance in the entity cache if it is enabled.
	*
	* @param employeeAttendance the employee attendance
	*/
	public void cacheResult(
		info.diit.portal.model.EmployeeAttendance employeeAttendance);

	/**
	* Caches the employee attendances in the entity cache if it is enabled.
	*
	* @param employeeAttendances the employee attendances
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.EmployeeAttendance> employeeAttendances);

	/**
	* Creates a new employee attendance with the primary key. Does not add the employee attendance to the database.
	*
	* @param employeeAttendanceId the primary key for the new employee attendance
	* @return the new employee attendance
	*/
	public info.diit.portal.model.EmployeeAttendance create(
		long employeeAttendanceId);

	/**
	* Removes the employee attendance with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param employeeAttendanceId the primary key of the employee attendance
	* @return the employee attendance that was removed
	* @throws info.diit.portal.NoSuchEmployeeAttendanceException if a employee attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeAttendance remove(
		long employeeAttendanceId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeAttendanceException;

	public info.diit.portal.model.EmployeeAttendance updateImpl(
		info.diit.portal.model.EmployeeAttendance employeeAttendance,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the employee attendance with the primary key or throws a {@link info.diit.portal.NoSuchEmployeeAttendanceException} if it could not be found.
	*
	* @param employeeAttendanceId the primary key of the employee attendance
	* @return the employee attendance
	* @throws info.diit.portal.NoSuchEmployeeAttendanceException if a employee attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeAttendance findByPrimaryKey(
		long employeeAttendanceId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeAttendanceException;

	/**
	* Returns the employee attendance with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param employeeAttendanceId the primary key of the employee attendance
	* @return the employee attendance, or <code>null</code> if a employee attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeAttendance fetchByPrimaryKey(
		long employeeAttendanceId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the employee attendances where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the matching employee attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeAttendance> findByEmployeeId(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the employee attendances where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of employee attendances
	* @param end the upper bound of the range of employee attendances (not inclusive)
	* @return the range of matching employee attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeAttendance> findByEmployeeId(
		long employeeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the employee attendances where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of employee attendances
	* @param end the upper bound of the range of employee attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching employee attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeAttendance> findByEmployeeId(
		long employeeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first employee attendance in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee attendance
	* @throws info.diit.portal.NoSuchEmployeeAttendanceException if a matching employee attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeAttendance findByEmployeeId_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeAttendanceException;

	/**
	* Returns the first employee attendance in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee attendance, or <code>null</code> if a matching employee attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeAttendance fetchByEmployeeId_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last employee attendance in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee attendance
	* @throws info.diit.portal.NoSuchEmployeeAttendanceException if a matching employee attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeAttendance findByEmployeeId_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeAttendanceException;

	/**
	* Returns the last employee attendance in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee attendance, or <code>null</code> if a matching employee attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeAttendance fetchByEmployeeId_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the employee attendances before and after the current employee attendance in the ordered set where employeeId = &#63;.
	*
	* @param employeeAttendanceId the primary key of the current employee attendance
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next employee attendance
	* @throws info.diit.portal.NoSuchEmployeeAttendanceException if a employee attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeAttendance[] findByEmployeeId_PrevAndNext(
		long employeeAttendanceId, long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeAttendanceException;

	/**
	* Returns all the employee attendances where employeeId = &#63; and realTime = &#63;.
	*
	* @param employeeId the employee ID
	* @param realTime the real time
	* @return the matching employee attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeAttendance> findByEmployeeAttendance(
		long employeeId, java.util.Date realTime)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the employee attendances where employeeId = &#63; and realTime = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param realTime the real time
	* @param start the lower bound of the range of employee attendances
	* @param end the upper bound of the range of employee attendances (not inclusive)
	* @return the range of matching employee attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeAttendance> findByEmployeeAttendance(
		long employeeId, java.util.Date realTime, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the employee attendances where employeeId = &#63; and realTime = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param realTime the real time
	* @param start the lower bound of the range of employee attendances
	* @param end the upper bound of the range of employee attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching employee attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeAttendance> findByEmployeeAttendance(
		long employeeId, java.util.Date realTime, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first employee attendance in the ordered set where employeeId = &#63; and realTime = &#63;.
	*
	* @param employeeId the employee ID
	* @param realTime the real time
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee attendance
	* @throws info.diit.portal.NoSuchEmployeeAttendanceException if a matching employee attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeAttendance findByEmployeeAttendance_First(
		long employeeId, java.util.Date realTime,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeAttendanceException;

	/**
	* Returns the first employee attendance in the ordered set where employeeId = &#63; and realTime = &#63;.
	*
	* @param employeeId the employee ID
	* @param realTime the real time
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee attendance, or <code>null</code> if a matching employee attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeAttendance fetchByEmployeeAttendance_First(
		long employeeId, java.util.Date realTime,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last employee attendance in the ordered set where employeeId = &#63; and realTime = &#63;.
	*
	* @param employeeId the employee ID
	* @param realTime the real time
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee attendance
	* @throws info.diit.portal.NoSuchEmployeeAttendanceException if a matching employee attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeAttendance findByEmployeeAttendance_Last(
		long employeeId, java.util.Date realTime,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeAttendanceException;

	/**
	* Returns the last employee attendance in the ordered set where employeeId = &#63; and realTime = &#63;.
	*
	* @param employeeId the employee ID
	* @param realTime the real time
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee attendance, or <code>null</code> if a matching employee attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeAttendance fetchByEmployeeAttendance_Last(
		long employeeId, java.util.Date realTime,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the employee attendances before and after the current employee attendance in the ordered set where employeeId = &#63; and realTime = &#63;.
	*
	* @param employeeAttendanceId the primary key of the current employee attendance
	* @param employeeId the employee ID
	* @param realTime the real time
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next employee attendance
	* @throws info.diit.portal.NoSuchEmployeeAttendanceException if a employee attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeAttendance[] findByEmployeeAttendance_PrevAndNext(
		long employeeAttendanceId, long employeeId, java.util.Date realTime,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeAttendanceException;

	/**
	* Returns all the employee attendances.
	*
	* @return the employee attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeAttendance> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the employee attendances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee attendances
	* @param end the upper bound of the range of employee attendances (not inclusive)
	* @return the range of employee attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeAttendance> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the employee attendances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee attendances
	* @param end the upper bound of the range of employee attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of employee attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeAttendance> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the employee attendances where employeeId = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByEmployeeId(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the employee attendances where employeeId = &#63; and realTime = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @param realTime the real time
	* @throws SystemException if a system exception occurred
	*/
	public void removeByEmployeeAttendance(long employeeId,
		java.util.Date realTime)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the employee attendances from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of employee attendances where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the number of matching employee attendances
	* @throws SystemException if a system exception occurred
	*/
	public int countByEmployeeId(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of employee attendances where employeeId = &#63; and realTime = &#63;.
	*
	* @param employeeId the employee ID
	* @param realTime the real time
	* @return the number of matching employee attendances
	* @throws SystemException if a system exception occurred
	*/
	public int countByEmployeeAttendance(long employeeId,
		java.util.Date realTime)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of employee attendances.
	*
	* @return the number of employee attendances
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
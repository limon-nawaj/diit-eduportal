/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.DayType;

import java.util.List;

/**
 * The persistence utility for the day type service. This utility wraps {@link DayTypePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see DayTypePersistence
 * @see DayTypePersistenceImpl
 * @generated
 */
public class DayTypeUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(DayType dayType) {
		getPersistence().clearCache(dayType);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<DayType> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<DayType> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<DayType> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static DayType update(DayType dayType, boolean merge)
		throws SystemException {
		return getPersistence().update(dayType, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static DayType update(DayType dayType, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(dayType, merge, serviceContext);
	}

	/**
	* Caches the day type in the entity cache if it is enabled.
	*
	* @param dayType the day type
	*/
	public static void cacheResult(info.diit.portal.model.DayType dayType) {
		getPersistence().cacheResult(dayType);
	}

	/**
	* Caches the day types in the entity cache if it is enabled.
	*
	* @param dayTypes the day types
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.DayType> dayTypes) {
		getPersistence().cacheResult(dayTypes);
	}

	/**
	* Creates a new day type with the primary key. Does not add the day type to the database.
	*
	* @param dayTypeId the primary key for the new day type
	* @return the new day type
	*/
	public static info.diit.portal.model.DayType create(long dayTypeId) {
		return getPersistence().create(dayTypeId);
	}

	/**
	* Removes the day type with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dayTypeId the primary key of the day type
	* @return the day type that was removed
	* @throws info.diit.portal.NoSuchDayTypeException if a day type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DayType remove(long dayTypeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException {
		return getPersistence().remove(dayTypeId);
	}

	public static info.diit.portal.model.DayType updateImpl(
		info.diit.portal.model.DayType dayType, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(dayType, merge);
	}

	/**
	* Returns the day type with the primary key or throws a {@link info.diit.portal.NoSuchDayTypeException} if it could not be found.
	*
	* @param dayTypeId the primary key of the day type
	* @return the day type
	* @throws info.diit.portal.NoSuchDayTypeException if a day type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DayType findByPrimaryKey(
		long dayTypeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException {
		return getPersistence().findByPrimaryKey(dayTypeId);
	}

	/**
	* Returns the day type with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param dayTypeId the primary key of the day type
	* @return the day type, or <code>null</code> if a day type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DayType fetchByPrimaryKey(
		long dayTypeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(dayTypeId);
	}

	/**
	* Returns all the day types where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching day types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DayType> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganization(organizationId);
	}

	/**
	* Returns a range of all the day types where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of day types
	* @param end the upper bound of the range of day types (not inclusive)
	* @return the range of matching day types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DayType> findByOrganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganization(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the day types where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of day types
	* @param end the upper bound of the range of day types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching day types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DayType> findByOrganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByOrganization(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first day type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching day type
	* @throws info.diit.portal.NoSuchDayTypeException if a matching day type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DayType findByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException {
		return getPersistence()
				   .findByOrganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first day type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching day type, or <code>null</code> if a matching day type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DayType fetchByOrganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the last day type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching day type
	* @throws info.diit.portal.NoSuchDayTypeException if a matching day type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DayType findByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException {
		return getPersistence()
				   .findByOrganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last day type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching day type, or <code>null</code> if a matching day type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DayType fetchByOrganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the day types before and after the current day type in the ordered set where organizationId = &#63;.
	*
	* @param dayTypeId the primary key of the current day type
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next day type
	* @throws info.diit.portal.NoSuchDayTypeException if a day type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DayType[] findByOrganization_PrevAndNext(
		long dayTypeId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException {
		return getPersistence()
				   .findByOrganization_PrevAndNext(dayTypeId, organizationId,
			orderByComparator);
	}

	/**
	* Returns the day type where type = &#63; and organizationId = &#63; or throws a {@link info.diit.portal.NoSuchDayTypeException} if it could not be found.
	*
	* @param type the type
	* @param organizationId the organization ID
	* @return the matching day type
	* @throws info.diit.portal.NoSuchDayTypeException if a matching day type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DayType findByTypeOrganization(
		java.lang.String type, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException {
		return getPersistence().findByTypeOrganization(type, organizationId);
	}

	/**
	* Returns the day type where type = &#63; and organizationId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param type the type
	* @param organizationId the organization ID
	* @return the matching day type, or <code>null</code> if a matching day type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DayType fetchByTypeOrganization(
		java.lang.String type, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByTypeOrganization(type, organizationId);
	}

	/**
	* Returns the day type where type = &#63; and organizationId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param type the type
	* @param organizationId the organization ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching day type, or <code>null</code> if a matching day type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DayType fetchByTypeOrganization(
		java.lang.String type, long organizationId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByTypeOrganization(type, organizationId,
			retrieveFromCache);
	}

	/**
	* Returns all the day types.
	*
	* @return the day types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DayType> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the day types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of day types
	* @param end the upper bound of the range of day types (not inclusive)
	* @return the range of day types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DayType> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the day types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of day types
	* @param end the upper bound of the range of day types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of day types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DayType> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the day types where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByOrganization(organizationId);
	}

	/**
	* Removes the day type where type = &#63; and organizationId = &#63; from the database.
	*
	* @param type the type
	* @param organizationId the organization ID
	* @return the day type that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DayType removeByTypeOrganization(
		java.lang.String type, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDayTypeException {
		return getPersistence().removeByTypeOrganization(type, organizationId);
	}

	/**
	* Removes all the day types from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of day types where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching day types
	* @throws SystemException if a system exception occurred
	*/
	public static int countByOrganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByOrganization(organizationId);
	}

	/**
	* Returns the number of day types where type = &#63; and organizationId = &#63;.
	*
	* @param type the type
	* @param organizationId the organization ID
	* @return the number of matching day types
	* @throws SystemException if a system exception occurred
	*/
	public static int countByTypeOrganization(java.lang.String type,
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByTypeOrganization(type, organizationId);
	}

	/**
	* Returns the number of day types.
	*
	* @return the number of day types
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static DayTypePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (DayTypePersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					DayTypePersistence.class.getName());

			ReferenceRegistry.registerReference(DayTypeUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(DayTypePersistence persistence) {
	}

	private static DayTypePersistence _persistence;
}
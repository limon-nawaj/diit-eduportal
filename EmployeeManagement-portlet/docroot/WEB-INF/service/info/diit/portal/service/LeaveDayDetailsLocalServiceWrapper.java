/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link LeaveDayDetailsLocalService}.
 * </p>
 *
 * @author    limon
 * @see       LeaveDayDetailsLocalService
 * @generated
 */
public class LeaveDayDetailsLocalServiceWrapper
	implements LeaveDayDetailsLocalService,
		ServiceWrapper<LeaveDayDetailsLocalService> {
	public LeaveDayDetailsLocalServiceWrapper(
		LeaveDayDetailsLocalService leaveDayDetailsLocalService) {
		_leaveDayDetailsLocalService = leaveDayDetailsLocalService;
	}

	/**
	* Adds the leave day details to the database. Also notifies the appropriate model listeners.
	*
	* @param leaveDayDetails the leave day details
	* @return the leave day details that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails addLeaveDayDetails(
		info.diit.portal.model.LeaveDayDetails leaveDayDetails)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.addLeaveDayDetails(leaveDayDetails);
	}

	/**
	* Creates a new leave day details with the primary key. Does not add the leave day details to the database.
	*
	* @param leaveDayDetailsId the primary key for the new leave day details
	* @return the new leave day details
	*/
	public info.diit.portal.model.LeaveDayDetails createLeaveDayDetails(
		long leaveDayDetailsId) {
		return _leaveDayDetailsLocalService.createLeaveDayDetails(leaveDayDetailsId);
	}

	/**
	* Deletes the leave day details with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param leaveDayDetailsId the primary key of the leave day details
	* @return the leave day details that was removed
	* @throws PortalException if a leave day details with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails deleteLeaveDayDetails(
		long leaveDayDetailsId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.deleteLeaveDayDetails(leaveDayDetailsId);
	}

	/**
	* Deletes the leave day details from the database. Also notifies the appropriate model listeners.
	*
	* @param leaveDayDetails the leave day details
	* @return the leave day details that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails deleteLeaveDayDetails(
		info.diit.portal.model.LeaveDayDetails leaveDayDetails)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.deleteLeaveDayDetails(leaveDayDetails);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _leaveDayDetailsLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.LeaveDayDetails fetchLeaveDayDetails(
		long leaveDayDetailsId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.fetchLeaveDayDetails(leaveDayDetailsId);
	}

	/**
	* Returns the leave day details with the primary key.
	*
	* @param leaveDayDetailsId the primary key of the leave day details
	* @return the leave day details
	* @throws PortalException if a leave day details with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails getLeaveDayDetails(
		long leaveDayDetailsId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.getLeaveDayDetails(leaveDayDetailsId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the leave day detailses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leave day detailses
	* @param end the upper bound of the range of leave day detailses (not inclusive)
	* @return the range of leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveDayDetails> getLeaveDayDetailses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.getLeaveDayDetailses(start, end);
	}

	/**
	* Returns the number of leave day detailses.
	*
	* @return the number of leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public int getLeaveDayDetailsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.getLeaveDayDetailsesCount();
	}

	/**
	* Updates the leave day details in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param leaveDayDetails the leave day details
	* @return the leave day details that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails updateLeaveDayDetails(
		info.diit.portal.model.LeaveDayDetails leaveDayDetails)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.updateLeaveDayDetails(leaveDayDetails);
	}

	/**
	* Updates the leave day details in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param leaveDayDetails the leave day details
	* @param merge whether to merge the leave day details with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the leave day details that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails updateLeaveDayDetails(
		info.diit.portal.model.LeaveDayDetails leaveDayDetails, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.updateLeaveDayDetails(leaveDayDetails,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _leaveDayDetailsLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_leaveDayDetailsLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _leaveDayDetailsLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.model.LeaveDayDetails> findByLeave(
		long leave) throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveDayDetailsLocalService.findByLeave(leave);
	}

	public info.diit.portal.model.LeaveDayDetails findByLeaveDay(long leaveId,
		java.util.Date leaveDate)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveDayDetailsException {
		return _leaveDayDetailsLocalService.findByLeaveDay(leaveId, leaveDate);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public LeaveDayDetailsLocalService getWrappedLeaveDayDetailsLocalService() {
		return _leaveDayDetailsLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedLeaveDayDetailsLocalService(
		LeaveDayDetailsLocalService leaveDayDetailsLocalService) {
		_leaveDayDetailsLocalService = leaveDayDetailsLocalService;
	}

	public LeaveDayDetailsLocalService getWrappedService() {
		return _leaveDayDetailsLocalService;
	}

	public void setWrappedService(
		LeaveDayDetailsLocalService leaveDayDetailsLocalService) {
		_leaveDayDetailsLocalService = leaveDayDetailsLocalService;
	}

	private LeaveDayDetailsLocalService _leaveDayDetailsLocalService;
}
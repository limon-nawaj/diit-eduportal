/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link EmployeeMobileLocalService}.
 * </p>
 *
 * @author    limon
 * @see       EmployeeMobileLocalService
 * @generated
 */
public class EmployeeMobileLocalServiceWrapper
	implements EmployeeMobileLocalService,
		ServiceWrapper<EmployeeMobileLocalService> {
	public EmployeeMobileLocalServiceWrapper(
		EmployeeMobileLocalService employeeMobileLocalService) {
		_employeeMobileLocalService = employeeMobileLocalService;
	}

	/**
	* Adds the employee mobile to the database. Also notifies the appropriate model listeners.
	*
	* @param employeeMobile the employee mobile
	* @return the employee mobile that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile addEmployeeMobile(
		info.diit.portal.model.EmployeeMobile employeeMobile)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.addEmployeeMobile(employeeMobile);
	}

	/**
	* Creates a new employee mobile with the primary key. Does not add the employee mobile to the database.
	*
	* @param mobileId the primary key for the new employee mobile
	* @return the new employee mobile
	*/
	public info.diit.portal.model.EmployeeMobile createEmployeeMobile(
		long mobileId) {
		return _employeeMobileLocalService.createEmployeeMobile(mobileId);
	}

	/**
	* Deletes the employee mobile with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param mobileId the primary key of the employee mobile
	* @return the employee mobile that was removed
	* @throws PortalException if a employee mobile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile deleteEmployeeMobile(
		long mobileId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.deleteEmployeeMobile(mobileId);
	}

	/**
	* Deletes the employee mobile from the database. Also notifies the appropriate model listeners.
	*
	* @param employeeMobile the employee mobile
	* @return the employee mobile that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile deleteEmployeeMobile(
		info.diit.portal.model.EmployeeMobile employeeMobile)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.deleteEmployeeMobile(employeeMobile);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _employeeMobileLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.EmployeeMobile fetchEmployeeMobile(
		long mobileId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.fetchEmployeeMobile(mobileId);
	}

	/**
	* Returns the employee mobile with the primary key.
	*
	* @param mobileId the primary key of the employee mobile
	* @return the employee mobile
	* @throws PortalException if a employee mobile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile getEmployeeMobile(
		long mobileId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.getEmployeeMobile(mobileId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the employee mobiles.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee mobiles
	* @param end the upper bound of the range of employee mobiles (not inclusive)
	* @return the range of employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeMobile> getEmployeeMobiles(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.getEmployeeMobiles(start, end);
	}

	/**
	* Returns the number of employee mobiles.
	*
	* @return the number of employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public int getEmployeeMobilesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.getEmployeeMobilesCount();
	}

	/**
	* Updates the employee mobile in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param employeeMobile the employee mobile
	* @return the employee mobile that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile updateEmployeeMobile(
		info.diit.portal.model.EmployeeMobile employeeMobile)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.updateEmployeeMobile(employeeMobile);
	}

	/**
	* Updates the employee mobile in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param employeeMobile the employee mobile
	* @param merge whether to merge the employee mobile with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the employee mobile that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeMobile updateEmployeeMobile(
		info.diit.portal.model.EmployeeMobile employeeMobile, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.updateEmployeeMobile(employeeMobile,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _employeeMobileLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_employeeMobileLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _employeeMobileLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployee(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.findByEmployee(employeeId);
	}

	public java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployeeStatus(
		long employeeId, long status)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeMobileLocalService.findByEmployeeStatus(employeeId,
			status);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public EmployeeMobileLocalService getWrappedEmployeeMobileLocalService() {
		return _employeeMobileLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedEmployeeMobileLocalService(
		EmployeeMobileLocalService employeeMobileLocalService) {
		_employeeMobileLocalService = employeeMobileLocalService;
	}

	public EmployeeMobileLocalService getWrappedService() {
		return _employeeMobileLocalService;
	}

	public void setWrappedService(
		EmployeeMobileLocalService employeeMobileLocalService) {
		_employeeMobileLocalService = employeeMobileLocalService;
	}

	private EmployeeMobileLocalService _employeeMobileLocalService;
}
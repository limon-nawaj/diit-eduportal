/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import info.diit.portal.model.AttendanceCorrectionClp;
import info.diit.portal.model.DayTypeClp;
import info.diit.portal.model.EmployeeAttendanceClp;
import info.diit.portal.model.EmployeeClp;
import info.diit.portal.model.EmployeeEmailClp;
import info.diit.portal.model.EmployeeMobileClp;
import info.diit.portal.model.EmployeeRoleClp;
import info.diit.portal.model.EmployeeTimeScheduleClp;
import info.diit.portal.model.LeaveCategoryClp;
import info.diit.portal.model.LeaveClp;
import info.diit.portal.model.LeaveDayDetailsClp;
import info.diit.portal.model.LeaveReceiverClp;
import info.diit.portal.model.WorkingDayClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Brian Wing Shun Chan
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"EmployeeManagement-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"EmployeeManagement-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "EmployeeManagement-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(AttendanceCorrectionClp.class.getName())) {
			return translateInputAttendanceCorrection(oldModel);
		}

		if (oldModelClassName.equals(DayTypeClp.class.getName())) {
			return translateInputDayType(oldModel);
		}

		if (oldModelClassName.equals(EmployeeClp.class.getName())) {
			return translateInputEmployee(oldModel);
		}

		if (oldModelClassName.equals(EmployeeAttendanceClp.class.getName())) {
			return translateInputEmployeeAttendance(oldModel);
		}

		if (oldModelClassName.equals(EmployeeEmailClp.class.getName())) {
			return translateInputEmployeeEmail(oldModel);
		}

		if (oldModelClassName.equals(EmployeeMobileClp.class.getName())) {
			return translateInputEmployeeMobile(oldModel);
		}

		if (oldModelClassName.equals(EmployeeRoleClp.class.getName())) {
			return translateInputEmployeeRole(oldModel);
		}

		if (oldModelClassName.equals(EmployeeTimeScheduleClp.class.getName())) {
			return translateInputEmployeeTimeSchedule(oldModel);
		}

		if (oldModelClassName.equals(LeaveClp.class.getName())) {
			return translateInputLeave(oldModel);
		}

		if (oldModelClassName.equals(LeaveCategoryClp.class.getName())) {
			return translateInputLeaveCategory(oldModel);
		}

		if (oldModelClassName.equals(LeaveDayDetailsClp.class.getName())) {
			return translateInputLeaveDayDetails(oldModel);
		}

		if (oldModelClassName.equals(LeaveReceiverClp.class.getName())) {
			return translateInputLeaveReceiver(oldModel);
		}

		if (oldModelClassName.equals(WorkingDayClp.class.getName())) {
			return translateInputWorkingDay(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputAttendanceCorrection(
		BaseModel<?> oldModel) {
		AttendanceCorrectionClp oldClpModel = (AttendanceCorrectionClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAttendanceCorrectionRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputDayType(BaseModel<?> oldModel) {
		DayTypeClp oldClpModel = (DayTypeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getDayTypeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEmployee(BaseModel<?> oldModel) {
		EmployeeClp oldClpModel = (EmployeeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEmployeeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEmployeeAttendance(BaseModel<?> oldModel) {
		EmployeeAttendanceClp oldClpModel = (EmployeeAttendanceClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEmployeeAttendanceRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEmployeeEmail(BaseModel<?> oldModel) {
		EmployeeEmailClp oldClpModel = (EmployeeEmailClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEmployeeEmailRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEmployeeMobile(BaseModel<?> oldModel) {
		EmployeeMobileClp oldClpModel = (EmployeeMobileClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEmployeeMobileRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEmployeeRole(BaseModel<?> oldModel) {
		EmployeeRoleClp oldClpModel = (EmployeeRoleClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEmployeeRoleRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEmployeeTimeSchedule(
		BaseModel<?> oldModel) {
		EmployeeTimeScheduleClp oldClpModel = (EmployeeTimeScheduleClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEmployeeTimeScheduleRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLeave(BaseModel<?> oldModel) {
		LeaveClp oldClpModel = (LeaveClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLeaveRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLeaveCategory(BaseModel<?> oldModel) {
		LeaveCategoryClp oldClpModel = (LeaveCategoryClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLeaveCategoryRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLeaveDayDetails(BaseModel<?> oldModel) {
		LeaveDayDetailsClp oldClpModel = (LeaveDayDetailsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLeaveDayDetailsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLeaveReceiver(BaseModel<?> oldModel) {
		LeaveReceiverClp oldClpModel = (LeaveReceiverClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLeaveReceiverRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputWorkingDay(BaseModel<?> oldModel) {
		WorkingDayClp oldClpModel = (WorkingDayClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getWorkingDayRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.AttendanceCorrectionImpl")) {
			return translateOutputAttendanceCorrection(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.DayTypeImpl")) {
			return translateOutputDayType(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.EmployeeImpl")) {
			return translateOutputEmployee(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.EmployeeAttendanceImpl")) {
			return translateOutputEmployeeAttendance(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.EmployeeEmailImpl")) {
			return translateOutputEmployeeEmail(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.EmployeeMobileImpl")) {
			return translateOutputEmployeeMobile(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.EmployeeRoleImpl")) {
			return translateOutputEmployeeRole(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.EmployeeTimeScheduleImpl")) {
			return translateOutputEmployeeTimeSchedule(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.LeaveImpl")) {
			return translateOutputLeave(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.LeaveCategoryImpl")) {
			return translateOutputLeaveCategory(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.LeaveDayDetailsImpl")) {
			return translateOutputLeaveDayDetails(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.LeaveReceiverImpl")) {
			return translateOutputLeaveReceiver(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.WorkingDayImpl")) {
			return translateOutputWorkingDay(oldModel);
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchAttendanceCorrectionException")) {
			return new info.diit.portal.NoSuchAttendanceCorrectionException();
		}

		if (className.equals("info.diit.portal.NoSuchDayTypeException")) {
			return new info.diit.portal.NoSuchDayTypeException();
		}

		if (className.equals("info.diit.portal.NoSuchEmployeeException")) {
			return new info.diit.portal.NoSuchEmployeeException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchEmployeeAttendanceException")) {
			return new info.diit.portal.NoSuchEmployeeAttendanceException();
		}

		if (className.equals("info.diit.portal.NoSuchEmployeeEmailException")) {
			return new info.diit.portal.NoSuchEmployeeEmailException();
		}

		if (className.equals("info.diit.portal.NoSuchEmployeeMobileException")) {
			return new info.diit.portal.NoSuchEmployeeMobileException();
		}

		if (className.equals("info.diit.portal.NoSuchEmployeeRoleException")) {
			return new info.diit.portal.NoSuchEmployeeRoleException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchEmployeeTimeScheduleException")) {
			return new info.diit.portal.NoSuchEmployeeTimeScheduleException();
		}

		if (className.equals("info.diit.portal.NoSuchLeaveException")) {
			return new info.diit.portal.NoSuchLeaveException();
		}

		if (className.equals("info.diit.portal.NoSuchLeaveCategoryException")) {
			return new info.diit.portal.NoSuchLeaveCategoryException();
		}

		if (className.equals("info.diit.portal.NoSuchLeaveDayDetailsException")) {
			return new info.diit.portal.NoSuchLeaveDayDetailsException();
		}

		if (className.equals("info.diit.portal.NoSuchLeaveReceiverException")) {
			return new info.diit.portal.NoSuchLeaveReceiverException();
		}

		if (className.equals("info.diit.portal.NoSuchWorkingDayException")) {
			return new info.diit.portal.NoSuchWorkingDayException();
		}

		return throwable;
	}

	public static Object translateOutputAttendanceCorrection(
		BaseModel<?> oldModel) {
		AttendanceCorrectionClp newModel = new AttendanceCorrectionClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAttendanceCorrectionRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputDayType(BaseModel<?> oldModel) {
		DayTypeClp newModel = new DayTypeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setDayTypeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEmployee(BaseModel<?> oldModel) {
		EmployeeClp newModel = new EmployeeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEmployeeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEmployeeAttendance(
		BaseModel<?> oldModel) {
		EmployeeAttendanceClp newModel = new EmployeeAttendanceClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEmployeeAttendanceRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEmployeeEmail(BaseModel<?> oldModel) {
		EmployeeEmailClp newModel = new EmployeeEmailClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEmployeeEmailRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEmployeeMobile(BaseModel<?> oldModel) {
		EmployeeMobileClp newModel = new EmployeeMobileClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEmployeeMobileRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEmployeeRole(BaseModel<?> oldModel) {
		EmployeeRoleClp newModel = new EmployeeRoleClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEmployeeRoleRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEmployeeTimeSchedule(
		BaseModel<?> oldModel) {
		EmployeeTimeScheduleClp newModel = new EmployeeTimeScheduleClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEmployeeTimeScheduleRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLeave(BaseModel<?> oldModel) {
		LeaveClp newModel = new LeaveClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLeaveRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLeaveCategory(BaseModel<?> oldModel) {
		LeaveCategoryClp newModel = new LeaveCategoryClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLeaveCategoryRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLeaveDayDetails(BaseModel<?> oldModel) {
		LeaveDayDetailsClp newModel = new LeaveDayDetailsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLeaveDayDetailsRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLeaveReceiver(BaseModel<?> oldModel) {
		LeaveReceiverClp newModel = new LeaveReceiverClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLeaveReceiverRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputWorkingDay(BaseModel<?> oldModel) {
		WorkingDayClp newModel = new WorkingDayClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setWorkingDayRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}
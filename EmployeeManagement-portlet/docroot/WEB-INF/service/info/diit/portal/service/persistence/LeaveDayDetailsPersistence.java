/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.LeaveDayDetails;

/**
 * The persistence interface for the leave day details service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see LeaveDayDetailsPersistenceImpl
 * @see LeaveDayDetailsUtil
 * @generated
 */
public interface LeaveDayDetailsPersistence extends BasePersistence<LeaveDayDetails> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LeaveDayDetailsUtil} to access the leave day details persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the leave day details in the entity cache if it is enabled.
	*
	* @param leaveDayDetails the leave day details
	*/
	public void cacheResult(
		info.diit.portal.model.LeaveDayDetails leaveDayDetails);

	/**
	* Caches the leave day detailses in the entity cache if it is enabled.
	*
	* @param leaveDayDetailses the leave day detailses
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.LeaveDayDetails> leaveDayDetailses);

	/**
	* Creates a new leave day details with the primary key. Does not add the leave day details to the database.
	*
	* @param leaveDayDetailsId the primary key for the new leave day details
	* @return the new leave day details
	*/
	public info.diit.portal.model.LeaveDayDetails create(long leaveDayDetailsId);

	/**
	* Removes the leave day details with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param leaveDayDetailsId the primary key of the leave day details
	* @return the leave day details that was removed
	* @throws info.diit.portal.NoSuchLeaveDayDetailsException if a leave day details with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails remove(long leaveDayDetailsId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveDayDetailsException;

	public info.diit.portal.model.LeaveDayDetails updateImpl(
		info.diit.portal.model.LeaveDayDetails leaveDayDetails, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the leave day details with the primary key or throws a {@link info.diit.portal.NoSuchLeaveDayDetailsException} if it could not be found.
	*
	* @param leaveDayDetailsId the primary key of the leave day details
	* @return the leave day details
	* @throws info.diit.portal.NoSuchLeaveDayDetailsException if a leave day details with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails findByPrimaryKey(
		long leaveDayDetailsId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveDayDetailsException;

	/**
	* Returns the leave day details with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param leaveDayDetailsId the primary key of the leave day details
	* @return the leave day details, or <code>null</code> if a leave day details with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails fetchByPrimaryKey(
		long leaveDayDetailsId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the leave day detailses where leaveId = &#63;.
	*
	* @param leaveId the leave ID
	* @return the matching leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveDayDetails> findByLeaveId(
		long leaveId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the leave day detailses where leaveId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param leaveId the leave ID
	* @param start the lower bound of the range of leave day detailses
	* @param end the upper bound of the range of leave day detailses (not inclusive)
	* @return the range of matching leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveDayDetails> findByLeaveId(
		long leaveId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the leave day detailses where leaveId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param leaveId the leave ID
	* @param start the lower bound of the range of leave day detailses
	* @param end the upper bound of the range of leave day detailses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveDayDetails> findByLeaveId(
		long leaveId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first leave day details in the ordered set where leaveId = &#63;.
	*
	* @param leaveId the leave ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave day details
	* @throws info.diit.portal.NoSuchLeaveDayDetailsException if a matching leave day details could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails findByLeaveId_First(
		long leaveId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveDayDetailsException;

	/**
	* Returns the first leave day details in the ordered set where leaveId = &#63;.
	*
	* @param leaveId the leave ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave day details, or <code>null</code> if a matching leave day details could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails fetchByLeaveId_First(
		long leaveId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last leave day details in the ordered set where leaveId = &#63;.
	*
	* @param leaveId the leave ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave day details
	* @throws info.diit.portal.NoSuchLeaveDayDetailsException if a matching leave day details could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails findByLeaveId_Last(
		long leaveId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveDayDetailsException;

	/**
	* Returns the last leave day details in the ordered set where leaveId = &#63;.
	*
	* @param leaveId the leave ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave day details, or <code>null</code> if a matching leave day details could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails fetchByLeaveId_Last(
		long leaveId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the leave day detailses before and after the current leave day details in the ordered set where leaveId = &#63;.
	*
	* @param leaveDayDetailsId the primary key of the current leave day details
	* @param leaveId the leave ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next leave day details
	* @throws info.diit.portal.NoSuchLeaveDayDetailsException if a leave day details with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails[] findByLeaveId_PrevAndNext(
		long leaveDayDetailsId, long leaveId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveDayDetailsException;

	/**
	* Returns the leave day details where leaveId = &#63; and leaveDate = &#63; or throws a {@link info.diit.portal.NoSuchLeaveDayDetailsException} if it could not be found.
	*
	* @param leaveId the leave ID
	* @param leaveDate the leave date
	* @return the matching leave day details
	* @throws info.diit.portal.NoSuchLeaveDayDetailsException if a matching leave day details could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails findByLeaveDay(long leaveId,
		java.util.Date leaveDate)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveDayDetailsException;

	/**
	* Returns the leave day details where leaveId = &#63; and leaveDate = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param leaveId the leave ID
	* @param leaveDate the leave date
	* @return the matching leave day details, or <code>null</code> if a matching leave day details could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails fetchByLeaveDay(
		long leaveId, java.util.Date leaveDate)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the leave day details where leaveId = &#63; and leaveDate = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param leaveId the leave ID
	* @param leaveDate the leave date
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching leave day details, or <code>null</code> if a matching leave day details could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails fetchByLeaveDay(
		long leaveId, java.util.Date leaveDate, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the leave day detailses.
	*
	* @return the leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveDayDetails> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the leave day detailses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leave day detailses
	* @param end the upper bound of the range of leave day detailses (not inclusive)
	* @return the range of leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveDayDetails> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the leave day detailses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leave day detailses
	* @param end the upper bound of the range of leave day detailses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveDayDetails> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the leave day detailses where leaveId = &#63; from the database.
	*
	* @param leaveId the leave ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByLeaveId(long leaveId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the leave day details where leaveId = &#63; and leaveDate = &#63; from the database.
	*
	* @param leaveId the leave ID
	* @param leaveDate the leave date
	* @return the leave day details that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveDayDetails removeByLeaveDay(
		long leaveId, java.util.Date leaveDate)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveDayDetailsException;

	/**
	* Removes all the leave day detailses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of leave day detailses where leaveId = &#63;.
	*
	* @param leaveId the leave ID
	* @return the number of matching leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public int countByLeaveId(long leaveId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of leave day detailses where leaveId = &#63; and leaveDate = &#63;.
	*
	* @param leaveId the leave ID
	* @param leaveDate the leave date
	* @return the number of matching leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public int countByLeaveDay(long leaveId, java.util.Date leaveDate)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of leave day detailses.
	*
	* @return the number of leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
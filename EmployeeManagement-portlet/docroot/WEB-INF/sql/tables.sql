create table DiiTPortal_AttendanceCorrection (
	attendanceCorrectionId LONG not null primary key IDENTITY,
	organizationId LONG,
	companyId LONG,
	employeeId LONG,
	correctionDate DATE null,
	realIn DATE null,
	realOut DATE null,
	expectedIn DATE null,
	expectedOut DATE null,
	status INTEGER
);

create table DiiTPortal_DayType (
	dayTypeId LONG not null primary key IDENTITY,
	organizationId LONG,
	type_ VARCHAR(75) null
);

create table DiiTPortal_Employee (
	employeeId LONG not null primary key,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	employeeUserId LONG,
	name VARCHAR(75) null,
	gender LONG,
	religion LONG,
	blood LONG,
	dob DATE null,
	designation LONG,
	branch LONG,
	status LONG,
	presentAddress VARCHAR(75) null,
	permanentAddress VARCHAR(75) null,
	photo LONG,
	supervisor LONG,
	role LONG,
	duration DOUBLE,
	minimumDuration DOUBLE,
	type_ INTEGER
);

create table DiiTPortal_EmployeeAttendance (
	employeeAttendanceId LONG not null primary key IDENTITY,
	employeeId LONG,
	expectedStart DATE null,
	expectedEnd DATE null,
	realTime DATE null,
	status LONG
);

create table DiiTPortal_EmployeeEmail (
	emailId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	employeeId LONG,
	workEmail VARCHAR(75) null,
	personalEmail VARCHAR(75) null,
	status LONG
);

create table DiiTPortal_EmployeeMobile (
	mobileId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	employeeId LONG,
	mobile VARCHAR(75) null,
	homePhone VARCHAR(75) null,
	status LONG
);

create table DiiTPortal_EmployeeRole (
	employeeRoleId LONG not null primary key IDENTITY,
	organizationId LONG,
	companyId LONG,
	role VARCHAR(75) null,
	startTime DATE null,
	endTime DATE null
);

create table DiiTPortal_EmployeeTimeSchedule (
	employeeTimeScheduleId LONG not null primary key IDENTITY,
	organizationId LONG,
	companyId LONG,
	employeeId LONG,
	day INTEGER,
	startTime DATE null,
	endTime DATE null,
	duration DOUBLE
);

create table DiiTPortal_Leave (
	leaveId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	applicationDate DATE null,
	employee LONG,
	responsibleEmployee LONG,
	startDate DATE null,
	endDate DATE null,
	numberOfDay DOUBLE,
	phoneNumber VARCHAR(75) null,
	causeOfLeave VARCHAR(75) null,
	whereEnjoy VARCHAR(75) null,
	firstRecommendation LONG,
	secondRecommendation LONG,
	leaveCategory LONG,
	responsibleEmployeeStatus INTEGER,
	applicationStatus INTEGER,
	comments VARCHAR(75) null,
	complementedBy LONG
);

create table DiiTPortal_LeaveCategory (
	leaveCategoryId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	title VARCHAR(75) null,
	totalLeave INTEGER
);

create table DiiTPortal_LeaveDayDetails (
	leaveDayDetailsId LONG not null primary key IDENTITY,
	leaveId LONG,
	leaveDate DATE null,
	day DOUBLE
);

create table DiiTPortal_LeaveReceiver (
	leaveReceiverId LONG not null primary key IDENTITY,
	organizationId LONG,
	companyId LONG,
	employeeId LONG
);

create table DiiTPortal_WorkingDay (
	workingDayId LONG not null primary key IDENTITY,
	organizationId LONG,
	date_ DATE null,
	dayOfWeek INTEGER,
	week INTEGER,
	status LONG
);
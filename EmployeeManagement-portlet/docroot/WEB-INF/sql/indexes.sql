create index IX_A5F0FA38 on DiiTPortal_AttendanceCorrection (companyId);
create index IX_5D26048D on DiiTPortal_AttendanceCorrection (employeeId);

create index IX_17E19ADE on DiiTPortal_DayOff (organizationId);

create index IX_2C79993B on DiiTPortal_DayType (organizationId);
create index IX_77A7741C on DiiTPortal_DayType (type_);
create index IX_7396BFBE on DiiTPortal_DayType (type_, organizationId);

create index IX_194E3991 on DiiTPortal_Employee (companyId);
create index IX_5AE90127 on DiiTPortal_Employee (companyId, branch);
create index IX_33D0773F on DiiTPortal_Employee (employeeUserId);
create index IX_8B74C7D9 on DiiTPortal_Employee (organizationId);
create index IX_FB094393 on DiiTPortal_Employee (supervisor);

create index IX_21EC325D on DiiTPortal_EmployeeAttendance (employeeId);
create index IX_5A59A7DC on DiiTPortal_EmployeeAttendance (employeeId, realTime);

create index IX_E2FFD78E on DiiTPortal_EmployeeEmail (employeeId);

create index IX_9ABA78D6 on DiiTPortal_EmployeeMobile (employeeId);
create index IX_CE3581BC on DiiTPortal_EmployeeMobile (employeeId, status);

create index IX_8597D93B on DiiTPortal_EmployeeRole (companyId);
create index IX_D96E626F on DiiTPortal_EmployeeRole (organizationId);

create index IX_D9D56078 on DiiTPortal_EmployeeTimeSchedule (employeeId);
create index IX_179B1554 on DiiTPortal_EmployeeTimeSchedule (employeeId, day);

create index IX_46CA5277 on DiiTPortal_HolidayType (organizationId);

create index IX_15F1C9FC on DiiTPortal_Leave (employee);
create index IX_6BD0F3FC on DiiTPortal_Leave (organizationId);
create index IX_FC91B4DA on DiiTPortal_Leave (responsibleEmployee);

create index IX_E3D7759A on DiiTPortal_LeaveCategory (organizationId);

create index IX_9DFDABE2 on DiiTPortal_LeaveDayDetails (leaveId);
create index IX_BB93EC53 on DiiTPortal_LeaveDayDetails (leaveId, leaveDate);

create index IX_20E599C6 on DiiTPortal_LeaveReceiver (employeeId);
create index IX_5693A84B on DiiTPortal_LeaveReceiver (organizationId);

create index IX_A5FDBDD6 on DiiTPortal_WorkingDay (organizationId);
create index IX_45D3EA4B on DiiTPortal_WorkingDay (organizationId, date_);

create index IX_4BAE33B7 on EduPortal_Employee (companyId);
create index IX_94D7754D on EduPortal_Employee (companyId, branch);
create index IX_8134E4D9 on EduPortal_Employee (employeeUserId);
create index IX_D8D93573 on EduPortal_Employee (organizationId);
create index IX_14A88E2D on EduPortal_Employee (supervisor);

create index IX_42291D34 on EduPortal_EmployeeEmail (employeeId);

create index IX_20B9E7F0 on EduPortal_EmployeeMobile (employeeId);
create index IX_A04936D6 on EduPortal_EmployeeMobile (employeeId, status);

create index IX_35825061 on EduPortal_EmployeeRole (companyId);
create index IX_DD4E7309 on EduPortal_EmployeeRole (organizationId);

create index IX_D7A79412 on EduPortal_EmployeeTimeSchedule (employeeId);
create index IX_FAFB11FA on EduPortal_EmployeeTimeSchedule (employeeId, day);
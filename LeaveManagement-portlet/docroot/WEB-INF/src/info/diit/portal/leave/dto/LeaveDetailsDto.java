package info.diit.portal.leave.dto;

import java.util.Date;

public class LeaveDetailsDto {
	private int id;
	private Date leaveDate;
	private Integer numberOfDay;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getLeaveDate() {
		return leaveDate;
	}
	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}
	public Integer getNumberOfDay() {
		return numberOfDay;
	}
	public void setNumberOfDay(Integer numberOfDay) {
		this.numberOfDay = numberOfDay;
	}
}

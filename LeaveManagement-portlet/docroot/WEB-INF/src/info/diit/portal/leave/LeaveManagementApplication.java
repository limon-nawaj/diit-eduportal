package info.diit.portal.leave;

import info.diit.portal.leave.dto.LeaveDetailsDto;

import com.vaadin.Application;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class LeaveManagementApplication extends Application {
	
	private final static String COLUMN_DATE 			= "leaveDate";
	private final static String COLUMN_NUMBER_OF_DAY 	= "numberOfDay";

	public void init() {
		Window window = new Window();

		setMainWindow(window);

		window.addComponent(mainLayout());
	}
	
	private GridLayout  mainLayout;
	private ComboBox    employeeComboBox;
	private ComboBox    responsiblePersonComboBox;
	private ComboBox 	designationComboBox;
	private ComboBox 	resDesignationComboBox;
	private ComboBox    leaveCatagoryComboBox;
	private TextField 	numberOfDayField;
	private TextField 	phoneNumberField;
	private DateField 	applicationDtaeField;
	private DateField   startDateField;
	private DateField 	endDateField;
	private TextArea	causeOfLeaveArea;
	private TextArea	whereLeaveArea;
	private ComboBox 	recommendedBy1ComboBox;
	private ComboBox 	recommendedBy2ComboBox;
	private ComboBox 	approveBy1ComboBox;
	private ComboBox 	approveBy2ComboBox;
	
	private Table 		leaveDetailsTable;
	private BeanItemContainer<LeaveDetailsDto> leaveDetailsContainer;
	
	private GridLayout mainLayout(){
		mainLayout 					= new GridLayout(8, 12);
		mainLayout.setWidth("100%");
		mainLayout.setSpacing(true);
		
		leaveCatagoryComboBox		= new ComboBox("Leave Catagory");
		applicationDtaeField 		= new DateField("Application Date");
		
		employeeComboBox	 		= new ComboBox("Employee");
		designationComboBox			= new ComboBox("Designation");
		
		responsiblePersonComboBox	= new ComboBox("Responsible Person");
		resDesignationComboBox		= new ComboBox("Responsible Designation Person");
		
		startDateField 				= new DateField("Start Date");
		endDateField 				= new DateField("End Date");
		
		numberOfDayField 			= new TextField("Number of Day");
		phoneNumberField			= new TextField("Phone Number");
		
		causeOfLeaveArea			= new TextArea("Specify cause of leave Clearly");
		whereLeaveArea				= new TextArea("Specify where leave enjoy");
				
		recommendedBy1ComboBox		= new ComboBox("First Recommendation");
		recommendedBy2ComboBox		= new ComboBox("Second Recommendation");
		approveBy1ComboBox			= new ComboBox("First Approbation");
		approveBy2ComboBox			= new ComboBox("Second Approbation");
		
		leaveDetailsContainer		= new BeanItemContainer<LeaveDetailsDto>(LeaveDetailsDto.class);
		leaveDetailsTable			= new Table("Leave Details", leaveDetailsContainer);
				
		employeeComboBox.setWidth("100%");
		applicationDtaeField.setWidth("100%");
		leaveCatagoryComboBox.setWidth("100%");
		employeeComboBox.setWidth("100%");
		designationComboBox.setWidth("100%");
		responsiblePersonComboBox.setWidth("100%");
		resDesignationComboBox.setWidth("100%");
		startDateField.setWidth("100%");
		endDateField.setWidth("100%");
		numberOfDayField.setWidth("100%");
		phoneNumberField.setWidth("100%");
		causeOfLeaveArea.setWidth("100%");
		whereLeaveArea.setWidth("100%");
		recommendedBy1ComboBox.setWidth("100%");
		recommendedBy2ComboBox.setWidth("100%");
		approveBy1ComboBox.setWidth("100%");
		approveBy2ComboBox.setWidth("100%");
		leaveDetailsTable.setWidth("70%");
		
		leaveDetailsTable.setColumnHeader(COLUMN_DATE, "Leave Date");
		leaveDetailsTable.setColumnHeader(COLUMN_NUMBER_OF_DAY, "Day");
		
		leaveDetailsTable.setVisibleColumns(new String[]{COLUMN_DATE, COLUMN_NUMBER_OF_DAY});
		
		mainLayout.addComponent(leaveCatagoryComboBox, 0, 0, 2, 0);
		mainLayout.addComponent(employeeComboBox, 0, 1, 2, 1);
		mainLayout.addComponent(responsiblePersonComboBox, 0, 2, 2, 2);
		mainLayout.addComponent(startDateField, 0, 3, 2, 3);
		mainLayout.addComponent(numberOfDayField, 0, 4, 2, 4);
				
		mainLayout.addComponent(applicationDtaeField, 4, 0, 7, 0);
		mainLayout.addComponent(designationComboBox, 4, 1, 7, 1);
		mainLayout.addComponent(resDesignationComboBox, 4, 2, 7, 2);
		mainLayout.addComponent(endDateField, 4, 3, 7, 3);
		mainLayout.addComponent(phoneNumberField, 4, 4, 7, 4);
		
		mainLayout.addComponent(causeOfLeaveArea, 0, 5, 7, 5);
		mainLayout.addComponent(whereLeaveArea, 0, 6, 7, 6);
		mainLayout.addComponent(new Label("<hr />",Label.CONTENT_XHTML), 0, 7, 7, 7);
		
		mainLayout.addComponent(leaveDetailsTable, 0, 8, 4, 8);
		
		mainLayout.addComponent(recommendedBy1ComboBox, 0, 9, 2, 9);
		mainLayout.addComponent(recommendedBy2ComboBox, 4, 9, 7, 9);
		
		mainLayout.addComponent(approveBy1ComboBox, 0, 10, 2, 10);
		mainLayout.addComponent(approveBy2ComboBox, 4, 10, 7, 10);
		
		return mainLayout;
	}

}
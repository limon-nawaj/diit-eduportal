create table EduPortal_Batch_Batch (
	batchId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	batchName VARCHAR(75) null,
	courseId LONG,
	sessionId LONG,
	startDate DATE null,
	endDate DATE null,
	batchTeacherId LONG,
	status INTEGER,
	note VARCHAR(75) null
);

create table EduPortal_Batch_BatchTeacher (
	batchTeacherId LONG not null primary key IDENTITY,
	teacherId LONG,
	startDate DATE null,
	endDate DATE null,
	batchId LONG
);
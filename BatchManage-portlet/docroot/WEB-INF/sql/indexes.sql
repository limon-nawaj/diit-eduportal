create index IX_FC96EC7 on Batch_EduPortal_Batch (batchId);
create index IX_4BA659B7 on Batch_EduPortal_Batch (batchName);
create index IX_3AB0C28A on Batch_EduPortal_Batch (companyId);
create index IX_8F0F0968 on Batch_EduPortal_Batch (courseId);
create index IX_B34DF280 on Batch_EduPortal_Batch (organizationId);
create index IX_CF3381A8 on Batch_EduPortal_Batch (organizationId, companyId);
create index IX_424600AD on Batch_EduPortal_Batch (organizationId, companyId, batchName);
create index IX_51C8EE8A on Batch_EduPortal_Batch (organizationId, courseId);

create index IX_552BA0F5 on Batch_EduPortal_BatchTeacher (batchId);
create index IX_3189761D on Batch_EduPortal_BatchTeacher (teacherId);

create index IX_2211F47 on EduPortal_Batch_Batch (batchId);
create index IX_F5DE530A on EduPortal_Batch_Batch (companyId);
create index IX_E7AD68E8 on EduPortal_Batch_Batch (courseId);
create index IX_72301228 on EduPortal_Batch_Batch (organizationId, companyId);
create index IX_EFA2202D on EduPortal_Batch_Batch (organizationId, companyId, batchName);
create index IX_CB84E0A on EduPortal_Batch_Batch (organizationId, courseId);

create index IX_22646075 on EduPortal_Batch_BatchTeacher (batchId);
create index IX_9390559D on EduPortal_Batch_BatchTeacher (teacherId);
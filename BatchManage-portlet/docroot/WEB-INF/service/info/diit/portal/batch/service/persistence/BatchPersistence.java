/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.batch.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.batch.model.Batch;

/**
 * The persistence interface for the batch service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author saeid
 * @see BatchPersistenceImpl
 * @see BatchUtil
 * @generated
 */
public interface BatchPersistence extends BasePersistence<Batch> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BatchUtil} to access the batch persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the batch in the entity cache if it is enabled.
	*
	* @param batch the batch
	*/
	public void cacheResult(info.diit.portal.batch.model.Batch batch);

	/**
	* Caches the batchs in the entity cache if it is enabled.
	*
	* @param batchs the batchs
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.batch.model.Batch> batchs);

	/**
	* Creates a new batch with the primary key. Does not add the batch to the database.
	*
	* @param batchId the primary key for the new batch
	* @return the new batch
	*/
	public info.diit.portal.batch.model.Batch create(long batchId);

	/**
	* Removes the batch with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchId the primary key of the batch
	* @return the batch that was removed
	* @throws info.diit.portal.batch.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch remove(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	public info.diit.portal.batch.model.Batch updateImpl(
		info.diit.portal.batch.model.Batch batch, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch with the primary key or throws a {@link info.diit.portal.batch.NoSuchBatchException} if it could not be found.
	*
	* @param batchId the primary key of the batch
	* @return the batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch findByPrimaryKey(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns the batch with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param batchId the primary key of the batch
	* @return the batch, or <code>null</code> if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch fetchByPrimaryKey(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the batchs where organizationId = &#63; and companyId = &#63;.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @return the matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findByBatchesByComOrg(
		long organizationId, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batchs where organizationId = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @return the range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findByBatchesByComOrg(
		long organizationId, long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batchs where organizationId = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findByBatchesByComOrg(
		long organizationId, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch findByBatchesByComOrg_First(
		long organizationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns the first batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch fetchByBatchesByComOrg_First(
		long organizationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch findByBatchesByComOrg_Last(
		long organizationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns the last batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch fetchByBatchesByComOrg_Last(
		long organizationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batchs before and after the current batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	*
	* @param batchId the primary key of the current batch
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch[] findByBatchesByComOrg_PrevAndNext(
		long batchId, long organizationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns all the batchs where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findByCompanyId(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batchs where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @return the range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findByCompanyId(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batchs where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findByCompanyId(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch findByCompanyId_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns the first batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch fetchByCompanyId_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch findByCompanyId_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns the last batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch fetchByCompanyId_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batchs before and after the current batch in the ordered set where companyId = &#63;.
	*
	* @param batchId the primary key of the current batch
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch[] findByCompanyId_PrevAndNext(
		long batchId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns the batch where organizationId = &#63; and companyId = &#63; and batchName = &#63; or throws a {@link info.diit.portal.batch.NoSuchBatchException} if it could not be found.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param batchName the batch name
	* @return the matching batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch findByBatchByCompanyOrg(
		long organizationId, long companyId, java.lang.String batchName)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns the batch where organizationId = &#63; and companyId = &#63; and batchName = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param batchName the batch name
	* @return the matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch fetchByBatchByCompanyOrg(
		long organizationId, long companyId, java.lang.String batchName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch where organizationId = &#63; and companyId = &#63; and batchName = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param batchName the batch name
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch fetchByBatchByCompanyOrg(
		long organizationId, long companyId, java.lang.String batchName,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch where batchId = &#63; or throws a {@link info.diit.portal.batch.NoSuchBatchException} if it could not be found.
	*
	* @param batchId the batch ID
	* @return the matching batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch findByBatchId(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns the batch where batchId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param batchId the batch ID
	* @return the matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch fetchByBatchId(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch where batchId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param batchId the batch ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch fetchByBatchId(long batchId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the batchs where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findByCourseId(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batchs where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @return the range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findByCourseId(
		long courseId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batchs where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findByCourseId(
		long courseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch findByCourseId_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns the first batch in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch fetchByCourseId_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch findByCourseId_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns the last batch in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch fetchByCourseId_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batchs before and after the current batch in the ordered set where courseId = &#63;.
	*
	* @param batchId the primary key of the current batch
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch[] findByCourseId_PrevAndNext(
		long batchId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns all the batchs where organizationId = &#63; and courseId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @return the matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findByBatchesByOrgCourse(
		long organizationId, long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batchs where organizationId = &#63; and courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @return the range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findByBatchesByOrgCourse(
		long organizationId, long courseId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batchs where organizationId = &#63; and courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findByBatchesByOrgCourse(
		long organizationId, long courseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch findByBatchesByOrgCourse_First(
		long organizationId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns the first batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch fetchByBatchesByOrgCourse_First(
		long organizationId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch findByBatchesByOrgCourse_Last(
		long organizationId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns the last batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch fetchByBatchesByOrgCourse_Last(
		long organizationId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batchs before and after the current batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	*
	* @param batchId the primary key of the current batch
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch
	* @throws info.diit.portal.batch.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch[] findByBatchesByOrgCourse_PrevAndNext(
		long batchId, long organizationId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Returns all the batchs.
	*
	* @return the batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batchs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @return the range of batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batchs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batchs where organizationId = &#63; and companyId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBatchesByComOrg(long organizationId, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batchs where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCompanyId(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the batch where organizationId = &#63; and companyId = &#63; and batchName = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param batchName the batch name
	* @return the batch that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch removeByBatchByCompanyOrg(
		long organizationId, long companyId, java.lang.String batchName)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Removes the batch where batchId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @return the batch that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch removeByBatchId(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.batch.NoSuchBatchException;

	/**
	* Removes all the batchs where courseId = &#63; from the database.
	*
	* @param courseId the course ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batchs where organizationId = &#63; and courseId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBatchesByOrgCourse(long organizationId, long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batchs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batchs where organizationId = &#63; and companyId = &#63;.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @return the number of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public int countByBatchesByComOrg(long organizationId, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batchs where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public int countByCompanyId(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batchs where organizationId = &#63; and companyId = &#63; and batchName = &#63;.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param batchName the batch name
	* @return the number of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public int countByBatchByCompanyOrg(long organizationId, long companyId,
		java.lang.String batchName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batchs where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the number of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public int countByBatchId(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batchs where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the number of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public int countByCourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batchs where organizationId = &#63; and courseId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @return the number of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public int countByBatchesByOrgCourse(long organizationId, long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batchs.
	*
	* @return the number of batchs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
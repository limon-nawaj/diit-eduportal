/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.batch.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link BatchLocalService}.
 * </p>
 *
 * @author    saeid
 * @see       BatchLocalService
 * @generated
 */
public class BatchLocalServiceWrapper implements BatchLocalService,
	ServiceWrapper<BatchLocalService> {
	public BatchLocalServiceWrapper(BatchLocalService batchLocalService) {
		_batchLocalService = batchLocalService;
	}

	/**
	* Adds the batch to the database. Also notifies the appropriate model listeners.
	*
	* @param batch the batch
	* @return the batch that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch addBatch(
		info.diit.portal.batch.model.Batch batch)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.addBatch(batch);
	}

	/**
	* Creates a new batch with the primary key. Does not add the batch to the database.
	*
	* @param batchId the primary key for the new batch
	* @return the new batch
	*/
	public info.diit.portal.batch.model.Batch createBatch(long batchId) {
		return _batchLocalService.createBatch(batchId);
	}

	/**
	* Deletes the batch with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchId the primary key of the batch
	* @return the batch that was removed
	* @throws PortalException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch deleteBatch(long batchId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.deleteBatch(batchId);
	}

	/**
	* Deletes the batch from the database. Also notifies the appropriate model listeners.
	*
	* @param batch the batch
	* @return the batch that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch deleteBatch(
		info.diit.portal.batch.model.Batch batch)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.deleteBatch(batch);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _batchLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.batch.model.Batch fetchBatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.fetchBatch(batchId);
	}

	/**
	* Returns the batch with the primary key.
	*
	* @param batchId the primary key of the batch
	* @return the batch
	* @throws PortalException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch getBatch(long batchId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.getBatch(batchId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the batchs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @return the range of batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.Batch> getBatchs(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.getBatchs(start, end);
	}

	/**
	* Returns the number of batchs.
	*
	* @return the number of batchs
	* @throws SystemException if a system exception occurred
	*/
	public int getBatchsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.getBatchsCount();
	}

	/**
	* Updates the batch in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param batch the batch
	* @return the batch that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch updateBatch(
		info.diit.portal.batch.model.Batch batch)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.updateBatch(batch);
	}

	/**
	* Updates the batch in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param batch the batch
	* @param merge whether to merge the batch with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the batch that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.Batch updateBatch(
		info.diit.portal.batch.model.Batch batch, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.updateBatch(batch, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _batchLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_batchLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _batchLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	public info.diit.portal.batch.model.Batch findBatchByComanyOrg(
		long companyId, long organizationId, java.lang.String batchName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.findBatchByComanyOrg(companyId,
			organizationId, batchName);
	}

	public java.util.List<info.diit.portal.batch.model.Batch> findBatchesByComOrg(
		long companyId, long orgId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.findBatchesByComOrg(companyId, orgId);
	}

	public java.util.List<info.diit.portal.batch.model.Batch> findBatchesByOrgCourseId(
		long organizationId, long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.findBatchesByOrgCourseId(organizationId,
			courseId);
	}

	public java.util.List<info.diit.portal.batch.model.Batch> findAllBatchByCompanyId(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.findAllBatchByCompanyId(companyId);
	}

	public java.util.List<info.diit.portal.batch.model.Batch> findBatchByCourseId(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchLocalService.findBatchByCourseId(courseId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public BatchLocalService getWrappedBatchLocalService() {
		return _batchLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedBatchLocalService(BatchLocalService batchLocalService) {
		_batchLocalService = batchLocalService;
	}

	public BatchLocalService getWrappedService() {
		return _batchLocalService;
	}

	public void setWrappedService(BatchLocalService batchLocalService) {
		_batchLocalService = batchLocalService;
	}

	private BatchLocalService _batchLocalService;
}
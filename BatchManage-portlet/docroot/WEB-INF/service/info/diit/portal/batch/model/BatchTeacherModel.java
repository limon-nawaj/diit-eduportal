/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.batch.model;

import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the BatchTeacher service. Represents a row in the &quot;EduPortal_Batch_BatchTeacher&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link info.diit.portal.batch.model.impl.BatchTeacherModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link info.diit.portal.batch.model.impl.BatchTeacherImpl}.
 * </p>
 *
 * @author saeid
 * @see BatchTeacher
 * @see info.diit.portal.batch.model.impl.BatchTeacherImpl
 * @see info.diit.portal.batch.model.impl.BatchTeacherModelImpl
 * @generated
 */
public interface BatchTeacherModel extends BaseModel<BatchTeacher> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a batch teacher model instance should use the {@link BatchTeacher} interface instead.
	 */

	/**
	 * Returns the primary key of this batch teacher.
	 *
	 * @return the primary key of this batch teacher
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this batch teacher.
	 *
	 * @param primaryKey the primary key of this batch teacher
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the batch teacher ID of this batch teacher.
	 *
	 * @return the batch teacher ID of this batch teacher
	 */
	public long getBatchTeacherId();

	/**
	 * Sets the batch teacher ID of this batch teacher.
	 *
	 * @param batchTeacherId the batch teacher ID of this batch teacher
	 */
	public void setBatchTeacherId(long batchTeacherId);

	/**
	 * Returns the teacher ID of this batch teacher.
	 *
	 * @return the teacher ID of this batch teacher
	 */
	public long getTeacherId();

	/**
	 * Sets the teacher ID of this batch teacher.
	 *
	 * @param teacherId the teacher ID of this batch teacher
	 */
	public void setTeacherId(long teacherId);

	/**
	 * Returns the start date of this batch teacher.
	 *
	 * @return the start date of this batch teacher
	 */
	public Date getStartDate();

	/**
	 * Sets the start date of this batch teacher.
	 *
	 * @param startDate the start date of this batch teacher
	 */
	public void setStartDate(Date startDate);

	/**
	 * Returns the end date of this batch teacher.
	 *
	 * @return the end date of this batch teacher
	 */
	public Date getEndDate();

	/**
	 * Sets the end date of this batch teacher.
	 *
	 * @param endDate the end date of this batch teacher
	 */
	public void setEndDate(Date endDate);

	/**
	 * Returns the batch ID of this batch teacher.
	 *
	 * @return the batch ID of this batch teacher
	 */
	public long getBatchId();

	/**
	 * Sets the batch ID of this batch teacher.
	 *
	 * @param batchId the batch ID of this batch teacher
	 */
	public void setBatchId(long batchId);

	public boolean isNew();

	public void setNew(boolean n);

	public boolean isCachedModel();

	public void setCachedModel(boolean cachedModel);

	public boolean isEscapedModel();

	public Serializable getPrimaryKeyObj();

	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	public ExpandoBridge getExpandoBridge();

	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	public Object clone();

	public int compareTo(BatchTeacher batchTeacher);

	public int hashCode();

	public CacheModel<BatchTeacher> toCacheModel();

	public BatchTeacher toEscapedModel();

	public String toString();

	public String toXmlString();
}
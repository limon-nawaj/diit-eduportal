/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.batch.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import info.diit.portal.batch.service.BatchTeacherLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author saeid
 */
public class BatchTeacherClp extends BaseModelImpl<BatchTeacher>
	implements BatchTeacher {
	public BatchTeacherClp() {
	}

	public Class<?> getModelClass() {
		return BatchTeacher.class;
	}

	public String getModelClassName() {
		return BatchTeacher.class.getName();
	}

	public long getPrimaryKey() {
		return _batchTeacherId;
	}

	public void setPrimaryKey(long primaryKey) {
		setBatchTeacherId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_batchTeacherId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("batchTeacherId", getBatchTeacherId());
		attributes.put("teacherId", getTeacherId());
		attributes.put("startDate", getStartDate());
		attributes.put("endDate", getEndDate());
		attributes.put("batchId", getBatchId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long batchTeacherId = (Long)attributes.get("batchTeacherId");

		if (batchTeacherId != null) {
			setBatchTeacherId(batchTeacherId);
		}

		Long teacherId = (Long)attributes.get("teacherId");

		if (teacherId != null) {
			setTeacherId(teacherId);
		}

		Date startDate = (Date)attributes.get("startDate");

		if (startDate != null) {
			setStartDate(startDate);
		}

		Date endDate = (Date)attributes.get("endDate");

		if (endDate != null) {
			setEndDate(endDate);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}
	}

	public long getBatchTeacherId() {
		return _batchTeacherId;
	}

	public void setBatchTeacherId(long batchTeacherId) {
		_batchTeacherId = batchTeacherId;
	}

	public long getTeacherId() {
		return _teacherId;
	}

	public void setTeacherId(long teacherId) {
		_teacherId = teacherId;
	}

	public Date getStartDate() {
		return _startDate;
	}

	public void setStartDate(Date startDate) {
		_startDate = startDate;
	}

	public Date getEndDate() {
		return _endDate;
	}

	public void setEndDate(Date endDate) {
		_endDate = endDate;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public BaseModel<?> getBatchTeacherRemoteModel() {
		return _batchTeacherRemoteModel;
	}

	public void setBatchTeacherRemoteModel(BaseModel<?> batchTeacherRemoteModel) {
		_batchTeacherRemoteModel = batchTeacherRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			BatchTeacherLocalServiceUtil.addBatchTeacher(this);
		}
		else {
			BatchTeacherLocalServiceUtil.updateBatchTeacher(this);
		}
	}

	@Override
	public BatchTeacher toEscapedModel() {
		return (BatchTeacher)Proxy.newProxyInstance(BatchTeacher.class.getClassLoader(),
			new Class[] { BatchTeacher.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		BatchTeacherClp clone = new BatchTeacherClp();

		clone.setBatchTeacherId(getBatchTeacherId());
		clone.setTeacherId(getTeacherId());
		clone.setStartDate(getStartDate());
		clone.setEndDate(getEndDate());
		clone.setBatchId(getBatchId());

		return clone;
	}

	public int compareTo(BatchTeacher batchTeacher) {
		long primaryKey = batchTeacher.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		BatchTeacherClp batchTeacher = null;

		try {
			batchTeacher = (BatchTeacherClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = batchTeacher.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{batchTeacherId=");
		sb.append(getBatchTeacherId());
		sb.append(", teacherId=");
		sb.append(getTeacherId());
		sb.append(", startDate=");
		sb.append(getStartDate());
		sb.append(", endDate=");
		sb.append(getEndDate());
		sb.append(", batchId=");
		sb.append(getBatchId());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.batch.model.BatchTeacher");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>batchTeacherId</column-name><column-value><![CDATA[");
		sb.append(getBatchTeacherId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>teacherId</column-name><column-value><![CDATA[");
		sb.append(getTeacherId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>startDate</column-name><column-value><![CDATA[");
		sb.append(getStartDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>endDate</column-name><column-value><![CDATA[");
		sb.append(getEndDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>batchId</column-name><column-value><![CDATA[");
		sb.append(getBatchId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _batchTeacherId;
	private long _teacherId;
	private Date _startDate;
	private Date _endDate;
	private long _batchId;
	private BaseModel<?> _batchTeacherRemoteModel;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.batch.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link BatchTeacherLocalService}.
 * </p>
 *
 * @author    saeid
 * @see       BatchTeacherLocalService
 * @generated
 */
public class BatchTeacherLocalServiceWrapper implements BatchTeacherLocalService,
	ServiceWrapper<BatchTeacherLocalService> {
	public BatchTeacherLocalServiceWrapper(
		BatchTeacherLocalService batchTeacherLocalService) {
		_batchTeacherLocalService = batchTeacherLocalService;
	}

	/**
	* Adds the batch teacher to the database. Also notifies the appropriate model listeners.
	*
	* @param batchTeacher the batch teacher
	* @return the batch teacher that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.BatchTeacher addBatchTeacher(
		info.diit.portal.batch.model.BatchTeacher batchTeacher)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchTeacherLocalService.addBatchTeacher(batchTeacher);
	}

	/**
	* Creates a new batch teacher with the primary key. Does not add the batch teacher to the database.
	*
	* @param batchTeacherId the primary key for the new batch teacher
	* @return the new batch teacher
	*/
	public info.diit.portal.batch.model.BatchTeacher createBatchTeacher(
		long batchTeacherId) {
		return _batchTeacherLocalService.createBatchTeacher(batchTeacherId);
	}

	/**
	* Deletes the batch teacher with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchTeacherId the primary key of the batch teacher
	* @return the batch teacher that was removed
	* @throws PortalException if a batch teacher with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.BatchTeacher deleteBatchTeacher(
		long batchTeacherId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _batchTeacherLocalService.deleteBatchTeacher(batchTeacherId);
	}

	/**
	* Deletes the batch teacher from the database. Also notifies the appropriate model listeners.
	*
	* @param batchTeacher the batch teacher
	* @return the batch teacher that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.BatchTeacher deleteBatchTeacher(
		info.diit.portal.batch.model.BatchTeacher batchTeacher)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchTeacherLocalService.deleteBatchTeacher(batchTeacher);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _batchTeacherLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchTeacherLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _batchTeacherLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchTeacherLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchTeacherLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.batch.model.BatchTeacher fetchBatchTeacher(
		long batchTeacherId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchTeacherLocalService.fetchBatchTeacher(batchTeacherId);
	}

	/**
	* Returns the batch teacher with the primary key.
	*
	* @param batchTeacherId the primary key of the batch teacher
	* @return the batch teacher
	* @throws PortalException if a batch teacher with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.BatchTeacher getBatchTeacher(
		long batchTeacherId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _batchTeacherLocalService.getBatchTeacher(batchTeacherId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _batchTeacherLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the batch teachers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch teachers
	* @param end the upper bound of the range of batch teachers (not inclusive)
	* @return the range of batch teachers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.batch.model.BatchTeacher> getBatchTeachers(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchTeacherLocalService.getBatchTeachers(start, end);
	}

	/**
	* Returns the number of batch teachers.
	*
	* @return the number of batch teachers
	* @throws SystemException if a system exception occurred
	*/
	public int getBatchTeachersCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchTeacherLocalService.getBatchTeachersCount();
	}

	/**
	* Updates the batch teacher in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param batchTeacher the batch teacher
	* @return the batch teacher that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.BatchTeacher updateBatchTeacher(
		info.diit.portal.batch.model.BatchTeacher batchTeacher)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchTeacherLocalService.updateBatchTeacher(batchTeacher);
	}

	/**
	* Updates the batch teacher in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param batchTeacher the batch teacher
	* @param merge whether to merge the batch teacher with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the batch teacher that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.batch.model.BatchTeacher updateBatchTeacher(
		info.diit.portal.batch.model.BatchTeacher batchTeacher, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchTeacherLocalService.updateBatchTeacher(batchTeacher, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _batchTeacherLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_batchTeacherLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _batchTeacherLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public BatchTeacherLocalService getWrappedBatchTeacherLocalService() {
		return _batchTeacherLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedBatchTeacherLocalService(
		BatchTeacherLocalService batchTeacherLocalService) {
		_batchTeacherLocalService = batchTeacherLocalService;
	}

	public BatchTeacherLocalService getWrappedService() {
		return _batchTeacherLocalService;
	}

	public void setWrappedService(
		BatchTeacherLocalService batchTeacherLocalService) {
		_batchTeacherLocalService = batchTeacherLocalService;
	}

	private BatchTeacherLocalService _batchTeacherLocalService;
}
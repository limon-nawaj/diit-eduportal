/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.batch.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Batch}.
 * </p>
 *
 * @author    saeid
 * @see       Batch
 * @generated
 */
public class BatchWrapper implements Batch, ModelWrapper<Batch> {
	public BatchWrapper(Batch batch) {
		_batch = batch;
	}

	public Class<?> getModelClass() {
		return Batch.class;
	}

	public String getModelClassName() {
		return Batch.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("batchId", getBatchId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("batchName", getBatchName());
		attributes.put("courseId", getCourseId());
		attributes.put("sessionId", getSessionId());
		attributes.put("startDate", getStartDate());
		attributes.put("endDate", getEndDate());
		attributes.put("batchTeacherId", getBatchTeacherId());
		attributes.put("status", getStatus());
		attributes.put("note", getNote());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String batchName = (String)attributes.get("batchName");

		if (batchName != null) {
			setBatchName(batchName);
		}

		Long courseId = (Long)attributes.get("courseId");

		if (courseId != null) {
			setCourseId(courseId);
		}

		Long sessionId = (Long)attributes.get("sessionId");

		if (sessionId != null) {
			setSessionId(sessionId);
		}

		Date startDate = (Date)attributes.get("startDate");

		if (startDate != null) {
			setStartDate(startDate);
		}

		Date endDate = (Date)attributes.get("endDate");

		if (endDate != null) {
			setEndDate(endDate);
		}

		Long batchTeacherId = (Long)attributes.get("batchTeacherId");

		if (batchTeacherId != null) {
			setBatchTeacherId(batchTeacherId);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}
	}

	/**
	* Returns the primary key of this batch.
	*
	* @return the primary key of this batch
	*/
	public long getPrimaryKey() {
		return _batch.getPrimaryKey();
	}

	/**
	* Sets the primary key of this batch.
	*
	* @param primaryKey the primary key of this batch
	*/
	public void setPrimaryKey(long primaryKey) {
		_batch.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the batch ID of this batch.
	*
	* @return the batch ID of this batch
	*/
	public long getBatchId() {
		return _batch.getBatchId();
	}

	/**
	* Sets the batch ID of this batch.
	*
	* @param batchId the batch ID of this batch
	*/
	public void setBatchId(long batchId) {
		_batch.setBatchId(batchId);
	}

	/**
	* Returns the company ID of this batch.
	*
	* @return the company ID of this batch
	*/
	public long getCompanyId() {
		return _batch.getCompanyId();
	}

	/**
	* Sets the company ID of this batch.
	*
	* @param companyId the company ID of this batch
	*/
	public void setCompanyId(long companyId) {
		_batch.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this batch.
	*
	* @return the organization ID of this batch
	*/
	public long getOrganizationId() {
		return _batch.getOrganizationId();
	}

	/**
	* Sets the organization ID of this batch.
	*
	* @param organizationId the organization ID of this batch
	*/
	public void setOrganizationId(long organizationId) {
		_batch.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this batch.
	*
	* @return the user ID of this batch
	*/
	public long getUserId() {
		return _batch.getUserId();
	}

	/**
	* Sets the user ID of this batch.
	*
	* @param userId the user ID of this batch
	*/
	public void setUserId(long userId) {
		_batch.setUserId(userId);
	}

	/**
	* Returns the user uuid of this batch.
	*
	* @return the user uuid of this batch
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batch.getUserUuid();
	}

	/**
	* Sets the user uuid of this batch.
	*
	* @param userUuid the user uuid of this batch
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_batch.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this batch.
	*
	* @return the user name of this batch
	*/
	public java.lang.String getUserName() {
		return _batch.getUserName();
	}

	/**
	* Sets the user name of this batch.
	*
	* @param userName the user name of this batch
	*/
	public void setUserName(java.lang.String userName) {
		_batch.setUserName(userName);
	}

	/**
	* Returns the create date of this batch.
	*
	* @return the create date of this batch
	*/
	public java.util.Date getCreateDate() {
		return _batch.getCreateDate();
	}

	/**
	* Sets the create date of this batch.
	*
	* @param createDate the create date of this batch
	*/
	public void setCreateDate(java.util.Date createDate) {
		_batch.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this batch.
	*
	* @return the modified date of this batch
	*/
	public java.util.Date getModifiedDate() {
		return _batch.getModifiedDate();
	}

	/**
	* Sets the modified date of this batch.
	*
	* @param modifiedDate the modified date of this batch
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_batch.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the batch name of this batch.
	*
	* @return the batch name of this batch
	*/
	public java.lang.String getBatchName() {
		return _batch.getBatchName();
	}

	/**
	* Sets the batch name of this batch.
	*
	* @param batchName the batch name of this batch
	*/
	public void setBatchName(java.lang.String batchName) {
		_batch.setBatchName(batchName);
	}

	/**
	* Returns the course ID of this batch.
	*
	* @return the course ID of this batch
	*/
	public long getCourseId() {
		return _batch.getCourseId();
	}

	/**
	* Sets the course ID of this batch.
	*
	* @param courseId the course ID of this batch
	*/
	public void setCourseId(long courseId) {
		_batch.setCourseId(courseId);
	}

	/**
	* Returns the session ID of this batch.
	*
	* @return the session ID of this batch
	*/
	public long getSessionId() {
		return _batch.getSessionId();
	}

	/**
	* Sets the session ID of this batch.
	*
	* @param sessionId the session ID of this batch
	*/
	public void setSessionId(long sessionId) {
		_batch.setSessionId(sessionId);
	}

	/**
	* Returns the start date of this batch.
	*
	* @return the start date of this batch
	*/
	public java.util.Date getStartDate() {
		return _batch.getStartDate();
	}

	/**
	* Sets the start date of this batch.
	*
	* @param startDate the start date of this batch
	*/
	public void setStartDate(java.util.Date startDate) {
		_batch.setStartDate(startDate);
	}

	/**
	* Returns the end date of this batch.
	*
	* @return the end date of this batch
	*/
	public java.util.Date getEndDate() {
		return _batch.getEndDate();
	}

	/**
	* Sets the end date of this batch.
	*
	* @param endDate the end date of this batch
	*/
	public void setEndDate(java.util.Date endDate) {
		_batch.setEndDate(endDate);
	}

	/**
	* Returns the batch teacher ID of this batch.
	*
	* @return the batch teacher ID of this batch
	*/
	public long getBatchTeacherId() {
		return _batch.getBatchTeacherId();
	}

	/**
	* Sets the batch teacher ID of this batch.
	*
	* @param batchTeacherId the batch teacher ID of this batch
	*/
	public void setBatchTeacherId(long batchTeacherId) {
		_batch.setBatchTeacherId(batchTeacherId);
	}

	/**
	* Returns the status of this batch.
	*
	* @return the status of this batch
	*/
	public int getStatus() {
		return _batch.getStatus();
	}

	/**
	* Sets the status of this batch.
	*
	* @param status the status of this batch
	*/
	public void setStatus(int status) {
		_batch.setStatus(status);
	}

	/**
	* Returns the note of this batch.
	*
	* @return the note of this batch
	*/
	public java.lang.String getNote() {
		return _batch.getNote();
	}

	/**
	* Sets the note of this batch.
	*
	* @param note the note of this batch
	*/
	public void setNote(java.lang.String note) {
		_batch.setNote(note);
	}

	public boolean isNew() {
		return _batch.isNew();
	}

	public void setNew(boolean n) {
		_batch.setNew(n);
	}

	public boolean isCachedModel() {
		return _batch.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_batch.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _batch.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _batch.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_batch.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _batch.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_batch.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new BatchWrapper((Batch)_batch.clone());
	}

	public int compareTo(info.diit.portal.batch.model.Batch batch) {
		return _batch.compareTo(batch);
	}

	@Override
	public int hashCode() {
		return _batch.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.batch.model.Batch> toCacheModel() {
		return _batch.toCacheModel();
	}

	public info.diit.portal.batch.model.Batch toEscapedModel() {
		return new BatchWrapper(_batch.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _batch.toString();
	}

	public java.lang.String toXmlString() {
		return _batch.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_batch.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Batch getWrappedBatch() {
		return _batch;
	}

	public Batch getWrappedModel() {
		return _batch;
	}

	public void resetOriginalValues() {
		_batch.resetOriginalValues();
	}

	private Batch _batch;
}
package info.diit.portal.batch.dto;

import java.io.Serializable;

public class CourseDto implements Serializable{
	public long courseId;
	public String courseName;
	
	public CourseDto(){
	}
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String toString(){
		return getCourseName();
	}
	
}

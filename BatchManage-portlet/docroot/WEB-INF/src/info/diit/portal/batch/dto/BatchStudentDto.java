package info.diit.portal.batch.dto;

import java.io.Serializable;

import info.diit.portal.student.service.model.BatchStudent;
import info.diit.portal.student.service.model.Student;

public class BatchStudentDto implements Serializable {
	long   		studentId;
	String 		name;
	boolean 	check;
	long 		batchId;
	long 		organizationId;	
	long		newBatchId;
	long 		userId;
	long 		companyId;
	long 		batchStudentId;

	public long getNewBatchId() {
		return newBatchId;
	}

	public void setNewBatchId(long newBatchId) {
		this.newBatchId = newBatchId;
	}

	public BatchStudentDto(){
		
	}
	
	public BatchStudentDto (Student student){
		this.studentId = student.getStudentId();
		this.name = student.getName();
	}
	
	public BatchStudentDto(Student student, BatchStudent batchStudent){
		this.studentId = student.getStudentId();
		this.name = student.getName();
		this.batchId = batchStudent.getBatchId();
		this.organizationId = batchStudent.getOrganizationId();
		this.userId = student.getUserId();
		this.companyId = student.getCompanyId();
		this.batchStudentId = batchStudent.getBatchStudentId();
	}
	
	public long getBatchStudentId() {
		return batchStudentId;
	}

	public void setBatchStudentId(long batchStudentId) {
		this.batchStudentId = batchStudentId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getStudentId() {
		return studentId;
	}
	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isCheck() {
		return check;
	}
	public void setCheck(boolean check) {
		this.check = check;
	}
	public long getBatchId() {
		return batchId;
	}

	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

}

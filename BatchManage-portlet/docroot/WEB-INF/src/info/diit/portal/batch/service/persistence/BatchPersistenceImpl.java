/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.batch.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.batch.NoSuchBatchException;
import info.diit.portal.batch.model.Batch;
import info.diit.portal.batch.model.impl.BatchImpl;
import info.diit.portal.batch.model.impl.BatchModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the batch service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author saeid
 * @see BatchPersistence
 * @see BatchUtil
 * @generated
 */
public class BatchPersistenceImpl extends BasePersistenceImpl<Batch>
	implements BatchPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link BatchUtil} to access the batch persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = BatchImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHESBYCOMORG =
		new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, BatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBatchesByComOrg",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYCOMORG =
		new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, BatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBatchesByComOrg",
			new String[] { Long.class.getName(), Long.class.getName() },
			BatchModelImpl.ORGANIZATIONID_COLUMN_BITMASK |
			BatchModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCHESBYCOMORG = new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBatchesByComOrg",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, BatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, BatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyId",
			new String[] { Long.class.getName() },
			BatchModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FETCH_BY_BATCHBYCOMPANYORG = new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, BatchImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByBatchByCompanyOrg",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			},
			BatchModelImpl.ORGANIZATIONID_COLUMN_BITMASK |
			BatchModelImpl.COMPANYID_COLUMN_BITMASK |
			BatchModelImpl.BATCHNAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCHBYCOMPANYORG = new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBatchByCompanyOrg",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			});
	public static final FinderPath FINDER_PATH_FETCH_BY_BATCHID = new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, BatchImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByBatchId",
			new String[] { Long.class.getName() },
			BatchModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCHID = new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBatchId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COURSEID = new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, BatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCourseId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID =
		new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, BatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCourseId",
			new String[] { Long.class.getName() },
			BatchModelImpl.COURSEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COURSEID = new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCourseId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHESBYORGCOURSE =
		new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, BatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBatchesByOrgCourse",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYORGCOURSE =
		new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, BatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBatchesByOrgCourse",
			new String[] { Long.class.getName(), Long.class.getName() },
			BatchModelImpl.ORGANIZATIONID_COLUMN_BITMASK |
			BatchModelImpl.COURSEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCHESBYORGCOURSE = new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBatchesByOrgCourse",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, BatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, BatchImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the batch in the entity cache if it is enabled.
	 *
	 * @param batch the batch
	 */
	public void cacheResult(Batch batch) {
		EntityCacheUtil.putResult(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchImpl.class, batch.getPrimaryKey(), batch);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BATCHBYCOMPANYORG,
			new Object[] {
				Long.valueOf(batch.getOrganizationId()),
				Long.valueOf(batch.getCompanyId()),
				
			batch.getBatchName()
			}, batch);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BATCHID,
			new Object[] { Long.valueOf(batch.getBatchId()) }, batch);

		batch.resetOriginalValues();
	}

	/**
	 * Caches the batchs in the entity cache if it is enabled.
	 *
	 * @param batchs the batchs
	 */
	public void cacheResult(List<Batch> batchs) {
		for (Batch batch : batchs) {
			if (EntityCacheUtil.getResult(BatchModelImpl.ENTITY_CACHE_ENABLED,
						BatchImpl.class, batch.getPrimaryKey()) == null) {
				cacheResult(batch);
			}
			else {
				batch.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all batchs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(BatchImpl.class.getName());
		}

		EntityCacheUtil.clearCache(BatchImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the batch.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Batch batch) {
		EntityCacheUtil.removeResult(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchImpl.class, batch.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(batch);
	}

	@Override
	public void clearCache(List<Batch> batchs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Batch batch : batchs) {
			EntityCacheUtil.removeResult(BatchModelImpl.ENTITY_CACHE_ENABLED,
				BatchImpl.class, batch.getPrimaryKey());

			clearUniqueFindersCache(batch);
		}
	}

	protected void clearUniqueFindersCache(Batch batch) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BATCHBYCOMPANYORG,
			new Object[] {
				Long.valueOf(batch.getOrganizationId()),
				Long.valueOf(batch.getCompanyId()),
				
			batch.getBatchName()
			});

		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BATCHID,
			new Object[] { Long.valueOf(batch.getBatchId()) });
	}

	/**
	 * Creates a new batch with the primary key. Does not add the batch to the database.
	 *
	 * @param batchId the primary key for the new batch
	 * @return the new batch
	 */
	public Batch create(long batchId) {
		Batch batch = new BatchImpl();

		batch.setNew(true);
		batch.setPrimaryKey(batchId);

		return batch;
	}

	/**
	 * Removes the batch with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param batchId the primary key of the batch
	 * @return the batch that was removed
	 * @throws info.diit.portal.batch.NoSuchBatchException if a batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch remove(long batchId)
		throws NoSuchBatchException, SystemException {
		return remove(Long.valueOf(batchId));
	}

	/**
	 * Removes the batch with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the batch
	 * @return the batch that was removed
	 * @throws info.diit.portal.batch.NoSuchBatchException if a batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Batch remove(Serializable primaryKey)
		throws NoSuchBatchException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Batch batch = (Batch)session.get(BatchImpl.class, primaryKey);

			if (batch == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchBatchException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(batch);
		}
		catch (NoSuchBatchException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Batch removeImpl(Batch batch) throws SystemException {
		batch = toUnwrappedModel(batch);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, batch);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(batch);

		return batch;
	}

	@Override
	public Batch updateImpl(info.diit.portal.batch.model.Batch batch,
		boolean merge) throws SystemException {
		batch = toUnwrappedModel(batch);

		boolean isNew = batch.isNew();

		BatchModelImpl batchModelImpl = (BatchModelImpl)batch;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, batch, merge);

			batch.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !BatchModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((batchModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYCOMORG.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchModelImpl.getOriginalOrganizationId()),
						Long.valueOf(batchModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHESBYCOMORG,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYCOMORG,
					args);

				args = new Object[] {
						Long.valueOf(batchModelImpl.getOrganizationId()),
						Long.valueOf(batchModelImpl.getCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHESBYCOMORG,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYCOMORG,
					args);
			}

			if ((batchModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] { Long.valueOf(batchModelImpl.getCompanyId()) };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}

			if ((batchModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchModelImpl.getOriginalCourseId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COURSEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID,
					args);

				args = new Object[] { Long.valueOf(batchModelImpl.getCourseId()) };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COURSEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID,
					args);
			}

			if ((batchModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYORGCOURSE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchModelImpl.getOriginalOrganizationId()),
						Long.valueOf(batchModelImpl.getOriginalCourseId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHESBYORGCOURSE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYORGCOURSE,
					args);

				args = new Object[] {
						Long.valueOf(batchModelImpl.getOrganizationId()),
						Long.valueOf(batchModelImpl.getCourseId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHESBYORGCOURSE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYORGCOURSE,
					args);
			}
		}

		EntityCacheUtil.putResult(BatchModelImpl.ENTITY_CACHE_ENABLED,
			BatchImpl.class, batch.getPrimaryKey(), batch);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BATCHBYCOMPANYORG,
				new Object[] {
					Long.valueOf(batch.getOrganizationId()),
					Long.valueOf(batch.getCompanyId()),
					
				batch.getBatchName()
				}, batch);

			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BATCHID,
				new Object[] { Long.valueOf(batch.getBatchId()) }, batch);
		}
		else {
			if ((batchModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_BATCHBYCOMPANYORG.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchModelImpl.getOriginalOrganizationId()),
						Long.valueOf(batchModelImpl.getOriginalCompanyId()),
						
						batchModelImpl.getOriginalBatchName()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHBYCOMPANYORG,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BATCHBYCOMPANYORG,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BATCHBYCOMPANYORG,
					new Object[] {
						Long.valueOf(batch.getOrganizationId()),
						Long.valueOf(batch.getCompanyId()),
						
					batch.getBatchName()
					}, batch);
			}

			if ((batchModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_BATCHID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHID, args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BATCHID, args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BATCHID,
					new Object[] { Long.valueOf(batch.getBatchId()) }, batch);
			}
		}

		return batch;
	}

	protected Batch toUnwrappedModel(Batch batch) {
		if (batch instanceof BatchImpl) {
			return batch;
		}

		BatchImpl batchImpl = new BatchImpl();

		batchImpl.setNew(batch.isNew());
		batchImpl.setPrimaryKey(batch.getPrimaryKey());

		batchImpl.setBatchId(batch.getBatchId());
		batchImpl.setCompanyId(batch.getCompanyId());
		batchImpl.setOrganizationId(batch.getOrganizationId());
		batchImpl.setUserId(batch.getUserId());
		batchImpl.setUserName(batch.getUserName());
		batchImpl.setCreateDate(batch.getCreateDate());
		batchImpl.setModifiedDate(batch.getModifiedDate());
		batchImpl.setBatchName(batch.getBatchName());
		batchImpl.setCourseId(batch.getCourseId());
		batchImpl.setSessionId(batch.getSessionId());
		batchImpl.setStartDate(batch.getStartDate());
		batchImpl.setEndDate(batch.getEndDate());
		batchImpl.setBatchTeacherId(batch.getBatchTeacherId());
		batchImpl.setStatus(batch.getStatus());
		batchImpl.setNote(batch.getNote());

		return batchImpl;
	}

	/**
	 * Returns the batch with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the batch
	 * @return the batch
	 * @throws com.liferay.portal.NoSuchModelException if a batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Batch findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the batch with the primary key or throws a {@link info.diit.portal.batch.NoSuchBatchException} if it could not be found.
	 *
	 * @param batchId the primary key of the batch
	 * @return the batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch findByPrimaryKey(long batchId)
		throws NoSuchBatchException, SystemException {
		Batch batch = fetchByPrimaryKey(batchId);

		if (batch == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + batchId);
			}

			throw new NoSuchBatchException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				batchId);
		}

		return batch;
	}

	/**
	 * Returns the batch with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the batch
	 * @return the batch, or <code>null</code> if a batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Batch fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the batch with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param batchId the primary key of the batch
	 * @return the batch, or <code>null</code> if a batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch fetchByPrimaryKey(long batchId) throws SystemException {
		Batch batch = (Batch)EntityCacheUtil.getResult(BatchModelImpl.ENTITY_CACHE_ENABLED,
				BatchImpl.class, batchId);

		if (batch == _nullBatch) {
			return null;
		}

		if (batch == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				batch = (Batch)session.get(BatchImpl.class,
						Long.valueOf(batchId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (batch != null) {
					cacheResult(batch);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(BatchModelImpl.ENTITY_CACHE_ENABLED,
						BatchImpl.class, batchId, _nullBatch);
				}

				closeSession(session);
			}
		}

		return batch;
	}

	/**
	 * Returns all the batchs where organizationId = &#63; and companyId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @return the matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findByBatchesByComOrg(long organizationId, long companyId)
		throws SystemException {
		return findByBatchesByComOrg(organizationId, companyId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batchs where organizationId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of batchs
	 * @param end the upper bound of the range of batchs (not inclusive)
	 * @return the range of matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findByBatchesByComOrg(long organizationId,
		long companyId, int start, int end) throws SystemException {
		return findByBatchesByComOrg(organizationId, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the batchs where organizationId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of batchs
	 * @param end the upper bound of the range of batchs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findByBatchesByComOrg(long organizationId,
		long companyId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYCOMORG;
			finderArgs = new Object[] { organizationId, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHESBYCOMORG;
			finderArgs = new Object[] {
					organizationId, companyId,
					
					start, end, orderByComparator
				};
		}

		List<Batch> list = (List<Batch>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Batch batch : list) {
				if ((organizationId != batch.getOrganizationId()) ||
						(companyId != batch.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_BATCH_WHERE);

			query.append(_FINDER_COLUMN_BATCHESBYCOMORG_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_BATCHESBYCOMORG_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BatchModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(companyId);

				list = (List<Batch>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch findByBatchesByComOrg_First(long organizationId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchBatchException, SystemException {
		Batch batch = fetchByBatchesByComOrg_First(organizationId, companyId,
				orderByComparator);

		if (batch != null) {
			return batch;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchException(msg.toString());
	}

	/**
	 * Returns the first batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch, or <code>null</code> if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch fetchByBatchesByComOrg_First(long organizationId,
		long companyId, OrderByComparator orderByComparator)
		throws SystemException {
		List<Batch> list = findByBatchesByComOrg(organizationId, companyId, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch findByBatchesByComOrg_Last(long organizationId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchBatchException, SystemException {
		Batch batch = fetchByBatchesByComOrg_Last(organizationId, companyId,
				orderByComparator);

		if (batch != null) {
			return batch;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchException(msg.toString());
	}

	/**
	 * Returns the last batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch, or <code>null</code> if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch fetchByBatchesByComOrg_Last(long organizationId,
		long companyId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByBatchesByComOrg(organizationId, companyId);

		List<Batch> list = findByBatchesByComOrg(organizationId, companyId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batchs before and after the current batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	 *
	 * @param batchId the primary key of the current batch
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch[] findByBatchesByComOrg_PrevAndNext(long batchId,
		long organizationId, long companyId, OrderByComparator orderByComparator)
		throws NoSuchBatchException, SystemException {
		Batch batch = findByPrimaryKey(batchId);

		Session session = null;

		try {
			session = openSession();

			Batch[] array = new BatchImpl[3];

			array[0] = getByBatchesByComOrg_PrevAndNext(session, batch,
					organizationId, companyId, orderByComparator, true);

			array[1] = batch;

			array[2] = getByBatchesByComOrg_PrevAndNext(session, batch,
					organizationId, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Batch getByBatchesByComOrg_PrevAndNext(Session session,
		Batch batch, long organizationId, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCH_WHERE);

		query.append(_FINDER_COLUMN_BATCHESBYCOMORG_ORGANIZATIONID_2);

		query.append(_FINDER_COLUMN_BATCHESBYCOMORG_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BatchModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batch);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Batch> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the batchs where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findByCompanyId(long companyId)
		throws SystemException {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the batchs where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of batchs
	 * @param end the upper bound of the range of batchs (not inclusive)
	 * @return the range of matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findByCompanyId(long companyId, int start, int end)
		throws SystemException {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the batchs where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of batchs
	 * @param end the upper bound of the range of batchs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findByCompanyId(long companyId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<Batch> list = (List<Batch>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Batch batch : list) {
				if ((companyId != batch.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BATCH_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BatchModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<Batch>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch findByCompanyId_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchException, SystemException {
		Batch batch = fetchByCompanyId_First(companyId, orderByComparator);

		if (batch != null) {
			return batch;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchException(msg.toString());
	}

	/**
	 * Returns the first batch in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch, or <code>null</code> if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch fetchByCompanyId_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Batch> list = findByCompanyId(companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch findByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchException, SystemException {
		Batch batch = fetchByCompanyId_Last(companyId, orderByComparator);

		if (batch != null) {
			return batch;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchException(msg.toString());
	}

	/**
	 * Returns the last batch in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch, or <code>null</code> if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch fetchByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompanyId(companyId);

		List<Batch> list = findByCompanyId(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batchs before and after the current batch in the ordered set where companyId = &#63;.
	 *
	 * @param batchId the primary key of the current batch
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch[] findByCompanyId_PrevAndNext(long batchId, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchException, SystemException {
		Batch batch = findByPrimaryKey(batchId);

		Session session = null;

		try {
			session = openSession();

			Batch[] array = new BatchImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session, batch, companyId,
					orderByComparator, true);

			array[1] = batch;

			array[2] = getByCompanyId_PrevAndNext(session, batch, companyId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Batch getByCompanyId_PrevAndNext(Session session, Batch batch,
		long companyId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCH_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BatchModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batch);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Batch> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns the batch where organizationId = &#63; and companyId = &#63; and batchName = &#63; or throws a {@link info.diit.portal.batch.NoSuchBatchException} if it could not be found.
	 *
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @param batchName the batch name
	 * @return the matching batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch findByBatchByCompanyOrg(long organizationId, long companyId,
		String batchName) throws NoSuchBatchException, SystemException {
		Batch batch = fetchByBatchByCompanyOrg(organizationId, companyId,
				batchName);

		if (batch == null) {
			StringBundler msg = new StringBundler(8);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("organizationId=");
			msg.append(organizationId);

			msg.append(", companyId=");
			msg.append(companyId);

			msg.append(", batchName=");
			msg.append(batchName);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchBatchException(msg.toString());
		}

		return batch;
	}

	/**
	 * Returns the batch where organizationId = &#63; and companyId = &#63; and batchName = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @param batchName the batch name
	 * @return the matching batch, or <code>null</code> if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch fetchByBatchByCompanyOrg(long organizationId, long companyId,
		String batchName) throws SystemException {
		return fetchByBatchByCompanyOrg(organizationId, companyId, batchName,
			true);
	}

	/**
	 * Returns the batch where organizationId = &#63; and companyId = &#63; and batchName = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @param batchName the batch name
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching batch, or <code>null</code> if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch fetchByBatchByCompanyOrg(long organizationId, long companyId,
		String batchName, boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { organizationId, companyId, batchName };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_BATCHBYCOMPANYORG,
					finderArgs, this);
		}

		if (result instanceof Batch) {
			Batch batch = (Batch)result;

			if ((organizationId != batch.getOrganizationId()) ||
					(companyId != batch.getCompanyId()) ||
					!Validator.equals(batchName, batch.getBatchName())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(5);

			query.append(_SQL_SELECT_BATCH_WHERE);

			query.append(_FINDER_COLUMN_BATCHBYCOMPANYORG_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_BATCHBYCOMPANYORG_COMPANYID_2);

			if (batchName == null) {
				query.append(_FINDER_COLUMN_BATCHBYCOMPANYORG_BATCHNAME_1);
			}
			else {
				if (batchName.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BATCHBYCOMPANYORG_BATCHNAME_3);
				}
				else {
					query.append(_FINDER_COLUMN_BATCHBYCOMPANYORG_BATCHNAME_2);
				}
			}

			query.append(BatchModelImpl.ORDER_BY_JPQL);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(companyId);

				if (batchName != null) {
					qPos.add(batchName);
				}

				List<Batch> list = q.list();

				result = list;

				Batch batch = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BATCHBYCOMPANYORG,
						finderArgs, list);
				}
				else {
					batch = list.get(0);

					cacheResult(batch);

					if ((batch.getOrganizationId() != organizationId) ||
							(batch.getCompanyId() != companyId) ||
							(batch.getBatchName() == null) ||
							!batch.getBatchName().equals(batchName)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BATCHBYCOMPANYORG,
							finderArgs, batch);
					}
				}

				return batch;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BATCHBYCOMPANYORG,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (Batch)result;
			}
		}
	}

	/**
	 * Returns the batch where batchId = &#63; or throws a {@link info.diit.portal.batch.NoSuchBatchException} if it could not be found.
	 *
	 * @param batchId the batch ID
	 * @return the matching batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch findByBatchId(long batchId)
		throws NoSuchBatchException, SystemException {
		Batch batch = fetchByBatchId(batchId);

		if (batch == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("batchId=");
			msg.append(batchId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchBatchException(msg.toString());
		}

		return batch;
	}

	/**
	 * Returns the batch where batchId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param batchId the batch ID
	 * @return the matching batch, or <code>null</code> if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch fetchByBatchId(long batchId) throws SystemException {
		return fetchByBatchId(batchId, true);
	}

	/**
	 * Returns the batch where batchId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param batchId the batch ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching batch, or <code>null</code> if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch fetchByBatchId(long batchId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { batchId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_BATCHID,
					finderArgs, this);
		}

		if (result instanceof Batch) {
			Batch batch = (Batch)result;

			if ((batchId != batch.getBatchId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_BATCH_WHERE);

			query.append(_FINDER_COLUMN_BATCHID_BATCHID_2);

			query.append(BatchModelImpl.ORDER_BY_JPQL);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				List<Batch> list = q.list();

				result = list;

				Batch batch = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BATCHID,
						finderArgs, list);
				}
				else {
					batch = list.get(0);

					cacheResult(batch);

					if ((batch.getBatchId() != batchId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BATCHID,
							finderArgs, batch);
					}
				}

				return batch;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BATCHID,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (Batch)result;
			}
		}
	}

	/**
	 * Returns all the batchs where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @return the matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findByCourseId(long courseId) throws SystemException {
		return findByCourseId(courseId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the batchs where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course ID
	 * @param start the lower bound of the range of batchs
	 * @param end the upper bound of the range of batchs (not inclusive)
	 * @return the range of matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findByCourseId(long courseId, int start, int end)
		throws SystemException {
		return findByCourseId(courseId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the batchs where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course ID
	 * @param start the lower bound of the range of batchs
	 * @param end the upper bound of the range of batchs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findByCourseId(long courseId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID;
			finderArgs = new Object[] { courseId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COURSEID;
			finderArgs = new Object[] { courseId, start, end, orderByComparator };
		}

		List<Batch> list = (List<Batch>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Batch batch : list) {
				if ((courseId != batch.getCourseId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BATCH_WHERE);

			query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BatchModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				list = (List<Batch>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch findByCourseId_First(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchException, SystemException {
		Batch batch = fetchByCourseId_First(courseId, orderByComparator);

		if (batch != null) {
			return batch;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("courseId=");
		msg.append(courseId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchException(msg.toString());
	}

	/**
	 * Returns the first batch in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch, or <code>null</code> if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch fetchByCourseId_First(long courseId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Batch> list = findByCourseId(courseId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch findByCourseId_Last(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchException, SystemException {
		Batch batch = fetchByCourseId_Last(courseId, orderByComparator);

		if (batch != null) {
			return batch;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("courseId=");
		msg.append(courseId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchException(msg.toString());
	}

	/**
	 * Returns the last batch in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch, or <code>null</code> if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch fetchByCourseId_Last(long courseId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCourseId(courseId);

		List<Batch> list = findByCourseId(courseId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batchs before and after the current batch in the ordered set where courseId = &#63;.
	 *
	 * @param batchId the primary key of the current batch
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch[] findByCourseId_PrevAndNext(long batchId, long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchException, SystemException {
		Batch batch = findByPrimaryKey(batchId);

		Session session = null;

		try {
			session = openSession();

			Batch[] array = new BatchImpl[3];

			array[0] = getByCourseId_PrevAndNext(session, batch, courseId,
					orderByComparator, true);

			array[1] = batch;

			array[2] = getByCourseId_PrevAndNext(session, batch, courseId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Batch getByCourseId_PrevAndNext(Session session, Batch batch,
		long courseId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCH_WHERE);

		query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BatchModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(courseId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batch);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Batch> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the batchs where organizationId = &#63; and courseId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @return the matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findByBatchesByOrgCourse(long organizationId,
		long courseId) throws SystemException {
		return findByBatchesByOrgCourse(organizationId, courseId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batchs where organizationId = &#63; and courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param start the lower bound of the range of batchs
	 * @param end the upper bound of the range of batchs (not inclusive)
	 * @return the range of matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findByBatchesByOrgCourse(long organizationId,
		long courseId, int start, int end) throws SystemException {
		return findByBatchesByOrgCourse(organizationId, courseId, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the batchs where organizationId = &#63; and courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param start the lower bound of the range of batchs
	 * @param end the upper bound of the range of batchs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findByBatchesByOrgCourse(long organizationId,
		long courseId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYORGCOURSE;
			finderArgs = new Object[] { organizationId, courseId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHESBYORGCOURSE;
			finderArgs = new Object[] {
					organizationId, courseId,
					
					start, end, orderByComparator
				};
		}

		List<Batch> list = (List<Batch>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Batch batch : list) {
				if ((organizationId != batch.getOrganizationId()) ||
						(courseId != batch.getCourseId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_BATCH_WHERE);

			query.append(_FINDER_COLUMN_BATCHESBYORGCOURSE_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_BATCHESBYORGCOURSE_COURSEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(BatchModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(courseId);

				list = (List<Batch>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch findByBatchesByOrgCourse_First(long organizationId,
		long courseId, OrderByComparator orderByComparator)
		throws NoSuchBatchException, SystemException {
		Batch batch = fetchByBatchesByOrgCourse_First(organizationId, courseId,
				orderByComparator);

		if (batch != null) {
			return batch;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(", courseId=");
		msg.append(courseId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchException(msg.toString());
	}

	/**
	 * Returns the first batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch, or <code>null</code> if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch fetchByBatchesByOrgCourse_First(long organizationId,
		long courseId, OrderByComparator orderByComparator)
		throws SystemException {
		List<Batch> list = findByBatchesByOrgCourse(organizationId, courseId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch findByBatchesByOrgCourse_Last(long organizationId,
		long courseId, OrderByComparator orderByComparator)
		throws NoSuchBatchException, SystemException {
		Batch batch = fetchByBatchesByOrgCourse_Last(organizationId, courseId,
				orderByComparator);

		if (batch != null) {
			return batch;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(", courseId=");
		msg.append(courseId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchException(msg.toString());
	}

	/**
	 * Returns the last batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch, or <code>null</code> if a matching batch could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch fetchByBatchesByOrgCourse_Last(long organizationId,
		long courseId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByBatchesByOrgCourse(organizationId, courseId);

		List<Batch> list = findByBatchesByOrgCourse(organizationId, courseId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batchs before and after the current batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	 *
	 * @param batchId the primary key of the current batch
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch
	 * @throws info.diit.portal.batch.NoSuchBatchException if a batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch[] findByBatchesByOrgCourse_PrevAndNext(long batchId,
		long organizationId, long courseId, OrderByComparator orderByComparator)
		throws NoSuchBatchException, SystemException {
		Batch batch = findByPrimaryKey(batchId);

		Session session = null;

		try {
			session = openSession();

			Batch[] array = new BatchImpl[3];

			array[0] = getByBatchesByOrgCourse_PrevAndNext(session, batch,
					organizationId, courseId, orderByComparator, true);

			array[1] = batch;

			array[2] = getByBatchesByOrgCourse_PrevAndNext(session, batch,
					organizationId, courseId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Batch getByBatchesByOrgCourse_PrevAndNext(Session session,
		Batch batch, long organizationId, long courseId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCH_WHERE);

		query.append(_FINDER_COLUMN_BATCHESBYORGCOURSE_ORGANIZATIONID_2);

		query.append(_FINDER_COLUMN_BATCHESBYORGCOURSE_COURSEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(BatchModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		qPos.add(courseId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batch);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Batch> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the batchs.
	 *
	 * @return the batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batchs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of batchs
	 * @param end the upper bound of the range of batchs (not inclusive)
	 * @return the range of batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the batchs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of batchs
	 * @param end the upper bound of the range of batchs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Batch> list = (List<Batch>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_BATCH);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_BATCH.concat(BatchModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Batch>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Batch>)QueryUtil.list(q, getDialect(), start,
							end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the batchs where organizationId = &#63; and companyId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBatchesByComOrg(long organizationId, long companyId)
		throws SystemException {
		for (Batch batch : findByBatchesByComOrg(organizationId, companyId)) {
			remove(batch);
		}
	}

	/**
	 * Removes all the batchs where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCompanyId(long companyId) throws SystemException {
		for (Batch batch : findByCompanyId(companyId)) {
			remove(batch);
		}
	}

	/**
	 * Removes the batch where organizationId = &#63; and companyId = &#63; and batchName = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @param batchName the batch name
	 * @return the batch that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public Batch removeByBatchByCompanyOrg(long organizationId, long companyId,
		String batchName) throws NoSuchBatchException, SystemException {
		Batch batch = findByBatchByCompanyOrg(organizationId, companyId,
				batchName);

		return remove(batch);
	}

	/**
	 * Removes the batch where batchId = &#63; from the database.
	 *
	 * @param batchId the batch ID
	 * @return the batch that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public Batch removeByBatchId(long batchId)
		throws NoSuchBatchException, SystemException {
		Batch batch = findByBatchId(batchId);

		return remove(batch);
	}

	/**
	 * Removes all the batchs where courseId = &#63; from the database.
	 *
	 * @param courseId the course ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCourseId(long courseId) throws SystemException {
		for (Batch batch : findByCourseId(courseId)) {
			remove(batch);
		}
	}

	/**
	 * Removes all the batchs where organizationId = &#63; and courseId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBatchesByOrgCourse(long organizationId, long courseId)
		throws SystemException {
		for (Batch batch : findByBatchesByOrgCourse(organizationId, courseId)) {
			remove(batch);
		}
	}

	/**
	 * Removes all the batchs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Batch batch : findAll()) {
			remove(batch);
		}
	}

	/**
	 * Returns the number of batchs where organizationId = &#63; and companyId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @return the number of matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBatchesByComOrg(long organizationId, long companyId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId, companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCHESBYCOMORG,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BATCH_WHERE);

			query.append(_FINDER_COLUMN_BATCHESBYCOMORG_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_BATCHESBYCOMORG_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCHESBYCOMORG,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batchs where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCompanyId(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANYID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BATCH_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANYID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batchs where organizationId = &#63; and companyId = &#63; and batchName = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param companyId the company ID
	 * @param batchName the batch name
	 * @return the number of matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBatchByCompanyOrg(long organizationId, long companyId,
		String batchName) throws SystemException {
		Object[] finderArgs = new Object[] { organizationId, companyId, batchName };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCHBYCOMPANYORG,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_BATCH_WHERE);

			query.append(_FINDER_COLUMN_BATCHBYCOMPANYORG_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_BATCHBYCOMPANYORG_COMPANYID_2);

			if (batchName == null) {
				query.append(_FINDER_COLUMN_BATCHBYCOMPANYORG_BATCHNAME_1);
			}
			else {
				if (batchName.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_BATCHBYCOMPANYORG_BATCHNAME_3);
				}
				else {
					query.append(_FINDER_COLUMN_BATCHBYCOMPANYORG_BATCHNAME_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(companyId);

				if (batchName != null) {
					qPos.add(batchName);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCHBYCOMPANYORG,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batchs where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the number of matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBatchId(long batchId) throws SystemException {
		Object[] finderArgs = new Object[] { batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCHID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BATCH_WHERE);

			query.append(_FINDER_COLUMN_BATCHID_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCHID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batchs where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @return the number of matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCourseId(long courseId) throws SystemException {
		Object[] finderArgs = new Object[] { courseId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COURSEID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BATCH_WHERE);

			query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COURSEID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batchs where organizationId = &#63; and courseId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @return the number of matching batchs
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBatchesByOrgCourse(long organizationId, long courseId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId, courseId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCHESBYORGCOURSE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BATCH_WHERE);

			query.append(_FINDER_COLUMN_BATCHESBYORGCOURSE_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_BATCHESBYORGCOURSE_COURSEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(courseId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCHESBYORGCOURSE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batchs.
	 *
	 * @return the number of batchs
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_BATCH);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the batch persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.batch.model.Batch")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Batch>> listenersList = new ArrayList<ModelListener<Batch>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Batch>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(BatchImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_BATCH = "SELECT batch FROM Batch batch";
	private static final String _SQL_SELECT_BATCH_WHERE = "SELECT batch FROM Batch batch WHERE ";
	private static final String _SQL_COUNT_BATCH = "SELECT COUNT(batch) FROM Batch batch";
	private static final String _SQL_COUNT_BATCH_WHERE = "SELECT COUNT(batch) FROM Batch batch WHERE ";
	private static final String _FINDER_COLUMN_BATCHESBYCOMORG_ORGANIZATIONID_2 = "batch.organizationId = ? AND ";
	private static final String _FINDER_COLUMN_BATCHESBYCOMORG_COMPANYID_2 = "batch.companyId = ?";
	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "batch.companyId = ?";
	private static final String _FINDER_COLUMN_BATCHBYCOMPANYORG_ORGANIZATIONID_2 =
		"batch.organizationId = ? AND ";
	private static final String _FINDER_COLUMN_BATCHBYCOMPANYORG_COMPANYID_2 = "batch.companyId = ? AND ";
	private static final String _FINDER_COLUMN_BATCHBYCOMPANYORG_BATCHNAME_1 = "batch.batchName IS NULL";
	private static final String _FINDER_COLUMN_BATCHBYCOMPANYORG_BATCHNAME_2 = "batch.batchName = ?";
	private static final String _FINDER_COLUMN_BATCHBYCOMPANYORG_BATCHNAME_3 = "(batch.batchName IS NULL OR batch.batchName = ?)";
	private static final String _FINDER_COLUMN_BATCHID_BATCHID_2 = "batch.batchId = ?";
	private static final String _FINDER_COLUMN_COURSEID_COURSEID_2 = "batch.courseId = ?";
	private static final String _FINDER_COLUMN_BATCHESBYORGCOURSE_ORGANIZATIONID_2 =
		"batch.organizationId = ? AND ";
	private static final String _FINDER_COLUMN_BATCHESBYORGCOURSE_COURSEID_2 = "batch.courseId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "batch.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Batch exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Batch exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(BatchPersistenceImpl.class);
	private static Batch _nullBatch = new BatchImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Batch> toCacheModel() {
				return _nullBatchCacheModel;
			}
		};

	private static CacheModel<Batch> _nullBatchCacheModel = new CacheModel<Batch>() {
			public Batch toEntityModel() {
				return _nullBatch;
			}
		};
}
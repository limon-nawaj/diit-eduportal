/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.batch.service.base;

import com.liferay.counter.service.CounterLocalService;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.bean.IdentifiableBean;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.model.PersistedModel;
import com.liferay.portal.service.BaseLocalServiceImpl;
import com.liferay.portal.service.PersistedModelLocalServiceRegistryUtil;
import com.liferay.portal.service.ResourceLocalService;
import com.liferay.portal.service.ResourceService;
import com.liferay.portal.service.UserLocalService;
import com.liferay.portal.service.UserService;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;

import info.diit.portal.batch.model.Batch;
import info.diit.portal.batch.service.BatchLocalService;
import info.diit.portal.batch.service.BatchTeacherLocalService;
import info.diit.portal.batch.service.persistence.BatchPersistence;
import info.diit.portal.batch.service.persistence.BatchTeacherPersistence;

import java.io.Serializable;

import java.util.List;

import javax.sql.DataSource;

/**
 * The base implementation of the batch local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link info.diit.portal.batch.service.impl.BatchLocalServiceImpl}.
 * </p>
 *
 * @author saeid
 * @see info.diit.portal.batch.service.impl.BatchLocalServiceImpl
 * @see info.diit.portal.batch.service.BatchLocalServiceUtil
 * @generated
 */
public abstract class BatchLocalServiceBaseImpl extends BaseLocalServiceImpl
	implements BatchLocalService, IdentifiableBean {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link info.diit.portal.batch.service.BatchLocalServiceUtil} to access the batch local service.
	 */

	/**
	 * Adds the batch to the database. Also notifies the appropriate model listeners.
	 *
	 * @param batch the batch
	 * @return the batch that was added
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Batch addBatch(Batch batch) throws SystemException {
		batch.setNew(true);

		return batchPersistence.update(batch, false);
	}

	/**
	 * Creates a new batch with the primary key. Does not add the batch to the database.
	 *
	 * @param batchId the primary key for the new batch
	 * @return the new batch
	 */
	public Batch createBatch(long batchId) {
		return batchPersistence.create(batchId);
	}

	/**
	 * Deletes the batch with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param batchId the primary key of the batch
	 * @return the batch that was removed
	 * @throws PortalException if a batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.DELETE)
	public Batch deleteBatch(long batchId)
		throws PortalException, SystemException {
		return batchPersistence.remove(batchId);
	}

	/**
	 * Deletes the batch from the database. Also notifies the appropriate model listeners.
	 *
	 * @param batch the batch
	 * @return the batch that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.DELETE)
	public Batch deleteBatch(Batch batch) throws SystemException {
		return batchPersistence.remove(batch);
	}

	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(Batch.class,
			clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return batchPersistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return batchPersistence.findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return batchPersistence.findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	public long dynamicQueryCount(DynamicQuery dynamicQuery)
		throws SystemException {
		return batchPersistence.countWithDynamicQuery(dynamicQuery);
	}

	public Batch fetchBatch(long batchId) throws SystemException {
		return batchPersistence.fetchByPrimaryKey(batchId);
	}

	/**
	 * Returns the batch with the primary key.
	 *
	 * @param batchId the primary key of the batch
	 * @return the batch
	 * @throws PortalException if a batch with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Batch getBatch(long batchId) throws PortalException, SystemException {
		return batchPersistence.findByPrimaryKey(batchId);
	}

	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException, SystemException {
		return batchPersistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns a range of all the batchs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of batchs
	 * @param end the upper bound of the range of batchs (not inclusive)
	 * @return the range of batchs
	 * @throws SystemException if a system exception occurred
	 */
	public List<Batch> getBatchs(int start, int end) throws SystemException {
		return batchPersistence.findAll(start, end);
	}

	/**
	 * Returns the number of batchs.
	 *
	 * @return the number of batchs
	 * @throws SystemException if a system exception occurred
	 */
	public int getBatchsCount() throws SystemException {
		return batchPersistence.countAll();
	}

	/**
	 * Updates the batch in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param batch the batch
	 * @return the batch that was updated
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Batch updateBatch(Batch batch) throws SystemException {
		return updateBatch(batch, true);
	}

	/**
	 * Updates the batch in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param batch the batch
	 * @param merge whether to merge the batch with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	 * @return the batch that was updated
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Batch updateBatch(Batch batch, boolean merge)
		throws SystemException {
		batch.setNew(false);

		return batchPersistence.update(batch, merge);
	}

	/**
	 * Returns the batch local service.
	 *
	 * @return the batch local service
	 */
	public BatchLocalService getBatchLocalService() {
		return batchLocalService;
	}

	/**
	 * Sets the batch local service.
	 *
	 * @param batchLocalService the batch local service
	 */
	public void setBatchLocalService(BatchLocalService batchLocalService) {
		this.batchLocalService = batchLocalService;
	}

	/**
	 * Returns the batch persistence.
	 *
	 * @return the batch persistence
	 */
	public BatchPersistence getBatchPersistence() {
		return batchPersistence;
	}

	/**
	 * Sets the batch persistence.
	 *
	 * @param batchPersistence the batch persistence
	 */
	public void setBatchPersistence(BatchPersistence batchPersistence) {
		this.batchPersistence = batchPersistence;
	}

	/**
	 * Returns the batch teacher local service.
	 *
	 * @return the batch teacher local service
	 */
	public BatchTeacherLocalService getBatchTeacherLocalService() {
		return batchTeacherLocalService;
	}

	/**
	 * Sets the batch teacher local service.
	 *
	 * @param batchTeacherLocalService the batch teacher local service
	 */
	public void setBatchTeacherLocalService(
		BatchTeacherLocalService batchTeacherLocalService) {
		this.batchTeacherLocalService = batchTeacherLocalService;
	}

	/**
	 * Returns the batch teacher persistence.
	 *
	 * @return the batch teacher persistence
	 */
	public BatchTeacherPersistence getBatchTeacherPersistence() {
		return batchTeacherPersistence;
	}

	/**
	 * Sets the batch teacher persistence.
	 *
	 * @param batchTeacherPersistence the batch teacher persistence
	 */
	public void setBatchTeacherPersistence(
		BatchTeacherPersistence batchTeacherPersistence) {
		this.batchTeacherPersistence = batchTeacherPersistence;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public CounterLocalService getCounterLocalService() {
		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public ResourceLocalService getResourceLocalService() {
		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the resource remote service.
	 *
	 * @return the resource remote service
	 */
	public ResourceService getResourceService() {
		return resourceService;
	}

	/**
	 * Sets the resource remote service.
	 *
	 * @param resourceService the resource remote service
	 */
	public void setResourceService(ResourceService resourceService) {
		this.resourceService = resourceService;
	}

	/**
	 * Returns the resource persistence.
	 *
	 * @return the resource persistence
	 */
	public ResourcePersistence getResourcePersistence() {
		return resourcePersistence;
	}

	/**
	 * Sets the resource persistence.
	 *
	 * @param resourcePersistence the resource persistence
	 */
	public void setResourcePersistence(ResourcePersistence resourcePersistence) {
		this.resourcePersistence = resourcePersistence;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public UserLocalService getUserLocalService() {
		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user remote service.
	 *
	 * @return the user remote service
	 */
	public UserService getUserService() {
		return userService;
	}

	/**
	 * Sets the user remote service.
	 *
	 * @param userService the user remote service
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	public void afterPropertiesSet() {
		PersistedModelLocalServiceRegistryUtil.register("info.diit.portal.batch.model.Batch",
			batchLocalService);
	}

	public void destroy() {
		PersistedModelLocalServiceRegistryUtil.unregister(
			"info.diit.portal.batch.model.Batch");
	}

	/**
	 * Returns the Spring bean ID for this bean.
	 *
	 * @return the Spring bean ID for this bean
	 */
	public String getBeanIdentifier() {
		return _beanIdentifier;
	}

	/**
	 * Sets the Spring bean ID for this bean.
	 *
	 * @param beanIdentifier the Spring bean ID for this bean
	 */
	public void setBeanIdentifier(String beanIdentifier) {
		_beanIdentifier = beanIdentifier;
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		return _clpInvoker.invokeMethod(name, parameterTypes, arguments);
	}

	protected Class<?> getModelClass() {
		return Batch.class;
	}

	protected String getModelClassName() {
		return Batch.class.getName();
	}

	/**
	 * Performs an SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) throws SystemException {
		try {
			DataSource dataSource = batchPersistence.getDataSource();

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(dataSource,
					sql, new int[0]);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(type = BatchLocalService.class)
	protected BatchLocalService batchLocalService;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchTeacherLocalService.class)
	protected BatchTeacherLocalService batchTeacherLocalService;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = CounterLocalService.class)
	protected CounterLocalService counterLocalService;
	@BeanReference(type = ResourceLocalService.class)
	protected ResourceLocalService resourceLocalService;
	@BeanReference(type = ResourceService.class)
	protected ResourceService resourceService;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserLocalService.class)
	protected UserLocalService userLocalService;
	@BeanReference(type = UserService.class)
	protected UserService userService;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private String _beanIdentifier;
	private BatchLocalServiceClpInvoker _clpInvoker = new BatchLocalServiceClpInvoker();
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.batch.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.batch.NoSuchBatchTeacherException;
import info.diit.portal.batch.model.BatchTeacher;
import info.diit.portal.batch.model.impl.BatchTeacherImpl;
import info.diit.portal.batch.model.impl.BatchTeacherModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the batch teacher service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author saeid
 * @see BatchTeacherPersistence
 * @see BatchTeacherUtil
 * @generated
 */
public class BatchTeacherPersistenceImpl extends BasePersistenceImpl<BatchTeacher>
	implements BatchTeacherPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link BatchTeacherUtil} to access the batch teacher persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = BatchTeacherImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TEACHERBYBATCHID =
		new FinderPath(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
			BatchTeacherModelImpl.FINDER_CACHE_ENABLED, BatchTeacherImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTeacherByBatchId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TEACHERBYBATCHID =
		new FinderPath(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
			BatchTeacherModelImpl.FINDER_CACHE_ENABLED, BatchTeacherImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByTeacherByBatchId", new String[] { Long.class.getName() },
			BatchTeacherModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TEACHERBYBATCHID = new FinderPath(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
			BatchTeacherModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByTeacherByBatchId", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHESBYTEACHERID =
		new FinderPath(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
			BatchTeacherModelImpl.FINDER_CACHE_ENABLED, BatchTeacherImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBatchesByTeacherId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYTEACHERID =
		new FinderPath(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
			BatchTeacherModelImpl.FINDER_CACHE_ENABLED, BatchTeacherImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBatchesByTeacherId", new String[] { Long.class.getName() },
			BatchTeacherModelImpl.TEACHERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCHESBYTEACHERID = new FinderPath(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
			BatchTeacherModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBatchesByTeacherId", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
			BatchTeacherModelImpl.FINDER_CACHE_ENABLED, BatchTeacherImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
			BatchTeacherModelImpl.FINDER_CACHE_ENABLED, BatchTeacherImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
			BatchTeacherModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the batch teacher in the entity cache if it is enabled.
	 *
	 * @param batchTeacher the batch teacher
	 */
	public void cacheResult(BatchTeacher batchTeacher) {
		EntityCacheUtil.putResult(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
			BatchTeacherImpl.class, batchTeacher.getPrimaryKey(), batchTeacher);

		batchTeacher.resetOriginalValues();
	}

	/**
	 * Caches the batch teachers in the entity cache if it is enabled.
	 *
	 * @param batchTeachers the batch teachers
	 */
	public void cacheResult(List<BatchTeacher> batchTeachers) {
		for (BatchTeacher batchTeacher : batchTeachers) {
			if (EntityCacheUtil.getResult(
						BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
						BatchTeacherImpl.class, batchTeacher.getPrimaryKey()) == null) {
				cacheResult(batchTeacher);
			}
			else {
				batchTeacher.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all batch teachers.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(BatchTeacherImpl.class.getName());
		}

		EntityCacheUtil.clearCache(BatchTeacherImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the batch teacher.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(BatchTeacher batchTeacher) {
		EntityCacheUtil.removeResult(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
			BatchTeacherImpl.class, batchTeacher.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<BatchTeacher> batchTeachers) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (BatchTeacher batchTeacher : batchTeachers) {
			EntityCacheUtil.removeResult(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
				BatchTeacherImpl.class, batchTeacher.getPrimaryKey());
		}
	}

	/**
	 * Creates a new batch teacher with the primary key. Does not add the batch teacher to the database.
	 *
	 * @param batchTeacherId the primary key for the new batch teacher
	 * @return the new batch teacher
	 */
	public BatchTeacher create(long batchTeacherId) {
		BatchTeacher batchTeacher = new BatchTeacherImpl();

		batchTeacher.setNew(true);
		batchTeacher.setPrimaryKey(batchTeacherId);

		return batchTeacher;
	}

	/**
	 * Removes the batch teacher with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param batchTeacherId the primary key of the batch teacher
	 * @return the batch teacher that was removed
	 * @throws info.diit.portal.batch.NoSuchBatchTeacherException if a batch teacher with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchTeacher remove(long batchTeacherId)
		throws NoSuchBatchTeacherException, SystemException {
		return remove(Long.valueOf(batchTeacherId));
	}

	/**
	 * Removes the batch teacher with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the batch teacher
	 * @return the batch teacher that was removed
	 * @throws info.diit.portal.batch.NoSuchBatchTeacherException if a batch teacher with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchTeacher remove(Serializable primaryKey)
		throws NoSuchBatchTeacherException, SystemException {
		Session session = null;

		try {
			session = openSession();

			BatchTeacher batchTeacher = (BatchTeacher)session.get(BatchTeacherImpl.class,
					primaryKey);

			if (batchTeacher == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchBatchTeacherException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(batchTeacher);
		}
		catch (NoSuchBatchTeacherException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected BatchTeacher removeImpl(BatchTeacher batchTeacher)
		throws SystemException {
		batchTeacher = toUnwrappedModel(batchTeacher);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, batchTeacher);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(batchTeacher);

		return batchTeacher;
	}

	@Override
	public BatchTeacher updateImpl(
		info.diit.portal.batch.model.BatchTeacher batchTeacher, boolean merge)
		throws SystemException {
		batchTeacher = toUnwrappedModel(batchTeacher);

		boolean isNew = batchTeacher.isNew();

		BatchTeacherModelImpl batchTeacherModelImpl = (BatchTeacherModelImpl)batchTeacher;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, batchTeacher, merge);

			batchTeacher.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !BatchTeacherModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((batchTeacherModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TEACHERBYBATCHID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchTeacherModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TEACHERBYBATCHID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TEACHERBYBATCHID,
					args);

				args = new Object[] {
						Long.valueOf(batchTeacherModelImpl.getBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TEACHERBYBATCHID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TEACHERBYBATCHID,
					args);
			}

			if ((batchTeacherModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYTEACHERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchTeacherModelImpl.getOriginalTeacherId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHESBYTEACHERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYTEACHERID,
					args);

				args = new Object[] {
						Long.valueOf(batchTeacherModelImpl.getTeacherId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHESBYTEACHERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYTEACHERID,
					args);
			}
		}

		EntityCacheUtil.putResult(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
			BatchTeacherImpl.class, batchTeacher.getPrimaryKey(), batchTeacher);

		return batchTeacher;
	}

	protected BatchTeacher toUnwrappedModel(BatchTeacher batchTeacher) {
		if (batchTeacher instanceof BatchTeacherImpl) {
			return batchTeacher;
		}

		BatchTeacherImpl batchTeacherImpl = new BatchTeacherImpl();

		batchTeacherImpl.setNew(batchTeacher.isNew());
		batchTeacherImpl.setPrimaryKey(batchTeacher.getPrimaryKey());

		batchTeacherImpl.setBatchTeacherId(batchTeacher.getBatchTeacherId());
		batchTeacherImpl.setTeacherId(batchTeacher.getTeacherId());
		batchTeacherImpl.setStartDate(batchTeacher.getStartDate());
		batchTeacherImpl.setEndDate(batchTeacher.getEndDate());
		batchTeacherImpl.setBatchId(batchTeacher.getBatchId());

		return batchTeacherImpl;
	}

	/**
	 * Returns the batch teacher with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the batch teacher
	 * @return the batch teacher
	 * @throws com.liferay.portal.NoSuchModelException if a batch teacher with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchTeacher findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the batch teacher with the primary key or throws a {@link info.diit.portal.batch.NoSuchBatchTeacherException} if it could not be found.
	 *
	 * @param batchTeacherId the primary key of the batch teacher
	 * @return the batch teacher
	 * @throws info.diit.portal.batch.NoSuchBatchTeacherException if a batch teacher with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchTeacher findByPrimaryKey(long batchTeacherId)
		throws NoSuchBatchTeacherException, SystemException {
		BatchTeacher batchTeacher = fetchByPrimaryKey(batchTeacherId);

		if (batchTeacher == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + batchTeacherId);
			}

			throw new NoSuchBatchTeacherException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				batchTeacherId);
		}

		return batchTeacher;
	}

	/**
	 * Returns the batch teacher with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the batch teacher
	 * @return the batch teacher, or <code>null</code> if a batch teacher with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchTeacher fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the batch teacher with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param batchTeacherId the primary key of the batch teacher
	 * @return the batch teacher, or <code>null</code> if a batch teacher with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchTeacher fetchByPrimaryKey(long batchTeacherId)
		throws SystemException {
		BatchTeacher batchTeacher = (BatchTeacher)EntityCacheUtil.getResult(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
				BatchTeacherImpl.class, batchTeacherId);

		if (batchTeacher == _nullBatchTeacher) {
			return null;
		}

		if (batchTeacher == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				batchTeacher = (BatchTeacher)session.get(BatchTeacherImpl.class,
						Long.valueOf(batchTeacherId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (batchTeacher != null) {
					cacheResult(batchTeacher);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(BatchTeacherModelImpl.ENTITY_CACHE_ENABLED,
						BatchTeacherImpl.class, batchTeacherId,
						_nullBatchTeacher);
				}

				closeSession(session);
			}
		}

		return batchTeacher;
	}

	/**
	 * Returns all the batch teachers where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the matching batch teachers
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchTeacher> findByTeacherByBatchId(long batchId)
		throws SystemException {
		return findByTeacherByBatchId(batchId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch teachers where batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of batch teachers
	 * @param end the upper bound of the range of batch teachers (not inclusive)
	 * @return the range of matching batch teachers
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchTeacher> findByTeacherByBatchId(long batchId, int start,
		int end) throws SystemException {
		return findByTeacherByBatchId(batchId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch teachers where batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of batch teachers
	 * @param end the upper bound of the range of batch teachers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batch teachers
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchTeacher> findByTeacherByBatchId(long batchId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TEACHERBYBATCHID;
			finderArgs = new Object[] { batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TEACHERBYBATCHID;
			finderArgs = new Object[] { batchId, start, end, orderByComparator };
		}

		List<BatchTeacher> list = (List<BatchTeacher>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BatchTeacher batchTeacher : list) {
				if ((batchId != batchTeacher.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_BATCHTEACHER_WHERE);

			query.append(_FINDER_COLUMN_TEACHERBYBATCHID_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				list = (List<BatchTeacher>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch teacher in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch teacher
	 * @throws info.diit.portal.batch.NoSuchBatchTeacherException if a matching batch teacher could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchTeacher findByTeacherByBatchId_First(long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchTeacherException, SystemException {
		BatchTeacher batchTeacher = fetchByTeacherByBatchId_First(batchId,
				orderByComparator);

		if (batchTeacher != null) {
			return batchTeacher;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchTeacherException(msg.toString());
	}

	/**
	 * Returns the first batch teacher in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch teacher, or <code>null</code> if a matching batch teacher could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchTeacher fetchByTeacherByBatchId_First(long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BatchTeacher> list = findByTeacherByBatchId(batchId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch teacher in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch teacher
	 * @throws info.diit.portal.batch.NoSuchBatchTeacherException if a matching batch teacher could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchTeacher findByTeacherByBatchId_Last(long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchTeacherException, SystemException {
		BatchTeacher batchTeacher = fetchByTeacherByBatchId_Last(batchId,
				orderByComparator);

		if (batchTeacher != null) {
			return batchTeacher;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchTeacherException(msg.toString());
	}

	/**
	 * Returns the last batch teacher in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch teacher, or <code>null</code> if a matching batch teacher could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchTeacher fetchByTeacherByBatchId_Last(long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByTeacherByBatchId(batchId);

		List<BatchTeacher> list = findByTeacherByBatchId(batchId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batch teachers before and after the current batch teacher in the ordered set where batchId = &#63;.
	 *
	 * @param batchTeacherId the primary key of the current batch teacher
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch teacher
	 * @throws info.diit.portal.batch.NoSuchBatchTeacherException if a batch teacher with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchTeacher[] findByTeacherByBatchId_PrevAndNext(
		long batchTeacherId, long batchId, OrderByComparator orderByComparator)
		throws NoSuchBatchTeacherException, SystemException {
		BatchTeacher batchTeacher = findByPrimaryKey(batchTeacherId);

		Session session = null;

		try {
			session = openSession();

			BatchTeacher[] array = new BatchTeacherImpl[3];

			array[0] = getByTeacherByBatchId_PrevAndNext(session, batchTeacher,
					batchId, orderByComparator, true);

			array[1] = batchTeacher;

			array[2] = getByTeacherByBatchId_PrevAndNext(session, batchTeacher,
					batchId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BatchTeacher getByTeacherByBatchId_PrevAndNext(Session session,
		BatchTeacher batchTeacher, long batchId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCHTEACHER_WHERE);

		query.append(_FINDER_COLUMN_TEACHERBYBATCHID_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batchTeacher);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BatchTeacher> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the batch teachers where teacherId = &#63;.
	 *
	 * @param teacherId the teacher ID
	 * @return the matching batch teachers
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchTeacher> findByBatchesByTeacherId(long teacherId)
		throws SystemException {
		return findByBatchesByTeacherId(teacherId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch teachers where teacherId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param teacherId the teacher ID
	 * @param start the lower bound of the range of batch teachers
	 * @param end the upper bound of the range of batch teachers (not inclusive)
	 * @return the range of matching batch teachers
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchTeacher> findByBatchesByTeacherId(long teacherId,
		int start, int end) throws SystemException {
		return findByBatchesByTeacherId(teacherId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch teachers where teacherId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param teacherId the teacher ID
	 * @param start the lower bound of the range of batch teachers
	 * @param end the upper bound of the range of batch teachers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batch teachers
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchTeacher> findByBatchesByTeacherId(long teacherId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHESBYTEACHERID;
			finderArgs = new Object[] { teacherId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHESBYTEACHERID;
			finderArgs = new Object[] { teacherId, start, end, orderByComparator };
		}

		List<BatchTeacher> list = (List<BatchTeacher>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BatchTeacher batchTeacher : list) {
				if ((teacherId != batchTeacher.getTeacherId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_BATCHTEACHER_WHERE);

			query.append(_FINDER_COLUMN_BATCHESBYTEACHERID_TEACHERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(teacherId);

				list = (List<BatchTeacher>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch teacher in the ordered set where teacherId = &#63;.
	 *
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch teacher
	 * @throws info.diit.portal.batch.NoSuchBatchTeacherException if a matching batch teacher could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchTeacher findByBatchesByTeacherId_First(long teacherId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchTeacherException, SystemException {
		BatchTeacher batchTeacher = fetchByBatchesByTeacherId_First(teacherId,
				orderByComparator);

		if (batchTeacher != null) {
			return batchTeacher;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("teacherId=");
		msg.append(teacherId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchTeacherException(msg.toString());
	}

	/**
	 * Returns the first batch teacher in the ordered set where teacherId = &#63;.
	 *
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch teacher, or <code>null</code> if a matching batch teacher could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchTeacher fetchByBatchesByTeacherId_First(long teacherId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BatchTeacher> list = findByBatchesByTeacherId(teacherId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch teacher in the ordered set where teacherId = &#63;.
	 *
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch teacher
	 * @throws info.diit.portal.batch.NoSuchBatchTeacherException if a matching batch teacher could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchTeacher findByBatchesByTeacherId_Last(long teacherId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchTeacherException, SystemException {
		BatchTeacher batchTeacher = fetchByBatchesByTeacherId_Last(teacherId,
				orderByComparator);

		if (batchTeacher != null) {
			return batchTeacher;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("teacherId=");
		msg.append(teacherId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchTeacherException(msg.toString());
	}

	/**
	 * Returns the last batch teacher in the ordered set where teacherId = &#63;.
	 *
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch teacher, or <code>null</code> if a matching batch teacher could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchTeacher fetchByBatchesByTeacherId_Last(long teacherId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBatchesByTeacherId(teacherId);

		List<BatchTeacher> list = findByBatchesByTeacherId(teacherId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batch teachers before and after the current batch teacher in the ordered set where teacherId = &#63;.
	 *
	 * @param batchTeacherId the primary key of the current batch teacher
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch teacher
	 * @throws info.diit.portal.batch.NoSuchBatchTeacherException if a batch teacher with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchTeacher[] findByBatchesByTeacherId_PrevAndNext(
		long batchTeacherId, long teacherId, OrderByComparator orderByComparator)
		throws NoSuchBatchTeacherException, SystemException {
		BatchTeacher batchTeacher = findByPrimaryKey(batchTeacherId);

		Session session = null;

		try {
			session = openSession();

			BatchTeacher[] array = new BatchTeacherImpl[3];

			array[0] = getByBatchesByTeacherId_PrevAndNext(session,
					batchTeacher, teacherId, orderByComparator, true);

			array[1] = batchTeacher;

			array[2] = getByBatchesByTeacherId_PrevAndNext(session,
					batchTeacher, teacherId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BatchTeacher getByBatchesByTeacherId_PrevAndNext(
		Session session, BatchTeacher batchTeacher, long teacherId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCHTEACHER_WHERE);

		query.append(_FINDER_COLUMN_BATCHESBYTEACHERID_TEACHERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(teacherId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batchTeacher);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BatchTeacher> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the batch teachers.
	 *
	 * @return the batch teachers
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchTeacher> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch teachers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of batch teachers
	 * @param end the upper bound of the range of batch teachers (not inclusive)
	 * @return the range of batch teachers
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchTeacher> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch teachers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of batch teachers
	 * @param end the upper bound of the range of batch teachers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of batch teachers
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchTeacher> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<BatchTeacher> list = (List<BatchTeacher>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_BATCHTEACHER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_BATCHTEACHER;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<BatchTeacher>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<BatchTeacher>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the batch teachers where batchId = &#63; from the database.
	 *
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByTeacherByBatchId(long batchId)
		throws SystemException {
		for (BatchTeacher batchTeacher : findByTeacherByBatchId(batchId)) {
			remove(batchTeacher);
		}
	}

	/**
	 * Removes all the batch teachers where teacherId = &#63; from the database.
	 *
	 * @param teacherId the teacher ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBatchesByTeacherId(long teacherId)
		throws SystemException {
		for (BatchTeacher batchTeacher : findByBatchesByTeacherId(teacherId)) {
			remove(batchTeacher);
		}
	}

	/**
	 * Removes all the batch teachers from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (BatchTeacher batchTeacher : findAll()) {
			remove(batchTeacher);
		}
	}

	/**
	 * Returns the number of batch teachers where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the number of matching batch teachers
	 * @throws SystemException if a system exception occurred
	 */
	public int countByTeacherByBatchId(long batchId) throws SystemException {
		Object[] finderArgs = new Object[] { batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_TEACHERBYBATCHID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BATCHTEACHER_WHERE);

			query.append(_FINDER_COLUMN_TEACHERBYBATCHID_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TEACHERBYBATCHID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch teachers where teacherId = &#63;.
	 *
	 * @param teacherId the teacher ID
	 * @return the number of matching batch teachers
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBatchesByTeacherId(long teacherId)
		throws SystemException {
		Object[] finderArgs = new Object[] { teacherId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCHESBYTEACHERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BATCHTEACHER_WHERE);

			query.append(_FINDER_COLUMN_BATCHESBYTEACHERID_TEACHERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(teacherId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCHESBYTEACHERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch teachers.
	 *
	 * @return the number of batch teachers
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_BATCHTEACHER);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the batch teacher persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.batch.model.BatchTeacher")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<BatchTeacher>> listenersList = new ArrayList<ModelListener<BatchTeacher>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<BatchTeacher>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(BatchTeacherImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_BATCHTEACHER = "SELECT batchTeacher FROM BatchTeacher batchTeacher";
	private static final String _SQL_SELECT_BATCHTEACHER_WHERE = "SELECT batchTeacher FROM BatchTeacher batchTeacher WHERE ";
	private static final String _SQL_COUNT_BATCHTEACHER = "SELECT COUNT(batchTeacher) FROM BatchTeacher batchTeacher";
	private static final String _SQL_COUNT_BATCHTEACHER_WHERE = "SELECT COUNT(batchTeacher) FROM BatchTeacher batchTeacher WHERE ";
	private static final String _FINDER_COLUMN_TEACHERBYBATCHID_BATCHID_2 = "batchTeacher.batchId = ?";
	private static final String _FINDER_COLUMN_BATCHESBYTEACHERID_TEACHERID_2 = "batchTeacher.teacherId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "batchTeacher.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BatchTeacher exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No BatchTeacher exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(BatchTeacherPersistenceImpl.class);
	private static BatchTeacher _nullBatchTeacher = new BatchTeacherImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<BatchTeacher> toCacheModel() {
				return _nullBatchTeacherCacheModel;
			}
		};

	private static CacheModel<BatchTeacher> _nullBatchTeacherCacheModel = new CacheModel<BatchTeacher>() {
			public BatchTeacher toEntityModel() {
				return _nullBatchTeacher;
			}
		};
}
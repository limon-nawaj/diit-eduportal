package info.diit.portal.batch.dto;

import java.io.Serializable;
import java.util.Date;

public class BatchDto implements Serializable {
	
	public long batchId;
	
	public long organizationId;
	public String organizationName;
	public long courseId;
	public String courseCode;
	public long sessionId;
	public String session;
	public String batchName;
	public Date	batchStartDate;
	public Date batchEndDate;
	public String batchClassTeacher;
	public long teacherId;
	public int statusKey;
	public int getStatusKey() {
		return statusKey;
	}
	public void setStatusKey(int statusKey) {
		this.statusKey = statusKey;
	}
	public String status;
	public String note;
	
	public long getBatchId() {
		return batchId;
	}
	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}
	public long getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public long getSessionId() {
		return sessionId;
	}
	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}
	public long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(long teacherId) {
		this.teacherId = teacherId;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getCourseCode() {
		return courseCode;
	}
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public Date getBatchStartDate() {
		return batchStartDate;
	}
	public void setBatchStartDate(Date batchStartDate) {
		this.batchStartDate = batchStartDate;
	}
	public Date getBatchEndDate() {
		return batchEndDate;
	}
	public void setBatchEndDate(Date batchEndDate) {
		this.batchEndDate = batchEndDate;
	}
	public String getBatchClassTeacher() {
		return batchClassTeacher;
	}
	public void setBatchClassTeacher(String batchClassTeacher) {
		this.batchClassTeacher = batchClassTeacher;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	public String toString(){
		return getBatchName();
	}

}

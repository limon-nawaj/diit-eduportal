/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

/**
 * @author Jack A. Rider
 */
public class courseSubjectsClp extends BaseModelImpl<courseSubjects>
	implements courseSubjects {
	public courseSubjectsClp() {
	}

	public long getPrimaryKey() {
		return _courseSubjectId;
	}

	public void setPrimaryKey(long pk) {
		setCourseSubjectId(pk);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_courseSubjectId);
	}

	public long getCourseSubjectId() {
		return _courseSubjectId;
	}

	public void setCourseSubjectId(long courseSubjectId) {
		_courseSubjectId = courseSubjectId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public long getCourseId() {
		return _courseId;
	}

	public void setCourseId(long courseId) {
		_courseId = courseId;
	}

	public long getSubjectId() {
		return _subjectId;
	}

	public void setSubjectId(long subjectId) {
		_subjectId = subjectId;
	}

	public long getTeacherId() {
		return _teacherId;
	}

	public void setTeacherId(long teacherId) {
		_teacherId = teacherId;
	}

	public courseSubjects toEscapedModel() {
		if (isEscapedModel()) {
			return this;
		}
		else {
			return (courseSubjects)Proxy.newProxyInstance(courseSubjects.class.getClassLoader(),
				new Class[] { courseSubjects.class },
				new AutoEscapeBeanHandler(this));
		}
	}

	public Object clone() {
		courseSubjectsClp clone = new courseSubjectsClp();

		clone.setCourseSubjectId(getCourseSubjectId());
		clone.setCompanyId(getCompanyId());
		clone.setGroupId(getGroupId());
		clone.setUserId(getUserId());
		clone.setCourseId(getCourseId());
		clone.setSubjectId(getSubjectId());
		clone.setTeacherId(getTeacherId());

		return clone;
	}

	public int compareTo(courseSubjects courseSubjects) {
		long pk = courseSubjects.getPrimaryKey();

		if (getPrimaryKey() < pk) {
			return -1;
		}
		else if (getPrimaryKey() > pk) {
			return 1;
		}
		else {
			return 0;
		}
	}

	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		courseSubjectsClp courseSubjects = null;

		try {
			courseSubjects = (courseSubjectsClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long pk = courseSubjects.getPrimaryKey();

		if (getPrimaryKey() == pk) {
			return true;
		}
		else {
			return false;
		}
	}

	public int hashCode() {
		return (int)getPrimaryKey();
	}

	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{courseSubjectId=");
		sb.append(getCourseSubjectId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", courseId=");
		sb.append(getCourseId());
		sb.append(", subjectId=");
		sb.append(getSubjectId());
		sb.append(", teacherId=");
		sb.append(getTeacherId());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append("org.xmlportletfactory.portal.school.model.courseSubjects");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>courseSubjectId</column-name><column-value><![CDATA[");
		sb.append(getCourseSubjectId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>courseId</column-name><column-value><![CDATA[");
		sb.append(getCourseId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>subjectId</column-name><column-value><![CDATA[");
		sb.append(getSubjectId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>teacherId</column-name><column-value><![CDATA[");
		sb.append(getTeacherId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _courseSubjectId;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private String _userUuid;
	private long _courseId;
	private long _subjectId;
	private long _teacherId;
}
/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.model;

/**
 * The model interface for the teachers service. Represents a row in the &quot;coursesexample_teachers&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Never modify this interface directly. Add methods to {@link org.xmlportletfactory.portal.school.model.impl.teachersImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
 * </p>
 *
 * <p>
 * Never reference this interface directly. All methods that expect a teachers model instance should use the {@link teachers} interface instead.
 * </p>
 *
 * @author Jack A. Rider
 * @see teachersModel
 * @see org.xmlportletfactory.portal.school.model.impl.teachersImpl
 * @see org.xmlportletfactory.portal.school.model.impl.teachersModelImpl
 * @generated
 */
public interface teachers extends teachersModel {
}
/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.model;

/**
 * <p>
 * This class is a wrapper for {@link holidays}.
 * </p>
 *
 * @author    Jack A. Rider
 * @see       holidays
 * @generated
 */
public class holidaysWrapper implements holidays {
	public holidaysWrapper(holidays holidays) {
		_holidays = holidays;
	}

	public long getPrimaryKey() {
		return _holidays.getPrimaryKey();
	}

	public void setPrimaryKey(long pk) {
		_holidays.setPrimaryKey(pk);
	}

	public long getHolidayId() {
		return _holidays.getHolidayId();
	}

	public void setHolidayId(long holidayId) {
		_holidays.setHolidayId(holidayId);
	}

	public long getCompanyId() {
		return _holidays.getCompanyId();
	}

	public void setCompanyId(long companyId) {
		_holidays.setCompanyId(companyId);
	}

	public long getGroupId() {
		return _holidays.getGroupId();
	}

	public void setGroupId(long groupId) {
		_holidays.setGroupId(groupId);
	}

	public long getUserId() {
		return _holidays.getUserId();
	}

	public void setUserId(long userId) {
		_holidays.setUserId(userId);
	}

	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidays.getUserUuid();
	}

	public void setUserUuid(java.lang.String userUuid) {
		_holidays.setUserUuid(userUuid);
	}

	public long getCourseId() {
		return _holidays.getCourseId();
	}

	public void setCourseId(long courseId) {
		_holidays.setCourseId(courseId);
	}

	public java.lang.String getHolidayName() {
		return _holidays.getHolidayName();
	}

	public void setHolidayName(java.lang.String holidayName) {
		_holidays.setHolidayName(holidayName);
	}

	public java.util.Date getHolidayDate() {
		return _holidays.getHolidayDate();
	}

	public void setHolidayDate(java.util.Date holidayDate) {
		_holidays.setHolidayDate(holidayDate);
	}

	public org.xmlportletfactory.portal.school.model.holidays toEscapedModel() {
		return _holidays.toEscapedModel();
	}

	public boolean isNew() {
		return _holidays.isNew();
	}

	public void setNew(boolean n) {
		_holidays.setNew(n);
	}

	public boolean isCachedModel() {
		return _holidays.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_holidays.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _holidays.isEscapedModel();
	}

	public void setEscapedModel(boolean escapedModel) {
		_holidays.setEscapedModel(escapedModel);
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _holidays.getPrimaryKeyObj();
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _holidays.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_holidays.setExpandoBridgeAttributes(serviceContext);
	}

	public java.lang.Object clone() {
		return _holidays.clone();
	}

	public int compareTo(
		org.xmlportletfactory.portal.school.model.holidays holidays) {
		return _holidays.compareTo(holidays);
	}

	public int hashCode() {
		return _holidays.hashCode();
	}

	public java.lang.String toString() {
		return _holidays.toString();
	}

	public java.lang.String toXmlString() {
		return _holidays.toXmlString();
	}

	public holidays getWrappedholidays() {
		return _holidays;
	}

	private holidays _holidays;
}
/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.model;

/**
 * <p>
 * This class is a wrapper for {@link classrooms}.
 * </p>
 *
 * @author    Jack A. Rider
 * @see       classrooms
 * @generated
 */
public class classroomsWrapper implements classrooms {
	public classroomsWrapper(classrooms classrooms) {
		_classrooms = classrooms;
	}

	public long getPrimaryKey() {
		return _classrooms.getPrimaryKey();
	}

	public void setPrimaryKey(long pk) {
		_classrooms.setPrimaryKey(pk);
	}

	public long getClassroomId() {
		return _classrooms.getClassroomId();
	}

	public void setClassroomId(long classroomId) {
		_classrooms.setClassroomId(classroomId);
	}

	public long getCompanyId() {
		return _classrooms.getCompanyId();
	}

	public void setCompanyId(long companyId) {
		_classrooms.setCompanyId(companyId);
	}

	public long getGroupId() {
		return _classrooms.getGroupId();
	}

	public void setGroupId(long groupId) {
		_classrooms.setGroupId(groupId);
	}

	public long getUserId() {
		return _classrooms.getUserId();
	}

	public void setUserId(long userId) {
		_classrooms.setUserId(userId);
	}

	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classrooms.getUserUuid();
	}

	public void setUserUuid(java.lang.String userUuid) {
		_classrooms.setUserUuid(userUuid);
	}

	public java.lang.String getClassroomName() {
		return _classrooms.getClassroomName();
	}

	public void setClassroomName(java.lang.String classroomName) {
		_classrooms.setClassroomName(classroomName);
	}

	public org.xmlportletfactory.portal.school.model.classrooms toEscapedModel() {
		return _classrooms.toEscapedModel();
	}

	public boolean isNew() {
		return _classrooms.isNew();
	}

	public void setNew(boolean n) {
		_classrooms.setNew(n);
	}

	public boolean isCachedModel() {
		return _classrooms.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_classrooms.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _classrooms.isEscapedModel();
	}

	public void setEscapedModel(boolean escapedModel) {
		_classrooms.setEscapedModel(escapedModel);
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _classrooms.getPrimaryKeyObj();
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _classrooms.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_classrooms.setExpandoBridgeAttributes(serviceContext);
	}

	public java.lang.Object clone() {
		return _classrooms.clone();
	}

	public int compareTo(
		org.xmlportletfactory.portal.school.model.classrooms classrooms) {
		return _classrooms.compareTo(classrooms);
	}

	public int hashCode() {
		return _classrooms.hashCode();
	}

	public java.lang.String toString() {
		return _classrooms.toString();
	}

	public java.lang.String toXmlString() {
		return _classrooms.toXmlString();
	}

	public classrooms getWrappedclassrooms() {
		return _classrooms;
	}

	private classrooms _classrooms;
}
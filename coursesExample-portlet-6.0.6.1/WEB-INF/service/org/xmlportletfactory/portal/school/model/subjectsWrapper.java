/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.model;

/**
 * <p>
 * This class is a wrapper for {@link subjects}.
 * </p>
 *
 * @author    Jack A. Rider
 * @see       subjects
 * @generated
 */
public class subjectsWrapper implements subjects {
	public subjectsWrapper(subjects subjects) {
		_subjects = subjects;
	}

	public long getPrimaryKey() {
		return _subjects.getPrimaryKey();
	}

	public void setPrimaryKey(long pk) {
		_subjects.setPrimaryKey(pk);
	}

	public long getSubjectId() {
		return _subjects.getSubjectId();
	}

	public void setSubjectId(long subjectId) {
		_subjects.setSubjectId(subjectId);
	}

	public long getCompanyId() {
		return _subjects.getCompanyId();
	}

	public void setCompanyId(long companyId) {
		_subjects.setCompanyId(companyId);
	}

	public long getGroupId() {
		return _subjects.getGroupId();
	}

	public void setGroupId(long groupId) {
		_subjects.setGroupId(groupId);
	}

	public long getUserId() {
		return _subjects.getUserId();
	}

	public void setUserId(long userId) {
		_subjects.setUserId(userId);
	}

	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subjects.getUserUuid();
	}

	public void setUserUuid(java.lang.String userUuid) {
		_subjects.setUserUuid(userUuid);
	}

	public java.lang.String getSubjectName() {
		return _subjects.getSubjectName();
	}

	public void setSubjectName(java.lang.String subjectName) {
		_subjects.setSubjectName(subjectName);
	}

	public org.xmlportletfactory.portal.school.model.subjects toEscapedModel() {
		return _subjects.toEscapedModel();
	}

	public boolean isNew() {
		return _subjects.isNew();
	}

	public void setNew(boolean n) {
		_subjects.setNew(n);
	}

	public boolean isCachedModel() {
		return _subjects.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_subjects.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _subjects.isEscapedModel();
	}

	public void setEscapedModel(boolean escapedModel) {
		_subjects.setEscapedModel(escapedModel);
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _subjects.getPrimaryKeyObj();
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _subjects.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_subjects.setExpandoBridgeAttributes(serviceContext);
	}

	public java.lang.Object clone() {
		return _subjects.clone();
	}

	public int compareTo(
		org.xmlportletfactory.portal.school.model.subjects subjects) {
		return _subjects.compareTo(subjects);
	}

	public int hashCode() {
		return _subjects.hashCode();
	}

	public java.lang.String toString() {
		return _subjects.toString();
	}

	public java.lang.String toXmlString() {
		return _subjects.toXmlString();
	}

	public subjects getWrappedsubjects() {
		return _subjects;
	}

	private subjects _subjects;
}
/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.model;

/**
 * <p>
 * This class is a wrapper for {@link students}.
 * </p>
 *
 * @author    Jack A. Rider
 * @see       students
 * @generated
 */
public class studentsWrapper implements students {
	public studentsWrapper(students students) {
		_students = students;
	}

	public long getPrimaryKey() {
		return _students.getPrimaryKey();
	}

	public void setPrimaryKey(long pk) {
		_students.setPrimaryKey(pk);
	}

	public long getStudentId() {
		return _students.getStudentId();
	}

	public void setStudentId(long studentId) {
		_students.setStudentId(studentId);
	}

	public long getCompanyId() {
		return _students.getCompanyId();
	}

	public void setCompanyId(long companyId) {
		_students.setCompanyId(companyId);
	}

	public long getGroupId() {
		return _students.getGroupId();
	}

	public void setGroupId(long groupId) {
		_students.setGroupId(groupId);
	}

	public long getUserId() {
		return _students.getUserId();
	}

	public void setUserId(long userId) {
		_students.setUserId(userId);
	}

	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _students.getUserUuid();
	}

	public void setUserUuid(java.lang.String userUuid) {
		_students.setUserUuid(userUuid);
	}

	public long getCourseId() {
		return _students.getCourseId();
	}

	public void setCourseId(long courseId) {
		_students.setCourseId(courseId);
	}

	public java.lang.String getStudentName() {
		return _students.getStudentName();
	}

	public void setStudentName(java.lang.String studentName) {
		_students.setStudentName(studentName);
	}

	public long getClassroomId() {
		return _students.getClassroomId();
	}

	public void setClassroomId(long classroomId) {
		_students.setClassroomId(classroomId);
	}

	public long getStudentPhoto() {
		return _students.getStudentPhoto();
	}

	public void setStudentPhoto(long studentPhoto) {
		_students.setStudentPhoto(studentPhoto);
	}

	public long getFolderIGId() {
		return _students.getFolderIGId();
	}

	public void setFolderIGId(long folderIGId) {
		_students.setFolderIGId(folderIGId);
	}

	public org.xmlportletfactory.portal.school.model.students toEscapedModel() {
		return _students.toEscapedModel();
	}

	public boolean isNew() {
		return _students.isNew();
	}

	public void setNew(boolean n) {
		_students.setNew(n);
	}

	public boolean isCachedModel() {
		return _students.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_students.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _students.isEscapedModel();
	}

	public void setEscapedModel(boolean escapedModel) {
		_students.setEscapedModel(escapedModel);
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _students.getPrimaryKeyObj();
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _students.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_students.setExpandoBridgeAttributes(serviceContext);
	}

	public java.lang.Object clone() {
		return _students.clone();
	}

	public int compareTo(
		org.xmlportletfactory.portal.school.model.students students) {
		return _students.compareTo(students);
	}

	public int hashCode() {
		return _students.hashCode();
	}

	public java.lang.String toString() {
		return _students.toString();
	}

	public java.lang.String toXmlString() {
		return _students.toXmlString();
	}

	public students getWrappedstudents() {
		return _students;
	}

	private students _students;
}
/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;

/**
 * @author Jack A. Rider
 */
public class holidaysClp extends BaseModelImpl<holidays> implements holidays {
	public holidaysClp() {
	}

	public long getPrimaryKey() {
		return _holidayId;
	}

	public void setPrimaryKey(long pk) {
		setHolidayId(pk);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_holidayId);
	}

	public long getHolidayId() {
		return _holidayId;
	}

	public void setHolidayId(long holidayId) {
		_holidayId = holidayId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public long getCourseId() {
		return _courseId;
	}

	public void setCourseId(long courseId) {
		_courseId = courseId;
	}

	public String getHolidayName() {
		return _holidayName;
	}

	public void setHolidayName(String holidayName) {
		_holidayName = holidayName;
	}

	public Date getHolidayDate() {
		return _holidayDate;
	}

	public void setHolidayDate(Date holidayDate) {
		_holidayDate = holidayDate;
	}

	public holidays toEscapedModel() {
		if (isEscapedModel()) {
			return this;
		}
		else {
			return (holidays)Proxy.newProxyInstance(holidays.class.getClassLoader(),
				new Class[] { holidays.class }, new AutoEscapeBeanHandler(this));
		}
	}

	public Object clone() {
		holidaysClp clone = new holidaysClp();

		clone.setHolidayId(getHolidayId());
		clone.setCompanyId(getCompanyId());
		clone.setGroupId(getGroupId());
		clone.setUserId(getUserId());
		clone.setCourseId(getCourseId());
		clone.setHolidayName(getHolidayName());
		clone.setHolidayDate(getHolidayDate());

		return clone;
	}

	public int compareTo(holidays holidays) {
		long pk = holidays.getPrimaryKey();

		if (getPrimaryKey() < pk) {
			return -1;
		}
		else if (getPrimaryKey() > pk) {
			return 1;
		}
		else {
			return 0;
		}
	}

	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		holidaysClp holidays = null;

		try {
			holidays = (holidaysClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long pk = holidays.getPrimaryKey();

		if (getPrimaryKey() == pk) {
			return true;
		}
		else {
			return false;
		}
	}

	public int hashCode() {
		return (int)getPrimaryKey();
	}

	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{holidayId=");
		sb.append(getHolidayId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", courseId=");
		sb.append(getCourseId());
		sb.append(", holidayName=");
		sb.append(getHolidayName());
		sb.append(", holidayDate=");
		sb.append(getHolidayDate());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append("org.xmlportletfactory.portal.school.model.holidays");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>holidayId</column-name><column-value><![CDATA[");
		sb.append(getHolidayId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>courseId</column-name><column-value><![CDATA[");
		sb.append(getCourseId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>holidayName</column-name><column-value><![CDATA[");
		sb.append(getHolidayName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>holidayDate</column-name><column-value><![CDATA[");
		sb.append(getHolidayDate());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _holidayId;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private String _userUuid;
	private long _courseId;
	private String _holidayName;
	private Date _holidayDate;
}
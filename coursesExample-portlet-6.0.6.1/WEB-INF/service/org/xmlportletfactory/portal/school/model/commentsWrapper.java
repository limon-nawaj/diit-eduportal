/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.model;

/**
 * <p>
 * This class is a wrapper for {@link comments}.
 * </p>
 *
 * @author    Jack A. Rider
 * @see       comments
 * @generated
 */
public class commentsWrapper implements comments {
	public commentsWrapper(comments comments) {
		_comments = comments;
	}

	public long getPrimaryKey() {
		return _comments.getPrimaryKey();
	}

	public void setPrimaryKey(long pk) {
		_comments.setPrimaryKey(pk);
	}

	public long getCommentId() {
		return _comments.getCommentId();
	}

	public void setCommentId(long commentId) {
		_comments.setCommentId(commentId);
	}

	public long getCompanyId() {
		return _comments.getCompanyId();
	}

	public void setCompanyId(long companyId) {
		_comments.setCompanyId(companyId);
	}

	public long getGroupId() {
		return _comments.getGroupId();
	}

	public void setGroupId(long groupId) {
		_comments.setGroupId(groupId);
	}

	public long getUserId() {
		return _comments.getUserId();
	}

	public void setUserId(long userId) {
		_comments.setUserId(userId);
	}

	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _comments.getUserUuid();
	}

	public void setUserUuid(java.lang.String userUuid) {
		_comments.setUserUuid(userUuid);
	}

	public long getStudentId() {
		return _comments.getStudentId();
	}

	public void setStudentId(long studentId) {
		_comments.setStudentId(studentId);
	}

	public java.lang.String getComment() {
		return _comments.getComment();
	}

	public void setComment(java.lang.String comment) {
		_comments.setComment(comment);
	}

	public org.xmlportletfactory.portal.school.model.comments toEscapedModel() {
		return _comments.toEscapedModel();
	}

	public boolean isNew() {
		return _comments.isNew();
	}

	public void setNew(boolean n) {
		_comments.setNew(n);
	}

	public boolean isCachedModel() {
		return _comments.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_comments.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _comments.isEscapedModel();
	}

	public void setEscapedModel(boolean escapedModel) {
		_comments.setEscapedModel(escapedModel);
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _comments.getPrimaryKeyObj();
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _comments.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_comments.setExpandoBridgeAttributes(serviceContext);
	}

	public java.lang.Object clone() {
		return _comments.clone();
	}

	public int compareTo(
		org.xmlportletfactory.portal.school.model.comments comments) {
		return _comments.compareTo(comments);
	}

	public int hashCode() {
		return _comments.hashCode();
	}

	public java.lang.String toString() {
		return _comments.toString();
	}

	public java.lang.String toXmlString() {
		return _comments.toXmlString();
	}

	public comments getWrappedcomments() {
		return _comments;
	}

	private comments _comments;
}
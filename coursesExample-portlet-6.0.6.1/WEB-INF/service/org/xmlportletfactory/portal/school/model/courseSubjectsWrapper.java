/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.model;

/**
 * <p>
 * This class is a wrapper for {@link courseSubjects}.
 * </p>
 *
 * @author    Jack A. Rider
 * @see       courseSubjects
 * @generated
 */
public class courseSubjectsWrapper implements courseSubjects {
	public courseSubjectsWrapper(courseSubjects courseSubjects) {
		_courseSubjects = courseSubjects;
	}

	public long getPrimaryKey() {
		return _courseSubjects.getPrimaryKey();
	}

	public void setPrimaryKey(long pk) {
		_courseSubjects.setPrimaryKey(pk);
	}

	public long getCourseSubjectId() {
		return _courseSubjects.getCourseSubjectId();
	}

	public void setCourseSubjectId(long courseSubjectId) {
		_courseSubjects.setCourseSubjectId(courseSubjectId);
	}

	public long getCompanyId() {
		return _courseSubjects.getCompanyId();
	}

	public void setCompanyId(long companyId) {
		_courseSubjects.setCompanyId(companyId);
	}

	public long getGroupId() {
		return _courseSubjects.getGroupId();
	}

	public void setGroupId(long groupId) {
		_courseSubjects.setGroupId(groupId);
	}

	public long getUserId() {
		return _courseSubjects.getUserId();
	}

	public void setUserId(long userId) {
		_courseSubjects.setUserId(userId);
	}

	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjects.getUserUuid();
	}

	public void setUserUuid(java.lang.String userUuid) {
		_courseSubjects.setUserUuid(userUuid);
	}

	public long getCourseId() {
		return _courseSubjects.getCourseId();
	}

	public void setCourseId(long courseId) {
		_courseSubjects.setCourseId(courseId);
	}

	public long getSubjectId() {
		return _courseSubjects.getSubjectId();
	}

	public void setSubjectId(long subjectId) {
		_courseSubjects.setSubjectId(subjectId);
	}

	public long getTeacherId() {
		return _courseSubjects.getTeacherId();
	}

	public void setTeacherId(long teacherId) {
		_courseSubjects.setTeacherId(teacherId);
	}

	public org.xmlportletfactory.portal.school.model.courseSubjects toEscapedModel() {
		return _courseSubjects.toEscapedModel();
	}

	public boolean isNew() {
		return _courseSubjects.isNew();
	}

	public void setNew(boolean n) {
		_courseSubjects.setNew(n);
	}

	public boolean isCachedModel() {
		return _courseSubjects.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_courseSubjects.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _courseSubjects.isEscapedModel();
	}

	public void setEscapedModel(boolean escapedModel) {
		_courseSubjects.setEscapedModel(escapedModel);
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _courseSubjects.getPrimaryKeyObj();
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _courseSubjects.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_courseSubjects.setExpandoBridgeAttributes(serviceContext);
	}

	public java.lang.Object clone() {
		return _courseSubjects.clone();
	}

	public int compareTo(
		org.xmlportletfactory.portal.school.model.courseSubjects courseSubjects) {
		return _courseSubjects.compareTo(courseSubjects);
	}

	public int hashCode() {
		return _courseSubjects.hashCode();
	}

	public java.lang.String toString() {
		return _courseSubjects.toString();
	}

	public java.lang.String toXmlString() {
		return _courseSubjects.toXmlString();
	}

	public courseSubjects getWrappedcourseSubjects() {
		return _courseSubjects;
	}

	private courseSubjects _courseSubjects;
}
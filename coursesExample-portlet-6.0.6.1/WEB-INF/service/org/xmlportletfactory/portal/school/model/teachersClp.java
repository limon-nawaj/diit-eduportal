/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

/**
 * @author Jack A. Rider
 */
public class teachersClp extends BaseModelImpl<teachers> implements teachers {
	public teachersClp() {
	}

	public long getPrimaryKey() {
		return _teacherId;
	}

	public void setPrimaryKey(long pk) {
		setTeacherId(pk);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_teacherId);
	}

	public long getTeacherId() {
		return _teacherId;
	}

	public void setTeacherId(long teacherId) {
		_teacherId = teacherId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getTeacherName() {
		return _teacherName;
	}

	public void setTeacherName(String teacherName) {
		_teacherName = teacherName;
	}

	public teachers toEscapedModel() {
		if (isEscapedModel()) {
			return this;
		}
		else {
			return (teachers)Proxy.newProxyInstance(teachers.class.getClassLoader(),
				new Class[] { teachers.class }, new AutoEscapeBeanHandler(this));
		}
	}

	public Object clone() {
		teachersClp clone = new teachersClp();

		clone.setTeacherId(getTeacherId());
		clone.setCompanyId(getCompanyId());
		clone.setGroupId(getGroupId());
		clone.setUserId(getUserId());
		clone.setTeacherName(getTeacherName());

		return clone;
	}

	public int compareTo(teachers teachers) {
		long pk = teachers.getPrimaryKey();

		if (getPrimaryKey() < pk) {
			return -1;
		}
		else if (getPrimaryKey() > pk) {
			return 1;
		}
		else {
			return 0;
		}
	}

	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		teachersClp teachers = null;

		try {
			teachers = (teachersClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long pk = teachers.getPrimaryKey();

		if (getPrimaryKey() == pk) {
			return true;
		}
		else {
			return false;
		}
	}

	public int hashCode() {
		return (int)getPrimaryKey();
	}

	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{teacherId=");
		sb.append(getTeacherId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", teacherName=");
		sb.append(getTeacherName());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("org.xmlportletfactory.portal.school.model.teachers");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>teacherId</column-name><column-value><![CDATA[");
		sb.append(getTeacherId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>teacherName</column-name><column-value><![CDATA[");
		sb.append(getTeacherName());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _teacherId;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private String _userUuid;
	private String _teacherName;
}
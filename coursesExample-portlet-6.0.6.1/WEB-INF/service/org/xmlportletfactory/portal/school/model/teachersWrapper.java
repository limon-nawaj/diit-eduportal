/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.model;

/**
 * <p>
 * This class is a wrapper for {@link teachers}.
 * </p>
 *
 * @author    Jack A. Rider
 * @see       teachers
 * @generated
 */
public class teachersWrapper implements teachers {
	public teachersWrapper(teachers teachers) {
		_teachers = teachers;
	}

	public long getPrimaryKey() {
		return _teachers.getPrimaryKey();
	}

	public void setPrimaryKey(long pk) {
		_teachers.setPrimaryKey(pk);
	}

	public long getTeacherId() {
		return _teachers.getTeacherId();
	}

	public void setTeacherId(long teacherId) {
		_teachers.setTeacherId(teacherId);
	}

	public long getCompanyId() {
		return _teachers.getCompanyId();
	}

	public void setCompanyId(long companyId) {
		_teachers.setCompanyId(companyId);
	}

	public long getGroupId() {
		return _teachers.getGroupId();
	}

	public void setGroupId(long groupId) {
		_teachers.setGroupId(groupId);
	}

	public long getUserId() {
		return _teachers.getUserId();
	}

	public void setUserId(long userId) {
		_teachers.setUserId(userId);
	}

	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachers.getUserUuid();
	}

	public void setUserUuid(java.lang.String userUuid) {
		_teachers.setUserUuid(userUuid);
	}

	public java.lang.String getTeacherName() {
		return _teachers.getTeacherName();
	}

	public void setTeacherName(java.lang.String teacherName) {
		_teachers.setTeacherName(teacherName);
	}

	public org.xmlportletfactory.portal.school.model.teachers toEscapedModel() {
		return _teachers.toEscapedModel();
	}

	public boolean isNew() {
		return _teachers.isNew();
	}

	public void setNew(boolean n) {
		_teachers.setNew(n);
	}

	public boolean isCachedModel() {
		return _teachers.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_teachers.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _teachers.isEscapedModel();
	}

	public void setEscapedModel(boolean escapedModel) {
		_teachers.setEscapedModel(escapedModel);
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _teachers.getPrimaryKeyObj();
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _teachers.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_teachers.setExpandoBridgeAttributes(serviceContext);
	}

	public java.lang.Object clone() {
		return _teachers.clone();
	}

	public int compareTo(
		org.xmlportletfactory.portal.school.model.teachers teachers) {
		return _teachers.compareTo(teachers);
	}

	public int hashCode() {
		return _teachers.hashCode();
	}

	public java.lang.String toString() {
		return _teachers.toString();
	}

	public java.lang.String toXmlString() {
		return _teachers.toXmlString();
	}

	public teachers getWrappedteachers() {
		return _teachers;
	}

	private teachers _teachers;
}
/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.model;

/**
 * <p>
 * This class is a wrapper for {@link courses}.
 * </p>
 *
 * @author    Jack A. Rider
 * @see       courses
 * @generated
 */
public class coursesWrapper implements courses {
	public coursesWrapper(courses courses) {
		_courses = courses;
	}

	public long getPrimaryKey() {
		return _courses.getPrimaryKey();
	}

	public void setPrimaryKey(long pk) {
		_courses.setPrimaryKey(pk);
	}

	public long getCourseId() {
		return _courses.getCourseId();
	}

	public void setCourseId(long courseId) {
		_courses.setCourseId(courseId);
	}

	public long getCompanyId() {
		return _courses.getCompanyId();
	}

	public void setCompanyId(long companyId) {
		_courses.setCompanyId(companyId);
	}

	public long getGroupId() {
		return _courses.getGroupId();
	}

	public void setGroupId(long groupId) {
		_courses.setGroupId(groupId);
	}

	public long getUserId() {
		return _courses.getUserId();
	}

	public void setUserId(long userId) {
		_courses.setUserId(userId);
	}

	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courses.getUserUuid();
	}

	public void setUserUuid(java.lang.String userUuid) {
		_courses.setUserUuid(userUuid);
	}

	public java.lang.String getCourseName() {
		return _courses.getCourseName();
	}

	public void setCourseName(java.lang.String courseName) {
		_courses.setCourseName(courseName);
	}

	public boolean getCourseActive() {
		return _courses.getCourseActive();
	}

	public boolean isCourseActive() {
		return _courses.isCourseActive();
	}

	public void setCourseActive(boolean courseActive) {
		_courses.setCourseActive(courseActive);
	}

	public org.xmlportletfactory.portal.school.model.courses toEscapedModel() {
		return _courses.toEscapedModel();
	}

	public boolean isNew() {
		return _courses.isNew();
	}

	public void setNew(boolean n) {
		_courses.setNew(n);
	}

	public boolean isCachedModel() {
		return _courses.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_courses.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _courses.isEscapedModel();
	}

	public void setEscapedModel(boolean escapedModel) {
		_courses.setEscapedModel(escapedModel);
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _courses.getPrimaryKeyObj();
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _courses.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_courses.setExpandoBridgeAttributes(serviceContext);
	}

	public java.lang.Object clone() {
		return _courses.clone();
	}

	public int compareTo(
		org.xmlportletfactory.portal.school.model.courses courses) {
		return _courses.compareTo(courses);
	}

	public int hashCode() {
		return _courses.hashCode();
	}

	public java.lang.String toString() {
		return _courses.toString();
	}

	public java.lang.String toXmlString() {
		return _courses.toXmlString();
	}

	public courses getWrappedcourses() {
		return _courses;
	}

	private courses _courses;
}
/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

/**
 * @author Jack A. Rider
 */
public class classroomsClp extends BaseModelImpl<classrooms>
	implements classrooms {
	public classroomsClp() {
	}

	public long getPrimaryKey() {
		return _classroomId;
	}

	public void setPrimaryKey(long pk) {
		setClassroomId(pk);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_classroomId);
	}

	public long getClassroomId() {
		return _classroomId;
	}

	public void setClassroomId(long classroomId) {
		_classroomId = classroomId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getClassroomName() {
		return _classroomName;
	}

	public void setClassroomName(String classroomName) {
		_classroomName = classroomName;
	}

	public classrooms toEscapedModel() {
		if (isEscapedModel()) {
			return this;
		}
		else {
			return (classrooms)Proxy.newProxyInstance(classrooms.class.getClassLoader(),
				new Class[] { classrooms.class },
				new AutoEscapeBeanHandler(this));
		}
	}

	public Object clone() {
		classroomsClp clone = new classroomsClp();

		clone.setClassroomId(getClassroomId());
		clone.setCompanyId(getCompanyId());
		clone.setGroupId(getGroupId());
		clone.setUserId(getUserId());
		clone.setClassroomName(getClassroomName());

		return clone;
	}

	public int compareTo(classrooms classrooms) {
		long pk = classrooms.getPrimaryKey();

		if (getPrimaryKey() < pk) {
			return -1;
		}
		else if (getPrimaryKey() > pk) {
			return 1;
		}
		else {
			return 0;
		}
	}

	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		classroomsClp classrooms = null;

		try {
			classrooms = (classroomsClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long pk = classrooms.getPrimaryKey();

		if (getPrimaryKey() == pk) {
			return true;
		}
		else {
			return false;
		}
	}

	public int hashCode() {
		return (int)getPrimaryKey();
	}

	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{classroomId=");
		sb.append(getClassroomId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", classroomName=");
		sb.append(getClassroomName());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("org.xmlportletfactory.portal.school.model.classrooms");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>classroomId</column-name><column-value><![CDATA[");
		sb.append(getClassroomId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>classroomName</column-name><column-value><![CDATA[");
		sb.append(getClassroomName());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _classroomId;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private String _userUuid;
	private String _classroomName;
}
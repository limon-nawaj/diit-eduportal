/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

/**
 * @author Jack A. Rider
 */
public class studentsClp extends BaseModelImpl<students> implements students {
	public studentsClp() {
	}

	public long getPrimaryKey() {
		return _studentId;
	}

	public void setPrimaryKey(long pk) {
		setStudentId(pk);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_studentId);
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public long getCourseId() {
		return _courseId;
	}

	public void setCourseId(long courseId) {
		_courseId = courseId;
	}

	public String getStudentName() {
		return _studentName;
	}

	public void setStudentName(String studentName) {
		_studentName = studentName;
	}

	public long getClassroomId() {
		return _classroomId;
	}

	public void setClassroomId(long classroomId) {
		_classroomId = classroomId;
	}

	public long getStudentPhoto() {
		return _studentPhoto;
	}

	public void setStudentPhoto(long studentPhoto) {
		_studentPhoto = studentPhoto;
	}

	public long getFolderIGId() {
		return _folderIGId;
	}

	public void setFolderIGId(long folderIGId) {
		_folderIGId = folderIGId;
	}

	public students toEscapedModel() {
		if (isEscapedModel()) {
			return this;
		}
		else {
			return (students)Proxy.newProxyInstance(students.class.getClassLoader(),
				new Class[] { students.class }, new AutoEscapeBeanHandler(this));
		}
	}

	public Object clone() {
		studentsClp clone = new studentsClp();

		clone.setStudentId(getStudentId());
		clone.setCompanyId(getCompanyId());
		clone.setGroupId(getGroupId());
		clone.setUserId(getUserId());
		clone.setCourseId(getCourseId());
		clone.setStudentName(getStudentName());
		clone.setClassroomId(getClassroomId());
		clone.setStudentPhoto(getStudentPhoto());
		clone.setFolderIGId(getFolderIGId());

		return clone;
	}

	public int compareTo(students students) {
		long pk = students.getPrimaryKey();

		if (getPrimaryKey() < pk) {
			return -1;
		}
		else if (getPrimaryKey() > pk) {
			return 1;
		}
		else {
			return 0;
		}
	}

	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		studentsClp students = null;

		try {
			students = (studentsClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long pk = students.getPrimaryKey();

		if (getPrimaryKey() == pk) {
			return true;
		}
		else {
			return false;
		}
	}

	public int hashCode() {
		return (int)getPrimaryKey();
	}

	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{studentId=");
		sb.append(getStudentId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", courseId=");
		sb.append(getCourseId());
		sb.append(", studentName=");
		sb.append(getStudentName());
		sb.append(", classroomId=");
		sb.append(getClassroomId());
		sb.append(", studentPhoto=");
		sb.append(getStudentPhoto());
		sb.append(", folderIGId=");
		sb.append(getFolderIGId());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(31);

		sb.append("<model><model-name>");
		sb.append("org.xmlportletfactory.portal.school.model.students");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>studentId</column-name><column-value><![CDATA[");
		sb.append(getStudentId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>courseId</column-name><column-value><![CDATA[");
		sb.append(getCourseId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>studentName</column-name><column-value><![CDATA[");
		sb.append(getStudentName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>classroomId</column-name><column-value><![CDATA[");
		sb.append(getClassroomId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>studentPhoto</column-name><column-value><![CDATA[");
		sb.append(getStudentPhoto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>folderIGId</column-name><column-value><![CDATA[");
		sb.append(getFolderIGId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _studentId;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private String _userUuid;
	private long _courseId;
	private String _studentName;
	private long _classroomId;
	private long _studentPhoto;
	private long _folderIGId;
}
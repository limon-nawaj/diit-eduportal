/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.BaseModel;

import org.xmlportletfactory.portal.school.model.classroomsClp;
import org.xmlportletfactory.portal.school.model.commentsClp;
import org.xmlportletfactory.portal.school.model.courseSubjectsClp;
import org.xmlportletfactory.portal.school.model.coursesClp;
import org.xmlportletfactory.portal.school.model.holidaysClp;
import org.xmlportletfactory.portal.school.model.studentsClp;
import org.xmlportletfactory.portal.school.model.subjectsClp;
import org.xmlportletfactory.portal.school.model.teachersClp;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Brian Wing Shun Chan
 */
public class ClpSerializer {
	public static final String SERVLET_CONTEXT_NAME = "coursesExample-portlet";

	public static void setClassLoader(ClassLoader classLoader) {
		_classLoader = classLoader;
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(coursesClp.class.getName())) {
			coursesClp oldCplModel = (coursesClp)oldModel;

			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					Class<?> newModelClass = Class.forName("org.xmlportletfactory.portal.school.model.impl.coursesImpl",
							true, _classLoader);

					Object newModel = newModelClass.newInstance();

					Method method0 = newModelClass.getMethod("setCourseId",
							new Class[] { Long.TYPE });

					Long value0 = new Long(oldCplModel.getCourseId());

					method0.invoke(newModel, value0);

					Method method1 = newModelClass.getMethod("setCompanyId",
							new Class[] { Long.TYPE });

					Long value1 = new Long(oldCplModel.getCompanyId());

					method1.invoke(newModel, value1);

					Method method2 = newModelClass.getMethod("setGroupId",
							new Class[] { Long.TYPE });

					Long value2 = new Long(oldCplModel.getGroupId());

					method2.invoke(newModel, value2);

					Method method3 = newModelClass.getMethod("setUserId",
							new Class[] { Long.TYPE });

					Long value3 = new Long(oldCplModel.getUserId());

					method3.invoke(newModel, value3);

					Method method4 = newModelClass.getMethod("setCourseName",
							new Class[] { String.class });

					String value4 = oldCplModel.getCourseName();

					method4.invoke(newModel, value4);

					Method method5 = newModelClass.getMethod("setCourseActive",
							new Class[] { Boolean.TYPE });

					Boolean value5 = new Boolean(oldCplModel.getCourseActive());

					method5.invoke(newModel, value5);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		if (oldModelClassName.equals(studentsClp.class.getName())) {
			studentsClp oldCplModel = (studentsClp)oldModel;

			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					Class<?> newModelClass = Class.forName("org.xmlportletfactory.portal.school.model.impl.studentsImpl",
							true, _classLoader);

					Object newModel = newModelClass.newInstance();

					Method method0 = newModelClass.getMethod("setStudentId",
							new Class[] { Long.TYPE });

					Long value0 = new Long(oldCplModel.getStudentId());

					method0.invoke(newModel, value0);

					Method method1 = newModelClass.getMethod("setCompanyId",
							new Class[] { Long.TYPE });

					Long value1 = new Long(oldCplModel.getCompanyId());

					method1.invoke(newModel, value1);

					Method method2 = newModelClass.getMethod("setGroupId",
							new Class[] { Long.TYPE });

					Long value2 = new Long(oldCplModel.getGroupId());

					method2.invoke(newModel, value2);

					Method method3 = newModelClass.getMethod("setUserId",
							new Class[] { Long.TYPE });

					Long value3 = new Long(oldCplModel.getUserId());

					method3.invoke(newModel, value3);

					Method method4 = newModelClass.getMethod("setCourseId",
							new Class[] { Long.TYPE });

					Long value4 = new Long(oldCplModel.getCourseId());

					method4.invoke(newModel, value4);

					Method method5 = newModelClass.getMethod("setStudentName",
							new Class[] { String.class });

					String value5 = oldCplModel.getStudentName();

					method5.invoke(newModel, value5);

					Method method6 = newModelClass.getMethod("setClassroomId",
							new Class[] { Long.TYPE });

					Long value6 = new Long(oldCplModel.getClassroomId());

					method6.invoke(newModel, value6);

					Method method7 = newModelClass.getMethod("setStudentPhoto",
							new Class[] { Long.TYPE });

					Long value7 = new Long(oldCplModel.getStudentPhoto());

					method7.invoke(newModel, value7);

					Method method8 = newModelClass.getMethod("setFolderIGId",
							new Class[] { Long.TYPE });

					Long value8 = new Long(oldCplModel.getFolderIGId());

					method8.invoke(newModel, value8);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		if (oldModelClassName.equals(classroomsClp.class.getName())) {
			classroomsClp oldCplModel = (classroomsClp)oldModel;

			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					Class<?> newModelClass = Class.forName("org.xmlportletfactory.portal.school.model.impl.classroomsImpl",
							true, _classLoader);

					Object newModel = newModelClass.newInstance();

					Method method0 = newModelClass.getMethod("setClassroomId",
							new Class[] { Long.TYPE });

					Long value0 = new Long(oldCplModel.getClassroomId());

					method0.invoke(newModel, value0);

					Method method1 = newModelClass.getMethod("setCompanyId",
							new Class[] { Long.TYPE });

					Long value1 = new Long(oldCplModel.getCompanyId());

					method1.invoke(newModel, value1);

					Method method2 = newModelClass.getMethod("setGroupId",
							new Class[] { Long.TYPE });

					Long value2 = new Long(oldCplModel.getGroupId());

					method2.invoke(newModel, value2);

					Method method3 = newModelClass.getMethod("setUserId",
							new Class[] { Long.TYPE });

					Long value3 = new Long(oldCplModel.getUserId());

					method3.invoke(newModel, value3);

					Method method4 = newModelClass.getMethod("setClassroomName",
							new Class[] { String.class });

					String value4 = oldCplModel.getClassroomName();

					method4.invoke(newModel, value4);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		if (oldModelClassName.equals(commentsClp.class.getName())) {
			commentsClp oldCplModel = (commentsClp)oldModel;

			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					Class<?> newModelClass = Class.forName("org.xmlportletfactory.portal.school.model.impl.commentsImpl",
							true, _classLoader);

					Object newModel = newModelClass.newInstance();

					Method method0 = newModelClass.getMethod("setCommentId",
							new Class[] { Long.TYPE });

					Long value0 = new Long(oldCplModel.getCommentId());

					method0.invoke(newModel, value0);

					Method method1 = newModelClass.getMethod("setCompanyId",
							new Class[] { Long.TYPE });

					Long value1 = new Long(oldCplModel.getCompanyId());

					method1.invoke(newModel, value1);

					Method method2 = newModelClass.getMethod("setGroupId",
							new Class[] { Long.TYPE });

					Long value2 = new Long(oldCplModel.getGroupId());

					method2.invoke(newModel, value2);

					Method method3 = newModelClass.getMethod("setUserId",
							new Class[] { Long.TYPE });

					Long value3 = new Long(oldCplModel.getUserId());

					method3.invoke(newModel, value3);

					Method method4 = newModelClass.getMethod("setStudentId",
							new Class[] { Long.TYPE });

					Long value4 = new Long(oldCplModel.getStudentId());

					method4.invoke(newModel, value4);

					Method method5 = newModelClass.getMethod("setComment",
							new Class[] { String.class });

					String value5 = oldCplModel.getComment();

					method5.invoke(newModel, value5);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		if (oldModelClassName.equals(courseSubjectsClp.class.getName())) {
			courseSubjectsClp oldCplModel = (courseSubjectsClp)oldModel;

			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					Class<?> newModelClass = Class.forName("org.xmlportletfactory.portal.school.model.impl.courseSubjectsImpl",
							true, _classLoader);

					Object newModel = newModelClass.newInstance();

					Method method0 = newModelClass.getMethod("setCourseSubjectId",
							new Class[] { Long.TYPE });

					Long value0 = new Long(oldCplModel.getCourseSubjectId());

					method0.invoke(newModel, value0);

					Method method1 = newModelClass.getMethod("setCompanyId",
							new Class[] { Long.TYPE });

					Long value1 = new Long(oldCplModel.getCompanyId());

					method1.invoke(newModel, value1);

					Method method2 = newModelClass.getMethod("setGroupId",
							new Class[] { Long.TYPE });

					Long value2 = new Long(oldCplModel.getGroupId());

					method2.invoke(newModel, value2);

					Method method3 = newModelClass.getMethod("setUserId",
							new Class[] { Long.TYPE });

					Long value3 = new Long(oldCplModel.getUserId());

					method3.invoke(newModel, value3);

					Method method4 = newModelClass.getMethod("setCourseId",
							new Class[] { Long.TYPE });

					Long value4 = new Long(oldCplModel.getCourseId());

					method4.invoke(newModel, value4);

					Method method5 = newModelClass.getMethod("setSubjectId",
							new Class[] { Long.TYPE });

					Long value5 = new Long(oldCplModel.getSubjectId());

					method5.invoke(newModel, value5);

					Method method6 = newModelClass.getMethod("setTeacherId",
							new Class[] { Long.TYPE });

					Long value6 = new Long(oldCplModel.getTeacherId());

					method6.invoke(newModel, value6);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		if (oldModelClassName.equals(subjectsClp.class.getName())) {
			subjectsClp oldCplModel = (subjectsClp)oldModel;

			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					Class<?> newModelClass = Class.forName("org.xmlportletfactory.portal.school.model.impl.subjectsImpl",
							true, _classLoader);

					Object newModel = newModelClass.newInstance();

					Method method0 = newModelClass.getMethod("setSubjectId",
							new Class[] { Long.TYPE });

					Long value0 = new Long(oldCplModel.getSubjectId());

					method0.invoke(newModel, value0);

					Method method1 = newModelClass.getMethod("setCompanyId",
							new Class[] { Long.TYPE });

					Long value1 = new Long(oldCplModel.getCompanyId());

					method1.invoke(newModel, value1);

					Method method2 = newModelClass.getMethod("setGroupId",
							new Class[] { Long.TYPE });

					Long value2 = new Long(oldCplModel.getGroupId());

					method2.invoke(newModel, value2);

					Method method3 = newModelClass.getMethod("setUserId",
							new Class[] { Long.TYPE });

					Long value3 = new Long(oldCplModel.getUserId());

					method3.invoke(newModel, value3);

					Method method4 = newModelClass.getMethod("setSubjectName",
							new Class[] { String.class });

					String value4 = oldCplModel.getSubjectName();

					method4.invoke(newModel, value4);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		if (oldModelClassName.equals(teachersClp.class.getName())) {
			teachersClp oldCplModel = (teachersClp)oldModel;

			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					Class<?> newModelClass = Class.forName("org.xmlportletfactory.portal.school.model.impl.teachersImpl",
							true, _classLoader);

					Object newModel = newModelClass.newInstance();

					Method method0 = newModelClass.getMethod("setTeacherId",
							new Class[] { Long.TYPE });

					Long value0 = new Long(oldCplModel.getTeacherId());

					method0.invoke(newModel, value0);

					Method method1 = newModelClass.getMethod("setCompanyId",
							new Class[] { Long.TYPE });

					Long value1 = new Long(oldCplModel.getCompanyId());

					method1.invoke(newModel, value1);

					Method method2 = newModelClass.getMethod("setGroupId",
							new Class[] { Long.TYPE });

					Long value2 = new Long(oldCplModel.getGroupId());

					method2.invoke(newModel, value2);

					Method method3 = newModelClass.getMethod("setUserId",
							new Class[] { Long.TYPE });

					Long value3 = new Long(oldCplModel.getUserId());

					method3.invoke(newModel, value3);

					Method method4 = newModelClass.getMethod("setTeacherName",
							new Class[] { String.class });

					String value4 = oldCplModel.getTeacherName();

					method4.invoke(newModel, value4);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		if (oldModelClassName.equals(holidaysClp.class.getName())) {
			holidaysClp oldCplModel = (holidaysClp)oldModel;

			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					Class<?> newModelClass = Class.forName("org.xmlportletfactory.portal.school.model.impl.holidaysImpl",
							true, _classLoader);

					Object newModel = newModelClass.newInstance();

					Method method0 = newModelClass.getMethod("setHolidayId",
							new Class[] { Long.TYPE });

					Long value0 = new Long(oldCplModel.getHolidayId());

					method0.invoke(newModel, value0);

					Method method1 = newModelClass.getMethod("setCompanyId",
							new Class[] { Long.TYPE });

					Long value1 = new Long(oldCplModel.getCompanyId());

					method1.invoke(newModel, value1);

					Method method2 = newModelClass.getMethod("setGroupId",
							new Class[] { Long.TYPE });

					Long value2 = new Long(oldCplModel.getGroupId());

					method2.invoke(newModel, value2);

					Method method3 = newModelClass.getMethod("setUserId",
							new Class[] { Long.TYPE });

					Long value3 = new Long(oldCplModel.getUserId());

					method3.invoke(newModel, value3);

					Method method4 = newModelClass.getMethod("setCourseId",
							new Class[] { Long.TYPE });

					Long value4 = new Long(oldCplModel.getCourseId());

					method4.invoke(newModel, value4);

					Method method5 = newModelClass.getMethod("setHolidayName",
							new Class[] { String.class });

					String value5 = oldCplModel.getHolidayName();

					method5.invoke(newModel, value5);

					Method method6 = newModelClass.getMethod("setHolidayDate",
							new Class[] { Date.class });

					Date value6 = oldCplModel.getHolidayDate();

					method6.invoke(newModel, value6);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(
					"org.xmlportletfactory.portal.school.model.impl.coursesImpl")) {
			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					coursesClp newModel = new coursesClp();

					Method method0 = oldModelClass.getMethod("getCourseId");

					Long value0 = (Long)method0.invoke(oldModel, (Object[])null);

					newModel.setCourseId(value0);

					Method method1 = oldModelClass.getMethod("getCompanyId");

					Long value1 = (Long)method1.invoke(oldModel, (Object[])null);

					newModel.setCompanyId(value1);

					Method method2 = oldModelClass.getMethod("getGroupId");

					Long value2 = (Long)method2.invoke(oldModel, (Object[])null);

					newModel.setGroupId(value2);

					Method method3 = oldModelClass.getMethod("getUserId");

					Long value3 = (Long)method3.invoke(oldModel, (Object[])null);

					newModel.setUserId(value3);

					Method method4 = oldModelClass.getMethod("getCourseName");

					String value4 = (String)method4.invoke(oldModel,
							(Object[])null);

					newModel.setCourseName(value4);

					Method method5 = oldModelClass.getMethod("getCourseActive");

					Boolean value5 = (Boolean)method5.invoke(oldModel,
							(Object[])null);

					newModel.setCourseActive(value5);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		if (oldModelClassName.equals(
					"org.xmlportletfactory.portal.school.model.impl.studentsImpl")) {
			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					studentsClp newModel = new studentsClp();

					Method method0 = oldModelClass.getMethod("getStudentId");

					Long value0 = (Long)method0.invoke(oldModel, (Object[])null);

					newModel.setStudentId(value0);

					Method method1 = oldModelClass.getMethod("getCompanyId");

					Long value1 = (Long)method1.invoke(oldModel, (Object[])null);

					newModel.setCompanyId(value1);

					Method method2 = oldModelClass.getMethod("getGroupId");

					Long value2 = (Long)method2.invoke(oldModel, (Object[])null);

					newModel.setGroupId(value2);

					Method method3 = oldModelClass.getMethod("getUserId");

					Long value3 = (Long)method3.invoke(oldModel, (Object[])null);

					newModel.setUserId(value3);

					Method method4 = oldModelClass.getMethod("getCourseId");

					Long value4 = (Long)method4.invoke(oldModel, (Object[])null);

					newModel.setCourseId(value4);

					Method method5 = oldModelClass.getMethod("getStudentName");

					String value5 = (String)method5.invoke(oldModel,
							(Object[])null);

					newModel.setStudentName(value5);

					Method method6 = oldModelClass.getMethod("getClassroomId");

					Long value6 = (Long)method6.invoke(oldModel, (Object[])null);

					newModel.setClassroomId(value6);

					Method method7 = oldModelClass.getMethod("getStudentPhoto");

					Long value7 = (Long)method7.invoke(oldModel, (Object[])null);

					newModel.setStudentPhoto(value7);

					Method method8 = oldModelClass.getMethod("getFolderIGId");

					Long value8 = (Long)method8.invoke(oldModel, (Object[])null);

					newModel.setFolderIGId(value8);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		if (oldModelClassName.equals(
					"org.xmlportletfactory.portal.school.model.impl.classroomsImpl")) {
			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					classroomsClp newModel = new classroomsClp();

					Method method0 = oldModelClass.getMethod("getClassroomId");

					Long value0 = (Long)method0.invoke(oldModel, (Object[])null);

					newModel.setClassroomId(value0);

					Method method1 = oldModelClass.getMethod("getCompanyId");

					Long value1 = (Long)method1.invoke(oldModel, (Object[])null);

					newModel.setCompanyId(value1);

					Method method2 = oldModelClass.getMethod("getGroupId");

					Long value2 = (Long)method2.invoke(oldModel, (Object[])null);

					newModel.setGroupId(value2);

					Method method3 = oldModelClass.getMethod("getUserId");

					Long value3 = (Long)method3.invoke(oldModel, (Object[])null);

					newModel.setUserId(value3);

					Method method4 = oldModelClass.getMethod("getClassroomName");

					String value4 = (String)method4.invoke(oldModel,
							(Object[])null);

					newModel.setClassroomName(value4);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		if (oldModelClassName.equals(
					"org.xmlportletfactory.portal.school.model.impl.commentsImpl")) {
			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					commentsClp newModel = new commentsClp();

					Method method0 = oldModelClass.getMethod("getCommentId");

					Long value0 = (Long)method0.invoke(oldModel, (Object[])null);

					newModel.setCommentId(value0);

					Method method1 = oldModelClass.getMethod("getCompanyId");

					Long value1 = (Long)method1.invoke(oldModel, (Object[])null);

					newModel.setCompanyId(value1);

					Method method2 = oldModelClass.getMethod("getGroupId");

					Long value2 = (Long)method2.invoke(oldModel, (Object[])null);

					newModel.setGroupId(value2);

					Method method3 = oldModelClass.getMethod("getUserId");

					Long value3 = (Long)method3.invoke(oldModel, (Object[])null);

					newModel.setUserId(value3);

					Method method4 = oldModelClass.getMethod("getStudentId");

					Long value4 = (Long)method4.invoke(oldModel, (Object[])null);

					newModel.setStudentId(value4);

					Method method5 = oldModelClass.getMethod("getComment");

					String value5 = (String)method5.invoke(oldModel,
							(Object[])null);

					newModel.setComment(value5);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		if (oldModelClassName.equals(
					"org.xmlportletfactory.portal.school.model.impl.courseSubjectsImpl")) {
			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					courseSubjectsClp newModel = new courseSubjectsClp();

					Method method0 = oldModelClass.getMethod(
							"getCourseSubjectId");

					Long value0 = (Long)method0.invoke(oldModel, (Object[])null);

					newModel.setCourseSubjectId(value0);

					Method method1 = oldModelClass.getMethod("getCompanyId");

					Long value1 = (Long)method1.invoke(oldModel, (Object[])null);

					newModel.setCompanyId(value1);

					Method method2 = oldModelClass.getMethod("getGroupId");

					Long value2 = (Long)method2.invoke(oldModel, (Object[])null);

					newModel.setGroupId(value2);

					Method method3 = oldModelClass.getMethod("getUserId");

					Long value3 = (Long)method3.invoke(oldModel, (Object[])null);

					newModel.setUserId(value3);

					Method method4 = oldModelClass.getMethod("getCourseId");

					Long value4 = (Long)method4.invoke(oldModel, (Object[])null);

					newModel.setCourseId(value4);

					Method method5 = oldModelClass.getMethod("getSubjectId");

					Long value5 = (Long)method5.invoke(oldModel, (Object[])null);

					newModel.setSubjectId(value5);

					Method method6 = oldModelClass.getMethod("getTeacherId");

					Long value6 = (Long)method6.invoke(oldModel, (Object[])null);

					newModel.setTeacherId(value6);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		if (oldModelClassName.equals(
					"org.xmlportletfactory.portal.school.model.impl.subjectsImpl")) {
			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					subjectsClp newModel = new subjectsClp();

					Method method0 = oldModelClass.getMethod("getSubjectId");

					Long value0 = (Long)method0.invoke(oldModel, (Object[])null);

					newModel.setSubjectId(value0);

					Method method1 = oldModelClass.getMethod("getCompanyId");

					Long value1 = (Long)method1.invoke(oldModel, (Object[])null);

					newModel.setCompanyId(value1);

					Method method2 = oldModelClass.getMethod("getGroupId");

					Long value2 = (Long)method2.invoke(oldModel, (Object[])null);

					newModel.setGroupId(value2);

					Method method3 = oldModelClass.getMethod("getUserId");

					Long value3 = (Long)method3.invoke(oldModel, (Object[])null);

					newModel.setUserId(value3);

					Method method4 = oldModelClass.getMethod("getSubjectName");

					String value4 = (String)method4.invoke(oldModel,
							(Object[])null);

					newModel.setSubjectName(value4);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		if (oldModelClassName.equals(
					"org.xmlportletfactory.portal.school.model.impl.teachersImpl")) {
			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					teachersClp newModel = new teachersClp();

					Method method0 = oldModelClass.getMethod("getTeacherId");

					Long value0 = (Long)method0.invoke(oldModel, (Object[])null);

					newModel.setTeacherId(value0);

					Method method1 = oldModelClass.getMethod("getCompanyId");

					Long value1 = (Long)method1.invoke(oldModel, (Object[])null);

					newModel.setCompanyId(value1);

					Method method2 = oldModelClass.getMethod("getGroupId");

					Long value2 = (Long)method2.invoke(oldModel, (Object[])null);

					newModel.setGroupId(value2);

					Method method3 = oldModelClass.getMethod("getUserId");

					Long value3 = (Long)method3.invoke(oldModel, (Object[])null);

					newModel.setUserId(value3);

					Method method4 = oldModelClass.getMethod("getTeacherName");

					String value4 = (String)method4.invoke(oldModel,
							(Object[])null);

					newModel.setTeacherName(value4);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		if (oldModelClassName.equals(
					"org.xmlportletfactory.portal.school.model.impl.holidaysImpl")) {
			ClassLoader contextClassLoader = Thread.currentThread()
												   .getContextClassLoader();

			try {
				Thread.currentThread().setContextClassLoader(_classLoader);

				try {
					holidaysClp newModel = new holidaysClp();

					Method method0 = oldModelClass.getMethod("getHolidayId");

					Long value0 = (Long)method0.invoke(oldModel, (Object[])null);

					newModel.setHolidayId(value0);

					Method method1 = oldModelClass.getMethod("getCompanyId");

					Long value1 = (Long)method1.invoke(oldModel, (Object[])null);

					newModel.setCompanyId(value1);

					Method method2 = oldModelClass.getMethod("getGroupId");

					Long value2 = (Long)method2.invoke(oldModel, (Object[])null);

					newModel.setGroupId(value2);

					Method method3 = oldModelClass.getMethod("getUserId");

					Long value3 = (Long)method3.invoke(oldModel, (Object[])null);

					newModel.setUserId(value3);

					Method method4 = oldModelClass.getMethod("getCourseId");

					Long value4 = (Long)method4.invoke(oldModel, (Object[])null);

					newModel.setCourseId(value4);

					Method method5 = oldModelClass.getMethod("getHolidayName");

					String value5 = (String)method5.invoke(oldModel,
							(Object[])null);

					newModel.setHolidayName(value5);

					Method method6 = oldModelClass.getMethod("getHolidayDate");

					Date value6 = (Date)method6.invoke(oldModel, (Object[])null);

					newModel.setHolidayDate(value6);

					return newModel;
				}
				catch (Exception e) {
					_log.error(e, e);
				}
			}
			finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static ClassLoader _classLoader;
}
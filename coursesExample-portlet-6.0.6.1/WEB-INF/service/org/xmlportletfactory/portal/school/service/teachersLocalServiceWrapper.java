/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service;

/**
 * <p>
 * This class is a wrapper for {@link teachersLocalService}.
 * </p>
 *
 * @author    Jack A. Rider
 * @see       teachersLocalService
 * @generated
 */
public class teachersLocalServiceWrapper implements teachersLocalService {
	public teachersLocalServiceWrapper(
		teachersLocalService teachersLocalService) {
		_teachersLocalService = teachersLocalService;
	}

	/**
	* Adds the teachers to the database. Also notifies the appropriate model listeners.
	*
	* @param teachers the teachers to add
	* @return the teachers that was added
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.teachers addteachers(
		org.xmlportletfactory.portal.school.model.teachers teachers)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.addteachers(teachers);
	}

	/**
	* Creates a new teachers with the primary key. Does not add the teachers to the database.
	*
	* @param teacherId the primary key for the new teachers
	* @return the new teachers
	*/
	public org.xmlportletfactory.portal.school.model.teachers createteachers(
		long teacherId) {
		return _teachersLocalService.createteachers(teacherId);
	}

	/**
	* Deletes the teachers with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param teacherId the primary key of the teachers to delete
	* @throws PortalException if a teachers with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public void deleteteachers(long teacherId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		_teachersLocalService.deleteteachers(teacherId);
	}

	/**
	* Deletes the teachers from the database. Also notifies the appropriate model listeners.
	*
	* @param teachers the teachers to delete
	* @throws SystemException if a system exception occurred
	*/
	public void deleteteachers(
		org.xmlportletfactory.portal.school.model.teachers teachers)
		throws com.liferay.portal.kernel.exception.SystemException {
		_teachersLocalService.deleteteachers(teachers);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Counts the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Gets the teachers with the primary key.
	*
	* @param teacherId the primary key of the teachers to get
	* @return the teachers
	* @throws PortalException if a teachers with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.teachers getteachers(
		long teacherId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.getteachers(teacherId);
	}

	/**
	* Gets a range of all the teacherses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of teacherses to return
	* @param end the upper bound of the range of teacherses to return (not inclusive)
	* @return the range of teacherses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.teachers> getteacherses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.getteacherses(start, end);
	}

	/**
	* Gets the number of teacherses.
	*
	* @return the number of teacherses
	* @throws SystemException if a system exception occurred
	*/
	public int getteachersesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.getteachersesCount();
	}

	/**
	* Updates the teachers in the database. Also notifies the appropriate model listeners.
	*
	* @param teachers the teachers to update
	* @return the teachers that was updated
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.teachers updateteachers(
		org.xmlportletfactory.portal.school.model.teachers teachers)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.updateteachers(teachers);
	}

	/**
	* Updates the teachers in the database. Also notifies the appropriate model listeners.
	*
	* @param teachers the teachers to update
	* @param merge whether to merge the teachers with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the teachers that was updated
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.teachers updateteachers(
		org.xmlportletfactory.portal.school.model.teachers teachers,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.updateteachers(teachers, merge);
	}

	public java.util.List findAllInUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.findAllInUser(userId);
	}

	public java.util.List findAllInUser(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.findAllInUser(userId, orderByComparator);
	}

	public java.util.List findAllInGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.findAllInGroup(groupId);
	}

	public java.util.List findAllInGroup(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.findAllInGroup(groupId, orderByComparator);
	}

	public java.util.List findAllInUserAndGroup(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.findAllInUserAndGroup(userId, groupId);
	}

	public java.util.List findAllInUserAndGroup(long userId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teachersLocalService.findAllInUserAndGroup(userId, groupId,
			orderByComparator);
	}

	public void remove(
		org.xmlportletfactory.portal.school.model.teachers fileobj)
		throws com.liferay.portal.kernel.exception.SystemException {
		_teachersLocalService.remove(fileobj);
	}

	public teachersLocalService getWrappedteachersLocalService() {
		return _teachersLocalService;
	}

	private teachersLocalService _teachersLocalService;
}
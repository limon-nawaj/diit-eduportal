/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service;

/**
 * <p>
 * This class is a wrapper for {@link commentsLocalService}.
 * </p>
 *
 * @author    Jack A. Rider
 * @see       commentsLocalService
 * @generated
 */
public class commentsLocalServiceWrapper implements commentsLocalService {
	public commentsLocalServiceWrapper(
		commentsLocalService commentsLocalService) {
		_commentsLocalService = commentsLocalService;
	}

	/**
	* Adds the comments to the database. Also notifies the appropriate model listeners.
	*
	* @param comments the comments to add
	* @return the comments that was added
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.comments addcomments(
		org.xmlportletfactory.portal.school.model.comments comments)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.addcomments(comments);
	}

	/**
	* Creates a new comments with the primary key. Does not add the comments to the database.
	*
	* @param commentId the primary key for the new comments
	* @return the new comments
	*/
	public org.xmlportletfactory.portal.school.model.comments createcomments(
		long commentId) {
		return _commentsLocalService.createcomments(commentId);
	}

	/**
	* Deletes the comments with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param commentId the primary key of the comments to delete
	* @throws PortalException if a comments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public void deletecomments(long commentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		_commentsLocalService.deletecomments(commentId);
	}

	/**
	* Deletes the comments from the database. Also notifies the appropriate model listeners.
	*
	* @param comments the comments to delete
	* @throws SystemException if a system exception occurred
	*/
	public void deletecomments(
		org.xmlportletfactory.portal.school.model.comments comments)
		throws com.liferay.portal.kernel.exception.SystemException {
		_commentsLocalService.deletecomments(comments);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Counts the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Gets the comments with the primary key.
	*
	* @param commentId the primary key of the comments to get
	* @return the comments
	* @throws PortalException if a comments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.comments getcomments(
		long commentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.getcomments(commentId);
	}

	/**
	* Gets a range of all the commentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of commentses to return
	* @param end the upper bound of the range of commentses to return (not inclusive)
	* @return the range of commentses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.comments> getcommentses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.getcommentses(start, end);
	}

	/**
	* Gets the number of commentses.
	*
	* @return the number of commentses
	* @throws SystemException if a system exception occurred
	*/
	public int getcommentsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.getcommentsesCount();
	}

	/**
	* Updates the comments in the database. Also notifies the appropriate model listeners.
	*
	* @param comments the comments to update
	* @return the comments that was updated
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.comments updatecomments(
		org.xmlportletfactory.portal.school.model.comments comments)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.updatecomments(comments);
	}

	/**
	* Updates the comments in the database. Also notifies the appropriate model listeners.
	*
	* @param comments the comments to update
	* @param merge whether to merge the comments with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the comments that was updated
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.comments updatecomments(
		org.xmlportletfactory.portal.school.model.comments comments,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.updatecomments(comments, merge);
	}

	public java.util.List findAllInUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.findAllInUser(userId);
	}

	public java.util.List findAllInUser(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.findAllInUser(userId, orderByComparator);
	}

	public java.util.List findAllInGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.findAllInGroup(groupId);
	}

	public java.util.List findAllInGroup(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.findAllInGroup(groupId, orderByComparator);
	}

	public java.util.List findAllInUserAndGroup(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.findAllInUserAndGroup(userId, groupId);
	}

	public java.util.List findAllInUserAndGroup(long userId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.findAllInUserAndGroup(userId, groupId,
			orderByComparator);
	}

	public java.util.List findAllInstudentIdGroup(long studentId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.findAllInstudentIdGroup(studentId, groupId);
	}

	public java.util.List findAllInstudentId(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.findAllInstudentId(studentId);
	}

	public java.util.List findAllInstudentIdGroup(long studentId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _commentsLocalService.findAllInstudentIdGroup(studentId,
			groupId, orderByComparator);
	}

	public void remove(
		org.xmlportletfactory.portal.school.model.comments fileobj)
		throws com.liferay.portal.kernel.exception.SystemException {
		_commentsLocalService.remove(fileobj);
	}

	public commentsLocalService getWrappedcommentsLocalService() {
		return _commentsLocalService;
	}

	private commentsLocalService _commentsLocalService;
}
/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service;

/**
 * <p>
 * This class is a wrapper for {@link classroomsLocalService}.
 * </p>
 *
 * @author    Jack A. Rider
 * @see       classroomsLocalService
 * @generated
 */
public class classroomsLocalServiceWrapper implements classroomsLocalService {
	public classroomsLocalServiceWrapper(
		classroomsLocalService classroomsLocalService) {
		_classroomsLocalService = classroomsLocalService;
	}

	/**
	* Adds the classrooms to the database. Also notifies the appropriate model listeners.
	*
	* @param classrooms the classrooms to add
	* @return the classrooms that was added
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.classrooms addclassrooms(
		org.xmlportletfactory.portal.school.model.classrooms classrooms)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.addclassrooms(classrooms);
	}

	/**
	* Creates a new classrooms with the primary key. Does not add the classrooms to the database.
	*
	* @param classroomId the primary key for the new classrooms
	* @return the new classrooms
	*/
	public org.xmlportletfactory.portal.school.model.classrooms createclassrooms(
		long classroomId) {
		return _classroomsLocalService.createclassrooms(classroomId);
	}

	/**
	* Deletes the classrooms with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param classroomId the primary key of the classrooms to delete
	* @throws PortalException if a classrooms with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public void deleteclassrooms(long classroomId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		_classroomsLocalService.deleteclassrooms(classroomId);
	}

	/**
	* Deletes the classrooms from the database. Also notifies the appropriate model listeners.
	*
	* @param classrooms the classrooms to delete
	* @throws SystemException if a system exception occurred
	*/
	public void deleteclassrooms(
		org.xmlportletfactory.portal.school.model.classrooms classrooms)
		throws com.liferay.portal.kernel.exception.SystemException {
		_classroomsLocalService.deleteclassrooms(classrooms);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Counts the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Gets the classrooms with the primary key.
	*
	* @param classroomId the primary key of the classrooms to get
	* @return the classrooms
	* @throws PortalException if a classrooms with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.classrooms getclassrooms(
		long classroomId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.getclassrooms(classroomId);
	}

	/**
	* Gets a range of all the classroomses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of classroomses to return
	* @param end the upper bound of the range of classroomses to return (not inclusive)
	* @return the range of classroomses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.classrooms> getclassroomses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.getclassroomses(start, end);
	}

	/**
	* Gets the number of classroomses.
	*
	* @return the number of classroomses
	* @throws SystemException if a system exception occurred
	*/
	public int getclassroomsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.getclassroomsesCount();
	}

	/**
	* Updates the classrooms in the database. Also notifies the appropriate model listeners.
	*
	* @param classrooms the classrooms to update
	* @return the classrooms that was updated
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.classrooms updateclassrooms(
		org.xmlportletfactory.portal.school.model.classrooms classrooms)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.updateclassrooms(classrooms);
	}

	/**
	* Updates the classrooms in the database. Also notifies the appropriate model listeners.
	*
	* @param classrooms the classrooms to update
	* @param merge whether to merge the classrooms with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the classrooms that was updated
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.classrooms updateclassrooms(
		org.xmlportletfactory.portal.school.model.classrooms classrooms,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.updateclassrooms(classrooms, merge);
	}

	public java.util.List findAllInUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.findAllInUser(userId);
	}

	public java.util.List findAllInUser(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.findAllInUser(userId, orderByComparator);
	}

	public java.util.List findAllInGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.findAllInGroup(groupId);
	}

	public java.util.List findAllInGroup(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.findAllInGroup(groupId, orderByComparator);
	}

	public java.util.List findAllInUserAndGroup(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.findAllInUserAndGroup(userId, groupId);
	}

	public java.util.List findAllInUserAndGroup(long userId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classroomsLocalService.findAllInUserAndGroup(userId, groupId,
			orderByComparator);
	}

	public void remove(
		org.xmlportletfactory.portal.school.model.classrooms fileobj)
		throws com.liferay.portal.kernel.exception.SystemException {
		_classroomsLocalService.remove(fileobj);
	}

	public classroomsLocalService getWrappedclassroomsLocalService() {
		return _classroomsLocalService;
	}

	private classroomsLocalService _classroomsLocalService;
}
/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service;

/**
 * <p>
 * This class is a wrapper for {@link holidaysLocalService}.
 * </p>
 *
 * @author    Jack A. Rider
 * @see       holidaysLocalService
 * @generated
 */
public class holidaysLocalServiceWrapper implements holidaysLocalService {
	public holidaysLocalServiceWrapper(
		holidaysLocalService holidaysLocalService) {
		_holidaysLocalService = holidaysLocalService;
	}

	/**
	* Adds the holidays to the database. Also notifies the appropriate model listeners.
	*
	* @param holidays the holidays to add
	* @return the holidays that was added
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.holidays addholidays(
		org.xmlportletfactory.portal.school.model.holidays holidays)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.addholidays(holidays);
	}

	/**
	* Creates a new holidays with the primary key. Does not add the holidays to the database.
	*
	* @param holidayId the primary key for the new holidays
	* @return the new holidays
	*/
	public org.xmlportletfactory.portal.school.model.holidays createholidays(
		long holidayId) {
		return _holidaysLocalService.createholidays(holidayId);
	}

	/**
	* Deletes the holidays with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param holidayId the primary key of the holidays to delete
	* @throws PortalException if a holidays with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public void deleteholidays(long holidayId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		_holidaysLocalService.deleteholidays(holidayId);
	}

	/**
	* Deletes the holidays from the database. Also notifies the appropriate model listeners.
	*
	* @param holidays the holidays to delete
	* @throws SystemException if a system exception occurred
	*/
	public void deleteholidays(
		org.xmlportletfactory.portal.school.model.holidays holidays)
		throws com.liferay.portal.kernel.exception.SystemException {
		_holidaysLocalService.deleteholidays(holidays);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Counts the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Gets the holidays with the primary key.
	*
	* @param holidayId the primary key of the holidays to get
	* @return the holidays
	* @throws PortalException if a holidays with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.holidays getholidays(
		long holidayId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.getholidays(holidayId);
	}

	/**
	* Gets a range of all the holidayses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of holidayses to return
	* @param end the upper bound of the range of holidayses to return (not inclusive)
	* @return the range of holidayses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.holidays> getholidayses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.getholidayses(start, end);
	}

	/**
	* Gets the number of holidayses.
	*
	* @return the number of holidayses
	* @throws SystemException if a system exception occurred
	*/
	public int getholidaysesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.getholidaysesCount();
	}

	/**
	* Updates the holidays in the database. Also notifies the appropriate model listeners.
	*
	* @param holidays the holidays to update
	* @return the holidays that was updated
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.holidays updateholidays(
		org.xmlportletfactory.portal.school.model.holidays holidays)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.updateholidays(holidays);
	}

	/**
	* Updates the holidays in the database. Also notifies the appropriate model listeners.
	*
	* @param holidays the holidays to update
	* @param merge whether to merge the holidays with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the holidays that was updated
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.holidays updateholidays(
		org.xmlportletfactory.portal.school.model.holidays holidays,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.updateholidays(holidays, merge);
	}

	public java.util.List findAllInUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.findAllInUser(userId);
	}

	public java.util.List findAllInUser(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.findAllInUser(userId, orderByComparator);
	}

	public java.util.List findAllInGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.findAllInGroup(groupId);
	}

	public java.util.List findAllInGroup(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.findAllInGroup(groupId, orderByComparator);
	}

	public java.util.List findAllInUserAndGroup(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.findAllInUserAndGroup(userId, groupId);
	}

	public java.util.List findAllInUserAndGroup(long userId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.findAllInUserAndGroup(userId, groupId,
			orderByComparator);
	}

	public java.util.List findAllIncourseIdGroup(long courseId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.findAllIncourseIdGroup(courseId, groupId);
	}

	public java.util.List findAllIncourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.findAllIncourseId(courseId);
	}

	public java.util.List findAllIncourseIdGroup(long courseId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _holidaysLocalService.findAllIncourseIdGroup(courseId, groupId,
			orderByComparator);
	}

	public void remove(
		org.xmlportletfactory.portal.school.model.holidays fileobj)
		throws com.liferay.portal.kernel.exception.SystemException {
		_holidaysLocalService.remove(fileobj);
	}

	public holidaysLocalService getWrappedholidaysLocalService() {
		return _holidaysLocalService;
	}

	private holidaysLocalService _holidaysLocalService;
}
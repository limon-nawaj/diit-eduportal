/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;

/**
 * The utility for the courses local service. This utility wraps {@link org.xmlportletfactory.portal.school.service.impl.coursesLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * Never modify this class directly. Add custom service methods to {@link org.xmlportletfactory.portal.school.service.impl.coursesLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
 * </p>
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Jack A. Rider
 * @see coursesLocalService
 * @see org.xmlportletfactory.portal.school.service.base.coursesLocalServiceBaseImpl
 * @see org.xmlportletfactory.portal.school.service.impl.coursesLocalServiceImpl
 * @generated
 */
public class coursesLocalServiceUtil {
	/**
	* Adds the courses to the database. Also notifies the appropriate model listeners.
	*
	* @param courses the courses to add
	* @return the courses that was added
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.courses addcourses(
		org.xmlportletfactory.portal.school.model.courses courses)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addcourses(courses);
	}

	/**
	* Creates a new courses with the primary key. Does not add the courses to the database.
	*
	* @param courseId the primary key for the new courses
	* @return the new courses
	*/
	public static org.xmlportletfactory.portal.school.model.courses createcourses(
		long courseId) {
		return getService().createcourses(courseId);
	}

	/**
	* Deletes the courses with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param courseId the primary key of the courses to delete
	* @throws PortalException if a courses with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static void deletecourses(long courseId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		getService().deletecourses(courseId);
	}

	/**
	* Deletes the courses from the database. Also notifies the appropriate model listeners.
	*
	* @param courses the courses to delete
	* @throws SystemException if a system exception occurred
	*/
	public static void deletecourses(
		org.xmlportletfactory.portal.school.model.courses courses)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deletecourses(courses);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Counts the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Gets the courses with the primary key.
	*
	* @param courseId the primary key of the courses to get
	* @return the courses
	* @throws PortalException if a courses with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.courses getcourses(
		long courseId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getcourses(courseId);
	}

	/**
	* Gets a range of all the courseses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of courseses to return
	* @param end the upper bound of the range of courseses to return (not inclusive)
	* @return the range of courseses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.xmlportletfactory.portal.school.model.courses> getcourseses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getcourseses(start, end);
	}

	/**
	* Gets the number of courseses.
	*
	* @return the number of courseses
	* @throws SystemException if a system exception occurred
	*/
	public static int getcoursesesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getcoursesesCount();
	}

	/**
	* Updates the courses in the database. Also notifies the appropriate model listeners.
	*
	* @param courses the courses to update
	* @return the courses that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.courses updatecourses(
		org.xmlportletfactory.portal.school.model.courses courses)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updatecourses(courses);
	}

	/**
	* Updates the courses in the database. Also notifies the appropriate model listeners.
	*
	* @param courses the courses to update
	* @param merge whether to merge the courses with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the courses that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.courses updatecourses(
		org.xmlportletfactory.portal.school.model.courses courses, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updatecourses(courses, merge);
	}

	public static java.util.List findAllInUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInUser(userId);
	}

	public static java.util.List findAllInUser(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInUser(userId, orderByComparator);
	}

	public static java.util.List findAllInGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInGroup(groupId);
	}

	public static java.util.List findAllInGroup(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInGroup(groupId, orderByComparator);
	}

	public static java.util.List findAllInUserAndGroup(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInUserAndGroup(userId, groupId);
	}

	public static java.util.List findAllInUserAndGroup(long userId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .findAllInUserAndGroup(userId, groupId, orderByComparator);
	}

	public static void remove(
		org.xmlportletfactory.portal.school.model.courses fileobj)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().remove(fileobj);
	}

	public static void clearService() {
		_service = null;
	}

	public static coursesLocalService getService() {
		if (_service == null) {
			Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
					coursesLocalService.class.getName());
			ClassLoader portletClassLoader = (ClassLoader)PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
					"portletClassLoader");

			ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
					portletClassLoader);

			_service = new coursesLocalServiceClp(classLoaderProxy);

			ClpSerializer.setClassLoader(portletClassLoader);
		}

		return _service;
	}

	public void setService(coursesLocalService service) {
		_service = service;
	}

	private static coursesLocalService _service;
}
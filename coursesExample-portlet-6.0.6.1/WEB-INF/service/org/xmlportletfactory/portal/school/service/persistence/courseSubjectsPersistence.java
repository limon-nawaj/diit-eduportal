/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import org.xmlportletfactory.portal.school.model.courseSubjects;

/**
 * The persistence interface for the course subjects service.
 *
 * <p>
 * Never modify or reference this interface directly. Always use {@link courseSubjectsUtil} to access the course subjects persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
 * </p>
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Jack A. Rider
 * @see courseSubjectsPersistenceImpl
 * @see courseSubjectsUtil
 * @generated
 */
public interface courseSubjectsPersistence extends BasePersistence<courseSubjects> {
	/**
	* Caches the course subjects in the entity cache if it is enabled.
	*
	* @param courseSubjects the course subjects to cache
	*/
	public void cacheResult(
		org.xmlportletfactory.portal.school.model.courseSubjects courseSubjects);

	/**
	* Caches the course subjectses in the entity cache if it is enabled.
	*
	* @param courseSubjectses the course subjectses to cache
	*/
	public void cacheResult(
		java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> courseSubjectses);

	/**
	* Creates a new course subjects with the primary key. Does not add the course subjects to the database.
	*
	* @param courseSubjectId the primary key for the new course subjects
	* @return the new course subjects
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects create(
		long courseSubjectId);

	/**
	* Removes the course subjects with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjectId the primary key of the course subjects to remove
	* @return the course subjects that was removed
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects remove(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	public org.xmlportletfactory.portal.school.model.courseSubjects updateImpl(
		org.xmlportletfactory.portal.school.model.courseSubjects courseSubjects,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds the course subjects with the primary key or throws a {@link org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException} if it could not be found.
	*
	* @param courseSubjectId the primary key of the course subjects to find
	* @return the course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findByPrimaryKey(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the course subjects with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param courseSubjectId the primary key of the course subjects to find
	* @return the course subjects, or <code>null</code> if a course subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects fetchByPrimaryKey(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds all the course subjectses where groupId = &#63;.
	*
	* @param groupId the group id to search with
	* @return the matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds a range of all the course subjectses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @return the range of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByGroupId(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds an ordered range of all the course subjectses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds the first course subjects in the ordered set where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the last course subjects in the ordered set where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the course subjectses before and after the current course subjects in the ordered set where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseSubjectId the primary key of the current course subjects
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects[] findByGroupId_PrevAndNext(
		long courseSubjectId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Filters by the user's permissions and finds all the course subjectses where groupId = &#63;.
	*
	* @param groupId the group id to search with
	* @return the matching course subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> filterFindByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and finds a range of all the course subjectses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @return the range of matching course subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> filterFindByGroupId(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and finds an ordered range of all the course subjectses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching course subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> filterFindByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds all the course subjectses where userId = &#63;.
	*
	* @param userId the user id to search with
	* @return the matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds a range of all the course subjectses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @return the range of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByUserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds an ordered range of all the course subjectses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByUserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds the first course subjects in the ordered set where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findByUserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the last course subjects in the ordered set where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findByUserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the course subjectses before and after the current course subjects in the ordered set where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseSubjectId the primary key of the current course subjects
	* @param userId the user id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects[] findByUserId_PrevAndNext(
		long courseSubjectId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds all the course subjectses where userId = &#63; and groupId = &#63;.
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @return the matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByUserIdGroupId(
		long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds a range of all the course subjectses where userId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @return the range of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByUserIdGroupId(
		long userId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds an ordered range of all the course subjectses where userId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByUserIdGroupId(
		long userId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds the first course subjects in the ordered set where userId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findByUserIdGroupId_First(
		long userId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the last course subjects in the ordered set where userId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findByUserIdGroupId_Last(
		long userId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the course subjectses before and after the current course subjects in the ordered set where userId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseSubjectId the primary key of the current course subjects
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects[] findByUserIdGroupId_PrevAndNext(
		long courseSubjectId, long userId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Filters by the user's permissions and finds all the course subjectses where userId = &#63; and groupId = &#63;.
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @return the matching course subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> filterFindByUserIdGroupId(
		long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and finds a range of all the course subjectses where userId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @return the range of matching course subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> filterFindByUserIdGroupId(
		long userId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and finds an ordered range of all the course subjectses where userId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching course subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> filterFindByUserIdGroupId(
		long userId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds all the course subjectses where subjectId = &#63;.
	*
	* @param subjectId the subject id to search with
	* @return the matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findBySubjectId(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds a range of all the course subjectses where subjectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @return the range of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findBySubjectId(
		long subjectId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds an ordered range of all the course subjectses where subjectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findBySubjectId(
		long subjectId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds the first course subjects in the ordered set where subjectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findBySubjectId_First(
		long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the last course subjects in the ordered set where subjectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findBySubjectId_Last(
		long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the course subjectses before and after the current course subjects in the ordered set where subjectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseSubjectId the primary key of the current course subjects
	* @param subjectId the subject id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects[] findBySubjectId_PrevAndNext(
		long courseSubjectId, long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds all the course subjectses where teacherId = &#63;.
	*
	* @param teacherId the teacher id to search with
	* @return the matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByTeacherId(
		long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds a range of all the course subjectses where teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param teacherId the teacher id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @return the range of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByTeacherId(
		long teacherId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds an ordered range of all the course subjectses where teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param teacherId the teacher id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByTeacherId(
		long teacherId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds the first course subjects in the ordered set where teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param teacherId the teacher id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findByTeacherId_First(
		long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the last course subjects in the ordered set where teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param teacherId the teacher id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findByTeacherId_Last(
		long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the course subjectses before and after the current course subjects in the ordered set where teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseSubjectId the primary key of the current course subjects
	* @param teacherId the teacher id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects[] findByTeacherId_PrevAndNext(
		long courseSubjectId, long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds all the course subjectses where courseId = &#63;.
	*
	* @param courseId the course id to search with
	* @return the matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByCourseId(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds a range of all the course subjectses where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @return the range of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByCourseId(
		long courseId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds an ordered range of all the course subjectses where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByCourseId(
		long courseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds the first course subjects in the ordered set where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findByCourseId_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the last course subjects in the ordered set where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findByCourseId_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the course subjectses before and after the current course subjects in the ordered set where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseSubjectId the primary key of the current course subjects
	* @param courseId the course id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects[] findByCourseId_PrevAndNext(
		long courseSubjectId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds all the course subjectses where courseId = &#63; and groupId = &#63;.
	*
	* @param courseId the course id to search with
	* @param groupId the group id to search with
	* @return the matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByCourseIdGroupId(
		long courseId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds a range of all the course subjectses where courseId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course id to search with
	* @param groupId the group id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @return the range of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByCourseIdGroupId(
		long courseId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds an ordered range of all the course subjectses where courseId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course id to search with
	* @param groupId the group id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findByCourseIdGroupId(
		long courseId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds the first course subjects in the ordered set where courseId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course id to search with
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findByCourseIdGroupId_First(
		long courseId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the last course subjects in the ordered set where courseId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course id to search with
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects findByCourseIdGroupId_Last(
		long courseId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Finds the course subjectses before and after the current course subjects in the ordered set where courseId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseSubjectId the primary key of the current course subjects
	* @param courseId the course id to search with
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next course subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects[] findByCourseIdGroupId_PrevAndNext(
		long courseSubjectId, long courseId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;

	/**
	* Filters by the user's permissions and finds all the course subjectses where courseId = &#63; and groupId = &#63;.
	*
	* @param courseId the course id to search with
	* @param groupId the group id to search with
	* @return the matching course subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> filterFindByCourseIdGroupId(
		long courseId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and finds a range of all the course subjectses where courseId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course id to search with
	* @param groupId the group id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @return the range of matching course subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> filterFindByCourseIdGroupId(
		long courseId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and finds an ordered range of all the course subjectses where courseId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course id to search with
	* @param groupId the group id to search with
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching course subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> filterFindByCourseIdGroupId(
		long courseId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds all the course subjectses.
	*
	* @return the course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds a range of all the course subjectses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @return the range of course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds an ordered range of all the course subjectses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course subjectses where groupId = &#63; from the database.
	*
	* @param groupId the group id to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course subjectses where userId = &#63; from the database.
	*
	* @param userId the user id to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course subjectses where userId = &#63; and groupId = &#63; from the database.
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUserIdGroupId(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course subjectses where subjectId = &#63; from the database.
	*
	* @param subjectId the subject id to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeBySubjectId(long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course subjectses where teacherId = &#63; from the database.
	*
	* @param teacherId the teacher id to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByTeacherId(long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course subjectses where courseId = &#63; from the database.
	*
	* @param courseId the course id to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course subjectses where courseId = &#63; and groupId = &#63; from the database.
	*
	* @param courseId the course id to search with
	* @param groupId the group id to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCourseIdGroupId(long courseId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course subjectses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Counts all the course subjectses where groupId = &#63;.
	*
	* @param groupId the group id to search with
	* @return the number of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public int countByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and counts all the course subjectses where groupId = &#63;.
	*
	* @param groupId the group id to search with
	* @return the number of matching course subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public int filterCountByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Counts all the course subjectses where userId = &#63;.
	*
	* @param userId the user id to search with
	* @return the number of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Counts all the course subjectses where userId = &#63; and groupId = &#63;.
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @return the number of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserIdGroupId(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and counts all the course subjectses where userId = &#63; and groupId = &#63;.
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @return the number of matching course subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public int filterCountByUserIdGroupId(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Counts all the course subjectses where subjectId = &#63;.
	*
	* @param subjectId the subject id to search with
	* @return the number of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public int countBySubjectId(long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Counts all the course subjectses where teacherId = &#63;.
	*
	* @param teacherId the teacher id to search with
	* @return the number of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public int countByTeacherId(long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Counts all the course subjectses where courseId = &#63;.
	*
	* @param courseId the course id to search with
	* @return the number of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public int countByCourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Counts all the course subjectses where courseId = &#63; and groupId = &#63;.
	*
	* @param courseId the course id to search with
	* @param groupId the group id to search with
	* @return the number of matching course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public int countByCourseIdGroupId(long courseId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and counts all the course subjectses where courseId = &#63; and groupId = &#63;.
	*
	* @param courseId the course id to search with
	* @param groupId the group id to search with
	* @return the number of matching course subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public int filterCountByCourseIdGroupId(long courseId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Counts all the course subjectses.
	*
	* @return the number of course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
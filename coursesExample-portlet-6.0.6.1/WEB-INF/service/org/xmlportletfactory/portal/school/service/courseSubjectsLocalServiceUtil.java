/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;

/**
 * The utility for the course subjects local service. This utility wraps {@link org.xmlportletfactory.portal.school.service.impl.courseSubjectsLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * Never modify this class directly. Add custom service methods to {@link org.xmlportletfactory.portal.school.service.impl.courseSubjectsLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
 * </p>
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Jack A. Rider
 * @see courseSubjectsLocalService
 * @see org.xmlportletfactory.portal.school.service.base.courseSubjectsLocalServiceBaseImpl
 * @see org.xmlportletfactory.portal.school.service.impl.courseSubjectsLocalServiceImpl
 * @generated
 */
public class courseSubjectsLocalServiceUtil {
	/**
	* Adds the course subjects to the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjects the course subjects to add
	* @return the course subjects that was added
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.courseSubjects addcourseSubjects(
		org.xmlportletfactory.portal.school.model.courseSubjects courseSubjects)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addcourseSubjects(courseSubjects);
	}

	/**
	* Creates a new course subjects with the primary key. Does not add the course subjects to the database.
	*
	* @param courseSubjectId the primary key for the new course subjects
	* @return the new course subjects
	*/
	public static org.xmlportletfactory.portal.school.model.courseSubjects createcourseSubjects(
		long courseSubjectId) {
		return getService().createcourseSubjects(courseSubjectId);
	}

	/**
	* Deletes the course subjects with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjectId the primary key of the course subjects to delete
	* @throws PortalException if a course subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static void deletecourseSubjects(long courseSubjectId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		getService().deletecourseSubjects(courseSubjectId);
	}

	/**
	* Deletes the course subjects from the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjects the course subjects to delete
	* @throws SystemException if a system exception occurred
	*/
	public static void deletecourseSubjects(
		org.xmlportletfactory.portal.school.model.courseSubjects courseSubjects)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deletecourseSubjects(courseSubjects);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Counts the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Gets the course subjects with the primary key.
	*
	* @param courseSubjectId the primary key of the course subjects to get
	* @return the course subjects
	* @throws PortalException if a course subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.courseSubjects getcourseSubjects(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getcourseSubjects(courseSubjectId);
	}

	/**
	* Gets a range of all the course subjectses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @return the range of course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> getcourseSubjectses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getcourseSubjectses(start, end);
	}

	/**
	* Gets the number of course subjectses.
	*
	* @return the number of course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public static int getcourseSubjectsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getcourseSubjectsesCount();
	}

	/**
	* Updates the course subjects in the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjects the course subjects to update
	* @return the course subjects that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.courseSubjects updatecourseSubjects(
		org.xmlportletfactory.portal.school.model.courseSubjects courseSubjects)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updatecourseSubjects(courseSubjects);
	}

	/**
	* Updates the course subjects in the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjects the course subjects to update
	* @param merge whether to merge the course subjects with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the course subjects that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.courseSubjects updatecourseSubjects(
		org.xmlportletfactory.portal.school.model.courseSubjects courseSubjects,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updatecourseSubjects(courseSubjects, merge);
	}

	public static java.util.List findAllInUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInUser(userId);
	}

	public static java.util.List findAllInUser(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInUser(userId, orderByComparator);
	}

	public static java.util.List findAllInGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInGroup(groupId);
	}

	public static java.util.List findAllInGroup(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInGroup(groupId, orderByComparator);
	}

	public static java.util.List findAllInUserAndGroup(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInUserAndGroup(userId, groupId);
	}

	public static java.util.List findAllInUserAndGroup(long userId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .findAllInUserAndGroup(userId, groupId, orderByComparator);
	}

	public static java.util.List findAllInSubjectId(long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInSubjectId(subjectId);
	}

	public static java.util.List findAllInTeacherId(long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInTeacherId(teacherId);
	}

	public static java.util.List findAllIncourseIdGroup(long courseId,
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllIncourseIdGroup(courseId, groupId);
	}

	public static java.util.List findAllIncourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllIncourseId(courseId);
	}

	public static java.util.List findAllIncourseIdGroup(long courseId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .findAllIncourseIdGroup(courseId, groupId, orderByComparator);
	}

	public static void remove(
		org.xmlportletfactory.portal.school.model.courseSubjects fileobj)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().remove(fileobj);
	}

	public static void clearService() {
		_service = null;
	}

	public static courseSubjectsLocalService getService() {
		if (_service == null) {
			Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
					courseSubjectsLocalService.class.getName());
			ClassLoader portletClassLoader = (ClassLoader)PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
					"portletClassLoader");

			ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
					portletClassLoader);

			_service = new courseSubjectsLocalServiceClp(classLoaderProxy);

			ClpSerializer.setClassLoader(portletClassLoader);
		}

		return _service;
	}

	public void setService(courseSubjectsLocalService service) {
		_service = service;
	}

	private static courseSubjectsLocalService _service;
}
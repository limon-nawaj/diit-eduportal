/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;

/**
 * The utility for the students local service. This utility wraps {@link org.xmlportletfactory.portal.school.service.impl.studentsLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * Never modify this class directly. Add custom service methods to {@link org.xmlportletfactory.portal.school.service.impl.studentsLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
 * </p>
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Jack A. Rider
 * @see studentsLocalService
 * @see org.xmlportletfactory.portal.school.service.base.studentsLocalServiceBaseImpl
 * @see org.xmlportletfactory.portal.school.service.impl.studentsLocalServiceImpl
 * @generated
 */
public class studentsLocalServiceUtil {
	/**
	* Adds the students to the database. Also notifies the appropriate model listeners.
	*
	* @param students the students to add
	* @return the students that was added
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.students addstudents(
		org.xmlportletfactory.portal.school.model.students students)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addstudents(students);
	}

	/**
	* Creates a new students with the primary key. Does not add the students to the database.
	*
	* @param studentId the primary key for the new students
	* @return the new students
	*/
	public static org.xmlportletfactory.portal.school.model.students createstudents(
		long studentId) {
		return getService().createstudents(studentId);
	}

	/**
	* Deletes the students with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param studentId the primary key of the students to delete
	* @throws PortalException if a students with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static void deletestudents(long studentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		getService().deletestudents(studentId);
	}

	/**
	* Deletes the students from the database. Also notifies the appropriate model listeners.
	*
	* @param students the students to delete
	* @throws SystemException if a system exception occurred
	*/
	public static void deletestudents(
		org.xmlportletfactory.portal.school.model.students students)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deletestudents(students);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Counts the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Gets the students with the primary key.
	*
	* @param studentId the primary key of the students to get
	* @return the students
	* @throws PortalException if a students with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.students getstudents(
		long studentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getstudents(studentId);
	}

	/**
	* Gets a range of all the studentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of studentses to return
	* @param end the upper bound of the range of studentses to return (not inclusive)
	* @return the range of studentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.xmlportletfactory.portal.school.model.students> getstudentses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getstudentses(start, end);
	}

	/**
	* Gets the number of studentses.
	*
	* @return the number of studentses
	* @throws SystemException if a system exception occurred
	*/
	public static int getstudentsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getstudentsesCount();
	}

	/**
	* Updates the students in the database. Also notifies the appropriate model listeners.
	*
	* @param students the students to update
	* @return the students that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.students updatestudents(
		org.xmlportletfactory.portal.school.model.students students)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updatestudents(students);
	}

	/**
	* Updates the students in the database. Also notifies the appropriate model listeners.
	*
	* @param students the students to update
	* @param merge whether to merge the students with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the students that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.students updatestudents(
		org.xmlportletfactory.portal.school.model.students students,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updatestudents(students, merge);
	}

	public static java.util.List findAllInUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInUser(userId);
	}

	public static java.util.List findAllInUser(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInUser(userId, orderByComparator);
	}

	public static java.util.List findAllInGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInGroup(groupId);
	}

	public static java.util.List findAllInGroup(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInGroup(groupId, orderByComparator);
	}

	public static java.util.List findAllInUserAndGroup(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInUserAndGroup(userId, groupId);
	}

	public static java.util.List findAllInUserAndGroup(long userId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .findAllInUserAndGroup(userId, groupId, orderByComparator);
	}

	public static java.util.List findAllInClassroomId(long classroomId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInClassroomId(classroomId);
	}

	public static java.util.List findAllIncourseIdGroup(long courseId,
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllIncourseIdGroup(courseId, groupId);
	}

	public static java.util.List findAllIncourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllIncourseId(courseId);
	}

	public static java.util.List findAllIncourseIdGroup(long courseId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .findAllIncourseIdGroup(courseId, groupId, orderByComparator);
	}

	public static void remove(
		org.xmlportletfactory.portal.school.model.students fileobj)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().remove(fileobj);
	}

	public static void clearService() {
		_service = null;
	}

	public static studentsLocalService getService() {
		if (_service == null) {
			Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
					studentsLocalService.class.getName());
			ClassLoader portletClassLoader = (ClassLoader)PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
					"portletClassLoader");

			ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
					portletClassLoader);

			_service = new studentsLocalServiceClp(classLoaderProxy);

			ClpSerializer.setClassLoader(portletClassLoader);
		}

		return _service;
	}

	public void setService(studentsLocalService service) {
		_service = service;
	}

	private static studentsLocalService _service;
}
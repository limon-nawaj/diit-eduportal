/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import org.xmlportletfactory.portal.school.model.subjects;

/**
 * The persistence interface for the subjects service.
 *
 * <p>
 * Never modify or reference this interface directly. Always use {@link subjectsUtil} to access the subjects persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
 * </p>
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Jack A. Rider
 * @see subjectsPersistenceImpl
 * @see subjectsUtil
 * @generated
 */
public interface subjectsPersistence extends BasePersistence<subjects> {
	/**
	* Caches the subjects in the entity cache if it is enabled.
	*
	* @param subjects the subjects to cache
	*/
	public void cacheResult(
		org.xmlportletfactory.portal.school.model.subjects subjects);

	/**
	* Caches the subjectses in the entity cache if it is enabled.
	*
	* @param subjectses the subjectses to cache
	*/
	public void cacheResult(
		java.util.List<org.xmlportletfactory.portal.school.model.subjects> subjectses);

	/**
	* Creates a new subjects with the primary key. Does not add the subjects to the database.
	*
	* @param subjectId the primary key for the new subjects
	* @return the new subjects
	*/
	public org.xmlportletfactory.portal.school.model.subjects create(
		long subjectId);

	/**
	* Removes the subjects with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param subjectId the primary key of the subjects to remove
	* @return the subjects that was removed
	* @throws org.xmlportletfactory.portal.school.NoSuchsubjectsException if a subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects remove(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchsubjectsException;

	public org.xmlportletfactory.portal.school.model.subjects updateImpl(
		org.xmlportletfactory.portal.school.model.subjects subjects,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds the subjects with the primary key or throws a {@link org.xmlportletfactory.portal.school.NoSuchsubjectsException} if it could not be found.
	*
	* @param subjectId the primary key of the subjects to find
	* @return the subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchsubjectsException if a subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects findByPrimaryKey(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchsubjectsException;

	/**
	* Finds the subjects with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param subjectId the primary key of the subjects to find
	* @return the subjects, or <code>null</code> if a subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects fetchByPrimaryKey(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds all the subjectses where groupId = &#63;.
	*
	* @param groupId the group id to search with
	* @return the matching subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds a range of all the subjectses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group id to search with
	* @param start the lower bound of the range of subjectses to return
	* @param end the upper bound of the range of subjectses to return (not inclusive)
	* @return the range of matching subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> findByGroupId(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds an ordered range of all the subjectses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group id to search with
	* @param start the lower bound of the range of subjectses to return
	* @param end the upper bound of the range of subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds the first subjects in the ordered set where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchsubjectsException if a matching subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects findByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchsubjectsException;

	/**
	* Finds the last subjects in the ordered set where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchsubjectsException if a matching subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects findByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchsubjectsException;

	/**
	* Finds the subjectses before and after the current subjects in the ordered set where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the primary key of the current subjects
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchsubjectsException if a subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects[] findByGroupId_PrevAndNext(
		long subjectId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchsubjectsException;

	/**
	* Filters by the user's permissions and finds all the subjectses where groupId = &#63;.
	*
	* @param groupId the group id to search with
	* @return the matching subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> filterFindByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and finds a range of all the subjectses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group id to search with
	* @param start the lower bound of the range of subjectses to return
	* @param end the upper bound of the range of subjectses to return (not inclusive)
	* @return the range of matching subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> filterFindByGroupId(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and finds an ordered range of all the subjectses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group id to search with
	* @param start the lower bound of the range of subjectses to return
	* @param end the upper bound of the range of subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> filterFindByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds all the subjectses where userId = &#63;.
	*
	* @param userId the user id to search with
	* @return the matching subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> findByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds a range of all the subjectses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param start the lower bound of the range of subjectses to return
	* @param end the upper bound of the range of subjectses to return (not inclusive)
	* @return the range of matching subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> findByUserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds an ordered range of all the subjectses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param start the lower bound of the range of subjectses to return
	* @param end the upper bound of the range of subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> findByUserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds the first subjects in the ordered set where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchsubjectsException if a matching subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects findByUserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchsubjectsException;

	/**
	* Finds the last subjects in the ordered set where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchsubjectsException if a matching subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects findByUserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchsubjectsException;

	/**
	* Finds the subjectses before and after the current subjects in the ordered set where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the primary key of the current subjects
	* @param userId the user id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchsubjectsException if a subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects[] findByUserId_PrevAndNext(
		long subjectId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchsubjectsException;

	/**
	* Finds all the subjectses where userId = &#63; and groupId = &#63;.
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @return the matching subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> findByUserIdGroupId(
		long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds a range of all the subjectses where userId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @param start the lower bound of the range of subjectses to return
	* @param end the upper bound of the range of subjectses to return (not inclusive)
	* @return the range of matching subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> findByUserIdGroupId(
		long userId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds an ordered range of all the subjectses where userId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @param start the lower bound of the range of subjectses to return
	* @param end the upper bound of the range of subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> findByUserIdGroupId(
		long userId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds the first subjects in the ordered set where userId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the first matching subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchsubjectsException if a matching subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects findByUserIdGroupId_First(
		long userId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchsubjectsException;

	/**
	* Finds the last subjects in the ordered set where userId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the last matching subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchsubjectsException if a matching subjects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects findByUserIdGroupId_Last(
		long userId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchsubjectsException;

	/**
	* Finds the subjectses before and after the current subjects in the ordered set where userId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the primary key of the current subjects
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @param orderByComparator the comparator to order the set by
	* @return the previous, current, and next subjects
	* @throws org.xmlportletfactory.portal.school.NoSuchsubjectsException if a subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects[] findByUserIdGroupId_PrevAndNext(
		long subjectId, long userId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.xmlportletfactory.portal.school.NoSuchsubjectsException;

	/**
	* Filters by the user's permissions and finds all the subjectses where userId = &#63; and groupId = &#63;.
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @return the matching subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> filterFindByUserIdGroupId(
		long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and finds a range of all the subjectses where userId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @param start the lower bound of the range of subjectses to return
	* @param end the upper bound of the range of subjectses to return (not inclusive)
	* @return the range of matching subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> filterFindByUserIdGroupId(
		long userId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and finds an ordered range of all the subjectses where userId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @param start the lower bound of the range of subjectses to return
	* @param end the upper bound of the range of subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> filterFindByUserIdGroupId(
		long userId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds all the subjectses.
	*
	* @return the subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds a range of all the subjectses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of subjectses to return
	* @param end the upper bound of the range of subjectses to return (not inclusive)
	* @return the range of subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Finds an ordered range of all the subjectses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of subjectses to return
	* @param end the upper bound of the range of subjectses to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the subjectses where groupId = &#63; from the database.
	*
	* @param groupId the group id to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the subjectses where userId = &#63; from the database.
	*
	* @param userId the user id to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the subjectses where userId = &#63; and groupId = &#63; from the database.
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUserIdGroupId(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the subjectses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Counts all the subjectses where groupId = &#63;.
	*
	* @param groupId the group id to search with
	* @return the number of matching subjectses
	* @throws SystemException if a system exception occurred
	*/
	public int countByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and counts all the subjectses where groupId = &#63;.
	*
	* @param groupId the group id to search with
	* @return the number of matching subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public int filterCountByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Counts all the subjectses where userId = &#63;.
	*
	* @param userId the user id to search with
	* @return the number of matching subjectses
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Counts all the subjectses where userId = &#63; and groupId = &#63;.
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @return the number of matching subjectses
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserIdGroupId(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Filters by the user's permissions and counts all the subjectses where userId = &#63; and groupId = &#63;.
	*
	* @param userId the user id to search with
	* @param groupId the group id to search with
	* @return the number of matching subjectses that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public int filterCountByUserIdGroupId(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Counts all the subjectses.
	*
	* @return the number of subjectses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ClassLoaderProxy;

/**
 * The utility for the classrooms local service. This utility wraps {@link org.xmlportletfactory.portal.school.service.impl.classroomsLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * Never modify this class directly. Add custom service methods to {@link org.xmlportletfactory.portal.school.service.impl.classroomsLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
 * </p>
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Jack A. Rider
 * @see classroomsLocalService
 * @see org.xmlportletfactory.portal.school.service.base.classroomsLocalServiceBaseImpl
 * @see org.xmlportletfactory.portal.school.service.impl.classroomsLocalServiceImpl
 * @generated
 */
public class classroomsLocalServiceUtil {
	/**
	* Adds the classrooms to the database. Also notifies the appropriate model listeners.
	*
	* @param classrooms the classrooms to add
	* @return the classrooms that was added
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.classrooms addclassrooms(
		org.xmlportletfactory.portal.school.model.classrooms classrooms)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addclassrooms(classrooms);
	}

	/**
	* Creates a new classrooms with the primary key. Does not add the classrooms to the database.
	*
	* @param classroomId the primary key for the new classrooms
	* @return the new classrooms
	*/
	public static org.xmlportletfactory.portal.school.model.classrooms createclassrooms(
		long classroomId) {
		return getService().createclassrooms(classroomId);
	}

	/**
	* Deletes the classrooms with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param classroomId the primary key of the classrooms to delete
	* @throws PortalException if a classrooms with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteclassrooms(long classroomId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		getService().deleteclassrooms(classroomId);
	}

	/**
	* Deletes the classrooms from the database. Also notifies the appropriate model listeners.
	*
	* @param classrooms the classrooms to delete
	* @throws SystemException if a system exception occurred
	*/
	public static void deleteclassrooms(
		org.xmlportletfactory.portal.school.model.classrooms classrooms)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteclassrooms(classrooms);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Counts the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Gets the classrooms with the primary key.
	*
	* @param classroomId the primary key of the classrooms to get
	* @return the classrooms
	* @throws PortalException if a classrooms with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.classrooms getclassrooms(
		long classroomId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getclassrooms(classroomId);
	}

	/**
	* Gets a range of all the classroomses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of classroomses to return
	* @param end the upper bound of the range of classroomses to return (not inclusive)
	* @return the range of classroomses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.xmlportletfactory.portal.school.model.classrooms> getclassroomses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getclassroomses(start, end);
	}

	/**
	* Gets the number of classroomses.
	*
	* @return the number of classroomses
	* @throws SystemException if a system exception occurred
	*/
	public static int getclassroomsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getclassroomsesCount();
	}

	/**
	* Updates the classrooms in the database. Also notifies the appropriate model listeners.
	*
	* @param classrooms the classrooms to update
	* @return the classrooms that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.classrooms updateclassrooms(
		org.xmlportletfactory.portal.school.model.classrooms classrooms)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateclassrooms(classrooms);
	}

	/**
	* Updates the classrooms in the database. Also notifies the appropriate model listeners.
	*
	* @param classrooms the classrooms to update
	* @param merge whether to merge the classrooms with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the classrooms that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static org.xmlportletfactory.portal.school.model.classrooms updateclassrooms(
		org.xmlportletfactory.portal.school.model.classrooms classrooms,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateclassrooms(classrooms, merge);
	}

	public static java.util.List findAllInUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInUser(userId);
	}

	public static java.util.List findAllInUser(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInUser(userId, orderByComparator);
	}

	public static java.util.List findAllInGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInGroup(groupId);
	}

	public static java.util.List findAllInGroup(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInGroup(groupId, orderByComparator);
	}

	public static java.util.List findAllInUserAndGroup(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllInUserAndGroup(userId, groupId);
	}

	public static java.util.List findAllInUserAndGroup(long userId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .findAllInUserAndGroup(userId, groupId, orderByComparator);
	}

	public static void remove(
		org.xmlportletfactory.portal.school.model.classrooms fileobj)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().remove(fileobj);
	}

	public static void clearService() {
		_service = null;
	}

	public static classroomsLocalService getService() {
		if (_service == null) {
			Object obj = PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
					classroomsLocalService.class.getName());
			ClassLoader portletClassLoader = (ClassLoader)PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,
					"portletClassLoader");

			ClassLoaderProxy classLoaderProxy = new ClassLoaderProxy(obj,
					portletClassLoader);

			_service = new classroomsLocalServiceClp(classLoaderProxy);

			ClpSerializer.setClassLoader(portletClassLoader);
		}

		return _service;
	}

	public void setService(classroomsLocalService service) {
		_service = service;
	}

	private static classroomsLocalService _service;
}
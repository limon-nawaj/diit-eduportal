/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service;

import com.liferay.portal.kernel.annotation.Isolation;
import com.liferay.portal.kernel.annotation.Propagation;
import com.liferay.portal.kernel.annotation.Transactional;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The interface for the subjects local service.
 *
 * <p>
 * Never modify or reference this interface directly. Always use {@link subjectsLocalServiceUtil} to access the subjects local service. Add custom service methods to {@link org.xmlportletfactory.portal.school.service.impl.subjectsLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
 * </p>
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Jack A. Rider
 * @see subjectsLocalServiceUtil
 * @see org.xmlportletfactory.portal.school.service.base.subjectsLocalServiceBaseImpl
 * @see org.xmlportletfactory.portal.school.service.impl.subjectsLocalServiceImpl
 * @generated
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface subjectsLocalService {
	/**
	* Adds the subjects to the database. Also notifies the appropriate model listeners.
	*
	* @param subjects the subjects to add
	* @return the subjects that was added
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects addsubjects(
		org.xmlportletfactory.portal.school.model.subjects subjects)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Creates a new subjects with the primary key. Does not add the subjects to the database.
	*
	* @param subjectId the primary key for the new subjects
	* @return the new subjects
	*/
	public org.xmlportletfactory.portal.school.model.subjects createsubjects(
		long subjectId);

	/**
	* Deletes the subjects with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param subjectId the primary key of the subjects to delete
	* @throws PortalException if a subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public void deletesubjects(long subjectId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Deletes the subjects from the database. Also notifies the appropriate model listeners.
	*
	* @param subjects the subjects to delete
	* @throws SystemException if a system exception occurred
	*/
	public void deletesubjects(
		org.xmlportletfactory.portal.school.model.subjects subjects)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Counts the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Gets the subjects with the primary key.
	*
	* @param subjectId the primary key of the subjects to get
	* @return the subjects
	* @throws PortalException if a subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public org.xmlportletfactory.portal.school.model.subjects getsubjects(
		long subjectId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Gets a range of all the subjectses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of subjectses to return
	* @param end the upper bound of the range of subjectses to return (not inclusive)
	* @return the range of subjectses
	* @throws SystemException if a system exception occurred
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.List<org.xmlportletfactory.portal.school.model.subjects> getsubjectses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Gets the number of subjectses.
	*
	* @return the number of subjectses
	* @throws SystemException if a system exception occurred
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getsubjectsesCount()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Updates the subjects in the database. Also notifies the appropriate model listeners.
	*
	* @param subjects the subjects to update
	* @return the subjects that was updated
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects updatesubjects(
		org.xmlportletfactory.portal.school.model.subjects subjects)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Updates the subjects in the database. Also notifies the appropriate model listeners.
	*
	* @param subjects the subjects to update
	* @param merge whether to merge the subjects with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the subjects that was updated
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.subjects updatesubjects(
		org.xmlportletfactory.portal.school.model.subjects subjects,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	public java.util.List findAllInUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	public java.util.List findAllInUser(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	public java.util.List findAllInGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	public java.util.List findAllInGroup(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	public java.util.List findAllInUserAndGroup(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	public java.util.List findAllInUserAndGroup(long userId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	public void remove(
		org.xmlportletfactory.portal.school.model.subjects fileobj)
		throws com.liferay.portal.kernel.exception.SystemException;
}
/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service;

/**
 * <p>
 * This class is a wrapper for {@link courseSubjectsLocalService}.
 * </p>
 *
 * @author    Jack A. Rider
 * @see       courseSubjectsLocalService
 * @generated
 */
public class courseSubjectsLocalServiceWrapper
	implements courseSubjectsLocalService {
	public courseSubjectsLocalServiceWrapper(
		courseSubjectsLocalService courseSubjectsLocalService) {
		_courseSubjectsLocalService = courseSubjectsLocalService;
	}

	/**
	* Adds the course subjects to the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjects the course subjects to add
	* @return the course subjects that was added
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects addcourseSubjects(
		org.xmlportletfactory.portal.school.model.courseSubjects courseSubjects)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.addcourseSubjects(courseSubjects);
	}

	/**
	* Creates a new course subjects with the primary key. Does not add the course subjects to the database.
	*
	* @param courseSubjectId the primary key for the new course subjects
	* @return the new course subjects
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects createcourseSubjects(
		long courseSubjectId) {
		return _courseSubjectsLocalService.createcourseSubjects(courseSubjectId);
	}

	/**
	* Deletes the course subjects with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjectId the primary key of the course subjects to delete
	* @throws PortalException if a course subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public void deletecourseSubjects(long courseSubjectId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		_courseSubjectsLocalService.deletecourseSubjects(courseSubjectId);
	}

	/**
	* Deletes the course subjects from the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjects the course subjects to delete
	* @throws SystemException if a system exception occurred
	*/
	public void deletecourseSubjects(
		org.xmlportletfactory.portal.school.model.courseSubjects courseSubjects)
		throws com.liferay.portal.kernel.exception.SystemException {
		_courseSubjectsLocalService.deletecourseSubjects(courseSubjects);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query to search with
	* @param start the lower bound of the range of model instances to return
	* @param end the upper bound of the range of model instances to return (not inclusive)
	* @param orderByComparator the comparator to order the results by
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Counts the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query to search with
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Gets the course subjects with the primary key.
	*
	* @param courseSubjectId the primary key of the course subjects to get
	* @return the course subjects
	* @throws PortalException if a course subjects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects getcourseSubjects(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.getcourseSubjects(courseSubjectId);
	}

	/**
	* Gets a range of all the course subjectses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course subjectses to return
	* @param end the upper bound of the range of course subjectses to return (not inclusive)
	* @return the range of course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.xmlportletfactory.portal.school.model.courseSubjects> getcourseSubjectses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.getcourseSubjectses(start, end);
	}

	/**
	* Gets the number of course subjectses.
	*
	* @return the number of course subjectses
	* @throws SystemException if a system exception occurred
	*/
	public int getcourseSubjectsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.getcourseSubjectsesCount();
	}

	/**
	* Updates the course subjects in the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjects the course subjects to update
	* @return the course subjects that was updated
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects updatecourseSubjects(
		org.xmlportletfactory.portal.school.model.courseSubjects courseSubjects)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.updatecourseSubjects(courseSubjects);
	}

	/**
	* Updates the course subjects in the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjects the course subjects to update
	* @param merge whether to merge the course subjects with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the course subjects that was updated
	* @throws SystemException if a system exception occurred
	*/
	public org.xmlportletfactory.portal.school.model.courseSubjects updatecourseSubjects(
		org.xmlportletfactory.portal.school.model.courseSubjects courseSubjects,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.updatecourseSubjects(courseSubjects,
			merge);
	}

	public java.util.List findAllInUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.findAllInUser(userId);
	}

	public java.util.List findAllInUser(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.findAllInUser(userId,
			orderByComparator);
	}

	public java.util.List findAllInGroup(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.findAllInGroup(groupId);
	}

	public java.util.List findAllInGroup(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.findAllInGroup(groupId,
			orderByComparator);
	}

	public java.util.List findAllInUserAndGroup(long userId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.findAllInUserAndGroup(userId, groupId);
	}

	public java.util.List findAllInUserAndGroup(long userId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.findAllInUserAndGroup(userId,
			groupId, orderByComparator);
	}

	public java.util.List findAllInSubjectId(long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.findAllInSubjectId(subjectId);
	}

	public java.util.List findAllInTeacherId(long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.findAllInTeacherId(teacherId);
	}

	public java.util.List findAllIncourseIdGroup(long courseId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.findAllIncourseIdGroup(courseId,
			groupId);
	}

	public java.util.List findAllIncourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.findAllIncourseId(courseId);
	}

	public java.util.List findAllIncourseIdGroup(long courseId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectsLocalService.findAllIncourseIdGroup(courseId,
			groupId, orderByComparator);
	}

	public void remove(
		org.xmlportletfactory.portal.school.model.courseSubjects fileobj)
		throws com.liferay.portal.kernel.exception.SystemException {
		_courseSubjectsLocalService.remove(fileobj);
	}

	public courseSubjectsLocalService getWrappedcourseSubjectsLocalService() {
		return _courseSubjectsLocalService;
	}

	private courseSubjectsLocalService _courseSubjectsLocalService;
}
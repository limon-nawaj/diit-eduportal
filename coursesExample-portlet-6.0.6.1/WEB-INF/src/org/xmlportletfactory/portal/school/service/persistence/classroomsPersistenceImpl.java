/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.security.permission.InlineSQLHelperUtil;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import org.xmlportletfactory.portal.school.NoSuchclassroomsException;
import org.xmlportletfactory.portal.school.model.classrooms;
import org.xmlportletfactory.portal.school.model.impl.classroomsImpl;
import org.xmlportletfactory.portal.school.model.impl.classroomsModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the classrooms service.
 *
 * <p>
 * Never modify or reference this class directly. Always use {@link classroomsUtil} to access the classrooms persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
 * </p>
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Jack A. Rider
 * @see classroomsPersistence
 * @see classroomsUtil
 * @generated
 */
public class classroomsPersistenceImpl extends BasePersistenceImpl<classrooms>
	implements classroomsPersistence {
	public static final String FINDER_CLASS_NAME_ENTITY = classroomsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
		".List";
	public static final FinderPath FINDER_PATH_FIND_BY_GROUPID = new FinderPath(classroomsModelImpl.ENTITY_CACHE_ENABLED,
			classroomsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findByGroupId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(classroomsModelImpl.ENTITY_CACHE_ENABLED,
			classroomsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countByGroupId", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_USERID = new FinderPath(classroomsModelImpl.ENTITY_CACHE_ENABLED,
			classroomsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findByUserId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(classroomsModelImpl.ENTITY_CACHE_ENABLED,
			classroomsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countByUserId", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_USERIDGROUPID = new FinderPath(classroomsModelImpl.ENTITY_CACHE_ENABLED,
			classroomsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findByUserIdGroupId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_USERIDGROUPID = new FinderPath(classroomsModelImpl.ENTITY_CACHE_ENABLED,
			classroomsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countByUserIdGroupId",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(classroomsModelImpl.ENTITY_CACHE_ENABLED,
			classroomsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(classroomsModelImpl.ENTITY_CACHE_ENABLED,
			classroomsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countAll", new String[0]);

	/**
	 * Caches the classrooms in the entity cache if it is enabled.
	 *
	 * @param classrooms the classrooms to cache
	 */
	public void cacheResult(classrooms classrooms) {
		EntityCacheUtil.putResult(classroomsModelImpl.ENTITY_CACHE_ENABLED,
			classroomsImpl.class, classrooms.getPrimaryKey(), classrooms);
	}

	/**
	 * Caches the classroomses in the entity cache if it is enabled.
	 *
	 * @param classroomses the classroomses to cache
	 */
	public void cacheResult(List<classrooms> classroomses) {
		for (classrooms classrooms : classroomses) {
			if (EntityCacheUtil.getResult(
						classroomsModelImpl.ENTITY_CACHE_ENABLED,
						classroomsImpl.class, classrooms.getPrimaryKey(), this) == null) {
				cacheResult(classrooms);
			}
		}
	}

	/**
	 * Clears the cache for all classroomses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	public void clearCache() {
		CacheRegistryUtil.clear(classroomsImpl.class.getName());
		EntityCacheUtil.clearCache(classroomsImpl.class.getName());
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
	}

	/**
	 * Clears the cache for the classrooms.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	public void clearCache(classrooms classrooms) {
		EntityCacheUtil.removeResult(classroomsModelImpl.ENTITY_CACHE_ENABLED,
			classroomsImpl.class, classrooms.getPrimaryKey());
	}

	/**
	 * Creates a new classrooms with the primary key. Does not add the classrooms to the database.
	 *
	 * @param classroomId the primary key for the new classrooms
	 * @return the new classrooms
	 */
	public classrooms create(long classroomId) {
		classrooms classrooms = new classroomsImpl();

		classrooms.setNew(true);
		classrooms.setPrimaryKey(classroomId);

		return classrooms;
	}

	/**
	 * Removes the classrooms with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the classrooms to remove
	 * @return the classrooms that was removed
	 * @throws com.liferay.portal.NoSuchModelException if a classrooms with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms remove(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return remove(((Long)primaryKey).longValue());
	}

	/**
	 * Removes the classrooms with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param classroomId the primary key of the classrooms to remove
	 * @return the classrooms that was removed
	 * @throws org.xmlportletfactory.portal.school.NoSuchclassroomsException if a classrooms with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms remove(long classroomId)
		throws NoSuchclassroomsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			classrooms classrooms = (classrooms)session.get(classroomsImpl.class,
					new Long(classroomId));

			if (classrooms == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + classroomId);
				}

				throw new NoSuchclassroomsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					classroomId);
			}

			return remove(classrooms);
		}
		catch (NoSuchclassroomsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected classrooms removeImpl(classrooms classrooms)
		throws SystemException {
		classrooms = toUnwrappedModel(classrooms);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, classrooms);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

		EntityCacheUtil.removeResult(classroomsModelImpl.ENTITY_CACHE_ENABLED,
			classroomsImpl.class, classrooms.getPrimaryKey());

		return classrooms;
	}

	public classrooms updateImpl(
		org.xmlportletfactory.portal.school.model.classrooms classrooms,
		boolean merge) throws SystemException {
		classrooms = toUnwrappedModel(classrooms);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, classrooms, merge);

			classrooms.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

		EntityCacheUtil.putResult(classroomsModelImpl.ENTITY_CACHE_ENABLED,
			classroomsImpl.class, classrooms.getPrimaryKey(), classrooms);

		return classrooms;
	}

	protected classrooms toUnwrappedModel(classrooms classrooms) {
		if (classrooms instanceof classroomsImpl) {
			return classrooms;
		}

		classroomsImpl classroomsImpl = new classroomsImpl();

		classroomsImpl.setNew(classrooms.isNew());
		classroomsImpl.setPrimaryKey(classrooms.getPrimaryKey());

		classroomsImpl.setClassroomId(classrooms.getClassroomId());
		classroomsImpl.setCompanyId(classrooms.getCompanyId());
		classroomsImpl.setGroupId(classrooms.getGroupId());
		classroomsImpl.setUserId(classrooms.getUserId());
		classroomsImpl.setClassroomName(classrooms.getClassroomName());

		return classroomsImpl;
	}

	/**
	 * Finds the classrooms with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the classrooms to find
	 * @return the classrooms
	 * @throws com.liferay.portal.NoSuchModelException if a classrooms with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Finds the classrooms with the primary key or throws a {@link org.xmlportletfactory.portal.school.NoSuchclassroomsException} if it could not be found.
	 *
	 * @param classroomId the primary key of the classrooms to find
	 * @return the classrooms
	 * @throws org.xmlportletfactory.portal.school.NoSuchclassroomsException if a classrooms with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms findByPrimaryKey(long classroomId)
		throws NoSuchclassroomsException, SystemException {
		classrooms classrooms = fetchByPrimaryKey(classroomId);

		if (classrooms == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + classroomId);
			}

			throw new NoSuchclassroomsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				classroomId);
		}

		return classrooms;
	}

	/**
	 * Finds the classrooms with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the classrooms to find
	 * @return the classrooms, or <code>null</code> if a classrooms with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Finds the classrooms with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param classroomId the primary key of the classrooms to find
	 * @return the classrooms, or <code>null</code> if a classrooms with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms fetchByPrimaryKey(long classroomId)
		throws SystemException {
		classrooms classrooms = (classrooms)EntityCacheUtil.getResult(classroomsModelImpl.ENTITY_CACHE_ENABLED,
				classroomsImpl.class, classroomId, this);

		if (classrooms == null) {
			Session session = null;

			try {
				session = openSession();

				classrooms = (classrooms)session.get(classroomsImpl.class,
						new Long(classroomId));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (classrooms != null) {
					cacheResult(classrooms);
				}

				closeSession(session);
			}
		}

		return classrooms;
	}

	/**
	 * Finds all the classroomses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the matching classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> findByGroupId(long groupId)
		throws SystemException {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the classroomses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of classroomses to return
	 * @param end the upper bound of the range of classroomses to return (not inclusive)
	 * @return the range of matching classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> findByGroupId(long groupId, int start, int end)
		throws SystemException {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the classroomses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of classroomses to return
	 * @param end the upper bound of the range of classroomses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> findByGroupId(long groupId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				groupId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<classrooms> list = (List<classrooms>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_GROUPID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_CLASSROOMS_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				list = (List<classrooms>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_GROUPID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_GROUPID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first classrooms in the ordered set where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching classrooms
	 * @throws org.xmlportletfactory.portal.school.NoSuchclassroomsException if a matching classrooms could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms findByGroupId_First(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchclassroomsException, SystemException {
		List<classrooms> list = findByGroupId(groupId, 0, 1, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchclassroomsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last classrooms in the ordered set where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching classrooms
	 * @throws org.xmlportletfactory.portal.school.NoSuchclassroomsException if a matching classrooms could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms findByGroupId_Last(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchclassroomsException, SystemException {
		int count = countByGroupId(groupId);

		List<classrooms> list = findByGroupId(groupId, count - 1, count,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchclassroomsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the classroomses before and after the current classrooms in the ordered set where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param classroomId the primary key of the current classrooms
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next classrooms
	 * @throws org.xmlportletfactory.portal.school.NoSuchclassroomsException if a classrooms with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms[] findByGroupId_PrevAndNext(long classroomId,
		long groupId, OrderByComparator orderByComparator)
		throws NoSuchclassroomsException, SystemException {
		classrooms classrooms = findByPrimaryKey(classroomId);

		Session session = null;

		try {
			session = openSession();

			classrooms[] array = new classroomsImpl[3];

			array[0] = getByGroupId_PrevAndNext(session, classrooms, groupId,
					orderByComparator, true);

			array[1] = classrooms;

			array[2] = getByGroupId_PrevAndNext(session, classrooms, groupId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected classrooms getByGroupId_PrevAndNext(Session session,
		classrooms classrooms, long groupId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLASSROOMS_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(classrooms);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<classrooms> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Filters by the user's permissions and finds all the classroomses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the matching classroomses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> filterFindByGroupId(long groupId)
		throws SystemException {
		return filterFindByGroupId(groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Filters by the user's permissions and finds a range of all the classroomses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of classroomses to return
	 * @param end the upper bound of the range of classroomses to return (not inclusive)
	 * @return the range of matching classroomses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> filterFindByGroupId(long groupId, int start, int end)
		throws SystemException {
		return filterFindByGroupId(groupId, start, end, null);
	}

	/**
	 * Filters by the user's permissions and finds an ordered range of all the classroomses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of classroomses to return
	 * @param end the upper bound of the range of classroomses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching classroomses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> filterFindByGroupId(long groupId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByGroupId(groupId, start, end, orderByComparator);
		}

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(3 +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(2);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_CLASSROOMS_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_CLASSROOMS_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_CLASSROOMS_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_TABLE,
					orderByComparator);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				classrooms.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				q.addEntity(_FILTER_ENTITY_ALIAS, classroomsImpl.class);
			}
			else {
				q.addEntity(_FILTER_ENTITY_TABLE, classroomsImpl.class);
			}

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(groupId);

			return (List<classrooms>)QueryUtil.list(q, getDialect(), start, end);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Finds all the classroomses where userId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @return the matching classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> findByUserId(long userId) throws SystemException {
		return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the classroomses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param start the lower bound of the range of classroomses to return
	 * @param end the upper bound of the range of classroomses to return (not inclusive)
	 * @return the range of matching classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> findByUserId(long userId, int start, int end)
		throws SystemException {
		return findByUserId(userId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the classroomses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param start the lower bound of the range of classroomses to return
	 * @param end the upper bound of the range of classroomses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> findByUserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				userId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<classrooms> list = (List<classrooms>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_USERID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_CLASSROOMS_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				list = (List<classrooms>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_USERID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_USERID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first classrooms in the ordered set where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching classrooms
	 * @throws org.xmlportletfactory.portal.school.NoSuchclassroomsException if a matching classrooms could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms findByUserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchclassroomsException, SystemException {
		List<classrooms> list = findByUserId(userId, 0, 1, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchclassroomsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last classrooms in the ordered set where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching classrooms
	 * @throws org.xmlportletfactory.portal.school.NoSuchclassroomsException if a matching classrooms could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms findByUserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchclassroomsException, SystemException {
		int count = countByUserId(userId);

		List<classrooms> list = findByUserId(userId, count - 1, count,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchclassroomsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the classroomses before and after the current classrooms in the ordered set where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param classroomId the primary key of the current classrooms
	 * @param userId the user id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next classrooms
	 * @throws org.xmlportletfactory.portal.school.NoSuchclassroomsException if a classrooms with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms[] findByUserId_PrevAndNext(long classroomId, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchclassroomsException, SystemException {
		classrooms classrooms = findByPrimaryKey(classroomId);

		Session session = null;

		try {
			session = openSession();

			classrooms[] array = new classroomsImpl[3];

			array[0] = getByUserId_PrevAndNext(session, classrooms, userId,
					orderByComparator, true);

			array[1] = classrooms;

			array[2] = getByUserId_PrevAndNext(session, classrooms, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected classrooms getByUserId_PrevAndNext(Session session,
		classrooms classrooms, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLASSROOMS_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(classrooms);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<classrooms> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Finds all the classroomses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the matching classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> findByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		return findByUserIdGroupId(userId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the classroomses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of classroomses to return
	 * @param end the upper bound of the range of classroomses to return (not inclusive)
	 * @return the range of matching classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> findByUserIdGroupId(long userId, long groupId,
		int start, int end) throws SystemException {
		return findByUserIdGroupId(userId, groupId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the classroomses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of classroomses to return
	 * @param end the upper bound of the range of classroomses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> findByUserIdGroupId(long userId, long groupId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		Object[] finderArgs = new Object[] {
				userId, groupId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<classrooms> list = (List<classrooms>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_USERIDGROUPID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLASSROOMS_WHERE);

			query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

			query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(groupId);

				list = (List<classrooms>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_USERIDGROUPID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_USERIDGROUPID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first classrooms in the ordered set where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching classrooms
	 * @throws org.xmlportletfactory.portal.school.NoSuchclassroomsException if a matching classrooms could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms findByUserIdGroupId_First(long userId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchclassroomsException, SystemException {
		List<classrooms> list = findByUserIdGroupId(userId, groupId, 0, 1,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchclassroomsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last classrooms in the ordered set where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching classrooms
	 * @throws org.xmlportletfactory.portal.school.NoSuchclassroomsException if a matching classrooms could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms findByUserIdGroupId_Last(long userId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchclassroomsException, SystemException {
		int count = countByUserIdGroupId(userId, groupId);

		List<classrooms> list = findByUserIdGroupId(userId, groupId, count - 1,
				count, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchclassroomsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the classroomses before and after the current classrooms in the ordered set where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param classroomId the primary key of the current classrooms
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next classrooms
	 * @throws org.xmlportletfactory.portal.school.NoSuchclassroomsException if a classrooms with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public classrooms[] findByUserIdGroupId_PrevAndNext(long classroomId,
		long userId, long groupId, OrderByComparator orderByComparator)
		throws NoSuchclassroomsException, SystemException {
		classrooms classrooms = findByPrimaryKey(classroomId);

		Session session = null;

		try {
			session = openSession();

			classrooms[] array = new classroomsImpl[3];

			array[0] = getByUserIdGroupId_PrevAndNext(session, classrooms,
					userId, groupId, orderByComparator, true);

			array[1] = classrooms;

			array[2] = getByUserIdGroupId_PrevAndNext(session, classrooms,
					userId, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected classrooms getByUserIdGroupId_PrevAndNext(Session session,
		classrooms classrooms, long userId, long groupId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLASSROOMS_WHERE);

		query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

		query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(classrooms);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<classrooms> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Filters by the user's permissions and finds all the classroomses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the matching classroomses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> filterFindByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		return filterFindByUserIdGroupId(userId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Filters by the user's permissions and finds a range of all the classroomses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of classroomses to return
	 * @param end the upper bound of the range of classroomses to return (not inclusive)
	 * @return the range of matching classroomses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> filterFindByUserIdGroupId(long userId,
		long groupId, int start, int end) throws SystemException {
		return filterFindByUserIdGroupId(userId, groupId, start, end, null);
	}

	/**
	 * Filters by the user's permissions and finds an ordered range of all the classroomses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of classroomses to return
	 * @param end the upper bound of the range of classroomses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching classroomses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> filterFindByUserIdGroupId(long userId,
		long groupId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByUserIdGroupId(userId, groupId, start, end,
				orderByComparator);
		}

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_CLASSROOMS_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_CLASSROOMS_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

		query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_CLASSROOMS_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_TABLE,
					orderByComparator);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				classrooms.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				q.addEntity(_FILTER_ENTITY_ALIAS, classroomsImpl.class);
			}
			else {
				q.addEntity(_FILTER_ENTITY_TABLE, classroomsImpl.class);
			}

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(userId);

			qPos.add(groupId);

			return (List<classrooms>)QueryUtil.list(q, getDialect(), start, end);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Finds all the classroomses.
	 *
	 * @return the classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the classroomses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of classroomses to return
	 * @param end the upper bound of the range of classroomses to return (not inclusive)
	 * @return the range of classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Finds an ordered range of all the classroomses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of classroomses to return
	 * @param end the upper bound of the range of classroomses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public List<classrooms> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<classrooms> list = (List<classrooms>)FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLASSROOMS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLASSROOMS;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<classrooms>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<classrooms>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_ALL,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs,
						list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the classroomses where groupId = &#63; from the database.
	 *
	 * @param groupId the group id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByGroupId(long groupId) throws SystemException {
		for (classrooms classrooms : findByGroupId(groupId)) {
			remove(classrooms);
		}
	}

	/**
	 * Removes all the classroomses where userId = &#63; from the database.
	 *
	 * @param userId the user id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUserId(long userId) throws SystemException {
		for (classrooms classrooms : findByUserId(userId)) {
			remove(classrooms);
		}
	}

	/**
	 * Removes all the classroomses where userId = &#63; and groupId = &#63; from the database.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		for (classrooms classrooms : findByUserIdGroupId(userId, groupId)) {
			remove(classrooms);
		}
	}

	/**
	 * Removes all the classroomses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (classrooms classrooms : findAll()) {
			remove(classrooms);
		}
	}

	/**
	 * Counts all the classroomses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the number of matching classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByGroupId(long groupId) throws SystemException {
		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_GROUPID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLASSROOMS_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_GROUPID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Filters by the user's permissions and counts all the classroomses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the number of matching classroomses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public int filterCountByGroupId(long groupId) throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByGroupId(groupId);
		}

		StringBundler query = new StringBundler(2);

		query.append(_FILTER_SQL_COUNT_CLASSROOMS_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				classrooms.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			q.addScalar(COUNT_COLUMN_NAME,
				com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(groupId);

			Long count = (Long)q.uniqueResult();

			return count.intValue();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Counts all the classroomses where userId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @return the number of matching classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUserId(long userId) throws SystemException {
		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLASSROOMS_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Counts all the classroomses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the number of matching classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		Object[] finderArgs = new Object[] { userId, groupId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERIDGROUPID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLASSROOMS_WHERE);

			query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

			query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERIDGROUPID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Filters by the user's permissions and counts all the classroomses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the number of matching classroomses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public int filterCountByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByUserIdGroupId(userId, groupId);
		}

		StringBundler query = new StringBundler(3);

		query.append(_FILTER_SQL_COUNT_CLASSROOMS_WHERE);

		query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

		query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				classrooms.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			q.addScalar(COUNT_COLUMN_NAME,
				com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(userId);

			qPos.add(groupId);

			Long count = (Long)q.uniqueResult();

			return count.intValue();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Counts all the classroomses.
	 *
	 * @return the number of classroomses
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Object[] finderArgs = new Object[0];

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLASSROOMS);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
					count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the classrooms persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.org.xmlportletfactory.portal.school.model.classrooms")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<classrooms>> listenersList = new ArrayList<ModelListener<classrooms>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<classrooms>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(classroomsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST);
	}

	@BeanReference(type = coursesPersistence.class)
	protected coursesPersistence coursesPersistence;
	@BeanReference(type = studentsPersistence.class)
	protected studentsPersistence studentsPersistence;
	@BeanReference(type = classroomsPersistence.class)
	protected classroomsPersistence classroomsPersistence;
	@BeanReference(type = commentsPersistence.class)
	protected commentsPersistence commentsPersistence;
	@BeanReference(type = courseSubjectsPersistence.class)
	protected courseSubjectsPersistence courseSubjectsPersistence;
	@BeanReference(type = subjectsPersistence.class)
	protected subjectsPersistence subjectsPersistence;
	@BeanReference(type = teachersPersistence.class)
	protected teachersPersistence teachersPersistence;
	@BeanReference(type = holidaysPersistence.class)
	protected holidaysPersistence holidaysPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_CLASSROOMS = "SELECT classrooms FROM classrooms classrooms";
	private static final String _SQL_SELECT_CLASSROOMS_WHERE = "SELECT classrooms FROM classrooms classrooms WHERE ";
	private static final String _SQL_COUNT_CLASSROOMS = "SELECT COUNT(classrooms) FROM classrooms classrooms";
	private static final String _SQL_COUNT_CLASSROOMS_WHERE = "SELECT COUNT(classrooms) FROM classrooms classrooms WHERE ";
	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "classrooms.groupId = ?";
	private static final String _FINDER_COLUMN_USERID_USERID_2 = "classrooms.userId = ?";
	private static final String _FINDER_COLUMN_USERIDGROUPID_USERID_2 = "classrooms.userId = ? AND ";
	private static final String _FINDER_COLUMN_USERIDGROUPID_GROUPID_2 = "classrooms.groupId = ?";
	private static final String _FILTER_SQL_SELECT_CLASSROOMS_WHERE = "SELECT DISTINCT {classrooms.*} FROM coursesexample_classrooms classrooms WHERE ";
	private static final String _FILTER_SQL_SELECT_CLASSROOMS_NO_INLINE_DISTINCT_WHERE_1 =
		"SELECT {coursesexample_classrooms.*} FROM (SELECT DISTINCT classrooms.classroomId FROM coursesexample_classrooms classrooms WHERE ";
	private static final String _FILTER_SQL_SELECT_CLASSROOMS_NO_INLINE_DISTINCT_WHERE_2 =
		") TEMP_TABLE INNER JOIN coursesexample_classrooms ON TEMP_TABLE.classroomId = coursesexample_classrooms.classroomId";
	private static final String _FILTER_SQL_COUNT_CLASSROOMS_WHERE = "SELECT COUNT(DISTINCT classrooms.classroomId) AS COUNT_VALUE FROM coursesexample_classrooms classrooms WHERE ";
	private static final String _FILTER_COLUMN_PK = "classrooms.classroomId";
	private static final String _FILTER_COLUMN_USERID = "classrooms.userId";
	private static final String _FILTER_ENTITY_ALIAS = "classrooms";
	private static final String _FILTER_ENTITY_TABLE = "coursesexample_classrooms";
	private static final String _ORDER_BY_ENTITY_ALIAS = "classrooms.";
	private static final String _ORDER_BY_ENTITY_TABLE = "coursesexample_classrooms.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No classrooms exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No classrooms exists with the key {";
	private static Log _log = LogFactoryUtil.getLog(classroomsPersistenceImpl.class);
}
/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.security.permission.InlineSQLHelperUtil;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import org.xmlportletfactory.portal.school.NoSuchstudentsException;
import org.xmlportletfactory.portal.school.model.impl.studentsImpl;
import org.xmlportletfactory.portal.school.model.impl.studentsModelImpl;
import org.xmlportletfactory.portal.school.model.students;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the students service.
 *
 * <p>
 * Never modify or reference this class directly. Always use {@link studentsUtil} to access the students persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
 * </p>
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Jack A. Rider
 * @see studentsPersistence
 * @see studentsUtil
 * @generated
 */
public class studentsPersistenceImpl extends BasePersistenceImpl<students>
	implements studentsPersistence {
	public static final String FINDER_CLASS_NAME_ENTITY = studentsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
		".List";
	public static final FinderPath FINDER_PATH_FIND_BY_GROUPID = new FinderPath(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findByGroupId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countByGroupId", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_USERID = new FinderPath(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findByUserId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countByUserId", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_USERIDGROUPID = new FinderPath(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findByUserIdGroupId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_USERIDGROUPID = new FinderPath(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countByUserIdGroupId",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_CLASSROOMID = new FinderPath(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findByClassroomId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_CLASSROOMID = new FinderPath(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countByClassroomId", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_COURSEID = new FinderPath(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findByCourseId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_COURSEID = new FinderPath(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countByCourseId", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_COURSEIDGROUPID = new FinderPath(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findByCourseIdGroupId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_COURSEIDGROUPID = new FinderPath(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countByCourseIdGroupId",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countAll", new String[0]);

	/**
	 * Caches the students in the entity cache if it is enabled.
	 *
	 * @param students the students to cache
	 */
	public void cacheResult(students students) {
		EntityCacheUtil.putResult(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsImpl.class, students.getPrimaryKey(), students);
	}

	/**
	 * Caches the studentses in the entity cache if it is enabled.
	 *
	 * @param studentses the studentses to cache
	 */
	public void cacheResult(List<students> studentses) {
		for (students students : studentses) {
			if (EntityCacheUtil.getResult(
						studentsModelImpl.ENTITY_CACHE_ENABLED,
						studentsImpl.class, students.getPrimaryKey(), this) == null) {
				cacheResult(students);
			}
		}
	}

	/**
	 * Clears the cache for all studentses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	public void clearCache() {
		CacheRegistryUtil.clear(studentsImpl.class.getName());
		EntityCacheUtil.clearCache(studentsImpl.class.getName());
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
	}

	/**
	 * Clears the cache for the students.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	public void clearCache(students students) {
		EntityCacheUtil.removeResult(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsImpl.class, students.getPrimaryKey());
	}

	/**
	 * Creates a new students with the primary key. Does not add the students to the database.
	 *
	 * @param studentId the primary key for the new students
	 * @return the new students
	 */
	public students create(long studentId) {
		students students = new studentsImpl();

		students.setNew(true);
		students.setPrimaryKey(studentId);

		return students;
	}

	/**
	 * Removes the students with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the students to remove
	 * @return the students that was removed
	 * @throws com.liferay.portal.NoSuchModelException if a students with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students remove(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return remove(((Long)primaryKey).longValue());
	}

	/**
	 * Removes the students with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param studentId the primary key of the students to remove
	 * @return the students that was removed
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a students with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students remove(long studentId)
		throws NoSuchstudentsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			students students = (students)session.get(studentsImpl.class,
					new Long(studentId));

			if (students == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + studentId);
				}

				throw new NoSuchstudentsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					studentId);
			}

			return remove(students);
		}
		catch (NoSuchstudentsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected students removeImpl(students students) throws SystemException {
		students = toUnwrappedModel(students);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, students);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

		EntityCacheUtil.removeResult(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsImpl.class, students.getPrimaryKey());

		return students;
	}

	public students updateImpl(
		org.xmlportletfactory.portal.school.model.students students,
		boolean merge) throws SystemException {
		students = toUnwrappedModel(students);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, students, merge);

			students.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

		EntityCacheUtil.putResult(studentsModelImpl.ENTITY_CACHE_ENABLED,
			studentsImpl.class, students.getPrimaryKey(), students);

		return students;
	}

	protected students toUnwrappedModel(students students) {
		if (students instanceof studentsImpl) {
			return students;
		}

		studentsImpl studentsImpl = new studentsImpl();

		studentsImpl.setNew(students.isNew());
		studentsImpl.setPrimaryKey(students.getPrimaryKey());

		studentsImpl.setStudentId(students.getStudentId());
		studentsImpl.setCompanyId(students.getCompanyId());
		studentsImpl.setGroupId(students.getGroupId());
		studentsImpl.setUserId(students.getUserId());
		studentsImpl.setCourseId(students.getCourseId());
		studentsImpl.setStudentName(students.getStudentName());
		studentsImpl.setClassroomId(students.getClassroomId());
		studentsImpl.setStudentPhoto(students.getStudentPhoto());
		studentsImpl.setFolderIGId(students.getFolderIGId());

		return studentsImpl;
	}

	/**
	 * Finds the students with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the students to find
	 * @return the students
	 * @throws com.liferay.portal.NoSuchModelException if a students with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Finds the students with the primary key or throws a {@link org.xmlportletfactory.portal.school.NoSuchstudentsException} if it could not be found.
	 *
	 * @param studentId the primary key of the students to find
	 * @return the students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a students with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students findByPrimaryKey(long studentId)
		throws NoSuchstudentsException, SystemException {
		students students = fetchByPrimaryKey(studentId);

		if (students == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + studentId);
			}

			throw new NoSuchstudentsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				studentId);
		}

		return students;
	}

	/**
	 * Finds the students with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the students to find
	 * @return the students, or <code>null</code> if a students with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Finds the students with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param studentId the primary key of the students to find
	 * @return the students, or <code>null</code> if a students with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students fetchByPrimaryKey(long studentId) throws SystemException {
		students students = (students)EntityCacheUtil.getResult(studentsModelImpl.ENTITY_CACHE_ENABLED,
				studentsImpl.class, studentId, this);

		if (students == null) {
			Session session = null;

			try {
				session = openSession();

				students = (students)session.get(studentsImpl.class,
						new Long(studentId));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (students != null) {
					cacheResult(students);
				}

				closeSession(session);
			}
		}

		return students;
	}

	/**
	 * Finds all the studentses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByGroupId(long groupId) throws SystemException {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the studentses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @return the range of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByGroupId(long groupId, int start, int end)
		throws SystemException {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the studentses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByGroupId(long groupId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				groupId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<students> list = (List<students>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_GROUPID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_STUDENTS_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				list = (List<students>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_GROUPID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_GROUPID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first students in the ordered set where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a matching students could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students findByGroupId_First(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		List<students> list = findByGroupId(groupId, 0, 1, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchstudentsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last students in the ordered set where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a matching students could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students findByGroupId_Last(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		int count = countByGroupId(groupId);

		List<students> list = findByGroupId(groupId, count - 1, count,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchstudentsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the studentses before and after the current students in the ordered set where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the primary key of the current students
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a students with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students[] findByGroupId_PrevAndNext(long studentId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		students students = findByPrimaryKey(studentId);

		Session session = null;

		try {
			session = openSession();

			students[] array = new studentsImpl[3];

			array[0] = getByGroupId_PrevAndNext(session, students, groupId,
					orderByComparator, true);

			array[1] = students;

			array[2] = getByGroupId_PrevAndNext(session, students, groupId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected students getByGroupId_PrevAndNext(Session session,
		students students, long groupId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_STUDENTS_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(students);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<students> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Filters by the user's permissions and finds all the studentses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the matching studentses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> filterFindByGroupId(long groupId)
		throws SystemException {
		return filterFindByGroupId(groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Filters by the user's permissions and finds a range of all the studentses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @return the range of matching studentses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> filterFindByGroupId(long groupId, int start, int end)
		throws SystemException {
		return filterFindByGroupId(groupId, start, end, null);
	}

	/**
	 * Filters by the user's permissions and finds an ordered range of all the studentses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching studentses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> filterFindByGroupId(long groupId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByGroupId(groupId, start, end, orderByComparator);
		}

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(3 +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(2);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_STUDENTS_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_STUDENTS_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_STUDENTS_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_TABLE,
					orderByComparator);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				students.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				q.addEntity(_FILTER_ENTITY_ALIAS, studentsImpl.class);
			}
			else {
				q.addEntity(_FILTER_ENTITY_TABLE, studentsImpl.class);
			}

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(groupId);

			return (List<students>)QueryUtil.list(q, getDialect(), start, end);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Finds all the studentses where userId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @return the matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByUserId(long userId) throws SystemException {
		return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the studentses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @return the range of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByUserId(long userId, int start, int end)
		throws SystemException {
		return findByUserId(userId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the studentses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByUserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				userId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<students> list = (List<students>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_USERID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_STUDENTS_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				list = (List<students>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_USERID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_USERID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first students in the ordered set where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a matching students could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students findByUserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		List<students> list = findByUserId(userId, 0, 1, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchstudentsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last students in the ordered set where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a matching students could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students findByUserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		int count = countByUserId(userId);

		List<students> list = findByUserId(userId, count - 1, count,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchstudentsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the studentses before and after the current students in the ordered set where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the primary key of the current students
	 * @param userId the user id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a students with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students[] findByUserId_PrevAndNext(long studentId, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		students students = findByPrimaryKey(studentId);

		Session session = null;

		try {
			session = openSession();

			students[] array = new studentsImpl[3];

			array[0] = getByUserId_PrevAndNext(session, students, userId,
					orderByComparator, true);

			array[1] = students;

			array[2] = getByUserId_PrevAndNext(session, students, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected students getByUserId_PrevAndNext(Session session,
		students students, long userId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_STUDENTS_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(students);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<students> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Finds all the studentses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		return findByUserIdGroupId(userId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the studentses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @return the range of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByUserIdGroupId(long userId, long groupId,
		int start, int end) throws SystemException {
		return findByUserIdGroupId(userId, groupId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the studentses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByUserIdGroupId(long userId, long groupId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		Object[] finderArgs = new Object[] {
				userId, groupId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<students> list = (List<students>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_USERIDGROUPID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_STUDENTS_WHERE);

			query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

			query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(groupId);

				list = (List<students>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_USERIDGROUPID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_USERIDGROUPID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first students in the ordered set where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a matching students could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students findByUserIdGroupId_First(long userId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		List<students> list = findByUserIdGroupId(userId, groupId, 0, 1,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchstudentsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last students in the ordered set where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a matching students could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students findByUserIdGroupId_Last(long userId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		int count = countByUserIdGroupId(userId, groupId);

		List<students> list = findByUserIdGroupId(userId, groupId, count - 1,
				count, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchstudentsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the studentses before and after the current students in the ordered set where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the primary key of the current students
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a students with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students[] findByUserIdGroupId_PrevAndNext(long studentId,
		long userId, long groupId, OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		students students = findByPrimaryKey(studentId);

		Session session = null;

		try {
			session = openSession();

			students[] array = new studentsImpl[3];

			array[0] = getByUserIdGroupId_PrevAndNext(session, students,
					userId, groupId, orderByComparator, true);

			array[1] = students;

			array[2] = getByUserIdGroupId_PrevAndNext(session, students,
					userId, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected students getByUserIdGroupId_PrevAndNext(Session session,
		students students, long userId, long groupId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_STUDENTS_WHERE);

		query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

		query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(students);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<students> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Filters by the user's permissions and finds all the studentses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the matching studentses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> filterFindByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		return filterFindByUserIdGroupId(userId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Filters by the user's permissions and finds a range of all the studentses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @return the range of matching studentses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> filterFindByUserIdGroupId(long userId, long groupId,
		int start, int end) throws SystemException {
		return filterFindByUserIdGroupId(userId, groupId, start, end, null);
	}

	/**
	 * Filters by the user's permissions and finds an ordered range of all the studentses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching studentses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> filterFindByUserIdGroupId(long userId, long groupId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByUserIdGroupId(userId, groupId, start, end,
				orderByComparator);
		}

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_STUDENTS_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_STUDENTS_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

		query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_STUDENTS_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_TABLE,
					orderByComparator);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				students.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				q.addEntity(_FILTER_ENTITY_ALIAS, studentsImpl.class);
			}
			else {
				q.addEntity(_FILTER_ENTITY_TABLE, studentsImpl.class);
			}

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(userId);

			qPos.add(groupId);

			return (List<students>)QueryUtil.list(q, getDialect(), start, end);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Finds all the studentses where classroomId = &#63;.
	 *
	 * @param classroomId the classroom id to search with
	 * @return the matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByClassroomId(long classroomId)
		throws SystemException {
		return findByClassroomId(classroomId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the studentses where classroomId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param classroomId the classroom id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @return the range of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByClassroomId(long classroomId, int start, int end)
		throws SystemException {
		return findByClassroomId(classroomId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the studentses where classroomId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param classroomId the classroom id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByClassroomId(long classroomId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				classroomId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<students> list = (List<students>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_CLASSROOMID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_STUDENTS_WHERE);

			query.append(_FINDER_COLUMN_CLASSROOMID_CLASSROOMID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(classroomId);

				list = (List<students>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_CLASSROOMID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_CLASSROOMID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first students in the ordered set where classroomId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param classroomId the classroom id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a matching students could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students findByClassroomId_First(long classroomId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		List<students> list = findByClassroomId(classroomId, 0, 1,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("classroomId=");
			msg.append(classroomId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchstudentsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last students in the ordered set where classroomId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param classroomId the classroom id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a matching students could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students findByClassroomId_Last(long classroomId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		int count = countByClassroomId(classroomId);

		List<students> list = findByClassroomId(classroomId, count - 1, count,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("classroomId=");
			msg.append(classroomId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchstudentsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the studentses before and after the current students in the ordered set where classroomId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the primary key of the current students
	 * @param classroomId the classroom id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a students with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students[] findByClassroomId_PrevAndNext(long studentId,
		long classroomId, OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		students students = findByPrimaryKey(studentId);

		Session session = null;

		try {
			session = openSession();

			students[] array = new studentsImpl[3];

			array[0] = getByClassroomId_PrevAndNext(session, students,
					classroomId, orderByComparator, true);

			array[1] = students;

			array[2] = getByClassroomId_PrevAndNext(session, students,
					classroomId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected students getByClassroomId_PrevAndNext(Session session,
		students students, long classroomId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_STUDENTS_WHERE);

		query.append(_FINDER_COLUMN_CLASSROOMID_CLASSROOMID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(classroomId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(students);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<students> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Finds all the studentses where courseId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @return the matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByCourseId(long courseId)
		throws SystemException {
		return findByCourseId(courseId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Finds a range of all the studentses where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @return the range of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByCourseId(long courseId, int start, int end)
		throws SystemException {
		return findByCourseId(courseId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the studentses where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByCourseId(long courseId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				courseId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<students> list = (List<students>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_COURSEID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_STUDENTS_WHERE);

			query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				list = (List<students>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_COURSEID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_COURSEID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first students in the ordered set where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a matching students could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students findByCourseId_First(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		List<students> list = findByCourseId(courseId, 0, 1, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("courseId=");
			msg.append(courseId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchstudentsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last students in the ordered set where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a matching students could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students findByCourseId_Last(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		int count = countByCourseId(courseId);

		List<students> list = findByCourseId(courseId, count - 1, count,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("courseId=");
			msg.append(courseId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchstudentsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the studentses before and after the current students in the ordered set where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the primary key of the current students
	 * @param courseId the course id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a students with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students[] findByCourseId_PrevAndNext(long studentId, long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		students students = findByPrimaryKey(studentId);

		Session session = null;

		try {
			session = openSession();

			students[] array = new studentsImpl[3];

			array[0] = getByCourseId_PrevAndNext(session, students, courseId,
					orderByComparator, true);

			array[1] = students;

			array[2] = getByCourseId_PrevAndNext(session, students, courseId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected students getByCourseId_PrevAndNext(Session session,
		students students, long courseId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_STUDENTS_WHERE);

		query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(courseId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(students);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<students> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Finds all the studentses where courseId = &#63; and groupId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @return the matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByCourseIdGroupId(long courseId, long groupId)
		throws SystemException {
		return findByCourseIdGroupId(courseId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the studentses where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @return the range of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByCourseIdGroupId(long courseId, long groupId,
		int start, int end) throws SystemException {
		return findByCourseIdGroupId(courseId, groupId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the studentses where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findByCourseIdGroupId(long courseId, long groupId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		Object[] finderArgs = new Object[] {
				courseId, groupId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<students> list = (List<students>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_COURSEIDGROUPID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_STUDENTS_WHERE);

			query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

			query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				qPos.add(groupId);

				list = (List<students>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_COURSEIDGROUPID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_COURSEIDGROUPID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first students in the ordered set where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a matching students could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students findByCourseIdGroupId_First(long courseId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		List<students> list = findByCourseIdGroupId(courseId, groupId, 0, 1,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("courseId=");
			msg.append(courseId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchstudentsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last students in the ordered set where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a matching students could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students findByCourseIdGroupId_Last(long courseId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		int count = countByCourseIdGroupId(courseId, groupId);

		List<students> list = findByCourseIdGroupId(courseId, groupId,
				count - 1, count, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("courseId=");
			msg.append(courseId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchstudentsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the studentses before and after the current students in the ordered set where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the primary key of the current students
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next students
	 * @throws org.xmlportletfactory.portal.school.NoSuchstudentsException if a students with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public students[] findByCourseIdGroupId_PrevAndNext(long studentId,
		long courseId, long groupId, OrderByComparator orderByComparator)
		throws NoSuchstudentsException, SystemException {
		students students = findByPrimaryKey(studentId);

		Session session = null;

		try {
			session = openSession();

			students[] array = new studentsImpl[3];

			array[0] = getByCourseIdGroupId_PrevAndNext(session, students,
					courseId, groupId, orderByComparator, true);

			array[1] = students;

			array[2] = getByCourseIdGroupId_PrevAndNext(session, students,
					courseId, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected students getByCourseIdGroupId_PrevAndNext(Session session,
		students students, long courseId, long groupId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_STUDENTS_WHERE);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(courseId);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(students);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<students> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Filters by the user's permissions and finds all the studentses where courseId = &#63; and groupId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @return the matching studentses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> filterFindByCourseIdGroupId(long courseId,
		long groupId) throws SystemException {
		return filterFindByCourseIdGroupId(courseId, groupId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Filters by the user's permissions and finds a range of all the studentses where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @return the range of matching studentses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> filterFindByCourseIdGroupId(long courseId,
		long groupId, int start, int end) throws SystemException {
		return filterFindByCourseIdGroupId(courseId, groupId, start, end, null);
	}

	/**
	 * Filters by the user's permissions and finds an ordered range of all the studentses where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching studentses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> filterFindByCourseIdGroupId(long courseId,
		long groupId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByCourseIdGroupId(courseId, groupId, start, end,
				orderByComparator);
		}

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_STUDENTS_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_STUDENTS_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_STUDENTS_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_TABLE,
					orderByComparator);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				students.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				q.addEntity(_FILTER_ENTITY_ALIAS, studentsImpl.class);
			}
			else {
				q.addEntity(_FILTER_ENTITY_TABLE, studentsImpl.class);
			}

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(courseId);

			qPos.add(groupId);

			return (List<students>)QueryUtil.list(q, getDialect(), start, end);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Finds all the studentses.
	 *
	 * @return the studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the studentses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @return the range of studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Finds an ordered range of all the studentses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of studentses to return
	 * @param end the upper bound of the range of studentses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of studentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<students> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<students> list = (List<students>)FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_STUDENTS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_STUDENTS;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<students>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<students>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_ALL,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs,
						list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the studentses where groupId = &#63; from the database.
	 *
	 * @param groupId the group id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByGroupId(long groupId) throws SystemException {
		for (students students : findByGroupId(groupId)) {
			remove(students);
		}
	}

	/**
	 * Removes all the studentses where userId = &#63; from the database.
	 *
	 * @param userId the user id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUserId(long userId) throws SystemException {
		for (students students : findByUserId(userId)) {
			remove(students);
		}
	}

	/**
	 * Removes all the studentses where userId = &#63; and groupId = &#63; from the database.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		for (students students : findByUserIdGroupId(userId, groupId)) {
			remove(students);
		}
	}

	/**
	 * Removes all the studentses where classroomId = &#63; from the database.
	 *
	 * @param classroomId the classroom id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByClassroomId(long classroomId) throws SystemException {
		for (students students : findByClassroomId(classroomId)) {
			remove(students);
		}
	}

	/**
	 * Removes all the studentses where courseId = &#63; from the database.
	 *
	 * @param courseId the course id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCourseId(long courseId) throws SystemException {
		for (students students : findByCourseId(courseId)) {
			remove(students);
		}
	}

	/**
	 * Removes all the studentses where courseId = &#63; and groupId = &#63; from the database.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCourseIdGroupId(long courseId, long groupId)
		throws SystemException {
		for (students students : findByCourseIdGroupId(courseId, groupId)) {
			remove(students);
		}
	}

	/**
	 * Removes all the studentses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (students students : findAll()) {
			remove(students);
		}
	}

	/**
	 * Counts all the studentses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the number of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByGroupId(long groupId) throws SystemException {
		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_GROUPID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_STUDENTS_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_GROUPID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Filters by the user's permissions and counts all the studentses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the number of matching studentses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public int filterCountByGroupId(long groupId) throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByGroupId(groupId);
		}

		StringBundler query = new StringBundler(2);

		query.append(_FILTER_SQL_COUNT_STUDENTS_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				students.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			q.addScalar(COUNT_COLUMN_NAME,
				com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(groupId);

			Long count = (Long)q.uniqueResult();

			return count.intValue();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Counts all the studentses where userId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @return the number of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUserId(long userId) throws SystemException {
		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_STUDENTS_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Counts all the studentses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the number of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		Object[] finderArgs = new Object[] { userId, groupId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERIDGROUPID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_STUDENTS_WHERE);

			query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

			query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERIDGROUPID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Filters by the user's permissions and counts all the studentses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the number of matching studentses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public int filterCountByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByUserIdGroupId(userId, groupId);
		}

		StringBundler query = new StringBundler(3);

		query.append(_FILTER_SQL_COUNT_STUDENTS_WHERE);

		query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

		query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				students.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			q.addScalar(COUNT_COLUMN_NAME,
				com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(userId);

			qPos.add(groupId);

			Long count = (Long)q.uniqueResult();

			return count.intValue();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Counts all the studentses where classroomId = &#63;.
	 *
	 * @param classroomId the classroom id to search with
	 * @return the number of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByClassroomId(long classroomId) throws SystemException {
		Object[] finderArgs = new Object[] { classroomId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_CLASSROOMID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_STUDENTS_WHERE);

			query.append(_FINDER_COLUMN_CLASSROOMID_CLASSROOMID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(classroomId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CLASSROOMID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Counts all the studentses where courseId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @return the number of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCourseId(long courseId) throws SystemException {
		Object[] finderArgs = new Object[] { courseId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COURSEID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_STUDENTS_WHERE);

			query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COURSEID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Counts all the studentses where courseId = &#63; and groupId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @return the number of matching studentses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCourseIdGroupId(long courseId, long groupId)
		throws SystemException {
		Object[] finderArgs = new Object[] { courseId, groupId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COURSEIDGROUPID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_STUDENTS_WHERE);

			query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

			query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COURSEIDGROUPID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Filters by the user's permissions and counts all the studentses where courseId = &#63; and groupId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @return the number of matching studentses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public int filterCountByCourseIdGroupId(long courseId, long groupId)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByCourseIdGroupId(courseId, groupId);
		}

		StringBundler query = new StringBundler(3);

		query.append(_FILTER_SQL_COUNT_STUDENTS_WHERE);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				students.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			q.addScalar(COUNT_COLUMN_NAME,
				com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(courseId);

			qPos.add(groupId);

			Long count = (Long)q.uniqueResult();

			return count.intValue();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Counts all the studentses.
	 *
	 * @return the number of studentses
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Object[] finderArgs = new Object[0];

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_STUDENTS);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
					count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the students persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.org.xmlportletfactory.portal.school.model.students")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<students>> listenersList = new ArrayList<ModelListener<students>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<students>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(studentsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST);
	}

	@BeanReference(type = coursesPersistence.class)
	protected coursesPersistence coursesPersistence;
	@BeanReference(type = studentsPersistence.class)
	protected studentsPersistence studentsPersistence;
	@BeanReference(type = classroomsPersistence.class)
	protected classroomsPersistence classroomsPersistence;
	@BeanReference(type = commentsPersistence.class)
	protected commentsPersistence commentsPersistence;
	@BeanReference(type = courseSubjectsPersistence.class)
	protected courseSubjectsPersistence courseSubjectsPersistence;
	@BeanReference(type = subjectsPersistence.class)
	protected subjectsPersistence subjectsPersistence;
	@BeanReference(type = teachersPersistence.class)
	protected teachersPersistence teachersPersistence;
	@BeanReference(type = holidaysPersistence.class)
	protected holidaysPersistence holidaysPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_STUDENTS = "SELECT students FROM students students";
	private static final String _SQL_SELECT_STUDENTS_WHERE = "SELECT students FROM students students WHERE ";
	private static final String _SQL_COUNT_STUDENTS = "SELECT COUNT(students) FROM students students";
	private static final String _SQL_COUNT_STUDENTS_WHERE = "SELECT COUNT(students) FROM students students WHERE ";
	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "students.groupId = ?";
	private static final String _FINDER_COLUMN_USERID_USERID_2 = "students.userId = ?";
	private static final String _FINDER_COLUMN_USERIDGROUPID_USERID_2 = "students.userId = ? AND ";
	private static final String _FINDER_COLUMN_USERIDGROUPID_GROUPID_2 = "students.groupId = ?";
	private static final String _FINDER_COLUMN_CLASSROOMID_CLASSROOMID_2 = "students.classroomId = ?";
	private static final String _FINDER_COLUMN_COURSEID_COURSEID_2 = "students.courseId = ?";
	private static final String _FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2 = "students.courseId = ? AND ";
	private static final String _FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2 = "students.groupId = ?";
	private static final String _FILTER_SQL_SELECT_STUDENTS_WHERE = "SELECT DISTINCT {students.*} FROM coursesexample_students students WHERE ";
	private static final String _FILTER_SQL_SELECT_STUDENTS_NO_INLINE_DISTINCT_WHERE_1 =
		"SELECT {coursesexample_students.*} FROM (SELECT DISTINCT students.studentId FROM coursesexample_students students WHERE ";
	private static final String _FILTER_SQL_SELECT_STUDENTS_NO_INLINE_DISTINCT_WHERE_2 =
		") TEMP_TABLE INNER JOIN coursesexample_students ON TEMP_TABLE.studentId = coursesexample_students.studentId";
	private static final String _FILTER_SQL_COUNT_STUDENTS_WHERE = "SELECT COUNT(DISTINCT students.studentId) AS COUNT_VALUE FROM coursesexample_students students WHERE ";
	private static final String _FILTER_COLUMN_PK = "students.studentId";
	private static final String _FILTER_COLUMN_USERID = "students.userId";
	private static final String _FILTER_ENTITY_ALIAS = "students";
	private static final String _FILTER_ENTITY_TABLE = "coursesexample_students";
	private static final String _ORDER_BY_ENTITY_ALIAS = "students.";
	private static final String _ORDER_BY_ENTITY_TABLE = "coursesexample_students.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No students exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No students exists with the key {";
	private static Log _log = LogFactoryUtil.getLog(studentsPersistenceImpl.class);
}
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school.service.impl;

import java.util.List;

import org.xmlportletfactory.portal.school.model.students;
import org.xmlportletfactory.portal.school.service.base.studentsLocalServiceBaseImpl;
import org.xmlportletfactory.portal.school.service.persistence.studentsUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.dao.orm.QueryUtil;

import com.liferay.portlet.imagegallery.service.IGFolderLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;

/**
 * @author Jack A. Rider
 */
public class studentsLocalServiceImpl extends studentsLocalServiceBaseImpl {

	@SuppressWarnings("unchecked")
	public List findAllInUser(long userId)throws SystemException {
		List<students> list = (List<students>) studentsUtil.findByUserId(userId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUser(long userId, OrderByComparator orderByComparator) throws SystemException {
		List<students> list = (List<students>) studentsUtil.findByUserId(userId, QueryUtil.ALL_POS,QueryUtil.ALL_POS, orderByComparator);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInGroup(long groupId) throws SystemException {
		List<students> list = (List<students>) studentsUtil.findByGroupId(groupId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInGroup(long groupId, OrderByComparator orderByComparator) throws SystemException{
		List <students> list = (List<students>) studentsUtil.findByGroupId(groupId,QueryUtil.ALL_POS,QueryUtil.ALL_POS, orderByComparator);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUserAndGroup(long userId, long groupId) throws SystemException {
		List<students> list = (List<students>) studentsUtil.findByUserIdGroupId(userId, groupId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUserAndGroup(long userId, long groupId, OrderByComparator orderByComparator) throws SystemException {
		List<students> list = (List<students>) studentsUtil.findByUserIdGroupId(userId, groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, orderByComparator);
		return list;
	}


	@SuppressWarnings("unchecked")
	public List findAllInClassroomId(long classroomId)
		throws SystemException {

		List<students> list = (List<students>) studentsUtil.findByClassroomId(classroomId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllIncourseIdGroup(long courseId, long groupId)	throws SystemException {
		List<students> list = (List<students>) studentsUtil.findByCourseIdGroupId(courseId, groupId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllIncourseId(long courseId)	throws SystemException {
		List<students> list = (List<students>) studentsUtil.findByCourseId(courseId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllIncourseIdGroup(long courseId, long groupId, OrderByComparator orderByComparator) throws SystemException {
		List<students> list = (List<students>) studentsUtil.findByCourseIdGroupId(courseId, groupId, QueryUtil.ALL_POS,QueryUtil.ALL_POS, orderByComparator);
		return list;
	}


	public students addstudents (students validstudents) throws SystemException {
	    students fileobj = studentsUtil.create(CounterLocalServiceUtil.increment(students.class.getName()));

	    fileobj.setCompanyId(validstudents.getCompanyId());
	    fileobj.setGroupId(validstudents.getGroupId());
	    fileobj.setUserId(validstudents.getUserId());

	    fileobj.setCourseId(validstudents.getCourseId());
	    fileobj.setStudentName(validstudents.getStudentName());
	    fileobj.setClassroomId(validstudents.getClassroomId());
		fileobj.setStudentPhoto(validstudents.getStudentPhoto());
		fileobj.setFolderIGId(validstudents.getFolderIGId());

	    return studentsUtil.update(fileobj, false);
	}

	public void deletestudents (students fileobj) throws SystemException {
		//Remove imageFile dir
		Long igFolder = fileobj.getFolderIGId();
		if((igFolder!=null) && (igFolder!=0L)) {
			try {
				IGFolderLocalServiceUtil.deleteFolder(igFolder);
			} catch (PortalException ex) {
				throw new SystemException (ex.getMessage(),ex.getCause());
			}
		}
		studentsUtil.remove(fileobj);
	}

	public void remove(students fileobj) throws SystemException {
	    studentsUtil.remove(fileobj);
	}
}
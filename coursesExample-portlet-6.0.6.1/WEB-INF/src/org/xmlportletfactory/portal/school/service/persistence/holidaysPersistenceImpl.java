/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.security.permission.InlineSQLHelperUtil;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import org.xmlportletfactory.portal.school.NoSuchholidaysException;
import org.xmlportletfactory.portal.school.model.holidays;
import org.xmlportletfactory.portal.school.model.impl.holidaysImpl;
import org.xmlportletfactory.portal.school.model.impl.holidaysModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the holidays service.
 *
 * <p>
 * Never modify or reference this class directly. Always use {@link holidaysUtil} to access the holidays persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
 * </p>
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Jack A. Rider
 * @see holidaysPersistence
 * @see holidaysUtil
 * @generated
 */
public class holidaysPersistenceImpl extends BasePersistenceImpl<holidays>
	implements holidaysPersistence {
	public static final String FINDER_CLASS_NAME_ENTITY = holidaysImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
		".List";
	public static final FinderPath FINDER_PATH_FIND_BY_GROUPID = new FinderPath(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findByGroupId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countByGroupId", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_USERID = new FinderPath(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findByUserId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countByUserId", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_USERIDGROUPID = new FinderPath(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findByUserIdGroupId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_USERIDGROUPID = new FinderPath(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countByUserIdGroupId",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_COURSEID = new FinderPath(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findByCourseId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_COURSEID = new FinderPath(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countByCourseId", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_COURSEIDGROUPID = new FinderPath(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findByCourseIdGroupId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_COURSEIDGROUPID = new FinderPath(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countByCourseIdGroupId",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysModelImpl.FINDER_CACHE_ENABLED, FINDER_CLASS_NAME_LIST,
			"countAll", new String[0]);

	/**
	 * Caches the holidays in the entity cache if it is enabled.
	 *
	 * @param holidays the holidays to cache
	 */
	public void cacheResult(holidays holidays) {
		EntityCacheUtil.putResult(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysImpl.class, holidays.getPrimaryKey(), holidays);
	}

	/**
	 * Caches the holidayses in the entity cache if it is enabled.
	 *
	 * @param holidayses the holidayses to cache
	 */
	public void cacheResult(List<holidays> holidayses) {
		for (holidays holidays : holidayses) {
			if (EntityCacheUtil.getResult(
						holidaysModelImpl.ENTITY_CACHE_ENABLED,
						holidaysImpl.class, holidays.getPrimaryKey(), this) == null) {
				cacheResult(holidays);
			}
		}
	}

	/**
	 * Clears the cache for all holidayses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	public void clearCache() {
		CacheRegistryUtil.clear(holidaysImpl.class.getName());
		EntityCacheUtil.clearCache(holidaysImpl.class.getName());
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
	}

	/**
	 * Clears the cache for the holidays.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	public void clearCache(holidays holidays) {
		EntityCacheUtil.removeResult(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysImpl.class, holidays.getPrimaryKey());
	}

	/**
	 * Creates a new holidays with the primary key. Does not add the holidays to the database.
	 *
	 * @param holidayId the primary key for the new holidays
	 * @return the new holidays
	 */
	public holidays create(long holidayId) {
		holidays holidays = new holidaysImpl();

		holidays.setNew(true);
		holidays.setPrimaryKey(holidayId);

		return holidays;
	}

	/**
	 * Removes the holidays with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the holidays to remove
	 * @return the holidays that was removed
	 * @throws com.liferay.portal.NoSuchModelException if a holidays with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays remove(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return remove(((Long)primaryKey).longValue());
	}

	/**
	 * Removes the holidays with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param holidayId the primary key of the holidays to remove
	 * @return the holidays that was removed
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a holidays with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays remove(long holidayId)
		throws NoSuchholidaysException, SystemException {
		Session session = null;

		try {
			session = openSession();

			holidays holidays = (holidays)session.get(holidaysImpl.class,
					new Long(holidayId));

			if (holidays == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + holidayId);
				}

				throw new NoSuchholidaysException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					holidayId);
			}

			return remove(holidays);
		}
		catch (NoSuchholidaysException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected holidays removeImpl(holidays holidays) throws SystemException {
		holidays = toUnwrappedModel(holidays);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, holidays);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

		EntityCacheUtil.removeResult(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysImpl.class, holidays.getPrimaryKey());

		return holidays;
	}

	public holidays updateImpl(
		org.xmlportletfactory.portal.school.model.holidays holidays,
		boolean merge) throws SystemException {
		holidays = toUnwrappedModel(holidays);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, holidays, merge);

			holidays.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

		EntityCacheUtil.putResult(holidaysModelImpl.ENTITY_CACHE_ENABLED,
			holidaysImpl.class, holidays.getPrimaryKey(), holidays);

		return holidays;
	}

	protected holidays toUnwrappedModel(holidays holidays) {
		if (holidays instanceof holidaysImpl) {
			return holidays;
		}

		holidaysImpl holidaysImpl = new holidaysImpl();

		holidaysImpl.setNew(holidays.isNew());
		holidaysImpl.setPrimaryKey(holidays.getPrimaryKey());

		holidaysImpl.setHolidayId(holidays.getHolidayId());
		holidaysImpl.setCompanyId(holidays.getCompanyId());
		holidaysImpl.setGroupId(holidays.getGroupId());
		holidaysImpl.setUserId(holidays.getUserId());
		holidaysImpl.setCourseId(holidays.getCourseId());
		holidaysImpl.setHolidayName(holidays.getHolidayName());
		holidaysImpl.setHolidayDate(holidays.getHolidayDate());

		return holidaysImpl;
	}

	/**
	 * Finds the holidays with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the holidays to find
	 * @return the holidays
	 * @throws com.liferay.portal.NoSuchModelException if a holidays with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Finds the holidays with the primary key or throws a {@link org.xmlportletfactory.portal.school.NoSuchholidaysException} if it could not be found.
	 *
	 * @param holidayId the primary key of the holidays to find
	 * @return the holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a holidays with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays findByPrimaryKey(long holidayId)
		throws NoSuchholidaysException, SystemException {
		holidays holidays = fetchByPrimaryKey(holidayId);

		if (holidays == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + holidayId);
			}

			throw new NoSuchholidaysException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				holidayId);
		}

		return holidays;
	}

	/**
	 * Finds the holidays with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the holidays to find
	 * @return the holidays, or <code>null</code> if a holidays with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Finds the holidays with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param holidayId the primary key of the holidays to find
	 * @return the holidays, or <code>null</code> if a holidays with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays fetchByPrimaryKey(long holidayId) throws SystemException {
		holidays holidays = (holidays)EntityCacheUtil.getResult(holidaysModelImpl.ENTITY_CACHE_ENABLED,
				holidaysImpl.class, holidayId, this);

		if (holidays == null) {
			Session session = null;

			try {
				session = openSession();

				holidays = (holidays)session.get(holidaysImpl.class,
						new Long(holidayId));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (holidays != null) {
					cacheResult(holidays);
				}

				closeSession(session);
			}
		}

		return holidays;
	}

	/**
	 * Finds all the holidayses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByGroupId(long groupId) throws SystemException {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the holidayses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @return the range of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByGroupId(long groupId, int start, int end)
		throws SystemException {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the holidayses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByGroupId(long groupId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				groupId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<holidays> list = (List<holidays>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_GROUPID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_HOLIDAYS_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				list = (List<holidays>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_GROUPID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_GROUPID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first holidays in the ordered set where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a matching holidays could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays findByGroupId_First(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		List<holidays> list = findByGroupId(groupId, 0, 1, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchholidaysException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last holidays in the ordered set where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a matching holidays could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays findByGroupId_Last(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		int count = countByGroupId(groupId);

		List<holidays> list = findByGroupId(groupId, count - 1, count,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchholidaysException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the holidayses before and after the current holidays in the ordered set where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param holidayId the primary key of the current holidays
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a holidays with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays[] findByGroupId_PrevAndNext(long holidayId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		holidays holidays = findByPrimaryKey(holidayId);

		Session session = null;

		try {
			session = openSession();

			holidays[] array = new holidaysImpl[3];

			array[0] = getByGroupId_PrevAndNext(session, holidays, groupId,
					orderByComparator, true);

			array[1] = holidays;

			array[2] = getByGroupId_PrevAndNext(session, holidays, groupId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected holidays getByGroupId_PrevAndNext(Session session,
		holidays holidays, long groupId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_HOLIDAYS_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(holidays);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<holidays> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Filters by the user's permissions and finds all the holidayses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the matching holidayses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> filterFindByGroupId(long groupId)
		throws SystemException {
		return filterFindByGroupId(groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Filters by the user's permissions and finds a range of all the holidayses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @return the range of matching holidayses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> filterFindByGroupId(long groupId, int start, int end)
		throws SystemException {
		return filterFindByGroupId(groupId, start, end, null);
	}

	/**
	 * Filters by the user's permissions and finds an ordered range of all the holidayses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching holidayses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> filterFindByGroupId(long groupId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByGroupId(groupId, start, end, orderByComparator);
		}

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(3 +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(2);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_HOLIDAYS_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_HOLIDAYS_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_HOLIDAYS_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_TABLE,
					orderByComparator);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				holidays.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				q.addEntity(_FILTER_ENTITY_ALIAS, holidaysImpl.class);
			}
			else {
				q.addEntity(_FILTER_ENTITY_TABLE, holidaysImpl.class);
			}

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(groupId);

			return (List<holidays>)QueryUtil.list(q, getDialect(), start, end);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Finds all the holidayses where userId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @return the matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByUserId(long userId) throws SystemException {
		return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the holidayses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @return the range of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByUserId(long userId, int start, int end)
		throws SystemException {
		return findByUserId(userId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the holidayses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByUserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				userId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<holidays> list = (List<holidays>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_USERID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_HOLIDAYS_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				list = (List<holidays>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_USERID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_USERID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first holidays in the ordered set where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a matching holidays could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays findByUserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		List<holidays> list = findByUserId(userId, 0, 1, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchholidaysException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last holidays in the ordered set where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a matching holidays could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays findByUserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		int count = countByUserId(userId);

		List<holidays> list = findByUserId(userId, count - 1, count,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchholidaysException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the holidayses before and after the current holidays in the ordered set where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param holidayId the primary key of the current holidays
	 * @param userId the user id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a holidays with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays[] findByUserId_PrevAndNext(long holidayId, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		holidays holidays = findByPrimaryKey(holidayId);

		Session session = null;

		try {
			session = openSession();

			holidays[] array = new holidaysImpl[3];

			array[0] = getByUserId_PrevAndNext(session, holidays, userId,
					orderByComparator, true);

			array[1] = holidays;

			array[2] = getByUserId_PrevAndNext(session, holidays, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected holidays getByUserId_PrevAndNext(Session session,
		holidays holidays, long userId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_HOLIDAYS_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(holidays);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<holidays> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Finds all the holidayses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		return findByUserIdGroupId(userId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the holidayses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @return the range of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByUserIdGroupId(long userId, long groupId,
		int start, int end) throws SystemException {
		return findByUserIdGroupId(userId, groupId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the holidayses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByUserIdGroupId(long userId, long groupId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		Object[] finderArgs = new Object[] {
				userId, groupId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<holidays> list = (List<holidays>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_USERIDGROUPID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_HOLIDAYS_WHERE);

			query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

			query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(groupId);

				list = (List<holidays>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_USERIDGROUPID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_USERIDGROUPID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first holidays in the ordered set where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a matching holidays could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays findByUserIdGroupId_First(long userId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		List<holidays> list = findByUserIdGroupId(userId, groupId, 0, 1,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchholidaysException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last holidays in the ordered set where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a matching holidays could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays findByUserIdGroupId_Last(long userId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		int count = countByUserIdGroupId(userId, groupId);

		List<holidays> list = findByUserIdGroupId(userId, groupId, count - 1,
				count, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchholidaysException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the holidayses before and after the current holidays in the ordered set where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param holidayId the primary key of the current holidays
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a holidays with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays[] findByUserIdGroupId_PrevAndNext(long holidayId,
		long userId, long groupId, OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		holidays holidays = findByPrimaryKey(holidayId);

		Session session = null;

		try {
			session = openSession();

			holidays[] array = new holidaysImpl[3];

			array[0] = getByUserIdGroupId_PrevAndNext(session, holidays,
					userId, groupId, orderByComparator, true);

			array[1] = holidays;

			array[2] = getByUserIdGroupId_PrevAndNext(session, holidays,
					userId, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected holidays getByUserIdGroupId_PrevAndNext(Session session,
		holidays holidays, long userId, long groupId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_HOLIDAYS_WHERE);

		query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

		query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(holidays);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<holidays> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Filters by the user's permissions and finds all the holidayses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the matching holidayses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> filterFindByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		return filterFindByUserIdGroupId(userId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Filters by the user's permissions and finds a range of all the holidayses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @return the range of matching holidayses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> filterFindByUserIdGroupId(long userId, long groupId,
		int start, int end) throws SystemException {
		return filterFindByUserIdGroupId(userId, groupId, start, end, null);
	}

	/**
	 * Filters by the user's permissions and finds an ordered range of all the holidayses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching holidayses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> filterFindByUserIdGroupId(long userId, long groupId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByUserIdGroupId(userId, groupId, start, end,
				orderByComparator);
		}

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_HOLIDAYS_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_HOLIDAYS_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

		query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_HOLIDAYS_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_TABLE,
					orderByComparator);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				holidays.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				q.addEntity(_FILTER_ENTITY_ALIAS, holidaysImpl.class);
			}
			else {
				q.addEntity(_FILTER_ENTITY_TABLE, holidaysImpl.class);
			}

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(userId);

			qPos.add(groupId);

			return (List<holidays>)QueryUtil.list(q, getDialect(), start, end);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Finds all the holidayses where courseId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @return the matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByCourseId(long courseId)
		throws SystemException {
		return findByCourseId(courseId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Finds a range of all the holidayses where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @return the range of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByCourseId(long courseId, int start, int end)
		throws SystemException {
		return findByCourseId(courseId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the holidayses where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByCourseId(long courseId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				courseId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<holidays> list = (List<holidays>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_COURSEID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_HOLIDAYS_WHERE);

			query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				list = (List<holidays>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_COURSEID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_COURSEID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first holidays in the ordered set where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a matching holidays could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays findByCourseId_First(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		List<holidays> list = findByCourseId(courseId, 0, 1, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("courseId=");
			msg.append(courseId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchholidaysException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last holidays in the ordered set where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a matching holidays could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays findByCourseId_Last(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		int count = countByCourseId(courseId);

		List<holidays> list = findByCourseId(courseId, count - 1, count,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("courseId=");
			msg.append(courseId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchholidaysException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the holidayses before and after the current holidays in the ordered set where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param holidayId the primary key of the current holidays
	 * @param courseId the course id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a holidays with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays[] findByCourseId_PrevAndNext(long holidayId, long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		holidays holidays = findByPrimaryKey(holidayId);

		Session session = null;

		try {
			session = openSession();

			holidays[] array = new holidaysImpl[3];

			array[0] = getByCourseId_PrevAndNext(session, holidays, courseId,
					orderByComparator, true);

			array[1] = holidays;

			array[2] = getByCourseId_PrevAndNext(session, holidays, courseId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected holidays getByCourseId_PrevAndNext(Session session,
		holidays holidays, long courseId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_HOLIDAYS_WHERE);

		query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(courseId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(holidays);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<holidays> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Finds all the holidayses where courseId = &#63; and groupId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @return the matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByCourseIdGroupId(long courseId, long groupId)
		throws SystemException {
		return findByCourseIdGroupId(courseId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the holidayses where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @return the range of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByCourseIdGroupId(long courseId, long groupId,
		int start, int end) throws SystemException {
		return findByCourseIdGroupId(courseId, groupId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the holidayses where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findByCourseIdGroupId(long courseId, long groupId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		Object[] finderArgs = new Object[] {
				courseId, groupId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<holidays> list = (List<holidays>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_COURSEIDGROUPID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_HOLIDAYS_WHERE);

			query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

			query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				qPos.add(groupId);

				list = (List<holidays>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_COURSEIDGROUPID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_COURSEIDGROUPID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first holidays in the ordered set where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a matching holidays could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays findByCourseIdGroupId_First(long courseId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		List<holidays> list = findByCourseIdGroupId(courseId, groupId, 0, 1,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("courseId=");
			msg.append(courseId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchholidaysException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last holidays in the ordered set where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a matching holidays could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays findByCourseIdGroupId_Last(long courseId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		int count = countByCourseIdGroupId(courseId, groupId);

		List<holidays> list = findByCourseIdGroupId(courseId, groupId,
				count - 1, count, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("courseId=");
			msg.append(courseId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchholidaysException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the holidayses before and after the current holidays in the ordered set where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param holidayId the primary key of the current holidays
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next holidays
	 * @throws org.xmlportletfactory.portal.school.NoSuchholidaysException if a holidays with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public holidays[] findByCourseIdGroupId_PrevAndNext(long holidayId,
		long courseId, long groupId, OrderByComparator orderByComparator)
		throws NoSuchholidaysException, SystemException {
		holidays holidays = findByPrimaryKey(holidayId);

		Session session = null;

		try {
			session = openSession();

			holidays[] array = new holidaysImpl[3];

			array[0] = getByCourseIdGroupId_PrevAndNext(session, holidays,
					courseId, groupId, orderByComparator, true);

			array[1] = holidays;

			array[2] = getByCourseIdGroupId_PrevAndNext(session, holidays,
					courseId, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected holidays getByCourseIdGroupId_PrevAndNext(Session session,
		holidays holidays, long courseId, long groupId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_HOLIDAYS_WHERE);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(courseId);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(holidays);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<holidays> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Filters by the user's permissions and finds all the holidayses where courseId = &#63; and groupId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @return the matching holidayses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> filterFindByCourseIdGroupId(long courseId,
		long groupId) throws SystemException {
		return filterFindByCourseIdGroupId(courseId, groupId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Filters by the user's permissions and finds a range of all the holidayses where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @return the range of matching holidayses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> filterFindByCourseIdGroupId(long courseId,
		long groupId, int start, int end) throws SystemException {
		return filterFindByCourseIdGroupId(courseId, groupId, start, end, null);
	}

	/**
	 * Filters by the user's permissions and finds an ordered range of all the holidayses where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching holidayses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> filterFindByCourseIdGroupId(long courseId,
		long groupId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByCourseIdGroupId(courseId, groupId, start, end,
				orderByComparator);
		}

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_HOLIDAYS_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_HOLIDAYS_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_HOLIDAYS_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_TABLE,
					orderByComparator);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				holidays.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				q.addEntity(_FILTER_ENTITY_ALIAS, holidaysImpl.class);
			}
			else {
				q.addEntity(_FILTER_ENTITY_TABLE, holidaysImpl.class);
			}

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(courseId);

			qPos.add(groupId);

			return (List<holidays>)QueryUtil.list(q, getDialect(), start, end);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Finds all the holidayses.
	 *
	 * @return the holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the holidayses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @return the range of holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Finds an ordered range of all the holidayses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of holidayses to return
	 * @param end the upper bound of the range of holidayses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public List<holidays> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<holidays> list = (List<holidays>)FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_HOLIDAYS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_HOLIDAYS;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<holidays>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<holidays>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_ALL,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs,
						list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the holidayses where groupId = &#63; from the database.
	 *
	 * @param groupId the group id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByGroupId(long groupId) throws SystemException {
		for (holidays holidays : findByGroupId(groupId)) {
			remove(holidays);
		}
	}

	/**
	 * Removes all the holidayses where userId = &#63; from the database.
	 *
	 * @param userId the user id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUserId(long userId) throws SystemException {
		for (holidays holidays : findByUserId(userId)) {
			remove(holidays);
		}
	}

	/**
	 * Removes all the holidayses where userId = &#63; and groupId = &#63; from the database.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		for (holidays holidays : findByUserIdGroupId(userId, groupId)) {
			remove(holidays);
		}
	}

	/**
	 * Removes all the holidayses where courseId = &#63; from the database.
	 *
	 * @param courseId the course id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCourseId(long courseId) throws SystemException {
		for (holidays holidays : findByCourseId(courseId)) {
			remove(holidays);
		}
	}

	/**
	 * Removes all the holidayses where courseId = &#63; and groupId = &#63; from the database.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCourseIdGroupId(long courseId, long groupId)
		throws SystemException {
		for (holidays holidays : findByCourseIdGroupId(courseId, groupId)) {
			remove(holidays);
		}
	}

	/**
	 * Removes all the holidayses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (holidays holidays : findAll()) {
			remove(holidays);
		}
	}

	/**
	 * Counts all the holidayses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the number of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByGroupId(long groupId) throws SystemException {
		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_GROUPID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_HOLIDAYS_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_GROUPID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Filters by the user's permissions and counts all the holidayses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the number of matching holidayses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public int filterCountByGroupId(long groupId) throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByGroupId(groupId);
		}

		StringBundler query = new StringBundler(2);

		query.append(_FILTER_SQL_COUNT_HOLIDAYS_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				holidays.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			q.addScalar(COUNT_COLUMN_NAME,
				com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(groupId);

			Long count = (Long)q.uniqueResult();

			return count.intValue();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Counts all the holidayses where userId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @return the number of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUserId(long userId) throws SystemException {
		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_HOLIDAYS_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Counts all the holidayses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the number of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		Object[] finderArgs = new Object[] { userId, groupId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERIDGROUPID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_HOLIDAYS_WHERE);

			query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

			query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERIDGROUPID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Filters by the user's permissions and counts all the holidayses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the number of matching holidayses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public int filterCountByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByUserIdGroupId(userId, groupId);
		}

		StringBundler query = new StringBundler(3);

		query.append(_FILTER_SQL_COUNT_HOLIDAYS_WHERE);

		query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

		query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				holidays.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			q.addScalar(COUNT_COLUMN_NAME,
				com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(userId);

			qPos.add(groupId);

			Long count = (Long)q.uniqueResult();

			return count.intValue();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Counts all the holidayses where courseId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @return the number of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCourseId(long courseId) throws SystemException {
		Object[] finderArgs = new Object[] { courseId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COURSEID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_HOLIDAYS_WHERE);

			query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COURSEID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Counts all the holidayses where courseId = &#63; and groupId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @return the number of matching holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCourseIdGroupId(long courseId, long groupId)
		throws SystemException {
		Object[] finderArgs = new Object[] { courseId, groupId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COURSEIDGROUPID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_HOLIDAYS_WHERE);

			query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

			query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COURSEIDGROUPID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Filters by the user's permissions and counts all the holidayses where courseId = &#63; and groupId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @return the number of matching holidayses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public int filterCountByCourseIdGroupId(long courseId, long groupId)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByCourseIdGroupId(courseId, groupId);
		}

		StringBundler query = new StringBundler(3);

		query.append(_FILTER_SQL_COUNT_HOLIDAYS_WHERE);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				holidays.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			q.addScalar(COUNT_COLUMN_NAME,
				com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(courseId);

			qPos.add(groupId);

			Long count = (Long)q.uniqueResult();

			return count.intValue();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Counts all the holidayses.
	 *
	 * @return the number of holidayses
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Object[] finderArgs = new Object[0];

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_HOLIDAYS);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
					count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the holidays persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.org.xmlportletfactory.portal.school.model.holidays")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<holidays>> listenersList = new ArrayList<ModelListener<holidays>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<holidays>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(holidaysImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST);
	}

	@BeanReference(type = coursesPersistence.class)
	protected coursesPersistence coursesPersistence;
	@BeanReference(type = studentsPersistence.class)
	protected studentsPersistence studentsPersistence;
	@BeanReference(type = classroomsPersistence.class)
	protected classroomsPersistence classroomsPersistence;
	@BeanReference(type = commentsPersistence.class)
	protected commentsPersistence commentsPersistence;
	@BeanReference(type = courseSubjectsPersistence.class)
	protected courseSubjectsPersistence courseSubjectsPersistence;
	@BeanReference(type = subjectsPersistence.class)
	protected subjectsPersistence subjectsPersistence;
	@BeanReference(type = teachersPersistence.class)
	protected teachersPersistence teachersPersistence;
	@BeanReference(type = holidaysPersistence.class)
	protected holidaysPersistence holidaysPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_HOLIDAYS = "SELECT holidays FROM holidays holidays";
	private static final String _SQL_SELECT_HOLIDAYS_WHERE = "SELECT holidays FROM holidays holidays WHERE ";
	private static final String _SQL_COUNT_HOLIDAYS = "SELECT COUNT(holidays) FROM holidays holidays";
	private static final String _SQL_COUNT_HOLIDAYS_WHERE = "SELECT COUNT(holidays) FROM holidays holidays WHERE ";
	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "holidays.groupId = ?";
	private static final String _FINDER_COLUMN_USERID_USERID_2 = "holidays.userId = ?";
	private static final String _FINDER_COLUMN_USERIDGROUPID_USERID_2 = "holidays.userId = ? AND ";
	private static final String _FINDER_COLUMN_USERIDGROUPID_GROUPID_2 = "holidays.groupId = ?";
	private static final String _FINDER_COLUMN_COURSEID_COURSEID_2 = "holidays.courseId = ?";
	private static final String _FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2 = "holidays.courseId = ? AND ";
	private static final String _FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2 = "holidays.groupId = ?";
	private static final String _FILTER_SQL_SELECT_HOLIDAYS_WHERE = "SELECT DISTINCT {holidays.*} FROM coursesexample_holidays holidays WHERE ";
	private static final String _FILTER_SQL_SELECT_HOLIDAYS_NO_INLINE_DISTINCT_WHERE_1 =
		"SELECT {coursesexample_holidays.*} FROM (SELECT DISTINCT holidays.holidayId FROM coursesexample_holidays holidays WHERE ";
	private static final String _FILTER_SQL_SELECT_HOLIDAYS_NO_INLINE_DISTINCT_WHERE_2 =
		") TEMP_TABLE INNER JOIN coursesexample_holidays ON TEMP_TABLE.holidayId = coursesexample_holidays.holidayId";
	private static final String _FILTER_SQL_COUNT_HOLIDAYS_WHERE = "SELECT COUNT(DISTINCT holidays.holidayId) AS COUNT_VALUE FROM coursesexample_holidays holidays WHERE ";
	private static final String _FILTER_COLUMN_PK = "holidays.holidayId";
	private static final String _FILTER_COLUMN_USERID = "holidays.userId";
	private static final String _FILTER_ENTITY_ALIAS = "holidays";
	private static final String _FILTER_ENTITY_TABLE = "coursesexample_holidays";
	private static final String _ORDER_BY_ENTITY_ALIAS = "holidays.";
	private static final String _ORDER_BY_ENTITY_TABLE = "coursesexample_holidays.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No holidays exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No holidays exists with the key {";
	private static Log _log = LogFactoryUtil.getLog(holidaysPersistenceImpl.class);
}
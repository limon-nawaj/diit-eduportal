/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.xmlportletfactory.portal.school.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.annotation.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.security.permission.InlineSQLHelperUtil;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException;
import org.xmlportletfactory.portal.school.model.courseSubjects;
import org.xmlportletfactory.portal.school.model.impl.courseSubjectsImpl;
import org.xmlportletfactory.portal.school.model.impl.courseSubjectsModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the course subjects service.
 *
 * <p>
 * Never modify or reference this class directly. Always use {@link courseSubjectsUtil} to access the course subjects persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
 * </p>
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Jack A. Rider
 * @see courseSubjectsPersistence
 * @see courseSubjectsUtil
 * @generated
 */
public class courseSubjectsPersistenceImpl extends BasePersistenceImpl<courseSubjects>
	implements courseSubjectsPersistence {
	public static final String FINDER_CLASS_NAME_ENTITY = courseSubjectsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST = FINDER_CLASS_NAME_ENTITY +
		".List";
	public static final FinderPath FINDER_PATH_FIND_BY_GROUPID = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "findByGroupId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "countByGroupId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_USERID = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "findByUserId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "countByUserId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_USERIDGROUPID = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "findByUserIdGroupId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_USERIDGROUPID = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "countByUserIdGroupId",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_SUBJECTID = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "findBySubjectId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_SUBJECTID = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "countBySubjectId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_TEACHERID = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "findByTeacherId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_TEACHERID = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "countByTeacherId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_COURSEID = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "findByCourseId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_COURSEID = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "countByCourseId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_BY_COURSEIDGROUPID = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "findByCourseIdGroupId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_COUNT_BY_COURSEIDGROUPID = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "countByCourseIdGroupId",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_FIND_ALL = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsModelImpl.FINDER_CACHE_ENABLED,
			FINDER_CLASS_NAME_LIST, "countAll", new String[0]);

	/**
	 * Caches the course subjects in the entity cache if it is enabled.
	 *
	 * @param courseSubjects the course subjects to cache
	 */
	public void cacheResult(courseSubjects courseSubjects) {
		EntityCacheUtil.putResult(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsImpl.class, courseSubjects.getPrimaryKey(),
			courseSubjects);
	}

	/**
	 * Caches the course subjectses in the entity cache if it is enabled.
	 *
	 * @param courseSubjectses the course subjectses to cache
	 */
	public void cacheResult(List<courseSubjects> courseSubjectses) {
		for (courseSubjects courseSubjects : courseSubjectses) {
			if (EntityCacheUtil.getResult(
						courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
						courseSubjectsImpl.class,
						courseSubjects.getPrimaryKey(), this) == null) {
				cacheResult(courseSubjects);
			}
		}
	}

	/**
	 * Clears the cache for all course subjectses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	public void clearCache() {
		CacheRegistryUtil.clear(courseSubjectsImpl.class.getName());
		EntityCacheUtil.clearCache(courseSubjectsImpl.class.getName());
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);
	}

	/**
	 * Clears the cache for the course subjects.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	public void clearCache(courseSubjects courseSubjects) {
		EntityCacheUtil.removeResult(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsImpl.class, courseSubjects.getPrimaryKey());
	}

	/**
	 * Creates a new course subjects with the primary key. Does not add the course subjects to the database.
	 *
	 * @param courseSubjectId the primary key for the new course subjects
	 * @return the new course subjects
	 */
	public courseSubjects create(long courseSubjectId) {
		courseSubjects courseSubjects = new courseSubjectsImpl();

		courseSubjects.setNew(true);
		courseSubjects.setPrimaryKey(courseSubjectId);

		return courseSubjects;
	}

	/**
	 * Removes the course subjects with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the course subjects to remove
	 * @return the course subjects that was removed
	 * @throws com.liferay.portal.NoSuchModelException if a course subjects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects remove(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return remove(((Long)primaryKey).longValue());
	}

	/**
	 * Removes the course subjects with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param courseSubjectId the primary key of the course subjects to remove
	 * @return the course subjects that was removed
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects remove(long courseSubjectId)
		throws NoSuchcourseSubjectsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			courseSubjects courseSubjects = (courseSubjects)session.get(courseSubjectsImpl.class,
					new Long(courseSubjectId));

			if (courseSubjects == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
						courseSubjectId);
				}

				throw new NoSuchcourseSubjectsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					courseSubjectId);
			}

			return remove(courseSubjects);
		}
		catch (NoSuchcourseSubjectsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected courseSubjects removeImpl(courseSubjects courseSubjects)
		throws SystemException {
		courseSubjects = toUnwrappedModel(courseSubjects);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, courseSubjects);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

		EntityCacheUtil.removeResult(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsImpl.class, courseSubjects.getPrimaryKey());

		return courseSubjects;
	}

	public courseSubjects updateImpl(
		org.xmlportletfactory.portal.school.model.courseSubjects courseSubjects,
		boolean merge) throws SystemException {
		courseSubjects = toUnwrappedModel(courseSubjects);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, courseSubjects, merge);

			courseSubjects.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST);

		EntityCacheUtil.putResult(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
			courseSubjectsImpl.class, courseSubjects.getPrimaryKey(),
			courseSubjects);

		return courseSubjects;
	}

	protected courseSubjects toUnwrappedModel(courseSubjects courseSubjects) {
		if (courseSubjects instanceof courseSubjectsImpl) {
			return courseSubjects;
		}

		courseSubjectsImpl courseSubjectsImpl = new courseSubjectsImpl();

		courseSubjectsImpl.setNew(courseSubjects.isNew());
		courseSubjectsImpl.setPrimaryKey(courseSubjects.getPrimaryKey());

		courseSubjectsImpl.setCourseSubjectId(courseSubjects.getCourseSubjectId());
		courseSubjectsImpl.setCompanyId(courseSubjects.getCompanyId());
		courseSubjectsImpl.setGroupId(courseSubjects.getGroupId());
		courseSubjectsImpl.setUserId(courseSubjects.getUserId());
		courseSubjectsImpl.setCourseId(courseSubjects.getCourseId());
		courseSubjectsImpl.setSubjectId(courseSubjects.getSubjectId());
		courseSubjectsImpl.setTeacherId(courseSubjects.getTeacherId());

		return courseSubjectsImpl;
	}

	/**
	 * Finds the course subjects with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the course subjects to find
	 * @return the course subjects
	 * @throws com.liferay.portal.NoSuchModelException if a course subjects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Finds the course subjects with the primary key or throws a {@link org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException} if it could not be found.
	 *
	 * @param courseSubjectId the primary key of the course subjects to find
	 * @return the course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findByPrimaryKey(long courseSubjectId)
		throws NoSuchcourseSubjectsException, SystemException {
		courseSubjects courseSubjects = fetchByPrimaryKey(courseSubjectId);

		if (courseSubjects == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + courseSubjectId);
			}

			throw new NoSuchcourseSubjectsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				courseSubjectId);
		}

		return courseSubjects;
	}

	/**
	 * Finds the course subjects with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the course subjects to find
	 * @return the course subjects, or <code>null</code> if a course subjects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Finds the course subjects with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param courseSubjectId the primary key of the course subjects to find
	 * @return the course subjects, or <code>null</code> if a course subjects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects fetchByPrimaryKey(long courseSubjectId)
		throws SystemException {
		courseSubjects courseSubjects = (courseSubjects)EntityCacheUtil.getResult(courseSubjectsModelImpl.ENTITY_CACHE_ENABLED,
				courseSubjectsImpl.class, courseSubjectId, this);

		if (courseSubjects == null) {
			Session session = null;

			try {
				session = openSession();

				courseSubjects = (courseSubjects)session.get(courseSubjectsImpl.class,
						new Long(courseSubjectId));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (courseSubjects != null) {
					cacheResult(courseSubjects);
				}

				closeSession(session);
			}
		}

		return courseSubjects;
	}

	/**
	 * Finds all the course subjectses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByGroupId(long groupId)
		throws SystemException {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the course subjectses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @return the range of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByGroupId(long groupId, int start, int end)
		throws SystemException {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the course subjectses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByGroupId(long groupId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				groupId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<courseSubjects> list = (List<courseSubjects>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_GROUPID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_COURSESUBJECTS_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				list = (List<courseSubjects>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_GROUPID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_GROUPID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first course subjects in the ordered set where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findByGroupId_First(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		List<courseSubjects> list = findByGroupId(groupId, 0, 1,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchcourseSubjectsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last course subjects in the ordered set where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findByGroupId_Last(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		int count = countByGroupId(groupId);

		List<courseSubjects> list = findByGroupId(groupId, count - 1, count,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchcourseSubjectsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the course subjectses before and after the current course subjects in the ordered set where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseSubjectId the primary key of the current course subjects
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects[] findByGroupId_PrevAndNext(long courseSubjectId,
		long groupId, OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		courseSubjects courseSubjects = findByPrimaryKey(courseSubjectId);

		Session session = null;

		try {
			session = openSession();

			courseSubjects[] array = new courseSubjectsImpl[3];

			array[0] = getByGroupId_PrevAndNext(session, courseSubjects,
					groupId, orderByComparator, true);

			array[1] = courseSubjects;

			array[2] = getByGroupId_PrevAndNext(session, courseSubjects,
					groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected courseSubjects getByGroupId_PrevAndNext(Session session,
		courseSubjects courseSubjects, long groupId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSESUBJECTS_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(courseSubjects);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<courseSubjects> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Filters by the user's permissions and finds all the course subjectses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the matching course subjectses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> filterFindByGroupId(long groupId)
		throws SystemException {
		return filterFindByGroupId(groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Filters by the user's permissions and finds a range of all the course subjectses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @return the range of matching course subjectses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> filterFindByGroupId(long groupId, int start,
		int end) throws SystemException {
		return filterFindByGroupId(groupId, start, end, null);
	}

	/**
	 * Filters by the user's permissions and finds an ordered range of all the course subjectses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching course subjectses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> filterFindByGroupId(long groupId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByGroupId(groupId, start, end, orderByComparator);
		}

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(3 +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(2);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_COURSESUBJECTS_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_COURSESUBJECTS_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_COURSESUBJECTS_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_TABLE,
					orderByComparator);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				courseSubjects.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				q.addEntity(_FILTER_ENTITY_ALIAS, courseSubjectsImpl.class);
			}
			else {
				q.addEntity(_FILTER_ENTITY_TABLE, courseSubjectsImpl.class);
			}

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(groupId);

			return (List<courseSubjects>)QueryUtil.list(q, getDialect(), start,
				end);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Finds all the course subjectses where userId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @return the matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByUserId(long userId)
		throws SystemException {
		return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the course subjectses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @return the range of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByUserId(long userId, int start, int end)
		throws SystemException {
		return findByUserId(userId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the course subjectses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByUserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				userId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<courseSubjects> list = (List<courseSubjects>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_USERID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_COURSESUBJECTS_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				list = (List<courseSubjects>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_USERID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_USERID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first course subjects in the ordered set where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findByUserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		List<courseSubjects> list = findByUserId(userId, 0, 1, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchcourseSubjectsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last course subjects in the ordered set where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findByUserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		int count = countByUserId(userId);

		List<courseSubjects> list = findByUserId(userId, count - 1, count,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchcourseSubjectsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the course subjectses before and after the current course subjects in the ordered set where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseSubjectId the primary key of the current course subjects
	 * @param userId the user id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects[] findByUserId_PrevAndNext(long courseSubjectId,
		long userId, OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		courseSubjects courseSubjects = findByPrimaryKey(courseSubjectId);

		Session session = null;

		try {
			session = openSession();

			courseSubjects[] array = new courseSubjectsImpl[3];

			array[0] = getByUserId_PrevAndNext(session, courseSubjects, userId,
					orderByComparator, true);

			array[1] = courseSubjects;

			array[2] = getByUserId_PrevAndNext(session, courseSubjects, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected courseSubjects getByUserId_PrevAndNext(Session session,
		courseSubjects courseSubjects, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSESUBJECTS_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(courseSubjects);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<courseSubjects> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Finds all the course subjectses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		return findByUserIdGroupId(userId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the course subjectses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @return the range of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByUserIdGroupId(long userId, long groupId,
		int start, int end) throws SystemException {
		return findByUserIdGroupId(userId, groupId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the course subjectses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByUserIdGroupId(long userId, long groupId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		Object[] finderArgs = new Object[] {
				userId, groupId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<courseSubjects> list = (List<courseSubjects>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_USERIDGROUPID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COURSESUBJECTS_WHERE);

			query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

			query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(groupId);

				list = (List<courseSubjects>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_USERIDGROUPID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_USERIDGROUPID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first course subjects in the ordered set where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findByUserIdGroupId_First(long userId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		List<courseSubjects> list = findByUserIdGroupId(userId, groupId, 0, 1,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchcourseSubjectsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last course subjects in the ordered set where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findByUserIdGroupId_Last(long userId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		int count = countByUserIdGroupId(userId, groupId);

		List<courseSubjects> list = findByUserIdGroupId(userId, groupId,
				count - 1, count, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchcourseSubjectsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the course subjectses before and after the current course subjects in the ordered set where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseSubjectId the primary key of the current course subjects
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects[] findByUserIdGroupId_PrevAndNext(
		long courseSubjectId, long userId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		courseSubjects courseSubjects = findByPrimaryKey(courseSubjectId);

		Session session = null;

		try {
			session = openSession();

			courseSubjects[] array = new courseSubjectsImpl[3];

			array[0] = getByUserIdGroupId_PrevAndNext(session, courseSubjects,
					userId, groupId, orderByComparator, true);

			array[1] = courseSubjects;

			array[2] = getByUserIdGroupId_PrevAndNext(session, courseSubjects,
					userId, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected courseSubjects getByUserIdGroupId_PrevAndNext(Session session,
		courseSubjects courseSubjects, long userId, long groupId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSESUBJECTS_WHERE);

		query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

		query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(courseSubjects);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<courseSubjects> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Filters by the user's permissions and finds all the course subjectses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the matching course subjectses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> filterFindByUserIdGroupId(long userId,
		long groupId) throws SystemException {
		return filterFindByUserIdGroupId(userId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Filters by the user's permissions and finds a range of all the course subjectses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @return the range of matching course subjectses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> filterFindByUserIdGroupId(long userId,
		long groupId, int start, int end) throws SystemException {
		return filterFindByUserIdGroupId(userId, groupId, start, end, null);
	}

	/**
	 * Filters by the user's permissions and finds an ordered range of all the course subjectses where userId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching course subjectses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> filterFindByUserIdGroupId(long userId,
		long groupId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByUserIdGroupId(userId, groupId, start, end,
				orderByComparator);
		}

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_COURSESUBJECTS_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_COURSESUBJECTS_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

		query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_COURSESUBJECTS_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_TABLE,
					orderByComparator);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				courseSubjects.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				q.addEntity(_FILTER_ENTITY_ALIAS, courseSubjectsImpl.class);
			}
			else {
				q.addEntity(_FILTER_ENTITY_TABLE, courseSubjectsImpl.class);
			}

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(userId);

			qPos.add(groupId);

			return (List<courseSubjects>)QueryUtil.list(q, getDialect(), start,
				end);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Finds all the course subjectses where subjectId = &#63;.
	 *
	 * @param subjectId the subject id to search with
	 * @return the matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findBySubjectId(long subjectId)
		throws SystemException {
		return findBySubjectId(subjectId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Finds a range of all the course subjectses where subjectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param subjectId the subject id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @return the range of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findBySubjectId(long subjectId, int start,
		int end) throws SystemException {
		return findBySubjectId(subjectId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the course subjectses where subjectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param subjectId the subject id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findBySubjectId(long subjectId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				subjectId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<courseSubjects> list = (List<courseSubjects>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_SUBJECTID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_COURSESUBJECTS_WHERE);

			query.append(_FINDER_COLUMN_SUBJECTID_SUBJECTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				list = (List<courseSubjects>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_SUBJECTID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_SUBJECTID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first course subjects in the ordered set where subjectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param subjectId the subject id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findBySubjectId_First(long subjectId,
		OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		List<courseSubjects> list = findBySubjectId(subjectId, 0, 1,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("subjectId=");
			msg.append(subjectId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchcourseSubjectsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last course subjects in the ordered set where subjectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param subjectId the subject id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findBySubjectId_Last(long subjectId,
		OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		int count = countBySubjectId(subjectId);

		List<courseSubjects> list = findBySubjectId(subjectId, count - 1,
				count, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("subjectId=");
			msg.append(subjectId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchcourseSubjectsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the course subjectses before and after the current course subjects in the ordered set where subjectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseSubjectId the primary key of the current course subjects
	 * @param subjectId the subject id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects[] findBySubjectId_PrevAndNext(long courseSubjectId,
		long subjectId, OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		courseSubjects courseSubjects = findByPrimaryKey(courseSubjectId);

		Session session = null;

		try {
			session = openSession();

			courseSubjects[] array = new courseSubjectsImpl[3];

			array[0] = getBySubjectId_PrevAndNext(session, courseSubjects,
					subjectId, orderByComparator, true);

			array[1] = courseSubjects;

			array[2] = getBySubjectId_PrevAndNext(session, courseSubjects,
					subjectId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected courseSubjects getBySubjectId_PrevAndNext(Session session,
		courseSubjects courseSubjects, long subjectId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSESUBJECTS_WHERE);

		query.append(_FINDER_COLUMN_SUBJECTID_SUBJECTID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(subjectId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(courseSubjects);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<courseSubjects> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Finds all the course subjectses where teacherId = &#63;.
	 *
	 * @param teacherId the teacher id to search with
	 * @return the matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByTeacherId(long teacherId)
		throws SystemException {
		return findByTeacherId(teacherId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Finds a range of all the course subjectses where teacherId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param teacherId the teacher id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @return the range of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByTeacherId(long teacherId, int start,
		int end) throws SystemException {
		return findByTeacherId(teacherId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the course subjectses where teacherId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param teacherId the teacher id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByTeacherId(long teacherId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				teacherId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<courseSubjects> list = (List<courseSubjects>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_TEACHERID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_COURSESUBJECTS_WHERE);

			query.append(_FINDER_COLUMN_TEACHERID_TEACHERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(teacherId);

				list = (List<courseSubjects>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_TEACHERID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_TEACHERID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first course subjects in the ordered set where teacherId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param teacherId the teacher id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findByTeacherId_First(long teacherId,
		OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		List<courseSubjects> list = findByTeacherId(teacherId, 0, 1,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("teacherId=");
			msg.append(teacherId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchcourseSubjectsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last course subjects in the ordered set where teacherId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param teacherId the teacher id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findByTeacherId_Last(long teacherId,
		OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		int count = countByTeacherId(teacherId);

		List<courseSubjects> list = findByTeacherId(teacherId, count - 1,
				count, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("teacherId=");
			msg.append(teacherId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchcourseSubjectsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the course subjectses before and after the current course subjects in the ordered set where teacherId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseSubjectId the primary key of the current course subjects
	 * @param teacherId the teacher id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects[] findByTeacherId_PrevAndNext(long courseSubjectId,
		long teacherId, OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		courseSubjects courseSubjects = findByPrimaryKey(courseSubjectId);

		Session session = null;

		try {
			session = openSession();

			courseSubjects[] array = new courseSubjectsImpl[3];

			array[0] = getByTeacherId_PrevAndNext(session, courseSubjects,
					teacherId, orderByComparator, true);

			array[1] = courseSubjects;

			array[2] = getByTeacherId_PrevAndNext(session, courseSubjects,
					teacherId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected courseSubjects getByTeacherId_PrevAndNext(Session session,
		courseSubjects courseSubjects, long teacherId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSESUBJECTS_WHERE);

		query.append(_FINDER_COLUMN_TEACHERID_TEACHERID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(teacherId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(courseSubjects);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<courseSubjects> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Finds all the course subjectses where courseId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @return the matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByCourseId(long courseId)
		throws SystemException {
		return findByCourseId(courseId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Finds a range of all the course subjectses where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @return the range of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByCourseId(long courseId, int start, int end)
		throws SystemException {
		return findByCourseId(courseId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the course subjectses where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByCourseId(long courseId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				courseId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<courseSubjects> list = (List<courseSubjects>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_COURSEID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_COURSESUBJECTS_WHERE);

			query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				list = (List<courseSubjects>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_COURSEID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_COURSEID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first course subjects in the ordered set where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findByCourseId_First(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		List<courseSubjects> list = findByCourseId(courseId, 0, 1,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("courseId=");
			msg.append(courseId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchcourseSubjectsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last course subjects in the ordered set where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findByCourseId_Last(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		int count = countByCourseId(courseId);

		List<courseSubjects> list = findByCourseId(courseId, count - 1, count,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("courseId=");
			msg.append(courseId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchcourseSubjectsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the course subjectses before and after the current course subjects in the ordered set where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseSubjectId the primary key of the current course subjects
	 * @param courseId the course id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects[] findByCourseId_PrevAndNext(long courseSubjectId,
		long courseId, OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		courseSubjects courseSubjects = findByPrimaryKey(courseSubjectId);

		Session session = null;

		try {
			session = openSession();

			courseSubjects[] array = new courseSubjectsImpl[3];

			array[0] = getByCourseId_PrevAndNext(session, courseSubjects,
					courseId, orderByComparator, true);

			array[1] = courseSubjects;

			array[2] = getByCourseId_PrevAndNext(session, courseSubjects,
					courseId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected courseSubjects getByCourseId_PrevAndNext(Session session,
		courseSubjects courseSubjects, long courseId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSESUBJECTS_WHERE);

		query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(courseId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(courseSubjects);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<courseSubjects> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Finds all the course subjectses where courseId = &#63; and groupId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @return the matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByCourseIdGroupId(long courseId,
		long groupId) throws SystemException {
		return findByCourseIdGroupId(courseId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the course subjectses where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @return the range of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByCourseIdGroupId(long courseId,
		long groupId, int start, int end) throws SystemException {
		return findByCourseIdGroupId(courseId, groupId, start, end, null);
	}

	/**
	 * Finds an ordered range of all the course subjectses where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findByCourseIdGroupId(long courseId,
		long groupId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		Object[] finderArgs = new Object[] {
				courseId, groupId,
				
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<courseSubjects> list = (List<courseSubjects>)FinderCacheUtil.getResult(FINDER_PATH_FIND_BY_COURSEIDGROUPID,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COURSESUBJECTS_WHERE);

			query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

			query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				qPos.add(groupId);

				list = (List<courseSubjects>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_BY_COURSEIDGROUPID,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_BY_COURSEIDGROUPID,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Finds the first course subjects in the ordered set where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the first matching course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findByCourseIdGroupId_First(long courseId,
		long groupId, OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		List<courseSubjects> list = findByCourseIdGroupId(courseId, groupId, 0,
				1, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("courseId=");
			msg.append(courseId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchcourseSubjectsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the last course subjects in the ordered set where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the last matching course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a matching course subjects could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects findByCourseIdGroupId_Last(long courseId,
		long groupId, OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		int count = countByCourseIdGroupId(courseId, groupId);

		List<courseSubjects> list = findByCourseIdGroupId(courseId, groupId,
				count - 1, count, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("courseId=");
			msg.append(courseId);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchcourseSubjectsException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Finds the course subjectses before and after the current course subjects in the ordered set where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseSubjectId the primary key of the current course subjects
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param orderByComparator the comparator to order the set by
	 * @return the previous, current, and next course subjects
	 * @throws org.xmlportletfactory.portal.school.NoSuchcourseSubjectsException if a course subjects with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public courseSubjects[] findByCourseIdGroupId_PrevAndNext(
		long courseSubjectId, long courseId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchcourseSubjectsException, SystemException {
		courseSubjects courseSubjects = findByPrimaryKey(courseSubjectId);

		Session session = null;

		try {
			session = openSession();

			courseSubjects[] array = new courseSubjectsImpl[3];

			array[0] = getByCourseIdGroupId_PrevAndNext(session,
					courseSubjects, courseId, groupId, orderByComparator, true);

			array[1] = courseSubjects;

			array[2] = getByCourseIdGroupId_PrevAndNext(session,
					courseSubjects, courseId, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected courseSubjects getByCourseIdGroupId_PrevAndNext(Session session,
		courseSubjects courseSubjects, long courseId, long groupId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSESUBJECTS_WHERE);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByFields = orderByComparator.getOrderByFields();

			if (orderByFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(courseId);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByValues(courseSubjects);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<courseSubjects> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Filters by the user's permissions and finds all the course subjectses where courseId = &#63; and groupId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @return the matching course subjectses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> filterFindByCourseIdGroupId(long courseId,
		long groupId) throws SystemException {
		return filterFindByCourseIdGroupId(courseId, groupId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Filters by the user's permissions and finds a range of all the course subjectses where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @return the range of matching course subjectses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> filterFindByCourseIdGroupId(long courseId,
		long groupId, int start, int end) throws SystemException {
		return filterFindByCourseIdGroupId(courseId, groupId, start, end, null);
	}

	/**
	 * Filters by the user's permissions and finds an ordered range of all the course subjectses where courseId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of matching course subjectses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> filterFindByCourseIdGroupId(long courseId,
		long groupId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByCourseIdGroupId(courseId, groupId, start, end,
				orderByComparator);
		}

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_COURSESUBJECTS_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_COURSESUBJECTS_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_COURSESUBJECTS_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_TABLE,
					orderByComparator);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				courseSubjects.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				q.addEntity(_FILTER_ENTITY_ALIAS, courseSubjectsImpl.class);
			}
			else {
				q.addEntity(_FILTER_ENTITY_TABLE, courseSubjectsImpl.class);
			}

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(courseId);

			qPos.add(groupId);

			return (List<courseSubjects>)QueryUtil.list(q, getDialect(), start,
				end);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Finds all the course subjectses.
	 *
	 * @return the course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Finds a range of all the course subjectses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @return the range of course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Finds an ordered range of all the course subjectses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of course subjectses to return
	 * @param end the upper bound of the range of course subjectses to return (not inclusive)
	 * @param orderByComparator the comparator to order the results by
	 * @return the ordered range of course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public List<courseSubjects> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		Object[] finderArgs = new Object[] {
				String.valueOf(start), String.valueOf(end),
				String.valueOf(orderByComparator)
			};

		List<courseSubjects> list = (List<courseSubjects>)FinderCacheUtil.getResult(FINDER_PATH_FIND_ALL,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_COURSESUBJECTS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COURSESUBJECTS;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<courseSubjects>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<courseSubjects>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FIND_ALL,
						finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_FIND_ALL, finderArgs,
						list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the course subjectses where groupId = &#63; from the database.
	 *
	 * @param groupId the group id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByGroupId(long groupId) throws SystemException {
		for (courseSubjects courseSubjects : findByGroupId(groupId)) {
			remove(courseSubjects);
		}
	}

	/**
	 * Removes all the course subjectses where userId = &#63; from the database.
	 *
	 * @param userId the user id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUserId(long userId) throws SystemException {
		for (courseSubjects courseSubjects : findByUserId(userId)) {
			remove(courseSubjects);
		}
	}

	/**
	 * Removes all the course subjectses where userId = &#63; and groupId = &#63; from the database.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		for (courseSubjects courseSubjects : findByUserIdGroupId(userId, groupId)) {
			remove(courseSubjects);
		}
	}

	/**
	 * Removes all the course subjectses where subjectId = &#63; from the database.
	 *
	 * @param subjectId the subject id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBySubjectId(long subjectId) throws SystemException {
		for (courseSubjects courseSubjects : findBySubjectId(subjectId)) {
			remove(courseSubjects);
		}
	}

	/**
	 * Removes all the course subjectses where teacherId = &#63; from the database.
	 *
	 * @param teacherId the teacher id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByTeacherId(long teacherId) throws SystemException {
		for (courseSubjects courseSubjects : findByTeacherId(teacherId)) {
			remove(courseSubjects);
		}
	}

	/**
	 * Removes all the course subjectses where courseId = &#63; from the database.
	 *
	 * @param courseId the course id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCourseId(long courseId) throws SystemException {
		for (courseSubjects courseSubjects : findByCourseId(courseId)) {
			remove(courseSubjects);
		}
	}

	/**
	 * Removes all the course subjectses where courseId = &#63; and groupId = &#63; from the database.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCourseIdGroupId(long courseId, long groupId)
		throws SystemException {
		for (courseSubjects courseSubjects : findByCourseIdGroupId(courseId,
				groupId)) {
			remove(courseSubjects);
		}
	}

	/**
	 * Removes all the course subjectses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (courseSubjects courseSubjects : findAll()) {
			remove(courseSubjects);
		}
	}

	/**
	 * Counts all the course subjectses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the number of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByGroupId(long groupId) throws SystemException {
		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_GROUPID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSESUBJECTS_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_GROUPID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Filters by the user's permissions and counts all the course subjectses where groupId = &#63;.
	 *
	 * @param groupId the group id to search with
	 * @return the number of matching course subjectses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public int filterCountByGroupId(long groupId) throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByGroupId(groupId);
		}

		StringBundler query = new StringBundler(2);

		query.append(_FILTER_SQL_COUNT_COURSESUBJECTS_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				courseSubjects.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			q.addScalar(COUNT_COLUMN_NAME,
				com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(groupId);

			Long count = (Long)q.uniqueResult();

			return count.intValue();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Counts all the course subjectses where userId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @return the number of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUserId(long userId) throws SystemException {
		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSESUBJECTS_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Counts all the course subjectses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the number of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		Object[] finderArgs = new Object[] { userId, groupId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERIDGROUPID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_COURSESUBJECTS_WHERE);

			query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

			query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERIDGROUPID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Filters by the user's permissions and counts all the course subjectses where userId = &#63; and groupId = &#63;.
	 *
	 * @param userId the user id to search with
	 * @param groupId the group id to search with
	 * @return the number of matching course subjectses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public int filterCountByUserIdGroupId(long userId, long groupId)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByUserIdGroupId(userId, groupId);
		}

		StringBundler query = new StringBundler(3);

		query.append(_FILTER_SQL_COUNT_COURSESUBJECTS_WHERE);

		query.append(_FINDER_COLUMN_USERIDGROUPID_USERID_2);

		query.append(_FINDER_COLUMN_USERIDGROUPID_GROUPID_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				courseSubjects.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			q.addScalar(COUNT_COLUMN_NAME,
				com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(userId);

			qPos.add(groupId);

			Long count = (Long)q.uniqueResult();

			return count.intValue();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Counts all the course subjectses where subjectId = &#63;.
	 *
	 * @param subjectId the subject id to search with
	 * @return the number of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public int countBySubjectId(long subjectId) throws SystemException {
		Object[] finderArgs = new Object[] { subjectId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_SUBJECTID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSESUBJECTS_WHERE);

			query.append(_FINDER_COLUMN_SUBJECTID_SUBJECTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_SUBJECTID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Counts all the course subjectses where teacherId = &#63;.
	 *
	 * @param teacherId the teacher id to search with
	 * @return the number of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByTeacherId(long teacherId) throws SystemException {
		Object[] finderArgs = new Object[] { teacherId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_TEACHERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSESUBJECTS_WHERE);

			query.append(_FINDER_COLUMN_TEACHERID_TEACHERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(teacherId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TEACHERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Counts all the course subjectses where courseId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @return the number of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCourseId(long courseId) throws SystemException {
		Object[] finderArgs = new Object[] { courseId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COURSEID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSESUBJECTS_WHERE);

			query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COURSEID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Counts all the course subjectses where courseId = &#63; and groupId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @return the number of matching course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCourseIdGroupId(long courseId, long groupId)
		throws SystemException {
		Object[] finderArgs = new Object[] { courseId, groupId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COURSEIDGROUPID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_COURSESUBJECTS_WHERE);

			query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

			query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COURSEIDGROUPID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Filters by the user's permissions and counts all the course subjectses where courseId = &#63; and groupId = &#63;.
	 *
	 * @param courseId the course id to search with
	 * @param groupId the group id to search with
	 * @return the number of matching course subjectses that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public int filterCountByCourseIdGroupId(long courseId, long groupId)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByCourseIdGroupId(courseId, groupId);
		}

		StringBundler query = new StringBundler(3);

		query.append(_FILTER_SQL_COUNT_COURSESUBJECTS_WHERE);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2);

		query.append(_FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				courseSubjects.class.getName(), _FILTER_COLUMN_PK,
				_FILTER_COLUMN_USERID, groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			q.addScalar(COUNT_COLUMN_NAME,
				com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(courseId);

			qPos.add(groupId);

			Long count = (Long)q.uniqueResult();

			return count.intValue();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Counts all the course subjectses.
	 *
	 * @return the number of course subjectses
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Object[] finderArgs = new Object[0];

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COURSESUBJECTS);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, finderArgs,
					count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the course subjects persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.org.xmlportletfactory.portal.school.model.courseSubjects")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<courseSubjects>> listenersList = new ArrayList<ModelListener<courseSubjects>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<courseSubjects>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(courseSubjectsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST);
	}

	@BeanReference(type = coursesPersistence.class)
	protected coursesPersistence coursesPersistence;
	@BeanReference(type = studentsPersistence.class)
	protected studentsPersistence studentsPersistence;
	@BeanReference(type = classroomsPersistence.class)
	protected classroomsPersistence classroomsPersistence;
	@BeanReference(type = commentsPersistence.class)
	protected commentsPersistence commentsPersistence;
	@BeanReference(type = courseSubjectsPersistence.class)
	protected courseSubjectsPersistence courseSubjectsPersistence;
	@BeanReference(type = subjectsPersistence.class)
	protected subjectsPersistence subjectsPersistence;
	@BeanReference(type = teachersPersistence.class)
	protected teachersPersistence teachersPersistence;
	@BeanReference(type = holidaysPersistence.class)
	protected holidaysPersistence holidaysPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_COURSESUBJECTS = "SELECT courseSubjects FROM courseSubjects courseSubjects";
	private static final String _SQL_SELECT_COURSESUBJECTS_WHERE = "SELECT courseSubjects FROM courseSubjects courseSubjects WHERE ";
	private static final String _SQL_COUNT_COURSESUBJECTS = "SELECT COUNT(courseSubjects) FROM courseSubjects courseSubjects";
	private static final String _SQL_COUNT_COURSESUBJECTS_WHERE = "SELECT COUNT(courseSubjects) FROM courseSubjects courseSubjects WHERE ";
	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "courseSubjects.groupId = ?";
	private static final String _FINDER_COLUMN_USERID_USERID_2 = "courseSubjects.userId = ?";
	private static final String _FINDER_COLUMN_USERIDGROUPID_USERID_2 = "courseSubjects.userId = ? AND ";
	private static final String _FINDER_COLUMN_USERIDGROUPID_GROUPID_2 = "courseSubjects.groupId = ?";
	private static final String _FINDER_COLUMN_SUBJECTID_SUBJECTID_2 = "courseSubjects.subjectId = ?";
	private static final String _FINDER_COLUMN_TEACHERID_TEACHERID_2 = "courseSubjects.teacherId = ?";
	private static final String _FINDER_COLUMN_COURSEID_COURSEID_2 = "courseSubjects.courseId = ?";
	private static final String _FINDER_COLUMN_COURSEIDGROUPID_COURSEID_2 = "courseSubjects.courseId = ? AND ";
	private static final String _FINDER_COLUMN_COURSEIDGROUPID_GROUPID_2 = "courseSubjects.groupId = ?";
	private static final String _FILTER_SQL_SELECT_COURSESUBJECTS_WHERE = "SELECT DISTINCT {courseSubjects.*} FROM coursesexample_courseSubjects courseSubjects WHERE ";
	private static final String _FILTER_SQL_SELECT_COURSESUBJECTS_NO_INLINE_DISTINCT_WHERE_1 =
		"SELECT {coursesexample_courseSubjects.*} FROM (SELECT DISTINCT courseSubjects.courseSubjectId FROM coursesexample_courseSubjects courseSubjects WHERE ";
	private static final String _FILTER_SQL_SELECT_COURSESUBJECTS_NO_INLINE_DISTINCT_WHERE_2 =
		") TEMP_TABLE INNER JOIN coursesexample_courseSubjects ON TEMP_TABLE.courseSubjectId = coursesexample_courseSubjects.courseSubjectId";
	private static final String _FILTER_SQL_COUNT_COURSESUBJECTS_WHERE = "SELECT COUNT(DISTINCT courseSubjects.courseSubjectId) AS COUNT_VALUE FROM coursesexample_courseSubjects courseSubjects WHERE ";
	private static final String _FILTER_COLUMN_PK = "courseSubjects.courseSubjectId";
	private static final String _FILTER_COLUMN_USERID = "courseSubjects.userId";
	private static final String _FILTER_ENTITY_ALIAS = "courseSubjects";
	private static final String _FILTER_ENTITY_TABLE = "coursesexample_courseSubjects";
	private static final String _ORDER_BY_ENTITY_ALIAS = "courseSubjects.";
	private static final String _ORDER_BY_ENTITY_TABLE = "coursesexample_courseSubjects.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No courseSubjects exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No courseSubjects exists with the key {";
	private static Log _log = LogFactoryUtil.getLog(courseSubjectsPersistenceImpl.class);
}
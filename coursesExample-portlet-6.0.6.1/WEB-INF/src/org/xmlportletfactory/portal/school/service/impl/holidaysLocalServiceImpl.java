/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school.service.impl;

import java.util.List;

import org.xmlportletfactory.portal.school.model.holidays;
import org.xmlportletfactory.portal.school.service.base.holidaysLocalServiceBaseImpl;
import org.xmlportletfactory.portal.school.service.persistence.holidaysUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.dao.orm.QueryUtil;

import com.liferay.portal.kernel.exception.PortalException;

/**
 * @author Jack A. Rider
 */
public class holidaysLocalServiceImpl extends holidaysLocalServiceBaseImpl {

	@SuppressWarnings("unchecked")
	public List findAllInUser(long userId)throws SystemException {
		List<holidays> list = (List<holidays>) holidaysUtil.findByUserId(userId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUser(long userId, OrderByComparator orderByComparator) throws SystemException {
		List<holidays> list = (List<holidays>) holidaysUtil.findByUserId(userId, QueryUtil.ALL_POS,QueryUtil.ALL_POS, orderByComparator);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInGroup(long groupId) throws SystemException {
		List<holidays> list = (List<holidays>) holidaysUtil.findByGroupId(groupId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInGroup(long groupId, OrderByComparator orderByComparator) throws SystemException{
		List <holidays> list = (List<holidays>) holidaysUtil.findByGroupId(groupId,QueryUtil.ALL_POS,QueryUtil.ALL_POS, orderByComparator);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUserAndGroup(long userId, long groupId) throws SystemException {
		List<holidays> list = (List<holidays>) holidaysUtil.findByUserIdGroupId(userId, groupId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUserAndGroup(long userId, long groupId, OrderByComparator orderByComparator) throws SystemException {
		List<holidays> list = (List<holidays>) holidaysUtil.findByUserIdGroupId(userId, groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, orderByComparator);
		return list;
	}



	@SuppressWarnings("unchecked")
	public List findAllIncourseIdGroup(long courseId, long groupId)	throws SystemException {
		List<holidays> list = (List<holidays>) holidaysUtil.findByCourseIdGroupId(courseId, groupId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllIncourseId(long courseId)	throws SystemException {
		List<holidays> list = (List<holidays>) holidaysUtil.findByCourseId(courseId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllIncourseIdGroup(long courseId, long groupId, OrderByComparator orderByComparator) throws SystemException {
		List<holidays> list = (List<holidays>) holidaysUtil.findByCourseIdGroupId(courseId, groupId, QueryUtil.ALL_POS,QueryUtil.ALL_POS, orderByComparator);
		return list;
	}


	public holidays addholidays (holidays validholidays) throws SystemException {
	    holidays fileobj = holidaysUtil.create(CounterLocalServiceUtil.increment(holidays.class.getName()));

	    fileobj.setCompanyId(validholidays.getCompanyId());
	    fileobj.setGroupId(validholidays.getGroupId());
	    fileobj.setUserId(validholidays.getUserId());

	    fileobj.setCourseId(validholidays.getCourseId());
	    fileobj.setHolidayName(validholidays.getHolidayName());
	    fileobj.setHolidayDate(validholidays.getHolidayDate());

	    return holidaysUtil.update(fileobj, false);
	}

	public void remove(holidays fileobj) throws SystemException {
	    holidaysUtil.remove(fileobj);
	}
}
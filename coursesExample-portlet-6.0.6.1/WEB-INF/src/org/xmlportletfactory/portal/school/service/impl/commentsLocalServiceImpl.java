/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school.service.impl;

import java.util.List;

import org.xmlportletfactory.portal.school.model.comments;
import org.xmlportletfactory.portal.school.service.base.commentsLocalServiceBaseImpl;
import org.xmlportletfactory.portal.school.service.persistence.commentsUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.dao.orm.QueryUtil;

import com.liferay.portal.kernel.exception.PortalException;

/**
 * @author Jack A. Rider
 */
public class commentsLocalServiceImpl extends commentsLocalServiceBaseImpl {

	@SuppressWarnings("unchecked")
	public List findAllInUser(long userId)throws SystemException {
		List<comments> list = (List<comments>) commentsUtil.findByUserId(userId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUser(long userId, OrderByComparator orderByComparator) throws SystemException {
		List<comments> list = (List<comments>) commentsUtil.findByUserId(userId, QueryUtil.ALL_POS,QueryUtil.ALL_POS, orderByComparator);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInGroup(long groupId) throws SystemException {
		List<comments> list = (List<comments>) commentsUtil.findByGroupId(groupId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInGroup(long groupId, OrderByComparator orderByComparator) throws SystemException{
		List <comments> list = (List<comments>) commentsUtil.findByGroupId(groupId,QueryUtil.ALL_POS,QueryUtil.ALL_POS, orderByComparator);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUserAndGroup(long userId, long groupId) throws SystemException {
		List<comments> list = (List<comments>) commentsUtil.findByUserIdGroupId(userId, groupId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUserAndGroup(long userId, long groupId, OrderByComparator orderByComparator) throws SystemException {
		List<comments> list = (List<comments>) commentsUtil.findByUserIdGroupId(userId, groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, orderByComparator);
		return list;
	}



	@SuppressWarnings("unchecked")
	public List findAllInstudentIdGroup(long studentId, long groupId)	throws SystemException {
		List<comments> list = (List<comments>) commentsUtil.findByStudentIdGroupId(studentId, groupId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInstudentId(long studentId)	throws SystemException {
		List<comments> list = (List<comments>) commentsUtil.findByStudentId(studentId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInstudentIdGroup(long studentId, long groupId, OrderByComparator orderByComparator) throws SystemException {
		List<comments> list = (List<comments>) commentsUtil.findByStudentIdGroupId(studentId, groupId, QueryUtil.ALL_POS,QueryUtil.ALL_POS, orderByComparator);
		return list;
	}


	public comments addcomments (comments validcomments) throws SystemException {
	    comments fileobj = commentsUtil.create(CounterLocalServiceUtil.increment(comments.class.getName()));

	    fileobj.setCompanyId(validcomments.getCompanyId());
	    fileobj.setGroupId(validcomments.getGroupId());
	    fileobj.setUserId(validcomments.getUserId());

	    fileobj.setStudentId(validcomments.getStudentId());
	    fileobj.setComment(validcomments.getComment());

	    return commentsUtil.update(fileobj, false);
	}

	public void remove(comments fileobj) throws SystemException {
	    commentsUtil.remove(fileobj);
	}
}
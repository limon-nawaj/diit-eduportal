/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school.service.impl;

import java.util.List;

import org.xmlportletfactory.portal.school.model.courseSubjects;
import org.xmlportletfactory.portal.school.service.base.courseSubjectsLocalServiceBaseImpl;
import org.xmlportletfactory.portal.school.service.persistence.courseSubjectsUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.dao.orm.QueryUtil;

import com.liferay.portal.kernel.exception.PortalException;

/**
 * @author Jack A. Rider
 */
public class courseSubjectsLocalServiceImpl extends courseSubjectsLocalServiceBaseImpl {

	@SuppressWarnings("unchecked")
	public List findAllInUser(long userId)throws SystemException {
		List<courseSubjects> list = (List<courseSubjects>) courseSubjectsUtil.findByUserId(userId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUser(long userId, OrderByComparator orderByComparator) throws SystemException {
		List<courseSubjects> list = (List<courseSubjects>) courseSubjectsUtil.findByUserId(userId, QueryUtil.ALL_POS,QueryUtil.ALL_POS, orderByComparator);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInGroup(long groupId) throws SystemException {
		List<courseSubjects> list = (List<courseSubjects>) courseSubjectsUtil.findByGroupId(groupId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInGroup(long groupId, OrderByComparator orderByComparator) throws SystemException{
		List <courseSubjects> list = (List<courseSubjects>) courseSubjectsUtil.findByGroupId(groupId,QueryUtil.ALL_POS,QueryUtil.ALL_POS, orderByComparator);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUserAndGroup(long userId, long groupId) throws SystemException {
		List<courseSubjects> list = (List<courseSubjects>) courseSubjectsUtil.findByUserIdGroupId(userId, groupId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUserAndGroup(long userId, long groupId, OrderByComparator orderByComparator) throws SystemException {
		List<courseSubjects> list = (List<courseSubjects>) courseSubjectsUtil.findByUserIdGroupId(userId, groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, orderByComparator);
		return list;
	}


	@SuppressWarnings("unchecked")
	public List findAllInSubjectId(long subjectId)
		throws SystemException {

		List<courseSubjects> list = (List<courseSubjects>) courseSubjectsUtil.findBySubjectId(subjectId);
		return list;
	}
	@SuppressWarnings("unchecked")
	public List findAllInTeacherId(long teacherId)
		throws SystemException {

		List<courseSubjects> list = (List<courseSubjects>) courseSubjectsUtil.findByTeacherId(teacherId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllIncourseIdGroup(long courseId, long groupId)	throws SystemException {
		List<courseSubjects> list = (List<courseSubjects>) courseSubjectsUtil.findByCourseIdGroupId(courseId, groupId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllIncourseId(long courseId)	throws SystemException {
		List<courseSubjects> list = (List<courseSubjects>) courseSubjectsUtil.findByCourseId(courseId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllIncourseIdGroup(long courseId, long groupId, OrderByComparator orderByComparator) throws SystemException {
		List<courseSubjects> list = (List<courseSubjects>) courseSubjectsUtil.findByCourseIdGroupId(courseId, groupId, QueryUtil.ALL_POS,QueryUtil.ALL_POS, orderByComparator);
		return list;
	}


	public courseSubjects addcourseSubjects (courseSubjects validcourseSubjects) throws SystemException {
	    courseSubjects fileobj = courseSubjectsUtil.create(CounterLocalServiceUtil.increment(courseSubjects.class.getName()));

	    fileobj.setCompanyId(validcourseSubjects.getCompanyId());
	    fileobj.setGroupId(validcourseSubjects.getGroupId());
	    fileobj.setUserId(validcourseSubjects.getUserId());

	    fileobj.setCourseId(validcourseSubjects.getCourseId());
	    fileobj.setSubjectId(validcourseSubjects.getSubjectId());
	    fileobj.setTeacherId(validcourseSubjects.getTeacherId());

	    return courseSubjectsUtil.update(fileobj, false);
	}

	public void remove(courseSubjects fileobj) throws SystemException {
	    courseSubjectsUtil.remove(fileobj);
	}
}
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school.service.impl;

import java.util.List;

import org.xmlportletfactory.portal.school.model.classrooms;
import org.xmlportletfactory.portal.school.service.base.classroomsLocalServiceBaseImpl;
import org.xmlportletfactory.portal.school.service.persistence.classroomsUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.dao.orm.QueryUtil;

import com.liferay.portal.kernel.exception.PortalException;

/**
 * @author Jack A. Rider
 */
public class classroomsLocalServiceImpl extends classroomsLocalServiceBaseImpl {

	@SuppressWarnings("unchecked")
	public List findAllInUser(long userId)throws SystemException {
		List<classrooms> list = (List<classrooms>) classroomsUtil.findByUserId(userId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUser(long userId, OrderByComparator orderByComparator) throws SystemException {
		List<classrooms> list = (List<classrooms>) classroomsUtil.findByUserId(userId, QueryUtil.ALL_POS,QueryUtil.ALL_POS, orderByComparator);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInGroup(long groupId) throws SystemException {
		List<classrooms> list = (List<classrooms>) classroomsUtil.findByGroupId(groupId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInGroup(long groupId, OrderByComparator orderByComparator) throws SystemException{
		List <classrooms> list = (List<classrooms>) classroomsUtil.findByGroupId(groupId,QueryUtil.ALL_POS,QueryUtil.ALL_POS, orderByComparator);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUserAndGroup(long userId, long groupId) throws SystemException {
		List<classrooms> list = (List<classrooms>) classroomsUtil.findByUserIdGroupId(userId, groupId);
		return list;
	}

	@SuppressWarnings("unchecked")
	public List findAllInUserAndGroup(long userId, long groupId, OrderByComparator orderByComparator) throws SystemException {
		List<classrooms> list = (List<classrooms>) classroomsUtil.findByUserIdGroupId(userId, groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, orderByComparator);
		return list;
	}





	public classrooms addclassrooms (classrooms validclassrooms) throws SystemException {
	    classrooms fileobj = classroomsUtil.create(CounterLocalServiceUtil.increment(classrooms.class.getName()));

	    fileobj.setCompanyId(validclassrooms.getCompanyId());
	    fileobj.setGroupId(validclassrooms.getGroupId());
	    fileobj.setUserId(validclassrooms.getUserId());

	    fileobj.setClassroomName(validclassrooms.getClassroomName());

	    return classroomsUtil.update(fileobj, false);
	}

	public void remove(classrooms fileobj) throws SystemException {
	    classroomsUtil.remove(fileobj);
	}
}
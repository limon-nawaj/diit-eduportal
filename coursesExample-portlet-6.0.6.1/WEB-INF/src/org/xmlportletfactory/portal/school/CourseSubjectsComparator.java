/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school;

import java.util.Date;

import com.liferay.portal.kernel.util.OrderByComparator;
import org.xmlportletfactory.portal.school.model.courseSubjects;
import org.xmlportletfactory.portal.school.service.subjectsLocalServiceUtil;
import org.xmlportletfactory.portal.school.service.teachersLocalServiceUtil;

public class CourseSubjectsComparator {

	public static String ORDER_BY_ASC =  " ASC";
	public static String ORDER_BY_DESC = " DESC";

	public static OrderByComparator getCourseSubjectsOrderByComparator(String orderByCol,String orderByType) {

		boolean orderByAsc = false;
		if(orderByType==null) {
			orderByAsc = true;
		} else if (orderByType.equalsIgnoreCase(ORDER_BY_ASC.trim())){
			orderByAsc = true;
		}

		OrderByComparator orderByComparator = null;
			if(orderByCol==null) {
			orderByComparator = new OrderByCourseSubjectsCourseSubjectId(orderByAsc);
			} else if(orderByCol.equals("courseSubjectId")){
			orderByComparator = new OrderByCourseSubjectsCourseSubjectId(orderByAsc);
			} else if(orderByCol.equals("courseId")){
			orderByComparator = new OrderByCourseSubjectsCourseId(orderByAsc);
			} else if(orderByCol.equals("subjectId")){
			orderByComparator = new OrderByCourseSubjectsSubjectId(orderByAsc);
			} else if(orderByCol.equals("teacherId")){
			orderByComparator = new OrderByCourseSubjectsTeacherId(orderByAsc);
	    }
	    return orderByComparator;
	}
}

class OrderByCourseSubjectsCourseSubjectId extends OrderByComparator {
	public static String ORDER_BY_FIELD = "courseSubjectId";

	public OrderByCourseSubjectsCourseSubjectId(){
		this(false);
	}

	public OrderByCourseSubjectsCourseSubjectId(boolean asc){
		_asc = asc;
	}

	@Override
	public int compare(Object o1,Object o2) {

		Long lo1 = 0L;
		Long lo2 = 0L;

		if(o1!=null) lo1 = (Long)o1;
		if(o2!=null) lo2 = (Long)o2;

		return lo1.compareTo(lo2);
	}

	@Override
	public String[] getOrderByFields() {
		String[] orderByFields = new String[1];
		orderByFields[0] = ORDER_BY_FIELD;
		return orderByFields;
	}

	@Override
	public String getOrderBy() {
		if(_asc) return CourseSubjectsComparator.ORDER_BY_ASC;
		else return CourseSubjectsComparator.ORDER_BY_DESC;
	}

	private boolean _asc;
}

class OrderByCourseSubjectsCourseId extends OrderByComparator {
	public static String ORDER_BY_FIELD = "courseId";

	public OrderByCourseSubjectsCourseId(){
		this(false);
	}

	public OrderByCourseSubjectsCourseId(boolean asc){
		_asc = asc;
	}

	@Override
	public int compare(Object o1,Object o2) {

		Long lo1 = 0L;
		Long lo2 = 0L;

		if(o1!=null) lo1 = (Long)o1;
		if(o2!=null) lo2 = (Long)o2;

		return lo1.compareTo(lo2);
	}

	@Override
	public String[] getOrderByFields() {
		String[] orderByFields = new String[1];
		orderByFields[0] = ORDER_BY_FIELD;
		return orderByFields;
	}

	@Override
	public String getOrderBy() {
		if(_asc) return CourseSubjectsComparator.ORDER_BY_ASC;
		else return CourseSubjectsComparator.ORDER_BY_DESC;
	}

	private boolean _asc;
}

class OrderByCourseSubjectsSubjectId extends OrderByComparator {
	public static String ORDER_BY_FIELD = "subjectId";

	public OrderByCourseSubjectsSubjectId(){
		this(false);
	}

	public OrderByCourseSubjectsSubjectId(boolean asc){
		_asc = asc;
	}

	@Override
	public int compare(Object o1,Object o2) {

		Long lo1 = 0L;
		Long lo2 = 0L;

		if(o1!=null) lo1 = (Long)o1;
		if(o2!=null) lo2 = (Long)o2;

/*
*
*      Shorting in case of a validation field.
*
*/
		if (o1!=null && o2!=null){
			try {
				String s01 = subjectsLocalServiceUtil.getsubjects(lo1).getSubjectName();
				String s02 = subjectsLocalServiceUtil.getsubjects(lo2).getSubjectName();
				return s01.compareTo(s02);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lo1.compareTo(lo2);
	}

	@Override
	public String[] getOrderByFields() {
		String[] orderByFields = new String[1];
		orderByFields[0] = ORDER_BY_FIELD;
		return orderByFields;
	}

	@Override
	public String getOrderBy() {
		if(_asc) return CourseSubjectsComparator.ORDER_BY_ASC;
		else return CourseSubjectsComparator.ORDER_BY_DESC;
	}

	private boolean _asc;
}

class OrderByCourseSubjectsTeacherId extends OrderByComparator {
	public static String ORDER_BY_FIELD = "teacherId";

	public OrderByCourseSubjectsTeacherId(){
		this(false);
	}

	public OrderByCourseSubjectsTeacherId(boolean asc){
		_asc = asc;
	}

	@Override
	public int compare(Object o1,Object o2) {

		Long lo1 = 0L;
		Long lo2 = 0L;

		if(o1!=null) lo1 = (Long)o1;
		if(o2!=null) lo2 = (Long)o2;

/*
*
*      Shorting in case of a validation field.
*
*/
		if (o1!=null && o2!=null){
			try {
				String s01 = teachersLocalServiceUtil.getteachers(lo1).getTeacherName();
				String s02 = teachersLocalServiceUtil.getteachers(lo2).getTeacherName();
				return s01.compareTo(s02);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lo1.compareTo(lo2);
	}

	@Override
	public String[] getOrderByFields() {
		String[] orderByFields = new String[1];
		orderByFields[0] = ORDER_BY_FIELD;
		return orderByFields;
	}

	@Override
	public String getOrderBy() {
		if(_asc) return CourseSubjectsComparator.ORDER_BY_ASC;
		else return CourseSubjectsComparator.ORDER_BY_DESC;
	}

	private boolean _asc;
}





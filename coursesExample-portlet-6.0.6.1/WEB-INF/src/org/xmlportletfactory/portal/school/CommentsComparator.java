/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school;

import java.util.Date;

import com.liferay.portal.kernel.util.OrderByComparator;
import org.xmlportletfactory.portal.school.model.comments;

public class CommentsComparator {

	public static String ORDER_BY_ASC =  " ASC";
	public static String ORDER_BY_DESC = " DESC";

	public static OrderByComparator getCommentsOrderByComparator(String orderByCol,String orderByType) {

		boolean orderByAsc = false;
		if(orderByType==null) {
			orderByAsc = true;
		} else if (orderByType.equalsIgnoreCase(ORDER_BY_ASC.trim())){
			orderByAsc = true;
		}

		OrderByComparator orderByComparator = null;
			if(orderByCol==null) {
			orderByComparator = new OrderByCommentsCommentId(orderByAsc);
			} else if(orderByCol.equals("commentId")){
			orderByComparator = new OrderByCommentsCommentId(orderByAsc);
			} else if(orderByCol.equals("studentId")){
			orderByComparator = new OrderByCommentsStudentId(orderByAsc);
		    }
	    return orderByComparator;
	}
}

class OrderByCommentsCommentId extends OrderByComparator {
	public static String ORDER_BY_FIELD = "commentId";

	public OrderByCommentsCommentId(){
		this(false);
	}

	public OrderByCommentsCommentId(boolean asc){
		_asc = asc;
	}

	@Override
	public int compare(Object o1,Object o2) {

		Long lo1 = 0L;
		Long lo2 = 0L;

		if(o1!=null) lo1 = (Long)o1;
		if(o2!=null) lo2 = (Long)o2;

		return lo1.compareTo(lo2);
	}

	@Override
	public String[] getOrderByFields() {
		String[] orderByFields = new String[1];
		orderByFields[0] = ORDER_BY_FIELD;
		return orderByFields;
	}

	@Override
	public String getOrderBy() {
		if(_asc) return CommentsComparator.ORDER_BY_ASC;
		else return CommentsComparator.ORDER_BY_DESC;
	}

	private boolean _asc;
}

class OrderByCommentsStudentId extends OrderByComparator {
	public static String ORDER_BY_FIELD = "studentId";

	public OrderByCommentsStudentId(){
		this(false);
	}

	public OrderByCommentsStudentId(boolean asc){
		_asc = asc;
	}

	@Override
	public int compare(Object o1,Object o2) {

		Long lo1 = 0L;
		Long lo2 = 0L;

		if(o1!=null) lo1 = (Long)o1;
		if(o2!=null) lo2 = (Long)o2;

		return lo1.compareTo(lo2);
	}

	@Override
	public String[] getOrderByFields() {
		String[] orderByFields = new String[1];
		orderByFields[0] = ORDER_BY_FIELD;
		return orderByFields;
	}

	@Override
	public String getOrderBy() {
		if(_asc) return CommentsComparator.ORDER_BY_ASC;
		else return CommentsComparator.ORDER_BY_DESC;
	}

	private boolean _asc;
}





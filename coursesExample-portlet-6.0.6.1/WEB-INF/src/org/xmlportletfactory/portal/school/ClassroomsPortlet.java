/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school;
 
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletURL;
import javax.portlet.ProcessAction;
import javax.portlet.ProcessEvent;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.portlet.PortletFileUpload;
import org.apache.commons.beanutils.BeanComparator;

import org.xmlportletfactory.portal.school.model.classrooms;
import org.xmlportletfactory.portal.school.model.impl.classroomsImpl;
import org.xmlportletfactory.portal.school.service.classroomsLocalServiceUtil;


import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class Classrooms
 */
public class ClassroomsPortlet extends MVCPortlet {



	public void init() throws PortletException {

		// Edit Mode Pages
		editJSP = getInitParameter("edit-jsp");

		// Help Mode Pages
		helpJSP = getInitParameter("help-jsp");

		// View Mode Pages
		viewJSP = getInitParameter("view-jsp");

		// View Mode Edit classrooms
		editclassroomsJSP = getInitParameter("edit-classrooms-jsp");
	}

	protected void include(String path, RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		PortletRequestDispatcher portletRequestDispatcher = getPortletContext()
				.getRequestDispatcher(path);

		if (portletRequestDispatcher == null) {
			// do nothing
			// _log.error(path + " is not a valid include");
		} else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		String jsp = (String) renderRequest.getParameter("view");
		if (jsp == null || jsp.equals("")) {
			showViewDefault(renderRequest, renderResponse);
		} else if (jsp.equalsIgnoreCase("editClassrooms")) {
			try {
				showViewEditClassrooms(renderRequest, renderResponse);
			} catch (Exception ex) {
				_log.debug(ex);
				try {
					showViewDefault(renderRequest, renderResponse);
				} catch (Exception ex1) {
					_log.debug(ex1);
				}
			}
		}
	}

	public void doEdit(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		showEditDefault(renderRequest, renderResponse);
	}

	public void doHelp(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		include(helpJSP, renderRequest, renderResponse);
	}

	@SuppressWarnings("unchecked")
	public void showViewDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		long groupId = themeDisplay.getScopeGroupId();

		PermissionChecker permissionChecker = themeDisplay
				.getPermissionChecker();

		boolean hasAddPermission = permissionChecker.hasPermission(groupId,
				"org.xmlportletfactory.portal.school.model", groupId, "ADD_CLASSROOMS");

		List<classrooms> tempResults = Collections.EMPTY_LIST;
		try {
			String orderByType = renderRequest.getParameter("orderByType");
			String orderByCol  = renderRequest.getParameter("orderByCol");
			OrderByComparator comparator = ClassroomsComparator.getClassroomsOrderByComparator(orderByCol,orderByType);
			MultiVMPoolUtil.clear();

			String classroomsFilter = ParamUtil.getString(renderRequest, "classroomsFilter");
			if (classroomsFilter.equalsIgnoreCase("")) {
				tempResults = classroomsLocalServiceUtil.findAllInGroup(groupId, comparator);
			} else {
				DynamicQuery query = DynamicQueryFactoryUtil.forClass(classrooms.class)
				.add(
					PropertyFactoryUtil.forName("classroomName").like("%"+ParamUtil.getString(renderRequest, "classroomsFilter")+"%")
				);
				tempResults = classroomsLocalServiceUtil.dynamicQuery(query, -1, -1, comparator);
			}
		
		} catch (Exception e) {
			_log.debug(e);
		}
		renderRequest.setAttribute("highlightRowWithKey", renderRequest.getParameter("highlightRowWithKey"));
		renderRequest.setAttribute("containerStart", renderRequest.getParameter("containerStart"));
		renderRequest.setAttribute("containerEnd", renderRequest.getParameter("containerEnd"));
		renderRequest.setAttribute("tempResults", tempResults);
		renderRequest.setAttribute("hasAddPermission", hasAddPermission);

		PortletURL addClassroomsURL = renderResponse.createActionURL();
		addClassroomsURL.setParameter("javax.portlet.action", "newClassrooms");
		renderRequest.setAttribute("addClassroomsURL", addClassroomsURL.toString());

		PortletURL classroomsFilterURL = renderResponse.createRenderURL();
		classroomsFilterURL.setParameter("javax.portlet.action", "doView");
		renderRequest.setAttribute("classroomsFilterURL", classroomsFilterURL.toString());

		include(viewJSP, renderRequest, renderResponse);
	}

	public void showViewEditClassrooms(RenderRequest renderRequest, RenderResponse renderResponse) throws Exception {

		SimpleDateFormat formatDia    = new SimpleDateFormat("dd");
		SimpleDateFormat formatMes    = new SimpleDateFormat("MM");
		SimpleDateFormat formatAno    = new SimpleDateFormat("yyyy");
		SimpleDateFormat formatHora   = new SimpleDateFormat("HH");
		SimpleDateFormat formatMinuto = new SimpleDateFormat("mm");

		PortletURL editClassroomsURL = renderResponse.createActionURL();
		String editType = (String) renderRequest.getParameter("editType");
		if (editType.equalsIgnoreCase("edit")) {
			editClassroomsURL.setParameter("javax.portlet.action", "updateClassrooms");
			long classroomId = Long.parseLong(renderRequest.getParameter("classroomId"));
			classrooms classrooms = classroomsLocalServiceUtil.getclassrooms(classroomId);
			String classroomName = classrooms.getClassroomName()+"";
			renderRequest.setAttribute("classroomName", classroomName);
            renderRequest.setAttribute("classrooms", classrooms);
		} else {
			editClassroomsURL.setParameter("javax.portlet.action", "addClassrooms");
			classrooms errorClassrooms = (classrooms) renderRequest.getAttribute("errorClassrooms");
			if (errorClassrooms != null) {
				if (editType.equalsIgnoreCase("update")) {
					editClassroomsURL.setParameter("javax.portlet.action", "updateClassrooms");
                }
				renderRequest.setAttribute("classrooms", errorClassrooms);
			} else {
				classroomsImpl blankClassrooms = new classroomsImpl();
				blankClassrooms.setClassroomId(0);
				blankClassrooms.setClassroomName("");
				renderRequest.setAttribute("classrooms", blankClassrooms);
			}

		}

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		renderRequest.setAttribute("editClassroomsURL", editClassroomsURL.toString());

		include(editclassroomsJSP, renderRequest, renderResponse);
	}

	private String dateToJsp(ActionRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(RenderRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(PortletPreferences prefs, Date date) {
		SimpleDateFormat format = new SimpleDateFormat(prefs.getValue("classrooms-date-format", "yyyy/MM/dd"));
		String stringDate = format.format(date);
		return stringDate;
	}
	private String dateTimeToJsp(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String stringDate = format.format(date);
		return stringDate;
	}

	public void showEditDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortletException, IOException {

		include(editJSP, renderRequest, renderResponse);
	}

	/* Portlet Actions */

	@ProcessAction(name = "newClassrooms")
	public void newClassrooms(ActionRequest request, ActionResponse response) {
		response.setRenderParameter("view", "editClassrooms");
		response.setRenderParameter("editType", "add");
	}

	@ProcessAction(name = "addClassrooms")
	public void addClassrooms(ActionRequest request, ActionResponse response) throws Exception {
            classrooms classrooms = classroomsFromRequest(request);
            ArrayList<String> errors = ClassroomsValidator.validateClassrooms(classrooms, request); 
            
            if (errors.isEmpty()) {
				classroomsLocalServiceUtil.addclassrooms(classrooms);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "classrooms-added-successfully");
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
                response.setRenderParameter("view", "editClassrooms");
                response.setRenderParameter("editType", "add");
                request.setAttribute("errorClassrooms", classrooms);
            }
	}

	@ProcessAction(name = "eventClassrooms")
	public void eventClassrooms(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		int containerStart = ParamUtil.getInteger(request, "containerStart");
		int containerEnd = ParamUtil.getInteger(request, "containerEnd");
		if (Validator.isNotNull(key)) {
            response.setRenderParameter("highlightRowWithKey", Long.toString(key));
            response.setRenderParameter("containerStart", Integer.toString(containerStart));
            response.setRenderParameter("containerEnd", Integer.toString(containerEnd));
		}
	}

	@ProcessAction(name = "editClassrooms")
	public void editClassrooms(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		if (Validator.isNotNull(key)) {
			response.setRenderParameter("classroomId", Long.toString(key));
			response.setRenderParameter("view", "editClassrooms");
			response.setRenderParameter("editType", "edit");
		}
	}

	@ProcessAction(name = "deleteClassrooms")
	public void deleteClassrooms(ActionRequest request, ActionResponse response)throws Exception {
		long id = ParamUtil.getLong(request, "resourcePrimKey");
		if (Validator.isNotNull(id)) {
			classrooms classrooms = classroomsLocalServiceUtil.getclassrooms(id);
			classroomsLocalServiceUtil.deleteclassrooms(classrooms);
            MultiVMPoolUtil.clear();
			SessionMessages.add(request, "classrooms-deleted-successfully");
		} else {
			SessionErrors.add(request, "classrooms-error-deleting");
		}
	}

	@ProcessAction(name = "updateClassrooms")
	public void updateClassrooms(ActionRequest request, ActionResponse response) throws Exception {
            classrooms classrooms = classroomsFromRequest(request);
            ArrayList<String> errors = ClassroomsValidator.validateClassrooms(classrooms, request); 
            
            if (errors.isEmpty()) {
                classroomsLocalServiceUtil.updateclassrooms(classrooms);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "classrooms-updated-successfully");
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
				response.setRenderParameter("classroomId)",Long.toString(classrooms.getPrimaryKey()));
				response.setRenderParameter("view", "editClassrooms");
				response.setRenderParameter("editType", "update");
				request.setAttribute("errorClassrooms", classrooms);
            }
        }

	@ProcessAction(name = "setClassroomsPref")
	public void setClassroomsPref(ActionRequest request, ActionResponse response) throws Exception {

		String rowsPerPage = ParamUtil.getString(request, "classrooms-rows-per-page");
		String dateFormat = ParamUtil.getString(request, "classrooms-date-format");
		String datetimeFormat = ParamUtil.getString(request, "classrooms-datetime-format");

		ArrayList<String> errors = new ArrayList();
		if (ClassroomsValidator.validateEditClassrooms(rowsPerPage, dateFormat, datetimeFormat, errors)) {
			response.setRenderParameter("classrooms-rows-per-page", "");
			response.setRenderParameter("classrooms-date-format", "");
			response.setRenderParameter("classrooms-datetime-format", "");

			PortletPreferences prefs = request.getPreferences();
			prefs.setValue("classrooms-rows-per-page", rowsPerPage);
			prefs.setValue("classrooms-date-format", dateFormat);
			prefs.setValue("classrooms-datetime-format", datetimeFormat);
			prefs.store();

			SessionMessages.add(request, "classrooms-prefs-success");
		}
	}

	private classrooms classroomsFromRequest(ActionRequest request) {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		classroomsImpl classrooms = new classroomsImpl();
        try {
            classrooms.setClassroomId(ParamUtil.getLong(request, "classroomId"));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
		classrooms.setClassroomName(ParamUtil.getString(request, "classroomName"));
		try {
		    classrooms.setPrimaryKey(ParamUtil.getLong(request,"resourcePrimKey"));
		} catch (NumberFormatException nfe) {
			//Controled en Validator
        }
		classrooms.setCompanyId(themeDisplay.getCompanyId());
		classrooms.setGroupId(themeDisplay.getScopeGroupId());
		classrooms.setUserId(themeDisplay.getUserId());
		return classrooms;
	}

	protected String editclassroomsJSP;
	protected String editJSP;
	protected String helpJSP;
	protected String viewJSP;

	private static Log _log = LogFactoryUtil.getLog(ClassroomsPortlet.class);

}

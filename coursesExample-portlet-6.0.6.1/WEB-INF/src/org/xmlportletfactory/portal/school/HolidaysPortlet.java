/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school;
 
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletURL;
import javax.portlet.ProcessAction;
import javax.portlet.ProcessEvent;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.portlet.PortletFileUpload;
import org.apache.commons.beanutils.BeanComparator;

import org.xmlportletfactory.portal.school.model.holidays;
import org.xmlportletfactory.portal.school.model.impl.holidaysImpl;
import org.xmlportletfactory.portal.school.service.holidaysLocalServiceUtil;


import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class Holidays
 */
public class HolidaysPortlet extends MVCPortlet {



	public void init() throws PortletException {

		// Edit Mode Pages
		editJSP = getInitParameter("edit-jsp");

		// Help Mode Pages
		helpJSP = getInitParameter("help-jsp");

		// View Mode Pages
		viewJSP = getInitParameter("view-jsp");

		// View Mode Edit holidays
		editholidaysJSP = getInitParameter("edit-holidays-jsp");
	}

	protected void include(String path, RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		PortletRequestDispatcher portletRequestDispatcher = getPortletContext()
				.getRequestDispatcher(path);

		if (portletRequestDispatcher == null) {
			// do nothing
			// _log.error(path + " is not a valid include");
		} else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		String jsp = (String) renderRequest.getParameter("view");
		if (jsp == null || jsp.equals("")) {
			showViewDefault(renderRequest, renderResponse);
		} else if (jsp.equalsIgnoreCase("editHolidays")) {
			try {
				showViewEditHolidays(renderRequest, renderResponse);
			} catch (Exception ex) {
				_log.debug(ex);
				try {
					showViewDefault(renderRequest, renderResponse);
				} catch (Exception ex1) {
					_log.debug(ex1);
				}
			}
		}
	}

	public void doEdit(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		showEditDefault(renderRequest, renderResponse);
	}

	public void doHelp(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		include(helpJSP, renderRequest, renderResponse);
	}

	@SuppressWarnings("unchecked")
	public void showViewDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		long groupId = themeDisplay.getScopeGroupId();

		PermissionChecker permissionChecker = themeDisplay
				.getPermissionChecker();

		boolean hasAddPermission = permissionChecker.hasPermission(groupId,
				"org.xmlportletfactory.portal.school.model", groupId, "ADD_HOLIDAYS");

		List<holidays> tempResults = Collections.EMPTY_LIST;
		long courseId = 0;
		String courseIdStr = renderRequest.getPortletSession().getAttribute("courseId")+ "";
		if (!courseIdStr.trim().equalsIgnoreCase("")){
			try {
				courseId = Long.parseLong(courseIdStr);
			} catch (Exception e) {
				courseId = 0;
			}
		}
		try {
			String orderByType = renderRequest.getParameter("orderByType");
			String orderByCol  = renderRequest.getParameter("orderByCol");
			OrderByComparator comparator = HolidaysComparator.getHolidaysOrderByComparator(orderByCol,orderByType);
			tempResults = holidaysLocalServiceUtil.findAllIncourseIdGroup(courseId, groupId,comparator);
		} catch (Exception e) {
			_log.debug(e);
		}
        if (courseId == 0) {
            hasAddPermission = false;
        } else {
            renderRequest.getPortletSession().setAttribute("HOLIDAYScourseId", courseId);
        }
		renderRequest.setAttribute("highlightRowWithKey", renderRequest.getParameter("highlightRowWithKey"));
		renderRequest.setAttribute("containerStart", renderRequest.getParameter("containerStart"));
		renderRequest.setAttribute("containerEnd", renderRequest.getParameter("containerEnd"));
		renderRequest.setAttribute("tempResults", tempResults);
		renderRequest.setAttribute("hasAddPermission", hasAddPermission);

		PortletURL addHolidaysURL = renderResponse.createActionURL();
		addHolidaysURL.setParameter("javax.portlet.action", "newHolidays");
		renderRequest.setAttribute("addHolidaysURL", addHolidaysURL.toString());

		PortletURL holidaysFilterURL = renderResponse.createRenderURL();
		holidaysFilterURL.setParameter("javax.portlet.action", "doView");
		renderRequest.setAttribute("holidaysFilterURL", holidaysFilterURL.toString());

		include(viewJSP, renderRequest, renderResponse);
	}

	public void showViewEditHolidays(RenderRequest renderRequest, RenderResponse renderResponse) throws Exception {

		SimpleDateFormat formatDia    = new SimpleDateFormat("dd");
		SimpleDateFormat formatMes    = new SimpleDateFormat("MM");
		SimpleDateFormat formatAno    = new SimpleDateFormat("yyyy");
		SimpleDateFormat formatHora   = new SimpleDateFormat("HH");
		SimpleDateFormat formatMinuto = new SimpleDateFormat("mm");

		PortletURL editHolidaysURL = renderResponse.createActionURL();
		String editType = (String) renderRequest.getParameter("editType");
		if (editType.equalsIgnoreCase("edit")) {
			editHolidaysURL.setParameter("javax.portlet.action", "updateHolidays");
			long holidayId = Long.parseLong(renderRequest.getParameter("holidayId"));
			holidays holidays = holidaysLocalServiceUtil.getholidays(holidayId);
			String courseId = holidays.getCourseId()+"";
			renderRequest.setAttribute("courseId", courseId);
			String holidayName = holidays.getHolidayName()+"";
			renderRequest.setAttribute("holidayName", holidayName);
			renderRequest.setAttribute("holidayDateDia", formatDia.format(holidays.getHolidayDate()));
			renderRequest.setAttribute("holidayDateMes", formatMes.format(holidays.getHolidayDate()));
			renderRequest.setAttribute("holidayDateAno", formatAno.format(holidays.getHolidayDate()));
			String holidayDate = dateToJsp(renderRequest, holidays.getHolidayDate());
			renderRequest.setAttribute("holidayDate", holidayDate);
            renderRequest.setAttribute("holidays", holidays);
		} else {
			editHolidaysURL.setParameter("javax.portlet.action", "addHolidays");
			holidays errorHolidays = (holidays) renderRequest.getAttribute("errorHolidays");
			if (errorHolidays != null) {
				if (editType.equalsIgnoreCase("update")) {
					editHolidaysURL.setParameter("javax.portlet.action", "updateHolidays");
                }
				renderRequest.setAttribute("holidays", errorHolidays);
                renderRequest.setAttribute("holidayDateDia", formatDia.format(errorHolidays.getHolidayDate()));
                renderRequest.setAttribute("holidayDateMes", formatMes.format(errorHolidays.getHolidayDate()));
                renderRequest.setAttribute("holidayDateAno", formatAno.format(errorHolidays.getHolidayDate()));
				String holidayDate = dateToJsp(renderRequest,errorHolidays.getHolidayDate());
				renderRequest.setAttribute("holidayDate", holidayDate);
			} else {
				holidaysImpl blankHolidays = new holidaysImpl();
				blankHolidays.setHolidayId(0);
				blankHolidays.setCourseId((Long) renderRequest.getPortletSession().getAttribute("HOLIDAYScourseId"));
				blankHolidays.setHolidayName("");
				blankHolidays.setHolidayDate(new Date());
				renderRequest.setAttribute("holidayDateDia", formatDia.format(blankHolidays.getHolidayDate()));
				renderRequest.setAttribute("holidayDateMes", formatMes.format(blankHolidays.getHolidayDate()));
				renderRequest.setAttribute("holidayDateAno", formatAno.format(blankHolidays.getHolidayDate()));
				String holidayDate = dateToJsp(renderRequest, blankHolidays.getHolidayDate());
				renderRequest.setAttribute("holidayDate", holidayDate);
                String courseIdStr = (String) renderRequest.getPortletSession().getAttribute("claseId");
				renderRequest.setAttribute("courseId", courseIdStr);
				renderRequest.setAttribute("holidays", blankHolidays);
			}

		}

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		renderRequest.setAttribute("editHolidaysURL", editHolidaysURL.toString());

		include(editholidaysJSP, renderRequest, renderResponse);
	}

	private String dateToJsp(ActionRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(RenderRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(PortletPreferences prefs, Date date) {
		SimpleDateFormat format = new SimpleDateFormat(prefs.getValue("holidays-date-format", "yyyy/MM/dd"));
		String stringDate = format.format(date);
		return stringDate;
	}
	private String dateTimeToJsp(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String stringDate = format.format(date);
		return stringDate;
	}

	public void showEditDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortletException, IOException {

		include(editJSP, renderRequest, renderResponse);
	}

	/* Portlet Actions */

	@ProcessAction(name = "newHolidays")
	public void newHolidays(ActionRequest request, ActionResponse response) {
		response.setRenderParameter("view", "editHolidays");
		response.setRenderParameter("editType", "add");
	}

	@ProcessAction(name = "addHolidays")
	public void addHolidays(ActionRequest request, ActionResponse response) throws Exception {
            holidays holidays = holidaysFromRequest(request);
            ArrayList<String> errors = HolidaysValidator.validateHolidays(holidays, request); 
            
            if (errors.isEmpty()) {
				holidaysLocalServiceUtil.addholidays(holidays);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "holidays-added-successfully");
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
                response.setRenderParameter("view", "editHolidays");
                response.setRenderParameter("editType", "add");
                request.setAttribute("errorHolidays", holidays);
            }
	}

	@ProcessAction(name = "eventHolidays")
	public void eventHolidays(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		int containerStart = ParamUtil.getInteger(request, "containerStart");
		int containerEnd = ParamUtil.getInteger(request, "containerEnd");
		if (Validator.isNotNull(key)) {
            response.setRenderParameter("highlightRowWithKey", Long.toString(key));
            response.setRenderParameter("containerStart", Integer.toString(containerStart));
            response.setRenderParameter("containerEnd", Integer.toString(containerEnd));
		}
	}

	@ProcessAction(name = "editHolidays")
	public void editHolidays(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		if (Validator.isNotNull(key)) {
			response.setRenderParameter("holidayId", Long.toString(key));
			response.setRenderParameter("view", "editHolidays");
			response.setRenderParameter("editType", "edit");
		}
	}

	@ProcessAction(name = "deleteHolidays")
	public void deleteHolidays(ActionRequest request, ActionResponse response)throws Exception {
		long id = ParamUtil.getLong(request, "resourcePrimKey");
		if (Validator.isNotNull(id)) {
			holidays holidays = holidaysLocalServiceUtil.getholidays(id);
			holidaysLocalServiceUtil.deleteholidays(holidays);
            MultiVMPoolUtil.clear();
			SessionMessages.add(request, "holidays-deleted-successfully");
		} else {
			SessionErrors.add(request, "holidays-error-deleting");
		}
	}

	@ProcessAction(name = "updateHolidays")
	public void updateHolidays(ActionRequest request, ActionResponse response) throws Exception {
            holidays holidays = holidaysFromRequest(request);
            ArrayList<String> errors = HolidaysValidator.validateHolidays(holidays, request); 
            
            if (errors.isEmpty()) {
                holidaysLocalServiceUtil.updateholidays(holidays);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "holidays-updated-successfully");
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
				response.setRenderParameter("holidayId)",Long.toString(holidays.getPrimaryKey()));
				response.setRenderParameter("view", "editHolidays");
				response.setRenderParameter("editType", "update");
				request.setAttribute("errorHolidays", holidays);
            }
        }

	@ProcessAction(name = "setHolidaysPref")
	public void setHolidaysPref(ActionRequest request, ActionResponse response) throws Exception {

		String rowsPerPage = ParamUtil.getString(request, "holidays-rows-per-page");
		String dateFormat = ParamUtil.getString(request, "holidays-date-format");
		String datetimeFormat = ParamUtil.getString(request, "holidays-datetime-format");

		ArrayList<String> errors = new ArrayList();
		if (HolidaysValidator.validateEditHolidays(rowsPerPage, dateFormat, datetimeFormat, errors)) {
			response.setRenderParameter("holidays-rows-per-page", "");
			response.setRenderParameter("holidays-date-format", "");
			response.setRenderParameter("holidays-datetime-format", "");

			PortletPreferences prefs = request.getPreferences();
			prefs.setValue("holidays-rows-per-page", rowsPerPage);
			prefs.setValue("holidays-date-format", dateFormat);
			prefs.setValue("holidays-datetime-format", datetimeFormat);
			prefs.store();

			SessionMessages.add(request, "holidays-prefs-success");
		}
	}

	private holidays holidaysFromRequest(ActionRequest request) {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		holidaysImpl holidays = new holidaysImpl();
        try {
            holidays.setHolidayId(ParamUtil.getLong(request, "holidayId"));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
        try {
            holidays.setCourseId(ParamUtil.getLong(request, "courseId"));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
		holidays.setHolidayName(ParamUtil.getString(request, "holidayName"));
	    PortletPreferences prefs = request.getPreferences();
        SimpleDateFormat format = new SimpleDateFormat(prefs.getValue("holidays-date-format", "yyyy/MM/dd"));
        int holidayDateAno = ParamUtil.getInteger(request, "holidayDateAno");
        int holidayDateMes = ParamUtil.getInteger(request, "holidayDateMes")+1;
        int holidayDateDia = ParamUtil.getInteger(request, "holidayDateDia");
        try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
            holidays.setHolidayDate(formatter.parse(holidayDateAno + "/" + holidayDateMes + "/" + holidayDateDia));
        } catch (ParseException e) {
			holidays.setHolidayDate(new Date());
        }
		try {
		    holidays.setPrimaryKey(ParamUtil.getLong(request,"resourcePrimKey"));
		} catch (NumberFormatException nfe) {
			//Controled en Validator
        }
		holidays.setCompanyId(themeDisplay.getCompanyId());
		holidays.setGroupId(themeDisplay.getScopeGroupId());
		holidays.setUserId(themeDisplay.getUserId());
		return holidays;
	}

	@ProcessEvent(qname="{http://liferay.com/events}Courses.courseId")
	public void reciveEvent(EventRequest request, EventResponse response) {
		Event event = request.getEvent();
		String courseId = (String)event.getValue();
		request.getPortletSession().setAttribute("courseId",courseId);
		response.setRenderParameter("courseId", courseId);
	}
	protected String editholidaysJSP;
	protected String editJSP;
	protected String helpJSP;
	protected String viewJSP;

	private static Log _log = LogFactoryUtil.getLog(HolidaysPortlet.class);

}

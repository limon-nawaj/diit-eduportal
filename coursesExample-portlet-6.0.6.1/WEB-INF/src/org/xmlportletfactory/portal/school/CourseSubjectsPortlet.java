/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school;
 
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletURL;
import javax.portlet.ProcessAction;
import javax.portlet.ProcessEvent;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.portlet.PortletFileUpload;
import org.apache.commons.beanutils.BeanComparator;

import org.xmlportletfactory.portal.school.model.courseSubjects;
import org.xmlportletfactory.portal.school.model.impl.courseSubjectsImpl;
import org.xmlportletfactory.portal.school.service.courseSubjectsLocalServiceUtil;

import org.xmlportletfactory.portal.school.model.subjects;
import org.xmlportletfactory.portal.school.service.subjectsLocalServiceUtil;
import org.xmlportletfactory.portal.school.model.teachers;
import org.xmlportletfactory.portal.school.service.teachersLocalServiceUtil;

import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class CourseSubjects
 */
public class CourseSubjectsPortlet extends MVCPortlet {



	public void init() throws PortletException {

		// Edit Mode Pages
		editJSP = getInitParameter("edit-jsp");

		// Help Mode Pages
		helpJSP = getInitParameter("help-jsp");

		// View Mode Pages
		viewJSP = getInitParameter("view-jsp");

		// View Mode Edit courseSubjects
		editcourseSubjectsJSP = getInitParameter("edit-courseSubjects-jsp");
	}

	protected void include(String path, RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		PortletRequestDispatcher portletRequestDispatcher = getPortletContext()
				.getRequestDispatcher(path);

		if (portletRequestDispatcher == null) {
			// do nothing
			// _log.error(path + " is not a valid include");
		} else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		String jsp = (String) renderRequest.getParameter("view");
		if (jsp == null || jsp.equals("")) {
			showViewDefault(renderRequest, renderResponse);
		} else if (jsp.equalsIgnoreCase("editCourseSubjects")) {
			try {
				showViewEditCourseSubjects(renderRequest, renderResponse);
			} catch (Exception ex) {
				_log.debug(ex);
				try {
					showViewDefault(renderRequest, renderResponse);
				} catch (Exception ex1) {
					_log.debug(ex1);
				}
			}
		}
	}

	public void doEdit(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		showEditDefault(renderRequest, renderResponse);
	}

	public void doHelp(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		include(helpJSP, renderRequest, renderResponse);
	}

	@SuppressWarnings("unchecked")
	public void showViewDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		long groupId = themeDisplay.getScopeGroupId();

		PermissionChecker permissionChecker = themeDisplay
				.getPermissionChecker();

		boolean hasAddPermission = permissionChecker.hasPermission(groupId,
				"org.xmlportletfactory.portal.school.model", groupId, "ADD_COURSESUBJECTS");

		List<courseSubjects> tempResults = Collections.EMPTY_LIST;
		long courseId = 0;
		String courseIdStr = renderRequest.getPortletSession().getAttribute("courseId")+ "";
		if (!courseIdStr.trim().equalsIgnoreCase("")){
			try {
				courseId = Long.parseLong(courseIdStr);
			} catch (Exception e) {
				courseId = 0;
			}
		}
		try {
			String orderByType = renderRequest.getParameter("orderByType");
			String orderByCol  = renderRequest.getParameter("orderByCol");
			OrderByComparator comparator = CourseSubjectsComparator.getCourseSubjectsOrderByComparator(orderByCol,orderByType);
			tempResults = courseSubjectsLocalServiceUtil.findAllIncourseIdGroup(courseId, groupId,comparator);
		} catch (Exception e) {
			_log.debug(e);
		}
        if (courseId == 0) {
            hasAddPermission = false;
        } else {
            renderRequest.getPortletSession().setAttribute("COURSESUBJECTScourseId", courseId);
        }
		renderRequest.setAttribute("highlightRowWithKey", renderRequest.getParameter("highlightRowWithKey"));
		renderRequest.setAttribute("containerStart", renderRequest.getParameter("containerStart"));
		renderRequest.setAttribute("containerEnd", renderRequest.getParameter("containerEnd"));
		renderRequest.setAttribute("tempResults", tempResults);
		renderRequest.setAttribute("hasAddPermission", hasAddPermission);

		PortletURL addCourseSubjectsURL = renderResponse.createActionURL();
		addCourseSubjectsURL.setParameter("javax.portlet.action", "newCourseSubjects");
		renderRequest.setAttribute("addCourseSubjectsURL", addCourseSubjectsURL.toString());

		PortletURL courseSubjectsFilterURL = renderResponse.createRenderURL();
		courseSubjectsFilterURL.setParameter("javax.portlet.action", "doView");
		renderRequest.setAttribute("courseSubjectsFilterURL", courseSubjectsFilterURL.toString());

		include(viewJSP, renderRequest, renderResponse);
	}

	public void showViewEditCourseSubjects(RenderRequest renderRequest, RenderResponse renderResponse) throws Exception {

		SimpleDateFormat formatDia    = new SimpleDateFormat("dd");
		SimpleDateFormat formatMes    = new SimpleDateFormat("MM");
		SimpleDateFormat formatAno    = new SimpleDateFormat("yyyy");
		SimpleDateFormat formatHora   = new SimpleDateFormat("HH");
		SimpleDateFormat formatMinuto = new SimpleDateFormat("mm");

		PortletURL editCourseSubjectsURL = renderResponse.createActionURL();
		String editType = (String) renderRequest.getParameter("editType");
		if (editType.equalsIgnoreCase("edit")) {
			editCourseSubjectsURL.setParameter("javax.portlet.action", "updateCourseSubjects");
			long courseSubjectId = Long.parseLong(renderRequest.getParameter("courseSubjectId"));
			courseSubjects courseSubjects = courseSubjectsLocalServiceUtil.getcourseSubjects(courseSubjectId);
			String courseId = courseSubjects.getCourseId()+"";
			renderRequest.setAttribute("courseId", courseId);
			String subjectId = courseSubjects.getSubjectId()+"";
			renderRequest.setAttribute("subjectId", subjectId);
			String teacherId = courseSubjects.getTeacherId()+"";
			renderRequest.setAttribute("teacherId", teacherId);
            renderRequest.setAttribute("courseSubjects", courseSubjects);
		} else {
			editCourseSubjectsURL.setParameter("javax.portlet.action", "addCourseSubjects");
			courseSubjects errorCourseSubjects = (courseSubjects) renderRequest.getAttribute("errorCourseSubjects");
			if (errorCourseSubjects != null) {
				if (editType.equalsIgnoreCase("update")) {
					editCourseSubjectsURL.setParameter("javax.portlet.action", "updateCourseSubjects");
                }
				renderRequest.setAttribute("courseSubjects", errorCourseSubjects);
			} else {
				courseSubjectsImpl blankCourseSubjects = new courseSubjectsImpl();
				blankCourseSubjects.setCourseSubjectId(0);
				blankCourseSubjects.setCourseId((Long) renderRequest.getPortletSession().getAttribute("COURSESUBJECTScourseId"));
				blankCourseSubjects.setSubjectId(0);
				blankCourseSubjects.setTeacherId(0);
                String courseIdStr = (String) renderRequest.getPortletSession().getAttribute("claseId");
				renderRequest.setAttribute("courseId", courseIdStr);
				renderRequest.setAttribute("courseSubjects", blankCourseSubjects);
			}

		}

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		OrderByComparator Subjects_comparator1 = SubjectsComparator.getSubjectsOrderByComparator("subjectName", "ASC");
		MultiVMPoolUtil.clear();
		List<subjects> subjectIdList = subjectsLocalServiceUtil.findAllInGroup(themeDisplay.getScopeGroupId(),Subjects_comparator1);
		renderRequest.setAttribute("subjectIdList", subjectIdList);

		OrderByComparator Teachers_comparator2 = TeachersComparator.getTeachersOrderByComparator("teacherName", "ASC");
		MultiVMPoolUtil.clear();
		List<teachers> teacherIdList = teachersLocalServiceUtil.findAllInGroup(themeDisplay.getScopeGroupId(),Teachers_comparator2);
		renderRequest.setAttribute("teacherIdList", teacherIdList);


		renderRequest.setAttribute("editCourseSubjectsURL", editCourseSubjectsURL.toString());

		include(editcourseSubjectsJSP, renderRequest, renderResponse);
	}

	private String dateToJsp(ActionRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(RenderRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(PortletPreferences prefs, Date date) {
		SimpleDateFormat format = new SimpleDateFormat(prefs.getValue("courseSubjects-date-format", "yyyy/MM/dd"));
		String stringDate = format.format(date);
		return stringDate;
	}
	private String dateTimeToJsp(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String stringDate = format.format(date);
		return stringDate;
	}

	public void showEditDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortletException, IOException {

		include(editJSP, renderRequest, renderResponse);
	}

	/* Portlet Actions */

	@ProcessAction(name = "newCourseSubjects")
	public void newCourseSubjects(ActionRequest request, ActionResponse response) {
		response.setRenderParameter("view", "editCourseSubjects");
		response.setRenderParameter("editType", "add");
	}

	@ProcessAction(name = "addCourseSubjects")
	public void addCourseSubjects(ActionRequest request, ActionResponse response) throws Exception {
            courseSubjects courseSubjects = courseSubjectsFromRequest(request);
            ArrayList<String> errors = CourseSubjectsValidator.validateCourseSubjects(courseSubjects, request); 
            
            if (errors.isEmpty()) {
				courseSubjectsLocalServiceUtil.addcourseSubjects(courseSubjects);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "courseSubjects-added-successfully");
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
                response.setRenderParameter("view", "editCourseSubjects");
                response.setRenderParameter("editType", "add");
                request.setAttribute("errorCourseSubjects", courseSubjects);
            }
	}

	@ProcessAction(name = "eventCourseSubjects")
	public void eventCourseSubjects(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		int containerStart = ParamUtil.getInteger(request, "containerStart");
		int containerEnd = ParamUtil.getInteger(request, "containerEnd");
		if (Validator.isNotNull(key)) {
            response.setRenderParameter("highlightRowWithKey", Long.toString(key));
            response.setRenderParameter("containerStart", Integer.toString(containerStart));
            response.setRenderParameter("containerEnd", Integer.toString(containerEnd));
		}
	}

	@ProcessAction(name = "editCourseSubjects")
	public void editCourseSubjects(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		if (Validator.isNotNull(key)) {
			response.setRenderParameter("courseSubjectId", Long.toString(key));
			response.setRenderParameter("view", "editCourseSubjects");
			response.setRenderParameter("editType", "edit");
		}
	}

	@ProcessAction(name = "deleteCourseSubjects")
	public void deleteCourseSubjects(ActionRequest request, ActionResponse response)throws Exception {
		long id = ParamUtil.getLong(request, "resourcePrimKey");
		if (Validator.isNotNull(id)) {
			courseSubjects courseSubjects = courseSubjectsLocalServiceUtil.getcourseSubjects(id);
			courseSubjectsLocalServiceUtil.deletecourseSubjects(courseSubjects);
            MultiVMPoolUtil.clear();
			SessionMessages.add(request, "courseSubjects-deleted-successfully");
		} else {
			SessionErrors.add(request, "courseSubjects-error-deleting");
		}
	}

	@ProcessAction(name = "updateCourseSubjects")
	public void updateCourseSubjects(ActionRequest request, ActionResponse response) throws Exception {
            courseSubjects courseSubjects = courseSubjectsFromRequest(request);
            ArrayList<String> errors = CourseSubjectsValidator.validateCourseSubjects(courseSubjects, request); 
            
            if (errors.isEmpty()) {
                courseSubjectsLocalServiceUtil.updatecourseSubjects(courseSubjects);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "courseSubjects-updated-successfully");
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
				response.setRenderParameter("courseSubjectId)",Long.toString(courseSubjects.getPrimaryKey()));
				response.setRenderParameter("view", "editCourseSubjects");
				response.setRenderParameter("editType", "update");
				request.setAttribute("errorCourseSubjects", courseSubjects);
            }
        }

	@ProcessAction(name = "setCourseSubjectsPref")
	public void setCourseSubjectsPref(ActionRequest request, ActionResponse response) throws Exception {

		String rowsPerPage = ParamUtil.getString(request, "courseSubjects-rows-per-page");
		String dateFormat = ParamUtil.getString(request, "courseSubjects-date-format");
		String datetimeFormat = ParamUtil.getString(request, "courseSubjects-datetime-format");

		ArrayList<String> errors = new ArrayList();
		if (CourseSubjectsValidator.validateEditCourseSubjects(rowsPerPage, dateFormat, datetimeFormat, errors)) {
			response.setRenderParameter("courseSubjects-rows-per-page", "");
			response.setRenderParameter("courseSubjects-date-format", "");
			response.setRenderParameter("courseSubjects-datetime-format", "");

			PortletPreferences prefs = request.getPreferences();
			prefs.setValue("courseSubjects-rows-per-page", rowsPerPage);
			prefs.setValue("courseSubjects-date-format", dateFormat);
			prefs.setValue("courseSubjects-datetime-format", datetimeFormat);
			prefs.store();

			SessionMessages.add(request, "courseSubjects-prefs-success");
		}
	}

	private courseSubjects courseSubjectsFromRequest(ActionRequest request) {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		courseSubjectsImpl courseSubjects = new courseSubjectsImpl();
        try {
            courseSubjects.setCourseSubjectId(ParamUtil.getLong(request, "courseSubjectId"));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
        try {
            courseSubjects.setCourseId(ParamUtil.getLong(request, "courseId"));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
        try {
            courseSubjects.setSubjectId(ParamUtil.getLong(request, "subjectId"));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
        try {
            courseSubjects.setTeacherId(ParamUtil.getLong(request, "teacherId"));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
		try {
		    courseSubjects.setPrimaryKey(ParamUtil.getLong(request,"resourcePrimKey"));
		} catch (NumberFormatException nfe) {
			//Controled en Validator
        }
		courseSubjects.setCompanyId(themeDisplay.getCompanyId());
		courseSubjects.setGroupId(themeDisplay.getScopeGroupId());
		courseSubjects.setUserId(themeDisplay.getUserId());
		return courseSubjects;
	}

	@ProcessEvent(qname="{http://liferay.com/events}Courses.courseId")
	public void reciveEvent(EventRequest request, EventResponse response) {
		Event event = request.getEvent();
		String courseId = (String)event.getValue();
		request.getPortletSession().setAttribute("courseId",courseId);
		response.setRenderParameter("courseId", courseId);
	}
	protected String editcourseSubjectsJSP;
	protected String editJSP;
	protected String helpJSP;
	protected String viewJSP;

	private static Log _log = LogFactoryUtil.getLog(CourseSubjectsPortlet.class);

}

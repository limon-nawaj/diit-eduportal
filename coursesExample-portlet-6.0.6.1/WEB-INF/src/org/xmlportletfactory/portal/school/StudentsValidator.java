/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.portlet.ActionRequest;

import org.xmlportletfactory.portal.school.model.students;
import org.xmlportletfactory.portal.school.service.classroomsLocalServiceUtil;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.ParamUtil;

public class StudentsValidator {

	public static ArrayList<String> validateStudents(students students, ActionRequest request) throws IOException {
		ArrayList<String> errors = new ArrayList<String>();
		Properties props = new Properties();
		ClassLoader classLoader = StudentsValidator.class.getClassLoader();
		InputStream is = classLoader.getResourceAsStream("regexp.properties");
		props.load(is);


	//Field courseId
	
	
		if(!validateCourseId(props, request.getAttribute("courseId").toString())){
		    errors.add("error_number_format");
		}

	//Field studentName
	
	
		if(!validateStudentName(props, request.getAttribute("studentName").toString())){
		    errors.add("error");
		}
		if (Validator.isNull(students.getStudentName())) {
			errors.add("Students-studentName-required");
		}

		try {
			classroomsLocalServiceUtil.getclassrooms(students.getClassroomId());
		} catch (NoSuchclassroomsException e){
			errors.add("Students-classroomId-required");
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return errors;
	}

	public static boolean validateEditStudents(
		String rowsPerPage, String dateFormat, String datetimeFormat, List errors) {
		boolean valid = true;
		if (Validator.isNull(rowsPerPage)) {
			errors.add("students-rows-per-page-required");
			valid = false;
		} else if (!Validator.isNumber(rowsPerPage)) {
			errors.add("students-rows-per-page-invalid");
			valid = false;
		} else if (Validator.isNull(dateFormat)) {
			errors.add("students-date-format-required");
			valid = false;
		} else if (Validator.isNull(datetimeFormat)) {
			errors.add("students-datetime_format.required");
			valid = false;
		}
		return valid;
	}

	//Field studentId
	private static boolean validateStudentId(Properties props,String field) {
		boolean valid = true;
		try {
			Double.parseDouble(field);
		} catch (NumberFormatException nfe) {
		    valid = false;
		}
		return valid;
	}
	//Field courseId
	private static boolean validateCourseId(Properties props,String field) {
		boolean valid = true;
		try {
			Double.parseDouble(field);
		} catch (NumberFormatException nfe) {
		    valid = false;
		}
		return valid;
	}
	//Field studentName
	private static boolean validateStudentName(Properties props,String field) {
		boolean valid = true;
		return valid;
	}
	//Field classroomId
	private static boolean validateClassroomId(Properties props,String field) {
		boolean valid = true;
		try {
			Double.parseDouble(field);
		} catch (NumberFormatException nfe) {
		    valid = false;
		}
		return valid;
	}
	//Field studentPhoto
	private static boolean validateStudentPhoto(Properties props,String field) {
		boolean valid = true;
		return valid;
	}
}

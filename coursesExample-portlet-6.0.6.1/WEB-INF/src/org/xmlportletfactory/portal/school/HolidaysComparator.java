/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school;

import java.util.Date;

import com.liferay.portal.kernel.util.OrderByComparator;
import org.xmlportletfactory.portal.school.model.holidays;

public class HolidaysComparator {

	public static String ORDER_BY_ASC =  " ASC";
	public static String ORDER_BY_DESC = " DESC";

	public static OrderByComparator getHolidaysOrderByComparator(String orderByCol,String orderByType) {

		boolean orderByAsc = false;
		if(orderByType==null) {
			orderByAsc = true;
		} else if (orderByType.equalsIgnoreCase(ORDER_BY_ASC.trim())){
			orderByAsc = true;
		}

		OrderByComparator orderByComparator = null;
			if(orderByCol==null) {
			orderByComparator = new OrderByHolidaysHolidayId(orderByAsc);
			} else if(orderByCol.equals("holidayId")){
			orderByComparator = new OrderByHolidaysHolidayId(orderByAsc);
			} else if(orderByCol.equals("courseId")){
			orderByComparator = new OrderByHolidaysCourseId(orderByAsc);
			} else if(orderByCol.equals("holidayName")){
			orderByComparator = new OrderByHolidaysHolidayName(orderByAsc);
			} else if(orderByCol.equals("holidayDate")){
			orderByComparator = new OrderByHolidaysHolidayDate(orderByAsc);
	    }
	    return orderByComparator;
	}
}

class OrderByHolidaysHolidayId extends OrderByComparator {
	public static String ORDER_BY_FIELD = "holidayId";

	public OrderByHolidaysHolidayId(){
		this(false);
	}

	public OrderByHolidaysHolidayId(boolean asc){
		_asc = asc;
	}

	@Override
	public int compare(Object o1,Object o2) {

		Long lo1 = 0L;
		Long lo2 = 0L;

		if(o1!=null) lo1 = (Long)o1;
		if(o2!=null) lo2 = (Long)o2;

		return lo1.compareTo(lo2);
	}

	@Override
	public String[] getOrderByFields() {
		String[] orderByFields = new String[1];
		orderByFields[0] = ORDER_BY_FIELD;
		return orderByFields;
	}

	@Override
	public String getOrderBy() {
		if(_asc) return HolidaysComparator.ORDER_BY_ASC;
		else return HolidaysComparator.ORDER_BY_DESC;
	}

	private boolean _asc;
}

class OrderByHolidaysCourseId extends OrderByComparator {
	public static String ORDER_BY_FIELD = "courseId";

	public OrderByHolidaysCourseId(){
		this(false);
	}

	public OrderByHolidaysCourseId(boolean asc){
		_asc = asc;
	}

	@Override
	public int compare(Object o1,Object o2) {

		Long lo1 = 0L;
		Long lo2 = 0L;

		if(o1!=null) lo1 = (Long)o1;
		if(o2!=null) lo2 = (Long)o2;

		return lo1.compareTo(lo2);
	}

	@Override
	public String[] getOrderByFields() {
		String[] orderByFields = new String[1];
		orderByFields[0] = ORDER_BY_FIELD;
		return orderByFields;
	}

	@Override
	public String getOrderBy() {
		if(_asc) return HolidaysComparator.ORDER_BY_ASC;
		else return HolidaysComparator.ORDER_BY_DESC;
	}

	private boolean _asc;
}

class OrderByHolidaysHolidayName extends OrderByComparator {
	public static String ORDER_BY_FIELD = "holidayName";

	public OrderByHolidaysHolidayName(){
		this(false);
	}

	public OrderByHolidaysHolidayName(boolean asc){
		_asc = asc;
	}

	@Override
	public int compare(Object o1,Object o2) {

		String str1 = "";
		String str2 = "";

		if(str1!=null) str1 = (String)o1;
		if(str2!=null) str2 = (String)o1;

		return str1.compareTo(str2);
	}

	@Override
	public String[] getOrderByFields() {
		String[] orderByFields = new String[1];
		orderByFields[0] = ORDER_BY_FIELD;
		return orderByFields;
	}

	@Override
	public String getOrderBy() {
		if(_asc) return HolidaysComparator.ORDER_BY_ASC;
		else return HolidaysComparator.ORDER_BY_DESC;
	}

	private boolean _asc;
}
class OrderByHolidaysHolidayDate extends OrderByComparator {
	public static String ORDER_BY_FIELD = "holidayDate";

	public OrderByHolidaysHolidayDate(){
		this(false);
	}

	public OrderByHolidaysHolidayDate(boolean asc){
		_asc = asc;
	}

	@Override
	public int compare(Object o1,Object o2) {

		Date date1 = new Date();
		Date date2 = new Date();

		if(o1!=null) date1 = (Date)o1;
		if(o2!=null) date2 = (Date)o2;

		return date1.compareTo(date2);
	}

	@Override
	public String[] getOrderByFields() {
		String[] orderByFields = new String[1];
		orderByFields[0] = ORDER_BY_FIELD;
		return orderByFields;
	}

	@Override
	public String getOrderBy() {
		if(_asc) return HolidaysComparator.ORDER_BY_ASC;
		else return HolidaysComparator.ORDER_BY_DESC;
	}

	private boolean _asc;
}





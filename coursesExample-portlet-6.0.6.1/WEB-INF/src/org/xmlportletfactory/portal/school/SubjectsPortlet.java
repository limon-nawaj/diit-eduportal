/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school;
 
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletURL;
import javax.portlet.ProcessAction;
import javax.portlet.ProcessEvent;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.portlet.PortletFileUpload;
import org.apache.commons.beanutils.BeanComparator;

import org.xmlportletfactory.portal.school.model.subjects;
import org.xmlportletfactory.portal.school.model.impl.subjectsImpl;
import org.xmlportletfactory.portal.school.service.subjectsLocalServiceUtil;


import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class Subjects
 */
public class SubjectsPortlet extends MVCPortlet {



	public void init() throws PortletException {

		// Edit Mode Pages
		editJSP = getInitParameter("edit-jsp");

		// Help Mode Pages
		helpJSP = getInitParameter("help-jsp");

		// View Mode Pages
		viewJSP = getInitParameter("view-jsp");

		// View Mode Edit subjects
		editsubjectsJSP = getInitParameter("edit-subjects-jsp");
	}

	protected void include(String path, RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		PortletRequestDispatcher portletRequestDispatcher = getPortletContext()
				.getRequestDispatcher(path);

		if (portletRequestDispatcher == null) {
			// do nothing
			// _log.error(path + " is not a valid include");
		} else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		String jsp = (String) renderRequest.getParameter("view");
		if (jsp == null || jsp.equals("")) {
			showViewDefault(renderRequest, renderResponse);
		} else if (jsp.equalsIgnoreCase("editSubjects")) {
			try {
				showViewEditSubjects(renderRequest, renderResponse);
			} catch (Exception ex) {
				_log.debug(ex);
				try {
					showViewDefault(renderRequest, renderResponse);
				} catch (Exception ex1) {
					_log.debug(ex1);
				}
			}
		}
	}

	public void doEdit(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		showEditDefault(renderRequest, renderResponse);
	}

	public void doHelp(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		include(helpJSP, renderRequest, renderResponse);
	}

	@SuppressWarnings("unchecked")
	public void showViewDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		long groupId = themeDisplay.getScopeGroupId();

		PermissionChecker permissionChecker = themeDisplay
				.getPermissionChecker();

		boolean hasAddPermission = permissionChecker.hasPermission(groupId,
				"org.xmlportletfactory.portal.school.model", groupId, "ADD_SUBJECTS");

		List<subjects> tempResults = Collections.EMPTY_LIST;
		try {
			String orderByType = renderRequest.getParameter("orderByType");
			String orderByCol  = renderRequest.getParameter("orderByCol");
			OrderByComparator comparator = SubjectsComparator.getSubjectsOrderByComparator(orderByCol,orderByType);
			MultiVMPoolUtil.clear();

			String subjectsFilter = ParamUtil.getString(renderRequest, "subjectsFilter");
			if (subjectsFilter.equalsIgnoreCase("")) {
				tempResults = subjectsLocalServiceUtil.findAllInGroup(groupId, comparator);
			} else {
				DynamicQuery query = DynamicQueryFactoryUtil.forClass(subjects.class)
				.add(
					PropertyFactoryUtil.forName("subjectName").like("%"+ParamUtil.getString(renderRequest, "subjectsFilter")+"%")
				);
				tempResults = subjectsLocalServiceUtil.dynamicQuery(query, -1, -1, comparator);
			}
		
		} catch (Exception e) {
			_log.debug(e);
		}
		renderRequest.setAttribute("highlightRowWithKey", renderRequest.getParameter("highlightRowWithKey"));
		renderRequest.setAttribute("containerStart", renderRequest.getParameter("containerStart"));
		renderRequest.setAttribute("containerEnd", renderRequest.getParameter("containerEnd"));
		renderRequest.setAttribute("tempResults", tempResults);
		renderRequest.setAttribute("hasAddPermission", hasAddPermission);

		PortletURL addSubjectsURL = renderResponse.createActionURL();
		addSubjectsURL.setParameter("javax.portlet.action", "newSubjects");
		renderRequest.setAttribute("addSubjectsURL", addSubjectsURL.toString());

		PortletURL subjectsFilterURL = renderResponse.createRenderURL();
		subjectsFilterURL.setParameter("javax.portlet.action", "doView");
		renderRequest.setAttribute("subjectsFilterURL", subjectsFilterURL.toString());

		include(viewJSP, renderRequest, renderResponse);
	}

	public void showViewEditSubjects(RenderRequest renderRequest, RenderResponse renderResponse) throws Exception {

		SimpleDateFormat formatDia    = new SimpleDateFormat("dd");
		SimpleDateFormat formatMes    = new SimpleDateFormat("MM");
		SimpleDateFormat formatAno    = new SimpleDateFormat("yyyy");
		SimpleDateFormat formatHora   = new SimpleDateFormat("HH");
		SimpleDateFormat formatMinuto = new SimpleDateFormat("mm");

		PortletURL editSubjectsURL = renderResponse.createActionURL();
		String editType = (String) renderRequest.getParameter("editType");
		if (editType.equalsIgnoreCase("edit")) {
			editSubjectsURL.setParameter("javax.portlet.action", "updateSubjects");
			long subjectId = Long.parseLong(renderRequest.getParameter("subjectId"));
			subjects subjects = subjectsLocalServiceUtil.getsubjects(subjectId);
			String subjectName = subjects.getSubjectName()+"";
			renderRequest.setAttribute("subjectName", subjectName);
            renderRequest.setAttribute("subjects", subjects);
		} else {
			editSubjectsURL.setParameter("javax.portlet.action", "addSubjects");
			subjects errorSubjects = (subjects) renderRequest.getAttribute("errorSubjects");
			if (errorSubjects != null) {
				if (editType.equalsIgnoreCase("update")) {
					editSubjectsURL.setParameter("javax.portlet.action", "updateSubjects");
                }
				renderRequest.setAttribute("subjects", errorSubjects);
			} else {
				subjectsImpl blankSubjects = new subjectsImpl();
				blankSubjects.setSubjectId(0);
				blankSubjects.setSubjectName("");
				renderRequest.setAttribute("subjects", blankSubjects);
			}

		}

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		renderRequest.setAttribute("editSubjectsURL", editSubjectsURL.toString());

		include(editsubjectsJSP, renderRequest, renderResponse);
	}

	private String dateToJsp(ActionRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(RenderRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(PortletPreferences prefs, Date date) {
		SimpleDateFormat format = new SimpleDateFormat(prefs.getValue("subjects-date-format", "yyyy/MM/dd"));
		String stringDate = format.format(date);
		return stringDate;
	}
	private String dateTimeToJsp(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String stringDate = format.format(date);
		return stringDate;
	}

	public void showEditDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortletException, IOException {

		include(editJSP, renderRequest, renderResponse);
	}

	/* Portlet Actions */

	@ProcessAction(name = "newSubjects")
	public void newSubjects(ActionRequest request, ActionResponse response) {
		response.setRenderParameter("view", "editSubjects");
		response.setRenderParameter("editType", "add");
	}

	@ProcessAction(name = "addSubjects")
	public void addSubjects(ActionRequest request, ActionResponse response) throws Exception {
            subjects subjects = subjectsFromRequest(request);
            ArrayList<String> errors = SubjectsValidator.validateSubjects(subjects, request); 
            
            if (errors.isEmpty()) {
				subjectsLocalServiceUtil.addsubjects(subjects);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "subjects-added-successfully");
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
                response.setRenderParameter("view", "editSubjects");
                response.setRenderParameter("editType", "add");
                request.setAttribute("errorSubjects", subjects);
            }
	}

	@ProcessAction(name = "eventSubjects")
	public void eventSubjects(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		int containerStart = ParamUtil.getInteger(request, "containerStart");
		int containerEnd = ParamUtil.getInteger(request, "containerEnd");
		if (Validator.isNotNull(key)) {
            response.setRenderParameter("highlightRowWithKey", Long.toString(key));
            response.setRenderParameter("containerStart", Integer.toString(containerStart));
            response.setRenderParameter("containerEnd", Integer.toString(containerEnd));
		}
	}

	@ProcessAction(name = "editSubjects")
	public void editSubjects(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		if (Validator.isNotNull(key)) {
			response.setRenderParameter("subjectId", Long.toString(key));
			response.setRenderParameter("view", "editSubjects");
			response.setRenderParameter("editType", "edit");
		}
	}

	@ProcessAction(name = "deleteSubjects")
	public void deleteSubjects(ActionRequest request, ActionResponse response)throws Exception {
		long id = ParamUtil.getLong(request, "resourcePrimKey");
		if (Validator.isNotNull(id)) {
			subjects subjects = subjectsLocalServiceUtil.getsubjects(id);
			subjectsLocalServiceUtil.deletesubjects(subjects);
            MultiVMPoolUtil.clear();
			SessionMessages.add(request, "subjects-deleted-successfully");
		} else {
			SessionErrors.add(request, "subjects-error-deleting");
		}
	}

	@ProcessAction(name = "updateSubjects")
	public void updateSubjects(ActionRequest request, ActionResponse response) throws Exception {
            subjects subjects = subjectsFromRequest(request);
            ArrayList<String> errors = SubjectsValidator.validateSubjects(subjects, request); 
            
            if (errors.isEmpty()) {
                subjectsLocalServiceUtil.updatesubjects(subjects);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "subjects-updated-successfully");
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
				response.setRenderParameter("subjectId)",Long.toString(subjects.getPrimaryKey()));
				response.setRenderParameter("view", "editSubjects");
				response.setRenderParameter("editType", "update");
				request.setAttribute("errorSubjects", subjects);
            }
        }

	@ProcessAction(name = "setSubjectsPref")
	public void setSubjectsPref(ActionRequest request, ActionResponse response) throws Exception {

		String rowsPerPage = ParamUtil.getString(request, "subjects-rows-per-page");
		String dateFormat = ParamUtil.getString(request, "subjects-date-format");
		String datetimeFormat = ParamUtil.getString(request, "subjects-datetime-format");

		ArrayList<String> errors = new ArrayList();
		if (SubjectsValidator.validateEditSubjects(rowsPerPage, dateFormat, datetimeFormat, errors)) {
			response.setRenderParameter("subjects-rows-per-page", "");
			response.setRenderParameter("subjects-date-format", "");
			response.setRenderParameter("subjects-datetime-format", "");

			PortletPreferences prefs = request.getPreferences();
			prefs.setValue("subjects-rows-per-page", rowsPerPage);
			prefs.setValue("subjects-date-format", dateFormat);
			prefs.setValue("subjects-datetime-format", datetimeFormat);
			prefs.store();

			SessionMessages.add(request, "subjects-prefs-success");
		}
	}

	private subjects subjectsFromRequest(ActionRequest request) {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		subjectsImpl subjects = new subjectsImpl();
        try {
            subjects.setSubjectId(ParamUtil.getLong(request, "subjectId"));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
		subjects.setSubjectName(ParamUtil.getString(request, "subjectName"));
		try {
		    subjects.setPrimaryKey(ParamUtil.getLong(request,"resourcePrimKey"));
		} catch (NumberFormatException nfe) {
			//Controled en Validator
        }
		subjects.setCompanyId(themeDisplay.getCompanyId());
		subjects.setGroupId(themeDisplay.getScopeGroupId());
		subjects.setUserId(themeDisplay.getUserId());
		return subjects;
	}

	protected String editsubjectsJSP;
	protected String editJSP;
	protected String helpJSP;
	protected String viewJSP;

	private static Log _log = LogFactoryUtil.getLog(SubjectsPortlet.class);

}

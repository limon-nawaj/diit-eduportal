/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school;
 
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletURL;
import javax.portlet.ProcessAction;
import javax.portlet.ProcessEvent;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.portlet.PortletFileUpload;
import org.apache.commons.beanutils.BeanComparator;

import org.xmlportletfactory.portal.school.model.comments;
import org.xmlportletfactory.portal.school.model.impl.commentsImpl;
import org.xmlportletfactory.portal.school.service.commentsLocalServiceUtil;


import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class Comments
 */
public class CommentsPortlet extends MVCPortlet {



	public void init() throws PortletException {

		// Edit Mode Pages
		editJSP = getInitParameter("edit-jsp");

		// Help Mode Pages
		helpJSP = getInitParameter("help-jsp");

		// View Mode Pages
		viewJSP = getInitParameter("view-jsp");

		// View Mode Edit comments
		editcommentsJSP = getInitParameter("edit-comments-jsp");
	}

	protected void include(String path, RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		PortletRequestDispatcher portletRequestDispatcher = getPortletContext()
				.getRequestDispatcher(path);

		if (portletRequestDispatcher == null) {
			// do nothing
			// _log.error(path + " is not a valid include");
		} else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		String jsp = (String) renderRequest.getParameter("view");
		if (jsp == null || jsp.equals("")) {
			showViewDefault(renderRequest, renderResponse);
		} else if (jsp.equalsIgnoreCase("editComments")) {
			try {
				showViewEditComments(renderRequest, renderResponse);
			} catch (Exception ex) {
				_log.debug(ex);
				try {
					showViewDefault(renderRequest, renderResponse);
				} catch (Exception ex1) {
					_log.debug(ex1);
				}
			}
		}
	}

	public void doEdit(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		showEditDefault(renderRequest, renderResponse);
	}

	public void doHelp(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		include(helpJSP, renderRequest, renderResponse);
	}

	@SuppressWarnings("unchecked")
	public void showViewDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		long groupId = themeDisplay.getScopeGroupId();

		PermissionChecker permissionChecker = themeDisplay
				.getPermissionChecker();

		boolean hasAddPermission = permissionChecker.hasPermission(groupId,
				"org.xmlportletfactory.portal.school.model", groupId, "ADD_COMMENTS");

		List<comments> tempResults = Collections.EMPTY_LIST;
		long studentId = 0;
		String studentIdStr = renderRequest.getPortletSession().getAttribute("studentId")+ "";
		if (!studentIdStr.trim().equalsIgnoreCase("")){
			try {
				studentId = Long.parseLong(studentIdStr);
			} catch (Exception e) {
				studentId = 0;
			}
		}
		try {
			String orderByType = renderRequest.getParameter("orderByType");
			String orderByCol  = renderRequest.getParameter("orderByCol");
			OrderByComparator comparator = CommentsComparator.getCommentsOrderByComparator(orderByCol,orderByType);
			tempResults = commentsLocalServiceUtil.findAllInstudentIdGroup(studentId, groupId,comparator);
		} catch (Exception e) {
			_log.debug(e);
		}
        if (studentId == 0) {
            hasAddPermission = false;
        } else {
            renderRequest.getPortletSession().setAttribute("COMMENTSstudentId", studentId);
        }
		renderRequest.setAttribute("highlightRowWithKey", renderRequest.getParameter("highlightRowWithKey"));
		renderRequest.setAttribute("containerStart", renderRequest.getParameter("containerStart"));
		renderRequest.setAttribute("containerEnd", renderRequest.getParameter("containerEnd"));
		renderRequest.setAttribute("tempResults", tempResults);
		renderRequest.setAttribute("hasAddPermission", hasAddPermission);

		PortletURL addCommentsURL = renderResponse.createActionURL();
		addCommentsURL.setParameter("javax.portlet.action", "newComments");
		renderRequest.setAttribute("addCommentsURL", addCommentsURL.toString());

		PortletURL commentsFilterURL = renderResponse.createRenderURL();
		commentsFilterURL.setParameter("javax.portlet.action", "doView");
		renderRequest.setAttribute("commentsFilterURL", commentsFilterURL.toString());

		include(viewJSP, renderRequest, renderResponse);
	}

	public void showViewEditComments(RenderRequest renderRequest, RenderResponse renderResponse) throws Exception {

		SimpleDateFormat formatDia    = new SimpleDateFormat("dd");
		SimpleDateFormat formatMes    = new SimpleDateFormat("MM");
		SimpleDateFormat formatAno    = new SimpleDateFormat("yyyy");
		SimpleDateFormat formatHora   = new SimpleDateFormat("HH");
		SimpleDateFormat formatMinuto = new SimpleDateFormat("mm");

		PortletURL editCommentsURL = renderResponse.createActionURL();
		String editType = (String) renderRequest.getParameter("editType");
		if (editType.equalsIgnoreCase("edit")) {
			editCommentsURL.setParameter("javax.portlet.action", "updateComments");
			long commentId = Long.parseLong(renderRequest.getParameter("commentId"));
			comments comments = commentsLocalServiceUtil.getcomments(commentId);
			String studentId = comments.getStudentId()+"";
			renderRequest.setAttribute("studentId", studentId);
			String comment = comments.getComment()+"";
			renderRequest.setAttribute("comment", comment);
            renderRequest.setAttribute("comments", comments);
		} else {
			editCommentsURL.setParameter("javax.portlet.action", "addComments");
			comments errorComments = (comments) renderRequest.getAttribute("errorComments");
			if (errorComments != null) {
				if (editType.equalsIgnoreCase("update")) {
					editCommentsURL.setParameter("javax.portlet.action", "updateComments");
                }
				renderRequest.setAttribute("comments", errorComments);
			} else {
				commentsImpl blankComments = new commentsImpl();
				blankComments.setCommentId(0);
				blankComments.setStudentId((Long) renderRequest.getPortletSession().getAttribute("COMMENTSstudentId"));
				blankComments.setComment("");
                String studentIdStr = (String) renderRequest.getPortletSession().getAttribute("claseId");
				renderRequest.setAttribute("studentId", studentIdStr);
				renderRequest.setAttribute("comments", blankComments);
			}

		}

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		renderRequest.setAttribute("editCommentsURL", editCommentsURL.toString());

		include(editcommentsJSP, renderRequest, renderResponse);
	}

	private String dateToJsp(ActionRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(RenderRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(PortletPreferences prefs, Date date) {
		SimpleDateFormat format = new SimpleDateFormat(prefs.getValue("comments-date-format", "yyyy/MM/dd"));
		String stringDate = format.format(date);
		return stringDate;
	}
	private String dateTimeToJsp(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String stringDate = format.format(date);
		return stringDate;
	}

	public void showEditDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortletException, IOException {

		include(editJSP, renderRequest, renderResponse);
	}

	/* Portlet Actions */

	@ProcessAction(name = "newComments")
	public void newComments(ActionRequest request, ActionResponse response) {
		response.setRenderParameter("view", "editComments");
		response.setRenderParameter("editType", "add");
	}

	@ProcessAction(name = "addComments")
	public void addComments(ActionRequest request, ActionResponse response) throws Exception {
            comments comments = commentsFromRequest(request);
            ArrayList<String> errors = CommentsValidator.validateComments(comments, request); 
            
            if (errors.isEmpty()) {
				commentsLocalServiceUtil.addcomments(comments);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "comments-added-successfully");
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
                response.setRenderParameter("view", "editComments");
                response.setRenderParameter("editType", "add");
                request.setAttribute("errorComments", comments);
            }
	}

	@ProcessAction(name = "eventComments")
	public void eventComments(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		int containerStart = ParamUtil.getInteger(request, "containerStart");
		int containerEnd = ParamUtil.getInteger(request, "containerEnd");
		if (Validator.isNotNull(key)) {
            response.setRenderParameter("highlightRowWithKey", Long.toString(key));
            response.setRenderParameter("containerStart", Integer.toString(containerStart));
            response.setRenderParameter("containerEnd", Integer.toString(containerEnd));
		}
	}

	@ProcessAction(name = "editComments")
	public void editComments(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		if (Validator.isNotNull(key)) {
			response.setRenderParameter("commentId", Long.toString(key));
			response.setRenderParameter("view", "editComments");
			response.setRenderParameter("editType", "edit");
		}
	}

	@ProcessAction(name = "deleteComments")
	public void deleteComments(ActionRequest request, ActionResponse response)throws Exception {
		long id = ParamUtil.getLong(request, "resourcePrimKey");
		if (Validator.isNotNull(id)) {
			comments comments = commentsLocalServiceUtil.getcomments(id);
			commentsLocalServiceUtil.deletecomments(comments);
            MultiVMPoolUtil.clear();
			SessionMessages.add(request, "comments-deleted-successfully");
		} else {
			SessionErrors.add(request, "comments-error-deleting");
		}
	}

	@ProcessAction(name = "updateComments")
	public void updateComments(ActionRequest request, ActionResponse response) throws Exception {
            comments comments = commentsFromRequest(request);
            ArrayList<String> errors = CommentsValidator.validateComments(comments, request); 
            
            if (errors.isEmpty()) {
                commentsLocalServiceUtil.updatecomments(comments);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "comments-updated-successfully");
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
				response.setRenderParameter("commentId)",Long.toString(comments.getPrimaryKey()));
				response.setRenderParameter("view", "editComments");
				response.setRenderParameter("editType", "update");
				request.setAttribute("errorComments", comments);
            }
        }

	@ProcessAction(name = "setCommentsPref")
	public void setCommentsPref(ActionRequest request, ActionResponse response) throws Exception {

		String rowsPerPage = ParamUtil.getString(request, "comments-rows-per-page");
		String dateFormat = ParamUtil.getString(request, "comments-date-format");
		String datetimeFormat = ParamUtil.getString(request, "comments-datetime-format");

		ArrayList<String> errors = new ArrayList();
		if (CommentsValidator.validateEditComments(rowsPerPage, dateFormat, datetimeFormat, errors)) {
			response.setRenderParameter("comments-rows-per-page", "");
			response.setRenderParameter("comments-date-format", "");
			response.setRenderParameter("comments-datetime-format", "");

			PortletPreferences prefs = request.getPreferences();
			prefs.setValue("comments-rows-per-page", rowsPerPage);
			prefs.setValue("comments-date-format", dateFormat);
			prefs.setValue("comments-datetime-format", datetimeFormat);
			prefs.store();

			SessionMessages.add(request, "comments-prefs-success");
		}
	}

	private comments commentsFromRequest(ActionRequest request) {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		commentsImpl comments = new commentsImpl();
        try {
            comments.setCommentId(ParamUtil.getLong(request, "commentId"));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
        try {
            comments.setStudentId(ParamUtil.getLong(request, "studentId"));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
		comments.setComment(ParamUtil.getString(request, "comment"));
		try {
		    comments.setPrimaryKey(ParamUtil.getLong(request,"resourcePrimKey"));
		} catch (NumberFormatException nfe) {
			//Controled en Validator
        }
		comments.setCompanyId(themeDisplay.getCompanyId());
		comments.setGroupId(themeDisplay.getScopeGroupId());
		comments.setUserId(themeDisplay.getUserId());
		return comments;
	}

	@ProcessEvent(qname="{http://liferay.com/events}Students.studentId")
	public void reciveEvent(EventRequest request, EventResponse response) {
		Event event = request.getEvent();
		String studentId = (String)event.getValue();
		request.getPortletSession().setAttribute("studentId",studentId);
		response.setRenderParameter("studentId", studentId);
	}
	protected String editcommentsJSP;
	protected String editJSP;
	protected String helpJSP;
	protected String viewJSP;

	private static Log _log = LogFactoryUtil.getLog(CommentsPortlet.class);

}

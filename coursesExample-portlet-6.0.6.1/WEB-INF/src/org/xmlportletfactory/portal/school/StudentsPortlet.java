/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school;
 
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletURL;
import javax.portlet.ProcessAction;
import javax.portlet.ProcessEvent;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.portlet.PortletFileUpload;
import org.apache.commons.beanutils.BeanComparator;

import org.xmlportletfactory.portal.school.model.students;
import org.xmlportletfactory.portal.school.model.impl.studentsImpl;
import org.xmlportletfactory.portal.school.service.studentsLocalServiceUtil;
import org.xmlportletfactory.portal.school.service.commentsLocalServiceUtil;

import org.xmlportletfactory.portal.school.model.classrooms;
import org.xmlportletfactory.portal.school.service.classroomsLocalServiceUtil;

import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.imagegallery.model.IGFolder;
import com.liferay.portlet.imagegallery.model.IGImage;
import com.liferay.portlet.imagegallery.service.IGFolderLocalServiceUtil;
import com.liferay.portlet.imagegallery.service.IGImageLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class Students
 */
public class StudentsPortlet extends MVCPortlet {


	private StudentsUpload uploadManager;

	public void init() throws PortletException {

		// Edit Mode Pages
		editJSP = getInitParameter("edit-jsp");

		// Help Mode Pages
		helpJSP = getInitParameter("help-jsp");

		// View Mode Pages
		viewJSP = getInitParameter("view-jsp");

		// View Mode Edit students
		editstudentsJSP = getInitParameter("edit-students-jsp");
	}

	protected void include(String path, RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		PortletRequestDispatcher portletRequestDispatcher = getPortletContext()
				.getRequestDispatcher(path);

		if (portletRequestDispatcher == null) {
			// do nothing
			// _log.error(path + " is not a valid include");
		} else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		String jsp = (String) renderRequest.getParameter("view");
		if (jsp == null || jsp.equals("")) {
			showViewDefault(renderRequest, renderResponse);
		} else if (jsp.equalsIgnoreCase("editStudents")) {
			try {
				showViewEditStudents(renderRequest, renderResponse);
			} catch (Exception ex) {
				_log.debug(ex);
				try {
					showViewDefault(renderRequest, renderResponse);
				} catch (Exception ex1) {
					_log.debug(ex1);
				}
			}
		}
	}

	public void doEdit(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		showEditDefault(renderRequest, renderResponse);
	}

	public void doHelp(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		include(helpJSP, renderRequest, renderResponse);
	}

	@SuppressWarnings("unchecked")
	public void showViewDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		long groupId = themeDisplay.getScopeGroupId();

		PermissionChecker permissionChecker = themeDisplay
				.getPermissionChecker();

		boolean hasAddPermission = permissionChecker.hasPermission(groupId,
				"org.xmlportletfactory.portal.school.model", groupId, "ADD_STUDENTS");

		List<students> tempResults = Collections.EMPTY_LIST;
		long courseId = 0;
		String courseIdStr = renderRequest.getPortletSession().getAttribute("courseId")+ "";
		if (!courseIdStr.trim().equalsIgnoreCase("")){
			try {
				courseId = Long.parseLong(courseIdStr);
			} catch (Exception e) {
				courseId = 0;
			}
		}
		try {
			String orderByType = renderRequest.getParameter("orderByType");
			String orderByCol  = renderRequest.getParameter("orderByCol");
			OrderByComparator comparator = StudentsComparator.getStudentsOrderByComparator(orderByCol,orderByType);
			tempResults = studentsLocalServiceUtil.findAllIncourseIdGroup(courseId, groupId,comparator);
		} catch (Exception e) {
			_log.debug(e);
		}
        if (courseId == 0) {
            hasAddPermission = false;
        } else {
            renderRequest.getPortletSession().setAttribute("STUDENTScourseId", courseId);
        }
		renderRequest.setAttribute("highlightRowWithKey", renderRequest.getParameter("highlightRowWithKey"));
		renderRequest.setAttribute("containerStart", renderRequest.getParameter("containerStart"));
		renderRequest.setAttribute("containerEnd", renderRequest.getParameter("containerEnd"));
		renderRequest.setAttribute("tempResults", tempResults);
		renderRequest.setAttribute("hasAddPermission", hasAddPermission);

		PortletURL addStudentsURL = renderResponse.createActionURL();
		addStudentsURL.setParameter("javax.portlet.action", "newStudents");
		renderRequest.setAttribute("addStudentsURL", addStudentsURL.toString());

		PortletURL studentsFilterURL = renderResponse.createRenderURL();
		studentsFilterURL.setParameter("javax.portlet.action", "doView");
		renderRequest.setAttribute("studentsFilterURL", studentsFilterURL.toString());

		include(viewJSP, renderRequest, renderResponse);
	}

	public void showViewEditStudents(RenderRequest renderRequest, RenderResponse renderResponse) throws Exception {

		SimpleDateFormat formatDia    = new SimpleDateFormat("dd");
		SimpleDateFormat formatMes    = new SimpleDateFormat("MM");
		SimpleDateFormat formatAno    = new SimpleDateFormat("yyyy");
		SimpleDateFormat formatHora   = new SimpleDateFormat("HH");
		SimpleDateFormat formatMinuto = new SimpleDateFormat("mm");

		PortletURL editStudentsURL = renderResponse.createActionURL();
		String editType = (String) renderRequest.getParameter("editType");
		if (editType.equalsIgnoreCase("edit")) {
			editStudentsURL.setParameter("javax.portlet.action", "updateStudents");
			long studentId = Long.parseLong(renderRequest.getParameter("studentId"));
			students students = studentsLocalServiceUtil.getstudents(studentId);
			String courseId = students.getCourseId()+"";
			renderRequest.setAttribute("courseId", courseId);
			String studentName = students.getStudentName()+"";
			renderRequest.setAttribute("studentName", studentName);
			String classroomId = students.getClassroomId()+"";
			renderRequest.setAttribute("classroomId", classroomId);
			String studentPhoto = students.getStudentPhoto()+"";
			renderRequest.setAttribute("studentPhoto", studentPhoto);
		    String folderIGId = students.getFolderIGId()+"";
			renderRequest.setAttribute("folderIGId", folderIGId);
            renderRequest.setAttribute("students", students);
		} else {
			editStudentsURL.setParameter("javax.portlet.action", "addStudents");
			students errorStudents = (students) renderRequest.getAttribute("errorStudents");
			if (errorStudents != null) {
				if (editType.equalsIgnoreCase("update")) {
					editStudentsURL.setParameter("javax.portlet.action", "updateStudents");
                }
				renderRequest.setAttribute("students", errorStudents);
	            String studentPhoto = errorStudents.getStudentPhoto()+"";
				renderRequest.setAttribute("studentPhoto",studentPhoto);
	            String folderIGId = errorStudents.getStudentPhoto()+"";
				renderRequest.setAttribute("folderIGId",folderIGId);
			} else {
				studentsImpl blankStudents = new studentsImpl();
				blankStudents.setStudentId(0);
				blankStudents.setCourseId((Long) renderRequest.getPortletSession().getAttribute("STUDENTScourseId"));
				blankStudents.setStudentName("");
				blankStudents.setClassroomId(0);
                String courseIdStr = (String) renderRequest.getPortletSession().getAttribute("claseId");
				renderRequest.setAttribute("courseId", courseIdStr);
				renderRequest.setAttribute("students", blankStudents);
			}

		}

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		OrderByComparator Classrooms_comparator1 = ClassroomsComparator.getClassroomsOrderByComparator("classroomName", "ASC");
		MultiVMPoolUtil.clear();
		List<classrooms> classroomIdList = classroomsLocalServiceUtil.findAllInGroup(themeDisplay.getScopeGroupId(),Classrooms_comparator1);
		renderRequest.setAttribute("classroomIdList", classroomIdList);


		renderRequest.setAttribute("editStudentsURL", editStudentsURL.toString());

		include(editstudentsJSP, renderRequest, renderResponse);
	}

	private String dateToJsp(ActionRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(RenderRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(PortletPreferences prefs, Date date) {
		SimpleDateFormat format = new SimpleDateFormat(prefs.getValue("students-date-format", "yyyy/MM/dd"));
		String stringDate = format.format(date);
		return stringDate;
	}
	private String dateTimeToJsp(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String stringDate = format.format(date);
		return stringDate;
	}

	public void showEditDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortletException, IOException {

		include(editJSP, renderRequest, renderResponse);
	}

	/* Portlet Actions */

	@ProcessAction(name = "newStudents")
	public void newStudents(ActionRequest request, ActionResponse response) {
		response.setRenderParameter("view", "editStudents");
		response.setRenderParameter("editType", "add");
		response.setRenderParameter("studentId", "0");
        QName qNameStudents = new QName("http://liferay.com/events", "Students.studentId");
        response.setEvent(qNameStudents, ParamUtil.getString(request, "studentId"));
	}

	@ProcessAction(name = "addStudents")
	public void addStudents(ActionRequest request, ActionResponse response) throws Exception {
            boolean isMultipart = PortletFileUpload.isMultipartContent(request);
            if (isMultipart) {
            	uploadManager = new StudentsUpload();
				request = extractFields(request,false);
            }
            students students = studentsFromRequest(request);
            ArrayList<String> errors = StudentsValidator.validateStudents(students, request); 
            
            if (errors.isEmpty()) {
	            students = uploadManager.uploadFiles(request,students);
				studentsLocalServiceUtil.addstudents(students);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "students-added-successfully");
		        response.setRenderParameter("studentId", "0");
                QName qNameStudents = new QName("http://liferay.com/events", "Students.studentId");
                response.setEvent(qNameStudents, ParamUtil.getString(request, "studentId"));
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
                response.setRenderParameter("view", "editStudents");
                response.setRenderParameter("editType", "add");
                request.setAttribute("errorStudents", students);
            }
	}

	@ProcessAction(name = "eventStudents")
	public void eventStudents(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		int containerStart = ParamUtil.getInteger(request, "containerStart");
		int containerEnd = ParamUtil.getInteger(request, "containerEnd");
		if (Validator.isNotNull(key)) {
            response.setRenderParameter("highlightRowWithKey", Long.toString(key));
            response.setRenderParameter("containerStart", Integer.toString(containerStart));
            response.setRenderParameter("containerEnd", Integer.toString(containerEnd));
            QName qNameStudents = new QName("http://liferay.com/events", "Students.studentId");
            response.setEvent(qNameStudents, ParamUtil.getString(request, "studentId"));
		}
	}

	@ProcessAction(name = "editStudents")
	public void editStudents(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		if (Validator.isNotNull(key)) {
			response.setRenderParameter("studentId", Long.toString(key));
			response.setRenderParameter("view", "editStudents");
			response.setRenderParameter("editType", "edit");
            QName qNameStudents = new QName("http://liferay.com/events", "Students.studentId");
            response.setEvent(qNameStudents, ParamUtil.getString(request, "studentId"));
		}
	}

	@ProcessAction(name = "deleteStudents")
	public void deleteStudents(ActionRequest request, ActionResponse response)throws Exception {
		long id = ParamUtil.getLong(request, "resourcePrimKey");
		// check there no is detail rows before deleting in master table
		boolean noDetailRows = true;
		if(!commentsLocalServiceUtil.findAllInstudentId(id).isEmpty()) {
			noDetailRows = false;
		}
		if (noDetailRows) {
		if (Validator.isNotNull(id)) {
			students students = studentsLocalServiceUtil.getstudents(id);
			studentsLocalServiceUtil.deletestudents(students);
            MultiVMPoolUtil.clear();
			SessionMessages.add(request, "students-deleted-successfully");
            response.setRenderParameter("studentId", "0");
            QName qNameStudents = new QName("http://liferay.com/events", "Students.${detailfile.getConnectionFieldName()}");
            response.setEvent(qNameStudents, ParamUtil.getString(request, "${detailfile.getConnectionFieldName()}"));
		} else {
			SessionErrors.add(request, "students-error-deleting");
		}
		} else {
			SessionErrors.add(request, "dependent-rows-exist-error-deleting");
		}
	}

	@ProcessAction(name = "updateStudents")
	public void updateStudents(ActionRequest request, ActionResponse response) throws Exception {
            boolean isMultipart = PortletFileUpload.isMultipartContent(request);
            if (isMultipart) {
				uploadManager = new StudentsUpload();
				request = extractFields(request,true);
            }
            students students = studentsFromRequest(request);
            ArrayList<String> errors = StudentsValidator.validateStudents(students, request); 
            
		    students = uploadManager.uploadFiles(request, students);
            if (errors.isEmpty()) {
                studentsLocalServiceUtil.updatestudents(students);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "students-updated-successfully");
		        response.setRenderParameter("studentId", "0");
                QName qNameStudents = new QName("http://liferay.com/events", "Students.studentId");
                response.setEvent(qNameStudents, ParamUtil.getString(request, "studentId"));
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
				response.setRenderParameter("studentId)",Long.toString(students.getPrimaryKey()));
				response.setRenderParameter("view", "editStudents");
				response.setRenderParameter("editType", "update");
				request.setAttribute("errorStudents", students);
            }
		    QName qNameStudents = new QName("http://liferay.com/events", "Students.studentId");
            response.setEvent(qNameStudents, ParamUtil.getString(request, "0"));
        }

	@ProcessAction(name = "setStudentsPref")
	public void setStudentsPref(ActionRequest request, ActionResponse response) throws Exception {

		String rowsPerPage = ParamUtil.getString(request, "students-rows-per-page");
		String dateFormat = ParamUtil.getString(request, "students-date-format");
		String datetimeFormat = ParamUtil.getString(request, "students-datetime-format");

		ArrayList<String> errors = new ArrayList();
		if (StudentsValidator.validateEditStudents(rowsPerPage, dateFormat, datetimeFormat, errors)) {
			response.setRenderParameter("students-rows-per-page", "");
			response.setRenderParameter("students-date-format", "");
			response.setRenderParameter("students-datetime-format", "");

			PortletPreferences prefs = request.getPreferences();
			prefs.setValue("students-rows-per-page", rowsPerPage);
			prefs.setValue("students-date-format", dateFormat);
			prefs.setValue("students-datetime-format", datetimeFormat);
			prefs.store();

			SessionMessages.add(request, "students-prefs-success");
		}
	}

	private students studentsFromRequest(ActionRequest request) {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		studentsImpl students = new studentsImpl();
        try {
		    students.setStudentId(Long.valueOf(request.getAttribute("studentId").toString()));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
        try {
		    students.setCourseId(Long.valueOf(request.getAttribute("courseId").toString()));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
		students.setStudentName(request.getAttribute("studentName").toString());
        try {
		    students.setClassroomId(Long.valueOf(request.getAttribute("classroomId").toString()));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
		try {
			students.setPrimaryKey(Long.valueOf(request.getAttribute("resourcePrimKey").toString()));
		} catch (NumberFormatException nfe) {
			//Controled en Validator
        }
		students.setCompanyId(themeDisplay.getCompanyId());
		students.setGroupId(themeDisplay.getScopeGroupId());
		students.setUserId(themeDisplay.getUserId());
		return students;
	}

	@ProcessEvent(qname="{http://liferay.com/events}Courses.courseId")
	public void reciveEvent(EventRequest request, EventResponse response) {
		Event event = request.getEvent();
		String courseId = (String)event.getValue();
		request.getPortletSession().setAttribute("courseId",courseId);
		response.setRenderParameter("courseId", courseId);
		QName qNameStudents = new QName("http://liferay.com/events", "Students.studentId");
		response.setEvent(qNameStudents, "0");
	}
	private ActionRequest extractFields(ActionRequest request,boolean edit) throws FileUploadException{

		FileItemFactory factory = new DiskFileItemFactory();
        PortletFileUpload uploadItems = new PortletFileUpload(factory);
        List <FileItem>allItems = uploadItems.parseRequest(request);
         //Separate formFields <-> fileItems
         for(FileItem item : allItems){
         	String formField = item.getFieldName();
         	if (item.isFormField()) {
         		//Non-file items
         		//Push all to request object
				if(formField.startsWith(StudentsUpload.HIDDEN)) {
					uploadManager.addHidden(formField,Long.parseLong(item.getString()));
				} else if (formField.endsWith(StudentsUpload.IMAGE_DELETE)) {
					int pos = formField.indexOf(StudentsUpload.IMAGE_DELETE);
					formField = formField.substring(0,pos-1);
					int pos2 = formField.lastIndexOf(StudentsUpload.SEPARATOR);
					formField = formField.substring(pos2+1);
					if(item.getString().equals("true")) uploadManager.addDeleted(formField);
				} else if (formField.endsWith(StudentsUpload.DOCUMENT_DELETE)) {
					int pos = formField.indexOf(StudentsUpload.DOCUMENT_DELETE);
					formField = formField.substring(0,pos-1);
					int pos2 = formField.lastIndexOf(StudentsUpload.SEPARATOR);
					formField = formField.substring(pos2+1);
					if(item.getString().equals("true")) uploadManager.addDeleted(formField);
				} else {
					int pos = formField.lastIndexOf(StudentsUpload.SEPARATOR);
					formField = formField.substring(pos+1);
					try {
					    request.setAttribute(formField,item.getString("UTF-8").trim());
					} catch (Exception e) {
					}
				}
         	} else {
         		uploadManager.add(item);
         	}
         }
		return request;
	}
	protected String editstudentsJSP;
	protected String editJSP;
	protected String helpJSP;
	protected String viewJSP;

	private static Log _log = LogFactoryUtil.getLog(StudentsPortlet.class);

}

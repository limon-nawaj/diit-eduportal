/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school;
 
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletURL;
import javax.portlet.ProcessAction;
import javax.portlet.ProcessEvent;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.portlet.PortletFileUpload;
import org.apache.commons.beanutils.BeanComparator;

import org.xmlportletfactory.portal.school.model.courses;
import org.xmlportletfactory.portal.school.model.impl.coursesImpl;
import org.xmlportletfactory.portal.school.service.coursesLocalServiceUtil;
import org.xmlportletfactory.portal.school.service.studentsLocalServiceUtil;
import org.xmlportletfactory.portal.school.service.holidaysLocalServiceUtil;
import org.xmlportletfactory.portal.school.service.courseSubjectsLocalServiceUtil;


import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class Courses
 */
public class CoursesPortlet extends MVCPortlet {



	public void init() throws PortletException {

		// Edit Mode Pages
		editJSP = getInitParameter("edit-jsp");

		// Help Mode Pages
		helpJSP = getInitParameter("help-jsp");

		// View Mode Pages
		viewJSP = getInitParameter("view-jsp");

		// View Mode Edit courses
		editcoursesJSP = getInitParameter("edit-courses-jsp");
	}

	protected void include(String path, RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		PortletRequestDispatcher portletRequestDispatcher = getPortletContext()
				.getRequestDispatcher(path);

		if (portletRequestDispatcher == null) {
			// do nothing
			// _log.error(path + " is not a valid include");
		} else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		String jsp = (String) renderRequest.getParameter("view");
		if (jsp == null || jsp.equals("")) {
			showViewDefault(renderRequest, renderResponse);
		} else if (jsp.equalsIgnoreCase("editCourses")) {
			try {
				showViewEditCourses(renderRequest, renderResponse);
			} catch (Exception ex) {
				_log.debug(ex);
				try {
					showViewDefault(renderRequest, renderResponse);
				} catch (Exception ex1) {
					_log.debug(ex1);
				}
			}
		}
	}

	public void doEdit(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		showEditDefault(renderRequest, renderResponse);
	}

	public void doHelp(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		include(helpJSP, renderRequest, renderResponse);
	}

	@SuppressWarnings("unchecked")
	public void showViewDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		long groupId = themeDisplay.getScopeGroupId();

		PermissionChecker permissionChecker = themeDisplay
				.getPermissionChecker();

		boolean hasAddPermission = permissionChecker.hasPermission(groupId,
				"org.xmlportletfactory.portal.school.model", groupId, "ADD_COURSES");

		List<courses> tempResults = Collections.EMPTY_LIST;
		try {
			String orderByType = renderRequest.getParameter("orderByType");
			String orderByCol  = renderRequest.getParameter("orderByCol");
			OrderByComparator comparator = CoursesComparator.getCoursesOrderByComparator(orderByCol,orderByType);
			MultiVMPoolUtil.clear();

			String coursesFilter = ParamUtil.getString(renderRequest, "coursesFilter");
			if (coursesFilter.equalsIgnoreCase("")) {
				tempResults = coursesLocalServiceUtil.findAllInGroup(groupId, comparator);
			} else {
				DynamicQuery query = DynamicQueryFactoryUtil.forClass(courses.class)
				.add(
					PropertyFactoryUtil.forName("courseName").like("%"+ParamUtil.getString(renderRequest, "coursesFilter")+"%")
				);
				tempResults = coursesLocalServiceUtil.dynamicQuery(query, -1, -1, comparator);
			}
		
		} catch (Exception e) {
			_log.debug(e);
		}
		renderRequest.setAttribute("highlightRowWithKey", renderRequest.getParameter("highlightRowWithKey"));
		renderRequest.setAttribute("containerStart", renderRequest.getParameter("containerStart"));
		renderRequest.setAttribute("containerEnd", renderRequest.getParameter("containerEnd"));
		renderRequest.setAttribute("tempResults", tempResults);
		renderRequest.setAttribute("hasAddPermission", hasAddPermission);

		PortletURL addCoursesURL = renderResponse.createActionURL();
		addCoursesURL.setParameter("javax.portlet.action", "newCourses");
		renderRequest.setAttribute("addCoursesURL", addCoursesURL.toString());

		PortletURL coursesFilterURL = renderResponse.createRenderURL();
		coursesFilterURL.setParameter("javax.portlet.action", "doView");
		renderRequest.setAttribute("coursesFilterURL", coursesFilterURL.toString());

		include(viewJSP, renderRequest, renderResponse);
	}

	public void showViewEditCourses(RenderRequest renderRequest, RenderResponse renderResponse) throws Exception {

		SimpleDateFormat formatDia    = new SimpleDateFormat("dd");
		SimpleDateFormat formatMes    = new SimpleDateFormat("MM");
		SimpleDateFormat formatAno    = new SimpleDateFormat("yyyy");
		SimpleDateFormat formatHora   = new SimpleDateFormat("HH");
		SimpleDateFormat formatMinuto = new SimpleDateFormat("mm");

		PortletURL editCoursesURL = renderResponse.createActionURL();
		String editType = (String) renderRequest.getParameter("editType");
		if (editType.equalsIgnoreCase("edit")) {
			editCoursesURL.setParameter("javax.portlet.action", "updateCourses");
			long courseId = Long.parseLong(renderRequest.getParameter("courseId"));
			courses courses = coursesLocalServiceUtil.getcourses(courseId);
			String courseName = courses.getCourseName()+"";
			renderRequest.setAttribute("courseName", courseName);
			String courseActive = courses.getCourseActive()+"";
			renderRequest.setAttribute("courseActive", courseActive);
            renderRequest.setAttribute("courses", courses);
		} else {
			editCoursesURL.setParameter("javax.portlet.action", "addCourses");
			courses errorCourses = (courses) renderRequest.getAttribute("errorCourses");
			if (errorCourses != null) {
				if (editType.equalsIgnoreCase("update")) {
					editCoursesURL.setParameter("javax.portlet.action", "updateCourses");
                }
				renderRequest.setAttribute("courses", errorCourses);
			} else {
				coursesImpl blankCourses = new coursesImpl();
				blankCourses.setCourseId(0);
				blankCourses.setCourseName("");
				blankCourses.setCourseActive(true);
				renderRequest.setAttribute("courses", blankCourses);
			}

		}

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		renderRequest.setAttribute("editCoursesURL", editCoursesURL.toString());

		include(editcoursesJSP, renderRequest, renderResponse);
	}

	private String dateToJsp(ActionRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(RenderRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(PortletPreferences prefs, Date date) {
		SimpleDateFormat format = new SimpleDateFormat(prefs.getValue("courses-date-format", "yyyy/MM/dd"));
		String stringDate = format.format(date);
		return stringDate;
	}
	private String dateTimeToJsp(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String stringDate = format.format(date);
		return stringDate;
	}

	public void showEditDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortletException, IOException {

		include(editJSP, renderRequest, renderResponse);
	}

	/* Portlet Actions */

	@ProcessAction(name = "newCourses")
	public void newCourses(ActionRequest request, ActionResponse response) {
		response.setRenderParameter("view", "editCourses");
		response.setRenderParameter("editType", "add");
		response.setRenderParameter("courseId", "0");
        QName qNameCourses = new QName("http://liferay.com/events", "Courses.courseId");
        response.setEvent(qNameCourses, ParamUtil.getString(request, "courseId"));
	}

	@ProcessAction(name = "addCourses")
	public void addCourses(ActionRequest request, ActionResponse response) throws Exception {
            courses courses = coursesFromRequest(request);
            ArrayList<String> errors = CoursesValidator.validateCourses(courses, request); 
            
            if (errors.isEmpty()) {
				coursesLocalServiceUtil.addcourses(courses);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "courses-added-successfully");
		        response.setRenderParameter("courseId", "0");
                QName qNameCourses = new QName("http://liferay.com/events", "Courses.courseId");
                response.setEvent(qNameCourses, ParamUtil.getString(request, "courseId"));
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
                response.setRenderParameter("view", "editCourses");
                response.setRenderParameter("editType", "add");
                request.setAttribute("errorCourses", courses);
            }
	}

	@ProcessAction(name = "eventCourses")
	public void eventCourses(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		int containerStart = ParamUtil.getInteger(request, "containerStart");
		int containerEnd = ParamUtil.getInteger(request, "containerEnd");
		if (Validator.isNotNull(key)) {
            response.setRenderParameter("highlightRowWithKey", Long.toString(key));
            response.setRenderParameter("containerStart", Integer.toString(containerStart));
            response.setRenderParameter("containerEnd", Integer.toString(containerEnd));
            QName qNameCourses = new QName("http://liferay.com/events", "Courses.courseId");
            response.setEvent(qNameCourses, ParamUtil.getString(request, "courseId"));
		}
	}

	@ProcessAction(name = "editCourses")
	public void editCourses(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		if (Validator.isNotNull(key)) {
			response.setRenderParameter("courseId", Long.toString(key));
			response.setRenderParameter("view", "editCourses");
			response.setRenderParameter("editType", "edit");
            QName qNameCourses = new QName("http://liferay.com/events", "Courses.courseId");
            response.setEvent(qNameCourses, ParamUtil.getString(request, "courseId"));
		}
	}

	@ProcessAction(name = "deleteCourses")
	public void deleteCourses(ActionRequest request, ActionResponse response)throws Exception {
		long id = ParamUtil.getLong(request, "resourcePrimKey");
		// check there no is detail rows before deleting in master table
		boolean noDetailRows = true;
		if(!studentsLocalServiceUtil.findAllIncourseId(id).isEmpty()) {
			noDetailRows = false;
		}
		if(!holidaysLocalServiceUtil.findAllIncourseId(id).isEmpty()) {
			noDetailRows = false;
		}
		if(!courseSubjectsLocalServiceUtil.findAllIncourseId(id).isEmpty()) {
			noDetailRows = false;
		}
		if (noDetailRows) {
		if (Validator.isNotNull(id)) {
			courses courses = coursesLocalServiceUtil.getcourses(id);
			coursesLocalServiceUtil.deletecourses(courses);
            MultiVMPoolUtil.clear();
			SessionMessages.add(request, "courses-deleted-successfully");
            response.setRenderParameter("courseId", "0");
            QName qNameCourses = new QName("http://liferay.com/events", "Courses.${detailfile.getConnectionFieldName()}");
            response.setEvent(qNameCourses, ParamUtil.getString(request, "${detailfile.getConnectionFieldName()}"));
		} else {
			SessionErrors.add(request, "courses-error-deleting");
		}
		} else {
			SessionErrors.add(request, "dependent-rows-exist-error-deleting");
		}
	}

	@ProcessAction(name = "updateCourses")
	public void updateCourses(ActionRequest request, ActionResponse response) throws Exception {
            courses courses = coursesFromRequest(request);
            ArrayList<String> errors = CoursesValidator.validateCourses(courses, request); 
            
            if (errors.isEmpty()) {
                coursesLocalServiceUtil.updatecourses(courses);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "courses-updated-successfully");
		        response.setRenderParameter("courseId", "0");
                QName qNameCourses = new QName("http://liferay.com/events", "Courses.courseId");
                response.setEvent(qNameCourses, ParamUtil.getString(request, "courseId"));
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
				response.setRenderParameter("courseId)",Long.toString(courses.getPrimaryKey()));
				response.setRenderParameter("view", "editCourses");
				response.setRenderParameter("editType", "update");
				request.setAttribute("errorCourses", courses);
            }
		    QName qNameCourses = new QName("http://liferay.com/events", "Courses.courseId");
            response.setEvent(qNameCourses, ParamUtil.getString(request, "0"));
        }

	@ProcessAction(name = "setCoursesPref")
	public void setCoursesPref(ActionRequest request, ActionResponse response) throws Exception {

		String rowsPerPage = ParamUtil.getString(request, "courses-rows-per-page");
		String dateFormat = ParamUtil.getString(request, "courses-date-format");
		String datetimeFormat = ParamUtil.getString(request, "courses-datetime-format");

		ArrayList<String> errors = new ArrayList();
		if (CoursesValidator.validateEditCourses(rowsPerPage, dateFormat, datetimeFormat, errors)) {
			response.setRenderParameter("courses-rows-per-page", "");
			response.setRenderParameter("courses-date-format", "");
			response.setRenderParameter("courses-datetime-format", "");

			PortletPreferences prefs = request.getPreferences();
			prefs.setValue("courses-rows-per-page", rowsPerPage);
			prefs.setValue("courses-date-format", dateFormat);
			prefs.setValue("courses-datetime-format", datetimeFormat);
			prefs.store();

			SessionMessages.add(request, "courses-prefs-success");
		}
	}

	private courses coursesFromRequest(ActionRequest request) {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		coursesImpl courses = new coursesImpl();
        try {
            courses.setCourseId(ParamUtil.getLong(request, "courseId"));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
		courses.setCourseName(ParamUtil.getString(request, "courseName"));
        courses.setCourseActive(ParamUtil.getBoolean(request, "courseActive"));
		try {
		    courses.setPrimaryKey(ParamUtil.getLong(request,"resourcePrimKey"));
		} catch (NumberFormatException nfe) {
			//Controled en Validator
        }
		courses.setCompanyId(themeDisplay.getCompanyId());
		courses.setGroupId(themeDisplay.getScopeGroupId());
		courses.setUserId(themeDisplay.getUserId());
		return courses;
	}

	protected String editcoursesJSP;
	protected String editJSP;
	protected String helpJSP;
	protected String viewJSP;

	private static Log _log = LogFactoryUtil.getLog(CoursesPortlet.class);

}

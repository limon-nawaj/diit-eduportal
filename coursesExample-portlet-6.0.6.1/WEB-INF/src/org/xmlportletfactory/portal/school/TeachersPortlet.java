/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
 
 package org.xmlportletfactory.portal.school;
 
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletURL;
import javax.portlet.ProcessAction;
import javax.portlet.ProcessEvent;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.portlet.PortletFileUpload;
import org.apache.commons.beanutils.BeanComparator;

import org.xmlportletfactory.portal.school.model.teachers;
import org.xmlportletfactory.portal.school.model.impl.teachersImpl;
import org.xmlportletfactory.portal.school.service.teachersLocalServiceUtil;


import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class Teachers
 */
public class TeachersPortlet extends MVCPortlet {



	public void init() throws PortletException {

		// Edit Mode Pages
		editJSP = getInitParameter("edit-jsp");

		// Help Mode Pages
		helpJSP = getInitParameter("help-jsp");

		// View Mode Pages
		viewJSP = getInitParameter("view-jsp");

		// View Mode Edit teachers
		editteachersJSP = getInitParameter("edit-teachers-jsp");
	}

	protected void include(String path, RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		PortletRequestDispatcher portletRequestDispatcher = getPortletContext()
				.getRequestDispatcher(path);

		if (portletRequestDispatcher == null) {
			// do nothing
			// _log.error(path + " is not a valid include");
		} else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		String jsp = (String) renderRequest.getParameter("view");
		if (jsp == null || jsp.equals("")) {
			showViewDefault(renderRequest, renderResponse);
		} else if (jsp.equalsIgnoreCase("editTeachers")) {
			try {
				showViewEditTeachers(renderRequest, renderResponse);
			} catch (Exception ex) {
				_log.debug(ex);
				try {
					showViewDefault(renderRequest, renderResponse);
				} catch (Exception ex1) {
					_log.debug(ex1);
				}
			}
		}
	}

	public void doEdit(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		showEditDefault(renderRequest, renderResponse);
	}

	public void doHelp(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		include(helpJSP, renderRequest, renderResponse);
	}

	@SuppressWarnings("unchecked")
	public void showViewDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		long groupId = themeDisplay.getScopeGroupId();

		PermissionChecker permissionChecker = themeDisplay
				.getPermissionChecker();

		boolean hasAddPermission = permissionChecker.hasPermission(groupId,
				"org.xmlportletfactory.portal.school.model", groupId, "ADD_TEACHERS");

		List<teachers> tempResults = Collections.EMPTY_LIST;
		try {
			String orderByType = renderRequest.getParameter("orderByType");
			String orderByCol  = renderRequest.getParameter("orderByCol");
			OrderByComparator comparator = TeachersComparator.getTeachersOrderByComparator(orderByCol,orderByType);
			MultiVMPoolUtil.clear();

			String teachersFilter = ParamUtil.getString(renderRequest, "teachersFilter");
			if (teachersFilter.equalsIgnoreCase("")) {
				tempResults = teachersLocalServiceUtil.findAllInGroup(groupId, comparator);
			} else {
				DynamicQuery query = DynamicQueryFactoryUtil.forClass(teachers.class)
				.add(
					PropertyFactoryUtil.forName("teacherName").like("%"+ParamUtil.getString(renderRequest, "teachersFilter")+"%")
				);
				tempResults = teachersLocalServiceUtil.dynamicQuery(query, -1, -1, comparator);
			}
		
		} catch (Exception e) {
			_log.debug(e);
		}
		renderRequest.setAttribute("highlightRowWithKey", renderRequest.getParameter("highlightRowWithKey"));
		renderRequest.setAttribute("containerStart", renderRequest.getParameter("containerStart"));
		renderRequest.setAttribute("containerEnd", renderRequest.getParameter("containerEnd"));
		renderRequest.setAttribute("tempResults", tempResults);
		renderRequest.setAttribute("hasAddPermission", hasAddPermission);

		PortletURL addTeachersURL = renderResponse.createActionURL();
		addTeachersURL.setParameter("javax.portlet.action", "newTeachers");
		renderRequest.setAttribute("addTeachersURL", addTeachersURL.toString());

		PortletURL teachersFilterURL = renderResponse.createRenderURL();
		teachersFilterURL.setParameter("javax.portlet.action", "doView");
		renderRequest.setAttribute("teachersFilterURL", teachersFilterURL.toString());

		include(viewJSP, renderRequest, renderResponse);
	}

	public void showViewEditTeachers(RenderRequest renderRequest, RenderResponse renderResponse) throws Exception {

		SimpleDateFormat formatDia    = new SimpleDateFormat("dd");
		SimpleDateFormat formatMes    = new SimpleDateFormat("MM");
		SimpleDateFormat formatAno    = new SimpleDateFormat("yyyy");
		SimpleDateFormat formatHora   = new SimpleDateFormat("HH");
		SimpleDateFormat formatMinuto = new SimpleDateFormat("mm");

		PortletURL editTeachersURL = renderResponse.createActionURL();
		String editType = (String) renderRequest.getParameter("editType");
		if (editType.equalsIgnoreCase("edit")) {
			editTeachersURL.setParameter("javax.portlet.action", "updateTeachers");
			long teacherId = Long.parseLong(renderRequest.getParameter("teacherId"));
			teachers teachers = teachersLocalServiceUtil.getteachers(teacherId);
			String teacherName = teachers.getTeacherName()+"";
			renderRequest.setAttribute("teacherName", teacherName);
            renderRequest.setAttribute("teachers", teachers);
		} else {
			editTeachersURL.setParameter("javax.portlet.action", "addTeachers");
			teachers errorTeachers = (teachers) renderRequest.getAttribute("errorTeachers");
			if (errorTeachers != null) {
				if (editType.equalsIgnoreCase("update")) {
					editTeachersURL.setParameter("javax.portlet.action", "updateTeachers");
                }
				renderRequest.setAttribute("teachers", errorTeachers);
			} else {
				teachersImpl blankTeachers = new teachersImpl();
				blankTeachers.setTeacherId(0);
				blankTeachers.setTeacherName("");
				renderRequest.setAttribute("teachers", blankTeachers);
			}

		}

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		renderRequest.setAttribute("editTeachersURL", editTeachersURL.toString());

		include(editteachersJSP, renderRequest, renderResponse);
	}

	private String dateToJsp(ActionRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(RenderRequest request, Date date) {
		PortletPreferences prefs = request.getPreferences();
		return dateToJsp(prefs, date);
	}
	private String dateToJsp(PortletPreferences prefs, Date date) {
		SimpleDateFormat format = new SimpleDateFormat(prefs.getValue("teachers-date-format", "yyyy/MM/dd"));
		String stringDate = format.format(date);
		return stringDate;
	}
	private String dateTimeToJsp(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String stringDate = format.format(date);
		return stringDate;
	}

	public void showEditDefault(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortletException, IOException {

		include(editJSP, renderRequest, renderResponse);
	}

	/* Portlet Actions */

	@ProcessAction(name = "newTeachers")
	public void newTeachers(ActionRequest request, ActionResponse response) {
		response.setRenderParameter("view", "editTeachers");
		response.setRenderParameter("editType", "add");
	}

	@ProcessAction(name = "addTeachers")
	public void addTeachers(ActionRequest request, ActionResponse response) throws Exception {
            teachers teachers = teachersFromRequest(request);
            ArrayList<String> errors = TeachersValidator.validateTeachers(teachers, request); 
            
            if (errors.isEmpty()) {
				teachersLocalServiceUtil.addteachers(teachers);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "teachers-added-successfully");
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
                response.setRenderParameter("view", "editTeachers");
                response.setRenderParameter("editType", "add");
                request.setAttribute("errorTeachers", teachers);
            }
	}

	@ProcessAction(name = "eventTeachers")
	public void eventTeachers(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		int containerStart = ParamUtil.getInteger(request, "containerStart");
		int containerEnd = ParamUtil.getInteger(request, "containerEnd");
		if (Validator.isNotNull(key)) {
            response.setRenderParameter("highlightRowWithKey", Long.toString(key));
            response.setRenderParameter("containerStart", Integer.toString(containerStart));
            response.setRenderParameter("containerEnd", Integer.toString(containerEnd));
		}
	}

	@ProcessAction(name = "editTeachers")
	public void editTeachers(ActionRequest request, ActionResponse response)
			throws Exception {
		long key = ParamUtil.getLong(request, "resourcePrimKey");
		if (Validator.isNotNull(key)) {
			response.setRenderParameter("teacherId", Long.toString(key));
			response.setRenderParameter("view", "editTeachers");
			response.setRenderParameter("editType", "edit");
		}
	}

	@ProcessAction(name = "deleteTeachers")
	public void deleteTeachers(ActionRequest request, ActionResponse response)throws Exception {
		long id = ParamUtil.getLong(request, "resourcePrimKey");
		if (Validator.isNotNull(id)) {
			teachers teachers = teachersLocalServiceUtil.getteachers(id);
			teachersLocalServiceUtil.deleteteachers(teachers);
            MultiVMPoolUtil.clear();
			SessionMessages.add(request, "teachers-deleted-successfully");
		} else {
			SessionErrors.add(request, "teachers-error-deleting");
		}
	}

	@ProcessAction(name = "updateTeachers")
	public void updateTeachers(ActionRequest request, ActionResponse response) throws Exception {
            teachers teachers = teachersFromRequest(request);
            ArrayList<String> errors = TeachersValidator.validateTeachers(teachers, request); 
            
            if (errors.isEmpty()) {
                teachersLocalServiceUtil.updateteachers(teachers);
                MultiVMPoolUtil.clear();
                response.setRenderParameter("view", "");
                SessionMessages.add(request, "teachers-updated-successfully");
            } else {
                for (String error : errors) {
                        SessionErrors.add(request, error);
                }
				response.setRenderParameter("teacherId)",Long.toString(teachers.getPrimaryKey()));
				response.setRenderParameter("view", "editTeachers");
				response.setRenderParameter("editType", "update");
				request.setAttribute("errorTeachers", teachers);
            }
        }

	@ProcessAction(name = "setTeachersPref")
	public void setTeachersPref(ActionRequest request, ActionResponse response) throws Exception {

		String rowsPerPage = ParamUtil.getString(request, "teachers-rows-per-page");
		String dateFormat = ParamUtil.getString(request, "teachers-date-format");
		String datetimeFormat = ParamUtil.getString(request, "teachers-datetime-format");

		ArrayList<String> errors = new ArrayList();
		if (TeachersValidator.validateEditTeachers(rowsPerPage, dateFormat, datetimeFormat, errors)) {
			response.setRenderParameter("teachers-rows-per-page", "");
			response.setRenderParameter("teachers-date-format", "");
			response.setRenderParameter("teachers-datetime-format", "");

			PortletPreferences prefs = request.getPreferences();
			prefs.setValue("teachers-rows-per-page", rowsPerPage);
			prefs.setValue("teachers-date-format", dateFormat);
			prefs.setValue("teachers-datetime-format", datetimeFormat);
			prefs.store();

			SessionMessages.add(request, "teachers-prefs-success");
		}
	}

	private teachers teachersFromRequest(ActionRequest request) {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		teachersImpl teachers = new teachersImpl();
        try {
            teachers.setTeacherId(ParamUtil.getLong(request, "teacherId"));
        } catch (Exception nfe) {
		    //Controled en Validator
        }
		teachers.setTeacherName(ParamUtil.getString(request, "teacherName"));
		try {
		    teachers.setPrimaryKey(ParamUtil.getLong(request,"resourcePrimKey"));
		} catch (NumberFormatException nfe) {
			//Controled en Validator
        }
		teachers.setCompanyId(themeDisplay.getCompanyId());
		teachers.setGroupId(themeDisplay.getScopeGroupId());
		teachers.setUserId(themeDisplay.getUserId());
		return teachers;
	}

	protected String editteachersJSP;
	protected String editJSP;
	protected String helpJSP;
	protected String viewJSP;

	private static Log _log = LogFactoryUtil.getLog(TeachersPortlet.class);

}

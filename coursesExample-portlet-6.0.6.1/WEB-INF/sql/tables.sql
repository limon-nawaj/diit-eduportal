create table coursesexample_classrooms (
	classroomId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	classroomName VARCHAR(30) null
);

create table coursesexample_comments (
	commentId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	studentId LONG,
	comment_ TEXT null
);

create table coursesexample_courseSubjects (
	courseSubjectId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	courseId LONG,
	subjectId LONG,
	teacherId LONG
);

create table coursesexample_courses (
	courseId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	courseName VARCHAR(30) null,
	courseActive BOOLEAN
);

create table coursesexample_holidays (
	holidayId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	courseId LONG,
	holidayName VARCHAR(30) null,
	holidayDate DATE null
);

create table coursesexample_students (
	studentId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	courseId LONG,
	studentName VARCHAR(30) null,
	classroomId LONG,
	studentPhoto LONG,
	folderIGId LONG
);

create table coursesexample_subjects (
	subjectId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	subjectName VARCHAR(30) null
);

create table coursesexample_teachers (
	teacherId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	teacherName VARCHAR(30) null
);
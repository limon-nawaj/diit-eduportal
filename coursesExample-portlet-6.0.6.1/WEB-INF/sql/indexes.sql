create index IX_21288157 on coursesexample_classrooms (groupId);
create index IX_5F5F81ED on coursesexample_classrooms (userId);
create index IX_4804CD5D on coursesexample_classrooms (userId, groupId);

create index IX_2B210023 on coursesexample_comments (groupId);
create index IX_5802CC1F on coursesexample_comments (studentId);
create index IX_47CCDB6B on coursesexample_comments (studentId, groupId);
create index IX_7035F9A1 on coursesexample_comments (userId);
create index IX_AF27C029 on coursesexample_comments (userId, groupId);

create index IX_4B93C0FF on coursesexample_courseSubjects (courseId);
create index IX_1651628B on coursesexample_courseSubjects (courseId, groupId);
create index IX_CE86DD15 on coursesexample_courseSubjects (groupId);
create index IX_91C2702 on coursesexample_courseSubjects (subjectId);
create index IX_8FFD52B8 on coursesexample_courseSubjects (teacherId);
create index IX_A707B66F on coursesexample_courseSubjects (userId);
create index IX_15A28B1B on coursesexample_courseSubjects (userId, groupId);

create index IX_16D3EF99 on coursesexample_courses (groupId);
create index IX_674C436B on coursesexample_courses (userId);
create index IX_406D99F on coursesexample_courses (userId, groupId);

create index IX_F8CC8258 on coursesexample_holidays (courseId);
create index IX_13EA1552 on coursesexample_holidays (courseId, groupId);
create index IX_2EF40C9C on coursesexample_holidays (groupId);
create index IX_2602FA08 on coursesexample_holidays (userId);
create index IX_798743A2 on coursesexample_holidays (userId, groupId);

create index IX_63297653 on coursesexample_students (classroomId);
create index IX_4F459155 on coursesexample_students (courseId);
create index IX_665592F5 on coursesexample_students (courseId, groupId);
create index IX_73CEA9FF on coursesexample_students (groupId);
create index IX_307DA445 on coursesexample_students (userId);
create index IX_92EBCE05 on coursesexample_students (userId, groupId);

create index IX_D0A89B70 on coursesexample_subjects (groupId);
create index IX_9ED740B4 on coursesexample_subjects (userId);
create index IX_E579BE76 on coursesexample_subjects (userId, groupId);

create index IX_CDD5B126 on coursesexample_teachers (groupId);
create index IX_75759C3E on coursesexample_teachers (userId);
create index IX_6B68BE2C on coursesexample_teachers (userId, groupId);
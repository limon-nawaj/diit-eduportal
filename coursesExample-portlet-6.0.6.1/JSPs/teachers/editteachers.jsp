<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>
<%@ page import="org.xmlportletfactory.portal.school.model.teachers" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.HttpUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>


<jsp:useBean class="java.lang.String" id="editTeachersURL" scope="request" />
<jsp:useBean id="teachers" type="org.xmlportletfactory.portal.school.model.teachers" scope="request"/>
<jsp:useBean id="teacherId" class="java.lang.String" scope="request" />
<jsp:useBean id="teacherName" class="java.lang.String" scope="request" />

<portlet:defineObjects />

<liferay-ui:success key="teachers-added-successfully" message="teachers-added-successfully" />
<form name="addTeachers" action="<%=editTeachersURL %>" method="POST">

	<input type="hidden" name="resourcePrimKey" value="<%=teachers.getPrimaryKey() %>">

	<table border="0">
		<tr>
			<td>
				<liferay-ui:message key="Teachers-teacherName" /><br>
            </td>
            <td>
				<liferay-ui:input-field model="<%= teachers.class %>" field="teacherName" fieldParam="teacherName" defaultValue="<%= teacherName %>" disabled="false" />
		        *

				<liferay-ui:error key="Teachers-teacherName-required" message="Teachers-teacherName-required" />
	            <br>
				<br>
			</td>
		</tr>
	</table>
	<input type="submit" value="<liferay-ui:message key="submit" />" >
	<input type="button" value="<liferay-ui:message key="cancel" />" onClick="self.location = '<portlet:renderURL></portlet:renderURL>';" />
</form>
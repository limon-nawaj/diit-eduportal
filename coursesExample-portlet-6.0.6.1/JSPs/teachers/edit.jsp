<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>

<liferay-ui:success key="teachers-prefs-success" message="teachers-prefs-success" />

<form name="setTeachersPref" action="<portlet:actionURL name="setTeachersPref" />" method="POST">
<table border="0">
	<tbody>
		<tr>
			<td>
				<liferay-ui:message key="teachers-rows-per-page" />*<br>
				<input type="text" name="teachers-rows-per-page" value="<%=prefs.getValue("teachers-rows-per-page","") %>" size="5" />
				<liferay-ui:error key="teachers-rows-per-page-required" message="teachers-rows-per-page-required" />
				<liferay-ui:error key="teachers-rows-per-page-invalid" message="teachers-rows-per-page-invalid" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="teachers-date-format" />*<br>
				<input type="text" name="teachers-date-format" value="<%=prefs.getValue("teachers-date-format","")%>" size="45" />
				<liferay-ui:error key="teachers-date-format-required" message="teachers-date-format-required" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="teachers-datetime-format" />*<br>
				<input type="text" name="teachers-datetime-format" value="<%=prefs.getValue("teachers-datetime-format","")%>" size="45" />
				<liferay-ui:error key="teachers-datetime-format-required" message="teachers-datetime-format-required" />
			</td>
		</tr>
	</tbody>
</table>
<input type="submit" value="Submit" />
</form>

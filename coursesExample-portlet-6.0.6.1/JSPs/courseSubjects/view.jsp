<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>

<%@ page import="org.xmlportletfactory.portal.school.model.courseSubjects" %>
<%@ page import="org.xmlportletfactory.portal.school.service.courseSubjectsLocalServiceUtil" %>

<%@ page import="com.liferay.portlet.PortalPreferences" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.kernel.util.OrderByComparator" %>
<%@ page import="org.xmlportletfactory.portal.school.CourseSubjectsComparator" %>
<%@ page import="org.xmlportletfactory.portal.school.model.subjects" %>
<%@ page import="org.xmlportletfactory.portal.school.service.subjectsLocalServiceUtil" %>
<%@ page import="org.xmlportletfactory.portal.school.model.teachers" %>
<%@ page import="org.xmlportletfactory.portal.school.service.teachersLocalServiceUtil" %>

<jsp:useBean id="addCourseSubjectsURL" class="java.lang.String" scope="request" />
<jsp:useBean id="courseSubjectsFilterURL" class="java.lang.String" scope="request" />
<jsp:useBean id="courseSubjectsFilter" class="java.lang.String" scope="request" />
<link rel="stylesheet" type="text/css" href="/coursesExample-portlet/css/Portlet_CourseSubjects.css" />
<liferay-ui:success key="courseSubjects-prefs-success" message="courseSubjects-prefs-success" />
<liferay-ui:success key="courseSubjects-added-successfully" message="courseSubjects-added-successfully" />
<liferay-ui:success key="courseSubjects-deleted-successfully" message="courseSubjects-deleted-successfully" />
<liferay-ui:success key="courseSubjects-updated-successfully" message="courseSubjects-updated-successfully" />
<liferay-ui:error key="courseSubjects-error-deleting" message="courseSubjects-error-deleting" />
<liferay-ui:error key="dependent-rows-exist-error-deleting" message="dependent-rows-exist-error-deleting" />

<c:choose>
	<c:when test='<%= (Boolean)request.getAttribute("hasAddPermission") %>'>
		<input type="button" name="addCourseSubjectsButton" value="<liferay-ui:message key="courseSubjects-add" />" onClick="self.location = '<%=addCourseSubjectsURL %>';">
	</c:when>
</c:choose>


<%
	String iconChecked = "checked";
	String iconUnchecked = "unchecked";
	int rows_per_page = new Integer(prefs.getValue("courseSubjects-rows-per-page", "5"));
	if (Validator.isNotNull(courseSubjectsFilter) || !courseSubjectsFilter.equalsIgnoreCase("")) {
		rows_per_page = 100;
	}
	
	SimpleDateFormat dateFormat = new SimpleDateFormat(prefs.getValue("courseSubjects-date-format", "yyyy/MM/dd"));
	SimpleDateFormat dateTimeFormat = new SimpleDateFormat(prefs.getValue("courseSubjects-datetime-format","yyyy/MM/dd HH:mm"));

	PortalPreferences portalPrefs = PortletPreferencesFactoryUtil.getPortalPreferences(request);

	String orderByCol = ParamUtil.getString(request, "orderByCol");
	String orderByType = ParamUtil.getString(request, "orderByType");

	if (Validator.isNotNull(orderByCol) && Validator.isNotNull(orderByType)) {
		portalPrefs.setValue("courseSubjects_order", "courseSubjects-order-by-col", orderByCol);
		portalPrefs.setValue("courseSubjects_order", "courseSubjects-order-by-type", orderByType);
	} else {
		orderByCol = portalPrefs.getValue("courseSubjects_order", "courseSubjects-order-by-col", "courseSubjectId");
		orderByType = portalPrefs.getValue("courseSubjects_order", "courseSubjects-order-by-type", "asc");
	}
%>
<liferay-ui:search-container  delta='<%= rows_per_page %>' emptyResultsMessage="courseSubjects-empty-results-message" orderByCol="<%= orderByCol%>" orderByType="<%= orderByType%>">
	<liferay-ui:search-container-results>

		<%
		int containerStart;
		int containerEnd;
		try {
			containerStart = ParamUtil.getInteger(request, "containerStart");
			containerEnd = ParamUtil.getInteger(request, "containerEnd");
		} catch (Exception e) {
			containerStart = searchContainer.getStart();
			containerEnd = searchContainer.getEnd();
		}
		if (containerStart <=0) {
			containerStart = searchContainer.getStart();
			containerEnd = searchContainer.getEnd();
		}
		
		List<courseSubjects> tempResults = (List<courseSubjects>)request.getAttribute("tempResults");
		results = ListUtil.subList(tempResults, containerStart, containerEnd);
		total = tempResults.size();

		pageContext.setAttribute("results", results);
		pageContext.setAttribute("total", total);

		request.setAttribute("containerStart",String.valueOf(containerStart));
		request.setAttribute("containerEnd",String.valueOf(containerEnd));
		%>

	</liferay-ui:search-container-results>

	<liferay-ui:search-container-row
		className="org.xmlportletfactory.portal.school.model.courseSubjects"
		keyProperty="courseSubjectId"
		modelVar="courseSubjects"
	>

		<liferay-ui:search-container-column-text
			name="Course Subject Id"
		    property="courseSubjectId"
			orderable="true"
			orderableProperty="courseSubjectId"
			align="right"
		/>
                <%
                String validation_subjectId = "";
                try {
                        subjects validationsubjects = subjectsLocalServiceUtil.getsubjects(courseSubjects.getSubjectId());
                        validation_subjectId = validationsubjects.getSubjectName();
                    } catch (Exception e) {
                        validation_subjectId = "-";
                    }
                %>
		<liferay-ui:search-container-column-text
			name="Subject Id"
			value="<%= validation_subjectId %>"
			align="left"
			orderable="true"
			orderableProperty="subjectId"

		/>
                <%
                String validation_teacherId = "";
                try {
                        teachers validationteachers = teachersLocalServiceUtil.getteachers(courseSubjects.getTeacherId());
                        validation_teacherId = validationteachers.getTeacherName();
                    } catch (Exception e) {
                        validation_teacherId = "-";
                    }
                %>
		<liferay-ui:search-container-column-text
			name="Teacher Id"
			value="<%= validation_teacherId %>"
			align="left"
			orderable="true"
			orderableProperty="teacherId"

		/>
		<liferay-ui:search-container-column-jsp
			align="right"
			path="/JSPs/courseSubjects/edit_actions.jsp"
		/>

	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator />

</liferay-ui:search-container>

<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>
<%@ page import="org.xmlportletfactory.portal.school.model.courseSubjects" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.HttpUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>

<%@ page import="org.xmlportletfactory.portal.school.model.impl.subjectsImpl" %>
<%@ page import="org.xmlportletfactory.portal.school.model.impl.teachersImpl" %>

<jsp:useBean class="java.lang.String" id="editCourseSubjectsURL" scope="request" />
<jsp:useBean id="courseSubjects" type="org.xmlportletfactory.portal.school.model.courseSubjects" scope="request"/>
<jsp:useBean id="courseSubjectId" class="java.lang.String" scope="request" />
<jsp:useBean id="courseId" class="java.lang.String" scope="request" />
<jsp:useBean id="subjectId" class="java.lang.String" scope="request" />
<jsp:useBean id="subjectIdList" type="java.util.List" scope="request" />
<jsp:useBean id="teacherId" class="java.lang.String" scope="request" />
<jsp:useBean id="teacherIdList" type="java.util.List" scope="request" />

<portlet:defineObjects />

<liferay-ui:success key="courseSubjects-added-successfully" message="courseSubjects-added-successfully" />
<form name="addCourseSubjects" action="<%=editCourseSubjectsURL %>" method="POST">

	<input type="hidden" name="resourcePrimKey" value="<%=courseSubjects.getPrimaryKey() %>">
	<input type="hidden" name="courseId" value="<%=courseSubjects.getCourseId() %>">

	<table border="0">
		<tr>
			<td>
				<liferay-ui:message key="CourseSubjects-courseId" /><br>
            </td>
            <td>
				<liferay-ui:input-field model="<%= courseSubjects.class %>" field="courseId" fieldParam="courseId" defaultValue="<%= courseId %>" disabled="true" />
		        *

				<liferay-ui:error key="CourseSubjects-courseId-required" message="CourseSubjects-courseId-required" />
				<liferay-ui:error key="error_number_format" message="error_number_format" />
	            <br>
				<br>
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="CourseSubjects-subjectId" /><br>
            </td>
            <td>
			<select name="<portlet:namespace />subjectId">
				<option value=""><liferay-ui:message key="combo-select" /></option>
			<%
				for (int i=0;i<subjectIdList.size();i++) {
					subjectsImpl subjectsSelector = (subjectsImpl)subjectIdList.get(i);
					if (subjectsSelector.getPrimaryKey() == courseSubjects.getSubjectId() ) {
			%>
				<option value="<%= subjectsSelector.getPrimaryKey() %>" selected="selected"><%= subjectsSelector.getSubjectName() %></option>

			<% } else {
			%>
				<option value="<%= subjectsSelector.getPrimaryKey() %>"><%= subjectsSelector.getSubjectName() %></option>
			<%
					}
				}
			%>
			</select>
				<liferay-ui:error key="CourseSubjects-subjectId-required" message="CourseSubjects-subjectId-required" />
				<liferay-ui:error key="error_number_format" message="error_number_format" />
	            <br>
				<br>
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="CourseSubjects-teacherId" /><br>
            </td>
            <td>
			<select name="<portlet:namespace />teacherId">
				<option value=""><liferay-ui:message key="combo-select" /></option>
			<%
				for (int i=0;i<teacherIdList.size();i++) {
					teachersImpl teachersSelector = (teachersImpl)teacherIdList.get(i);
					if (teachersSelector.getPrimaryKey() == courseSubjects.getTeacherId() ) {
			%>
				<option value="<%= teachersSelector.getPrimaryKey() %>" selected="selected"><%= teachersSelector.getTeacherName() %></option>

			<% } else {
			%>
				<option value="<%= teachersSelector.getPrimaryKey() %>"><%= teachersSelector.getTeacherName() %></option>
			<%
					}
				}
			%>
			</select>
				<liferay-ui:error key="CourseSubjects-teacherId-required" message="CourseSubjects-teacherId-required" />
				<liferay-ui:error key="error_number_format" message="error_number_format" />
	            <br>
				<br>
			</td>
		</tr>
	</table>
	<input type="submit" value="<liferay-ui:message key="submit" />" >
	<input type="button" value="<liferay-ui:message key="cancel" />" onClick="self.location = '<portlet:renderURL></portlet:renderURL>';" />
</form>
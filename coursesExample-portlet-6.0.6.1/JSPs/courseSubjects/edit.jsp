<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>

<liferay-ui:success key="courseSubjects-prefs-success" message="courseSubjects-prefs-success" />

<form name="setCourseSubjectsPref" action="<portlet:actionURL name="setCourseSubjectsPref" />" method="POST">
<table border="0">
	<tbody>
		<tr>
			<td>
				<liferay-ui:message key="courseSubjects-rows-per-page" />*<br>
				<input type="text" name="courseSubjects-rows-per-page" value="<%=prefs.getValue("courseSubjects-rows-per-page","") %>" size="5" />
				<liferay-ui:error key="courseSubjects-rows-per-page-required" message="courseSubjects-rows-per-page-required" />
				<liferay-ui:error key="courseSubjects-rows-per-page-invalid" message="courseSubjects-rows-per-page-invalid" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="courseSubjects-date-format" />*<br>
				<input type="text" name="courseSubjects-date-format" value="<%=prefs.getValue("courseSubjects-date-format","")%>" size="45" />
				<liferay-ui:error key="courseSubjects-date-format-required" message="courseSubjects-date-format-required" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="courseSubjects-datetime-format" />*<br>
				<input type="text" name="courseSubjects-datetime-format" value="<%=prefs.getValue("courseSubjects-datetime-format","")%>" size="45" />
				<liferay-ui:error key="courseSubjects-datetime-format-required" message="courseSubjects-datetime-format-required" />
			</td>
		</tr>
	</tbody>
</table>
<input type="submit" value="Submit" />
</form>

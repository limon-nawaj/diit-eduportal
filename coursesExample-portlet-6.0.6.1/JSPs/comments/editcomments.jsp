<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>
<%@ page import="org.xmlportletfactory.portal.school.model.comments" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.HttpUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>


<jsp:useBean class="java.lang.String" id="editCommentsURL" scope="request" />
<jsp:useBean id="comments" type="org.xmlportletfactory.portal.school.model.comments" scope="request"/>
<jsp:useBean id="commentId" class="java.lang.String" scope="request" />
<jsp:useBean id="studentId" class="java.lang.String" scope="request" />
<jsp:useBean id="comment" class="java.lang.String" scope="request" />

<portlet:defineObjects />

<liferay-ui:success key="comments-added-successfully" message="comments-added-successfully" />
<form name="addComments" action="<%=editCommentsURL %>" method="POST">

	<input type="hidden" name="resourcePrimKey" value="<%=comments.getPrimaryKey() %>">
	<input type="hidden" name="studentId" value="<%=comments.getStudentId() %>">

	<table border="0">
		<tr>
			<td>
				<liferay-ui:message key="Comments-studentId" /><br>
            </td>
            <td>
				<liferay-ui:input-field model="<%= comments.class %>" field="studentId" fieldParam="studentId" defaultValue="<%= studentId %>" disabled="true" />
		        *

				<liferay-ui:error key="Comments-studentId-required" message="Comments-studentId-required" />
				<liferay-ui:error key="error_number_format" message="error_number_format" />
	            <br>
				<br>
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="Comments-comment" /><br>
            </td>
            <td>
				<liferay-ui:input-textarea param="comment" defaultValue="<%=comment %>" />

				<liferay-ui:error key="Comments-comment-required" message="Comments-comment-required" />
	            <br>
				<br>
			</td>
		</tr>
	</table>
	<input type="submit" value="<liferay-ui:message key="submit" />" >
	<input type="button" value="<liferay-ui:message key="cancel" />" onClick="self.location = '<portlet:renderURL></portlet:renderURL>';" />
</form>
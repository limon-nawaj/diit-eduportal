<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>

<liferay-ui:success key="comments-prefs-success" message="comments-prefs-success" />

<form name="setCommentsPref" action="<portlet:actionURL name="setCommentsPref" />" method="POST">
<table border="0">
	<tbody>
		<tr>
			<td>
				<liferay-ui:message key="comments-rows-per-page" />*<br>
				<input type="text" name="comments-rows-per-page" value="<%=prefs.getValue("comments-rows-per-page","") %>" size="5" />
				<liferay-ui:error key="comments-rows-per-page-required" message="comments-rows-per-page-required" />
				<liferay-ui:error key="comments-rows-per-page-invalid" message="comments-rows-per-page-invalid" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="comments-date-format" />*<br>
				<input type="text" name="comments-date-format" value="<%=prefs.getValue("comments-date-format","")%>" size="45" />
				<liferay-ui:error key="comments-date-format-required" message="comments-date-format-required" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="comments-datetime-format" />*<br>
				<input type="text" name="comments-datetime-format" value="<%=prefs.getValue("comments-datetime-format","")%>" size="45" />
				<liferay-ui:error key="comments-datetime-format-required" message="comments-datetime-format-required" />
			</td>
		</tr>
	</tbody>
</table>
<input type="submit" value="Submit" />
</form>

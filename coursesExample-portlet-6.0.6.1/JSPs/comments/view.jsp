<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>

<%@ page import="org.xmlportletfactory.portal.school.model.comments" %>
<%@ page import="org.xmlportletfactory.portal.school.service.commentsLocalServiceUtil" %>

<%@ page import="com.liferay.portlet.PortalPreferences" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.kernel.util.OrderByComparator" %>
<%@ page import="org.xmlportletfactory.portal.school.CommentsComparator" %>

<jsp:useBean id="addCommentsURL" class="java.lang.String" scope="request" />
<jsp:useBean id="commentsFilterURL" class="java.lang.String" scope="request" />
<jsp:useBean id="commentsFilter" class="java.lang.String" scope="request" />
<link rel="stylesheet" type="text/css" href="/coursesExample-portlet/css/Portlet_Comments.css" />
<liferay-ui:success key="comments-prefs-success" message="comments-prefs-success" />
<liferay-ui:success key="comments-added-successfully" message="comments-added-successfully" />
<liferay-ui:success key="comments-deleted-successfully" message="comments-deleted-successfully" />
<liferay-ui:success key="comments-updated-successfully" message="comments-updated-successfully" />
<liferay-ui:error key="comments-error-deleting" message="comments-error-deleting" />
<liferay-ui:error key="dependent-rows-exist-error-deleting" message="dependent-rows-exist-error-deleting" />

<c:choose>
	<c:when test='<%= (Boolean)request.getAttribute("hasAddPermission") %>'>
		<input type="button" name="addCommentsButton" value="<liferay-ui:message key="comments-add" />" onClick="self.location = '<%=addCommentsURL %>';">
	</c:when>
</c:choose>


<%
	String iconChecked = "checked";
	String iconUnchecked = "unchecked";
	int rows_per_page = new Integer(prefs.getValue("comments-rows-per-page", "5"));
	if (Validator.isNotNull(commentsFilter) || !commentsFilter.equalsIgnoreCase("")) {
		rows_per_page = 100;
	}
	
	SimpleDateFormat dateFormat = new SimpleDateFormat(prefs.getValue("comments-date-format", "yyyy/MM/dd"));
	SimpleDateFormat dateTimeFormat = new SimpleDateFormat(prefs.getValue("comments-datetime-format","yyyy/MM/dd HH:mm"));

	PortalPreferences portalPrefs = PortletPreferencesFactoryUtil.getPortalPreferences(request);

	String orderByCol = ParamUtil.getString(request, "orderByCol");
	String orderByType = ParamUtil.getString(request, "orderByType");

	if (Validator.isNotNull(orderByCol) && Validator.isNotNull(orderByType)) {
		portalPrefs.setValue("comments_order", "comments-order-by-col", orderByCol);
		portalPrefs.setValue("comments_order", "comments-order-by-type", orderByType);
	} else {
		orderByCol = portalPrefs.getValue("comments_order", "comments-order-by-col", "commentId");
		orderByType = portalPrefs.getValue("comments_order", "comments-order-by-type", "asc");
	}
%>
<liferay-ui:search-container  delta='<%= rows_per_page %>' emptyResultsMessage="comments-empty-results-message" orderByCol="<%= orderByCol%>" orderByType="<%= orderByType%>">
	<liferay-ui:search-container-results>

		<%
		int containerStart;
		int containerEnd;
		try {
			containerStart = ParamUtil.getInteger(request, "containerStart");
			containerEnd = ParamUtil.getInteger(request, "containerEnd");
		} catch (Exception e) {
			containerStart = searchContainer.getStart();
			containerEnd = searchContainer.getEnd();
		}
		if (containerStart <=0) {
			containerStart = searchContainer.getStart();
			containerEnd = searchContainer.getEnd();
		}
		
		List<comments> tempResults = (List<comments>)request.getAttribute("tempResults");
		results = ListUtil.subList(tempResults, containerStart, containerEnd);
		total = tempResults.size();

		pageContext.setAttribute("results", results);
		pageContext.setAttribute("total", total);

		request.setAttribute("containerStart",String.valueOf(containerStart));
		request.setAttribute("containerEnd",String.valueOf(containerEnd));
		%>

	</liferay-ui:search-container-results>

	<liferay-ui:search-container-row
		className="org.xmlportletfactory.portal.school.model.comments"
		keyProperty="commentId"
		modelVar="comments"
	>

		<liferay-ui:search-container-column-text
			name="Comment Id"
		    property="commentId"
			orderable="true"
			orderableProperty="commentId"
			align="right"
		/>
		<liferay-ui:search-container-column-text name="Comment" align="center">
			<%
 				String commentIcon = iconUnchecked;
				String comment = comments.getComment();
				if (!comment.equals("")) {
 					commentIcon= iconChecked;
 				}
 			  %>
 			  <liferay-ui:icon image="<%=commentIcon %>"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-jsp
			align="right"
			path="/JSPs/comments/edit_actions.jsp"
		/>

	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator />

</liferay-ui:search-container>

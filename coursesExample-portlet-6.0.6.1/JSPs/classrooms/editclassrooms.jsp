<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>
<%@ page import="org.xmlportletfactory.portal.school.model.classrooms" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.HttpUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>


<jsp:useBean class="java.lang.String" id="editClassroomsURL" scope="request" />
<jsp:useBean id="classrooms" type="org.xmlportletfactory.portal.school.model.classrooms" scope="request"/>
<jsp:useBean id="classroomId" class="java.lang.String" scope="request" />
<jsp:useBean id="classroomName" class="java.lang.String" scope="request" />

<portlet:defineObjects />

<liferay-ui:success key="classrooms-added-successfully" message="classrooms-added-successfully" />
<form name="addClassrooms" action="<%=editClassroomsURL %>" method="POST">

	<input type="hidden" name="resourcePrimKey" value="<%=classrooms.getPrimaryKey() %>">

	<table border="0">
		<tr>
			<td>
				<liferay-ui:message key="Classrooms-classroomName" /><br>
            </td>
            <td>
				<liferay-ui:input-field model="<%= classrooms.class %>" field="classroomName" fieldParam="classroomName" defaultValue="<%= classroomName %>" disabled="false" />
		        *

				<liferay-ui:error key="Classrooms-classroomName-required" message="Classrooms-classroomName-required" />
	            <br>
				<br>
			</td>
		</tr>
	</table>
	<input type="submit" value="<liferay-ui:message key="submit" />" >
	<input type="button" value="<liferay-ui:message key="cancel" />" onClick="self.location = '<portlet:renderURL></portlet:renderURL>';" />
</form>
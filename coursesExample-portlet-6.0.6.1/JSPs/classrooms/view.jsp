<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>

<%@ page import="org.xmlportletfactory.portal.school.model.classrooms" %>
<%@ page import="org.xmlportletfactory.portal.school.service.classroomsLocalServiceUtil" %>

<%@ page import="com.liferay.portlet.PortalPreferences" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.kernel.util.OrderByComparator" %>
<%@ page import="org.xmlportletfactory.portal.school.ClassroomsComparator" %>

<jsp:useBean id="addClassroomsURL" class="java.lang.String" scope="request" />
<jsp:useBean id="classroomsFilterURL" class="java.lang.String" scope="request" />
<jsp:useBean id="classroomsFilter" class="java.lang.String" scope="request" />
<link rel="stylesheet" type="text/css" href="/coursesExample-portlet/css/Portlet_Classrooms.css" />
<liferay-ui:success key="classrooms-prefs-success" message="classrooms-prefs-success" />
<liferay-ui:success key="classrooms-added-successfully" message="classrooms-added-successfully" />
<liferay-ui:success key="classrooms-deleted-successfully" message="classrooms-deleted-successfully" />
<liferay-ui:success key="classrooms-updated-successfully" message="classrooms-updated-successfully" />
<liferay-ui:error key="classrooms-error-deleting" message="classrooms-error-deleting" />
<liferay-ui:error key="dependent-rows-exist-error-deleting" message="dependent-rows-exist-error-deleting" />

<c:choose>
	<c:when test='<%= (Boolean)request.getAttribute("hasAddPermission") %>'>
		<input type="button" name="addClassroomsButton" value="<liferay-ui:message key="classrooms-add" />" onClick="self.location = '<%=addClassroomsURL %>';">
	</c:when>
</c:choose>


<form id="classroomsFilterForm" name="classroomsFilterForm" action="<%=classroomsFilterURL %>" method="POST">
	<input type="text" name="classroomsFilter" value="<%= classroomsFilter %>" />
	<input type="submit" value="<liferay-ui:message key="filter" />">
</form>
<%
	String iconChecked = "checked";
	String iconUnchecked = "unchecked";
	int rows_per_page = new Integer(prefs.getValue("classrooms-rows-per-page", "5"));
	if (Validator.isNotNull(classroomsFilter) || !classroomsFilter.equalsIgnoreCase("")) {
		rows_per_page = 100;
	}
	
	SimpleDateFormat dateFormat = new SimpleDateFormat(prefs.getValue("classrooms-date-format", "yyyy/MM/dd"));
	SimpleDateFormat dateTimeFormat = new SimpleDateFormat(prefs.getValue("classrooms-datetime-format","yyyy/MM/dd HH:mm"));

	PortalPreferences portalPrefs = PortletPreferencesFactoryUtil.getPortalPreferences(request);

	String orderByCol = ParamUtil.getString(request, "orderByCol");
	String orderByType = ParamUtil.getString(request, "orderByType");

	if (Validator.isNotNull(orderByCol) && Validator.isNotNull(orderByType)) {
		portalPrefs.setValue("classrooms_order", "classrooms-order-by-col", orderByCol);
		portalPrefs.setValue("classrooms_order", "classrooms-order-by-type", orderByType);
	} else {
		orderByCol = portalPrefs.getValue("classrooms_order", "classrooms-order-by-col", "classroomId");
		orderByType = portalPrefs.getValue("classrooms_order", "classrooms-order-by-type", "asc");
	}
%>
<liferay-ui:search-container  delta='<%= rows_per_page %>' emptyResultsMessage="classrooms-empty-results-message" orderByCol="<%= orderByCol%>" orderByType="<%= orderByType%>">
	<liferay-ui:search-container-results>

		<%
		int containerStart;
		int containerEnd;
		try {
			containerStart = ParamUtil.getInteger(request, "containerStart");
			containerEnd = ParamUtil.getInteger(request, "containerEnd");
		} catch (Exception e) {
			containerStart = searchContainer.getStart();
			containerEnd = searchContainer.getEnd();
		}
		if (containerStart <=0) {
			containerStart = searchContainer.getStart();
			containerEnd = searchContainer.getEnd();
		}
		
		List<classrooms> tempResults = (List<classrooms>)request.getAttribute("tempResults");
		results = ListUtil.subList(tempResults, containerStart, containerEnd);
		total = tempResults.size();

		pageContext.setAttribute("results", results);
		pageContext.setAttribute("total", total);

		request.setAttribute("containerStart",String.valueOf(containerStart));
		request.setAttribute("containerEnd",String.valueOf(containerEnd));
		%>

	</liferay-ui:search-container-results>

	<liferay-ui:search-container-row
		className="org.xmlportletfactory.portal.school.model.classrooms"
		keyProperty="classroomId"
		modelVar="classrooms"
	>

		<liferay-ui:search-container-column-text
			name="Id"
		    property="classroomId"
			orderable="true"
			orderableProperty="classroomId"
			align="right"
		/>
		<liferay-ui:search-container-column-text
			name="Name"
			property="classroomName"
			orderable="true"
			orderableProperty="classroomName"
			align="left"
		/>
		<liferay-ui:search-container-column-jsp
			align="right"
			path="/JSPs/classrooms/edit_actions.jsp"
		/>

	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator />

</liferay-ui:search-container>

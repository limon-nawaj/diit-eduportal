<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>

<liferay-ui:success key="classrooms-prefs-success" message="classrooms-prefs-success" />

<form name="setClassroomsPref" action="<portlet:actionURL name="setClassroomsPref" />" method="POST">
<table border="0">
	<tbody>
		<tr>
			<td>
				<liferay-ui:message key="classrooms-rows-per-page" />*<br>
				<input type="text" name="classrooms-rows-per-page" value="<%=prefs.getValue("classrooms-rows-per-page","") %>" size="5" />
				<liferay-ui:error key="classrooms-rows-per-page-required" message="classrooms-rows-per-page-required" />
				<liferay-ui:error key="classrooms-rows-per-page-invalid" message="classrooms-rows-per-page-invalid" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="classrooms-date-format" />*<br>
				<input type="text" name="classrooms-date-format" value="<%=prefs.getValue("classrooms-date-format","")%>" size="45" />
				<liferay-ui:error key="classrooms-date-format-required" message="classrooms-date-format-required" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="classrooms-datetime-format" />*<br>
				<input type="text" name="classrooms-datetime-format" value="<%=prefs.getValue("classrooms-datetime-format","")%>" size="45" />
				<liferay-ui:error key="classrooms-datetime-format-required" message="classrooms-datetime-format-required" />
			</td>
		</tr>
	</tbody>
</table>
<input type="submit" value="Submit" />
</form>

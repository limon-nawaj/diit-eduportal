<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>
<%@ page import="org.xmlportletfactory.portal.school.model.courses" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.HttpUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>


<jsp:useBean class="java.lang.String" id="editCoursesURL" scope="request" />
<jsp:useBean id="courses" type="org.xmlportletfactory.portal.school.model.courses" scope="request"/>
<jsp:useBean id="courseId" class="java.lang.String" scope="request" />
<jsp:useBean id="courseName" class="java.lang.String" scope="request" />
<jsp:useBean id="courseActive" class="java.lang.String" scope="request" />

<portlet:defineObjects />

<liferay-ui:success key="courses-added-successfully" message="courses-added-successfully" />
<form name="addCourses" action="<%=editCoursesURL %>" method="POST">

	<input type="hidden" name="resourcePrimKey" value="<%=courses.getPrimaryKey() %>">

	<table border="0">
		<tr>
			<td>
				<liferay-ui:message key="Courses-courseName" /><br>
            </td>
            <td>
				<liferay-ui:input-field model="<%= courses.class %>" field="courseName" fieldParam="courseName" defaultValue="<%= courseName %>" disabled="false" />
		        *

				<liferay-ui:error key="Courses-courseName-required" message="Courses-courseName-required" />
	            <br>
				<br>
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="Courses-courseActive" /><br>
            </td>
            <td>
				<liferay-ui:input-checkbox param="courseActive" defaultValue="<%= Boolean.valueOf(courseActive) %>" />

				<liferay-ui:error key="Courses-courseActive-required" message="Courses-courseActive-required" />
	            <br>
				<br>
			</td>
		</tr>
	</table>
	<input type="submit" value="<liferay-ui:message key="submit" />" >
	<input type="button" value="<liferay-ui:message key="cancel" />" onClick="self.location = '<portlet:renderURL></portlet:renderURL>';" />
</form>
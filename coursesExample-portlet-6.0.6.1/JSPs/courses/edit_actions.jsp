<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>

<%@ page import="org.xmlportletfactory.portal.school.model.courses"%>
<%@ page import="org.xmlportletfactory.portal.school.service.coursesLocalServiceUtil"%>

<%
ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
courses courses = (courses)row.getObject();

long groupId = courses.getGroupId();
String name = courses.class.getName();
String primKey = String.valueOf(courses.getPrimaryKey());
String courseIdStr = courses.getCourseId() + "";

%>
<liferay-ui:icon-menu cssClass="">

	<c:if test="<%= permissionChecker.hasPermission(groupId, name, primKey, ActionKeys.UPDATE) %>">
		<portlet:actionURL name="editCourses" var="editCoursesURL">
			<portlet:param name="resourcePrimKey" value="<%=primKey %>" />
			<portlet:param name="courseId" value="<%= courseIdStr %>" />
		</portlet:actionURL>

		<liferay-ui:icon image="edit" url="<%=editCoursesURL.toString() %>" />
	</c:if>

	<c:if test="<%= permissionChecker.hasPermission(groupId, name, primKey, ActionKeys.DELETE) %>">
		<portlet:actionURL name="deleteCourses" var="deleteCoursesURL">
			<portlet:param name="resourcePrimKey" value="<%= primKey %>" />
		</portlet:actionURL>

		<liferay-ui:icon image="delete" url="<%=deleteCoursesURL.toString() %>" />
	</c:if>
</liferay-ui:icon-menu>
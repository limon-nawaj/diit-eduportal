<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>

<%@ page import="org.xmlportletfactory.portal.school.model.courses" %>
<%@ page import="org.xmlportletfactory.portal.school.service.coursesLocalServiceUtil" %>

<%@ page import="com.liferay.portlet.PortalPreferences" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.kernel.util.OrderByComparator" %>
<%@ page import="org.xmlportletfactory.portal.school.CoursesComparator" %>

<jsp:useBean id="addCoursesURL" class="java.lang.String" scope="request" />
<jsp:useBean id="coursesFilterURL" class="java.lang.String" scope="request" />
<jsp:useBean id="coursesFilter" class="java.lang.String" scope="request" />
<link rel="stylesheet" type="text/css" href="/coursesExample-portlet/css/Portlet_Courses.css" />
<liferay-ui:success key="courses-prefs-success" message="courses-prefs-success" />
<liferay-ui:success key="courses-added-successfully" message="courses-added-successfully" />
<liferay-ui:success key="courses-deleted-successfully" message="courses-deleted-successfully" />
<liferay-ui:success key="courses-updated-successfully" message="courses-updated-successfully" />
<liferay-ui:error key="courses-error-deleting" message="courses-error-deleting" />
<liferay-ui:error key="dependent-rows-exist-error-deleting" message="dependent-rows-exist-error-deleting" />

<c:choose>
	<c:when test='<%= (Boolean)request.getAttribute("hasAddPermission") %>'>
		<input type="button" name="addCoursesButton" value="<liferay-ui:message key="courses-add" />" onClick="self.location = '<%=addCoursesURL %>';">
	</c:when>
</c:choose>


<form id="coursesFilterForm" name="coursesFilterForm" action="<%=coursesFilterURL %>" method="POST">
	<input type="text" name="coursesFilter" value="<%= coursesFilter %>" />
	<input type="submit" value="<liferay-ui:message key="filter" />">
</form>
<%
	String iconChecked = "checked";
	String iconUnchecked = "unchecked";
	int rows_per_page = new Integer(prefs.getValue("courses-rows-per-page", "5"));
	if (Validator.isNotNull(coursesFilter) || !coursesFilter.equalsIgnoreCase("")) {
		rows_per_page = 100;
	}
	
	SimpleDateFormat dateFormat = new SimpleDateFormat(prefs.getValue("courses-date-format", "yyyy/MM/dd"));
	SimpleDateFormat dateTimeFormat = new SimpleDateFormat(prefs.getValue("courses-datetime-format","yyyy/MM/dd HH:mm"));

	PortalPreferences portalPrefs = PortletPreferencesFactoryUtil.getPortalPreferences(request);

	String orderByCol = ParamUtil.getString(request, "orderByCol");
	String orderByType = ParamUtil.getString(request, "orderByType");

	if (Validator.isNotNull(orderByCol) && Validator.isNotNull(orderByType)) {
		portalPrefs.setValue("courses_order", "courses-order-by-col", orderByCol);
		portalPrefs.setValue("courses_order", "courses-order-by-type", orderByType);
	} else {
		orderByCol = portalPrefs.getValue("courses_order", "courses-order-by-col", "courseId");
		orderByType = portalPrefs.getValue("courses_order", "courses-order-by-type", "asc");
	}
%>
<liferay-ui:search-container  delta='<%= rows_per_page %>' emptyResultsMessage="courses-empty-results-message" orderByCol="<%= orderByCol%>" orderByType="<%= orderByType%>">
	<liferay-ui:search-container-results>

		<%
		int containerStart;
		int containerEnd;
		try {
			containerStart = ParamUtil.getInteger(request, "containerStart");
			containerEnd = ParamUtil.getInteger(request, "containerEnd");
		} catch (Exception e) {
			containerStart = searchContainer.getStart();
			containerEnd = searchContainer.getEnd();
		}
		if (containerStart <=0) {
			containerStart = searchContainer.getStart();
			containerEnd = searchContainer.getEnd();
		}
		
		List<courses> tempResults = (List<courses>)request.getAttribute("tempResults");
		results = ListUtil.subList(tempResults, containerStart, containerEnd);
		total = tempResults.size();

		pageContext.setAttribute("results", results);
		pageContext.setAttribute("total", total);

		request.setAttribute("containerStart",String.valueOf(containerStart));
		request.setAttribute("containerEnd",String.valueOf(containerEnd));
		%>

	</liferay-ui:search-container-results>

	<liferay-ui:search-container-row
		className="org.xmlportletfactory.portal.school.model.courses"
		keyProperty="courseId"
		modelVar="courses"
	>

            <liferay-ui:search-container-column-jsp
                    align="left"
                    path="/JSPs/courses/view_action.jsp"
            />
		<liferay-ui:search-container-column-text
			name="Id"
		    property="courseId"
			orderable="true"
			orderableProperty="courseId"
			align="right"
		/>
		<liferay-ui:search-container-column-text
			name="Course Name"
			property="courseName"
			orderable="true"
			orderableProperty="courseName"
			align="left"
		/>
		<liferay-ui:search-container-column-text name="Is Active"	align="center" >
			 <%
 				String courseActiveIcon = iconUnchecked;
  				if (courses.isCourseActive()) {
 					courseActiveIcon = iconChecked;
 				}
 			  %>
			<liferay-ui:icon image="<%=courseActiveIcon %>"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-jsp
			align="right"
			path="/JSPs/courses/edit_actions.jsp"
		/>

	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator />

</liferay-ui:search-container>

<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>

<liferay-ui:success key="courses-prefs-success" message="courses-prefs-success" />

<form name="setCoursesPref" action="<portlet:actionURL name="setCoursesPref" />" method="POST">
<table border="0">
	<tbody>
		<tr>
			<td>
				<liferay-ui:message key="courses-rows-per-page" />*<br>
				<input type="text" name="courses-rows-per-page" value="<%=prefs.getValue("courses-rows-per-page","") %>" size="5" />
				<liferay-ui:error key="courses-rows-per-page-required" message="courses-rows-per-page-required" />
				<liferay-ui:error key="courses-rows-per-page-invalid" message="courses-rows-per-page-invalid" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="courses-date-format" />*<br>
				<input type="text" name="courses-date-format" value="<%=prefs.getValue("courses-date-format","")%>" size="45" />
				<liferay-ui:error key="courses-date-format-required" message="courses-date-format-required" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="courses-datetime-format" />*<br>
				<input type="text" name="courses-datetime-format" value="<%=prefs.getValue("courses-datetime-format","")%>" size="45" />
				<liferay-ui:error key="courses-datetime-format-required" message="courses-datetime-format-required" />
			</td>
		</tr>
	</tbody>
</table>
<input type="submit" value="Submit" />
</form>

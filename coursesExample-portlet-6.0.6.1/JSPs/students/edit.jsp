<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>

<liferay-ui:success key="students-prefs-success" message="students-prefs-success" />

<form name="setStudentsPref" action="<portlet:actionURL name="setStudentsPref" />" method="POST">
<table border="0">
	<tbody>
		<tr>
			<td>
				<liferay-ui:message key="students-rows-per-page" />*<br>
				<input type="text" name="students-rows-per-page" value="<%=prefs.getValue("students-rows-per-page","") %>" size="5" />
				<liferay-ui:error key="students-rows-per-page-required" message="students-rows-per-page-required" />
				<liferay-ui:error key="students-rows-per-page-invalid" message="students-rows-per-page-invalid" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="students-date-format" />*<br>
				<input type="text" name="students-date-format" value="<%=prefs.getValue("students-date-format","")%>" size="45" />
				<liferay-ui:error key="students-date-format-required" message="students-date-format-required" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="students-datetime-format" />*<br>
				<input type="text" name="students-datetime-format" value="<%=prefs.getValue("students-datetime-format","")%>" size="45" />
				<liferay-ui:error key="students-datetime-format-required" message="students-datetime-format-required" />
			</td>
		</tr>
	</tbody>
</table>
<input type="submit" value="Submit" />
</form>

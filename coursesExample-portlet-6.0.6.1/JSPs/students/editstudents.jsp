<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>
<%@ page import="org.xmlportletfactory.portal.school.model.students" %>
<%@ page import="com.liferay.portal.kernel.servlet.ImageServletTokenUtil" %>
<%@ page import="com.liferay.portlet.imagegallery.model.IGImage" %>
<%@ page import="com.liferay.portlet.imagegallery.service.IGFolderLocalServiceUtil" %>
<%@ page import="com.liferay.portlet.imagegallery.service.IGImageLocalServiceUtil" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.HttpUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>

<%@ page import="org.xmlportletfactory.portal.school.model.impl.classroomsImpl" %>

<jsp:useBean class="java.lang.String" id="editStudentsURL" scope="request" />
<jsp:useBean id="students" type="org.xmlportletfactory.portal.school.model.students" scope="request"/>
<jsp:useBean id="studentId" class="java.lang.String" scope="request" />
<jsp:useBean id="courseId" class="java.lang.String" scope="request" />
<jsp:useBean id="studentName" class="java.lang.String" scope="request" />
<jsp:useBean id="classroomId" class="java.lang.String" scope="request" />
<jsp:useBean id="classroomIdList" type="java.util.List" scope="request" />
<jsp:useBean id="studentPhoto" class="java.lang.String" scope="request" />
<jsp:useBean id="folderIGId" class="java.lang.String" scope="request" />

<portlet:defineObjects />

<liferay-ui:success key="students-added-successfully" message="students-added-successfully" />
<form name="addStudents" action="<%=editStudentsURL %>" method="POST" enctype='multipart/form-data'>

	<input type="hidden" name="resourcePrimKey" value="<%=students.getPrimaryKey() %>">
	<input type="hidden" name="HIDDEN_studentPhoto" value="<%=students.getStudentPhoto() %>">
	<input type="hidden" name="HIDDEN_folderIGId" value="<%=students.getFolderIGId() %>">
	<input type="hidden" name="courseId" value="<%=students.getCourseId() %>">

	<table border="0">
		<tr>
			<td>
				<liferay-ui:message key="Students-courseId" /><br>
            </td>
            <td>
				<liferay-ui:input-field model="<%= students.class %>" field="courseId" fieldParam="courseId" defaultValue="<%= courseId %>" disabled="true" />
		        *

				<liferay-ui:error key="Students-courseId-required" message="Students-courseId-required" />
				<liferay-ui:error key="error_number_format" message="error_number_format" />
	            <br>
				<br>
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="Students-studentName" /><br>
            </td>
            <td>
				<liferay-ui:input-field model="<%= students.class %>" field="studentName" fieldParam="studentName" defaultValue="<%= studentName %>" disabled="false" />
		        *

				<liferay-ui:error key="Students-studentName-required" message="Students-studentName-required" />
	            <br>
				<br>
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="Students-classroomId" /><br>
            </td>
            <td>
			<select name="<portlet:namespace />classroomId">
				<option value=""><liferay-ui:message key="combo-select" /></option>
			<%
				for (int i=0;i<classroomIdList.size();i++) {
					classroomsImpl classroomsSelector = (classroomsImpl)classroomIdList.get(i);
					if (classroomsSelector.getPrimaryKey() == students.getClassroomId() ) {
			%>
				<option value="<%= classroomsSelector.getPrimaryKey() %>" selected="selected"><%= classroomsSelector.getClassroomName() %></option>

			<% } else {
			%>
				<option value="<%= classroomsSelector.getPrimaryKey() %>"><%= classroomsSelector.getClassroomName() %></option>
			<%
					}
				}
			%>
			</select>
				<liferay-ui:error key="Students-classroomId-required" message="Students-classroomId-required" />
				<liferay-ui:error key="error_number_format" message="error_number_format" />
	            <br>
				<br>
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="Students-studentPhoto" /><br>
            </td>
            <td>
<% if (!studentPhoto.trim().equalsIgnoreCase("")  && !studentPhoto.trim().equalsIgnoreCase("0")) {
	IGImage studentPhoto_img = IGImageLocalServiceUtil.getIGImage(Long.parseLong(studentPhoto));
%>
			<img src="<%= themeDisplay.getPathImage() %>/image_gallery?img_id=<%= studentPhoto_img.getSmallImageId() %>&t=<%=ImageServletTokenUtil.getToken(studentPhoto_img.getSmallImageId()) %>" />
			<aui:input name="studentPhoto_DELETEIMAGE" label="delete-field" type="checkbox" inlineField="true" />
<% } %>
	        <aui:input name="studentPhoto_IMAGEFILE" label="" type="file"  />

				<liferay-ui:error key="Students-studentPhoto-required" message="Students-studentPhoto-required" />
	            <br>
				<br>
			</td>
		</tr>
	</table>
	<input type="submit" value="<liferay-ui:message key="submit" />" >
	<input type="button" value="<liferay-ui:message key="cancel" />" onClick="self.location = '<portlet:renderURL></portlet:renderURL>';" />
</form>
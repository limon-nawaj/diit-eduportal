<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>

<%@ page import="org.xmlportletfactory.portal.school.model.holidays" %>
<%@ page import="org.xmlportletfactory.portal.school.service.holidaysLocalServiceUtil" %>

<%@ page import="com.liferay.portlet.PortalPreferences" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.kernel.util.OrderByComparator" %>
<%@ page import="org.xmlportletfactory.portal.school.HolidaysComparator" %>

<jsp:useBean id="addHolidaysURL" class="java.lang.String" scope="request" />
<jsp:useBean id="holidaysFilterURL" class="java.lang.String" scope="request" />
<jsp:useBean id="holidaysFilter" class="java.lang.String" scope="request" />
<link rel="stylesheet" type="text/css" href="/coursesExample-portlet/css/Portlet_Holidays.css" />
<liferay-ui:success key="holidays-prefs-success" message="holidays-prefs-success" />
<liferay-ui:success key="holidays-added-successfully" message="holidays-added-successfully" />
<liferay-ui:success key="holidays-deleted-successfully" message="holidays-deleted-successfully" />
<liferay-ui:success key="holidays-updated-successfully" message="holidays-updated-successfully" />
<liferay-ui:error key="holidays-error-deleting" message="holidays-error-deleting" />
<liferay-ui:error key="dependent-rows-exist-error-deleting" message="dependent-rows-exist-error-deleting" />

<c:choose>
	<c:when test='<%= (Boolean)request.getAttribute("hasAddPermission") %>'>
		<input type="button" name="addHolidaysButton" value="<liferay-ui:message key="holidays-add" />" onClick="self.location = '<%=addHolidaysURL %>';">
	</c:when>
</c:choose>


<form id="holidaysFilterForm" name="holidaysFilterForm" action="<%=holidaysFilterURL %>" method="POST">
	<input type="text" name="holidaysFilter" value="<%= holidaysFilter %>" />
	<input type="submit" value="<liferay-ui:message key="filter" />">
</form>
<%
	String iconChecked = "checked";
	String iconUnchecked = "unchecked";
	int rows_per_page = new Integer(prefs.getValue("holidays-rows-per-page", "5"));
	if (Validator.isNotNull(holidaysFilter) || !holidaysFilter.equalsIgnoreCase("")) {
		rows_per_page = 100;
	}
	
	SimpleDateFormat dateFormat = new SimpleDateFormat(prefs.getValue("holidays-date-format", "yyyy/MM/dd"));
	SimpleDateFormat dateTimeFormat = new SimpleDateFormat(prefs.getValue("holidays-datetime-format","yyyy/MM/dd HH:mm"));

	PortalPreferences portalPrefs = PortletPreferencesFactoryUtil.getPortalPreferences(request);

	String orderByCol = ParamUtil.getString(request, "orderByCol");
	String orderByType = ParamUtil.getString(request, "orderByType");

	if (Validator.isNotNull(orderByCol) && Validator.isNotNull(orderByType)) {
		portalPrefs.setValue("holidays_order", "holidays-order-by-col", orderByCol);
		portalPrefs.setValue("holidays_order", "holidays-order-by-type", orderByType);
	} else {
		orderByCol = portalPrefs.getValue("holidays_order", "holidays-order-by-col", "holidayId");
		orderByType = portalPrefs.getValue("holidays_order", "holidays-order-by-type", "asc");
	}
%>
<liferay-ui:search-container  delta='<%= rows_per_page %>' emptyResultsMessage="holidays-empty-results-message" orderByCol="<%= orderByCol%>" orderByType="<%= orderByType%>">
	<liferay-ui:search-container-results>

		<%
		int containerStart;
		int containerEnd;
		try {
			containerStart = ParamUtil.getInteger(request, "containerStart");
			containerEnd = ParamUtil.getInteger(request, "containerEnd");
		} catch (Exception e) {
			containerStart = searchContainer.getStart();
			containerEnd = searchContainer.getEnd();
		}
		if (containerStart <=0) {
			containerStart = searchContainer.getStart();
			containerEnd = searchContainer.getEnd();
		}
		
		List<holidays> tempResults = (List<holidays>)request.getAttribute("tempResults");
		results = ListUtil.subList(tempResults, containerStart, containerEnd);
		total = tempResults.size();

		pageContext.setAttribute("results", results);
		pageContext.setAttribute("total", total);

		request.setAttribute("containerStart",String.valueOf(containerStart));
		request.setAttribute("containerEnd",String.valueOf(containerEnd));
		%>

	</liferay-ui:search-container-results>

	<liferay-ui:search-container-row
		className="org.xmlportletfactory.portal.school.model.holidays"
		keyProperty="holidayId"
		modelVar="holidays"
	>

		<liferay-ui:search-container-column-text
			name="Holiday Id"
		    property="holidayId"
			orderable="true"
			orderableProperty="holidayId"
			align="right"
		/>
		<liferay-ui:search-container-column-text
			name="Holiday Name"
			property="holidayName"
			orderable="true"
			orderableProperty="holidayName"
			align="left"
		/>
		<liferay-ui:search-container-column-text
			name="Date"
			value="<%= dateFormat.format(holidays.getHolidayDate()) %>"
			orderable="true"
			orderableProperty="holidayDate"
			align="center"
		/>
		<liferay-ui:search-container-column-jsp
			align="right"
			path="/JSPs/holidays/edit_actions.jsp"
		/>

	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator />

</liferay-ui:search-container>

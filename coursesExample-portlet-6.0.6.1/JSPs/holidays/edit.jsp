<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>

<liferay-ui:success key="holidays-prefs-success" message="holidays-prefs-success" />

<form name="setHolidaysPref" action="<portlet:actionURL name="setHolidaysPref" />" method="POST">
<table border="0">
	<tbody>
		<tr>
			<td>
				<liferay-ui:message key="holidays-rows-per-page" />*<br>
				<input type="text" name="holidays-rows-per-page" value="<%=prefs.getValue("holidays-rows-per-page","") %>" size="5" />
				<liferay-ui:error key="holidays-rows-per-page-required" message="holidays-rows-per-page-required" />
				<liferay-ui:error key="holidays-rows-per-page-invalid" message="holidays-rows-per-page-invalid" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="holidays-date-format" />*<br>
				<input type="text" name="holidays-date-format" value="<%=prefs.getValue("holidays-date-format","")%>" size="45" />
				<liferay-ui:error key="holidays-date-format-required" message="holidays-date-format-required" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="holidays-datetime-format" />*<br>
				<input type="text" name="holidays-datetime-format" value="<%=prefs.getValue("holidays-datetime-format","")%>" size="45" />
				<liferay-ui:error key="holidays-datetime-format-required" message="holidays-datetime-format-required" />
			</td>
		</tr>
	</tbody>
</table>
<input type="submit" value="Submit" />
</form>

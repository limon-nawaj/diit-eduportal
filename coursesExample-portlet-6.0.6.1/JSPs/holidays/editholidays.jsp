<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>
<%@ page import="org.xmlportletfactory.portal.school.model.holidays" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.HttpUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>


<jsp:useBean class="java.lang.String" id="editHolidaysURL" scope="request" />
<jsp:useBean id="holidays" type="org.xmlportletfactory.portal.school.model.holidays" scope="request"/>
<jsp:useBean id="holidayId" class="java.lang.String" scope="request" />
<jsp:useBean id="courseId" class="java.lang.String" scope="request" />
<jsp:useBean id="holidayName" class="java.lang.String" scope="request" />
<jsp:useBean id="holidayDateDia" class="java.lang.String" scope="request" />
<jsp:useBean id="holidayDateMes" class="java.lang.String" scope="request" />
<jsp:useBean id="holidayDateAno" class="java.lang.String" scope="request" />

<portlet:defineObjects />

<liferay-ui:success key="holidays-added-successfully" message="holidays-added-successfully" />
<form name="addHolidays" action="<%=editHolidaysURL %>" method="POST">

	<input type="hidden" name="resourcePrimKey" value="<%=holidays.getPrimaryKey() %>">
	<input type="hidden" name="courseId" value="<%=holidays.getCourseId() %>">

	<table border="0">
		<tr>
			<td>
				<liferay-ui:message key="Holidays-courseId" /><br>
            </td>
            <td>
				<liferay-ui:input-field model="<%= holidays.class %>" field="courseId" fieldParam="courseId" defaultValue="<%= courseId %>" disabled="true" />
		        *

				<liferay-ui:error key="Holidays-courseId-required" message="Holidays-courseId-required" />
				<liferay-ui:error key="error_number_format" message="error_number_format" />
	            <br>
				<br>
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="Holidays-holidayName" /><br>
            </td>
            <td>
				<liferay-ui:input-field model="<%= holidays.class %>" field="holidayName" fieldParam="holidayName" defaultValue="<%= holidayName %>" disabled="false" />
		        *

				<liferay-ui:error key="Holidays-holidayName-required" message="Holidays-holidayName-required" />
	            <br>
				<br>
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="Holidays-holidayDate" /><br>
            </td>
            <td>
				<liferay-ui:input-date yearRangeEnd="2100" yearRangeStart="1900" dayParam="holidayDateDia" dayValue="<%= Integer.valueOf(holidayDateDia) %>" monthParam="holidayDateMes" monthValue="<%= Integer.valueOf(holidayDateMes)-1 %>" yearParam="holidayDateAno" yearValue="<%= Integer.valueOf(holidayDateAno) %>" />

				<liferay-ui:error key="Holidays-holidayDate-required" message="Holidays-holidayDate-required" />
	            <br>
				<br>
			</td>
		</tr>
	</table>
	<input type="submit" value="<liferay-ui:message key="submit" />" >
	<input type="button" value="<liferay-ui:message key="cancel" />" onClick="self.location = '<portlet:renderURL></portlet:renderURL>';" />
</form>
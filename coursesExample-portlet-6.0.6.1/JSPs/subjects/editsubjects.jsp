<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>
<%@ page import="org.xmlportletfactory.portal.school.model.subjects" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.HttpUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>


<jsp:useBean class="java.lang.String" id="editSubjectsURL" scope="request" />
<jsp:useBean id="subjects" type="org.xmlportletfactory.portal.school.model.subjects" scope="request"/>
<jsp:useBean id="subjectId" class="java.lang.String" scope="request" />
<jsp:useBean id="subjectName" class="java.lang.String" scope="request" />

<portlet:defineObjects />

<liferay-ui:success key="subjects-added-successfully" message="subjects-added-successfully" />
<form name="addSubjects" action="<%=editSubjectsURL %>" method="POST">

	<input type="hidden" name="resourcePrimKey" value="<%=subjects.getPrimaryKey() %>">

	<table border="0">
		<tr>
			<td>
				<liferay-ui:message key="Subjects-subjectName" /><br>
            </td>
            <td>
				<liferay-ui:input-field model="<%= subjects.class %>" field="subjectName" fieldParam="subjectName" defaultValue="<%= subjectName %>" disabled="false" />
		        *

				<liferay-ui:error key="Subjects-subjectName-required" message="Subjects-subjectName-required" />
	            <br>
				<br>
			</td>
		</tr>
	</table>
	<input type="submit" value="<liferay-ui:message key="submit" />" >
	<input type="button" value="<liferay-ui:message key="cancel" />" onClick="self.location = '<portlet:renderURL></portlet:renderURL>';" />
</form>
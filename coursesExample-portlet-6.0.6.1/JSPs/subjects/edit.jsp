<%
/**
 *     Copyright (C) 2009-2011  Jack A. Rider All rights reserved.
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 */
%> 
<%@include file="../init.jsp" %>

<liferay-ui:success key="subjects-prefs-success" message="subjects-prefs-success" />

<form name="setSubjectsPref" action="<portlet:actionURL name="setSubjectsPref" />" method="POST">
<table border="0">
	<tbody>
		<tr>
			<td>
				<liferay-ui:message key="subjects-rows-per-page" />*<br>
				<input type="text" name="subjects-rows-per-page" value="<%=prefs.getValue("subjects-rows-per-page","") %>" size="5" />
				<liferay-ui:error key="subjects-rows-per-page-required" message="subjects-rows-per-page-required" />
				<liferay-ui:error key="subjects-rows-per-page-invalid" message="subjects-rows-per-page-invalid" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="subjects-date-format" />*<br>
				<input type="text" name="subjects-date-format" value="<%=prefs.getValue("subjects-date-format","")%>" size="45" />
				<liferay-ui:error key="subjects-date-format-required" message="subjects-date-format-required" />
			</td>
		</tr>
		<tr>
			<td>
				<liferay-ui:message key="subjects-datetime-format" />*<br>
				<input type="text" name="subjects-datetime-format" value="<%=prefs.getValue("subjects-datetime-format","")%>" size="45" />
				<liferay-ui:error key="subjects-datetime-format-required" message="subjects-datetime-format-required" />
			</td>
		</tr>
	</tbody>
</table>
<input type="submit" value="Submit" />
</form>

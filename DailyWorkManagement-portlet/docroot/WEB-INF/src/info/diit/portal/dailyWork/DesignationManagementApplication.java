package info.diit.portal.dailyWork;

import java.util.Date;
import java.util.List;

import info.diit.portal.dailyWork.dto.DesignationDto;
import info.diit.portal.dailyWork.model.Designation;
import info.diit.portal.dailyWork.model.impl.DesignationImpl;
import info.diit.portal.dailyWork.service.DesignationLocalServiceUtil;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class DesignationManagementApplication extends Application implements PortletRequestListener {

	private ThemeDisplay themeDisplay;
	private Window window;
	private BeanItemContainer<Designation> designationContainer;
	
	private final static String COLUMN_DESIGNATION = "designationTitle";
	
	private Table designationTable;
	private Designation designation;
	private TextField designationField;
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        window.addComponent(mainLayout());
        loadDesignationList();
    }

	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}
	
	private GridLayout mainLayout(){
		GridLayout mainGridLayout = new GridLayout(8, 4);
		mainGridLayout.setWidth("85%");
		mainGridLayout.setHeight("100%");
		mainGridLayout.setSpacing(true);
		
		VerticalLayout formLayout = new VerticalLayout();
		formLayout.setWidth("100%");
		formLayout.setSpacing(true);
		designationField = new TextField("Designation");
		designationField.setWidth("100%");
		formLayout.addComponent(designationField);
		Button saveButton = new Button("Save");
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (designation==null) {
					designation = new DesignationImpl();
					designation.setNew(true);
				}
				
				designation.setCompanyId(themeDisplay.getCompanyId());
				designation.setUserId(themeDisplay.getUser().getUserId());
				designation.setUserName(themeDisplay.getUser().getScreenName());
				
				String title = designationField.getValue().toString();
				if (title!=null) {
					designation.setDesignationTitle(title);
				}
				
				try {
					if (designation.isNew()) {
						designation.setCreateDate(new Date());
						DesignationLocalServiceUtil.addDesignation(designation);
						window.showNotification("Designation saved successfully");
					}else{
						designation.setModifiedDate(new Date());
						DesignationLocalServiceUtil.updateDesignation(designation);
						window.showNotification("Designation updated successfully");
					}
					loadDesignationList();
					clear();
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
		formLayout.addComponent(saveButton);
		
		designationContainer = new BeanItemContainer<Designation>(Designation.class);
		designationTable = new Table("Designation List", designationContainer);
		designationTable.setWidth("100%");
		designationTable.setHeight("100%");
		designationTable.setSelectable(true);
		
		designationTable.setColumnHeader(COLUMN_DESIGNATION, "Designations");
		designationTable.setVisibleColumns(new String[]{COLUMN_DESIGNATION});
		
		HorizontalLayout rowLayout = new HorizontalLayout();
		rowLayout.setSpacing(true);
		rowLayout.setWidth("100%");
		Label spacer = new Label();
		Button editButton = new Button("Edit");
		editButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				designation = (Designation) designationTable.getValue();
				if (designation!=null) {
					designation.setNew(false);
					editDesignat(designation);
				}
			}
		});
		Button deleteButton = new Button("Delete");
		deleteButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				designation = (Designation) designationTable.getValue();
				if (designation!=null) {
					try {
						DesignationLocalServiceUtil.deleteDesignation(designation);
						window.showNotification("Designation Deleted successfully");
						loadDesignationList();
						clear();
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}
			}
		});
		
		rowLayout.addComponent(spacer);
		rowLayout.addComponent(editButton);
		rowLayout.addComponent(deleteButton);
		rowLayout.setExpandRatio(spacer, 1);
		
		mainGridLayout.addComponent(formLayout, 0, 0, 2, 0);
		mainGridLayout.addComponent(designationTable, 0, 1, 4, 2);
		mainGridLayout.addComponent(rowLayout, 0, 3, 4, 3);
		
		return mainGridLayout;
	}
	
	public void loadDesignationList(){
		if (designationContainer!=null) {
			designationContainer.removeAllItems();
			try {
				List<Designation> designationList = DesignationLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
				for (Designation designation : designationList) {
					designation.getDesignationId();
					designation.getDesignationTitle();
					designationContainer.addBean(designation);
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void editDesignat(Designation designation){
		designationField.setValue(designation.getDesignationTitle());
	}
	
	public void clear(){
		designation = null;
		designationField.setValue("");
	}

}

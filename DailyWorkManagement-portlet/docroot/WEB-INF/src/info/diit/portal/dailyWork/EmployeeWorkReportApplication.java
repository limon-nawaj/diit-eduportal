package info.diit.portal.dailyWork;

import info.diit.portal.NoSuchEmployeeException;
import info.diit.portal.dailyWork.dto.CampusDto;
import info.diit.portal.dailyWork.dto.UserDto;
import info.diit.portal.dailyWork.dto.WorkReportDto;
import info.diit.portal.dailyWork.model.DailyWork;
import info.diit.portal.dailyWork.model.Designation;
import info.diit.portal.dailyWork.model.Task;
import info.diit.portal.dailyWork.service.DailyWorkLocalServiceUtil;
import info.diit.portal.dailyWork.service.DesignationLocalServiceUtil;
import info.diit.portal.dailyWork.service.TaskLocalServiceUtil;
import info.diit.portal.model.Employee;
import info.diit.portal.model.EmployeeMobile;
import info.diit.portal.service.EmployeeLocalServiceUtil;
import info.diit.portal.service.EmployeeMobileLocalServiceUtil;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.theme.ThemeDisplay;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.vaadin.Application;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.Resource;
import com.vaadin.terminal.StreamResource;
import com.vaadin.terminal.StreamResource.StreamSource;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;

public class EmployeeWorkReportApplication extends Application implements PortletRequestListener {

	private static ThemeDisplay themeDisplay;
	private Window window;
	
	private final static String ITEM_TODAY = "Today";
	private final static String ITEM_YESTERDAY = "Yesterday";
	private final static String ITEM_THIS_WEEK = "This Week";
	private final static String ITEM_LAST_WEEK = "Last Week";
	private final static String ITEM_THIS_MONTH = "This Month";
	private final static String ITEM_LAST_MONTH = "Last Month";
	
	private final static String COLUMN_TASK = "task";
	private final static String COLUMN_TIME = "time";
	private final static String COLUMN_AVERAGE = "average";
	
	private final static String DATE_FORMAT = "dd/MM/yyyy";
	SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
	
	private List<Organization> userOrganizationList;
	private List<CampusDto> campusDtoList;
	private List<UserDto> employeeList;
	
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        
        try {
			userOrganizationList = themeDisplay.getUser().getOrganizations();
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
        
        loadUserOrganization();
        
        window.addComponent(mainLayout());
        if (userOrganizationList.size()<2) {
        	loadEmployee();
        	loadEmployeComboBox();
		}
//        window.showNotification("User organization size: "+userOrganizationList.size());
    }

	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}
	
	private ComboBox campusComboBox;
	private static ComboBox employeeComboBox;
	private ComboBox durationBox;
	private static DateField startDateField;
	private static DateField endDateField;
	
	private static BeanItemContainer<WorkReportDto> reportContainer;
	private Table reportTable;
	
	private GridLayout mainLayout(){
		GridLayout mainLayout = new GridLayout(9, 8);
		mainLayout.setSpacing(true);
		mainLayout.setWidth("100%");
		
		campusComboBox = new ComboBox("Campus");
		campusComboBox.setImmediate(true);
		campusComboBox.setWidth("100%");
		
		if (campusDtoList!=null) {
			for (CampusDto campus : campusDtoList) {
				campusComboBox.addItem(campus);
			}
		}
		
		employeeComboBox = new ComboBox("Employee");
		employeeComboBox.setImmediate(true);
		employeeComboBox.setWidth("100%");
		
		campusComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				employeeComboBox.removeAllItems();
				reportContainer.removeAllItems();
				CampusDto campus = (CampusDto) campusComboBox.getValue();
				if (campus!=null) {
					loadEmployee();
					loadEmployeComboBox();
//					loadReport();
				}
			}
		});
		
		employeeComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				UserDto user = (UserDto) employeeComboBox.getValue();
				reportContainer.removeAllItems();
				if (user!=null) {
					loadReport();
				}
			}
		});
		
		durationBox = new ComboBox("Duration");
		durationBox.setImmediate(true);
		durationBox.setWidth("100%");
		durationBox.addItem(ITEM_TODAY);
		durationBox.addItem(ITEM_YESTERDAY);
		durationBox.addItem(ITEM_THIS_WEEK);
		durationBox.addItem(ITEM_LAST_WEEK);
		durationBox.addItem(ITEM_THIS_MONTH);
		durationBox.addItem(ITEM_LAST_MONTH);
		durationBox.setValue(ITEM_YESTERDAY);
		
		startDateField = new DateField("Start Date");
		startDateField.setResolution(DateField.RESOLUTION_DAY);
		startDateField.setDateFormat(DATE_FORMAT);
		startDateField.setImmediate(true);
		startDateField.setWidth("100%");
		endDateField = new DateField("End Date");
		endDateField.setResolution(DateField.RESOLUTION_DAY);
		endDateField.setDateFormat(DATE_FORMAT);
		endDateField.setImmediate(true);
		endDateField.setWidth("100%");
		
		final Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);
		Date lastDay = calendar.getTime();
		lastDay.setHours(0);
		lastDay.setMinutes(0);
		lastDay.setSeconds(0);
        startDateField.setValue(lastDay);
        endDateField.setValue(lastDay);
		
		durationBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Object duration = durationBox.getValue();
				
				if (duration!=null) {
					if (duration.equals(ITEM_TODAY)) {
						startDateField.setValue(new Date());
						endDateField.setValue(new Date());
					}else if (duration.equals(ITEM_YESTERDAY)) {
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.DATE, -1);
						Date lastDay = cal.getTime();
						lastDay.setHours(0);
						lastDay.setMinutes(0);
						lastDay.setSeconds(0);
						startDateField.setValue(lastDay);
						endDateField.setValue(lastDay);
					}else if (duration.equals(ITEM_THIS_WEEK)) {
						Calendar cal = Calendar.getInstance();
						
						cal.setFirstDayOfWeek(Calendar.SATURDAY);
						int firstDayOfWeek = cal.getFirstDayOfWeek();
						
						Calendar startDay = Calendar.getInstance();
						startDay.setTime(cal.getTime());
						
						int days = (startDay.get(Calendar.DAY_OF_WEEK)+7-firstDayOfWeek)%7;
						startDay.add(Calendar.DATE, -days);
						
						Date startDateOfWeek = startDay.getTime();
						startDateOfWeek.setHours(0);
						startDateOfWeek.setMinutes(0);
						startDateOfWeek.setSeconds(0);
						startDateField.setValue(startDateOfWeek);
						endDateField.setValue(new Date());
//						window.showNotification("Start date:"+startDayOfWeek.getTime());
						
					}else if (duration.equals(ITEM_LAST_WEEK)) {
						Calendar cal = Calendar.getInstance();
						cal.setFirstDayOfWeek(Calendar.SATURDAY);
						int firstDayOfWeek = cal.getFirstDayOfWeek();
						
						Calendar startDayOfWeek = Calendar.getInstance();
						startDayOfWeek.setTime(cal.getTime());
						
						int days = (startDayOfWeek.get(Calendar.DAY_OF_WEEK)+7-firstDayOfWeek)%14;
						startDayOfWeek.add(Calendar.DATE, -days);
						
						Date startDateOfWeek = startDayOfWeek.getTime();
						startDateOfWeek.setHours(0);
						startDateOfWeek.setMinutes(0);
						startDateOfWeek.setSeconds(0);
						startDateField.setValue(startDateOfWeek);
						
//						cal.add(Calendar.DATE, -days+6);
						startDayOfWeek.add(Calendar.DATE, 6);
						Date endDateOfWeek = startDayOfWeek.getTime();
						endDateOfWeek.setHours(0);
						endDateOfWeek.setMinutes(0);
						endDateOfWeek.setSeconds(0);
						
						endDateField.setValue(endDateOfWeek);
						
					}else if (duration.equals(ITEM_THIS_MONTH)) {
				        calendar.setTime(new Date());  
				        calendar.set(Calendar.DAY_OF_MONTH, 1);  
				        Date firstDayOfMonth = calendar.getTime();
				        firstDayOfMonth.setHours(0);
				        firstDayOfMonth.setMinutes(0);
				        firstDayOfMonth.setSeconds(0);
				        startDateField.setValue(firstDayOfMonth);
				        endDateField.setValue(new Date());
					}else if (duration.equals(ITEM_LAST_MONTH)) {
				        int monthdays = calendar.get(Calendar.DAY_OF_MONTH);
//				        window.showNotification("Month days:"+monthdays);
				        calendar.add(Calendar.DAY_OF_MONTH,-monthdays);        
				        Date lastdayofmonth = calendar.getTime();
				        lastdayofmonth.setHours(0);
				        lastdayofmonth.setMinutes(0);
				        lastdayofmonth.setSeconds(0);
				        int monthdays2 = calendar.get(Calendar.DAY_OF_MONTH);
				        int anotherday = monthdays2 - 1;
				        calendar.add(Calendar.DAY_OF_MONTH, - anotherday);
				        Date firstdayofmonth = calendar.getTime();
				        firstdayofmonth.setHours(0);
				        firstdayofmonth.setMinutes(0);
				        firstdayofmonth.setSeconds(0);
				        startDateField.setValue(firstdayofmonth);
				        endDateField.setValue(lastdayofmonth);
					}
					
				}
//				loadReport();
			}
		});
		
		startDateField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Date start = (Date) startDateField.getValue();
				Date end = (Date) endDateField.getValue();
				end.setHours(23);
				end.setMinutes(59);
				end.setSeconds(59);
				if (start!=null) {
					if (start.before(end)) {
						loadReport();
					}
				}
			}
		});
		
		
		endDateField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Date start = (Date) startDateField.getValue();
				Date end = (Date) endDateField.getValue();
				end.setHours(23);
				end.setMinutes(59);
				end.setSeconds(59);
				if (end!=null) {
					if (end.before(start)) {
//						endDate.setValue(null);
						window.showNotification("Please select valid end date", Window.Notification.TYPE_ERROR_MESSAGE);
						return;
					}else{
						loadReport();
					}
				}
			}
		});
		
		reportContainer = new BeanItemContainer<WorkReportDto>(WorkReportDto.class);
//		reportContainer.sort(new Object[]{COLUMN_TASK}, new boolean[]{true});
		
		reportTable = new Table("Task Report", reportContainer);
		reportTable.setImmediate(true);
		reportTable.setWidth("100%");
		reportTable.setFooterVisible(true);
		
		reportTable.setColumnHeader(COLUMN_TASK, "Task");
		reportTable.setColumnHeader(COLUMN_TIME, "Time");
		reportTable.setColumnHeader(COLUMN_AVERAGE, "Average");
		reportTable.setVisibleColumns(new String[]{COLUMN_TASK, COLUMN_TIME, COLUMN_AVERAGE});
		
		reportTable.setColumnExpandRatio(COLUMN_TASK, 4);
		reportTable.setColumnExpandRatio(COLUMN_TIME, 2);
		reportTable.setColumnExpandRatio(COLUMN_AVERAGE, 2);
		
		Label spacer = new Label();
		
		final Embedded pdfContents = new Embedded();
        pdfContents.setSizeFull();
        pdfContents.setType(Embedded.TYPE_BROWSER);
        
		Button printButton = new Button("Print");
		printButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				displayPopup();
			}
		});
		HorizontalLayout rowLayout = new HorizontalLayout();
		rowLayout.setWidth("100%");
		rowLayout.addComponent(spacer);
		rowLayout.addComponent(printButton);
		rowLayout.setExpandRatio(spacer, 1);
		
		if (userOrganizationList.size()>1) {
			mainLayout.addComponent(campusComboBox, 0, 0, 3, 0);
			mainLayout.addComponent(employeeComboBox, 0, 1, 3, 1);
			mainLayout.addComponent(durationBox, 5, 1, 8, 1);
			mainLayout.addComponent(startDateField, 0, 2, 3, 2);
			mainLayout.addComponent(endDateField, 5, 2, 8, 2);
		}else{
			mainLayout.addComponent(employeeComboBox, 0, 0, 3, 0);
			mainLayout.addComponent(durationBox, 5, 0, 8, 0);
			mainLayout.addComponent(startDateField, 0, 1, 3, 1);
			mainLayout.addComponent(endDateField, 5, 1, 8, 1);
		}
		
		mainLayout.addComponent(reportTable, 0, 3, 8, 3);
		mainLayout.addComponent(rowLayout, 0, 4, 8, 4);
		return mainLayout;
	}
	
	private void loadEmployeComboBox(){
		if (employeeList!=null) {
			for (UserDto user : employeeList) {
				employeeComboBox.addItem(user);
			}
		}
	}
	
	private void loadUserOrganization(){
		if (userOrganizationList.size()>1) {
			if (campusDtoList!=null) {
				campusDtoList.clear();
			}else{
				campusDtoList = new ArrayList<CampusDto>();
			}
			
			for (Organization organization : userOrganizationList) {
				CampusDto campus = new CampusDto();
				campus.setId(organization.getOrganizationId());
				campus.setName(organization.getName());
				campusDtoList.add(campus);
			}
		}
	}
	
	private void loadEmployee(){
		if (employeeList!=null) {
			employeeList.clear();
		}else{
			employeeList = new ArrayList<UserDto>();
		}
		try {
			List<Employee> allEmployee = null;
			if (userOrganizationList.size()>1) {
				CampusDto campus = (CampusDto) campusComboBox.getValue();
				if (campus!=null) {
					allEmployee = EmployeeLocalServiceUtil.findByCompanyOrganization(themeDisplay.getCompany().getCompanyId(), campus.getId());
				}
			}else{
				if (userOrganizationList!=null) {
					Organization orgUser = userOrganizationList.get(0);
					allEmployee = EmployeeLocalServiceUtil.findByCompanyOrganization(themeDisplay.getCompany().getCompanyId(), orgUser.getOrganizationId());
				}
			}
			
			if (allEmployee!=null) {
				for (Employee employee : allEmployee) {
					UserDto userDto = new UserDto();
					userDto.setUserId(employee.getEmployeeId());
					userDto.setName(employee.getName());
					employeeList.add(userDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private Set<Long> taskIds;
	private static Set<Date> dateList;
	
	private void loadReport(){
		Date startDay = (Date) startDateField.getValue();
		startDay.setHours(0);
		startDay.setMinutes(0);
		startDay.setSeconds(0);
		Date endDay = (Date) endDateField.getValue();
		endDay.setHours(0);
		endDay.setMinutes(0);
		endDay.setSeconds(0);
		
		Criterion criterion = RestrictionsFactoryUtil.between("worKingDay", startDay, endDay);
		
		try {
			UserDto user = (UserDto) employeeComboBox.getValue();
			CampusDto campus = (CampusDto) campusComboBox.getValue();
			Employee employee = EmployeeLocalServiceUtil.findByUserId(themeDisplay.getUser().getUserId());
			
			List<DailyWork> dailyWorks = null;
			
			if (userOrganizationList.size()>1) {
				if (user!=null && campus!=null) {
					List<Employee> employeeList = EmployeeLocalServiceUtil.findByCompanyOrganization(themeDisplay.getCompany().getCompanyId(), campus.getId());
					if (employeeList!=null) {
						for (Employee emp : employeeList) {
							DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(DailyWork.class);
							if (user.getUserId()==emp.getEmployeeId()) {
								dynamicQuery.add(criterion).add(PropertyFactoryUtil.forName("employeeId").eq(emp.getEmployeeId()));
								dailyWorks = DailyWorkLocalServiceUtil.dynamicQuery(dynamicQuery);
							}
						}
					}
				}
			}else{
				List<Employee> employeeList = null;
				for (int i = 0; i < 1; i++) {
					Organization orgUser = userOrganizationList.get(i);
					employeeList = EmployeeLocalServiceUtil.findByCompanyOrganization(themeDisplay.getCompany().getCompanyId(), orgUser.getOrganizationId());
				}
				
				if (employeeList!=null) {
					for (Employee emp : employeeList) {
						DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(DailyWork.class);
						if (user.getUserId()==emp.getEmployeeId()) {
							dynamicQuery.add(criterion).add(PropertyFactoryUtil.forName("employeeId").eq(emp.getEmployeeId()));
							dailyWorks = DailyWorkLocalServiceUtil.dynamicQuery(dynamicQuery);
						}
					}
				}
			}
			
			if (taskIds!=null) {
				taskIds.clear();
			}else{
				taskIds = new HashSet<Long>();
			}
			
			dateList = new HashSet<Date>();
			
			if (dailyWorks!=null) {
				for (DailyWork dailyWork : dailyWorks) {
					taskIds.add(dailyWork.getTaskId());
					dateList.add(dailyWork.getWorKingDay());
				}
			}
			
			if (reportContainer!=null) {
				reportContainer.removeAllItems();
			}
			
			int totalSpentTime = 0;
			int rowCount = 0;
			
			for (Long id : taskIds) {
				DynamicQuery timQeuery = DynamicQueryFactoryUtil.forClass(DailyWork.class);
				if (user!=null) {
					timQeuery.add(criterion).add(PropertyFactoryUtil.forName("employeeId").eq(user.getUserId()))
					.add(PropertyFactoryUtil.forName("taskId").eq(id)).setProjection(ProjectionFactoryUtil.sum("time"));
				}
				
				
				List timeList = DailyWorkLocalServiceUtil.dynamicQuery(timQeuery);
				int time = 0;
				if(timeList != null)
				{
					time = ((Long)timeList.get(0)).intValue();
				}
				
				totalSpentTime = totalSpentTime + time;
				
				int quotient = 0;
				int reminder = 0;
				
				
				
				DynamicQuery avgTimQeuery = DynamicQueryFactoryUtil.forClass(DailyWork.class);
				if (user!=null) {
					avgTimQeuery.add(criterion).add(PropertyFactoryUtil.forName("employeeId").eq(user.getUserId()))
					.add(PropertyFactoryUtil.forName("taskId").eq(id)).setProjection(ProjectionFactoryUtil.rowCount());
				}
				
				
				List avgTimeList = DailyWorkLocalServiceUtil.dynamicQuery(avgTimQeuery);
				
				int rows = 0;
				if (avgTimeList!=null) {
					rows = ((Long)avgTimeList.get(0)).intValue();
				}
//				rowCount = rows + rowCount;
				
				int avgQuotient = 0;
				int avgReminder = 0;
				
				if (time>0) {
					quotient = time/60;
					reminder = time%60;
					
					avgQuotient = time/rows;
					avgReminder = time/rows;
				}
				
				if (avgQuotient>0) {
					avgQuotient = avgQuotient/60;
				}
				
				if (avgReminder>0) {
					avgReminder = avgReminder%60;
				}
				
				for (DailyWork dailyWork : dailyWorks) {
					if (dailyWork.getTaskId()==id) {
						Task task = TaskLocalServiceUtil.fetchTask(id);
//						window.showNotification("Task:"+task.getTitle());
						WorkReportDto workReportDto = new WorkReportDto();
						workReportDto.setId(dailyWork.getDailyWorkId());
						workReportDto.setTaskId(task.getTaskId());
						workReportDto.setTask(task.getTitle());
						
						if (reminder<=9) {
							workReportDto.setTime(quotient+":0"+reminder);
						}else{
							workReportDto.setTime(quotient+":"+reminder);
						}
						
						if (avgReminder<=9) {
							workReportDto.setAverage(avgQuotient+":0"+avgReminder);
						}else{
							workReportDto.setAverage(avgQuotient+":"+avgReminder);
						}
						reportContainer.sort(new Object[]{COLUMN_TIME}, new boolean[]{false});
						reportContainer.addBean(workReportDto);
						
						break;
					}
				}
				
			}
//			window.showNotification("Total Spent time:"+totalSpentTime);
			
			reportTable.setColumnFooter(COLUMN_TASK, "TOTAL");
			
			int footerQuotient = 0;
			int footerReminder = 0;
			
			if (totalSpentTime>0) {
				footerQuotient = totalSpentTime/60;
				footerReminder = totalSpentTime%60;
			}
			
			if (footerReminder<=9) {
				reportTable.setColumnFooter(COLUMN_TIME, footerQuotient+":0"+footerReminder);
			}else{
				reportTable.setColumnFooter(COLUMN_TIME, footerQuotient+":"+footerReminder);
			}
			
			int perDayQuotient = 0;
			int perDayReminder = 0;
			double perDayAvg = 0;
			if (totalSpentTime>0) {
				perDayAvg = totalSpentTime/dateList.size();
				
				perDayQuotient = (int) (perDayAvg/60);
				perDayReminder = (int) (perDayAvg%60);
			}
			
			
			
			if (perDayReminder<=9) {
				reportTable.setColumnFooter(COLUMN_AVERAGE, perDayQuotient+":0"+perDayReminder+" (per day)");
			}else{
				reportTable.setColumnFooter(COLUMN_AVERAGE, perDayQuotient+":"+perDayReminder+" (per day)");
			}
			
			reportContainer.sort(new Object[]{COLUMN_TASK, COLUMN_TIME}, new boolean[]{true, false});
			
			
		} catch (NoSuchEmployeeException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}
	
//	PDF
	private Resource createPdf() {
        StreamResource resource = new StreamResource(new Pdf(), "test.pdf?" + System.currentTimeMillis(), this);
        resource.setMIMEType("application/pdf");
        return resource;
    }

    private void displayPopup() {
        Window win = new Window();
        win.getContent().setSizeFull();
        win.setResizable(true);
        win.setWidth("800");
        win.setHeight("600");
        win.center();
        Embedded e = new Embedded();
        e.setSizeFull();
        e.setType(Embedded.TYPE_BROWSER);

        StreamResource resource = new StreamResource(new Pdf(), "test.pdf?" + System.currentTimeMillis(), this);
        resource.setMIMEType("application/pdf");

        e.setSource(resource);
        win.addComponent(e);
        getMainWindow().addWindow(win);
    }
    
    public static class Pdf implements StreamSource {
        private final ByteArrayOutputStream os = new ByteArrayOutputStream();

        public Pdf() {
            Document document = null;

            try {
                document = new Document(PageSize.A4, 30, 5, 30, 5);
                PdfWriter.getInstance(document, os);
                document.setMarginMirroring(true);
                document.open();
                
                Paragraph companyTitleParagraph = new Paragraph();
                Font companyTitleFont = new Font();
                companyTitleFont.setSize(16);
                companyTitleParagraph.setFont(companyTitleFont);
                companyTitleParagraph.add(themeDisplay.getCompany().getName());
                companyTitleParagraph.setAlignment(Element.ALIGN_CENTER);
                companyTitleParagraph.setSpacingAfter(10);
                document.add(companyTitleParagraph);
                
                Paragraph titleParagraph = new Paragraph();
                Font titleFont = new Font();
                titleFont.setSize(14);
                titleParagraph.setFont(titleFont);
                titleParagraph.add("Work Report");
                titleParagraph.setAlignment(Element.ALIGN_CENTER);
                titleParagraph.setSpacingAfter(10);
                document.add(titleParagraph);
                
                Paragraph dateParagraph = new Paragraph();
                Font dateFont = new Font();
                dateFont.setSize(14);
                dateParagraph.setFont(dateFont);
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
                Date start = (Date) startDateField.getValue();
                Date end = (Date) endDateField.getValue();
//                Employee employee = EmployeeLocalServiceUtil.findByUserId(themeDisplay.getUser().getUserId());
                dateParagraph.add(dateFormat.format(start)+" to "+dateFormat.format(end));
                dateParagraph.setAlignment(Element.ALIGN_CENTER);
                dateParagraph.setSpacingAfter(10);
                document.add(dateParagraph);
                
                PdfPTable detailTable = new PdfPTable(2);
                detailTable.setSpacingAfter(30);
                detailTable.setHorizontalAlignment(Element.ALIGN_CENTER);
                PdfPCell detailCell;
                
                Employee employee = EmployeeLocalServiceUtil.findByUserId(themeDisplay.getUser().getUserId());
                
                detailCell = new PdfPCell(new Phrase("Employee Id: "+employee.getEmployeeId()));
                detailCell.setBorder(PdfPCell.NO_BORDER);
                detailTable.addCell(detailCell);
                
                
                Designation designation = DesignationLocalServiceUtil.fetchDesignation(employee.getDesignation());
                
                detailCell = new PdfPCell(new Phrase("Designation: "+designation.getDesignationTitle()));
                detailCell.setBorder(PdfPCell.NO_BORDER);
                detailTable.addCell(detailCell);
                
                detailCell = new PdfPCell(new Phrase("Name: "+employee.getName())); 
                detailCell.setBorder(PdfPCell.NO_BORDER);
                detailTable.addCell(detailCell);
                
                List<EmployeeMobile> mobiles = EmployeeMobileLocalServiceUtil.findByEmployeeStatus(employee.getEmployeeId(), 1);
                Phrase mobile = null;
                if (mobiles.size()>0) {
                	for (int i = 0; i < 1; i++) {
    					EmployeeMobile employeeMobile = mobiles.get(i);
    					detailCell = new PdfPCell(new Phrase("Mobile: "+employeeMobile.getMobile()));
    				}
				}else{
					detailCell = new PdfPCell(new Phrase("Mobile: "));
				}
                
				detailCell.setBorder(PdfPCell.NO_BORDER);
                detailTable.addCell(detailCell);
                document.add(detailTable);
                float[] colsWidth = {3f, 1.5f, 1.5f};
                PdfPTable table = new PdfPTable(colsWidth);
                PdfPCell cell;
                
                cell = new PdfPCell(new Phrase("Task"));
                cell.setBackgroundColor(Color.LIGHT_GRAY);
                cell.setPadding(5);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("Time Spent"));
                cell.setBackgroundColor(Color.LIGHT_GRAY);
                cell.setPadding(5);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("Average"));
                cell.setBackgroundColor(Color.LIGHT_GRAY);
                cell.setPadding(5);
                table.addCell(cell);
                
                if (reportContainer!=null) {
					for (int i = 0; i < reportContainer.size(); i++) {
						WorkReportDto workReport = reportContainer.getIdByIndex(i);
						cell = new PdfPCell(new Phrase(workReport.getTask()));
						cell.setPadding(5);
						table.addCell(cell);
						cell = new PdfPCell(new Phrase(workReport.getTime()));
						cell.setPadding(5);
						table.addCell(cell);
						cell = new PdfPCell(new Phrase(workReport.getAverage()));
						cell.setPadding(5);
						table.addCell(cell);
					}
					
					UserDto user = (UserDto) employeeComboBox.getValue();
					
					DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(DailyWork.class);
					Date startDay = (Date) startDateField.getValue();
					startDay.setHours(0);
					startDay.setMinutes(0);
					startDay.setSeconds(0);
					Date endDay = (Date) endDateField.getValue();
					endDay.setHours(0);
					endDay.setMinutes(0);
					endDay.setSeconds(0);
					Criterion criterion = RestrictionsFactoryUtil.between("worKingDay", startDay, endDay);
					
					Employee emp = EmployeeLocalServiceUtil.findByUserId(themeDisplay.getUser().getUserId());
					dynamicQuery.add(criterion).add(PropertyFactoryUtil.forName("employeeId").eq(user.getUserId()));
					
					List<DailyWork> dailyWorks = DailyWorkLocalServiceUtil.dynamicQuery(dynamicQuery);
					
					Set<Long> taskIdList = new HashSet<Long>();
 					
					for (DailyWork dailyWork : dailyWorks) {
						taskIdList.add(dailyWork.getTaskId());
					}
					
					int totalSpentTime = 0;
//					int rowCount = 0;
					
					
					
					for (Long id : taskIdList) {
						DynamicQuery timQeuery = DynamicQueryFactoryUtil.forClass(DailyWork.class);
						if (user!=null) {
							timQeuery.add(criterion).add(PropertyFactoryUtil.forName("employeeId").eq(user.getUserId()))
							.add(PropertyFactoryUtil.forName("taskId").eq(id)).setProjection(ProjectionFactoryUtil.sum("time"));
						}
						
						
						List timeList = DailyWorkLocalServiceUtil.dynamicQuery(timQeuery);
						int time = 0;
						if(timeList != null)
						{
							time = ((Long)timeList.get(0)).intValue();
						}
						
						totalSpentTime = totalSpentTime + time;
						
						
						
						DynamicQuery avgTimQeuery = DynamicQueryFactoryUtil.forClass(DailyWork.class);
						if (user!=null) {
							avgTimQeuery.add(criterion).add(PropertyFactoryUtil.forName("employeeId").eq(user.getUserId()))
							.add(PropertyFactoryUtil.forName("taskId").eq(id)).setProjection(ProjectionFactoryUtil.rowCount());
						}
						
						
						List totalRows = DailyWorkLocalServiceUtil.dynamicQuery(avgTimQeuery);
						
						int rows = 0;
						if (totalRows!=null) {
							rows = ((Long)totalRows.get(0)).intValue();
						}
						
//						rowCount = rows + rowCount;
					}
					
					cell = new PdfPCell(new Phrase("Total"));
	                cell.setBackgroundColor(Color.LIGHT_GRAY);
	                cell.setPadding(5);
	                table.addCell(cell);
	                
	                int quotient = 0;
	                int reminder = 0;
	                
	                
	                if (totalSpentTime>0) {
						quotient = totalSpentTime/60;
						reminder = totalSpentTime%60;
					}
	                
	                if (reminder<=9) {
	                	cell = new PdfPCell(new Phrase(quotient+":0"+reminder));
					}else{
						cell = new PdfPCell(new Phrase(quotient+":"+reminder));
					}
	                cell.setBackgroundColor(Color.LIGHT_GRAY);
	                cell.setPadding(5);
	                table.addCell(cell);
	                
	                int perDayQuotient = 0;
	    			int perDayReminder = 0;
	    			double perDayAvg = 0;
	    			if (totalSpentTime>0) {
	    				perDayAvg = totalSpentTime/dateList.size();
	    				
	    				perDayQuotient = (int) (perDayAvg/60);
	    				perDayReminder = (int) (perDayAvg%60);
	    			}
	                
	                if (perDayReminder<=9) {
	                	cell = new PdfPCell(new Phrase(perDayQuotient+":0"+perDayReminder+" (per day)"));
					}else{
						cell = new PdfPCell(new Phrase(perDayQuotient+":"+perDayReminder+" (per day)"));
					}
	                cell.setBackgroundColor(Color.LIGHT_GRAY);
	                cell.setPadding(5);
	                table.addCell(cell);
	                
					document.add(table);
				}
                
                
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (document != null) {
                    document.close();
                }
            }
        }
        
        public InputStream getStream() {
            // Here we return the pdf contents as a byte-array
            return new ByteArrayInputStream(os.toByteArray());
        }
    }

}

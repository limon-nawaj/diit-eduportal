package info.diit.portal.dailyWork.dto;

import java.util.Date;

import info.diit.portal.dailyWork.enums.Gender;

public class UserDto {

	private long userId;
	private String name;
	private Date dateOfBirth;
	private int gender;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String toString(){
		return getUserId()+"-"+getName();
	}
}

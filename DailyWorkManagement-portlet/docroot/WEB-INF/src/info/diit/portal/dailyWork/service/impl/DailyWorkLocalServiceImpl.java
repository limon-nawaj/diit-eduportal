/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.dailyWork.service.impl;

import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import info.diit.portal.dailyWork.NoSuchDailyWorkException;
import info.diit.portal.dailyWork.model.DailyWork;
import info.diit.portal.dailyWork.service.base.DailyWorkLocalServiceBaseImpl;
import info.diit.portal.dailyWork.service.persistence.DailyWorkUtil;

/**
 * The implementation of the daily work local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.dailyWork.service.DailyWorkLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see info.diit.portal.dailyWork.service.base.DailyWorkLocalServiceBaseImpl
 * @see info.diit.portal.dailyWork.service.DailyWorkLocalServiceUtil
 */
public class DailyWorkLocalServiceImpl extends DailyWorkLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.dailyWork.service.DailyWorkLocalServiceUtil} to access the daily work local service.
	 */
	public List<DailyWork> findByEmployeeWorkingDay(long employeeId, Date date) throws SystemException{
		return DailyWorkUtil.findByEmployeeWorkingDay(employeeId, date);
	}
	
	public DailyWork findByEmployeeTaskWorkingDay(long employeeId, long taskId, Date date) throws SystemException, NoSuchDailyWorkException{
		return DailyWorkUtil.findByEmployeeTaskWorkingDay(employeeId, taskId, date);
	}
	
	public List<DailyWork> findByTask(long taskId) throws SystemException{
		return DailyWorkUtil.findByTask(taskId);
	}
	
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.dailyWork.service.impl;

import info.diit.portal.dailyWork.NoSuchTaskDesignationException;
import info.diit.portal.dailyWork.model.TaskDesignation;
import info.diit.portal.dailyWork.service.base.TaskDesignationLocalServiceBaseImpl;
import info.diit.portal.dailyWork.service.persistence.TaskDesignationUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the task designation local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.dailyWork.service.TaskDesignationLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see info.diit.portal.dailyWork.service.base.TaskDesignationLocalServiceBaseImpl
 * @see info.diit.portal.dailyWork.service.TaskDesignationLocalServiceUtil
 */
public class TaskDesignationLocalServiceImpl
	extends TaskDesignationLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.dailyWork.service.TaskDesignationLocalServiceUtil} to access the task designation local service.
	 */
	public List<TaskDesignation> findByDesignation(long designationId) throws SystemException{
		return TaskDesignationUtil.findByDesignation(designationId);
	}
	
	public TaskDesignation findByDesignationTask(long designationId, long taskId) throws NoSuchTaskDesignationException, SystemException{
		return TaskDesignationUtil.findByDesignationTask(designationId, taskId);
	}
}
package info.diit.portal.dailyWork.enums;

public enum Status {

	PRESENT_EMPLOYEE(1, "Present Employee"), X_EMPLOYEE(2, "X-Employee");
	
	private int key;
	private String value;
	
	private Status(int key, String value){
		this.key = key;
		this.value = value;
	}
	
	public int getKey() {
		return key;
	}
	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return value;
	}
	
	public static Status getStatus(int key){
		Status statuses[] = Status.values();
		for (Status status : statuses) {
			if (status.key==key) {
				return status;
			}
		}
		return null;
	}
}

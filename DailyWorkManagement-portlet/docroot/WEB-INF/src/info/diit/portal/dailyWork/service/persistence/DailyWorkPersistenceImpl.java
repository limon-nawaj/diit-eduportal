/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.dailyWork.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.dailyWork.NoSuchDailyWorkException;
import info.diit.portal.dailyWork.model.DailyWork;
import info.diit.portal.dailyWork.model.impl.DailyWorkImpl;
import info.diit.portal.dailyWork.model.impl.DailyWorkModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * The persistence implementation for the daily work service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see DailyWorkPersistence
 * @see DailyWorkUtil
 * @generated
 */
public class DailyWorkPersistenceImpl extends BasePersistenceImpl<DailyWork>
	implements DailyWorkPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link DailyWorkUtil} to access the daily work persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = DailyWorkImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEE = new FinderPath(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkModelImpl.FINDER_CACHE_ENABLED, DailyWorkImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByEmployee",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE =
		new FinderPath(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkModelImpl.FINDER_CACHE_ENABLED, DailyWorkImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByEmployee",
			new String[] { Long.class.getName() },
			DailyWorkModelImpl.EMPLOYEEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMPLOYEE = new FinderPath(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEmployee",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEEWORKINGDAY =
		new FinderPath(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkModelImpl.FINDER_CACHE_ENABLED, DailyWorkImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByEmployeeWorkingDay",
			new String[] {
				Long.class.getName(), Date.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEWORKINGDAY =
		new FinderPath(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkModelImpl.FINDER_CACHE_ENABLED, DailyWorkImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByEmployeeWorkingDay",
			new String[] { Long.class.getName(), Date.class.getName() },
			DailyWorkModelImpl.EMPLOYEEID_COLUMN_BITMASK |
			DailyWorkModelImpl.WORKINGDAY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMPLOYEEWORKINGDAY = new FinderPath(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByEmployeeWorkingDay",
			new String[] { Long.class.getName(), Date.class.getName() });
	public static final FinderPath FINDER_PATH_FETCH_BY_EMPLOYEETASKWORKINGDAY = new FinderPath(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkModelImpl.FINDER_CACHE_ENABLED, DailyWorkImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByEmployeeTaskWorkingDay",
			new String[] {
				Long.class.getName(), Long.class.getName(), Date.class.getName()
			},
			DailyWorkModelImpl.EMPLOYEEID_COLUMN_BITMASK |
			DailyWorkModelImpl.TASKID_COLUMN_BITMASK |
			DailyWorkModelImpl.WORKINGDAY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMPLOYEETASKWORKINGDAY = new FinderPath(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByEmployeeTaskWorkingDay",
			new String[] {
				Long.class.getName(), Long.class.getName(), Date.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TASK = new FinderPath(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkModelImpl.FINDER_CACHE_ENABLED, DailyWorkImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTask",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TASK = new FinderPath(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkModelImpl.FINDER_CACHE_ENABLED, DailyWorkImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTask",
			new String[] { Long.class.getName() },
			DailyWorkModelImpl.TASKID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TASK = new FinderPath(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTask",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkModelImpl.FINDER_CACHE_ENABLED, DailyWorkImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkModelImpl.FINDER_CACHE_ENABLED, DailyWorkImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the daily work in the entity cache if it is enabled.
	 *
	 * @param dailyWork the daily work
	 */
	public void cacheResult(DailyWork dailyWork) {
		EntityCacheUtil.putResult(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkImpl.class, dailyWork.getPrimaryKey(), dailyWork);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEETASKWORKINGDAY,
			new Object[] {
				Long.valueOf(dailyWork.getEmployeeId()),
				Long.valueOf(dailyWork.getTaskId()),
				
			dailyWork.getWorKingDay()
			}, dailyWork);

		dailyWork.resetOriginalValues();
	}

	/**
	 * Caches the daily works in the entity cache if it is enabled.
	 *
	 * @param dailyWorks the daily works
	 */
	public void cacheResult(List<DailyWork> dailyWorks) {
		for (DailyWork dailyWork : dailyWorks) {
			if (EntityCacheUtil.getResult(
						DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
						DailyWorkImpl.class, dailyWork.getPrimaryKey()) == null) {
				cacheResult(dailyWork);
			}
			else {
				dailyWork.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all daily works.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(DailyWorkImpl.class.getName());
		}

		EntityCacheUtil.clearCache(DailyWorkImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the daily work.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(DailyWork dailyWork) {
		EntityCacheUtil.removeResult(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkImpl.class, dailyWork.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(dailyWork);
	}

	@Override
	public void clearCache(List<DailyWork> dailyWorks) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (DailyWork dailyWork : dailyWorks) {
			EntityCacheUtil.removeResult(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
				DailyWorkImpl.class, dailyWork.getPrimaryKey());

			clearUniqueFindersCache(dailyWork);
		}
	}

	protected void clearUniqueFindersCache(DailyWork dailyWork) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EMPLOYEETASKWORKINGDAY,
			new Object[] {
				Long.valueOf(dailyWork.getEmployeeId()),
				Long.valueOf(dailyWork.getTaskId()),
				
			dailyWork.getWorKingDay()
			});
	}

	/**
	 * Creates a new daily work with the primary key. Does not add the daily work to the database.
	 *
	 * @param dailyWorkId the primary key for the new daily work
	 * @return the new daily work
	 */
	public DailyWork create(long dailyWorkId) {
		DailyWork dailyWork = new DailyWorkImpl();

		dailyWork.setNew(true);
		dailyWork.setPrimaryKey(dailyWorkId);

		return dailyWork;
	}

	/**
	 * Removes the daily work with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param dailyWorkId the primary key of the daily work
	 * @return the daily work that was removed
	 * @throws info.diit.portal.dailyWork.NoSuchDailyWorkException if a daily work with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork remove(long dailyWorkId)
		throws NoSuchDailyWorkException, SystemException {
		return remove(Long.valueOf(dailyWorkId));
	}

	/**
	 * Removes the daily work with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the daily work
	 * @return the daily work that was removed
	 * @throws info.diit.portal.dailyWork.NoSuchDailyWorkException if a daily work with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DailyWork remove(Serializable primaryKey)
		throws NoSuchDailyWorkException, SystemException {
		Session session = null;

		try {
			session = openSession();

			DailyWork dailyWork = (DailyWork)session.get(DailyWorkImpl.class,
					primaryKey);

			if (dailyWork == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDailyWorkException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(dailyWork);
		}
		catch (NoSuchDailyWorkException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected DailyWork removeImpl(DailyWork dailyWork)
		throws SystemException {
		dailyWork = toUnwrappedModel(dailyWork);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, dailyWork);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(dailyWork);

		return dailyWork;
	}

	@Override
	public DailyWork updateImpl(
		info.diit.portal.dailyWork.model.DailyWork dailyWork, boolean merge)
		throws SystemException {
		dailyWork = toUnwrappedModel(dailyWork);

		boolean isNew = dailyWork.isNew();

		DailyWorkModelImpl dailyWorkModelImpl = (DailyWorkModelImpl)dailyWork;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, dailyWork, merge);

			dailyWork.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !DailyWorkModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((dailyWorkModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(dailyWorkModelImpl.getOriginalEmployeeId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE,
					args);

				args = new Object[] {
						Long.valueOf(dailyWorkModelImpl.getEmployeeId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE,
					args);
			}

			if ((dailyWorkModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEWORKINGDAY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(dailyWorkModelImpl.getOriginalEmployeeId()),
						
						dailyWorkModelImpl.getOriginalWorKingDay()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEEWORKINGDAY,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEWORKINGDAY,
					args);

				args = new Object[] {
						Long.valueOf(dailyWorkModelImpl.getEmployeeId()),
						
						dailyWorkModelImpl.getWorKingDay()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEEWORKINGDAY,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEWORKINGDAY,
					args);
			}

			if ((dailyWorkModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TASK.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(dailyWorkModelImpl.getOriginalTaskId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TASK, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TASK,
					args);

				args = new Object[] { Long.valueOf(dailyWorkModelImpl.getTaskId()) };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TASK, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TASK,
					args);
			}
		}

		EntityCacheUtil.putResult(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
			DailyWorkImpl.class, dailyWork.getPrimaryKey(), dailyWork);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEETASKWORKINGDAY,
				new Object[] {
					Long.valueOf(dailyWork.getEmployeeId()),
					Long.valueOf(dailyWork.getTaskId()),
					
				dailyWork.getWorKingDay()
				}, dailyWork);
		}
		else {
			if ((dailyWorkModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_EMPLOYEETASKWORKINGDAY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(dailyWorkModelImpl.getOriginalEmployeeId()),
						Long.valueOf(dailyWorkModelImpl.getOriginalTaskId()),
						
						dailyWorkModelImpl.getOriginalWorKingDay()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEETASKWORKINGDAY,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EMPLOYEETASKWORKINGDAY,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEETASKWORKINGDAY,
					new Object[] {
						Long.valueOf(dailyWork.getEmployeeId()),
						Long.valueOf(dailyWork.getTaskId()),
						
					dailyWork.getWorKingDay()
					}, dailyWork);
			}
		}

		return dailyWork;
	}

	protected DailyWork toUnwrappedModel(DailyWork dailyWork) {
		if (dailyWork instanceof DailyWorkImpl) {
			return dailyWork;
		}

		DailyWorkImpl dailyWorkImpl = new DailyWorkImpl();

		dailyWorkImpl.setNew(dailyWork.isNew());
		dailyWorkImpl.setPrimaryKey(dailyWork.getPrimaryKey());

		dailyWorkImpl.setDailyWorkId(dailyWork.getDailyWorkId());
		dailyWorkImpl.setCompanyId(dailyWork.getCompanyId());
		dailyWorkImpl.setUserId(dailyWork.getUserId());
		dailyWorkImpl.setUserName(dailyWork.getUserName());
		dailyWorkImpl.setCreateDate(dailyWork.getCreateDate());
		dailyWorkImpl.setModifiedDate(dailyWork.getModifiedDate());
		dailyWorkImpl.setEmployeeId(dailyWork.getEmployeeId());
		dailyWorkImpl.setWorKingDay(dailyWork.getWorKingDay());
		dailyWorkImpl.setTaskId(dailyWork.getTaskId());
		dailyWorkImpl.setTime(dailyWork.getTime());
		dailyWorkImpl.setNote(dailyWork.getNote());

		return dailyWorkImpl;
	}

	/**
	 * Returns the daily work with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the daily work
	 * @return the daily work
	 * @throws com.liferay.portal.NoSuchModelException if a daily work with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DailyWork findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the daily work with the primary key or throws a {@link info.diit.portal.dailyWork.NoSuchDailyWorkException} if it could not be found.
	 *
	 * @param dailyWorkId the primary key of the daily work
	 * @return the daily work
	 * @throws info.diit.portal.dailyWork.NoSuchDailyWorkException if a daily work with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork findByPrimaryKey(long dailyWorkId)
		throws NoSuchDailyWorkException, SystemException {
		DailyWork dailyWork = fetchByPrimaryKey(dailyWorkId);

		if (dailyWork == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + dailyWorkId);
			}

			throw new NoSuchDailyWorkException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				dailyWorkId);
		}

		return dailyWork;
	}

	/**
	 * Returns the daily work with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the daily work
	 * @return the daily work, or <code>null</code> if a daily work with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public DailyWork fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the daily work with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param dailyWorkId the primary key of the daily work
	 * @return the daily work, or <code>null</code> if a daily work with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork fetchByPrimaryKey(long dailyWorkId)
		throws SystemException {
		DailyWork dailyWork = (DailyWork)EntityCacheUtil.getResult(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
				DailyWorkImpl.class, dailyWorkId);

		if (dailyWork == _nullDailyWork) {
			return null;
		}

		if (dailyWork == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				dailyWork = (DailyWork)session.get(DailyWorkImpl.class,
						Long.valueOf(dailyWorkId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (dailyWork != null) {
					cacheResult(dailyWork);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(DailyWorkModelImpl.ENTITY_CACHE_ENABLED,
						DailyWorkImpl.class, dailyWorkId, _nullDailyWork);
				}

				closeSession(session);
			}
		}

		return dailyWork;
	}

	/**
	 * Returns all the daily works where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @return the matching daily works
	 * @throws SystemException if a system exception occurred
	 */
	public List<DailyWork> findByEmployee(long employeeId)
		throws SystemException {
		return findByEmployee(employeeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the daily works where employeeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employeeId the employee ID
	 * @param start the lower bound of the range of daily works
	 * @param end the upper bound of the range of daily works (not inclusive)
	 * @return the range of matching daily works
	 * @throws SystemException if a system exception occurred
	 */
	public List<DailyWork> findByEmployee(long employeeId, int start, int end)
		throws SystemException {
		return findByEmployee(employeeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the daily works where employeeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employeeId the employee ID
	 * @param start the lower bound of the range of daily works
	 * @param end the upper bound of the range of daily works (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching daily works
	 * @throws SystemException if a system exception occurred
	 */
	public List<DailyWork> findByEmployee(long employeeId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE;
			finderArgs = new Object[] { employeeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEE;
			finderArgs = new Object[] { employeeId, start, end, orderByComparator };
		}

		List<DailyWork> list = (List<DailyWork>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (DailyWork dailyWork : list) {
				if ((employeeId != dailyWork.getEmployeeId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DAILYWORK_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(DailyWorkModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				list = (List<DailyWork>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first daily work in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching daily work
	 * @throws info.diit.portal.dailyWork.NoSuchDailyWorkException if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork findByEmployee_First(long employeeId,
		OrderByComparator orderByComparator)
		throws NoSuchDailyWorkException, SystemException {
		DailyWork dailyWork = fetchByEmployee_First(employeeId,
				orderByComparator);

		if (dailyWork != null) {
			return dailyWork;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employeeId=");
		msg.append(employeeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDailyWorkException(msg.toString());
	}

	/**
	 * Returns the first daily work in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching daily work, or <code>null</code> if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork fetchByEmployee_First(long employeeId,
		OrderByComparator orderByComparator) throws SystemException {
		List<DailyWork> list = findByEmployee(employeeId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last daily work in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching daily work
	 * @throws info.diit.portal.dailyWork.NoSuchDailyWorkException if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork findByEmployee_Last(long employeeId,
		OrderByComparator orderByComparator)
		throws NoSuchDailyWorkException, SystemException {
		DailyWork dailyWork = fetchByEmployee_Last(employeeId, orderByComparator);

		if (dailyWork != null) {
			return dailyWork;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employeeId=");
		msg.append(employeeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDailyWorkException(msg.toString());
	}

	/**
	 * Returns the last daily work in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching daily work, or <code>null</code> if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork fetchByEmployee_Last(long employeeId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByEmployee(employeeId);

		List<DailyWork> list = findByEmployee(employeeId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the daily works before and after the current daily work in the ordered set where employeeId = &#63;.
	 *
	 * @param dailyWorkId the primary key of the current daily work
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next daily work
	 * @throws info.diit.portal.dailyWork.NoSuchDailyWorkException if a daily work with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork[] findByEmployee_PrevAndNext(long dailyWorkId,
		long employeeId, OrderByComparator orderByComparator)
		throws NoSuchDailyWorkException, SystemException {
		DailyWork dailyWork = findByPrimaryKey(dailyWorkId);

		Session session = null;

		try {
			session = openSession();

			DailyWork[] array = new DailyWorkImpl[3];

			array[0] = getByEmployee_PrevAndNext(session, dailyWork,
					employeeId, orderByComparator, true);

			array[1] = dailyWork;

			array[2] = getByEmployee_PrevAndNext(session, dailyWork,
					employeeId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DailyWork getByEmployee_PrevAndNext(Session session,
		DailyWork dailyWork, long employeeId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DAILYWORK_WHERE);

		query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(DailyWorkModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(employeeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(dailyWork);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DailyWork> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the daily works where employeeId = &#63; and worKingDay = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param worKingDay the wor king day
	 * @return the matching daily works
	 * @throws SystemException if a system exception occurred
	 */
	public List<DailyWork> findByEmployeeWorkingDay(long employeeId,
		Date worKingDay) throws SystemException {
		return findByEmployeeWorkingDay(employeeId, worKingDay,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the daily works where employeeId = &#63; and worKingDay = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employeeId the employee ID
	 * @param worKingDay the wor king day
	 * @param start the lower bound of the range of daily works
	 * @param end the upper bound of the range of daily works (not inclusive)
	 * @return the range of matching daily works
	 * @throws SystemException if a system exception occurred
	 */
	public List<DailyWork> findByEmployeeWorkingDay(long employeeId,
		Date worKingDay, int start, int end) throws SystemException {
		return findByEmployeeWorkingDay(employeeId, worKingDay, start, end, null);
	}

	/**
	 * Returns an ordered range of all the daily works where employeeId = &#63; and worKingDay = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employeeId the employee ID
	 * @param worKingDay the wor king day
	 * @param start the lower bound of the range of daily works
	 * @param end the upper bound of the range of daily works (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching daily works
	 * @throws SystemException if a system exception occurred
	 */
	public List<DailyWork> findByEmployeeWorkingDay(long employeeId,
		Date worKingDay, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEEWORKINGDAY;
			finderArgs = new Object[] { employeeId, worKingDay };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEEWORKINGDAY;
			finderArgs = new Object[] {
					employeeId, worKingDay,
					
					start, end, orderByComparator
				};
		}

		List<DailyWork> list = (List<DailyWork>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (DailyWork dailyWork : list) {
				if ((employeeId != dailyWork.getEmployeeId()) ||
						!Validator.equals(worKingDay, dailyWork.getWorKingDay())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_DAILYWORK_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEEWORKINGDAY_EMPLOYEEID_2);

			if (worKingDay == null) {
				query.append(_FINDER_COLUMN_EMPLOYEEWORKINGDAY_WORKINGDAY_1);
			}
			else {
				query.append(_FINDER_COLUMN_EMPLOYEEWORKINGDAY_WORKINGDAY_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(DailyWorkModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				if (worKingDay != null) {
					qPos.add(CalendarUtil.getTimestamp(worKingDay));
				}

				list = (List<DailyWork>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first daily work in the ordered set where employeeId = &#63; and worKingDay = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param worKingDay the wor king day
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching daily work
	 * @throws info.diit.portal.dailyWork.NoSuchDailyWorkException if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork findByEmployeeWorkingDay_First(long employeeId,
		Date worKingDay, OrderByComparator orderByComparator)
		throws NoSuchDailyWorkException, SystemException {
		DailyWork dailyWork = fetchByEmployeeWorkingDay_First(employeeId,
				worKingDay, orderByComparator);

		if (dailyWork != null) {
			return dailyWork;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employeeId=");
		msg.append(employeeId);

		msg.append(", worKingDay=");
		msg.append(worKingDay);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDailyWorkException(msg.toString());
	}

	/**
	 * Returns the first daily work in the ordered set where employeeId = &#63; and worKingDay = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param worKingDay the wor king day
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching daily work, or <code>null</code> if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork fetchByEmployeeWorkingDay_First(long employeeId,
		Date worKingDay, OrderByComparator orderByComparator)
		throws SystemException {
		List<DailyWork> list = findByEmployeeWorkingDay(employeeId, worKingDay,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last daily work in the ordered set where employeeId = &#63; and worKingDay = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param worKingDay the wor king day
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching daily work
	 * @throws info.diit.portal.dailyWork.NoSuchDailyWorkException if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork findByEmployeeWorkingDay_Last(long employeeId,
		Date worKingDay, OrderByComparator orderByComparator)
		throws NoSuchDailyWorkException, SystemException {
		DailyWork dailyWork = fetchByEmployeeWorkingDay_Last(employeeId,
				worKingDay, orderByComparator);

		if (dailyWork != null) {
			return dailyWork;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employeeId=");
		msg.append(employeeId);

		msg.append(", worKingDay=");
		msg.append(worKingDay);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDailyWorkException(msg.toString());
	}

	/**
	 * Returns the last daily work in the ordered set where employeeId = &#63; and worKingDay = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param worKingDay the wor king day
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching daily work, or <code>null</code> if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork fetchByEmployeeWorkingDay_Last(long employeeId,
		Date worKingDay, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByEmployeeWorkingDay(employeeId, worKingDay);

		List<DailyWork> list = findByEmployeeWorkingDay(employeeId, worKingDay,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the daily works before and after the current daily work in the ordered set where employeeId = &#63; and worKingDay = &#63;.
	 *
	 * @param dailyWorkId the primary key of the current daily work
	 * @param employeeId the employee ID
	 * @param worKingDay the wor king day
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next daily work
	 * @throws info.diit.portal.dailyWork.NoSuchDailyWorkException if a daily work with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork[] findByEmployeeWorkingDay_PrevAndNext(long dailyWorkId,
		long employeeId, Date worKingDay, OrderByComparator orderByComparator)
		throws NoSuchDailyWorkException, SystemException {
		DailyWork dailyWork = findByPrimaryKey(dailyWorkId);

		Session session = null;

		try {
			session = openSession();

			DailyWork[] array = new DailyWorkImpl[3];

			array[0] = getByEmployeeWorkingDay_PrevAndNext(session, dailyWork,
					employeeId, worKingDay, orderByComparator, true);

			array[1] = dailyWork;

			array[2] = getByEmployeeWorkingDay_PrevAndNext(session, dailyWork,
					employeeId, worKingDay, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DailyWork getByEmployeeWorkingDay_PrevAndNext(Session session,
		DailyWork dailyWork, long employeeId, Date worKingDay,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DAILYWORK_WHERE);

		query.append(_FINDER_COLUMN_EMPLOYEEWORKINGDAY_EMPLOYEEID_2);

		if (worKingDay == null) {
			query.append(_FINDER_COLUMN_EMPLOYEEWORKINGDAY_WORKINGDAY_1);
		}
		else {
			query.append(_FINDER_COLUMN_EMPLOYEEWORKINGDAY_WORKINGDAY_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(DailyWorkModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(employeeId);

		if (worKingDay != null) {
			qPos.add(CalendarUtil.getTimestamp(worKingDay));
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(dailyWork);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DailyWork> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns the daily work where employeeId = &#63; and taskId = &#63; and worKingDay = &#63; or throws a {@link info.diit.portal.dailyWork.NoSuchDailyWorkException} if it could not be found.
	 *
	 * @param employeeId the employee ID
	 * @param taskId the task ID
	 * @param worKingDay the wor king day
	 * @return the matching daily work
	 * @throws info.diit.portal.dailyWork.NoSuchDailyWorkException if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork findByEmployeeTaskWorkingDay(long employeeId, long taskId,
		Date worKingDay) throws NoSuchDailyWorkException, SystemException {
		DailyWork dailyWork = fetchByEmployeeTaskWorkingDay(employeeId, taskId,
				worKingDay);

		if (dailyWork == null) {
			StringBundler msg = new StringBundler(8);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("employeeId=");
			msg.append(employeeId);

			msg.append(", taskId=");
			msg.append(taskId);

			msg.append(", worKingDay=");
			msg.append(worKingDay);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchDailyWorkException(msg.toString());
		}

		return dailyWork;
	}

	/**
	 * Returns the daily work where employeeId = &#63; and taskId = &#63; and worKingDay = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param employeeId the employee ID
	 * @param taskId the task ID
	 * @param worKingDay the wor king day
	 * @return the matching daily work, or <code>null</code> if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork fetchByEmployeeTaskWorkingDay(long employeeId,
		long taskId, Date worKingDay) throws SystemException {
		return fetchByEmployeeTaskWorkingDay(employeeId, taskId, worKingDay,
			true);
	}

	/**
	 * Returns the daily work where employeeId = &#63; and taskId = &#63; and worKingDay = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param employeeId the employee ID
	 * @param taskId the task ID
	 * @param worKingDay the wor king day
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching daily work, or <code>null</code> if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork fetchByEmployeeTaskWorkingDay(long employeeId,
		long taskId, Date worKingDay, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { employeeId, taskId, worKingDay };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_EMPLOYEETASKWORKINGDAY,
					finderArgs, this);
		}

		if (result instanceof DailyWork) {
			DailyWork dailyWork = (DailyWork)result;

			if ((employeeId != dailyWork.getEmployeeId()) ||
					(taskId != dailyWork.getTaskId()) ||
					!Validator.equals(worKingDay, dailyWork.getWorKingDay())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(5);

			query.append(_SQL_SELECT_DAILYWORK_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEETASKWORKINGDAY_EMPLOYEEID_2);

			query.append(_FINDER_COLUMN_EMPLOYEETASKWORKINGDAY_TASKID_2);

			if (worKingDay == null) {
				query.append(_FINDER_COLUMN_EMPLOYEETASKWORKINGDAY_WORKINGDAY_1);
			}
			else {
				query.append(_FINDER_COLUMN_EMPLOYEETASKWORKINGDAY_WORKINGDAY_2);
			}

			query.append(DailyWorkModelImpl.ORDER_BY_JPQL);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				qPos.add(taskId);

				if (worKingDay != null) {
					qPos.add(CalendarUtil.getTimestamp(worKingDay));
				}

				List<DailyWork> list = q.list();

				result = list;

				DailyWork dailyWork = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEETASKWORKINGDAY,
						finderArgs, list);
				}
				else {
					dailyWork = list.get(0);

					cacheResult(dailyWork);

					if ((dailyWork.getEmployeeId() != employeeId) ||
							(dailyWork.getTaskId() != taskId) ||
							(dailyWork.getWorKingDay() == null) ||
							!dailyWork.getWorKingDay().equals(worKingDay)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEETASKWORKINGDAY,
							finderArgs, dailyWork);
					}
				}

				return dailyWork;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EMPLOYEETASKWORKINGDAY,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (DailyWork)result;
			}
		}
	}

	/**
	 * Returns all the daily works where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @return the matching daily works
	 * @throws SystemException if a system exception occurred
	 */
	public List<DailyWork> findByTask(long taskId) throws SystemException {
		return findByTask(taskId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the daily works where taskId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param taskId the task ID
	 * @param start the lower bound of the range of daily works
	 * @param end the upper bound of the range of daily works (not inclusive)
	 * @return the range of matching daily works
	 * @throws SystemException if a system exception occurred
	 */
	public List<DailyWork> findByTask(long taskId, int start, int end)
		throws SystemException {
		return findByTask(taskId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the daily works where taskId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param taskId the task ID
	 * @param start the lower bound of the range of daily works
	 * @param end the upper bound of the range of daily works (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching daily works
	 * @throws SystemException if a system exception occurred
	 */
	public List<DailyWork> findByTask(long taskId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TASK;
			finderArgs = new Object[] { taskId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TASK;
			finderArgs = new Object[] { taskId, start, end, orderByComparator };
		}

		List<DailyWork> list = (List<DailyWork>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (DailyWork dailyWork : list) {
				if ((taskId != dailyWork.getTaskId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DAILYWORK_WHERE);

			query.append(_FINDER_COLUMN_TASK_TASKID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(DailyWorkModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(taskId);

				list = (List<DailyWork>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first daily work in the ordered set where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching daily work
	 * @throws info.diit.portal.dailyWork.NoSuchDailyWorkException if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork findByTask_First(long taskId,
		OrderByComparator orderByComparator)
		throws NoSuchDailyWorkException, SystemException {
		DailyWork dailyWork = fetchByTask_First(taskId, orderByComparator);

		if (dailyWork != null) {
			return dailyWork;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("taskId=");
		msg.append(taskId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDailyWorkException(msg.toString());
	}

	/**
	 * Returns the first daily work in the ordered set where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching daily work, or <code>null</code> if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork fetchByTask_First(long taskId,
		OrderByComparator orderByComparator) throws SystemException {
		List<DailyWork> list = findByTask(taskId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last daily work in the ordered set where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching daily work
	 * @throws info.diit.portal.dailyWork.NoSuchDailyWorkException if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork findByTask_Last(long taskId,
		OrderByComparator orderByComparator)
		throws NoSuchDailyWorkException, SystemException {
		DailyWork dailyWork = fetchByTask_Last(taskId, orderByComparator);

		if (dailyWork != null) {
			return dailyWork;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("taskId=");
		msg.append(taskId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchDailyWorkException(msg.toString());
	}

	/**
	 * Returns the last daily work in the ordered set where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching daily work, or <code>null</code> if a matching daily work could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork fetchByTask_Last(long taskId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByTask(taskId);

		List<DailyWork> list = findByTask(taskId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the daily works before and after the current daily work in the ordered set where taskId = &#63;.
	 *
	 * @param dailyWorkId the primary key of the current daily work
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next daily work
	 * @throws info.diit.portal.dailyWork.NoSuchDailyWorkException if a daily work with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork[] findByTask_PrevAndNext(long dailyWorkId, long taskId,
		OrderByComparator orderByComparator)
		throws NoSuchDailyWorkException, SystemException {
		DailyWork dailyWork = findByPrimaryKey(dailyWorkId);

		Session session = null;

		try {
			session = openSession();

			DailyWork[] array = new DailyWorkImpl[3];

			array[0] = getByTask_PrevAndNext(session, dailyWork, taskId,
					orderByComparator, true);

			array[1] = dailyWork;

			array[2] = getByTask_PrevAndNext(session, dailyWork, taskId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DailyWork getByTask_PrevAndNext(Session session,
		DailyWork dailyWork, long taskId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DAILYWORK_WHERE);

		query.append(_FINDER_COLUMN_TASK_TASKID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(DailyWorkModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(taskId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(dailyWork);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DailyWork> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the daily works.
	 *
	 * @return the daily works
	 * @throws SystemException if a system exception occurred
	 */
	public List<DailyWork> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the daily works.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of daily works
	 * @param end the upper bound of the range of daily works (not inclusive)
	 * @return the range of daily works
	 * @throws SystemException if a system exception occurred
	 */
	public List<DailyWork> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the daily works.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of daily works
	 * @param end the upper bound of the range of daily works (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of daily works
	 * @throws SystemException if a system exception occurred
	 */
	public List<DailyWork> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<DailyWork> list = (List<DailyWork>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_DAILYWORK);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DAILYWORK.concat(DailyWorkModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<DailyWork>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<DailyWork>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the daily works where employeeId = &#63; from the database.
	 *
	 * @param employeeId the employee ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByEmployee(long employeeId) throws SystemException {
		for (DailyWork dailyWork : findByEmployee(employeeId)) {
			remove(dailyWork);
		}
	}

	/**
	 * Removes all the daily works where employeeId = &#63; and worKingDay = &#63; from the database.
	 *
	 * @param employeeId the employee ID
	 * @param worKingDay the wor king day
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByEmployeeWorkingDay(long employeeId, Date worKingDay)
		throws SystemException {
		for (DailyWork dailyWork : findByEmployeeWorkingDay(employeeId,
				worKingDay)) {
			remove(dailyWork);
		}
	}

	/**
	 * Removes the daily work where employeeId = &#63; and taskId = &#63; and worKingDay = &#63; from the database.
	 *
	 * @param employeeId the employee ID
	 * @param taskId the task ID
	 * @param worKingDay the wor king day
	 * @return the daily work that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public DailyWork removeByEmployeeTaskWorkingDay(long employeeId,
		long taskId, Date worKingDay)
		throws NoSuchDailyWorkException, SystemException {
		DailyWork dailyWork = findByEmployeeTaskWorkingDay(employeeId, taskId,
				worKingDay);

		return remove(dailyWork);
	}

	/**
	 * Removes all the daily works where taskId = &#63; from the database.
	 *
	 * @param taskId the task ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByTask(long taskId) throws SystemException {
		for (DailyWork dailyWork : findByTask(taskId)) {
			remove(dailyWork);
		}
	}

	/**
	 * Removes all the daily works from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (DailyWork dailyWork : findAll()) {
			remove(dailyWork);
		}
	}

	/**
	 * Returns the number of daily works where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @return the number of matching daily works
	 * @throws SystemException if a system exception occurred
	 */
	public int countByEmployee(long employeeId) throws SystemException {
		Object[] finderArgs = new Object[] { employeeId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_EMPLOYEE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DAILYWORK_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EMPLOYEE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of daily works where employeeId = &#63; and worKingDay = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param worKingDay the wor king day
	 * @return the number of matching daily works
	 * @throws SystemException if a system exception occurred
	 */
	public int countByEmployeeWorkingDay(long employeeId, Date worKingDay)
		throws SystemException {
		Object[] finderArgs = new Object[] { employeeId, worKingDay };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_EMPLOYEEWORKINGDAY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_DAILYWORK_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEEWORKINGDAY_EMPLOYEEID_2);

			if (worKingDay == null) {
				query.append(_FINDER_COLUMN_EMPLOYEEWORKINGDAY_WORKINGDAY_1);
			}
			else {
				query.append(_FINDER_COLUMN_EMPLOYEEWORKINGDAY_WORKINGDAY_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				if (worKingDay != null) {
					qPos.add(CalendarUtil.getTimestamp(worKingDay));
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EMPLOYEEWORKINGDAY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of daily works where employeeId = &#63; and taskId = &#63; and worKingDay = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param taskId the task ID
	 * @param worKingDay the wor king day
	 * @return the number of matching daily works
	 * @throws SystemException if a system exception occurred
	 */
	public int countByEmployeeTaskWorkingDay(long employeeId, long taskId,
		Date worKingDay) throws SystemException {
		Object[] finderArgs = new Object[] { employeeId, taskId, worKingDay };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_EMPLOYEETASKWORKINGDAY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_DAILYWORK_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEETASKWORKINGDAY_EMPLOYEEID_2);

			query.append(_FINDER_COLUMN_EMPLOYEETASKWORKINGDAY_TASKID_2);

			if (worKingDay == null) {
				query.append(_FINDER_COLUMN_EMPLOYEETASKWORKINGDAY_WORKINGDAY_1);
			}
			else {
				query.append(_FINDER_COLUMN_EMPLOYEETASKWORKINGDAY_WORKINGDAY_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				qPos.add(taskId);

				if (worKingDay != null) {
					qPos.add(CalendarUtil.getTimestamp(worKingDay));
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EMPLOYEETASKWORKINGDAY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of daily works where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @return the number of matching daily works
	 * @throws SystemException if a system exception occurred
	 */
	public int countByTask(long taskId) throws SystemException {
		Object[] finderArgs = new Object[] { taskId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_TASK,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DAILYWORK_WHERE);

			query.append(_FINDER_COLUMN_TASK_TASKID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(taskId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TASK,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of daily works.
	 *
	 * @return the number of daily works
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DAILYWORK);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the daily work persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.dailyWork.model.DailyWork")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<DailyWork>> listenersList = new ArrayList<ModelListener<DailyWork>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<DailyWork>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(DailyWorkImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_DAILYWORK = "SELECT dailyWork FROM DailyWork dailyWork";
	private static final String _SQL_SELECT_DAILYWORK_WHERE = "SELECT dailyWork FROM DailyWork dailyWork WHERE ";
	private static final String _SQL_COUNT_DAILYWORK = "SELECT COUNT(dailyWork) FROM DailyWork dailyWork";
	private static final String _SQL_COUNT_DAILYWORK_WHERE = "SELECT COUNT(dailyWork) FROM DailyWork dailyWork WHERE ";
	private static final String _FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2 = "dailyWork.employeeId = ?";
	private static final String _FINDER_COLUMN_EMPLOYEEWORKINGDAY_EMPLOYEEID_2 = "dailyWork.employeeId = ? AND ";
	private static final String _FINDER_COLUMN_EMPLOYEEWORKINGDAY_WORKINGDAY_1 = "dailyWork.worKingDay IS NULL";
	private static final String _FINDER_COLUMN_EMPLOYEEWORKINGDAY_WORKINGDAY_2 = "dailyWork.worKingDay = ?";
	private static final String _FINDER_COLUMN_EMPLOYEETASKWORKINGDAY_EMPLOYEEID_2 =
		"dailyWork.employeeId = ? AND ";
	private static final String _FINDER_COLUMN_EMPLOYEETASKWORKINGDAY_TASKID_2 = "dailyWork.taskId = ? AND ";
	private static final String _FINDER_COLUMN_EMPLOYEETASKWORKINGDAY_WORKINGDAY_1 =
		"dailyWork.worKingDay IS NULL";
	private static final String _FINDER_COLUMN_EMPLOYEETASKWORKINGDAY_WORKINGDAY_2 =
		"dailyWork.worKingDay = ?";
	private static final String _FINDER_COLUMN_TASK_TASKID_2 = "dailyWork.taskId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "dailyWork.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No DailyWork exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No DailyWork exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(DailyWorkPersistenceImpl.class);
	private static DailyWork _nullDailyWork = new DailyWorkImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<DailyWork> toCacheModel() {
				return _nullDailyWorkCacheModel;
			}
		};

	private static CacheModel<DailyWork> _nullDailyWorkCacheModel = new CacheModel<DailyWork>() {
			public DailyWork toEntityModel() {
				return _nullDailyWork;
			}
		};
}
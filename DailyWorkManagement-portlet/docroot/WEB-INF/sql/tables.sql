create table EduPortal_DailyWork_DailyWork (
	dailyWorkId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	employeeId LONG,
	worKingDay DATE null,
	taskId LONG,
	time_ INTEGER,
	note STRING null
);

create table EduPortal_DailyWork_Designation (
	designationId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	designationTitle VARCHAR(75) null
);

create table EduPortal_DailyWork_Employee (
	employeeId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	employeeUserId LONG,
	name VARCHAR(75) null,
	gender LONG,
	religion LONG,
	blood LONG,
	dob DATE null,
	designation LONG,
	branch LONG,
	status LONG,
	presentAddress STRING null,
	permanentAddress STRING null,
	photo LONG
);

create table EduPortal_DailyWork_EmployeeEmail (
	emailId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	employeeId LONG,
	workEmail VARCHAR(75) null,
	personalEmail VARCHAR(75) null,
	status LONG
);

create table EduPortal_DailyWork_EmployeeMobile (
	mobileId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	employeeId LONG,
	mobile VARCHAR(75) null,
	homePhone VARCHAR(75) null,
	status LONG
);

create table EduPortal_DailyWork_Task (
	taskId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	title VARCHAR(75) null
);

create table EduPortal_DailyWork_TaskDesignation (
	taskDesignationId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	taskId LONG,
	designationId LONG
);
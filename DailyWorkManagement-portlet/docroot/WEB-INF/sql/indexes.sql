create index IX_D02DC9B on EduPortal_DailyWork_DailyWork (employeeId);
create index IX_26B9024E on EduPortal_DailyWork_DailyWork (employeeId, taskId, worKingDay);
create index IX_802BE65A on EduPortal_DailyWork_DailyWork (employeeId, worKingDay);
create index IX_36EF87F2 on EduPortal_DailyWork_DailyWork (taskId);
create index IX_D982BE3D on EduPortal_DailyWork_DailyWork (worKingDay);

create index IX_400F1DD on EduPortal_DailyWork_Designation (companyId);

create index IX_684A6E22 on EduPortal_DailyWork_Employee (companyId);
create index IX_CF3414B8 on EduPortal_DailyWork_Employee (companyId, branch);
create index IX_A700EDC4 on EduPortal_DailyWork_Employee (companyId, organizationId);
create index IX_95C3754E on EduPortal_DailyWork_Employee (employeeUserId);

create index IX_BF6C9B5F on EduPortal_DailyWork_EmployeeEmail (employeeId);

create index IX_4BE62F25 on EduPortal_DailyWork_EmployeeMobile (employeeId);
create index IX_8C69790B on EduPortal_DailyWork_EmployeeMobile (employeeId, status);

create index IX_EA301BAB on EduPortal_DailyWork_Task (companyId);
create index IX_4D2489A5 on EduPortal_DailyWork_Task (designationId);

create index IX_CA961382 on EduPortal_DailyWork_TaskDesignation (companyId);
create index IX_178CBDFC on EduPortal_DailyWork_TaskDesignation (designationId);
create index IX_660A36F0 on EduPortal_DailyWork_TaskDesignation (designationId, taskId);
create index IX_32F8F3DA on EduPortal_DailyWork_TaskDesignation (taskId);
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.dailyWork.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.dailyWork.model.Designation;

/**
 * The persistence interface for the designation service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see DesignationPersistenceImpl
 * @see DesignationUtil
 * @generated
 */
public interface DesignationPersistence extends BasePersistence<Designation> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DesignationUtil} to access the designation persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the designation in the entity cache if it is enabled.
	*
	* @param designation the designation
	*/
	public void cacheResult(
		info.diit.portal.dailyWork.model.Designation designation);

	/**
	* Caches the designations in the entity cache if it is enabled.
	*
	* @param designations the designations
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.dailyWork.model.Designation> designations);

	/**
	* Creates a new designation with the primary key. Does not add the designation to the database.
	*
	* @param designationId the primary key for the new designation
	* @return the new designation
	*/
	public info.diit.portal.dailyWork.model.Designation create(
		long designationId);

	/**
	* Removes the designation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param designationId the primary key of the designation
	* @return the designation that was removed
	* @throws info.diit.portal.dailyWork.NoSuchDesignationException if a designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.dailyWork.model.Designation remove(
		long designationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchDesignationException;

	public info.diit.portal.dailyWork.model.Designation updateImpl(
		info.diit.portal.dailyWork.model.Designation designation, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the designation with the primary key or throws a {@link info.diit.portal.dailyWork.NoSuchDesignationException} if it could not be found.
	*
	* @param designationId the primary key of the designation
	* @return the designation
	* @throws info.diit.portal.dailyWork.NoSuchDesignationException if a designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.dailyWork.model.Designation findByPrimaryKey(
		long designationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchDesignationException;

	/**
	* Returns the designation with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param designationId the primary key of the designation
	* @return the designation, or <code>null</code> if a designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.dailyWork.model.Designation fetchByPrimaryKey(
		long designationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the designations where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.dailyWork.model.Designation> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the designations where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of designations
	* @param end the upper bound of the range of designations (not inclusive)
	* @return the range of matching designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.dailyWork.model.Designation> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the designations where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of designations
	* @param end the upper bound of the range of designations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.dailyWork.model.Designation> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching designation
	* @throws info.diit.portal.dailyWork.NoSuchDesignationException if a matching designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.dailyWork.model.Designation findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchDesignationException;

	/**
	* Returns the first designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching designation, or <code>null</code> if a matching designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.dailyWork.model.Designation fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching designation
	* @throws info.diit.portal.dailyWork.NoSuchDesignationException if a matching designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.dailyWork.model.Designation findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchDesignationException;

	/**
	* Returns the last designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching designation, or <code>null</code> if a matching designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.dailyWork.model.Designation fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the designations before and after the current designation in the ordered set where companyId = &#63;.
	*
	* @param designationId the primary key of the current designation
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next designation
	* @throws info.diit.portal.dailyWork.NoSuchDesignationException if a designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.dailyWork.model.Designation[] findByCompany_PrevAndNext(
		long designationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchDesignationException;

	/**
	* Returns all the designations.
	*
	* @return the designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.dailyWork.model.Designation> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the designations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of designations
	* @param end the upper bound of the range of designations (not inclusive)
	* @return the range of designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.dailyWork.model.Designation> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the designations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of designations
	* @param end the upper bound of the range of designations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.dailyWork.model.Designation> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the designations where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the designations from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of designations where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching designations
	* @throws SystemException if a system exception occurred
	*/
	public int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of designations.
	*
	* @return the number of designations
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
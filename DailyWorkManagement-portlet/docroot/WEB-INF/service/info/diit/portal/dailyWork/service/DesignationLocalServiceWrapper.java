/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.dailyWork.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link DesignationLocalService}.
 * </p>
 *
 * @author    limon
 * @see       DesignationLocalService
 * @generated
 */
public class DesignationLocalServiceWrapper implements DesignationLocalService,
	ServiceWrapper<DesignationLocalService> {
	public DesignationLocalServiceWrapper(
		DesignationLocalService designationLocalService) {
		_designationLocalService = designationLocalService;
	}

	/**
	* Adds the designation to the database. Also notifies the appropriate model listeners.
	*
	* @param designation the designation
	* @return the designation that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.dailyWork.model.Designation addDesignation(
		info.diit.portal.dailyWork.model.Designation designation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.addDesignation(designation);
	}

	/**
	* Creates a new designation with the primary key. Does not add the designation to the database.
	*
	* @param designationId the primary key for the new designation
	* @return the new designation
	*/
	public info.diit.portal.dailyWork.model.Designation createDesignation(
		long designationId) {
		return _designationLocalService.createDesignation(designationId);
	}

	/**
	* Deletes the designation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param designationId the primary key of the designation
	* @return the designation that was removed
	* @throws PortalException if a designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.dailyWork.model.Designation deleteDesignation(
		long designationId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.deleteDesignation(designationId);
	}

	/**
	* Deletes the designation from the database. Also notifies the appropriate model listeners.
	*
	* @param designation the designation
	* @return the designation that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.dailyWork.model.Designation deleteDesignation(
		info.diit.portal.dailyWork.model.Designation designation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.deleteDesignation(designation);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _designationLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.dailyWork.model.Designation fetchDesignation(
		long designationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.fetchDesignation(designationId);
	}

	/**
	* Returns the designation with the primary key.
	*
	* @param designationId the primary key of the designation
	* @return the designation
	* @throws PortalException if a designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.dailyWork.model.Designation getDesignation(
		long designationId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.getDesignation(designationId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the designations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of designations
	* @param end the upper bound of the range of designations (not inclusive)
	* @return the range of designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.dailyWork.model.Designation> getDesignations(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.getDesignations(start, end);
	}

	/**
	* Returns the number of designations.
	*
	* @return the number of designations
	* @throws SystemException if a system exception occurred
	*/
	public int getDesignationsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.getDesignationsCount();
	}

	/**
	* Updates the designation in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param designation the designation
	* @return the designation that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.dailyWork.model.Designation updateDesignation(
		info.diit.portal.dailyWork.model.Designation designation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.updateDesignation(designation);
	}

	/**
	* Updates the designation in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param designation the designation
	* @param merge whether to merge the designation with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the designation that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.dailyWork.model.Designation updateDesignation(
		info.diit.portal.dailyWork.model.Designation designation, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.updateDesignation(designation, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _designationLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_designationLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _designationLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.dailyWork.model.Designation> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _designationLocalService.findByCompany(companyId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public DesignationLocalService getWrappedDesignationLocalService() {
		return _designationLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedDesignationLocalService(
		DesignationLocalService designationLocalService) {
		_designationLocalService = designationLocalService;
	}

	public DesignationLocalService getWrappedService() {
		return _designationLocalService;
	}

	public void setWrappedService(
		DesignationLocalService designationLocalService) {
		_designationLocalService = designationLocalService;
	}

	private DesignationLocalService _designationLocalService;
}
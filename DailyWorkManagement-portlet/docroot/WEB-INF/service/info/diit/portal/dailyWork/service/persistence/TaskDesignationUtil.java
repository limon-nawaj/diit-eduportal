/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.dailyWork.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.dailyWork.model.TaskDesignation;

import java.util.List;

/**
 * The persistence utility for the task designation service. This utility wraps {@link TaskDesignationPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see TaskDesignationPersistence
 * @see TaskDesignationPersistenceImpl
 * @generated
 */
public class TaskDesignationUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(TaskDesignation taskDesignation) {
		getPersistence().clearCache(taskDesignation);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<TaskDesignation> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<TaskDesignation> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<TaskDesignation> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static TaskDesignation update(TaskDesignation taskDesignation,
		boolean merge) throws SystemException {
		return getPersistence().update(taskDesignation, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static TaskDesignation update(TaskDesignation taskDesignation,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(taskDesignation, merge, serviceContext);
	}

	/**
	* Caches the task designation in the entity cache if it is enabled.
	*
	* @param taskDesignation the task designation
	*/
	public static void cacheResult(
		info.diit.portal.dailyWork.model.TaskDesignation taskDesignation) {
		getPersistence().cacheResult(taskDesignation);
	}

	/**
	* Caches the task designations in the entity cache if it is enabled.
	*
	* @param taskDesignations the task designations
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.dailyWork.model.TaskDesignation> taskDesignations) {
		getPersistence().cacheResult(taskDesignations);
	}

	/**
	* Creates a new task designation with the primary key. Does not add the task designation to the database.
	*
	* @param taskDesignationId the primary key for the new task designation
	* @return the new task designation
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation create(
		long taskDesignationId) {
		return getPersistence().create(taskDesignationId);
	}

	/**
	* Removes the task designation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param taskDesignationId the primary key of the task designation
	* @return the task designation that was removed
	* @throws info.diit.portal.dailyWork.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation remove(
		long taskDesignationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchTaskDesignationException {
		return getPersistence().remove(taskDesignationId);
	}

	public static info.diit.portal.dailyWork.model.TaskDesignation updateImpl(
		info.diit.portal.dailyWork.model.TaskDesignation taskDesignation,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(taskDesignation, merge);
	}

	/**
	* Returns the task designation with the primary key or throws a {@link info.diit.portal.dailyWork.NoSuchTaskDesignationException} if it could not be found.
	*
	* @param taskDesignationId the primary key of the task designation
	* @return the task designation
	* @throws info.diit.portal.dailyWork.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation findByPrimaryKey(
		long taskDesignationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchTaskDesignationException {
		return getPersistence().findByPrimaryKey(taskDesignationId);
	}

	/**
	* Returns the task designation with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param taskDesignationId the primary key of the task designation
	* @return the task designation, or <code>null</code> if a task designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation fetchByPrimaryKey(
		long taskDesignationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(taskDesignationId);
	}

	/**
	* Returns all the task designations where designationId = &#63;.
	*
	* @param designationId the designation ID
	* @return the matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.dailyWork.model.TaskDesignation> findByDesignation(
		long designationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByDesignation(designationId);
	}

	/**
	* Returns a range of all the task designations where designationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param designationId the designation ID
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @return the range of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.dailyWork.model.TaskDesignation> findByDesignation(
		long designationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByDesignation(designationId, start, end);
	}

	/**
	* Returns an ordered range of all the task designations where designationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param designationId the designation ID
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.dailyWork.model.TaskDesignation> findByDesignation(
		long designationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByDesignation(designationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first task designation in the ordered set where designationId = &#63;.
	*
	* @param designationId the designation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching task designation
	* @throws info.diit.portal.dailyWork.NoSuchTaskDesignationException if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation findByDesignation_First(
		long designationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchTaskDesignationException {
		return getPersistence()
				   .findByDesignation_First(designationId, orderByComparator);
	}

	/**
	* Returns the first task designation in the ordered set where designationId = &#63;.
	*
	* @param designationId the designation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation fetchByDesignation_First(
		long designationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByDesignation_First(designationId, orderByComparator);
	}

	/**
	* Returns the last task designation in the ordered set where designationId = &#63;.
	*
	* @param designationId the designation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching task designation
	* @throws info.diit.portal.dailyWork.NoSuchTaskDesignationException if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation findByDesignation_Last(
		long designationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchTaskDesignationException {
		return getPersistence()
				   .findByDesignation_Last(designationId, orderByComparator);
	}

	/**
	* Returns the last task designation in the ordered set where designationId = &#63;.
	*
	* @param designationId the designation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation fetchByDesignation_Last(
		long designationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByDesignation_Last(designationId, orderByComparator);
	}

	/**
	* Returns the task designations before and after the current task designation in the ordered set where designationId = &#63;.
	*
	* @param taskDesignationId the primary key of the current task designation
	* @param designationId the designation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next task designation
	* @throws info.diit.portal.dailyWork.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation[] findByDesignation_PrevAndNext(
		long taskDesignationId, long designationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchTaskDesignationException {
		return getPersistence()
				   .findByDesignation_PrevAndNext(taskDesignationId,
			designationId, orderByComparator);
	}

	/**
	* Returns all the task designations where taskId = &#63;.
	*
	* @param taskId the task ID
	* @return the matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.dailyWork.model.TaskDesignation> findByTask(
		long taskId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByTask(taskId);
	}

	/**
	* Returns a range of all the task designations where taskId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param taskId the task ID
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @return the range of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.dailyWork.model.TaskDesignation> findByTask(
		long taskId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByTask(taskId, start, end);
	}

	/**
	* Returns an ordered range of all the task designations where taskId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param taskId the task ID
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.dailyWork.model.TaskDesignation> findByTask(
		long taskId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByTask(taskId, start, end, orderByComparator);
	}

	/**
	* Returns the first task designation in the ordered set where taskId = &#63;.
	*
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching task designation
	* @throws info.diit.portal.dailyWork.NoSuchTaskDesignationException if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation findByTask_First(
		long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchTaskDesignationException {
		return getPersistence().findByTask_First(taskId, orderByComparator);
	}

	/**
	* Returns the first task designation in the ordered set where taskId = &#63;.
	*
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation fetchByTask_First(
		long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByTask_First(taskId, orderByComparator);
	}

	/**
	* Returns the last task designation in the ordered set where taskId = &#63;.
	*
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching task designation
	* @throws info.diit.portal.dailyWork.NoSuchTaskDesignationException if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation findByTask_Last(
		long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchTaskDesignationException {
		return getPersistence().findByTask_Last(taskId, orderByComparator);
	}

	/**
	* Returns the last task designation in the ordered set where taskId = &#63;.
	*
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation fetchByTask_Last(
		long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByTask_Last(taskId, orderByComparator);
	}

	/**
	* Returns the task designations before and after the current task designation in the ordered set where taskId = &#63;.
	*
	* @param taskDesignationId the primary key of the current task designation
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next task designation
	* @throws info.diit.portal.dailyWork.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation[] findByTask_PrevAndNext(
		long taskDesignationId, long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchTaskDesignationException {
		return getPersistence()
				   .findByTask_PrevAndNext(taskDesignationId, taskId,
			orderByComparator);
	}

	/**
	* Returns the task designation where designationId = &#63; and taskId = &#63; or throws a {@link info.diit.portal.dailyWork.NoSuchTaskDesignationException} if it could not be found.
	*
	* @param designationId the designation ID
	* @param taskId the task ID
	* @return the matching task designation
	* @throws info.diit.portal.dailyWork.NoSuchTaskDesignationException if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation findByDesignationTask(
		long designationId, long taskId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchTaskDesignationException {
		return getPersistence().findByDesignationTask(designationId, taskId);
	}

	/**
	* Returns the task designation where designationId = &#63; and taskId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param designationId the designation ID
	* @param taskId the task ID
	* @return the matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation fetchByDesignationTask(
		long designationId, long taskId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByDesignationTask(designationId, taskId);
	}

	/**
	* Returns the task designation where designationId = &#63; and taskId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param designationId the designation ID
	* @param taskId the task ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation fetchByDesignationTask(
		long designationId, long taskId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByDesignationTask(designationId, taskId,
			retrieveFromCache);
	}

	/**
	* Returns all the task designations where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.dailyWork.model.TaskDesignation> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId);
	}

	/**
	* Returns a range of all the task designations where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @return the range of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.dailyWork.model.TaskDesignation> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the task designations where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.dailyWork.model.TaskDesignation> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first task designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching task designation
	* @throws info.diit.portal.dailyWork.NoSuchTaskDesignationException if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchTaskDesignationException {
		return getPersistence().findByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first task designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last task designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching task designation
	* @throws info.diit.portal.dailyWork.NoSuchTaskDesignationException if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchTaskDesignationException {
		return getPersistence().findByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last task designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the task designations before and after the current task designation in the ordered set where companyId = &#63;.
	*
	* @param taskDesignationId the primary key of the current task designation
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next task designation
	* @throws info.diit.portal.dailyWork.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation[] findByCompany_PrevAndNext(
		long taskDesignationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchTaskDesignationException {
		return getPersistence()
				   .findByCompany_PrevAndNext(taskDesignationId, companyId,
			orderByComparator);
	}

	/**
	* Returns all the task designations.
	*
	* @return the task designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.dailyWork.model.TaskDesignation> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the task designations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @return the range of task designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.dailyWork.model.TaskDesignation> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the task designations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of task designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.dailyWork.model.TaskDesignation> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the task designations where designationId = &#63; from the database.
	*
	* @param designationId the designation ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByDesignation(long designationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByDesignation(designationId);
	}

	/**
	* Removes all the task designations where taskId = &#63; from the database.
	*
	* @param taskId the task ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByTask(long taskId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByTask(taskId);
	}

	/**
	* Removes the task designation where designationId = &#63; and taskId = &#63; from the database.
	*
	* @param designationId the designation ID
	* @param taskId the task ID
	* @return the task designation that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.dailyWork.model.TaskDesignation removeByDesignationTask(
		long designationId, long taskId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.dailyWork.NoSuchTaskDesignationException {
		return getPersistence().removeByDesignationTask(designationId, taskId);
	}

	/**
	* Removes all the task designations where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCompany(companyId);
	}

	/**
	* Removes all the task designations from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of task designations where designationId = &#63;.
	*
	* @param designationId the designation ID
	* @return the number of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public static int countByDesignation(long designationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByDesignation(designationId);
	}

	/**
	* Returns the number of task designations where taskId = &#63;.
	*
	* @param taskId the task ID
	* @return the number of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public static int countByTask(long taskId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByTask(taskId);
	}

	/**
	* Returns the number of task designations where designationId = &#63; and taskId = &#63;.
	*
	* @param designationId the designation ID
	* @param taskId the task ID
	* @return the number of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public static int countByDesignationTask(long designationId, long taskId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByDesignationTask(designationId, taskId);
	}

	/**
	* Returns the number of task designations where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCompany(companyId);
	}

	/**
	* Returns the number of task designations.
	*
	* @return the number of task designations
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static TaskDesignationPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (TaskDesignationPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.dailyWork.service.ClpSerializer.getServletContextName(),
					TaskDesignationPersistence.class.getName());

			ReferenceRegistry.registerReference(TaskDesignationUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(TaskDesignationPersistence persistence) {
	}

	private static TaskDesignationPersistence _persistence;
}
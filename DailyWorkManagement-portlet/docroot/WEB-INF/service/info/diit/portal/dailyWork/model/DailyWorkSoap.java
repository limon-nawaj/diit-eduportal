/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.dailyWork.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    limon
 * @generated
 */
public class DailyWorkSoap implements Serializable {
	public static DailyWorkSoap toSoapModel(DailyWork model) {
		DailyWorkSoap soapModel = new DailyWorkSoap();

		soapModel.setDailyWorkId(model.getDailyWorkId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setEmployeeId(model.getEmployeeId());
		soapModel.setWorKingDay(model.getWorKingDay());
		soapModel.setTaskId(model.getTaskId());
		soapModel.setTime(model.getTime());
		soapModel.setNote(model.getNote());

		return soapModel;
	}

	public static DailyWorkSoap[] toSoapModels(DailyWork[] models) {
		DailyWorkSoap[] soapModels = new DailyWorkSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DailyWorkSoap[][] toSoapModels(DailyWork[][] models) {
		DailyWorkSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DailyWorkSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DailyWorkSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DailyWorkSoap[] toSoapModels(List<DailyWork> models) {
		List<DailyWorkSoap> soapModels = new ArrayList<DailyWorkSoap>(models.size());

		for (DailyWork model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DailyWorkSoap[soapModels.size()]);
	}

	public DailyWorkSoap() {
	}

	public long getPrimaryKey() {
		return _dailyWorkId;
	}

	public void setPrimaryKey(long pk) {
		setDailyWorkId(pk);
	}

	public long getDailyWorkId() {
		return _dailyWorkId;
	}

	public void setDailyWorkId(long dailyWorkId) {
		_dailyWorkId = dailyWorkId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getEmployeeId() {
		return _employeeId;
	}

	public void setEmployeeId(long employeeId) {
		_employeeId = employeeId;
	}

	public Date getWorKingDay() {
		return _worKingDay;
	}

	public void setWorKingDay(Date worKingDay) {
		_worKingDay = worKingDay;
	}

	public long getTaskId() {
		return _taskId;
	}

	public void setTaskId(long taskId) {
		_taskId = taskId;
	}

	public int getTime() {
		return _time;
	}

	public void setTime(int time) {
		_time = time;
	}

	public String getNote() {
		return _note;
	}

	public void setNote(String note) {
		_note = note;
	}

	private long _dailyWorkId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _employeeId;
	private Date _worKingDay;
	private long _taskId;
	private int _time;
	private String _note;
}
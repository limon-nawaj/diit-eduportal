package info.diit.portal.student;

import info.diit.portal.student.addmission.gui.StudentAddmissionGui;

import com.vaadin.Application;
import com.vaadin.ui.Label;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class StudentAddmissionSystemApplication extends Application {

	public void init() {
		Window window = new Window();

		setMainWindow(window);
       StudentAddmissionGui addmissionGui=new StudentAddmissionGui();
		window.addComponent(addmissionGui);
	}

}
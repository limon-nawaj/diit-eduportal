package info.diit.portal.teachercomment;

import info.diit.portal.batch.model.Batch;
import info.diit.portal.batch.service.BatchLocalServiceUtil;
import info.diit.portal.batch.subject.model.BatchSubject;
import info.diit.portal.batch.subject.service.BatchSubjectLocalServiceUtil;
import info.diit.portal.student.service.model.BatchStudent;
import info.diit.portal.student.service.model.Student;
import info.diit.portal.student.service.service.BatchStudentLocalServiceUtil;
import info.diit.portal.student.service.service.StudentLocalServiceUtil;
import info.diit.portal.teachercomment.dto.BatchDto;
import info.diit.portal.teachercomment.dto.CampusDto;
import info.diit.portal.teachercomment.dto.CommentsDto;
import info.diit.portal.teachercomment.dto.StudentDto;
import info.diit.portal.teachercomment.model.Comments;
import info.diit.portal.teachercomment.model.impl.CommentsImpl;
import info.diit.portal.teachercomment.service.CommentsLocalServiceUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component.Event;
import com.vaadin.ui.Component.Listener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class TeachersCommentApplication extends Application implements PortletRequestListener {

	private ThemeDisplay themeDisplay;
	private List<Organization> userOrganizationList;
	private Window window;
	
	private final static String DATE_FORMAT = "dd/MM/yyyy";
	
	private final static String COLUMN_BATCH = "batch";
	private final static String COLUMN_STUDENT_ID = "studentId";
	private final static String COLUMN_STUDENT_NAME = "studentName";
	private final static String COLUMN_DATE = "date";
	private final static String COLUMN_COMMENT = "comments";
	
	private BeanItemContainer<CommentsDto> commentsContainer;
	
	private Comments comment;
	
	private TabSheet tab;
	
	public void init() {
		window = new Window();

		setMainWindow(window);

		try {
			userOrganizationList = themeDisplay.getUser().getOrganizations();
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		loadUserOrganization();
//		window.showNotification("Size:"+userOrganizationList.size());
		window.addComponent(tabSheet());
		loadBatch();
		filterBatch();
		if (userOrganizationList.size()==1) {
			batchComboBox();
			finderBatchComboBox();
		}
		loadComments();
		
	}
	
	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}
	
	private TabSheet tabSheet(){
		tab = new TabSheet();
		tab.addTab(noteLayout(), "Notes");
		tab.addTab(manageNote(), "Manage Notes");
		return tab;
	}
	
	private ComboBox campusComboBox;
	private ComboBox batchComboBox;
	private ComboBox studentComboBox;
	private DateField dateField;
	private TextField notesByField;
	private TextArea noteTextArea;
	
	private List<CampusDto> campusDtoList;
	private List<BatchDto> batchList;
	private List<BatchDto> filterBatchList;
	private List<StudentDto> studentList;
	
	private Table commentsTable;
	private ComboBox finderCampusComboBox;
	private ComboBox finderBatchComboBox;
	
	private GridLayout notaLayout;
	
	private GridLayout noteLayout(){
		notaLayout = new GridLayout(8, 10);
		notaLayout.setWidth("100%");
		notaLayout.setSpacing(true);
		
		campusComboBox = new ComboBox("Campus");
		campusComboBox.setWidth("100%");
		campusComboBox.setImmediate(true);
		
		if (campusDtoList!=null) {
			for (CampusDto campus : campusDtoList) {
				campusComboBox.addItem(campus);
			}
		}
		
		batchComboBox = new ComboBox("Batch");
		batchComboBox.setWidth("100%");
		batchComboBox.setImmediate(true);
		batchComboBox.setRequired(true);
		
		studentComboBox = new ComboBox("Student");
		studentComboBox.setWidth("100%");
		studentComboBox.setImmediate(true);
		studentComboBox.setRequired(true);
		
		batchComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				studentComboBox.removeAllItems();
				loadStudent();
				if (studentList!=null) {
					for (StudentDto studentDto : studentList) {
						studentComboBox.addItem(studentDto);
					}
				}
			}
		});
		
		dateField = new DateField("Date");
		dateField.setWidth("100%");
		dateField.setDateFormat(DATE_FORMAT);
		dateField.setRequired(true);
		dateField.setResolution(DateField.RESOLUTION_DAY);
		dateField.setValue(new Date());
		
		notesByField = new TextField("Notes By");
		notesByField.setWidth("100%");
		notesByField.setImmediate(true);
		notesByField.setValue(themeDisplay.getUser().getFullName());
		notesByField.setReadOnly(true);
		
		noteTextArea = new TextArea("Note");
		noteTextArea.setWidth("100%");
		noteTextArea.setRequired(true);
		
		Label specer = new Label();
		Button saveButton = new Button("Save");
		
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (comment==null) {
					comment = new CommentsImpl();
					comment.setNew(true);
				}
				
				comment.setCompanyId(themeDisplay.getCompanyId());
				comment.setUserId(themeDisplay.getUser().getUserId());
				comment.setUserName(themeDisplay.getUser().getFullName());
				
				CampusDto campus = (CampusDto) campusComboBox.getValue();
				
				if (campus!=null) {
					comment.setOrganizationId(campus.getId());
				}
				
				BatchDto batch = (BatchDto) batchComboBox.getValue();
				if (batch!=null) {
					comment.setBatchId(batch.getId());
				}else{
					window.showNotification("Please select a batch!!", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				StudentDto student = (StudentDto) studentComboBox.getValue();
				if (student!=null) {
					comment.setStudentId(student.getId());
				}else{
					window.showNotification("Please select a student!!", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				Date date = (Date) dateField.getValue();
				if (date!=null) {
					date.setHours(0);
					date.setMinutes(0);
					date.setSeconds(0);
					comment.setCommentDate(date);
				}else{
					window.showNotification("Please select date!!", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				String note = noteTextArea.getValue().toString();
				if (!note.equals(null) && !note.equals("")) {
					comment.setComments(note);
				}else{
					window.showNotification("Write some note", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				
				try {
					if (comment.isNew()) {
						comment.setCreateDate(new Date());
						CommentsLocalServiceUtil.addComments(comment);
						window.showNotification("Note saved successfully");
					}else{
						comment.setModifiedDate(new Date());
						CommentsLocalServiceUtil.updateComments(comment);
						window.showNotification("Note updated successfully");
					}
					loadComments();
					clear();
				} catch (SystemException e) {
					e.printStackTrace();
				}
				
			}
		});
		
		Button resetButton = new Button("Clear");
		resetButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				clear();
			}
		});
		
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setWidth("100%");
		buttonLayout.setSpacing(true);
		buttonLayout.addComponent(specer);
		buttonLayout.addComponent(saveButton);
		buttonLayout.addComponent(resetButton);
		buttonLayout.setExpandRatio(specer, 1);
		
		
		if (userOrganizationList.size()==1) {
			notaLayout.addComponent(batchComboBox, 0, 0, 2, 0);
			notaLayout.addComponent(dateField, 0, 1, 2, 1);
			
			notaLayout.addComponent(studentComboBox, 5, 0, 7, 0);
			notaLayout.addComponent(notesByField, 5, 1, 7, 1);
			
			notaLayout.addComponent(noteTextArea, 0, 2, 7, 4);
			notaLayout.addComponent(buttonLayout, 0, 5, 7, 5);
		}else{
			notaLayout.addComponent(campusComboBox, 0, 0, 2, 0);
			notaLayout.addComponent(batchComboBox, 0, 1, 2, 1);
			notaLayout.addComponent(dateField, 0, 2, 2, 2);
			
			notaLayout.addComponent(studentComboBox, 5, 1, 7, 1);
			notaLayout.addComponent(notesByField, 5, 2, 7, 2);
			
			notaLayout.addComponent(noteTextArea, 0, 3, 7, 5);
			notaLayout.addComponent(buttonLayout, 0, 6, 7, 6);
		}
		return notaLayout;
	}
	
	private void batchComboBox(){
		if (batchList!=null) {
			for (BatchDto batchDto : batchList) {
				batchComboBox.addItem(batchDto);
			}
		}
	}
	
	private void finderBatchComboBox(){
		if (filterBatchList!=null) {
			for (BatchDto batchDto : filterBatchList) {
				finderBatchComboBox.addItem(batchDto);
			}
		}
	}
	
	private GridLayout manageNote(){
		GridLayout manageNoteLayout = new GridLayout(8, 6);
		manageNoteLayout.setWidth("100%");
		manageNoteLayout.setSpacing(true);
		
		finderCampusComboBox = new ComboBox("Campus");
		finderCampusComboBox.setWidth("100%");
		finderCampusComboBox.setImmediate(true);
		
		if (campusDtoList!=null) {
			for (CampusDto campus : campusDtoList) {
				finderCampusComboBox.addItem(campus);
			}
		}
		
		finderBatchComboBox = new ComboBox("Batch");
		finderBatchComboBox.setWidth("100%");
		finderBatchComboBox.setImmediate(true);
		
		campusComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				batchComboBox.removeAllItems();
				finderBatchComboBox.removeAllItems();
				if (userOrganizationList.size()>1) {
					loadBatch();
					batchComboBox();
				}
			}
		});
		
		finderCampusComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				batchComboBox.removeAllItems();
				finderBatchComboBox.removeAllItems();
				if (userOrganizationList.size()>1) {
					filterBatch();
					finderBatchComboBox();
					loadComments();
				}
			}
		});
		
		commentsContainer = new BeanItemContainer<CommentsDto>(CommentsDto.class);
		commentsTable = new Table("List of Notes", commentsContainer){
			@Override
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				if (property.getType()==Date.class) {
					final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
					return dateFormat.format(property.getValue());
				}
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		commentsTable.setWidth("100%");
		commentsTable.setHeight("100%");
		commentsTable.setSelectable(true);
		
		commentsTable.setColumnHeader(COLUMN_BATCH, "Batch");
		commentsTable.setColumnHeader(COLUMN_STUDENT_ID, "Student Id");
		commentsTable.setColumnHeader(COLUMN_STUDENT_NAME, "Name");
		commentsTable.setColumnHeader(COLUMN_DATE, "Date");
		commentsTable.setColumnHeader(COLUMN_COMMENT, "Notes");
		
		commentsTable.setVisibleColumns(new String[]{COLUMN_BATCH, COLUMN_STUDENT_ID, COLUMN_STUDENT_NAME, COLUMN_DATE, COLUMN_COMMENT});
		
		commentsTable.setColumnExpandRatio(COLUMN_COMMENT, 5);
		commentsTable.setColumnExpandRatio(COLUMN_STUDENT_NAME, 2);
		
		Label spece = new Label();
		Button editButton = new Button("Edit");
		editButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				CommentsDto comment = (CommentsDto) commentsTable.getValue();
				if (comment!=null) {
					tab.setSelectedTab(notaLayout);
					editComments(comment.getId());
					comment.getComment().setNew(false);
				}else{
					window.showNotification("Please select a row!!", Window.Notification.TYPE_ERROR_MESSAGE);
				}
			}
		});
		
		Button deleteButton = new Button("Delete");
		deleteButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				CommentsDto commentDto = (CommentsDto) commentsTable.getValue();
				try {
					if (commentDto!=null) {
						CommentsLocalServiceUtil.deleteComments(commentDto.getId());
						loadComments();
						window.showNotification("Note Deleted successfully");
						clear();
					}else{
						window.showNotification("Please select a row!!", Window.Notification.TYPE_ERROR_MESSAGE);
					}
				} catch (PortalException e) {
					e.printStackTrace();
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
		
		finderBatchComboBox.addListener(new Listener() {
			
			@Override
			public void componentEvent(Event event) {
				BatchDto batchDto = (BatchDto) finderBatchComboBox.getValue();
				if (batchDto!=null) {
					commentsTable.setVisibleColumns(new String[]{COLUMN_STUDENT_ID, COLUMN_STUDENT_NAME, COLUMN_DATE, COLUMN_COMMENT});
				}else{
					commentsTable.setColumnHeader(COLUMN_BATCH, "Batch");
					commentsTable.setVisibleColumns(new String[]{COLUMN_BATCH, COLUMN_STUDENT_ID, COLUMN_STUDENT_NAME, COLUMN_DATE, COLUMN_COMMENT});
					
				}
				loadComments();
			}
		});
		
		HorizontalLayout tableButtonLayout = new HorizontalLayout();
		tableButtonLayout.setWidth("100%");
		tableButtonLayout.setSpacing(true);
		tableButtonLayout.addComponent(spece);
		tableButtonLayout.addComponent(editButton);
		tableButtonLayout.addComponent(deleteButton);
		tableButtonLayout.setExpandRatio(spece, 1);
		
		if (userOrganizationList.size()==1) {
			manageNoteLayout.addComponent(finderBatchComboBox, 0, 0, 2, 0);
			manageNoteLayout.addComponent(commentsTable, 0, 1, 7, 1);
			manageNoteLayout.addComponent(tableButtonLayout, 0, 2, 7, 2);
		}else{
			manageNoteLayout.addComponent(finderCampusComboBox, 0, 0, 2, 0);
			manageNoteLayout.addComponent(finderBatchComboBox, 5, 0, 7, 0);
			manageNoteLayout.addComponent(commentsTable, 0, 1, 7, 1);
			manageNoteLayout.addComponent(tableButtonLayout, 0, 2, 7, 2);
		}
		
		return manageNoteLayout;
	}
	
	private void loadUserOrganization(){
		if (userOrganizationList.size()>1) {
			if (campusDtoList!=null) {
				campusDtoList.clear();
			}else{
				campusDtoList = new ArrayList<CampusDto>();
			}
			
			for (Organization organization : userOrganizationList) {
				CampusDto campus = new CampusDto();
				campus.setId(organization.getOrganizationId());
				campus.setTitle(organization.getName());
				campusDtoList.add(campus);
			}
		}
	}
	
	private void loadBatch(){
		try {
			List<BatchSubject> batchSubjects = BatchSubjectLocalServiceUtil.findByBatchSubjectTeacher(themeDisplay.getUser().getUserId());
			if (userOrganizationList.size()>0 && batchSubjects!=null) {
				
				List<Batch> batchs = new ArrayList<Batch>();
				
				for (BatchSubject batchSubject : batchSubjects) {
					if (userOrganizationList.size()==1) {
						for (Organization organization : userOrganizationList) {
							if (organization.getOrganizationId()==batchSubject.getOrganizationId()) {
								Batch batch = BatchLocalServiceUtil.fetchBatch(batchSubject.getBatchId());
								batchs.add(batch);
							}
						}
					}else{
						CampusDto campus = (CampusDto) campusComboBox.getValue();
						if (campus!=null) {
							batchComboBox.removeAllItems();
							if (campus.getId()==batchSubject.getOrganizationId()) {
								Batch batch = BatchLocalServiceUtil.fetchBatch(batchSubject.getBatchId());
								batchs.add(batch);
							}
						}
						
						CampusDto finderCampus = (CampusDto) finderCampusComboBox.getValue();
						if (finderCampus!=null) {
							finderBatchComboBox.removeAllItems();
							if (finderCampus.getId()==batchSubject.getOrganizationId()) {
								Batch batch = BatchLocalServiceUtil.fetchBatch(batchSubject.getBatchId());
								batchs.add(batch);
							}
						}
					}
					
				}
				
				if (batchList!=null) {
					batchList.clear();
				}else{
					batchList = new ArrayList<BatchDto>();
				}
				
				if (filterBatchList!=null) {
					filterBatchList.clear();
				}else{
					filterBatchList = new ArrayList<BatchDto>();
				}
				
				for (Batch batch : batchs) {
					BatchDto batchDto = new BatchDto();
					batchDto.setId(batch.getBatchId());
					batchDto.setTitle(batch.getBatchName());
					batchList.add(batchDto);
				}
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	}
	
	private void filterBatch(){
		try {
			List<BatchSubject> batchSubjects = BatchSubjectLocalServiceUtil.findByBatchSubjectTeacher(themeDisplay.getUser().getUserId());
			if (userOrganizationList.size()>0 && batchSubjects!=null) {
				
				List<Batch> batchs = new ArrayList<Batch>();
				
				for (BatchSubject batchSubject : batchSubjects) {
					if (userOrganizationList.size()==1) {
						for (Organization organization : userOrganizationList) {
							if (organization.getOrganizationId()==batchSubject.getOrganizationId()) {
								Batch batch = BatchLocalServiceUtil.fetchBatch(batchSubject.getBatchId());
								batchs.add(batch);
							}
						}
					}else{
						
						CampusDto finderCampus = (CampusDto) finderCampusComboBox.getValue();
						if (finderCampus!=null) {
							finderBatchComboBox.removeAllItems();
							if (finderCampus.getId()==batchSubject.getOrganizationId()) {
								Batch batch = BatchLocalServiceUtil.fetchBatch(batchSubject.getBatchId());
								batchs.add(batch);
							}
						}
					}
					
				}
				
				if (filterBatchList!=null) {
					filterBatchList.clear();
				}else{
					filterBatchList = new ArrayList<BatchDto>();
				}
				
				for (Batch batch : batchs) {
					BatchDto batchDto = new BatchDto();
					batchDto.setId(batch.getBatchId());
					batchDto.setTitle(batch.getBatchName());
					filterBatchList.add(batchDto);
				}
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	}
	
	private void loadStudent(){
		BatchDto batch = (BatchDto) batchComboBox.getValue();
		try {
			if (batch!=null) {
				List<BatchStudent> batchStudentList = BatchStudentLocalServiceUtil.findByBatch(batch.getId());
//				window.showNotification("Size:"+batchStudentList.size());
				if (studentList!=null) {
					studentList.clear();
				}else{
					studentList = new ArrayList<StudentDto>();
				}
				for (BatchStudent batchStudent : batchStudentList) {
					StudentDto studentDto = new StudentDto();
					Student student = StudentLocalServiceUtil.fetchStudent(batchStudent.getStudentId());
					studentDto.setId(student.getStudentId());
					studentDto.setName(student.getName());
					studentList.add(studentDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void loadComments(){
		if (commentsContainer!=null) {
			commentsContainer.removeAllItems();
		}
		BatchDto batchCombo = (BatchDto) finderBatchComboBox.getValue();
		CampusDto campusDto = (CampusDto) finderCampusComboBox.getValue();
		List<Comments> commentList = null;
		
		try {
			if (campusDto!=null && batchCombo==null) {
				commentList = CommentsLocalServiceUtil.findByUserOrganization(themeDisplay.getUser().getUserId(), campusDto.getId());
			}else if (batchCombo!=null) {
				if (campusDto!=null) {
					commentList = CommentsLocalServiceUtil.findByOrganizationBatch(themeDisplay.getUser().getUserId(), campusDto.getId(), batchCombo.getId());
				}else{
					commentList = CommentsLocalServiceUtil.findByOrganizationBatch(themeDisplay.getUser().getUserId(), themeDisplay.getLayout().getGroup().getOrganizationId(), batchCombo.getId());
				}
				
			}else if(userOrganizationList.size()==1){
				commentList = CommentsLocalServiceUtil.findByUserOrganization(themeDisplay.getUser().getUserId(), themeDisplay.getLayout().getGroup().getOrganizationId());
			}else{
				commentList = CommentsLocalServiceUtil.findByUser(themeDisplay.getUser().getUserId());
			}
			
			if (commentList!=null) {
				for (Comments comments : commentList) {
					CommentsDto commentDto = new CommentsDto();
					commentDto.setId(comments.getCommentId());
					
					Batch batch = BatchLocalServiceUtil.fetchBatch(comments.getBatchId());
					BatchDto batchDto = new BatchDto();
					batchDto.setId(batch.getBatchId());
					batchDto.setTitle(batch.getBatchName());
					commentDto.setBatch(batchDto);
					
					commentDto.setStudentId(comments.getStudentId());
					Student student = StudentLocalServiceUtil.fetchStudent(comments.getStudentId());
					commentDto.setStudentName(student.getName());
					
					commentDto.setDate(comments.getCommentDate());
					commentDto.setComments(comments.getComments());
					
					commentsContainer.addBean(commentDto);
//					commentsContainer.sort(new Object[]{COLUMN_BATCH, COLUMN_STUDENT_ID, COLUMN_DATE}, new boolean[]{true, true, false});
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}
	
	private void editComments(long commentId){
		try {
			comment = CommentsLocalServiceUtil.fetchComments(commentId);
			if (userOrganizationList.size()>1) {
				campusComboBox.setValue(getUserOrganization(comment.getOrganizationId()));
			}
			batchComboBox.setValue(getBatch(comment.getBatchId()));
			studentComboBox.setValue(getStudent(comment.getStudentId()));
			dateField.setValue(comment.getCommentDate());
			noteTextArea.setValue(comment.getComments());
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private CampusDto getUserOrganization(long campusId){
		if (campusDtoList!=null) {
			for (CampusDto campusDto : campusDtoList) {
				if (campusDto.getId()==campusId) {
					return campusDto;
				}
			}
		}
		return null;
	}
	
	private BatchDto getBatch(long batchId){
		if (batchList!=null) {
			for (BatchDto batch : batchList) {
				if (batch.getId()==batchId) {
					return batch;
				}
			}
		}
		return null;
	}
	
	private StudentDto getStudent(long studentId){
		if (studentList!=null) {
			for (StudentDto student : studentList) {
				if (student.getId()==studentId) {
					return student;
				}
			}
		}
		return null;
	}
	
	private void clear(){
		comment = null;
//		campusComboBox.setValue(null);
//		batchComboBox.setValue(null);
		studentComboBox.setValue(null);
		dateField.setValue(new Date());
		noteTextArea.setValue("");
	}

}
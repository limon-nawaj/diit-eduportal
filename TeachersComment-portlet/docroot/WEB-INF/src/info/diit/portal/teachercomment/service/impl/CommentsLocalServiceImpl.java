/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.teachercomment.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import info.diit.portal.teachercomment.model.Comments;
import info.diit.portal.teachercomment.service.base.CommentsLocalServiceBaseImpl;
import info.diit.portal.teachercomment.service.persistence.CommentsUtil;

/**
 * The implementation of the comments local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.teachercomment.service.CommentsLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see info.diit.portal.teachercomment.service.base.CommentsLocalServiceBaseImpl
 * @see info.diit.portal.teachercomment.service.CommentsLocalServiceUtil
 */
public class CommentsLocalServiceImpl extends CommentsLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.teachercomment.service.CommentsLocalServiceUtil} to access the comments local service.
	 */
	
	public List<Comments> findByUser(long userId) throws SystemException{
		return CommentsUtil.findByUser(userId);
	}
	
	public List<Comments> findByUserBatch(long userId, long batchId) throws SystemException{
		return CommentsUtil.findByUserBatch(userId, batchId);
	}
	
	public List<Comments> findByUserOrganization(long userId, long organizationId) throws SystemException{
		return CommentsUtil.findByUserOrganization(userId, organizationId);
	}
	
	public List<Comments> findByOrganizationBatch(long userId, long organizationId, long batchId) throws SystemException{
		return CommentsUtil.findByUserOrganizationBatch(userId, organizationId, batchId);
	}
}
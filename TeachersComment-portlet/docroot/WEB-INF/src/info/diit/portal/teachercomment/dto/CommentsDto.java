package info.diit.portal.teachercomment.dto;

import info.diit.portal.teachercomment.model.Comments;
import info.diit.portal.teachercomment.model.impl.CommentsImpl;

import java.util.Date;

public class CommentsDto {

	private long id;
	private BatchDto batch;
	private long studentId;
	private String studentName;
	private Date date;
	private String comments;
	
	private Comments comment;
	
	public CommentsDto(){
		comment = new CommentsImpl();
		comment.setNew(true);
	}
	
	public Comments getComment() {
		return comment;
	}
	public void setComment(Comments comment) {
		this.comment = comment;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public BatchDto getBatch() {
		return batch;
	}
	public void setBatch(BatchDto batch) {
		this.batch = batch;
	}
	public long getStudentId() {
		return studentId;
	}
	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
}

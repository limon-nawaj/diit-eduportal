/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.teachercomment.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.teachercomment.model.Comments;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Comments in entity cache.
 *
 * @author limon
 * @see Comments
 * @generated
 */
public class CommentsCacheModel implements CacheModel<Comments>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{commentId=");
		sb.append(commentId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", batchId=");
		sb.append(batchId);
		sb.append(", studentId=");
		sb.append(studentId);
		sb.append(", commentDate=");
		sb.append(commentDate);
		sb.append(", comments=");
		sb.append(comments);
		sb.append("}");

		return sb.toString();
	}

	public Comments toEntityModel() {
		CommentsImpl commentsImpl = new CommentsImpl();

		commentsImpl.setCommentId(commentId);
		commentsImpl.setCompanyId(companyId);
		commentsImpl.setOrganizationId(organizationId);
		commentsImpl.setUserId(userId);

		if (userName == null) {
			commentsImpl.setUserName(StringPool.BLANK);
		}
		else {
			commentsImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			commentsImpl.setCreateDate(null);
		}
		else {
			commentsImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			commentsImpl.setModifiedDate(null);
		}
		else {
			commentsImpl.setModifiedDate(new Date(modifiedDate));
		}

		commentsImpl.setBatchId(batchId);
		commentsImpl.setStudentId(studentId);

		if (commentDate == Long.MIN_VALUE) {
			commentsImpl.setCommentDate(null);
		}
		else {
			commentsImpl.setCommentDate(new Date(commentDate));
		}

		if (comments == null) {
			commentsImpl.setComments(StringPool.BLANK);
		}
		else {
			commentsImpl.setComments(comments);
		}

		commentsImpl.resetOriginalValues();

		return commentsImpl;
	}

	public long commentId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long batchId;
	public long studentId;
	public long commentDate;
	public String comments;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.teachercomment.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.teachercomment.NoSuchCommentsException;
import info.diit.portal.teachercomment.model.Comments;
import info.diit.portal.teachercomment.model.impl.CommentsImpl;
import info.diit.portal.teachercomment.model.impl.CommentsModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the comments service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see CommentsPersistence
 * @see CommentsUtil
 * @generated
 */
public class CommentsPersistenceImpl extends BasePersistenceImpl<Comments>
	implements CommentsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CommentsUtil} to access the comments persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CommentsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USER = new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, CommentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUser",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER = new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, CommentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUser",
			new String[] { Long.class.getName() },
			CommentsModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USER = new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUser",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERBATCH =
		new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, CommentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserBatch",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERBATCH =
		new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, CommentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserBatch",
			new String[] { Long.class.getName(), Long.class.getName() },
			CommentsModelImpl.USERID_COLUMN_BITMASK |
			CommentsModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERBATCH = new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserBatch",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERORGANIZATION =
		new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, CommentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserOrganization",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATION =
		new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, CommentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUserOrganization",
			new String[] { Long.class.getName(), Long.class.getName() },
			CommentsModelImpl.USERID_COLUMN_BITMASK |
			CommentsModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERORGANIZATION = new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUserOrganization",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERORGANIZATIONBATCH =
		new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, CommentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUserOrganizationBatch",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATIONBATCH =
		new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, CommentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUserOrganizationBatch",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			},
			CommentsModelImpl.USERID_COLUMN_BITMASK |
			CommentsModelImpl.ORGANIZATIONID_COLUMN_BITMASK |
			CommentsModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERORGANIZATIONBATCH = new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUserOrganizationBatch",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, CommentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, CommentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the comments in the entity cache if it is enabled.
	 *
	 * @param comments the comments
	 */
	public void cacheResult(Comments comments) {
		EntityCacheUtil.putResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsImpl.class, comments.getPrimaryKey(), comments);

		comments.resetOriginalValues();
	}

	/**
	 * Caches the commentses in the entity cache if it is enabled.
	 *
	 * @param commentses the commentses
	 */
	public void cacheResult(List<Comments> commentses) {
		for (Comments comments : commentses) {
			if (EntityCacheUtil.getResult(
						CommentsModelImpl.ENTITY_CACHE_ENABLED,
						CommentsImpl.class, comments.getPrimaryKey()) == null) {
				cacheResult(comments);
			}
			else {
				comments.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all commentses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CommentsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CommentsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the comments.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Comments comments) {
		EntityCacheUtil.removeResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsImpl.class, comments.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Comments> commentses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Comments comments : commentses) {
			EntityCacheUtil.removeResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
				CommentsImpl.class, comments.getPrimaryKey());
		}
	}

	/**
	 * Creates a new comments with the primary key. Does not add the comments to the database.
	 *
	 * @param commentId the primary key for the new comments
	 * @return the new comments
	 */
	public Comments create(long commentId) {
		Comments comments = new CommentsImpl();

		comments.setNew(true);
		comments.setPrimaryKey(commentId);

		return comments;
	}

	/**
	 * Removes the comments with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param commentId the primary key of the comments
	 * @return the comments that was removed
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a comments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments remove(long commentId)
		throws NoSuchCommentsException, SystemException {
		return remove(Long.valueOf(commentId));
	}

	/**
	 * Removes the comments with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the comments
	 * @return the comments that was removed
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a comments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Comments remove(Serializable primaryKey)
		throws NoSuchCommentsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Comments comments = (Comments)session.get(CommentsImpl.class,
					primaryKey);

			if (comments == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCommentsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(comments);
		}
		catch (NoSuchCommentsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Comments removeImpl(Comments comments) throws SystemException {
		comments = toUnwrappedModel(comments);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, comments);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(comments);

		return comments;
	}

	@Override
	public Comments updateImpl(
		info.diit.portal.teachercomment.model.Comments comments, boolean merge)
		throws SystemException {
		comments = toUnwrappedModel(comments);

		boolean isNew = comments.isNew();

		CommentsModelImpl commentsModelImpl = (CommentsModelImpl)comments;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, comments, merge);

			comments.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CommentsModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((commentsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(commentsModelImpl.getOriginalUserId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USER, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER,
					args);

				args = new Object[] { Long.valueOf(commentsModelImpl.getUserId()) };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USER, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER,
					args);
			}

			if ((commentsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERBATCH.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(commentsModelImpl.getOriginalUserId()),
						Long.valueOf(commentsModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERBATCH,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERBATCH,
					args);

				args = new Object[] {
						Long.valueOf(commentsModelImpl.getUserId()),
						Long.valueOf(commentsModelImpl.getBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERBATCH,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERBATCH,
					args);
			}

			if ((commentsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(commentsModelImpl.getOriginalUserId()),
						Long.valueOf(commentsModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(commentsModelImpl.getUserId()),
						Long.valueOf(commentsModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATION,
					args);
			}

			if ((commentsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATIONBATCH.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(commentsModelImpl.getOriginalUserId()),
						Long.valueOf(commentsModelImpl.getOriginalOrganizationId()),
						Long.valueOf(commentsModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERORGANIZATIONBATCH,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATIONBATCH,
					args);

				args = new Object[] {
						Long.valueOf(commentsModelImpl.getUserId()),
						Long.valueOf(commentsModelImpl.getOrganizationId()),
						Long.valueOf(commentsModelImpl.getBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERORGANIZATIONBATCH,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATIONBATCH,
					args);
			}
		}

		EntityCacheUtil.putResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsImpl.class, comments.getPrimaryKey(), comments);

		return comments;
	}

	protected Comments toUnwrappedModel(Comments comments) {
		if (comments instanceof CommentsImpl) {
			return comments;
		}

		CommentsImpl commentsImpl = new CommentsImpl();

		commentsImpl.setNew(comments.isNew());
		commentsImpl.setPrimaryKey(comments.getPrimaryKey());

		commentsImpl.setCommentId(comments.getCommentId());
		commentsImpl.setCompanyId(comments.getCompanyId());
		commentsImpl.setOrganizationId(comments.getOrganizationId());
		commentsImpl.setUserId(comments.getUserId());
		commentsImpl.setUserName(comments.getUserName());
		commentsImpl.setCreateDate(comments.getCreateDate());
		commentsImpl.setModifiedDate(comments.getModifiedDate());
		commentsImpl.setBatchId(comments.getBatchId());
		commentsImpl.setStudentId(comments.getStudentId());
		commentsImpl.setCommentDate(comments.getCommentDate());
		commentsImpl.setComments(comments.getComments());

		return commentsImpl;
	}

	/**
	 * Returns the comments with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the comments
	 * @return the comments
	 * @throws com.liferay.portal.NoSuchModelException if a comments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Comments findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the comments with the primary key or throws a {@link info.diit.portal.teachercomment.NoSuchCommentsException} if it could not be found.
	 *
	 * @param commentId the primary key of the comments
	 * @return the comments
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a comments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments findByPrimaryKey(long commentId)
		throws NoSuchCommentsException, SystemException {
		Comments comments = fetchByPrimaryKey(commentId);

		if (comments == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + commentId);
			}

			throw new NoSuchCommentsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				commentId);
		}

		return comments;
	}

	/**
	 * Returns the comments with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the comments
	 * @return the comments, or <code>null</code> if a comments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Comments fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the comments with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param commentId the primary key of the comments
	 * @return the comments, or <code>null</code> if a comments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments fetchByPrimaryKey(long commentId) throws SystemException {
		Comments comments = (Comments)EntityCacheUtil.getResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
				CommentsImpl.class, commentId);

		if (comments == _nullComments) {
			return null;
		}

		if (comments == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				comments = (Comments)session.get(CommentsImpl.class,
						Long.valueOf(commentId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (comments != null) {
					cacheResult(comments);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
						CommentsImpl.class, commentId, _nullComments);
				}

				closeSession(session);
			}
		}

		return comments;
	}

	/**
	 * Returns all the commentses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findByUser(long userId) throws SystemException {
		return findByUser(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the commentses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of commentses
	 * @param end the upper bound of the range of commentses (not inclusive)
	 * @return the range of matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findByUser(long userId, int start, int end)
		throws SystemException {
		return findByUser(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the commentses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of commentses
	 * @param end the upper bound of the range of commentses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findByUser(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USER;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<Comments> list = (List<Comments>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Comments comments : list) {
				if ((userId != comments.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COMMENTS_WHERE);

			query.append(_FINDER_COLUMN_USER_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CommentsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				list = (List<Comments>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first comments in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching comments
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments findByUser_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCommentsException, SystemException {
		Comments comments = fetchByUser_First(userId, orderByComparator);

		if (comments != null) {
			return comments;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCommentsException(msg.toString());
	}

	/**
	 * Returns the first comments in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching comments, or <code>null</code> if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments fetchByUser_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Comments> list = findByUser(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last comments in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching comments
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments findByUser_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCommentsException, SystemException {
		Comments comments = fetchByUser_Last(userId, orderByComparator);

		if (comments != null) {
			return comments;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCommentsException(msg.toString());
	}

	/**
	 * Returns the last comments in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching comments, or <code>null</code> if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments fetchByUser_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUser(userId);

		List<Comments> list = findByUser(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the commentses before and after the current comments in the ordered set where userId = &#63;.
	 *
	 * @param commentId the primary key of the current comments
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next comments
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a comments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments[] findByUser_PrevAndNext(long commentId, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCommentsException, SystemException {
		Comments comments = findByPrimaryKey(commentId);

		Session session = null;

		try {
			session = openSession();

			Comments[] array = new CommentsImpl[3];

			array[0] = getByUser_PrevAndNext(session, comments, userId,
					orderByComparator, true);

			array[1] = comments;

			array[2] = getByUser_PrevAndNext(session, comments, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Comments getByUser_PrevAndNext(Session session,
		Comments comments, long userId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COMMENTS_WHERE);

		query.append(_FINDER_COLUMN_USER_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CommentsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(comments);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Comments> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the commentses where userId = &#63; and batchId = &#63;.
	 *
	 * @param userId the user ID
	 * @param batchId the batch ID
	 * @return the matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findByUserBatch(long userId, long batchId)
		throws SystemException {
		return findByUserBatch(userId, batchId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the commentses where userId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of commentses
	 * @param end the upper bound of the range of commentses (not inclusive)
	 * @return the range of matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findByUserBatch(long userId, long batchId, int start,
		int end) throws SystemException {
		return findByUserBatch(userId, batchId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the commentses where userId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of commentses
	 * @param end the upper bound of the range of commentses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findByUserBatch(long userId, long batchId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERBATCH;
			finderArgs = new Object[] { userId, batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERBATCH;
			finderArgs = new Object[] {
					userId, batchId,
					
					start, end, orderByComparator
				};
		}

		List<Comments> list = (List<Comments>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Comments comments : list) {
				if ((userId != comments.getUserId()) ||
						(batchId != comments.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_COMMENTS_WHERE);

			query.append(_FINDER_COLUMN_USERBATCH_USERID_2);

			query.append(_FINDER_COLUMN_USERBATCH_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CommentsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(batchId);

				list = (List<Comments>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first comments in the ordered set where userId = &#63; and batchId = &#63;.
	 *
	 * @param userId the user ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching comments
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments findByUserBatch_First(long userId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchCommentsException, SystemException {
		Comments comments = fetchByUserBatch_First(userId, batchId,
				orderByComparator);

		if (comments != null) {
			return comments;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCommentsException(msg.toString());
	}

	/**
	 * Returns the first comments in the ordered set where userId = &#63; and batchId = &#63;.
	 *
	 * @param userId the user ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching comments, or <code>null</code> if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments fetchByUserBatch_First(long userId, long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Comments> list = findByUserBatch(userId, batchId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last comments in the ordered set where userId = &#63; and batchId = &#63;.
	 *
	 * @param userId the user ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching comments
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments findByUserBatch_Last(long userId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchCommentsException, SystemException {
		Comments comments = fetchByUserBatch_Last(userId, batchId,
				orderByComparator);

		if (comments != null) {
			return comments;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCommentsException(msg.toString());
	}

	/**
	 * Returns the last comments in the ordered set where userId = &#63; and batchId = &#63;.
	 *
	 * @param userId the user ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching comments, or <code>null</code> if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments fetchByUserBatch_Last(long userId, long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUserBatch(userId, batchId);

		List<Comments> list = findByUserBatch(userId, batchId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the commentses before and after the current comments in the ordered set where userId = &#63; and batchId = &#63;.
	 *
	 * @param commentId the primary key of the current comments
	 * @param userId the user ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next comments
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a comments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments[] findByUserBatch_PrevAndNext(long commentId, long userId,
		long batchId, OrderByComparator orderByComparator)
		throws NoSuchCommentsException, SystemException {
		Comments comments = findByPrimaryKey(commentId);

		Session session = null;

		try {
			session = openSession();

			Comments[] array = new CommentsImpl[3];

			array[0] = getByUserBatch_PrevAndNext(session, comments, userId,
					batchId, orderByComparator, true);

			array[1] = comments;

			array[2] = getByUserBatch_PrevAndNext(session, comments, userId,
					batchId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Comments getByUserBatch_PrevAndNext(Session session,
		Comments comments, long userId, long batchId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COMMENTS_WHERE);

		query.append(_FINDER_COLUMN_USERBATCH_USERID_2);

		query.append(_FINDER_COLUMN_USERBATCH_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CommentsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(comments);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Comments> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the commentses where userId = &#63; and organizationId = &#63;.
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @return the matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findByUserOrganization(long userId,
		long organizationId) throws SystemException {
		return findByUserOrganization(userId, organizationId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the commentses where userId = &#63; and organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of commentses
	 * @param end the upper bound of the range of commentses (not inclusive)
	 * @return the range of matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findByUserOrganization(long userId,
		long organizationId, int start, int end) throws SystemException {
		return findByUserOrganization(userId, organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the commentses where userId = &#63; and organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of commentses
	 * @param end the upper bound of the range of commentses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findByUserOrganization(long userId,
		long organizationId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATION;
			finderArgs = new Object[] { userId, organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERORGANIZATION;
			finderArgs = new Object[] {
					userId, organizationId,
					
					start, end, orderByComparator
				};
		}

		List<Comments> list = (List<Comments>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Comments comments : list) {
				if ((userId != comments.getUserId()) ||
						(organizationId != comments.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_COMMENTS_WHERE);

			query.append(_FINDER_COLUMN_USERORGANIZATION_USERID_2);

			query.append(_FINDER_COLUMN_USERORGANIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CommentsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(organizationId);

				list = (List<Comments>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first comments in the ordered set where userId = &#63; and organizationId = &#63;.
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching comments
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments findByUserOrganization_First(long userId,
		long organizationId, OrderByComparator orderByComparator)
		throws NoSuchCommentsException, SystemException {
		Comments comments = fetchByUserOrganization_First(userId,
				organizationId, orderByComparator);

		if (comments != null) {
			return comments;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCommentsException(msg.toString());
	}

	/**
	 * Returns the first comments in the ordered set where userId = &#63; and organizationId = &#63;.
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching comments, or <code>null</code> if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments fetchByUserOrganization_First(long userId,
		long organizationId, OrderByComparator orderByComparator)
		throws SystemException {
		List<Comments> list = findByUserOrganization(userId, organizationId, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last comments in the ordered set where userId = &#63; and organizationId = &#63;.
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching comments
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments findByUserOrganization_Last(long userId,
		long organizationId, OrderByComparator orderByComparator)
		throws NoSuchCommentsException, SystemException {
		Comments comments = fetchByUserOrganization_Last(userId,
				organizationId, orderByComparator);

		if (comments != null) {
			return comments;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCommentsException(msg.toString());
	}

	/**
	 * Returns the last comments in the ordered set where userId = &#63; and organizationId = &#63;.
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching comments, or <code>null</code> if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments fetchByUserOrganization_Last(long userId,
		long organizationId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByUserOrganization(userId, organizationId);

		List<Comments> list = findByUserOrganization(userId, organizationId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the commentses before and after the current comments in the ordered set where userId = &#63; and organizationId = &#63;.
	 *
	 * @param commentId the primary key of the current comments
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next comments
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a comments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments[] findByUserOrganization_PrevAndNext(long commentId,
		long userId, long organizationId, OrderByComparator orderByComparator)
		throws NoSuchCommentsException, SystemException {
		Comments comments = findByPrimaryKey(commentId);

		Session session = null;

		try {
			session = openSession();

			Comments[] array = new CommentsImpl[3];

			array[0] = getByUserOrganization_PrevAndNext(session, comments,
					userId, organizationId, orderByComparator, true);

			array[1] = comments;

			array[2] = getByUserOrganization_PrevAndNext(session, comments,
					userId, organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Comments getByUserOrganization_PrevAndNext(Session session,
		Comments comments, long userId, long organizationId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COMMENTS_WHERE);

		query.append(_FINDER_COLUMN_USERORGANIZATION_USERID_2);

		query.append(_FINDER_COLUMN_USERORGANIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CommentsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(comments);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Comments> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the commentses where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @return the matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findByUserOrganizationBatch(long userId,
		long organizationId, long batchId) throws SystemException {
		return findByUserOrganizationBatch(userId, organizationId, batchId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the commentses where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of commentses
	 * @param end the upper bound of the range of commentses (not inclusive)
	 * @return the range of matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findByUserOrganizationBatch(long userId,
		long organizationId, long batchId, int start, int end)
		throws SystemException {
		return findByUserOrganizationBatch(userId, organizationId, batchId,
			start, end, null);
	}

	/**
	 * Returns an ordered range of all the commentses where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of commentses
	 * @param end the upper bound of the range of commentses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findByUserOrganizationBatch(long userId,
		long organizationId, long batchId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATIONBATCH;
			finderArgs = new Object[] { userId, organizationId, batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERORGANIZATIONBATCH;
			finderArgs = new Object[] {
					userId, organizationId, batchId,
					
					start, end, orderByComparator
				};
		}

		List<Comments> list = (List<Comments>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Comments comments : list) {
				if ((userId != comments.getUserId()) ||
						(organizationId != comments.getOrganizationId()) ||
						(batchId != comments.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_COMMENTS_WHERE);

			query.append(_FINDER_COLUMN_USERORGANIZATIONBATCH_USERID_2);

			query.append(_FINDER_COLUMN_USERORGANIZATIONBATCH_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_USERORGANIZATIONBATCH_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CommentsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(organizationId);

				qPos.add(batchId);

				list = (List<Comments>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first comments in the ordered set where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching comments
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments findByUserOrganizationBatch_First(long userId,
		long organizationId, long batchId, OrderByComparator orderByComparator)
		throws NoSuchCommentsException, SystemException {
		Comments comments = fetchByUserOrganizationBatch_First(userId,
				organizationId, batchId, orderByComparator);

		if (comments != null) {
			return comments;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", organizationId=");
		msg.append(organizationId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCommentsException(msg.toString());
	}

	/**
	 * Returns the first comments in the ordered set where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching comments, or <code>null</code> if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments fetchByUserOrganizationBatch_First(long userId,
		long organizationId, long batchId, OrderByComparator orderByComparator)
		throws SystemException {
		List<Comments> list = findByUserOrganizationBatch(userId,
				organizationId, batchId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last comments in the ordered set where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching comments
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments findByUserOrganizationBatch_Last(long userId,
		long organizationId, long batchId, OrderByComparator orderByComparator)
		throws NoSuchCommentsException, SystemException {
		Comments comments = fetchByUserOrganizationBatch_Last(userId,
				organizationId, batchId, orderByComparator);

		if (comments != null) {
			return comments;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", organizationId=");
		msg.append(organizationId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCommentsException(msg.toString());
	}

	/**
	 * Returns the last comments in the ordered set where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching comments, or <code>null</code> if a matching comments could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments fetchByUserOrganizationBatch_Last(long userId,
		long organizationId, long batchId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByUserOrganizationBatch(userId, organizationId, batchId);

		List<Comments> list = findByUserOrganizationBatch(userId,
				organizationId, batchId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the commentses before and after the current comments in the ordered set where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	 *
	 * @param commentId the primary key of the current comments
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next comments
	 * @throws info.diit.portal.teachercomment.NoSuchCommentsException if a comments with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Comments[] findByUserOrganizationBatch_PrevAndNext(long commentId,
		long userId, long organizationId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchCommentsException, SystemException {
		Comments comments = findByPrimaryKey(commentId);

		Session session = null;

		try {
			session = openSession();

			Comments[] array = new CommentsImpl[3];

			array[0] = getByUserOrganizationBatch_PrevAndNext(session,
					comments, userId, organizationId, batchId,
					orderByComparator, true);

			array[1] = comments;

			array[2] = getByUserOrganizationBatch_PrevAndNext(session,
					comments, userId, organizationId, batchId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Comments getByUserOrganizationBatch_PrevAndNext(Session session,
		Comments comments, long userId, long organizationId, long batchId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COMMENTS_WHERE);

		query.append(_FINDER_COLUMN_USERORGANIZATIONBATCH_USERID_2);

		query.append(_FINDER_COLUMN_USERORGANIZATIONBATCH_ORGANIZATIONID_2);

		query.append(_FINDER_COLUMN_USERORGANIZATIONBATCH_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CommentsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		qPos.add(organizationId);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(comments);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Comments> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the commentses.
	 *
	 * @return the commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the commentses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of commentses
	 * @param end the upper bound of the range of commentses (not inclusive)
	 * @return the range of commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the commentses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of commentses
	 * @param end the upper bound of the range of commentses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of commentses
	 * @throws SystemException if a system exception occurred
	 */
	public List<Comments> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Comments> list = (List<Comments>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_COMMENTS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COMMENTS.concat(CommentsModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Comments>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Comments>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the commentses where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUser(long userId) throws SystemException {
		for (Comments comments : findByUser(userId)) {
			remove(comments);
		}
	}

	/**
	 * Removes all the commentses where userId = &#63; and batchId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUserBatch(long userId, long batchId)
		throws SystemException {
		for (Comments comments : findByUserBatch(userId, batchId)) {
			remove(comments);
		}
	}

	/**
	 * Removes all the commentses where userId = &#63; and organizationId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUserOrganization(long userId, long organizationId)
		throws SystemException {
		for (Comments comments : findByUserOrganization(userId, organizationId)) {
			remove(comments);
		}
	}

	/**
	 * Removes all the commentses where userId = &#63; and organizationId = &#63; and batchId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUserOrganizationBatch(long userId, long organizationId,
		long batchId) throws SystemException {
		for (Comments comments : findByUserOrganizationBatch(userId,
				organizationId, batchId)) {
			remove(comments);
		}
	}

	/**
	 * Removes all the commentses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Comments comments : findAll()) {
			remove(comments);
		}
	}

	/**
	 * Returns the number of commentses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUser(long userId) throws SystemException {
		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USER,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COMMENTS_WHERE);

			query.append(_FINDER_COLUMN_USER_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USER,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of commentses where userId = &#63; and batchId = &#63;.
	 *
	 * @param userId the user ID
	 * @param batchId the batch ID
	 * @return the number of matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUserBatch(long userId, long batchId)
		throws SystemException {
		Object[] finderArgs = new Object[] { userId, batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERBATCH,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_COMMENTS_WHERE);

			query.append(_FINDER_COLUMN_USERBATCH_USERID_2);

			query.append(_FINDER_COLUMN_USERBATCH_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERBATCH,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of commentses where userId = &#63; and organizationId = &#63;.
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @return the number of matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUserOrganization(long userId, long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { userId, organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_COMMENTS_WHERE);

			query.append(_FINDER_COLUMN_USERORGANIZATION_USERID_2);

			query.append(_FINDER_COLUMN_USERORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of commentses where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	 *
	 * @param userId the user ID
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @return the number of matching commentses
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUserOrganizationBatch(long userId, long organizationId,
		long batchId) throws SystemException {
		Object[] finderArgs = new Object[] { userId, organizationId, batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERORGANIZATIONBATCH,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_COMMENTS_WHERE);

			query.append(_FINDER_COLUMN_USERORGANIZATIONBATCH_USERID_2);

			query.append(_FINDER_COLUMN_USERORGANIZATIONBATCH_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_USERORGANIZATIONBATCH_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(organizationId);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERORGANIZATIONBATCH,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of commentses.
	 *
	 * @return the number of commentses
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COMMENTS);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the comments persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.teachercomment.model.Comments")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Comments>> listenersList = new ArrayList<ModelListener<Comments>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Comments>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CommentsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_COMMENTS = "SELECT comments FROM Comments comments";
	private static final String _SQL_SELECT_COMMENTS_WHERE = "SELECT comments FROM Comments comments WHERE ";
	private static final String _SQL_COUNT_COMMENTS = "SELECT COUNT(comments) FROM Comments comments";
	private static final String _SQL_COUNT_COMMENTS_WHERE = "SELECT COUNT(comments) FROM Comments comments WHERE ";
	private static final String _FINDER_COLUMN_USER_USERID_2 = "comments.userId = ?";
	private static final String _FINDER_COLUMN_USERBATCH_USERID_2 = "comments.userId = ? AND ";
	private static final String _FINDER_COLUMN_USERBATCH_BATCHID_2 = "comments.batchId = ?";
	private static final String _FINDER_COLUMN_USERORGANIZATION_USERID_2 = "comments.userId = ? AND ";
	private static final String _FINDER_COLUMN_USERORGANIZATION_ORGANIZATIONID_2 =
		"comments.organizationId = ?";
	private static final String _FINDER_COLUMN_USERORGANIZATIONBATCH_USERID_2 = "comments.userId = ? AND ";
	private static final String _FINDER_COLUMN_USERORGANIZATIONBATCH_ORGANIZATIONID_2 =
		"comments.organizationId = ? AND ";
	private static final String _FINDER_COLUMN_USERORGANIZATIONBATCH_BATCHID_2 = "comments.batchId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "comments.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Comments exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Comments exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CommentsPersistenceImpl.class);
	private static Comments _nullComments = new CommentsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Comments> toCacheModel() {
				return _nullCommentsCacheModel;
			}
		};

	private static CacheModel<Comments> _nullCommentsCacheModel = new CacheModel<Comments>() {
			public Comments toEntityModel() {
				return _nullComments;
			}
		};
}
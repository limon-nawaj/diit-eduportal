create table EduPortal_TeachersComment_Comment (
	commentId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	batchId LONG,
	studentId LONG,
	commentDate DATE null,
	comments VARCHAR(75) null
);

create table EduPortal_TeachersComment_Comments (
	commentId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	batchId LONG,
	studentId LONG,
	commentDate DATE null,
	comments VARCHAR(75) null
);
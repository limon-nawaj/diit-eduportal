create index IX_32268482 on EduPortal_TeachersComment_Comments (userId);
create index IX_84CBEDA3 on EduPortal_TeachersComment_Comments (userId, batchId);
create index IX_11A44424 on EduPortal_TeachersComment_Comments (userId, organizationId);
create index IX_23CEA441 on EduPortal_TeachersComment_Comments (userId, organizationId, batchId);
create index IX_7C4298A9 on EduPortal_TeachersComment_Comments (userId, userId, batchId);
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.teachercomment.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.teachercomment.model.Comments;

import java.util.List;

/**
 * The persistence utility for the comments service. This utility wraps {@link CommentsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see CommentsPersistence
 * @see CommentsPersistenceImpl
 * @generated
 */
public class CommentsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Comments comments) {
		getPersistence().clearCache(comments);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Comments> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Comments> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Comments> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Comments update(Comments comments, boolean merge)
		throws SystemException {
		return getPersistence().update(comments, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Comments update(Comments comments, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(comments, merge, serviceContext);
	}

	/**
	* Caches the comments in the entity cache if it is enabled.
	*
	* @param comments the comments
	*/
	public static void cacheResult(
		info.diit.portal.teachercomment.model.Comments comments) {
		getPersistence().cacheResult(comments);
	}

	/**
	* Caches the commentses in the entity cache if it is enabled.
	*
	* @param commentses the commentses
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.teachercomment.model.Comments> commentses) {
		getPersistence().cacheResult(commentses);
	}

	/**
	* Creates a new comments with the primary key. Does not add the comments to the database.
	*
	* @param commentId the primary key for the new comments
	* @return the new comments
	*/
	public static info.diit.portal.teachercomment.model.Comments create(
		long commentId) {
		return getPersistence().create(commentId);
	}

	/**
	* Removes the comments with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param commentId the primary key of the comments
	* @return the comments that was removed
	* @throws info.diit.portal.teachercomment.NoSuchCommentsException if a comments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments remove(
		long commentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.teachercomment.NoSuchCommentsException {
		return getPersistence().remove(commentId);
	}

	public static info.diit.portal.teachercomment.model.Comments updateImpl(
		info.diit.portal.teachercomment.model.Comments comments, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(comments, merge);
	}

	/**
	* Returns the comments with the primary key or throws a {@link info.diit.portal.teachercomment.NoSuchCommentsException} if it could not be found.
	*
	* @param commentId the primary key of the comments
	* @return the comments
	* @throws info.diit.portal.teachercomment.NoSuchCommentsException if a comments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments findByPrimaryKey(
		long commentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.teachercomment.NoSuchCommentsException {
		return getPersistence().findByPrimaryKey(commentId);
	}

	/**
	* Returns the comments with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param commentId the primary key of the comments
	* @return the comments, or <code>null</code> if a comments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments fetchByPrimaryKey(
		long commentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(commentId);
	}

	/**
	* Returns all the commentses where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findByUser(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUser(userId);
	}

	/**
	* Returns a range of all the commentses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of commentses
	* @param end the upper bound of the range of commentses (not inclusive)
	* @return the range of matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findByUser(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUser(userId, start, end);
	}

	/**
	* Returns an ordered range of all the commentses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of commentses
	* @param end the upper bound of the range of commentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findByUser(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUser(userId, start, end, orderByComparator);
	}

	/**
	* Returns the first comments in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching comments
	* @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments findByUser_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.teachercomment.NoSuchCommentsException {
		return getPersistence().findByUser_First(userId, orderByComparator);
	}

	/**
	* Returns the first comments in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching comments, or <code>null</code> if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments fetchByUser_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUser_First(userId, orderByComparator);
	}

	/**
	* Returns the last comments in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching comments
	* @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments findByUser_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.teachercomment.NoSuchCommentsException {
		return getPersistence().findByUser_Last(userId, orderByComparator);
	}

	/**
	* Returns the last comments in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching comments, or <code>null</code> if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments fetchByUser_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUser_Last(userId, orderByComparator);
	}

	/**
	* Returns the commentses before and after the current comments in the ordered set where userId = &#63;.
	*
	* @param commentId the primary key of the current comments
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next comments
	* @throws info.diit.portal.teachercomment.NoSuchCommentsException if a comments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments[] findByUser_PrevAndNext(
		long commentId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.teachercomment.NoSuchCommentsException {
		return getPersistence()
				   .findByUser_PrevAndNext(commentId, userId, orderByComparator);
	}

	/**
	* Returns all the commentses where userId = &#63; and batchId = &#63;.
	*
	* @param userId the user ID
	* @param batchId the batch ID
	* @return the matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findByUserBatch(
		long userId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserBatch(userId, batchId);
	}

	/**
	* Returns a range of all the commentses where userId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of commentses
	* @param end the upper bound of the range of commentses (not inclusive)
	* @return the range of matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findByUserBatch(
		long userId, long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserBatch(userId, batchId, start, end);
	}

	/**
	* Returns an ordered range of all the commentses where userId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of commentses
	* @param end the upper bound of the range of commentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findByUserBatch(
		long userId, long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserBatch(userId, batchId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first comments in the ordered set where userId = &#63; and batchId = &#63;.
	*
	* @param userId the user ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching comments
	* @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments findByUserBatch_First(
		long userId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.teachercomment.NoSuchCommentsException {
		return getPersistence()
				   .findByUserBatch_First(userId, batchId, orderByComparator);
	}

	/**
	* Returns the first comments in the ordered set where userId = &#63; and batchId = &#63;.
	*
	* @param userId the user ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching comments, or <code>null</code> if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments fetchByUserBatch_First(
		long userId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUserBatch_First(userId, batchId, orderByComparator);
	}

	/**
	* Returns the last comments in the ordered set where userId = &#63; and batchId = &#63;.
	*
	* @param userId the user ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching comments
	* @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments findByUserBatch_Last(
		long userId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.teachercomment.NoSuchCommentsException {
		return getPersistence()
				   .findByUserBatch_Last(userId, batchId, orderByComparator);
	}

	/**
	* Returns the last comments in the ordered set where userId = &#63; and batchId = &#63;.
	*
	* @param userId the user ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching comments, or <code>null</code> if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments fetchByUserBatch_Last(
		long userId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUserBatch_Last(userId, batchId, orderByComparator);
	}

	/**
	* Returns the commentses before and after the current comments in the ordered set where userId = &#63; and batchId = &#63;.
	*
	* @param commentId the primary key of the current comments
	* @param userId the user ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next comments
	* @throws info.diit.portal.teachercomment.NoSuchCommentsException if a comments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments[] findByUserBatch_PrevAndNext(
		long commentId, long userId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.teachercomment.NoSuchCommentsException {
		return getPersistence()
				   .findByUserBatch_PrevAndNext(commentId, userId, batchId,
			orderByComparator);
	}

	/**
	* Returns all the commentses where userId = &#63; and organizationId = &#63;.
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @return the matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findByUserOrganization(
		long userId, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserOrganization(userId, organizationId);
	}

	/**
	* Returns a range of all the commentses where userId = &#63; and organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param start the lower bound of the range of commentses
	* @param end the upper bound of the range of commentses (not inclusive)
	* @return the range of matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findByUserOrganization(
		long userId, long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserOrganization(userId, organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the commentses where userId = &#63; and organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param start the lower bound of the range of commentses
	* @param end the upper bound of the range of commentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findByUserOrganization(
		long userId, long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserOrganization(userId, organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first comments in the ordered set where userId = &#63; and organizationId = &#63;.
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching comments
	* @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments findByUserOrganization_First(
		long userId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.teachercomment.NoSuchCommentsException {
		return getPersistence()
				   .findByUserOrganization_First(userId, organizationId,
			orderByComparator);
	}

	/**
	* Returns the first comments in the ordered set where userId = &#63; and organizationId = &#63;.
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching comments, or <code>null</code> if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments fetchByUserOrganization_First(
		long userId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUserOrganization_First(userId, organizationId,
			orderByComparator);
	}

	/**
	* Returns the last comments in the ordered set where userId = &#63; and organizationId = &#63;.
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching comments
	* @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments findByUserOrganization_Last(
		long userId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.teachercomment.NoSuchCommentsException {
		return getPersistence()
				   .findByUserOrganization_Last(userId, organizationId,
			orderByComparator);
	}

	/**
	* Returns the last comments in the ordered set where userId = &#63; and organizationId = &#63;.
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching comments, or <code>null</code> if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments fetchByUserOrganization_Last(
		long userId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUserOrganization_Last(userId, organizationId,
			orderByComparator);
	}

	/**
	* Returns the commentses before and after the current comments in the ordered set where userId = &#63; and organizationId = &#63;.
	*
	* @param commentId the primary key of the current comments
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next comments
	* @throws info.diit.portal.teachercomment.NoSuchCommentsException if a comments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments[] findByUserOrganization_PrevAndNext(
		long commentId, long userId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.teachercomment.NoSuchCommentsException {
		return getPersistence()
				   .findByUserOrganization_PrevAndNext(commentId, userId,
			organizationId, orderByComparator);
	}

	/**
	* Returns all the commentses where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @return the matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findByUserOrganizationBatch(
		long userId, long organizationId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserOrganizationBatch(userId, organizationId, batchId);
	}

	/**
	* Returns a range of all the commentses where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of commentses
	* @param end the upper bound of the range of commentses (not inclusive)
	* @return the range of matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findByUserOrganizationBatch(
		long userId, long organizationId, long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserOrganizationBatch(userId, organizationId,
			batchId, start, end);
	}

	/**
	* Returns an ordered range of all the commentses where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of commentses
	* @param end the upper bound of the range of commentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findByUserOrganizationBatch(
		long userId, long organizationId, long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserOrganizationBatch(userId, organizationId,
			batchId, start, end, orderByComparator);
	}

	/**
	* Returns the first comments in the ordered set where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching comments
	* @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments findByUserOrganizationBatch_First(
		long userId, long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.teachercomment.NoSuchCommentsException {
		return getPersistence()
				   .findByUserOrganizationBatch_First(userId, organizationId,
			batchId, orderByComparator);
	}

	/**
	* Returns the first comments in the ordered set where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching comments, or <code>null</code> if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments fetchByUserOrganizationBatch_First(
		long userId, long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUserOrganizationBatch_First(userId, organizationId,
			batchId, orderByComparator);
	}

	/**
	* Returns the last comments in the ordered set where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching comments
	* @throws info.diit.portal.teachercomment.NoSuchCommentsException if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments findByUserOrganizationBatch_Last(
		long userId, long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.teachercomment.NoSuchCommentsException {
		return getPersistence()
				   .findByUserOrganizationBatch_Last(userId, organizationId,
			batchId, orderByComparator);
	}

	/**
	* Returns the last comments in the ordered set where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching comments, or <code>null</code> if a matching comments could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments fetchByUserOrganizationBatch_Last(
		long userId, long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUserOrganizationBatch_Last(userId, organizationId,
			batchId, orderByComparator);
	}

	/**
	* Returns the commentses before and after the current comments in the ordered set where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	*
	* @param commentId the primary key of the current comments
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next comments
	* @throws info.diit.portal.teachercomment.NoSuchCommentsException if a comments with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.teachercomment.model.Comments[] findByUserOrganizationBatch_PrevAndNext(
		long commentId, long userId, long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.teachercomment.NoSuchCommentsException {
		return getPersistence()
				   .findByUserOrganizationBatch_PrevAndNext(commentId, userId,
			organizationId, batchId, orderByComparator);
	}

	/**
	* Returns all the commentses.
	*
	* @return the commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the commentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of commentses
	* @param end the upper bound of the range of commentses (not inclusive)
	* @return the range of commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the commentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of commentses
	* @param end the upper bound of the range of commentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of commentses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.teachercomment.model.Comments> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the commentses where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUser(userId);
	}

	/**
	* Removes all the commentses where userId = &#63; and batchId = &#63; from the database.
	*
	* @param userId the user ID
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUserBatch(long userId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUserBatch(userId, batchId);
	}

	/**
	* Removes all the commentses where userId = &#63; and organizationId = &#63; from the database.
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUserOrganization(long userId, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUserOrganization(userId, organizationId);
	}

	/**
	* Removes all the commentses where userId = &#63; and organizationId = &#63; and batchId = &#63; from the database.
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUserOrganizationBatch(long userId,
		long organizationId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByUserOrganizationBatch(userId, organizationId, batchId);
	}

	/**
	* Removes all the commentses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of commentses where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUser(userId);
	}

	/**
	* Returns the number of commentses where userId = &#63; and batchId = &#63;.
	*
	* @param userId the user ID
	* @param batchId the batch ID
	* @return the number of matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUserBatch(long userId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUserBatch(userId, batchId);
	}

	/**
	* Returns the number of commentses where userId = &#63; and organizationId = &#63;.
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @return the number of matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUserOrganization(long userId, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUserOrganization(userId, organizationId);
	}

	/**
	* Returns the number of commentses where userId = &#63; and organizationId = &#63; and batchId = &#63;.
	*
	* @param userId the user ID
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @return the number of matching commentses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUserOrganizationBatch(long userId,
		long organizationId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByUserOrganizationBatch(userId, organizationId, batchId);
	}

	/**
	* Returns the number of commentses.
	*
	* @return the number of commentses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CommentsPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CommentsPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.teachercomment.service.ClpSerializer.getServletContextName(),
					CommentsPersistence.class.getName());

			ReferenceRegistry.registerReference(CommentsUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(CommentsPersistence persistence) {
	}

	private static CommentsPersistence _persistence;
}
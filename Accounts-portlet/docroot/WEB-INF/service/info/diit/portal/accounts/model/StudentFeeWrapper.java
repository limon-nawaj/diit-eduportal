/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link StudentFee}.
 * </p>
 *
 * @author    shamsuddin
 * @see       StudentFee
 * @generated
 */
public class StudentFeeWrapper implements StudentFee, ModelWrapper<StudentFee> {
	public StudentFeeWrapper(StudentFee studentFee) {
		_studentFee = studentFee;
	}

	public Class<?> getModelClass() {
		return StudentFee.class;
	}

	public String getModelClassName() {
		return StudentFee.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("studentFeeId", getStudentFeeId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("studentId", getStudentId());
		attributes.put("batchId", getBatchId());
		attributes.put("feeTypeId", getFeeTypeId());
		attributes.put("amount", getAmount());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long studentFeeId = (Long)attributes.get("studentFeeId");

		if (studentFeeId != null) {
			setStudentFeeId(studentFeeId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Long feeTypeId = (Long)attributes.get("feeTypeId");

		if (feeTypeId != null) {
			setFeeTypeId(feeTypeId);
		}

		Double amount = (Double)attributes.get("amount");

		if (amount != null) {
			setAmount(amount);
		}
	}

	/**
	* Returns the primary key of this student fee.
	*
	* @return the primary key of this student fee
	*/
	public long getPrimaryKey() {
		return _studentFee.getPrimaryKey();
	}

	/**
	* Sets the primary key of this student fee.
	*
	* @param primaryKey the primary key of this student fee
	*/
	public void setPrimaryKey(long primaryKey) {
		_studentFee.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the student fee ID of this student fee.
	*
	* @return the student fee ID of this student fee
	*/
	public long getStudentFeeId() {
		return _studentFee.getStudentFeeId();
	}

	/**
	* Sets the student fee ID of this student fee.
	*
	* @param studentFeeId the student fee ID of this student fee
	*/
	public void setStudentFeeId(long studentFeeId) {
		_studentFee.setStudentFeeId(studentFeeId);
	}

	/**
	* Returns the company ID of this student fee.
	*
	* @return the company ID of this student fee
	*/
	public long getCompanyId() {
		return _studentFee.getCompanyId();
	}

	/**
	* Sets the company ID of this student fee.
	*
	* @param companyId the company ID of this student fee
	*/
	public void setCompanyId(long companyId) {
		_studentFee.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this student fee.
	*
	* @return the user ID of this student fee
	*/
	public long getUserId() {
		return _studentFee.getUserId();
	}

	/**
	* Sets the user ID of this student fee.
	*
	* @param userId the user ID of this student fee
	*/
	public void setUserId(long userId) {
		_studentFee.setUserId(userId);
	}

	/**
	* Returns the user uuid of this student fee.
	*
	* @return the user uuid of this student fee
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentFee.getUserUuid();
	}

	/**
	* Sets the user uuid of this student fee.
	*
	* @param userUuid the user uuid of this student fee
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_studentFee.setUserUuid(userUuid);
	}

	/**
	* Returns the create date of this student fee.
	*
	* @return the create date of this student fee
	*/
	public java.util.Date getCreateDate() {
		return _studentFee.getCreateDate();
	}

	/**
	* Sets the create date of this student fee.
	*
	* @param createDate the create date of this student fee
	*/
	public void setCreateDate(java.util.Date createDate) {
		_studentFee.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this student fee.
	*
	* @return the modified date of this student fee
	*/
	public java.util.Date getModifiedDate() {
		return _studentFee.getModifiedDate();
	}

	/**
	* Sets the modified date of this student fee.
	*
	* @param modifiedDate the modified date of this student fee
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_studentFee.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the student ID of this student fee.
	*
	* @return the student ID of this student fee
	*/
	public long getStudentId() {
		return _studentFee.getStudentId();
	}

	/**
	* Sets the student ID of this student fee.
	*
	* @param studentId the student ID of this student fee
	*/
	public void setStudentId(long studentId) {
		_studentFee.setStudentId(studentId);
	}

	/**
	* Returns the batch ID of this student fee.
	*
	* @return the batch ID of this student fee
	*/
	public long getBatchId() {
		return _studentFee.getBatchId();
	}

	/**
	* Sets the batch ID of this student fee.
	*
	* @param batchId the batch ID of this student fee
	*/
	public void setBatchId(long batchId) {
		_studentFee.setBatchId(batchId);
	}

	/**
	* Returns the fee type ID of this student fee.
	*
	* @return the fee type ID of this student fee
	*/
	public long getFeeTypeId() {
		return _studentFee.getFeeTypeId();
	}

	/**
	* Sets the fee type ID of this student fee.
	*
	* @param feeTypeId the fee type ID of this student fee
	*/
	public void setFeeTypeId(long feeTypeId) {
		_studentFee.setFeeTypeId(feeTypeId);
	}

	/**
	* Returns the amount of this student fee.
	*
	* @return the amount of this student fee
	*/
	public double getAmount() {
		return _studentFee.getAmount();
	}

	/**
	* Sets the amount of this student fee.
	*
	* @param amount the amount of this student fee
	*/
	public void setAmount(double amount) {
		_studentFee.setAmount(amount);
	}

	public boolean isNew() {
		return _studentFee.isNew();
	}

	public void setNew(boolean n) {
		_studentFee.setNew(n);
	}

	public boolean isCachedModel() {
		return _studentFee.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_studentFee.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _studentFee.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _studentFee.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_studentFee.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _studentFee.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_studentFee.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new StudentFeeWrapper((StudentFee)_studentFee.clone());
	}

	public int compareTo(info.diit.portal.accounts.model.StudentFee studentFee) {
		return _studentFee.compareTo(studentFee);
	}

	@Override
	public int hashCode() {
		return _studentFee.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.accounts.model.StudentFee> toCacheModel() {
		return _studentFee.toCacheModel();
	}

	public info.diit.portal.accounts.model.StudentFee toEscapedModel() {
		return new StudentFeeWrapper(_studentFee.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _studentFee.toString();
	}

	public java.lang.String toXmlString() {
		return _studentFee.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_studentFee.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public StudentFee getWrappedStudentFee() {
		return _studentFee;
	}

	public StudentFee getWrappedModel() {
		return _studentFee;
	}

	public void resetOriginalValues() {
		_studentFee.resetOriginalValues();
	}

	private StudentFee _studentFee;
}
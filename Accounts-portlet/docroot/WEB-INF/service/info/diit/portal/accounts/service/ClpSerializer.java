/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import info.diit.portal.accounts.model.BatchFeeClp;
import info.diit.portal.accounts.model.BatchPaymentScheduleClp;
import info.diit.portal.accounts.model.CourseFeeClp;
import info.diit.portal.accounts.model.FeeTypeClp;
import info.diit.portal.accounts.model.PaymentClp;
import info.diit.portal.accounts.model.StudentDiscountClp;
import info.diit.portal.accounts.model.StudentFeeClp;
import info.diit.portal.accounts.model.StudentPaymentScheduleClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Brian Wing Shun Chan
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"Accounts-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"Accounts-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "Accounts-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(BatchFeeClp.class.getName())) {
			return translateInputBatchFee(oldModel);
		}

		if (oldModelClassName.equals(BatchPaymentScheduleClp.class.getName())) {
			return translateInputBatchPaymentSchedule(oldModel);
		}

		if (oldModelClassName.equals(CourseFeeClp.class.getName())) {
			return translateInputCourseFee(oldModel);
		}

		if (oldModelClassName.equals(FeeTypeClp.class.getName())) {
			return translateInputFeeType(oldModel);
		}

		if (oldModelClassName.equals(PaymentClp.class.getName())) {
			return translateInputPayment(oldModel);
		}

		if (oldModelClassName.equals(StudentDiscountClp.class.getName())) {
			return translateInputStudentDiscount(oldModel);
		}

		if (oldModelClassName.equals(StudentFeeClp.class.getName())) {
			return translateInputStudentFee(oldModel);
		}

		if (oldModelClassName.equals(StudentPaymentScheduleClp.class.getName())) {
			return translateInputStudentPaymentSchedule(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputBatchFee(BaseModel<?> oldModel) {
		BatchFeeClp oldClpModel = (BatchFeeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBatchFeeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputBatchPaymentSchedule(
		BaseModel<?> oldModel) {
		BatchPaymentScheduleClp oldClpModel = (BatchPaymentScheduleClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBatchPaymentScheduleRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCourseFee(BaseModel<?> oldModel) {
		CourseFeeClp oldClpModel = (CourseFeeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCourseFeeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputFeeType(BaseModel<?> oldModel) {
		FeeTypeClp oldClpModel = (FeeTypeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getFeeTypeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputPayment(BaseModel<?> oldModel) {
		PaymentClp oldClpModel = (PaymentClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getPaymentRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStudentDiscount(BaseModel<?> oldModel) {
		StudentDiscountClp oldClpModel = (StudentDiscountClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStudentDiscountRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStudentFee(BaseModel<?> oldModel) {
		StudentFeeClp oldClpModel = (StudentFeeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStudentFeeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStudentPaymentSchedule(
		BaseModel<?> oldModel) {
		StudentPaymentScheduleClp oldClpModel = (StudentPaymentScheduleClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStudentPaymentScheduleRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(
					"info.diit.portal.accounts.model.impl.BatchFeeImpl")) {
			return translateOutputBatchFee(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.accounts.model.impl.BatchPaymentScheduleImpl")) {
			return translateOutputBatchPaymentSchedule(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.accounts.model.impl.CourseFeeImpl")) {
			return translateOutputCourseFee(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.accounts.model.impl.FeeTypeImpl")) {
			return translateOutputFeeType(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.accounts.model.impl.PaymentImpl")) {
			return translateOutputPayment(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.accounts.model.impl.StudentDiscountImpl")) {
			return translateOutputStudentDiscount(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.accounts.model.impl.StudentFeeImpl")) {
			return translateOutputStudentFee(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.accounts.model.impl.StudentPaymentScheduleImpl")) {
			return translateOutputStudentPaymentSchedule(oldModel);
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals(
					"info.diit.portal.accounts.NoSuchBatchFeeException")) {
			return new info.diit.portal.accounts.NoSuchBatchFeeException();
		}

		if (className.equals(
					"info.diit.portal.accounts.NoSuchBatchPaymentScheduleException")) {
			return new info.diit.portal.accounts.NoSuchBatchPaymentScheduleException();
		}

		if (className.equals(
					"info.diit.portal.accounts.NoSuchCourseFeeException")) {
			return new info.diit.portal.accounts.NoSuchCourseFeeException();
		}

		if (className.equals("info.diit.portal.accounts.NoSuchFeeTypeException")) {
			return new info.diit.portal.accounts.NoSuchFeeTypeException();
		}

		if (className.equals("info.diit.portal.accounts.NoSuchPaymentException")) {
			return new info.diit.portal.accounts.NoSuchPaymentException();
		}

		if (className.equals(
					"info.diit.portal.accounts.NoSuchStudentDiscountException")) {
			return new info.diit.portal.accounts.NoSuchStudentDiscountException();
		}

		if (className.equals(
					"info.diit.portal.accounts.NoSuchStudentFeeException")) {
			return new info.diit.portal.accounts.NoSuchStudentFeeException();
		}

		if (className.equals(
					"info.diit.portal.accounts.NoSuchStudentPaymentScheduleException")) {
			return new info.diit.portal.accounts.NoSuchStudentPaymentScheduleException();
		}

		return throwable;
	}

	public static Object translateOutputBatchFee(BaseModel<?> oldModel) {
		BatchFeeClp newModel = new BatchFeeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBatchFeeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputBatchPaymentSchedule(
		BaseModel<?> oldModel) {
		BatchPaymentScheduleClp newModel = new BatchPaymentScheduleClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBatchPaymentScheduleRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCourseFee(BaseModel<?> oldModel) {
		CourseFeeClp newModel = new CourseFeeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCourseFeeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputFeeType(BaseModel<?> oldModel) {
		FeeTypeClp newModel = new FeeTypeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setFeeTypeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputPayment(BaseModel<?> oldModel) {
		PaymentClp newModel = new PaymentClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setPaymentRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStudentDiscount(BaseModel<?> oldModel) {
		StudentDiscountClp newModel = new StudentDiscountClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStudentDiscountRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStudentFee(BaseModel<?> oldModel) {
		StudentFeeClp newModel = new StudentFeeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStudentFeeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStudentPaymentSchedule(
		BaseModel<?> oldModel) {
		StudentPaymentScheduleClp newModel = new StudentPaymentScheduleClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStudentPaymentScheduleRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}
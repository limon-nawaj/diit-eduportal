/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.accounts.model.BatchFee;

/**
 * The persistence interface for the batch fee service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shamsuddin
 * @see BatchFeePersistenceImpl
 * @see BatchFeeUtil
 * @generated
 */
public interface BatchFeePersistence extends BasePersistence<BatchFee> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BatchFeeUtil} to access the batch fee persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the batch fee in the entity cache if it is enabled.
	*
	* @param batchFee the batch fee
	*/
	public void cacheResult(info.diit.portal.accounts.model.BatchFee batchFee);

	/**
	* Caches the batch fees in the entity cache if it is enabled.
	*
	* @param batchFees the batch fees
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.accounts.model.BatchFee> batchFees);

	/**
	* Creates a new batch fee with the primary key. Does not add the batch fee to the database.
	*
	* @param batchFeeId the primary key for the new batch fee
	* @return the new batch fee
	*/
	public info.diit.portal.accounts.model.BatchFee create(long batchFeeId);

	/**
	* Removes the batch fee with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchFeeId the primary key of the batch fee
	* @return the batch fee that was removed
	* @throws info.diit.portal.accounts.NoSuchBatchFeeException if a batch fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchFee remove(long batchFeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchBatchFeeException;

	public info.diit.portal.accounts.model.BatchFee updateImpl(
		info.diit.portal.accounts.model.BatchFee batchFee, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch fee with the primary key or throws a {@link info.diit.portal.accounts.NoSuchBatchFeeException} if it could not be found.
	*
	* @param batchFeeId the primary key of the batch fee
	* @return the batch fee
	* @throws info.diit.portal.accounts.NoSuchBatchFeeException if a batch fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchFee findByPrimaryKey(
		long batchFeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchBatchFeeException;

	/**
	* Returns the batch fee with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param batchFeeId the primary key of the batch fee
	* @return the batch fee, or <code>null</code> if a batch fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchFee fetchByPrimaryKey(
		long batchFeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the batch fees where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the matching batch fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.BatchFee> findByBatch(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch fees where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch fees
	* @param end the upper bound of the range of batch fees (not inclusive)
	* @return the range of matching batch fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.BatchFee> findByBatch(
		long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch fees where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch fees
	* @param end the upper bound of the range of batch fees (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.BatchFee> findByBatch(
		long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch fee in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch fee
	* @throws info.diit.portal.accounts.NoSuchBatchFeeException if a matching batch fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchFee findByBatch_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchBatchFeeException;

	/**
	* Returns the first batch fee in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch fee, or <code>null</code> if a matching batch fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchFee fetchByBatch_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch fee in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch fee
	* @throws info.diit.portal.accounts.NoSuchBatchFeeException if a matching batch fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchFee findByBatch_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchBatchFeeException;

	/**
	* Returns the last batch fee in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch fee, or <code>null</code> if a matching batch fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchFee fetchByBatch_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch fees before and after the current batch fee in the ordered set where batchId = &#63;.
	*
	* @param batchFeeId the primary key of the current batch fee
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch fee
	* @throws info.diit.portal.accounts.NoSuchBatchFeeException if a batch fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchFee[] findByBatch_PrevAndNext(
		long batchFeeId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchBatchFeeException;

	/**
	* Returns all the batch fees.
	*
	* @return the batch fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.BatchFee> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch fees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch fees
	* @param end the upper bound of the range of batch fees (not inclusive)
	* @return the range of batch fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.BatchFee> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch fees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch fees
	* @param end the upper bound of the range of batch fees (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of batch fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.BatchFee> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch fees where batchId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch fees from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch fees where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the number of matching batch fees
	* @throws SystemException if a system exception occurred
	*/
	public int countByBatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch fees.
	*
	* @return the number of batch fees
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
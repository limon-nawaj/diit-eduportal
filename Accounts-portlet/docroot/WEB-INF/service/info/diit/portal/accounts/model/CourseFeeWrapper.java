/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CourseFee}.
 * </p>
 *
 * @author    shamsuddin
 * @see       CourseFee
 * @generated
 */
public class CourseFeeWrapper implements CourseFee, ModelWrapper<CourseFee> {
	public CourseFeeWrapper(CourseFee courseFee) {
		_courseFee = courseFee;
	}

	public Class<?> getModelClass() {
		return CourseFee.class;
	}

	public String getModelClassName() {
		return CourseFee.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("courseFeeId", getCourseFeeId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("courseId", getCourseId());
		attributes.put("feeTypeId", getFeeTypeId());
		attributes.put("amount", getAmount());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long courseFeeId = (Long)attributes.get("courseFeeId");

		if (courseFeeId != null) {
			setCourseFeeId(courseFeeId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long courseId = (Long)attributes.get("courseId");

		if (courseId != null) {
			setCourseId(courseId);
		}

		Long feeTypeId = (Long)attributes.get("feeTypeId");

		if (feeTypeId != null) {
			setFeeTypeId(feeTypeId);
		}

		Double amount = (Double)attributes.get("amount");

		if (amount != null) {
			setAmount(amount);
		}
	}

	/**
	* Returns the primary key of this course fee.
	*
	* @return the primary key of this course fee
	*/
	public long getPrimaryKey() {
		return _courseFee.getPrimaryKey();
	}

	/**
	* Sets the primary key of this course fee.
	*
	* @param primaryKey the primary key of this course fee
	*/
	public void setPrimaryKey(long primaryKey) {
		_courseFee.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the course fee ID of this course fee.
	*
	* @return the course fee ID of this course fee
	*/
	public long getCourseFeeId() {
		return _courseFee.getCourseFeeId();
	}

	/**
	* Sets the course fee ID of this course fee.
	*
	* @param courseFeeId the course fee ID of this course fee
	*/
	public void setCourseFeeId(long courseFeeId) {
		_courseFee.setCourseFeeId(courseFeeId);
	}

	/**
	* Returns the company ID of this course fee.
	*
	* @return the company ID of this course fee
	*/
	public long getCompanyId() {
		return _courseFee.getCompanyId();
	}

	/**
	* Sets the company ID of this course fee.
	*
	* @param companyId the company ID of this course fee
	*/
	public void setCompanyId(long companyId) {
		_courseFee.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this course fee.
	*
	* @return the user ID of this course fee
	*/
	public long getUserId() {
		return _courseFee.getUserId();
	}

	/**
	* Sets the user ID of this course fee.
	*
	* @param userId the user ID of this course fee
	*/
	public void setUserId(long userId) {
		_courseFee.setUserId(userId);
	}

	/**
	* Returns the user uuid of this course fee.
	*
	* @return the user uuid of this course fee
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseFee.getUserUuid();
	}

	/**
	* Sets the user uuid of this course fee.
	*
	* @param userUuid the user uuid of this course fee
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_courseFee.setUserUuid(userUuid);
	}

	/**
	* Returns the create date of this course fee.
	*
	* @return the create date of this course fee
	*/
	public java.util.Date getCreateDate() {
		return _courseFee.getCreateDate();
	}

	/**
	* Sets the create date of this course fee.
	*
	* @param createDate the create date of this course fee
	*/
	public void setCreateDate(java.util.Date createDate) {
		_courseFee.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this course fee.
	*
	* @return the modified date of this course fee
	*/
	public java.util.Date getModifiedDate() {
		return _courseFee.getModifiedDate();
	}

	/**
	* Sets the modified date of this course fee.
	*
	* @param modifiedDate the modified date of this course fee
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_courseFee.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the course ID of this course fee.
	*
	* @return the course ID of this course fee
	*/
	public long getCourseId() {
		return _courseFee.getCourseId();
	}

	/**
	* Sets the course ID of this course fee.
	*
	* @param courseId the course ID of this course fee
	*/
	public void setCourseId(long courseId) {
		_courseFee.setCourseId(courseId);
	}

	/**
	* Returns the fee type ID of this course fee.
	*
	* @return the fee type ID of this course fee
	*/
	public long getFeeTypeId() {
		return _courseFee.getFeeTypeId();
	}

	/**
	* Sets the fee type ID of this course fee.
	*
	* @param feeTypeId the fee type ID of this course fee
	*/
	public void setFeeTypeId(long feeTypeId) {
		_courseFee.setFeeTypeId(feeTypeId);
	}

	/**
	* Returns the amount of this course fee.
	*
	* @return the amount of this course fee
	*/
	public double getAmount() {
		return _courseFee.getAmount();
	}

	/**
	* Sets the amount of this course fee.
	*
	* @param amount the amount of this course fee
	*/
	public void setAmount(double amount) {
		_courseFee.setAmount(amount);
	}

	public boolean isNew() {
		return _courseFee.isNew();
	}

	public void setNew(boolean n) {
		_courseFee.setNew(n);
	}

	public boolean isCachedModel() {
		return _courseFee.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_courseFee.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _courseFee.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _courseFee.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_courseFee.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _courseFee.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_courseFee.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CourseFeeWrapper((CourseFee)_courseFee.clone());
	}

	public int compareTo(info.diit.portal.accounts.model.CourseFee courseFee) {
		return _courseFee.compareTo(courseFee);
	}

	@Override
	public int hashCode() {
		return _courseFee.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.accounts.model.CourseFee> toCacheModel() {
		return _courseFee.toCacheModel();
	}

	public info.diit.portal.accounts.model.CourseFee toEscapedModel() {
		return new CourseFeeWrapper(_courseFee.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _courseFee.toString();
	}

	public java.lang.String toXmlString() {
		return _courseFee.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_courseFee.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public CourseFee getWrappedCourseFee() {
		return _courseFee;
	}

	public CourseFee getWrappedModel() {
		return _courseFee;
	}

	public void resetOriginalValues() {
		_courseFee.resetOriginalValues();
	}

	private CourseFee _courseFee;
}
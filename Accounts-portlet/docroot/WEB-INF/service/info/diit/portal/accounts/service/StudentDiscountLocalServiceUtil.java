/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the student discount local service. This utility wraps {@link info.diit.portal.accounts.service.impl.StudentDiscountLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author shamsuddin
 * @see StudentDiscountLocalService
 * @see info.diit.portal.accounts.service.base.StudentDiscountLocalServiceBaseImpl
 * @see info.diit.portal.accounts.service.impl.StudentDiscountLocalServiceImpl
 * @generated
 */
public class StudentDiscountLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.accounts.service.impl.StudentDiscountLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the student discount to the database. Also notifies the appropriate model listeners.
	*
	* @param studentDiscount the student discount
	* @return the student discount that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentDiscount addStudentDiscount(
		info.diit.portal.accounts.model.StudentDiscount studentDiscount)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addStudentDiscount(studentDiscount);
	}

	/**
	* Creates a new student discount with the primary key. Does not add the student discount to the database.
	*
	* @param studentDiscountId the primary key for the new student discount
	* @return the new student discount
	*/
	public static info.diit.portal.accounts.model.StudentDiscount createStudentDiscount(
		long studentDiscountId) {
		return getService().createStudentDiscount(studentDiscountId);
	}

	/**
	* Deletes the student discount with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param studentDiscountId the primary key of the student discount
	* @return the student discount that was removed
	* @throws PortalException if a student discount with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentDiscount deleteStudentDiscount(
		long studentDiscountId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteStudentDiscount(studentDiscountId);
	}

	/**
	* Deletes the student discount from the database. Also notifies the appropriate model listeners.
	*
	* @param studentDiscount the student discount
	* @return the student discount that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentDiscount deleteStudentDiscount(
		info.diit.portal.accounts.model.StudentDiscount studentDiscount)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteStudentDiscount(studentDiscount);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.accounts.model.StudentDiscount fetchStudentDiscount(
		long studentDiscountId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchStudentDiscount(studentDiscountId);
	}

	/**
	* Returns the student discount with the primary key.
	*
	* @param studentDiscountId the primary key of the student discount
	* @return the student discount
	* @throws PortalException if a student discount with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentDiscount getStudentDiscount(
		long studentDiscountId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getStudentDiscount(studentDiscountId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the student discounts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student discounts
	* @param end the upper bound of the range of student discounts (not inclusive)
	* @return the range of student discounts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.accounts.model.StudentDiscount> getStudentDiscounts(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getStudentDiscounts(start, end);
	}

	/**
	* Returns the number of student discounts.
	*
	* @return the number of student discounts
	* @throws SystemException if a system exception occurred
	*/
	public static int getStudentDiscountsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getStudentDiscountsCount();
	}

	/**
	* Updates the student discount in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param studentDiscount the student discount
	* @return the student discount that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentDiscount updateStudentDiscount(
		info.diit.portal.accounts.model.StudentDiscount studentDiscount)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateStudentDiscount(studentDiscount);
	}

	/**
	* Updates the student discount in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param studentDiscount the student discount
	* @param merge whether to merge the student discount with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the student discount that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentDiscount updateStudentDiscount(
		info.diit.portal.accounts.model.StudentDiscount studentDiscount,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateStudentDiscount(studentDiscount, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static info.diit.portal.accounts.model.StudentDiscount getStudentDiscount(
		long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getStudentDiscount(studentId, batchId);
	}

	public static void clearService() {
		_service = null;
	}

	public static StudentDiscountLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					StudentDiscountLocalService.class.getName());

			if (invokableLocalService instanceof StudentDiscountLocalService) {
				_service = (StudentDiscountLocalService)invokableLocalService;
			}
			else {
				_service = new StudentDiscountLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(StudentDiscountLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(StudentDiscountLocalService service) {
	}

	private static StudentDiscountLocalService _service;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link StudentPaymentScheduleLocalService}.
 * </p>
 *
 * @author    shamsuddin
 * @see       StudentPaymentScheduleLocalService
 * @generated
 */
public class StudentPaymentScheduleLocalServiceWrapper
	implements StudentPaymentScheduleLocalService,
		ServiceWrapper<StudentPaymentScheduleLocalService> {
	public StudentPaymentScheduleLocalServiceWrapper(
		StudentPaymentScheduleLocalService studentPaymentScheduleLocalService) {
		_studentPaymentScheduleLocalService = studentPaymentScheduleLocalService;
	}

	/**
	* Adds the student payment schedule to the database. Also notifies the appropriate model listeners.
	*
	* @param studentPaymentSchedule the student payment schedule
	* @return the student payment schedule that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule addStudentPaymentSchedule(
		info.diit.portal.accounts.model.StudentPaymentSchedule studentPaymentSchedule)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.addStudentPaymentSchedule(studentPaymentSchedule);
	}

	/**
	* Creates a new student payment schedule with the primary key. Does not add the student payment schedule to the database.
	*
	* @param studentPaymentScheduleId the primary key for the new student payment schedule
	* @return the new student payment schedule
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule createStudentPaymentSchedule(
		long studentPaymentScheduleId) {
		return _studentPaymentScheduleLocalService.createStudentPaymentSchedule(studentPaymentScheduleId);
	}

	/**
	* Deletes the student payment schedule with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param studentPaymentScheduleId the primary key of the student payment schedule
	* @return the student payment schedule that was removed
	* @throws PortalException if a student payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule deleteStudentPaymentSchedule(
		long studentPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.deleteStudentPaymentSchedule(studentPaymentScheduleId);
	}

	/**
	* Deletes the student payment schedule from the database. Also notifies the appropriate model listeners.
	*
	* @param studentPaymentSchedule the student payment schedule
	* @return the student payment schedule that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule deleteStudentPaymentSchedule(
		info.diit.portal.accounts.model.StudentPaymentSchedule studentPaymentSchedule)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.deleteStudentPaymentSchedule(studentPaymentSchedule);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _studentPaymentScheduleLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.accounts.model.StudentPaymentSchedule fetchStudentPaymentSchedule(
		long studentPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.fetchStudentPaymentSchedule(studentPaymentScheduleId);
	}

	/**
	* Returns the student payment schedule with the primary key.
	*
	* @param studentPaymentScheduleId the primary key of the student payment schedule
	* @return the student payment schedule
	* @throws PortalException if a student payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule getStudentPaymentSchedule(
		long studentPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.getStudentPaymentSchedule(studentPaymentScheduleId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the student payment schedules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student payment schedules
	* @param end the upper bound of the range of student payment schedules (not inclusive)
	* @return the range of student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentPaymentSchedule> getStudentPaymentSchedules(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.getStudentPaymentSchedules(start,
			end);
	}

	/**
	* Returns the number of student payment schedules.
	*
	* @return the number of student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public int getStudentPaymentSchedulesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.getStudentPaymentSchedulesCount();
	}

	/**
	* Updates the student payment schedule in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param studentPaymentSchedule the student payment schedule
	* @return the student payment schedule that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule updateStudentPaymentSchedule(
		info.diit.portal.accounts.model.StudentPaymentSchedule studentPaymentSchedule)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.updateStudentPaymentSchedule(studentPaymentSchedule);
	}

	/**
	* Updates the student payment schedule in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param studentPaymentSchedule the student payment schedule
	* @param merge whether to merge the student payment schedule with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the student payment schedule that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule updateStudentPaymentSchedule(
		info.diit.portal.accounts.model.StudentPaymentSchedule studentPaymentSchedule,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.updateStudentPaymentSchedule(studentPaymentSchedule,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _studentPaymentScheduleLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_studentPaymentScheduleLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _studentPaymentScheduleLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	public java.util.List<info.diit.portal.accounts.model.StudentPaymentSchedule> getStudentPaymentScheduleList(
		long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentScheduleLocalService.getStudentPaymentScheduleList(studentId,
			batchId);
	}

	public void updateStudentPaymentSchedules(long studentId, long batchId,
		java.util.List<info.diit.portal.accounts.model.StudentPaymentSchedule> studentPaymentSchedules)
		throws com.liferay.portal.kernel.exception.SystemException {
		_studentPaymentScheduleLocalService.updateStudentPaymentSchedules(studentId,
			batchId, studentPaymentSchedules);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public StudentPaymentScheduleLocalService getWrappedStudentPaymentScheduleLocalService() {
		return _studentPaymentScheduleLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedStudentPaymentScheduleLocalService(
		StudentPaymentScheduleLocalService studentPaymentScheduleLocalService) {
		_studentPaymentScheduleLocalService = studentPaymentScheduleLocalService;
	}

	public StudentPaymentScheduleLocalService getWrappedService() {
		return _studentPaymentScheduleLocalService;
	}

	public void setWrappedService(
		StudentPaymentScheduleLocalService studentPaymentScheduleLocalService) {
		_studentPaymentScheduleLocalService = studentPaymentScheduleLocalService;
	}

	private StudentPaymentScheduleLocalService _studentPaymentScheduleLocalService;
}
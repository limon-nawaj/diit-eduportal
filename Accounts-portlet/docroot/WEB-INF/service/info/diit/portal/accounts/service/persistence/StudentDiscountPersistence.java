/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.accounts.model.StudentDiscount;

/**
 * The persistence interface for the student discount service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shamsuddin
 * @see StudentDiscountPersistenceImpl
 * @see StudentDiscountUtil
 * @generated
 */
public interface StudentDiscountPersistence extends BasePersistence<StudentDiscount> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link StudentDiscountUtil} to access the student discount persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the student discount in the entity cache if it is enabled.
	*
	* @param studentDiscount the student discount
	*/
	public void cacheResult(
		info.diit.portal.accounts.model.StudentDiscount studentDiscount);

	/**
	* Caches the student discounts in the entity cache if it is enabled.
	*
	* @param studentDiscounts the student discounts
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.accounts.model.StudentDiscount> studentDiscounts);

	/**
	* Creates a new student discount with the primary key. Does not add the student discount to the database.
	*
	* @param studentDiscountId the primary key for the new student discount
	* @return the new student discount
	*/
	public info.diit.portal.accounts.model.StudentDiscount create(
		long studentDiscountId);

	/**
	* Removes the student discount with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param studentDiscountId the primary key of the student discount
	* @return the student discount that was removed
	* @throws info.diit.portal.accounts.NoSuchStudentDiscountException if a student discount with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentDiscount remove(
		long studentDiscountId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentDiscountException;

	public info.diit.portal.accounts.model.StudentDiscount updateImpl(
		info.diit.portal.accounts.model.StudentDiscount studentDiscount,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student discount with the primary key or throws a {@link info.diit.portal.accounts.NoSuchStudentDiscountException} if it could not be found.
	*
	* @param studentDiscountId the primary key of the student discount
	* @return the student discount
	* @throws info.diit.portal.accounts.NoSuchStudentDiscountException if a student discount with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentDiscount findByPrimaryKey(
		long studentDiscountId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentDiscountException;

	/**
	* Returns the student discount with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param studentDiscountId the primary key of the student discount
	* @return the student discount, or <code>null</code> if a student discount with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentDiscount fetchByPrimaryKey(
		long studentDiscountId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the student discounts where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @return the matching student discounts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentDiscount> findByStudentBatch(
		long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the student discounts where studentId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of student discounts
	* @param end the upper bound of the range of student discounts (not inclusive)
	* @return the range of matching student discounts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentDiscount> findByStudentBatch(
		long studentId, long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the student discounts where studentId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of student discounts
	* @param end the upper bound of the range of student discounts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching student discounts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentDiscount> findByStudentBatch(
		long studentId, long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student discount
	* @throws info.diit.portal.accounts.NoSuchStudentDiscountException if a matching student discount could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentDiscount findByStudentBatch_First(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentDiscountException;

	/**
	* Returns the first student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student discount, or <code>null</code> if a matching student discount could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentDiscount fetchByStudentBatch_First(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student discount
	* @throws info.diit.portal.accounts.NoSuchStudentDiscountException if a matching student discount could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentDiscount findByStudentBatch_Last(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentDiscountException;

	/**
	* Returns the last student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student discount, or <code>null</code> if a matching student discount could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentDiscount fetchByStudentBatch_Last(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student discounts before and after the current student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentDiscountId the primary key of the current student discount
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next student discount
	* @throws info.diit.portal.accounts.NoSuchStudentDiscountException if a student discount with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentDiscount[] findByStudentBatch_PrevAndNext(
		long studentDiscountId, long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentDiscountException;

	/**
	* Returns all the student discounts.
	*
	* @return the student discounts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentDiscount> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the student discounts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student discounts
	* @param end the upper bound of the range of student discounts (not inclusive)
	* @return the range of student discounts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentDiscount> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the student discounts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student discounts
	* @param end the upper bound of the range of student discounts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of student discounts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentDiscount> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the student discounts where studentId = &#63; and batchId = &#63; from the database.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByStudentBatch(long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the student discounts from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of student discounts where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @return the number of matching student discounts
	* @throws SystemException if a system exception occurred
	*/
	public int countByStudentBatch(long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of student discounts.
	*
	* @return the number of student discounts
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
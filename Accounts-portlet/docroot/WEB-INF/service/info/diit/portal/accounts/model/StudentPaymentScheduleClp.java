/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.accounts.service.StudentPaymentScheduleLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author shamsuddin
 */
public class StudentPaymentScheduleClp extends BaseModelImpl<StudentPaymentSchedule>
	implements StudentPaymentSchedule {
	public StudentPaymentScheduleClp() {
	}

	public Class<?> getModelClass() {
		return StudentPaymentSchedule.class;
	}

	public String getModelClassName() {
		return StudentPaymentSchedule.class.getName();
	}

	public long getPrimaryKey() {
		return _studentPaymentScheduleId;
	}

	public void setPrimaryKey(long primaryKey) {
		setStudentPaymentScheduleId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_studentPaymentScheduleId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("studentPaymentScheduleId", getStudentPaymentScheduleId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("studentId", getStudentId());
		attributes.put("batchId", getBatchId());
		attributes.put("scheduleDate", getScheduleDate());
		attributes.put("feeTypeId", getFeeTypeId());
		attributes.put("amount", getAmount());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long studentPaymentScheduleId = (Long)attributes.get(
				"studentPaymentScheduleId");

		if (studentPaymentScheduleId != null) {
			setStudentPaymentScheduleId(studentPaymentScheduleId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Date scheduleDate = (Date)attributes.get("scheduleDate");

		if (scheduleDate != null) {
			setScheduleDate(scheduleDate);
		}

		Long feeTypeId = (Long)attributes.get("feeTypeId");

		if (feeTypeId != null) {
			setFeeTypeId(feeTypeId);
		}

		Double amount = (Double)attributes.get("amount");

		if (amount != null) {
			setAmount(amount);
		}
	}

	public long getStudentPaymentScheduleId() {
		return _studentPaymentScheduleId;
	}

	public void setStudentPaymentScheduleId(long studentPaymentScheduleId) {
		_studentPaymentScheduleId = studentPaymentScheduleId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public Date getScheduleDate() {
		return _scheduleDate;
	}

	public void setScheduleDate(Date scheduleDate) {
		_scheduleDate = scheduleDate;
	}

	public long getFeeTypeId() {
		return _feeTypeId;
	}

	public void setFeeTypeId(long feeTypeId) {
		_feeTypeId = feeTypeId;
	}

	public double getAmount() {
		return _amount;
	}

	public void setAmount(double amount) {
		_amount = amount;
	}

	public BaseModel<?> getStudentPaymentScheduleRemoteModel() {
		return _studentPaymentScheduleRemoteModel;
	}

	public void setStudentPaymentScheduleRemoteModel(
		BaseModel<?> studentPaymentScheduleRemoteModel) {
		_studentPaymentScheduleRemoteModel = studentPaymentScheduleRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			StudentPaymentScheduleLocalServiceUtil.addStudentPaymentSchedule(this);
		}
		else {
			StudentPaymentScheduleLocalServiceUtil.updateStudentPaymentSchedule(this);
		}
	}

	@Override
	public StudentPaymentSchedule toEscapedModel() {
		return (StudentPaymentSchedule)Proxy.newProxyInstance(StudentPaymentSchedule.class.getClassLoader(),
			new Class[] { StudentPaymentSchedule.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		StudentPaymentScheduleClp clone = new StudentPaymentScheduleClp();

		clone.setStudentPaymentScheduleId(getStudentPaymentScheduleId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setStudentId(getStudentId());
		clone.setBatchId(getBatchId());
		clone.setScheduleDate(getScheduleDate());
		clone.setFeeTypeId(getFeeTypeId());
		clone.setAmount(getAmount());

		return clone;
	}

	public int compareTo(StudentPaymentSchedule studentPaymentSchedule) {
		long primaryKey = studentPaymentSchedule.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		StudentPaymentScheduleClp studentPaymentSchedule = null;

		try {
			studentPaymentSchedule = (StudentPaymentScheduleClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = studentPaymentSchedule.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{studentPaymentScheduleId=");
		sb.append(getStudentPaymentScheduleId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", studentId=");
		sb.append(getStudentId());
		sb.append(", batchId=");
		sb.append(getBatchId());
		sb.append(", scheduleDate=");
		sb.append(getScheduleDate());
		sb.append(", feeTypeId=");
		sb.append(getFeeTypeId());
		sb.append(", amount=");
		sb.append(getAmount());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.accounts.model.StudentPaymentSchedule");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>studentPaymentScheduleId</column-name><column-value><![CDATA[");
		sb.append(getStudentPaymentScheduleId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>studentId</column-name><column-value><![CDATA[");
		sb.append(getStudentId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>batchId</column-name><column-value><![CDATA[");
		sb.append(getBatchId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>scheduleDate</column-name><column-value><![CDATA[");
		sb.append(getScheduleDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>feeTypeId</column-name><column-value><![CDATA[");
		sb.append(getFeeTypeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>amount</column-name><column-value><![CDATA[");
		sb.append(getAmount());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _studentPaymentScheduleId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private Date _createDate;
	private Date _modifiedDate;
	private long _studentId;
	private long _batchId;
	private Date _scheduleDate;
	private long _feeTypeId;
	private double _amount;
	private BaseModel<?> _studentPaymentScheduleRemoteModel;
}
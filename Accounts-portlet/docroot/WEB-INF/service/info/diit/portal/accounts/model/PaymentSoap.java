/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    shamsuddin
 * @generated
 */
public class PaymentSoap implements Serializable {
	public static PaymentSoap toSoapModel(Payment model) {
		PaymentSoap soapModel = new PaymentSoap();

		soapModel.setPaymentId(model.getPaymentId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setStudentId(model.getStudentId());
		soapModel.setBatchId(model.getBatchId());
		soapModel.setFeeTypeId(model.getFeeTypeId());
		soapModel.setAmount(model.getAmount());
		soapModel.setPaymentDate(model.getPaymentDate());

		return soapModel;
	}

	public static PaymentSoap[] toSoapModels(Payment[] models) {
		PaymentSoap[] soapModels = new PaymentSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PaymentSoap[][] toSoapModels(Payment[][] models) {
		PaymentSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PaymentSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PaymentSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PaymentSoap[] toSoapModels(List<Payment> models) {
		List<PaymentSoap> soapModels = new ArrayList<PaymentSoap>(models.size());

		for (Payment model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PaymentSoap[soapModels.size()]);
	}

	public PaymentSoap() {
	}

	public long getPrimaryKey() {
		return _paymentId;
	}

	public void setPrimaryKey(long pk) {
		setPaymentId(pk);
	}

	public long getPaymentId() {
		return _paymentId;
	}

	public void setPaymentId(long paymentId) {
		_paymentId = paymentId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public long getFeeTypeId() {
		return _feeTypeId;
	}

	public void setFeeTypeId(long feeTypeId) {
		_feeTypeId = feeTypeId;
	}

	public double getAmount() {
		return _amount;
	}

	public void setAmount(double amount) {
		_amount = amount;
	}

	public Date getPaymentDate() {
		return _paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		_paymentDate = paymentDate;
	}

	private long _paymentId;
	private long _companyId;
	private long _userId;
	private Date _createDate;
	private Date _modifiedDate;
	private long _studentId;
	private long _batchId;
	private long _feeTypeId;
	private double _amount;
	private Date _paymentDate;
}
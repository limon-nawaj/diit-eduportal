/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.accounts.model.StudentFee;

import java.util.List;

/**
 * The persistence utility for the student fee service. This utility wraps {@link StudentFeePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shamsuddin
 * @see StudentFeePersistence
 * @see StudentFeePersistenceImpl
 * @generated
 */
public class StudentFeeUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(StudentFee studentFee) {
		getPersistence().clearCache(studentFee);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<StudentFee> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<StudentFee> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<StudentFee> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static StudentFee update(StudentFee studentFee, boolean merge)
		throws SystemException {
		return getPersistence().update(studentFee, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static StudentFee update(StudentFee studentFee, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(studentFee, merge, serviceContext);
	}

	/**
	* Caches the student fee in the entity cache if it is enabled.
	*
	* @param studentFee the student fee
	*/
	public static void cacheResult(
		info.diit.portal.accounts.model.StudentFee studentFee) {
		getPersistence().cacheResult(studentFee);
	}

	/**
	* Caches the student fees in the entity cache if it is enabled.
	*
	* @param studentFees the student fees
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.accounts.model.StudentFee> studentFees) {
		getPersistence().cacheResult(studentFees);
	}

	/**
	* Creates a new student fee with the primary key. Does not add the student fee to the database.
	*
	* @param studentFeeId the primary key for the new student fee
	* @return the new student fee
	*/
	public static info.diit.portal.accounts.model.StudentFee create(
		long studentFeeId) {
		return getPersistence().create(studentFeeId);
	}

	/**
	* Removes the student fee with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param studentFeeId the primary key of the student fee
	* @return the student fee that was removed
	* @throws info.diit.portal.accounts.NoSuchStudentFeeException if a student fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentFee remove(
		long studentFeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentFeeException {
		return getPersistence().remove(studentFeeId);
	}

	public static info.diit.portal.accounts.model.StudentFee updateImpl(
		info.diit.portal.accounts.model.StudentFee studentFee, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(studentFee, merge);
	}

	/**
	* Returns the student fee with the primary key or throws a {@link info.diit.portal.accounts.NoSuchStudentFeeException} if it could not be found.
	*
	* @param studentFeeId the primary key of the student fee
	* @return the student fee
	* @throws info.diit.portal.accounts.NoSuchStudentFeeException if a student fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentFee findByPrimaryKey(
		long studentFeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentFeeException {
		return getPersistence().findByPrimaryKey(studentFeeId);
	}

	/**
	* Returns the student fee with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param studentFeeId the primary key of the student fee
	* @return the student fee, or <code>null</code> if a student fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentFee fetchByPrimaryKey(
		long studentFeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(studentFeeId);
	}

	/**
	* Returns all the student fees where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @return the matching student fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.accounts.model.StudentFee> findByStudentBatch(
		long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStudentBatch(studentId, batchId);
	}

	/**
	* Returns a range of all the student fees where studentId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of student fees
	* @param end the upper bound of the range of student fees (not inclusive)
	* @return the range of matching student fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.accounts.model.StudentFee> findByStudentBatch(
		long studentId, long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentBatch(studentId, batchId, start, end);
	}

	/**
	* Returns an ordered range of all the student fees where studentId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of student fees
	* @param end the upper bound of the range of student fees (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching student fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.accounts.model.StudentFee> findByStudentBatch(
		long studentId, long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentBatch(studentId, batchId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student fee
	* @throws info.diit.portal.accounts.NoSuchStudentFeeException if a matching student fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentFee findByStudentBatch_First(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentFeeException {
		return getPersistence()
				   .findByStudentBatch_First(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the first student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student fee, or <code>null</code> if a matching student fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentFee fetchByStudentBatch_First(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentBatch_First(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the last student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student fee
	* @throws info.diit.portal.accounts.NoSuchStudentFeeException if a matching student fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentFee findByStudentBatch_Last(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentFeeException {
		return getPersistence()
				   .findByStudentBatch_Last(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the last student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student fee, or <code>null</code> if a matching student fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentFee fetchByStudentBatch_Last(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentBatch_Last(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the student fees before and after the current student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentFeeId the primary key of the current student fee
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next student fee
	* @throws info.diit.portal.accounts.NoSuchStudentFeeException if a student fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentFee[] findByStudentBatch_PrevAndNext(
		long studentFeeId, long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentFeeException {
		return getPersistence()
				   .findByStudentBatch_PrevAndNext(studentFeeId, studentId,
			batchId, orderByComparator);
	}

	/**
	* Returns all the student fees.
	*
	* @return the student fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.accounts.model.StudentFee> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the student fees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student fees
	* @param end the upper bound of the range of student fees (not inclusive)
	* @return the range of student fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.accounts.model.StudentFee> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the student fees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student fees
	* @param end the upper bound of the range of student fees (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of student fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.accounts.model.StudentFee> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the student fees where studentId = &#63; and batchId = &#63; from the database.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByStudentBatch(long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByStudentBatch(studentId, batchId);
	}

	/**
	* Removes all the student fees from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of student fees where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @return the number of matching student fees
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStudentBatch(long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStudentBatch(studentId, batchId);
	}

	/**
	* Returns the number of student fees.
	*
	* @return the number of student fees
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static StudentFeePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (StudentFeePersistence)PortletBeanLocatorUtil.locate(info.diit.portal.accounts.service.ClpSerializer.getServletContextName(),
					StudentFeePersistence.class.getName());

			ReferenceRegistry.registerReference(StudentFeeUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(StudentFeePersistence persistence) {
	}

	private static StudentFeePersistence _persistence;
}
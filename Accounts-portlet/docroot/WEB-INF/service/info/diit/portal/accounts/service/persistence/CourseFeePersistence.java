/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.accounts.model.CourseFee;

/**
 * The persistence interface for the course fee service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shamsuddin
 * @see CourseFeePersistenceImpl
 * @see CourseFeeUtil
 * @generated
 */
public interface CourseFeePersistence extends BasePersistence<CourseFee> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CourseFeeUtil} to access the course fee persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the course fee in the entity cache if it is enabled.
	*
	* @param courseFee the course fee
	*/
	public void cacheResult(info.diit.portal.accounts.model.CourseFee courseFee);

	/**
	* Caches the course fees in the entity cache if it is enabled.
	*
	* @param courseFees the course fees
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.accounts.model.CourseFee> courseFees);

	/**
	* Creates a new course fee with the primary key. Does not add the course fee to the database.
	*
	* @param courseFeeId the primary key for the new course fee
	* @return the new course fee
	*/
	public info.diit.portal.accounts.model.CourseFee create(long courseFeeId);

	/**
	* Removes the course fee with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param courseFeeId the primary key of the course fee
	* @return the course fee that was removed
	* @throws info.diit.portal.accounts.NoSuchCourseFeeException if a course fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.CourseFee remove(long courseFeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchCourseFeeException;

	public info.diit.portal.accounts.model.CourseFee updateImpl(
		info.diit.portal.accounts.model.CourseFee courseFee, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course fee with the primary key or throws a {@link info.diit.portal.accounts.NoSuchCourseFeeException} if it could not be found.
	*
	* @param courseFeeId the primary key of the course fee
	* @return the course fee
	* @throws info.diit.portal.accounts.NoSuchCourseFeeException if a course fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.CourseFee findByPrimaryKey(
		long courseFeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchCourseFeeException;

	/**
	* Returns the course fee with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param courseFeeId the primary key of the course fee
	* @return the course fee, or <code>null</code> if a course fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.CourseFee fetchByPrimaryKey(
		long courseFeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the course fees where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the matching course fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.CourseFee> findByCourse(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course fees where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course fees
	* @param end the upper bound of the range of course fees (not inclusive)
	* @return the range of matching course fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.CourseFee> findByCourse(
		long courseId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course fees where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course fees
	* @param end the upper bound of the range of course fees (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.CourseFee> findByCourse(
		long courseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course fee in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course fee
	* @throws info.diit.portal.accounts.NoSuchCourseFeeException if a matching course fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.CourseFee findByCourse_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchCourseFeeException;

	/**
	* Returns the first course fee in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course fee, or <code>null</code> if a matching course fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.CourseFee fetchByCourse_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course fee in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course fee
	* @throws info.diit.portal.accounts.NoSuchCourseFeeException if a matching course fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.CourseFee findByCourse_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchCourseFeeException;

	/**
	* Returns the last course fee in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course fee, or <code>null</code> if a matching course fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.CourseFee fetchByCourse_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course fees before and after the current course fee in the ordered set where courseId = &#63;.
	*
	* @param courseFeeId the primary key of the current course fee
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course fee
	* @throws info.diit.portal.accounts.NoSuchCourseFeeException if a course fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.CourseFee[] findByCourse_PrevAndNext(
		long courseFeeId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchCourseFeeException;

	/**
	* Returns all the course fees.
	*
	* @return the course fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.CourseFee> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course fees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course fees
	* @param end the upper bound of the range of course fees (not inclusive)
	* @return the range of course fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.CourseFee> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course fees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course fees
	* @param end the upper bound of the range of course fees (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of course fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.CourseFee> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course fees where courseId = &#63; from the database.
	*
	* @param courseId the course ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCourse(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course fees from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course fees where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the number of matching course fees
	* @throws SystemException if a system exception occurred
	*/
	public int countByCourse(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course fees.
	*
	* @return the number of course fees
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
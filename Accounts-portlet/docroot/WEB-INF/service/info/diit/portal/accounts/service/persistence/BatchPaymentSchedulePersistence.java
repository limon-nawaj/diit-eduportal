/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.accounts.model.BatchPaymentSchedule;

/**
 * The persistence interface for the batch payment schedule service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shamsuddin
 * @see BatchPaymentSchedulePersistenceImpl
 * @see BatchPaymentScheduleUtil
 * @generated
 */
public interface BatchPaymentSchedulePersistence extends BasePersistence<BatchPaymentSchedule> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BatchPaymentScheduleUtil} to access the batch payment schedule persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the batch payment schedule in the entity cache if it is enabled.
	*
	* @param batchPaymentSchedule the batch payment schedule
	*/
	public void cacheResult(
		info.diit.portal.accounts.model.BatchPaymentSchedule batchPaymentSchedule);

	/**
	* Caches the batch payment schedules in the entity cache if it is enabled.
	*
	* @param batchPaymentSchedules the batch payment schedules
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.accounts.model.BatchPaymentSchedule> batchPaymentSchedules);

	/**
	* Creates a new batch payment schedule with the primary key. Does not add the batch payment schedule to the database.
	*
	* @param batchPaymentScheduleId the primary key for the new batch payment schedule
	* @return the new batch payment schedule
	*/
	public info.diit.portal.accounts.model.BatchPaymentSchedule create(
		long batchPaymentScheduleId);

	/**
	* Removes the batch payment schedule with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchPaymentScheduleId the primary key of the batch payment schedule
	* @return the batch payment schedule that was removed
	* @throws info.diit.portal.accounts.NoSuchBatchPaymentScheduleException if a batch payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchPaymentSchedule remove(
		long batchPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchBatchPaymentScheduleException;

	public info.diit.portal.accounts.model.BatchPaymentSchedule updateImpl(
		info.diit.portal.accounts.model.BatchPaymentSchedule batchPaymentSchedule,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch payment schedule with the primary key or throws a {@link info.diit.portal.accounts.NoSuchBatchPaymentScheduleException} if it could not be found.
	*
	* @param batchPaymentScheduleId the primary key of the batch payment schedule
	* @return the batch payment schedule
	* @throws info.diit.portal.accounts.NoSuchBatchPaymentScheduleException if a batch payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchPaymentSchedule findByPrimaryKey(
		long batchPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchBatchPaymentScheduleException;

	/**
	* Returns the batch payment schedule with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param batchPaymentScheduleId the primary key of the batch payment schedule
	* @return the batch payment schedule, or <code>null</code> if a batch payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchPaymentSchedule fetchByPrimaryKey(
		long batchPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the batch payment schedules where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the matching batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.BatchPaymentSchedule> findByBatch(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch payment schedules where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch payment schedules
	* @param end the upper bound of the range of batch payment schedules (not inclusive)
	* @return the range of matching batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.BatchPaymentSchedule> findByBatch(
		long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch payment schedules where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch payment schedules
	* @param end the upper bound of the range of batch payment schedules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.BatchPaymentSchedule> findByBatch(
		long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch payment schedule in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch payment schedule
	* @throws info.diit.portal.accounts.NoSuchBatchPaymentScheduleException if a matching batch payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchPaymentSchedule findByBatch_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchBatchPaymentScheduleException;

	/**
	* Returns the first batch payment schedule in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch payment schedule, or <code>null</code> if a matching batch payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchPaymentSchedule fetchByBatch_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch payment schedule in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch payment schedule
	* @throws info.diit.portal.accounts.NoSuchBatchPaymentScheduleException if a matching batch payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchPaymentSchedule findByBatch_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchBatchPaymentScheduleException;

	/**
	* Returns the last batch payment schedule in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch payment schedule, or <code>null</code> if a matching batch payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchPaymentSchedule fetchByBatch_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch payment schedules before and after the current batch payment schedule in the ordered set where batchId = &#63;.
	*
	* @param batchPaymentScheduleId the primary key of the current batch payment schedule
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch payment schedule
	* @throws info.diit.portal.accounts.NoSuchBatchPaymentScheduleException if a batch payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchPaymentSchedule[] findByBatch_PrevAndNext(
		long batchPaymentScheduleId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchBatchPaymentScheduleException;

	/**
	* Returns all the batch payment schedules.
	*
	* @return the batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.BatchPaymentSchedule> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch payment schedules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch payment schedules
	* @param end the upper bound of the range of batch payment schedules (not inclusive)
	* @return the range of batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.BatchPaymentSchedule> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch payment schedules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch payment schedules
	* @param end the upper bound of the range of batch payment schedules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.BatchPaymentSchedule> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch payment schedules where batchId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch payment schedules from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch payment schedules where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the number of matching batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public int countByBatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch payment schedules.
	*
	* @return the number of batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link BatchFeeLocalService}.
 * </p>
 *
 * @author    shamsuddin
 * @see       BatchFeeLocalService
 * @generated
 */
public class BatchFeeLocalServiceWrapper implements BatchFeeLocalService,
	ServiceWrapper<BatchFeeLocalService> {
	public BatchFeeLocalServiceWrapper(
		BatchFeeLocalService batchFeeLocalService) {
		_batchFeeLocalService = batchFeeLocalService;
	}

	/**
	* Adds the batch fee to the database. Also notifies the appropriate model listeners.
	*
	* @param batchFee the batch fee
	* @return the batch fee that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchFee addBatchFee(
		info.diit.portal.accounts.model.BatchFee batchFee)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.addBatchFee(batchFee);
	}

	/**
	* Creates a new batch fee with the primary key. Does not add the batch fee to the database.
	*
	* @param batchFeeId the primary key for the new batch fee
	* @return the new batch fee
	*/
	public info.diit.portal.accounts.model.BatchFee createBatchFee(
		long batchFeeId) {
		return _batchFeeLocalService.createBatchFee(batchFeeId);
	}

	/**
	* Deletes the batch fee with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchFeeId the primary key of the batch fee
	* @return the batch fee that was removed
	* @throws PortalException if a batch fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchFee deleteBatchFee(
		long batchFeeId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.deleteBatchFee(batchFeeId);
	}

	/**
	* Deletes the batch fee from the database. Also notifies the appropriate model listeners.
	*
	* @param batchFee the batch fee
	* @return the batch fee that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchFee deleteBatchFee(
		info.diit.portal.accounts.model.BatchFee batchFee)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.deleteBatchFee(batchFee);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _batchFeeLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.accounts.model.BatchFee fetchBatchFee(
		long batchFeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.fetchBatchFee(batchFeeId);
	}

	/**
	* Returns the batch fee with the primary key.
	*
	* @param batchFeeId the primary key of the batch fee
	* @return the batch fee
	* @throws PortalException if a batch fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchFee getBatchFee(long batchFeeId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.getBatchFee(batchFeeId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the batch fees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch fees
	* @param end the upper bound of the range of batch fees (not inclusive)
	* @return the range of batch fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.BatchFee> getBatchFees(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.getBatchFees(start, end);
	}

	/**
	* Returns the number of batch fees.
	*
	* @return the number of batch fees
	* @throws SystemException if a system exception occurred
	*/
	public int getBatchFeesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.getBatchFeesCount();
	}

	/**
	* Updates the batch fee in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param batchFee the batch fee
	* @return the batch fee that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchFee updateBatchFee(
		info.diit.portal.accounts.model.BatchFee batchFee)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.updateBatchFee(batchFee);
	}

	/**
	* Updates the batch fee in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param batchFee the batch fee
	* @param merge whether to merge the batch fee with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the batch fee that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.BatchFee updateBatchFee(
		info.diit.portal.accounts.model.BatchFee batchFee, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.updateBatchFee(batchFee, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _batchFeeLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_batchFeeLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _batchFeeLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.accounts.model.BatchFee> getBatchFeesList(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchFeeLocalService.getBatchFeesList(batchId);
	}

	public void updateBatchFees(long batchId,
		java.util.List<info.diit.portal.accounts.model.BatchFee> batchFees,
		java.util.List<info.diit.portal.accounts.model.BatchPaymentSchedule> batchPaymentSchedules)
		throws com.liferay.portal.kernel.exception.SystemException {
		_batchFeeLocalService.updateBatchFees(batchId, batchFees,
			batchPaymentSchedules);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public BatchFeeLocalService getWrappedBatchFeeLocalService() {
		return _batchFeeLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedBatchFeeLocalService(
		BatchFeeLocalService batchFeeLocalService) {
		_batchFeeLocalService = batchFeeLocalService;
	}

	public BatchFeeLocalService getWrappedService() {
		return _batchFeeLocalService;
	}

	public void setWrappedService(BatchFeeLocalService batchFeeLocalService) {
		_batchFeeLocalService = batchFeeLocalService;
	}

	private BatchFeeLocalService _batchFeeLocalService;
}
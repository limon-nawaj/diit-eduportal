/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link FeeTypeLocalService}.
 * </p>
 *
 * @author    shamsuddin
 * @see       FeeTypeLocalService
 * @generated
 */
public class FeeTypeLocalServiceWrapper implements FeeTypeLocalService,
	ServiceWrapper<FeeTypeLocalService> {
	public FeeTypeLocalServiceWrapper(FeeTypeLocalService feeTypeLocalService) {
		_feeTypeLocalService = feeTypeLocalService;
	}

	/**
	* Adds the fee type to the database. Also notifies the appropriate model listeners.
	*
	* @param feeType the fee type
	* @return the fee type that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.FeeType addFeeType(
		info.diit.portal.accounts.model.FeeType feeType)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.addFeeType(feeType);
	}

	/**
	* Creates a new fee type with the primary key. Does not add the fee type to the database.
	*
	* @param feeTypeId the primary key for the new fee type
	* @return the new fee type
	*/
	public info.diit.portal.accounts.model.FeeType createFeeType(long feeTypeId) {
		return _feeTypeLocalService.createFeeType(feeTypeId);
	}

	/**
	* Deletes the fee type with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param feeTypeId the primary key of the fee type
	* @return the fee type that was removed
	* @throws PortalException if a fee type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.FeeType deleteFeeType(long feeTypeId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.deleteFeeType(feeTypeId);
	}

	/**
	* Deletes the fee type from the database. Also notifies the appropriate model listeners.
	*
	* @param feeType the fee type
	* @return the fee type that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.FeeType deleteFeeType(
		info.diit.portal.accounts.model.FeeType feeType)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.deleteFeeType(feeType);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _feeTypeLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.accounts.model.FeeType fetchFeeType(long feeTypeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.fetchFeeType(feeTypeId);
	}

	/**
	* Returns the fee type with the primary key.
	*
	* @param feeTypeId the primary key of the fee type
	* @return the fee type
	* @throws PortalException if a fee type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.FeeType getFeeType(long feeTypeId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.getFeeType(feeTypeId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the fee types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of fee types
	* @param end the upper bound of the range of fee types (not inclusive)
	* @return the range of fee types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.FeeType> getFeeTypes(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.getFeeTypes(start, end);
	}

	/**
	* Returns the number of fee types.
	*
	* @return the number of fee types
	* @throws SystemException if a system exception occurred
	*/
	public int getFeeTypesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.getFeeTypesCount();
	}

	/**
	* Updates the fee type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param feeType the fee type
	* @return the fee type that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.FeeType updateFeeType(
		info.diit.portal.accounts.model.FeeType feeType)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.updateFeeType(feeType);
	}

	/**
	* Updates the fee type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param feeType the fee type
	* @param merge whether to merge the fee type with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the fee type that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.FeeType updateFeeType(
		info.diit.portal.accounts.model.FeeType feeType, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.updateFeeType(feeType, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _feeTypeLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_feeTypeLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _feeTypeLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	public java.util.List<info.diit.portal.accounts.model.FeeType> findByFeeTypeName(
		java.lang.String feeTypeName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _feeTypeLocalService.findByFeeTypeName(feeTypeName);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public FeeTypeLocalService getWrappedFeeTypeLocalService() {
		return _feeTypeLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedFeeTypeLocalService(
		FeeTypeLocalService feeTypeLocalService) {
		_feeTypeLocalService = feeTypeLocalService;
	}

	public FeeTypeLocalService getWrappedService() {
		return _feeTypeLocalService;
	}

	public void setWrappedService(FeeTypeLocalService feeTypeLocalService) {
		_feeTypeLocalService = feeTypeLocalService;
	}

	private FeeTypeLocalService _feeTypeLocalService;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.accounts.model.StudentPaymentSchedule;

/**
 * The persistence interface for the student payment schedule service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shamsuddin
 * @see StudentPaymentSchedulePersistenceImpl
 * @see StudentPaymentScheduleUtil
 * @generated
 */
public interface StudentPaymentSchedulePersistence extends BasePersistence<StudentPaymentSchedule> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link StudentPaymentScheduleUtil} to access the student payment schedule persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the student payment schedule in the entity cache if it is enabled.
	*
	* @param studentPaymentSchedule the student payment schedule
	*/
	public void cacheResult(
		info.diit.portal.accounts.model.StudentPaymentSchedule studentPaymentSchedule);

	/**
	* Caches the student payment schedules in the entity cache if it is enabled.
	*
	* @param studentPaymentSchedules the student payment schedules
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.accounts.model.StudentPaymentSchedule> studentPaymentSchedules);

	/**
	* Creates a new student payment schedule with the primary key. Does not add the student payment schedule to the database.
	*
	* @param studentPaymentScheduleId the primary key for the new student payment schedule
	* @return the new student payment schedule
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule create(
		long studentPaymentScheduleId);

	/**
	* Removes the student payment schedule with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param studentPaymentScheduleId the primary key of the student payment schedule
	* @return the student payment schedule that was removed
	* @throws info.diit.portal.accounts.NoSuchStudentPaymentScheduleException if a student payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule remove(
		long studentPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentPaymentScheduleException;

	public info.diit.portal.accounts.model.StudentPaymentSchedule updateImpl(
		info.diit.portal.accounts.model.StudentPaymentSchedule studentPaymentSchedule,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student payment schedule with the primary key or throws a {@link info.diit.portal.accounts.NoSuchStudentPaymentScheduleException} if it could not be found.
	*
	* @param studentPaymentScheduleId the primary key of the student payment schedule
	* @return the student payment schedule
	* @throws info.diit.portal.accounts.NoSuchStudentPaymentScheduleException if a student payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule findByPrimaryKey(
		long studentPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentPaymentScheduleException;

	/**
	* Returns the student payment schedule with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param studentPaymentScheduleId the primary key of the student payment schedule
	* @return the student payment schedule, or <code>null</code> if a student payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule fetchByPrimaryKey(
		long studentPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the student payment schedules where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @return the matching student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentPaymentSchedule> findByStudentBatch(
		long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the student payment schedules where studentId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of student payment schedules
	* @param end the upper bound of the range of student payment schedules (not inclusive)
	* @return the range of matching student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentPaymentSchedule> findByStudentBatch(
		long studentId, long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the student payment schedules where studentId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of student payment schedules
	* @param end the upper bound of the range of student payment schedules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentPaymentSchedule> findByStudentBatch(
		long studentId, long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student payment schedule
	* @throws info.diit.portal.accounts.NoSuchStudentPaymentScheduleException if a matching student payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule findByStudentBatch_First(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentPaymentScheduleException;

	/**
	* Returns the first student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student payment schedule, or <code>null</code> if a matching student payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule fetchByStudentBatch_First(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student payment schedule
	* @throws info.diit.portal.accounts.NoSuchStudentPaymentScheduleException if a matching student payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule findByStudentBatch_Last(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentPaymentScheduleException;

	/**
	* Returns the last student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student payment schedule, or <code>null</code> if a matching student payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule fetchByStudentBatch_Last(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student payment schedules before and after the current student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentPaymentScheduleId the primary key of the current student payment schedule
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next student payment schedule
	* @throws info.diit.portal.accounts.NoSuchStudentPaymentScheduleException if a student payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentPaymentSchedule[] findByStudentBatch_PrevAndNext(
		long studentPaymentScheduleId, long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentPaymentScheduleException;

	/**
	* Returns all the student payment schedules.
	*
	* @return the student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentPaymentSchedule> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the student payment schedules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student payment schedules
	* @param end the upper bound of the range of student payment schedules (not inclusive)
	* @return the range of student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentPaymentSchedule> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the student payment schedules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student payment schedules
	* @param end the upper bound of the range of student payment schedules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentPaymentSchedule> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the student payment schedules where studentId = &#63; and batchId = &#63; from the database.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByStudentBatch(long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the student payment schedules from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of student payment schedules where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @return the number of matching student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public int countByStudentBatch(long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of student payment schedules.
	*
	* @return the number of student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
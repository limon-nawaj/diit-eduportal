/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.accounts.model.StudentDiscount;

import java.util.List;

/**
 * The persistence utility for the student discount service. This utility wraps {@link StudentDiscountPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shamsuddin
 * @see StudentDiscountPersistence
 * @see StudentDiscountPersistenceImpl
 * @generated
 */
public class StudentDiscountUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(StudentDiscount studentDiscount) {
		getPersistence().clearCache(studentDiscount);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<StudentDiscount> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<StudentDiscount> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<StudentDiscount> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static StudentDiscount update(StudentDiscount studentDiscount,
		boolean merge) throws SystemException {
		return getPersistence().update(studentDiscount, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static StudentDiscount update(StudentDiscount studentDiscount,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(studentDiscount, merge, serviceContext);
	}

	/**
	* Caches the student discount in the entity cache if it is enabled.
	*
	* @param studentDiscount the student discount
	*/
	public static void cacheResult(
		info.diit.portal.accounts.model.StudentDiscount studentDiscount) {
		getPersistence().cacheResult(studentDiscount);
	}

	/**
	* Caches the student discounts in the entity cache if it is enabled.
	*
	* @param studentDiscounts the student discounts
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.accounts.model.StudentDiscount> studentDiscounts) {
		getPersistence().cacheResult(studentDiscounts);
	}

	/**
	* Creates a new student discount with the primary key. Does not add the student discount to the database.
	*
	* @param studentDiscountId the primary key for the new student discount
	* @return the new student discount
	*/
	public static info.diit.portal.accounts.model.StudentDiscount create(
		long studentDiscountId) {
		return getPersistence().create(studentDiscountId);
	}

	/**
	* Removes the student discount with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param studentDiscountId the primary key of the student discount
	* @return the student discount that was removed
	* @throws info.diit.portal.accounts.NoSuchStudentDiscountException if a student discount with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentDiscount remove(
		long studentDiscountId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentDiscountException {
		return getPersistence().remove(studentDiscountId);
	}

	public static info.diit.portal.accounts.model.StudentDiscount updateImpl(
		info.diit.portal.accounts.model.StudentDiscount studentDiscount,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(studentDiscount, merge);
	}

	/**
	* Returns the student discount with the primary key or throws a {@link info.diit.portal.accounts.NoSuchStudentDiscountException} if it could not be found.
	*
	* @param studentDiscountId the primary key of the student discount
	* @return the student discount
	* @throws info.diit.portal.accounts.NoSuchStudentDiscountException if a student discount with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentDiscount findByPrimaryKey(
		long studentDiscountId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentDiscountException {
		return getPersistence().findByPrimaryKey(studentDiscountId);
	}

	/**
	* Returns the student discount with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param studentDiscountId the primary key of the student discount
	* @return the student discount, or <code>null</code> if a student discount with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentDiscount fetchByPrimaryKey(
		long studentDiscountId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(studentDiscountId);
	}

	/**
	* Returns all the student discounts where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @return the matching student discounts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.accounts.model.StudentDiscount> findByStudentBatch(
		long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStudentBatch(studentId, batchId);
	}

	/**
	* Returns a range of all the student discounts where studentId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of student discounts
	* @param end the upper bound of the range of student discounts (not inclusive)
	* @return the range of matching student discounts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.accounts.model.StudentDiscount> findByStudentBatch(
		long studentId, long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentBatch(studentId, batchId, start, end);
	}

	/**
	* Returns an ordered range of all the student discounts where studentId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of student discounts
	* @param end the upper bound of the range of student discounts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching student discounts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.accounts.model.StudentDiscount> findByStudentBatch(
		long studentId, long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentBatch(studentId, batchId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student discount
	* @throws info.diit.portal.accounts.NoSuchStudentDiscountException if a matching student discount could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentDiscount findByStudentBatch_First(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentDiscountException {
		return getPersistence()
				   .findByStudentBatch_First(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the first student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student discount, or <code>null</code> if a matching student discount could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentDiscount fetchByStudentBatch_First(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentBatch_First(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the last student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student discount
	* @throws info.diit.portal.accounts.NoSuchStudentDiscountException if a matching student discount could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentDiscount findByStudentBatch_Last(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentDiscountException {
		return getPersistence()
				   .findByStudentBatch_Last(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the last student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student discount, or <code>null</code> if a matching student discount could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentDiscount fetchByStudentBatch_Last(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentBatch_Last(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the student discounts before and after the current student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentDiscountId the primary key of the current student discount
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next student discount
	* @throws info.diit.portal.accounts.NoSuchStudentDiscountException if a student discount with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.accounts.model.StudentDiscount[] findByStudentBatch_PrevAndNext(
		long studentDiscountId, long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentDiscountException {
		return getPersistence()
				   .findByStudentBatch_PrevAndNext(studentDiscountId,
			studentId, batchId, orderByComparator);
	}

	/**
	* Returns all the student discounts.
	*
	* @return the student discounts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.accounts.model.StudentDiscount> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the student discounts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student discounts
	* @param end the upper bound of the range of student discounts (not inclusive)
	* @return the range of student discounts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.accounts.model.StudentDiscount> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the student discounts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student discounts
	* @param end the upper bound of the range of student discounts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of student discounts
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.accounts.model.StudentDiscount> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the student discounts where studentId = &#63; and batchId = &#63; from the database.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByStudentBatch(long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByStudentBatch(studentId, batchId);
	}

	/**
	* Removes all the student discounts from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of student discounts where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @return the number of matching student discounts
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStudentBatch(long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStudentBatch(studentId, batchId);
	}

	/**
	* Returns the number of student discounts.
	*
	* @return the number of student discounts
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static StudentDiscountPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (StudentDiscountPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.accounts.service.ClpSerializer.getServletContextName(),
					StudentDiscountPersistence.class.getName());

			ReferenceRegistry.registerReference(StudentDiscountUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(StudentDiscountPersistence persistence) {
	}

	private static StudentDiscountPersistence _persistence;
}
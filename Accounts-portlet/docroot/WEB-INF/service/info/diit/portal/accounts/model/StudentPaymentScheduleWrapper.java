/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link StudentPaymentSchedule}.
 * </p>
 *
 * @author    shamsuddin
 * @see       StudentPaymentSchedule
 * @generated
 */
public class StudentPaymentScheduleWrapper implements StudentPaymentSchedule,
	ModelWrapper<StudentPaymentSchedule> {
	public StudentPaymentScheduleWrapper(
		StudentPaymentSchedule studentPaymentSchedule) {
		_studentPaymentSchedule = studentPaymentSchedule;
	}

	public Class<?> getModelClass() {
		return StudentPaymentSchedule.class;
	}

	public String getModelClassName() {
		return StudentPaymentSchedule.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("studentPaymentScheduleId", getStudentPaymentScheduleId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("studentId", getStudentId());
		attributes.put("batchId", getBatchId());
		attributes.put("scheduleDate", getScheduleDate());
		attributes.put("feeTypeId", getFeeTypeId());
		attributes.put("amount", getAmount());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long studentPaymentScheduleId = (Long)attributes.get(
				"studentPaymentScheduleId");

		if (studentPaymentScheduleId != null) {
			setStudentPaymentScheduleId(studentPaymentScheduleId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Date scheduleDate = (Date)attributes.get("scheduleDate");

		if (scheduleDate != null) {
			setScheduleDate(scheduleDate);
		}

		Long feeTypeId = (Long)attributes.get("feeTypeId");

		if (feeTypeId != null) {
			setFeeTypeId(feeTypeId);
		}

		Double amount = (Double)attributes.get("amount");

		if (amount != null) {
			setAmount(amount);
		}
	}

	/**
	* Returns the primary key of this student payment schedule.
	*
	* @return the primary key of this student payment schedule
	*/
	public long getPrimaryKey() {
		return _studentPaymentSchedule.getPrimaryKey();
	}

	/**
	* Sets the primary key of this student payment schedule.
	*
	* @param primaryKey the primary key of this student payment schedule
	*/
	public void setPrimaryKey(long primaryKey) {
		_studentPaymentSchedule.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the student payment schedule ID of this student payment schedule.
	*
	* @return the student payment schedule ID of this student payment schedule
	*/
	public long getStudentPaymentScheduleId() {
		return _studentPaymentSchedule.getStudentPaymentScheduleId();
	}

	/**
	* Sets the student payment schedule ID of this student payment schedule.
	*
	* @param studentPaymentScheduleId the student payment schedule ID of this student payment schedule
	*/
	public void setStudentPaymentScheduleId(long studentPaymentScheduleId) {
		_studentPaymentSchedule.setStudentPaymentScheduleId(studentPaymentScheduleId);
	}

	/**
	* Returns the company ID of this student payment schedule.
	*
	* @return the company ID of this student payment schedule
	*/
	public long getCompanyId() {
		return _studentPaymentSchedule.getCompanyId();
	}

	/**
	* Sets the company ID of this student payment schedule.
	*
	* @param companyId the company ID of this student payment schedule
	*/
	public void setCompanyId(long companyId) {
		_studentPaymentSchedule.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this student payment schedule.
	*
	* @return the user ID of this student payment schedule
	*/
	public long getUserId() {
		return _studentPaymentSchedule.getUserId();
	}

	/**
	* Sets the user ID of this student payment schedule.
	*
	* @param userId the user ID of this student payment schedule
	*/
	public void setUserId(long userId) {
		_studentPaymentSchedule.setUserId(userId);
	}

	/**
	* Returns the user uuid of this student payment schedule.
	*
	* @return the user uuid of this student payment schedule
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentPaymentSchedule.getUserUuid();
	}

	/**
	* Sets the user uuid of this student payment schedule.
	*
	* @param userUuid the user uuid of this student payment schedule
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_studentPaymentSchedule.setUserUuid(userUuid);
	}

	/**
	* Returns the create date of this student payment schedule.
	*
	* @return the create date of this student payment schedule
	*/
	public java.util.Date getCreateDate() {
		return _studentPaymentSchedule.getCreateDate();
	}

	/**
	* Sets the create date of this student payment schedule.
	*
	* @param createDate the create date of this student payment schedule
	*/
	public void setCreateDate(java.util.Date createDate) {
		_studentPaymentSchedule.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this student payment schedule.
	*
	* @return the modified date of this student payment schedule
	*/
	public java.util.Date getModifiedDate() {
		return _studentPaymentSchedule.getModifiedDate();
	}

	/**
	* Sets the modified date of this student payment schedule.
	*
	* @param modifiedDate the modified date of this student payment schedule
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_studentPaymentSchedule.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the student ID of this student payment schedule.
	*
	* @return the student ID of this student payment schedule
	*/
	public long getStudentId() {
		return _studentPaymentSchedule.getStudentId();
	}

	/**
	* Sets the student ID of this student payment schedule.
	*
	* @param studentId the student ID of this student payment schedule
	*/
	public void setStudentId(long studentId) {
		_studentPaymentSchedule.setStudentId(studentId);
	}

	/**
	* Returns the batch ID of this student payment schedule.
	*
	* @return the batch ID of this student payment schedule
	*/
	public long getBatchId() {
		return _studentPaymentSchedule.getBatchId();
	}

	/**
	* Sets the batch ID of this student payment schedule.
	*
	* @param batchId the batch ID of this student payment schedule
	*/
	public void setBatchId(long batchId) {
		_studentPaymentSchedule.setBatchId(batchId);
	}

	/**
	* Returns the schedule date of this student payment schedule.
	*
	* @return the schedule date of this student payment schedule
	*/
	public java.util.Date getScheduleDate() {
		return _studentPaymentSchedule.getScheduleDate();
	}

	/**
	* Sets the schedule date of this student payment schedule.
	*
	* @param scheduleDate the schedule date of this student payment schedule
	*/
	public void setScheduleDate(java.util.Date scheduleDate) {
		_studentPaymentSchedule.setScheduleDate(scheduleDate);
	}

	/**
	* Returns the fee type ID of this student payment schedule.
	*
	* @return the fee type ID of this student payment schedule
	*/
	public long getFeeTypeId() {
		return _studentPaymentSchedule.getFeeTypeId();
	}

	/**
	* Sets the fee type ID of this student payment schedule.
	*
	* @param feeTypeId the fee type ID of this student payment schedule
	*/
	public void setFeeTypeId(long feeTypeId) {
		_studentPaymentSchedule.setFeeTypeId(feeTypeId);
	}

	/**
	* Returns the amount of this student payment schedule.
	*
	* @return the amount of this student payment schedule
	*/
	public double getAmount() {
		return _studentPaymentSchedule.getAmount();
	}

	/**
	* Sets the amount of this student payment schedule.
	*
	* @param amount the amount of this student payment schedule
	*/
	public void setAmount(double amount) {
		_studentPaymentSchedule.setAmount(amount);
	}

	public boolean isNew() {
		return _studentPaymentSchedule.isNew();
	}

	public void setNew(boolean n) {
		_studentPaymentSchedule.setNew(n);
	}

	public boolean isCachedModel() {
		return _studentPaymentSchedule.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_studentPaymentSchedule.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _studentPaymentSchedule.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _studentPaymentSchedule.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_studentPaymentSchedule.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _studentPaymentSchedule.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_studentPaymentSchedule.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new StudentPaymentScheduleWrapper((StudentPaymentSchedule)_studentPaymentSchedule.clone());
	}

	public int compareTo(
		info.diit.portal.accounts.model.StudentPaymentSchedule studentPaymentSchedule) {
		return _studentPaymentSchedule.compareTo(studentPaymentSchedule);
	}

	@Override
	public int hashCode() {
		return _studentPaymentSchedule.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.accounts.model.StudentPaymentSchedule> toCacheModel() {
		return _studentPaymentSchedule.toCacheModel();
	}

	public info.diit.portal.accounts.model.StudentPaymentSchedule toEscapedModel() {
		return new StudentPaymentScheduleWrapper(_studentPaymentSchedule.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _studentPaymentSchedule.toString();
	}

	public java.lang.String toXmlString() {
		return _studentPaymentSchedule.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_studentPaymentSchedule.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public StudentPaymentSchedule getWrappedStudentPaymentSchedule() {
		return _studentPaymentSchedule;
	}

	public StudentPaymentSchedule getWrappedModel() {
		return _studentPaymentSchedule;
	}

	public void resetOriginalValues() {
		_studentPaymentSchedule.resetOriginalValues();
	}

	private StudentPaymentSchedule _studentPaymentSchedule;
}
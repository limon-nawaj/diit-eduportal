/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.accounts.model.FeeType;

/**
 * The persistence interface for the fee type service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shamsuddin
 * @see FeeTypePersistenceImpl
 * @see FeeTypeUtil
 * @generated
 */
public interface FeeTypePersistence extends BasePersistence<FeeType> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link FeeTypeUtil} to access the fee type persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the fee type in the entity cache if it is enabled.
	*
	* @param feeType the fee type
	*/
	public void cacheResult(info.diit.portal.accounts.model.FeeType feeType);

	/**
	* Caches the fee types in the entity cache if it is enabled.
	*
	* @param feeTypes the fee types
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.accounts.model.FeeType> feeTypes);

	/**
	* Creates a new fee type with the primary key. Does not add the fee type to the database.
	*
	* @param feeTypeId the primary key for the new fee type
	* @return the new fee type
	*/
	public info.diit.portal.accounts.model.FeeType create(long feeTypeId);

	/**
	* Removes the fee type with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param feeTypeId the primary key of the fee type
	* @return the fee type that was removed
	* @throws info.diit.portal.accounts.NoSuchFeeTypeException if a fee type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.FeeType remove(long feeTypeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchFeeTypeException;

	public info.diit.portal.accounts.model.FeeType updateImpl(
		info.diit.portal.accounts.model.FeeType feeType, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the fee type with the primary key or throws a {@link info.diit.portal.accounts.NoSuchFeeTypeException} if it could not be found.
	*
	* @param feeTypeId the primary key of the fee type
	* @return the fee type
	* @throws info.diit.portal.accounts.NoSuchFeeTypeException if a fee type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.FeeType findByPrimaryKey(
		long feeTypeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchFeeTypeException;

	/**
	* Returns the fee type with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param feeTypeId the primary key of the fee type
	* @return the fee type, or <code>null</code> if a fee type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.FeeType fetchByPrimaryKey(
		long feeTypeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the fee types where typeName = &#63;.
	*
	* @param typeName the type name
	* @return the matching fee types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.FeeType> findByTypeName(
		java.lang.String typeName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the fee types where typeName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param typeName the type name
	* @param start the lower bound of the range of fee types
	* @param end the upper bound of the range of fee types (not inclusive)
	* @return the range of matching fee types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.FeeType> findByTypeName(
		java.lang.String typeName, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the fee types where typeName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param typeName the type name
	* @param start the lower bound of the range of fee types
	* @param end the upper bound of the range of fee types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching fee types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.FeeType> findByTypeName(
		java.lang.String typeName, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first fee type in the ordered set where typeName = &#63;.
	*
	* @param typeName the type name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching fee type
	* @throws info.diit.portal.accounts.NoSuchFeeTypeException if a matching fee type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.FeeType findByTypeName_First(
		java.lang.String typeName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchFeeTypeException;

	/**
	* Returns the first fee type in the ordered set where typeName = &#63;.
	*
	* @param typeName the type name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching fee type, or <code>null</code> if a matching fee type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.FeeType fetchByTypeName_First(
		java.lang.String typeName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last fee type in the ordered set where typeName = &#63;.
	*
	* @param typeName the type name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching fee type
	* @throws info.diit.portal.accounts.NoSuchFeeTypeException if a matching fee type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.FeeType findByTypeName_Last(
		java.lang.String typeName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchFeeTypeException;

	/**
	* Returns the last fee type in the ordered set where typeName = &#63;.
	*
	* @param typeName the type name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching fee type, or <code>null</code> if a matching fee type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.FeeType fetchByTypeName_Last(
		java.lang.String typeName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the fee types before and after the current fee type in the ordered set where typeName = &#63;.
	*
	* @param feeTypeId the primary key of the current fee type
	* @param typeName the type name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next fee type
	* @throws info.diit.portal.accounts.NoSuchFeeTypeException if a fee type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.FeeType[] findByTypeName_PrevAndNext(
		long feeTypeId, java.lang.String typeName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchFeeTypeException;

	/**
	* Returns all the fee types.
	*
	* @return the fee types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.FeeType> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the fee types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of fee types
	* @param end the upper bound of the range of fee types (not inclusive)
	* @return the range of fee types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.FeeType> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the fee types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of fee types
	* @param end the upper bound of the range of fee types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of fee types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.FeeType> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the fee types where typeName = &#63; from the database.
	*
	* @param typeName the type name
	* @throws SystemException if a system exception occurred
	*/
	public void removeByTypeName(java.lang.String typeName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the fee types from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of fee types where typeName = &#63;.
	*
	* @param typeName the type name
	* @return the number of matching fee types
	* @throws SystemException if a system exception occurred
	*/
	public int countByTypeName(java.lang.String typeName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of fee types.
	*
	* @return the number of fee types
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
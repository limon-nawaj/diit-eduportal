/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.accounts.model.StudentFee;

/**
 * The persistence interface for the student fee service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shamsuddin
 * @see StudentFeePersistenceImpl
 * @see StudentFeeUtil
 * @generated
 */
public interface StudentFeePersistence extends BasePersistence<StudentFee> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link StudentFeeUtil} to access the student fee persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the student fee in the entity cache if it is enabled.
	*
	* @param studentFee the student fee
	*/
	public void cacheResult(
		info.diit.portal.accounts.model.StudentFee studentFee);

	/**
	* Caches the student fees in the entity cache if it is enabled.
	*
	* @param studentFees the student fees
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.accounts.model.StudentFee> studentFees);

	/**
	* Creates a new student fee with the primary key. Does not add the student fee to the database.
	*
	* @param studentFeeId the primary key for the new student fee
	* @return the new student fee
	*/
	public info.diit.portal.accounts.model.StudentFee create(long studentFeeId);

	/**
	* Removes the student fee with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param studentFeeId the primary key of the student fee
	* @return the student fee that was removed
	* @throws info.diit.portal.accounts.NoSuchStudentFeeException if a student fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentFee remove(long studentFeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentFeeException;

	public info.diit.portal.accounts.model.StudentFee updateImpl(
		info.diit.portal.accounts.model.StudentFee studentFee, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student fee with the primary key or throws a {@link info.diit.portal.accounts.NoSuchStudentFeeException} if it could not be found.
	*
	* @param studentFeeId the primary key of the student fee
	* @return the student fee
	* @throws info.diit.portal.accounts.NoSuchStudentFeeException if a student fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentFee findByPrimaryKey(
		long studentFeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentFeeException;

	/**
	* Returns the student fee with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param studentFeeId the primary key of the student fee
	* @return the student fee, or <code>null</code> if a student fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentFee fetchByPrimaryKey(
		long studentFeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the student fees where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @return the matching student fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentFee> findByStudentBatch(
		long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the student fees where studentId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of student fees
	* @param end the upper bound of the range of student fees (not inclusive)
	* @return the range of matching student fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentFee> findByStudentBatch(
		long studentId, long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the student fees where studentId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of student fees
	* @param end the upper bound of the range of student fees (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching student fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentFee> findByStudentBatch(
		long studentId, long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student fee
	* @throws info.diit.portal.accounts.NoSuchStudentFeeException if a matching student fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentFee findByStudentBatch_First(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentFeeException;

	/**
	* Returns the first student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student fee, or <code>null</code> if a matching student fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentFee fetchByStudentBatch_First(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student fee
	* @throws info.diit.portal.accounts.NoSuchStudentFeeException if a matching student fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentFee findByStudentBatch_Last(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentFeeException;

	/**
	* Returns the last student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student fee, or <code>null</code> if a matching student fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentFee fetchByStudentBatch_Last(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student fees before and after the current student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentFeeId the primary key of the current student fee
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next student fee
	* @throws info.diit.portal.accounts.NoSuchStudentFeeException if a student fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.accounts.model.StudentFee[] findByStudentBatch_PrevAndNext(
		long studentFeeId, long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.accounts.NoSuchStudentFeeException;

	/**
	* Returns all the student fees.
	*
	* @return the student fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentFee> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the student fees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student fees
	* @param end the upper bound of the range of student fees (not inclusive)
	* @return the range of student fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentFee> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the student fees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student fees
	* @param end the upper bound of the range of student fees (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of student fees
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.accounts.model.StudentFee> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the student fees where studentId = &#63; and batchId = &#63; from the database.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByStudentBatch(long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the student fees from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of student fees where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @return the number of matching student fees
	* @throws SystemException if a system exception occurred
	*/
	public int countByStudentBatch(long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of student fees.
	*
	* @return the number of student fees
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
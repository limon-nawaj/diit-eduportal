/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.impl;

import info.diit.portal.accounts.model.StudentPaymentSchedule;
import info.diit.portal.accounts.service.StudentPaymentScheduleLocalServiceUtil;
import info.diit.portal.accounts.service.base.StudentPaymentScheduleLocalServiceBaseImpl;
import info.diit.portal.accounts.service.persistence.StudentPaymentScheduleUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the student payment schedule local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.accounts.service.StudentPaymentScheduleLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author shamsuddin
 * @see info.diit.portal.accounts.service.base.StudentPaymentScheduleLocalServiceBaseImpl
 * @see info.diit.portal.accounts.service.StudentPaymentScheduleLocalServiceUtil
 */
public class StudentPaymentScheduleLocalServiceImpl
	extends StudentPaymentScheduleLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.accounts.service.StudentPaymentScheduleLocalServiceUtil} to access the student payment schedule local service.
	 */
	
	public List<StudentPaymentSchedule> getStudentPaymentScheduleList(long studentId,long batchId) throws SystemException
	{
		return StudentPaymentScheduleUtil.findByStudentBatch(studentId, batchId);
	}
	
	public void updateStudentPaymentSchedules(long studentId,long batchId,List<StudentPaymentSchedule> studentPaymentSchedules) throws SystemException
	{
		StudentPaymentScheduleUtil.removeByStudentBatch(studentId, batchId);
		for(StudentPaymentSchedule studentPaymentSchedule:studentPaymentSchedules)
		{
			StudentPaymentScheduleLocalServiceUtil.addStudentPaymentSchedule(studentPaymentSchedule);
		}
		
	}
}
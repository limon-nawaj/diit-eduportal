package info.diit.portal.accounts.dto;

import java.io.Serializable;

public class CampusDto implements Serializable {

	private long campusId;
	private String campusName;
	
	public long getCampusId() {
		return campusId;
	}
	
	public void setCampusId(long campusId) {
		this.campusId = campusId;
	}
	
	public String getCampusName() {
		return campusName;
	}
	
	public void setCampusName(String campusName) {
		this.campusName = campusName;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getCampusName();
	}
}

package info.diit.portal.accounts.dto;

import java.io.Serializable;
import java.util.ArrayList;

public class StudentDto implements Serializable {

	private long studnetId;
	private String name;
	
	
	private ArrayList<BatchDto> batchList = new ArrayList<BatchDto>();
	
	public StudentDto() {
		batchList = new ArrayList<BatchDto>();
	}
	
	public long getStudnetId() {
		return studnetId;
	}
	
	public void setStudnetId(long studnetId) {
		this.studnetId = studnetId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	public ArrayList<BatchDto> getBatchList() {
		return batchList;
	}
	
	public void setBatchList(ArrayList<BatchDto> batchList) {
		this.batchList = batchList;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getName();
	}
	
}

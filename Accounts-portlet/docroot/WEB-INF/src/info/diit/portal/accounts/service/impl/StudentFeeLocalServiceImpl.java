/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.impl;

import info.diit.portal.accounts.model.StudentDiscount;
import info.diit.portal.accounts.model.StudentFee;
import info.diit.portal.accounts.model.StudentPaymentSchedule;
import info.diit.portal.accounts.service.StudentDiscountLocalServiceUtil;
import info.diit.portal.accounts.service.StudentFeeLocalServiceUtil;
import info.diit.portal.accounts.service.StudentPaymentScheduleLocalServiceUtil;
import info.diit.portal.accounts.service.base.StudentFeeLocalServiceBaseImpl;
import info.diit.portal.accounts.service.persistence.StudentFeeUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the student fee local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.accounts.service.StudentFeeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author shamsuddin
 * @see info.diit.portal.accounts.service.base.StudentFeeLocalServiceBaseImpl
 * @see info.diit.portal.accounts.service.StudentFeeLocalServiceUtil
 */
public class StudentFeeLocalServiceImpl extends StudentFeeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.accounts.service.StudentFeeLocalServiceUtil} to access the student fee local service.
	 */
	
	public List<StudentFee> getStudentFeeList(long studentId,long batchId) throws SystemException
	{
		return StudentFeeUtil.findByStudentBatch(studentId, batchId);
	}
	
	public void updateStudentFees(long studentId,long batchId,List<StudentFee> studentFees) throws SystemException
	{
		StudentFeeUtil.removeByStudentBatch(studentId, batchId);
		
		for(StudentFee studentFee:studentFees)
		{
			StudentFeeLocalServiceUtil.addStudentFee(studentFee);
		}
	}
	
	public void updateStudentFees(long studentId,long batchId,List<StudentFee> studentFees,StudentDiscount studentDiscount,List<StudentPaymentSchedule> studentPaymentSchedules) throws SystemException
	{
		StudentFeeLocalServiceUtil.updateStudentFees(studentId, batchId, studentFees);
		StudentDiscountLocalServiceUtil.updateStudentDiscount(studentDiscount);
		StudentPaymentScheduleLocalServiceUtil.updateStudentPaymentSchedules(studentId, batchId, studentPaymentSchedules);
	}
}
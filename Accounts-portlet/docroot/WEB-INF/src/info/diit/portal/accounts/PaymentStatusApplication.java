package info.diit.portal.accounts;

import java.util.ArrayList;
import java.util.List;

import info.diit.portal.accounts.dao.AccountsDao;
import info.diit.portal.accounts.dto.BatchDto;
import info.diit.portal.accounts.dto.CampusDto;
import info.diit.portal.accounts.dto.CourseDto;
import info.diit.portal.accounts.dto.StatusDto;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;

public class PaymentStatusApplication extends Application  implements PortletRequestListener{

	private ThemeDisplay themeDisplay;
	private Window window;
	
	private static final String COLUMN_STUDENT_ID = "studentId";
	private static final String COLUMN_NAME = "name";
	private static final String COLUMN_TOTAL_FEE = "totalFee";
	private static final String COLUMN_TOTAL_PAID = "totalPaid";
	private static final String COLUMN_TOTAL_DUE = "totalDue";
	private static final String COLUMN_REMAINING = "remaining";
	
	private BeanItemContainer<StatusDto> paymentStatusBeanItemContainer;
	
	private ComboBox campusComboBox;
	private ComboBox courseComboBox;
	private ComboBox batchComboBox;
	
	private Table paymentStatusTable;
	
	private List<CampusDto> campusList;
	private CampusDto selectedCampus;
	
    public void init() {
    	window = new Window();
		window.setSizeFull();
		setMainWindow(window);
		
		window.addComponent(initPaymentStatusLayout());
		
    }
    
    public GridLayout initPaymentStatusLayout()
    {
    	campusComboBox = new ComboBox("Campus");
		campusComboBox.setImmediate(true);
		campusComboBox.setWidth("100%");
		campusComboBox.setNewItemsAllowed(false);
		
		
		if(campusList!=null)
		{
			for(CampusDto campus:campusList)
			{
				campusComboBox.addItem(campus);
			}
		}
		
		if(campusList!=null && campusList.size()<=1)
		{
			campusComboBox.setVisible(false);
		}
    	
    	courseComboBox = new ComboBox("Course");
    	courseComboBox.setWidth("100%");
    	courseComboBox.setImmediate(true);
    	
    	if(selectedCampus!=null)
		{
			
			try {
				List<CourseDto> courseList = AccountsDao.getCourseListByOrganization(selectedCampus.getCampusId());
				for(CourseDto courseDto:courseList)
				{
					courseComboBox.addItem(courseDto);
				}
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

		}
    	
    	batchComboBox = new ComboBox("Batch");
    	batchComboBox.setWidth("100%");
    	batchComboBox.setImmediate(true);
    	
    	paymentStatusBeanItemContainer = new BeanItemContainer<StatusDto>(StatusDto.class);
    	paymentStatusTable = new Table("Total Fees",paymentStatusBeanItemContainer);
    	paymentStatusTable.setWidth("100%");
    	paymentStatusTable.setPageLength(20);
		
    	paymentStatusTable.setColumnHeader(COLUMN_STUDENT_ID, "Student ID");
    	paymentStatusTable.setColumnHeader(COLUMN_NAME, "Name");
    	paymentStatusTable.setColumnHeader(COLUMN_TOTAL_FEE, "Total Fee");
    	paymentStatusTable.setColumnHeader(COLUMN_TOTAL_PAID, "Total Paid");
    	paymentStatusTable.setColumnHeader(COLUMN_TOTAL_DUE, "Total Due");
    	paymentStatusTable.setColumnHeader(COLUMN_REMAINING, "Remaining");
    	
    	paymentStatusTable.setColumnAlignment(COLUMN_TOTAL_FEE, Table.ALIGN_RIGHT);
    	paymentStatusTable.setColumnAlignment(COLUMN_TOTAL_PAID, Table.ALIGN_RIGHT);
    	paymentStatusTable.setColumnAlignment(COLUMN_TOTAL_DUE, Table.ALIGN_RIGHT);
    	paymentStatusTable.setColumnAlignment(COLUMN_REMAINING, Table.ALIGN_RIGHT);
    	
    	paymentStatusTable.setColumnExpandRatio(COLUMN_STUDENT_ID, 1);
    	paymentStatusTable.setColumnExpandRatio(COLUMN_NAME, 3);
    	paymentStatusTable.setColumnExpandRatio(COLUMN_TOTAL_FEE, 1);
    	paymentStatusTable.setColumnExpandRatio(COLUMN_TOTAL_PAID, 1);
    	paymentStatusTable.setColumnExpandRatio(COLUMN_TOTAL_DUE, 1);
    	paymentStatusTable.setColumnExpandRatio(COLUMN_REMAINING, 1);

    	paymentStatusTable.setVisibleColumns(new String[]{COLUMN_STUDENT_ID,COLUMN_NAME,COLUMN_TOTAL_FEE,COLUMN_TOTAL_PAID,COLUMN_TOTAL_DUE,COLUMN_REMAINING});
    	
    	paymentStatusTable.setFooterVisible(true);
    	
    	//add dummy data
    	
    	StatusDto statusDto1 = new StatusDto();
    	statusDto1.setName("Kamrul Islam");
    	statusDto1.setRemaining(24000);
    	statusDto1.setStudentId(101);
    	statusDto1.setTotalDue(0);
    	statusDto1.setTotalFee(60000);
    	statusDto1.setTotalPaid(36000);
    	
    	StatusDto statusDto2 = new StatusDto();
    	statusDto2.setName("Habibur Rahman");
    	statusDto2.setRemaining(36000);
    	statusDto2.setStudentId(102);
    	statusDto2.setTotalDue(12000);
    	statusDto2.setTotalFee(60000);
    	statusDto2.setTotalPaid(24000);

    	
    	StatusDto statusDto3 = new StatusDto();
    	statusDto3.setName("Jashim Uddin");
    	statusDto3.setRemaining(24000);
    	statusDto3.setStudentId(103);
    	statusDto3.setTotalDue(0);
    	statusDto3.setTotalFee(60000);
    	statusDto3.setTotalPaid(36000);
    	
    	paymentStatusBeanItemContainer.addBean(statusDto1);
    	paymentStatusBeanItemContainer.addBean(statusDto2);
    	paymentStatusBeanItemContainer.addBean(statusDto3);
    	
    	paymentStatusTable.setColumnFooter(COLUMN_NAME, "Total");
    	paymentStatusTable.setColumnFooter(COLUMN_TOTAL_FEE, String.valueOf(getTotalFee()));
    	paymentStatusTable.setColumnFooter(COLUMN_TOTAL_PAID, String.valueOf(getTotalPaid()));
    	paymentStatusTable.setColumnFooter(COLUMN_TOTAL_DUE, String.valueOf(getTotalDue()));
    	paymentStatusTable.setColumnFooter(COLUMN_REMAINING, String.valueOf(getTotalRemaining()));

    	
    	paymentStatusTable.refreshRowCache();
    	
    	
    	GridLayout gridLayout = new GridLayout(5,2);
    	gridLayout.setWidth("100%");
    	gridLayout.setSpacing(true);
    	
    	gridLayout.addComponent(campusComboBox,0,0);
    	gridLayout.addComponent(courseComboBox,1,0,2,0);
    	gridLayout.addComponent(batchComboBox,3,0);
    	gridLayout.addComponent(paymentStatusTable,0,1,4,1);
    	
    	campusComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				
				courseComboBox.removeAllItems();
				
				selectedCampus = (CampusDto)campusComboBox.getValue();
				
				if(selectedCampus!=null)
				{
					
					try {
						ArrayList<CourseDto> courseList = AccountsDao.getCourseListByOrganization(selectedCampus.getCampusId());
						for(CourseDto course:courseList)
						{
							courseComboBox.addItem(course);
						}
					} catch (SystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
				}
				
			}
		});
		
		courseComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				batchComboBox.removeAllItems();
				
				CourseDto selectedCourse = (CourseDto)courseComboBox.getValue();
				
				if(selectedCourse!=null)
				{
					try {
						long organizationId = 0;
						if(selectedCampus!=null)
						{
							organizationId = selectedCampus.getCampusId();
						}
						
						ArrayList<BatchDto> batchList = AccountsDao.getBatchListByCourse(organizationId,selectedCourse.getCourseId());
						for(BatchDto batch:batchList)
						{
							batchComboBox.addItem(batch);
						}
						
					} catch (SystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
    	
    	return gridLayout;
    }
    
    @Override
    public void onRequestStart(PortletRequest p_request, PortletResponse p_response) {
themeDisplay = (ThemeDisplay) p_request.getAttribute(WebKeys.THEME_DISPLAY);
        
        try {
			User user = themeDisplay.getUser();
			List<Organization> organizationList = user.getOrganizations();
			campusList = new ArrayList<CampusDto>(organizationList.size());
			for(Organization org:organizationList)
			{
				CampusDto campus = new CampusDto();
				campus.setCampusId(org.getOrganizationId());
				campus.setCampusName(org.getName());
				
				campusList.add(campus);
			}
			
			if(campusList.size()==1)
			{
				selectedCampus = campusList.get(0);
				
			}
			
			
			
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }
	
	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}

	private long getTotalFee()
	{
		long total = 0;
		
		for(int i=0;i<paymentStatusBeanItemContainer.size();i++)
		{
			StatusDto statusDto = paymentStatusBeanItemContainer.getIdByIndex(i);
			total+=statusDto.getTotalFee();
		}
		
		return total;
	}
	
	private long getTotalPaid()
	{
		long total = 0;
		
		for(int i=0;i<paymentStatusBeanItemContainer.size();i++)
		{
			StatusDto statusDto = paymentStatusBeanItemContainer.getIdByIndex(i);
			total+=statusDto.getTotalPaid();
		}
		
		return total;
	}
	
	private long getTotalDue()
	{
		long total = 0;
		
		for(int i=0;i<paymentStatusBeanItemContainer.size();i++)
		{
			StatusDto statusDto = paymentStatusBeanItemContainer.getIdByIndex(i);
			total+=statusDto.getTotalDue();
		}
		
		return total;
	}
	
	private long getTotalRemaining()
	{
		long total = 0;
		
		for(int i=0;i<paymentStatusBeanItemContainer.size();i++)
		{
			StatusDto statusDto = paymentStatusBeanItemContainer.getIdByIndex(i);
			total+=statusDto.getRemaining();
		}
		
		return total;
	}



}

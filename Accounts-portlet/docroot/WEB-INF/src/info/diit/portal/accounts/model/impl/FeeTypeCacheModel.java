/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.accounts.model.FeeType;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing FeeType in entity cache.
 *
 * @author shamsuddin
 * @see FeeType
 * @generated
 */
public class FeeTypeCacheModel implements CacheModel<FeeType>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{feeTypeId=");
		sb.append(feeTypeId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", typeName=");
		sb.append(typeName);
		sb.append(", description=");
		sb.append(description);
		sb.append("}");

		return sb.toString();
	}

	public FeeType toEntityModel() {
		FeeTypeImpl feeTypeImpl = new FeeTypeImpl();

		feeTypeImpl.setFeeTypeId(feeTypeId);
		feeTypeImpl.setCompanyId(companyId);
		feeTypeImpl.setUserId(userId);

		if (userName == null) {
			feeTypeImpl.setUserName(StringPool.BLANK);
		}
		else {
			feeTypeImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			feeTypeImpl.setCreateDate(null);
		}
		else {
			feeTypeImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			feeTypeImpl.setModifiedDate(null);
		}
		else {
			feeTypeImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (typeName == null) {
			feeTypeImpl.setTypeName(StringPool.BLANK);
		}
		else {
			feeTypeImpl.setTypeName(typeName);
		}

		if (description == null) {
			feeTypeImpl.setDescription(StringPool.BLANK);
		}
		else {
			feeTypeImpl.setDescription(description);
		}

		feeTypeImpl.resetOriginalValues();

		return feeTypeImpl;
	}

	public long feeTypeId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String typeName;
	public String description;
}
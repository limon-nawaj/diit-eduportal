package info.diit.portal.accounts.dto;

import java.io.Serializable;

public class StatusDto implements Serializable {

	private Integer studentId;
	private String name;
	private Integer totalFee;
	private Integer totalPaid;
	private Integer totalDue;
	private Integer remaining;
	
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(Integer totalFee) {
		this.totalFee = totalFee;
	}
	public Integer getTotalPaid() {
		return totalPaid;
	}
	public void setTotalPaid(Integer totalPaid) {
		this.totalPaid = totalPaid;
	}
	public Integer getTotalDue() {
		return totalDue;
	}
	public void setTotalDue(Integer totalDue) {
		this.totalDue = totalDue;
	}
	public Integer getRemaining() {
		return remaining;
	}
	public void setRemaining(Integer remaining) {
		this.remaining = remaining;
	}
	
	
}

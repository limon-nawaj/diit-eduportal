/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.accounts.model.StudentDiscount;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing StudentDiscount in entity cache.
 *
 * @author shamsuddin
 * @see StudentDiscount
 * @generated
 */
public class StudentDiscountCacheModel implements CacheModel<StudentDiscount>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{studentDiscountId=");
		sb.append(studentDiscountId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", studentId=");
		sb.append(studentId);
		sb.append(", batchId=");
		sb.append(batchId);
		sb.append(", scholarships=");
		sb.append(scholarships);
		sb.append(", discount=");
		sb.append(discount);
		sb.append("}");

		return sb.toString();
	}

	public StudentDiscount toEntityModel() {
		StudentDiscountImpl studentDiscountImpl = new StudentDiscountImpl();

		studentDiscountImpl.setStudentDiscountId(studentDiscountId);
		studentDiscountImpl.setCompanyId(companyId);
		studentDiscountImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			studentDiscountImpl.setCreateDate(null);
		}
		else {
			studentDiscountImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			studentDiscountImpl.setModifiedDate(null);
		}
		else {
			studentDiscountImpl.setModifiedDate(new Date(modifiedDate));
		}

		studentDiscountImpl.setStudentId(studentId);
		studentDiscountImpl.setBatchId(batchId);
		studentDiscountImpl.setScholarships(scholarships);
		studentDiscountImpl.setDiscount(discount);

		studentDiscountImpl.resetOriginalValues();

		return studentDiscountImpl;
	}

	public long studentDiscountId;
	public long companyId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public long studentId;
	public long batchId;
	public double scholarships;
	public double discount;
}
package info.diit.portal.accounts;

import info.diit.portal.accounts.dao.AccountsDao;
import info.diit.portal.accounts.dto.BatchDto;
import info.diit.portal.accounts.dto.CampusDto;
import info.diit.portal.accounts.dto.CourseDto;
import info.diit.portal.accounts.dto.FeesDto;
import info.diit.portal.accounts.dto.ScheduleDto;
import info.diit.portal.accounts.dto.StudentDto;
import info.diit.portal.accounts.model.BatchFee;
import info.diit.portal.accounts.model.BatchPaymentSchedule;
import info.diit.portal.accounts.model.CourseFee;
import info.diit.portal.accounts.model.FeeType;
import info.diit.portal.accounts.model.StudentDiscount;
import info.diit.portal.accounts.model.StudentFee;
import info.diit.portal.accounts.model.StudentPaymentSchedule;
import info.diit.portal.accounts.model.impl.BatchFeeImpl;
import info.diit.portal.accounts.model.impl.BatchPaymentScheduleImpl;
import info.diit.portal.accounts.model.impl.CourseFeeImpl;
import info.diit.portal.accounts.model.impl.FeeTypeImpl;
import info.diit.portal.accounts.model.impl.StudentDiscountImpl;
import info.diit.portal.accounts.model.impl.StudentFeeImpl;
import info.diit.portal.accounts.model.impl.StudentPaymentScheduleImpl;
import info.diit.portal.accounts.service.BatchFeeLocalServiceUtil;
import info.diit.portal.accounts.service.CourseFeeLocalServiceUtil;
import info.diit.portal.accounts.service.FeeTypeLocalServiceUtil;
import info.diit.portal.accounts.service.StudentDiscountLocalServiceUtil;
import info.diit.portal.accounts.service.StudentFeeLocalServiceUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import net.atontech.vaadin.ui.numericfield.NumericField;
import net.atontech.vaadin.ui.numericfield.widgetset.shared.NumericFieldType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.Notification;


@SuppressWarnings("serial")
public class AccountsSetupApplication extends Application  implements PortletRequestListener{
	
	private ThemeDisplay themeDisplay;
	private Window window;
	
	private TabSheet accountsSetupTabSheet;
	
	//componets for fee types tab
	private TextField feeTypeNameField;
	private TextArea feeTypeDescriptionField;
	private Table feeTypesTable;
	
	private static final int TOTAL_SCHEDULE_ROWS_COUNT = 50;
	
	private static final String COLUMN_FEE_TYPE = "feeType";
	private static final String COLUMN_DESCRIPTION = "description";
	private static final String COLUMN_AMOUNT = "amount";
	private static final String COLUMN_SCHEDULE_DATE = "scheduleDate";
	
	private BeanItemContainer<FeesDto> feeTypesBeanItemContainer;
	
	//components for course fee setup
	private ListSelect courseListSelect;
	private Table courseFeeTable;
	private BeanItemContainer<FeesDto> courseFeeBeanItemContainer;
	
	
	//components for batch fee setup
	private ComboBox campusComboBox;
	private ComboBox courseComboBox;
	private ComboBox batchComboBox;
	private CheckBox editBatchFeesCheckBox;
	private Table batchFeeTable;
	private BeanItemContainer<FeesDto> batchFeeBeanItemContainer;
	private Table batchPaymentScheduleTable;
	private BeanItemContainer<ScheduleDto> batchPaymentScheduleBeanItemContainer;
	
	
	//components for Student setup
	private NumericField studentIdField;
	private TextField nameField;
	private ComboBox studentBatchComboBox;
	private NumericField scholarshipField;
	private NumericField discountField;
	private Table studentFeeTable;
	private BeanItemContainer<FeesDto> studentFeeBeanItemContainer;
	private Table studentPaymentScheduleTable;
	private BeanItemContainer<ScheduleDto> studentPaymentScheduleBeanItemContainer;
	
	
	
	//data
	//private List<CourseDto> courseList;
	private List<CampusDto> campusList;
	private CampusDto selectedCampus;
	
	public final static String DATE_FORMAT = "dd/MM/yyyy";

	public void init() {
		window = new Window();
		window.setSizeFull();
		setMainWindow(window);
		
		
		loadFeeTypesContainer();
		
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.setSpacing(true);

		verticalLayout.addComponent(initAccountsSetupTabSheet());
		
		window.addComponent(verticalLayout);
		
		
		
	}
	
	@Override
    public void onRequestStart(PortletRequest p_request, PortletResponse p_response) {
        themeDisplay = (ThemeDisplay) p_request.getAttribute(WebKeys.THEME_DISPLAY);
        
        try {
			User user = themeDisplay.getUser();
			List<Organization> organizationList = user.getOrganizations();
			campusList = new ArrayList<CampusDto>(organizationList.size());
			for(Organization org:organizationList)
			{
				CampusDto campus = new CampusDto();
				campus.setCampusId(org.getOrganizationId());
				campus.setCampusName(org.getName());
				
				campusList.add(campus);
			}
			
			if(campusList.size()==1)
			{
				selectedCampus = campusList.get(0);
				
			}
			
			
			
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
    }
	
	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}
	
	private TabSheet initAccountsSetupTabSheet()
	{
		accountsSetupTabSheet = new TabSheet();
		accountsSetupTabSheet.setWidth("100%");
		
		accountsSetupTabSheet.addTab(initCourseFeeSetupLayout(),"Course");
		accountsSetupTabSheet.addTab(initBatchFeeSetupLayout(),"Batch");
		accountsSetupTabSheet.addTab(initStudentFeeSetupLayout(),"Student");
		accountsSetupTabSheet.addTab(initFeeTypesLayout(),"Fee Types");
		
		
		return accountsSetupTabSheet;

	}
	
	private GridLayout initFeeTypesLayout()
	{
		feeTypeNameField = new TextField("Fee Type");
		feeTypeNameField.setWidth("100%");
		feeTypeNameField.setRequired(true);
		
		feeTypeDescriptionField = new TextArea("Description");
		feeTypeDescriptionField.setWidth("100%");
		
		feeTypesTable = new Table("Fee Types",feeTypesBeanItemContainer);
		feeTypesTable.setWidth("100%");
		feeTypesTable.setPageLength(10);
		feeTypesTable.setColumnHeader(COLUMN_FEE_TYPE, "Name");
		feeTypesTable.setSelectable(true);
		feeTypesTable.setImmediate(true);
		
		feeTypesTable.setVisibleColumns(new String[]{COLUMN_FEE_TYPE,COLUMN_DESCRIPTION});
		feeTypesTable.setColumnHeader(COLUMN_FEE_TYPE, "Name");
		feeTypesTable.setColumnHeader(COLUMN_DESCRIPTION, "Description");
		
		Button saveButton = new Button("Save");
		Button deleteButton = new Button("Delete");
		Button clearButton = new Button("Clear");
		Label spacer = new Label();
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		horizontalLayout.setWidth("100%");
		horizontalLayout.addComponent(spacer);
		horizontalLayout.addComponent(saveButton);
		horizontalLayout.addComponent(deleteButton);
		horizontalLayout.addComponent(clearButton);
		
		horizontalLayout.setExpandRatio(spacer, 1);

		GridLayout gridLayout = new GridLayout(3,5);
		gridLayout.setSpacing(true);
		gridLayout.setWidth("100%");
		
		gridLayout.addComponent(feeTypeNameField,0,0,0,0);
		gridLayout.addComponent(feeTypesTable,1,0,2,4);
		gridLayout.addComponent(feeTypeDescriptionField,0,1,0,2);
		gridLayout.addComponent(horizontalLayout,0,3,0,3);
		

		feeTypesTable.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				FeesDto accountsDto = (FeesDto) feeTypesTable.getValue();
				
				if(accountsDto!=null)
				{
					feeTypeNameField.setValue(accountsDto.getFeeType());
					feeTypeDescriptionField.setValue(accountsDto.getDescription());
				}
				else
				{
					feeTypeNameField.setValue("");
					feeTypeDescriptionField.setValue("");
				}
				
			}
		});
		
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				boolean isNew = true;
				FeesDto accountsDto = (FeesDto)feeTypesTable.getValue();
				
				FeeType feeType = null;
				
				if(accountsDto==null)
				{
					feeType = new FeeTypeImpl();
				}
				else
				{
					isNew = false;
					long feeTypeId = accountsDto.getId();
					try {
						feeType = FeeTypeLocalServiceUtil.fetchFeeType(feeTypeId);
					} catch (SystemException e) {
						
						e.printStackTrace();
					}
				}
					
				if(feeTypeNameField.isValid() || !feeTypeNameField.getValue().toString().trim().equals(""))
				{
					feeType.setTypeName(feeTypeNameField.getValue().toString());
					
				}
				else
				{
					window.showNotification("Name cannot be blank!");
					return;
				}
				
				Object description = feeTypeDescriptionField.getValue();
				
				if(description!=null)
				{
					feeType.setDescription(description.toString());
				}
					
					
				if(!isFeeTypeDuplicate(feeType))
				{
					try
					{
						feeType = FeeTypeLocalServiceUtil.updateFeeType(feeType);
						
						if(isNew)
						{
							feeTypesBeanItemContainer.addBean(convertFeeTypeModel(feeType));
							
						}
						
						feeTypesTable.refreshRowCache();
						
						window.showNotification("Fee Type Saved!");
						clearFeeTypesForm();
					}
					catch (SystemException e)
					{
						window.showNotification("Cannot save the Fee Type for a System Error!");
						e.printStackTrace();
					}
						
				}
				else
				{
					window.showNotification("'"+feeType.getTypeName()+"' already exists!");
				}
				
			}
		});
		
		clearButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				clearFeeTypesForm();
				
			}
		});
		
		
		//hide buttons and disable edit if not site administrator
		long siteId = themeDisplay.getScopeGroupId();
		PermissionChecker permissionChecker = themeDisplay.getPermissionChecker();
		
		if(!permissionChecker.isGroupAdmin(siteId))
		{
			saveButton.setVisible(false);
			deleteButton.setVisible(false);
			clearButton.setVisible(false);
		}

		
		return gridLayout;
	}
	
	private GridLayout initCourseFeeSetupLayout()
	{
		GridLayout gridLayout = new GridLayout(2,2);
		gridLayout.setSpacing(true);
		gridLayout.setSizeFull();
		
		
		courseListSelect = new ListSelect("Courses");
		courseListSelect.setWidth("100%");
		courseListSelect.setHeight("100%");
		courseListSelect.setNullSelectionAllowed(false);
		courseListSelect.setImmediate(true);
		
		try {
			ArrayList<CourseDto> courseList = AccountsDao.getCourseListByCompany(themeDisplay.getCompanyId());
			for(CourseDto courseDto:courseList)
			{
				courseListSelect.addItem(courseDto);
			}
		} catch (SystemException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		courseListSelect.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				CourseDto courseDto = (CourseDto) courseListSelect.getValue();
				
				if(courseDto!=null)
				{
					loadCourseFeeContainer();
				}
				
			}
		});
		
		courseFeeBeanItemContainer = new BeanItemContainer<FeesDto>(FeesDto.class);
		courseFeeTable = new Table("Fees",courseFeeBeanItemContainer);
		courseFeeTable.setWidth("100%");
		courseFeeTable.setPageLength(6);
		courseFeeTable.setImmediate(true);
		courseFeeTable.setFooterVisible(true);
		
		courseFeeTable.setColumnHeader(COLUMN_FEE_TYPE, "Fee Type");
		courseFeeTable.setColumnHeader(COLUMN_AMOUNT, "Amount");
		
		courseFeeTable.setColumnAlignment(COLUMN_AMOUNT, Table.ALIGN_RIGHT);

		courseFeeTable.setVisibleColumns(new String[]{COLUMN_FEE_TYPE,COLUMN_AMOUNT});
		
		courseFeeTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {

				
				if(propertyId.equals(COLUMN_FEE_TYPE))
				{
					TextField field = new TextField();
					field.setWidth("100%");
					field.addStyleName("align-right");
					field.setReadOnly(true);
					
					
					return field;
				}
				
				if(propertyId.equals(COLUMN_AMOUNT))
				{
					NumericField field = new NumericField();
					field.setWidth("100%");
					field.setAllowNegative(false);
					field.setNullRepresentation("");
					field.setNumberType(NumericFieldType.INTEGER);
					field.setImmediate(true);
					
					field.addListener(new ValueChangeListener() {
						
						@Override
						public void valueChange(ValueChangeEvent event) {
							
							courseFeeTable.setColumnFooter(COLUMN_AMOUNT, String.valueOf(getTotalCourseFee()));
							
						}
					});
					
					return field;
					
				}
				
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		
		courseFeeTable.setEditable(true);
		
		if(courseListSelect.getItemIds().iterator().hasNext())
		{
			courseListSelect.setValue(courseListSelect.getItemIds().iterator().next());
		}

		
		Button saveButton = new Button("Save");
		
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				CourseDto selectedCourse = (CourseDto)courseListSelect.getValue();
				
				if(selectedCourse!=null)
				{
					try {
						CourseFeeLocalServiceUtil.deleteAllCourseFees(selectedCourse.getCourseId());
						Collection<FeesDto> feesItems = courseFeeBeanItemContainer.getItemIds();

						Iterator<FeesDto> iterator = feesItems.iterator();						
						
						while(iterator.hasNext())
						{
							FeesDto accountsDto = iterator.next();
							
							if(accountsDto.getAmount()!=null)
							{
								CourseFee courseFee = new CourseFeeImpl();
								
								courseFee.setAmount(accountsDto.getAmount());
								courseFee.setCompanyId(themeDisplay.getCompanyId());
								courseFee.setCourseId(selectedCourse.getCourseId());
								courseFee.setCreateDate(new Date());
								courseFee.setFeeTypeId(accountsDto.getId());
								courseFee.setUserId(themeDisplay.getUserId());
								
								courseFee = CourseFeeLocalServiceUtil.addCourseFee(courseFee);

							}
							
							
						}
						
						window.showNotification("Course Fees Saved!");

					} catch (SystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				else
				{
					window.showNotification("No Course is selected !");
				}
				
			}
		});
		
		
		Label spacer = new Label();
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		horizontalLayout.setWidth("100%");
		
		horizontalLayout.addComponent(spacer);
		horizontalLayout.addComponent(saveButton);
		
		horizontalLayout.setExpandRatio(spacer, 1);
		
		gridLayout.addComponent(courseListSelect,0,0,0,0);
		gridLayout.addComponent(courseFeeTable,1,0,1,0);
		gridLayout.addComponent(horizontalLayout,1,1,1,1);
		
		loadCourseFeeContainer();
		
		//hide buttons and disable edit if not site administrator
		long siteId = themeDisplay.getScopeGroupId();
		PermissionChecker permissionChecker = themeDisplay.getPermissionChecker();
		
		if(!permissionChecker.isGroupAdmin(siteId))
		{
			saveButton.setVisible(false);
			
		}
		
		return gridLayout;
	}

	private GridLayout initBatchFeeSetupLayout()
	{
		campusComboBox = new ComboBox("Campus");
		campusComboBox.setImmediate(true);
		campusComboBox.setWidth("100%");
		campusComboBox.setNewItemsAllowed(false);
		
		
		if(campusList!=null)
		{
			for(CampusDto campus:campusList)
			{
				campusComboBox.addItem(campus);
			}
		}
		
		if(campusList!=null && campusList.size()<=1)
		{
			campusComboBox.setVisible(false);
		}

		courseComboBox = new ComboBox("Course");
		courseComboBox.setImmediate(true);
		courseComboBox.setWidth("100%");
		
		if(selectedCampus!=null)
		{
			
			try {
				List<CourseDto> courseList = AccountsDao.getCourseListByOrganization(selectedCampus.getCampusId());
				for(CourseDto courseDto:courseList)
				{
					courseComboBox.addItem(courseDto);
				}
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

		}
		

		
		batchComboBox = new ComboBox("Batch");
		batchComboBox.setImmediate(true);
		batchComboBox.setWidth("100%");
		
		
		editBatchFeesCheckBox = new CheckBox("Edit Batch Fees Information");
		editBatchFeesCheckBox.setWidth("100%");
		editBatchFeesCheckBox.setEnabled(false);
		editBatchFeesCheckBox.setImmediate(true);
		
		batchFeeBeanItemContainer = new BeanItemContainer<FeesDto>(FeesDto.class);
		batchFeeTable = new Table("Total Fees",batchFeeBeanItemContainer);
		batchFeeTable.setWidth("100%");
		batchFeeTable.setPageLength(8);
		
		batchFeeTable.setColumnHeader(COLUMN_FEE_TYPE, "Fee Type");
		batchFeeTable.setColumnHeader(COLUMN_AMOUNT, "Amount");
		
		batchFeeTable.setFooterVisible(true);

		batchFeeTable.setVisibleColumns(new String[]{COLUMN_FEE_TYPE,COLUMN_AMOUNT});
		
		batchFeeTable.setColumnAlignment(COLUMN_FEE_TYPE, Table.ALIGN_RIGHT);
		batchFeeTable.setColumnAlignment(COLUMN_AMOUNT, Table.ALIGN_RIGHT);
		
		batchFeeTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				
				if(propertyId.equals(COLUMN_FEE_TYPE))
				{
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					field.addStyleName("align-right");
					
					return field;
				}
				else if(propertyId.equals(COLUMN_AMOUNT))
				{
					NumericField field = new NumericField();
					field.setWidth("100%");
					field.setAllowNegative(false);
					field.setNullSettingAllowed(true);
					field.setNullRepresentation("");
					field.setNumberType(NumericFieldType.INTEGER);
					field.setImmediate(true);
					
					field.addListener(new ValueChangeListener() {
						
						@Override
						public void valueChange(ValueChangeEvent event) {
							
							batchFeeTable.setColumnFooter(COLUMN_AMOUNT, String.valueOf(getTotalBatchFee()));
							
						}
					});

					
					return field;
				}
				
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		batchPaymentScheduleBeanItemContainer = new BeanItemContainer<ScheduleDto>(ScheduleDto.class);
		batchPaymentScheduleTable = new Table("Payment Schedule",batchPaymentScheduleBeanItemContainer);
		batchPaymentScheduleTable.setEditable(true);
		batchPaymentScheduleTable.setWidth("100%");
		batchPaymentScheduleTable.setPageLength(10);
		
		batchPaymentScheduleTable.setColumnHeader(COLUMN_SCHEDULE_DATE, "Date");
		batchPaymentScheduleTable.setColumnHeader(COLUMN_FEE_TYPE, "Fee Type");
		batchPaymentScheduleTable.setColumnHeader(COLUMN_AMOUNT, "Amount");
		
		batchPaymentScheduleTable.setColumnExpandRatio(COLUMN_SCHEDULE_DATE, 1);
		batchPaymentScheduleTable.setColumnExpandRatio(COLUMN_FEE_TYPE, 1);
		batchPaymentScheduleTable.setColumnExpandRatio(COLUMN_AMOUNT, 1);
		
		batchPaymentScheduleTable.setVisibleColumns(new String[]{COLUMN_SCHEDULE_DATE,COLUMN_FEE_TYPE,COLUMN_AMOUNT});
		
		batchPaymentScheduleTable.setFooterVisible(true);
		batchPaymentScheduleTable.setColumnFooter(COLUMN_FEE_TYPE, "Total");
		batchPaymentScheduleTable.setColumnFooter(COLUMN_AMOUNT, "0");
		
		batchPaymentScheduleTable.setColumnAlignment(COLUMN_AMOUNT, Table.ALIGN_RIGHT);
		batchPaymentScheduleTable.setColumnAlignment(COLUMN_FEE_TYPE, Table.ALIGN_RIGHT);
		
		batchPaymentScheduleTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				
				if(propertyId.equals(COLUMN_SCHEDULE_DATE))
				{
					DateField field = new DateField();
					field.setWidth("100%");
					field.setResolution(DateField.RESOLUTION_DAY);
					field.setDateFormat(DATE_FORMAT);
					field.setImmediate(true);
					
					return field;

				}
				
				else if(propertyId.equals(COLUMN_FEE_TYPE))
				{
					ComboBox field = new ComboBox();
					field.setWidth("100%");
					
					Collection<FeesDto> feesDtoCollection = feeTypesBeanItemContainer.getItemIds();
					for(FeesDto feesDto:feesDtoCollection)
					{
						field.addItem(feesDto.getFeeType());
					}
					
					field.setImmediate(true);
					return field;
				}
				
				else if(propertyId.equals(COLUMN_AMOUNT))
				{
					NumericField field = new NumericField();
					field.setWidth("100%");
					field.setAllowNegative(false);
					field.setNullRepresentation("");
					field.setNumberType(NumericFieldType.INTEGER);
					field.setImmediate(true);
					
					field.addListener(new ValueChangeListener() {
						
						@Override
						public void valueChange(ValueChangeEvent event) {
							
							batchPaymentScheduleTable.setColumnFooter(COLUMN_AMOUNT, String.valueOf(getTotalBatchPaymentScheduleAmount()));
							
						}
					});
					
					return field;
					
				}
				
				else
				{
					return super.createField(container, itemId, propertyId, uiContext);
				}
			}
		});
		
		//create 50 rows for the batch schedule table
		for(int i=1;i<=TOTAL_SCHEDULE_ROWS_COUNT;i++)
		{
			ScheduleDto scheduleDto = new ScheduleDto();
			batchPaymentScheduleBeanItemContainer.addBean(scheduleDto);
		}
		
		batchPaymentScheduleTable.refreshRowCache();
		
		Button saveButton = new Button("Save");
		
		Label spacer = new Label();
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		horizontalLayout.setWidth("100%");
		
		horizontalLayout.addComponent(spacer);
		horizontalLayout.addComponent(saveButton);
		
		horizontalLayout.setExpandRatio(spacer, 1);

		GridLayout gridLayout = new GridLayout(5,6);
		gridLayout.setWidth("100%");
		gridLayout.setSpacing(true);
		
		gridLayout.addComponent(campusComboBox,0,0,1,0);
		gridLayout.addComponent(courseComboBox,0,1,1,1);
		gridLayout.addComponent(batchComboBox,0,2,1,2);
		gridLayout.addComponent(editBatchFeesCheckBox,0,3,1,3);
		gridLayout.addComponent(batchFeeTable,0,4,1,4);
		gridLayout.addComponent(batchPaymentScheduleTable,2,0,4,4);
		gridLayout.addComponent(horizontalLayout,1,5,4,5);
		
		campusComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				
				courseComboBox.removeAllItems();
				
				selectedCampus = (CampusDto)campusComboBox.getValue();
				
				if(selectedCampus!=null)
				{
					
					try {
						ArrayList<CourseDto> courseList = AccountsDao.getCourseListByOrganization(selectedCampus.getCampusId());
						for(CourseDto course:courseList)
						{
							courseComboBox.addItem(course);
						}
					} catch (SystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
				}
				
			}
		});
		
		courseComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				batchComboBox.removeAllItems();
				
				CourseDto selectedCourse = (CourseDto)courseComboBox.getValue();
				
				if(selectedCourse!=null)
				{
					try {
						long organizationId = 0;
						if(selectedCampus!=null)
						{
							organizationId = selectedCampus.getCampusId();
						}
						
						ArrayList<BatchDto> batchList = AccountsDao.getBatchListByCourse(organizationId,selectedCourse.getCourseId());
						for(BatchDto batch:batchList)
						{
							batchComboBox.addItem(batch);
						}
						
					} catch (SystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		
		editBatchFeesCheckBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				CourseDto selectedCourse = (CourseDto)courseComboBox.getValue();
				
				if(selectedCourse!=null)
				{
					boolean selected = (Boolean)editBatchFeesCheckBox.getValue();
					
					if(selected)
					{	
						batchFeeTable.setEditable(true);
					}
					else
					{
						batchFeeTable.setEditable(false);
					}
					
				}
				
			}
		});
		
		batchComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				BatchDto batch = (BatchDto)batchComboBox.getValue();
				
				if(batch!=null)
				{
					editBatchFeesCheckBox.setValue(false);
					editBatchFeesCheckBox.setEnabled(true);
					
					loadBatchFees(batch.getBatchId());
					
					if(batchFeeBeanItemContainer.size()==0)
					{
						CourseDto selectedCourse = (CourseDto)courseComboBox.getValue();
						if(selectedCourse!=null)
						{
							loadFeeFromCourse(selectedCourse.getCourseId());
						}
						
					}
					
					loadBatchPaymentSchedule(batch.getBatchId());
				}
				else
				{
					editBatchFeesCheckBox.setValue(false);
					editBatchFeesCheckBox.setEnabled(false);
					batchFeeBeanItemContainer.removeAllItems();
					batchFeeTable.setColumnFooter(COLUMN_AMOUNT, "0");
					batchFeeTable.refreshRowCache();
					
					batchPaymentScheduleBeanItemContainer.removeAllItems();
					for(int i=1;i<=TOTAL_SCHEDULE_ROWS_COUNT;i++)
					{
						ScheduleDto scheduleDto = new ScheduleDto();
						batchPaymentScheduleBeanItemContainer.addBean(scheduleDto);
					}
					batchPaymentScheduleTable.setColumnFooter(COLUMN_AMOUNT, "0");
					batchPaymentScheduleTable.refreshRowCache();
					
				}
				
			}
		});
		
		
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				BatchDto selectedBatch = (BatchDto)batchComboBox.getValue();
				if(selectedBatch!=null)
				{
					ArrayList<BatchFee> batchFeeList = new ArrayList<BatchFee>();
					
					for(int i=0;i<batchFeeBeanItemContainer.size();i++)
					{
						FeesDto feesDto = batchFeeBeanItemContainer.getIdByIndex(i);
						
						BatchFee batchFee = new BatchFeeImpl();
						batchFee.setAmount(feesDto.getAmount());
						batchFee.setBatchId(selectedBatch.getBatchId());
						
						batchFee.setCompanyId(themeDisplay.getCompanyId());
						batchFee.setCreateDate(new Date());
						batchFee.setFeeTypeId(feesDto.getId());
						batchFee.setModifiedDate(new Date());
						batchFee.setUserId(themeDisplay.getUserId());
						
						batchFeeList.add(batchFee);
						
					}
					
					//add the payment schedules
					
					ArrayList<BatchPaymentSchedule> batchPaymentSchedules = new ArrayList<BatchPaymentSchedule>();
					
					for(int i=0;i<batchPaymentScheduleBeanItemContainer.size();i++)
					{
						ScheduleDto schedule = batchPaymentScheduleBeanItemContainer.getIdByIndex(i);
						
						if(schedule.getAmount()!= null && schedule.getFeeType() != null && schedule.getScheduleDate()!=null)
						{
							try {
								List<FeeType> feeTypes = FeeTypeLocalServiceUtil.findByFeeTypeName(schedule.getFeeType());
								if(feeTypes!=null && feeTypes.size()>0)
								{
									schedule.setId(feeTypes.get(0).getFeeTypeId());
								}
							} catch (SystemException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							BatchPaymentSchedule batchPaymentSchedule = new BatchPaymentScheduleImpl();
							batchPaymentSchedule.setAmount(schedule.getAmount());
							batchPaymentSchedule.setBatchId(selectedBatch.getBatchId());
							batchPaymentSchedule.setCompanyId(themeDisplay.getCompanyId());
							batchPaymentSchedule.setCreateDate(new Date());
							batchPaymentSchedule.setFeeTypeId(schedule.getId());
							batchPaymentSchedule.setModifiedDate(new Date());
							batchPaymentSchedule.setScheduleDate(schedule.getScheduleDate());
							batchPaymentSchedule.setUserId(themeDisplay.getUserId());
							
							batchPaymentSchedules.add(batchPaymentSchedule);
						}
					}
					
					try {
						BatchFeeLocalServiceUtil.updateBatchFees(selectedBatch.getBatchId(),batchFeeList,batchPaymentSchedules);
						window.showNotification("Batch Fees information saved.");
					} catch (SystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						window.showNotification("Internal Error! Cannot save Batch Fees information",Notification.TYPE_ERROR_MESSAGE);
					}
				}
				else
				{
					window.showNotification("Please select a batch", Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
			}
		});

		
		//hide buttons and disable edit if not site administrator
		long siteId = themeDisplay.getScopeGroupId();
		PermissionChecker permissionChecker = themeDisplay.getPermissionChecker();
		
		if(!permissionChecker.isGroupAdmin(siteId))
		{
			editBatchFeesCheckBox.setVisible(false);
			saveButton.setVisible(false);
		}
		
		return gridLayout;
	}
	
	private GridLayout initStudentFeeSetupLayout()
	{
		studentIdField = new NumericField("Student ID");
		studentIdField.setAllowNegative(false);
		studentIdField.setImmediate(true);
		studentIdField.setNullRepresentation("");
		studentIdField.setNumberType(NumericFieldType.INTEGER);
		studentIdField.setWidth("100%");
		
		nameField = new TextField("Student Name");
		nameField.setReadOnly(true);
		nameField.setWidth("100%");
		nameField.setNullSettingAllowed(true);
		nameField.setNullRepresentation("");
		
		
		studentBatchComboBox = new ComboBox("Batch");
		studentBatchComboBox.setImmediate(true);
		studentBatchComboBox.setWidth("100%");
		
		scholarshipField = new NumericField("Scholarships");
		scholarshipField.setWidth("100%");
		scholarshipField.setNullRepresentation("");
		scholarshipField.setNumberType(NumericFieldType.INTEGER);

		discountField = new NumericField("Discount");
		discountField.setWidth("100%");
		discountField.setNullRepresentation("");
		discountField.setNumberType(NumericFieldType.INTEGER);
		
		
		studentFeeBeanItemContainer = new BeanItemContainer<FeesDto>(FeesDto.class);
		studentFeeTable = new Table("Total Fees",studentFeeBeanItemContainer);
		studentFeeTable.setWidth("100%");
		studentFeeTable.setPageLength(8);
		studentFeeTable.setEditable(true);
		
		studentFeeTable.setColumnHeader(COLUMN_FEE_TYPE, "Fee Type");
		studentFeeTable.setColumnHeader(COLUMN_AMOUNT, "Amount");
		
		studentFeeTable.setColumnAlignment(COLUMN_FEE_TYPE, Table.ALIGN_RIGHT);
		studentFeeTable.setColumnAlignment(COLUMN_AMOUNT, Table.ALIGN_RIGHT);
		
		studentFeeTable.setFooterVisible(true);
		studentFeeTable.setColumnFooter(COLUMN_FEE_TYPE, "Total");

		studentFeeTable.setVisibleColumns(new String[]{COLUMN_FEE_TYPE,COLUMN_AMOUNT});
		
		studentFeeTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				
				if(propertyId.equals(COLUMN_FEE_TYPE))
				{
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					field.addStyleName("align-right");
					
					return field;
				}
				else if(propertyId.equals(COLUMN_AMOUNT))
				{
					NumericField field = new NumericField();
					field.setWidth("100%");
					field.setAllowNegative(false);
					field.setNullSettingAllowed(true);
					field.setNullRepresentation("");
					field.setNumberType(NumericFieldType.INTEGER);
					field.setImmediate(true);
					
					field.addListener(new ValueChangeListener() {
						
						@Override
						public void valueChange(ValueChangeEvent event) {
							
							studentFeeTable.setColumnFooter(COLUMN_AMOUNT, String.valueOf(getTotalStudentFee()));
							
						}
					});

					
					return field;
				}
				
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});

		
		studentPaymentScheduleBeanItemContainer = new BeanItemContainer<ScheduleDto>(ScheduleDto.class);
		studentPaymentScheduleTable = new Table("Payment Schedule",studentPaymentScheduleBeanItemContainer);
		studentPaymentScheduleTable.setEditable(true);
		studentPaymentScheduleTable.setWidth("100%");
		studentPaymentScheduleTable.setPageLength(13);
		studentPaymentScheduleTable.setFooterVisible(true);
		studentPaymentScheduleTable.setColumnFooter(COLUMN_FEE_TYPE, "Total");
		studentPaymentScheduleTable.setColumnFooter(COLUMN_AMOUNT, "0");
		
		studentPaymentScheduleTable.setColumnHeader(COLUMN_SCHEDULE_DATE, "Date");
		studentPaymentScheduleTable.setColumnHeader(COLUMN_FEE_TYPE, "Fee Type");
		studentPaymentScheduleTable.setColumnHeader(COLUMN_AMOUNT, "Amount");
		
		studentPaymentScheduleTable.setColumnExpandRatio(COLUMN_SCHEDULE_DATE, 1);
		studentPaymentScheduleTable.setColumnExpandRatio(COLUMN_FEE_TYPE, 1);
		studentPaymentScheduleTable.setColumnExpandRatio(COLUMN_AMOUNT, 1);
		
		studentPaymentScheduleTable.setColumnAlignment(COLUMN_AMOUNT, Table.ALIGN_RIGHT);
		studentPaymentScheduleTable.setColumnAlignment(COLUMN_FEE_TYPE, Table.ALIGN_RIGHT);
		
		studentPaymentScheduleTable.setVisibleColumns(new String[]{COLUMN_SCHEDULE_DATE,COLUMN_FEE_TYPE,COLUMN_AMOUNT});
		
		studentPaymentScheduleTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				
				if(propertyId.equals(COLUMN_SCHEDULE_DATE))
				{
					DateField field = new DateField();
					field.setWidth("100%");
					field.setResolution(DateField.RESOLUTION_DAY);
					field.setDateFormat(DATE_FORMAT);
					
					return field;

				}
				
				if(propertyId.equals(COLUMN_FEE_TYPE))
				{
					ComboBox field = new ComboBox();
					field.setWidth("100%");
					
					Collection<FeesDto> feesDtoCollection = feeTypesBeanItemContainer.getItemIds();
					for(FeesDto feesDto:feesDtoCollection)
					{
						field.addItem(feesDto.getFeeType());
					}
					
					return field;
				}
				
				if(propertyId.equals(COLUMN_AMOUNT))
				{
					NumericField field = new NumericField();
					field.setImmediate(true);
					field.setWidth("100%");
					field.setAllowNegative(false);
					field.setNullRepresentation("");
					field.setNumberType(NumericFieldType.INTEGER);
					
					field.addListener(new ValueChangeListener() {
						
						@Override
						public void valueChange(ValueChangeEvent event) {
							
							studentPaymentScheduleTable.setColumnFooter(COLUMN_AMOUNT, String.valueOf(getTotalStudentPaymentScheduleAmount()));
							
						}
					});
					
					return field;
					
				}
				
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		//create 50 rows for the batch schedule table
		for(int i=1;i<=50;i++)
		{
			ScheduleDto scheduleDto = new ScheduleDto();
			studentPaymentScheduleBeanItemContainer.addBean(scheduleDto);
		}
		
		studentPaymentScheduleTable.refreshRowCache();
		
		Button saveButton = new Button("Save");
		
		
		Label spacer = new Label();
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		horizontalLayout.setWidth("100%");
		
		horizontalLayout.addComponent(spacer);
		horizontalLayout.addComponent(saveButton);
		
		
		horizontalLayout.setExpandRatio(spacer, 1);

		GridLayout gridLayout = new GridLayout(5,5);
		gridLayout.setWidth("100%");
		gridLayout.setSpacing(true);
		
		gridLayout.addComponent(studentIdField,0,0);
		gridLayout.addComponent(studentBatchComboBox,1,0);
		gridLayout.addComponent(nameField,0,1,1,1);
		gridLayout.addComponent(scholarshipField,0,2);
		gridLayout.addComponent(discountField,1,2);
		gridLayout.addComponent(studentFeeTable,0,3,1,3);
		gridLayout.addComponent(studentPaymentScheduleTable,2,0,4,3);
		gridLayout.addComponent(horizontalLayout,1,4,4,4);
		
		studentIdField.addListener(new BlurListener() {
			
			@Override
			public void blur(BlurEvent event) {
				
				Object studentIdObject = studentIdField.getValue();
				if(studentIdObject!=null)
				{
					long studentId = Long.parseLong(studentIdObject.toString());
					
					try {
						
						StudentDto studentDto = AccountsDao.getStudentDto(studentId);
						
						if(studentDto!=null)
						{
							
							ArrayList<BatchDto> batchList = studentDto.getBatchList();
							
							studentBatchComboBox.removeAllItems();
							
							for(BatchDto batchDto:batchList)
							{
								studentBatchComboBox.addItem(batchDto);
							}
							nameField.setReadOnly(false);
							nameField.setValue(studentDto.getName());
							nameField.setReadOnly(true);
						}
						else
						{
							studentBatchComboBox.removeAllItems();
							nameField.setReadOnly(false);
							nameField.setValue("");
							nameField.setReadOnly(true);
							clearStudentPaymentForm();
							window.showNotification("No student found with ID "+studentId,Notification.TYPE_ERROR_MESSAGE);
						}
					} catch (SystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}
		});
		
		studentBatchComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				Object studentIdObject = studentIdField.getValue();
				if(studentIdObject!=null)
				{
					long studentId = Long.parseLong(studentIdObject.toString());
					BatchDto selectedBatch = (BatchDto)studentBatchComboBox.getValue();
					
					if(selectedBatch!=null)
					{
						try {
							StudentDiscount studentDiscount = StudentDiscountLocalServiceUtil.getStudentDiscount(studentId, selectedBatch.getBatchId());
							if(studentDiscount!=null)
							{
								scholarshipField.setValue(Math.round(studentDiscount.getScholarships()));
								discountField.setValue(Math.round(studentDiscount.getDiscount()));
							}
							else
							{
								scholarshipField.setValue("");
								discountField.setValue("");
								
							}
						} catch (SystemException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						try {
							ArrayList<FeesDto> studentFeesList = AccountsDao.getStudentFeesList(studentId, selectedBatch.getBatchId());
							
							if(studentFeesList.size()==0)
							{
								studentFeesList = AccountsDao.getBatchFeesList(selectedBatch.getBatchId());
							}
							
							for(FeesDto feesDto:studentFeesList)
							{
								studentFeeBeanItemContainer.addBean(feesDto);
							}
							studentFeeTable.setColumnFooter(COLUMN_AMOUNT, String.valueOf(getTotalStudentFee()));
							studentFeeTable.refreshRowCache();
							
						} catch (SystemException | PortalException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						// student payment schedule
						
						loadStudentPaymentSchedule(studentId,selectedBatch.getBatchId());
					}
					else
					{
						clearStudentPaymentForm();
					}
				}
				else
				{
					clearStudentPaymentForm();
				}
				
			}
		});
		
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				Object studentIdObject = studentIdField.getValue();
				long studentId = 0;
				
				if(studentIdObject!=null)
				{
					try
					{
						studentId = Long.parseLong(studentIdObject.toString());
					}
					catch(NumberFormatException nfe)
					{
						window.showNotification("Student ID is not valid!",Notification.TYPE_ERROR_MESSAGE);
					}
				}
				else
				{
					window.showNotification("No Student is selected!",Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				BatchDto selectedBatch = (BatchDto)studentBatchComboBox.getValue();
				if(selectedBatch!=null)
				{
					ArrayList<StudentFee> studentFeeList = new ArrayList<StudentFee>();
					
					for(int i=0;i<studentFeeBeanItemContainer.size();i++)
					{
						FeesDto feesDto = studentFeeBeanItemContainer.getIdByIndex(i);
						
						StudentFee studentFee = new StudentFeeImpl();
						studentFee.setStudentId(studentId);
						studentFee.setAmount(feesDto.getAmount());
						studentFee.setBatchId(selectedBatch.getBatchId());
						
						studentFee.setCompanyId(themeDisplay.getCompanyId());
						studentFee.setCreateDate(new Date());
						studentFee.setFeeTypeId(feesDto.getId());
						studentFee.setModifiedDate(new Date());
						studentFee.setUserId(themeDisplay.getUserId());
						
						studentFeeList.add(studentFee);
						
					}
					
					//add the payment schedules
					
					ArrayList<StudentPaymentSchedule> studentPaymentSchedules = new ArrayList<StudentPaymentSchedule>();
					
					for(int i=0;i<studentPaymentScheduleBeanItemContainer.size();i++)
					{
						ScheduleDto schedule = studentPaymentScheduleBeanItemContainer.getIdByIndex(i);
						
						if(schedule.getAmount()!= null && schedule.getFeeType() != null && schedule.getScheduleDate()!=null)
						{
							try {
								List<FeeType> feeTypes = FeeTypeLocalServiceUtil.findByFeeTypeName(schedule.getFeeType());
								if(feeTypes!=null && feeTypes.size()>0)
								{
									schedule.setId(feeTypes.get(0).getFeeTypeId());
								}
							} catch (SystemException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							StudentPaymentSchedule studentPaymentSchedule = new StudentPaymentScheduleImpl();
							studentPaymentSchedule.setStudentId(studentId);
							studentPaymentSchedule.setAmount(schedule.getAmount());
							studentPaymentSchedule.setBatchId(selectedBatch.getBatchId());
							studentPaymentSchedule.setCompanyId(themeDisplay.getCompanyId());
							studentPaymentSchedule.setCreateDate(new Date());
							studentPaymentSchedule.setFeeTypeId(schedule.getId());
							studentPaymentSchedule.setModifiedDate(new Date());
							studentPaymentSchedule.setScheduleDate(schedule.getScheduleDate());
							studentPaymentSchedule.setUserId(themeDisplay.getUserId());
							
							studentPaymentSchedules.add(studentPaymentSchedule);
						}
					}
					
					// student discount;
					
					StudentDiscount studentDiscount = new StudentDiscountImpl();
					Object discountObject = discountField.getValue();
					
					if(discountObject!=null)
					{
						try
						{
							studentDiscount.setDiscount(Double.parseDouble(discountObject.toString()));
						}
						catch(NumberFormatException nfe)
						{
							//nothing to do; zero will be set by default
						}
					}
					Object scholarshipObject = scholarshipField.getValue();
					
					if(scholarshipObject!=null)
					{
						try
						{
							studentDiscount.setScholarships(Double.parseDouble(scholarshipObject.toString()));
						}
						catch(NumberFormatException nfe)
						{
							//nothing to do; zero will be set by default
						}
					}
					
					studentDiscount.setBatchId(selectedBatch.getBatchId());
					studentDiscount.setCompanyId(themeDisplay.getCompanyId());
					studentDiscount.setCreateDate(new Date());
					studentDiscount.setModifiedDate(new Date());
					studentDiscount.setStudentId(studentId);
					studentDiscount.setUserId(themeDisplay.getUserId());
					
					
					try {
						StudentFeeLocalServiceUtil.updateStudentFees(studentId,selectedBatch.getBatchId(),studentFeeList,studentDiscount,studentPaymentSchedules);
						window.showNotification("Student Fees information saved.");
					} catch (SystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						window.showNotification("Internal Error! Cannot save Batch Fees information",Notification.TYPE_ERROR_MESSAGE);
					}
				}
				else
				{
					window.showNotification("Please select a batch", Notification.TYPE_ERROR_MESSAGE);
					return;
				}

				
			}
		});
		
		//hide buttons and disable edit if not site administrator
		long siteId = themeDisplay.getScopeGroupId();
		PermissionChecker permissionChecker = themeDisplay.getPermissionChecker();
		
		if(!permissionChecker.isGroupAdmin(siteId))
		{
			saveButton.setVisible(false);
		}

		
		return gridLayout;
	}

	public void clearStudentPaymentForm()
	{
		
		scholarshipField.setValue("");
		discountField.setValue("");
		
		studentFeeBeanItemContainer.removeAllItems();
		studentFeeTable.setColumnFooter(COLUMN_AMOUNT, "0");
		studentFeeTable.refreshRowCache();
		
		studentPaymentScheduleBeanItemContainer.removeAllItems();
		//create 50 blank rows for the batch schedule table
		for(int i=1;i<=50;i++)
		{
			ScheduleDto scheduleDto = new ScheduleDto();
			studentPaymentScheduleBeanItemContainer.addBean(scheduleDto);
		}
		studentPaymentScheduleTable.setColumnFooter(COLUMN_AMOUNT, "0");
		studentPaymentScheduleTable.refreshRowCache();
		
	}
	
	private void loadFeeTypesContainer() 
	{
		try
		{
			feeTypesBeanItemContainer = new BeanItemContainer<FeesDto>(FeesDto.class);
			List<FeeType> feeTypesList = FeeTypeLocalServiceUtil.getFeeTypes(0, FeeTypeLocalServiceUtil.getFeeTypesCount());
			
			for(FeeType feeType:feeTypesList)
			{
				FeesDto accountsDto = new FeesDto();
				accountsDto.setId(feeType.getFeeTypeId());
				accountsDto.setFeeType(feeType.getTypeName());
				accountsDto.setDescription(feeType.getDescription());
				
				feeTypesBeanItemContainer.addBean(accountsDto);
				
			}
			
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void loadCourseFeeContainer()
	{
		courseFeeBeanItemContainer.removeAllItems();
		CourseDto selectedCourse = (CourseDto)courseListSelect.getValue();
		long courseId = 0;
		
		if(selectedCourse!=null)
		{
			courseId = selectedCourse.getCourseId();
		}
		
		try {
			List<CourseFee> courseFeeList = CourseFeeLocalServiceUtil.getCourseFeeList(courseId);
			
			long totalFee = 0;
			
			for(CourseFee courseFee:courseFeeList)
			{
				FeesDto accountsDto = new FeesDto();
				accountsDto.setId(courseFee.getFeeTypeId());
				accountsDto.setFeeType(FeeTypeLocalServiceUtil.fetchFeeType(courseFee.getFeeTypeId()).getTypeName());
				accountsDto.setAmount(Math.round(courseFee.getAmount()));
				totalFee += accountsDto.getAmount();
				courseFeeBeanItemContainer.addBean(accountsDto);
			}
			
			//other fee types not setup
			List<FeeType> feeTypesList = FeeTypeLocalServiceUtil.getFeeTypes(0, FeeTypeLocalServiceUtil.getFeeTypesCount());
			
			for(FeeType feeType:feeTypesList)
			{
				Collection<FeesDto> existingAccounts = courseFeeBeanItemContainer.getItemIds();
				Iterator<FeesDto> iterator = existingAccounts.iterator();
				
				boolean exist = false;
				while(iterator.hasNext())
				{
					if(feeType.getFeeTypeId()==iterator.next().getId())
					{
						exist = true;
						break;
					}
				}
				
				if(!exist)
				{
					FeesDto accountsDto = new FeesDto();
					accountsDto.setId(feeType.getFeeTypeId());
					accountsDto.setFeeType(feeType.getTypeName());
				
					courseFeeBeanItemContainer.addBean(accountsDto);
				}
			}
			
			courseFeeTable.refreshRowCache();
			courseFeeTable.setColumnFooter(COLUMN_FEE_TYPE,"Total");
			courseFeeTable.setColumnFooter(COLUMN_AMOUNT,String.valueOf(totalFee));
			
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void clearFeeTypesForm()
	{
		feeTypeNameField.setValue("");
		feeTypeDescriptionField.setValue("");
		feeTypesTable.setValue(null);
	}
	
	private FeesDto convertFeeTypeModel(FeeType feeType)
	{
		FeesDto accountsDto = new FeesDto();
		accountsDto.setId(feeType.getFeeTypeId());
		accountsDto.setFeeType(feeType.getTypeName());
		accountsDto.setDescription(feeType.getDescription());
		
		return accountsDto;
	}
	
	public static boolean isFeeTypeDuplicate(FeeType feeType)
	{
		boolean exist = false;
		try {
			List<FeeType> feeTypes= FeeTypeLocalServiceUtil.findByFeeTypeName(feeType.getTypeName());
			
			for(FeeType oldFeeType:feeTypes)
			{
				if(feeType.getFeeTypeId()!=oldFeeType.getFeeTypeId() && feeType.getTypeName().equalsIgnoreCase(oldFeeType.getTypeName()))
				{
					exist = true;
				}
			}
			
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return exist;
	}

	public void loadFeeFromCourse(long courseId)
	{
		try
		{
			batchFeeBeanItemContainer.removeAllItems();
			List<CourseFee> courseFeeList = CourseFeeLocalServiceUtil.getCourseFeeList(courseId);
			
			long totalFee = 0;
			
			for(CourseFee courseFee:courseFeeList)
			{
				FeesDto feesDto = new FeesDto();
				feesDto.setId(courseFee.getFeeTypeId());
				feesDto.setFeeType(FeeTypeLocalServiceUtil.fetchFeeType(courseFee.getFeeTypeId()).getTypeName());
				feesDto.setAmount(Math.round(courseFee.getAmount()));
				totalFee += feesDto.getAmount();
				batchFeeBeanItemContainer.addBean(feesDto);
			}
			
			batchFeeTable.refreshRowCache();
			batchFeeTable.setColumnFooter(COLUMN_FEE_TYPE,"Total");
			batchFeeTable.setColumnFooter(COLUMN_AMOUNT,String.valueOf(totalFee));

		}
		catch(SystemException se)
		{
			se.printStackTrace();
		}

	}

	public void loadBatchFees(long batchId)
	{
		try
		{
			batchFeeBeanItemContainer.removeAllItems();
			List<FeesDto> batchFeeList = AccountsDao.getBatchFeesList(batchId);
			
			long totalFee = 0;
			
			for(FeesDto feesDto:batchFeeList)
			{
				totalFee += feesDto.getAmount();
				batchFeeBeanItemContainer.addBean(feesDto);
			}
			
			batchFeeTable.refreshRowCache();
			batchFeeTable.setColumnFooter(COLUMN_FEE_TYPE,"Total");
			batchFeeTable.setColumnFooter(COLUMN_AMOUNT,String.valueOf(totalFee));

		}
		catch(SystemException se)
		{
			se.printStackTrace();
		}
		catch(PortalException pe)
		{
			pe.printStackTrace();
		}

	}
	
	public void loadBatchPaymentSchedule(long batchId)
	{
		
		try {
			
			batchPaymentScheduleBeanItemContainer.removeAllItems();
			List<ScheduleDto> scheduleList = AccountsDao.getBatchPaymentSchedules(batchId);
			
			long totalFee = 0;
			
			for(ScheduleDto schedule:scheduleList)
			{
				totalFee += schedule.getAmount();
				batchPaymentScheduleBeanItemContainer.addBean(schedule);
			}
			
			for(int i=1;i<=TOTAL_SCHEDULE_ROWS_COUNT-batchPaymentScheduleBeanItemContainer.size();i++)
			{
				ScheduleDto schedule = new ScheduleDto();
				batchPaymentScheduleBeanItemContainer.addBean(schedule);
			}
			
			batchPaymentScheduleTable.refreshRowCache();
			batchPaymentScheduleTable.setColumnFooter(COLUMN_AMOUNT, String.valueOf(totalFee));
			
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadStudentPaymentSchedule(long studentId,long batchId)
	{
		
		try {
			
			studentPaymentScheduleBeanItemContainer.removeAllItems();
			List<ScheduleDto> scheduleList = AccountsDao.getStudentPaymentSchedules(studentId, batchId);
			
			long totalFee = 0;
			
			for(ScheduleDto schedule:scheduleList)
			{
				totalFee += schedule.getAmount();
				studentPaymentScheduleBeanItemContainer.addBean(schedule);
			}
			
			for(int i=1;i<=TOTAL_SCHEDULE_ROWS_COUNT-studentPaymentScheduleBeanItemContainer.size();i++)
			{
				ScheduleDto schedule = new ScheduleDto();
				studentPaymentScheduleBeanItemContainer.addBean(schedule);
			}
			
			studentPaymentScheduleTable.refreshRowCache();
			studentPaymentScheduleTable.setColumnFooter(COLUMN_AMOUNT, String.valueOf(totalFee));
			
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	public long getTotalCourseFee()
	{
		long total = 0;
		
		for(int i=0;i<courseFeeBeanItemContainer.size();i++)
		{
			FeesDto feesDto = courseFeeBeanItemContainer.getIdByIndex(i);
			
			if(feesDto.getAmount()!=null)
			{
				total += feesDto.getAmount();
			}
		}
		
		return total;
	}

	public long getTotalBatchFee()
	{
		long total = 0;
		
		for(int i=0;i<batchFeeBeanItemContainer.size();i++)
		{
			FeesDto feesDto = batchFeeBeanItemContainer.getIdByIndex(i);
			
			if(feesDto.getAmount()!=null)
			{
				total += feesDto.getAmount();
			}
		}
		
		return total;
	}

	public long getTotalStudentFee()
	{
		long total = 0;
		
		for(int i=0;i<studentFeeBeanItemContainer.size();i++)
		{
			FeesDto feesDto = studentFeeBeanItemContainer.getIdByIndex(i);
			
			if(feesDto.getAmount()!=null)
			{
				total += feesDto.getAmount();
			}
		}
		
		return total;
	}


	public long getTotalBatchPaymentScheduleAmount()
	{
		long total = 0;
		
		for(int i=0;i<batchPaymentScheduleBeanItemContainer.size();i++)
		{
			ScheduleDto scheduleDto = batchPaymentScheduleBeanItemContainer.getIdByIndex(i);
			
			if(scheduleDto.getAmount()!=null)
			{
				total += scheduleDto.getAmount();
			}
		}
		
		return total;
	}
	
	public long getTotalStudentPaymentScheduleAmount()
	{
		long total = 0;
		
		for(int i=0;i<studentPaymentScheduleBeanItemContainer.size();i++)
		{
			ScheduleDto scheduleDto = studentPaymentScheduleBeanItemContainer.getIdByIndex(i);
			
			if(scheduleDto.getAmount()!=null)
			{
				total += scheduleDto.getAmount();
			}
		}
		
		return total;
	}

	
	private void loadStudentFeeContainer()
	{
		studentFeeBeanItemContainer.removeAllItems();
		Object studentIdObject = studentIdField.getValue();
		
		long studentId = 0;
		
		if(studentIdObject!=null)
		{
			studentId = Long.parseLong(studentIdObject.toString());
		}
		
		Object batchObject = studentBatchComboBox.getValue();
		
		long batchId = 0;
		
		if(batchObject!=null)
		{
			batchId = Long.parseLong(batchObject.toString());
		}
		
		try {
			List<StudentFee> studentFeeList = StudentFeeLocalServiceUtil.getStudentFeeList(studentId, batchId);
			
			long totalFee = 0;
			
			for(StudentFee studentFee:studentFeeList)
			{
				FeesDto feesDto = new FeesDto();
				feesDto.setId(studentFee.getFeeTypeId());
				feesDto.setFeeType(FeeTypeLocalServiceUtil.fetchFeeType(studentFee.getFeeTypeId()).getTypeName());
				feesDto.setAmount(Math.round(studentFee.getAmount()));
				totalFee += feesDto.getAmount();
				studentFeeBeanItemContainer.addBean(feesDto);
			}
			
			if(studentFeeList.size()==0)
			{
				List<FeesDto> batchFeeList = AccountsDao.getBatchFeesList(batchId);
				
				for(FeesDto feesDto:batchFeeList)
				{
					totalFee += feesDto.getAmount();
					studentFeeBeanItemContainer.addBean(feesDto);
				}
			}
			
			studentFeeTable.refreshRowCache();
			studentFeeTable.setColumnFooter(COLUMN_FEE_TYPE,"Total");
			studentFeeTable.setColumnFooter(COLUMN_AMOUNT,String.valueOf(totalFee));
			
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (PortalException pe){
			pe.printStackTrace();
		}
	}


}
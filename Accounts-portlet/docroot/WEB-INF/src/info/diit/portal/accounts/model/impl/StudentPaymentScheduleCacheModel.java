/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.accounts.model.StudentPaymentSchedule;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing StudentPaymentSchedule in entity cache.
 *
 * @author shamsuddin
 * @see StudentPaymentSchedule
 * @generated
 */
public class StudentPaymentScheduleCacheModel implements CacheModel<StudentPaymentSchedule>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{studentPaymentScheduleId=");
		sb.append(studentPaymentScheduleId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", studentId=");
		sb.append(studentId);
		sb.append(", batchId=");
		sb.append(batchId);
		sb.append(", scheduleDate=");
		sb.append(scheduleDate);
		sb.append(", feeTypeId=");
		sb.append(feeTypeId);
		sb.append(", amount=");
		sb.append(amount);
		sb.append("}");

		return sb.toString();
	}

	public StudentPaymentSchedule toEntityModel() {
		StudentPaymentScheduleImpl studentPaymentScheduleImpl = new StudentPaymentScheduleImpl();

		studentPaymentScheduleImpl.setStudentPaymentScheduleId(studentPaymentScheduleId);
		studentPaymentScheduleImpl.setCompanyId(companyId);
		studentPaymentScheduleImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			studentPaymentScheduleImpl.setCreateDate(null);
		}
		else {
			studentPaymentScheduleImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			studentPaymentScheduleImpl.setModifiedDate(null);
		}
		else {
			studentPaymentScheduleImpl.setModifiedDate(new Date(modifiedDate));
		}

		studentPaymentScheduleImpl.setStudentId(studentId);
		studentPaymentScheduleImpl.setBatchId(batchId);

		if (scheduleDate == Long.MIN_VALUE) {
			studentPaymentScheduleImpl.setScheduleDate(null);
		}
		else {
			studentPaymentScheduleImpl.setScheduleDate(new Date(scheduleDate));
		}

		studentPaymentScheduleImpl.setFeeTypeId(feeTypeId);
		studentPaymentScheduleImpl.setAmount(amount);

		studentPaymentScheduleImpl.resetOriginalValues();

		return studentPaymentScheduleImpl;
	}

	public long studentPaymentScheduleId;
	public long companyId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public long studentId;
	public long batchId;
	public long scheduleDate;
	public long feeTypeId;
	public double amount;
}
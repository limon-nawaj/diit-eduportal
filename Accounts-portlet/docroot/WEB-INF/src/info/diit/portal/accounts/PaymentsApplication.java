package info.diit.portal.accounts;

import info.diit.portal.accounts.dao.AccountsDao;
import info.diit.portal.accounts.dto.BatchDto;
import info.diit.portal.accounts.dto.FeesDto;
import info.diit.portal.accounts.dto.PaymentDto;
import info.diit.portal.accounts.dto.StudentDto;
import info.diit.portal.accounts.model.FeeType;
import info.diit.portal.accounts.model.Payment;
import info.diit.portal.accounts.model.impl.PaymentImpl;
import info.diit.portal.accounts.service.FeeTypeLocalServiceUtil;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import net.atontech.vaadin.ui.numericfield.NumericField;
import net.atontech.vaadin.ui.numericfield.widgetset.shared.NumericFieldType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.Notification;

public class PaymentsApplication extends Application  implements PortletRequestListener{

	private ThemeDisplay themeDisplay;
	private Window window;
	
	private static final String COLUMN_FEE_TYPE = "feeType";
	private static final String COLUMN_AMOUNT = "amount";
	private static final String COLUMN_PAYMENT_DATE = "paymentDate";
	
	private NumericField studentIdField;
	private TextField nameField;
	private ComboBox studentBatchComboBox;
	private NumericField totalPaidField;
	private NumericField totalDueField;
	private DateField dateField;
	private Table paymentTable;
	private BeanItemContainer<FeesDto> feeBeanItemContainer;
	private Table studentPaymentHistoryTable;
	private BeanItemContainer<PaymentDto> studentPaymentHistoryBeanItemContainer;
	
	Button saveButton;
	
	public final static String DATE_FORMAT = "dd/MM/yyyy";
	public static SimpleDateFormat simpleDateFormat;
	private static NumberFormat numberFormat;
	
    public void init() {
    	
    	simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
    	numberFormat = NumberFormat.getIntegerInstance();
    	
    	window = new Window();
		window.setSizeFull();
		setMainWindow(window);
		
		loadFeeTypesContainer();
		
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.setSpacing(true);

		verticalLayout.addComponent(initPaymentsPanel());
		
		window.addComponent(verticalLayout);
    }
    
	@Override
    public void onRequestStart(PortletRequest p_request, PortletResponse p_response) {
        themeDisplay = (ThemeDisplay) p_request.getAttribute(WebKeys.THEME_DISPLAY);
        
    }
	
	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}

	private GridLayout initPaymentsPanel()
	{
		studentIdField = new NumericField("Student ID");
		studentIdField.setAllowNegative(false);
		studentIdField.setImmediate(true);
		studentIdField.setNullRepresentation("");
		studentIdField.setNumberType(NumericFieldType.INTEGER);
		studentIdField.setWidth("100%");
		
		nameField = new TextField("Student Name");
		nameField.setReadOnly(true);
		nameField.setWidth("100%");
		
		studentBatchComboBox = new ComboBox("Batch");
		studentBatchComboBox.setImmediate(true);
		studentBatchComboBox.setWidth("100%");
		
		totalPaidField = new NumericField("Total Paid");
		totalPaidField.setWidth("100%");
		totalPaidField.setNullRepresentation("");
		totalPaidField.setNumberType(NumericFieldType.INTEGER);
		totalPaidField.setReadOnly(true);

		totalDueField = new NumericField("Total Due");
		totalDueField.setWidth("100%");
		totalDueField.setNullRepresentation("");
		totalDueField.setNumberType(NumericFieldType.INTEGER);
		totalDueField.setReadOnly(true);
		
		dateField = new DateField("Date");
		dateField.setResolution(DateField.RESOLUTION_DAY);
		dateField.setValue(new Date());
		dateField.setDateFormat(DATE_FORMAT);
		dateField.setWidth("100%");
		
		paymentTable = new Table("New Payment",feeBeanItemContainer);
		paymentTable.setImmediate(true);
		paymentTable.setWidth("100%");
		paymentTable.setPageLength(5);
		
		paymentTable.setColumnHeader(COLUMN_FEE_TYPE, "Fee Type");
		paymentTable.setColumnHeader(COLUMN_AMOUNT, "Amount");

		paymentTable.setVisibleColumns(new String[]{COLUMN_FEE_TYPE,COLUMN_AMOUNT});
		
		paymentTable.setFooterVisible(true);
		
		paymentTable.setColumnFooter(COLUMN_FEE_TYPE, "Total");
		paymentTable.setColumnFooter(COLUMN_AMOUNT, "0");
		
		paymentTable.setColumnAlignment(COLUMN_AMOUNT, Table.ALIGN_RIGHT);
		
		paymentTable.setEditable(true);
		
		paymentTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {

				
				if(propertyId.equals(COLUMN_FEE_TYPE))
				{
					TextField field = new TextField();
					field.setWidth("100%");
					field.addStyleName("align-right");
					field.setReadOnly(true);
					
					
					return field;
				}
				
				if(propertyId.equals(COLUMN_AMOUNT))
				{
					NumericField field = new NumericField();
					field.setWidth("100%");
					field.setAllowNegative(false);
					field.setNullSettingAllowed(true);
					field.setNullRepresentation("");
					field.setNumberType(NumericFieldType.INTEGER);
					field.setImmediate(true);
					
					field.addListener(new ValueChangeListener() {
						
						@Override
						public void valueChange(ValueChangeEvent event) {
							
							long total = getNewPaymentTotal();
							paymentTable.setColumnFooter(COLUMN_AMOUNT, String.valueOf(total));
							
						}
					});
					
					return field;
					
				}
				
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});

		
		studentPaymentHistoryBeanItemContainer = new BeanItemContainer<PaymentDto>(PaymentDto.class);
		studentPaymentHistoryTable = new Table("Payment History",studentPaymentHistoryBeanItemContainer)
		{
			@Override
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				
				if(property.getType()==Date.class)
				{
					return simpleDateFormat.format(property.getValue());
				}
				
				else if(property.getType()==Double.class)
				{
					return numberFormat.format(property.getValue());
				}
				
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		studentPaymentHistoryTable.setWidth("100%");
		studentPaymentHistoryTable.setPageLength(17);
		
		studentPaymentHistoryTable.setColumnHeader(COLUMN_PAYMENT_DATE, "Date");
		studentPaymentHistoryTable.setColumnHeader(COLUMN_FEE_TYPE, "Fee Type");
		studentPaymentHistoryTable.setColumnHeader(COLUMN_AMOUNT, "Amount");
		
		studentPaymentHistoryTable.setColumnExpandRatio(COLUMN_PAYMENT_DATE, 1);
		studentPaymentHistoryTable.setColumnExpandRatio(COLUMN_FEE_TYPE, 1);
		studentPaymentHistoryTable.setColumnExpandRatio(COLUMN_AMOUNT, 1);
		
		studentPaymentHistoryTable.setColumnAlignment(COLUMN_AMOUNT, Table.ALIGN_RIGHT);
		
		studentPaymentHistoryTable.setVisibleColumns(new String[]{COLUMN_PAYMENT_DATE,COLUMN_FEE_TYPE,COLUMN_AMOUNT});
		
		studentPaymentHistoryTable.setSelectable(true);
		
		
		
		saveButton = new Button("Save");
		saveButton.setEnabled(false);
		
		Label spacer = new Label();
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		horizontalLayout.setWidth("100%");
		
		horizontalLayout.addComponent(spacer);
		horizontalLayout.addComponent(saveButton);
		
		horizontalLayout.setExpandRatio(spacer, 1);

		GridLayout gridLayout = new GridLayout(5,6);
		gridLayout.setWidth("100%");
		gridLayout.setSpacing(true);
		
		gridLayout.addComponent(studentIdField,0,0);
		gridLayout.addComponent(studentBatchComboBox,1,0);
		gridLayout.addComponent(nameField,0,1,1,1);
		gridLayout.addComponent(totalPaidField,0,2);
		gridLayout.addComponent(totalDueField,1,2);
		gridLayout.addComponent(dateField,0,3);
		gridLayout.addComponent(paymentTable,0,4,1,4);
		gridLayout.addComponent(studentPaymentHistoryTable,2,0,4,4);
		gridLayout.addComponent(horizontalLayout,0,5,1,5);
		
		studentIdField.addListener(new BlurListener() {
			
			@Override
			public void blur(BlurEvent event) {
				
				Object studentIdObject = studentIdField.getValue();
				if(studentIdObject!=null)
				{
					long studentId = Long.parseLong(studentIdObject.toString());
					
					try {
						
						StudentDto studentDto = AccountsDao.getStudentDto(studentId);
						
						if(studentDto!=null)
						{
							
							ArrayList<BatchDto> batchList = studentDto.getBatchList();
							
							studentBatchComboBox.removeAllItems();
							
							for(BatchDto batchDto:batchList)
							{
								studentBatchComboBox.addItem(batchDto);
							}
							nameField.setReadOnly(false);
							nameField.setValue(studentDto.getName());
							nameField.setReadOnly(true);
						}
						else
						{
							studentBatchComboBox.removeAllItems();
							nameField.setReadOnly(false);
							nameField.setValue("");
							nameField.setReadOnly(true);
							clearStudentBatchPaymentForm();
							window.showNotification("No student found with ID "+studentId,Notification.TYPE_ERROR_MESSAGE);
						}
					} catch (SystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}
		});
		
		studentBatchComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				showPaymentRecords();
			}
		});
		
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				ArrayList<Payment> paymentList = new ArrayList<Payment>();
				
				for(int i=0;i<feeBeanItemContainer.size();i++)
				{
					FeesDto feesDto = feeBeanItemContainer.getIdByIndex(i); 
					
					if(feesDto.getAmount()!=null && feesDto.getAmount()!=0)
					{
						Payment payment = new PaymentImpl();
						payment.setAmount(feesDto.getAmount());
						
						BatchDto batch = (BatchDto)studentBatchComboBox.getValue();
						payment.setBatchId(batch.getBatchId());
						
						payment.setCompanyId(themeDisplay.getCompanyId());
						payment.setCreateDate(new Date());
						payment.setFeeTypeId(feesDto.getId());
						payment.setModifiedDate(new Date());
						payment.setPaymentDate((Date)dateField.getValue());
						payment.setStudentId(Long.parseLong(studentIdField.getValue().toString()));
						payment.setUserId(themeDisplay.getUserId());
						
						paymentList.add(payment);
					}
					
				}
				
				try {
					AccountsDao.savePaymentList(paymentList);
					window.showNotification("Payment records saved!");
					showPaymentRecords();
				} catch (SystemException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});

		
		return gridLayout;
	}
	
	public void showPaymentRecords()
	{
		Object studentIdObject = studentIdField.getValue();
		if(studentIdObject!=null)
		{
			long studentId = Long.parseLong(studentIdObject.toString());
			BatchDto selectedBatch = (BatchDto)studentBatchComboBox.getValue();
			
			if(selectedBatch!=null)
			{
				try {
					double totalPaid = AccountsDao.getTotalPaidAmount(studentId, selectedBatch.getBatchId());
					totalPaidField.setReadOnly(false);
					totalPaidField.setValue(String.valueOf(Math.round(totalPaid)));
					totalPaidField.setReadOnly(true);
					
					
				} catch (SystemException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				try {
					
					Calendar today = Calendar.getInstance();
					today.set(Calendar.HOUR_OF_DAY, 23);
					today.set(Calendar.MINUTE,59);
					today.set(Calendar.SECOND, 59);
					
					double totalDue = AccountsDao.getTotalDueAmount(studentId, selectedBatch.getBatchId(), today.getTime());
					totalDueField.setReadOnly(false);
					totalDueField.setValue(String.valueOf(Math.round(totalDue)));
					totalDueField.setReadOnly(true);
				} catch (SystemException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (PortalException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					
					studentPaymentHistoryBeanItemContainer.removeAllItems();
					ArrayList<PaymentDto> paymentHistory = AccountsDao.getPaymentList(studentId, selectedBatch.getBatchId());
					
					for(PaymentDto payment:paymentHistory)
					{
						studentPaymentHistoryBeanItemContainer.addBean(payment);
					}
					
					studentPaymentHistoryTable.refreshRowCache();
					
				} catch (SystemException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (PortalException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				saveButton.setEnabled(true);
				
				
			}
			else
			{
				clearStudentBatchPaymentForm();
			}
		}
		else
		{
			clearStudentBatchPaymentForm();
		}

	}
	
	public void clearStudentBatchPaymentForm()
	{
		totalPaidField.setReadOnly(false);
		totalPaidField.setValue("");
		totalPaidField.setReadOnly(true);
		
		totalDueField.setReadOnly(false);
		totalDueField.setValue("");
		totalDueField.setReadOnly(true);
		
		saveButton.setEnabled(false);
		
		studentPaymentHistoryBeanItemContainer.removeAllItems();
		studentPaymentHistoryTable.refreshRowCache();
		
		for(int i=0;i<feeBeanItemContainer.size();i++)
		{
			feeBeanItemContainer.getIdByIndex(i).setAmount(null);
		}
		
		paymentTable.refreshRowCache();
		
	}

	private void loadFeeTypesContainer() 
	{
		try
		{
			feeBeanItemContainer = new BeanItemContainer<FeesDto>(FeesDto.class);
			List<FeeType> feeTypesList = FeeTypeLocalServiceUtil.getFeeTypes(0, FeeTypeLocalServiceUtil.getFeeTypesCount());
			
			for(FeeType feeType:feeTypesList)
			{
				FeesDto accountsDto = new FeesDto();
				accountsDto.setId(feeType.getFeeTypeId());
				accountsDto.setFeeType(feeType.getTypeName());
				accountsDto.setDescription(feeType.getDescription());
				
				feeBeanItemContainer.addBean(accountsDto);
				
			}
			
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public long getNewPaymentTotal()
	{
		long total = 0;
		
		for(int i=0;i<feeBeanItemContainer.size();i++)
		{
			FeesDto feesDto = feeBeanItemContainer.getIdByIndex(i);
			
			if(feesDto.getAmount()!=null)
			{
				total += feesDto.getAmount();
			}
		}
		
		return total;
	}

}

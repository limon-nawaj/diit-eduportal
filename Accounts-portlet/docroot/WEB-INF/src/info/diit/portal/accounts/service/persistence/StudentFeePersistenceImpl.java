/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.accounts.NoSuchStudentFeeException;
import info.diit.portal.accounts.model.StudentFee;
import info.diit.portal.accounts.model.impl.StudentFeeImpl;
import info.diit.portal.accounts.model.impl.StudentFeeModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the student fee service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shamsuddin
 * @see StudentFeePersistence
 * @see StudentFeeUtil
 * @generated
 */
public class StudentFeePersistenceImpl extends BasePersistenceImpl<StudentFee>
	implements StudentFeePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link StudentFeeUtil} to access the student fee persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = StudentFeeImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTBATCH =
		new FinderPath(StudentFeeModelImpl.ENTITY_CACHE_ENABLED,
			StudentFeeModelImpl.FINDER_CACHE_ENABLED, StudentFeeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByStudentBatch",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH =
		new FinderPath(StudentFeeModelImpl.ENTITY_CACHE_ENABLED,
			StudentFeeModelImpl.FINDER_CACHE_ENABLED, StudentFeeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStudentBatch",
			new String[] { Long.class.getName(), Long.class.getName() },
			StudentFeeModelImpl.STUDENTID_COLUMN_BITMASK |
			StudentFeeModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STUDENTBATCH = new FinderPath(StudentFeeModelImpl.ENTITY_CACHE_ENABLED,
			StudentFeeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByStudentBatch",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(StudentFeeModelImpl.ENTITY_CACHE_ENABLED,
			StudentFeeModelImpl.FINDER_CACHE_ENABLED, StudentFeeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(StudentFeeModelImpl.ENTITY_CACHE_ENABLED,
			StudentFeeModelImpl.FINDER_CACHE_ENABLED, StudentFeeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(StudentFeeModelImpl.ENTITY_CACHE_ENABLED,
			StudentFeeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the student fee in the entity cache if it is enabled.
	 *
	 * @param studentFee the student fee
	 */
	public void cacheResult(StudentFee studentFee) {
		EntityCacheUtil.putResult(StudentFeeModelImpl.ENTITY_CACHE_ENABLED,
			StudentFeeImpl.class, studentFee.getPrimaryKey(), studentFee);

		studentFee.resetOriginalValues();
	}

	/**
	 * Caches the student fees in the entity cache if it is enabled.
	 *
	 * @param studentFees the student fees
	 */
	public void cacheResult(List<StudentFee> studentFees) {
		for (StudentFee studentFee : studentFees) {
			if (EntityCacheUtil.getResult(
						StudentFeeModelImpl.ENTITY_CACHE_ENABLED,
						StudentFeeImpl.class, studentFee.getPrimaryKey()) == null) {
				cacheResult(studentFee);
			}
			else {
				studentFee.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all student fees.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(StudentFeeImpl.class.getName());
		}

		EntityCacheUtil.clearCache(StudentFeeImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the student fee.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(StudentFee studentFee) {
		EntityCacheUtil.removeResult(StudentFeeModelImpl.ENTITY_CACHE_ENABLED,
			StudentFeeImpl.class, studentFee.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<StudentFee> studentFees) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (StudentFee studentFee : studentFees) {
			EntityCacheUtil.removeResult(StudentFeeModelImpl.ENTITY_CACHE_ENABLED,
				StudentFeeImpl.class, studentFee.getPrimaryKey());
		}
	}

	/**
	 * Creates a new student fee with the primary key. Does not add the student fee to the database.
	 *
	 * @param studentFeeId the primary key for the new student fee
	 * @return the new student fee
	 */
	public StudentFee create(long studentFeeId) {
		StudentFee studentFee = new StudentFeeImpl();

		studentFee.setNew(true);
		studentFee.setPrimaryKey(studentFeeId);

		return studentFee;
	}

	/**
	 * Removes the student fee with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param studentFeeId the primary key of the student fee
	 * @return the student fee that was removed
	 * @throws info.diit.portal.accounts.NoSuchStudentFeeException if a student fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentFee remove(long studentFeeId)
		throws NoSuchStudentFeeException, SystemException {
		return remove(Long.valueOf(studentFeeId));
	}

	/**
	 * Removes the student fee with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the student fee
	 * @return the student fee that was removed
	 * @throws info.diit.portal.accounts.NoSuchStudentFeeException if a student fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentFee remove(Serializable primaryKey)
		throws NoSuchStudentFeeException, SystemException {
		Session session = null;

		try {
			session = openSession();

			StudentFee studentFee = (StudentFee)session.get(StudentFeeImpl.class,
					primaryKey);

			if (studentFee == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchStudentFeeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(studentFee);
		}
		catch (NoSuchStudentFeeException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected StudentFee removeImpl(StudentFee studentFee)
		throws SystemException {
		studentFee = toUnwrappedModel(studentFee);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, studentFee);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(studentFee);

		return studentFee;
	}

	@Override
	public StudentFee updateImpl(
		info.diit.portal.accounts.model.StudentFee studentFee, boolean merge)
		throws SystemException {
		studentFee = toUnwrappedModel(studentFee);

		boolean isNew = studentFee.isNew();

		StudentFeeModelImpl studentFeeModelImpl = (StudentFeeModelImpl)studentFee;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, studentFee, merge);

			studentFee.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !StudentFeeModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((studentFeeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(studentFeeModelImpl.getOriginalStudentId()),
						Long.valueOf(studentFeeModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH,
					args);

				args = new Object[] {
						Long.valueOf(studentFeeModelImpl.getStudentId()),
						Long.valueOf(studentFeeModelImpl.getBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH,
					args);
			}
		}

		EntityCacheUtil.putResult(StudentFeeModelImpl.ENTITY_CACHE_ENABLED,
			StudentFeeImpl.class, studentFee.getPrimaryKey(), studentFee);

		return studentFee;
	}

	protected StudentFee toUnwrappedModel(StudentFee studentFee) {
		if (studentFee instanceof StudentFeeImpl) {
			return studentFee;
		}

		StudentFeeImpl studentFeeImpl = new StudentFeeImpl();

		studentFeeImpl.setNew(studentFee.isNew());
		studentFeeImpl.setPrimaryKey(studentFee.getPrimaryKey());

		studentFeeImpl.setStudentFeeId(studentFee.getStudentFeeId());
		studentFeeImpl.setCompanyId(studentFee.getCompanyId());
		studentFeeImpl.setUserId(studentFee.getUserId());
		studentFeeImpl.setCreateDate(studentFee.getCreateDate());
		studentFeeImpl.setModifiedDate(studentFee.getModifiedDate());
		studentFeeImpl.setStudentId(studentFee.getStudentId());
		studentFeeImpl.setBatchId(studentFee.getBatchId());
		studentFeeImpl.setFeeTypeId(studentFee.getFeeTypeId());
		studentFeeImpl.setAmount(studentFee.getAmount());

		return studentFeeImpl;
	}

	/**
	 * Returns the student fee with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the student fee
	 * @return the student fee
	 * @throws com.liferay.portal.NoSuchModelException if a student fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentFee findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the student fee with the primary key or throws a {@link info.diit.portal.accounts.NoSuchStudentFeeException} if it could not be found.
	 *
	 * @param studentFeeId the primary key of the student fee
	 * @return the student fee
	 * @throws info.diit.portal.accounts.NoSuchStudentFeeException if a student fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentFee findByPrimaryKey(long studentFeeId)
		throws NoSuchStudentFeeException, SystemException {
		StudentFee studentFee = fetchByPrimaryKey(studentFeeId);

		if (studentFee == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + studentFeeId);
			}

			throw new NoSuchStudentFeeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				studentFeeId);
		}

		return studentFee;
	}

	/**
	 * Returns the student fee with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the student fee
	 * @return the student fee, or <code>null</code> if a student fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentFee fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the student fee with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param studentFeeId the primary key of the student fee
	 * @return the student fee, or <code>null</code> if a student fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentFee fetchByPrimaryKey(long studentFeeId)
		throws SystemException {
		StudentFee studentFee = (StudentFee)EntityCacheUtil.getResult(StudentFeeModelImpl.ENTITY_CACHE_ENABLED,
				StudentFeeImpl.class, studentFeeId);

		if (studentFee == _nullStudentFee) {
			return null;
		}

		if (studentFee == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				studentFee = (StudentFee)session.get(StudentFeeImpl.class,
						Long.valueOf(studentFeeId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (studentFee != null) {
					cacheResult(studentFee);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(StudentFeeModelImpl.ENTITY_CACHE_ENABLED,
						StudentFeeImpl.class, studentFeeId, _nullStudentFee);
				}

				closeSession(session);
			}
		}

		return studentFee;
	}

	/**
	 * Returns all the student fees where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @return the matching student fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentFee> findByStudentBatch(long studentId, long batchId)
		throws SystemException {
		return findByStudentBatch(studentId, batchId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the student fees where studentId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of student fees
	 * @param end the upper bound of the range of student fees (not inclusive)
	 * @return the range of matching student fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentFee> findByStudentBatch(long studentId, long batchId,
		int start, int end) throws SystemException {
		return findByStudentBatch(studentId, batchId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the student fees where studentId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of student fees
	 * @param end the upper bound of the range of student fees (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching student fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentFee> findByStudentBatch(long studentId, long batchId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH;
			finderArgs = new Object[] { studentId, batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTBATCH;
			finderArgs = new Object[] {
					studentId, batchId,
					
					start, end, orderByComparator
				};
		}

		List<StudentFee> list = (List<StudentFee>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (StudentFee studentFee : list) {
				if ((studentId != studentFee.getStudentId()) ||
						(batchId != studentFee.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_STUDENTFEE_WHERE);

			query.append(_FINDER_COLUMN_STUDENTBATCH_STUDENTID_2);

			query.append(_FINDER_COLUMN_STUDENTBATCH_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				qPos.add(batchId);

				list = (List<StudentFee>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student fee
	 * @throws info.diit.portal.accounts.NoSuchStudentFeeException if a matching student fee could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentFee findByStudentBatch_First(long studentId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchStudentFeeException, SystemException {
		StudentFee studentFee = fetchByStudentBatch_First(studentId, batchId,
				orderByComparator);

		if (studentFee != null) {
			return studentFee;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchStudentFeeException(msg.toString());
	}

	/**
	 * Returns the first student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student fee, or <code>null</code> if a matching student fee could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentFee fetchByStudentBatch_First(long studentId, long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		List<StudentFee> list = findByStudentBatch(studentId, batchId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student fee
	 * @throws info.diit.portal.accounts.NoSuchStudentFeeException if a matching student fee could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentFee findByStudentBatch_Last(long studentId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchStudentFeeException, SystemException {
		StudentFee studentFee = fetchByStudentBatch_Last(studentId, batchId,
				orderByComparator);

		if (studentFee != null) {
			return studentFee;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchStudentFeeException(msg.toString());
	}

	/**
	 * Returns the last student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student fee, or <code>null</code> if a matching student fee could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentFee fetchByStudentBatch_Last(long studentId, long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByStudentBatch(studentId, batchId);

		List<StudentFee> list = findByStudentBatch(studentId, batchId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the student fees before and after the current student fee in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentFeeId the primary key of the current student fee
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next student fee
	 * @throws info.diit.portal.accounts.NoSuchStudentFeeException if a student fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentFee[] findByStudentBatch_PrevAndNext(long studentFeeId,
		long studentId, long batchId, OrderByComparator orderByComparator)
		throws NoSuchStudentFeeException, SystemException {
		StudentFee studentFee = findByPrimaryKey(studentFeeId);

		Session session = null;

		try {
			session = openSession();

			StudentFee[] array = new StudentFeeImpl[3];

			array[0] = getByStudentBatch_PrevAndNext(session, studentFee,
					studentId, batchId, orderByComparator, true);

			array[1] = studentFee;

			array[2] = getByStudentBatch_PrevAndNext(session, studentFee,
					studentId, batchId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected StudentFee getByStudentBatch_PrevAndNext(Session session,
		StudentFee studentFee, long studentId, long batchId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_STUDENTFEE_WHERE);

		query.append(_FINDER_COLUMN_STUDENTBATCH_STUDENTID_2);

		query.append(_FINDER_COLUMN_STUDENTBATCH_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(studentId);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(studentFee);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<StudentFee> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the student fees.
	 *
	 * @return the student fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentFee> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the student fees.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of student fees
	 * @param end the upper bound of the range of student fees (not inclusive)
	 * @return the range of student fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentFee> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the student fees.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of student fees
	 * @param end the upper bound of the range of student fees (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of student fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentFee> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<StudentFee> list = (List<StudentFee>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_STUDENTFEE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_STUDENTFEE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<StudentFee>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<StudentFee>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the student fees where studentId = &#63; and batchId = &#63; from the database.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByStudentBatch(long studentId, long batchId)
		throws SystemException {
		for (StudentFee studentFee : findByStudentBatch(studentId, batchId)) {
			remove(studentFee);
		}
	}

	/**
	 * Removes all the student fees from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (StudentFee studentFee : findAll()) {
			remove(studentFee);
		}
	}

	/**
	 * Returns the number of student fees where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @return the number of matching student fees
	 * @throws SystemException if a system exception occurred
	 */
	public int countByStudentBatch(long studentId, long batchId)
		throws SystemException {
		Object[] finderArgs = new Object[] { studentId, batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_STUDENTFEE_WHERE);

			query.append(_FINDER_COLUMN_STUDENTBATCH_STUDENTID_2);

			query.append(_FINDER_COLUMN_STUDENTBATCH_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of student fees.
	 *
	 * @return the number of student fees
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_STUDENTFEE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the student fee persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.accounts.model.StudentFee")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<StudentFee>> listenersList = new ArrayList<ModelListener<StudentFee>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<StudentFee>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(StudentFeeImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_STUDENTFEE = "SELECT studentFee FROM StudentFee studentFee";
	private static final String _SQL_SELECT_STUDENTFEE_WHERE = "SELECT studentFee FROM StudentFee studentFee WHERE ";
	private static final String _SQL_COUNT_STUDENTFEE = "SELECT COUNT(studentFee) FROM StudentFee studentFee";
	private static final String _SQL_COUNT_STUDENTFEE_WHERE = "SELECT COUNT(studentFee) FROM StudentFee studentFee WHERE ";
	private static final String _FINDER_COLUMN_STUDENTBATCH_STUDENTID_2 = "studentFee.studentId = ? AND ";
	private static final String _FINDER_COLUMN_STUDENTBATCH_BATCHID_2 = "studentFee.batchId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "studentFee.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No StudentFee exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No StudentFee exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(StudentFeePersistenceImpl.class);
	private static StudentFee _nullStudentFee = new StudentFeeImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<StudentFee> toCacheModel() {
				return _nullStudentFeeCacheModel;
			}
		};

	private static CacheModel<StudentFee> _nullStudentFeeCacheModel = new CacheModel<StudentFee>() {
			public StudentFee toEntityModel() {
				return _nullStudentFee;
			}
		};
}
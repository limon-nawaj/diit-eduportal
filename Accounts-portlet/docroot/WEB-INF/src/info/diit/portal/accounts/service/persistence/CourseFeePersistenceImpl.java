/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.accounts.NoSuchCourseFeeException;
import info.diit.portal.accounts.model.CourseFee;
import info.diit.portal.accounts.model.impl.CourseFeeImpl;
import info.diit.portal.accounts.model.impl.CourseFeeModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the course fee service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shamsuddin
 * @see CourseFeePersistence
 * @see CourseFeeUtil
 * @generated
 */
public class CourseFeePersistenceImpl extends BasePersistenceImpl<CourseFee>
	implements CourseFeePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CourseFeeUtil} to access the course fee persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CourseFeeImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COURSE = new FinderPath(CourseFeeModelImpl.ENTITY_CACHE_ENABLED,
			CourseFeeModelImpl.FINDER_CACHE_ENABLED, CourseFeeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCourse",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSE =
		new FinderPath(CourseFeeModelImpl.ENTITY_CACHE_ENABLED,
			CourseFeeModelImpl.FINDER_CACHE_ENABLED, CourseFeeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCourse",
			new String[] { Long.class.getName() },
			CourseFeeModelImpl.COURSEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COURSE = new FinderPath(CourseFeeModelImpl.ENTITY_CACHE_ENABLED,
			CourseFeeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCourse",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CourseFeeModelImpl.ENTITY_CACHE_ENABLED,
			CourseFeeModelImpl.FINDER_CACHE_ENABLED, CourseFeeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CourseFeeModelImpl.ENTITY_CACHE_ENABLED,
			CourseFeeModelImpl.FINDER_CACHE_ENABLED, CourseFeeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CourseFeeModelImpl.ENTITY_CACHE_ENABLED,
			CourseFeeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the course fee in the entity cache if it is enabled.
	 *
	 * @param courseFee the course fee
	 */
	public void cacheResult(CourseFee courseFee) {
		EntityCacheUtil.putResult(CourseFeeModelImpl.ENTITY_CACHE_ENABLED,
			CourseFeeImpl.class, courseFee.getPrimaryKey(), courseFee);

		courseFee.resetOriginalValues();
	}

	/**
	 * Caches the course fees in the entity cache if it is enabled.
	 *
	 * @param courseFees the course fees
	 */
	public void cacheResult(List<CourseFee> courseFees) {
		for (CourseFee courseFee : courseFees) {
			if (EntityCacheUtil.getResult(
						CourseFeeModelImpl.ENTITY_CACHE_ENABLED,
						CourseFeeImpl.class, courseFee.getPrimaryKey()) == null) {
				cacheResult(courseFee);
			}
			else {
				courseFee.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all course fees.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CourseFeeImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CourseFeeImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the course fee.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CourseFee courseFee) {
		EntityCacheUtil.removeResult(CourseFeeModelImpl.ENTITY_CACHE_ENABLED,
			CourseFeeImpl.class, courseFee.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CourseFee> courseFees) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CourseFee courseFee : courseFees) {
			EntityCacheUtil.removeResult(CourseFeeModelImpl.ENTITY_CACHE_ENABLED,
				CourseFeeImpl.class, courseFee.getPrimaryKey());
		}
	}

	/**
	 * Creates a new course fee with the primary key. Does not add the course fee to the database.
	 *
	 * @param courseFeeId the primary key for the new course fee
	 * @return the new course fee
	 */
	public CourseFee create(long courseFeeId) {
		CourseFee courseFee = new CourseFeeImpl();

		courseFee.setNew(true);
		courseFee.setPrimaryKey(courseFeeId);

		return courseFee;
	}

	/**
	 * Removes the course fee with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param courseFeeId the primary key of the course fee
	 * @return the course fee that was removed
	 * @throws info.diit.portal.accounts.NoSuchCourseFeeException if a course fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseFee remove(long courseFeeId)
		throws NoSuchCourseFeeException, SystemException {
		return remove(Long.valueOf(courseFeeId));
	}

	/**
	 * Removes the course fee with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the course fee
	 * @return the course fee that was removed
	 * @throws info.diit.portal.accounts.NoSuchCourseFeeException if a course fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CourseFee remove(Serializable primaryKey)
		throws NoSuchCourseFeeException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CourseFee courseFee = (CourseFee)session.get(CourseFeeImpl.class,
					primaryKey);

			if (courseFee == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCourseFeeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(courseFee);
		}
		catch (NoSuchCourseFeeException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CourseFee removeImpl(CourseFee courseFee)
		throws SystemException {
		courseFee = toUnwrappedModel(courseFee);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, courseFee);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(courseFee);

		return courseFee;
	}

	@Override
	public CourseFee updateImpl(
		info.diit.portal.accounts.model.CourseFee courseFee, boolean merge)
		throws SystemException {
		courseFee = toUnwrappedModel(courseFee);

		boolean isNew = courseFee.isNew();

		CourseFeeModelImpl courseFeeModelImpl = (CourseFeeModelImpl)courseFee;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, courseFee, merge);

			courseFee.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CourseFeeModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((courseFeeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(courseFeeModelImpl.getOriginalCourseId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COURSE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSE,
					args);

				args = new Object[] {
						Long.valueOf(courseFeeModelImpl.getCourseId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COURSE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSE,
					args);
			}
		}

		EntityCacheUtil.putResult(CourseFeeModelImpl.ENTITY_CACHE_ENABLED,
			CourseFeeImpl.class, courseFee.getPrimaryKey(), courseFee);

		return courseFee;
	}

	protected CourseFee toUnwrappedModel(CourseFee courseFee) {
		if (courseFee instanceof CourseFeeImpl) {
			return courseFee;
		}

		CourseFeeImpl courseFeeImpl = new CourseFeeImpl();

		courseFeeImpl.setNew(courseFee.isNew());
		courseFeeImpl.setPrimaryKey(courseFee.getPrimaryKey());

		courseFeeImpl.setCourseFeeId(courseFee.getCourseFeeId());
		courseFeeImpl.setCompanyId(courseFee.getCompanyId());
		courseFeeImpl.setUserId(courseFee.getUserId());
		courseFeeImpl.setCreateDate(courseFee.getCreateDate());
		courseFeeImpl.setModifiedDate(courseFee.getModifiedDate());
		courseFeeImpl.setCourseId(courseFee.getCourseId());
		courseFeeImpl.setFeeTypeId(courseFee.getFeeTypeId());
		courseFeeImpl.setAmount(courseFee.getAmount());

		return courseFeeImpl;
	}

	/**
	 * Returns the course fee with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the course fee
	 * @return the course fee
	 * @throws com.liferay.portal.NoSuchModelException if a course fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CourseFee findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the course fee with the primary key or throws a {@link info.diit.portal.accounts.NoSuchCourseFeeException} if it could not be found.
	 *
	 * @param courseFeeId the primary key of the course fee
	 * @return the course fee
	 * @throws info.diit.portal.accounts.NoSuchCourseFeeException if a course fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseFee findByPrimaryKey(long courseFeeId)
		throws NoSuchCourseFeeException, SystemException {
		CourseFee courseFee = fetchByPrimaryKey(courseFeeId);

		if (courseFee == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + courseFeeId);
			}

			throw new NoSuchCourseFeeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				courseFeeId);
		}

		return courseFee;
	}

	/**
	 * Returns the course fee with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the course fee
	 * @return the course fee, or <code>null</code> if a course fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CourseFee fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the course fee with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param courseFeeId the primary key of the course fee
	 * @return the course fee, or <code>null</code> if a course fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseFee fetchByPrimaryKey(long courseFeeId)
		throws SystemException {
		CourseFee courseFee = (CourseFee)EntityCacheUtil.getResult(CourseFeeModelImpl.ENTITY_CACHE_ENABLED,
				CourseFeeImpl.class, courseFeeId);

		if (courseFee == _nullCourseFee) {
			return null;
		}

		if (courseFee == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				courseFee = (CourseFee)session.get(CourseFeeImpl.class,
						Long.valueOf(courseFeeId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (courseFee != null) {
					cacheResult(courseFee);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(CourseFeeModelImpl.ENTITY_CACHE_ENABLED,
						CourseFeeImpl.class, courseFeeId, _nullCourseFee);
				}

				closeSession(session);
			}
		}

		return courseFee;
	}

	/**
	 * Returns all the course fees where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @return the matching course fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseFee> findByCourse(long courseId)
		throws SystemException {
		return findByCourse(courseId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the course fees where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course ID
	 * @param start the lower bound of the range of course fees
	 * @param end the upper bound of the range of course fees (not inclusive)
	 * @return the range of matching course fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseFee> findByCourse(long courseId, int start, int end)
		throws SystemException {
		return findByCourse(courseId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the course fees where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course ID
	 * @param start the lower bound of the range of course fees
	 * @param end the upper bound of the range of course fees (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching course fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseFee> findByCourse(long courseId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSE;
			finderArgs = new Object[] { courseId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COURSE;
			finderArgs = new Object[] { courseId, start, end, orderByComparator };
		}

		List<CourseFee> list = (List<CourseFee>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CourseFee courseFee : list) {
				if ((courseId != courseFee.getCourseId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_COURSEFEE_WHERE);

			query.append(_FINDER_COLUMN_COURSE_COURSEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				list = (List<CourseFee>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first course fee in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course fee
	 * @throws info.diit.portal.accounts.NoSuchCourseFeeException if a matching course fee could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseFee findByCourse_First(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseFeeException, SystemException {
		CourseFee courseFee = fetchByCourse_First(courseId, orderByComparator);

		if (courseFee != null) {
			return courseFee;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("courseId=");
		msg.append(courseId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseFeeException(msg.toString());
	}

	/**
	 * Returns the first course fee in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course fee, or <code>null</code> if a matching course fee could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseFee fetchByCourse_First(long courseId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CourseFee> list = findByCourse(courseId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last course fee in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course fee
	 * @throws info.diit.portal.accounts.NoSuchCourseFeeException if a matching course fee could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseFee findByCourse_Last(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseFeeException, SystemException {
		CourseFee courseFee = fetchByCourse_Last(courseId, orderByComparator);

		if (courseFee != null) {
			return courseFee;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("courseId=");
		msg.append(courseId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseFeeException(msg.toString());
	}

	/**
	 * Returns the last course fee in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course fee, or <code>null</code> if a matching course fee could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseFee fetchByCourse_Last(long courseId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCourse(courseId);

		List<CourseFee> list = findByCourse(courseId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the course fees before and after the current course fee in the ordered set where courseId = &#63;.
	 *
	 * @param courseFeeId the primary key of the current course fee
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course fee
	 * @throws info.diit.portal.accounts.NoSuchCourseFeeException if a course fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseFee[] findByCourse_PrevAndNext(long courseFeeId,
		long courseId, OrderByComparator orderByComparator)
		throws NoSuchCourseFeeException, SystemException {
		CourseFee courseFee = findByPrimaryKey(courseFeeId);

		Session session = null;

		try {
			session = openSession();

			CourseFee[] array = new CourseFeeImpl[3];

			array[0] = getByCourse_PrevAndNext(session, courseFee, courseId,
					orderByComparator, true);

			array[1] = courseFee;

			array[2] = getByCourse_PrevAndNext(session, courseFee, courseId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourseFee getByCourse_PrevAndNext(Session session,
		CourseFee courseFee, long courseId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSEFEE_WHERE);

		query.append(_FINDER_COLUMN_COURSE_COURSEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(courseId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(courseFee);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CourseFee> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the course fees.
	 *
	 * @return the course fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseFee> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the course fees.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of course fees
	 * @param end the upper bound of the range of course fees (not inclusive)
	 * @return the range of course fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseFee> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the course fees.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of course fees
	 * @param end the upper bound of the range of course fees (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of course fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseFee> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CourseFee> list = (List<CourseFee>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_COURSEFEE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COURSEFEE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<CourseFee>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<CourseFee>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the course fees where courseId = &#63; from the database.
	 *
	 * @param courseId the course ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCourse(long courseId) throws SystemException {
		for (CourseFee courseFee : findByCourse(courseId)) {
			remove(courseFee);
		}
	}

	/**
	 * Removes all the course fees from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (CourseFee courseFee : findAll()) {
			remove(courseFee);
		}
	}

	/**
	 * Returns the number of course fees where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @return the number of matching course fees
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCourse(long courseId) throws SystemException {
		Object[] finderArgs = new Object[] { courseId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COURSE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSEFEE_WHERE);

			query.append(_FINDER_COLUMN_COURSE_COURSEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COURSE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of course fees.
	 *
	 * @return the number of course fees
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COURSEFEE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the course fee persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.accounts.model.CourseFee")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CourseFee>> listenersList = new ArrayList<ModelListener<CourseFee>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CourseFee>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CourseFeeImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_COURSEFEE = "SELECT courseFee FROM CourseFee courseFee";
	private static final String _SQL_SELECT_COURSEFEE_WHERE = "SELECT courseFee FROM CourseFee courseFee WHERE ";
	private static final String _SQL_COUNT_COURSEFEE = "SELECT COUNT(courseFee) FROM CourseFee courseFee";
	private static final String _SQL_COUNT_COURSEFEE_WHERE = "SELECT COUNT(courseFee) FROM CourseFee courseFee WHERE ";
	private static final String _FINDER_COLUMN_COURSE_COURSEID_2 = "courseFee.courseId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "courseFee.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CourseFee exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CourseFee exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CourseFeePersistenceImpl.class);
	private static CourseFee _nullCourseFee = new CourseFeeImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CourseFee> toCacheModel() {
				return _nullCourseFeeCacheModel;
			}
		};

	private static CacheModel<CourseFee> _nullCourseFeeCacheModel = new CacheModel<CourseFee>() {
			public CourseFee toEntityModel() {
				return _nullCourseFee;
			}
		};
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.impl;

import info.diit.portal.accounts.model.CourseFee;
import info.diit.portal.accounts.service.CourseFeeLocalServiceUtil;
import info.diit.portal.accounts.service.base.CourseFeeLocalServiceBaseImpl;
import info.diit.portal.accounts.service.persistence.CourseFeeUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the course fee local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.accounts.service.CourseFeeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author shamsuddin
 * @see info.diit.portal.accounts.service.base.CourseFeeLocalServiceBaseImpl
 * @see info.diit.portal.accounts.service.CourseFeeLocalServiceUtil
 */
public class CourseFeeLocalServiceImpl extends CourseFeeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.accounts.service.CourseFeeLocalServiceUtil} to access the course fee local service.
	 */
	
	public List<CourseFee> getCourseFeeList(long courseId) throws SystemException
	{
		return CourseFeeUtil.findByCourse(courseId);
	}
	
	public void deleteAllCourseFees(long courseId) throws SystemException
	{
		List<CourseFee> courseFees = CourseFeeLocalServiceUtil.getCourseFeeList(courseId);
		
		for(CourseFee courseFee:courseFees)
		{
			CourseFeeLocalServiceUtil.deleteCourseFee(courseFee);
		}
	}
}
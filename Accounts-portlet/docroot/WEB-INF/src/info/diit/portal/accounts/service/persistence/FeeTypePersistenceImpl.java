/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.accounts.NoSuchFeeTypeException;
import info.diit.portal.accounts.model.FeeType;
import info.diit.portal.accounts.model.impl.FeeTypeImpl;
import info.diit.portal.accounts.model.impl.FeeTypeModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the fee type service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shamsuddin
 * @see FeeTypePersistence
 * @see FeeTypeUtil
 * @generated
 */
public class FeeTypePersistenceImpl extends BasePersistenceImpl<FeeType>
	implements FeeTypePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link FeeTypeUtil} to access the fee type persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = FeeTypeImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TYPENAME = new FinderPath(FeeTypeModelImpl.ENTITY_CACHE_ENABLED,
			FeeTypeModelImpl.FINDER_CACHE_ENABLED, FeeTypeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTypeName",
			new String[] {
				String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPENAME =
		new FinderPath(FeeTypeModelImpl.ENTITY_CACHE_ENABLED,
			FeeTypeModelImpl.FINDER_CACHE_ENABLED, FeeTypeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTypeName",
			new String[] { String.class.getName() },
			FeeTypeModelImpl.TYPENAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TYPENAME = new FinderPath(FeeTypeModelImpl.ENTITY_CACHE_ENABLED,
			FeeTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTypeName",
			new String[] { String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(FeeTypeModelImpl.ENTITY_CACHE_ENABLED,
			FeeTypeModelImpl.FINDER_CACHE_ENABLED, FeeTypeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(FeeTypeModelImpl.ENTITY_CACHE_ENABLED,
			FeeTypeModelImpl.FINDER_CACHE_ENABLED, FeeTypeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(FeeTypeModelImpl.ENTITY_CACHE_ENABLED,
			FeeTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the fee type in the entity cache if it is enabled.
	 *
	 * @param feeType the fee type
	 */
	public void cacheResult(FeeType feeType) {
		EntityCacheUtil.putResult(FeeTypeModelImpl.ENTITY_CACHE_ENABLED,
			FeeTypeImpl.class, feeType.getPrimaryKey(), feeType);

		feeType.resetOriginalValues();
	}

	/**
	 * Caches the fee types in the entity cache if it is enabled.
	 *
	 * @param feeTypes the fee types
	 */
	public void cacheResult(List<FeeType> feeTypes) {
		for (FeeType feeType : feeTypes) {
			if (EntityCacheUtil.getResult(
						FeeTypeModelImpl.ENTITY_CACHE_ENABLED,
						FeeTypeImpl.class, feeType.getPrimaryKey()) == null) {
				cacheResult(feeType);
			}
			else {
				feeType.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all fee types.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(FeeTypeImpl.class.getName());
		}

		EntityCacheUtil.clearCache(FeeTypeImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the fee type.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(FeeType feeType) {
		EntityCacheUtil.removeResult(FeeTypeModelImpl.ENTITY_CACHE_ENABLED,
			FeeTypeImpl.class, feeType.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<FeeType> feeTypes) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (FeeType feeType : feeTypes) {
			EntityCacheUtil.removeResult(FeeTypeModelImpl.ENTITY_CACHE_ENABLED,
				FeeTypeImpl.class, feeType.getPrimaryKey());
		}
	}

	/**
	 * Creates a new fee type with the primary key. Does not add the fee type to the database.
	 *
	 * @param feeTypeId the primary key for the new fee type
	 * @return the new fee type
	 */
	public FeeType create(long feeTypeId) {
		FeeType feeType = new FeeTypeImpl();

		feeType.setNew(true);
		feeType.setPrimaryKey(feeTypeId);

		return feeType;
	}

	/**
	 * Removes the fee type with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param feeTypeId the primary key of the fee type
	 * @return the fee type that was removed
	 * @throws info.diit.portal.accounts.NoSuchFeeTypeException if a fee type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public FeeType remove(long feeTypeId)
		throws NoSuchFeeTypeException, SystemException {
		return remove(Long.valueOf(feeTypeId));
	}

	/**
	 * Removes the fee type with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the fee type
	 * @return the fee type that was removed
	 * @throws info.diit.portal.accounts.NoSuchFeeTypeException if a fee type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FeeType remove(Serializable primaryKey)
		throws NoSuchFeeTypeException, SystemException {
		Session session = null;

		try {
			session = openSession();

			FeeType feeType = (FeeType)session.get(FeeTypeImpl.class, primaryKey);

			if (feeType == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchFeeTypeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(feeType);
		}
		catch (NoSuchFeeTypeException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected FeeType removeImpl(FeeType feeType) throws SystemException {
		feeType = toUnwrappedModel(feeType);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, feeType);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(feeType);

		return feeType;
	}

	@Override
	public FeeType updateImpl(info.diit.portal.accounts.model.FeeType feeType,
		boolean merge) throws SystemException {
		feeType = toUnwrappedModel(feeType);

		boolean isNew = feeType.isNew();

		FeeTypeModelImpl feeTypeModelImpl = (FeeTypeModelImpl)feeType;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, feeType, merge);

			feeType.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !FeeTypeModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((feeTypeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPENAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						feeTypeModelImpl.getOriginalTypeName()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TYPENAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPENAME,
					args);

				args = new Object[] { feeTypeModelImpl.getTypeName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TYPENAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPENAME,
					args);
			}
		}

		EntityCacheUtil.putResult(FeeTypeModelImpl.ENTITY_CACHE_ENABLED,
			FeeTypeImpl.class, feeType.getPrimaryKey(), feeType);

		return feeType;
	}

	protected FeeType toUnwrappedModel(FeeType feeType) {
		if (feeType instanceof FeeTypeImpl) {
			return feeType;
		}

		FeeTypeImpl feeTypeImpl = new FeeTypeImpl();

		feeTypeImpl.setNew(feeType.isNew());
		feeTypeImpl.setPrimaryKey(feeType.getPrimaryKey());

		feeTypeImpl.setFeeTypeId(feeType.getFeeTypeId());
		feeTypeImpl.setCompanyId(feeType.getCompanyId());
		feeTypeImpl.setUserId(feeType.getUserId());
		feeTypeImpl.setUserName(feeType.getUserName());
		feeTypeImpl.setCreateDate(feeType.getCreateDate());
		feeTypeImpl.setModifiedDate(feeType.getModifiedDate());
		feeTypeImpl.setTypeName(feeType.getTypeName());
		feeTypeImpl.setDescription(feeType.getDescription());

		return feeTypeImpl;
	}

	/**
	 * Returns the fee type with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the fee type
	 * @return the fee type
	 * @throws com.liferay.portal.NoSuchModelException if a fee type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FeeType findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the fee type with the primary key or throws a {@link info.diit.portal.accounts.NoSuchFeeTypeException} if it could not be found.
	 *
	 * @param feeTypeId the primary key of the fee type
	 * @return the fee type
	 * @throws info.diit.portal.accounts.NoSuchFeeTypeException if a fee type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public FeeType findByPrimaryKey(long feeTypeId)
		throws NoSuchFeeTypeException, SystemException {
		FeeType feeType = fetchByPrimaryKey(feeTypeId);

		if (feeType == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + feeTypeId);
			}

			throw new NoSuchFeeTypeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				feeTypeId);
		}

		return feeType;
	}

	/**
	 * Returns the fee type with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the fee type
	 * @return the fee type, or <code>null</code> if a fee type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FeeType fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the fee type with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param feeTypeId the primary key of the fee type
	 * @return the fee type, or <code>null</code> if a fee type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public FeeType fetchByPrimaryKey(long feeTypeId) throws SystemException {
		FeeType feeType = (FeeType)EntityCacheUtil.getResult(FeeTypeModelImpl.ENTITY_CACHE_ENABLED,
				FeeTypeImpl.class, feeTypeId);

		if (feeType == _nullFeeType) {
			return null;
		}

		if (feeType == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				feeType = (FeeType)session.get(FeeTypeImpl.class,
						Long.valueOf(feeTypeId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (feeType != null) {
					cacheResult(feeType);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(FeeTypeModelImpl.ENTITY_CACHE_ENABLED,
						FeeTypeImpl.class, feeTypeId, _nullFeeType);
				}

				closeSession(session);
			}
		}

		return feeType;
	}

	/**
	 * Returns all the fee types where typeName = &#63;.
	 *
	 * @param typeName the type name
	 * @return the matching fee types
	 * @throws SystemException if a system exception occurred
	 */
	public List<FeeType> findByTypeName(String typeName)
		throws SystemException {
		return findByTypeName(typeName, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the fee types where typeName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param typeName the type name
	 * @param start the lower bound of the range of fee types
	 * @param end the upper bound of the range of fee types (not inclusive)
	 * @return the range of matching fee types
	 * @throws SystemException if a system exception occurred
	 */
	public List<FeeType> findByTypeName(String typeName, int start, int end)
		throws SystemException {
		return findByTypeName(typeName, start, end, null);
	}

	/**
	 * Returns an ordered range of all the fee types where typeName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param typeName the type name
	 * @param start the lower bound of the range of fee types
	 * @param end the upper bound of the range of fee types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching fee types
	 * @throws SystemException if a system exception occurred
	 */
	public List<FeeType> findByTypeName(String typeName, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPENAME;
			finderArgs = new Object[] { typeName };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TYPENAME;
			finderArgs = new Object[] { typeName, start, end, orderByComparator };
		}

		List<FeeType> list = (List<FeeType>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (FeeType feeType : list) {
				if (!Validator.equals(typeName, feeType.getTypeName())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_FEETYPE_WHERE);

			if (typeName == null) {
				query.append(_FINDER_COLUMN_TYPENAME_TYPENAME_1);
			}
			else {
				if (typeName.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_TYPENAME_TYPENAME_3);
				}
				else {
					query.append(_FINDER_COLUMN_TYPENAME_TYPENAME_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(FeeTypeModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (typeName != null) {
					qPos.add(typeName);
				}

				list = (List<FeeType>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first fee type in the ordered set where typeName = &#63;.
	 *
	 * @param typeName the type name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching fee type
	 * @throws info.diit.portal.accounts.NoSuchFeeTypeException if a matching fee type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public FeeType findByTypeName_First(String typeName,
		OrderByComparator orderByComparator)
		throws NoSuchFeeTypeException, SystemException {
		FeeType feeType = fetchByTypeName_First(typeName, orderByComparator);

		if (feeType != null) {
			return feeType;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("typeName=");
		msg.append(typeName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchFeeTypeException(msg.toString());
	}

	/**
	 * Returns the first fee type in the ordered set where typeName = &#63;.
	 *
	 * @param typeName the type name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching fee type, or <code>null</code> if a matching fee type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public FeeType fetchByTypeName_First(String typeName,
		OrderByComparator orderByComparator) throws SystemException {
		List<FeeType> list = findByTypeName(typeName, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last fee type in the ordered set where typeName = &#63;.
	 *
	 * @param typeName the type name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching fee type
	 * @throws info.diit.portal.accounts.NoSuchFeeTypeException if a matching fee type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public FeeType findByTypeName_Last(String typeName,
		OrderByComparator orderByComparator)
		throws NoSuchFeeTypeException, SystemException {
		FeeType feeType = fetchByTypeName_Last(typeName, orderByComparator);

		if (feeType != null) {
			return feeType;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("typeName=");
		msg.append(typeName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchFeeTypeException(msg.toString());
	}

	/**
	 * Returns the last fee type in the ordered set where typeName = &#63;.
	 *
	 * @param typeName the type name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching fee type, or <code>null</code> if a matching fee type could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public FeeType fetchByTypeName_Last(String typeName,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByTypeName(typeName);

		List<FeeType> list = findByTypeName(typeName, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the fee types before and after the current fee type in the ordered set where typeName = &#63;.
	 *
	 * @param feeTypeId the primary key of the current fee type
	 * @param typeName the type name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next fee type
	 * @throws info.diit.portal.accounts.NoSuchFeeTypeException if a fee type with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public FeeType[] findByTypeName_PrevAndNext(long feeTypeId,
		String typeName, OrderByComparator orderByComparator)
		throws NoSuchFeeTypeException, SystemException {
		FeeType feeType = findByPrimaryKey(feeTypeId);

		Session session = null;

		try {
			session = openSession();

			FeeType[] array = new FeeTypeImpl[3];

			array[0] = getByTypeName_PrevAndNext(session, feeType, typeName,
					orderByComparator, true);

			array[1] = feeType;

			array[2] = getByTypeName_PrevAndNext(session, feeType, typeName,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected FeeType getByTypeName_PrevAndNext(Session session,
		FeeType feeType, String typeName, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_FEETYPE_WHERE);

		if (typeName == null) {
			query.append(_FINDER_COLUMN_TYPENAME_TYPENAME_1);
		}
		else {
			if (typeName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TYPENAME_TYPENAME_3);
			}
			else {
				query.append(_FINDER_COLUMN_TYPENAME_TYPENAME_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(FeeTypeModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (typeName != null) {
			qPos.add(typeName);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(feeType);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<FeeType> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the fee types.
	 *
	 * @return the fee types
	 * @throws SystemException if a system exception occurred
	 */
	public List<FeeType> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the fee types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of fee types
	 * @param end the upper bound of the range of fee types (not inclusive)
	 * @return the range of fee types
	 * @throws SystemException if a system exception occurred
	 */
	public List<FeeType> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the fee types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of fee types
	 * @param end the upper bound of the range of fee types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of fee types
	 * @throws SystemException if a system exception occurred
	 */
	public List<FeeType> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<FeeType> list = (List<FeeType>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_FEETYPE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_FEETYPE.concat(FeeTypeModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<FeeType>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<FeeType>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the fee types where typeName = &#63; from the database.
	 *
	 * @param typeName the type name
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByTypeName(String typeName) throws SystemException {
		for (FeeType feeType : findByTypeName(typeName)) {
			remove(feeType);
		}
	}

	/**
	 * Removes all the fee types from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (FeeType feeType : findAll()) {
			remove(feeType);
		}
	}

	/**
	 * Returns the number of fee types where typeName = &#63;.
	 *
	 * @param typeName the type name
	 * @return the number of matching fee types
	 * @throws SystemException if a system exception occurred
	 */
	public int countByTypeName(String typeName) throws SystemException {
		Object[] finderArgs = new Object[] { typeName };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_TYPENAME,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_FEETYPE_WHERE);

			if (typeName == null) {
				query.append(_FINDER_COLUMN_TYPENAME_TYPENAME_1);
			}
			else {
				if (typeName.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_TYPENAME_TYPENAME_3);
				}
				else {
					query.append(_FINDER_COLUMN_TYPENAME_TYPENAME_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (typeName != null) {
					qPos.add(typeName);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TYPENAME,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of fee types.
	 *
	 * @return the number of fee types
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_FEETYPE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the fee type persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.accounts.model.FeeType")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<FeeType>> listenersList = new ArrayList<ModelListener<FeeType>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<FeeType>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(FeeTypeImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_FEETYPE = "SELECT feeType FROM FeeType feeType";
	private static final String _SQL_SELECT_FEETYPE_WHERE = "SELECT feeType FROM FeeType feeType WHERE ";
	private static final String _SQL_COUNT_FEETYPE = "SELECT COUNT(feeType) FROM FeeType feeType";
	private static final String _SQL_COUNT_FEETYPE_WHERE = "SELECT COUNT(feeType) FROM FeeType feeType WHERE ";
	private static final String _FINDER_COLUMN_TYPENAME_TYPENAME_1 = "feeType.typeName IS NULL";
	private static final String _FINDER_COLUMN_TYPENAME_TYPENAME_2 = "feeType.typeName = ?";
	private static final String _FINDER_COLUMN_TYPENAME_TYPENAME_3 = "(feeType.typeName IS NULL OR feeType.typeName = ?)";
	private static final String _ORDER_BY_ENTITY_ALIAS = "feeType.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No FeeType exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No FeeType exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(FeeTypePersistenceImpl.class);
	private static FeeType _nullFeeType = new FeeTypeImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<FeeType> toCacheModel() {
				return _nullFeeTypeCacheModel;
			}
		};

	private static CacheModel<FeeType> _nullFeeTypeCacheModel = new CacheModel<FeeType>() {
			public FeeType toEntityModel() {
				return _nullFeeType;
			}
		};
}
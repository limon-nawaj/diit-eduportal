package info.diit.portal.accounts.dao;

import info.diit.portal.accounts.dto.BatchDto;
import info.diit.portal.accounts.dto.CourseDto;
import info.diit.portal.accounts.dto.FeesDto;
import info.diit.portal.accounts.dto.PaymentDto;
import info.diit.portal.accounts.dto.ScheduleDto;
import info.diit.portal.accounts.dto.StudentDto;
import info.diit.portal.accounts.model.BatchFee;
import info.diit.portal.accounts.model.BatchPaymentSchedule;
import info.diit.portal.accounts.model.FeeType;
import info.diit.portal.accounts.model.Payment;
import info.diit.portal.accounts.model.StudentFee;
import info.diit.portal.accounts.model.StudentPaymentSchedule;
import info.diit.portal.accounts.service.BatchFeeLocalServiceUtil;
import info.diit.portal.accounts.service.BatchPaymentScheduleLocalServiceUtil;
import info.diit.portal.accounts.service.FeeTypeLocalServiceUtil;
import info.diit.portal.accounts.service.PaymentLocalServiceUtil;
import info.diit.portal.accounts.service.StudentFeeLocalServiceUtil;
import info.diit.portal.accounts.service.StudentPaymentScheduleLocalServiceUtil;
import info.diit.portal.accounts.service.persistence.PaymentUtil;
import info.diit.portal.batch.model.Batch;
import info.diit.portal.batch.service.BatchLocalServiceUtil;
import info.diit.portal.coursesubject.model.Course;
import info.diit.portal.coursesubject.model.CourseOrganization;
import info.diit.portal.coursesubject.service.CourseLocalServiceUtil;
import info.diit.portal.coursesubject.service.CourseOrganizationLocalServiceUtil;
import info.diit.portal.student.service.model.BatchStudent;
import info.diit.portal.student.service.model.Student;
import info.diit.portal.student.service.service.BatchStudentLocalServiceUtil;
import info.diit.portal.student.service.service.StudentLocalServiceUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

public class AccountsDao {

	public static ArrayList<CourseDto> getCourseListByCompany(long companyId) throws SystemException
	{
		List<Course> courses = CourseLocalServiceUtil.findByCompany(companyId);
		ArrayList<CourseDto> courseList = new ArrayList<CourseDto>(courses.size());
		
		for(Course course:courses)
		{
			CourseDto courseDto = new CourseDto();
			courseDto.setCourseCode(course.getCourseCode());
			courseDto.setCourseId(course.getCourseId());
			courseDto.setCourseName(course.getCourseName());
			
			courseList.add(courseDto);
		}
		
		return courseList;
	}
	
	public static ArrayList<CourseDto> getCourseListByOrganization(long organizationId) throws SystemException
	{
		List<CourseOrganization> courseOrganizations = CourseOrganizationLocalServiceUtil.findByOrganization(organizationId);
		
		ArrayList<CourseDto> courseList = new ArrayList<CourseDto>(courseOrganizations.size());
		
		for(CourseOrganization courseOrganization:courseOrganizations)
		{
			long courseId = courseOrganization.getCourseId();
			Course course = CourseLocalServiceUtil.fetchCourse(courseId);
			
			if(course!=null)
			{
				CourseDto courseDto = new CourseDto();
				courseDto.setCourseId(course.getCourseId());
				courseDto.setCourseCode(course.getCourseCode());
				courseDto.setCourseName(course.getCourseName());
				
				courseList.add(courseDto);
			}
			
		}
		
		return courseList;
		
	}
	
	public static ArrayList<BatchDto> getBatchListByCourse(long organizationId,long courseId) throws SystemException
	{
		List<Batch> batchList = BatchLocalServiceUtil.findBatchesByOrgCourseId(organizationId, courseId);
		ArrayList<BatchDto> batchDtoList = new ArrayList<BatchDto>(batchList.size());
		
		for(Batch batch:batchList)
		{
			BatchDto batchDto = new BatchDto();
			batchDto.setBatchId(batch.getBatchId());
			batchDto.setBatchName(batch.getBatchName());
			
			batchDtoList.add(batchDto);
		}
		
		return batchDtoList;
	}
	
	public static ArrayList<FeesDto> getBatchFeesList(long batchId) throws SystemException, PortalException
	{
		List<BatchFee> batchFeeList = BatchFeeLocalServiceUtil.getBatchFeesList(batchId);
		ArrayList<FeesDto> batchFees = new ArrayList<FeesDto>(batchFeeList.size());
		
		for(BatchFee batchFee:batchFeeList)
		{
			FeesDto feesDto = new FeesDto();
			feesDto.setAmount(Math.round(batchFee.getAmount()));
			feesDto.setDescription(feesDto.getDescription());
			
			FeeType feeType = FeeTypeLocalServiceUtil.getFeeType(batchFee.getFeeTypeId());
			
			if(feeType!=null)
			{
				feesDto.setFeeType(feeType.getTypeName());
				feesDto.setId(feeType.getFeeTypeId());
			}
			
			batchFees.add(feesDto);
			
		}
		
		return batchFees;
		
	}
	
	public static ArrayList<ScheduleDto> getBatchPaymentSchedules(long batchId) throws SystemException, PortalException
	{
		ArrayList<ScheduleDto> batchPaymentScheduleDtos = new ArrayList<ScheduleDto>();
		
		List<BatchPaymentSchedule> batchPaymentSchedules = BatchPaymentScheduleLocalServiceUtil.findBatchPaymentScheduleByBatch(batchId);
		
		for(BatchPaymentSchedule batchPaymentSchedule:batchPaymentSchedules)
		{
			ScheduleDto scheduleDto = new ScheduleDto();
			scheduleDto.setAmount(Math.round(batchPaymentSchedule.getAmount()));
			
			FeeType feeType = FeeTypeLocalServiceUtil.getFeeType(batchPaymentSchedule.getFeeTypeId());
			
			if(feeType!=null)
			{
				scheduleDto.setFeeType(feeType.getTypeName());
				scheduleDto.setId(feeType.getFeeTypeId());
			}
			
			scheduleDto.setScheduleDate(batchPaymentSchedule.getScheduleDate());
			
			batchPaymentScheduleDtos.add(scheduleDto);
		}
		
		return batchPaymentScheduleDtos;
	}
	
	public static ArrayList<BatchDto> getBatchListByStudent(long studentId) throws SystemException
	{
		List<BatchStudent> studentBatchList = BatchStudentLocalServiceUtil.findBystduentBatchStudentList(studentId);
		
		ArrayList<BatchDto> batchDtoList = new ArrayList<BatchDto>();
		
		for(BatchStudent batchStudent:studentBatchList)
		{
			long batchId = batchStudent.getBatchId();
			Batch batch = BatchLocalServiceUtil.fetchBatch(batchId);
			BatchDto batchDto = new BatchDto();
			batchDto.setBatchId(batch.getBatchId());
			batchDto.setBatchName(batch.getBatchName());
			
			batchDtoList.add(batchDto);
		}
		
		return batchDtoList;
	}
	
	public static StudentDto getStudentDto(long studentId) throws SystemException
	{
		Student student = StudentLocalServiceUtil.fetchStudent(studentId);
		StudentDto studentDto = null;
		
		if(student!=null)
		{
			studentDto = new StudentDto();
			studentDto.setStudnetId(student.getStudentId());
			studentDto.setName(student.getName());
			
			ArrayList<BatchDto> batchList = getBatchListByStudent(studentId);
			
			for(BatchDto batchDto:batchList)
			{
				studentDto.getBatchList().add(batchDto);
			}
			
			
		}
		
		return studentDto;
	}
	
	
	
	public static ArrayList<FeesDto> getStudentFeesList(long studentId,long batchId) throws SystemException, PortalException
	{
		List<StudentFee> studentFeeList = StudentFeeLocalServiceUtil.getStudentFeeList(studentId, batchId);
		ArrayList<FeesDto> studentFees = new ArrayList<FeesDto>(studentFeeList.size());
		
		for(StudentFee studentFee:studentFeeList)
		{
			FeesDto feesDto = new FeesDto();
			feesDto.setAmount(Math.round(studentFee.getAmount()));
			feesDto.setDescription(feesDto.getDescription());
			
			FeeType feeType = FeeTypeLocalServiceUtil.getFeeType(studentFee.getFeeTypeId());
			
			if(feeType!=null)
			{
				feesDto.setFeeType(feeType.getTypeName());
				feesDto.setId(feeType.getFeeTypeId());
			}
			
			studentFees.add(feesDto);
			
		}
		
		return studentFees;
		
	}

	
	public static ArrayList<ScheduleDto> getStudentPaymentSchedules(long studentId, long batchId) throws SystemException, PortalException
	{
		ArrayList<ScheduleDto> studentPaymentScheduleDtos = new ArrayList<ScheduleDto>();
		
		List<StudentPaymentSchedule> studentPaymentSchedules = StudentPaymentScheduleLocalServiceUtil.getStudentPaymentScheduleList(studentId, batchId);
		
		for(StudentPaymentSchedule studentPaymentSchedule:studentPaymentSchedules)
		{
			ScheduleDto scheduleDto = new ScheduleDto();
			scheduleDto.setAmount(Math.round(studentPaymentSchedule.getAmount()));
			
			FeeType feeType = FeeTypeLocalServiceUtil.getFeeType(studentPaymentSchedule.getFeeTypeId());
			
			if(feeType!=null)
			{
				scheduleDto.setFeeType(feeType.getTypeName());
				scheduleDto.setId(feeType.getFeeTypeId());
			}
			
			scheduleDto.setScheduleDate(studentPaymentSchedule.getScheduleDate());
			
			studentPaymentScheduleDtos.add(scheduleDto);
		}
		
		return studentPaymentScheduleDtos;
	}
 
	public static double getTotalPaidAmount(long studentId,long batchId) throws SystemException
	{
		double total = 0;

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Payment.class)
				.add(PropertyFactoryUtil.forName("studentId").eq(new Long(studentId)))
				.add(PropertyFactoryUtil.forName("batchId").eq(new Long(batchId)))
				.setProjection(ProjectionFactoryUtil.sum("amount"));
		
		List result = PaymentLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		if(result != null)
		{
			if(result.get(0)!=null)
			{
				total = ((Double)result.get(0)).doubleValue();
			}
		}
		
		return total;
	}
	
	public static double getTotalDueAmount(long studentId,long batchId,Date date) throws SystemException, PortalException
	{
		List result = null;
		
		if(getStudentPaymentSchedules(studentId, batchId).size()>0)
		{
			DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(StudentPaymentSchedule.class)
					.add(PropertyFactoryUtil.forName("studentId").eq(new Long(studentId)))
					.add(PropertyFactoryUtil.forName("batchId").eq(new Long(batchId)))
					.add(PropertyFactoryUtil.forName("scheduleDate").between(new Date(Long.MIN_VALUE), date))
					.setProjection(ProjectionFactoryUtil.sum("amount"));
			result = StudentPaymentScheduleLocalServiceUtil.dynamicQuery(dynamicQuery);
		}
		else
		{
			DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(BatchPaymentSchedule.class)
					.add(PropertyFactoryUtil.forName("batchId").eq(new Long(batchId)))
					.add(PropertyFactoryUtil.forName("scheduleDate").between(new Date(Long.MIN_VALUE), date))
					.setProjection(ProjectionFactoryUtil.sum("amount"));
			
			result = BatchPaymentScheduleLocalServiceUtil.dynamicQuery(dynamicQuery);
		}
		
		if(result != null)
		{
			if(result.get(0)!=null)
			{
				double totalPayable = (Double)result.get(0);
				double totalPaid = getTotalPaidAmount(studentId, batchId);
				
				return (totalPayable-totalPaid);
			}
		}
		
		return 0;
	}
	
	
	public static void savePaymentList(ArrayList<Payment> paymentList) throws SystemException
	{
		for(Payment payment:paymentList)
		{
			PaymentLocalServiceUtil.addPayment(payment);
		}
	}
	
	public static ArrayList<PaymentDto> getPaymentList(long studentId,long batchId) throws SystemException, PortalException
	{
		ArrayList<PaymentDto> paymentList = new ArrayList<PaymentDto>();
		
		List<Payment> payments = PaymentLocalServiceUtil.findByStudentBatch(studentId, batchId);
		
		for(Payment payment:payments)
		{
			PaymentDto paymentDto = new PaymentDto();
			paymentDto.setAmount(payment.getAmount());
			paymentDto.setBatchId(payment.getBatchId());
			paymentDto.setFeeTypeId(payment.getFeeTypeId());
			
			FeeType feeType = FeeTypeLocalServiceUtil.getFeeType(payment.getFeeTypeId());
			
			if(feeType!=null)
			{
				paymentDto.setFeeTypeId(feeType.getFeeTypeId());
				paymentDto.setFeeType(feeType.getTypeName());
			}
			
			paymentDto.setPaymentDate(payment.getPaymentDate());
			paymentDto.setPaymentId(payment.getPaymentId());
			paymentDto.setStudentId(payment.getStudentId());
			
			paymentList.add(paymentDto);
			
		}
		
		return paymentList;
	}
	
}

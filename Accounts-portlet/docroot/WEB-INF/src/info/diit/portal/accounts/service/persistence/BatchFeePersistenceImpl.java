/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.accounts.NoSuchBatchFeeException;
import info.diit.portal.accounts.model.BatchFee;
import info.diit.portal.accounts.model.impl.BatchFeeImpl;
import info.diit.portal.accounts.model.impl.BatchFeeModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the batch fee service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shamsuddin
 * @see BatchFeePersistence
 * @see BatchFeeUtil
 * @generated
 */
public class BatchFeePersistenceImpl extends BasePersistenceImpl<BatchFee>
	implements BatchFeePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link BatchFeeUtil} to access the batch fee persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = BatchFeeImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCH = new FinderPath(BatchFeeModelImpl.ENTITY_CACHE_ENABLED,
			BatchFeeModelImpl.FINDER_CACHE_ENABLED, BatchFeeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBatch",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH = new FinderPath(BatchFeeModelImpl.ENTITY_CACHE_ENABLED,
			BatchFeeModelImpl.FINDER_CACHE_ENABLED, BatchFeeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBatch",
			new String[] { Long.class.getName() },
			BatchFeeModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCH = new FinderPath(BatchFeeModelImpl.ENTITY_CACHE_ENABLED,
			BatchFeeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBatch",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BatchFeeModelImpl.ENTITY_CACHE_ENABLED,
			BatchFeeModelImpl.FINDER_CACHE_ENABLED, BatchFeeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BatchFeeModelImpl.ENTITY_CACHE_ENABLED,
			BatchFeeModelImpl.FINDER_CACHE_ENABLED, BatchFeeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BatchFeeModelImpl.ENTITY_CACHE_ENABLED,
			BatchFeeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the batch fee in the entity cache if it is enabled.
	 *
	 * @param batchFee the batch fee
	 */
	public void cacheResult(BatchFee batchFee) {
		EntityCacheUtil.putResult(BatchFeeModelImpl.ENTITY_CACHE_ENABLED,
			BatchFeeImpl.class, batchFee.getPrimaryKey(), batchFee);

		batchFee.resetOriginalValues();
	}

	/**
	 * Caches the batch fees in the entity cache if it is enabled.
	 *
	 * @param batchFees the batch fees
	 */
	public void cacheResult(List<BatchFee> batchFees) {
		for (BatchFee batchFee : batchFees) {
			if (EntityCacheUtil.getResult(
						BatchFeeModelImpl.ENTITY_CACHE_ENABLED,
						BatchFeeImpl.class, batchFee.getPrimaryKey()) == null) {
				cacheResult(batchFee);
			}
			else {
				batchFee.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all batch fees.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(BatchFeeImpl.class.getName());
		}

		EntityCacheUtil.clearCache(BatchFeeImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the batch fee.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(BatchFee batchFee) {
		EntityCacheUtil.removeResult(BatchFeeModelImpl.ENTITY_CACHE_ENABLED,
			BatchFeeImpl.class, batchFee.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<BatchFee> batchFees) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (BatchFee batchFee : batchFees) {
			EntityCacheUtil.removeResult(BatchFeeModelImpl.ENTITY_CACHE_ENABLED,
				BatchFeeImpl.class, batchFee.getPrimaryKey());
		}
	}

	/**
	 * Creates a new batch fee with the primary key. Does not add the batch fee to the database.
	 *
	 * @param batchFeeId the primary key for the new batch fee
	 * @return the new batch fee
	 */
	public BatchFee create(long batchFeeId) {
		BatchFee batchFee = new BatchFeeImpl();

		batchFee.setNew(true);
		batchFee.setPrimaryKey(batchFeeId);

		return batchFee;
	}

	/**
	 * Removes the batch fee with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param batchFeeId the primary key of the batch fee
	 * @return the batch fee that was removed
	 * @throws info.diit.portal.accounts.NoSuchBatchFeeException if a batch fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchFee remove(long batchFeeId)
		throws NoSuchBatchFeeException, SystemException {
		return remove(Long.valueOf(batchFeeId));
	}

	/**
	 * Removes the batch fee with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the batch fee
	 * @return the batch fee that was removed
	 * @throws info.diit.portal.accounts.NoSuchBatchFeeException if a batch fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchFee remove(Serializable primaryKey)
		throws NoSuchBatchFeeException, SystemException {
		Session session = null;

		try {
			session = openSession();

			BatchFee batchFee = (BatchFee)session.get(BatchFeeImpl.class,
					primaryKey);

			if (batchFee == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchBatchFeeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(batchFee);
		}
		catch (NoSuchBatchFeeException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected BatchFee removeImpl(BatchFee batchFee) throws SystemException {
		batchFee = toUnwrappedModel(batchFee);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, batchFee);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(batchFee);

		return batchFee;
	}

	@Override
	public BatchFee updateImpl(
		info.diit.portal.accounts.model.BatchFee batchFee, boolean merge)
		throws SystemException {
		batchFee = toUnwrappedModel(batchFee);

		boolean isNew = batchFee.isNew();

		BatchFeeModelImpl batchFeeModelImpl = (BatchFeeModelImpl)batchFee;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, batchFee, merge);

			batchFee.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !BatchFeeModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((batchFeeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchFeeModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCH, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH,
					args);

				args = new Object[] { Long.valueOf(batchFeeModelImpl.getBatchId()) };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCH, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH,
					args);
			}
		}

		EntityCacheUtil.putResult(BatchFeeModelImpl.ENTITY_CACHE_ENABLED,
			BatchFeeImpl.class, batchFee.getPrimaryKey(), batchFee);

		return batchFee;
	}

	protected BatchFee toUnwrappedModel(BatchFee batchFee) {
		if (batchFee instanceof BatchFeeImpl) {
			return batchFee;
		}

		BatchFeeImpl batchFeeImpl = new BatchFeeImpl();

		batchFeeImpl.setNew(batchFee.isNew());
		batchFeeImpl.setPrimaryKey(batchFee.getPrimaryKey());

		batchFeeImpl.setBatchFeeId(batchFee.getBatchFeeId());
		batchFeeImpl.setCompanyId(batchFee.getCompanyId());
		batchFeeImpl.setUserId(batchFee.getUserId());
		batchFeeImpl.setCreateDate(batchFee.getCreateDate());
		batchFeeImpl.setModifiedDate(batchFee.getModifiedDate());
		batchFeeImpl.setBatchId(batchFee.getBatchId());
		batchFeeImpl.setFeeTypeId(batchFee.getFeeTypeId());
		batchFeeImpl.setAmount(batchFee.getAmount());

		return batchFeeImpl;
	}

	/**
	 * Returns the batch fee with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the batch fee
	 * @return the batch fee
	 * @throws com.liferay.portal.NoSuchModelException if a batch fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchFee findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the batch fee with the primary key or throws a {@link info.diit.portal.accounts.NoSuchBatchFeeException} if it could not be found.
	 *
	 * @param batchFeeId the primary key of the batch fee
	 * @return the batch fee
	 * @throws info.diit.portal.accounts.NoSuchBatchFeeException if a batch fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchFee findByPrimaryKey(long batchFeeId)
		throws NoSuchBatchFeeException, SystemException {
		BatchFee batchFee = fetchByPrimaryKey(batchFeeId);

		if (batchFee == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + batchFeeId);
			}

			throw new NoSuchBatchFeeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				batchFeeId);
		}

		return batchFee;
	}

	/**
	 * Returns the batch fee with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the batch fee
	 * @return the batch fee, or <code>null</code> if a batch fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchFee fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the batch fee with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param batchFeeId the primary key of the batch fee
	 * @return the batch fee, or <code>null</code> if a batch fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchFee fetchByPrimaryKey(long batchFeeId)
		throws SystemException {
		BatchFee batchFee = (BatchFee)EntityCacheUtil.getResult(BatchFeeModelImpl.ENTITY_CACHE_ENABLED,
				BatchFeeImpl.class, batchFeeId);

		if (batchFee == _nullBatchFee) {
			return null;
		}

		if (batchFee == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				batchFee = (BatchFee)session.get(BatchFeeImpl.class,
						Long.valueOf(batchFeeId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (batchFee != null) {
					cacheResult(batchFee);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(BatchFeeModelImpl.ENTITY_CACHE_ENABLED,
						BatchFeeImpl.class, batchFeeId, _nullBatchFee);
				}

				closeSession(session);
			}
		}

		return batchFee;
	}

	/**
	 * Returns all the batch fees where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the matching batch fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchFee> findByBatch(long batchId) throws SystemException {
		return findByBatch(batchId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch fees where batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of batch fees
	 * @param end the upper bound of the range of batch fees (not inclusive)
	 * @return the range of matching batch fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchFee> findByBatch(long batchId, int start, int end)
		throws SystemException {
		return findByBatch(batchId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch fees where batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of batch fees
	 * @param end the upper bound of the range of batch fees (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batch fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchFee> findByBatch(long batchId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH;
			finderArgs = new Object[] { batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCH;
			finderArgs = new Object[] { batchId, start, end, orderByComparator };
		}

		List<BatchFee> list = (List<BatchFee>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BatchFee batchFee : list) {
				if ((batchId != batchFee.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_BATCHFEE_WHERE);

			query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				list = (List<BatchFee>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch fee in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch fee
	 * @throws info.diit.portal.accounts.NoSuchBatchFeeException if a matching batch fee could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchFee findByBatch_First(long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchFeeException, SystemException {
		BatchFee batchFee = fetchByBatch_First(batchId, orderByComparator);

		if (batchFee != null) {
			return batchFee;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchFeeException(msg.toString());
	}

	/**
	 * Returns the first batch fee in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch fee, or <code>null</code> if a matching batch fee could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchFee fetchByBatch_First(long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BatchFee> list = findByBatch(batchId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch fee in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch fee
	 * @throws info.diit.portal.accounts.NoSuchBatchFeeException if a matching batch fee could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchFee findByBatch_Last(long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchFeeException, SystemException {
		BatchFee batchFee = fetchByBatch_Last(batchId, orderByComparator);

		if (batchFee != null) {
			return batchFee;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchFeeException(msg.toString());
	}

	/**
	 * Returns the last batch fee in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch fee, or <code>null</code> if a matching batch fee could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchFee fetchByBatch_Last(long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBatch(batchId);

		List<BatchFee> list = findByBatch(batchId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batch fees before and after the current batch fee in the ordered set where batchId = &#63;.
	 *
	 * @param batchFeeId the primary key of the current batch fee
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch fee
	 * @throws info.diit.portal.accounts.NoSuchBatchFeeException if a batch fee with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchFee[] findByBatch_PrevAndNext(long batchFeeId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchFeeException, SystemException {
		BatchFee batchFee = findByPrimaryKey(batchFeeId);

		Session session = null;

		try {
			session = openSession();

			BatchFee[] array = new BatchFeeImpl[3];

			array[0] = getByBatch_PrevAndNext(session, batchFee, batchId,
					orderByComparator, true);

			array[1] = batchFee;

			array[2] = getByBatch_PrevAndNext(session, batchFee, batchId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BatchFee getByBatch_PrevAndNext(Session session,
		BatchFee batchFee, long batchId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCHFEE_WHERE);

		query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batchFee);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BatchFee> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the batch fees.
	 *
	 * @return the batch fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchFee> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch fees.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of batch fees
	 * @param end the upper bound of the range of batch fees (not inclusive)
	 * @return the range of batch fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchFee> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch fees.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of batch fees
	 * @param end the upper bound of the range of batch fees (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of batch fees
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchFee> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<BatchFee> list = (List<BatchFee>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_BATCHFEE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_BATCHFEE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<BatchFee>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<BatchFee>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the batch fees where batchId = &#63; from the database.
	 *
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBatch(long batchId) throws SystemException {
		for (BatchFee batchFee : findByBatch(batchId)) {
			remove(batchFee);
		}
	}

	/**
	 * Removes all the batch fees from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (BatchFee batchFee : findAll()) {
			remove(batchFee);
		}
	}

	/**
	 * Returns the number of batch fees where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the number of matching batch fees
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBatch(long batchId) throws SystemException {
		Object[] finderArgs = new Object[] { batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCH,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BATCHFEE_WHERE);

			query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCH,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch fees.
	 *
	 * @return the number of batch fees
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_BATCHFEE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the batch fee persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.accounts.model.BatchFee")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<BatchFee>> listenersList = new ArrayList<ModelListener<BatchFee>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<BatchFee>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(BatchFeeImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_BATCHFEE = "SELECT batchFee FROM BatchFee batchFee";
	private static final String _SQL_SELECT_BATCHFEE_WHERE = "SELECT batchFee FROM BatchFee batchFee WHERE ";
	private static final String _SQL_COUNT_BATCHFEE = "SELECT COUNT(batchFee) FROM BatchFee batchFee";
	private static final String _SQL_COUNT_BATCHFEE_WHERE = "SELECT COUNT(batchFee) FROM BatchFee batchFee WHERE ";
	private static final String _FINDER_COLUMN_BATCH_BATCHID_2 = "batchFee.batchId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "batchFee.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BatchFee exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No BatchFee exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(BatchFeePersistenceImpl.class);
	private static BatchFee _nullBatchFee = new BatchFeeImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<BatchFee> toCacheModel() {
				return _nullBatchFeeCacheModel;
			}
		};

	private static CacheModel<BatchFee> _nullBatchFeeCacheModel = new CacheModel<BatchFee>() {
			public BatchFee toEntityModel() {
				return _nullBatchFee;
			}
		};
}
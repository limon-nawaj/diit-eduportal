/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.accounts.model.StudentFee;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing StudentFee in entity cache.
 *
 * @author shamsuddin
 * @see StudentFee
 * @generated
 */
public class StudentFeeCacheModel implements CacheModel<StudentFee>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{studentFeeId=");
		sb.append(studentFeeId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", studentId=");
		sb.append(studentId);
		sb.append(", batchId=");
		sb.append(batchId);
		sb.append(", feeTypeId=");
		sb.append(feeTypeId);
		sb.append(", amount=");
		sb.append(amount);
		sb.append("}");

		return sb.toString();
	}

	public StudentFee toEntityModel() {
		StudentFeeImpl studentFeeImpl = new StudentFeeImpl();

		studentFeeImpl.setStudentFeeId(studentFeeId);
		studentFeeImpl.setCompanyId(companyId);
		studentFeeImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			studentFeeImpl.setCreateDate(null);
		}
		else {
			studentFeeImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			studentFeeImpl.setModifiedDate(null);
		}
		else {
			studentFeeImpl.setModifiedDate(new Date(modifiedDate));
		}

		studentFeeImpl.setStudentId(studentId);
		studentFeeImpl.setBatchId(batchId);
		studentFeeImpl.setFeeTypeId(feeTypeId);
		studentFeeImpl.setAmount(amount);

		studentFeeImpl.resetOriginalValues();

		return studentFeeImpl;
	}

	public long studentFeeId;
	public long companyId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public long studentId;
	public long batchId;
	public long feeTypeId;
	public double amount;
}
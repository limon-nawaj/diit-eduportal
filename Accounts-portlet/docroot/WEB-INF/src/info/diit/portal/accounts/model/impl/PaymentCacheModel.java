/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.accounts.model.Payment;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Payment in entity cache.
 *
 * @author shamsuddin
 * @see Payment
 * @generated
 */
public class PaymentCacheModel implements CacheModel<Payment>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{paymentId=");
		sb.append(paymentId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", studentId=");
		sb.append(studentId);
		sb.append(", batchId=");
		sb.append(batchId);
		sb.append(", feeTypeId=");
		sb.append(feeTypeId);
		sb.append(", amount=");
		sb.append(amount);
		sb.append(", paymentDate=");
		sb.append(paymentDate);
		sb.append("}");

		return sb.toString();
	}

	public Payment toEntityModel() {
		PaymentImpl paymentImpl = new PaymentImpl();

		paymentImpl.setPaymentId(paymentId);
		paymentImpl.setCompanyId(companyId);
		paymentImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			paymentImpl.setCreateDate(null);
		}
		else {
			paymentImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			paymentImpl.setModifiedDate(null);
		}
		else {
			paymentImpl.setModifiedDate(new Date(modifiedDate));
		}

		paymentImpl.setStudentId(studentId);
		paymentImpl.setBatchId(batchId);
		paymentImpl.setFeeTypeId(feeTypeId);
		paymentImpl.setAmount(amount);

		if (paymentDate == Long.MIN_VALUE) {
			paymentImpl.setPaymentDate(null);
		}
		else {
			paymentImpl.setPaymentDate(new Date(paymentDate));
		}

		paymentImpl.resetOriginalValues();

		return paymentImpl;
	}

	public long paymentId;
	public long companyId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public long studentId;
	public long batchId;
	public long feeTypeId;
	public double amount;
	public long paymentDate;
}
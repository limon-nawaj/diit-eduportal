create table EduPortal_Accounts_BatchFee (
	batchFeeId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	batchId LONG,
	feeTypeId LONG,
	amount DOUBLE
);

create table EduPortal_Accounts_BatchPaymentSchedule (
	batchPaymentScheduleId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	batchId LONG,
	scheduleDate DATE null,
	feeTypeId LONG,
	amount DOUBLE
);

create table EduPortal_Accounts_CourseFee (
	courseFeeId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	courseId LONG,
	feeTypeId LONG,
	amount DOUBLE
);

create table EduPortal_Accounts_FeeType (
	feeTypeId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	typeName VARCHAR(75) null,
	description VARCHAR(75) null
);

create table EduPortal_Accounts_Payment (
	paymentId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	studentId LONG,
	batchId LONG,
	feeTypeId LONG,
	amount DOUBLE,
	paymentDate DATE null
);

create table EduPortal_Accounts_StudentDiscount (
	studentDiscountId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	studentId LONG,
	batchId LONG,
	scholarships DOUBLE,
	discount DOUBLE
);

create table EduPortal_Accounts_StudentFee (
	studentFeeId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	studentId LONG,
	batchId LONG,
	feeTypeId LONG,
	amount DOUBLE
);

create table EduPortal_Accounts_StudentPaymentSchedule (
	studentPaymentScheduleId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	studentId LONG,
	batchId LONG,
	scheduleDate DATE null,
	feeTypeId LONG,
	amount DOUBLE
);

create table Eduportal_Accounts_BatchFee (
	batchFeeId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	batchId LONG,
	feeTypeId LONG,
	amount DOUBLE
);

create table Eduportal_Accounts_BatchPaymentSchedule (
	batchPaymentScheduleId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	batchId LONG,
	scheduleDate DATE null,
	feeTypeId LONG,
	amount DOUBLE
);

create table Eduportal_Accounts_CourseFee (
	courseFeeId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	courseId LONG,
	feeTypeId LONG,
	amount DOUBLE
);

create table Eduportal_Accounts_FeeType (
	feeTypeId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	typeName VARCHAR(75) null,
	description VARCHAR(75) null
);

create table Eduportal_Accounts_StudentDiscount (
	studentDiscountId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	studentId LONG,
	batchId LONG,
	scholarships DOUBLE,
	discount DOUBLE
);

create table Eduportal_Accounts_StudentFee (
	studentFeeId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	studentId LONG,
	batchId LONG,
	feeTypeId LONG,
	amount DOUBLE
);

create table Eduportal_Accounts_StudentPaymentSchedule (
	studentPaymentScheduleId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	studentId LONG,
	batchId LONG,
	scheduleDate DATE null,
	feeTypeId LONG,
	amount DOUBLE
);
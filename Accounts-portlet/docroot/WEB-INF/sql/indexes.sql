create index IX_2D7F7717 on EduPortal_Accounts_BatchFee (batchId);

create index IX_8A501A80 on EduPortal_Accounts_BatchPaymentSchedule (batchId);

create index IX_B557F9DF on EduPortal_Accounts_CourseFee (courseId);

create index IX_CFA95863 on EduPortal_Accounts_FeeType (typeName);

create index IX_B2659AAF on EduPortal_Accounts_Payment (studentId, batchId);

create index IX_902EC0C5 on EduPortal_Accounts_StudentDiscount (studentId, batchId);

create index IX_5C79E6C on EduPortal_Accounts_StudentFee (studentId, batchId);

create index IX_9CC438E3 on EduPortal_Accounts_StudentPaymentSchedule (studentId, batchId);

create index IX_8976F37 on Eduportal_Accounts_BatchFee (batchId);

create index IX_3994E2A0 on Eduportal_Accounts_BatchPaymentSchedule (batchId);

create index IX_2A5269FF on Eduportal_Accounts_CourseFee (courseId);

create index IX_AAC15083 on Eduportal_Accounts_FeeType (typeName);

create index IX_3784F0E5 on Eduportal_Accounts_StudentDiscount (studentId, batchId);

create index IX_3F1BDA4C on Eduportal_Accounts_StudentFee (studentId, batchId);

create index IX_5D3DA4C3 on Eduportal_Accounts_StudentPaymentSchedule (studentId, batchId);
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.service.impl;

import info.diit.portal.accounts.model.BatchFee;
import info.diit.portal.accounts.model.BatchPaymentSchedule;
import info.diit.portal.accounts.service.BatchFeeLocalServiceUtil;
import info.diit.portal.accounts.service.BatchPaymentScheduleLocalServiceUtil;
import info.diit.portal.accounts.service.base.BatchFeeLocalServiceBaseImpl;
import info.diit.portal.accounts.service.persistence.BatchFeeUtil;
import info.diit.portal.accounts.service.persistence.BatchPaymentScheduleUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the batch fee local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.accounts.service.BatchFeeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author shamsuddin
 * @see info.diit.portal.accounts.service.base.BatchFeeLocalServiceBaseImpl
 * @see info.diit.portal.accounts.service.BatchFeeLocalServiceUtil
 */
public class BatchFeeLocalServiceImpl extends BatchFeeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.accounts.service.BatchFeeLocalServiceUtil} to access the batch fee local service.
	 */
	
	public List<BatchFee> getBatchFeesList(long batchId) throws SystemException
	{
		return BatchFeeUtil.findByBatch(batchId);
	}
	
	public void updateBatchFees(long batchId,List<BatchFee> batchFees,List<BatchPaymentSchedule> batchPaymentSchedules) throws SystemException
	{
		//remove old batches
		
		BatchFeeUtil.removeByBatch(batchId);
		
		//add the new batch fees
		for(BatchFee batchFee:batchFees)
		{
			if(batchFee.getBatchId()==batchId)
			{
				BatchFeeLocalServiceUtil.addBatchFee(batchFee);
			}
		}
		
		//remove old payment schedules
		BatchPaymentScheduleUtil.removeByBatch(batchId);
		
		//add the new payment schedule
		for(BatchPaymentSchedule batchPaymentSchedule:batchPaymentSchedules)
		{
			if(batchPaymentSchedule.getBatchId()==batchId)
			{
				BatchPaymentScheduleLocalServiceUtil.addBatchPaymentSchedule(batchPaymentSchedule);
			}
		}
		
	}
}
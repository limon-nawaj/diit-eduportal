package info.diit.portal.accounts.dto;

import java.io.Serializable;

public class CourseDto implements Serializable{

	private long courseId;
	private String courseCode;
	private String courseName;
	
	public long getCourseId() {
		return courseId;
	}
	
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	
	public String getCourseCode() {
		return courseCode;
	}
	
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}
	
	public String getCourseName() {
		return courseName;
	}
	
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getCourseName();
	}
}

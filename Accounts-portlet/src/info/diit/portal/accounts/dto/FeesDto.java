package info.diit.portal.accounts.dto;

import java.io.Serializable;

public class FeesDto implements Serializable {

	private long id;
	private String feeType;
	private String description;
	private Long amount;

	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getFeeType() {
		return feeType;
	}
	
	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Long getAmount() {
		return amount;
	}
	
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj instanceof FeesDto)
		{
			return id==((FeesDto)obj).getId();
		}
		
		return super.equals(obj);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getFeeType();
	}
}

package info.diit.portal.accounts.dto;

import java.io.Serializable;

public class BatchDto implements Serializable{

	private Long batchId;
	private String batchName;
	
	public Long getBatchId() {
		return batchId;
	}
	
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}
	
	public String getBatchName() {
		return batchName;
	}
	
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	
	@Override
	public String toString() {
		
		return getBatchName();
	}
}

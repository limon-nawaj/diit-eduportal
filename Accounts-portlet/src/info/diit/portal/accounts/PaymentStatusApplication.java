package info.diit.portal.accounts;

import info.diit.portal.accounts.dto.FeesDto;
import info.diit.portal.accounts.dto.StatusDto;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;

public class PaymentStatusApplication extends Application  implements PortletRequestListener{

	private ThemeDisplay themeDisplay;
	private Window window;
	
	private static final String COLUMN_STUDENT_ID = "studentId";
	private static final String COLUMN_NAME = "name";
	private static final String COLUMN_TOTAL_FEE = "totalFee";
	private static final String COLUMN_TOTAL_PAID = "totalPaid";
	private static final String COLUMN_TOTAL_DUE = "totalDue";
	private static final String COLUMN_REMAINING = "remaining";
	
	private BeanItemContainer<StatusDto> paymentStatusBeanItemContainer;
	
	private ComboBox courseComboBox;
	private ComboBox batchComboBox;
	
	private Table paymentStatusTable;
	
    public void init() {
    	window = new Window();
		window.setSizeFull();
		setMainWindow(window);
		
		window.addComponent(initPaymentStatusLayout());
		
    }
    
    public GridLayout initPaymentStatusLayout()
    {
    	courseComboBox = new ComboBox("Course");
    	courseComboBox.setWidth("100%");
    	courseComboBox.setImmediate(true);
    	
    	batchComboBox = new ComboBox("Batch");
    	batchComboBox.setWidth("100%");
    	batchComboBox.setImmediate(true);
    	
    	paymentStatusBeanItemContainer = new BeanItemContainer<StatusDto>(StatusDto.class);
    	paymentStatusTable = new Table("Total Fees",paymentStatusBeanItemContainer);
    	paymentStatusTable.setWidth("100%");
    	paymentStatusTable.setPageLength(20);
		
    	paymentStatusTable.setColumnHeader(COLUMN_STUDENT_ID, "Student ID");
    	paymentStatusTable.setColumnHeader(COLUMN_NAME, "Name");
    	paymentStatusTable.setColumnHeader(COLUMN_TOTAL_FEE, "Total Fee");
    	paymentStatusTable.setColumnHeader(COLUMN_TOTAL_PAID, "Total Paid");
    	paymentStatusTable.setColumnHeader(COLUMN_TOTAL_DUE, "Total Due");
    	paymentStatusTable.setColumnHeader(COLUMN_REMAINING, "Remaining");
    	
    	paymentStatusTable.setColumnAlignment(COLUMN_TOTAL_FEE, Table.ALIGN_RIGHT);
    	paymentStatusTable.setColumnAlignment(COLUMN_TOTAL_PAID, Table.ALIGN_RIGHT);
    	paymentStatusTable.setColumnAlignment(COLUMN_TOTAL_DUE, Table.ALIGN_RIGHT);
    	paymentStatusTable.setColumnAlignment(COLUMN_REMAINING, Table.ALIGN_RIGHT);
    	
    	paymentStatusTable.setColumnExpandRatio(COLUMN_STUDENT_ID, 1);
    	paymentStatusTable.setColumnExpandRatio(COLUMN_NAME, 3);
    	paymentStatusTable.setColumnExpandRatio(COLUMN_TOTAL_FEE, 1);
    	paymentStatusTable.setColumnExpandRatio(COLUMN_TOTAL_PAID, 1);
    	paymentStatusTable.setColumnExpandRatio(COLUMN_TOTAL_DUE, 1);
    	paymentStatusTable.setColumnExpandRatio(COLUMN_REMAINING, 1);

    	paymentStatusTable.setVisibleColumns(new String[]{COLUMN_STUDENT_ID,COLUMN_NAME,COLUMN_TOTAL_FEE,COLUMN_TOTAL_PAID,COLUMN_TOTAL_DUE,COLUMN_REMAINING});
    	
    	GridLayout gridLayout = new GridLayout(5,2);
    	gridLayout.setWidth("100%");
    	gridLayout.setSpacing(true);
    	
    	gridLayout.addComponent(courseComboBox,0,0,1,0);
    	gridLayout.addComponent(batchComboBox,2,0);
    	gridLayout.addComponent(paymentStatusTable,0,1,4,1);
    	
    	return gridLayout;
    }
    
    @Override
    public void onRequestStart(PortletRequest p_request, PortletResponse p_response) {
        themeDisplay = (ThemeDisplay) p_request.getAttribute(WebKeys.THEME_DISPLAY);
        
    }
	
	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}

}

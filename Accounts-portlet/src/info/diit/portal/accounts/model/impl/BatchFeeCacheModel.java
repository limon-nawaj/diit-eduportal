/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.accounts.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.accounts.model.BatchFee;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing BatchFee in entity cache.
 *
 * @author shamsuddin
 * @see BatchFee
 * @generated
 */
public class BatchFeeCacheModel implements CacheModel<BatchFee>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{batchFeeId=");
		sb.append(batchFeeId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", batchId=");
		sb.append(batchId);
		sb.append(", feeTypeId=");
		sb.append(feeTypeId);
		sb.append(", amount=");
		sb.append(amount);
		sb.append("}");

		return sb.toString();
	}

	public BatchFee toEntityModel() {
		BatchFeeImpl batchFeeImpl = new BatchFeeImpl();

		batchFeeImpl.setBatchFeeId(batchFeeId);
		batchFeeImpl.setCompanyId(companyId);
		batchFeeImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			batchFeeImpl.setCreateDate(null);
		}
		else {
			batchFeeImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			batchFeeImpl.setModifiedDate(null);
		}
		else {
			batchFeeImpl.setModifiedDate(new Date(modifiedDate));
		}

		batchFeeImpl.setBatchId(batchId);
		batchFeeImpl.setFeeTypeId(feeTypeId);
		batchFeeImpl.setAmount(amount);

		batchFeeImpl.resetOriginalValues();

		return batchFeeImpl;
	}

	public long batchFeeId;
	public long companyId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public long batchId;
	public long feeTypeId;
	public double amount;
}
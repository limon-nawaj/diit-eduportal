create table EduPortal_AdmissionProcess_AccademicRecord (
	accademicRecordId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organisationId LONG,
	degree VARCHAR(75) null,
	board VARCHAR(75) null,
	year INTEGER,
	result VARCHAR(75) null,
	registrationNo VARCHAR(75) null,
	studentId LONG
);

create table EduPortal_AdmissionProcess_BatchStudent (
	batchStudentId LONG not null primary key IDENTITY,
	studentId LONG,
	batchId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	startDate DATE null,
	endDate DATE null,
	note VARCHAR(75) null,
	status INTEGER
);

create table EduPortal_AdmissionProcess_Experiance (
	experianceId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	organization VARCHAR(75) null,
	designation VARCHAR(75) null,
	startDate DATE null,
	endDate DATE null,
	currentStatus INTEGER,
	studentId LONG
);

create table EduPortal_AdmissionProcess_PersonEmail (
	personEmailId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	ownerType INTEGER,
	personEmail VARCHAR(75) null,
	studentId LONG
);

create table EduPortal_AdmissionProcess_PhoneNumber (
	phoneNumberId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	ownerType INTEGER,
	phoneNumber VARCHAR(75) null,
	studentId LONG
);

create table EduPortal_AdmissionProcess_StatusHistory (
	statusHistoryId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	batchStudentId LONG,
	fromStatus INTEGER,
	toStatus INTEGER
);

create table EduPortal_AdmissionProcess_Student (
	studentId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	name VARCHAR(75) null,
	fatherName VARCHAR(75) null,
	motherName VARCHAR(75) null,
	presentAddress VARCHAR(75) null,
	permanentAddress VARCHAR(75) null,
	homePhone VARCHAR(75) null,
	dateOfBirth DATE null,
	gender VARCHAR(75) null,
	religion VARCHAR(75) null,
	nationality VARCHAR(75) null,
	photo LONG,
	status INTEGER,
	studentUserID LONG
);

create table EduPortal_AdmissionProcess_StudentDocument (
	documentId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	type_ VARCHAR(75) null,
	documentData LONG,
	description VARCHAR(75) null,
	studentId LONG,
	fileEntryId LONG
);
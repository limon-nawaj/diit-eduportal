create index IX_C3826845 on EduPortal_AdmissionProcess_AccademicRecord (studentId);

create index IX_D4010782 on EduPortal_AdmissionProcess_BatchStudent (batchId);
create index IX_99387227 on EduPortal_AdmissionProcess_BatchStudent (companyId, organizationId);
create index IX_43AA2020 on EduPortal_AdmissionProcess_BatchStudent (organizationId, batchId);
create index IX_6112C6C6 on EduPortal_AdmissionProcess_BatchStudent (organizationId, batchId, studentId);
create index IX_7A074F43 on EduPortal_AdmissionProcess_BatchStudent (studentId);

create index IX_DB29E316 on EduPortal_AdmissionProcess_Experiance (studentId);

create index IX_8840CE26 on EduPortal_AdmissionProcess_PersonEmail (personEmail);
create index IX_17258115 on EduPortal_AdmissionProcess_PersonEmail (studentId);

create index IX_23F0E05 on EduPortal_AdmissionProcess_PhoneNumber (studentId);

create index IX_AC302FE3 on EduPortal_AdmissionProcess_Student (companyId);
create index IX_F149F4C7 on EduPortal_AdmissionProcess_Student (organizationId);
create index IX_6BE953BF on EduPortal_AdmissionProcess_Student (organizationId, studentId);

create index IX_B267132A on EduPortal_AdmissionProcess_StudentDocument (documentId);
create index IX_7C1FF846 on EduPortal_AdmissionProcess_StudentDocument (studentId);

create index IX_EBACAD88 on EduPortal_Student_Student (companyId);
create index IX_66A33642 on EduPortal_Student_Student (organizationId);

create index IX_6DFD1F25 on EduPortal_Student_StudentDocument (documentId);

create index IX_D183F26D on Student_AccademicRecord (studentId);

create index IX_E7EBC45A on Student_BatchStudent (batchId);
create index IX_EE2EA8FF on Student_BatchStudent (companyId, organizationId);
create index IX_4E20B87B on Student_BatchStudent (organizationId, companyId);
create index IX_3E36361B on Student_BatchStudent (studentId);

create index IX_EF149FEE on Student_Experiance (studentId);

create index IX_80925F3D on Student_PersonEmail (studentId);

create index IX_6BABEC2D on Student_PhoneNumber (studentId);

create index IX_F3D2620B on Student_Student (companyId);
create index IX_B578DB9F on Student_Student (organizationId);

create index IX_6496CE02 on Student_StudentDocument (documentId);
create index IX_8A21826E on Student_StudentDocument (studentId);

create index IX_6C9716E4 on eduportal_Student (companyId);
create index IX_B1D04666 on eduportal_Student (organizationId);

create index IX_3E1D7D49 on eduportal_StudentDocument (documentId);
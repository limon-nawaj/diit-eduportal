/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.student.service.model.BatchStudent;

import java.util.List;

/**
 * The persistence utility for the batch student service. This utility wraps {@link BatchStudentPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see BatchStudentPersistence
 * @see BatchStudentPersistenceImpl
 * @generated
 */
public class BatchStudentUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(BatchStudent batchStudent) {
		getPersistence().clearCache(batchStudent);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<BatchStudent> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<BatchStudent> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<BatchStudent> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static BatchStudent update(BatchStudent batchStudent, boolean merge)
		throws SystemException {
		return getPersistence().update(batchStudent, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static BatchStudent update(BatchStudent batchStudent, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(batchStudent, merge, serviceContext);
	}

	/**
	* Caches the batch student in the entity cache if it is enabled.
	*
	* @param batchStudent the batch student
	*/
	public static void cacheResult(
		info.diit.portal.student.service.model.BatchStudent batchStudent) {
		getPersistence().cacheResult(batchStudent);
	}

	/**
	* Caches the batch students in the entity cache if it is enabled.
	*
	* @param batchStudents the batch students
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.student.service.model.BatchStudent> batchStudents) {
		getPersistence().cacheResult(batchStudents);
	}

	/**
	* Creates a new batch student with the primary key. Does not add the batch student to the database.
	*
	* @param batchStudentId the primary key for the new batch student
	* @return the new batch student
	*/
	public static info.diit.portal.student.service.model.BatchStudent create(
		long batchStudentId) {
		return getPersistence().create(batchStudentId);
	}

	/**
	* Removes the batch student with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchStudentId the primary key of the batch student
	* @return the batch student that was removed
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent remove(
		long batchStudentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence().remove(batchStudentId);
	}

	public static info.diit.portal.student.service.model.BatchStudent updateImpl(
		info.diit.portal.student.service.model.BatchStudent batchStudent,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(batchStudent, merge);
	}

	/**
	* Returns the batch student with the primary key or throws a {@link info.diit.portal.student.service.NoSuchBatchStudentException} if it could not be found.
	*
	* @param batchStudentId the primary key of the batch student
	* @return the batch student
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent findByPrimaryKey(
		long batchStudentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence().findByPrimaryKey(batchStudentId);
	}

	/**
	* Returns the batch student with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param batchStudentId the primary key of the batch student
	* @return the batch student, or <code>null</code> if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent fetchByPrimaryKey(
		long batchStudentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(batchStudentId);
	}

	/**
	* Returns all the batch students where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findByBatch(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBatch(batchId);
	}

	/**
	* Returns a range of all the batch students where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @return the range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findByBatch(
		long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBatch(batchId, start, end);
	}

	/**
	* Returns an ordered range of all the batch students where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findByBatch(
		long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatch(batchId, start, end, orderByComparator);
	}

	/**
	* Returns the first batch student in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent findByBatch_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence().findByBatch_First(batchId, orderByComparator);
	}

	/**
	* Returns the first batch student in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent fetchByBatch_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByBatch_First(batchId, orderByComparator);
	}

	/**
	* Returns the last batch student in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent findByBatch_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence().findByBatch_Last(batchId, orderByComparator);
	}

	/**
	* Returns the last batch student in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent fetchByBatch_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByBatch_Last(batchId, orderByComparator);
	}

	/**
	* Returns the batch students before and after the current batch student in the ordered set where batchId = &#63;.
	*
	* @param batchStudentId the primary key of the current batch student
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch student
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent[] findByBatch_PrevAndNext(
		long batchStudentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence()
				   .findByBatch_PrevAndNext(batchStudentId, batchId,
			orderByComparator);
	}

	/**
	* Returns all the batch students where companyId = &#63; and organizationId = &#63;.
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @return the matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findByCompanyOrgnization(
		long companyId, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCompanyOrgnization(companyId, organizationId);
	}

	/**
	* Returns a range of all the batch students where companyId = &#63; and organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @return the range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findByCompanyOrgnization(
		long companyId, long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCompanyOrgnization(companyId, organizationId, start,
			end);
	}

	/**
	* Returns an ordered range of all the batch students where companyId = &#63; and organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findByCompanyOrgnization(
		long companyId, long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCompanyOrgnization(companyId, organizationId, start,
			end, orderByComparator);
	}

	/**
	* Returns the first batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent findByCompanyOrgnization_First(
		long companyId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence()
				   .findByCompanyOrgnization_First(companyId, organizationId,
			orderByComparator);
	}

	/**
	* Returns the first batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent fetchByCompanyOrgnization_First(
		long companyId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCompanyOrgnization_First(companyId, organizationId,
			orderByComparator);
	}

	/**
	* Returns the last batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent findByCompanyOrgnization_Last(
		long companyId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence()
				   .findByCompanyOrgnization_Last(companyId, organizationId,
			orderByComparator);
	}

	/**
	* Returns the last batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent fetchByCompanyOrgnization_Last(
		long companyId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCompanyOrgnization_Last(companyId, organizationId,
			orderByComparator);
	}

	/**
	* Returns the batch students before and after the current batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	*
	* @param batchStudentId the primary key of the current batch student
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch student
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent[] findByCompanyOrgnization_PrevAndNext(
		long batchStudentId, long companyId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence()
				   .findByCompanyOrgnization_PrevAndNext(batchStudentId,
			companyId, organizationId, orderByComparator);
	}

	/**
	* Returns all the batch students where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findByStudentBatchStudentList(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStudentBatchStudentList(studentId);
	}

	/**
	* Returns a range of all the batch students where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @return the range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findByStudentBatchStudentList(
		long studentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentBatchStudentList(studentId, start, end);
	}

	/**
	* Returns an ordered range of all the batch students where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findByStudentBatchStudentList(
		long studentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentBatchStudentList(studentId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first batch student in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent findByStudentBatchStudentList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence()
				   .findByStudentBatchStudentList_First(studentId,
			orderByComparator);
	}

	/**
	* Returns the first batch student in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent fetchByStudentBatchStudentList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentBatchStudentList_First(studentId,
			orderByComparator);
	}

	/**
	* Returns the last batch student in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent findByStudentBatchStudentList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence()
				   .findByStudentBatchStudentList_Last(studentId,
			orderByComparator);
	}

	/**
	* Returns the last batch student in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent fetchByStudentBatchStudentList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentBatchStudentList_Last(studentId,
			orderByComparator);
	}

	/**
	* Returns the batch students before and after the current batch student in the ordered set where studentId = &#63;.
	*
	* @param batchStudentId the primary key of the current batch student
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch student
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent[] findByStudentBatchStudentList_PrevAndNext(
		long batchStudentId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence()
				   .findByStudentBatchStudentList_PrevAndNext(batchStudentId,
			studentId, orderByComparator);
	}

	/**
	* Returns all the batch students where organizationId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @return the matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findByAllStudentsByBatchId(
		long organizationId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByAllStudentsByBatchId(organizationId, batchId);
	}

	/**
	* Returns a range of all the batch students where organizationId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @return the range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findByAllStudentsByBatchId(
		long organizationId, long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByAllStudentsByBatchId(organizationId, batchId, start,
			end);
	}

	/**
	* Returns an ordered range of all the batch students where organizationId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findByAllStudentsByBatchId(
		long organizationId, long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByAllStudentsByBatchId(organizationId, batchId, start,
			end, orderByComparator);
	}

	/**
	* Returns the first batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent findByAllStudentsByBatchId_First(
		long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence()
				   .findByAllStudentsByBatchId_First(organizationId, batchId,
			orderByComparator);
	}

	/**
	* Returns the first batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent fetchByAllStudentsByBatchId_First(
		long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByAllStudentsByBatchId_First(organizationId, batchId,
			orderByComparator);
	}

	/**
	* Returns the last batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent findByAllStudentsByBatchId_Last(
		long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence()
				   .findByAllStudentsByBatchId_Last(organizationId, batchId,
			orderByComparator);
	}

	/**
	* Returns the last batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent fetchByAllStudentsByBatchId_Last(
		long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByAllStudentsByBatchId_Last(organizationId, batchId,
			orderByComparator);
	}

	/**
	* Returns the batch students before and after the current batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	*
	* @param batchStudentId the primary key of the current batch student
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch student
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent[] findByAllStudentsByBatchId_PrevAndNext(
		long batchStudentId, long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence()
				   .findByAllStudentsByBatchId_PrevAndNext(batchStudentId,
			organizationId, batchId, orderByComparator);
	}

	/**
	* Returns the batch student where organizationId = &#63; and batchId = &#63; and studentId = &#63; or throws a {@link info.diit.portal.student.service.NoSuchBatchStudentException} if it could not be found.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param studentId the student ID
	* @return the matching batch student
	* @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent findByOrgBatchStudent(
		long organizationId, long batchId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence()
				   .findByOrgBatchStudent(organizationId, batchId, studentId);
	}

	/**
	* Returns the batch student where organizationId = &#63; and batchId = &#63; and studentId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param studentId the student ID
	* @return the matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent fetchByOrgBatchStudent(
		long organizationId, long batchId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrgBatchStudent(organizationId, batchId, studentId);
	}

	/**
	* Returns the batch student where organizationId = &#63; and batchId = &#63; and studentId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param studentId the student ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent fetchByOrgBatchStudent(
		long organizationId, long batchId, long studentId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrgBatchStudent(organizationId, batchId, studentId,
			retrieveFromCache);
	}

	/**
	* Returns all the batch students.
	*
	* @return the batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the batch students.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @return the range of batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the batch students.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the batch students where batchId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBatch(batchId);
	}

	/**
	* Removes all the batch students where companyId = &#63; and organizationId = &#63; from the database.
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCompanyOrgnization(long companyId,
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCompanyOrgnization(companyId, organizationId);
	}

	/**
	* Removes all the batch students where studentId = &#63; from the database.
	*
	* @param studentId the student ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByStudentBatchStudentList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByStudentBatchStudentList(studentId);
	}

	/**
	* Removes all the batch students where organizationId = &#63; and batchId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByAllStudentsByBatchId(long organizationId,
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByAllStudentsByBatchId(organizationId, batchId);
	}

	/**
	* Removes the batch student where organizationId = &#63; and batchId = &#63; and studentId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param studentId the student ID
	* @return the batch student that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent removeByOrgBatchStudent(
		long organizationId, long batchId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getPersistence()
				   .removeByOrgBatchStudent(organizationId, batchId, studentId);
	}

	/**
	* Removes all the batch students from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of batch students where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the number of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBatch(batchId);
	}

	/**
	* Returns the number of batch students where companyId = &#63; and organizationId = &#63;.
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @return the number of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCompanyOrgnization(long companyId,
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByCompanyOrgnization(companyId, organizationId);
	}

	/**
	* Returns the number of batch students where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the number of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStudentBatchStudentList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStudentBatchStudentList(studentId);
	}

	/**
	* Returns the number of batch students where organizationId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @return the number of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static int countByAllStudentsByBatchId(long organizationId,
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByAllStudentsByBatchId(organizationId, batchId);
	}

	/**
	* Returns the number of batch students where organizationId = &#63; and batchId = &#63; and studentId = &#63;.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param studentId the student ID
	* @return the number of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public static int countByOrgBatchStudent(long organizationId, long batchId,
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByOrgBatchStudent(organizationId, batchId, studentId);
	}

	/**
	* Returns the number of batch students.
	*
	* @return the number of batch students
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static BatchStudentPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (BatchStudentPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.student.service.service.ClpSerializer.getServletContextName(),
					BatchStudentPersistence.class.getName());

			ReferenceRegistry.registerReference(BatchStudentUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(BatchStudentPersistence persistence) {
	}

	private static BatchStudentPersistence _persistence;
}
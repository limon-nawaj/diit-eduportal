/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    nasimul
 * @generated
 */
public class PersonEmailSoap implements Serializable {
	public static PersonEmailSoap toSoapModel(PersonEmail model) {
		PersonEmailSoap soapModel = new PersonEmailSoap();

		soapModel.setPersonEmailId(model.getPersonEmailId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setOwnerType(model.getOwnerType());
		soapModel.setPersonEmail(model.getPersonEmail());
		soapModel.setStudentId(model.getStudentId());

		return soapModel;
	}

	public static PersonEmailSoap[] toSoapModels(PersonEmail[] models) {
		PersonEmailSoap[] soapModels = new PersonEmailSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PersonEmailSoap[][] toSoapModels(PersonEmail[][] models) {
		PersonEmailSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PersonEmailSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PersonEmailSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PersonEmailSoap[] toSoapModels(List<PersonEmail> models) {
		List<PersonEmailSoap> soapModels = new ArrayList<PersonEmailSoap>(models.size());

		for (PersonEmail model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PersonEmailSoap[soapModels.size()]);
	}

	public PersonEmailSoap() {
	}

	public long getPrimaryKey() {
		return _personEmailId;
	}

	public void setPrimaryKey(long pk) {
		setPersonEmailId(pk);
	}

	public long getPersonEmailId() {
		return _personEmailId;
	}

	public void setPersonEmailId(long personEmailId) {
		_personEmailId = personEmailId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public int getOwnerType() {
		return _ownerType;
	}

	public void setOwnerType(int ownerType) {
		_ownerType = ownerType;
	}

	public String getPersonEmail() {
		return _personEmail;
	}

	public void setPersonEmail(String personEmail) {
		_personEmail = personEmail;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	private long _personEmailId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private int _ownerType;
	private String _personEmail;
	private long _studentId;
}
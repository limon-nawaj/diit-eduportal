/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PhoneNumber}.
 * </p>
 *
 * @author    nasimul
 * @see       PhoneNumber
 * @generated
 */
public class PhoneNumberWrapper implements PhoneNumber,
	ModelWrapper<PhoneNumber> {
	public PhoneNumberWrapper(PhoneNumber phoneNumber) {
		_phoneNumber = phoneNumber;
	}

	public Class<?> getModelClass() {
		return PhoneNumber.class;
	}

	public String getModelClassName() {
		return PhoneNumber.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("phoneNumberId", getPhoneNumberId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("ownerType", getOwnerType());
		attributes.put("phoneNumber", getPhoneNumber());
		attributes.put("studentId", getStudentId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long phoneNumberId = (Long)attributes.get("phoneNumberId");

		if (phoneNumberId != null) {
			setPhoneNumberId(phoneNumberId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Integer ownerType = (Integer)attributes.get("ownerType");

		if (ownerType != null) {
			setOwnerType(ownerType);
		}

		String phoneNumber = (String)attributes.get("phoneNumber");

		if (phoneNumber != null) {
			setPhoneNumber(phoneNumber);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}
	}

	/**
	* Returns the primary key of this phone number.
	*
	* @return the primary key of this phone number
	*/
	public long getPrimaryKey() {
		return _phoneNumber.getPrimaryKey();
	}

	/**
	* Sets the primary key of this phone number.
	*
	* @param primaryKey the primary key of this phone number
	*/
	public void setPrimaryKey(long primaryKey) {
		_phoneNumber.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the phone number ID of this phone number.
	*
	* @return the phone number ID of this phone number
	*/
	public long getPhoneNumberId() {
		return _phoneNumber.getPhoneNumberId();
	}

	/**
	* Sets the phone number ID of this phone number.
	*
	* @param phoneNumberId the phone number ID of this phone number
	*/
	public void setPhoneNumberId(long phoneNumberId) {
		_phoneNumber.setPhoneNumberId(phoneNumberId);
	}

	/**
	* Returns the company ID of this phone number.
	*
	* @return the company ID of this phone number
	*/
	public long getCompanyId() {
		return _phoneNumber.getCompanyId();
	}

	/**
	* Sets the company ID of this phone number.
	*
	* @param companyId the company ID of this phone number
	*/
	public void setCompanyId(long companyId) {
		_phoneNumber.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this phone number.
	*
	* @return the user ID of this phone number
	*/
	public long getUserId() {
		return _phoneNumber.getUserId();
	}

	/**
	* Sets the user ID of this phone number.
	*
	* @param userId the user ID of this phone number
	*/
	public void setUserId(long userId) {
		_phoneNumber.setUserId(userId);
	}

	/**
	* Returns the user uuid of this phone number.
	*
	* @return the user uuid of this phone number
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _phoneNumber.getUserUuid();
	}

	/**
	* Sets the user uuid of this phone number.
	*
	* @param userUuid the user uuid of this phone number
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_phoneNumber.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this phone number.
	*
	* @return the user name of this phone number
	*/
	public java.lang.String getUserName() {
		return _phoneNumber.getUserName();
	}

	/**
	* Sets the user name of this phone number.
	*
	* @param userName the user name of this phone number
	*/
	public void setUserName(java.lang.String userName) {
		_phoneNumber.setUserName(userName);
	}

	/**
	* Returns the create date of this phone number.
	*
	* @return the create date of this phone number
	*/
	public java.util.Date getCreateDate() {
		return _phoneNumber.getCreateDate();
	}

	/**
	* Sets the create date of this phone number.
	*
	* @param createDate the create date of this phone number
	*/
	public void setCreateDate(java.util.Date createDate) {
		_phoneNumber.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this phone number.
	*
	* @return the modified date of this phone number
	*/
	public java.util.Date getModifiedDate() {
		return _phoneNumber.getModifiedDate();
	}

	/**
	* Sets the modified date of this phone number.
	*
	* @param modifiedDate the modified date of this phone number
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_phoneNumber.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the organization ID of this phone number.
	*
	* @return the organization ID of this phone number
	*/
	public long getOrganizationId() {
		return _phoneNumber.getOrganizationId();
	}

	/**
	* Sets the organization ID of this phone number.
	*
	* @param organizationId the organization ID of this phone number
	*/
	public void setOrganizationId(long organizationId) {
		_phoneNumber.setOrganizationId(organizationId);
	}

	/**
	* Returns the owner type of this phone number.
	*
	* @return the owner type of this phone number
	*/
	public int getOwnerType() {
		return _phoneNumber.getOwnerType();
	}

	/**
	* Sets the owner type of this phone number.
	*
	* @param ownerType the owner type of this phone number
	*/
	public void setOwnerType(int ownerType) {
		_phoneNumber.setOwnerType(ownerType);
	}

	/**
	* Returns the phone number of this phone number.
	*
	* @return the phone number of this phone number
	*/
	public java.lang.String getPhoneNumber() {
		return _phoneNumber.getPhoneNumber();
	}

	/**
	* Sets the phone number of this phone number.
	*
	* @param phoneNumber the phone number of this phone number
	*/
	public void setPhoneNumber(java.lang.String phoneNumber) {
		_phoneNumber.setPhoneNumber(phoneNumber);
	}

	/**
	* Returns the student ID of this phone number.
	*
	* @return the student ID of this phone number
	*/
	public long getStudentId() {
		return _phoneNumber.getStudentId();
	}

	/**
	* Sets the student ID of this phone number.
	*
	* @param studentId the student ID of this phone number
	*/
	public void setStudentId(long studentId) {
		_phoneNumber.setStudentId(studentId);
	}

	public boolean isNew() {
		return _phoneNumber.isNew();
	}

	public void setNew(boolean n) {
		_phoneNumber.setNew(n);
	}

	public boolean isCachedModel() {
		return _phoneNumber.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_phoneNumber.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _phoneNumber.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _phoneNumber.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_phoneNumber.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _phoneNumber.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_phoneNumber.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new PhoneNumberWrapper((PhoneNumber)_phoneNumber.clone());
	}

	public int compareTo(
		info.diit.portal.student.service.model.PhoneNumber phoneNumber) {
		return _phoneNumber.compareTo(phoneNumber);
	}

	@Override
	public int hashCode() {
		return _phoneNumber.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.student.service.model.PhoneNumber> toCacheModel() {
		return _phoneNumber.toCacheModel();
	}

	public info.diit.portal.student.service.model.PhoneNumber toEscapedModel() {
		return new PhoneNumberWrapper(_phoneNumber.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _phoneNumber.toString();
	}

	public java.lang.String toXmlString() {
		return _phoneNumber.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_phoneNumber.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public PhoneNumber getWrappedPhoneNumber() {
		return _phoneNumber;
	}

	public PhoneNumber getWrappedModel() {
		return _phoneNumber;
	}

	public void resetOriginalValues() {
		_phoneNumber.resetOriginalValues();
	}

	private PhoneNumber _phoneNumber;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the batch student local service. This utility wraps {@link info.diit.portal.student.service.service.impl.BatchStudentLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author nasimul
 * @see BatchStudentLocalService
 * @see info.diit.portal.student.service.service.base.BatchStudentLocalServiceBaseImpl
 * @see info.diit.portal.student.service.service.impl.BatchStudentLocalServiceImpl
 * @generated
 */
public class BatchStudentLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.student.service.service.impl.BatchStudentLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the batch student to the database. Also notifies the appropriate model listeners.
	*
	* @param batchStudent the batch student
	* @return the batch student that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent addBatchStudent(
		info.diit.portal.student.service.model.BatchStudent batchStudent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addBatchStudent(batchStudent);
	}

	/**
	* Creates a new batch student with the primary key. Does not add the batch student to the database.
	*
	* @param batchStudentId the primary key for the new batch student
	* @return the new batch student
	*/
	public static info.diit.portal.student.service.model.BatchStudent createBatchStudent(
		long batchStudentId) {
		return getService().createBatchStudent(batchStudentId);
	}

	/**
	* Deletes the batch student with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchStudentId the primary key of the batch student
	* @return the batch student that was removed
	* @throws PortalException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent deleteBatchStudent(
		long batchStudentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteBatchStudent(batchStudentId);
	}

	/**
	* Deletes the batch student from the database. Also notifies the appropriate model listeners.
	*
	* @param batchStudent the batch student
	* @return the batch student that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent deleteBatchStudent(
		info.diit.portal.student.service.model.BatchStudent batchStudent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteBatchStudent(batchStudent);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.student.service.model.BatchStudent fetchBatchStudent(
		long batchStudentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchBatchStudent(batchStudentId);
	}

	/**
	* Returns the batch student with the primary key.
	*
	* @param batchStudentId the primary key of the batch student
	* @return the batch student
	* @throws PortalException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent getBatchStudent(
		long batchStudentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getBatchStudent(batchStudentId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the batch students.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @return the range of batch students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> getBatchStudents(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBatchStudents(start, end);
	}

	/**
	* Returns the number of batch students.
	*
	* @return the number of batch students
	* @throws SystemException if a system exception occurred
	*/
	public static int getBatchStudentsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBatchStudentsCount();
	}

	/**
	* Updates the batch student in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param batchStudent the batch student
	* @return the batch student that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent updateBatchStudent(
		info.diit.portal.student.service.model.BatchStudent batchStudent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateBatchStudent(batchStudent);
	}

	/**
	* Updates the batch student in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param batchStudent the batch student
	* @param merge whether to merge the batch student with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the batch student that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.BatchStudent updateBatchStudent(
		info.diit.portal.student.service.model.BatchStudent batchStudent,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateBatchStudent(batchStudent, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findByBatch(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByBatch(batchId);
	}

	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findByCompanyOrganization(
		long companyId, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByCompanyOrganization(companyId, organizationId);
	}

	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findBystduentBatchStudentList(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findBystduentBatchStudentList(studentId);
	}

	public static java.util.List<info.diit.portal.student.service.model.BatchStudent> findAllStudentByOrgBatchId(
		long organizationId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findAllStudentByOrgBatchId(organizationId, batchId);
	}

	public static info.diit.portal.student.service.model.BatchStudent findIsStudentExistInBatch(
		long organizationId, long batchId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return getService()
				   .findIsStudentExistInBatch(organizationId, batchId, studentId);
	}

	public static void clearService() {
		_service = null;
	}

	public static BatchStudentLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					BatchStudentLocalService.class.getName());

			if (invokableLocalService instanceof BatchStudentLocalService) {
				_service = (BatchStudentLocalService)invokableLocalService;
			}
			else {
				_service = new BatchStudentLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(BatchStudentLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(BatchStudentLocalService service) {
	}

	private static BatchStudentLocalService _service;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link StatusHistory}.
 * </p>
 *
 * @author    nasimul
 * @see       StatusHistory
 * @generated
 */
public class StatusHistoryWrapper implements StatusHistory,
	ModelWrapper<StatusHistory> {
	public StatusHistoryWrapper(StatusHistory statusHistory) {
		_statusHistory = statusHistory;
	}

	public Class<?> getModelClass() {
		return StatusHistory.class;
	}

	public String getModelClassName() {
		return StatusHistory.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("statusHistoryId", getStatusHistoryId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("batchStudentId", getBatchStudentId());
		attributes.put("fromStatus", getFromStatus());
		attributes.put("toStatus", getToStatus());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long statusHistoryId = (Long)attributes.get("statusHistoryId");

		if (statusHistoryId != null) {
			setStatusHistoryId(statusHistoryId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long batchStudentId = (Long)attributes.get("batchStudentId");

		if (batchStudentId != null) {
			setBatchStudentId(batchStudentId);
		}

		Integer fromStatus = (Integer)attributes.get("fromStatus");

		if (fromStatus != null) {
			setFromStatus(fromStatus);
		}

		Integer toStatus = (Integer)attributes.get("toStatus");

		if (toStatus != null) {
			setToStatus(toStatus);
		}
	}

	/**
	* Returns the primary key of this status history.
	*
	* @return the primary key of this status history
	*/
	public long getPrimaryKey() {
		return _statusHistory.getPrimaryKey();
	}

	/**
	* Sets the primary key of this status history.
	*
	* @param primaryKey the primary key of this status history
	*/
	public void setPrimaryKey(long primaryKey) {
		_statusHistory.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the status history ID of this status history.
	*
	* @return the status history ID of this status history
	*/
	public long getStatusHistoryId() {
		return _statusHistory.getStatusHistoryId();
	}

	/**
	* Sets the status history ID of this status history.
	*
	* @param statusHistoryId the status history ID of this status history
	*/
	public void setStatusHistoryId(long statusHistoryId) {
		_statusHistory.setStatusHistoryId(statusHistoryId);
	}

	/**
	* Returns the company ID of this status history.
	*
	* @return the company ID of this status history
	*/
	public long getCompanyId() {
		return _statusHistory.getCompanyId();
	}

	/**
	* Sets the company ID of this status history.
	*
	* @param companyId the company ID of this status history
	*/
	public void setCompanyId(long companyId) {
		_statusHistory.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this status history.
	*
	* @return the user ID of this status history
	*/
	public long getUserId() {
		return _statusHistory.getUserId();
	}

	/**
	* Sets the user ID of this status history.
	*
	* @param userId the user ID of this status history
	*/
	public void setUserId(long userId) {
		_statusHistory.setUserId(userId);
	}

	/**
	* Returns the user uuid of this status history.
	*
	* @return the user uuid of this status history
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _statusHistory.getUserUuid();
	}

	/**
	* Sets the user uuid of this status history.
	*
	* @param userUuid the user uuid of this status history
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_statusHistory.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this status history.
	*
	* @return the user name of this status history
	*/
	public java.lang.String getUserName() {
		return _statusHistory.getUserName();
	}

	/**
	* Sets the user name of this status history.
	*
	* @param userName the user name of this status history
	*/
	public void setUserName(java.lang.String userName) {
		_statusHistory.setUserName(userName);
	}

	/**
	* Returns the create date of this status history.
	*
	* @return the create date of this status history
	*/
	public java.util.Date getCreateDate() {
		return _statusHistory.getCreateDate();
	}

	/**
	* Sets the create date of this status history.
	*
	* @param createDate the create date of this status history
	*/
	public void setCreateDate(java.util.Date createDate) {
		_statusHistory.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this status history.
	*
	* @return the modified date of this status history
	*/
	public java.util.Date getModifiedDate() {
		return _statusHistory.getModifiedDate();
	}

	/**
	* Sets the modified date of this status history.
	*
	* @param modifiedDate the modified date of this status history
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_statusHistory.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the organization ID of this status history.
	*
	* @return the organization ID of this status history
	*/
	public long getOrganizationId() {
		return _statusHistory.getOrganizationId();
	}

	/**
	* Sets the organization ID of this status history.
	*
	* @param organizationId the organization ID of this status history
	*/
	public void setOrganizationId(long organizationId) {
		_statusHistory.setOrganizationId(organizationId);
	}

	/**
	* Returns the batch student ID of this status history.
	*
	* @return the batch student ID of this status history
	*/
	public long getBatchStudentId() {
		return _statusHistory.getBatchStudentId();
	}

	/**
	* Sets the batch student ID of this status history.
	*
	* @param batchStudentId the batch student ID of this status history
	*/
	public void setBatchStudentId(long batchStudentId) {
		_statusHistory.setBatchStudentId(batchStudentId);
	}

	/**
	* Returns the from status of this status history.
	*
	* @return the from status of this status history
	*/
	public int getFromStatus() {
		return _statusHistory.getFromStatus();
	}

	/**
	* Sets the from status of this status history.
	*
	* @param fromStatus the from status of this status history
	*/
	public void setFromStatus(int fromStatus) {
		_statusHistory.setFromStatus(fromStatus);
	}

	/**
	* Returns the to status of this status history.
	*
	* @return the to status of this status history
	*/
	public int getToStatus() {
		return _statusHistory.getToStatus();
	}

	/**
	* Sets the to status of this status history.
	*
	* @param toStatus the to status of this status history
	*/
	public void setToStatus(int toStatus) {
		_statusHistory.setToStatus(toStatus);
	}

	public boolean isNew() {
		return _statusHistory.isNew();
	}

	public void setNew(boolean n) {
		_statusHistory.setNew(n);
	}

	public boolean isCachedModel() {
		return _statusHistory.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_statusHistory.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _statusHistory.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _statusHistory.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_statusHistory.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _statusHistory.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_statusHistory.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new StatusHistoryWrapper((StatusHistory)_statusHistory.clone());
	}

	public int compareTo(
		info.diit.portal.student.service.model.StatusHistory statusHistory) {
		return _statusHistory.compareTo(statusHistory);
	}

	@Override
	public int hashCode() {
		return _statusHistory.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.student.service.model.StatusHistory> toCacheModel() {
		return _statusHistory.toCacheModel();
	}

	public info.diit.portal.student.service.model.StatusHistory toEscapedModel() {
		return new StatusHistoryWrapper(_statusHistory.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _statusHistory.toString();
	}

	public java.lang.String toXmlString() {
		return _statusHistory.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_statusHistory.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public StatusHistory getWrappedStatusHistory() {
		return _statusHistory;
	}

	public StatusHistory getWrappedModel() {
		return _statusHistory;
	}

	public void resetOriginalValues() {
		_statusHistory.resetOriginalValues();
	}

	private StatusHistory _statusHistory;
}
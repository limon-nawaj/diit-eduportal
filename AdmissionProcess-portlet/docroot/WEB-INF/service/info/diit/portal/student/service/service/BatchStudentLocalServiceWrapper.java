/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link BatchStudentLocalService}.
 * </p>
 *
 * @author    nasimul
 * @see       BatchStudentLocalService
 * @generated
 */
public class BatchStudentLocalServiceWrapper implements BatchStudentLocalService,
	ServiceWrapper<BatchStudentLocalService> {
	public BatchStudentLocalServiceWrapper(
		BatchStudentLocalService batchStudentLocalService) {
		_batchStudentLocalService = batchStudentLocalService;
	}

	/**
	* Adds the batch student to the database. Also notifies the appropriate model listeners.
	*
	* @param batchStudent the batch student
	* @return the batch student that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.BatchStudent addBatchStudent(
		info.diit.portal.student.service.model.BatchStudent batchStudent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.addBatchStudent(batchStudent);
	}

	/**
	* Creates a new batch student with the primary key. Does not add the batch student to the database.
	*
	* @param batchStudentId the primary key for the new batch student
	* @return the new batch student
	*/
	public info.diit.portal.student.service.model.BatchStudent createBatchStudent(
		long batchStudentId) {
		return _batchStudentLocalService.createBatchStudent(batchStudentId);
	}

	/**
	* Deletes the batch student with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchStudentId the primary key of the batch student
	* @return the batch student that was removed
	* @throws PortalException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.BatchStudent deleteBatchStudent(
		long batchStudentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.deleteBatchStudent(batchStudentId);
	}

	/**
	* Deletes the batch student from the database. Also notifies the appropriate model listeners.
	*
	* @param batchStudent the batch student
	* @return the batch student that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.BatchStudent deleteBatchStudent(
		info.diit.portal.student.service.model.BatchStudent batchStudent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.deleteBatchStudent(batchStudent);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _batchStudentLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.student.service.model.BatchStudent fetchBatchStudent(
		long batchStudentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.fetchBatchStudent(batchStudentId);
	}

	/**
	* Returns the batch student with the primary key.
	*
	* @param batchStudentId the primary key of the batch student
	* @return the batch student
	* @throws PortalException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.BatchStudent getBatchStudent(
		long batchStudentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.getBatchStudent(batchStudentId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the batch students.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @return the range of batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.service.model.BatchStudent> getBatchStudents(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.getBatchStudents(start, end);
	}

	/**
	* Returns the number of batch students.
	*
	* @return the number of batch students
	* @throws SystemException if a system exception occurred
	*/
	public int getBatchStudentsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.getBatchStudentsCount();
	}

	/**
	* Updates the batch student in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param batchStudent the batch student
	* @return the batch student that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.BatchStudent updateBatchStudent(
		info.diit.portal.student.service.model.BatchStudent batchStudent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.updateBatchStudent(batchStudent);
	}

	/**
	* Updates the batch student in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param batchStudent the batch student
	* @param merge whether to merge the batch student with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the batch student that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.BatchStudent updateBatchStudent(
		info.diit.portal.student.service.model.BatchStudent batchStudent,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.updateBatchStudent(batchStudent, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _batchStudentLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_batchStudentLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _batchStudentLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.student.service.model.BatchStudent> findByBatch(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.findByBatch(batchId);
	}

	public java.util.List<info.diit.portal.student.service.model.BatchStudent> findByCompanyOrganization(
		long companyId, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.findByCompanyOrganization(companyId,
			organizationId);
	}

	public java.util.List<info.diit.portal.student.service.model.BatchStudent> findBystduentBatchStudentList(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.findBystduentBatchStudentList(studentId);
	}

	public java.util.List<info.diit.portal.student.service.model.BatchStudent> findAllStudentByOrgBatchId(
		long organizationId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudentLocalService.findAllStudentByOrgBatchId(organizationId,
			batchId);
	}

	public info.diit.portal.student.service.model.BatchStudent findIsStudentExistInBatch(
		long organizationId, long batchId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchBatchStudentException {
		return _batchStudentLocalService.findIsStudentExistInBatch(organizationId,
			batchId, studentId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public BatchStudentLocalService getWrappedBatchStudentLocalService() {
		return _batchStudentLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedBatchStudentLocalService(
		BatchStudentLocalService batchStudentLocalService) {
		_batchStudentLocalService = batchStudentLocalService;
	}

	public BatchStudentLocalService getWrappedService() {
		return _batchStudentLocalService;
	}

	public void setWrappedService(
		BatchStudentLocalService batchStudentLocalService) {
		_batchStudentLocalService = batchStudentLocalService;
	}

	private BatchStudentLocalService _batchStudentLocalService;
}
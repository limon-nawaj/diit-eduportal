/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.student.service.model.PersonEmail;

/**
 * The persistence interface for the person email service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see PersonEmailPersistenceImpl
 * @see PersonEmailUtil
 * @generated
 */
public interface PersonEmailPersistence extends BasePersistence<PersonEmail> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link PersonEmailUtil} to access the person email persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the person email in the entity cache if it is enabled.
	*
	* @param personEmail the person email
	*/
	public void cacheResult(
		info.diit.portal.student.service.model.PersonEmail personEmail);

	/**
	* Caches the person emails in the entity cache if it is enabled.
	*
	* @param personEmails the person emails
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.student.service.model.PersonEmail> personEmails);

	/**
	* Creates a new person email with the primary key. Does not add the person email to the database.
	*
	* @param personEmailId the primary key for the new person email
	* @return the new person email
	*/
	public info.diit.portal.student.service.model.PersonEmail create(
		long personEmailId);

	/**
	* Removes the person email with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param personEmailId the primary key of the person email
	* @return the person email that was removed
	* @throws info.diit.portal.student.service.NoSuchPersonEmailException if a person email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.PersonEmail remove(
		long personEmailId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchPersonEmailException;

	public info.diit.portal.student.service.model.PersonEmail updateImpl(
		info.diit.portal.student.service.model.PersonEmail personEmail,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the person email with the primary key or throws a {@link info.diit.portal.student.service.NoSuchPersonEmailException} if it could not be found.
	*
	* @param personEmailId the primary key of the person email
	* @return the person email
	* @throws info.diit.portal.student.service.NoSuchPersonEmailException if a person email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.PersonEmail findByPrimaryKey(
		long personEmailId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchPersonEmailException;

	/**
	* Returns the person email with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param personEmailId the primary key of the person email
	* @return the person email, or <code>null</code> if a person email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.PersonEmail fetchByPrimaryKey(
		long personEmailId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the person emails where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the matching person emails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.service.model.PersonEmail> findByStudentPersonEmailList(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the person emails where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of person emails
	* @param end the upper bound of the range of person emails (not inclusive)
	* @return the range of matching person emails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.service.model.PersonEmail> findByStudentPersonEmailList(
		long studentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the person emails where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of person emails
	* @param end the upper bound of the range of person emails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching person emails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.service.model.PersonEmail> findByStudentPersonEmailList(
		long studentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first person email in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching person email
	* @throws info.diit.portal.student.service.NoSuchPersonEmailException if a matching person email could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.PersonEmail findByStudentPersonEmailList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchPersonEmailException;

	/**
	* Returns the first person email in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching person email, or <code>null</code> if a matching person email could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.PersonEmail fetchByStudentPersonEmailList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last person email in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching person email
	* @throws info.diit.portal.student.service.NoSuchPersonEmailException if a matching person email could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.PersonEmail findByStudentPersonEmailList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchPersonEmailException;

	/**
	* Returns the last person email in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching person email, or <code>null</code> if a matching person email could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.PersonEmail fetchByStudentPersonEmailList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the person emails before and after the current person email in the ordered set where studentId = &#63;.
	*
	* @param personEmailId the primary key of the current person email
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next person email
	* @throws info.diit.portal.student.service.NoSuchPersonEmailException if a person email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.PersonEmail[] findByStudentPersonEmailList_PrevAndNext(
		long personEmailId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchPersonEmailException;

	/**
	* Returns all the person emails.
	*
	* @return the person emails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.service.model.PersonEmail> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the person emails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of person emails
	* @param end the upper bound of the range of person emails (not inclusive)
	* @return the range of person emails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.service.model.PersonEmail> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the person emails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of person emails
	* @param end the upper bound of the range of person emails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of person emails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.service.model.PersonEmail> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the person emails where studentId = &#63; from the database.
	*
	* @param studentId the student ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByStudentPersonEmailList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the person emails from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of person emails where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the number of matching person emails
	* @throws SystemException if a system exception occurred
	*/
	public int countByStudentPersonEmailList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of person emails.
	*
	* @return the number of person emails
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import info.diit.portal.student.service.model.AccademicRecordClp;
import info.diit.portal.student.service.model.BatchStudentClp;
import info.diit.portal.student.service.model.ExperianceClp;
import info.diit.portal.student.service.model.PersonEmailClp;
import info.diit.portal.student.service.model.PhoneNumberClp;
import info.diit.portal.student.service.model.StatusHistoryClp;
import info.diit.portal.student.service.model.StudentClp;
import info.diit.portal.student.service.model.StudentDocumentClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Brian Wing Shun Chan
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"AdmissionProcess-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"AdmissionProcess-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "AdmissionProcess-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(AccademicRecordClp.class.getName())) {
			return translateInputAccademicRecord(oldModel);
		}

		if (oldModelClassName.equals(BatchStudentClp.class.getName())) {
			return translateInputBatchStudent(oldModel);
		}

		if (oldModelClassName.equals(ExperianceClp.class.getName())) {
			return translateInputExperiance(oldModel);
		}

		if (oldModelClassName.equals(PersonEmailClp.class.getName())) {
			return translateInputPersonEmail(oldModel);
		}

		if (oldModelClassName.equals(PhoneNumberClp.class.getName())) {
			return translateInputPhoneNumber(oldModel);
		}

		if (oldModelClassName.equals(StatusHistoryClp.class.getName())) {
			return translateInputStatusHistory(oldModel);
		}

		if (oldModelClassName.equals(StudentClp.class.getName())) {
			return translateInputStudent(oldModel);
		}

		if (oldModelClassName.equals(StudentDocumentClp.class.getName())) {
			return translateInputStudentDocument(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputAccademicRecord(BaseModel<?> oldModel) {
		AccademicRecordClp oldClpModel = (AccademicRecordClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAccademicRecordRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputBatchStudent(BaseModel<?> oldModel) {
		BatchStudentClp oldClpModel = (BatchStudentClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBatchStudentRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputExperiance(BaseModel<?> oldModel) {
		ExperianceClp oldClpModel = (ExperianceClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getExperianceRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputPersonEmail(BaseModel<?> oldModel) {
		PersonEmailClp oldClpModel = (PersonEmailClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getPersonEmailRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputPhoneNumber(BaseModel<?> oldModel) {
		PhoneNumberClp oldClpModel = (PhoneNumberClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getPhoneNumberRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStatusHistory(BaseModel<?> oldModel) {
		StatusHistoryClp oldClpModel = (StatusHistoryClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStatusHistoryRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStudent(BaseModel<?> oldModel) {
		StudentClp oldClpModel = (StudentClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStudentRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStudentDocument(BaseModel<?> oldModel) {
		StudentDocumentClp oldClpModel = (StudentDocumentClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStudentDocumentRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(
					"info.diit.portal.student.service.model.impl.AccademicRecordImpl")) {
			return translateOutputAccademicRecord(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.student.service.model.impl.BatchStudentImpl")) {
			return translateOutputBatchStudent(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.student.service.model.impl.ExperianceImpl")) {
			return translateOutputExperiance(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.student.service.model.impl.PersonEmailImpl")) {
			return translateOutputPersonEmail(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.student.service.model.impl.PhoneNumberImpl")) {
			return translateOutputPhoneNumber(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.student.service.model.impl.StatusHistoryImpl")) {
			return translateOutputStatusHistory(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.student.service.model.impl.StudentImpl")) {
			return translateOutputStudent(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.student.service.model.impl.StudentDocumentImpl")) {
			return translateOutputStudentDocument(oldModel);
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals(
					"info.diit.portal.student.service.NoSuchAccademicRecordException")) {
			return new info.diit.portal.student.service.NoSuchAccademicRecordException();
		}

		if (className.equals(
					"info.diit.portal.student.service.NoSuchBatchStudentException")) {
			return new info.diit.portal.student.service.NoSuchBatchStudentException();
		}

		if (className.equals(
					"info.diit.portal.student.service.NoSuchExperianceException")) {
			return new info.diit.portal.student.service.NoSuchExperianceException();
		}

		if (className.equals(
					"info.diit.portal.student.service.NoSuchPersonEmailException")) {
			return new info.diit.portal.student.service.NoSuchPersonEmailException();
		}

		if (className.equals(
					"info.diit.portal.student.service.NoSuchPhoneNumberException")) {
			return new info.diit.portal.student.service.NoSuchPhoneNumberException();
		}

		if (className.equals(
					"info.diit.portal.student.service.NoSuchStatusHistoryException")) {
			return new info.diit.portal.student.service.NoSuchStatusHistoryException();
		}

		if (className.equals(
					"info.diit.portal.student.service.NoSuchStudentException")) {
			return new info.diit.portal.student.service.NoSuchStudentException();
		}

		if (className.equals(
					"info.diit.portal.student.service.NoSuchStudentDocumentException")) {
			return new info.diit.portal.student.service.NoSuchStudentDocumentException();
		}

		return throwable;
	}

	public static Object translateOutputAccademicRecord(BaseModel<?> oldModel) {
		AccademicRecordClp newModel = new AccademicRecordClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAccademicRecordRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputBatchStudent(BaseModel<?> oldModel) {
		BatchStudentClp newModel = new BatchStudentClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBatchStudentRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputExperiance(BaseModel<?> oldModel) {
		ExperianceClp newModel = new ExperianceClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setExperianceRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputPersonEmail(BaseModel<?> oldModel) {
		PersonEmailClp newModel = new PersonEmailClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setPersonEmailRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputPhoneNumber(BaseModel<?> oldModel) {
		PhoneNumberClp newModel = new PhoneNumberClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setPhoneNumberRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStatusHistory(BaseModel<?> oldModel) {
		StatusHistoryClp newModel = new StatusHistoryClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStatusHistoryRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStudent(BaseModel<?> oldModel) {
		StudentClp newModel = new StudentClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStudentRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStudentDocument(BaseModel<?> oldModel) {
		StudentDocumentClp newModel = new StudentDocumentClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStudentDocumentRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}
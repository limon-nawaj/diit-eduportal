/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link StudentDocument}.
 * </p>
 *
 * @author    nasimul
 * @see       StudentDocument
 * @generated
 */
public class StudentDocumentWrapper implements StudentDocument,
	ModelWrapper<StudentDocument> {
	public StudentDocumentWrapper(StudentDocument studentDocument) {
		_studentDocument = studentDocument;
	}

	public Class<?> getModelClass() {
		return StudentDocument.class;
	}

	public String getModelClassName() {
		return StudentDocument.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("documentId", getDocumentId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("type", getType());
		attributes.put("documentData", getDocumentData());
		attributes.put("description", getDescription());
		attributes.put("studentId", getStudentId());
		attributes.put("fileEntryId", getFileEntryId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long documentId = (Long)attributes.get("documentId");

		if (documentId != null) {
			setDocumentId(documentId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		Long documentData = (Long)attributes.get("documentData");

		if (documentData != null) {
			setDocumentData(documentData);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Long fileEntryId = (Long)attributes.get("fileEntryId");

		if (fileEntryId != null) {
			setFileEntryId(fileEntryId);
		}
	}

	/**
	* Returns the primary key of this student document.
	*
	* @return the primary key of this student document
	*/
	public long getPrimaryKey() {
		return _studentDocument.getPrimaryKey();
	}

	/**
	* Sets the primary key of this student document.
	*
	* @param primaryKey the primary key of this student document
	*/
	public void setPrimaryKey(long primaryKey) {
		_studentDocument.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the document ID of this student document.
	*
	* @return the document ID of this student document
	*/
	public long getDocumentId() {
		return _studentDocument.getDocumentId();
	}

	/**
	* Sets the document ID of this student document.
	*
	* @param documentId the document ID of this student document
	*/
	public void setDocumentId(long documentId) {
		_studentDocument.setDocumentId(documentId);
	}

	/**
	* Returns the company ID of this student document.
	*
	* @return the company ID of this student document
	*/
	public long getCompanyId() {
		return _studentDocument.getCompanyId();
	}

	/**
	* Sets the company ID of this student document.
	*
	* @param companyId the company ID of this student document
	*/
	public void setCompanyId(long companyId) {
		_studentDocument.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this student document.
	*
	* @return the user ID of this student document
	*/
	public long getUserId() {
		return _studentDocument.getUserId();
	}

	/**
	* Sets the user ID of this student document.
	*
	* @param userId the user ID of this student document
	*/
	public void setUserId(long userId) {
		_studentDocument.setUserId(userId);
	}

	/**
	* Returns the user uuid of this student document.
	*
	* @return the user uuid of this student document
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentDocument.getUserUuid();
	}

	/**
	* Sets the user uuid of this student document.
	*
	* @param userUuid the user uuid of this student document
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_studentDocument.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this student document.
	*
	* @return the user name of this student document
	*/
	public java.lang.String getUserName() {
		return _studentDocument.getUserName();
	}

	/**
	* Sets the user name of this student document.
	*
	* @param userName the user name of this student document
	*/
	public void setUserName(java.lang.String userName) {
		_studentDocument.setUserName(userName);
	}

	/**
	* Returns the create date of this student document.
	*
	* @return the create date of this student document
	*/
	public java.util.Date getCreateDate() {
		return _studentDocument.getCreateDate();
	}

	/**
	* Sets the create date of this student document.
	*
	* @param createDate the create date of this student document
	*/
	public void setCreateDate(java.util.Date createDate) {
		_studentDocument.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this student document.
	*
	* @return the modified date of this student document
	*/
	public java.util.Date getModifiedDate() {
		return _studentDocument.getModifiedDate();
	}

	/**
	* Sets the modified date of this student document.
	*
	* @param modifiedDate the modified date of this student document
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_studentDocument.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the organization ID of this student document.
	*
	* @return the organization ID of this student document
	*/
	public long getOrganizationId() {
		return _studentDocument.getOrganizationId();
	}

	/**
	* Sets the organization ID of this student document.
	*
	* @param organizationId the organization ID of this student document
	*/
	public void setOrganizationId(long organizationId) {
		_studentDocument.setOrganizationId(organizationId);
	}

	/**
	* Returns the type of this student document.
	*
	* @return the type of this student document
	*/
	public java.lang.String getType() {
		return _studentDocument.getType();
	}

	/**
	* Sets the type of this student document.
	*
	* @param type the type of this student document
	*/
	public void setType(java.lang.String type) {
		_studentDocument.setType(type);
	}

	/**
	* Returns the document data of this student document.
	*
	* @return the document data of this student document
	*/
	public long getDocumentData() {
		return _studentDocument.getDocumentData();
	}

	/**
	* Sets the document data of this student document.
	*
	* @param documentData the document data of this student document
	*/
	public void setDocumentData(long documentData) {
		_studentDocument.setDocumentData(documentData);
	}

	/**
	* Returns the description of this student document.
	*
	* @return the description of this student document
	*/
	public java.lang.String getDescription() {
		return _studentDocument.getDescription();
	}

	/**
	* Sets the description of this student document.
	*
	* @param description the description of this student document
	*/
	public void setDescription(java.lang.String description) {
		_studentDocument.setDescription(description);
	}

	/**
	* Returns the student ID of this student document.
	*
	* @return the student ID of this student document
	*/
	public long getStudentId() {
		return _studentDocument.getStudentId();
	}

	/**
	* Sets the student ID of this student document.
	*
	* @param studentId the student ID of this student document
	*/
	public void setStudentId(long studentId) {
		_studentDocument.setStudentId(studentId);
	}

	/**
	* Returns the file entry ID of this student document.
	*
	* @return the file entry ID of this student document
	*/
	public long getFileEntryId() {
		return _studentDocument.getFileEntryId();
	}

	/**
	* Sets the file entry ID of this student document.
	*
	* @param fileEntryId the file entry ID of this student document
	*/
	public void setFileEntryId(long fileEntryId) {
		_studentDocument.setFileEntryId(fileEntryId);
	}

	public boolean isNew() {
		return _studentDocument.isNew();
	}

	public void setNew(boolean n) {
		_studentDocument.setNew(n);
	}

	public boolean isCachedModel() {
		return _studentDocument.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_studentDocument.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _studentDocument.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _studentDocument.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_studentDocument.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _studentDocument.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_studentDocument.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new StudentDocumentWrapper((StudentDocument)_studentDocument.clone());
	}

	public int compareTo(
		info.diit.portal.student.service.model.StudentDocument studentDocument) {
		return _studentDocument.compareTo(studentDocument);
	}

	@Override
	public int hashCode() {
		return _studentDocument.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.student.service.model.StudentDocument> toCacheModel() {
		return _studentDocument.toCacheModel();
	}

	public info.diit.portal.student.service.model.StudentDocument toEscapedModel() {
		return new StudentDocumentWrapper(_studentDocument.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _studentDocument.toString();
	}

	public java.lang.String toXmlString() {
		return _studentDocument.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_studentDocument.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public StudentDocument getWrappedStudentDocument() {
		return _studentDocument;
	}

	public StudentDocument getWrappedModel() {
		return _studentDocument;
	}

	public void resetOriginalValues() {
		_studentDocument.resetOriginalValues();
	}

	private StudentDocument _studentDocument;
}
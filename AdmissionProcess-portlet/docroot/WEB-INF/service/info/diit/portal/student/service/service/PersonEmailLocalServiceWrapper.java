/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link PersonEmailLocalService}.
 * </p>
 *
 * @author    nasimul
 * @see       PersonEmailLocalService
 * @generated
 */
public class PersonEmailLocalServiceWrapper implements PersonEmailLocalService,
	ServiceWrapper<PersonEmailLocalService> {
	public PersonEmailLocalServiceWrapper(
		PersonEmailLocalService personEmailLocalService) {
		_personEmailLocalService = personEmailLocalService;
	}

	/**
	* Adds the person email to the database. Also notifies the appropriate model listeners.
	*
	* @param personEmail the person email
	* @return the person email that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.PersonEmail addPersonEmail(
		info.diit.portal.student.service.model.PersonEmail personEmail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.addPersonEmail(personEmail);
	}

	/**
	* Creates a new person email with the primary key. Does not add the person email to the database.
	*
	* @param personEmailId the primary key for the new person email
	* @return the new person email
	*/
	public info.diit.portal.student.service.model.PersonEmail createPersonEmail(
		long personEmailId) {
		return _personEmailLocalService.createPersonEmail(personEmailId);
	}

	/**
	* Deletes the person email with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param personEmailId the primary key of the person email
	* @return the person email that was removed
	* @throws PortalException if a person email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.PersonEmail deletePersonEmail(
		long personEmailId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.deletePersonEmail(personEmailId);
	}

	/**
	* Deletes the person email from the database. Also notifies the appropriate model listeners.
	*
	* @param personEmail the person email
	* @return the person email that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.PersonEmail deletePersonEmail(
		info.diit.portal.student.service.model.PersonEmail personEmail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.deletePersonEmail(personEmail);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _personEmailLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.student.service.model.PersonEmail fetchPersonEmail(
		long personEmailId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.fetchPersonEmail(personEmailId);
	}

	/**
	* Returns the person email with the primary key.
	*
	* @param personEmailId the primary key of the person email
	* @return the person email
	* @throws PortalException if a person email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.PersonEmail getPersonEmail(
		long personEmailId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.getPersonEmail(personEmailId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the person emails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of person emails
	* @param end the upper bound of the range of person emails (not inclusive)
	* @return the range of person emails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.service.model.PersonEmail> getPersonEmails(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.getPersonEmails(start, end);
	}

	/**
	* Returns the number of person emails.
	*
	* @return the number of person emails
	* @throws SystemException if a system exception occurred
	*/
	public int getPersonEmailsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.getPersonEmailsCount();
	}

	/**
	* Updates the person email in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param personEmail the person email
	* @return the person email that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.PersonEmail updatePersonEmail(
		info.diit.portal.student.service.model.PersonEmail personEmail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.updatePersonEmail(personEmail);
	}

	/**
	* Updates the person email in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param personEmail the person email
	* @param merge whether to merge the person email with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the person email that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.PersonEmail updatePersonEmail(
		info.diit.portal.student.service.model.PersonEmail personEmail,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.updatePersonEmail(personEmail, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _personEmailLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_personEmailLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _personEmailLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.student.service.model.PersonEmail> findByPersonEmailList(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.findByPersonEmailList(studentId);
	}

	public info.diit.portal.student.service.model.PersonEmail getPersonEmailCheck(
		java.lang.String email)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _personEmailLocalService.getPersonEmailCheck(email);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public PersonEmailLocalService getWrappedPersonEmailLocalService() {
		return _personEmailLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedPersonEmailLocalService(
		PersonEmailLocalService personEmailLocalService) {
		_personEmailLocalService = personEmailLocalService;
	}

	public PersonEmailLocalService getWrappedService() {
		return _personEmailLocalService;
	}

	public void setWrappedService(
		PersonEmailLocalService personEmailLocalService) {
		_personEmailLocalService = personEmailLocalService;
	}

	private PersonEmailLocalService _personEmailLocalService;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PersonEmail}.
 * </p>
 *
 * @author    nasimul
 * @see       PersonEmail
 * @generated
 */
public class PersonEmailWrapper implements PersonEmail,
	ModelWrapper<PersonEmail> {
	public PersonEmailWrapper(PersonEmail personEmail) {
		_personEmail = personEmail;
	}

	public Class<?> getModelClass() {
		return PersonEmail.class;
	}

	public String getModelClassName() {
		return PersonEmail.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("personEmailId", getPersonEmailId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("ownerType", getOwnerType());
		attributes.put("personEmail", getPersonEmail());
		attributes.put("studentId", getStudentId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long personEmailId = (Long)attributes.get("personEmailId");

		if (personEmailId != null) {
			setPersonEmailId(personEmailId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Integer ownerType = (Integer)attributes.get("ownerType");

		if (ownerType != null) {
			setOwnerType(ownerType);
		}

		String personEmail = (String)attributes.get("personEmail");

		if (personEmail != null) {
			setPersonEmail(personEmail);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}
	}

	/**
	* Returns the primary key of this person email.
	*
	* @return the primary key of this person email
	*/
	public long getPrimaryKey() {
		return _personEmail.getPrimaryKey();
	}

	/**
	* Sets the primary key of this person email.
	*
	* @param primaryKey the primary key of this person email
	*/
	public void setPrimaryKey(long primaryKey) {
		_personEmail.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the person email ID of this person email.
	*
	* @return the person email ID of this person email
	*/
	public long getPersonEmailId() {
		return _personEmail.getPersonEmailId();
	}

	/**
	* Sets the person email ID of this person email.
	*
	* @param personEmailId the person email ID of this person email
	*/
	public void setPersonEmailId(long personEmailId) {
		_personEmail.setPersonEmailId(personEmailId);
	}

	/**
	* Returns the company ID of this person email.
	*
	* @return the company ID of this person email
	*/
	public long getCompanyId() {
		return _personEmail.getCompanyId();
	}

	/**
	* Sets the company ID of this person email.
	*
	* @param companyId the company ID of this person email
	*/
	public void setCompanyId(long companyId) {
		_personEmail.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this person email.
	*
	* @return the user ID of this person email
	*/
	public long getUserId() {
		return _personEmail.getUserId();
	}

	/**
	* Sets the user ID of this person email.
	*
	* @param userId the user ID of this person email
	*/
	public void setUserId(long userId) {
		_personEmail.setUserId(userId);
	}

	/**
	* Returns the user uuid of this person email.
	*
	* @return the user uuid of this person email
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _personEmail.getUserUuid();
	}

	/**
	* Sets the user uuid of this person email.
	*
	* @param userUuid the user uuid of this person email
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_personEmail.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this person email.
	*
	* @return the user name of this person email
	*/
	public java.lang.String getUserName() {
		return _personEmail.getUserName();
	}

	/**
	* Sets the user name of this person email.
	*
	* @param userName the user name of this person email
	*/
	public void setUserName(java.lang.String userName) {
		_personEmail.setUserName(userName);
	}

	/**
	* Returns the create date of this person email.
	*
	* @return the create date of this person email
	*/
	public java.util.Date getCreateDate() {
		return _personEmail.getCreateDate();
	}

	/**
	* Sets the create date of this person email.
	*
	* @param createDate the create date of this person email
	*/
	public void setCreateDate(java.util.Date createDate) {
		_personEmail.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this person email.
	*
	* @return the modified date of this person email
	*/
	public java.util.Date getModifiedDate() {
		return _personEmail.getModifiedDate();
	}

	/**
	* Sets the modified date of this person email.
	*
	* @param modifiedDate the modified date of this person email
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_personEmail.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the organization ID of this person email.
	*
	* @return the organization ID of this person email
	*/
	public long getOrganizationId() {
		return _personEmail.getOrganizationId();
	}

	/**
	* Sets the organization ID of this person email.
	*
	* @param organizationId the organization ID of this person email
	*/
	public void setOrganizationId(long organizationId) {
		_personEmail.setOrganizationId(organizationId);
	}

	/**
	* Returns the owner type of this person email.
	*
	* @return the owner type of this person email
	*/
	public int getOwnerType() {
		return _personEmail.getOwnerType();
	}

	/**
	* Sets the owner type of this person email.
	*
	* @param ownerType the owner type of this person email
	*/
	public void setOwnerType(int ownerType) {
		_personEmail.setOwnerType(ownerType);
	}

	/**
	* Returns the person email of this person email.
	*
	* @return the person email of this person email
	*/
	public java.lang.String getPersonEmail() {
		return _personEmail.getPersonEmail();
	}

	/**
	* Sets the person email of this person email.
	*
	* @param personEmail the person email of this person email
	*/
	public void setPersonEmail(java.lang.String personEmail) {
		_personEmail.setPersonEmail(personEmail);
	}

	/**
	* Returns the student ID of this person email.
	*
	* @return the student ID of this person email
	*/
	public long getStudentId() {
		return _personEmail.getStudentId();
	}

	/**
	* Sets the student ID of this person email.
	*
	* @param studentId the student ID of this person email
	*/
	public void setStudentId(long studentId) {
		_personEmail.setStudentId(studentId);
	}

	public boolean isNew() {
		return _personEmail.isNew();
	}

	public void setNew(boolean n) {
		_personEmail.setNew(n);
	}

	public boolean isCachedModel() {
		return _personEmail.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_personEmail.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _personEmail.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _personEmail.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_personEmail.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _personEmail.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_personEmail.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new PersonEmailWrapper((PersonEmail)_personEmail.clone());
	}

	public int compareTo(
		info.diit.portal.student.service.model.PersonEmail personEmail) {
		return _personEmail.compareTo(personEmail);
	}

	@Override
	public int hashCode() {
		return _personEmail.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.student.service.model.PersonEmail> toCacheModel() {
		return _personEmail.toCacheModel();
	}

	public info.diit.portal.student.service.model.PersonEmail toEscapedModel() {
		return new PersonEmailWrapper(_personEmail.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _personEmail.toString();
	}

	public java.lang.String toXmlString() {
		return _personEmail.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_personEmail.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public PersonEmail getWrappedPersonEmail() {
		return _personEmail;
	}

	public PersonEmail getWrappedModel() {
		return _personEmail;
	}

	public void resetOriginalValues() {
		_personEmail.resetOriginalValues();
	}

	private PersonEmail _personEmail;
}
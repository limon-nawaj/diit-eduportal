/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the status history local service. This utility wraps {@link info.diit.portal.student.service.service.impl.StatusHistoryLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author nasimul
 * @see StatusHistoryLocalService
 * @see info.diit.portal.student.service.service.base.StatusHistoryLocalServiceBaseImpl
 * @see info.diit.portal.student.service.service.impl.StatusHistoryLocalServiceImpl
 * @generated
 */
public class StatusHistoryLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.student.service.service.impl.StatusHistoryLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the status history to the database. Also notifies the appropriate model listeners.
	*
	* @param statusHistory the status history
	* @return the status history that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StatusHistory addStatusHistory(
		info.diit.portal.student.service.model.StatusHistory statusHistory)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addStatusHistory(statusHistory);
	}

	/**
	* Creates a new status history with the primary key. Does not add the status history to the database.
	*
	* @param statusHistoryId the primary key for the new status history
	* @return the new status history
	*/
	public static info.diit.portal.student.service.model.StatusHistory createStatusHistory(
		long statusHistoryId) {
		return getService().createStatusHistory(statusHistoryId);
	}

	/**
	* Deletes the status history with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param statusHistoryId the primary key of the status history
	* @return the status history that was removed
	* @throws PortalException if a status history with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StatusHistory deleteStatusHistory(
		long statusHistoryId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteStatusHistory(statusHistoryId);
	}

	/**
	* Deletes the status history from the database. Also notifies the appropriate model listeners.
	*
	* @param statusHistory the status history
	* @return the status history that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StatusHistory deleteStatusHistory(
		info.diit.portal.student.service.model.StatusHistory statusHistory)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteStatusHistory(statusHistory);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.student.service.model.StatusHistory fetchStatusHistory(
		long statusHistoryId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchStatusHistory(statusHistoryId);
	}

	/**
	* Returns the status history with the primary key.
	*
	* @param statusHistoryId the primary key of the status history
	* @return the status history
	* @throws PortalException if a status history with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StatusHistory getStatusHistory(
		long statusHistoryId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getStatusHistory(statusHistoryId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the status histories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of status histories
	* @param end the upper bound of the range of status histories (not inclusive)
	* @return the range of status histories
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.StatusHistory> getStatusHistories(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getStatusHistories(start, end);
	}

	/**
	* Returns the number of status histories.
	*
	* @return the number of status histories
	* @throws SystemException if a system exception occurred
	*/
	public static int getStatusHistoriesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getStatusHistoriesCount();
	}

	/**
	* Updates the status history in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param statusHistory the status history
	* @return the status history that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StatusHistory updateStatusHistory(
		info.diit.portal.student.service.model.StatusHistory statusHistory)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateStatusHistory(statusHistory);
	}

	/**
	* Updates the status history in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param statusHistory the status history
	* @param merge whether to merge the status history with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the status history that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StatusHistory updateStatusHistory(
		info.diit.portal.student.service.model.StatusHistory statusHistory,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateStatusHistory(statusHistory, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static void clearService() {
		_service = null;
	}

	public static StatusHistoryLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					StatusHistoryLocalService.class.getName());

			if (invokableLocalService instanceof StatusHistoryLocalService) {
				_service = (StatusHistoryLocalService)invokableLocalService;
			}
			else {
				_service = new StatusHistoryLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(StatusHistoryLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(StatusHistoryLocalService service) {
	}

	private static StatusHistoryLocalService _service;
}
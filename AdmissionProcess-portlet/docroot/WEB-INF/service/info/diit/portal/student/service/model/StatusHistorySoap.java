/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    nasimul
 * @generated
 */
public class StatusHistorySoap implements Serializable {
	public static StatusHistorySoap toSoapModel(StatusHistory model) {
		StatusHistorySoap soapModel = new StatusHistorySoap();

		soapModel.setStatusHistoryId(model.getStatusHistoryId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setBatchStudentId(model.getBatchStudentId());
		soapModel.setFromStatus(model.getFromStatus());
		soapModel.setToStatus(model.getToStatus());

		return soapModel;
	}

	public static StatusHistorySoap[] toSoapModels(StatusHistory[] models) {
		StatusHistorySoap[] soapModels = new StatusHistorySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static StatusHistorySoap[][] toSoapModels(StatusHistory[][] models) {
		StatusHistorySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new StatusHistorySoap[models.length][models[0].length];
		}
		else {
			soapModels = new StatusHistorySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static StatusHistorySoap[] toSoapModels(List<StatusHistory> models) {
		List<StatusHistorySoap> soapModels = new ArrayList<StatusHistorySoap>(models.size());

		for (StatusHistory model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new StatusHistorySoap[soapModels.size()]);
	}

	public StatusHistorySoap() {
	}

	public long getPrimaryKey() {
		return _statusHistoryId;
	}

	public void setPrimaryKey(long pk) {
		setStatusHistoryId(pk);
	}

	public long getStatusHistoryId() {
		return _statusHistoryId;
	}

	public void setStatusHistoryId(long statusHistoryId) {
		_statusHistoryId = statusHistoryId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getBatchStudentId() {
		return _batchStudentId;
	}

	public void setBatchStudentId(long batchStudentId) {
		_batchStudentId = batchStudentId;
	}

	public int getFromStatus() {
		return _fromStatus;
	}

	public void setFromStatus(int fromStatus) {
		_fromStatus = fromStatus;
	}

	public int getToStatus() {
		return _toStatus;
	}

	public void setToStatus(int toStatus) {
		_toStatus = toStatus;
	}

	private long _statusHistoryId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private long _batchStudentId;
	private int _fromStatus;
	private int _toStatus;
}
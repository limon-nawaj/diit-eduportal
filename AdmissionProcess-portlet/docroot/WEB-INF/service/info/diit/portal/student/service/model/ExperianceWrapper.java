/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Experiance}.
 * </p>
 *
 * @author    nasimul
 * @see       Experiance
 * @generated
 */
public class ExperianceWrapper implements Experiance, ModelWrapper<Experiance> {
	public ExperianceWrapper(Experiance experiance) {
		_experiance = experiance;
	}

	public Class<?> getModelClass() {
		return Experiance.class;
	}

	public String getModelClassName() {
		return Experiance.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("experianceId", getExperianceId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("organization", getOrganization());
		attributes.put("designation", getDesignation());
		attributes.put("startDate", getStartDate());
		attributes.put("endDate", getEndDate());
		attributes.put("currentStatus", getCurrentStatus());
		attributes.put("studentId", getStudentId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long experianceId = (Long)attributes.get("experianceId");

		if (experianceId != null) {
			setExperianceId(experianceId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		String organization = (String)attributes.get("organization");

		if (organization != null) {
			setOrganization(organization);
		}

		String designation = (String)attributes.get("designation");

		if (designation != null) {
			setDesignation(designation);
		}

		Date startDate = (Date)attributes.get("startDate");

		if (startDate != null) {
			setStartDate(startDate);
		}

		Date endDate = (Date)attributes.get("endDate");

		if (endDate != null) {
			setEndDate(endDate);
		}

		Integer currentStatus = (Integer)attributes.get("currentStatus");

		if (currentStatus != null) {
			setCurrentStatus(currentStatus);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}
	}

	/**
	* Returns the primary key of this experiance.
	*
	* @return the primary key of this experiance
	*/
	public long getPrimaryKey() {
		return _experiance.getPrimaryKey();
	}

	/**
	* Sets the primary key of this experiance.
	*
	* @param primaryKey the primary key of this experiance
	*/
	public void setPrimaryKey(long primaryKey) {
		_experiance.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the experiance ID of this experiance.
	*
	* @return the experiance ID of this experiance
	*/
	public long getExperianceId() {
		return _experiance.getExperianceId();
	}

	/**
	* Sets the experiance ID of this experiance.
	*
	* @param experianceId the experiance ID of this experiance
	*/
	public void setExperianceId(long experianceId) {
		_experiance.setExperianceId(experianceId);
	}

	/**
	* Returns the company ID of this experiance.
	*
	* @return the company ID of this experiance
	*/
	public long getCompanyId() {
		return _experiance.getCompanyId();
	}

	/**
	* Sets the company ID of this experiance.
	*
	* @param companyId the company ID of this experiance
	*/
	public void setCompanyId(long companyId) {
		_experiance.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this experiance.
	*
	* @return the user ID of this experiance
	*/
	public long getUserId() {
		return _experiance.getUserId();
	}

	/**
	* Sets the user ID of this experiance.
	*
	* @param userId the user ID of this experiance
	*/
	public void setUserId(long userId) {
		_experiance.setUserId(userId);
	}

	/**
	* Returns the user uuid of this experiance.
	*
	* @return the user uuid of this experiance
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _experiance.getUserUuid();
	}

	/**
	* Sets the user uuid of this experiance.
	*
	* @param userUuid the user uuid of this experiance
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_experiance.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this experiance.
	*
	* @return the user name of this experiance
	*/
	public java.lang.String getUserName() {
		return _experiance.getUserName();
	}

	/**
	* Sets the user name of this experiance.
	*
	* @param userName the user name of this experiance
	*/
	public void setUserName(java.lang.String userName) {
		_experiance.setUserName(userName);
	}

	/**
	* Returns the create date of this experiance.
	*
	* @return the create date of this experiance
	*/
	public java.util.Date getCreateDate() {
		return _experiance.getCreateDate();
	}

	/**
	* Sets the create date of this experiance.
	*
	* @param createDate the create date of this experiance
	*/
	public void setCreateDate(java.util.Date createDate) {
		_experiance.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this experiance.
	*
	* @return the modified date of this experiance
	*/
	public java.util.Date getModifiedDate() {
		return _experiance.getModifiedDate();
	}

	/**
	* Sets the modified date of this experiance.
	*
	* @param modifiedDate the modified date of this experiance
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_experiance.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the organization ID of this experiance.
	*
	* @return the organization ID of this experiance
	*/
	public long getOrganizationId() {
		return _experiance.getOrganizationId();
	}

	/**
	* Sets the organization ID of this experiance.
	*
	* @param organizationId the organization ID of this experiance
	*/
	public void setOrganizationId(long organizationId) {
		_experiance.setOrganizationId(organizationId);
	}

	/**
	* Returns the organization of this experiance.
	*
	* @return the organization of this experiance
	*/
	public java.lang.String getOrganization() {
		return _experiance.getOrganization();
	}

	/**
	* Sets the organization of this experiance.
	*
	* @param organization the organization of this experiance
	*/
	public void setOrganization(java.lang.String organization) {
		_experiance.setOrganization(organization);
	}

	/**
	* Returns the designation of this experiance.
	*
	* @return the designation of this experiance
	*/
	public java.lang.String getDesignation() {
		return _experiance.getDesignation();
	}

	/**
	* Sets the designation of this experiance.
	*
	* @param designation the designation of this experiance
	*/
	public void setDesignation(java.lang.String designation) {
		_experiance.setDesignation(designation);
	}

	/**
	* Returns the start date of this experiance.
	*
	* @return the start date of this experiance
	*/
	public java.util.Date getStartDate() {
		return _experiance.getStartDate();
	}

	/**
	* Sets the start date of this experiance.
	*
	* @param startDate the start date of this experiance
	*/
	public void setStartDate(java.util.Date startDate) {
		_experiance.setStartDate(startDate);
	}

	/**
	* Returns the end date of this experiance.
	*
	* @return the end date of this experiance
	*/
	public java.util.Date getEndDate() {
		return _experiance.getEndDate();
	}

	/**
	* Sets the end date of this experiance.
	*
	* @param endDate the end date of this experiance
	*/
	public void setEndDate(java.util.Date endDate) {
		_experiance.setEndDate(endDate);
	}

	/**
	* Returns the current status of this experiance.
	*
	* @return the current status of this experiance
	*/
	public int getCurrentStatus() {
		return _experiance.getCurrentStatus();
	}

	/**
	* Sets the current status of this experiance.
	*
	* @param currentStatus the current status of this experiance
	*/
	public void setCurrentStatus(int currentStatus) {
		_experiance.setCurrentStatus(currentStatus);
	}

	/**
	* Returns the student ID of this experiance.
	*
	* @return the student ID of this experiance
	*/
	public long getStudentId() {
		return _experiance.getStudentId();
	}

	/**
	* Sets the student ID of this experiance.
	*
	* @param studentId the student ID of this experiance
	*/
	public void setStudentId(long studentId) {
		_experiance.setStudentId(studentId);
	}

	public boolean isNew() {
		return _experiance.isNew();
	}

	public void setNew(boolean n) {
		_experiance.setNew(n);
	}

	public boolean isCachedModel() {
		return _experiance.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_experiance.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _experiance.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _experiance.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_experiance.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _experiance.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_experiance.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ExperianceWrapper((Experiance)_experiance.clone());
	}

	public int compareTo(
		info.diit.portal.student.service.model.Experiance experiance) {
		return _experiance.compareTo(experiance);
	}

	@Override
	public int hashCode() {
		return _experiance.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.student.service.model.Experiance> toCacheModel() {
		return _experiance.toCacheModel();
	}

	public info.diit.portal.student.service.model.Experiance toEscapedModel() {
		return new ExperianceWrapper(_experiance.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _experiance.toString();
	}

	public java.lang.String toXmlString() {
		return _experiance.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_experiance.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Experiance getWrappedExperiance() {
		return _experiance;
	}

	public Experiance getWrappedModel() {
		return _experiance;
	}

	public void resetOriginalValues() {
		_experiance.resetOriginalValues();
	}

	private Experiance _experiance;
}
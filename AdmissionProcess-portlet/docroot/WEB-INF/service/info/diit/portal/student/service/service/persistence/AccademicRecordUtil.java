/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.student.service.model.AccademicRecord;

import java.util.List;

/**
 * The persistence utility for the accademic record service. This utility wraps {@link AccademicRecordPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see AccademicRecordPersistence
 * @see AccademicRecordPersistenceImpl
 * @generated
 */
public class AccademicRecordUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(AccademicRecord accademicRecord) {
		getPersistence().clearCache(accademicRecord);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<AccademicRecord> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<AccademicRecord> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<AccademicRecord> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static AccademicRecord update(AccademicRecord accademicRecord,
		boolean merge) throws SystemException {
		return getPersistence().update(accademicRecord, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static AccademicRecord update(AccademicRecord accademicRecord,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(accademicRecord, merge, serviceContext);
	}

	/**
	* Caches the accademic record in the entity cache if it is enabled.
	*
	* @param accademicRecord the accademic record
	*/
	public static void cacheResult(
		info.diit.portal.student.service.model.AccademicRecord accademicRecord) {
		getPersistence().cacheResult(accademicRecord);
	}

	/**
	* Caches the accademic records in the entity cache if it is enabled.
	*
	* @param accademicRecords the accademic records
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.student.service.model.AccademicRecord> accademicRecords) {
		getPersistence().cacheResult(accademicRecords);
	}

	/**
	* Creates a new accademic record with the primary key. Does not add the accademic record to the database.
	*
	* @param accademicRecordId the primary key for the new accademic record
	* @return the new accademic record
	*/
	public static info.diit.portal.student.service.model.AccademicRecord create(
		long accademicRecordId) {
		return getPersistence().create(accademicRecordId);
	}

	/**
	* Removes the accademic record with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param accademicRecordId the primary key of the accademic record
	* @return the accademic record that was removed
	* @throws info.diit.portal.student.service.NoSuchAccademicRecordException if a accademic record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.AccademicRecord remove(
		long accademicRecordId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchAccademicRecordException {
		return getPersistence().remove(accademicRecordId);
	}

	public static info.diit.portal.student.service.model.AccademicRecord updateImpl(
		info.diit.portal.student.service.model.AccademicRecord accademicRecord,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(accademicRecord, merge);
	}

	/**
	* Returns the accademic record with the primary key or throws a {@link info.diit.portal.student.service.NoSuchAccademicRecordException} if it could not be found.
	*
	* @param accademicRecordId the primary key of the accademic record
	* @return the accademic record
	* @throws info.diit.portal.student.service.NoSuchAccademicRecordException if a accademic record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.AccademicRecord findByPrimaryKey(
		long accademicRecordId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchAccademicRecordException {
		return getPersistence().findByPrimaryKey(accademicRecordId);
	}

	/**
	* Returns the accademic record with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param accademicRecordId the primary key of the accademic record
	* @return the accademic record, or <code>null</code> if a accademic record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.AccademicRecord fetchByPrimaryKey(
		long accademicRecordId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(accademicRecordId);
	}

	/**
	* Returns all the accademic records where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the matching accademic records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.AccademicRecord> findByStudentAccademicRecordList(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStudentAccademicRecordList(studentId);
	}

	/**
	* Returns a range of all the accademic records where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of accademic records
	* @param end the upper bound of the range of accademic records (not inclusive)
	* @return the range of matching accademic records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.AccademicRecord> findByStudentAccademicRecordList(
		long studentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentAccademicRecordList(studentId, start, end);
	}

	/**
	* Returns an ordered range of all the accademic records where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of accademic records
	* @param end the upper bound of the range of accademic records (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching accademic records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.AccademicRecord> findByStudentAccademicRecordList(
		long studentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentAccademicRecordList(studentId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first accademic record in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching accademic record
	* @throws info.diit.portal.student.service.NoSuchAccademicRecordException if a matching accademic record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.AccademicRecord findByStudentAccademicRecordList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchAccademicRecordException {
		return getPersistence()
				   .findByStudentAccademicRecordList_First(studentId,
			orderByComparator);
	}

	/**
	* Returns the first accademic record in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching accademic record, or <code>null</code> if a matching accademic record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.AccademicRecord fetchByStudentAccademicRecordList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentAccademicRecordList_First(studentId,
			orderByComparator);
	}

	/**
	* Returns the last accademic record in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching accademic record
	* @throws info.diit.portal.student.service.NoSuchAccademicRecordException if a matching accademic record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.AccademicRecord findByStudentAccademicRecordList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchAccademicRecordException {
		return getPersistence()
				   .findByStudentAccademicRecordList_Last(studentId,
			orderByComparator);
	}

	/**
	* Returns the last accademic record in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching accademic record, or <code>null</code> if a matching accademic record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.AccademicRecord fetchByStudentAccademicRecordList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentAccademicRecordList_Last(studentId,
			orderByComparator);
	}

	/**
	* Returns the accademic records before and after the current accademic record in the ordered set where studentId = &#63;.
	*
	* @param accademicRecordId the primary key of the current accademic record
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next accademic record
	* @throws info.diit.portal.student.service.NoSuchAccademicRecordException if a accademic record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.AccademicRecord[] findByStudentAccademicRecordList_PrevAndNext(
		long accademicRecordId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchAccademicRecordException {
		return getPersistence()
				   .findByStudentAccademicRecordList_PrevAndNext(accademicRecordId,
			studentId, orderByComparator);
	}

	/**
	* Returns all the accademic records.
	*
	* @return the accademic records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.AccademicRecord> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the accademic records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of accademic records
	* @param end the upper bound of the range of accademic records (not inclusive)
	* @return the range of accademic records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.AccademicRecord> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the accademic records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of accademic records
	* @param end the upper bound of the range of accademic records (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of accademic records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.AccademicRecord> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the accademic records where studentId = &#63; from the database.
	*
	* @param studentId the student ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByStudentAccademicRecordList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByStudentAccademicRecordList(studentId);
	}

	/**
	* Removes all the accademic records from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of accademic records where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the number of matching accademic records
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStudentAccademicRecordList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStudentAccademicRecordList(studentId);
	}

	/**
	* Returns the number of accademic records.
	*
	* @return the number of accademic records
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static AccademicRecordPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (AccademicRecordPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.student.service.service.ClpSerializer.getServletContextName(),
					AccademicRecordPersistence.class.getName());

			ReferenceRegistry.registerReference(AccademicRecordUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(AccademicRecordPersistence persistence) {
	}

	private static AccademicRecordPersistence _persistence;
}
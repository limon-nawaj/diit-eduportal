/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.student.service.model.StudentDocument;

import java.util.List;

/**
 * The persistence utility for the student document service. This utility wraps {@link StudentDocumentPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see StudentDocumentPersistence
 * @see StudentDocumentPersistenceImpl
 * @generated
 */
public class StudentDocumentUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(StudentDocument studentDocument) {
		getPersistence().clearCache(studentDocument);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<StudentDocument> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<StudentDocument> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<StudentDocument> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static StudentDocument update(StudentDocument studentDocument,
		boolean merge) throws SystemException {
		return getPersistence().update(studentDocument, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static StudentDocument update(StudentDocument studentDocument,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(studentDocument, merge, serviceContext);
	}

	/**
	* Caches the student document in the entity cache if it is enabled.
	*
	* @param studentDocument the student document
	*/
	public static void cacheResult(
		info.diit.portal.student.service.model.StudentDocument studentDocument) {
		getPersistence().cacheResult(studentDocument);
	}

	/**
	* Caches the student documents in the entity cache if it is enabled.
	*
	* @param studentDocuments the student documents
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.student.service.model.StudentDocument> studentDocuments) {
		getPersistence().cacheResult(studentDocuments);
	}

	/**
	* Creates a new student document with the primary key. Does not add the student document to the database.
	*
	* @param documentId the primary key for the new student document
	* @return the new student document
	*/
	public static info.diit.portal.student.service.model.StudentDocument create(
		long documentId) {
		return getPersistence().create(documentId);
	}

	/**
	* Removes the student document with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param documentId the primary key of the student document
	* @return the student document that was removed
	* @throws info.diit.portal.student.service.NoSuchStudentDocumentException if a student document with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StudentDocument remove(
		long documentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchStudentDocumentException {
		return getPersistence().remove(documentId);
	}

	public static info.diit.portal.student.service.model.StudentDocument updateImpl(
		info.diit.portal.student.service.model.StudentDocument studentDocument,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(studentDocument, merge);
	}

	/**
	* Returns the student document with the primary key or throws a {@link info.diit.portal.student.service.NoSuchStudentDocumentException} if it could not be found.
	*
	* @param documentId the primary key of the student document
	* @return the student document
	* @throws info.diit.portal.student.service.NoSuchStudentDocumentException if a student document with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StudentDocument findByPrimaryKey(
		long documentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchStudentDocumentException {
		return getPersistence().findByPrimaryKey(documentId);
	}

	/**
	* Returns the student document with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param documentId the primary key of the student document
	* @return the student document, or <code>null</code> if a student document with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StudentDocument fetchByPrimaryKey(
		long documentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(documentId);
	}

	/**
	* Returns the student document where documentId = &#63; or throws a {@link info.diit.portal.student.service.NoSuchStudentDocumentException} if it could not be found.
	*
	* @param documentId the document ID
	* @return the matching student document
	* @throws info.diit.portal.student.service.NoSuchStudentDocumentException if a matching student document could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StudentDocument findByStudentDocument(
		long documentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchStudentDocumentException {
		return getPersistence().findByStudentDocument(documentId);
	}

	/**
	* Returns the student document where documentId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param documentId the document ID
	* @return the matching student document, or <code>null</code> if a matching student document could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StudentDocument fetchByStudentDocument(
		long documentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByStudentDocument(documentId);
	}

	/**
	* Returns the student document where documentId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param documentId the document ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching student document, or <code>null</code> if a matching student document could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StudentDocument fetchByStudentDocument(
		long documentId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentDocument(documentId, retrieveFromCache);
	}

	/**
	* Returns all the student documents where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the matching student documents
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.StudentDocument> findByStudentDocumentList(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStudentDocumentList(studentId);
	}

	/**
	* Returns a range of all the student documents where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of student documents
	* @param end the upper bound of the range of student documents (not inclusive)
	* @return the range of matching student documents
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.StudentDocument> findByStudentDocumentList(
		long studentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStudentDocumentList(studentId, start, end);
	}

	/**
	* Returns an ordered range of all the student documents where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of student documents
	* @param end the upper bound of the range of student documents (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching student documents
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.StudentDocument> findByStudentDocumentList(
		long studentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentDocumentList(studentId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first student document in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student document
	* @throws info.diit.portal.student.service.NoSuchStudentDocumentException if a matching student document could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StudentDocument findByStudentDocumentList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchStudentDocumentException {
		return getPersistence()
				   .findByStudentDocumentList_First(studentId, orderByComparator);
	}

	/**
	* Returns the first student document in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student document, or <code>null</code> if a matching student document could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StudentDocument fetchByStudentDocumentList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentDocumentList_First(studentId,
			orderByComparator);
	}

	/**
	* Returns the last student document in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student document
	* @throws info.diit.portal.student.service.NoSuchStudentDocumentException if a matching student document could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StudentDocument findByStudentDocumentList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchStudentDocumentException {
		return getPersistence()
				   .findByStudentDocumentList_Last(studentId, orderByComparator);
	}

	/**
	* Returns the last student document in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student document, or <code>null</code> if a matching student document could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StudentDocument fetchByStudentDocumentList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentDocumentList_Last(studentId, orderByComparator);
	}

	/**
	* Returns the student documents before and after the current student document in the ordered set where studentId = &#63;.
	*
	* @param documentId the primary key of the current student document
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next student document
	* @throws info.diit.portal.student.service.NoSuchStudentDocumentException if a student document with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StudentDocument[] findByStudentDocumentList_PrevAndNext(
		long documentId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchStudentDocumentException {
		return getPersistence()
				   .findByStudentDocumentList_PrevAndNext(documentId,
			studentId, orderByComparator);
	}

	/**
	* Returns all the student documents.
	*
	* @return the student documents
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.StudentDocument> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the student documents.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student documents
	* @param end the upper bound of the range of student documents (not inclusive)
	* @return the range of student documents
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.StudentDocument> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the student documents.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student documents
	* @param end the upper bound of the range of student documents (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of student documents
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.service.model.StudentDocument> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes the student document where documentId = &#63; from the database.
	*
	* @param documentId the document ID
	* @return the student document that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.service.model.StudentDocument removeByStudentDocument(
		long documentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchStudentDocumentException {
		return getPersistence().removeByStudentDocument(documentId);
	}

	/**
	* Removes all the student documents where studentId = &#63; from the database.
	*
	* @param studentId the student ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByStudentDocumentList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByStudentDocumentList(studentId);
	}

	/**
	* Removes all the student documents from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of student documents where documentId = &#63;.
	*
	* @param documentId the document ID
	* @return the number of matching student documents
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStudentDocument(long documentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStudentDocument(documentId);
	}

	/**
	* Returns the number of student documents where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the number of matching student documents
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStudentDocumentList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStudentDocumentList(studentId);
	}

	/**
	* Returns the number of student documents.
	*
	* @return the number of student documents
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static StudentDocumentPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (StudentDocumentPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.student.service.service.ClpSerializer.getServletContextName(),
					StudentDocumentPersistence.class.getName());

			ReferenceRegistry.registerReference(StudentDocumentUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(StudentDocumentPersistence persistence) {
	}

	private static StudentDocumentPersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.student.service.model.StatusHistory;

/**
 * The persistence interface for the status history service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see StatusHistoryPersistenceImpl
 * @see StatusHistoryUtil
 * @generated
 */
public interface StatusHistoryPersistence extends BasePersistence<StatusHistory> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link StatusHistoryUtil} to access the status history persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the status history in the entity cache if it is enabled.
	*
	* @param statusHistory the status history
	*/
	public void cacheResult(
		info.diit.portal.student.service.model.StatusHistory statusHistory);

	/**
	* Caches the status histories in the entity cache if it is enabled.
	*
	* @param statusHistories the status histories
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.student.service.model.StatusHistory> statusHistories);

	/**
	* Creates a new status history with the primary key. Does not add the status history to the database.
	*
	* @param statusHistoryId the primary key for the new status history
	* @return the new status history
	*/
	public info.diit.portal.student.service.model.StatusHistory create(
		long statusHistoryId);

	/**
	* Removes the status history with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param statusHistoryId the primary key of the status history
	* @return the status history that was removed
	* @throws info.diit.portal.student.service.NoSuchStatusHistoryException if a status history with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.StatusHistory remove(
		long statusHistoryId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchStatusHistoryException;

	public info.diit.portal.student.service.model.StatusHistory updateImpl(
		info.diit.portal.student.service.model.StatusHistory statusHistory,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the status history with the primary key or throws a {@link info.diit.portal.student.service.NoSuchStatusHistoryException} if it could not be found.
	*
	* @param statusHistoryId the primary key of the status history
	* @return the status history
	* @throws info.diit.portal.student.service.NoSuchStatusHistoryException if a status history with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.StatusHistory findByPrimaryKey(
		long statusHistoryId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.service.NoSuchStatusHistoryException;

	/**
	* Returns the status history with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param statusHistoryId the primary key of the status history
	* @return the status history, or <code>null</code> if a status history with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.service.model.StatusHistory fetchByPrimaryKey(
		long statusHistoryId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the status histories.
	*
	* @return the status histories
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.service.model.StatusHistory> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the status histories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of status histories
	* @param end the upper bound of the range of status histories (not inclusive)
	* @return the range of status histories
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.service.model.StatusHistory> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the status histories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of status histories
	* @param end the upper bound of the range of status histories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of status histories
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.service.model.StatusHistory> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the status histories from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of status histories.
	*
	* @return the number of status histories
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BatchStudent}.
 * </p>
 *
 * @author    nasimul
 * @see       BatchStudent
 * @generated
 */
public class BatchStudentWrapper implements BatchStudent,
	ModelWrapper<BatchStudent> {
	public BatchStudentWrapper(BatchStudent batchStudent) {
		_batchStudent = batchStudent;
	}

	public Class<?> getModelClass() {
		return BatchStudent.class;
	}

	public String getModelClassName() {
		return BatchStudent.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("batchStudentId", getBatchStudentId());
		attributes.put("studentId", getStudentId());
		attributes.put("batchId", getBatchId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("startDate", getStartDate());
		attributes.put("endDate", getEndDate());
		attributes.put("note", getNote());
		attributes.put("status", getStatus());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long batchStudentId = (Long)attributes.get("batchStudentId");

		if (batchStudentId != null) {
			setBatchStudentId(batchStudentId);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Date startDate = (Date)attributes.get("startDate");

		if (startDate != null) {
			setStartDate(startDate);
		}

		Date endDate = (Date)attributes.get("endDate");

		if (endDate != null) {
			setEndDate(endDate);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	/**
	* Returns the primary key of this batch student.
	*
	* @return the primary key of this batch student
	*/
	public long getPrimaryKey() {
		return _batchStudent.getPrimaryKey();
	}

	/**
	* Sets the primary key of this batch student.
	*
	* @param primaryKey the primary key of this batch student
	*/
	public void setPrimaryKey(long primaryKey) {
		_batchStudent.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the batch student ID of this batch student.
	*
	* @return the batch student ID of this batch student
	*/
	public long getBatchStudentId() {
		return _batchStudent.getBatchStudentId();
	}

	/**
	* Sets the batch student ID of this batch student.
	*
	* @param batchStudentId the batch student ID of this batch student
	*/
	public void setBatchStudentId(long batchStudentId) {
		_batchStudent.setBatchStudentId(batchStudentId);
	}

	/**
	* Returns the student ID of this batch student.
	*
	* @return the student ID of this batch student
	*/
	public long getStudentId() {
		return _batchStudent.getStudentId();
	}

	/**
	* Sets the student ID of this batch student.
	*
	* @param studentId the student ID of this batch student
	*/
	public void setStudentId(long studentId) {
		_batchStudent.setStudentId(studentId);
	}

	/**
	* Returns the batch ID of this batch student.
	*
	* @return the batch ID of this batch student
	*/
	public long getBatchId() {
		return _batchStudent.getBatchId();
	}

	/**
	* Sets the batch ID of this batch student.
	*
	* @param batchId the batch ID of this batch student
	*/
	public void setBatchId(long batchId) {
		_batchStudent.setBatchId(batchId);
	}

	/**
	* Returns the company ID of this batch student.
	*
	* @return the company ID of this batch student
	*/
	public long getCompanyId() {
		return _batchStudent.getCompanyId();
	}

	/**
	* Sets the company ID of this batch student.
	*
	* @param companyId the company ID of this batch student
	*/
	public void setCompanyId(long companyId) {
		_batchStudent.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this batch student.
	*
	* @return the user ID of this batch student
	*/
	public long getUserId() {
		return _batchStudent.getUserId();
	}

	/**
	* Sets the user ID of this batch student.
	*
	* @param userId the user ID of this batch student
	*/
	public void setUserId(long userId) {
		_batchStudent.setUserId(userId);
	}

	/**
	* Returns the user uuid of this batch student.
	*
	* @return the user uuid of this batch student
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchStudent.getUserUuid();
	}

	/**
	* Sets the user uuid of this batch student.
	*
	* @param userUuid the user uuid of this batch student
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_batchStudent.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this batch student.
	*
	* @return the user name of this batch student
	*/
	public java.lang.String getUserName() {
		return _batchStudent.getUserName();
	}

	/**
	* Sets the user name of this batch student.
	*
	* @param userName the user name of this batch student
	*/
	public void setUserName(java.lang.String userName) {
		_batchStudent.setUserName(userName);
	}

	/**
	* Returns the create date of this batch student.
	*
	* @return the create date of this batch student
	*/
	public java.util.Date getCreateDate() {
		return _batchStudent.getCreateDate();
	}

	/**
	* Sets the create date of this batch student.
	*
	* @param createDate the create date of this batch student
	*/
	public void setCreateDate(java.util.Date createDate) {
		_batchStudent.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this batch student.
	*
	* @return the modified date of this batch student
	*/
	public java.util.Date getModifiedDate() {
		return _batchStudent.getModifiedDate();
	}

	/**
	* Sets the modified date of this batch student.
	*
	* @param modifiedDate the modified date of this batch student
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_batchStudent.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the organization ID of this batch student.
	*
	* @return the organization ID of this batch student
	*/
	public long getOrganizationId() {
		return _batchStudent.getOrganizationId();
	}

	/**
	* Sets the organization ID of this batch student.
	*
	* @param organizationId the organization ID of this batch student
	*/
	public void setOrganizationId(long organizationId) {
		_batchStudent.setOrganizationId(organizationId);
	}

	/**
	* Returns the start date of this batch student.
	*
	* @return the start date of this batch student
	*/
	public java.util.Date getStartDate() {
		return _batchStudent.getStartDate();
	}

	/**
	* Sets the start date of this batch student.
	*
	* @param startDate the start date of this batch student
	*/
	public void setStartDate(java.util.Date startDate) {
		_batchStudent.setStartDate(startDate);
	}

	/**
	* Returns the end date of this batch student.
	*
	* @return the end date of this batch student
	*/
	public java.util.Date getEndDate() {
		return _batchStudent.getEndDate();
	}

	/**
	* Sets the end date of this batch student.
	*
	* @param endDate the end date of this batch student
	*/
	public void setEndDate(java.util.Date endDate) {
		_batchStudent.setEndDate(endDate);
	}

	/**
	* Returns the note of this batch student.
	*
	* @return the note of this batch student
	*/
	public java.lang.String getNote() {
		return _batchStudent.getNote();
	}

	/**
	* Sets the note of this batch student.
	*
	* @param note the note of this batch student
	*/
	public void setNote(java.lang.String note) {
		_batchStudent.setNote(note);
	}

	/**
	* Returns the status of this batch student.
	*
	* @return the status of this batch student
	*/
	public int getStatus() {
		return _batchStudent.getStatus();
	}

	/**
	* Sets the status of this batch student.
	*
	* @param status the status of this batch student
	*/
	public void setStatus(int status) {
		_batchStudent.setStatus(status);
	}

	public boolean isNew() {
		return _batchStudent.isNew();
	}

	public void setNew(boolean n) {
		_batchStudent.setNew(n);
	}

	public boolean isCachedModel() {
		return _batchStudent.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_batchStudent.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _batchStudent.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _batchStudent.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_batchStudent.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _batchStudent.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_batchStudent.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new BatchStudentWrapper((BatchStudent)_batchStudent.clone());
	}

	public int compareTo(
		info.diit.portal.student.service.model.BatchStudent batchStudent) {
		return _batchStudent.compareTo(batchStudent);
	}

	@Override
	public int hashCode() {
		return _batchStudent.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.student.service.model.BatchStudent> toCacheModel() {
		return _batchStudent.toCacheModel();
	}

	public info.diit.portal.student.service.model.BatchStudent toEscapedModel() {
		return new BatchStudentWrapper(_batchStudent.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _batchStudent.toString();
	}

	public java.lang.String toXmlString() {
		return _batchStudent.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_batchStudent.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public BatchStudent getWrappedBatchStudent() {
		return _batchStudent;
	}

	public BatchStudent getWrappedModel() {
		return _batchStudent;
	}

	public void resetOriginalValues() {
		_batchStudent.resetOriginalValues();
	}

	private BatchStudent _batchStudent;
}
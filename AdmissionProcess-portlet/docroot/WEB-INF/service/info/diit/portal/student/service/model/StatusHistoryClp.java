/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.student.service.service.StatusHistoryLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author nasimul
 */
public class StatusHistoryClp extends BaseModelImpl<StatusHistory>
	implements StatusHistory {
	public StatusHistoryClp() {
	}

	public Class<?> getModelClass() {
		return StatusHistory.class;
	}

	public String getModelClassName() {
		return StatusHistory.class.getName();
	}

	public long getPrimaryKey() {
		return _statusHistoryId;
	}

	public void setPrimaryKey(long primaryKey) {
		setStatusHistoryId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_statusHistoryId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("statusHistoryId", getStatusHistoryId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("batchStudentId", getBatchStudentId());
		attributes.put("fromStatus", getFromStatus());
		attributes.put("toStatus", getToStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long statusHistoryId = (Long)attributes.get("statusHistoryId");

		if (statusHistoryId != null) {
			setStatusHistoryId(statusHistoryId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long batchStudentId = (Long)attributes.get("batchStudentId");

		if (batchStudentId != null) {
			setBatchStudentId(batchStudentId);
		}

		Integer fromStatus = (Integer)attributes.get("fromStatus");

		if (fromStatus != null) {
			setFromStatus(fromStatus);
		}

		Integer toStatus = (Integer)attributes.get("toStatus");

		if (toStatus != null) {
			setToStatus(toStatus);
		}
	}

	public long getStatusHistoryId() {
		return _statusHistoryId;
	}

	public void setStatusHistoryId(long statusHistoryId) {
		_statusHistoryId = statusHistoryId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getBatchStudentId() {
		return _batchStudentId;
	}

	public void setBatchStudentId(long batchStudentId) {
		_batchStudentId = batchStudentId;
	}

	public int getFromStatus() {
		return _fromStatus;
	}

	public void setFromStatus(int fromStatus) {
		_fromStatus = fromStatus;
	}

	public int getToStatus() {
		return _toStatus;
	}

	public void setToStatus(int toStatus) {
		_toStatus = toStatus;
	}

	public BaseModel<?> getStatusHistoryRemoteModel() {
		return _statusHistoryRemoteModel;
	}

	public void setStatusHistoryRemoteModel(
		BaseModel<?> statusHistoryRemoteModel) {
		_statusHistoryRemoteModel = statusHistoryRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			StatusHistoryLocalServiceUtil.addStatusHistory(this);
		}
		else {
			StatusHistoryLocalServiceUtil.updateStatusHistory(this);
		}
	}

	@Override
	public StatusHistory toEscapedModel() {
		return (StatusHistory)Proxy.newProxyInstance(StatusHistory.class.getClassLoader(),
			new Class[] { StatusHistory.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		StatusHistoryClp clone = new StatusHistoryClp();

		clone.setStatusHistoryId(getStatusHistoryId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setOrganizationId(getOrganizationId());
		clone.setBatchStudentId(getBatchStudentId());
		clone.setFromStatus(getFromStatus());
		clone.setToStatus(getToStatus());

		return clone;
	}

	public int compareTo(StatusHistory statusHistory) {
		long primaryKey = statusHistory.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		StatusHistoryClp statusHistory = null;

		try {
			statusHistory = (StatusHistoryClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = statusHistory.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{statusHistoryId=");
		sb.append(getStatusHistoryId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", batchStudentId=");
		sb.append(getBatchStudentId());
		sb.append(", fromStatus=");
		sb.append(getFromStatus());
		sb.append(", toStatus=");
		sb.append(getToStatus());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.student.service.model.StatusHistory");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>statusHistoryId</column-name><column-value><![CDATA[");
		sb.append(getStatusHistoryId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>batchStudentId</column-name><column-value><![CDATA[");
		sb.append(getBatchStudentId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fromStatus</column-name><column-value><![CDATA[");
		sb.append(getFromStatus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>toStatus</column-name><column-value><![CDATA[");
		sb.append(getToStatus());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _statusHistoryId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private long _batchStudentId;
	private int _fromStatus;
	private int _toStatus;
	private BaseModel<?> _statusHistoryRemoteModel;
}
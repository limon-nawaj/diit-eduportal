/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.student.service.model.StatusHistory;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing StatusHistory in entity cache.
 *
 * @author nasimul
 * @see StatusHistory
 * @generated
 */
public class StatusHistoryCacheModel implements CacheModel<StatusHistory>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{statusHistoryId=");
		sb.append(statusHistoryId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", batchStudentId=");
		sb.append(batchStudentId);
		sb.append(", fromStatus=");
		sb.append(fromStatus);
		sb.append(", toStatus=");
		sb.append(toStatus);
		sb.append("}");

		return sb.toString();
	}

	public StatusHistory toEntityModel() {
		StatusHistoryImpl statusHistoryImpl = new StatusHistoryImpl();

		statusHistoryImpl.setStatusHistoryId(statusHistoryId);
		statusHistoryImpl.setCompanyId(companyId);
		statusHistoryImpl.setUserId(userId);

		if (userName == null) {
			statusHistoryImpl.setUserName(StringPool.BLANK);
		}
		else {
			statusHistoryImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			statusHistoryImpl.setCreateDate(null);
		}
		else {
			statusHistoryImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			statusHistoryImpl.setModifiedDate(null);
		}
		else {
			statusHistoryImpl.setModifiedDate(new Date(modifiedDate));
		}

		statusHistoryImpl.setOrganizationId(organizationId);
		statusHistoryImpl.setBatchStudentId(batchStudentId);
		statusHistoryImpl.setFromStatus(fromStatus);
		statusHistoryImpl.setToStatus(toStatus);

		statusHistoryImpl.resetOriginalValues();

		return statusHistoryImpl;
	}

	public long statusHistoryId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long organizationId;
	public long batchStudentId;
	public int fromStatus;
	public int toStatus;
}
package info.diit.portal.student.dto;

import info.diit.portal.student.service.model.AccademicRecord;

import java.io.Serializable;

public class AcademicRecordDto implements Serializable {
	private long academicRecordId;
	private long organisationId;
	private String degree;
	private String board;
	private int year;
	private String result;
	private String registrationNo;
	
	private boolean isNew=true;
	
	public boolean isNew() {
		return isNew;
	}


	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}


	public AcademicRecordDto() {
		
	//	this.academicRecordId = academicRecordId;
		//this.organisationId = organisationId;
		this.degree = "";
		this.board = "";
		this.result = "";
		this.registrationNo = "";
	}
	
	
	public AcademicRecordDto(AccademicRecord record) {
		
		this.academicRecordId = record.getAccademicRecordId();
		this.organisationId = record.getOrganisationId();
		this.degree = record.getDegree();
		this.board = record.getBoard();
		this.year = record.getYear();
		this.result = record.getResult();
		this.registrationNo = record.getRegistrationNo();
	}


	public long getAcademicRecordId() {
		return academicRecordId;
	}
	public void setAcademicRecordId(long academicRecordId) {
		this.academicRecordId = academicRecordId;
	}
	public long getOrganisationId() {
		return organisationId;
	}
	public void setOrganisationId(long organisationId) {
		this.organisationId = organisationId;
	}
	
	public String getDegree() {
		return degree;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}
	public String getBoard() {
		return board;
	}
	public void setBoard(String board) {
		this.board = board;
	}
	
	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getRegistrationNo() {
		return registrationNo;
	}
	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}
	
	
	
}

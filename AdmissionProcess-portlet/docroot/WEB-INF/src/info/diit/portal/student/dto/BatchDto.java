package info.diit.portal.student.dto;

import info.diit.portal.batch.model.Batch;

import java.io.Serializable;
import java.util.Date;

public class BatchDto implements Serializable {

	public long batchStudentId;	
	public long batchId;
	public long organizationId;
	public String organizationName;
	public long courseId;
	public String courseName;
	public String getCourseName() {
		return courseName;
	}


	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public long sessionId;
	public String batchSession;
	public String batchName;
	public Date batchStartDate;
	public Date batchEndDate;
	public String batchClassTeacher;
	public long teacherId;
	public int statusKey;
	
	public long getBatchStudentId() {
		return batchStudentId;
	}


	public void setBatchStudentId(long batchStudentId) {
		this.batchStudentId = batchStudentId;
	}

	public int getStatusKey() {
		return statusKey;
	}


	public void setStatusKey(int statusKey) {
		this.statusKey = statusKey;
	}

	public String status;
    private boolean isNew=true;
	
	public boolean isNew() {
		return isNew;
	}


	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public String note;

	public String getBatchSession() {
		return batchSession;
	}

	public void setBatchSession(String batchSession) {
		this.batchSession = batchSession;
	}

	public BatchDto() {
		batchSession="";
		note="";
	}

	public BatchDto(Batch batch) {

		this.batchId = batch.getBatchId();
		this.organizationId = batch.getOrganizationId();

		this.courseId = batch.getCourseId();

		this.sessionId = batch.getSessionId();

		this.batchName = batch.getBatchName();
		this.batchStartDate = batch.getStartDate();
		this.batchEndDate = batch.getEndDate();

		this.teacherId = batch.getBatchTeacherId();
		
		this.statusKey = batch.getStatus();
		this.note = batch.getNote();
	}

	public long getBatchId() {
		return batchId;
	}

	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public long getCourseId() {
		return courseId;
	}

	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}

	public long getSessionId() {
		return sessionId;
	}

	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}

	public long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(long teacherId) {
		this.teacherId = teacherId;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public Date getBatchStartDate() {
		return batchStartDate;
	}

	public void setBatchStartDate(Date batchStartDate) {
		this.batchStartDate = batchStartDate;
	}

	public Date getBatchEndDate() {
		return batchEndDate;
	}

	public void setBatchEndDate(Date batchEndDate) {
		this.batchEndDate = batchEndDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String toString() {
		return batchName;

	}

}

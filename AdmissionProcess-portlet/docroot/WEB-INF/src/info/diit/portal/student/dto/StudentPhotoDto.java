package info.diit.portal.student.dto;

import java.io.File;
import java.io.Serializable;

public class StudentPhotoDto  implements Serializable{

private boolean isNew=true;
	
	public boolean isNew() {
		return isNew;
	}


	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	File file;

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
	
	
}

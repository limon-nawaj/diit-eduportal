package info.diit.portal.student.dto;

import java.io.Serializable;

import info.diit.portal.student.service.model.BatchStudent;
import info.diit.portal.student.service.model.Student;

public class BatchStudentDto implements Serializable {
	
	long 		companyId;
	long   		studentId;
	String 		name;
	boolean 	check;
	long 		batchId;
	long 		organizationId;	
	long		newBatchId;
	int			status;
	long		batchStudentId;
	
	
	

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getBatchStudentId() {
		return batchStudentId;
	}

	public void setBatchStudentId(long batchStudentId) {
		this.batchStudentId = batchStudentId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getNewBatchId() {
		return newBatchId;
	}

	public void setNewBatchId(long newBatchId) {
		this.newBatchId = newBatchId;
	}

	public BatchStudentDto(){
		
	}
	
	public BatchStudentDto (Student student){
		this.studentId = student.getStudentId();
		this.name = student.getName();
	}
	
	public BatchStudentDto(Student student, BatchStudent batchStudent){
		this.studentId = student.getStudentId();
		this.name = student.getName();
		this.batchId = batchStudent.getBatchId();
		this.organizationId = batchStudent.getOrganizationId();
		this.status = batchStudent.getStatus();
		this.batchStudentId = batchStudent.getBatchStudentId();
	}
	
	public long getStudentId() {
		return studentId;
	}
	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isCheck() {
		return check;
	}
	public void setCheck(boolean check) {
		this.check = check;
	}
	public long getBatchId() {
		return batchId;
	}

	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

}

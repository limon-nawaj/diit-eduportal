package info.diit.portal.student.assignStudent;

import info.diit.portal.batch.model.Batch;
import info.diit.portal.batch.service.BatchLocalServiceUtil;
import info.diit.portal.coursesubject.model.Course;
import info.diit.portal.coursesubject.model.CourseOrganization;
import info.diit.portal.coursesubject.service.CourseLocalServiceUtil;
import info.diit.portal.coursesubject.service.CourseOrganizationLocalServiceUtil;
import info.diit.portal.student.constant.BatchStatus;
import info.diit.portal.student.dto.BatchDto;
import info.diit.portal.student.dto.BatchStudentDto;
import info.diit.portal.student.dto.CourseDTO;
import info.diit.portal.student.dto.OrganizationDto;
import info.diit.portal.student.service.NoSuchBatchStudentException;
import info.diit.portal.student.service.NoSuchStudentException;
import info.diit.portal.student.service.model.BatchStudent;
import info.diit.portal.student.service.model.Student;
import info.diit.portal.student.service.model.impl.BatchStudentImpl;
import info.diit.portal.student.service.service.BatchStudentLocalServiceUtil;
import info.diit.portal.student.service.service.StudentLocalServiceUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.webdav.Status;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Component.Event;
import com.vaadin.ui.Component.Listener;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class AssignStudent extends Application implements PortletRequestListener {
	private Window 			window;
	private ComboBox 			campusComboBox;
	private ComboBox			courseBox;
	private ComboBox			fromBatchBox;
	private ComboBox			toBatchBox;
	private Table				assignBatchTable;
	private ComboBox			assignActionBox;
	private Button 				assignSaveButton;
	private Button				assignCancelButton;
	private Button				assignDeleteButton;
	private ThemeDisplay 					themeDisplay;
	private User 							user;
	private String 							userName;
	private long 							companyId;
	
	private BeanItemContainer<BatchStudentDto> studentBeanItemContainer;
	
	public final static String COLUMN_STUDENT_ID 		= "studentId";
	public final static String COLUMN_STUDENT_NAME		= "name";
	public final static String COLUMN_CHECK				= "check";
	
	private List<Organization> 							userOrganizationsList=new ArrayList<Organization>();
	private List<OrganizationDto>						userOganizationsDtos;
	private long 										userOrganizationid;

    public void init() {
    	window = new Window();
		window.setSizeFull();
		
		user = themeDisplay.getUser();
		userName = user.getFullName();
		companyId = themeDisplay.getCompanyId();
		
		try {
			userOrganizationsList = user.getOrganizations();
		} catch (PortalException e1) {
			e1.printStackTrace();
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
		
		VerticalLayout assignStudentLayout = new VerticalLayout();
		
		HorizontalLayout propertiesLayout1 = new HorizontalLayout();
		HorizontalLayout propertiesLayout2 = new HorizontalLayout();
		propertiesLayout1.setSpacing(true);
		propertiesLayout2.setSpacing(true);
		
		//final LiferayIPC liferayIPC = new LiferayIPC();
		
		campusComboBox = new ComboBox("Campuses");
		campusComboBox.setImmediate(true);
		campusComboBox.setNullSelectionAllowed(false);
		
		courseBox = new ComboBox("Courses");
		courseBox.setImmediate(true);
		courseBox.setNullSelectionAllowed(false);
		
		fromBatchBox = new ComboBox("Batches");
		fromBatchBox.setImmediate(true);
		fromBatchBox.setNullSelectionAllowed(false);

		toBatchBox = new ComboBox("To Batch");
		
		assignActionBox = new ComboBox("Action");
		assignActionBox.addItem("New Batch");
		assignActionBox.addItem("Transfer");
		assignActionBox.setNullSelectionAllowed(false);
		
		assignSaveButton = new Button("Save");
		assignCancelButton = new Button("Cancel");
		assignDeleteButton = new Button("Delete");
		
		assignSaveButton.setEnabled(false);
		assignDeleteButton.setEnabled(false);
		
		if (userOrganizationsList.size()>1) {
			campusComboBox.setVisible(true);
			userOganizationsDtos = new ArrayList<OrganizationDto>();
			for(Organization userOrg : userOrganizationsList){
				OrganizationDto userOrgDto = new OrganizationDto(userOrg);				
				userOganizationsDtos.add(userOrgDto);
				campusComboBox.addItem(userOrgDto);
			}			
		}
		else if(userOrganizationsList.size()==1){
			//window.showNotification("working organization size checking");
			userOganizationsDtos = new ArrayList<OrganizationDto>();
			Organization userOrg = userOrganizationsList.get(0);
			userOrganizationid  = userOrg.getOrganizationId();
			OrganizationDto userOrgDto = new OrganizationDto(userOrg);				
			userOganizationsDtos.add(userOrgDto);
			getCourseDtoByOrgId(userOrganizationid);
		}
		else{
			userOrganizationid = 0;
		}		
		
		campusComboBox.addListener(new Listener() {			
			public void componentEvent(Event event) {
				OrganizationDto organizationDto = (OrganizationDto) campusComboBox.getValue();
				if(organizationDto!=null){
					userOrganizationid = organizationDto.getOrganizationId();
					getCourseDtoByOrgId(userOrganizationid);					
					
				}else{
					courseBox.removeAllItems();
				}
				
			}
		});	
		
		courseBox.addListener(new Listener() {
			public void componentEvent(Event event) {
				CourseDTO courseDTO = (CourseDTO) courseBox.getValue();
				if(courseDTO!=null){
					long courseId = courseDTO.getCourseId();
					getBatchDtoByCourseId(courseId);
					if(searchBatchDtoList!=null){
						fromBatchBox.removeAllItems();
						for(BatchDto batchDto:searchBatchDtoList){
							fromBatchBox.addItem(batchDto);
							toBatchBox.addItem(batchDto);
						}						
					}
				}else{
					
					fromBatchBox.removeAllItems();
				}
			}
		});	
		
		fromBatchBox.addListener(new Listener() {
			public void componentEvent(Event event) {
				BatchDto batchDto = (BatchDto) fromBatchBox.getValue();
				if(batchDto!=null){
					assignSaveButton.setEnabled(true);
					assignDeleteButton.setEnabled(true);
					long batchId = batchDto.getBatchId();
					getStudentByBatchId(batchId);
					if(toBatchDtoList!=null){
						toBatchBox.removeAllItems();
						for(BatchDto tobatchDto:toBatchDtoList){
							if(batchId != tobatchDto.getBatchId() && tobatchDto.getStatusKey()!=Status.COMPLETED.key){
								toBatchBox.addItem(tobatchDto);
							}							
						}						
					}
				}
			}
		});		
		
		
		studentBeanItemContainer = new BeanItemContainer<BatchStudentDto>(BatchStudentDto.class);
		
		assignBatchTable = new Table("Students",studentBeanItemContainer);
		assignBatchTable.setPageLength(15);
		assignBatchTable.setWidth("100%");
		assignBatchTable.setSelectable(true);
		assignBatchTable.setEditable(true);
		
		assignBatchTable.setColumnHeader(COLUMN_STUDENT_ID, "Student ID");
		assignBatchTable.setColumnHeader(COLUMN_STUDENT_NAME, "Student Name");
		assignBatchTable.setColumnHeader(COLUMN_CHECK, "Check");
		
		assignBatchTable.setVisibleColumns(new String[] { COLUMN_CHECK , COLUMN_STUDENT_ID, COLUMN_STUDENT_NAME});
		
		assignBatchTable.setColumnExpandRatio(COLUMN_CHECK, 1);
		assignBatchTable.setColumnExpandRatio(COLUMN_STUDENT_ID, 1);
		assignBatchTable.setColumnExpandRatio(COLUMN_STUDENT_NAME, 3);
		
		assignBatchTable.setTableFieldFactory(new DefaultFieldFactory(){
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				if (propertyId.equals(COLUMN_STUDENT_ID)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				
				if (propertyId.equals(COLUMN_STUDENT_NAME)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				
				if (propertyId.equals(COLUMN_CHECK)) {
					CheckBox field = new CheckBox();
					field.setWidth("100%");
					field.setReadOnly(false);
					return field;
				}
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		propertiesLayout1.addComponent(campusComboBox);
		propertiesLayout1.addComponent(courseBox);
		propertiesLayout1.addComponent(fromBatchBox);
		propertiesLayout2.addComponent(assignActionBox);
		propertiesLayout2.addComponent(toBatchBox);	
		
		HorizontalLayout optionsLayout = new HorizontalLayout();
		optionsLayout.setWidth("100%");
		optionsLayout.setSpacing(true);
		
		assignSaveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				BatchDto toBatchDto = (BatchDto) toBatchBox.getValue();
				BatchDto fromBatchDto = (BatchDto) fromBatchBox.getValue();
				String action = (String) assignActionBox.getValue();
				if(toBatchDto!=null && action!=""){	
					long counter=0;
					
					for (int i = 0; i < studentBeanItemContainer.size(); i++) {
						BatchStudentDto studentDto = studentBeanItemContainer.getIdByIndex(i); 
						
						if(studentDto.isCheck()){
							long studentId = studentDto.getStudentId();	
							long assignBatchId = toBatchDto.getBatchId();
							long organizationId	= studentDto.getOrganizationId();
							long companyId = studentDto.getCompanyId();
							long batchStudentId = studentDto.getBatchStudentId();
							
							BatchStudent batchStudent = new BatchStudentImpl();
							
							batchStudent.setCompanyId(companyId);
							batchStudent.setOrganizationId(userOrganizationid);
							batchStudent.setStudentId(studentId);							
														
							BatchStudent isBatchStudent= null ;
							try {
								isBatchStudent = BatchStudentLocalServiceUtil.findIsStudentExistInBatch(organizationId, assignBatchId, studentId);
																	
							} catch (NoSuchBatchStudentException e1) {
								e1.printStackTrace();
							} catch (SystemException e1) {
								e1.printStackTrace();
							}
							
							if(isBatchStudent==null){
								try {
									if(action.equalsIgnoreCase("New Batch")){
										batchStudent.setBatchId(assignBatchId);
										batchStudent.setStatus(BatchStatus.RUNNING.getKey());
										BatchStudentLocalServiceUtil.addBatchStudent(batchStudent);
										counter ++;
									}else if(action.equalsIgnoreCase("Transfer")){
										if(fromBatchDto.getCourseId()==toBatchDto.getCourseId()){
											batchStudent.setStatus(BatchStatus.RUNNING.getKey());
											batchStudent.setNote("Transfered from -"+fromBatchDto.getBatchName());
											//batchStudent.setBatchStudentId(n);
											batchStudent.setBatchId(assignBatchId);
											
											BatchStudentLocalServiceUtil.addBatchStudent(batchStudent);
											
											batchStudent.setStatus(BatchStatus.TRANSFERED.getKey());
											batchStudent.setBatchId(fromBatchDto.getBatchId());
											batchStudent.setNote("Transfered to -"+toBatchDto.getBatchName());
											batchStudent.setBatchStudentId(batchStudentId);
											BatchStudentLocalServiceUtil.updateBatchStudent(batchStudent);										
											
											counter ++;
										}else{
											window.showNotification("Can't transfer to different course batch's",Window.Notification.TYPE_WARNING_MESSAGE);
										}
									}									
								} catch (SystemException e) {
									e.printStackTrace();
									return;
								}
								
								//System.out.print("Student Saved");
							}else{
								window.showNotification("Studnet, ID :"+studentId+" already Exist in Batch :" +toBatchDto.getBatchName(),Window.Notification.TYPE_ERROR_MESSAGE);
								return;
							}
						}						
					}
					if(counter > 0){
						window.showNotification(counter+"- Student Is Successfully Saved");
					}else{
						window.showNotification("No Student To Save!!!!!", Window.Notification.TYPE_WARNING_MESSAGE);
					}
				}else{
					window.showNotification("Select an Action & a Batch", Window.Notification.TYPE_WARNING_MESSAGE);
				}
			}
		});	
		
		assignCancelButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				cleanAssignTable();
			}
		});
		
		assignDeleteButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				BatchDto fromBatchDto = (BatchDto) fromBatchBox.getValue();
				if(fromBatchDto!=null){	
					long counter=0;
					boolean couldDelete = true;
					
					for (int i = 0; i < studentBeanItemContainer.size(); i++) {
						BatchStudentDto studentDto = studentBeanItemContainer.getIdByIndex(i); 
						
						if(studentDto.isCheck()){
							long studentId = studentDto.getStudentId();	
							
							long batchStudentId = studentDto.getBatchStudentId();
														
							try {
								List<BatchStudent> availableBatches = BatchStudentLocalServiceUtil.findBystduentBatchStudentList(studentId);
								if (availableBatches.size()>1) {
									try {
										BatchStudentLocalServiceUtil.deleteBatchStudent(batchStudentId);
										counter++;
									} catch (PortalException e) {
										e.printStackTrace();
									}
								}else{
									couldDelete = false;
								}
							} catch (SystemException e) {
								e.printStackTrace();
							}
						}						
					}
					if(counter > 0){
						window.showNotification(counter+"- Student Is Successfully Deleted");
						if(!couldDelete){
							window.showNotification("Some students were not deleted! </br> As they were enrolled only in a sigle batch");
						}
					}else{
						window.showNotification("No Student To delete!!!!!", Window.Notification.TYPE_WARNING_MESSAGE);
					}
				}else{
					window.showNotification("Select Batch", Window.Notification.TYPE_WARNING_MESSAGE);
				}
			}
		});
		
		Label expandLabel = new Label();
		
		optionsLayout.addComponent(expandLabel);
		optionsLayout.addComponent(assignSaveButton);
		optionsLayout.addComponent(assignCancelButton);
		optionsLayout.addComponent(assignDeleteButton);
		optionsLayout.setExpandRatio(expandLabel, 1);
		
		assignStudentLayout.addComponent(propertiesLayout1);
		assignStudentLayout.addComponent(propertiesLayout2);
		assignStudentLayout.addComponent(assignBatchTable);
		assignStudentLayout.addComponent(optionsLayout);
		//assignStudentLayout.addComponent(liferayIPC);
		
		window.addComponent(assignStudentLayout);

		setMainWindow(window);
    }
    
    private List<BatchDto> searchBatchDtoList;
	private List<BatchDto> toBatchDtoList;
	private void getBatchDtoByCourseId(long courseId){
		try {
			List<Batch> batchList = BatchLocalServiceUtil.findBatchesByOrgCourseId(userOrganizationid, courseId);	
			
			List<Batch> toBatchList = BatchLocalServiceUtil.findBatchesByComOrg(companyId, userOrganizationid);
					
			searchBatchDtoList = new ArrayList<BatchDto>();
			for(Batch batch : batchList){
				BatchDto batchDto = new BatchDto();
				batchDto.setBatchId(batch.getBatchId());
				batchDto.setBatchName(batch.getBatchName());
				batchDto.setStatusKey(batch.getStatus());
				batchDto.setCourseId(batch.getCourseId());
				searchBatchDtoList.add(batchDto);
			}
			
			toBatchDtoList = new ArrayList<BatchDto>();
			for(Batch batch : toBatchList){
				BatchDto batchDto = new BatchDto();
				batchDto.setBatchId(batch.getBatchId());
				batchDto.setBatchName(batch.getBatchName());
				batchDto.setStatusKey(batch.getStatus());
				batchDto.setCourseId(batch.getCourseId());
				toBatchDtoList.add(batchDto);
			}
						
		} catch (SystemException e) {
			e.printStackTrace();
		}		
	}
    
    
    
    List<CourseDTO>				courseDtoList;
    private void getCourseDtoByOrgId(long organizationId){
		try {
			List<CourseOrganization> courseOrganizations = CourseOrganizationLocalServiceUtil.findByOrganization(organizationId);
			
			if(courseOrganizations!=null){
				//System.out.println("couse size by organization"+courseOrganizations.size());
				courseDtoList = new ArrayList<CourseDTO>();
				for(CourseOrganization courseOrg : courseOrganizations){
					CourseDTO courseDto = new CourseDTO();
					long courseid = courseOrg.getCourseId();
					Course course = CourseLocalServiceUtil.fetchCourse(courseid);
					courseDto.setCourseId(courseid);
					courseDto.setCourseName(course.getCourseName());				
					courseDtoList.add(courseDto);
				}
				
				if (courseDtoList!=null) {
					courseBox.removeAllItems();
					for( CourseDTO courseDto: courseDtoList){
						courseBox.addItem(courseDto);
					}
				}
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
    
    private List<BatchStudentDto> studentDtoList;
	private void getStudentByBatchId(long batchId) {
		studentBeanItemContainer.removeAllItems();
		try {
			studentDtoList = new ArrayList<BatchStudentDto>();
			List<BatchStudent> batchStudnetList = BatchStudentLocalServiceUtil.findAllStudentByOrgBatchId(userOrganizationid, batchId);
			if(batchStudnetList!=null){
				for(BatchStudent batchStudent :batchStudnetList){
					try {
						Student student = StudentLocalServiceUtil.findStudentByOrgStudentId(userOrganizationid, batchStudent.getStudentId());
						BatchStudentDto studentDto = new BatchStudentDto(student,batchStudent);
						studentDtoList.add(studentDto);
					} catch (NoSuchStudentException e) {
						e.printStackTrace();
					}
				}
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
		if(studentDtoList!=null){
			for(BatchStudentDto studentDto:studentDtoList){
				studentBeanItemContainer.addBean(studentDto);
			}
		}

		assignBatchTable.refreshRowCache();
	}
	
	private enum Status implements Serializable
	{
		RUNNING(1,"Running"),
		PENDING(2,"Pending"),
		COMPLETED(3,"Completed"),
		STOPPED(4,"Stopped");
		
		private int key;
		private String value;
		
		private Status(int key,String value)
		{
			this.key = key;
			this.value = value;
		}
		
		@Override
		public String toString()
		{
			return value;
		}
		
	}

	private Status getStatus(int key)
	{		
		Status statuses[]=Status.values();
		
		for(Status status:statuses)
		{
			if(status.key==key)
			{
				return status;
			}
		}		
		return null;
	}
	private void cleanAssignTable() {
		campusComboBox.setValue(null);
		courseBox.setValue(null);
		toBatchBox.setValue(null);
		studentBeanItemContainer.removeAllItems();
		assignBatchTable.refreshRowCache();
	}

	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request
				.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

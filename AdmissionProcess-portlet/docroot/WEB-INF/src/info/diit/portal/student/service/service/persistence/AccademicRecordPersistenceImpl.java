/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.student.service.NoSuchAccademicRecordException;
import info.diit.portal.student.service.model.AccademicRecord;
import info.diit.portal.student.service.model.impl.AccademicRecordImpl;
import info.diit.portal.student.service.model.impl.AccademicRecordModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the accademic record service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see AccademicRecordPersistence
 * @see AccademicRecordUtil
 * @generated
 */
public class AccademicRecordPersistenceImpl extends BasePersistenceImpl<AccademicRecord>
	implements AccademicRecordPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AccademicRecordUtil} to access the accademic record persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AccademicRecordImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTACCADEMICRECORDLIST =
		new FinderPath(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordModelImpl.FINDER_CACHE_ENABLED,
			AccademicRecordImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByStudentAccademicRecordList",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTACCADEMICRECORDLIST =
		new FinderPath(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordModelImpl.FINDER_CACHE_ENABLED,
			AccademicRecordImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByStudentAccademicRecordList",
			new String[] { Long.class.getName() },
			AccademicRecordModelImpl.STUDENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STUDENTACCADEMICRECORDLIST =
		new FinderPath(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByStudentAccademicRecordList",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordModelImpl.FINDER_CACHE_ENABLED,
			AccademicRecordImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordModelImpl.FINDER_CACHE_ENABLED,
			AccademicRecordImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the accademic record in the entity cache if it is enabled.
	 *
	 * @param accademicRecord the accademic record
	 */
	public void cacheResult(AccademicRecord accademicRecord) {
		EntityCacheUtil.putResult(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordImpl.class, accademicRecord.getPrimaryKey(),
			accademicRecord);

		accademicRecord.resetOriginalValues();
	}

	/**
	 * Caches the accademic records in the entity cache if it is enabled.
	 *
	 * @param accademicRecords the accademic records
	 */
	public void cacheResult(List<AccademicRecord> accademicRecords) {
		for (AccademicRecord accademicRecord : accademicRecords) {
			if (EntityCacheUtil.getResult(
						AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
						AccademicRecordImpl.class,
						accademicRecord.getPrimaryKey()) == null) {
				cacheResult(accademicRecord);
			}
			else {
				accademicRecord.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all accademic records.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AccademicRecordImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AccademicRecordImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the accademic record.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AccademicRecord accademicRecord) {
		EntityCacheUtil.removeResult(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordImpl.class, accademicRecord.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<AccademicRecord> accademicRecords) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AccademicRecord accademicRecord : accademicRecords) {
			EntityCacheUtil.removeResult(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
				AccademicRecordImpl.class, accademicRecord.getPrimaryKey());
		}
	}

	/**
	 * Creates a new accademic record with the primary key. Does not add the accademic record to the database.
	 *
	 * @param accademicRecordId the primary key for the new accademic record
	 * @return the new accademic record
	 */
	public AccademicRecord create(long accademicRecordId) {
		AccademicRecord accademicRecord = new AccademicRecordImpl();

		accademicRecord.setNew(true);
		accademicRecord.setPrimaryKey(accademicRecordId);

		return accademicRecord;
	}

	/**
	 * Removes the accademic record with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param accademicRecordId the primary key of the accademic record
	 * @return the accademic record that was removed
	 * @throws info.diit.portal.student.service.NoSuchAccademicRecordException if a accademic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AccademicRecord remove(long accademicRecordId)
		throws NoSuchAccademicRecordException, SystemException {
		return remove(Long.valueOf(accademicRecordId));
	}

	/**
	 * Removes the accademic record with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the accademic record
	 * @return the accademic record that was removed
	 * @throws info.diit.portal.student.service.NoSuchAccademicRecordException if a accademic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AccademicRecord remove(Serializable primaryKey)
		throws NoSuchAccademicRecordException, SystemException {
		Session session = null;

		try {
			session = openSession();

			AccademicRecord accademicRecord = (AccademicRecord)session.get(AccademicRecordImpl.class,
					primaryKey);

			if (accademicRecord == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAccademicRecordException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(accademicRecord);
		}
		catch (NoSuchAccademicRecordException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AccademicRecord removeImpl(AccademicRecord accademicRecord)
		throws SystemException {
		accademicRecord = toUnwrappedModel(accademicRecord);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, accademicRecord);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(accademicRecord);

		return accademicRecord;
	}

	@Override
	public AccademicRecord updateImpl(
		info.diit.portal.student.service.model.AccademicRecord accademicRecord,
		boolean merge) throws SystemException {
		accademicRecord = toUnwrappedModel(accademicRecord);

		boolean isNew = accademicRecord.isNew();

		AccademicRecordModelImpl accademicRecordModelImpl = (AccademicRecordModelImpl)accademicRecord;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, accademicRecord, merge);

			accademicRecord.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !AccademicRecordModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((accademicRecordModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTACCADEMICRECORDLIST.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(accademicRecordModelImpl.getOriginalStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTACCADEMICRECORDLIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTACCADEMICRECORDLIST,
					args);

				args = new Object[] {
						Long.valueOf(accademicRecordModelImpl.getStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTACCADEMICRECORDLIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTACCADEMICRECORDLIST,
					args);
			}
		}

		EntityCacheUtil.putResult(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordImpl.class, accademicRecord.getPrimaryKey(),
			accademicRecord);

		return accademicRecord;
	}

	protected AccademicRecord toUnwrappedModel(AccademicRecord accademicRecord) {
		if (accademicRecord instanceof AccademicRecordImpl) {
			return accademicRecord;
		}

		AccademicRecordImpl accademicRecordImpl = new AccademicRecordImpl();

		accademicRecordImpl.setNew(accademicRecord.isNew());
		accademicRecordImpl.setPrimaryKey(accademicRecord.getPrimaryKey());

		accademicRecordImpl.setAccademicRecordId(accademicRecord.getAccademicRecordId());
		accademicRecordImpl.setCompanyId(accademicRecord.getCompanyId());
		accademicRecordImpl.setUserId(accademicRecord.getUserId());
		accademicRecordImpl.setUserName(accademicRecord.getUserName());
		accademicRecordImpl.setCreateDate(accademicRecord.getCreateDate());
		accademicRecordImpl.setModifiedDate(accademicRecord.getModifiedDate());
		accademicRecordImpl.setOrganisationId(accademicRecord.getOrganisationId());
		accademicRecordImpl.setDegree(accademicRecord.getDegree());
		accademicRecordImpl.setBoard(accademicRecord.getBoard());
		accademicRecordImpl.setYear(accademicRecord.getYear());
		accademicRecordImpl.setResult(accademicRecord.getResult());
		accademicRecordImpl.setRegistrationNo(accademicRecord.getRegistrationNo());
		accademicRecordImpl.setStudentId(accademicRecord.getStudentId());

		return accademicRecordImpl;
	}

	/**
	 * Returns the accademic record with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the accademic record
	 * @return the accademic record
	 * @throws com.liferay.portal.NoSuchModelException if a accademic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AccademicRecord findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the accademic record with the primary key or throws a {@link info.diit.portal.student.service.NoSuchAccademicRecordException} if it could not be found.
	 *
	 * @param accademicRecordId the primary key of the accademic record
	 * @return the accademic record
	 * @throws info.diit.portal.student.service.NoSuchAccademicRecordException if a accademic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AccademicRecord findByPrimaryKey(long accademicRecordId)
		throws NoSuchAccademicRecordException, SystemException {
		AccademicRecord accademicRecord = fetchByPrimaryKey(accademicRecordId);

		if (accademicRecord == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + accademicRecordId);
			}

			throw new NoSuchAccademicRecordException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				accademicRecordId);
		}

		return accademicRecord;
	}

	/**
	 * Returns the accademic record with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the accademic record
	 * @return the accademic record, or <code>null</code> if a accademic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AccademicRecord fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the accademic record with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param accademicRecordId the primary key of the accademic record
	 * @return the accademic record, or <code>null</code> if a accademic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AccademicRecord fetchByPrimaryKey(long accademicRecordId)
		throws SystemException {
		AccademicRecord accademicRecord = (AccademicRecord)EntityCacheUtil.getResult(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
				AccademicRecordImpl.class, accademicRecordId);

		if (accademicRecord == _nullAccademicRecord) {
			return null;
		}

		if (accademicRecord == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				accademicRecord = (AccademicRecord)session.get(AccademicRecordImpl.class,
						Long.valueOf(accademicRecordId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (accademicRecord != null) {
					cacheResult(accademicRecord);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
						AccademicRecordImpl.class, accademicRecordId,
						_nullAccademicRecord);
				}

				closeSession(session);
			}
		}

		return accademicRecord;
	}

	/**
	 * Returns all the accademic records where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the matching accademic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AccademicRecord> findByStudentAccademicRecordList(
		long studentId) throws SystemException {
		return findByStudentAccademicRecordList(studentId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the accademic records where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of accademic records
	 * @param end the upper bound of the range of accademic records (not inclusive)
	 * @return the range of matching accademic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AccademicRecord> findByStudentAccademicRecordList(
		long studentId, int start, int end) throws SystemException {
		return findByStudentAccademicRecordList(studentId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the accademic records where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of accademic records
	 * @param end the upper bound of the range of accademic records (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching accademic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AccademicRecord> findByStudentAccademicRecordList(
		long studentId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTACCADEMICRECORDLIST;
			finderArgs = new Object[] { studentId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTACCADEMICRECORDLIST;
			finderArgs = new Object[] { studentId, start, end, orderByComparator };
		}

		List<AccademicRecord> list = (List<AccademicRecord>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AccademicRecord accademicRecord : list) {
				if ((studentId != accademicRecord.getStudentId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_ACCADEMICRECORD_WHERE);

			query.append(_FINDER_COLUMN_STUDENTACCADEMICRECORDLIST_STUDENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				list = (List<AccademicRecord>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first accademic record in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching accademic record
	 * @throws info.diit.portal.student.service.NoSuchAccademicRecordException if a matching accademic record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AccademicRecord findByStudentAccademicRecordList_First(
		long studentId, OrderByComparator orderByComparator)
		throws NoSuchAccademicRecordException, SystemException {
		AccademicRecord accademicRecord = fetchByStudentAccademicRecordList_First(studentId,
				orderByComparator);

		if (accademicRecord != null) {
			return accademicRecord;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAccademicRecordException(msg.toString());
	}

	/**
	 * Returns the first accademic record in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching accademic record, or <code>null</code> if a matching accademic record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AccademicRecord fetchByStudentAccademicRecordList_First(
		long studentId, OrderByComparator orderByComparator)
		throws SystemException {
		List<AccademicRecord> list = findByStudentAccademicRecordList(studentId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last accademic record in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching accademic record
	 * @throws info.diit.portal.student.service.NoSuchAccademicRecordException if a matching accademic record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AccademicRecord findByStudentAccademicRecordList_Last(
		long studentId, OrderByComparator orderByComparator)
		throws NoSuchAccademicRecordException, SystemException {
		AccademicRecord accademicRecord = fetchByStudentAccademicRecordList_Last(studentId,
				orderByComparator);

		if (accademicRecord != null) {
			return accademicRecord;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAccademicRecordException(msg.toString());
	}

	/**
	 * Returns the last accademic record in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching accademic record, or <code>null</code> if a matching accademic record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AccademicRecord fetchByStudentAccademicRecordList_Last(
		long studentId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByStudentAccademicRecordList(studentId);

		List<AccademicRecord> list = findByStudentAccademicRecordList(studentId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the accademic records before and after the current accademic record in the ordered set where studentId = &#63;.
	 *
	 * @param accademicRecordId the primary key of the current accademic record
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next accademic record
	 * @throws info.diit.portal.student.service.NoSuchAccademicRecordException if a accademic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AccademicRecord[] findByStudentAccademicRecordList_PrevAndNext(
		long accademicRecordId, long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchAccademicRecordException, SystemException {
		AccademicRecord accademicRecord = findByPrimaryKey(accademicRecordId);

		Session session = null;

		try {
			session = openSession();

			AccademicRecord[] array = new AccademicRecordImpl[3];

			array[0] = getByStudentAccademicRecordList_PrevAndNext(session,
					accademicRecord, studentId, orderByComparator, true);

			array[1] = accademicRecord;

			array[2] = getByStudentAccademicRecordList_PrevAndNext(session,
					accademicRecord, studentId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AccademicRecord getByStudentAccademicRecordList_PrevAndNext(
		Session session, AccademicRecord accademicRecord, long studentId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ACCADEMICRECORD_WHERE);

		query.append(_FINDER_COLUMN_STUDENTACCADEMICRECORDLIST_STUDENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(studentId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(accademicRecord);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AccademicRecord> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the accademic records.
	 *
	 * @return the accademic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AccademicRecord> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the accademic records.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of accademic records
	 * @param end the upper bound of the range of accademic records (not inclusive)
	 * @return the range of accademic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AccademicRecord> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the accademic records.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of accademic records
	 * @param end the upper bound of the range of accademic records (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of accademic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AccademicRecord> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<AccademicRecord> list = (List<AccademicRecord>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ACCADEMICRECORD);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ACCADEMICRECORD;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<AccademicRecord>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<AccademicRecord>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the accademic records where studentId = &#63; from the database.
	 *
	 * @param studentId the student ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByStudentAccademicRecordList(long studentId)
		throws SystemException {
		for (AccademicRecord accademicRecord : findByStudentAccademicRecordList(
				studentId)) {
			remove(accademicRecord);
		}
	}

	/**
	 * Removes all the accademic records from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (AccademicRecord accademicRecord : findAll()) {
			remove(accademicRecord);
		}
	}

	/**
	 * Returns the number of accademic records where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the number of matching accademic records
	 * @throws SystemException if a system exception occurred
	 */
	public int countByStudentAccademicRecordList(long studentId)
		throws SystemException {
		Object[] finderArgs = new Object[] { studentId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_STUDENTACCADEMICRECORDLIST,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ACCADEMICRECORD_WHERE);

			query.append(_FINDER_COLUMN_STUDENTACCADEMICRECORDLIST_STUDENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_STUDENTACCADEMICRECORDLIST,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of accademic records.
	 *
	 * @return the number of accademic records
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ACCADEMICRECORD);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the accademic record persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.student.service.model.AccademicRecord")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<AccademicRecord>> listenersList = new ArrayList<ModelListener<AccademicRecord>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<AccademicRecord>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AccademicRecordImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AccademicRecordPersistence.class)
	protected AccademicRecordPersistence accademicRecordPersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_ACCADEMICRECORD = "SELECT accademicRecord FROM AccademicRecord accademicRecord";
	private static final String _SQL_SELECT_ACCADEMICRECORD_WHERE = "SELECT accademicRecord FROM AccademicRecord accademicRecord WHERE ";
	private static final String _SQL_COUNT_ACCADEMICRECORD = "SELECT COUNT(accademicRecord) FROM AccademicRecord accademicRecord";
	private static final String _SQL_COUNT_ACCADEMICRECORD_WHERE = "SELECT COUNT(accademicRecord) FROM AccademicRecord accademicRecord WHERE ";
	private static final String _FINDER_COLUMN_STUDENTACCADEMICRECORDLIST_STUDENTID_2 =
		"accademicRecord.studentId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "accademicRecord.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AccademicRecord exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No AccademicRecord exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AccademicRecordPersistenceImpl.class);
	private static AccademicRecord _nullAccademicRecord = new AccademicRecordImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<AccademicRecord> toCacheModel() {
				return _nullAccademicRecordCacheModel;
			}
		};

	private static CacheModel<AccademicRecord> _nullAccademicRecordCacheModel = new CacheModel<AccademicRecord>() {
			public AccademicRecord toEntityModel() {
				return _nullAccademicRecord;
			}
		};
}
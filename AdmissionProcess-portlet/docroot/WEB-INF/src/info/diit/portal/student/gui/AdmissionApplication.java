package info.diit.portal.student.gui;

import static info.diit.portal.student.constant.StudentConstant.COLUMN_BATCH_NAME;
import static info.diit.portal.student.constant.StudentConstant.COLUMN_COURSE;
import static info.diit.portal.student.constant.StudentConstant.COLUMN_FATHER_CONTACT;
import static info.diit.portal.student.constant.StudentConstant.COLUMN_GARDIAN_CONTACT;
import static info.diit.portal.student.constant.StudentConstant.COLUMN_HOME_CONTACT;
import static info.diit.portal.student.constant.StudentConstant.COLUMN_MOTHER_CONTACT;
import static info.diit.portal.student.constant.StudentConstant.COLUMN_STUDENT_CONTACT;
import static info.diit.portal.student.constant.StudentConstant.COLUMN_STUDENT_ID;
import static info.diit.portal.student.constant.StudentConstant.COLUMN_STUDENT_NAME;
import static info.diit.portal.student.constant.StudentConstant.EXPERIANCE_RECORD_DESINATION;
import static info.diit.portal.student.constant.StudentConstant.EXPERIANCE_RECORD_ENDDATE;
import static info.diit.portal.student.constant.StudentConstant.EXPERIANCE_RECORD_ORGANIZATION;
import static info.diit.portal.student.constant.StudentConstant.EXPERIANCE_RECORD_STARTDATE;
import static info.diit.portal.student.constant.StudentConstant.RECORD_BOARD;
import static info.diit.portal.student.constant.StudentConstant.RECORD_DEGREE;
import static info.diit.portal.student.constant.StudentConstant.RECORD_REGISTRATION_NO;
import static info.diit.portal.student.constant.StudentConstant.RECORD_RESULT;
import static info.diit.portal.student.constant.StudentConstant.RECORD_YEAR;
import static info.diit.portal.student.constant.StudentConstant.STUDENT_DOCUMENT_DESCRIPTION;
import static info.diit.portal.student.constant.StudentConstant.STUDENT_DOCUMENT_NAME;
import static info.diit.portal.student.constant.StudentConstant.STUDENT_DOCUMENT_TYPE;
import info.diit.portal.batch.model.Batch;
import info.diit.portal.batch.service.BatchLocalServiceUtil;
import info.diit.portal.coursesubject.model.Course;
import info.diit.portal.coursesubject.model.CourseOrganization;
import info.diit.portal.coursesubject.model.CourseSession;
import info.diit.portal.coursesubject.service.CourseLocalServiceUtil;
import info.diit.portal.coursesubject.service.CourseOrganizationLocalServiceUtil;
import info.diit.portal.coursesubject.service.CourseSessionLocalServiceUtil;
import info.diit.portal.student.constant.BatchStatus;
import info.diit.portal.student.constant.StudentConstant;
import info.diit.portal.student.dto.AcademicRecordDto;
import info.diit.portal.student.dto.BatchDto;
import info.diit.portal.student.dto.CourseDTO;
import info.diit.portal.student.dto.CustomSearchDto;
import info.diit.portal.student.dto.ExperianceDto;
import info.diit.portal.student.dto.OrganizationDto;
import info.diit.portal.student.dto.StudentDocumentDto;
import info.diit.portal.student.service.NoSuchStudentException;
import info.diit.portal.student.service.model.AccademicRecord;
import info.diit.portal.student.service.model.BatchStudent;
import info.diit.portal.student.service.model.Experiance;
import info.diit.portal.student.service.model.PersonEmail;
import info.diit.portal.student.service.model.PhoneNumber;
import info.diit.portal.student.service.model.Student;
import info.diit.portal.student.service.model.StudentDocument;
import info.diit.portal.student.service.model.impl.AccademicRecordImpl;
import info.diit.portal.student.service.model.impl.BatchStudentImpl;
import info.diit.portal.student.service.model.impl.ExperianceImpl;
import info.diit.portal.student.service.model.impl.PersonEmailImpl;
import info.diit.portal.student.service.model.impl.PhoneNumberImpl;
import info.diit.portal.student.service.model.impl.StudentDocumentImpl;
import info.diit.portal.student.service.model.impl.StudentImpl;
import info.diit.portal.student.service.service.AccademicRecordLocalServiceUtil;
import info.diit.portal.student.service.service.BatchStudentLocalServiceUtil;
import info.diit.portal.student.service.service.ExperianceLocalServiceUtil;
import info.diit.portal.student.service.service.PersonEmailLocalServiceUtil;
import info.diit.portal.student.service.service.PhoneNumberLocalServiceUtil;
import info.diit.portal.student.service.service.StudentDocumentLocalServiceUtil;
import info.diit.portal.student.service.service.StudentLocalServiceUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import net.atontech.vaadin.ui.numericfield.NumericField;
import net.atontech.vaadin.ui.numericfield.widgetset.shared.NumericFieldType;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.vaadin.tokenfield.TokenField;

import com.liferay.portal.kernel.dao.jdbc.DataAccess;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.vaadin.Application;
import com.vaadin.addon.ipcforliferay.LiferayIPC;
import com.vaadin.addon.ipcforliferay.event.LiferayIPCEvent;
import com.vaadin.addon.ipcforliferay.event.LiferayIPCEventListener;
import com.vaadin.data.Container;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.terminal.FileResource;
import com.vaadin.terminal.StreamResource;
import com.vaadin.terminal.StreamResource.StreamSource;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Component.Event;
import com.vaadin.ui.Component.Listener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.StartedListener;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
public class AdmissionApplication extends Application implements Receiver,
		SucceededListener, FailedListener, StartedListener,
		PortletRequestListener {

	
	BeanItemContainer<CustomSearchDto> 						customSearchContainer;
	BeanItemContainer<AcademicRecordDto> 					academicRecordContainer;
	BeanItemContainer<ExperianceDto> 						experianceRecordContainer;
	BeanItemContainer<BatchDto> 							batchRecordContainer;
	BeanItemContainer<StudentDocumentDto> 					studentDocumentContainer;

	//private List<StudentDocument> 							studentDocuments=new ArrayList<StudentDocument>();
	private List<Experiance> 								studentExpariances=new ArrayList<Experiance>();
	private List<AccademicRecord> 							studentAccademicRecords=new ArrayList<AccademicRecord>();
	private List<Organization> 								userOrganizationsList=new ArrayList<Organization>();
	private List<info.diit.portal.batch.model.Batch> 		batchList;
	private List<StudentDocument> 							stuentDocuments;
	public Window 							window;
	// upload
	private File 							file;
	private Upload 							upload;
	private Panel 							photoPanel;
	private Window 							myPopWindow;
	Application 							app;
	private TabSheet 						tabSheet;
	private TabSheet						buildTabSheet;

	private Table 							accademicRecordTable;
	private Table 							experienceTable;
	private Table 							batchTable;

	Date 									currentDate = new Date();
	SimpleDateFormat 						dateFormat = new SimpleDateFormat("dd-mm-yy");

	// entity
	private Student 						student = null;

	private PhoneNumber 					phoneNumber;
	private PersonEmail 					personEmail;
	// token process;
	private List<PhoneNumber> 				phoneList;
	private List<PersonEmail> 				emailList;

	// utilities
	private ThemeDisplay 					themeDisplay;
	private User 							user;
	private String 							userName;
	private long 							companyId;

	private ComboBox 						organizationOption;
	private Organization 					userOrganization;
	
	private OrganizationDto 				organizationDto;

	// private OrganizationDto orgDto;
	
	private VerticalLayout 					verticalLayout;
	private GridLayout						studentAssignLayout;
	
	Connection 								connection;
	JasperPrint 							jasperPrint;

	public void init() {

		window = new Window();
        app=this;
		setMainWindow(window);
		
		final LiferayIPC liferayIPC = new LiferayIPC();
		
		verticalLayout = new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.setSpacing(true);
		
		if(student==null){
			student = new StudentImpl();
			student.setNew(true);
		}
		

		academicRecordContainer = new BeanItemContainer<AcademicRecordDto>(AcademicRecordDto.class);
		experianceRecordContainer = new BeanItemContainer<ExperianceDto>(ExperianceDto.class);
		batchRecordContainer = new BeanItemContainer<BatchDto>(BatchDto.class);		
		studentDocumentContainer = new BeanItemContainer<StudentDocumentDto>(StudentDocumentDto.class);
		
		user = themeDisplay.getUser();
		userName = user.getFullName();
		companyId = themeDisplay.getCompanyId();
		
		try {
			connection = DataAccess.getConnection();
		} catch (SQLException e2) {
			e2.printStackTrace();
		}		
		
		try {
			userOrganizationsList = user.getOrganizations();
		} catch (PortalException e1) {
			e1.printStackTrace();
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
		window.addComponent(easySearch());
		window.addComponent(new HorizontalLayout());
		buildTabSheet = buildTabSheet();
		verticalLayout.addComponent(buildTabSheet);
		

		window.addComponent(verticalLayout);
		window.addComponent(liferayIPC);
	}
	
	

	private GridLayout initAdmissionForm;
	public TabSheet buildTabSheet() {

		tabSheet = new TabSheet();
		initAdmissionForm = initAdmissionForm();
		tabSheet.addTab(initAdmissionForm, "General Information");
		tabSheet.addTab(accademicRecordTable(), "Academic Records");		
		tabSheet.addTab(experienceTable(), "Experience");
		tabSheet.addTab(batchTable(), "Batch");
		tabSheet.addTab(studentDocumentTable(), "Student Document");
		// tabSheet.addTab(easySearch(), "Easy Search");
		tabSheet.addTab(customSearch(), "Custom Search");
		return tabSheet;
	}

	private Button 						uploadButton;
	private TextField 					studentNameField;
	private TextField 					fatherNameField;
	private TextField 					motherNameField;
	private ComboBox 					genderOption;
	private DateField 					dobField;
	private ComboBox 					batchComboBox;
	private TokenField 					studentContactToken;
	private TokenField 					fatherMobileToken;
	private TokenField 					motherMobileToken;
	private TokenField 					emailToken;
	private TokenField 					homePhoneToken;
	private TokenField 					gardianContactToken;
	private TextArea 					presentAdressArea;
	private TextArea 					permanentAdressArea;
	private ComboBox					religionComboBox;
	private TextField					nationalityField;
	private TextField 					studentIdLabel;
	private Button 						deleteButton;
	
	private List<OrganizationDto>			userOganizationsDtos;
	private long 							userOrganizationid;
	private int							uptype = 0;

	private GridLayout initAdmissionForm() {
		GridLayout admissionFormGridLayout = new GridLayout(4, 10);
		admissionFormGridLayout.setSpacing(true);
		admissionFormGridLayout.setWidth("100%");
		
		organizationOption = new ComboBox("Campus");
		organizationOption.setImmediate(true);

		// left field
		studentNameField = new TextField("Student Name");
		studentNameField.setRequired(true);
		studentNameField.setWidth("100%");
		
		fatherNameField = new TextField("Father's Name");
		fatherNameField.setWidth("100%");
		fatherNameField.setRequired(true);
		
		motherNameField = new TextField("Mother's Name");
		motherNameField.setWidth("100%");
		motherNameField.setRequired(true);
		
		genderOption = new ComboBox("Gender");
		//genderOption.setWidth("100%");
		
		dobField = new DateField("Date of Birth");
		//dobField.setWidth("100%");
		dobField.setDateFormat(StudentConstant.DATE_FORMAT);
		dobField.setResolution(DateField.RESOLUTION_DAY);
		Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.YEAR, -18);
		//window.showNotification("time" + cal2.getTime());
		dobField.setValue(cal2.getTime());
		dobField.setRequired(true);
		
		batchComboBox = new ComboBox("Batch");
		//batchComboBox.setWidth("100%");
		batchComboBox.setRequired(true);
		
		genderOption.setNullSelectionAllowed(false);
		genderOption.addItem("Male");
		genderOption.addItem("Female");
		genderOption.select(0);
		genderOption.setRequired(true);
		
		if (userOrganizationsList.size()>1) {
			organizationOption.setVisible(true);
			userOganizationsDtos = new ArrayList<OrganizationDto>();
			for(Organization userOrg : userOrganizationsList){
				OrganizationDto userOrgDto = new OrganizationDto(userOrg);				
				userOganizationsDtos.add(userOrgDto);
				organizationOption.addItem(userOrgDto);
			}			
		}
		else if(userOrganizationsList.size()==1){
			//window.showNotification("working organization size checking");
			userOganizationsDtos = new ArrayList<OrganizationDto>();
			Organization userOrg = userOrganizationsList.get(0);
			userOrganizationid  = userOrg.getOrganizationId();
			OrganizationDto userOrgDto = new OrganizationDto(userOrg);				
			userOganizationsDtos.add(userOrgDto);
			showBatchCombo(userOrganizationid);
		}
		else{
			userOrganizationid = 0;
		}	

		organizationOption.addListener(new ValueChangeListener() {

			public void valueChange(ValueChangeEvent event) {
				organizationDto = (OrganizationDto) organizationOption
						.getValue();
				if (organizationDto != null) {
					userOrganizationid = organizationDto.getOrganizationId();

					showBatchCombo(userOrganizationid);
					//showCourseBatchStudent();
				} else
					batchComboBox.removeAllItems();
			}
		});
		// right token field
		studentContactToken = new TokenField("Student Contact Number");
		studentContactToken.setWidth("100%");
		fatherMobileToken = new TokenField("Father's Mobile");
		fatherMobileToken.setWidth("100%");
		motherMobileToken = new TokenField("Mother's Mobile");
		emailToken = new TokenField("Email");
		emailToken.setWidth("100%");
		emailToken.setRequired(true);
		emailToken.addValidator(new EmailValidator("Invalid Email Address "));
		
		emailToken.addListener(new ValueChangeListener() {			
			@SuppressWarnings("unchecked")
			public void valueChange(ValueChangeEvent event) {
				if(student.isNew()){
					Collection<String>	 strings =(Collection<String>) emailToken.getValue();
					if(strings.size()>0){
						String str=strings.toArray()[strings.size()-1].toString();
						if(!str.isEmpty())
						try {							
							PersonEmail personEmail=PersonEmailLocalServiceUtil.getPersonEmailCheck(str);
						    if(personEmail!=null){
						    	window.showNotification(personEmail.getPersonEmail()+" is Duplicate..", Window.Notification.TYPE_ERROR_MESSAGE);
						    emailToken.removeToken(str);
						    strings.remove(str);
						    }
						    
						    for (String string : strings) {
						    	emailToken.addToken(string);
							}
						    
						} catch (SystemException e) {
							window.showNotification("Erron in email input");
							e.printStackTrace();
						}
					}	
				}			
			}
		});
		
		homePhoneToken = new TokenField("Home Mobile");
		homePhoneToken.setWidth("100%");
		
		gardianContactToken = new TokenField("Guardian Contact Number");
		gardianContactToken.setWidth("100%");
		
		presentAdressArea = new TextArea("Present Adress");
		presentAdressArea.setRequired(true);
		presentAdressArea.setWidth("100%");
		
		permanentAdressArea = new TextArea("Permanent Adress");
		permanentAdressArea.setRequired(true);
		permanentAdressArea.setWidth("100%");
		
		VerticalLayout photoPanelLayout = new VerticalLayout();
		photoPanel = new Panel("Photo");
		photoPanel.setWidth("160px");
		photoPanel.setHeight("200px");
		
		photoPanelLayout.addComponent(photoPanel);
		
		religionComboBox = new ComboBox("Religion");
		religionComboBox.addItem("Islam");
		religionComboBox.addItem("Hinduism");
		religionComboBox.addItem("Buddhism");
		religionComboBox.addItem("Christianity");
		religionComboBox.addItem("Other");
		
		religionComboBox.setRequired(true);
		religionComboBox.select("Islam");
		religionComboBox.setNullSelectionAllowed(false);
		
		nationalityField = new TextField("Nationality");
		nationalityField.setRequired(true);

		uploadButton = new Button("Upload");
		uploadButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				uptype = 1;
				showPopupWindow("Stuent Addmission System - Photo Upload ");
			}
		});
		
		photoPanelLayout.addComponent(new HorizontalLayout());
		photoPanelLayout.addComponent(uploadButton);
		//photoPanelLayout.setComponentAlignment(uploadButton, Alignment.MIDDLE_RIGHT);

		studentIdLabel = new TextField("Student Id :");
		studentIdLabel.setReadOnly(true);
		
		HorizontalLayout buttonLayout = new HorizontalLayout();
		//buttonLayout.setWidth("100%");
		buttonLayout.setSpacing(true);

		Button saveGeneralInfo = new Button("Save");
		
		Button cancelButton = new Button("Reset");
		cancelButton.setImmediate(true);
		
		deleteButton = new Button("Delete");
		deleteButton.setEnabled(false);	
			
		//Label expandLabel = new Label();
		
		//buttonLayout.addComponent(expandLabel);
		
		buttonLayout.addComponent(saveGeneralInfo);
		buttonLayout.addComponent(cancelButton);
		buttonLayout.addComponent(deleteButton);
		//buttonLayout.setExpandRatio(expandLabel, 1);
		
		saveGeneralInfo.addListener(new ClickListener() {

			@SuppressWarnings("unchecked")
			public void buttonClick(ClickEvent event) {
				if (userOrganizationsList.size() > 1)
					organizationDto = (OrganizationDto) organizationOption.getValue();
				if (student == null) {
					student = new StudentImpl();
					student.setNew(true);
				}

				long companyId = themeDisplay.getCompanyId();
				// long groupid=themeDisplay.getU
				if (!studentNameField.getValue().toString().trim().isEmpty()) {

					student.setName(studentNameField.getValue().toString());
				} else {
					window.showNotification("Please input Student Name", Window.Notification.TYPE_WARNING_MESSAGE);
					return;
				}
				if (!fatherNameField.getValue().toString().trim().isEmpty())
					student.setFatherName(fatherNameField.getValue().toString());
				else {
					window.showNotification("Please input Father Name", Window.Notification.TYPE_WARNING_MESSAGE);
					return;
				}
				if (!motherNameField.getValue().toString().trim().isEmpty())
					student.setMotherName(motherNameField.getValue().toString());
				else {
					window.showNotification("Please input Mother Name", Window.Notification.TYPE_WARNING_MESSAGE);
					return;
				}

				if(genderOption.getValue()!=null && genderOption.getValue().toString()!=""){
					student.setGender(genderOption.getValue().toString());					
				}else{
					window.showNotification("Select Student Gender! ", Window.Notification.TYPE_WARNING_MESSAGE);
					return;
				}

				if (dobField.getValue() != null)
					student.setDateOfBirth((Date) dobField.getValue());
				else {
					window.showNotification("Provide Date of Birth", Window.Notification.TYPE_WARNING_MESSAGE);
					return;

				}
				student.setCompanyId(companyId);
				student.setOrganizationId(userOrganizationid);
				student.setUserId(user.getUserId());

				if (homePhoneToken.getValue() != null){
					student.setHomePhone(homePhoneToken.getValue().toString());
				}
				
				if (!permanentAdressArea.getValue().toString().trim().isEmpty())
					student.setPermanentAddress(permanentAdressArea.getValue()
							.toString());
				else {
					window.showNotification("Please provide student's parmanent address", Window.Notification.TYPE_WARNING_MESSAGE);
					return;
				}
				if (!presentAdressArea.getValue().toString().trim().isEmpty())
					student.setPresentAddress(presentAdressArea.getValue()
							.toString());
				else {
					window.showNotification("Please provide student's present address", Window.Notification.TYPE_WARNING_MESSAGE);
					return;
				}
				
				student.setReligion(religionComboBox.getValue()
						.toString());
				
				if(nationalityField.getValue()!=null && nationalityField.getValue().toString()!=""){
					student.setNationality(nationalityField.getValue()
							.toString().trim());
				}else{
					window.showNotification("Please provide student's nationality", Window.Notification.TYPE_WARNING_MESSAGE);
					return;
				}
				
				
				// Adding student contract list

				phoneList = new ArrayList<PhoneNumber>();
				emailList = new ArrayList<PersonEmail>();
				Collection<String> studentContract = (Collection<String>) studentContactToken
						.getValue();
				if (studentContract != null) {
					for (String phone : studentContract) {
						phoneNumber = new PhoneNumberImpl();
						phoneNumber.setOwnerType(COLUMN_STUDENT_CONTACT);
						phoneNumber.setPhoneNumber(phone);
						phoneNumber.setCompanyId(companyId);
						// phoneNumber.setStudentId(studentDto.getStudentId());
						phoneNumber.setOrganizationId(userOrganizationid);
						phoneList.add(phoneNumber);
					}

				}
				Collection<String> fatherContract = (Collection<String>) fatherMobileToken
						.getValue();
				if (fatherContract != null) {
					for (String phone : fatherContract) {
						phoneNumber = new PhoneNumberImpl();
						phoneNumber.setOwnerType(COLUMN_FATHER_CONTACT);
						phoneNumber.setPhoneNumber(phone);
						phoneNumber.setCompanyId(companyId);
						phoneNumber.setOrganizationId(userOrganizationid);
						// phoneNumber.setStudentId(student.getStudentId());
						phoneList.add(phoneNumber);
					}
				}

				Collection<String> motherContract = (Collection<String>) motherMobileToken
						.getValue();
				if (motherContract != null) {
					for (String phone : motherContract) {
						phoneNumber = new PhoneNumberImpl();
						phoneNumber.setOwnerType(COLUMN_MOTHER_CONTACT);
						phoneNumber.setPhoneNumber(phone);
						phoneNumber.setOrganizationId(userOrganizationid);
						phoneNumber.setCompanyId(companyId);
						// phoneNumber.setStudentId(student.getStudentId());
						phoneList.add(phoneNumber);
					}
				}

				Collection<String> gardianContact = (Collection<String>) gardianContactToken
						.getValue();
				if (gardianContact != null) {
					for (String phone : gardianContact) {
						phoneNumber = new PhoneNumberImpl();
						phoneNumber.setOwnerType(COLUMN_GARDIAN_CONTACT);
						phoneNumber.setPhoneNumber(phone);
						phoneNumber.setCompanyId(companyId);
						phoneNumber.setOrganizationId(userOrganizationid);
						phoneList.add(phoneNumber);
					}
				}
				
				Collection<String> homeContact = (Collection<String>) homePhoneToken
						.getValue();
				if (homeContact != null) {
					for (String phone : homeContact) {
						phoneNumber = new PhoneNumberImpl();
						phoneNumber.setOwnerType(COLUMN_HOME_CONTACT);
						phoneNumber.setPhoneNumber(phone);
						phoneNumber.setCompanyId(companyId);
						phoneNumber.setOrganizationId(userOrganizationid);
						phoneList.add(phoneNumber);
					}
				}

				// System.out.print("student Contract .. "+phoneList.size());

				Collection<String> emailContact = (Collection<String>) emailToken
						.getValue();
				if (emailContact != null) {
					for (String email : emailContact) {
						personEmail = new PersonEmailImpl();
						personEmail.setOwnerType(COLUMN_STUDENT_CONTACT);
						personEmail.setPersonEmail(email);
						personEmail.setOrganizationId(userOrganizationid);
						personEmail.setCompanyId(companyId);
						emailList.add(personEmail);
					}
				} else {
					window.showNotification("At least one Email address required. ",Window.Notification.TYPE_WARNING_MESSAGE);
					return;
				}

				//System.out.print("student company " + student.getCompanyId());
				// studentDto.setPhoto(file);
				// window.showNotification("Student saved strated "+emailList.get(0).getPersonEmail()+" "+userOrganizationid);
				try {
					
					BatchStudent batchStudent = new BatchStudentImpl();
					
					batchStudent.setCompanyId(companyId);
					batchStudent.setOrganizationId(userOrganizationid);
					batchStudent.setCreateDate(new Date());
					batchStudent.setUserId(user.getUserId());
					
					if(student.isNew()){						
						BatchDto batchDto = ((BatchDto) batchComboBox.getValue());
						if(batchDto!=null){
							student = StudentLocalServiceUtil.studentSave(student, phoneList, emailList, file, user, true);
							
							studentIdLabel.setReadOnly(false);
							studentIdLabel.setValue(student.getStudentId());
							studentIdLabel.setReadOnly(true);
							
							long studentBatchId = batchDto.getBatchId();						
							batchStudent.setBatchId(studentBatchId);	
							
							batchStudent.setStatus(BatchStatus.RUNNING.getKey());
							batchStudent.setStudentId(student.getStudentId());
							batchStudent.setStartDate(batchDto.getBatchStartDate());
							
							batchStudent = BatchStudentLocalServiceUtil
									.addBatchStudent(batchStudent);
							window.showNotification("Student has been saved ");
							loadStudentBatchTable();
							batchLayout.addComponent(assignStudentLayout());
							deleteButton.setEnabled(true);
						}else{
							window.showNotification("Student Must be enrolled in batch", Window.Notification.TYPE_WARNING_MESSAGE);
						}
						
					}else{
						student = StudentLocalServiceUtil.studentSave(student, phoneList, emailList, file, user, false);
						window.showNotification("Student has been Updated ");
						studentIdLabel.setReadOnly(false);
						studentIdLabel.setValue(student.getStudentId());
						studentIdLabel.setReadOnly(true);
						
//						batchStudent = BatchStudentLocalServiceUtil.updateBatchStudent(batchStudent);
					}
				} catch (PortalException e) {
					e.printStackTrace();
				} catch (SystemException e) {
					e.printStackTrace();
				}

			}
		});
		
		cancelButton.addListener(new ClickListener() {			
			public void buttonClick(ClickEvent event) {
				blankStudentPanel();
				student = new StudentImpl();				
				student.setNew(true);				
				assignStudentLayout.removeAllComponents();	
				batchComboBox.setEnabled(true);		
				serchTextField.setValue("");
			}
		});
		
		deleteButton.addListener(new ClickListener() {			
			public void buttonClick(ClickEvent event) {
				try {
					StudentLocalServiceUtil.deleteSearchedStudent(student);
					window.showNotification("Student deleted successfully");
					blankStudentPanel();
					student = new StudentImpl();
					student.setNew(true);
					assignStudentLayout.removeAllComponents();
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
		
		
		if (userOrganizationsList.size() > 1)
			admissionFormGridLayout.addComponent(organizationOption, 0, 0);

		admissionFormGridLayout.addComponent(studentNameField, 0, 1, 0, 1);
		admissionFormGridLayout.addComponent(studentContactToken, 1, 1, 1, 1);

		admissionFormGridLayout.addComponent(fatherNameField, 0, 2, 0, 2);
		admissionFormGridLayout.addComponent(fatherMobileToken, 1, 2, 1, 2);

		admissionFormGridLayout.addComponent(motherNameField, 0, 3, 0, 3);
		admissionFormGridLayout.addComponent(motherMobileToken, 1, 3, 1, 3);

		admissionFormGridLayout.addComponent(genderOption, 0, 4, 0, 4);
		admissionFormGridLayout.addComponent(emailToken, 1, 4, 1, 4);

		admissionFormGridLayout.addComponent(dobField, 0, 5, 0, 5);
		admissionFormGridLayout.addComponent(homePhoneToken, 1, 5, 1, 5);

		admissionFormGridLayout.addComponent(batchComboBox, 0, 6, 0, 6);
		admissionFormGridLayout.addComponent(gardianContactToken, 1, 6, 1, 6);

		admissionFormGridLayout.addComponent(presentAdressArea, 0, 7, 0, 7);
		admissionFormGridLayout.addComponent(permanentAdressArea, 1, 7, 1, 7);

		admissionFormGridLayout.addComponent(photoPanelLayout, 2, 1, 3, 4);
		admissionFormGridLayout.addComponent(religionComboBox,2,5,2,5);
		
		admissionFormGridLayout.addComponent(nationalityField,2,6,2,6);
		
		admissionFormGridLayout.addComponent(studentIdLabel, 1, 0,1,0);

		admissionFormGridLayout.addComponent(buttonLayout, 2, 8,3,8);

		return admissionFormGridLayout;
	}

	private VerticalLayout 			accademicRecordLayout;
	private Button 					moreAccademicRecordButton;
	private Button 					resetAccademicRecordButton;
	private Button 					saveAccademicRecord;
	private Button 					deleteAcademicRecord;
	private AcademicRecordDto		selectedRecord;

	private VerticalLayout accademicRecordTable() {
		accademicRecordLayout = new VerticalLayout();

		accademicRecordTable = new Table("",academicRecordContainer);
		accademicRecordTable.setSelectable(true);
		accademicRecordTable.setEditable(true);
		accademicRecordTable.setWidth("100%");
		accademicRecordTable.setHeight("190px");
		accademicRecordTable.setColumnHeader(RECORD_DEGREE, "Degree");
		accademicRecordTable.setColumnHeader(RECORD_BOARD, "Examining Body");
		accademicRecordTable.setColumnHeader(RECORD_YEAR, "Year");
		accademicRecordTable.setColumnHeader(RECORD_RESULT, "Result");
		accademicRecordTable.setColumnHeader(RECORD_REGISTRATION_NO,"Roll No");
		accademicRecordTable.setVisibleColumns(new String[] { RECORD_DEGREE,RECORD_BOARD, RECORD_YEAR, RECORD_RESULT,RECORD_REGISTRATION_NO });
		//accademicRecordTable.addItem(new AcademicRecordDto());		
		
		accademicRecordLayout.addComponent(accademicRecordTable);
		
		accademicRecordTable.setTableFieldFactory(new DefaultFieldFactory(){
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				if (propertyId.equals(RECORD_YEAR)) {
					NumericField field = new NumericField();
					field.setNullSettingAllowed(true);
					field.setNullRepresentation("");
					field.setNumberType(NumericFieldType.INTEGER);
					return field;
				}
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		HorizontalLayout accademicRecordButtonLayout = new HorizontalLayout();
		accademicRecordButtonLayout.setWidth("100%");
		accademicRecordButtonLayout.setSpacing(true);
		Label expandLabel = new Label();
		accademicRecordButtonLayout.addComponent(expandLabel);
		
		accademicRecordLayout.addComponent(accademicRecordButtonLayout);
		
		moreAccademicRecordButton = new Button("More");
		accademicRecordButtonLayout.addComponent(moreAccademicRecordButton);

		/*resetAccademicRecordButton = new Button("Reset");
		accademicRecordButtonLayout.addComponent(resetAccademicRecordButton);*/
		moreAccademicRecordButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				accademicRecordTable.addItem(new AcademicRecordDto());
				saveAccademicRecord.setEnabled(true);
			}
		});

		/*resetAccademicRecordButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				academicRecordContainer.removeAllItems();
				// accademicRecordTable.removeAllItems();
				accademicRecordTable.addItem(new AcademicRecordDto());
			}
		});*/

		saveAccademicRecord = new Button("Save");
		saveAccademicRecord.setEnabled(false);
		
		saveAccademicRecord.addListener(new ClickListener() {

			public void buttonClick(ClickEvent event) {

				List<AccademicRecord> accademicRecords = new ArrayList<AccademicRecord>();
				if(academicRecordContainer!=null){
					for (int i = 0; i < academicRecordContainer.size(); i++) {
						AccademicRecord record = new AccademicRecordImpl();
						AcademicRecordDto accademicRecordDto = academicRecordContainer.getIdByIndex(i);

						record.setStudentId(student.getStudentId());
						record.setCompanyId(companyId);
						record.setCreateDate(new Date());
						record.setUserId(user.getUserId());
						record.setOrganisationId(student.getOrganizationId());
						
						String board = accademicRecordDto.getBoard();
						String registration = accademicRecordDto.getRegistrationNo();
						String result = accademicRecordDto.getResult();
						int year = accademicRecordDto.getYear();
						String degree = accademicRecordDto.getDegree();
						if (board!="" && board!=null && registration!="" && result!="" && year!=0 && degree!=""&& registration!=null && result!=null && year!=0 && degree!=null){
							record.setBoard(board);
							record.setRegistrationNo(registration);
							record.setResult(result);
							record.setYear(year);
							record.setDegree(degree);
							
							accademicRecords.add(record);
						}else{						
							window.showNotification("Fill the filds correctly",Window.Notification.TYPE_WARNING_MESSAGE);
							accademicRecords.clear();
							accademicRecords = null;
							break;
						}
											
					}
					if(accademicRecords!=null && academicRecordContainer.size()>0){
						if(student.getStudentId()!=0){
							try {
								AccademicRecordLocalServiceUtil.saveStudentAcaAccademicRecord(accademicRecords, student.getStudentId());
								window.showNotification("Save Successfully");
							} catch (SystemException e) {
								
								e.printStackTrace();
							}
						}else{
							window.showNotification("No student was saved! please save general info first",Window.Notification.TYPE_WARNING_MESSAGE);
						}
						
					}else{
						window.showNotification("No Information inserted!",Window.Notification.TYPE_WARNING_MESSAGE);
					}
				}				
			}
		});

		accademicRecordButtonLayout.addComponent(saveAccademicRecord);
		
		deleteAcademicRecord = new Button("Delete");
		
		deleteAcademicRecord.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				selectedRecord = (AcademicRecordDto)accademicRecordTable.getValue();
				
				if(selectedRecord!=null){
					if(selectedRecord.getBoard()!="" && selectedRecord.getRegistrationNo()!="" && selectedRecord.getResult()!="" && selectedRecord.getYear()!=0 && selectedRecord.getDegree()!=""){
						if(!student.isNew()){
							try {
								AccademicRecordLocalServiceUtil.deleteAccademicRecord(selectedRecord.getAcademicRecordId());
								loadAcademicRecordTable();
								window.showNotification("Record Deleted");
							} catch (PortalException e) {
								e.printStackTrace();
							} catch (SystemException e) {
								e.printStackTrace();
							}	
						}else{
							accademicRecordTable.removeItem(selectedRecord);
							accademicRecordTable.refreshRowCache();
						}
						
					}else{
						accademicRecordTable.removeItem(selectedRecord);
						accademicRecordTable.refreshRowCache();
					}
				}
				else{
					window.showNotification("Please Select a Record!", Window.Notification.TYPE_WARNING_MESSAGE);
				}
			}
		});
		
		accademicRecordButtonLayout.addComponent(deleteAcademicRecord);
		accademicRecordButtonLayout.setExpandRatio(expandLabel, 1);

		return accademicRecordLayout;
	}
	
	private VerticalLayout 								studentDocumentLayout;
	private Button 										moreStudentDocumentButton;
	private Button 										resetStudentDocumentButton;
	private Button 										saveStudentDocument;
	private ComboBox									docTypeComboBox;
	private TextField									docDescriptinoField;
	private Button										docUpload;
	private Panel										docPreviewPanel;
	private Button										docSaveButton;
	private Button										docCancelButton;
	private Button										docDeleteButton;
	private Button										docPreviewButton;
	private StudentDocument								studentDocument;
	
	private Table										studentDocumentTable;
	
	private VerticalLayout studentDocumentTable(){
		studentDocumentLayout = new VerticalLayout();
		studentDocumentLayout.setWidth("100%");
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setWidth("100%");
		horizontalLayout.setSpacing(true);
		
		docTypeComboBox = new ComboBox("Type");
		docTypeComboBox.setWidth("100%");
		docTypeComboBox.addItem("Result");
		docTypeComboBox.addItem("Certificate");
		docDescriptinoField = new TextField("Description");
		docDescriptinoField.setWidth("100%");
		docUpload = new Button("Upload");
		//docUpload.setWidth("100%");
		/*docPreviewPanel = new Panel("View");
		docPreviewPanel.setWidth("300px");
		docPreviewPanel.setHeight("320px");*/
		
		studentDocumentTable = new Table("Documents",studentDocumentContainer);
		studentDocumentTable.setWidth("100%");
		studentDocumentTable.setColumnHeader(STUDENT_DOCUMENT_TYPE, "Type");
		studentDocumentTable.setColumnHeader(STUDENT_DOCUMENT_DESCRIPTION, "Description");
		studentDocumentTable.setColumnHeader(STUDENT_DOCUMENT_NAME, "Name");
		//studentDocumentTable.setColumnHeader("CHANGE", "Change");
		
		studentDocumentTable.setVisibleColumns(new String[] {STUDENT_DOCUMENT_TYPE,STUDENT_DOCUMENT_DESCRIPTION,STUDENT_DOCUMENT_NAME});
		studentDocumentTable.setSelectable(true);
		
		HorizontalLayout docButtonLayout = new HorizontalLayout();
		docButtonLayout.setSpacing(true);
		docSaveButton = new Button("Save");
		docCancelButton = new Button("Cancel");
		docDeleteButton = new Button("Delete");
		docPreviewButton = new Button("Preview");
		
		docSaveButton.setEnabled(false);
		
		docButtonLayout.addComponent(docSaveButton);
		docButtonLayout.addComponent(docCancelButton);
		docButtonLayout.addComponent(docDeleteButton);
		docButtonLayout.addComponent(docPreviewButton);		
		
		horizontalLayout.addComponent(docTypeComboBox);
		horizontalLayout.addComponent(docDescriptinoField);
		horizontalLayout.addComponent(docUpload);
		//horizontalLayout.setComponentAlignment(docUpload, Alignment.MIDDLE_CENTER);
		
		studentDocumentLayout.addComponent(horizontalLayout);
		studentDocumentLayout.addComponent(studentDocumentTable);
		studentDocumentLayout.addComponent(docButtonLayout);
		studentDocumentLayout.setComponentAlignment(docButtonLayout, Alignment.BOTTOM_RIGHT);
		
		docUpload.addListener(new Listener() {
			public void componentEvent(Event event) {
				if (student!=null) {
					if(docTypeComboBox.getValue()!=null && docDescriptinoField.getValue()!=""){
						uptype = 2;
						docUploadPopup("Docmument Upload");					
					}else{
						window.showNotification("Select Type and Give some description",Window.Notification.TYPE_WARNING_MESSAGE);
					}
				} else {
					window.showNotification("Document can't be uploaded without student!", Window.Notification.TYPE_WARNING_MESSAGE);
				}
				
			}
		});
		docDeleteButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				StudentDocumentDto selectedDoc = (StudentDocumentDto) studentDocumentTable.getValue();
				if(selectedDoc!=null){
					try {
						if(!selectedDoc.isIsnew()){
							StudentDocumentLocalServiceUtil.deleteDocument(selectedDoc.getDocumentId(), selectedDoc.getFileEntryId());
							window.showNotification("Document Deleted!!");							
						}
						studentDocumentContainer.removeItem(selectedDoc);
						studentDocumentTable.refreshRowCache();
					} catch (PortalException e) {
						e.printStackTrace();
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}else{
					window.showNotification("Select a document from table",Window.Notification.TYPE_WARNING_MESSAGE);
				}
			}
		});
		
		docSaveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				if(student!=null){
					if(studentDocumentContainer.size()!=0){
						int count = 0; 
						for (int i = 0; i < studentDocumentContainer.size(); i++) {
							StudentDocumentDto documentDto = studentDocumentContainer.getIdByIndex(i);
							File file = documentDto.getFile();
							StudentDocument studentDocument = new StudentDocumentImpl();
							studentDocument.setCompanyId(student.getCompanyId());
							studentDocument.setDescription(documentDto.getDescription());
							studentDocument.setOrganizationId(student.getOrganizationId());
							studentDocument.setType(documentDto.getType());
							studentDocument.setModifiedDate(new Date());
							studentDocument.setStudentId(student.getStudentId());
							//studentDocument.setDocumentId(documentDto.getDocumentId());
							try {
								if(file!=null){
									if(documentDto.isIsnew()){
										StudentDocumentLocalServiceUtil.saveStudentDocmument(student, file, studentDocument);
										count ++;
									}							
								}
							} catch (PortalException e) {
								e.printStackTrace();
							} catch (SystemException e) {
								e.printStackTrace();
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							}
							if(count>0){
								window.showNotification(count+"- Documents saved successfully");
							}
						}
					}else{
						window.showNotification("Nothing to save",Window.Notification.TYPE_WARNING_MESSAGE);
					}
				}else{
					window.showNotification("Save or Find a Student first!!",Window.Notification.TYPE_WARNING_MESSAGE);
				}				
			}
		});
		
		docCancelButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				loadStudentDocumentTable();
				docSaveButton.setEnabled(false);
				docDeleteButton.setEnabled(false);
			}
		});
		
		docPreviewButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				//window.showNotification(PropsUtil.get("dl.hook.file.system.root.dir.pdf"));
				reportWindow();
				/*StudentDocumentDto documentDto = (StudentDocumentDto) studentDocumentTable.getValue();
				if(documentDto!=null){
					docPreviewWindow("Preview",documentDto.getFile());					
				}else{
					window.showNotification("Select a document to preview!!",Window.Notification.TYPE_WARNING_MESSAGE);
				}*/
			}
		});
		
		return studentDocumentLayout;
	}
	
	private VerticalLayout 			experienceLayout;
	private Button 					moreExperienceButton;
	private Button 					resetExperienceButton;
	private Button 					saveExperience;
	private Button					deleteExperience;
	private ExperianceDto			selectedExperience;
	
	private VerticalLayout experienceTable() {
		experienceLayout = new VerticalLayout();
		experienceTable = new Table("Experience", experianceRecordContainer){
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				if(property.getType()==Date.class)
				{
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					if(property.getValue()!=null){
						return dateFormat.format(property.getValue());
					}
				}
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		experienceTable.setSelectable(true);
		experienceTable.setEditable(true);
		experienceTable.setWidth("100%");
		experienceTable.setHeight("190px");
		
		experienceTable.setColumnHeader(EXPERIANCE_RECORD_ORGANIZATION,"Organization");
		experienceTable.setColumnHeader(EXPERIANCE_RECORD_DESINATION,"Designation");
		experienceTable.setColumnHeader(EXPERIANCE_RECORD_STARTDATE,"Start Date");
		experienceTable.setColumnHeader(EXPERIANCE_RECORD_ENDDATE,"End Date");
		experienceTable.setVisibleColumns(new String[] {EXPERIANCE_RECORD_ORGANIZATION, EXPERIANCE_RECORD_DESINATION,
				EXPERIANCE_RECORD_STARTDATE, EXPERIANCE_RECORD_ENDDATE});

		experienceLayout.addComponent(experienceTable);
		//experienceTable.addItem(new ExperianceDto());

		HorizontalLayout experienceButtonLayout = new HorizontalLayout();
		experienceButtonLayout.setWidth("100%");
		experienceButtonLayout.setSpacing(true);
		Label expandLabel = new Label();
		experienceButtonLayout.addComponent(expandLabel);
		experienceLayout.addComponent(experienceButtonLayout);

		moreExperienceButton = new Button("More");
		experienceButtonLayout.addComponent(moreExperienceButton);
		/*resetExperienceButton = new Button("Reset");
		experienceButtonLayout.addComponent(resetExperienceButton);*/

		moreExperienceButton.addListener(new ClickListener() {

			public void buttonClick(ClickEvent event) {

				experienceTable.addItem(new ExperianceDto());
				saveExperience.setEnabled(true);
			}
		});

		/*resetExperienceButton.addListener(new ClickListener() {

			public void buttonClick(ClickEvent event) {
				experienceTable.removeAllItems();
				experienceTable.addItem(new ExperianceDto());
			}
		});*/

		saveExperience = new Button("Save");
		if(student.isNew()){
			saveExperience.setEnabled(false);
		}
		experienceButtonLayout.addComponent(saveExperience);

		saveExperience.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				List<Experiance> experiances = new ArrayList<Experiance>();
				for (int index = 0; index < experianceRecordContainer.size(); index++) {
					Experiance record = new ExperianceImpl();
					ExperianceDto experianceDto = experianceRecordContainer.getIdByIndex(index);

					record.setStudentId(student.getStudentId());
					record.setCompanyId(companyId);
					record.setCreateDate(new Date());
					record.setUserId(student.getUserId());
					record.setOrganizationId(student.getOrganizationId());
					
					String designation  = experianceDto.getDesignation();
					String organization = experianceDto.getOrganization();
					//int status = experianceDto.getCurrentStatus();					
					
					
					if(designation!="" && organization!=""){
						record.setDesignation(designation);
						record.setOrganization(organization);
						//record.setCurrentStatus(status);
						record.setStartDate(experianceDto.getStartDate());
						record.setEndDate(experianceDto.getEndDate());
						
						experiances.add(record);
					}else{						
						window.showNotification("Fill the filds correctly",Window.Notification.TYPE_WARNING_MESSAGE);
						experiances.clear();
						experiances = null;
						break;
					}					
				}
				
				if(experiances!=null && experianceRecordContainer.size()>0 ){
					if(student.getStudentId()!=0){
						try {
							ExperianceLocalServiceUtil.saveStudentExperiences(experiances, student.getStudentId());
							window.showNotification("Record Saved");
							loadExperienceTable();
						} catch (SystemException e) {
							e.printStackTrace();
						}
					}else{
						window.showNotification("No student was saved! please save general info first",Window.Notification.TYPE_WARNING_MESSAGE);
					}
				}else{
					window.showNotification("No Informatio inserted!",Window.Notification.TYPE_WARNING_MESSAGE);
				}
				
			}
		});
		deleteExperience = new Button("Delete");
		experienceButtonLayout.addComponent(deleteExperience);
		experienceButtonLayout.setExpandRatio(expandLabel, 1);
		
		deleteExperience.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				selectedExperience = (ExperianceDto) experienceTable.getValue();
				
				if(selectedExperience!=null){
					if(selectedExperience.getDesignation()!="" && selectedExperience.getOrganization()!=null){
						if(!student.isNew()){
							try {
								ExperianceLocalServiceUtil.deleteExperiance(selectedExperience.getExperianceId());
								window.showNotification("Record Deleted");
								loadExperienceTable();
							} catch (PortalException e) {
								e.printStackTrace();
							} catch (SystemException e) {
								e.printStackTrace();
							}
						}else{
							experienceTable.removeItem(selectedExperience);
							experienceTable.refreshRowCache();
						}
						
					}else{
						experienceTable.removeItem(selectedExperience);
						experienceTable.refreshRowCache();
					}
					
				}else{
					window.showNotification("Select a Record!",Window.Notification.TYPE_WARNING_MESSAGE);
				}
			}
		});
		
		return experienceLayout;
	}

	private enum ExperienceStatus implements Serializable
	{
		CONTINUING(1,"Continuing"),
		LEFT(2,"Left");
		
		private int key;
		private String value;
		
		private ExperienceStatus(int key,String value)
		{
			this.key = key;
			this.value = value;
		}
		
		@Override
		public String toString()
		{
			return value;
		}
		
	}

	private ExperienceStatus getStatus(int key)
	{		
		ExperienceStatus statuses[]=ExperienceStatus.values();
		
		for(ExperienceStatus status:statuses)
		{
			if(status.key==key)
			{
				return status;
			}
		}		
		return null;
	}

	private VerticalLayout 			batchLayout;
	private Button 					moreBatchButton;
	private Button 					resetBatchButton;
	private Button 					saveBatch;
	
	//BeanItemContainer<BatchDto> batchContainer;
	
	private final static String BATCH_RCORD_BATCHSESSION 	= "batchSession";
	private final static String BATCH_RCORD_BATCHSTARTDATE 	= "batchStartDate";
	private final static String BATCH_RCORD_BATCHENDDATE 	= "batchEndDate";
	private final static String BATCH_RCORD_BATCH 			= "batchName";
	private final static String BATCH_RCORD_STATUS 			= "status";
	private final static String BATCH_RCORD_NOTE 			= "note";

	private VerticalLayout batchTable() {
		batchLayout = new VerticalLayout();
		
		//batchRecordContainer = new BeanItemContainer<BatchDto>(BatchDto.class);
		batchTable = new Table("Batch", batchRecordContainer)
		{
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				if(property.getType()==Date.class)
				{
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					if(property.getValue()!=null){
						return dateFormat.format(property.getValue());
					}
				}
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		batchLayout.addComponent(batchTable);
		batchTable.setSelectable(true);
		batchTable.setWidth("100%");
		batchTable.setHeight("190px");
		
		batchTable.setColumnHeader(BATCH_RCORD_BATCHSESSION, "Session");
		batchTable.setColumnHeader(BATCH_RCORD_BATCHSTARTDATE, "Start Date");
		batchTable.setColumnHeader(BATCH_RCORD_BATCHENDDATE, "End Date");
		batchTable.setColumnHeader(BATCH_RCORD_BATCH, "Batch");
		batchTable.setColumnHeader(BATCH_RCORD_STATUS, "Status");
		batchTable.setColumnHeader(BATCH_RCORD_NOTE, "Note");

		batchTable.setVisibleColumns(new String[] {BATCH_RCORD_BATCH, BATCH_RCORD_BATCHSESSION,BATCH_RCORD_BATCHSTARTDATE, BATCH_RCORD_BATCHENDDATE,
						BATCH_RCORD_STATUS,BATCH_RCORD_NOTE });

		batchTable.setColumnAlignment(BATCH_RCORD_BATCHSESSION,Table.ALIGN_RIGHT);
		batchTable.setColumnAlignment(BATCH_RCORD_BATCHSTARTDATE,Table.ALIGN_RIGHT);
		batchTable.setColumnAlignment(BATCH_RCORD_BATCHENDDATE,Table.ALIGN_RIGHT);
		batchTable.setColumnAlignment(BATCH_RCORD_BATCH,Table.ALIGN_RIGHT);
		batchTable.setColumnAlignment(BATCH_RCORD_STATUS, Table.ALIGN_RIGHT);
		batchTable.setColumnAlignment(BATCH_RCORD_NOTE, Table.ALIGN_RIGHT);
		
		batchTable.setColumnExpandRatio(BATCH_RCORD_BATCHSESSION,1);
		batchTable.setColumnExpandRatio(BATCH_RCORD_BATCHSTARTDATE,1);
		batchTable.setColumnExpandRatio(BATCH_RCORD_BATCHENDDATE,1);
		batchTable.setColumnExpandRatio(BATCH_RCORD_BATCH,2);
		batchTable.setColumnExpandRatio(BATCH_RCORD_STATUS,2);
		batchTable.setColumnExpandRatio(BATCH_RCORD_NOTE,2);
		
		batchTable.setPageLength(15);
		batchTable.setWidth("100%");
		batchTable.setSelectable(true);

		//batchTable.addItem(new BatchDto());	

		return batchLayout;
	}

	private TextField 					studentDetailField;
	private ComboBox					assignStudentBatchCombo;
	private ComboBox					assignActionComboBox;
	private ComboBox					allRunningBatchesCombo;
	private ComboBox					toBatchComboBox;
	private GridLayout 					assignStudentLayout;
	private Button 						assignDeleteButton;
	
	private GridLayout assignStudentLayout(){
		
		if (assignStudentLayout!=null) {
			assignStudentLayout.removeAllComponents();
		}else{
			assignStudentLayout = new GridLayout(2,6);
		}
		
		assignStudentLayout.setWidth("60%");
		
		studentDetailField = new TextField("Student Info");
		studentDetailField.setValue(student.getStudentId()+"-"+student.getName());
		studentDetailField.setWidth("100%");
		studentDetailField.setReadOnly(true);
		
		assignActionComboBox = new ComboBox("Actions");
		//assignActionComboBox.setWidth("100%");
		assignActionComboBox.setImmediate(true);
		assignActionComboBox.addItem("Assign to Another Batch");
		assignActionComboBox.addItem("Transfer to Another Batch");
		assignActionComboBox.setNullSelectionAllowed(false);
		
		assignStudentBatchCombo = new ComboBox("Student's Batches");
		assignStudentBatchCombo.setWidth("100%");
		
		allRunningBatchesCombo = new ComboBox();		
		
		toBatchComboBox = new ComboBox("TO");
		toBatchComboBox.setImmediate(true);
		
		assignStudentLayout.addComponent(studentDetailField,0,0,0,1);
		assignStudentLayout.addComponent(assignActionComboBox,0,2,0,2);
		
		assignActionComboBox.addListener(new Listener() {
			
			public void componentEvent(Event event) {
				saveBatch.setEnabled(true);
				List<BatchDto> allRunningBatches = getBatchDtoListByOrgId(student.getOrganizationId());
				String action = (String) assignActionComboBox.getValue();
				
				if(action.equalsIgnoreCase("Assign to Another Batch")){
					allRunningBatchesCombo.removeAllItems();
					allRunningBatchesCombo.setCaption("New Batches");
					
					for(BatchDto batches : allRunningBatches){	
						if(batches.getStatusKey()== BatchStatus.RUNNING.getKey()){
							allRunningBatchesCombo.addItem(batches);
						}						
					}
					
					for(BatchStudent studentBatches : batchStudentlist){
						for(BatchDto batches : allRunningBatches){
							long batchId = batches.getBatchId();
							if(batchId==studentBatches.getBatchId()){
								allRunningBatchesCombo.removeItem(batches);
							}											
						}						
					}
					
					assignStudentLayout.removeComponent(toBatchComboBox);
										
				}else if(action.equalsIgnoreCase("Transfer to Another Batch")){
					allRunningBatchesCombo.removeAllItems();
					toBatchComboBox.removeAllItems();
					allRunningBatchesCombo.setCaption("From");
					
					for(BatchStudent studentBatches : batchStudentlist){						
						if(studentBatches.getStatus()== BatchStatus.RUNNING.getKey()){
							for(BatchDto batchDto: allRunningBatches){								
								if(studentBatches.getBatchId() == batchDto.getBatchId()){
									batchDto.setBatchStudentId(studentBatches.getBatchStudentId());
									allRunningBatchesCombo.addItem(batchDto);
								}								
							}							
						}					
					}
					
					for(BatchDto batches : allRunningBatches){	
						if(batches.getStatusKey()== BatchStatus.RUNNING.getKey()){
							toBatchComboBox.addItem(batches);
						}						
					}
					
					for(BatchStudent studentBatches : batchStudentlist){
						for(BatchDto batches : allRunningBatches){
							long batchId = batches.getBatchId();
							if(batchId==studentBatches.getBatchId()){
								toBatchComboBox.removeItem(batches);
							}											
						}						
					}
					
					assignStudentLayout.removeComponent(toBatchComboBox);
					assignStudentLayout.addComponent(toBatchComboBox,1,3,1,3);
				}
				assignStudentLayout.removeComponent(allRunningBatchesCombo);
				assignStudentLayout.addComponent(allRunningBatchesCombo,0,3,0,3);
			}
		});
				
		HorizontalLayout batchButtonLayout = new HorizontalLayout();
		batchButtonLayout.setSpacing(true);
		assignStudentLayout.addComponent(batchButtonLayout,1,5);
		
		/*moreBatchButton = new Button("More");
		batchButtonLayout.addComponent(moreBatchButton);
		resetBatchButton = new Button("Reset");
		batchButtonLayout.addComponent(resetBatchButton);

		moreBatchButton.addListener(new ClickListener() {

			public void buttonClick(ClickEvent event) {
				batchTable.addItem(new BatchDto());
			}
		});

		resetBatchButton.addListener(new ClickListener() {

			public void buttonClick(ClickEvent event) {
				batchTable.removeAllItems();
				batchTable.addItem(new BatchDto());
			}
		});*/

		saveBatch = new Button("Save");		
		saveBatch.setEnabled(false);
		
		assignDeleteButton = new Button("Delete");
		
		assignDeleteButton.addListener(new ClickListener() {			
			public void buttonClick(ClickEvent event) {
				BatchDto batchStudentDto = (BatchDto) batchTable.getValue();
				if(batchStudentDto!=null){
					if(batchRecordContainer.size()>1){
						try {
							BatchStudentLocalServiceUtil.deleteBatchStudent(batchStudentDto.getBatchStudentId());
							window.showNotification("Batch Deleted Successfully");
							loadStudentBatchTable();
						} catch (PortalException e) {
							e.printStackTrace();
						} catch (SystemException e) {
							
							e.printStackTrace();
						}
					}else{
						window.showNotification("Student has to have atleast one batch, you can't delete lastone!",Window.Notification.TYPE_WARNING_MESSAGE);
					}
					
				}else{
					window.showNotification("Select A Batch",Window.Notification.TYPE_WARNING_MESSAGE);
				}
			}
		});
		
		saveBatch.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				
				BatchStudent batchStudent = new BatchStudentImpl();
				BatchDto newBatch = (BatchDto) allRunningBatchesCombo.getValue();
							
				if(newBatch!=null){
					long fromCourseId = newBatch.getCourseId();
					
					batchStudent.setBatchId(newBatch.getBatchId());
					batchStudent.setStudentId(student.getStudentId());
					batchStudent.setCompanyId(companyId);
					batchStudent.setOrganizationId(student.getOrganizationId());
					batchStudent.setStartDate(newBatch.getBatchStartDate());	
					try {
						if(assignActionComboBox.getValue().toString().equalsIgnoreCase("Assign to Another Batch")){
							batchStudent.setStatus(BatchStatus.RUNNING.getKey());
							//batchStudent.setNote("New Assigned");
							BatchStudentLocalServiceUtil.addBatchStudent(batchStudent);
							window.showNotification("Student is assigned to new batch successfully");
							loadStudentBatchTable();
							allRunningBatchesCombo.removeItem(newBatch);
							
						}else{	
							BatchDto toBatchDto = (BatchDto) toBatchComboBox.getValue();
							if(toBatchDto!=null){	
								long toCourseId = toBatchDto.getCourseId();
								
								batchStudent.setStatus(BatchStatus.TRANSFERED.getKey());
								batchStudent.setBatchStudentId(newBatch.getBatchStudentId());
								batchStudent.setNote("Tranfered To -"+toBatchDto.getBatchName());
								
								BatchStudent toNewBatch = new BatchStudentImpl();
								toNewBatch.setBatchId(toBatchDto.getBatchId());
								toNewBatch.setStudentId(student.getStudentId());
								toNewBatch.setCompanyId(companyId);
								toNewBatch.setOrganizationId(student.getOrganizationId());
								toNewBatch.setStartDate(toBatchDto.getBatchStartDate());
								toNewBatch.setStatus(BatchStatus.RUNNING.getKey());
								toNewBatch.setNote("Tranfered From -"+newBatch.getBatchName());
								//window.showNotification(fromCourseId+"-"+toCourseId);
								if(fromCourseId==toCourseId){
									BatchStudentLocalServiceUtil.updateBatchStudent(batchStudent);
									BatchStudentLocalServiceUtil.addBatchStudent(toNewBatch);	
									window.showNotification("Batch Transfer is successfully");
									
									toBatchComboBox.removeItem(toBatchDto);
									allRunningBatchesCombo.removeItem(newBatch);
									
									loadStudentBatchTable();
								}else{
									window.showNotification("Can't transfer to diffrent course",Window.Notification.TYPE_WARNING_MESSAGE);
								}								
								
							}else{
								window.showNotification("Please Select To Batch",Window.Notification.TYPE_WARNING_MESSAGE);
							}							
						}						
						
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}else{
					window.showNotification("Please Select a Batch",Window.Notification.TYPE_WARNING_MESSAGE);
				}				
			}
		});
		
		batchButtonLayout.addComponent(saveBatch);
		batchButtonLayout.addComponent(assignDeleteButton);
		
		return assignStudentLayout;
	}
	
	// upload panel
	public void showPopupWindow(String title) {

		myPopWindow = new Window(title);
		myPopWindow.setWidth("370px");
		myPopWindow.setPositionX(300);
		myPopWindow.setPositionY(300);

		// Create the Upload component.
		upload = new Upload("Upload here", this);

		// Use a custom button caption instead of plain "Upload".
		upload.setButtonCaption("Upload Now");

		// myPopWindow.setVisible(true)
		upload.addListener((Upload.SucceededListener) this);
		upload.addListener((Upload.FailedListener) this);
		upload.addListener((Upload.StartedListener) this);
		myPopWindow.addComponent(upload);
		// myPopWindow.addComponent(new
		// Label("Click 'Browse' to select a file and then click 'Upload'."));
		window.addWindow(myPopWindow);

	}
	public void docPreviewWindow(String title,final File file) {

		myPopWindow = new Window(title);
		myPopWindow.setWidth("60%");
		myPopWindow.setHeight("800px");
		myPopWindow.setPositionX(300);
		myPopWindow.setPositionY(300);
		
		StreamSource s = new StreamResource.StreamSource() {
			public InputStream getStream() {
				try {					
					FileInputStream fis = new FileInputStream(file);
					return fis;
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}
			}
		};
		
		StreamResource r = new StreamResource(s, "repy.pdf", this);
		
		Embedded e = new Embedded();
		e.setSizeFull();
		e.setHeight("700px");
		e.setType(Embedded.TYPE_BROWSER);
		r.setMIMEType("application/pdf");
		e.setSource(r);
		
		myPopWindow.addComponent(e);	

		window.addWindow(myPopWindow);

	}
	
	//test report
	
	SimpleDateFormat dateformat = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
	private final String timeStamp = dateformat.format(new Date());
	
	public void reportWindow() {
		
		myPopWindow = new Window("report");
		myPopWindow.setWidth("60%");
		myPopWindow.setHeight("800px");
		myPopWindow.setPositionX(300);
		myPopWindow.setPositionY(300);
		
		final Map imagelocation = new HashMap();
		imagelocation.put("studentId", student.getStudentId());
		/*long batchid = 25;
		imagelocation.put("batchId",batchid );*/
		
		StreamSource s = new StreamResource.StreamSource() {
			public InputStream getStream() {
				try {		
					try {
						jasperPrint = JasperFillManager.fillReport(PropsUtil.get("dl.hook.file.system.root.dir.report") + "/" 
					+"Student_Profile.jasper", imagelocation,connection);
						
					} catch (JRException e2) {
						e2.printStackTrace();
					}
					
					
					JasperExportManager.exportReportToPdfFile(jasperPrint,PropsUtil.get("dl.hook.file.system.root.dir.pdf")
							+"/"+user.getUserId()+"_"+timeStamp+ ".pdf");
					
					System.out.println("location ----------------------------"+PropsUtil.get("dl.hook.file.system.root.dir.pdf")
							+"/"+user.getUserId()+"_"+timeStamp+ ".pdf");
					
					File file = new File(PropsUtil.get("dl.hook.file.system.root.dir.pdf")
							+"/"+user.getUserId()+"_"+timeStamp+ ".pdf");
					
					//System.out.println("Absolute pateh --------------"+file.getAbsolutePath());
					FileInputStream fis = new FileInputStream(file);
					return fis;
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}
			}
		};
		
		StreamResource r = new StreamResource(s, "repy.pdf", this);
		
		Embedded e = new Embedded();
		e.setSizeFull();
		e.setHeight("700px");
		e.setType(Embedded.TYPE_BROWSER);
		r.setMIMEType("application/pdf");
		e.setSource(r);
		
		myPopWindow.addComponent(e);	

		window.addWindow(myPopWindow);
		
		myPopWindow.addListener(new CloseListener() {
			public void windowClose(CloseEvent e) {
				File file = new File(PropsUtil.get("dl.hook.file.system.root.dir.pdf")
						+"/"+user.getUserId()+"_"+timeStamp+ ".pdf");
				file.delete();
			}
		});
	}
	
	
	// test report
	
	
	// upload panel
		public void docUploadPopup(String title) {

			myPopWindow = new Window(title);
			myPopWindow.setWidth("400px");
			myPopWindow.setPositionX(300);
			myPopWindow.setPositionY(300);
			
			
			// Create the Upload component.
			upload = new Upload("Upload here", this);

			// Use a custom button caption instead of plain "Upload".
			upload.setButtonCaption("Upload Now");
			
			/*upload.addListener(new StartedListener() {
				public void uploadStarted(StartedEvent event) {
					if(event.getMIMEType().equalsIgnoreCase("application/pdf")){
						
						
					}else{
						window.showNotification("Please upload pdf format document!",Window.Notification.TYPE_WARNING_MESSAGE);
						file.delete();
					}
				}
			});*/

			// myPopWindow.setVisible(true)
			upload.addListener((Upload.SucceededListener) this);
			upload.addListener((Upload.FailedListener) this);
			upload.addListener((Upload.StartedListener) this);
			myPopWindow.addComponent(upload);
			// myPopWindow.addComponent(new
			// Label("Click 'Browse' to select a file and then click 'Upload'."));
			window.addWindow(myPopWindow);

		}

	public void uploadStarted(StartedEvent event) {
		/*if(event.getMIMEType().equalsIgnoreCase("image/jpeg")&& uptype != 1||event.getMIMEType().equalsIgnoreCase("image/png")&& uptype != 1||event.getMIMEType().equalsIgnoreCase("image/gif")&& uptype != 1||event.getMIMEType().equalsIgnoreCase("image/bmp") && uptype != 1 ){
			window.showNotification("error");
			return;
		}else if(event.getMIMEType().equalsIgnoreCase("application/pdf") && uptype != 2){
			return;
		}*/
	}

	public void uploadFailed(FailedEvent event) {
		myPopWindow.addComponent(new Label("Uploading " + event.getFilename()
				+ " of type '" + event.getMIMEType() + "' failed."));
	}

	public void uploadSucceeded(SucceededEvent event) {
		// Log the upload on screen.
		
		if(uptype==1){
			if(event.getMIMEType().equalsIgnoreCase("image/jpeg") || event.getMIMEType().equalsIgnoreCase("image/gif") || event.getMIMEType().equalsIgnoreCase("image/png") ||event.getMIMEType().equalsIgnoreCase("image/bmp")){
				// Display the uploaded file in the image panel.
					final FileResource imageResource = new FileResource(file, this);
					photoPanel.removeAllComponents();
					Embedded image = new Embedded("", imageResource);
					image.setWidth("100%");
					image.setHeight("125px");
					photoPanel.addComponent(image);	
					myPopWindow.setVisible(false);
					uptype = 0;
					myPopWindow.addComponent(new Label("File " + event.getFilename()
							+ " of type '" + event.getMIMEType() + "' uploaded."));
				}else{
					window.showNotification("Please upload JPG/PNG/GIF/BMP format image",Window.Notification.TYPE_WARNING_MESSAGE);
					//file.delete();
				}
			}else if(uptype==2){
			if(event.getMIMEType().equalsIgnoreCase("application/pdf")){
				StudentDocumentDto studentDocumentDto = new StudentDocumentDto();
				studentDocumentDto.setDescription(docDescriptinoField.getValue().toString());
				studentDocumentDto.setType(docTypeComboBox.getValue().toString());
				studentDocumentDto.setName(file.getName());
				studentDocumentDto.setFile(file);
				studentDocumentDto.setIsnew(true);
				
				studentDocumentContainer.addBean(studentDocumentDto);
				studentDocumentTable.refreshRowCache();
				docDescriptinoField.setValue("");
				docTypeComboBox.setValue("");
				myPopWindow.setVisible(false);
				docSaveButton.setEnabled(true);
				uptype = 0;
				myPopWindow.addComponent(new Label("File " + event.getFilename()
						+ " of type '" + event.getMIMEType() + "' uploaded."));
			}else{
				window.showNotification("Please upload pdf format document! inner error",Window.Notification.TYPE_WARNING_MESSAGE);
				//file.delete();
			}
		}
		
		
	}

	public OutputStream receiveUpload(String filename, String mimeType) {

		FileOutputStream fos = null; // Output stream to write to

		File tmpdir = new File("/tmp/uploads/");
		if (!tmpdir.exists()) {
			tmpdir.mkdirs();
			// System.out.println("Dir created ..");
		}
		// System.out.println("Exists created ..");
		file = new File(tmpdir, filename);
		try {
//			if(mimeType.equalsIgnoreCase("image/jpeg")&& uptype == 1||mimeType.equalsIgnoreCase("image/png")&& uptype == 1||mimeType.equalsIgnoreCase("image/gif")&& uptype == 1||mimeType.equalsIgnoreCase("image/bmp") && uptype == 1 ){
				fos = new FileOutputStream(file);
//			}else if(mimeType.equalsIgnoreCase("application/pdf") && uptype == 2){
//				fos = new FileOutputStream(file);
//			}
			// Open the file for writing.
			
			
		} catch (final java.io.FileNotFoundException e) {
			// Error while opening the file. Not reported here.
			e.printStackTrace();
			return null;
		}

		return fos; // Return the output stream to write to

	}

	private NumericField serchTextField;
	public HorizontalLayout easySearch() {
		HorizontalLayout easySearchLayout = new HorizontalLayout();
		easySearchLayout.setWidth("100%");
		easySearchLayout.setSpacing(true);
		serchTextField = new NumericField();
		Label textLabel = new Label("Student Id");
		textLabel.setWidth("100%");
		/*serchTextField.setNullRepresentation("");
		serchTextField.setNullSettingAllowed(true);*/
		serchTextField.setWidth("100%");
		serchTextField.setNullRepresentation("");
		serchTextField.setNullSettingAllowed(true);
		serchTextField.setNumberType(NumericFieldType.INTEGER);

		Button searchButton = new Button("Search");
		//searchButton.setWidth("100%");
		Label expandLabel = new Label();
		Label expandLabel1 = new Label();
		

		easySearchLayout.addComponent(expandLabel);
		easySearchLayout.addComponent(textLabel);
		easySearchLayout.addComponent(serchTextField);
		easySearchLayout.addComponent(searchButton);
		easySearchLayout.addComponent(expandLabel1);
		easySearchLayout.setExpandRatio(expandLabel, 3);
		easySearchLayout.setExpandRatio(textLabel, 1);
		easySearchLayout.setExpandRatio(serchTextField, 2);
		easySearchLayout.setExpandRatio(searchButton, 1);
		easySearchLayout.setExpandRatio(expandLabel1, 3);

		searchButton.addListener(new ClickListener() {		

			public void buttonClick(ClickEvent event) {
				blankStudentPanel();
				String searchValue = (String) serchTextField.getValue();
				if(searchValue!="" && searchValue!=null){
					int searchStudentId = Integer.parseInt(serchTextField.getValue().toString());

					try {
						student = StudentLocalServiceUtil.findStudentByOrgIdStudentId(searchStudentId, userOrganizationsList);
					} catch (SystemException e) {
						e.printStackTrace();
					}
					if (student != null) {
						
						//window.showNotification("Student is  found."+ student.getStudentId());
						
						student.setNew(false);
						studentIdLabel.setReadOnly(false);
						studentIdLabel.setValue(student.getStudentId());
						studentIdLabel.setReadOnly(true);	
										
						showStudentDetail(student);
						
						batchLayout.addComponent(assignStudentLayout());
						deleteButton.setEnabled(true);
					} else {
						window.showNotification("Student is not found. "
								+ searchStudentId);
						blankStudentPanel();
						photoPanel.removeAllComponents();
						student = new StudentImpl();
						student.setNew(true);
					}
				}else{
					window.showNotification("Please provide student ID:", Window.Notification.TYPE_WARNING_MESSAGE);
				}
				
			}
		});
		return easySearchLayout;
	}

	private Table 						searchTable;
	private ComboBox 					batchListBox;
	private ComboBox 					courseCodeListBox;
	private ComboBox					campusComboBox;
	private TextField 					nameField;
	
	private List<CourseDTO> 			courseDtoList;
	
	private long 						searchOrganizationId;
	

	public VerticalLayout customSearch() {
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setWidth("100%");
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		//horizontalLayout.setWidth("100%");
		horizontalLayout.setSpacing(true);
		
		campusComboBox = new ComboBox("Campus");
		campusComboBox.setVisible(false);
		campusComboBox.setImmediate(true);
		
		courseCodeListBox = new ComboBox();
		
		batchListBox = new ComboBox();
		batchListBox.setImmediate(true);
		
		nameField = new TextField("Student Name");
		nameField.setWidth("100%");
		nameField.setImmediate(true);

		if(userOganizationsDtos!=null){
			if(userOganizationsDtos.size()>1){
				campusComboBox.setVisible(true);
				for(OrganizationDto orgnization: userOganizationsDtos){
					campusComboBox.addItem(orgnization);
				}
				horizontalLayout.addComponent(campusComboBox);
			}
			else if(userOrganizationsList.size()==1){
				searchOrganizationId = userOrganizationsList.get(0).getOrganizationId();
				getCourseDtoByOrgId(searchOrganizationId);

				if (courseDtoList!=null) {
					courseCodeListBox.removeAllItems();
					for( CourseDTO courseDto: courseDtoList){
						courseCodeListBox.addItem(courseDto);
					}
				}
			}
			else{
				searchOrganizationId = 0;
			}
		}		
		
		campusComboBox.addListener(new Listener() {			
			public void componentEvent(Event event) {
				OrganizationDto organizationDto = (OrganizationDto) campusComboBox.getValue();
				if(organizationDto!=null){
					searchOrganizationId = organizationDto.getOrganizationId();
					getCourseDtoByOrgId(searchOrganizationId);
					
					if (courseDtoList!=null) {
						courseCodeListBox.removeAllItems();
						for( CourseDTO courseDto: courseDtoList){
							courseCodeListBox.addItem(courseDto);
						}
					}
				}else{
					courseCodeListBox.removeAllItems();
					customSearchContainer.removeAllItems();
					searchTable.refreshRowCache();
				}
				
			}
		});		
		
		courseCodeListBox.setImmediate(true);
		courseCodeListBox.setCaption("By Course");
		
		courseCodeListBox.addListener(new Listener() {
			public void componentEvent(Event event) {
				CourseDTO courseDTO = (CourseDTO) courseCodeListBox.getValue();
				if(courseDTO!=null){
					long courseId = courseDTO.getCourseId();
					getBatchDtoByCourseId(courseId);
					if(searchBatchDtoList!=null){
						batchListBox.removeAllItems();
						for(BatchDto batchDto:searchBatchDtoList){
							batchListBox.addItem(batchDto);
						}
						getBatchStudents(searchBatchDtoList);
						
						SimpleStringFilter filter = null;

						Filterable f = (Filterable) searchTable
								.getContainerDataSource();
						f.removeAllContainerFilters();
						
						loadCustomSearchTable();
					}
				}else{
					batchListBox.removeAllItems();
					customSearchContainer.removeAllItems();
					searchTable.refreshRowCache();
				}
			}
		});

		batchListBox.setCaption("By Batch");
		
		horizontalLayout.addComponent(courseCodeListBox);
		horizontalLayout.addComponent(batchListBox);
		horizontalLayout.addComponent(nameField);
		verticalLayout.addComponent(horizontalLayout);

		searchTable = new Table("Search Result");
		customSearchContainer = new BeanItemContainer<CustomSearchDto>(CustomSearchDto.class);

		searchTable.setContainerDataSource(customSearchContainer);
		
		batchListBox.addListener(new Listener() {
			SimpleStringFilter filter = null;

			public void componentEvent(Event event) {
				Filterable f = (Filterable) searchTable
						.getContainerDataSource();				
				
					if (filter != null)
						f.removeContainerFilter(filter);
					if (batchListBox.getValue() != null && batchListBox.getValue()!="") {
					// Set new filter for the "Name" column
					
					filter = new SimpleStringFilter(COLUMN_BATCH_NAME, batchListBox.getValue().toString(), true, false);
					f.addContainerFilter(filter);
				}else{ 
					f.removeAllContainerFilters();
				}				
			}
		});

		nameField.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {				
			SimpleStringFilter filter = null;

				Filterable f = (Filterable) searchTable
						.getContainerDataSource();
				
					if (filter != null)
						f.removeContainerFilter(filter);
					if(nameField.getValue()!=null && nameField.getValue().toString()!=""){
					// Set new filter for the "Name" column
					filter = new SimpleStringFilter(COLUMN_STUDENT_NAME, nameField.getValue().toString(), true, false);
					f.addContainerFilter(filter);
				}else{
					f.removeAllContainerFilters();
				}
			}			
		});

		searchTable.setColumnHeader(COLUMN_COURSE, "Course");
		searchTable.setColumnHeader(COLUMN_BATCH_NAME, "Batch");
		searchTable.setColumnHeader(COLUMN_STUDENT_ID, "Student ID");
		searchTable.setColumnHeader(COLUMN_STUDENT_NAME, "Student Name");

		searchTable.setVisibleColumns(new String[] { COLUMN_COURSE,
				COLUMN_BATCH_NAME, COLUMN_STUDENT_ID, COLUMN_STUDENT_NAME });

		searchTable.setColumnAlignment(COLUMN_COURSE, Table.ALIGN_LEFT);
		searchTable.setColumnAlignment(COLUMN_BATCH_NAME, Table.ALIGN_LEFT);
		searchTable.setColumnAlignment(COLUMN_STUDENT_ID, Table.ALIGN_LEFT);
		searchTable.setColumnAlignment(COLUMN_STUDENT_NAME, Table.ALIGN_LEFT);
		searchTable.setPageLength(15);
		searchTable.setWidth("100%");
		searchTable.setSelectable(true);

		Button editButton = new Button("Show");		
		
		editButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				CustomSearchDto customSearchSelected = (CustomSearchDto)searchTable.getValue();
				if(customSearchSelected!=null){
					try {
						student = StudentLocalServiceUtil.fetchStudent(customSearchSelected.getStudentId());
						student.setNew(false);
						blankStudentPanel();
						showStudentDetail(student);
						tabSheet.setSelectedTab(initAdmissionForm);
						batchLayout.addComponent(assignStudentLayout());
						deleteButton.setEnabled(true);
						customSearchSelected = null;
					} catch (SystemException e) {
						e.printStackTrace();
					}
					
				}else{
					window.showNotification("Please Select a Student!",Window.Notification.TYPE_WARNING_MESSAGE);
					customSearchSelected = null;
				}
			}
		});

		verticalLayout.addComponent(searchTable);
		verticalLayout.addComponent(editButton);
		verticalLayout.setComponentAlignment(editButton, Alignment.BOTTOM_RIGHT);

		return verticalLayout;

	}	
	
	private void getCourseDtoByOrgId(long organizationId){
		try {
			List<CourseOrganization> courseOrganizations = CourseOrganizationLocalServiceUtil.findByOrganization(organizationId);
			courseDtoList = new ArrayList<CourseDTO>();
			for(CourseOrganization courseOrg : courseOrganizations){
				CourseDTO courseDto = new CourseDTO();
				long courseid = courseOrg.getCourseId();
				Course course = CourseLocalServiceUtil.fetchCourse(courseid);
				courseDto.setCourseId(courseid);
				courseDto.setCourseName(course.getCourseName());				
				courseDtoList.add(courseDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private List<BatchDto> searchBatchDtoList;
	private void getBatchDtoByCourseId(long courseId){
		try {
			List<Batch> batchList = BatchLocalServiceUtil.findBatchesByOrgCourseId(searchOrganizationId, courseId);	
			
			searchBatchDtoList = new ArrayList<BatchDto>();
			for(Batch batch : batchList){
				BatchDto batchDto = new BatchDto();
				batchDto.setBatchId(batch.getBatchId());
				batchDto.setBatchName(batch.getBatchName());
				batchDto.setCourseId(batch.getCourseId());
				batchDto.setCourseName(getCourseNameById(batch.getCourseId()));
				searchBatchDtoList.add(batchDto);
			}
						
		} catch (SystemException e) {
			e.printStackTrace();
		}		
	}
	
	private List<CustomSearchDto> customSearchStudentsList;
	private void getBatchStudents(List<BatchDto> batchDtos){
		customSearchStudentsList = new ArrayList<CustomSearchDto>();
		for(BatchDto batchDto : batchDtos){
			try {
				List<BatchStudent>batchStudents = BatchStudentLocalServiceUtil.findAllStudentByOrgBatchId(searchOrganizationId, batchDto.getBatchId());
				
				for(BatchStudent batchStudent:batchStudents){
					Student student;
					try {
						student = StudentLocalServiceUtil.findStudentByOrgStudentId(searchOrganizationId, batchStudent.getStudentId());
						CustomSearchDto customSearchDto = new CustomSearchDto();
						long batchId = batchStudent.getBatchId();
						
						for(BatchDto batch: batchDtos){
							if(batchId == batch.getBatchId()){
								customSearchDto.setBatchName(batchId+"-"+batch.getBatchName());
								customSearchDto.setCourseName(batch.getCourseName());
							}
						}
						
						customSearchDto.setStudentId(student.getStudentId());
						customSearchDto.setStudentName(student.getName());
						
						customSearchStudentsList.add(customSearchDto);
					} catch (NoSuchStudentException e) {
						e.printStackTrace();
					}					
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		
	}

	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request
				.getAttribute(WebKeys.THEME_DISPLAY);

	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
	}

	public void showBatchCombo(long userOrganizationid) {		
		try {
			batchList = BatchLocalServiceUtil.findBatchesByComOrg(companyId, userOrganizationid);

		} catch (SystemException e) {
			e.printStackTrace();
		}
		batchComboBox.removeAllItems();
		int status = BatchStatus.RUNNING.getKey();
		for (Batch batch : batchList) {
			if(status==batch.getStatus()){
				BatchDto batchDto = new BatchDto(batch);
				batchComboBox.addItem(batchDto);
			}			
		}
	}

	
	List<BatchStudent> 					batchStudentlist;
	
	public void showStudentDetail(Student student) {
		if(userOrganizationsList.size()>1){
			organizationOption.setValue(getOrgDto(student.getOrganizationId()));
		}
		
		studentNameField.setValue(student.getName());
		genderOption.select(student.getGender());
		studentIdLabel.setReadOnly(false);
		studentIdLabel.setValue(student.getStudentId());
		studentIdLabel.setReadOnly(true);
		dobField.setValue(student.getDateOfBirth());
		try {		
		phoneList=PhoneNumberLocalServiceUtil.findByPhoneNumberList(student.getStudentId());
		emailList=PersonEmailLocalServiceUtil.findByPersonEmailList(student.getStudentId());
		
		
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		if (phoneList != null) {		
						
			for (PhoneNumber phone : phoneList) {

				switch (phone.getOwnerType()) {
				case COLUMN_STUDENT_CONTACT:
					studentContactToken.addToken(phone.getPhoneNumber());
					break;
				case COLUMN_FATHER_CONTACT:
					fatherMobileToken.addToken(phone.getPhoneNumber());
					break;
				case COLUMN_MOTHER_CONTACT:
					motherMobileToken.addToken(phone.getPhoneNumber());
					break;
				case COLUMN_GARDIAN_CONTACT:
					gardianContactToken.addToken(phone.getPhoneNumber());
					break;
				case COLUMN_HOME_CONTACT:
					homePhoneToken.addToken(phone.getPhoneNumber());
					break;
				}
			}
		}		
		if (emailList != null) {
			for (PersonEmail email : emailList) {
				emailToken.addToken(email.getPersonEmail());
			}
		}
		
		loadStudentBatchTable();
		
		batchComboBox.setEnabled(false);
		
		presentAdressArea.setValue(student.getPresentAddress());
		permanentAdressArea.setValue(student.getPermanentAddress());

		fatherNameField.setValue(student.getFatherName());
		motherNameField.setValue(student.getMotherName());
		
		DLFileEntry fileEntry = null;
		String path="";
		
		if(student.getPhoto()!=0){
			try {
				fileEntry = DLFileEntryLocalServiceUtil.getDLFileEntry(student.getPhoto());
				
				path = StudentLocalServiceUtil.getDLFileAbsPath(fileEntry);
				
			} catch (PortalException e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			}
			
			File photoFile=new File(path);
		
			final FileResource imageResource = new FileResource(photoFile, this);
			photoPanel.removeAllComponents();
			
			Embedded image = new Embedded("", imageResource);
			image.setType(Embedded.TYPE_IMAGE);
			image.setWidth("100%");
			image.setHeight("125px");
			photoPanel.addComponent(image);
		}	
		
		loadStudentDocumentTable();

		loadAcademicRecordTable();		
		
		loadExperienceTable();				
		
	}

	private void loadExperienceTable() {
		try {
			studentExpariances=ExperianceLocalServiceUtil.findByStduentExperiance(student.getStudentId());
		} catch (SystemException e) {
			e.printStackTrace();
		}
		if (studentExpariances != null) {
			experianceRecordContainer.removeAllItems();
			for (Experiance experiance : studentExpariances) {
				ExperianceDto experianceDto = new ExperianceDto(experiance);
				experianceDto.setNew(false);
				experianceRecordContainer.addBean(experianceDto);
			}
		}
		experienceTable.refreshRowCache();
	}

	private void loadAcademicRecordTable() {
		try {
			studentAccademicRecords=AccademicRecordLocalServiceUtil.findByAccademicRecordList(student.getStudentId());
		} catch (SystemException e) {
			e.printStackTrace();
		}
		if (studentAccademicRecords != null) {
			academicRecordContainer.removeAllItems();
			for (AccademicRecord accademicRecord : studentAccademicRecords) {
				AcademicRecordDto academicRecordDto = new AcademicRecordDto(accademicRecord);
				academicRecordDto.setNew(false);
				academicRecordContainer.addBean(academicRecordDto);
			}
		}
		accademicRecordTable.refreshRowCache();
	}	
	
	private void loadStudentBatchTable(){

		try {
			batchStudentlist = BatchStudentLocalServiceUtil
					.findBystduentBatchStudentList(student.getStudentId());		
			
			batchRecordContainer.removeAllItems();
			for (BatchStudent batchStudent : batchStudentlist) {

				Batch batch = BatchLocalServiceUtil.getBatch(batchStudent
						.getBatchId());
				BatchDto batchDto = new BatchDto(batch);
				batchDto.setBatchStudentId(batchStudent.getBatchStudentId());
				batchDto.setBatchName(getBatchNameById(batch.getBatchId()));
				batchDto.setStatusKey(batchStudent.getStatus());
				batchDto.setNew(false);
				batchDto.setNote(batchStudent.getNote());
				
				if(batch.getSessionId()!=0){
					CourseSession courseSession =CourseSessionLocalServiceUtil.getCourseSession(batch.getSessionId());
					batchDto.setBatchSession(courseSession.getSessionName());
				}				
			//	System.out.println(batch.getSessionId()+" batch student List "+batchDto);			
				BatchStatus status = BatchStatus.getStatus(batchStudent.getStatus());
				if (status!=null) {			
					batchDto.setStatus(status.getValue());
				}
				
				batchRecordContainer.addBean(batchDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
		batchTable.refreshRowCache();
	}
	
	private void loadStudentDocumentTable(){

		try {
			stuentDocuments=StudentDocumentLocalServiceUtil.findByStduentDocumentList(student.getStudentId());	
			//window.showNotification("Student Document Size" + stuentDocuments.size());
			studentDocumentContainer.removeAllItems();
			docTypeComboBox.setValue("");
			docDescriptinoField.setValue("");
			for (StudentDocument studentDocument : stuentDocuments) {
				StudentDocumentDto documentDto = new StudentDocumentDto();
				documentDto.setDocumentId(studentDocument.getDocumentId());
				documentDto.setDescription(studentDocument.getDescription());
				documentDto.setType(studentDocument.getType());
				documentDto.setFileEntryId(studentDocument.getFileEntryId());	
				documentDto.setIsnew(false);
				
				DLFileEntry fileEntry = null;
				String path="";
				try {
					fileEntry = DLFileEntryLocalServiceUtil.getDLFileEntry(studentDocument.getFileEntryId());
					path = StudentLocalServiceUtil.getDLFileAbsPath(fileEntry);
				} catch (PortalException e) {
					e.printStackTrace();
				}
				File docFile=new File(path);
				
				documentDto.setFile(docFile);
				
			

				studentDocumentContainer.addBean(documentDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		studentDocumentTable.refreshRowCache();
	}
	
	private void loadCustomSearchTable(){
		//customSearchSelected= null;
		customSearchContainer.removeAllItems();
		for(CustomSearchDto customSearchDto :customSearchStudentsList){
			customSearchContainer.addBean(customSearchDto);
		}
		searchTable.refreshRowCache();
	}
	
	private List<BatchDto> getBatchDtoListByOrgId(long organizationId){
		List<BatchDto> batchDtoList = new ArrayList<BatchDto>();
		try {
			List<Batch> batchList = BatchLocalServiceUtil.findBatchesByComOrg(companyId, organizationId);
			if(batchList!=null){
				for(Batch batch: batchList){
					BatchDto batchDto = new BatchDto(batch);
					batchDto.setBatchName(getBatchNameById(batch.getBatchId()));
					batchDtoList.add(batchDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return batchDtoList;		
	}
	private String getCourseNameById(long courseId){
		String name = "";
		try {
			name = CourseLocalServiceUtil.getCourse(courseId).getCourseName();
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return name;
	}
	
	private String getBatchNameById(long batchId){
		String name = "";
		try {
			name = BatchLocalServiceUtil.getBatch(batchId).getBatchName();
			return name;
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}		
		return name;
	}
	
	private OrganizationDto getOrgDto(long orgId){
		for(OrganizationDto userOrg : userOganizationsDtos){
			if(userOrg.getOrganizationId()==orgId){
				return userOrg;
			}			
		}
		return null;
	}
	
	public void blankStudentPanel() {
		if(userOganizationsDtos!=null){
			if(userOganizationsDtos.size()>1){
				organizationOption.setValue(null);
			}
		}
		
		studentNameField.setValue("");
		fatherNameField.setValue("");
		motherNameField.setValue("");
		genderOption.setValue(null);
		studentIdLabel.setReadOnly(false);
		studentIdLabel.setValue("");
		studentIdLabel.setReadOnly(true);
		Calendar cal2 = Calendar.getInstance();
		cal2.add(Calendar.YEAR, -18);
		dobField.setValue(cal2.getTime());
		permanentAdressArea.setValue("");
		presentAdressArea.setValue("");
		photoPanel.removeAllComponents();
		deleteButton.setEnabled(false);
		batchComboBox.setValue(0);
		
		fatherMobileToken.setValue(null);
		studentContactToken.setValue(null);
		motherMobileToken.setValue(null);
		gardianContactToken.setValue(null);
		emailToken.setValue(null);
		homePhoneToken.setValue(null);
		
		experienceTable.removeAllItems();
		batchTable.removeAllItems();
		accademicRecordTable.removeAllItems();
		batchRecordContainer.removeAllItems();
		academicRecordContainer.removeAllItems();
		experianceRecordContainer.removeAllItems();	
		studentDocumentContainer.removeAllItems();
		studentDocumentTable.removeAllItems();
		uptype = 0;
	}
}
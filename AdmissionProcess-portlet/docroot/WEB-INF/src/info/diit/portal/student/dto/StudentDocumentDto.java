package info.diit.portal.student.dto;

import java.io.File;

public class StudentDocumentDto {
	private long documentId;
	private long originalDocumentId;
	private String type;
	private long documentData;
	private long refferenceNo;
	private long studentId;
	private long fileEntryId;
	private String description;
	private String name;
	private File file;
	private boolean isnew;
    public boolean isIsnew() {
		return isnew;
	}
	public void setIsnew(boolean isnew) {
		this.isnew = isnew;
	}
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private boolean isNew=true;
	
	public boolean isNew() {
		return isNew;
	}
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	public long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(long documentId) {
		this.documentId = documentId;
	}
	public long getOriginalDocumentId() {
		return originalDocumentId;
	}
	public void setOriginalDocumentId(long originalDocumentId) {
		this.originalDocumentId = originalDocumentId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public long getDocumentData() {
		return documentData;
	}
	public void setDocumentData(long documentData) {
		this.documentData = documentData;
	}
	public long getRefferenceNo() {
		return refferenceNo;
	}
	public void setRefferenceNo(long refferenceNo) {
		this.refferenceNo = refferenceNo;
	}
	public long getStudentId() {
		return studentId;
	}
	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	public long getFileEntryId() {
		return fileEntryId;
	}
	public void setFileEntryId(long fileEntryId) {
		this.fileEntryId = fileEntryId;
	}
	
}

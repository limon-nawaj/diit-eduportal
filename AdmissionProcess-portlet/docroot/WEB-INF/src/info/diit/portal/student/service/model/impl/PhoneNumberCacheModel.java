/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.student.service.model.PhoneNumber;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing PhoneNumber in entity cache.
 *
 * @author nasimul
 * @see PhoneNumber
 * @generated
 */
public class PhoneNumberCacheModel implements CacheModel<PhoneNumber>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{phoneNumberId=");
		sb.append(phoneNumberId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", ownerType=");
		sb.append(ownerType);
		sb.append(", phoneNumber=");
		sb.append(phoneNumber);
		sb.append(", studentId=");
		sb.append(studentId);
		sb.append("}");

		return sb.toString();
	}

	public PhoneNumber toEntityModel() {
		PhoneNumberImpl phoneNumberImpl = new PhoneNumberImpl();

		phoneNumberImpl.setPhoneNumberId(phoneNumberId);
		phoneNumberImpl.setCompanyId(companyId);
		phoneNumberImpl.setUserId(userId);

		if (userName == null) {
			phoneNumberImpl.setUserName(StringPool.BLANK);
		}
		else {
			phoneNumberImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			phoneNumberImpl.setCreateDate(null);
		}
		else {
			phoneNumberImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			phoneNumberImpl.setModifiedDate(null);
		}
		else {
			phoneNumberImpl.setModifiedDate(new Date(modifiedDate));
		}

		phoneNumberImpl.setOrganizationId(organizationId);
		phoneNumberImpl.setOwnerType(ownerType);

		if (phoneNumber == null) {
			phoneNumberImpl.setPhoneNumber(StringPool.BLANK);
		}
		else {
			phoneNumberImpl.setPhoneNumber(phoneNumber);
		}

		phoneNumberImpl.setStudentId(studentId);

		phoneNumberImpl.resetOriginalValues();

		return phoneNumberImpl;
	}

	public long phoneNumberId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long organizationId;
	public int ownerType;
	public String phoneNumber;
	public long studentId;
}
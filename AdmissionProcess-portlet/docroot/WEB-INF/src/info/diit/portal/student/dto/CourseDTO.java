package info.diit.portal.student.dto;

import info.diit.portal.coursesubject.model.Course;

import java.io.Serializable;
import java.util.Date;

public class CourseDTO implements Serializable{


//	course field
	private long courseId;
	private String courseCode;
	private String courseName;
	private String description;
	
	public CourseDTO(){
		
	}

	public CourseDTO(Course course) {
	
		this.courseId = course.getCourseId();
		this.courseCode = course.getCourseCode();
		this.courseName =course.getCourseName();
		this.description = course.getDescription();
	}
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public String getCourseCode() {
		return courseCode;
	}
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return courseName ;
	}
	
	
}

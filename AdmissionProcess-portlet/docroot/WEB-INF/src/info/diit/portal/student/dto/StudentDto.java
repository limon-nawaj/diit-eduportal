package info.diit.portal.student.dto;



import info.diit.portal.student.service.model.AccademicRecord;
import info.diit.portal.student.service.model.Experiance;
import info.diit.portal.student.service.model.Student;
import info.diit.portal.student.service.model.StudentDocument;

import java.io.File;
import java.io.Serializable;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class StudentDto implements Serializable {
	private long studentId;
	private String name;
	private String fatherName;
	private String motherName;
	private String presentAddress;
	private String permanentAddress;
	private String homePhone;
	private Collection<String> fatherMobile =new ArrayList<String>() ;
	private Collection<String> motherMobile =new ArrayList<String>() ;
	private Collection<String> guardianMobile =new ArrayList<String>() ;
	private Date   dateOfBirth;
	private Collection<String>  email =new ArrayList<String>() ;
	private String gender;
	private String religion;
	private String nationality;
	private int status;
	private long photo;
	private long organizationId;
	private long companyId;
   private boolean isNew=true;
	
	public boolean isNew() {
		return isNew;
	}


	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	
	List<PhoneNumberDto>  phoneList=new ArrayList<PhoneNumberDto>();
	List<PersonEmailDto>  emailList=new ArrayList<PersonEmailDto>();
	List<StudentDocumentDto> stuentDocuments=new ArrayList<StudentDocumentDto>();
	List<ExperianceDto> studentExpariances=new ArrayList<ExperianceDto>();
	List<AcademicRecordDto> studentAccademicRecords=new ArrayList<AcademicRecordDto>();
	
	
	public StudentDto(Student student ,List<PhoneNumberDto> phoneNumberDtos,
			List<PersonEmailDto> emailList,List<StudentDocumentDto> stuentDocuments,List<ExperianceDto> studentExpariances,List<AcademicRecordDto> studentAccademicRecords) {
		super();
		this.studentId = student.getStudentId();
		this.name = student.getName();
		this.fatherName = student.getFatherName();
		this.motherName = student.getMotherName();
		this.presentAddress = student.getPresentAddress();
		this.permanentAddress = student.getPermanentAddress();
		
		
		this.dateOfBirth = student.getDateOfBirth();
	
		this.gender = student.getGender();
		this.religion = student.getReligion();
		this.nationality = student.getNationality();
		this.status = student.getStatus();
		this.photo = student.getPhoto();
		this.organizationId = student.getOrganizationId();
		this.companyId = student.getCompanyId();
		this.phoneList = phoneList;
		this.emailList = emailList;
		this.stuentDocuments=stuentDocuments;
		this.studentExpariances=studentExpariances;
		this.studentAccademicRecords=studentAccademicRecords;
		
		for(PhoneNumberDto phoneNumberDto:phoneNumberDtos){
			
			
		}
	}




	public List<PhoneNumberDto> getPhoneList() {
		return phoneList;
	}




	public void setPhoneList(List<PhoneNumberDto> phoneList) {
		this.phoneList = phoneList;
	}




	public List<PersonEmailDto> getEmailList() {
		return emailList;
	}




	public void setEmailList(List<PersonEmailDto> emailList) {
		this.emailList = emailList;
	}


	public long getCompanyId() {
		return companyId;
	}




	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getOrganizationId() {
		return organizationId;
	}


	
	
	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}


	


	public StudentDto() {
		
	}
	
	
	public StudentDto(long studentId, String name, String fatherName,
			String motherName, String presentAddress, String permanentAddress,
			String homePhone, Collection<String>  fatherMobile, Collection<String>  motherMobile,
			Collection<String>  guardianMobile, Date dateOfBirth, Collection<String>  email, String gender, String religion,
			String nationality, int status) {
		
		this.studentId = studentId;
		this.name = name;
		this.fatherName = fatherName;
		this.motherName = motherName;
		this.presentAddress = presentAddress;
		this.permanentAddress = permanentAddress;
		this.homePhone = homePhone;
		this.fatherMobile = fatherMobile;
		this.motherMobile = motherMobile;
		this.guardianMobile = guardianMobile;
		
		this.dateOfBirth = dateOfBirth;
		this.email = email;
		this.gender = gender;
		this.religion = religion;
		this.nationality = nationality;
		this.status = status;
	}
	
	
	public long getStudentId() {
		return studentId;
	}
	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getPresentAddress() {
		return presentAddress;
	}
	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}
	public String getPermanentAddress() {
		return permanentAddress;
	}
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public Collection<String>  getFatherMobile() {
		return fatherMobile;
	}
	public void setFatherMobile(Collection<String>  fatherMobile) {
		this.fatherMobile = fatherMobile;
	}
	public Collection<String>  getMotherMobile() {
		return motherMobile;
	}
	public void setMotherMobile(Collection<String>  motherMobile) {
		this.motherMobile = motherMobile;
	}
	public Collection<String>  getGuardianMobile() {
		return guardianMobile;
	}
	public void setGuardianMobile(Collection<String>  guardianMobile) {
		this.guardianMobile = guardianMobile;
	}
	
	
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public Collection<String>  getEmail() {
		return email;
	}
	public void setEmail(Collection<String>  email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}

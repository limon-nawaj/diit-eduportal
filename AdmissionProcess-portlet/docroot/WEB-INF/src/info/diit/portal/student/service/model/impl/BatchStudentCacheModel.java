/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.student.service.model.BatchStudent;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing BatchStudent in entity cache.
 *
 * @author nasimul
 * @see BatchStudent
 * @generated
 */
public class BatchStudentCacheModel implements CacheModel<BatchStudent>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{batchStudentId=");
		sb.append(batchStudentId);
		sb.append(", studentId=");
		sb.append(studentId);
		sb.append(", batchId=");
		sb.append(batchId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", endDate=");
		sb.append(endDate);
		sb.append(", note=");
		sb.append(note);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	public BatchStudent toEntityModel() {
		BatchStudentImpl batchStudentImpl = new BatchStudentImpl();

		batchStudentImpl.setBatchStudentId(batchStudentId);
		batchStudentImpl.setStudentId(studentId);
		batchStudentImpl.setBatchId(batchId);
		batchStudentImpl.setCompanyId(companyId);
		batchStudentImpl.setUserId(userId);

		if (userName == null) {
			batchStudentImpl.setUserName(StringPool.BLANK);
		}
		else {
			batchStudentImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			batchStudentImpl.setCreateDate(null);
		}
		else {
			batchStudentImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			batchStudentImpl.setModifiedDate(null);
		}
		else {
			batchStudentImpl.setModifiedDate(new Date(modifiedDate));
		}

		batchStudentImpl.setOrganizationId(organizationId);

		if (startDate == Long.MIN_VALUE) {
			batchStudentImpl.setStartDate(null);
		}
		else {
			batchStudentImpl.setStartDate(new Date(startDate));
		}

		if (endDate == Long.MIN_VALUE) {
			batchStudentImpl.setEndDate(null);
		}
		else {
			batchStudentImpl.setEndDate(new Date(endDate));
		}

		if (note == null) {
			batchStudentImpl.setNote(StringPool.BLANK);
		}
		else {
			batchStudentImpl.setNote(note);
		}

		batchStudentImpl.setStatus(status);

		batchStudentImpl.resetOriginalValues();

		return batchStudentImpl;
	}

	public long batchStudentId;
	public long studentId;
	public long batchId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long organizationId;
	public long startDate;
	public long endDate;
	public String note;
	public int status;
}
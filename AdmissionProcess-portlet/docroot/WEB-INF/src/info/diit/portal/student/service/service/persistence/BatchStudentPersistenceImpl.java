/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.student.service.NoSuchBatchStudentException;
import info.diit.portal.student.service.model.BatchStudent;
import info.diit.portal.student.service.model.impl.BatchStudentImpl;
import info.diit.portal.student.service.model.impl.BatchStudentModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the batch student service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see BatchStudentPersistence
 * @see BatchStudentUtil
 * @generated
 */
public class BatchStudentPersistenceImpl extends BasePersistenceImpl<BatchStudent>
	implements BatchStudentPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link BatchStudentUtil} to access the batch student persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = BatchStudentImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCH = new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, BatchStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBatch",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH = new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, BatchStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBatch",
			new String[] { Long.class.getName() },
			BatchStudentModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCH = new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBatch",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYORGNIZATION =
		new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, BatchStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompanyOrgnization",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYORGNIZATION =
		new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, BatchStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCompanyOrgnization",
			new String[] { Long.class.getName(), Long.class.getName() },
			BatchStudentModelImpl.COMPANYID_COLUMN_BITMASK |
			BatchStudentModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYORGNIZATION = new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCompanyOrgnization",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTBATCHSTUDENTLIST =
		new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, BatchStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByStudentBatchStudentList",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCHSTUDENTLIST =
		new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, BatchStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByStudentBatchStudentList",
			new String[] { Long.class.getName() },
			BatchStudentModelImpl.STUDENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STUDENTBATCHSTUDENTLIST = new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByStudentBatchStudentList",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ALLSTUDENTSBYBATCHID =
		new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, BatchStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByAllStudentsByBatchId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ALLSTUDENTSBYBATCHID =
		new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, BatchStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByAllStudentsByBatchId",
			new String[] { Long.class.getName(), Long.class.getName() },
			BatchStudentModelImpl.ORGANIZATIONID_COLUMN_BITMASK |
			BatchStudentModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ALLSTUDENTSBYBATCHID = new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByAllStudentsByBatchId",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_FETCH_BY_ORGBATCHSTUDENT = new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, BatchStudentImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByOrgBatchStudent",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			},
			BatchStudentModelImpl.ORGANIZATIONID_COLUMN_BITMASK |
			BatchStudentModelImpl.BATCHID_COLUMN_BITMASK |
			BatchStudentModelImpl.STUDENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGBATCHSTUDENT = new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByOrgBatchStudent",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, BatchStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, BatchStudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the batch student in the entity cache if it is enabled.
	 *
	 * @param batchStudent the batch student
	 */
	public void cacheResult(BatchStudent batchStudent) {
		EntityCacheUtil.putResult(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentImpl.class, batchStudent.getPrimaryKey(), batchStudent);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGBATCHSTUDENT,
			new Object[] {
				Long.valueOf(batchStudent.getOrganizationId()),
				Long.valueOf(batchStudent.getBatchId()),
				Long.valueOf(batchStudent.getStudentId())
			}, batchStudent);

		batchStudent.resetOriginalValues();
	}

	/**
	 * Caches the batch students in the entity cache if it is enabled.
	 *
	 * @param batchStudents the batch students
	 */
	public void cacheResult(List<BatchStudent> batchStudents) {
		for (BatchStudent batchStudent : batchStudents) {
			if (EntityCacheUtil.getResult(
						BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
						BatchStudentImpl.class, batchStudent.getPrimaryKey()) == null) {
				cacheResult(batchStudent);
			}
			else {
				batchStudent.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all batch students.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(BatchStudentImpl.class.getName());
		}

		EntityCacheUtil.clearCache(BatchStudentImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the batch student.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(BatchStudent batchStudent) {
		EntityCacheUtil.removeResult(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentImpl.class, batchStudent.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(batchStudent);
	}

	@Override
	public void clearCache(List<BatchStudent> batchStudents) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (BatchStudent batchStudent : batchStudents) {
			EntityCacheUtil.removeResult(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
				BatchStudentImpl.class, batchStudent.getPrimaryKey());

			clearUniqueFindersCache(batchStudent);
		}
	}

	protected void clearUniqueFindersCache(BatchStudent batchStudent) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ORGBATCHSTUDENT,
			new Object[] {
				Long.valueOf(batchStudent.getOrganizationId()),
				Long.valueOf(batchStudent.getBatchId()),
				Long.valueOf(batchStudent.getStudentId())
			});
	}

	/**
	 * Creates a new batch student with the primary key. Does not add the batch student to the database.
	 *
	 * @param batchStudentId the primary key for the new batch student
	 * @return the new batch student
	 */
	public BatchStudent create(long batchStudentId) {
		BatchStudent batchStudent = new BatchStudentImpl();

		batchStudent.setNew(true);
		batchStudent.setPrimaryKey(batchStudentId);

		return batchStudent;
	}

	/**
	 * Removes the batch student with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param batchStudentId the primary key of the batch student
	 * @return the batch student that was removed
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a batch student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent remove(long batchStudentId)
		throws NoSuchBatchStudentException, SystemException {
		return remove(Long.valueOf(batchStudentId));
	}

	/**
	 * Removes the batch student with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the batch student
	 * @return the batch student that was removed
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a batch student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchStudent remove(Serializable primaryKey)
		throws NoSuchBatchStudentException, SystemException {
		Session session = null;

		try {
			session = openSession();

			BatchStudent batchStudent = (BatchStudent)session.get(BatchStudentImpl.class,
					primaryKey);

			if (batchStudent == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchBatchStudentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(batchStudent);
		}
		catch (NoSuchBatchStudentException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected BatchStudent removeImpl(BatchStudent batchStudent)
		throws SystemException {
		batchStudent = toUnwrappedModel(batchStudent);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, batchStudent);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(batchStudent);

		return batchStudent;
	}

	@Override
	public BatchStudent updateImpl(
		info.diit.portal.student.service.model.BatchStudent batchStudent,
		boolean merge) throws SystemException {
		batchStudent = toUnwrappedModel(batchStudent);

		boolean isNew = batchStudent.isNew();

		BatchStudentModelImpl batchStudentModelImpl = (BatchStudentModelImpl)batchStudent;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, batchStudent, merge);

			batchStudent.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !BatchStudentModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((batchStudentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchStudentModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCH, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH,
					args);

				args = new Object[] {
						Long.valueOf(batchStudentModelImpl.getBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCH, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH,
					args);
			}

			if ((batchStudentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYORGNIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchStudentModelImpl.getOriginalCompanyId()),
						Long.valueOf(batchStudentModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYORGNIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYORGNIZATION,
					args);

				args = new Object[] {
						Long.valueOf(batchStudentModelImpl.getCompanyId()),
						Long.valueOf(batchStudentModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYORGNIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYORGNIZATION,
					args);
			}

			if ((batchStudentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCHSTUDENTLIST.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchStudentModelImpl.getOriginalStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTBATCHSTUDENTLIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCHSTUDENTLIST,
					args);

				args = new Object[] {
						Long.valueOf(batchStudentModelImpl.getStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTBATCHSTUDENTLIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCHSTUDENTLIST,
					args);
			}

			if ((batchStudentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ALLSTUDENTSBYBATCHID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchStudentModelImpl.getOriginalOrganizationId()),
						Long.valueOf(batchStudentModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ALLSTUDENTSBYBATCHID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ALLSTUDENTSBYBATCHID,
					args);

				args = new Object[] {
						Long.valueOf(batchStudentModelImpl.getOrganizationId()),
						Long.valueOf(batchStudentModelImpl.getBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ALLSTUDENTSBYBATCHID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ALLSTUDENTSBYBATCHID,
					args);
			}
		}

		EntityCacheUtil.putResult(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
			BatchStudentImpl.class, batchStudent.getPrimaryKey(), batchStudent);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGBATCHSTUDENT,
				new Object[] {
					Long.valueOf(batchStudent.getOrganizationId()),
					Long.valueOf(batchStudent.getBatchId()),
					Long.valueOf(batchStudent.getStudentId())
				}, batchStudent);
		}
		else {
			if ((batchStudentModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_ORGBATCHSTUDENT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchStudentModelImpl.getOriginalOrganizationId()),
						Long.valueOf(batchStudentModelImpl.getOriginalBatchId()),
						Long.valueOf(batchStudentModelImpl.getOriginalStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGBATCHSTUDENT,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ORGBATCHSTUDENT,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGBATCHSTUDENT,
					new Object[] {
						Long.valueOf(batchStudent.getOrganizationId()),
						Long.valueOf(batchStudent.getBatchId()),
						Long.valueOf(batchStudent.getStudentId())
					}, batchStudent);
			}
		}

		return batchStudent;
	}

	protected BatchStudent toUnwrappedModel(BatchStudent batchStudent) {
		if (batchStudent instanceof BatchStudentImpl) {
			return batchStudent;
		}

		BatchStudentImpl batchStudentImpl = new BatchStudentImpl();

		batchStudentImpl.setNew(batchStudent.isNew());
		batchStudentImpl.setPrimaryKey(batchStudent.getPrimaryKey());

		batchStudentImpl.setBatchStudentId(batchStudent.getBatchStudentId());
		batchStudentImpl.setStudentId(batchStudent.getStudentId());
		batchStudentImpl.setBatchId(batchStudent.getBatchId());
		batchStudentImpl.setCompanyId(batchStudent.getCompanyId());
		batchStudentImpl.setUserId(batchStudent.getUserId());
		batchStudentImpl.setUserName(batchStudent.getUserName());
		batchStudentImpl.setCreateDate(batchStudent.getCreateDate());
		batchStudentImpl.setModifiedDate(batchStudent.getModifiedDate());
		batchStudentImpl.setOrganizationId(batchStudent.getOrganizationId());
		batchStudentImpl.setStartDate(batchStudent.getStartDate());
		batchStudentImpl.setEndDate(batchStudent.getEndDate());
		batchStudentImpl.setNote(batchStudent.getNote());
		batchStudentImpl.setStatus(batchStudent.getStatus());

		return batchStudentImpl;
	}

	/**
	 * Returns the batch student with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the batch student
	 * @return the batch student
	 * @throws com.liferay.portal.NoSuchModelException if a batch student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchStudent findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the batch student with the primary key or throws a {@link info.diit.portal.student.service.NoSuchBatchStudentException} if it could not be found.
	 *
	 * @param batchStudentId the primary key of the batch student
	 * @return the batch student
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a batch student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent findByPrimaryKey(long batchStudentId)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = fetchByPrimaryKey(batchStudentId);

		if (batchStudent == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + batchStudentId);
			}

			throw new NoSuchBatchStudentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				batchStudentId);
		}

		return batchStudent;
	}

	/**
	 * Returns the batch student with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the batch student
	 * @return the batch student, or <code>null</code> if a batch student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchStudent fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the batch student with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param batchStudentId the primary key of the batch student
	 * @return the batch student, or <code>null</code> if a batch student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent fetchByPrimaryKey(long batchStudentId)
		throws SystemException {
		BatchStudent batchStudent = (BatchStudent)EntityCacheUtil.getResult(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
				BatchStudentImpl.class, batchStudentId);

		if (batchStudent == _nullBatchStudent) {
			return null;
		}

		if (batchStudent == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				batchStudent = (BatchStudent)session.get(BatchStudentImpl.class,
						Long.valueOf(batchStudentId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (batchStudent != null) {
					cacheResult(batchStudent);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(BatchStudentModelImpl.ENTITY_CACHE_ENABLED,
						BatchStudentImpl.class, batchStudentId,
						_nullBatchStudent);
				}

				closeSession(session);
			}
		}

		return batchStudent;
	}

	/**
	 * Returns all the batch students where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findByBatch(long batchId)
		throws SystemException {
		return findByBatch(batchId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch students where batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of batch students
	 * @param end the upper bound of the range of batch students (not inclusive)
	 * @return the range of matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findByBatch(long batchId, int start, int end)
		throws SystemException {
		return findByBatch(batchId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch students where batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of batch students
	 * @param end the upper bound of the range of batch students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findByBatch(long batchId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH;
			finderArgs = new Object[] { batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCH;
			finderArgs = new Object[] { batchId, start, end, orderByComparator };
		}

		List<BatchStudent> list = (List<BatchStudent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BatchStudent batchStudent : list) {
				if ((batchId != batchStudent.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_BATCHSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				list = (List<BatchStudent>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch student in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch student
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent findByBatch_First(long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = fetchByBatch_First(batchId,
				orderByComparator);

		if (batchStudent != null) {
			return batchStudent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchStudentException(msg.toString());
	}

	/**
	 * Returns the first batch student in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch student, or <code>null</code> if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent fetchByBatch_First(long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BatchStudent> list = findByBatch(batchId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch student in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch student
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent findByBatch_Last(long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = fetchByBatch_Last(batchId, orderByComparator);

		if (batchStudent != null) {
			return batchStudent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchStudentException(msg.toString());
	}

	/**
	 * Returns the last batch student in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch student, or <code>null</code> if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent fetchByBatch_Last(long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBatch(batchId);

		List<BatchStudent> list = findByBatch(batchId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batch students before and after the current batch student in the ordered set where batchId = &#63;.
	 *
	 * @param batchStudentId the primary key of the current batch student
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch student
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a batch student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent[] findByBatch_PrevAndNext(long batchStudentId,
		long batchId, OrderByComparator orderByComparator)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = findByPrimaryKey(batchStudentId);

		Session session = null;

		try {
			session = openSession();

			BatchStudent[] array = new BatchStudentImpl[3];

			array[0] = getByBatch_PrevAndNext(session, batchStudent, batchId,
					orderByComparator, true);

			array[1] = batchStudent;

			array[2] = getByBatch_PrevAndNext(session, batchStudent, batchId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BatchStudent getByBatch_PrevAndNext(Session session,
		BatchStudent batchStudent, long batchId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCHSTUDENT_WHERE);

		query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batchStudent);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BatchStudent> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the batch students where companyId = &#63; and organizationId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param organizationId the organization ID
	 * @return the matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findByCompanyOrgnization(long companyId,
		long organizationId) throws SystemException {
		return findByCompanyOrgnization(companyId, organizationId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch students where companyId = &#63; and organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of batch students
	 * @param end the upper bound of the range of batch students (not inclusive)
	 * @return the range of matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findByCompanyOrgnization(long companyId,
		long organizationId, int start, int end) throws SystemException {
		return findByCompanyOrgnization(companyId, organizationId, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the batch students where companyId = &#63; and organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of batch students
	 * @param end the upper bound of the range of batch students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findByCompanyOrgnization(long companyId,
		long organizationId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYORGNIZATION;
			finderArgs = new Object[] { companyId, organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYORGNIZATION;
			finderArgs = new Object[] {
					companyId, organizationId,
					
					start, end, orderByComparator
				};
		}

		List<BatchStudent> list = (List<BatchStudent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BatchStudent batchStudent : list) {
				if ((companyId != batchStudent.getCompanyId()) ||
						(organizationId != batchStudent.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BATCHSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_COMPANYORGNIZATION_COMPANYID_2);

			query.append(_FINDER_COLUMN_COMPANYORGNIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(organizationId);

				list = (List<BatchStudent>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch student
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent findByCompanyOrgnization_First(long companyId,
		long organizationId, OrderByComparator orderByComparator)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = fetchByCompanyOrgnization_First(companyId,
				organizationId, orderByComparator);

		if (batchStudent != null) {
			return batchStudent;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchStudentException(msg.toString());
	}

	/**
	 * Returns the first batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch student, or <code>null</code> if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent fetchByCompanyOrgnization_First(long companyId,
		long organizationId, OrderByComparator orderByComparator)
		throws SystemException {
		List<BatchStudent> list = findByCompanyOrgnization(companyId,
				organizationId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch student
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent findByCompanyOrgnization_Last(long companyId,
		long organizationId, OrderByComparator orderByComparator)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = fetchByCompanyOrgnization_Last(companyId,
				organizationId, orderByComparator);

		if (batchStudent != null) {
			return batchStudent;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchStudentException(msg.toString());
	}

	/**
	 * Returns the last batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch student, or <code>null</code> if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent fetchByCompanyOrgnization_Last(long companyId,
		long organizationId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByCompanyOrgnization(companyId, organizationId);

		List<BatchStudent> list = findByCompanyOrgnization(companyId,
				organizationId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batch students before and after the current batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	 *
	 * @param batchStudentId the primary key of the current batch student
	 * @param companyId the company ID
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch student
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a batch student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent[] findByCompanyOrgnization_PrevAndNext(
		long batchStudentId, long companyId, long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = findByPrimaryKey(batchStudentId);

		Session session = null;

		try {
			session = openSession();

			BatchStudent[] array = new BatchStudentImpl[3];

			array[0] = getByCompanyOrgnization_PrevAndNext(session,
					batchStudent, companyId, organizationId, orderByComparator,
					true);

			array[1] = batchStudent;

			array[2] = getByCompanyOrgnization_PrevAndNext(session,
					batchStudent, companyId, organizationId, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BatchStudent getByCompanyOrgnization_PrevAndNext(
		Session session, BatchStudent batchStudent, long companyId,
		long organizationId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCHSTUDENT_WHERE);

		query.append(_FINDER_COLUMN_COMPANYORGNIZATION_COMPANYID_2);

		query.append(_FINDER_COLUMN_COMPANYORGNIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batchStudent);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BatchStudent> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the batch students where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findByStudentBatchStudentList(long studentId)
		throws SystemException {
		return findByStudentBatchStudentList(studentId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch students where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of batch students
	 * @param end the upper bound of the range of batch students (not inclusive)
	 * @return the range of matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findByStudentBatchStudentList(long studentId,
		int start, int end) throws SystemException {
		return findByStudentBatchStudentList(studentId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch students where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of batch students
	 * @param end the upper bound of the range of batch students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findByStudentBatchStudentList(long studentId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCHSTUDENTLIST;
			finderArgs = new Object[] { studentId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTBATCHSTUDENTLIST;
			finderArgs = new Object[] { studentId, start, end, orderByComparator };
		}

		List<BatchStudent> list = (List<BatchStudent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BatchStudent batchStudent : list) {
				if ((studentId != batchStudent.getStudentId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_BATCHSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_STUDENTBATCHSTUDENTLIST_STUDENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				list = (List<BatchStudent>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch student in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch student
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent findByStudentBatchStudentList_First(long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = fetchByStudentBatchStudentList_First(studentId,
				orderByComparator);

		if (batchStudent != null) {
			return batchStudent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchStudentException(msg.toString());
	}

	/**
	 * Returns the first batch student in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch student, or <code>null</code> if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent fetchByStudentBatchStudentList_First(long studentId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BatchStudent> list = findByStudentBatchStudentList(studentId, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch student in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch student
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent findByStudentBatchStudentList_Last(long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = fetchByStudentBatchStudentList_Last(studentId,
				orderByComparator);

		if (batchStudent != null) {
			return batchStudent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchStudentException(msg.toString());
	}

	/**
	 * Returns the last batch student in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch student, or <code>null</code> if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent fetchByStudentBatchStudentList_Last(long studentId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByStudentBatchStudentList(studentId);

		List<BatchStudent> list = findByStudentBatchStudentList(studentId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batch students before and after the current batch student in the ordered set where studentId = &#63;.
	 *
	 * @param batchStudentId the primary key of the current batch student
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch student
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a batch student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent[] findByStudentBatchStudentList_PrevAndNext(
		long batchStudentId, long studentId, OrderByComparator orderByComparator)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = findByPrimaryKey(batchStudentId);

		Session session = null;

		try {
			session = openSession();

			BatchStudent[] array = new BatchStudentImpl[3];

			array[0] = getByStudentBatchStudentList_PrevAndNext(session,
					batchStudent, studentId, orderByComparator, true);

			array[1] = batchStudent;

			array[2] = getByStudentBatchStudentList_PrevAndNext(session,
					batchStudent, studentId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BatchStudent getByStudentBatchStudentList_PrevAndNext(
		Session session, BatchStudent batchStudent, long studentId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCHSTUDENT_WHERE);

		query.append(_FINDER_COLUMN_STUDENTBATCHSTUDENTLIST_STUDENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(studentId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batchStudent);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BatchStudent> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the batch students where organizationId = &#63; and batchId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @return the matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findByAllStudentsByBatchId(long organizationId,
		long batchId) throws SystemException {
		return findByAllStudentsByBatchId(organizationId, batchId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch students where organizationId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of batch students
	 * @param end the upper bound of the range of batch students (not inclusive)
	 * @return the range of matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findByAllStudentsByBatchId(long organizationId,
		long batchId, int start, int end) throws SystemException {
		return findByAllStudentsByBatchId(organizationId, batchId, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the batch students where organizationId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of batch students
	 * @param end the upper bound of the range of batch students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findByAllStudentsByBatchId(long organizationId,
		long batchId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ALLSTUDENTSBYBATCHID;
			finderArgs = new Object[] { organizationId, batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ALLSTUDENTSBYBATCHID;
			finderArgs = new Object[] {
					organizationId, batchId,
					
					start, end, orderByComparator
				};
		}

		List<BatchStudent> list = (List<BatchStudent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BatchStudent batchStudent : list) {
				if ((organizationId != batchStudent.getOrganizationId()) ||
						(batchId != batchStudent.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BATCHSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_ALLSTUDENTSBYBATCHID_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_ALLSTUDENTSBYBATCHID_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(batchId);

				list = (List<BatchStudent>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch student
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent findByAllStudentsByBatchId_First(long organizationId,
		long batchId, OrderByComparator orderByComparator)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = fetchByAllStudentsByBatchId_First(organizationId,
				batchId, orderByComparator);

		if (batchStudent != null) {
			return batchStudent;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchStudentException(msg.toString());
	}

	/**
	 * Returns the first batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch student, or <code>null</code> if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent fetchByAllStudentsByBatchId_First(long organizationId,
		long batchId, OrderByComparator orderByComparator)
		throws SystemException {
		List<BatchStudent> list = findByAllStudentsByBatchId(organizationId,
				batchId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch student
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent findByAllStudentsByBatchId_Last(long organizationId,
		long batchId, OrderByComparator orderByComparator)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = fetchByAllStudentsByBatchId_Last(organizationId,
				batchId, orderByComparator);

		if (batchStudent != null) {
			return batchStudent;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchStudentException(msg.toString());
	}

	/**
	 * Returns the last batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch student, or <code>null</code> if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent fetchByAllStudentsByBatchId_Last(long organizationId,
		long batchId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByAllStudentsByBatchId(organizationId, batchId);

		List<BatchStudent> list = findByAllStudentsByBatchId(organizationId,
				batchId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batch students before and after the current batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	 *
	 * @param batchStudentId the primary key of the current batch student
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch student
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a batch student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent[] findByAllStudentsByBatchId_PrevAndNext(
		long batchStudentId, long organizationId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = findByPrimaryKey(batchStudentId);

		Session session = null;

		try {
			session = openSession();

			BatchStudent[] array = new BatchStudentImpl[3];

			array[0] = getByAllStudentsByBatchId_PrevAndNext(session,
					batchStudent, organizationId, batchId, orderByComparator,
					true);

			array[1] = batchStudent;

			array[2] = getByAllStudentsByBatchId_PrevAndNext(session,
					batchStudent, organizationId, batchId, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BatchStudent getByAllStudentsByBatchId_PrevAndNext(
		Session session, BatchStudent batchStudent, long organizationId,
		long batchId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCHSTUDENT_WHERE);

		query.append(_FINDER_COLUMN_ALLSTUDENTSBYBATCHID_ORGANIZATIONID_2);

		query.append(_FINDER_COLUMN_ALLSTUDENTSBYBATCHID_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batchStudent);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BatchStudent> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns the batch student where organizationId = &#63; and batchId = &#63; and studentId = &#63; or throws a {@link info.diit.portal.student.service.NoSuchBatchStudentException} if it could not be found.
	 *
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param studentId the student ID
	 * @return the matching batch student
	 * @throws info.diit.portal.student.service.NoSuchBatchStudentException if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent findByOrgBatchStudent(long organizationId,
		long batchId, long studentId)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = fetchByOrgBatchStudent(organizationId,
				batchId, studentId);

		if (batchStudent == null) {
			StringBundler msg = new StringBundler(8);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("organizationId=");
			msg.append(organizationId);

			msg.append(", batchId=");
			msg.append(batchId);

			msg.append(", studentId=");
			msg.append(studentId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchBatchStudentException(msg.toString());
		}

		return batchStudent;
	}

	/**
	 * Returns the batch student where organizationId = &#63; and batchId = &#63; and studentId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param studentId the student ID
	 * @return the matching batch student, or <code>null</code> if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent fetchByOrgBatchStudent(long organizationId,
		long batchId, long studentId) throws SystemException {
		return fetchByOrgBatchStudent(organizationId, batchId, studentId, true);
	}

	/**
	 * Returns the batch student where organizationId = &#63; and batchId = &#63; and studentId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param studentId the student ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching batch student, or <code>null</code> if a matching batch student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent fetchByOrgBatchStudent(long organizationId,
		long batchId, long studentId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId, batchId, studentId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_ORGBATCHSTUDENT,
					finderArgs, this);
		}

		if (result instanceof BatchStudent) {
			BatchStudent batchStudent = (BatchStudent)result;

			if ((organizationId != batchStudent.getOrganizationId()) ||
					(batchId != batchStudent.getBatchId()) ||
					(studentId != batchStudent.getStudentId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_BATCHSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_ORGBATCHSTUDENT_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_ORGBATCHSTUDENT_BATCHID_2);

			query.append(_FINDER_COLUMN_ORGBATCHSTUDENT_STUDENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(batchId);

				qPos.add(studentId);

				List<BatchStudent> list = q.list();

				result = list;

				BatchStudent batchStudent = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGBATCHSTUDENT,
						finderArgs, list);
				}
				else {
					batchStudent = list.get(0);

					cacheResult(batchStudent);

					if ((batchStudent.getOrganizationId() != organizationId) ||
							(batchStudent.getBatchId() != batchId) ||
							(batchStudent.getStudentId() != studentId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGBATCHSTUDENT,
							finderArgs, batchStudent);
					}
				}

				return batchStudent;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ORGBATCHSTUDENT,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (BatchStudent)result;
			}
		}
	}

	/**
	 * Returns all the batch students.
	 *
	 * @return the batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of batch students
	 * @param end the upper bound of the range of batch students (not inclusive)
	 * @return the range of batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of batch students
	 * @param end the upper bound of the range of batch students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of batch students
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchStudent> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<BatchStudent> list = (List<BatchStudent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_BATCHSTUDENT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_BATCHSTUDENT;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<BatchStudent>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<BatchStudent>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the batch students where batchId = &#63; from the database.
	 *
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBatch(long batchId) throws SystemException {
		for (BatchStudent batchStudent : findByBatch(batchId)) {
			remove(batchStudent);
		}
	}

	/**
	 * Removes all the batch students where companyId = &#63; and organizationId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCompanyOrgnization(long companyId, long organizationId)
		throws SystemException {
		for (BatchStudent batchStudent : findByCompanyOrgnization(companyId,
				organizationId)) {
			remove(batchStudent);
		}
	}

	/**
	 * Removes all the batch students where studentId = &#63; from the database.
	 *
	 * @param studentId the student ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByStudentBatchStudentList(long studentId)
		throws SystemException {
		for (BatchStudent batchStudent : findByStudentBatchStudentList(
				studentId)) {
			remove(batchStudent);
		}
	}

	/**
	 * Removes all the batch students where organizationId = &#63; and batchId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByAllStudentsByBatchId(long organizationId, long batchId)
		throws SystemException {
		for (BatchStudent batchStudent : findByAllStudentsByBatchId(
				organizationId, batchId)) {
			remove(batchStudent);
		}
	}

	/**
	 * Removes the batch student where organizationId = &#63; and batchId = &#63; and studentId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param studentId the student ID
	 * @return the batch student that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public BatchStudent removeByOrgBatchStudent(long organizationId,
		long batchId, long studentId)
		throws NoSuchBatchStudentException, SystemException {
		BatchStudent batchStudent = findByOrgBatchStudent(organizationId,
				batchId, studentId);

		return remove(batchStudent);
	}

	/**
	 * Removes all the batch students from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (BatchStudent batchStudent : findAll()) {
			remove(batchStudent);
		}
	}

	/**
	 * Returns the number of batch students where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the number of matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBatch(long batchId) throws SystemException {
		Object[] finderArgs = new Object[] { batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCH,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BATCHSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCH,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch students where companyId = &#63; and organizationId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param organizationId the organization ID
	 * @return the number of matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCompanyOrgnization(long companyId, long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { companyId, organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANYORGNIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BATCHSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_COMPANYORGNIZATION_COMPANYID_2);

			query.append(_FINDER_COLUMN_COMPANYORGNIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANYORGNIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch students where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the number of matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public int countByStudentBatchStudentList(long studentId)
		throws SystemException {
		Object[] finderArgs = new Object[] { studentId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_STUDENTBATCHSTUDENTLIST,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BATCHSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_STUDENTBATCHSTUDENTLIST_STUDENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_STUDENTBATCHSTUDENTLIST,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch students where organizationId = &#63; and batchId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @return the number of matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public int countByAllStudentsByBatchId(long organizationId, long batchId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId, batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ALLSTUDENTSBYBATCHID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BATCHSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_ALLSTUDENTSBYBATCHID_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_ALLSTUDENTSBYBATCHID_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ALLSTUDENTSBYBATCHID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch students where organizationId = &#63; and batchId = &#63; and studentId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param batchId the batch ID
	 * @param studentId the student ID
	 * @return the number of matching batch students
	 * @throws SystemException if a system exception occurred
	 */
	public int countByOrgBatchStudent(long organizationId, long batchId,
		long studentId) throws SystemException {
		Object[] finderArgs = new Object[] { organizationId, batchId, studentId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGBATCHSTUDENT,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_BATCHSTUDENT_WHERE);

			query.append(_FINDER_COLUMN_ORGBATCHSTUDENT_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_ORGBATCHSTUDENT_BATCHID_2);

			query.append(_FINDER_COLUMN_ORGBATCHSTUDENT_STUDENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(batchId);

				qPos.add(studentId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGBATCHSTUDENT,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch students.
	 *
	 * @return the number of batch students
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_BATCHSTUDENT);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the batch student persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.student.service.model.BatchStudent")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<BatchStudent>> listenersList = new ArrayList<ModelListener<BatchStudent>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<BatchStudent>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(BatchStudentImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AccademicRecordPersistence.class)
	protected AccademicRecordPersistence accademicRecordPersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_BATCHSTUDENT = "SELECT batchStudent FROM BatchStudent batchStudent";
	private static final String _SQL_SELECT_BATCHSTUDENT_WHERE = "SELECT batchStudent FROM BatchStudent batchStudent WHERE ";
	private static final String _SQL_COUNT_BATCHSTUDENT = "SELECT COUNT(batchStudent) FROM BatchStudent batchStudent";
	private static final String _SQL_COUNT_BATCHSTUDENT_WHERE = "SELECT COUNT(batchStudent) FROM BatchStudent batchStudent WHERE ";
	private static final String _FINDER_COLUMN_BATCH_BATCHID_2 = "batchStudent.batchId = ?";
	private static final String _FINDER_COLUMN_COMPANYORGNIZATION_COMPANYID_2 = "batchStudent.companyId = ? AND ";
	private static final String _FINDER_COLUMN_COMPANYORGNIZATION_ORGANIZATIONID_2 =
		"batchStudent.organizationId = ?";
	private static final String _FINDER_COLUMN_STUDENTBATCHSTUDENTLIST_STUDENTID_2 =
		"batchStudent.studentId = ?";
	private static final String _FINDER_COLUMN_ALLSTUDENTSBYBATCHID_ORGANIZATIONID_2 =
		"batchStudent.organizationId = ? AND ";
	private static final String _FINDER_COLUMN_ALLSTUDENTSBYBATCHID_BATCHID_2 = "batchStudent.batchId = ?";
	private static final String _FINDER_COLUMN_ORGBATCHSTUDENT_ORGANIZATIONID_2 = "batchStudent.organizationId = ? AND ";
	private static final String _FINDER_COLUMN_ORGBATCHSTUDENT_BATCHID_2 = "batchStudent.batchId = ? AND ";
	private static final String _FINDER_COLUMN_ORGBATCHSTUDENT_STUDENTID_2 = "batchStudent.studentId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "batchStudent.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BatchStudent exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No BatchStudent exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(BatchStudentPersistenceImpl.class);
	private static BatchStudent _nullBatchStudent = new BatchStudentImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<BatchStudent> toCacheModel() {
				return _nullBatchStudentCacheModel;
			}
		};

	private static CacheModel<BatchStudent> _nullBatchStudentCacheModel = new CacheModel<BatchStudent>() {
			public BatchStudent toEntityModel() {
				return _nullBatchStudent;
			}
		};
}
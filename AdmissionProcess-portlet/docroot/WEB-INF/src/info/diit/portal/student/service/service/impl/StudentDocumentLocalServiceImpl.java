/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.service.impl;

import info.diit.portal.student.service.model.Student;
import info.diit.portal.student.service.model.StudentDocument;
import info.diit.portal.student.service.service.StudentDocumentLocalServiceUtil;
import info.diit.portal.student.service.service.base.StudentDocumentLocalServiceBaseImpl;
import info.diit.portal.student.service.service.persistence.StudentDocumentUtil;

import java.awt.image.SampleModel;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;

/**
 * The implementation of the student document local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.student.service.service.StudentDocumentLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author nasimul
 * @see info.diit.portal.student.service.service.base.StudentDocumentLocalServiceBaseImpl
 * @see info.diit.portal.student.service.service.StudentDocumentLocalServiceUtil
 */
public class StudentDocumentLocalServiceImpl
	extends StudentDocumentLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.student.service.service.StudentDocumentLocalServiceUtil} to access the student document local service.
	 */
	
	public List<StudentDocument> findByStduentDocumentList(long studentId) throws SystemException{
		return StudentDocumentUtil.findByStudentDocumentList(studentId);
	}
	
	public void saveStudentDocmument(Student student, File file, StudentDocument studentDocument) throws PortalException, SystemException, FileNotFoundException{
		final String STUDENT_GROUP_NAME = "Student";
		String fileName=file.getName();
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
		
		UserGroup userGroup;
			
			userGroup = UserGroupLocalServiceUtil.getUserGroup(student.getCompanyId(), STUDENT_GROUP_NAME);
			DLFileEntry fileENtry= DLFileEntryLocalServiceUtil.addFileEntry(student.getUserId(),
					 userGroup.getGroup().getGroupId(),0, 0, fileName, 
					 FileUtil.getExtension(file.getName()), "Id-"+student.getStudentId()+"_"+dateformat.format(new Date()), "Document of "+student.getName(),
					"Student Document", 0, null, file, new FileInputStream(file), 
					file.length(), new    ServiceContext());
			System.out.println("dl file entry save executed");
			
			studentDocument.setFileEntryId(fileENtry.getFileEntryId());
			StudentDocumentLocalServiceUtil.addStudentDocument(studentDocument);	
			System.out.println("student document save executed");
		
	}
	
	public void deleteDocument(long documentId, long fileEntryId) throws PortalException, SystemException{
		StudentDocumentLocalServiceUtil.deleteStudentDocument(documentId);
		DLFileEntryLocalServiceUtil.deleteDLFileEntry(fileEntryId);
	}
}
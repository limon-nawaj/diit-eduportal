package info.diit.portal.student.constant;

import info.diit.portal.student.service.service.StudentLocalServiceUtil;
import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

public class StudentPhotoScriptlet extends JRDefaultScriptlet{
	
	@Override
	public void afterDetailEval() throws JRScriptletException {
		// TODO Auto-generated method stub
		super.afterDetailEval();
		
		Object studentId = getFieldValue("studentId");
		
		if(studentId != null)
		{
			String imageLocation = StudentLocalServiceUtil.getStudentPhotoLocation(Long.parseLong(studentId.toString()));
			
			setVariableValue("imageLocation", imageLocation);
		}
		
	}

}

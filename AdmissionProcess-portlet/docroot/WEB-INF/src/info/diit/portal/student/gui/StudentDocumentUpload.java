package info.diit.portal.student.gui;

import info.diit.portal.student.service.model.Student;

import info.diit.portal.student.service.model.StudentDocument;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.vaadin.Application;
import com.vaadin.terminal.FileResource;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.StartedListener;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.Window;

public class StudentDocumentUpload extends  Panel implements Receiver, SucceededListener, FailedListener, StartedListener{
	private static final String STUDENT_GROUP_NAME = "Student";
	
	  Panel root;         // Root element for contained components.
	  Panel imagePanel =new Panel("Student Document ");   // Panel that contains the uploaded image.
	  File  file;         // File to write to.
	  Upload uploadDocument=new Upload("Upload here", this);
	  Application app ;
	  Student student;
	  StudentDocument document;
	/*public StudentDocumentUpload(Application app, Student student,StudentDocument document  ) {
		
		root = new Panel("Student Documet Upload......");
		root.setWidth("370px");
		uploadDocument.setButtonCaption("Upload Now");
		uploadDocument.addListener((Upload.SucceededListener) this);
		uploadDocument.addListener((Upload.FailedListener) this);
		uploadDocument.addListener((Upload.StartedListener) this);
		root.addComponent(uploadDocument);
		this.student=student;
		this.document=document;
		addComponent(root);
		this.app=app;
	}*/

	//@Override
	public void uploadStarted(StartedEvent event) {
		// TODO Auto-generated method stub
		
	}

	//@Override
	public void uploadFailed(FailedEvent event) {
		
	}

	//@Override
	public void uploadSucceeded(SucceededEvent event) {
		/*// TODO Auto-generated method stub
		root.addComponent(new Label("File " + event.getFilename()
				+ " of type '" + event.getMIMEType() + "' uploaded."));

		// Display the uploaded file in the image panel.
		final FileResource imageResource = new FileResource(file, app);
		imagePanel.removeAllComponents();
		Embedded image = new Embedded("", imageResource);
		image.setWidth("100%");
		image.setHeight("325px");
		imagePanel.addComponent(image);*/
		
		//save to DLFileEntry
		String fileName=file.getName();
		UserGroup userGroup;
		try {
			userGroup = UserGroupLocalServiceUtil.getUserGroup(student.getCompanyId(), STUDENT_GROUP_NAME);
			DLFileEntry fileENtry= DLFileEntryLocalServiceUtil.addFileEntry(student.getUserId(),
					 userGroup.getGroup().getGroupId(),0, 0, fileName, 
					 FileUtil.getExtension(file.getName()), "Student Document of Id "+student.getStudentId(), "Document of "+student.getName(),
					"Student Document", 0, null, file, new FileInputStream(file), 
					file.length(), new    ServiceContext());
			document.setFileEntryId(fileENtry.getFileEntryId());
			
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	//@Override
	public OutputStream receiveUpload(String filename, String mimeType) {

		FileOutputStream fos = null; // Output stream to write to

		File tmpdir = new File("/tmp/uploads/");
		if (!tmpdir.exists()) {
			tmpdir.mkdirs();
			// System.out.println("Dir created ..");
		}
		// System.out.println("Exists created ..");
		file = new File(tmpdir, filename);
		try {
			// Open the file for writing.
			fos = new FileOutputStream(file);
		} catch (final java.io.FileNotFoundException e) {
			// Error while opening the file. Not reported here.
			e.printStackTrace();
			return null;
		}

		return fos; // Return the output stream to write to

	}

	

}


<%
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>


<portlet:defineObjects />
<portlet:actionURL var="registrtationSubmit" name="registrationSubmit"></portlet:actionURL>
<div>
<form name="registration" action="<%=registrtationSubmit%>" method="post">
    <div class="control-group">
        <label class="control-label" for="FirstName">First Name</label>
        <div class="controls">
            <input name="FirstName" required="required" type="text" id="FirstName" placeholder="First Name">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="mname">Middle Name</label>
        <div class="controls">
            <input name="mname" type="text" id="mname" placeholder="Middle Name">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="lname">Last(Family) Name</label>
        <div class="controls">
            <input name="lname" type="text" id="lname" placeholder="Last Name">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="Password">Password</label>
        <div class="controls">
            <input name="Password" required="required" type="password" id="Password" placeholder="Last Name">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="ScreenName">Screen Name</label>
        <div class="controls">
            <input name="ScreenName" required="required" type="text" id="ScreenName" placeholder="Screen Name">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="EmailAddress">Email Address</label>
        <div class="controls">
            <input name="EmailAddress" required="required" type="text" id="EmailAddress" placeholder="Email Address">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="Gender">Gender</label>
        <div class="controls">
            <select name="Gender">
            	<option value="Male">Male</option>
                <option value="Female">Female</option>
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="JobTitle">JobTitle</label>
        <div class="controls">
            <input name="JobTitle" type="text" id="inputLast" placeholder="Email Address">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="Birthday">Birthday</label>
        <div class="controls">
        <liferay-ui:input-date dayParam="dd" monthParam="mm" yearParam="yyyy" yearRangeStart="1970" yearRangeEnd="2013"></liferay-ui:input-date>
        </div>
    </div>
    <div class="control-group">
		<div class="controls">
			<input type="submit" name="submit" class="btn btn-success" value="Submit">
		</div>
	</div>
	</form>
</div>

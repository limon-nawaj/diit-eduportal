/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.diit.training.portlets.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import org.diit.training.portlets.model.Registration;

/**
 * The persistence interface for the registration service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Shamsuddin
 * @see RegistrationPersistenceImpl
 * @see RegistrationUtil
 * @generated
 */
public interface RegistrationPersistence extends BasePersistence<Registration> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link RegistrationUtil} to access the registration persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the registration in the entity cache if it is enabled.
	*
	* @param registration the registration
	*/
	public void cacheResult(
		org.diit.training.portlets.model.Registration registration);

	/**
	* Caches the registrations in the entity cache if it is enabled.
	*
	* @param registrations the registrations
	*/
	public void cacheResult(
		java.util.List<org.diit.training.portlets.model.Registration> registrations);

	/**
	* Creates a new registration with the primary key. Does not add the registration to the database.
	*
	* @param registrationUserId the primary key for the new registration
	* @return the new registration
	*/
	public org.diit.training.portlets.model.Registration create(
		long registrationUserId);

	/**
	* Removes the registration with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param registrationUserId the primary key of the registration
	* @return the registration that was removed
	* @throws org.diit.training.portlets.NoSuchRegistrationException if a registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.diit.training.portlets.model.Registration remove(
		long registrationUserId)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.diit.training.portlets.NoSuchRegistrationException;

	public org.diit.training.portlets.model.Registration updateImpl(
		org.diit.training.portlets.model.Registration registration,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the registration with the primary key or throws a {@link org.diit.training.portlets.NoSuchRegistrationException} if it could not be found.
	*
	* @param registrationUserId the primary key of the registration
	* @return the registration
	* @throws org.diit.training.portlets.NoSuchRegistrationException if a registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.diit.training.portlets.model.Registration findByPrimaryKey(
		long registrationUserId)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.diit.training.portlets.NoSuchRegistrationException;

	/**
	* Returns the registration with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param registrationUserId the primary key of the registration
	* @return the registration, or <code>null</code> if a registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.diit.training.portlets.model.Registration fetchByPrimaryKey(
		long registrationUserId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the registrations.
	*
	* @return the registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.diit.training.portlets.model.Registration> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the registrations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of registrations
	* @param end the upper bound of the range of registrations (not inclusive)
	* @return the range of registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.diit.training.portlets.model.Registration> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the registrations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of registrations
	* @param end the upper bound of the range of registrations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.diit.training.portlets.model.Registration> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the registrations from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of registrations.
	*
	* @return the number of registrations
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
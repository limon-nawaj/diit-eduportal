/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.diit.training.portlets.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Registration}.
 * </p>
 *
 * @author    Shamsuddin
 * @see       Registration
 * @generated
 */
public class RegistrationWrapper implements Registration,
	ModelWrapper<Registration> {
	public RegistrationWrapper(Registration registration) {
		_registration = registration;
	}

	public Class<?> getModelClass() {
		return Registration.class;
	}

	public String getModelClassName() {
		return Registration.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("registrationUserId", getRegistrationUserId());
		attributes.put("screenName", getScreenName());
		attributes.put("emailAddress", getEmailAddress());
		attributes.put("firstName", getFirstName());
		attributes.put("password", getPassword());
		attributes.put("middleName", getMiddleName());
		attributes.put("lastName", getLastName());
		attributes.put("birthDate", getBirthDate());
		attributes.put("gender", getGender());
		attributes.put("jobTitle", getJobTitle());
		attributes.put("status", getStatus());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long registrationUserId = (Long)attributes.get("registrationUserId");

		if (registrationUserId != null) {
			setRegistrationUserId(registrationUserId);
		}

		String screenName = (String)attributes.get("screenName");

		if (screenName != null) {
			setScreenName(screenName);
		}

		String emailAddress = (String)attributes.get("emailAddress");

		if (emailAddress != null) {
			setEmailAddress(emailAddress);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String password = (String)attributes.get("password");

		if (password != null) {
			setPassword(password);
		}

		String middleName = (String)attributes.get("middleName");

		if (middleName != null) {
			setMiddleName(middleName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		Date birthDate = (Date)attributes.get("birthDate");

		if (birthDate != null) {
			setBirthDate(birthDate);
		}

		String gender = (String)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		String jobTitle = (String)attributes.get("jobTitle");

		if (jobTitle != null) {
			setJobTitle(jobTitle);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	/**
	* Returns the primary key of this registration.
	*
	* @return the primary key of this registration
	*/
	public long getPrimaryKey() {
		return _registration.getPrimaryKey();
	}

	/**
	* Sets the primary key of this registration.
	*
	* @param primaryKey the primary key of this registration
	*/
	public void setPrimaryKey(long primaryKey) {
		_registration.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the registration user ID of this registration.
	*
	* @return the registration user ID of this registration
	*/
	public long getRegistrationUserId() {
		return _registration.getRegistrationUserId();
	}

	/**
	* Sets the registration user ID of this registration.
	*
	* @param registrationUserId the registration user ID of this registration
	*/
	public void setRegistrationUserId(long registrationUserId) {
		_registration.setRegistrationUserId(registrationUserId);
	}

	/**
	* Returns the registration user uuid of this registration.
	*
	* @return the registration user uuid of this registration
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getRegistrationUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _registration.getRegistrationUserUuid();
	}

	/**
	* Sets the registration user uuid of this registration.
	*
	* @param registrationUserUuid the registration user uuid of this registration
	*/
	public void setRegistrationUserUuid(java.lang.String registrationUserUuid) {
		_registration.setRegistrationUserUuid(registrationUserUuid);
	}

	/**
	* Returns the screen name of this registration.
	*
	* @return the screen name of this registration
	*/
	public java.lang.String getScreenName() {
		return _registration.getScreenName();
	}

	/**
	* Sets the screen name of this registration.
	*
	* @param screenName the screen name of this registration
	*/
	public void setScreenName(java.lang.String screenName) {
		_registration.setScreenName(screenName);
	}

	/**
	* Returns the email address of this registration.
	*
	* @return the email address of this registration
	*/
	public java.lang.String getEmailAddress() {
		return _registration.getEmailAddress();
	}

	/**
	* Sets the email address of this registration.
	*
	* @param emailAddress the email address of this registration
	*/
	public void setEmailAddress(java.lang.String emailAddress) {
		_registration.setEmailAddress(emailAddress);
	}

	/**
	* Returns the first name of this registration.
	*
	* @return the first name of this registration
	*/
	public java.lang.String getFirstName() {
		return _registration.getFirstName();
	}

	/**
	* Sets the first name of this registration.
	*
	* @param firstName the first name of this registration
	*/
	public void setFirstName(java.lang.String firstName) {
		_registration.setFirstName(firstName);
	}

	/**
	* Returns the password of this registration.
	*
	* @return the password of this registration
	*/
	public java.lang.String getPassword() {
		return _registration.getPassword();
	}

	/**
	* Sets the password of this registration.
	*
	* @param password the password of this registration
	*/
	public void setPassword(java.lang.String password) {
		_registration.setPassword(password);
	}

	/**
	* Returns the middle name of this registration.
	*
	* @return the middle name of this registration
	*/
	public java.lang.String getMiddleName() {
		return _registration.getMiddleName();
	}

	/**
	* Sets the middle name of this registration.
	*
	* @param middleName the middle name of this registration
	*/
	public void setMiddleName(java.lang.String middleName) {
		_registration.setMiddleName(middleName);
	}

	/**
	* Returns the last name of this registration.
	*
	* @return the last name of this registration
	*/
	public java.lang.String getLastName() {
		return _registration.getLastName();
	}

	/**
	* Sets the last name of this registration.
	*
	* @param lastName the last name of this registration
	*/
	public void setLastName(java.lang.String lastName) {
		_registration.setLastName(lastName);
	}

	/**
	* Returns the birth date of this registration.
	*
	* @return the birth date of this registration
	*/
	public java.util.Date getBirthDate() {
		return _registration.getBirthDate();
	}

	/**
	* Sets the birth date of this registration.
	*
	* @param birthDate the birth date of this registration
	*/
	public void setBirthDate(java.util.Date birthDate) {
		_registration.setBirthDate(birthDate);
	}

	/**
	* Returns the gender of this registration.
	*
	* @return the gender of this registration
	*/
	public java.lang.String getGender() {
		return _registration.getGender();
	}

	/**
	* Sets the gender of this registration.
	*
	* @param gender the gender of this registration
	*/
	public void setGender(java.lang.String gender) {
		_registration.setGender(gender);
	}

	/**
	* Returns the job title of this registration.
	*
	* @return the job title of this registration
	*/
	public java.lang.String getJobTitle() {
		return _registration.getJobTitle();
	}

	/**
	* Sets the job title of this registration.
	*
	* @param jobTitle the job title of this registration
	*/
	public void setJobTitle(java.lang.String jobTitle) {
		_registration.setJobTitle(jobTitle);
	}

	/**
	* Returns the status of this registration.
	*
	* @return the status of this registration
	*/
	public java.lang.Integer getStatus() {
		return _registration.getStatus();
	}

	/**
	* Sets the status of this registration.
	*
	* @param status the status of this registration
	*/
	public void setStatus(java.lang.Integer status) {
		_registration.setStatus(status);
	}

	public boolean isNew() {
		return _registration.isNew();
	}

	public void setNew(boolean n) {
		_registration.setNew(n);
	}

	public boolean isCachedModel() {
		return _registration.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_registration.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _registration.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _registration.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_registration.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _registration.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_registration.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new RegistrationWrapper((Registration)_registration.clone());
	}

	public int compareTo(Registration registration) {
		return _registration.compareTo(registration);
	}

	@Override
	public int hashCode() {
		return _registration.hashCode();
	}

	public com.liferay.portal.model.CacheModel<Registration> toCacheModel() {
		return _registration.toCacheModel();
	}

	public Registration toEscapedModel() {
		return new RegistrationWrapper(_registration.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _registration.toString();
	}

	public java.lang.String toXmlString() {
		return _registration.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_registration.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Registration getWrappedRegistration() {
		return _registration;
	}

	public Registration getWrappedModel() {
		return _registration;
	}

	public void resetOriginalValues() {
		_registration.resetOriginalValues();
	}

	private Registration _registration;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.diit.training.portlets.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import org.diit.training.portlets.model.Registration;

import java.util.List;

/**
 * The persistence utility for the registration service. This utility wraps {@link RegistrationPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Shamsuddin
 * @see RegistrationPersistence
 * @see RegistrationPersistenceImpl
 * @generated
 */
public class RegistrationUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Registration registration) {
		getPersistence().clearCache(registration);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Registration> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Registration> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Registration> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Registration update(Registration registration, boolean merge)
		throws SystemException {
		return getPersistence().update(registration, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Registration update(Registration registration, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(registration, merge, serviceContext);
	}

	/**
	* Caches the registration in the entity cache if it is enabled.
	*
	* @param registration the registration
	*/
	public static void cacheResult(
		org.diit.training.portlets.model.Registration registration) {
		getPersistence().cacheResult(registration);
	}

	/**
	* Caches the registrations in the entity cache if it is enabled.
	*
	* @param registrations the registrations
	*/
	public static void cacheResult(
		java.util.List<org.diit.training.portlets.model.Registration> registrations) {
		getPersistence().cacheResult(registrations);
	}

	/**
	* Creates a new registration with the primary key. Does not add the registration to the database.
	*
	* @param registrationUserId the primary key for the new registration
	* @return the new registration
	*/
	public static org.diit.training.portlets.model.Registration create(
		long registrationUserId) {
		return getPersistence().create(registrationUserId);
	}

	/**
	* Removes the registration with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param registrationUserId the primary key of the registration
	* @return the registration that was removed
	* @throws org.diit.training.portlets.NoSuchRegistrationException if a registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.diit.training.portlets.model.Registration remove(
		long registrationUserId)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.diit.training.portlets.NoSuchRegistrationException {
		return getPersistence().remove(registrationUserId);
	}

	public static org.diit.training.portlets.model.Registration updateImpl(
		org.diit.training.portlets.model.Registration registration,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(registration, merge);
	}

	/**
	* Returns the registration with the primary key or throws a {@link org.diit.training.portlets.NoSuchRegistrationException} if it could not be found.
	*
	* @param registrationUserId the primary key of the registration
	* @return the registration
	* @throws org.diit.training.portlets.NoSuchRegistrationException if a registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.diit.training.portlets.model.Registration findByPrimaryKey(
		long registrationUserId)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.diit.training.portlets.NoSuchRegistrationException {
		return getPersistence().findByPrimaryKey(registrationUserId);
	}

	/**
	* Returns the registration with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param registrationUserId the primary key of the registration
	* @return the registration, or <code>null</code> if a registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.diit.training.portlets.model.Registration fetchByPrimaryKey(
		long registrationUserId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(registrationUserId);
	}

	/**
	* Returns all the registrations.
	*
	* @return the registrations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.diit.training.portlets.model.Registration> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the registrations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of registrations
	* @param end the upper bound of the range of registrations (not inclusive)
	* @return the range of registrations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.diit.training.portlets.model.Registration> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the registrations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of registrations
	* @param end the upper bound of the range of registrations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of registrations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.diit.training.portlets.model.Registration> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the registrations from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of registrations.
	*
	* @return the number of registrations
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static RegistrationPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (RegistrationPersistence)PortletBeanLocatorUtil.locate(org.diit.training.portlets.service.ClpSerializer.getServletContextName(),
					RegistrationPersistence.class.getName());

			ReferenceRegistry.registerReference(RegistrationUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(RegistrationPersistence persistence) {
	}

	private static RegistrationPersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.diit.training.portlets.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import org.diit.training.portlets.service.RegistrationLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Shamsuddin
 */
public class RegistrationClp extends BaseModelImpl<Registration>
	implements Registration {
	public RegistrationClp() {
	}

	public Class<?> getModelClass() {
		return Registration.class;
	}

	public String getModelClassName() {
		return Registration.class.getName();
	}

	public long getPrimaryKey() {
		return _registrationUserId;
	}

	public void setPrimaryKey(long primaryKey) {
		setRegistrationUserId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_registrationUserId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("registrationUserId", getRegistrationUserId());
		attributes.put("screenName", getScreenName());
		attributes.put("emailAddress", getEmailAddress());
		attributes.put("firstName", getFirstName());
		attributes.put("password", getPassword());
		attributes.put("middleName", getMiddleName());
		attributes.put("lastName", getLastName());
		attributes.put("birthDate", getBirthDate());
		attributes.put("gender", getGender());
		attributes.put("jobTitle", getJobTitle());
		attributes.put("status", getStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long registrationUserId = (Long)attributes.get("registrationUserId");

		if (registrationUserId != null) {
			setRegistrationUserId(registrationUserId);
		}

		String screenName = (String)attributes.get("screenName");

		if (screenName != null) {
			setScreenName(screenName);
		}

		String emailAddress = (String)attributes.get("emailAddress");

		if (emailAddress != null) {
			setEmailAddress(emailAddress);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String password = (String)attributes.get("password");

		if (password != null) {
			setPassword(password);
		}

		String middleName = (String)attributes.get("middleName");

		if (middleName != null) {
			setMiddleName(middleName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		Date birthDate = (Date)attributes.get("birthDate");

		if (birthDate != null) {
			setBirthDate(birthDate);
		}

		String gender = (String)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		String jobTitle = (String)attributes.get("jobTitle");

		if (jobTitle != null) {
			setJobTitle(jobTitle);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	public long getRegistrationUserId() {
		return _registrationUserId;
	}

	public void setRegistrationUserId(long registrationUserId) {
		_registrationUserId = registrationUserId;
	}

	public String getRegistrationUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getRegistrationUserId(), "uuid",
			_registrationUserUuid);
	}

	public void setRegistrationUserUuid(String registrationUserUuid) {
		_registrationUserUuid = registrationUserUuid;
	}

	public String getScreenName() {
		return _screenName;
	}

	public void setScreenName(String screenName) {
		_screenName = screenName;
	}

	public String getEmailAddress() {
		return _emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		_emailAddress = emailAddress;
	}

	public String getFirstName() {
		return _firstName;
	}

	public void setFirstName(String firstName) {
		_firstName = firstName;
	}

	public String getPassword() {
		return _password;
	}

	public void setPassword(String password) {
		_password = password;
	}

	public String getMiddleName() {
		return _middleName;
	}

	public void setMiddleName(String middleName) {
		_middleName = middleName;
	}

	public String getLastName() {
		return _lastName;
	}

	public void setLastName(String lastName) {
		_lastName = lastName;
	}

	public Date getBirthDate() {
		return _birthDate;
	}

	public void setBirthDate(Date birthDate) {
		_birthDate = birthDate;
	}

	public String getGender() {
		return _gender;
	}

	public void setGender(String gender) {
		_gender = gender;
	}

	public String getJobTitle() {
		return _jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		_jobTitle = jobTitle;
	}

	public Integer getStatus() {
		return _status;
	}

	public void setStatus(Integer status) {
		_status = status;
	}

	public BaseModel<?> getRegistrationRemoteModel() {
		return _registrationRemoteModel;
	}

	public void setRegistrationRemoteModel(BaseModel<?> registrationRemoteModel) {
		_registrationRemoteModel = registrationRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			RegistrationLocalServiceUtil.addRegistration(this);
		}
		else {
			RegistrationLocalServiceUtil.updateRegistration(this);
		}
	}

	@Override
	public Registration toEscapedModel() {
		return (Registration)Proxy.newProxyInstance(Registration.class.getClassLoader(),
			new Class[] { Registration.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		RegistrationClp clone = new RegistrationClp();

		clone.setRegistrationUserId(getRegistrationUserId());
		clone.setScreenName(getScreenName());
		clone.setEmailAddress(getEmailAddress());
		clone.setFirstName(getFirstName());
		clone.setPassword(getPassword());
		clone.setMiddleName(getMiddleName());
		clone.setLastName(getLastName());
		clone.setBirthDate(getBirthDate());
		clone.setGender(getGender());
		clone.setJobTitle(getJobTitle());
		clone.setStatus(getStatus());

		return clone;
	}

	public int compareTo(Registration registration) {
		int value = 0;

		if (getRegistrationUserId() < registration.getRegistrationUserId()) {
			value = -1;
		}
		else if (getRegistrationUserId() > registration.getRegistrationUserId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		RegistrationClp registration = null;

		try {
			registration = (RegistrationClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = registration.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{registrationUserId=");
		sb.append(getRegistrationUserId());
		sb.append(", screenName=");
		sb.append(getScreenName());
		sb.append(", emailAddress=");
		sb.append(getEmailAddress());
		sb.append(", firstName=");
		sb.append(getFirstName());
		sb.append(", password=");
		sb.append(getPassword());
		sb.append(", middleName=");
		sb.append(getMiddleName());
		sb.append(", lastName=");
		sb.append(getLastName());
		sb.append(", birthDate=");
		sb.append(getBirthDate());
		sb.append(", gender=");
		sb.append(getGender());
		sb.append(", jobTitle=");
		sb.append(getJobTitle());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(37);

		sb.append("<model><model-name>");
		sb.append("org.diit.training.portlets.model.Registration");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>registrationUserId</column-name><column-value><![CDATA[");
		sb.append(getRegistrationUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>screenName</column-name><column-value><![CDATA[");
		sb.append(getScreenName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>emailAddress</column-name><column-value><![CDATA[");
		sb.append(getEmailAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>firstName</column-name><column-value><![CDATA[");
		sb.append(getFirstName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>password</column-name><column-value><![CDATA[");
		sb.append(getPassword());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>middleName</column-name><column-value><![CDATA[");
		sb.append(getMiddleName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lastName</column-name><column-value><![CDATA[");
		sb.append(getLastName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>birthDate</column-name><column-value><![CDATA[");
		sb.append(getBirthDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>gender</column-name><column-value><![CDATA[");
		sb.append(getGender());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>jobTitle</column-name><column-value><![CDATA[");
		sb.append(getJobTitle());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _registrationUserId;
	private String _registrationUserUuid;
	private String _screenName;
	private String _emailAddress;
	private String _firstName;
	private String _password;
	private String _middleName;
	private String _lastName;
	private Date _birthDate;
	private String _gender;
	private String _jobTitle;
	private Integer _status;
	private BaseModel<?> _registrationRemoteModel;
}
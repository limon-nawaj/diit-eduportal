create table ShamsService_Registration (
	registrationUserId LONG not null primary key,
	screenName VARCHAR(75) null,
	emailAddress VARCHAR(75) null,
	firstName VARCHAR(75) null,
	password_ VARCHAR(75) null,
	middleName VARCHAR(75) null,
	lastName VARCHAR(75) null,
	birthDate DATE null,
	gender VARCHAR(75) null,
	jobTitle VARCHAR(75) null,
	status INTEGER
);
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.diit.training.portlets.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import org.diit.training.portlets.model.Registration;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Registration in entity cache.
 *
 * @author Shamsuddin
 * @see Registration
 * @generated
 */
public class RegistrationCacheModel implements CacheModel<Registration>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{registrationUserId=");
		sb.append(registrationUserId);
		sb.append(", screenName=");
		sb.append(screenName);
		sb.append(", emailAddress=");
		sb.append(emailAddress);
		sb.append(", firstName=");
		sb.append(firstName);
		sb.append(", password=");
		sb.append(password);
		sb.append(", middleName=");
		sb.append(middleName);
		sb.append(", lastName=");
		sb.append(lastName);
		sb.append(", birthDate=");
		sb.append(birthDate);
		sb.append(", gender=");
		sb.append(gender);
		sb.append(", jobTitle=");
		sb.append(jobTitle);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	public Registration toEntityModel() {
		RegistrationImpl registrationImpl = new RegistrationImpl();

		registrationImpl.setRegistrationUserId(registrationUserId);

		if (screenName == null) {
			registrationImpl.setScreenName(StringPool.BLANK);
		}
		else {
			registrationImpl.setScreenName(screenName);
		}

		if (emailAddress == null) {
			registrationImpl.setEmailAddress(StringPool.BLANK);
		}
		else {
			registrationImpl.setEmailAddress(emailAddress);
		}

		if (firstName == null) {
			registrationImpl.setFirstName(StringPool.BLANK);
		}
		else {
			registrationImpl.setFirstName(firstName);
		}

		if (password == null) {
			registrationImpl.setPassword(StringPool.BLANK);
		}
		else {
			registrationImpl.setPassword(password);
		}

		if (middleName == null) {
			registrationImpl.setMiddleName(StringPool.BLANK);
		}
		else {
			registrationImpl.setMiddleName(middleName);
		}

		if (lastName == null) {
			registrationImpl.setLastName(StringPool.BLANK);
		}
		else {
			registrationImpl.setLastName(lastName);
		}

		if (birthDate == Long.MIN_VALUE) {
			registrationImpl.setBirthDate(null);
		}
		else {
			registrationImpl.setBirthDate(new Date(birthDate));
		}

		if (gender == null) {
			registrationImpl.setGender(StringPool.BLANK);
		}
		else {
			registrationImpl.setGender(gender);
		}

		if (jobTitle == null) {
			registrationImpl.setJobTitle(StringPool.BLANK);
		}
		else {
			registrationImpl.setJobTitle(jobTitle);
		}

		registrationImpl.setStatus(status);

		registrationImpl.resetOriginalValues();

		return registrationImpl;
	}

	public long registrationUserId;
	public String screenName;
	public String emailAddress;
	public String firstName;
	public String password;
	public String middleName;
	public String lastName;
	public long birthDate;
	public String gender;
	public String jobTitle;
	public Integer status;
}
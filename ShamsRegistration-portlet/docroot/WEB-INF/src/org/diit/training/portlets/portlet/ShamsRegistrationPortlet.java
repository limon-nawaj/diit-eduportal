package org.diit.training.portlets.portlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.diit.training.portlets.dto.ShamsRegistrationDto;
import org.diit.training.portlets.model.Registration;
import org.diit.training.portlets.model.impl.RegistrationImpl;
import org.diit.training.portlets.service.RegistrationLocalService;
import org.diit.training.portlets.service.RegistrationLocalServiceUtil;
import org.diit.training.portlets.service.impl.RegistrationLocalServiceImpl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.RoleConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class ShamsRegistrationPortlet extends MVCPortlet {
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		// TODO Auto-generated method stub
		User user = null;
		try {
			user = PortalUtil.getUser(renderRequest);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean res = isAdmin(user);
		renderRequest.setAttribute("isAdmin", res);
		if (isAdmin(user)) {
			renderRequest.setAttribute("pendingusers", getPendingUsers());
		}
		super.doView(renderRequest, renderResponse);
	}

	public void registrationRedirect(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		actionResponse.setRenderParameter("jspPage", "/html/shamsregistration/Registration.jsp");

	}
	
	public void registrationSubmit(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException{
		ShamsRegistrationDto registrationDTO = new ShamsRegistrationDto();
		registrationDTO.setFirstName(actionRequest.getParameter("FirstName"));
		registrationDTO.setMiddleName(actionRequest.getParameter("mname"));
		registrationDTO.setLastName(actionRequest.getParameter("lname"));
		registrationDTO.setPassword(actionRequest.getParameter("Password"));
		registrationDTO.setScreenName(actionRequest.getParameter("ScreenName"));
		registrationDTO.setEmailAddress(actionRequest.getParameter("EmailAddress"));
		registrationDTO.setGender(actionRequest.getParameter("Gender"));
		
		Registration registration = new RegistrationImpl();
		
		registration.setBirthDate(registrationDTO.getBirthDate());
		registration.setEmailAddress(registrationDTO.getEmailAddress());
		registration.setFirstName(registrationDTO.getFirstName());
		registration.setGender(registrationDTO.getGender());
		registration.setJobTitle(registrationDTO.getJobTitle());
		registration.setLastName(registrationDTO.getJobTitle());
		registration.setMiddleName(registrationDTO.getMiddleName());
		registration.setPassword(registrationDTO.getPassword());
		registration.setScreenName(registrationDTO.getScreenName());
		
		
	
		try
		{
			RegistrationLocalServiceUtil.addRegistration(registration);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*RegistrationDao regitrationdaDao = new RegistrationDao();
		try
		{
			regitrationdaDao.save(new Registration(registrationDTO));
		}
		catch (Exception e)
		{
		
			e.printStackTrace();
		}*/
		
	}

	public List<ShamsRegistrationDto> getPendingUsers() {

		List<ShamsRegistrationDto> list = new ArrayList<ShamsRegistrationDto>();
		return list;

	}

	public boolean isAdmin(User user) {
		try {

			if (null != user) {
				List<Role> roles = user.getRoles();

				Iterator<Role> iterator = roles.iterator();

				while (iterator.hasNext()) {
					Role role = iterator.next();
					if (RoleConstants.ADMINISTRATOR.equals(role.getName())) {
						return true;
					}
				}
			}
		} catch (SystemException e) {

			e.printStackTrace();
		}

		return false;
	}

}

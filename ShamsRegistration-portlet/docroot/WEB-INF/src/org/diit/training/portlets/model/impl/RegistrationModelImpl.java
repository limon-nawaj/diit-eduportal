/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.diit.training.portlets.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.util.PortalUtil;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import org.diit.training.portlets.model.Registration;
import org.diit.training.portlets.model.RegistrationModel;
import org.diit.training.portlets.model.RegistrationSoap;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the Registration service. Represents a row in the &quot;ShamsService_Registration&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link org.diit.training.portlets.model.RegistrationModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link RegistrationImpl}.
 * </p>
 *
 * @author Shamsuddin
 * @see RegistrationImpl
 * @see org.diit.training.portlets.model.Registration
 * @see org.diit.training.portlets.model.RegistrationModel
 * @generated
 */
@JSON(strict = true)
public class RegistrationModelImpl extends BaseModelImpl<Registration>
	implements RegistrationModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a registration model instance should use the {@link org.diit.training.portlets.model.Registration} interface instead.
	 */
	public static final String TABLE_NAME = "ShamsService_Registration";
	public static final Object[][] TABLE_COLUMNS = {
			{ "registrationUserId", Types.BIGINT },
			{ "screenName", Types.VARCHAR },
			{ "emailAddress", Types.VARCHAR },
			{ "firstName", Types.VARCHAR },
			{ "password_", Types.VARCHAR },
			{ "middleName", Types.VARCHAR },
			{ "lastName", Types.VARCHAR },
			{ "birthDate", Types.TIMESTAMP },
			{ "gender", Types.VARCHAR },
			{ "jobTitle", Types.VARCHAR },
			{ "status", Types.INTEGER }
		};
	public static final String TABLE_SQL_CREATE = "create table ShamsService_Registration (registrationUserId LONG not null primary key,screenName VARCHAR(75) null,emailAddress VARCHAR(75) null,firstName VARCHAR(75) null,password_ VARCHAR(75) null,middleName VARCHAR(75) null,lastName VARCHAR(75) null,birthDate DATE null,gender VARCHAR(75) null,jobTitle VARCHAR(75) null,status INTEGER)";
	public static final String TABLE_SQL_DROP = "drop table ShamsService_Registration";
	public static final String ORDER_BY_JPQL = " ORDER BY registration.registrationUserId ASC";
	public static final String ORDER_BY_SQL = " ORDER BY ShamsService_Registration.registrationUserId ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.org.diit.training.portlets.model.Registration"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.org.diit.training.portlets.model.Registration"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = false;

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 */
	public static Registration toModel(RegistrationSoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		Registration model = new RegistrationImpl();

		model.setRegistrationUserId(soapModel.getRegistrationUserId());
		model.setScreenName(soapModel.getScreenName());
		model.setEmailAddress(soapModel.getEmailAddress());
		model.setFirstName(soapModel.getFirstName());
		model.setPassword(soapModel.getPassword());
		model.setMiddleName(soapModel.getMiddleName());
		model.setLastName(soapModel.getLastName());
		model.setBirthDate(soapModel.getBirthDate());
		model.setGender(soapModel.getGender());
		model.setJobTitle(soapModel.getJobTitle());
		model.setStatus(soapModel.getStatus());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 */
	public static List<Registration> toModels(RegistrationSoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<Registration> models = new ArrayList<Registration>(soapModels.length);

		for (RegistrationSoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.org.diit.training.portlets.model.Registration"));

	public RegistrationModelImpl() {
	}

	public long getPrimaryKey() {
		return _registrationUserId;
	}

	public void setPrimaryKey(long primaryKey) {
		setRegistrationUserId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_registrationUserId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	public Class<?> getModelClass() {
		return Registration.class;
	}

	public String getModelClassName() {
		return Registration.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("registrationUserId", getRegistrationUserId());
		attributes.put("screenName", getScreenName());
		attributes.put("emailAddress", getEmailAddress());
		attributes.put("firstName", getFirstName());
		attributes.put("password", getPassword());
		attributes.put("middleName", getMiddleName());
		attributes.put("lastName", getLastName());
		attributes.put("birthDate", getBirthDate());
		attributes.put("gender", getGender());
		attributes.put("jobTitle", getJobTitle());
		attributes.put("status", getStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long registrationUserId = (Long)attributes.get("registrationUserId");

		if (registrationUserId != null) {
			setRegistrationUserId(registrationUserId);
		}

		String screenName = (String)attributes.get("screenName");

		if (screenName != null) {
			setScreenName(screenName);
		}

		String emailAddress = (String)attributes.get("emailAddress");

		if (emailAddress != null) {
			setEmailAddress(emailAddress);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String password = (String)attributes.get("password");

		if (password != null) {
			setPassword(password);
		}

		String middleName = (String)attributes.get("middleName");

		if (middleName != null) {
			setMiddleName(middleName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		Date birthDate = (Date)attributes.get("birthDate");

		if (birthDate != null) {
			setBirthDate(birthDate);
		}

		String gender = (String)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		String jobTitle = (String)attributes.get("jobTitle");

		if (jobTitle != null) {
			setJobTitle(jobTitle);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	@JSON
	public long getRegistrationUserId() {
		return _registrationUserId;
	}

	public void setRegistrationUserId(long registrationUserId) {
		_registrationUserId = registrationUserId;
	}

	public String getRegistrationUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getRegistrationUserId(), "uuid",
			_registrationUserUuid);
	}

	public void setRegistrationUserUuid(String registrationUserUuid) {
		_registrationUserUuid = registrationUserUuid;
	}

	@JSON
	public String getScreenName() {
		if (_screenName == null) {
			return StringPool.BLANK;
		}
		else {
			return _screenName;
		}
	}

	public void setScreenName(String screenName) {
		_screenName = screenName;
	}

	@JSON
	public String getEmailAddress() {
		if (_emailAddress == null) {
			return StringPool.BLANK;
		}
		else {
			return _emailAddress;
		}
	}

	public void setEmailAddress(String emailAddress) {
		_emailAddress = emailAddress;
	}

	@JSON
	public String getFirstName() {
		if (_firstName == null) {
			return StringPool.BLANK;
		}
		else {
			return _firstName;
		}
	}

	public void setFirstName(String firstName) {
		_firstName = firstName;
	}

	@JSON
	public String getPassword() {
		if (_password == null) {
			return StringPool.BLANK;
		}
		else {
			return _password;
		}
	}

	public void setPassword(String password) {
		_password = password;
	}

	@JSON
	public String getMiddleName() {
		if (_middleName == null) {
			return StringPool.BLANK;
		}
		else {
			return _middleName;
		}
	}

	public void setMiddleName(String middleName) {
		_middleName = middleName;
	}

	@JSON
	public String getLastName() {
		if (_lastName == null) {
			return StringPool.BLANK;
		}
		else {
			return _lastName;
		}
	}

	public void setLastName(String lastName) {
		_lastName = lastName;
	}

	@JSON
	public Date getBirthDate() {
		return _birthDate;
	}

	public void setBirthDate(Date birthDate) {
		_birthDate = birthDate;
	}

	@JSON
	public String getGender() {
		if (_gender == null) {
			return StringPool.BLANK;
		}
		else {
			return _gender;
		}
	}

	public void setGender(String gender) {
		_gender = gender;
	}

	@JSON
	public String getJobTitle() {
		if (_jobTitle == null) {
			return StringPool.BLANK;
		}
		else {
			return _jobTitle;
		}
	}

	public void setJobTitle(String jobTitle) {
		_jobTitle = jobTitle;
	}

	@JSON
	public Integer getStatus() {
		return _status;
	}

	public void setStatus(Integer status) {
		_status = status;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
			Registration.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public Registration toEscapedModel() {
		if (_escapedModelProxy == null) {
			_escapedModelProxy = (Registration)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelProxyInterfaces,
					new AutoEscapeBeanHandler(this));
		}

		return _escapedModelProxy;
	}

	@Override
	public Object clone() {
		RegistrationImpl registrationImpl = new RegistrationImpl();

		registrationImpl.setRegistrationUserId(getRegistrationUserId());
		registrationImpl.setScreenName(getScreenName());
		registrationImpl.setEmailAddress(getEmailAddress());
		registrationImpl.setFirstName(getFirstName());
		registrationImpl.setPassword(getPassword());
		registrationImpl.setMiddleName(getMiddleName());
		registrationImpl.setLastName(getLastName());
		registrationImpl.setBirthDate(getBirthDate());
		registrationImpl.setGender(getGender());
		registrationImpl.setJobTitle(getJobTitle());
		registrationImpl.setStatus(getStatus());

		registrationImpl.resetOriginalValues();

		return registrationImpl;
	}

	public int compareTo(Registration registration) {
		int value = 0;

		if (getRegistrationUserId() < registration.getRegistrationUserId()) {
			value = -1;
		}
		else if (getRegistrationUserId() > registration.getRegistrationUserId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		Registration registration = null;

		try {
			registration = (Registration)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = registration.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public void resetOriginalValues() {
	}

	@Override
	public CacheModel<Registration> toCacheModel() {
		RegistrationCacheModel registrationCacheModel = new RegistrationCacheModel();

		registrationCacheModel.registrationUserId = getRegistrationUserId();

		registrationCacheModel.screenName = getScreenName();

		String screenName = registrationCacheModel.screenName;

		if ((screenName != null) && (screenName.length() == 0)) {
			registrationCacheModel.screenName = null;
		}

		registrationCacheModel.emailAddress = getEmailAddress();

		String emailAddress = registrationCacheModel.emailAddress;

		if ((emailAddress != null) && (emailAddress.length() == 0)) {
			registrationCacheModel.emailAddress = null;
		}

		registrationCacheModel.firstName = getFirstName();

		String firstName = registrationCacheModel.firstName;

		if ((firstName != null) && (firstName.length() == 0)) {
			registrationCacheModel.firstName = null;
		}

		registrationCacheModel.password = getPassword();

		String password = registrationCacheModel.password;

		if ((password != null) && (password.length() == 0)) {
			registrationCacheModel.password = null;
		}

		registrationCacheModel.middleName = getMiddleName();

		String middleName = registrationCacheModel.middleName;

		if ((middleName != null) && (middleName.length() == 0)) {
			registrationCacheModel.middleName = null;
		}

		registrationCacheModel.lastName = getLastName();

		String lastName = registrationCacheModel.lastName;

		if ((lastName != null) && (lastName.length() == 0)) {
			registrationCacheModel.lastName = null;
		}

		Date birthDate = getBirthDate();

		if (birthDate != null) {
			registrationCacheModel.birthDate = birthDate.getTime();
		}
		else {
			registrationCacheModel.birthDate = Long.MIN_VALUE;
		}

		registrationCacheModel.gender = getGender();

		String gender = registrationCacheModel.gender;

		if ((gender != null) && (gender.length() == 0)) {
			registrationCacheModel.gender = null;
		}

		registrationCacheModel.jobTitle = getJobTitle();

		String jobTitle = registrationCacheModel.jobTitle;

		if ((jobTitle != null) && (jobTitle.length() == 0)) {
			registrationCacheModel.jobTitle = null;
		}

		registrationCacheModel.status = getStatus();

		return registrationCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{registrationUserId=");
		sb.append(getRegistrationUserId());
		sb.append(", screenName=");
		sb.append(getScreenName());
		sb.append(", emailAddress=");
		sb.append(getEmailAddress());
		sb.append(", firstName=");
		sb.append(getFirstName());
		sb.append(", password=");
		sb.append(getPassword());
		sb.append(", middleName=");
		sb.append(getMiddleName());
		sb.append(", lastName=");
		sb.append(getLastName());
		sb.append(", birthDate=");
		sb.append(getBirthDate());
		sb.append(", gender=");
		sb.append(getGender());
		sb.append(", jobTitle=");
		sb.append(getJobTitle());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(37);

		sb.append("<model><model-name>");
		sb.append("org.diit.training.portlets.model.Registration");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>registrationUserId</column-name><column-value><![CDATA[");
		sb.append(getRegistrationUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>screenName</column-name><column-value><![CDATA[");
		sb.append(getScreenName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>emailAddress</column-name><column-value><![CDATA[");
		sb.append(getEmailAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>firstName</column-name><column-value><![CDATA[");
		sb.append(getFirstName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>password</column-name><column-value><![CDATA[");
		sb.append(getPassword());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>middleName</column-name><column-value><![CDATA[");
		sb.append(getMiddleName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lastName</column-name><column-value><![CDATA[");
		sb.append(getLastName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>birthDate</column-name><column-value><![CDATA[");
		sb.append(getBirthDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>gender</column-name><column-value><![CDATA[");
		sb.append(getGender());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>jobTitle</column-name><column-value><![CDATA[");
		sb.append(getJobTitle());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = Registration.class.getClassLoader();
	private static Class<?>[] _escapedModelProxyInterfaces = new Class[] {
			Registration.class
		};
	private long _registrationUserId;
	private String _registrationUserUuid;
	private String _screenName;
	private String _emailAddress;
	private String _firstName;
	private String _password;
	private String _middleName;
	private String _lastName;
	private Date _birthDate;
	private String _gender;
	private String _jobTitle;
	private Integer _status;
	private Registration _escapedModelProxy;
}
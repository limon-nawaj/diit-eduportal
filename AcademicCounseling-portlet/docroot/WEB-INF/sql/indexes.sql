create index IX_57792A41 on Counseling_Counseling (counselingId);
create index IX_73C99ABF on Counseling_Counseling (userId);

create index IX_93E9927E on Counseling_EduPortal_Counseling (counselingId);
create index IX_C5C834BC on Counseling_EduPortal_Counseling (userId);

create index IX_34F1972C on EduPortal_Counseling (counselingId);
create index IX_BBA4D3EA on EduPortal_Counseling (userId);

create index IX_60DF7B64 on EduPortal_Counseling_Counseling (counselingId);
create index IX_58004222 on EduPortal_Counseling_Counseling (userId);
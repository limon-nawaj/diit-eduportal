create table EduPortal_Counseling_Counseling (
	counselingId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	counselingDate DATE null,
	medium INTEGER,
	name VARCHAR(75) null,
	maximumQualifications VARCHAR(75) null,
	mobile VARCHAR(75) null,
	email VARCHAR(75) null,
	address VARCHAR(75) null,
	note VARCHAR(75) null,
	source INTEGER,
	sourceNote VARCHAR(75) null,
	status INTEGER,
	statusNote VARCHAR(75) null,
	majorInterestCourse LONG
);

create table EduPortal_Counseling_CounselingCourseInterest (
	CounselingCourseInterestId LONG not null primary key IDENTITY,
	courseId LONG,
	counselingId LONG
);
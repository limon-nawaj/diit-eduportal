package info.diit.portal.counseling.dto;

public class CourseDto {

	private long courseId;
	private String courseCode;
	
	public long getCourseId() {
		return courseId;
	}
	
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	
	public String getCourseCode() {
		return courseCode;
	}
	
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getCourseCode();
	}
}

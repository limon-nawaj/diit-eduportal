package info.diit.portal.counseling.dummy;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Shamsuddin Ahammad
 *
 */

public class CounselingDummy implements Serializable {

	private Date date;
	private String name;
	private String mobile;
	private String address;
	private String note;
	private String status;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}

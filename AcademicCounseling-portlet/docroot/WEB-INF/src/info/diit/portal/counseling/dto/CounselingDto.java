package info.diit.portal.counseling.dto;

import info.diit.portal.counseling.model.Counseling;
import info.diit.portal.counseling.model.CounselingCourseInterest;
import info.diit.portal.counseling.model.impl.CounselingImpl;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class CounselingDto implements Serializable{

	private Counseling counseling;
	private Set<CounselingCourseInterest> counselingCourseInterests;
	
	public CounselingDto()
	{
		counseling = new CounselingImpl();
		counseling.setNew(true);
		
		counselingCourseInterests = new HashSet<CounselingCourseInterest>();
	}
	
	public Counseling getCounseling() {
		return counseling;
	}
	public void setCounseling(Counseling counseling) {
		this.counseling = counseling;
	}
	public Set<CounselingCourseInterest> getCounselingCourseInterests() {
		return counselingCourseInterests;
	}
	public void setCounselingCourseInterests(
			Set<CounselingCourseInterest> counselingCourseInterests) {
		this.counselingCourseInterests = counselingCourseInterests;
	}
	
}

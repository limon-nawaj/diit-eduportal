package info.diit.portal.counseling.enums;

public enum Status
{
	ADMITTED(1,"Admitted"),
	FORM_SOLD(2,"Form Sold"),
	REFERRED(3,"Referred");
	
	private int key;
	private String value;
	
	private Status(int key,String value)
	{
		this.key = key;
		this.value = value;
	}
	
	public int getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}
	
	@Override
	public String toString()
	{
		return value;
	}
	
	public static Status getStatus(int key)
	{
		
		Status statuses[]=Status.values();
		
		for(Status status:statuses)
		{
			if(status.key==key)
			{
				return status;
			}
		}
		
		return null;
	}

	
}



package info.diit.portal.counseling.enums;

public enum Source
{
	NEWSPAPER(1,"Newspaper"),
	WEBSITE(2,"Website"),
	REFERENCE(3,"Reference"),
	MAGAZINES(4,"Magazines"),
	POSTERS(5,"Posters"),
	FRIENDS(6,"Friends"),
	RELATIVES(7,"Relatives"),
	BANNERS(8,"Banners"),
	TV(9,"TV"),
	OTHERS(10,"Others")
	;
	
	private int key;
	private String value;
	
	private Source(int key,String value)
	{
		this.key = key;
		this.value = value;
	}
	
	public int getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}
	
	@Override
	public String toString()
	{
		return value;
	}
	
	public static Source getSource(int key)
	{
		
		Source sources[]=Source.values();
		
		for(Source source:sources)
		{
			if(source.key==key)
			{
				return source;
			}
		}
		
		return null;
	}
}



package info.diit.portal.counseling.dao;

import info.diit.portal.counseling.dto.AnalyticsDto;
import info.diit.portal.counseling.dto.CounselingDto;
import info.diit.portal.counseling.dto.CounselingPerformanceDto;
import info.diit.portal.counseling.dto.CounselorDto;
import info.diit.portal.counseling.enums.Medium;
import info.diit.portal.counseling.enums.Source;
import info.diit.portal.counseling.enums.Status;
import info.diit.portal.counseling.model.Counseling;
import info.diit.portal.counseling.model.CounselingCourseInterest;
import info.diit.portal.counseling.service.CounselingLocalServiceUtil;
import info.diit.portal.coursesubject.model.Course;
import info.diit.portal.coursesubject.service.CourseLocalServiceUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

public class CounselingDao {

	public static final String COUNSELOR_GROUP_NAME = "Counselor";
	
	public static CounselingDto addCounseling(CounselingDto counselingDto) throws SystemException
	{
		Counseling counseling = counselingDto.getCounseling();
		
		Set<CounselingCourseInterest> courseInterests = counselingDto.getCounselingCourseInterests();
		
		counseling = CounselingLocalServiceUtil.addCounseling(counseling, courseInterests);
		courseInterests = CounselingLocalServiceUtil.getCounselingCourseInterests(counseling.getCounselingId());
		
		counselingDto.setCounseling(counseling);
		counselingDto.setCounselingCourseInterests(courseInterests);
		
		return counselingDto;
		
	}
	
	public static CounselingDto updateCounseling(CounselingDto counselingDto) throws SystemException
	{
		Counseling counseling = counselingDto.getCounseling();
		Set<CounselingCourseInterest> courseInterests = counselingDto.getCounselingCourseInterests();
		
		counseling = CounselingLocalServiceUtil.updateCounseling(counseling,courseInterests);
		courseInterests = CounselingLocalServiceUtil.getCounselingCourseInterests(counseling.getCounselingId());
		
		counselingDto.setCounseling(counseling);
		counselingDto.setCounselingCourseInterests(courseInterests);
		
		return counselingDto;
		
	}
	
	public static CounselingDto deleteCounseling(CounselingDto counselingDto) throws SystemException
	{
		Counseling counseling = counselingDto.getCounseling();
		
		counseling = CounselingLocalServiceUtil.deleteCounselingWithChild(counseling);
		
		counselingDto.setCounseling(counseling);
		
		return counselingDto;
		
	}
	
	public static CounselingDto findCounseling(long counselingId) throws SystemException
	{
		CounselingDto counselingDto = null;
		
		Counseling counseling = CounselingLocalServiceUtil.fetchCounseling(counselingId);
		
		if(counseling!=null)
		{
			Set<CounselingCourseInterest> courseInterests = CounselingLocalServiceUtil.getCounselingCourseInterests(counseling.getCounselingId());
			
			counselingDto = new CounselingDto();
			counselingDto.setCounseling(counseling);
			counselingDto.setCounselingCourseInterests(courseInterests);
		}
		
		return counselingDto;
	}

	public static List<Counseling> findCounselingByUser(long userId) throws SystemException
	{
		return CounselingLocalServiceUtil.findCounselingByUser(userId);
	}
	
	public static List<Course> findAllCourses() throws SystemException
	{
		return CourseLocalServiceUtil.getCourses(0, CourseLocalServiceUtil.getCoursesCount());
	}
	
	public static List<CounselingPerformanceDto> getPerformanceListByUser(long userId,long companyId,Date startDate,Date endDate) throws SystemException
	{
		List<CounselingPerformanceDto> performanceList = new ArrayList<CounselingPerformanceDto>();
		
		List<Course> courseList = CourseLocalServiceUtil.findByCompany(companyId);
		
		for(Course course:courseList)
		{
			CounselingPerformanceDto performanceDto = new CounselingPerformanceDto();
			
			performanceDto.setCourseCode(course.getCourseCode());
			
			performanceDto.setAdmission(getCountByStatusByUser(userId, course.getCourseId(), Status.ADMITTED.getKey(),startDate, endDate));
			performanceDto.setReference(getCountByStatusByUser(userId, course.getCourseId(), Status.REFERRED.getKey(),startDate, endDate));
			
			performanceDto.setPhysicalCounseling(getCountByMediumByUser(userId, course.getCourseId(), Medium.PHYSICAL.getKey(),startDate, endDate));
			performanceDto.setPhoneCounseling(getCountByMediumByUser(userId, course.getCourseId(), Medium.PHONE.getKey(),startDate, endDate));
			
			performanceList.add(performanceDto);
		}
		
		CounselingPerformanceDto otherCoursePerformanceDto = new CounselingPerformanceDto();
		otherCoursePerformanceDto.setCourseCode("Other");
		
		otherCoursePerformanceDto.setAdmission(getCountByStatusByUser(userId, 0, Status.ADMITTED.getKey(),startDate, endDate));
		otherCoursePerformanceDto.setReference(getCountByStatusByUser(userId, 0, Status.REFERRED.getKey(),startDate, endDate));
		
		otherCoursePerformanceDto.setPhysicalCounseling(getCountByMediumByUser(userId, 0, Medium.PHYSICAL.getKey(),startDate, endDate));
		otherCoursePerformanceDto.setPhoneCounseling(getCountByMediumByUser(userId, 0, Medium.PHONE.getKey(),startDate, endDate));
		
		performanceList.add(otherCoursePerformanceDto);
		
		
		return performanceList;
	}
	
	public static List<CounselingPerformanceDto> getPerformanceListByCompany(long companyId,Date startDate,Date endDate) throws SystemException
	{
		List<CounselingPerformanceDto> performanceList = new ArrayList<CounselingPerformanceDto>();
		
		List<Course> courseList = CourseLocalServiceUtil.findByCompany(companyId);
		
		for(Course course:courseList)
		{
			CounselingPerformanceDto performanceDto = new CounselingPerformanceDto();
			
			performanceDto.setCourseCode(course.getCourseCode());
			
			performanceDto.setAdmission(getCountByStatusByCompany(companyId, course.getCourseId(), Status.ADMITTED.getKey(),startDate, endDate));
			performanceDto.setReference(getCountByStatusByCompany(companyId, course.getCourseId(), Status.REFERRED.getKey(),startDate, endDate));
			
			performanceDto.setPhysicalCounseling(getCountByMediumByCompanyByCourse(companyId, course.getCourseId(), Medium.PHYSICAL.getKey(),startDate, endDate));
			performanceDto.setPhoneCounseling(getCountByMediumByCompanyByCourse(companyId, course.getCourseId(), Medium.PHONE.getKey(),startDate, endDate));
			
			performanceList.add(performanceDto);
		}
		
		CounselingPerformanceDto otherCoursePerformanceDto = new CounselingPerformanceDto();
		otherCoursePerformanceDto.setCourseCode("Other");
		
		otherCoursePerformanceDto.setAdmission(getCountByStatusByCompany(companyId, 0, Status.ADMITTED.getKey(),startDate, endDate));
		otherCoursePerformanceDto.setReference(getCountByStatusByCompany(companyId, 0, Status.REFERRED.getKey(),startDate, endDate));
		
		otherCoursePerformanceDto.setPhysicalCounseling(getCountByMediumByCompanyByCourse(companyId, 0, Medium.PHYSICAL.getKey(),startDate, endDate));
		otherCoursePerformanceDto.setPhoneCounseling(getCountByMediumByCompanyByCourse(companyId, 0, Medium.PHONE.getKey(),startDate, endDate));
		
		performanceList.add(otherCoursePerformanceDto);
		
		
		return performanceList;
	}

	
	public static int getCountByStatusByUser(long userId,long courseId,int status,Date startDate,Date endDate) throws SystemException
	{
		int count = 0;

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Counseling.class)
				.add(PropertyFactoryUtil.forName("userId").eq(new Long(userId)))
				.add(PropertyFactoryUtil.forName("majorInterestCourse").eq(new Long(courseId)))
				.add(PropertyFactoryUtil.forName("counselingDate").between(startDate, endDate))
				.add(PropertyFactoryUtil.forName("status").eq(new Integer(status)))
				.setProjection(ProjectionFactoryUtil.rowCount());
		
		List result = CounselingLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		if(result != null)
		{
			count = ((Long)result.get(0)).intValue();
		}
		
		return count;
	}
	
	public static int getCountByStatusByCompany(long companyId,long courseId,int status,Date startDate,Date endDate) throws SystemException
	{
		int count = 0;

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Counseling.class)
				.add(PropertyFactoryUtil.forName("companyId").eq(new Long(companyId)))
				.add(PropertyFactoryUtil.forName("majorInterestCourse").eq(new Long(courseId)))
				.add(PropertyFactoryUtil.forName("counselingDate").between(startDate, endDate))
				.add(PropertyFactoryUtil.forName("status").eq(new Integer(status)))
				.setProjection(ProjectionFactoryUtil.rowCount());
		
		List result = CounselingLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		if(result != null)
		{
			count = ((Long)result.get(0)).intValue();
		}
		
		return count;
	}

	
	public static int getCountByMediumByUser(long userId,long courseId,int medium,Date startDate,Date endDate) throws SystemException
	{
		int count = 0;

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Counseling.class)
				.add(PropertyFactoryUtil.forName("userId").eq(new Long(userId)))
				.add(PropertyFactoryUtil.forName("majorInterestCourse").eq(new Long(courseId)))
				.add(PropertyFactoryUtil.forName("counselingDate").between(startDate, endDate))
				.add(PropertyFactoryUtil.forName("medium").eq(new Integer(medium)))
				.setProjection(ProjectionFactoryUtil.rowCount());
		
		List result = CounselingLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		if(result != null)
		{
			count = ((Long)result.get(0)).intValue();
		}
		
		return count;
	}
	
	public static int getCountByMediumByCompanyByCourse(long companyId,long courseId,int medium,Date startDate,Date endDate) throws SystemException
	{
		int count = 0;

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Counseling.class)
				.add(PropertyFactoryUtil.forName("companyId").eq(new Long(companyId)))
				.add(PropertyFactoryUtil.forName("majorInterestCourse").eq(new Long(courseId)))
				.add(PropertyFactoryUtil.forName("counselingDate").between(startDate, endDate))
				.add(PropertyFactoryUtil.forName("medium").eq(new Integer(medium)))
				.setProjection(ProjectionFactoryUtil.rowCount());
		
		List result = CounselingLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		if(result != null)
		{
			count = ((Long)result.get(0)).intValue();
		}
		
		return count;
	}
	
	
	public static int getCountByMediumByCompany(long companyId,int medium,Date startDate,Date endDate) throws SystemException
	{
		int count = 0;

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Counseling.class)
				.add(PropertyFactoryUtil.forName("companyId").eq(new Long(companyId)))
				.add(PropertyFactoryUtil.forName("counselingDate").between(startDate, endDate))
				.add(PropertyFactoryUtil.forName("medium").eq(new Integer(medium)))
				.setProjection(ProjectionFactoryUtil.rowCount());
		
		List result = CounselingLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		if(result != null)
		{
			count = ((Long)result.get(0)).intValue();
		}
		
		return count;
	}

	
	public static List<CounselorDto> getCounselorList(long companyId) throws PortalException, SystemException
	{
		List<CounselorDto> counselors = new ArrayList<CounselorDto>();
		
		UserGroup userGroup = UserGroupLocalServiceUtil.getUserGroup(companyId, COUNSELOR_GROUP_NAME);
		
		List<User> users = UserLocalServiceUtil.getUserGroupUsers(userGroup.getUserGroupId());
		
		for(User user:users)
		{
			CounselorDto counselor = new CounselorDto();
			counselor.setUserId(user.getUserId());
			counselor.setName(user.getFullName());
			
			counselors.add(counselor);
		}
		
		return counselors;
	}
	
	public static List<AnalyticsDto> getMediumAnalytics(long companyId,Date startDate, Date endDate) throws SystemException
	{
		Medium media[] = Medium.values();
		
		List<AnalyticsDto> analytics = new ArrayList<AnalyticsDto>(media.length);
		int totalCount = 0;
		for(Medium medium:media)
		{
			AnalyticsDto analytic = new AnalyticsDto();
			analytic.setLabel(medium.getValue());
			analytic.setCount(getCountByMediumByCompany(companyId, medium.getKey(), startDate, endDate));
			
			totalCount += analytic.getCount();
			
			analytics.add(analytic);
		}
		
		for(AnalyticsDto analytic:analytics)
		{
			double percentage = 0;
			
			if(totalCount>0)
			{
				percentage = (double)analytic.getCount()/totalCount*100;
			}
			analytic.setPercentage(percentage);
		}
		
		return analytics;
	}
	
	public static int getCountCourseInterest(long companyId,long courseId,Date startDate, Date endDate) throws SystemException
	{
		int count = 0;

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Counseling.class)
				.add(PropertyFactoryUtil.forName("companyId").eq(new Long(companyId)))
				.add(PropertyFactoryUtil.forName("counselingDate").between(startDate, endDate))
				.add(PropertyFactoryUtil.forName("majorInterestCourse").eq(new Long(courseId)))
				.setProjection(ProjectionFactoryUtil.rowCount());
		
		List result = CounselingLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		if(result != null)
		{
			count = ((Long)result.get(0)).intValue();
		}
		
		return count;
	}
	
	public static List<AnalyticsDto> getCourseAnalytics(long companyId,Date startDate, Date endDate) throws SystemException
	{
		List<AnalyticsDto> analytics = new ArrayList<AnalyticsDto>();

		List<Course> courseList = CourseLocalServiceUtil.getCourses(0, CourseLocalServiceUtil.getCoursesCount());
		
		int totalCount = 0;
		
		for(Course course:courseList)
		{
			AnalyticsDto analytic = new AnalyticsDto();
			analytic.setLabel(course.getCourseName());
			analytic.setCount(getCountCourseInterest(companyId, course.getCourseId(), startDate, endDate));
			
			analytics.add(analytic);
			
			totalCount += analytic.getCount();
			
		}
		
		//add undefined majorIntereset course
		AnalyticsDto analytic = new AnalyticsDto();
		analytic.setLabel("Unknown");
		analytic.setCount(getCountCourseInterest(companyId, 0, startDate, endDate));
		
		analytics.add(analytic);
		totalCount += analytic.getCount();
		
		//calculate the percentage
		
		for(AnalyticsDto analyticDto:analytics)
		{
			double percentage = 0;
			
			if(totalCount>0)
			{
				percentage = (double)analyticDto.getCount()/totalCount*100;
			}
			
			analyticDto.setPercentage(percentage);
		}
		
		return analytics;
	}
	
	public static int getCountBySource(long companyId,int sourceId,Date startDate, Date endDate) throws SystemException
	{
		int count = 0;

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Counseling.class)
				.add(PropertyFactoryUtil.forName("companyId").eq(new Long(companyId)))
				.add(PropertyFactoryUtil.forName("counselingDate").between(startDate, endDate))
				.add(PropertyFactoryUtil.forName("source").eq(new Integer(sourceId)))
				.setProjection(ProjectionFactoryUtil.rowCount());
		
		List result = CounselingLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		if(result != null)
		{
			count = ((Long)result.get(0)).intValue();
		}
		
		return count;
	}

	public static List<AnalyticsDto> getSourceAnalytics(long companyId,Date startDate, Date endDate) throws SystemException
	{
		Source sources[] = Source.values();
		
		List<AnalyticsDto> analytics = new ArrayList<AnalyticsDto>(sources.length);
		int totalCount = 0;
		for(Source source:sources)
		{
			AnalyticsDto analytic = new AnalyticsDto();
			analytic.setLabel(source.getValue());
			analytic.setCount(getCountBySource(companyId, source.getKey(), startDate, endDate));
			
			totalCount += analytic.getCount();
			
			analytics.add(analytic);
		}
		
		//unknonw source
		AnalyticsDto analytic = new AnalyticsDto();
		analytic.setLabel("Unknown");
		analytic.setCount(getCountBySource(companyId, 0, startDate, endDate));
		
		totalCount += analytic.getCount();
		
		analytics.add(analytic);

		
		for(AnalyticsDto analyticDto:analytics)
		{
			double percentage = 0;
			
			if(totalCount>0)
			{
				percentage = (double)analyticDto.getCount()/totalCount*100;
			}
			analyticDto.setPercentage(percentage);
		}
		
		return analytics;
	}

	public static int getCountByStatus(long companyId,int statusId,Date startDate, Date endDate) throws SystemException
	{
		int count = 0;

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Counseling.class)
				.add(PropertyFactoryUtil.forName("companyId").eq(new Long(companyId)))
				.add(PropertyFactoryUtil.forName("counselingDate").between(startDate, endDate))
				.add(PropertyFactoryUtil.forName("status").eq(new Integer(statusId)))
				.setProjection(ProjectionFactoryUtil.rowCount());
		
		List result = CounselingLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		if(result != null)
		{
			count = ((Long)result.get(0)).intValue();
		}
		
		return count;
	}

	public static List<AnalyticsDto> getStatusAnalytics(long companyId,Date startDate, Date endDate) throws SystemException
	{
		Status statuses[] = Status.values();
		
		List<AnalyticsDto> analytics = new ArrayList<AnalyticsDto>(statuses.length);
		
		int totalCount = 0;
		
		for(Status status:statuses)
		{
			AnalyticsDto analytic = new AnalyticsDto();
			analytic.setLabel(status.getValue());
			analytic.setCount(getCountByStatus(companyId, status.getKey(), startDate, endDate));
			analytic.setPercentage(0);
			
			analytics.add(analytic);
			
			totalCount += analytic.getCount();
		}
		
		//unknonw source
		AnalyticsDto analytic = new AnalyticsDto();
		analytic.setLabel("Unknown");
		analytic.setCount(getCountByStatus(companyId, 0, startDate, endDate));
		
		analytics.add(analytic);
		
		totalCount += analytic.getCount();
		
		//calculate percentage
		for(AnalyticsDto analyticDto:analytics)
		{
			double percentage = 0;
			
			if(totalCount>0)
			{
				percentage = (double)analyticDto.getCount()/totalCount*100;
			}
			analyticDto.setPercentage(percentage);
		}
		
		return analytics;
	}
	
	
}

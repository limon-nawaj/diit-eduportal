package info.diit.portal.counseling.dto;

public class CounselingPerformanceDto {

	private String courseCode;
	private Integer admission;
	private Integer reference;
	private Integer physicalCounseling;
	private Integer phoneCounseling;
	private Integer emailCounseling;
	private Double successRate;
	
	public CounselingPerformanceDto() {
		
		courseCode = "";
		admission = new Integer(0);
		reference = new Integer(0);
		physicalCounseling = new Integer(0);
		phoneCounseling = new Integer(0);
		emailCounseling = new Integer(0);
		successRate = new Double(0);
	}

	public String getCourseCode() {
		return courseCode;
	}
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}
	public Integer getAdmission() {
		return admission;
	}
	public void setAdmission(Integer admission) {
		this.admission = admission;
	}
	public Integer getReference() {
		return reference;
	}
	public void setReference(Integer reference) {
		this.reference = reference;
	}
	
	public Integer getPhysicalCounseling() {
		return physicalCounseling;
	}
	public void setPhysicalCounseling(Integer physicalCounseling) {
		this.physicalCounseling = physicalCounseling;
	}
	public Integer getPhoneCounseling() {
		return phoneCounseling;
	}
	public void setPhoneCounseling(Integer phoneCounseling) {
		this.phoneCounseling = phoneCounseling;
	}
	public Integer getEmailCounseling() {
		return emailCounseling;
	}
	public void setEmailCounseling(Integer emailCounseling) {
		this.emailCounseling = emailCounseling;
	}
	public Double getSuccessRate() {
		
		if(getPhoneCounseling()+getPhysicalCounseling()+getEmailCounseling()!=0)
		{
			successRate = new Double(getAdmission())/(getPhoneCounseling()+getPhysicalCounseling()+getEmailCounseling())*100;
		}
		else
		{
			successRate = new Double(0);
		}
		return successRate;
		
	}
	public void setSuccessRate(Double successRate) {
		this.successRate = successRate;
	}
	
}

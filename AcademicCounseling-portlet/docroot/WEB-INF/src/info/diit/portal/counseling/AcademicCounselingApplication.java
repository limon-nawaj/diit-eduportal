package info.diit.portal.counseling;

import info.diit.portal.counseling.dao.CounselingDao;
import info.diit.portal.counseling.dto.CounselingDto;
import info.diit.portal.counseling.dto.CourseDto;
import info.diit.portal.counseling.enums.Medium;
import info.diit.portal.counseling.enums.Source;
import info.diit.portal.counseling.enums.Status;
import info.diit.portal.counseling.model.Counseling;
import info.diit.portal.counseling.model.CounselingCourseInterest;
import info.diit.portal.counseling.model.impl.CounselingCourseInterestImpl;
import info.diit.portal.coursesubject.model.Course;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.vaadin.dialogs.ConfirmDialog;

import com.jensjansson.pagedtable.PagedTable;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/**
 * 
 * @author Shamsuddin Ahammad
 *
 */

@SuppressWarnings("serial")
public class AcademicCounselingApplication extends Application  implements PortletRequestListener{

	//Constants
	public final static String DATE_FORMAT = "dd/MM/yyyy";
	
	
	//columns for manage entries table
	public final static String COLUMN_DATE 		= "counselingDate";
	public final static String COLUMN_NAME 		= "name";
	public final static String COLUMN_MOBILE	= "mobile";
	public final static String COLUMN_ADDRESS	= "address";
	public final static String COLUMN_NOTE 		= "note";
	public final static String COLUMN_STATUS	= "status";


	
	
	private TabSheet counselingTabSheet;
	
	//Counseling Form Components
	private DateField dateField;
	private ComboBox mediumComboBox;
	private TextField nameField;
	private ComboBox qualificationsComboBox;
	private TextField emailField;
	private TextField mobileField;
	private ComboBox majorInterestComboBox;
	private OptionGroup otherInterestsOptionGroup;
	private TextArea addressTextArea;
	private TextArea noteTextArea;
	private TextField sourceNoteField;
	private ComboBox sourceComboBox;
	private ComboBox statusComboBox;
	private TextField statusNoteField;
	
	private BeanItemContainer<Counseling> counselingBeanItemContainer;
	
	
	
	//Manage Entries Form Components
	
	private PagedTable manageEntriesTable;
	
	//Layouts
	
	private GridLayout counselingLayout;
	
	
	
	private ThemeDisplay themeDisplay;
	private Window window;
	
	//data
	
	private CounselingDto counselingDto;
	private List<CourseDto> courseList;
	private CourseDto selectedCourse;
	private Set<CourseDto> otherInterestCourses;
	
	private static DecimalFormat twoDecimalFormat = new DecimalFormat("0.00");
	
	public void init() {
		window = new Window();
		window.setSizeFull();
		setMainWindow(window);
		
		loadCourseList();
		
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.setSpacing(true);

		verticalLayout.addComponent(initCounselingTabSheet());
		
		window.addComponent(verticalLayout);
		
	}
	
	@Override
    public void onRequestStart(PortletRequest p_request, PortletResponse p_response) {
        themeDisplay = (ThemeDisplay) p_request.getAttribute(WebKeys.THEME_DISPLAY);
        
        try {
			User user = themeDisplay.getUser();
			List<Organization> orgList = user.getOrganizations();
			for(Organization org:orgList)
			{
				System.err.println("ORG: "+org.getName());
			}
			
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}
	
	private TabSheet initCounselingTabSheet()
	{
		counselingTabSheet = new TabSheet();
		counselingLayout = initCounselingLayout();
		counselingTabSheet.setWidth("100%");
		
		counselingTabSheet.addTab(counselingLayout,"Counseling");
		counselingTabSheet.addTab(initManageEntriesLayout(),"Manage Entries");
		
		
		return counselingTabSheet;

	}
	
	
	
	
	private GridLayout initCounselingLayout() 
	{
		GridLayout gridLayout = new GridLayout(3,8);
		gridLayout.setSpacing(true);
		
		gridLayout.setWidth("100%");
		
		
		dateField = new DateField("Date");
		dateField.setResolution(DateField.RESOLUTION_DAY);
		dateField.setDateFormat(DATE_FORMAT);
		dateField.setValue(new Date());
		gridLayout.addComponent(dateField,0,0);
		
		mediumComboBox = new ComboBox("Counseling Medium");
		
		Medium media[] = Medium.values();
		
		for(int i=0;i<media.length;i++)
		{
			mediumComboBox.addItem(media[i]);
		}
		
		if(media.length>0)
		{
			mediumComboBox.setValue(media[0]);
		}
		
		mediumComboBox.setRequired(true);
		mediumComboBox.setRequiredError("This field is required");
		mediumComboBox.setImmediate(true);
		mediumComboBox.setInvalidAllowed(false);
		mediumComboBox.setNullSelectionAllowed(false);
		
		
		mediumComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				mediumComboBox.validate();
				
			}
		});
		
		gridLayout.addComponent(mediumComboBox,1,0);
		
		nameField = new TextField("Name");
		gridLayout.addComponent(nameField,0,1);
		
		qualificationsComboBox = new ComboBox("Maximum Qualifications");
		qualificationsComboBox.addItem("HSC");
		qualificationsComboBox.addItem("A Level");
		qualificationsComboBox.addItem("O Level");
		qualificationsComboBox.addItem("Bachelor");
		qualificationsComboBox.addItem("Masters");
		qualificationsComboBox.setNewItemsAllowed(true);
		
		gridLayout.addComponent(qualificationsComboBox,1,1);
		
		mobileField = new TextField("Mobile");
		gridLayout.addComponent(mobileField,0,2);
		
		emailField = new TextField("Email");
		emailField.addValidator(new EmailValidator("Invalid Email Address"));
		
		gridLayout.addComponent(emailField,1,2);
		
		majorInterestComboBox = new ComboBox("Major Interest");
		majorInterestComboBox.setImmediate(true);
		
		otherInterestsOptionGroup = new OptionGroup("Other Interests");
		otherInterestsOptionGroup.setImmediate(true);
		
		if(courseList!=null)
		{
			for(CourseDto course:courseList)
			{
				otherInterestsOptionGroup.addItem(course);
				majorInterestComboBox.addItem(course);
			}
		}
		
		otherInterestsOptionGroup.setMultiSelect(true);
		otherInterestsOptionGroup.addStyleName("horizontal");
		
		gridLayout.addComponent(majorInterestComboBox,0,3);
		gridLayout.addComponent(otherInterestsOptionGroup,1,3,2,3);
		
		majorInterestComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if(selectedCourse!=null)
				{
					otherInterestsOptionGroup.setItemEnabled(selectedCourse,true);
				}
				
				selectedCourse = (CourseDto)majorInterestComboBox.getValue();
				
				if(selectedCourse!=null)
				{
					otherInterestsOptionGroup.setItemEnabled(selectedCourse,false);
				}
				
			}
		});
		
		addressTextArea = new TextArea("Address");
		addressTextArea.setWidth("100%");
		gridLayout.addComponent(addressTextArea,0,4);
		
		noteTextArea = new TextArea("Note");
		noteTextArea.setWidth("100%");
		gridLayout.addComponent(noteTextArea,1,4,2,4);
		
		sourceComboBox = new ComboBox("Source");
		
		for(Source source:Source.values())
		{
			sourceComboBox.addItem(source);
		}

		gridLayout.addComponent(sourceComboBox,0,5);

		sourceNoteField = new TextField("Source Note");
		sourceNoteField.setWidth("100%");
		
		gridLayout.addComponent(sourceNoteField,1,5);
		
		statusComboBox = new ComboBox("Status");
		statusComboBox.setTextInputAllowed(false);
		
		for(Status status:Status.values())
		{
			statusComboBox.addItem(status);
		}
		
		gridLayout.addComponent(statusComboBox,0,6);
		
		statusNoteField = new TextField("Status Note");
		statusNoteField.setWidth("100%");
		gridLayout.addComponent(statusNoteField,1,6);
		
		
		Button saveButton = new Button("Save");
		Button deleteButton = new Button("Delete");
		Button newButton = new Button("New");
		
		HorizontalLayout buttonRow = new HorizontalLayout();
		buttonRow.setSpacing(true);
		buttonRow.setWidth("100%");
		
		Label spacer = new Label();
		
		
		buttonRow.addComponent(spacer);
		buttonRow.addComponent(saveButton);
		buttonRow.addComponent(deleteButton);
		buttonRow.addComponent(newButton);
		
		buttonRow.setExpandRatio(spacer, 1);
		
		gridLayout.addComponent(buttonRow,0,7,2,7);
		
		newButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				clearCounselingForm();
				
			}
		});
		
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				if(counselingDto==null)
				{
					counselingDto = new CounselingDto();
					
				}
				
				Counseling counseling = counselingDto.getCounseling();
				
				Object address = addressTextArea.getValue();
				
				if(address!=null)
				{
					counseling.setAddress(addressTextArea.getValue().toString());
				}
				counseling.setCompanyId(themeDisplay.getCompanyId());
				counseling.setCounselingDate((Date)dateField.getValue());
				counseling.setCreateDate(new Date());
				
				Object email = emailField.getValue();
				
				if(emailField.isValid())
				{
					if(email!=null)
					{
						counseling.setEmail(email.toString());
					}

				}
				else
				{
					window.showNotification("Invalid Email Address!");
					
					return;
				}
				
				
				Object qualifications = qualificationsComboBox.getValue();
				if(qualifications!=null)
				{
					counseling.setMaximumQualifications(qualifications.toString());
				}
				
				if(mediumComboBox.isValid())
				{
					Medium medium = (Medium)mediumComboBox.getValue();
					
					if(medium!=null)
					{
						counseling.setMedium(medium.getKey());
					}
				}
				else
				{
					window.showNotification("Please select a Medium");
					return;
				}
					
				Object mobile = mobileField.getValue();
				if(mobile!=null)
				{
					counseling.setMobile(mobile.toString());
				}
				
				counseling.setModifiedDate(new Date());
				
				Object name = nameField.getValue();
				if(name!=null)
				{
					counseling.setName(name.toString());
				}
				
				Object note = noteTextArea.getValue();
				if(note!=null)
				{
					counseling.setNote(note.toString());
				}
				
				Source source = (Source)sourceComboBox.getValue();
				if(source!=null)
				{
					counseling.setSource(source.getKey());
				}
				
				Object sourceNote = sourceNoteField.getValue();
				if(sourceNote!=null)
				{
					counseling.setSourceNote(sourceNote.toString());
				}
				
				Status status = (Status)statusComboBox.getValue();
				if(status!=null)
				{
					counseling.setStatus(status.getKey());
				}
				
				Object statusNote = statusNoteField.getValue();
				if(statusNote!=null)
				{
					counseling.setStatusNote(statusNote.toString());
				}
				
				counseling.setUserId(themeDisplay.getUserId());
				counseling.setUserName(themeDisplay.getUser().getScreenName());
				
				CourseDto majorInterest = (CourseDto)majorInterestComboBox.getValue();
				
				if(majorInterest!=null)
				{
					counseling.setMajorInterestCourse(majorInterest.getCourseId());
					
				}
				
				//other interest courses
				
				otherInterestCourses = (Set<CourseDto>)otherInterestsOptionGroup.getValue();
				
				Set<CounselingCourseInterest> counselingCourseInterests = new HashSet<CounselingCourseInterest>(otherInterestCourses.size());
				
				long rogueId=0;
				for(CourseDto course:otherInterestCourses)
				{
					CounselingCourseInterest counselingCourseInterest = new CounselingCourseInterestImpl();
					counselingCourseInterest.setCounselingCourseInterestId(++rogueId);
					counselingCourseInterest.setCourseId(course.getCourseId());
					counselingCourseInterests.add(counselingCourseInterest);
				}
				counselingDto.setCounselingCourseInterests(counselingCourseInterests);
				
				
				try
				{
					if(counselingDto.getCounseling().isNew())
					{
						//counseling = CounselingLocalServiceUtil.addCounseling(counseling);
						counselingDto = CounselingDao.addCounseling(counselingDto);
						counselingDto.getCounseling().setNew(false);
						counselingBeanItemContainer.addItem(counselingDto.getCounseling());
						manageEntriesTable.refreshRowCache();
						window.showNotification("Counseling data saved!");
					}
					else
					{
						counselingDto = CounselingDao.updateCounseling(counselingDto);
						counselingDto.getCounseling().setNew(false);
						manageEntriesTable.refreshRowCache();
						window.showNotification("Counseling data updated!");
						
					}
					
					updateTablesData();
					//loadManageEntriesTableData();
					//clearCounselingForm();
				}
				catch (SystemException e)
				{
					e.printStackTrace();
				}
			}
		});
		
		deleteButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				deleteCounseling();
				
			}
		});
		
		return gridLayout;
		
	}
	
	public void showCounseling(CounselingDto counselingDto)
	{
		Counseling counseling = counselingDto.getCounseling();
		dateField.setValue(counseling.getCounselingDate());
		mediumComboBox.setValue(Medium.getMedium(counseling.getMedium()));
		nameField.setValue(counseling.getName());
		qualificationsComboBox.setValue(counseling.getMaximumQualifications());
		mobileField.setValue(counseling.getMobile());
		emailField.setValue(counseling.getEmail());
		majorInterestComboBox.setValue(getCourseDto(counseling.getMajorInterestCourse()));



		private CourseDto getCourseDto(long courseId)
	{
		if(courseList!=null)
		{
			for(CourseDto course:courseList)
			{
				if(course.getCourseId()==courseId)
				{
					return course;
				}
			}
		}
		
		return null;
	}




		
		if(otherInterestCourses!=null)
		{
			otherInterestsOptionGroup.setValue(otherInterestCourses);
		}
		addressTextArea.setValue(counseling.getAddress());
		noteTextArea.setValue(counseling.getNote());
		statusComboBox.setValue(Status.getStatus(counseling.getStatus()));
		sourceComboBox.setValue(Source.getSource(counseling.getSource()));
		sourceNoteField.setValue(counseling.getSourceNote());
		statusNoteField.setValue(counseling.getStatusNote());
		
	}
	
	
	public void clearCounselingForm()
	{
		dateField.setValue(new Date());
		mediumComboBox.setValue(Medium.PHYSICAL);
		nameField.setValue("");
		qualificationsComboBox.setValue(null);
		mobileField.setValue("");
		emailField.setValue("");
		majorInterestComboBox.setValue(null);
		otherInterestsOptionGroup.setValue(null);
		addressTextArea.setValue("");
		noteTextArea.setValue("");
		sourceComboBox.setValue(null);
		statusComboBox.setValue(null);
		sourceNoteField.setValue("");
		statusNoteField.setValue("");
		counselingDto = null;
	}

	
	public VerticalLayout initManageEntriesLayout()
	{
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.setSpacing(true);
		
		manageEntriesTable = new PagedTable()
		{
			@Override
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				if(property.getType()==Date.class)
				{
					if(property.getValue()!=null)
					{
						SimpleDateFormat dateFormat = new SimpleDateFormat("EEE dd/MM/yyyy");
						return dateFormat.format(property.getValue());
					}
				}
				
				if(colId.toString().equalsIgnoreCase(COLUMN_STATUS))
				{
					Status status = Status.getStatus(((Integer)property.getValue()).intValue()); 
					
					if(status!=null)
					{
						return status.getValue();
					}
					else
					{
						return "";
					}
					
				}
				
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		
		manageEntriesTable.setWidth("100%");
		manageEntriesTable.setSelectable(true);
		manageEntriesTable.setImmediate(true);
		
		counselingBeanItemContainer = new BeanItemContainer<Counseling>(Counseling.class);
		manageEntriesTable.setContainerDataSource(counselingBeanItemContainer);
		manageEntriesTable.setVisibleColumns(new String[]{COLUMN_DATE,COLUMN_NAME,COLUMN_MOBILE,COLUMN_ADDRESS,COLUMN_STATUS,COLUMN_NOTE});
		
		manageEntriesTable.setColumnHeader(COLUMN_DATE,"Date");
		manageEntriesTable.setColumnHeader(COLUMN_NAME,"Name");
		manageEntriesTable.setColumnHeader(COLUMN_MOBILE,"Mobile");
		manageEntriesTable.setColumnHeader(COLUMN_ADDRESS,"Address");
		manageEntriesTable.setColumnHeader(COLUMN_STATUS,"Status");
		manageEntriesTable.setColumnHeader(COLUMN_NOTE,"Note");
		
				
		loadManageEntriesTableData();
		verticalLayout.addComponent(manageEntriesTable);
		HorizontalLayout manageEntriesTableControls = manageEntriesTable.createControls();
		verticalLayout.addComponent(manageEntriesTableControls);
		manageEntriesTable.setPageLength(10);
		
		
		HorizontalLayout buttonRow = new HorizontalLayout();
		buttonRow.setSpacing(true);
		buttonRow.setWidth("100%");
		
		Label spacer = new Label();
		buttonRow.addComponent(spacer);
		buttonRow.setExpandRatio(spacer, 1);
		buttonRow.setComponentAlignment(spacer, Alignment.BOTTOM_RIGHT);
		
		Button editButton = new Button("Edit");
		buttonRow.addComponent(editButton);
		buttonRow.setComponentAlignment(editButton, Alignment.BOTTOM_RIGHT);
		
		editButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				try {
					counselingDto = CounselingDao.findCounseling(((Counseling)manageEntriesTable.getValue()).getCounselingId());
					if(counselingDto!=null)
					{
						counselingDto.getCounseling().setNew(false);
						otherInterestCourses = new HashSet();
						
						
						Set<CounselingCourseInterest> courseInterests;
						
						courseInterests = counselingDto.getCounselingCourseInterests();
						for(CounselingCourseInterest courseInterest:courseInterests)
						{
							CourseDto courseDto = getCourseDto(courseInterest.getCourseId());
							otherInterestCourses.add(courseDto);
						}
						
						showCounseling(counselingDto);
						counselingTabSheet.setSelectedTab(counselingLayout);
					}

				} catch (SystemException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
			
		});
		
		Button deleteButton = new Button("Delete");
		buttonRow.addComponent(deleteButton);
		buttonRow.setComponentAlignment(deleteButton, Alignment.BOTTOM_RIGHT);
		
		deleteButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				try
				{
					counselingDto = CounselingDao.findCounseling(((Counseling)manageEntriesTable.getValue()).getCounselingId());
					deleteCounseling();
					
				}
				catch(SystemException se)
				{
					se.printStackTrace();
				}
				
			}
		});
		
		
		verticalLayout.addComponent(buttonRow);
		
		return verticalLayout;
	}
	
	public void loadManageEntriesTableData()
	{
		if(counselingBeanItemContainer!=null)
		{
			counselingBeanItemContainer.removeAllItems();
			try {
				//List<Counseling> counselings = CounselingLocalServiceUtil.getCounselings(0, 10);
				
				List<Counseling> counselings = CounselingDao.findCounselingByUser(themeDisplay.getUserId());
				
				for(Counseling counseling:counselings)
				{
					counselingBeanItemContainer.addBean(counseling);
				}
				counselingBeanItemContainer.sort(new String[]{COLUMN_DATE}, new boolean[]{false});
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void deleteCounseling()
	{
		
			if(counselingDto!=null)
			{
				ConfirmDialog.show(window, "Are you sure you want to delete this counseling record?",
				        new ConfirmDialog.Listener() {

				            public void onClose(ConfirmDialog dialog) {
				                if (dialog.isConfirmed()) {
				                	
				                	try {
										CounselingDao.deleteCounseling(counselingDto);
										counselingBeanItemContainer.removeItem(counselingDto.getCounseling());
										manageEntriesTable.refreshRowCache();
										updateTablesData();
										clearCounselingForm();
										counselingDto = null;
										window.showNotification("One counseling record is deleted.");
									} catch (SystemException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										window.showNotification("Error! Cannot delete the counseling record.");
									}
				                    
				                } 
				            }
				        });
				
			}
			else
			{
				window.showNotification("No Counseling record is selected !!");
			}
		
	}
	
	public void loadCourseList()
	{
		try {
			
			List<Course> courses = CounselingDao.findAllCourses();
			
			if(courseList==null)
			{
				courseList = new ArrayList<CourseDto>(courses.size());
			}
			else
			{
				courseList.clear();
			}
			
			for(Course course:courses)
			{
				CourseDto courseDto = new CourseDto();
				courseDto.setCourseId(course.getCourseId());
				courseDto.setCourseCode(course.getCourseCode());
				
				courseList.add(courseDto);
			}
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private CourseDto getCourseDto(long courseId)
	{
		if(courseList!=null)
		{
			for(CourseDto course:courseList)
			{
				if(course.getCourseId()==courseId)
				{
					return course;
				}
			}
		}
		
		return null;
	}
	
	public void updateTablesData()
	{
		loadManageEntriesTableData();
		manageEntriesTable.refreshRowCache();
	
	}

}

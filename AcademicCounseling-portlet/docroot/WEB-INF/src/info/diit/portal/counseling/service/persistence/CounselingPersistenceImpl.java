/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.counseling.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.jdbc.MappingSqlQuery;
import com.liferay.portal.kernel.dao.jdbc.MappingSqlQueryFactoryUtil;
import com.liferay.portal.kernel.dao.jdbc.RowMapper;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.counseling.NoSuchCounselingException;
import info.diit.portal.counseling.model.Counseling;
import info.diit.portal.counseling.model.impl.CounselingImpl;
import info.diit.portal.counseling.model.impl.CounselingModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the counseling service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shamsuddin
 * @see CounselingPersistence
 * @see CounselingUtil
 * @generated
 */
public class CounselingPersistenceImpl extends BasePersistenceImpl<Counseling>
	implements CounselingPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CounselingUtil} to access the counseling persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CounselingImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_FETCH_BY_COUNSELING = new FinderPath(CounselingModelImpl.ENTITY_CACHE_ENABLED,
			CounselingModelImpl.FINDER_CACHE_ENABLED, CounselingImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByCounseling",
			new String[] { Long.class.getName() },
			CounselingModelImpl.COUNSELINGID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COUNSELING = new FinderPath(CounselingModelImpl.ENTITY_CACHE_ENABLED,
			CounselingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCounseling",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USER = new FinderPath(CounselingModelImpl.ENTITY_CACHE_ENABLED,
			CounselingModelImpl.FINDER_CACHE_ENABLED, CounselingImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUser",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER = new FinderPath(CounselingModelImpl.ENTITY_CACHE_ENABLED,
			CounselingModelImpl.FINDER_CACHE_ENABLED, CounselingImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUser",
			new String[] { Long.class.getName() },
			CounselingModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USER = new FinderPath(CounselingModelImpl.ENTITY_CACHE_ENABLED,
			CounselingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUser",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CounselingModelImpl.ENTITY_CACHE_ENABLED,
			CounselingModelImpl.FINDER_CACHE_ENABLED, CounselingImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CounselingModelImpl.ENTITY_CACHE_ENABLED,
			CounselingModelImpl.FINDER_CACHE_ENABLED, CounselingImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CounselingModelImpl.ENTITY_CACHE_ENABLED,
			CounselingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the counseling in the entity cache if it is enabled.
	 *
	 * @param counseling the counseling
	 */
	public void cacheResult(Counseling counseling) {
		EntityCacheUtil.putResult(CounselingModelImpl.ENTITY_CACHE_ENABLED,
			CounselingImpl.class, counseling.getPrimaryKey(), counseling);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUNSELING,
			new Object[] { Long.valueOf(counseling.getCounselingId()) },
			counseling);

		counseling.resetOriginalValues();
	}

	/**
	 * Caches the counselings in the entity cache if it is enabled.
	 *
	 * @param counselings the counselings
	 */
	public void cacheResult(List<Counseling> counselings) {
		for (Counseling counseling : counselings) {
			if (EntityCacheUtil.getResult(
						CounselingModelImpl.ENTITY_CACHE_ENABLED,
						CounselingImpl.class, counseling.getPrimaryKey()) == null) {
				cacheResult(counseling);
			}
			else {
				counseling.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all counselings.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CounselingImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CounselingImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the counseling.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Counseling counseling) {
		EntityCacheUtil.removeResult(CounselingModelImpl.ENTITY_CACHE_ENABLED,
			CounselingImpl.class, counseling.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(counseling);
	}

	@Override
	public void clearCache(List<Counseling> counselings) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Counseling counseling : counselings) {
			EntityCacheUtil.removeResult(CounselingModelImpl.ENTITY_CACHE_ENABLED,
				CounselingImpl.class, counseling.getPrimaryKey());

			clearUniqueFindersCache(counseling);
		}
	}

	protected void clearUniqueFindersCache(Counseling counseling) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_COUNSELING,
			new Object[] { Long.valueOf(counseling.getCounselingId()) });
	}

	/**
	 * Creates a new counseling with the primary key. Does not add the counseling to the database.
	 *
	 * @param counselingId the primary key for the new counseling
	 * @return the new counseling
	 */
	public Counseling create(long counselingId) {
		Counseling counseling = new CounselingImpl();

		counseling.setNew(true);
		counseling.setPrimaryKey(counselingId);

		return counseling;
	}

	/**
	 * Removes the counseling with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param counselingId the primary key of the counseling
	 * @return the counseling that was removed
	 * @throws info.diit.portal.counseling.NoSuchCounselingException if a counseling with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Counseling remove(long counselingId)
		throws NoSuchCounselingException, SystemException {
		return remove(Long.valueOf(counselingId));
	}

	/**
	 * Removes the counseling with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the counseling
	 * @return the counseling that was removed
	 * @throws info.diit.portal.counseling.NoSuchCounselingException if a counseling with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Counseling remove(Serializable primaryKey)
		throws NoSuchCounselingException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Counseling counseling = (Counseling)session.get(CounselingImpl.class,
					primaryKey);

			if (counseling == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCounselingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(counseling);
		}
		catch (NoSuchCounselingException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Counseling removeImpl(Counseling counseling)
		throws SystemException {
		counseling = toUnwrappedModel(counseling);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, counseling);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(counseling);

		return counseling;
	}

	@Override
	public Counseling updateImpl(
		info.diit.portal.counseling.model.Counseling counseling, boolean merge)
		throws SystemException {
		counseling = toUnwrappedModel(counseling);

		boolean isNew = counseling.isNew();

		CounselingModelImpl counselingModelImpl = (CounselingModelImpl)counseling;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, counseling, merge);

			counseling.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CounselingModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((counselingModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(counselingModelImpl.getOriginalUserId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USER, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER,
					args);

				args = new Object[] {
						Long.valueOf(counselingModelImpl.getUserId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USER, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER,
					args);
			}
		}

		EntityCacheUtil.putResult(CounselingModelImpl.ENTITY_CACHE_ENABLED,
			CounselingImpl.class, counseling.getPrimaryKey(), counseling);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUNSELING,
				new Object[] { Long.valueOf(counseling.getCounselingId()) },
				counseling);
		}
		else {
			if ((counselingModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_COUNSELING.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(counselingModelImpl.getOriginalCounselingId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COUNSELING,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_COUNSELING,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUNSELING,
					new Object[] { Long.valueOf(counseling.getCounselingId()) },
					counseling);
			}
		}

		return counseling;
	}

	protected Counseling toUnwrappedModel(Counseling counseling) {
		if (counseling instanceof CounselingImpl) {
			return counseling;
		}

		CounselingImpl counselingImpl = new CounselingImpl();

		counselingImpl.setNew(counseling.isNew());
		counselingImpl.setPrimaryKey(counseling.getPrimaryKey());

		counselingImpl.setCounselingId(counseling.getCounselingId());
		counselingImpl.setCompanyId(counseling.getCompanyId());
		counselingImpl.setUserId(counseling.getUserId());
		counselingImpl.setUserName(counseling.getUserName());
		counselingImpl.setCreateDate(counseling.getCreateDate());
		counselingImpl.setModifiedDate(counseling.getModifiedDate());
		counselingImpl.setCounselingDate(counseling.getCounselingDate());
		counselingImpl.setMedium(counseling.getMedium());
		counselingImpl.setName(counseling.getName());
		counselingImpl.setMaximumQualifications(counseling.getMaximumQualifications());
		counselingImpl.setMobile(counseling.getMobile());
		counselingImpl.setEmail(counseling.getEmail());
		counselingImpl.setAddress(counseling.getAddress());
		counselingImpl.setNote(counseling.getNote());
		counselingImpl.setSource(counseling.getSource());
		counselingImpl.setSourceNote(counseling.getSourceNote());
		counselingImpl.setStatus(counseling.getStatus());
		counselingImpl.setStatusNote(counseling.getStatusNote());
		counselingImpl.setMajorInterestCourse(counseling.getMajorInterestCourse());

		return counselingImpl;
	}

	/**
	 * Returns the counseling with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the counseling
	 * @return the counseling
	 * @throws com.liferay.portal.NoSuchModelException if a counseling with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Counseling findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the counseling with the primary key or throws a {@link info.diit.portal.counseling.NoSuchCounselingException} if it could not be found.
	 *
	 * @param counselingId the primary key of the counseling
	 * @return the counseling
	 * @throws info.diit.portal.counseling.NoSuchCounselingException if a counseling with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Counseling findByPrimaryKey(long counselingId)
		throws NoSuchCounselingException, SystemException {
		Counseling counseling = fetchByPrimaryKey(counselingId);

		if (counseling == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + counselingId);
			}

			throw new NoSuchCounselingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				counselingId);
		}

		return counseling;
	}

	/**
	 * Returns the counseling with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the counseling
	 * @return the counseling, or <code>null</code> if a counseling with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Counseling fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the counseling with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param counselingId the primary key of the counseling
	 * @return the counseling, or <code>null</code> if a counseling with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Counseling fetchByPrimaryKey(long counselingId)
		throws SystemException {
		Counseling counseling = (Counseling)EntityCacheUtil.getResult(CounselingModelImpl.ENTITY_CACHE_ENABLED,
				CounselingImpl.class, counselingId);

		if (counseling == _nullCounseling) {
			return null;
		}

		if (counseling == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				counseling = (Counseling)session.get(CounselingImpl.class,
						Long.valueOf(counselingId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (counseling != null) {
					cacheResult(counseling);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(CounselingModelImpl.ENTITY_CACHE_ENABLED,
						CounselingImpl.class, counselingId, _nullCounseling);
				}

				closeSession(session);
			}
		}

		return counseling;
	}

	/**
	 * Returns the counseling where counselingId = &#63; or throws a {@link info.diit.portal.counseling.NoSuchCounselingException} if it could not be found.
	 *
	 * @param counselingId the counseling ID
	 * @return the matching counseling
	 * @throws info.diit.portal.counseling.NoSuchCounselingException if a matching counseling could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Counseling findByCounseling(long counselingId)
		throws NoSuchCounselingException, SystemException {
		Counseling counseling = fetchByCounseling(counselingId);

		if (counseling == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("counselingId=");
			msg.append(counselingId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCounselingException(msg.toString());
		}

		return counseling;
	}

	/**
	 * Returns the counseling where counselingId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param counselingId the counseling ID
	 * @return the matching counseling, or <code>null</code> if a matching counseling could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Counseling fetchByCounseling(long counselingId)
		throws SystemException {
		return fetchByCounseling(counselingId, true);
	}

	/**
	 * Returns the counseling where counselingId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param counselingId the counseling ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching counseling, or <code>null</code> if a matching counseling could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Counseling fetchByCounseling(long counselingId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { counselingId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_COUNSELING,
					finderArgs, this);
		}

		if (result instanceof Counseling) {
			Counseling counseling = (Counseling)result;

			if ((counselingId != counseling.getCounselingId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_COUNSELING_WHERE);

			query.append(_FINDER_COLUMN_COUNSELING_COUNSELINGID_2);

			query.append(CounselingModelImpl.ORDER_BY_JPQL);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(counselingId);

				List<Counseling> list = q.list();

				result = list;

				Counseling counseling = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUNSELING,
						finderArgs, list);
				}
				else {
					counseling = list.get(0);

					cacheResult(counseling);

					if ((counseling.getCounselingId() != counselingId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUNSELING,
							finderArgs, counseling);
					}
				}

				return counseling;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_COUNSELING,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (Counseling)result;
			}
		}
	}

	/**
	 * Returns all the counselings where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching counselings
	 * @throws SystemException if a system exception occurred
	 */
	public List<Counseling> findByUser(long userId) throws SystemException {
		return findByUser(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the counselings where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of counselings
	 * @param end the upper bound of the range of counselings (not inclusive)
	 * @return the range of matching counselings
	 * @throws SystemException if a system exception occurred
	 */
	public List<Counseling> findByUser(long userId, int start, int end)
		throws SystemException {
		return findByUser(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the counselings where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of counselings
	 * @param end the upper bound of the range of counselings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching counselings
	 * @throws SystemException if a system exception occurred
	 */
	public List<Counseling> findByUser(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USER;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USER;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<Counseling> list = (List<Counseling>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Counseling counseling : list) {
				if ((userId != counseling.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COUNSELING_WHERE);

			query.append(_FINDER_COLUMN_USER_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CounselingModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				list = (List<Counseling>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first counseling in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching counseling
	 * @throws info.diit.portal.counseling.NoSuchCounselingException if a matching counseling could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Counseling findByUser_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCounselingException, SystemException {
		Counseling counseling = fetchByUser_First(userId, orderByComparator);

		if (counseling != null) {
			return counseling;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCounselingException(msg.toString());
	}

	/**
	 * Returns the first counseling in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching counseling, or <code>null</code> if a matching counseling could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Counseling fetchByUser_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Counseling> list = findByUser(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last counseling in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching counseling
	 * @throws info.diit.portal.counseling.NoSuchCounselingException if a matching counseling could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Counseling findByUser_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCounselingException, SystemException {
		Counseling counseling = fetchByUser_Last(userId, orderByComparator);

		if (counseling != null) {
			return counseling;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCounselingException(msg.toString());
	}

	/**
	 * Returns the last counseling in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching counseling, or <code>null</code> if a matching counseling could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Counseling fetchByUser_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUser(userId);

		List<Counseling> list = findByUser(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the counselings before and after the current counseling in the ordered set where userId = &#63;.
	 *
	 * @param counselingId the primary key of the current counseling
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next counseling
	 * @throws info.diit.portal.counseling.NoSuchCounselingException if a counseling with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Counseling[] findByUser_PrevAndNext(long counselingId, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCounselingException, SystemException {
		Counseling counseling = findByPrimaryKey(counselingId);

		Session session = null;

		try {
			session = openSession();

			Counseling[] array = new CounselingImpl[3];

			array[0] = getByUser_PrevAndNext(session, counseling, userId,
					orderByComparator, true);

			array[1] = counseling;

			array[2] = getByUser_PrevAndNext(session, counseling, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Counseling getByUser_PrevAndNext(Session session,
		Counseling counseling, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COUNSELING_WHERE);

		query.append(_FINDER_COLUMN_USER_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CounselingModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(counseling);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Counseling> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the counselings.
	 *
	 * @return the counselings
	 * @throws SystemException if a system exception occurred
	 */
	public List<Counseling> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the counselings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of counselings
	 * @param end the upper bound of the range of counselings (not inclusive)
	 * @return the range of counselings
	 * @throws SystemException if a system exception occurred
	 */
	public List<Counseling> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the counselings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of counselings
	 * @param end the upper bound of the range of counselings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of counselings
	 * @throws SystemException if a system exception occurred
	 */
	public List<Counseling> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Counseling> list = (List<Counseling>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_COUNSELING);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COUNSELING.concat(CounselingModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Counseling>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Counseling>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes the counseling where counselingId = &#63; from the database.
	 *
	 * @param counselingId the counseling ID
	 * @return the counseling that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public Counseling removeByCounseling(long counselingId)
		throws NoSuchCounselingException, SystemException {
		Counseling counseling = findByCounseling(counselingId);

		return remove(counseling);
	}

	/**
	 * Removes all the counselings where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUser(long userId) throws SystemException {
		for (Counseling counseling : findByUser(userId)) {
			remove(counseling);
		}
	}

	/**
	 * Removes all the counselings from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Counseling counseling : findAll()) {
			remove(counseling);
		}
	}

	/**
	 * Returns the number of counselings where counselingId = &#63;.
	 *
	 * @param counselingId the counseling ID
	 * @return the number of matching counselings
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCounseling(long counselingId) throws SystemException {
		Object[] finderArgs = new Object[] { counselingId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COUNSELING,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COUNSELING_WHERE);

			query.append(_FINDER_COLUMN_COUNSELING_COUNSELINGID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(counselingId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COUNSELING,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of counselings where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching counselings
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUser(long userId) throws SystemException {
		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USER,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COUNSELING_WHERE);

			query.append(_FINDER_COLUMN_USER_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USER,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of counselings.
	 *
	 * @return the number of counselings
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COUNSELING);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns all the counseling course interests associated with the counseling.
	 *
	 * @param pk the primary key of the counseling
	 * @return the counseling course interests associated with the counseling
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.counseling.model.CounselingCourseInterest> getCounselingCourseInterests(
		long pk) throws SystemException {
		return getCounselingCourseInterests(pk, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the counseling course interests associated with the counseling.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the counseling
	 * @param start the lower bound of the range of counselings
	 * @param end the upper bound of the range of counselings (not inclusive)
	 * @return the range of counseling course interests associated with the counseling
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.counseling.model.CounselingCourseInterest> getCounselingCourseInterests(
		long pk, int start, int end) throws SystemException {
		return getCounselingCourseInterests(pk, start, end, null);
	}

	public static final FinderPath FINDER_PATH_GET_COUNSELINGCOURSEINTERESTS = new FinderPath(info.diit.portal.counseling.model.impl.CounselingCourseInterestModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.counseling.model.impl.CounselingCourseInterestModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.counseling.model.impl.CounselingCourseInterestImpl.class,
			info.diit.portal.counseling.service.persistence.CounselingCourseInterestPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getCounselingCourseInterests",
			new String[] {
				Long.class.getName(), "java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});

	static {
		FINDER_PATH_GET_COUNSELINGCOURSEINTERESTS.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns an ordered range of all the counseling course interests associated with the counseling.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the counseling
	 * @param start the lower bound of the range of counselings
	 * @param end the upper bound of the range of counselings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of counseling course interests associated with the counseling
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.counseling.model.CounselingCourseInterest> getCounselingCourseInterests(
		long pk, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, start, end, orderByComparator };

		List<info.diit.portal.counseling.model.CounselingCourseInterest> list = (List<info.diit.portal.counseling.model.CounselingCourseInterest>)FinderCacheUtil.getResult(FINDER_PATH_GET_COUNSELINGCOURSEINTERESTS,
				finderArgs, this);

		if (list == null) {
			Session session = null;

			try {
				session = openSession();

				String sql = null;

				if (orderByComparator != null) {
					sql = _SQL_GETCOUNSELINGCOURSEINTERESTS.concat(ORDER_BY_CLAUSE)
														   .concat(orderByComparator.getOrderBy());
				}
				else {
					sql = _SQL_GETCOUNSELINGCOURSEINTERESTS;
				}

				SQLQuery q = session.createSQLQuery(sql);

				q.addEntity("EduPortal_Counseling_CounselingCourseInterest",
					info.diit.portal.counseling.model.impl.CounselingCourseInterestImpl.class);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				list = (List<info.diit.portal.counseling.model.CounselingCourseInterest>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_GET_COUNSELINGCOURSEINTERESTS,
						finderArgs);
				}
				else {
					counselingCourseInterestPersistence.cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_GET_COUNSELINGCOURSEINTERESTS,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	public static final FinderPath FINDER_PATH_GET_COUNSELINGCOURSEINTERESTS_SIZE =
		new FinderPath(info.diit.portal.counseling.model.impl.CounselingCourseInterestModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.counseling.model.impl.CounselingCourseInterestModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.counseling.model.impl.CounselingCourseInterestImpl.class,
			info.diit.portal.counseling.service.persistence.CounselingCourseInterestPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getCounselingCourseInterestsSize",
			new String[] { Long.class.getName() });

	static {
		FINDER_PATH_GET_COUNSELINGCOURSEINTERESTS_SIZE.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns the number of counseling course interests associated with the counseling.
	 *
	 * @param pk the primary key of the counseling
	 * @return the number of counseling course interests associated with the counseling
	 * @throws SystemException if a system exception occurred
	 */
	public int getCounselingCourseInterestsSize(long pk)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_GET_COUNSELINGCOURSEINTERESTS_SIZE,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				SQLQuery q = session.createSQLQuery(_SQL_GETCOUNSELINGCOURSEINTERESTSSIZE);

				q.addScalar(COUNT_COLUMN_NAME,
					com.liferay.portal.kernel.dao.orm.Type.LONG);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_GET_COUNSELINGCOURSEINTERESTS_SIZE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	public static final FinderPath FINDER_PATH_CONTAINS_COUNSELINGCOURSEINTEREST =
		new FinderPath(info.diit.portal.counseling.model.impl.CounselingCourseInterestModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.counseling.model.impl.CounselingCourseInterestModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.counseling.model.impl.CounselingCourseInterestImpl.class,
			info.diit.portal.counseling.service.persistence.CounselingCourseInterestPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"containsCounselingCourseInterest",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns <code>true</code> if the counseling course interest is associated with the counseling.
	 *
	 * @param pk the primary key of the counseling
	 * @param counselingCourseInterestPK the primary key of the counseling course interest
	 * @return <code>true</code> if the counseling course interest is associated with the counseling; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsCounselingCourseInterest(long pk,
		long counselingCourseInterestPK) throws SystemException {
		Object[] finderArgs = new Object[] { pk, counselingCourseInterestPK };

		Boolean value = (Boolean)FinderCacheUtil.getResult(FINDER_PATH_CONTAINS_COUNSELINGCOURSEINTEREST,
				finderArgs, this);

		if (value == null) {
			try {
				value = Boolean.valueOf(containsCounselingCourseInterest.contains(
							pk, counselingCourseInterestPK));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (value == null) {
					value = Boolean.FALSE;
				}

				FinderCacheUtil.putResult(FINDER_PATH_CONTAINS_COUNSELINGCOURSEINTEREST,
					finderArgs, value);
			}
		}

		return value.booleanValue();
	}

	/**
	 * Returns <code>true</code> if the counseling has any counseling course interests associated with it.
	 *
	 * @param pk the primary key of the counseling to check for associations with counseling course interests
	 * @return <code>true</code> if the counseling has any counseling course interests associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsCounselingCourseInterests(long pk)
		throws SystemException {
		if (getCounselingCourseInterestsSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Initializes the counseling persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.counseling.model.Counseling")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Counseling>> listenersList = new ArrayList<ModelListener<Counseling>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Counseling>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}

		containsCounselingCourseInterest = new ContainsCounselingCourseInterest();
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CounselingImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	protected ContainsCounselingCourseInterest containsCounselingCourseInterest;

	protected class ContainsCounselingCourseInterest {
		protected ContainsCounselingCourseInterest() {
			_mappingSqlQuery = MappingSqlQueryFactoryUtil.getMappingSqlQuery(getDataSource(),
					_SQL_CONTAINSCOUNSELINGCOURSEINTEREST,
					new int[] { java.sql.Types.BIGINT, java.sql.Types.BIGINT },
					RowMapper.COUNT);
		}

		protected boolean contains(long counselingId,
			long CounselingCourseInterestId) {
			List<Integer> results = _mappingSqlQuery.execute(new Object[] {
						new Long(counselingId),
						new Long(CounselingCourseInterestId)
					});

			if (results.size() > 0) {
				Integer count = results.get(0);

				if (count.intValue() > 0) {
					return true;
				}
			}

			return false;
		}

		private MappingSqlQuery<Integer> _mappingSqlQuery;
	}

	private static final String _SQL_SELECT_COUNSELING = "SELECT counseling FROM Counseling counseling";
	private static final String _SQL_SELECT_COUNSELING_WHERE = "SELECT counseling FROM Counseling counseling WHERE ";
	private static final String _SQL_COUNT_COUNSELING = "SELECT COUNT(counseling) FROM Counseling counseling";
	private static final String _SQL_COUNT_COUNSELING_WHERE = "SELECT COUNT(counseling) FROM Counseling counseling WHERE ";
	private static final String _SQL_GETCOUNSELINGCOURSEINTERESTS = "SELECT {EduPortal_Counseling_CounselingCourseInterest.*} FROM EduPortal_Counseling_CounselingCourseInterest INNER JOIN EduPortal_Counseling_Counseling ON (EduPortal_Counseling_Counseling.counselingId = EduPortal_Counseling_CounselingCourseInterest.counselingId) WHERE (EduPortal_Counseling_Counseling.counselingId = ?)";
	private static final String _SQL_GETCOUNSELINGCOURSEINTERESTSSIZE = "SELECT COUNT(*) AS COUNT_VALUE FROM EduPortal_Counseling_CounselingCourseInterest WHERE counselingId = ?";
	private static final String _SQL_CONTAINSCOUNSELINGCOURSEINTEREST = "SELECT COUNT(*) AS COUNT_VALUE FROM EduPortal_Counseling_CounselingCourseInterest WHERE counselingId = ? AND CounselingCourseInterestId = ?";
	private static final String _FINDER_COLUMN_COUNSELING_COUNSELINGID_2 = "counseling.counselingId = ?";
	private static final String _FINDER_COLUMN_USER_USERID_2 = "counseling.userId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "counseling.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Counseling exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Counseling exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CounselingPersistenceImpl.class);
	private static Counseling _nullCounseling = new CounselingImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Counseling> toCacheModel() {
				return _nullCounselingCacheModel;
			}
		};

	private static CacheModel<Counseling> _nullCounselingCacheModel = new CacheModel<Counseling>() {
			public Counseling toEntityModel() {
				return _nullCounseling;
			}
		};
}
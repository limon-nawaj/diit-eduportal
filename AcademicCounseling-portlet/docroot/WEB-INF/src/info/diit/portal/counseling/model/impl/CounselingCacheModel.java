/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.counseling.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.counseling.model.Counseling;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Counseling in entity cache.
 *
 * @author shamsuddin
 * @see Counseling
 * @generated
 */
public class CounselingCacheModel implements CacheModel<Counseling>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(39);

		sb.append("{counselingId=");
		sb.append(counselingId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", counselingDate=");
		sb.append(counselingDate);
		sb.append(", medium=");
		sb.append(medium);
		sb.append(", name=");
		sb.append(name);
		sb.append(", maximumQualifications=");
		sb.append(maximumQualifications);
		sb.append(", mobile=");
		sb.append(mobile);
		sb.append(", email=");
		sb.append(email);
		sb.append(", address=");
		sb.append(address);
		sb.append(", note=");
		sb.append(note);
		sb.append(", source=");
		sb.append(source);
		sb.append(", sourceNote=");
		sb.append(sourceNote);
		sb.append(", status=");
		sb.append(status);
		sb.append(", statusNote=");
		sb.append(statusNote);
		sb.append(", majorInterestCourse=");
		sb.append(majorInterestCourse);
		sb.append("}");

		return sb.toString();
	}

	public Counseling toEntityModel() {
		CounselingImpl counselingImpl = new CounselingImpl();

		counselingImpl.setCounselingId(counselingId);
		counselingImpl.setCompanyId(companyId);
		counselingImpl.setUserId(userId);

		if (userName == null) {
			counselingImpl.setUserName(StringPool.BLANK);
		}
		else {
			counselingImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			counselingImpl.setCreateDate(null);
		}
		else {
			counselingImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			counselingImpl.setModifiedDate(null);
		}
		else {
			counselingImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (counselingDate == Long.MIN_VALUE) {
			counselingImpl.setCounselingDate(null);
		}
		else {
			counselingImpl.setCounselingDate(new Date(counselingDate));
		}

		counselingImpl.setMedium(medium);

		if (name == null) {
			counselingImpl.setName(StringPool.BLANK);
		}
		else {
			counselingImpl.setName(name);
		}

		if (maximumQualifications == null) {
			counselingImpl.setMaximumQualifications(StringPool.BLANK);
		}
		else {
			counselingImpl.setMaximumQualifications(maximumQualifications);
		}

		if (mobile == null) {
			counselingImpl.setMobile(StringPool.BLANK);
		}
		else {
			counselingImpl.setMobile(mobile);
		}

		if (email == null) {
			counselingImpl.setEmail(StringPool.BLANK);
		}
		else {
			counselingImpl.setEmail(email);
		}

		if (address == null) {
			counselingImpl.setAddress(StringPool.BLANK);
		}
		else {
			counselingImpl.setAddress(address);
		}

		if (note == null) {
			counselingImpl.setNote(StringPool.BLANK);
		}
		else {
			counselingImpl.setNote(note);
		}

		counselingImpl.setSource(source);

		if (sourceNote == null) {
			counselingImpl.setSourceNote(StringPool.BLANK);
		}
		else {
			counselingImpl.setSourceNote(sourceNote);
		}

		counselingImpl.setStatus(status);

		if (statusNote == null) {
			counselingImpl.setStatusNote(StringPool.BLANK);
		}
		else {
			counselingImpl.setStatusNote(statusNote);
		}

		counselingImpl.setMajorInterestCourse(majorInterestCourse);

		counselingImpl.resetOriginalValues();

		return counselingImpl;
	}

	public long counselingId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long counselingDate;
	public int medium;
	public String name;
	public String maximumQualifications;
	public String mobile;
	public String email;
	public String address;
	public String note;
	public int source;
	public String sourceNote;
	public int status;
	public String statusNote;
	public long majorInterestCourse;
}
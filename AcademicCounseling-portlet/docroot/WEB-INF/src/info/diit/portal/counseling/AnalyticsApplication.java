package info.diit.portal.counseling;

import info.diit.portal.counseling.dao.CounselingDao;
import info.diit.portal.counseling.dto.AnalyticsDto;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;

public class AnalyticsApplication extends Application implements PortletRequestListener{

	private final String COLUMN_LABEL = "label";
	private final String COLUMN_COUNT = "count";
	private final String COLUMN_PERCENTAGE = "percentage";
	
	private static DecimalFormat twoDecimalFormat = new DecimalFormat("0.00");
	
	private ThemeDisplay themeDisplay;
	
	private Window window;
	
	private DateField startDateField;
	private DateField endDateField;
	
	private BeanItemContainer<AnalyticsDto> mediumBeanItemContainer;
	private Table mediumAnalyticsTable;
	
	private BeanItemContainer<AnalyticsDto> courseInterestBeanItemContainer;
	private Table courseAnalyticsTable;
	

	private BeanItemContainer<AnalyticsDto> sourceBeanItemContainer;
	private Table sourceAnalyticsTable;

	private BeanItemContainer<AnalyticsDto> statusBeanItemContainer;
	private Table statusAnalyticsTable;
	
	
    public void init() {
    	window = new Window();
		window.setSizeFull();
		setMainWindow(window);
		

		Date today = new Date();
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MONTH,-3);
		Date threeMonthBefore = now.getTime();

		startDateField = new DateField("Start");
		startDateField.setDateFormat("dd/MM/yyyy");
		startDateField.setResolution(DateField.RESOLUTION_DAY);
		startDateField.setValue(threeMonthBefore);
		startDateField.setImmediate(true);
		
		endDateField = new DateField("End");
		endDateField.setDateFormat("dd/MM/yyyy");
		endDateField.setResolution(DateField.RESOLUTION_DAY);
		endDateField.setValue(today);
		endDateField.setImmediate(true);
		
		startDateField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				showAnalyticsData();
				
			}
		});
		
		endDateField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				showAnalyticsData();
				
			}
		});

		
		//Media Analytics
		
		mediumBeanItemContainer = new BeanItemContainer<AnalyticsDto>(AnalyticsDto.class);
		mediumAnalyticsTable = new Table("Medium Analytics",mediumBeanItemContainer)
		{
			@Override
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				
				if(property.getType()==Double.class)
				{
					return twoDecimalFormat.format(property.getValue());
				}
				
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		mediumAnalyticsTable.setSelectable(true);
		mediumAnalyticsTable.setImmediate(true);
		
		mediumAnalyticsTable.setColumnHeader(COLUMN_LABEL, "Medium");
		mediumAnalyticsTable.setColumnHeader(COLUMN_COUNT, "Count");
		mediumAnalyticsTable.setColumnHeader(COLUMN_PERCENTAGE, "%");
		
		mediumAnalyticsTable.setColumnAlignment(COLUMN_COUNT,Table.ALIGN_RIGHT);
		mediumAnalyticsTable.setColumnAlignment(COLUMN_PERCENTAGE,Table.ALIGN_RIGHT);
		
		mediumAnalyticsTable.setVisibleColumns(new String[]{COLUMN_LABEL,COLUMN_COUNT,COLUMN_PERCENTAGE});
		mediumAnalyticsTable.setPageLength(10);
		
		//Course interest analytics
		
		courseInterestBeanItemContainer = new BeanItemContainer<AnalyticsDto>(AnalyticsDto.class);
		courseAnalyticsTable = new Table("Course Interest Analytics",courseInterestBeanItemContainer)
		{
			@Override
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				
				if(property.getType()==Double.class)
				{
					return twoDecimalFormat.format(property.getValue());
				}
				
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		courseAnalyticsTable.setSelectable(true);
		courseAnalyticsTable.setImmediate(true);
		
		courseAnalyticsTable.setColumnHeader(COLUMN_LABEL, "Course");
		courseAnalyticsTable.setColumnHeader(COLUMN_COUNT, "Interest Count");
		courseAnalyticsTable.setColumnHeader(COLUMN_PERCENTAGE, "%");
		
		courseAnalyticsTable.setColumnAlignment(COLUMN_COUNT,Table.ALIGN_RIGHT);
		courseAnalyticsTable.setColumnAlignment(COLUMN_PERCENTAGE,Table.ALIGN_RIGHT);
		
		courseAnalyticsTable.setVisibleColumns(new String[]{COLUMN_LABEL,COLUMN_COUNT,COLUMN_PERCENTAGE});
		courseAnalyticsTable.setPageLength(10);
		
		
		//Source Analytics
		
		sourceBeanItemContainer = new BeanItemContainer<AnalyticsDto>(AnalyticsDto.class);
		sourceAnalyticsTable = new Table("Source Analytics",sourceBeanItemContainer)
		{
			@Override
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				
				if(property.getType()==Double.class)
				{
					return twoDecimalFormat.format(property.getValue());
				}
				
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		sourceAnalyticsTable.setSelectable(true);
		sourceAnalyticsTable.setImmediate(true);
		
		sourceAnalyticsTable.setColumnHeader(COLUMN_LABEL, "Source");
		sourceAnalyticsTable.setColumnHeader(COLUMN_COUNT, "Count");
		sourceAnalyticsTable.setColumnHeader(COLUMN_PERCENTAGE, "%");
		
		sourceAnalyticsTable.setColumnAlignment(COLUMN_COUNT,Table.ALIGN_RIGHT);
		sourceAnalyticsTable.setColumnAlignment(COLUMN_PERCENTAGE,Table.ALIGN_RIGHT);
		
		sourceAnalyticsTable.setVisibleColumns(new String[]{COLUMN_LABEL,COLUMN_COUNT,COLUMN_PERCENTAGE});
		sourceAnalyticsTable.setPageLength(5);


		//Status Analytics
		
		statusBeanItemContainer = new BeanItemContainer<AnalyticsDto>(AnalyticsDto.class);
		statusAnalyticsTable = new Table("Status Analytics",statusBeanItemContainer)
		{
			@Override
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				
				if(property.getType()==Double.class)
				{
					return twoDecimalFormat.format(property.getValue());
				}
				
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		statusAnalyticsTable.setSelectable(true);
		statusAnalyticsTable.setImmediate(true);
		
		statusAnalyticsTable.setColumnHeader(COLUMN_LABEL, "Status");
		statusAnalyticsTable.setColumnHeader(COLUMN_COUNT, "Count");
		statusAnalyticsTable.setColumnHeader(COLUMN_PERCENTAGE, "%");
		
		statusAnalyticsTable.setColumnAlignment(COLUMN_COUNT,Table.ALIGN_RIGHT);
		statusAnalyticsTable.setColumnAlignment(COLUMN_PERCENTAGE,Table.ALIGN_RIGHT);
		
		statusAnalyticsTable.setVisibleColumns(new String[]{COLUMN_LABEL,COLUMN_COUNT,COLUMN_PERCENTAGE});
		statusAnalyticsTable.setPageLength(5);
		
		
		showAnalyticsData();
		
		GridLayout gridLayout = new GridLayout(8,3);
		
		
		gridLayout.setSizeFull();
		gridLayout.setSpacing(true);
		
		mediumAnalyticsTable.setWidth("100%");
		courseAnalyticsTable.setWidth("100%");
		sourceAnalyticsTable.setWidth("100%");
		statusAnalyticsTable.setWidth("100%");
		
		gridLayout.addComponent(startDateField,0,0);
		gridLayout.addComponent(endDateField,1,0);
		gridLayout.addComponent(mediumAnalyticsTable,0,1);
		gridLayout.addComponent(courseAnalyticsTable,1,1,7,1);
		gridLayout.addComponent(sourceAnalyticsTable,0,2);
		gridLayout.addComponent(statusAnalyticsTable,1,2);
		
		
		
		window.addComponent(gridLayout);
    }
    
    @Override
    public void onRequestStart(PortletRequest p_request, PortletResponse p_response) {
        themeDisplay = (ThemeDisplay) p_request.getAttribute(WebKeys.THEME_DISPLAY);
        
    }
	
	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		// TODO Auto-generated method stub
		
	}
	
	public void showAnalyticsData()
	{
		showMediumAnalyticsData();
		showCourseAnalyticsData();
		showSourceAnalyticsData();
		showStatusAnalyticsData();
		
	}

	public void showMediumAnalyticsData()
	{
		long companyId = themeDisplay.getCompanyId();
	
		Date startDate = (Date) startDateField.getValue();
		Date endDate = (Date) endDateField.getValue();
		
		startDate.setHours(0);
		startDate.setMinutes(0);
		startDate.setSeconds(0);

		
		if(mediumBeanItemContainer!=null)
		{
			mediumBeanItemContainer.removeAllItems();
			
			try {
				mediumBeanItemContainer.addAll(CounselingDao.getMediumAnalytics(companyId, startDate, endDate));
				mediumBeanItemContainer.sort(new String[]{COLUMN_COUNT}, new boolean[]{false});
				mediumAnalyticsTable.refreshRowCache();
			} catch (SystemException e) {
				
				e.printStackTrace();
			}
		}
	}
	
	public void showCourseAnalyticsData()
	{
		long companyId = themeDisplay.getCompanyId();
	
		Date startDate = (Date) startDateField.getValue();
		Date endDate = (Date) endDateField.getValue();
		
		startDate.setHours(0);
		startDate.setMinutes(0);
		startDate.setSeconds(0);

		
		if(courseInterestBeanItemContainer!=null)
		{
			courseInterestBeanItemContainer.removeAllItems();
			
			try {
				courseInterestBeanItemContainer.addAll(CounselingDao.getCourseAnalytics(companyId, startDate, endDate));
				courseInterestBeanItemContainer.sort(new String[]{COLUMN_COUNT}, new boolean[]{false});
				courseAnalyticsTable.refreshRowCache();
			} catch (SystemException e) {
				
				e.printStackTrace();
			}
		}
	}

	public void showSourceAnalyticsData()
	{
		long companyId = themeDisplay.getCompanyId();
	
		Date startDate = (Date) startDateField.getValue();
		Date endDate = (Date) endDateField.getValue();
		
		startDate.setHours(0);
		startDate.setMinutes(0);
		startDate.setSeconds(0);

		
		if(sourceBeanItemContainer!=null)
		{
			sourceBeanItemContainer.removeAllItems();
			
			try {
				sourceBeanItemContainer.addAll(CounselingDao.getSourceAnalytics(companyId, startDate, endDate));
				sourceBeanItemContainer.sort(new String[]{COLUMN_COUNT}, new boolean[]{false});
				sourceAnalyticsTable.refreshRowCache();
			} catch (SystemException e) {
				
				e.printStackTrace();
			}
		}
	}
	
	public void showStatusAnalyticsData()
	{
		long companyId = themeDisplay.getCompanyId();
	
		Date startDate = (Date) startDateField.getValue();
		Date endDate = (Date) endDateField.getValue();
		
		startDate.setHours(0);
		startDate.setMinutes(0);
		startDate.setSeconds(0);

		
		if(statusBeanItemContainer!=null)
		{
			statusBeanItemContainer.removeAllItems();
			
			try {
				statusBeanItemContainer.addAll(CounselingDao.getStatusAnalytics(companyId, startDate, endDate));
				statusBeanItemContainer.sort(new String[]{COLUMN_COUNT}, new boolean[]{false});
				statusAnalyticsTable.refreshRowCache();
			} catch (SystemException e) {
				
				e.printStackTrace();
			}
		}
	}

}

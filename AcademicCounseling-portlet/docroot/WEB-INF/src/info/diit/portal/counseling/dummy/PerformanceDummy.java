package info.diit.portal.counseling.dummy;

import java.io.Serializable;

public class PerformanceDummy implements Serializable{

	private String counselor;
	private Integer admission;
	private Integer reference;
	private Integer physicalCounseling;
	private Integer phoneCounseling;
	private Double successRate;
	public String getCounselor() {
		return counselor;
	}
	public void setCounselor(String counselor) {
		this.counselor = counselor;
	}
	public Integer getAdmission() {
		return admission;
	}
	public void setAdmission(Integer admission) {
		this.admission = admission;
	}
	public Integer getReference() {
		return reference;
	}
	public void setReference(Integer reference) {
		this.reference = reference;
	}
	
	public Integer getPhysicalCounseling() {
		return physicalCounseling;
	}
	public void setPhysicalCounseling(Integer physicalCounseling) {
		this.physicalCounseling = physicalCounseling;
	}
	public Integer getPhoneCounseling() {
		return phoneCounseling;
	}
	public void setPhoneCounseling(Integer phoneCounseling) {
		this.phoneCounseling = phoneCounseling;
	}
	public Double getSuccessRate() {
		
		return new Double(getAdmission())/(getPhoneCounseling()+getPhysicalCounseling())*100;
		
	}
	public void setSuccessRate(Double successRate) {
		this.successRate = successRate;
	}
	
	
}

package VaadinTestProject;

import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class VaadinTestProjectApplication extends UI {

	@Override
	public void init(VaadinRequest request) {
		VerticalLayout view = new VerticalLayout();
        view.addComponent(new Label("Hello Vaadin! with 7"));
        setContent(view);
	}

}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.coursesubject.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CourseSession}.
 * </p>
 *
 * @author    limon
 * @see       CourseSession
 * @generated
 */
public class CourseSessionWrapper implements CourseSession,
	ModelWrapper<CourseSession> {
	public CourseSessionWrapper(CourseSession courseSession) {
		_courseSession = courseSession;
	}

	public Class<?> getModelClass() {
		return CourseSession.class;
	}

	public String getModelClassName() {
		return CourseSession.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("sessionId", getSessionId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("sessionName", getSessionName());
		attributes.put("courseId", getCourseId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long sessionId = (Long)attributes.get("sessionId");

		if (sessionId != null) {
			setSessionId(sessionId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String sessionName = (String)attributes.get("sessionName");

		if (sessionName != null) {
			setSessionName(sessionName);
		}

		Long courseId = (Long)attributes.get("courseId");

		if (courseId != null) {
			setCourseId(courseId);
		}
	}

	/**
	* Returns the primary key of this course session.
	*
	* @return the primary key of this course session
	*/
	public long getPrimaryKey() {
		return _courseSession.getPrimaryKey();
	}

	/**
	* Sets the primary key of this course session.
	*
	* @param primaryKey the primary key of this course session
	*/
	public void setPrimaryKey(long primaryKey) {
		_courseSession.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the session ID of this course session.
	*
	* @return the session ID of this course session
	*/
	public long getSessionId() {
		return _courseSession.getSessionId();
	}

	/**
	* Sets the session ID of this course session.
	*
	* @param sessionId the session ID of this course session
	*/
	public void setSessionId(long sessionId) {
		_courseSession.setSessionId(sessionId);
	}

	/**
	* Returns the company ID of this course session.
	*
	* @return the company ID of this course session
	*/
	public long getCompanyId() {
		return _courseSession.getCompanyId();
	}

	/**
	* Sets the company ID of this course session.
	*
	* @param companyId the company ID of this course session
	*/
	public void setCompanyId(long companyId) {
		_courseSession.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this course session.
	*
	* @return the organization ID of this course session
	*/
	public long getOrganizationId() {
		return _courseSession.getOrganizationId();
	}

	/**
	* Sets the organization ID of this course session.
	*
	* @param organizationId the organization ID of this course session
	*/
	public void setOrganizationId(long organizationId) {
		_courseSession.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this course session.
	*
	* @return the user ID of this course session
	*/
	public long getUserId() {
		return _courseSession.getUserId();
	}

	/**
	* Sets the user ID of this course session.
	*
	* @param userId the user ID of this course session
	*/
	public void setUserId(long userId) {
		_courseSession.setUserId(userId);
	}

	/**
	* Returns the user uuid of this course session.
	*
	* @return the user uuid of this course session
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSession.getUserUuid();
	}

	/**
	* Sets the user uuid of this course session.
	*
	* @param userUuid the user uuid of this course session
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_courseSession.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this course session.
	*
	* @return the user name of this course session
	*/
	public java.lang.String getUserName() {
		return _courseSession.getUserName();
	}

	/**
	* Sets the user name of this course session.
	*
	* @param userName the user name of this course session
	*/
	public void setUserName(java.lang.String userName) {
		_courseSession.setUserName(userName);
	}

	/**
	* Returns the create date of this course session.
	*
	* @return the create date of this course session
	*/
	public java.util.Date getCreateDate() {
		return _courseSession.getCreateDate();
	}

	/**
	* Sets the create date of this course session.
	*
	* @param createDate the create date of this course session
	*/
	public void setCreateDate(java.util.Date createDate) {
		_courseSession.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this course session.
	*
	* @return the modified date of this course session
	*/
	public java.util.Date getModifiedDate() {
		return _courseSession.getModifiedDate();
	}

	/**
	* Sets the modified date of this course session.
	*
	* @param modifiedDate the modified date of this course session
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_courseSession.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the session name of this course session.
	*
	* @return the session name of this course session
	*/
	public java.lang.String getSessionName() {
		return _courseSession.getSessionName();
	}

	/**
	* Sets the session name of this course session.
	*
	* @param sessionName the session name of this course session
	*/
	public void setSessionName(java.lang.String sessionName) {
		_courseSession.setSessionName(sessionName);
	}

	/**
	* Returns the course ID of this course session.
	*
	* @return the course ID of this course session
	*/
	public long getCourseId() {
		return _courseSession.getCourseId();
	}

	/**
	* Sets the course ID of this course session.
	*
	* @param courseId the course ID of this course session
	*/
	public void setCourseId(long courseId) {
		_courseSession.setCourseId(courseId);
	}

	public boolean isNew() {
		return _courseSession.isNew();
	}

	public void setNew(boolean n) {
		_courseSession.setNew(n);
	}

	public boolean isCachedModel() {
		return _courseSession.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_courseSession.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _courseSession.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _courseSession.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_courseSession.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _courseSession.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_courseSession.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CourseSessionWrapper((CourseSession)_courseSession.clone());
	}

	public int compareTo(
		info.diit.portal.coursesubject.model.CourseSession courseSession) {
		return _courseSession.compareTo(courseSession);
	}

	@Override
	public int hashCode() {
		return _courseSession.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.coursesubject.model.CourseSession> toCacheModel() {
		return _courseSession.toCacheModel();
	}

	public info.diit.portal.coursesubject.model.CourseSession toEscapedModel() {
		return new CourseSessionWrapper(_courseSession.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _courseSession.toString();
	}

	public java.lang.String toXmlString() {
		return _courseSession.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_courseSession.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public CourseSession getWrappedCourseSession() {
		return _courseSession;
	}

	public CourseSession getWrappedModel() {
		return _courseSession;
	}

	public void resetOriginalValues() {
		_courseSession.resetOriginalValues();
	}

	private CourseSession _courseSession;
}
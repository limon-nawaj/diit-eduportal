/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.coursesubject.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.coursesubject.model.CourseSession;

import java.util.List;

/**
 * The persistence utility for the course session service. This utility wraps {@link CourseSessionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see CourseSessionPersistence
 * @see CourseSessionPersistenceImpl
 * @generated
 */
public class CourseSessionUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CourseSession courseSession) {
		getPersistence().clearCache(courseSession);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CourseSession> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CourseSession> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CourseSession> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static CourseSession update(CourseSession courseSession,
		boolean merge) throws SystemException {
		return getPersistence().update(courseSession, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static CourseSession update(CourseSession courseSession,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(courseSession, merge, serviceContext);
	}

	/**
	* Caches the course session in the entity cache if it is enabled.
	*
	* @param courseSession the course session
	*/
	public static void cacheResult(
		info.diit.portal.coursesubject.model.CourseSession courseSession) {
		getPersistence().cacheResult(courseSession);
	}

	/**
	* Caches the course sessions in the entity cache if it is enabled.
	*
	* @param courseSessions the course sessions
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.coursesubject.model.CourseSession> courseSessions) {
		getPersistence().cacheResult(courseSessions);
	}

	/**
	* Creates a new course session with the primary key. Does not add the course session to the database.
	*
	* @param sessionId the primary key for the new course session
	* @return the new course session
	*/
	public static info.diit.portal.coursesubject.model.CourseSession create(
		long sessionId) {
		return getPersistence().create(sessionId);
	}

	/**
	* Removes the course session with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param sessionId the primary key of the course session
	* @return the course session that was removed
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a course session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession remove(
		long sessionId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException {
		return getPersistence().remove(sessionId);
	}

	public static info.diit.portal.coursesubject.model.CourseSession updateImpl(
		info.diit.portal.coursesubject.model.CourseSession courseSession,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(courseSession, merge);
	}

	/**
	* Returns the course session with the primary key or throws a {@link info.diit.portal.coursesubject.NoSuchCourseSessionException} if it could not be found.
	*
	* @param sessionId the primary key of the course session
	* @return the course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a course session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession findByPrimaryKey(
		long sessionId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException {
		return getPersistence().findByPrimaryKey(sessionId);
	}

	/**
	* Returns the course session with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param sessionId the primary key of the course session
	* @return the course session, or <code>null</code> if a course session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession fetchByPrimaryKey(
		long sessionId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(sessionId);
	}

	/**
	* Returns all the course sessions where sessionName = &#63;.
	*
	* @param sessionName the session name
	* @return the matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findBysessionName(
		java.lang.String sessionName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBysessionName(sessionName);
	}

	/**
	* Returns a range of all the course sessions where sessionName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param sessionName the session name
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @return the range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findBysessionName(
		java.lang.String sessionName, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBysessionName(sessionName, start, end);
	}

	/**
	* Returns an ordered range of all the course sessions where sessionName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param sessionName the session name
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findBysessionName(
		java.lang.String sessionName, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBysessionName(sessionName, start, end, orderByComparator);
	}

	/**
	* Returns the first course session in the ordered set where sessionName = &#63;.
	*
	* @param sessionName the session name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession findBysessionName_First(
		java.lang.String sessionName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException {
		return getPersistence()
				   .findBysessionName_First(sessionName, orderByComparator);
	}

	/**
	* Returns the first course session in the ordered set where sessionName = &#63;.
	*
	* @param sessionName the session name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession fetchBysessionName_First(
		java.lang.String sessionName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBysessionName_First(sessionName, orderByComparator);
	}

	/**
	* Returns the last course session in the ordered set where sessionName = &#63;.
	*
	* @param sessionName the session name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession findBysessionName_Last(
		java.lang.String sessionName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException {
		return getPersistence()
				   .findBysessionName_Last(sessionName, orderByComparator);
	}

	/**
	* Returns the last course session in the ordered set where sessionName = &#63;.
	*
	* @param sessionName the session name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession fetchBysessionName_Last(
		java.lang.String sessionName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBysessionName_Last(sessionName, orderByComparator);
	}

	/**
	* Returns the course sessions before and after the current course session in the ordered set where sessionName = &#63;.
	*
	* @param sessionId the primary key of the current course session
	* @param sessionName the session name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a course session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession[] findBysessionName_PrevAndNext(
		long sessionId, java.lang.String sessionName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException {
		return getPersistence()
				   .findBysessionName_PrevAndNext(sessionId, sessionName,
			orderByComparator);
	}

	/**
	* Returns all the course sessions where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findByCourseId(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCourseId(courseId);
	}

	/**
	* Returns a range of all the course sessions where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @return the range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findByCourseId(
		long courseId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCourseId(courseId, start, end);
	}

	/**
	* Returns an ordered range of all the course sessions where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findByCourseId(
		long courseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCourseId(courseId, start, end, orderByComparator);
	}

	/**
	* Returns the first course session in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession findByCourseId_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException {
		return getPersistence().findByCourseId_First(courseId, orderByComparator);
	}

	/**
	* Returns the first course session in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession fetchByCourseId_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCourseId_First(courseId, orderByComparator);
	}

	/**
	* Returns the last course session in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession findByCourseId_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException {
		return getPersistence().findByCourseId_Last(courseId, orderByComparator);
	}

	/**
	* Returns the last course session in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession fetchByCourseId_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCourseId_Last(courseId, orderByComparator);
	}

	/**
	* Returns the course sessions before and after the current course session in the ordered set where courseId = &#63;.
	*
	* @param sessionId the primary key of the current course session
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a course session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession[] findByCourseId_PrevAndNext(
		long sessionId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException {
		return getPersistence()
				   .findByCourseId_PrevAndNext(sessionId, courseId,
			orderByComparator);
	}

	/**
	* Returns all the course sessions where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId);
	}

	/**
	* Returns a range of all the course sessions where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @return the range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the course sessions where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByorganization(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first course session in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException {
		return getPersistence()
				   .findByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first course session in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the last course session in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException {
		return getPersistence()
				   .findByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last course session in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the course sessions before and after the current course session in the ordered set where organizationId = &#63;.
	*
	* @param sessionId the primary key of the current course session
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a course session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession[] findByorganization_PrevAndNext(
		long sessionId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException {
		return getPersistence()
				   .findByorganization_PrevAndNext(sessionId, organizationId,
			orderByComparator);
	}

	/**
	* Returns all the course sessions where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findBycompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycompany(companyId);
	}

	/**
	* Returns a range of all the course sessions where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @return the range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findBycompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the course sessions where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findBycompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first course session in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession findBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException {
		return getPersistence().findBycompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first course session in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession fetchBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBycompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last course session in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession findBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException {
		return getPersistence().findBycompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last course session in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession fetchBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBycompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the course sessions before and after the current course session in the ordered set where companyId = &#63;.
	*
	* @param sessionId the primary key of the current course session
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a course session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSession[] findBycompany_PrevAndNext(
		long sessionId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException {
		return getPersistence()
				   .findBycompany_PrevAndNext(sessionId, companyId,
			orderByComparator);
	}

	/**
	* Returns all the course sessions.
	*
	* @return the course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the course sessions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @return the range of course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the course sessions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSession> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the course sessions where sessionName = &#63; from the database.
	*
	* @param sessionName the session name
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBysessionName(java.lang.String sessionName)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBysessionName(sessionName);
	}

	/**
	* Removes all the course sessions where courseId = &#63; from the database.
	*
	* @param courseId the course ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCourseId(courseId);
	}

	/**
	* Removes all the course sessions where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByorganization(organizationId);
	}

	/**
	* Removes all the course sessions where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBycompany(companyId);
	}

	/**
	* Removes all the course sessions from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of course sessions where sessionName = &#63;.
	*
	* @param sessionName the session name
	* @return the number of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static int countBysessionName(java.lang.String sessionName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBysessionName(sessionName);
	}

	/**
	* Returns the number of course sessions where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the number of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCourseId(courseId);
	}

	/**
	* Returns the number of course sessions where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByorganization(organizationId);
	}

	/**
	* Returns the number of course sessions where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static int countBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBycompany(companyId);
	}

	/**
	* Returns the number of course sessions.
	*
	* @return the number of course sessions
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CourseSessionPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CourseSessionPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.coursesubject.service.ClpSerializer.getServletContextName(),
					CourseSessionPersistence.class.getName());

			ReferenceRegistry.registerReference(CourseSessionUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(CourseSessionPersistence persistence) {
	}

	private static CourseSessionPersistence _persistence;
}
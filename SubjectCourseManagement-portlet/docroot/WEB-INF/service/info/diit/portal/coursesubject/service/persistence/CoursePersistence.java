/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.coursesubject.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.coursesubject.model.Course;

/**
 * The persistence interface for the course service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see CoursePersistenceImpl
 * @see CourseUtil
 * @generated
 */
public interface CoursePersistence extends BasePersistence<Course> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CourseUtil} to access the course persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the course in the entity cache if it is enabled.
	*
	* @param course the course
	*/
	public void cacheResult(info.diit.portal.coursesubject.model.Course course);

	/**
	* Caches the courses in the entity cache if it is enabled.
	*
	* @param courses the courses
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.coursesubject.model.Course> courses);

	/**
	* Creates a new course with the primary key. Does not add the course to the database.
	*
	* @param courseId the primary key for the new course
	* @return the new course
	*/
	public info.diit.portal.coursesubject.model.Course create(long courseId);

	/**
	* Removes the course with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param courseId the primary key of the course
	* @return the course that was removed
	* @throws info.diit.portal.coursesubject.NoSuchCourseException if a course with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course remove(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseException;

	public info.diit.portal.coursesubject.model.Course updateImpl(
		info.diit.portal.coursesubject.model.Course course, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course with the primary key or throws a {@link info.diit.portal.coursesubject.NoSuchCourseException} if it could not be found.
	*
	* @param courseId the primary key of the course
	* @return the course
	* @throws info.diit.portal.coursesubject.NoSuchCourseException if a course with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course findByPrimaryKey(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseException;

	/**
	* Returns the course with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param courseId the primary key of the course
	* @return the course, or <code>null</code> if a course with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course fetchByPrimaryKey(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the courses where courseCode = &#63;.
	*
	* @param courseCode the course code
	* @return the matching courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findBycourseCode(
		java.lang.String courseCode)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the courses where courseCode = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseCode the course code
	* @param start the lower bound of the range of courses
	* @param end the upper bound of the range of courses (not inclusive)
	* @return the range of matching courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findBycourseCode(
		java.lang.String courseCode, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the courses where courseCode = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseCode the course code
	* @param start the lower bound of the range of courses
	* @param end the upper bound of the range of courses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findBycourseCode(
		java.lang.String courseCode, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course in the ordered set where courseCode = &#63;.
	*
	* @param courseCode the course code
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course
	* @throws info.diit.portal.coursesubject.NoSuchCourseException if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course findBycourseCode_First(
		java.lang.String courseCode,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseException;

	/**
	* Returns the first course in the ordered set where courseCode = &#63;.
	*
	* @param courseCode the course code
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course, or <code>null</code> if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course fetchBycourseCode_First(
		java.lang.String courseCode,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course in the ordered set where courseCode = &#63;.
	*
	* @param courseCode the course code
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course
	* @throws info.diit.portal.coursesubject.NoSuchCourseException if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course findBycourseCode_Last(
		java.lang.String courseCode,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseException;

	/**
	* Returns the last course in the ordered set where courseCode = &#63;.
	*
	* @param courseCode the course code
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course, or <code>null</code> if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course fetchBycourseCode_Last(
		java.lang.String courseCode,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the courses before and after the current course in the ordered set where courseCode = &#63;.
	*
	* @param courseId the primary key of the current course
	* @param courseCode the course code
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course
	* @throws info.diit.portal.coursesubject.NoSuchCourseException if a course with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course[] findBycourseCode_PrevAndNext(
		long courseId, java.lang.String courseCode,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseException;

	/**
	* Returns all the courses where courseName = &#63;.
	*
	* @param courseName the course name
	* @return the matching courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findBycourseName(
		java.lang.String courseName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the courses where courseName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseName the course name
	* @param start the lower bound of the range of courses
	* @param end the upper bound of the range of courses (not inclusive)
	* @return the range of matching courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findBycourseName(
		java.lang.String courseName, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the courses where courseName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseName the course name
	* @param start the lower bound of the range of courses
	* @param end the upper bound of the range of courses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findBycourseName(
		java.lang.String courseName, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course in the ordered set where courseName = &#63;.
	*
	* @param courseName the course name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course
	* @throws info.diit.portal.coursesubject.NoSuchCourseException if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course findBycourseName_First(
		java.lang.String courseName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseException;

	/**
	* Returns the first course in the ordered set where courseName = &#63;.
	*
	* @param courseName the course name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course, or <code>null</code> if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course fetchBycourseName_First(
		java.lang.String courseName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course in the ordered set where courseName = &#63;.
	*
	* @param courseName the course name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course
	* @throws info.diit.portal.coursesubject.NoSuchCourseException if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course findBycourseName_Last(
		java.lang.String courseName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseException;

	/**
	* Returns the last course in the ordered set where courseName = &#63;.
	*
	* @param courseName the course name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course, or <code>null</code> if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course fetchBycourseName_Last(
		java.lang.String courseName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the courses before and after the current course in the ordered set where courseName = &#63;.
	*
	* @param courseId the primary key of the current course
	* @param courseName the course name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course
	* @throws info.diit.portal.coursesubject.NoSuchCourseException if a course with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course[] findBycourseName_PrevAndNext(
		long courseId, java.lang.String courseName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseException;

	/**
	* Returns all the courses where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the courses where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of courses
	* @param end the upper bound of the range of courses (not inclusive)
	* @return the range of matching courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the courses where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of courses
	* @param end the upper bound of the range of courses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course
	* @throws info.diit.portal.coursesubject.NoSuchCourseException if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseException;

	/**
	* Returns the first course in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course, or <code>null</code> if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course
	* @throws info.diit.portal.coursesubject.NoSuchCourseException if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseException;

	/**
	* Returns the last course in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course, or <code>null</code> if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the courses before and after the current course in the ordered set where organizationId = &#63;.
	*
	* @param courseId the primary key of the current course
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course
	* @throws info.diit.portal.coursesubject.NoSuchCourseException if a course with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course[] findByorganization_PrevAndNext(
		long courseId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseException;

	/**
	* Returns all the courses where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findBycompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the courses where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of courses
	* @param end the upper bound of the range of courses (not inclusive)
	* @return the range of matching courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findBycompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the courses where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of courses
	* @param end the upper bound of the range of courses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findBycompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course
	* @throws info.diit.portal.coursesubject.NoSuchCourseException if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course findBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseException;

	/**
	* Returns the first course in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course, or <code>null</code> if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course fetchBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course
	* @throws info.diit.portal.coursesubject.NoSuchCourseException if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course findBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseException;

	/**
	* Returns the last course in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course, or <code>null</code> if a matching course could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course fetchBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the courses before and after the current course in the ordered set where companyId = &#63;.
	*
	* @param courseId the primary key of the current course
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course
	* @throws info.diit.portal.coursesubject.NoSuchCourseException if a course with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.Course[] findBycompany_PrevAndNext(
		long courseId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseException;

	/**
	* Returns all the courses.
	*
	* @return the courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the courses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of courses
	* @param end the upper bound of the range of courses (not inclusive)
	* @return the range of courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the courses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of courses
	* @param end the upper bound of the range of courses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of courses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.Course> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the courses where courseCode = &#63; from the database.
	*
	* @param courseCode the course code
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycourseCode(java.lang.String courseCode)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the courses where courseName = &#63; from the database.
	*
	* @param courseName the course name
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycourseName(java.lang.String courseName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the courses where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the courses where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the courses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of courses where courseCode = &#63;.
	*
	* @param courseCode the course code
	* @return the number of matching courses
	* @throws SystemException if a system exception occurred
	*/
	public int countBycourseCode(java.lang.String courseCode)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of courses where courseName = &#63;.
	*
	* @param courseName the course name
	* @return the number of matching courses
	* @throws SystemException if a system exception occurred
	*/
	public int countBycourseName(java.lang.String courseName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of courses where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching courses
	* @throws SystemException if a system exception occurred
	*/
	public int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of courses where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching courses
	* @throws SystemException if a system exception occurred
	*/
	public int countBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of courses.
	*
	* @return the number of courses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
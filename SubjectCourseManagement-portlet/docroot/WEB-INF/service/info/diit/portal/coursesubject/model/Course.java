/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.coursesubject.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Course service. Represents a row in the &quot;EduPortal_SubjectCourse_Course&quot; database table, with each column mapped to a property of this class.
 *
 * @author limon
 * @see CourseModel
 * @see info.diit.portal.coursesubject.model.impl.CourseImpl
 * @see info.diit.portal.coursesubject.model.impl.CourseModelImpl
 * @generated
 */
public interface Course extends CourseModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link info.diit.portal.coursesubject.model.impl.CourseImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
}
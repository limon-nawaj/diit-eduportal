/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.coursesubject.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.coursesubject.model.CourseSession;

/**
 * The persistence interface for the course session service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see CourseSessionPersistenceImpl
 * @see CourseSessionUtil
 * @generated
 */
public interface CourseSessionPersistence extends BasePersistence<CourseSession> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CourseSessionUtil} to access the course session persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the course session in the entity cache if it is enabled.
	*
	* @param courseSession the course session
	*/
	public void cacheResult(
		info.diit.portal.coursesubject.model.CourseSession courseSession);

	/**
	* Caches the course sessions in the entity cache if it is enabled.
	*
	* @param courseSessions the course sessions
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.coursesubject.model.CourseSession> courseSessions);

	/**
	* Creates a new course session with the primary key. Does not add the course session to the database.
	*
	* @param sessionId the primary key for the new course session
	* @return the new course session
	*/
	public info.diit.portal.coursesubject.model.CourseSession create(
		long sessionId);

	/**
	* Removes the course session with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param sessionId the primary key of the course session
	* @return the course session that was removed
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a course session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession remove(
		long sessionId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException;

	public info.diit.portal.coursesubject.model.CourseSession updateImpl(
		info.diit.portal.coursesubject.model.CourseSession courseSession,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course session with the primary key or throws a {@link info.diit.portal.coursesubject.NoSuchCourseSessionException} if it could not be found.
	*
	* @param sessionId the primary key of the course session
	* @return the course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a course session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession findByPrimaryKey(
		long sessionId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException;

	/**
	* Returns the course session with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param sessionId the primary key of the course session
	* @return the course session, or <code>null</code> if a course session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession fetchByPrimaryKey(
		long sessionId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the course sessions where sessionName = &#63;.
	*
	* @param sessionName the session name
	* @return the matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findBysessionName(
		java.lang.String sessionName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course sessions where sessionName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param sessionName the session name
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @return the range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findBysessionName(
		java.lang.String sessionName, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course sessions where sessionName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param sessionName the session name
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findBysessionName(
		java.lang.String sessionName, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course session in the ordered set where sessionName = &#63;.
	*
	* @param sessionName the session name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession findBysessionName_First(
		java.lang.String sessionName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException;

	/**
	* Returns the first course session in the ordered set where sessionName = &#63;.
	*
	* @param sessionName the session name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession fetchBysessionName_First(
		java.lang.String sessionName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course session in the ordered set where sessionName = &#63;.
	*
	* @param sessionName the session name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession findBysessionName_Last(
		java.lang.String sessionName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException;

	/**
	* Returns the last course session in the ordered set where sessionName = &#63;.
	*
	* @param sessionName the session name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession fetchBysessionName_Last(
		java.lang.String sessionName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course sessions before and after the current course session in the ordered set where sessionName = &#63;.
	*
	* @param sessionId the primary key of the current course session
	* @param sessionName the session name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a course session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession[] findBysessionName_PrevAndNext(
		long sessionId, java.lang.String sessionName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException;

	/**
	* Returns all the course sessions where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findByCourseId(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course sessions where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @return the range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findByCourseId(
		long courseId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course sessions where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findByCourseId(
		long courseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course session in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession findByCourseId_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException;

	/**
	* Returns the first course session in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession fetchByCourseId_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course session in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession findByCourseId_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException;

	/**
	* Returns the last course session in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession fetchByCourseId_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course sessions before and after the current course session in the ordered set where courseId = &#63;.
	*
	* @param sessionId the primary key of the current course session
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a course session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession[] findByCourseId_PrevAndNext(
		long sessionId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException;

	/**
	* Returns all the course sessions where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course sessions where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @return the range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course sessions where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course session in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException;

	/**
	* Returns the first course session in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course session in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException;

	/**
	* Returns the last course session in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course sessions before and after the current course session in the ordered set where organizationId = &#63;.
	*
	* @param sessionId the primary key of the current course session
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a course session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession[] findByorganization_PrevAndNext(
		long sessionId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException;

	/**
	* Returns all the course sessions where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findBycompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course sessions where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @return the range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findBycompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course sessions where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findBycompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course session in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession findBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException;

	/**
	* Returns the first course session in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession fetchBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course session in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession findBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException;

	/**
	* Returns the last course session in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course session, or <code>null</code> if a matching course session could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession fetchBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course sessions before and after the current course session in the ordered set where companyId = &#63;.
	*
	* @param sessionId the primary key of the current course session
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course session
	* @throws info.diit.portal.coursesubject.NoSuchCourseSessionException if a course session with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSession[] findBycompany_PrevAndNext(
		long sessionId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSessionException;

	/**
	* Returns all the course sessions.
	*
	* @return the course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course sessions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @return the range of course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course sessions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course sessions
	* @param end the upper bound of the range of course sessions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of course sessions
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSession> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course sessions where sessionName = &#63; from the database.
	*
	* @param sessionName the session name
	* @throws SystemException if a system exception occurred
	*/
	public void removeBysessionName(java.lang.String sessionName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course sessions where courseId = &#63; from the database.
	*
	* @param courseId the course ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course sessions where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course sessions where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course sessions from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course sessions where sessionName = &#63;.
	*
	* @param sessionName the session name
	* @return the number of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public int countBysessionName(java.lang.String sessionName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course sessions where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the number of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public int countByCourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course sessions where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course sessions where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching course sessions
	* @throws SystemException if a system exception occurred
	*/
	public int countBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course sessions.
	*
	* @return the number of course sessions
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
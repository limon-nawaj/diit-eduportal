/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.coursesubject.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the course subject local service. This utility wraps {@link info.diit.portal.coursesubject.service.impl.CourseSubjectLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see CourseSubjectLocalService
 * @see info.diit.portal.coursesubject.service.base.CourseSubjectLocalServiceBaseImpl
 * @see info.diit.portal.coursesubject.service.impl.CourseSubjectLocalServiceImpl
 * @generated
 */
public class CourseSubjectLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.coursesubject.service.impl.CourseSubjectLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the course subject to the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubject the course subject
	* @return the course subject that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSubject addCourseSubject(
		info.diit.portal.coursesubject.model.CourseSubject courseSubject)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addCourseSubject(courseSubject);
	}

	/**
	* Creates a new course subject with the primary key. Does not add the course subject to the database.
	*
	* @param courseSubjectId the primary key for the new course subject
	* @return the new course subject
	*/
	public static info.diit.portal.coursesubject.model.CourseSubject createCourseSubject(
		long courseSubjectId) {
		return getService().createCourseSubject(courseSubjectId);
	}

	/**
	* Deletes the course subject with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjectId the primary key of the course subject
	* @return the course subject that was removed
	* @throws PortalException if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSubject deleteCourseSubject(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCourseSubject(courseSubjectId);
	}

	/**
	* Deletes the course subject from the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubject the course subject
	* @return the course subject that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSubject deleteCourseSubject(
		info.diit.portal.coursesubject.model.CourseSubject courseSubject)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCourseSubject(courseSubject);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.coursesubject.model.CourseSubject fetchCourseSubject(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchCourseSubject(courseSubjectId);
	}

	/**
	* Returns the course subject with the primary key.
	*
	* @param courseSubjectId the primary key of the course subject
	* @return the course subject
	* @throws PortalException if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSubject getCourseSubject(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getCourseSubject(courseSubjectId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the course subjects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @return the range of course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.CourseSubject> getCourseSubjects(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCourseSubjects(start, end);
	}

	/**
	* Returns the number of course subjects.
	*
	* @return the number of course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int getCourseSubjectsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCourseSubjectsCount();
	}

	/**
	* Updates the course subject in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param courseSubject the course subject
	* @return the course subject that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSubject updateCourseSubject(
		info.diit.portal.coursesubject.model.CourseSubject courseSubject)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateCourseSubject(courseSubject);
	}

	/**
	* Updates the course subject in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param courseSubject the course subject
	* @param merge whether to merge the course subject with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the course subject that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.CourseSubject updateCourseSubject(
		info.diit.portal.coursesubject.model.CourseSubject courseSubject,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateCourseSubject(courseSubject, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<info.diit.portal.coursesubject.model.CourseSubject> findByCourseId(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByCourseId(courseId);
	}

	public static info.diit.portal.coursesubject.model.CourseSubject findBySubject(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSubjectException {
		return getService().findBySubject(subjectId);
	}

	public static java.util.List<info.diit.portal.coursesubject.model.CourseSubject> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByCompany(companyId);
	}

	public static java.util.List<info.diit.portal.coursesubject.model.CourseSubject> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByOrganization(organizationId);
	}

	public static void clearService() {
		_service = null;
	}

	public static CourseSubjectLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					CourseSubjectLocalService.class.getName());

			if (invokableLocalService instanceof CourseSubjectLocalService) {
				_service = (CourseSubjectLocalService)invokableLocalService;
			}
			else {
				_service = new CourseSubjectLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(CourseSubjectLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(CourseSubjectLocalService service) {
	}

	private static CourseSubjectLocalService _service;
}
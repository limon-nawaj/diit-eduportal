/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.coursesubject.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link CourseSubjectLocalService}.
 * </p>
 *
 * @author    limon
 * @see       CourseSubjectLocalService
 * @generated
 */
public class CourseSubjectLocalServiceWrapper
	implements CourseSubjectLocalService,
		ServiceWrapper<CourseSubjectLocalService> {
	public CourseSubjectLocalServiceWrapper(
		CourseSubjectLocalService courseSubjectLocalService) {
		_courseSubjectLocalService = courseSubjectLocalService;
	}

	/**
	* Adds the course subject to the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubject the course subject
	* @return the course subject that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSubject addCourseSubject(
		info.diit.portal.coursesubject.model.CourseSubject courseSubject)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.addCourseSubject(courseSubject);
	}

	/**
	* Creates a new course subject with the primary key. Does not add the course subject to the database.
	*
	* @param courseSubjectId the primary key for the new course subject
	* @return the new course subject
	*/
	public info.diit.portal.coursesubject.model.CourseSubject createCourseSubject(
		long courseSubjectId) {
		return _courseSubjectLocalService.createCourseSubject(courseSubjectId);
	}

	/**
	* Deletes the course subject with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjectId the primary key of the course subject
	* @return the course subject that was removed
	* @throws PortalException if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSubject deleteCourseSubject(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.deleteCourseSubject(courseSubjectId);
	}

	/**
	* Deletes the course subject from the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubject the course subject
	* @return the course subject that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSubject deleteCourseSubject(
		info.diit.portal.coursesubject.model.CourseSubject courseSubject)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.deleteCourseSubject(courseSubject);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _courseSubjectLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.coursesubject.model.CourseSubject fetchCourseSubject(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.fetchCourseSubject(courseSubjectId);
	}

	/**
	* Returns the course subject with the primary key.
	*
	* @param courseSubjectId the primary key of the course subject
	* @return the course subject
	* @throws PortalException if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSubject getCourseSubject(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.getCourseSubject(courseSubjectId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the course subjects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @return the range of course subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.coursesubject.model.CourseSubject> getCourseSubjects(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.getCourseSubjects(start, end);
	}

	/**
	* Returns the number of course subjects.
	*
	* @return the number of course subjects
	* @throws SystemException if a system exception occurred
	*/
	public int getCourseSubjectsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.getCourseSubjectsCount();
	}

	/**
	* Updates the course subject in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param courseSubject the course subject
	* @return the course subject that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSubject updateCourseSubject(
		info.diit.portal.coursesubject.model.CourseSubject courseSubject)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.updateCourseSubject(courseSubject);
	}

	/**
	* Updates the course subject in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param courseSubject the course subject
	* @param merge whether to merge the course subject with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the course subject that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.coursesubject.model.CourseSubject updateCourseSubject(
		info.diit.portal.coursesubject.model.CourseSubject courseSubject,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.updateCourseSubject(courseSubject,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _courseSubjectLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_courseSubjectLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _courseSubjectLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.coursesubject.model.CourseSubject> findByCourseId(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.findByCourseId(courseId);
	}

	public info.diit.portal.coursesubject.model.CourseSubject findBySubject(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchCourseSubjectException {
		return _courseSubjectLocalService.findBySubject(subjectId);
	}

	public java.util.List<info.diit.portal.coursesubject.model.CourseSubject> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.findByCompany(companyId);
	}

	public java.util.List<info.diit.portal.coursesubject.model.CourseSubject> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubjectLocalService.findByOrganization(organizationId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public CourseSubjectLocalService getWrappedCourseSubjectLocalService() {
		return _courseSubjectLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedCourseSubjectLocalService(
		CourseSubjectLocalService courseSubjectLocalService) {
		_courseSubjectLocalService = courseSubjectLocalService;
	}

	public CourseSubjectLocalService getWrappedService() {
		return _courseSubjectLocalService;
	}

	public void setWrappedService(
		CourseSubjectLocalService courseSubjectLocalService) {
		_courseSubjectLocalService = courseSubjectLocalService;
	}

	private CourseSubjectLocalService _courseSubjectLocalService;
}
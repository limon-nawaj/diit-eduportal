/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.coursesubject.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.coursesubject.model.Subject;

import java.util.List;

/**
 * The persistence utility for the subject service. This utility wraps {@link SubjectPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author limon
 * @see SubjectPersistence
 * @see SubjectPersistenceImpl
 * @generated
 */
public class SubjectUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Subject subject) {
		getPersistence().clearCache(subject);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Subject> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Subject> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Subject> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Subject update(Subject subject, boolean merge)
		throws SystemException {
		return getPersistence().update(subject, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Subject update(Subject subject, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(subject, merge, serviceContext);
	}

	/**
	* Caches the subject in the entity cache if it is enabled.
	*
	* @param subject the subject
	*/
	public static void cacheResult(
		info.diit.portal.coursesubject.model.Subject subject) {
		getPersistence().cacheResult(subject);
	}

	/**
	* Caches the subjects in the entity cache if it is enabled.
	*
	* @param subjects the subjects
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.coursesubject.model.Subject> subjects) {
		getPersistence().cacheResult(subjects);
	}

	/**
	* Creates a new subject with the primary key. Does not add the subject to the database.
	*
	* @param subjectId the primary key for the new subject
	* @return the new subject
	*/
	public static info.diit.portal.coursesubject.model.Subject create(
		long subjectId) {
		return getPersistence().create(subjectId);
	}

	/**
	* Removes the subject with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param subjectId the primary key of the subject
	* @return the subject that was removed
	* @throws info.diit.portal.coursesubject.NoSuchSubjectException if a subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject remove(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchSubjectException {
		return getPersistence().remove(subjectId);
	}

	public static info.diit.portal.coursesubject.model.Subject updateImpl(
		info.diit.portal.coursesubject.model.Subject subject, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(subject, merge);
	}

	/**
	* Returns the subject with the primary key or throws a {@link info.diit.portal.coursesubject.NoSuchSubjectException} if it could not be found.
	*
	* @param subjectId the primary key of the subject
	* @return the subject
	* @throws info.diit.portal.coursesubject.NoSuchSubjectException if a subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject findByPrimaryKey(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchSubjectException {
		return getPersistence().findByPrimaryKey(subjectId);
	}

	/**
	* Returns the subject with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param subjectId the primary key of the subject
	* @return the subject, or <code>null</code> if a subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject fetchByPrimaryKey(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(subjectId);
	}

	/**
	* Returns all the subjects where subjectCode = &#63;.
	*
	* @param subjectCode the subject code
	* @return the matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findBysubjectCode(
		java.lang.String subjectCode)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBysubjectCode(subjectCode);
	}

	/**
	* Returns a range of all the subjects where subjectCode = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectCode the subject code
	* @param start the lower bound of the range of subjects
	* @param end the upper bound of the range of subjects (not inclusive)
	* @return the range of matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findBysubjectCode(
		java.lang.String subjectCode, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBysubjectCode(subjectCode, start, end);
	}

	/**
	* Returns an ordered range of all the subjects where subjectCode = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectCode the subject code
	* @param start the lower bound of the range of subjects
	* @param end the upper bound of the range of subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findBysubjectCode(
		java.lang.String subjectCode, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBysubjectCode(subjectCode, start, end, orderByComparator);
	}

	/**
	* Returns the first subject in the ordered set where subjectCode = &#63;.
	*
	* @param subjectCode the subject code
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject
	* @throws info.diit.portal.coursesubject.NoSuchSubjectException if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject findBysubjectCode_First(
		java.lang.String subjectCode,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchSubjectException {
		return getPersistence()
				   .findBysubjectCode_First(subjectCode, orderByComparator);
	}

	/**
	* Returns the first subject in the ordered set where subjectCode = &#63;.
	*
	* @param subjectCode the subject code
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject, or <code>null</code> if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject fetchBysubjectCode_First(
		java.lang.String subjectCode,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBysubjectCode_First(subjectCode, orderByComparator);
	}

	/**
	* Returns the last subject in the ordered set where subjectCode = &#63;.
	*
	* @param subjectCode the subject code
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject
	* @throws info.diit.portal.coursesubject.NoSuchSubjectException if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject findBysubjectCode_Last(
		java.lang.String subjectCode,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchSubjectException {
		return getPersistence()
				   .findBysubjectCode_Last(subjectCode, orderByComparator);
	}

	/**
	* Returns the last subject in the ordered set where subjectCode = &#63;.
	*
	* @param subjectCode the subject code
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject, or <code>null</code> if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject fetchBysubjectCode_Last(
		java.lang.String subjectCode,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBysubjectCode_Last(subjectCode, orderByComparator);
	}

	/**
	* Returns the subjects before and after the current subject in the ordered set where subjectCode = &#63;.
	*
	* @param subjectId the primary key of the current subject
	* @param subjectCode the subject code
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next subject
	* @throws info.diit.portal.coursesubject.NoSuchSubjectException if a subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject[] findBysubjectCode_PrevAndNext(
		long subjectId, java.lang.String subjectCode,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchSubjectException {
		return getPersistence()
				   .findBysubjectCode_PrevAndNext(subjectId, subjectCode,
			orderByComparator);
	}

	/**
	* Returns all the subjects where subjectName = &#63;.
	*
	* @param subjectName the subject name
	* @return the matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findBysubjectName(
		java.lang.String subjectName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBysubjectName(subjectName);
	}

	/**
	* Returns a range of all the subjects where subjectName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectName the subject name
	* @param start the lower bound of the range of subjects
	* @param end the upper bound of the range of subjects (not inclusive)
	* @return the range of matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findBysubjectName(
		java.lang.String subjectName, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBysubjectName(subjectName, start, end);
	}

	/**
	* Returns an ordered range of all the subjects where subjectName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectName the subject name
	* @param start the lower bound of the range of subjects
	* @param end the upper bound of the range of subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findBysubjectName(
		java.lang.String subjectName, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBysubjectName(subjectName, start, end, orderByComparator);
	}

	/**
	* Returns the first subject in the ordered set where subjectName = &#63;.
	*
	* @param subjectName the subject name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject
	* @throws info.diit.portal.coursesubject.NoSuchSubjectException if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject findBysubjectName_First(
		java.lang.String subjectName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchSubjectException {
		return getPersistence()
				   .findBysubjectName_First(subjectName, orderByComparator);
	}

	/**
	* Returns the first subject in the ordered set where subjectName = &#63;.
	*
	* @param subjectName the subject name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject, or <code>null</code> if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject fetchBysubjectName_First(
		java.lang.String subjectName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBysubjectName_First(subjectName, orderByComparator);
	}

	/**
	* Returns the last subject in the ordered set where subjectName = &#63;.
	*
	* @param subjectName the subject name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject
	* @throws info.diit.portal.coursesubject.NoSuchSubjectException if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject findBysubjectName_Last(
		java.lang.String subjectName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchSubjectException {
		return getPersistence()
				   .findBysubjectName_Last(subjectName, orderByComparator);
	}

	/**
	* Returns the last subject in the ordered set where subjectName = &#63;.
	*
	* @param subjectName the subject name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject, or <code>null</code> if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject fetchBysubjectName_Last(
		java.lang.String subjectName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBysubjectName_Last(subjectName, orderByComparator);
	}

	/**
	* Returns the subjects before and after the current subject in the ordered set where subjectName = &#63;.
	*
	* @param subjectId the primary key of the current subject
	* @param subjectName the subject name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next subject
	* @throws info.diit.portal.coursesubject.NoSuchSubjectException if a subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject[] findBysubjectName_PrevAndNext(
		long subjectId, java.lang.String subjectName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchSubjectException {
		return getPersistence()
				   .findBysubjectName_PrevAndNext(subjectId, subjectName,
			orderByComparator);
	}

	/**
	* Returns all the subjects where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId);
	}

	/**
	* Returns a range of all the subjects where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of subjects
	* @param end the upper bound of the range of subjects (not inclusive)
	* @return the range of matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the subjects where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of subjects
	* @param end the upper bound of the range of subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByorganization(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject
	* @throws info.diit.portal.coursesubject.NoSuchSubjectException if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchSubjectException {
		return getPersistence()
				   .findByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject, or <code>null</code> if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the last subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject
	* @throws info.diit.portal.coursesubject.NoSuchSubjectException if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchSubjectException {
		return getPersistence()
				   .findByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject, or <code>null</code> if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the subjects before and after the current subject in the ordered set where organizationId = &#63;.
	*
	* @param subjectId the primary key of the current subject
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next subject
	* @throws info.diit.portal.coursesubject.NoSuchSubjectException if a subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject[] findByorganization_PrevAndNext(
		long subjectId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchSubjectException {
		return getPersistence()
				   .findByorganization_PrevAndNext(subjectId, organizationId,
			orderByComparator);
	}

	/**
	* Returns all the subjects where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findBycompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycompany(companyId);
	}

	/**
	* Returns a range of all the subjects where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of subjects
	* @param end the upper bound of the range of subjects (not inclusive)
	* @return the range of matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findBycompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the subjects where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of subjects
	* @param end the upper bound of the range of subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findBycompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first subject in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject
	* @throws info.diit.portal.coursesubject.NoSuchSubjectException if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject findBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchSubjectException {
		return getPersistence().findBycompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first subject in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching subject, or <code>null</code> if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject fetchBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBycompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last subject in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject
	* @throws info.diit.portal.coursesubject.NoSuchSubjectException if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject findBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchSubjectException {
		return getPersistence().findBycompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last subject in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching subject, or <code>null</code> if a matching subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject fetchBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBycompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the subjects before and after the current subject in the ordered set where companyId = &#63;.
	*
	* @param subjectId the primary key of the current subject
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next subject
	* @throws info.diit.portal.coursesubject.NoSuchSubjectException if a subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.coursesubject.model.Subject[] findBycompany_PrevAndNext(
		long subjectId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.coursesubject.NoSuchSubjectException {
		return getPersistence()
				   .findBycompany_PrevAndNext(subjectId, companyId,
			orderByComparator);
	}

	/**
	* Returns all the subjects.
	*
	* @return the subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the subjects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of subjects
	* @param end the upper bound of the range of subjects (not inclusive)
	* @return the range of subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the subjects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of subjects
	* @param end the upper bound of the range of subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.coursesubject.model.Subject> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the subjects where subjectCode = &#63; from the database.
	*
	* @param subjectCode the subject code
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBysubjectCode(java.lang.String subjectCode)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBysubjectCode(subjectCode);
	}

	/**
	* Removes all the subjects where subjectName = &#63; from the database.
	*
	* @param subjectName the subject name
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBysubjectName(java.lang.String subjectName)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBysubjectName(subjectName);
	}

	/**
	* Removes all the subjects where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByorganization(organizationId);
	}

	/**
	* Removes all the subjects where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBycompany(companyId);
	}

	/**
	* Removes all the subjects from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of subjects where subjectCode = &#63;.
	*
	* @param subjectCode the subject code
	* @return the number of matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countBysubjectCode(java.lang.String subjectCode)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBysubjectCode(subjectCode);
	}

	/**
	* Returns the number of subjects where subjectName = &#63;.
	*
	* @param subjectName the subject name
	* @return the number of matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countBysubjectName(java.lang.String subjectName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBysubjectName(subjectName);
	}

	/**
	* Returns the number of subjects where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByorganization(organizationId);
	}

	/**
	* Returns the number of subjects where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBycompany(companyId);
	}

	/**
	* Returns the number of subjects.
	*
	* @return the number of subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static SubjectPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (SubjectPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.coursesubject.service.ClpSerializer.getServletContextName(),
					SubjectPersistence.class.getName());

			ReferenceRegistry.registerReference(SubjectUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(SubjectPersistence persistence) {
	}

	private static SubjectPersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.coursesubject.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.AuditedModel;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the CourseSubject service. Represents a row in the &quot;EduPortal_SubjectCourse_CourseSubject&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link info.diit.portal.coursesubject.model.impl.CourseSubjectModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link info.diit.portal.coursesubject.model.impl.CourseSubjectImpl}.
 * </p>
 *
 * @author limon
 * @see CourseSubject
 * @see info.diit.portal.coursesubject.model.impl.CourseSubjectImpl
 * @see info.diit.portal.coursesubject.model.impl.CourseSubjectModelImpl
 * @generated
 */
public interface CourseSubjectModel extends AuditedModel,
	BaseModel<CourseSubject> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a course subject model instance should use the {@link CourseSubject} interface instead.
	 */

	/**
	 * Returns the primary key of this course subject.
	 *
	 * @return the primary key of this course subject
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this course subject.
	 *
	 * @param primaryKey the primary key of this course subject
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the course subject ID of this course subject.
	 *
	 * @return the course subject ID of this course subject
	 */
	public long getCourseSubjectId();

	/**
	 * Sets the course subject ID of this course subject.
	 *
	 * @param courseSubjectId the course subject ID of this course subject
	 */
	public void setCourseSubjectId(long courseSubjectId);

	/**
	 * Returns the company ID of this course subject.
	 *
	 * @return the company ID of this course subject
	 */
	public long getCompanyId();

	/**
	 * Sets the company ID of this course subject.
	 *
	 * @param companyId the company ID of this course subject
	 */
	public void setCompanyId(long companyId);

	/**
	 * Returns the organization ID of this course subject.
	 *
	 * @return the organization ID of this course subject
	 */
	public long getOrganizationId();

	/**
	 * Sets the organization ID of this course subject.
	 *
	 * @param organizationId the organization ID of this course subject
	 */
	public void setOrganizationId(long organizationId);

	/**
	 * Returns the user ID of this course subject.
	 *
	 * @return the user ID of this course subject
	 */
	public long getUserId();

	/**
	 * Sets the user ID of this course subject.
	 *
	 * @param userId the user ID of this course subject
	 */
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this course subject.
	 *
	 * @return the user uuid of this course subject
	 * @throws SystemException if a system exception occurred
	 */
	public String getUserUuid() throws SystemException;

	/**
	 * Sets the user uuid of this course subject.
	 *
	 * @param userUuid the user uuid of this course subject
	 */
	public void setUserUuid(String userUuid);

	/**
	 * Returns the user name of this course subject.
	 *
	 * @return the user name of this course subject
	 */
	@AutoEscape
	public String getUserName();

	/**
	 * Sets the user name of this course subject.
	 *
	 * @param userName the user name of this course subject
	 */
	public void setUserName(String userName);

	/**
	 * Returns the create date of this course subject.
	 *
	 * @return the create date of this course subject
	 */
	public Date getCreateDate();

	/**
	 * Sets the create date of this course subject.
	 *
	 * @param createDate the create date of this course subject
	 */
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this course subject.
	 *
	 * @return the modified date of this course subject
	 */
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this course subject.
	 *
	 * @param modifiedDate the modified date of this course subject
	 */
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the course ID of this course subject.
	 *
	 * @return the course ID of this course subject
	 */
	public long getCourseId();

	/**
	 * Sets the course ID of this course subject.
	 *
	 * @param courseId the course ID of this course subject
	 */
	public void setCourseId(long courseId);

	/**
	 * Returns the subject ID of this course subject.
	 *
	 * @return the subject ID of this course subject
	 */
	public long getSubjectId();

	/**
	 * Sets the subject ID of this course subject.
	 *
	 * @param subjectId the subject ID of this course subject
	 */
	public void setSubjectId(long subjectId);

	public boolean isNew();

	public void setNew(boolean n);

	public boolean isCachedModel();

	public void setCachedModel(boolean cachedModel);

	public boolean isEscapedModel();

	public Serializable getPrimaryKeyObj();

	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	public ExpandoBridge getExpandoBridge();

	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	public Object clone();

	public int compareTo(CourseSubject courseSubject);

	public int hashCode();

	public CacheModel<CourseSubject> toCacheModel();

	public CourseSubject toEscapedModel();

	public String toString();

	public String toXmlString();
}
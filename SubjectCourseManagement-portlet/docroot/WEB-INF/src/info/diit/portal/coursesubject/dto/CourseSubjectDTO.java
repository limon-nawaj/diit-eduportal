package info.diit.portal.coursesubject.dto;

import java.util.Date;

public class CourseSubjectDTO {

//	Audit field
	private long companyId;
	private long organizationId;
	private long userId;
	private String userName;
	private Date createDate;
	private Date modifiedDate;

//	course subject field
	private long coursesubjectId;
	
//	foreign key
	private long courseId;
	private long subjectId;
	
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public long getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public long getCoursesubjectId() {
		return coursesubjectId;
	}
	public void setCoursesubjectId(long coursesubjectId) {
		this.coursesubjectId = coursesubjectId;
	}
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public long getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(long subjectId) {
		this.subjectId = subjectId;
	}
}

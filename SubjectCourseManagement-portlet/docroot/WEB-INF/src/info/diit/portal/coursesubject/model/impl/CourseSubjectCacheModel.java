/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.coursesubject.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.coursesubject.model.CourseSubject;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing CourseSubject in entity cache.
 *
 * @author limon
 * @see CourseSubject
 * @generated
 */
public class CourseSubjectCacheModel implements CacheModel<CourseSubject>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{courseSubjectId=");
		sb.append(courseSubjectId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", courseId=");
		sb.append(courseId);
		sb.append(", subjectId=");
		sb.append(subjectId);
		sb.append("}");

		return sb.toString();
	}

	public CourseSubject toEntityModel() {
		CourseSubjectImpl courseSubjectImpl = new CourseSubjectImpl();

		courseSubjectImpl.setCourseSubjectId(courseSubjectId);
		courseSubjectImpl.setCompanyId(companyId);
		courseSubjectImpl.setOrganizationId(organizationId);
		courseSubjectImpl.setUserId(userId);

		if (userName == null) {
			courseSubjectImpl.setUserName(StringPool.BLANK);
		}
		else {
			courseSubjectImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			courseSubjectImpl.setCreateDate(null);
		}
		else {
			courseSubjectImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			courseSubjectImpl.setModifiedDate(null);
		}
		else {
			courseSubjectImpl.setModifiedDate(new Date(modifiedDate));
		}

		courseSubjectImpl.setCourseId(courseId);
		courseSubjectImpl.setSubjectId(subjectId);

		courseSubjectImpl.resetOriginalValues();

		return courseSubjectImpl;
	}

	public long courseSubjectId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long courseId;
	public long subjectId;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.coursesubject.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.coursesubject.model.Subject;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Subject in entity cache.
 *
 * @author limon
 * @see Subject
 * @generated
 */
public class SubjectCacheModel implements CacheModel<Subject>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{subjectId=");
		sb.append(subjectId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", subjectCode=");
		sb.append(subjectCode);
		sb.append(", subjectName=");
		sb.append(subjectName);
		sb.append(", description=");
		sb.append(description);
		sb.append("}");

		return sb.toString();
	}

	public Subject toEntityModel() {
		SubjectImpl subjectImpl = new SubjectImpl();

		subjectImpl.setSubjectId(subjectId);
		subjectImpl.setCompanyId(companyId);
		subjectImpl.setOrganizationId(organizationId);
		subjectImpl.setUserId(userId);

		if (userName == null) {
			subjectImpl.setUserName(StringPool.BLANK);
		}
		else {
			subjectImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			subjectImpl.setCreateDate(null);
		}
		else {
			subjectImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			subjectImpl.setModifiedDate(null);
		}
		else {
			subjectImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (subjectCode == null) {
			subjectImpl.setSubjectCode(StringPool.BLANK);
		}
		else {
			subjectImpl.setSubjectCode(subjectCode);
		}

		if (subjectName == null) {
			subjectImpl.setSubjectName(StringPool.BLANK);
		}
		else {
			subjectImpl.setSubjectName(subjectName);
		}

		if (description == null) {
			subjectImpl.setDescription(StringPool.BLANK);
		}
		else {
			subjectImpl.setDescription(description);
		}

		subjectImpl.resetOriginalValues();

		return subjectImpl;
	}

	public long subjectId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String subjectCode;
	public String subjectName;
	public String description;
}
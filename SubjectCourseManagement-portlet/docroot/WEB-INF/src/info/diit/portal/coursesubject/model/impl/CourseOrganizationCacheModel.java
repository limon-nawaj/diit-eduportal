/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.coursesubject.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.coursesubject.model.CourseOrganization;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing CourseOrganization in entity cache.
 *
 * @author limon
 * @see CourseOrganization
 * @generated
 */
public class CourseOrganizationCacheModel implements CacheModel<CourseOrganization>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{courseOrganizationId=");
		sb.append(courseOrganizationId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", courseId=");
		sb.append(courseId);
		sb.append("}");

		return sb.toString();
	}

	public CourseOrganization toEntityModel() {
		CourseOrganizationImpl courseOrganizationImpl = new CourseOrganizationImpl();

		courseOrganizationImpl.setCourseOrganizationId(courseOrganizationId);
		courseOrganizationImpl.setCompanyId(companyId);
		courseOrganizationImpl.setOrganizationId(organizationId);
		courseOrganizationImpl.setUserId(userId);

		if (userName == null) {
			courseOrganizationImpl.setUserName(StringPool.BLANK);
		}
		else {
			courseOrganizationImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			courseOrganizationImpl.setCreateDate(null);
		}
		else {
			courseOrganizationImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			courseOrganizationImpl.setModifiedDate(null);
		}
		else {
			courseOrganizationImpl.setModifiedDate(new Date(modifiedDate));
		}

		courseOrganizationImpl.setCourseId(courseId);

		courseOrganizationImpl.resetOriginalValues();

		return courseOrganizationImpl;
	}

	public long courseOrganizationId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long courseId;
}
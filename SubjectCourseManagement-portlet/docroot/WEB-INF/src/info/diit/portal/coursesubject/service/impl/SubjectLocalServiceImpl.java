/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.coursesubject.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import info.diit.portal.coursesubject.NoSuchSubjectException;
import info.diit.portal.coursesubject.model.Subject;
import info.diit.portal.coursesubject.service.base.SubjectLocalServiceBaseImpl;
import info.diit.portal.coursesubject.service.persistence.SubjectUtil;

/**
 * The implementation of the subject local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.coursesubject.service.SubjectLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author limon
 * @see info.diit.portal.coursesubject.service.base.SubjectLocalServiceBaseImpl
 * @see info.diit.portal.coursesubject.service.SubjectLocalServiceUtil
 */
public class SubjectLocalServiceImpl extends SubjectLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.coursesubject.service.SubjectLocalServiceUtil} to access the subject local service.
	 */
	
	public List<Subject> findBysubjectCode(String subjectCode) throws SystemException{
		return SubjectUtil.findBysubjectCode(subjectCode);
	}
	
	public Subject findBy(long subjectId) throws NoSuchSubjectException, SystemException{
		return SubjectUtil.findByPrimaryKey(subjectId);
	}
	
	public List<Subject> findByOrganization(long organizationId) throws SystemException{
		return SubjectUtil.findByorganization(organizationId);
	}
	
	public List<Subject> findByCompany(long companyId) throws SystemException{
		return SubjectUtil.findBycompany(companyId);
	}
}
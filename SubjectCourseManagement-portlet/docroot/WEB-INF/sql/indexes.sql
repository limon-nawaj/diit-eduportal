create index IX_1FD5982A on EduPortal_Course (companyId);
create index IX_BD61915A on EduPortal_Course (courseCode);
create index IX_6D28FFC8 on EduPortal_Course (courseId);
create index IX_CF65AE78 on EduPortal_Course (courseName);
create index IX_EDEDA0E0 on EduPortal_Course (organizationId);

create index IX_FB9C49B7 on EduPortal_CourseOrganization (companyId);
create index IX_322F689B on EduPortal_CourseOrganization (courseId);
create index IX_7848DF73 on EduPortal_CourseOrganization (organizationId);

create index IX_93A78564 on EduPortal_CourseSession (companyId);
create index IX_7927834E on EduPortal_CourseSession (courseId);
create index IX_440E7E6 on EduPortal_CourseSession (organizationId);
create index IX_9F1CC6D on EduPortal_CourseSession (sessionName);

create index IX_1947168E on EduPortal_CourseSubject (companyId);
create index IX_121C24E4 on EduPortal_CourseSubject (courseId);
create index IX_E05F80FC on EduPortal_CourseSubject (organizationId);
create index IX_13A03FBD on EduPortal_CourseSubject (subjectId);

create index IX_6B43C793 on EduPortal_Subject (companyId);
create index IX_7E31AB17 on EduPortal_Subject (organizationId);
create index IX_689D2BD4 on EduPortal_Subject (subjectCode);
create index IX_659CF0C2 on EduPortal_Subject (subjectId);
create index IX_7AA148F2 on EduPortal_Subject (subjectName);

create index IX_8945F2F2 on EduPortal_SubjectCourse_Course (companyId);
create index IX_81FC8F92 on EduPortal_SubjectCourse_Course (courseCode);
create index IX_9400ACB0 on EduPortal_SubjectCourse_Course (courseName);
create index IX_D3EEC318 on EduPortal_SubjectCourse_Course (organizationId);

create index IX_23B2B87F on EduPortal_SubjectCourse_CourseOrganization (companyId);
create index IX_F9AC00D3 on EduPortal_SubjectCourse_CourseOrganization (courseId);
create index IX_F8506DAB on EduPortal_SubjectCourse_CourseOrganization (organizationId);

create index IX_FDE8F99C on EduPortal_SubjectCourse_CourseSession (companyId);
create index IX_534AA816 on EduPortal_SubjectCourse_CourseSession (courseId);
create index IX_2C5756AE on EduPortal_SubjectCourse_CourseSession (organizationId);
create index IX_E9A712A5 on EduPortal_SubjectCourse_CourseSession (sessionName);

create index IX_83888AC6 on EduPortal_SubjectCourse_CourseSubject (companyId);
create index IX_EC3F49AC on EduPortal_SubjectCourse_CourseSubject (courseId);
create index IX_875EFC4 on EduPortal_SubjectCourse_CourseSubject (organizationId);
create index IX_7DE1B3F5 on EduPortal_SubjectCourse_CourseSubject (subjectId);

create index IX_2FDEC5CB on EduPortal_SubjectCourse_Subject (companyId);
create index IX_5854CFDF on EduPortal_SubjectCourse_Subject (organizationId);
create index IX_72717C0C on EduPortal_SubjectCourse_Subject (subjectCode);
create index IX_8475992A on EduPortal_SubjectCourse_Subject (subjectName);

create index IX_D81B984F on SubjectCourse_Course (companyId);
create index IX_DDB95D5 on SubjectCourse_Course (courseCode);
create index IX_7BFE703 on SubjectCourse_Course (courseId);
create index IX_1FDFB2F3 on SubjectCourse_Course (courseName);
create index IX_B08AEFDB on SubjectCourse_Course (organizationId);

create index IX_8F0925E8 on SubjectCourse_CourseSession (sessionName);

create index IX_A328B549 on SubjectCourse_CourseSubject (courseId);
create index IX_A425BBF8 on SubjectCourse_CourseSubject (subjectId);

create index IX_41189232 on SubjectCourse_EduPortal_Course (companyId);
create index IX_C47DD852 on SubjectCourse_EduPortal_Course (courseCode);
create index IX_D681F570 on SubjectCourse_EduPortal_Course (courseName);
create index IX_EF1DABD8 on SubjectCourse_EduPortal_Course (organizationId);

create index IX_B25077BF on SubjectCourse_EduPortal_CourseOrganization (companyId);
create index IX_CCB95993 on SubjectCourse_EduPortal_CourseOrganization (courseId);
create index IX_5EA8366B on SubjectCourse_EduPortal_CourseOrganization (organizationId);

create index IX_900B25C on SubjectCourse_EduPortal_CourseSession (companyId);
create index IX_9DF8D756 on SubjectCourse_EduPortal_CourseSession (courseId);
create index IX_BAF515EE on SubjectCourse_EduPortal_CourseSession (organizationId);
create index IX_8DB39B65 on SubjectCourse_EduPortal_CourseSession (sessionName);

create index IX_8EA04386 on SubjectCourse_EduPortal_CourseSubject (companyId);
create index IX_36ED78EC on SubjectCourse_EduPortal_CourseSubject (courseId);
create index IX_9713AF04 on SubjectCourse_EduPortal_CourseSubject (organizationId);
create index IX_BC7FD60E on SubjectCourse_EduPortal_CourseSubject (organizationId, courseId);
create index IX_88F96CB5 on SubjectCourse_EduPortal_CourseSubject (subjectId);

create index IX_72600E8B on SubjectCourse_EduPortal_Subject (companyId);
create index IX_A302FF1F on SubjectCourse_EduPortal_Subject (organizationId);
create index IX_19C394CC on SubjectCourse_EduPortal_Subject (subjectCode);
create index IX_6CB937BA on SubjectCourse_EduPortal_Subject (subjectId);
create index IX_2BC7B1EA on SubjectCourse_EduPortal_Subject (subjectName);

create index IX_BBBDCC0E on SubjectCourse_Subject (companyId);
create index IX_F3E3B7C on SubjectCourse_Subject (organizationId);
create index IX_82A7FD8F on SubjectCourse_Subject (subjectCode);
create index IX_B616F53D on SubjectCourse_Subject (subjectId);
create index IX_94AC1AAD on SubjectCourse_Subject (subjectName);
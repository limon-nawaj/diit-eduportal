create table EduPortal_SubjectCourse_Course (
	courseId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	courseCode VARCHAR(75) null,
	courseName VARCHAR(75) null,
	description VARCHAR(75) null
);

create table EduPortal_SubjectCourse_CourseOrganization (
	courseOrganizationId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	courseId LONG
);

create table EduPortal_SubjectCourse_CourseSession (
	sessionId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	sessionName VARCHAR(75) null,
	courseId LONG
);

create table EduPortal_SubjectCourse_CourseSubject (
	courseSubjectId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	courseId LONG,
	subjectId LONG
);

create table EduPortal_SubjectCourse_Subject (
	subjectId LONG not null primary key IDENTITY,
	companyId LONG,
	organizationId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	subjectCode VARCHAR(75) null,
	subjectName VARCHAR(75) null,
	description VARCHAR(75) null
);
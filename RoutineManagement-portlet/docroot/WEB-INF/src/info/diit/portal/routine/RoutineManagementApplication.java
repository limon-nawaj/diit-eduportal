package info.diit.portal.routine;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.vaadin.Application;
import com.vaadin.addon.calendar.event.BasicEvent;
import com.vaadin.addon.calendar.event.CalendarEvent;
import com.vaadin.addon.calendar.ui.Calendar;
import com.vaadin.addon.calendar.ui.CalendarComponentEvents.RangeSelectEvent;
import com.vaadin.addon.calendar.ui.CalendarComponentEvents.RangeSelectHandler;
import com.vaadin.addon.calendar.ui.CalendarDateRange;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class RoutineManagementApplication extends Application {

	private Window window;
	
	public void init() {
		window = new Window();

		setMainWindow(window);
		window.addComponent(mainLayout());
	}
	
	private GridLayout mainLayout;
	BeanItemContainer<BasicEvent> container;
	
	private GridLayout mainLayout(){
		mainLayout = new GridLayout(8, 8);
		mainLayout.setWidth("100%");
		
		container = new BeanItemContainer<BasicEvent>(BasicEvent.class);
		
		final Calendar calendar = new Calendar("Routine");
		calendar.setWidth("100%");
		
		calendar.setContainerDataSource(container, "caption","description", "start", "end", "styleName");
		
		Action.Handler actionHandler = new Action.Handler() {
			
			Action addEventAction = new Action("Add Event");
			Action deleteEventAction = new Action("Delete");
			
			@Override
			public void handleAction(Action action, Object sender,
					Object target) {
                Calendar calendar = (Calendar) sender;

                if (action == addEventAction) {
                    if (target instanceof Date) {
                        Date date = (Date) target;
                        GregorianCalendar start = new GregorianCalendar();
                        start.setTime(date);
                        GregorianCalendar end   = new GregorianCalendar();
                        end.setTime(date);
                        end.add(java.util.Calendar.HOUR, 1);
                        calendar.addEvent(new BasicEvent("Calendar study",
                                "Learning how to use Vaadin Calendar",
                                start.getTime(), end.getTime()));
                    } else
                        window.showNotification("Can't add on an event");
                } else if (action == deleteEventAction) {
                    if (target instanceof CalendarEvent) {
                        CalendarEvent event = (CalendarEvent) target;
                        calendar.removeEvent(event);
                    } else
                        window.showNotification("No event to delete");
                }
				
			}
			
			@Override
			public Action[] getActions(Object target, Object sender) {
				if (! (target instanceof CalendarDateRange))
                    return null;
                CalendarDateRange dateRange = (CalendarDateRange) target;

                if (! (sender instanceof Calendar))
                    return null;
                Calendar calendar = (Calendar) sender;
                
                List<CalendarEvent> events = calendar.getEvents(dateRange.getStart(), dateRange.getEnd());
                
                if (events.size() == 0)
                    return new Action[] {addEventAction};
                else
                    return new Action[] {addEventAction, deleteEventAction};
			}
		};
		
		calendar.addActionHandler(actionHandler);
		
		
		calendar.setHandler(new RangeSelectHandler() {
			public void rangeSelect(RangeSelectEvent event) {
			BasicEvent calendarEvent = new BasicEvent();
			calendarEvent.setStart(event.getStart());
			calendarEvent.setEnd(event.getEnd());
			// Create popup window and add a form in it.
			VerticalLayout layout = new VerticalLayout();
			layout.setMargin(true);
			layout.setSpacing(true);
			final Window w = new Window(null, layout);
			//...
			// Wrap the calendar event to a BeanItem
			// and pass it to the form
			
			final BeanItem<BasicEvent> item = new BeanItem<BasicEvent>(calendarEvent);
			final Form form = new Form();
			form.setItemDataSource(item);
			//...
			layout.addComponent(form);
			HorizontalLayout buttons = new HorizontalLayout();
			buttons.setSpacing(true);
			buttons.addComponent(new Button("OK", new ClickListener() {
			public void buttonClick(ClickEvent event) {
			form.commit();
			// Update event provider's data source
			container.addBean(item.getBean());
			// Calendar needs to be repainted
			calendar.requestRepaint();
			getMainWindow().removeWindow(w);
			}
			}));
			
			layout.addComponent(buttons);
			w.setWidth("400px");
			w.center();
			w.setModal(true);
			getMainWindow().addWindow(w);
			//...
			}
			});
		
		
		/*container = new BeanItemContainer<BasicEvent>(BasicEvent.class);
		
		container.addBean(new BasicEvent("The Event", "Single Event", 
				new GregorianCalendar(2012,1,14,12,00).getTime(),
				new GregorianCalendar(2012,1,14,14,00).getTime()));
		calendar.setContainerDataSource(container, "caption", "description", "start", "end", "styleName");
		container.sort(new Object[]{"start"}, new boolean[]{true});*/
		
		/*GregorianCalendar start = new GregorianCalendar();
		GregorianCalendar end = new GregorianCalendar();
		end.add(java.util.Calendar.HOUR, 2);
		calendar.addEvent(new BasicEvent("Calendar study", "Learning how to use Vaadin Calendar", start.getTime(), end.getTime()));*/
				
		mainLayout.addComponent(calendar, 0, 1, 7, 1);
		return mainLayout;
	}

}
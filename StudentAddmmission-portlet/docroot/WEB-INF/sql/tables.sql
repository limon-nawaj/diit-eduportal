create table student_AcademicRecord (
	academicRecordId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organisationId LONG,
	degree VARCHAR(75) null,
	board VARCHAR(75) null,
	year INTEGER,
	result VARCHAR(75) null,
	registrationNo VARCHAR(75) null
);

create table student_AccademicRecord (
	accademicRecordId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organisationId LONG,
	degree VARCHAR(75) null,
	board VARCHAR(75) null,
	year INTEGER,
	result VARCHAR(75) null,
	registrationNo VARCHAR(75) null
);

create table student_Batch (
	batchId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	batchSession VARCHAR(75) null,
	startDate DATE null,
	endDate DATE null,
	courseCodeId LONG,
	note VARCHAR(75) null,
	status VARCHAR(75) null
);

create table student_Experiance (
	experianceId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	organization VARCHAR(75) null,
	designation VARCHAR(75) null,
	startDate DATE null,
	endDate DATE null,
	currentStatus BOOLEAN
);

create table student_Student (
	studentId LONG not null primary key IDENTITY,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	name VARCHAR(75) null,
	fatherName VARCHAR(75) null,
	motherName VARCHAR(75) null,
	presentAddress VARCHAR(75) null,
	permanentAddress VARCHAR(75) null,
	homePhone VARCHAR(75) null,
	fatherMobile VARCHAR(75) null,
	motherMobile VARCHAR(75) null,
	guardianMobile VARCHAR(75) null,
	Mobile1 VARCHAR(75) null,
	Mobile2 VARCHAR(75) null,
	dateOfBirth DATE null,
	email VARCHAR(75) null,
	gender VARCHAR(75) null,
	religion VARCHAR(75) null,
	nationality VARCHAR(75) null,
	photo BLOB,
	status VARCHAR(75) null
);

create table student_StudentDocument (
	documentId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	organizationId LONG,
	type_ VARCHAR(75) null,
	documentData BLOB,
	refferenceNo LONG
);
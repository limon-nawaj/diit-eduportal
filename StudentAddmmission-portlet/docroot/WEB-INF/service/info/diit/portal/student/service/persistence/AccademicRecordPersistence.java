/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.student.model.AccademicRecord;

/**
 * The persistence interface for the accademic record service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see AccademicRecordPersistenceImpl
 * @see AccademicRecordUtil
 * @generated
 */
public interface AccademicRecordPersistence extends BasePersistence<AccademicRecord> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AccademicRecordUtil} to access the accademic record persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the accademic record in the entity cache if it is enabled.
	*
	* @param accademicRecord the accademic record
	*/
	public void cacheResult(
		info.diit.portal.student.model.AccademicRecord accademicRecord);

	/**
	* Caches the accademic records in the entity cache if it is enabled.
	*
	* @param accademicRecords the accademic records
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.student.model.AccademicRecord> accademicRecords);

	/**
	* Creates a new accademic record with the primary key. Does not add the accademic record to the database.
	*
	* @param accademicRecordId the primary key for the new accademic record
	* @return the new accademic record
	*/
	public info.diit.portal.student.model.AccademicRecord create(
		long accademicRecordId);

	/**
	* Removes the accademic record with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param accademicRecordId the primary key of the accademic record
	* @return the accademic record that was removed
	* @throws info.diit.portal.student.NoSuchAccademicRecordException if a accademic record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.AccademicRecord remove(
		long accademicRecordId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchAccademicRecordException;

	public info.diit.portal.student.model.AccademicRecord updateImpl(
		info.diit.portal.student.model.AccademicRecord accademicRecord,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the accademic record with the primary key or throws a {@link info.diit.portal.student.NoSuchAccademicRecordException} if it could not be found.
	*
	* @param accademicRecordId the primary key of the accademic record
	* @return the accademic record
	* @throws info.diit.portal.student.NoSuchAccademicRecordException if a accademic record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.AccademicRecord findByPrimaryKey(
		long accademicRecordId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchAccademicRecordException;

	/**
	* Returns the accademic record with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param accademicRecordId the primary key of the accademic record
	* @return the accademic record, or <code>null</code> if a accademic record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.AccademicRecord fetchByPrimaryKey(
		long accademicRecordId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the accademic records.
	*
	* @return the accademic records
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.AccademicRecord> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the accademic records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of accademic records
	* @param end the upper bound of the range of accademic records (not inclusive)
	* @return the range of accademic records
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.AccademicRecord> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the accademic records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of accademic records
	* @param end the upper bound of the range of accademic records (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of accademic records
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.AccademicRecord> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the accademic records from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of accademic records.
	*
	* @return the number of accademic records
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
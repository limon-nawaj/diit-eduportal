/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model;

import com.liferay.portal.model.ModelWrapper;

import java.sql.Blob;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Student}.
 * </p>
 *
 * @author    nasimul
 * @see       Student
 * @generated
 */
public class StudentWrapper implements Student, ModelWrapper<Student> {
	public StudentWrapper(Student student) {
		_student = student;
	}

	public Class<?> getModelClass() {
		return Student.class;
	}

	public String getModelClassName() {
		return Student.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("studentId", getStudentId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("name", getName());
		attributes.put("fatherName", getFatherName());
		attributes.put("motherName", getMotherName());
		attributes.put("presentAddress", getPresentAddress());
		attributes.put("permanentAddress", getPermanentAddress());
		attributes.put("homePhone", getHomePhone());
		attributes.put("fatherMobile", getFatherMobile());
		attributes.put("motherMobile", getMotherMobile());
		attributes.put("guardianMobile", getGuardianMobile());
		attributes.put("Mobile1", getMobile1());
		attributes.put("Mobile2", getMobile2());
		attributes.put("dateOfBirth", getDateOfBirth());
		attributes.put("email", getEmail());
		attributes.put("gender", getGender());
		attributes.put("religion", getReligion());
		attributes.put("nationality", getNationality());
		attributes.put("photo", getPhoto());
		attributes.put("status", getStatus());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String fatherName = (String)attributes.get("fatherName");

		if (fatherName != null) {
			setFatherName(fatherName);
		}

		String motherName = (String)attributes.get("motherName");

		if (motherName != null) {
			setMotherName(motherName);
		}

		String presentAddress = (String)attributes.get("presentAddress");

		if (presentAddress != null) {
			setPresentAddress(presentAddress);
		}

		String permanentAddress = (String)attributes.get("permanentAddress");

		if (permanentAddress != null) {
			setPermanentAddress(permanentAddress);
		}

		String homePhone = (String)attributes.get("homePhone");

		if (homePhone != null) {
			setHomePhone(homePhone);
		}

		String fatherMobile = (String)attributes.get("fatherMobile");

		if (fatherMobile != null) {
			setFatherMobile(fatherMobile);
		}

		String motherMobile = (String)attributes.get("motherMobile");

		if (motherMobile != null) {
			setMotherMobile(motherMobile);
		}

		String guardianMobile = (String)attributes.get("guardianMobile");

		if (guardianMobile != null) {
			setGuardianMobile(guardianMobile);
		}

		String Mobile1 = (String)attributes.get("Mobile1");

		if (Mobile1 != null) {
			setMobile1(Mobile1);
		}

		String Mobile2 = (String)attributes.get("Mobile2");

		if (Mobile2 != null) {
			setMobile2(Mobile2);
		}

		Date dateOfBirth = (Date)attributes.get("dateOfBirth");

		if (dateOfBirth != null) {
			setDateOfBirth(dateOfBirth);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String gender = (String)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		String religion = (String)attributes.get("religion");

		if (religion != null) {
			setReligion(religion);
		}

		String nationality = (String)attributes.get("nationality");

		if (nationality != null) {
			setNationality(nationality);
		}

		Blob photo = (Blob)attributes.get("photo");

		if (photo != null) {
			setPhoto(photo);
		}

		String status = (String)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	/**
	* Returns the primary key of this student.
	*
	* @return the primary key of this student
	*/
	public long getPrimaryKey() {
		return _student.getPrimaryKey();
	}

	/**
	* Sets the primary key of this student.
	*
	* @param primaryKey the primary key of this student
	*/
	public void setPrimaryKey(long primaryKey) {
		_student.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the student ID of this student.
	*
	* @return the student ID of this student
	*/
	public long getStudentId() {
		return _student.getStudentId();
	}

	/**
	* Sets the student ID of this student.
	*
	* @param studentId the student ID of this student
	*/
	public void setStudentId(long studentId) {
		_student.setStudentId(studentId);
	}

	/**
	* Returns the company ID of this student.
	*
	* @return the company ID of this student
	*/
	public long getCompanyId() {
		return _student.getCompanyId();
	}

	/**
	* Sets the company ID of this student.
	*
	* @param companyId the company ID of this student
	*/
	public void setCompanyId(long companyId) {
		_student.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this student.
	*
	* @return the user ID of this student
	*/
	public long getUserId() {
		return _student.getUserId();
	}

	/**
	* Sets the user ID of this student.
	*
	* @param userId the user ID of this student
	*/
	public void setUserId(long userId) {
		_student.setUserId(userId);
	}

	/**
	* Returns the user uuid of this student.
	*
	* @return the user uuid of this student
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _student.getUserUuid();
	}

	/**
	* Sets the user uuid of this student.
	*
	* @param userUuid the user uuid of this student
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_student.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this student.
	*
	* @return the user name of this student
	*/
	public java.lang.String getUserName() {
		return _student.getUserName();
	}

	/**
	* Sets the user name of this student.
	*
	* @param userName the user name of this student
	*/
	public void setUserName(java.lang.String userName) {
		_student.setUserName(userName);
	}

	/**
	* Returns the create date of this student.
	*
	* @return the create date of this student
	*/
	public java.util.Date getCreateDate() {
		return _student.getCreateDate();
	}

	/**
	* Sets the create date of this student.
	*
	* @param createDate the create date of this student
	*/
	public void setCreateDate(java.util.Date createDate) {
		_student.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this student.
	*
	* @return the modified date of this student
	*/
	public java.util.Date getModifiedDate() {
		return _student.getModifiedDate();
	}

	/**
	* Sets the modified date of this student.
	*
	* @param modifiedDate the modified date of this student
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_student.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the organization ID of this student.
	*
	* @return the organization ID of this student
	*/
	public long getOrganizationId() {
		return _student.getOrganizationId();
	}

	/**
	* Sets the organization ID of this student.
	*
	* @param organizationId the organization ID of this student
	*/
	public void setOrganizationId(long organizationId) {
		_student.setOrganizationId(organizationId);
	}

	/**
	* Returns the name of this student.
	*
	* @return the name of this student
	*/
	public java.lang.String getName() {
		return _student.getName();
	}

	/**
	* Sets the name of this student.
	*
	* @param name the name of this student
	*/
	public void setName(java.lang.String name) {
		_student.setName(name);
	}

	/**
	* Returns the father name of this student.
	*
	* @return the father name of this student
	*/
	public java.lang.String getFatherName() {
		return _student.getFatherName();
	}

	/**
	* Sets the father name of this student.
	*
	* @param fatherName the father name of this student
	*/
	public void setFatherName(java.lang.String fatherName) {
		_student.setFatherName(fatherName);
	}

	/**
	* Returns the mother name of this student.
	*
	* @return the mother name of this student
	*/
	public java.lang.String getMotherName() {
		return _student.getMotherName();
	}

	/**
	* Sets the mother name of this student.
	*
	* @param motherName the mother name of this student
	*/
	public void setMotherName(java.lang.String motherName) {
		_student.setMotherName(motherName);
	}

	/**
	* Returns the present address of this student.
	*
	* @return the present address of this student
	*/
	public java.lang.String getPresentAddress() {
		return _student.getPresentAddress();
	}

	/**
	* Sets the present address of this student.
	*
	* @param presentAddress the present address of this student
	*/
	public void setPresentAddress(java.lang.String presentAddress) {
		_student.setPresentAddress(presentAddress);
	}

	/**
	* Returns the permanent address of this student.
	*
	* @return the permanent address of this student
	*/
	public java.lang.String getPermanentAddress() {
		return _student.getPermanentAddress();
	}

	/**
	* Sets the permanent address of this student.
	*
	* @param permanentAddress the permanent address of this student
	*/
	public void setPermanentAddress(java.lang.String permanentAddress) {
		_student.setPermanentAddress(permanentAddress);
	}

	/**
	* Returns the home phone of this student.
	*
	* @return the home phone of this student
	*/
	public java.lang.String getHomePhone() {
		return _student.getHomePhone();
	}

	/**
	* Sets the home phone of this student.
	*
	* @param homePhone the home phone of this student
	*/
	public void setHomePhone(java.lang.String homePhone) {
		_student.setHomePhone(homePhone);
	}

	/**
	* Returns the father mobile of this student.
	*
	* @return the father mobile of this student
	*/
	public java.lang.String getFatherMobile() {
		return _student.getFatherMobile();
	}

	/**
	* Sets the father mobile of this student.
	*
	* @param fatherMobile the father mobile of this student
	*/
	public void setFatherMobile(java.lang.String fatherMobile) {
		_student.setFatherMobile(fatherMobile);
	}

	/**
	* Returns the mother mobile of this student.
	*
	* @return the mother mobile of this student
	*/
	public java.lang.String getMotherMobile() {
		return _student.getMotherMobile();
	}

	/**
	* Sets the mother mobile of this student.
	*
	* @param motherMobile the mother mobile of this student
	*/
	public void setMotherMobile(java.lang.String motherMobile) {
		_student.setMotherMobile(motherMobile);
	}

	/**
	* Returns the guardian mobile of this student.
	*
	* @return the guardian mobile of this student
	*/
	public java.lang.String getGuardianMobile() {
		return _student.getGuardianMobile();
	}

	/**
	* Sets the guardian mobile of this student.
	*
	* @param guardianMobile the guardian mobile of this student
	*/
	public void setGuardianMobile(java.lang.String guardianMobile) {
		_student.setGuardianMobile(guardianMobile);
	}

	/**
	* Returns the mobile1 of this student.
	*
	* @return the mobile1 of this student
	*/
	public java.lang.String getMobile1() {
		return _student.getMobile1();
	}

	/**
	* Sets the mobile1 of this student.
	*
	* @param Mobile1 the mobile1 of this student
	*/
	public void setMobile1(java.lang.String Mobile1) {
		_student.setMobile1(Mobile1);
	}

	/**
	* Returns the mobile2 of this student.
	*
	* @return the mobile2 of this student
	*/
	public java.lang.String getMobile2() {
		return _student.getMobile2();
	}

	/**
	* Sets the mobile2 of this student.
	*
	* @param Mobile2 the mobile2 of this student
	*/
	public void setMobile2(java.lang.String Mobile2) {
		_student.setMobile2(Mobile2);
	}

	/**
	* Returns the date of birth of this student.
	*
	* @return the date of birth of this student
	*/
	public java.util.Date getDateOfBirth() {
		return _student.getDateOfBirth();
	}

	/**
	* Sets the date of birth of this student.
	*
	* @param dateOfBirth the date of birth of this student
	*/
	public void setDateOfBirth(java.util.Date dateOfBirth) {
		_student.setDateOfBirth(dateOfBirth);
	}

	/**
	* Returns the email of this student.
	*
	* @return the email of this student
	*/
	public java.lang.String getEmail() {
		return _student.getEmail();
	}

	/**
	* Sets the email of this student.
	*
	* @param email the email of this student
	*/
	public void setEmail(java.lang.String email) {
		_student.setEmail(email);
	}

	/**
	* Returns the gender of this student.
	*
	* @return the gender of this student
	*/
	public java.lang.String getGender() {
		return _student.getGender();
	}

	/**
	* Sets the gender of this student.
	*
	* @param gender the gender of this student
	*/
	public void setGender(java.lang.String gender) {
		_student.setGender(gender);
	}

	/**
	* Returns the religion of this student.
	*
	* @return the religion of this student
	*/
	public java.lang.String getReligion() {
		return _student.getReligion();
	}

	/**
	* Sets the religion of this student.
	*
	* @param religion the religion of this student
	*/
	public void setReligion(java.lang.String religion) {
		_student.setReligion(religion);
	}

	/**
	* Returns the nationality of this student.
	*
	* @return the nationality of this student
	*/
	public java.lang.String getNationality() {
		return _student.getNationality();
	}

	/**
	* Sets the nationality of this student.
	*
	* @param nationality the nationality of this student
	*/
	public void setNationality(java.lang.String nationality) {
		_student.setNationality(nationality);
	}

	/**
	* Returns the photo of this student.
	*
	* @return the photo of this student
	*/
	public java.sql.Blob getPhoto() {
		return _student.getPhoto();
	}

	/**
	* Sets the photo of this student.
	*
	* @param photo the photo of this student
	*/
	public void setPhoto(java.sql.Blob photo) {
		_student.setPhoto(photo);
	}

	/**
	* Returns the status of this student.
	*
	* @return the status of this student
	*/
	public java.lang.String getStatus() {
		return _student.getStatus();
	}

	/**
	* Sets the status of this student.
	*
	* @param status the status of this student
	*/
	public void setStatus(java.lang.String status) {
		_student.setStatus(status);
	}

	public boolean isNew() {
		return _student.isNew();
	}

	public void setNew(boolean n) {
		_student.setNew(n);
	}

	public boolean isCachedModel() {
		return _student.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_student.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _student.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _student.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_student.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _student.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_student.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new StudentWrapper((Student)_student.clone());
	}

	public int compareTo(info.diit.portal.student.model.Student student) {
		return _student.compareTo(student);
	}

	@Override
	public int hashCode() {
		return _student.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.student.model.Student> toCacheModel() {
		return _student.toCacheModel();
	}

	public info.diit.portal.student.model.Student toEscapedModel() {
		return new StudentWrapper(_student.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _student.toString();
	}

	public java.lang.String toXmlString() {
		return _student.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_student.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Student getWrappedStudent() {
		return _student;
	}

	public Student getWrappedModel() {
		return _student;
	}

	public void resetOriginalValues() {
		_student.resetOriginalValues();
	}

	private Student _student;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.student.model.Student;

/**
 * The persistence interface for the student service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see StudentPersistenceImpl
 * @see StudentUtil
 * @generated
 */
public interface StudentPersistence extends BasePersistence<Student> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link StudentUtil} to access the student persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the student in the entity cache if it is enabled.
	*
	* @param student the student
	*/
	public void cacheResult(info.diit.portal.student.model.Student student);

	/**
	* Caches the students in the entity cache if it is enabled.
	*
	* @param students the students
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.student.model.Student> students);

	/**
	* Creates a new student with the primary key. Does not add the student to the database.
	*
	* @param studentId the primary key for the new student
	* @return the new student
	*/
	public info.diit.portal.student.model.Student create(long studentId);

	/**
	* Removes the student with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param studentId the primary key of the student
	* @return the student that was removed
	* @throws info.diit.portal.student.NoSuchStudentException if a student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.Student remove(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchStudentException;

	public info.diit.portal.student.model.Student updateImpl(
		info.diit.portal.student.model.Student student, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student with the primary key or throws a {@link info.diit.portal.student.NoSuchStudentException} if it could not be found.
	*
	* @param studentId the primary key of the student
	* @return the student
	* @throws info.diit.portal.student.NoSuchStudentException if a student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.Student findByPrimaryKey(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchStudentException;

	/**
	* Returns the student with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param studentId the primary key of the student
	* @return the student, or <code>null</code> if a student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.Student fetchByPrimaryKey(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student where studentId = &#63; or throws a {@link info.diit.portal.student.NoSuchStudentException} if it could not be found.
	*
	* @param studentId the student ID
	* @return the matching student
	* @throws info.diit.portal.student.NoSuchStudentException if a matching student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.Student findByS_PHOTO(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchStudentException;

	/**
	* Returns the student where studentId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param studentId the student ID
	* @return the matching student, or <code>null</code> if a matching student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.Student fetchByS_PHOTO(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student where studentId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param studentId the student ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching student, or <code>null</code> if a matching student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.Student fetchByS_PHOTO(
		long studentId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the students.
	*
	* @return the students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Student> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the students.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of students
	* @param end the upper bound of the range of students (not inclusive)
	* @return the range of students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Student> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the students.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of students
	* @param end the upper bound of the range of students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Student> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the student where studentId = &#63; from the database.
	*
	* @param studentId the student ID
	* @return the student that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.Student removeByS_PHOTO(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchStudentException;

	/**
	* Removes all the students from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of students where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the number of matching students
	* @throws SystemException if a system exception occurred
	*/
	public int countByS_PHOTO(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of students.
	*
	* @return the number of students
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the academic records associated with the student.
	*
	* @param pk the primary key of the student
	* @return the academic records associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.AcademicRecord> getAcademicRecords(
		long pk) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the academic records associated with the student.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the student
	* @param start the lower bound of the range of students
	* @param end the upper bound of the range of students (not inclusive)
	* @return the range of academic records associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.AcademicRecord> getAcademicRecords(
		long pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the academic records associated with the student.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the student
	* @param start the lower bound of the range of students
	* @param end the upper bound of the range of students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of academic records associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.AcademicRecord> getAcademicRecords(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of academic records associated with the student.
	*
	* @param pk the primary key of the student
	* @return the number of academic records associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public int getAcademicRecordsSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the academic record is associated with the student.
	*
	* @param pk the primary key of the student
	* @param academicRecordPK the primary key of the academic record
	* @return <code>true</code> if the academic record is associated with the student; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsAcademicRecord(long pk, long academicRecordPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the student has any academic records associated with it.
	*
	* @param pk the primary key of the student to check for associations with academic records
	* @return <code>true</code> if the student has any academic records associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsAcademicRecords(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the student documents associated with the student.
	*
	* @param pk the primary key of the student
	* @return the student documents associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.StudentDocument> getStudentDocuments(
		long pk) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the student documents associated with the student.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the student
	* @param start the lower bound of the range of students
	* @param end the upper bound of the range of students (not inclusive)
	* @return the range of student documents associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.StudentDocument> getStudentDocuments(
		long pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the student documents associated with the student.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the student
	* @param start the lower bound of the range of students
	* @param end the upper bound of the range of students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of student documents associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.StudentDocument> getStudentDocuments(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of student documents associated with the student.
	*
	* @param pk the primary key of the student
	* @return the number of student documents associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public int getStudentDocumentsSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the student document is associated with the student.
	*
	* @param pk the primary key of the student
	* @param studentDocumentPK the primary key of the student document
	* @return <code>true</code> if the student document is associated with the student; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsStudentDocument(long pk, long studentDocumentPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the student has any student documents associated with it.
	*
	* @param pk the primary key of the student to check for associations with student documents
	* @return <code>true</code> if the student has any student documents associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsStudentDocuments(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the experiances associated with the student.
	*
	* @param pk the primary key of the student
	* @return the experiances associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Experiance> getExperiances(
		long pk) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the experiances associated with the student.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the student
	* @param start the lower bound of the range of students
	* @param end the upper bound of the range of students (not inclusive)
	* @return the range of experiances associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Experiance> getExperiances(
		long pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the experiances associated with the student.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the student
	* @param start the lower bound of the range of students
	* @param end the upper bound of the range of students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of experiances associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Experiance> getExperiances(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of experiances associated with the student.
	*
	* @param pk the primary key of the student
	* @return the number of experiances associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public int getExperiancesSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the experiance is associated with the student.
	*
	* @param pk the primary key of the student
	* @param experiancePK the primary key of the experiance
	* @return <code>true</code> if the experiance is associated with the student; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsExperiance(long pk, long experiancePK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the student has any experiances associated with it.
	*
	* @param pk the primary key of the student to check for associations with experiances
	* @return <code>true</code> if the student has any experiances associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsExperiances(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the batchs associated with the student.
	*
	* @param pk the primary key of the student
	* @return the batchs associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Batch> getBatchs(
		long pk) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batchs associated with the student.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the student
	* @param start the lower bound of the range of students
	* @param end the upper bound of the range of students (not inclusive)
	* @return the range of batchs associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Batch> getBatchs(
		long pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batchs associated with the student.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the student
	* @param start the lower bound of the range of students
	* @param end the upper bound of the range of students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of batchs associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Batch> getBatchs(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batchs associated with the student.
	*
	* @param pk the primary key of the student
	* @return the number of batchs associated with the student
	* @throws SystemException if a system exception occurred
	*/
	public int getBatchsSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the batch is associated with the student.
	*
	* @param pk the primary key of the student
	* @param batchPK the primary key of the batch
	* @return <code>true</code> if the batch is associated with the student; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsBatch(long pk, long batchPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the student has any batchs associated with it.
	*
	* @param pk the primary key of the student to check for associations with batchs
	* @return <code>true</code> if the student has any batchs associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsBatchs(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;
}
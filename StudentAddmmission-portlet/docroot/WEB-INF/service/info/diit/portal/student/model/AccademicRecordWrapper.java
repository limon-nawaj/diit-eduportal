/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AccademicRecord}.
 * </p>
 *
 * @author    nasimul
 * @see       AccademicRecord
 * @generated
 */
public class AccademicRecordWrapper implements AccademicRecord,
	ModelWrapper<AccademicRecord> {
	public AccademicRecordWrapper(AccademicRecord accademicRecord) {
		_accademicRecord = accademicRecord;
	}

	public Class<?> getModelClass() {
		return AccademicRecord.class;
	}

	public String getModelClassName() {
		return AccademicRecord.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("accademicRecordId", getAccademicRecordId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organisationId", getOrganisationId());
		attributes.put("degree", getDegree());
		attributes.put("board", getBoard());
		attributes.put("year", getYear());
		attributes.put("result", getResult());
		attributes.put("registrationNo", getRegistrationNo());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long accademicRecordId = (Long)attributes.get("accademicRecordId");

		if (accademicRecordId != null) {
			setAccademicRecordId(accademicRecordId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organisationId = (Long)attributes.get("organisationId");

		if (organisationId != null) {
			setOrganisationId(organisationId);
		}

		String degree = (String)attributes.get("degree");

		if (degree != null) {
			setDegree(degree);
		}

		String board = (String)attributes.get("board");

		if (board != null) {
			setBoard(board);
		}

		Integer year = (Integer)attributes.get("year");

		if (year != null) {
			setYear(year);
		}

		String result = (String)attributes.get("result");

		if (result != null) {
			setResult(result);
		}

		String registrationNo = (String)attributes.get("registrationNo");

		if (registrationNo != null) {
			setRegistrationNo(registrationNo);
		}
	}

	/**
	* Returns the primary key of this accademic record.
	*
	* @return the primary key of this accademic record
	*/
	public long getPrimaryKey() {
		return _accademicRecord.getPrimaryKey();
	}

	/**
	* Sets the primary key of this accademic record.
	*
	* @param primaryKey the primary key of this accademic record
	*/
	public void setPrimaryKey(long primaryKey) {
		_accademicRecord.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the accademic record ID of this accademic record.
	*
	* @return the accademic record ID of this accademic record
	*/
	public long getAccademicRecordId() {
		return _accademicRecord.getAccademicRecordId();
	}

	/**
	* Sets the accademic record ID of this accademic record.
	*
	* @param accademicRecordId the accademic record ID of this accademic record
	*/
	public void setAccademicRecordId(long accademicRecordId) {
		_accademicRecord.setAccademicRecordId(accademicRecordId);
	}

	/**
	* Returns the company ID of this accademic record.
	*
	* @return the company ID of this accademic record
	*/
	public long getCompanyId() {
		return _accademicRecord.getCompanyId();
	}

	/**
	* Sets the company ID of this accademic record.
	*
	* @param companyId the company ID of this accademic record
	*/
	public void setCompanyId(long companyId) {
		_accademicRecord.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this accademic record.
	*
	* @return the user ID of this accademic record
	*/
	public long getUserId() {
		return _accademicRecord.getUserId();
	}

	/**
	* Sets the user ID of this accademic record.
	*
	* @param userId the user ID of this accademic record
	*/
	public void setUserId(long userId) {
		_accademicRecord.setUserId(userId);
	}

	/**
	* Returns the user uuid of this accademic record.
	*
	* @return the user uuid of this accademic record
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _accademicRecord.getUserUuid();
	}

	/**
	* Sets the user uuid of this accademic record.
	*
	* @param userUuid the user uuid of this accademic record
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_accademicRecord.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this accademic record.
	*
	* @return the user name of this accademic record
	*/
	public java.lang.String getUserName() {
		return _accademicRecord.getUserName();
	}

	/**
	* Sets the user name of this accademic record.
	*
	* @param userName the user name of this accademic record
	*/
	public void setUserName(java.lang.String userName) {
		_accademicRecord.setUserName(userName);
	}

	/**
	* Returns the create date of this accademic record.
	*
	* @return the create date of this accademic record
	*/
	public java.util.Date getCreateDate() {
		return _accademicRecord.getCreateDate();
	}

	/**
	* Sets the create date of this accademic record.
	*
	* @param createDate the create date of this accademic record
	*/
	public void setCreateDate(java.util.Date createDate) {
		_accademicRecord.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this accademic record.
	*
	* @return the modified date of this accademic record
	*/
	public java.util.Date getModifiedDate() {
		return _accademicRecord.getModifiedDate();
	}

	/**
	* Sets the modified date of this accademic record.
	*
	* @param modifiedDate the modified date of this accademic record
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_accademicRecord.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the organisation ID of this accademic record.
	*
	* @return the organisation ID of this accademic record
	*/
	public long getOrganisationId() {
		return _accademicRecord.getOrganisationId();
	}

	/**
	* Sets the organisation ID of this accademic record.
	*
	* @param organisationId the organisation ID of this accademic record
	*/
	public void setOrganisationId(long organisationId) {
		_accademicRecord.setOrganisationId(organisationId);
	}

	/**
	* Returns the degree of this accademic record.
	*
	* @return the degree of this accademic record
	*/
	public java.lang.String getDegree() {
		return _accademicRecord.getDegree();
	}

	/**
	* Sets the degree of this accademic record.
	*
	* @param degree the degree of this accademic record
	*/
	public void setDegree(java.lang.String degree) {
		_accademicRecord.setDegree(degree);
	}

	/**
	* Returns the board of this accademic record.
	*
	* @return the board of this accademic record
	*/
	public java.lang.String getBoard() {
		return _accademicRecord.getBoard();
	}

	/**
	* Sets the board of this accademic record.
	*
	* @param board the board of this accademic record
	*/
	public void setBoard(java.lang.String board) {
		_accademicRecord.setBoard(board);
	}

	/**
	* Returns the year of this accademic record.
	*
	* @return the year of this accademic record
	*/
	public int getYear() {
		return _accademicRecord.getYear();
	}

	/**
	* Sets the year of this accademic record.
	*
	* @param year the year of this accademic record
	*/
	public void setYear(int year) {
		_accademicRecord.setYear(year);
	}

	/**
	* Returns the result of this accademic record.
	*
	* @return the result of this accademic record
	*/
	public java.lang.String getResult() {
		return _accademicRecord.getResult();
	}

	/**
	* Sets the result of this accademic record.
	*
	* @param result the result of this accademic record
	*/
	public void setResult(java.lang.String result) {
		_accademicRecord.setResult(result);
	}

	/**
	* Returns the registration no of this accademic record.
	*
	* @return the registration no of this accademic record
	*/
	public java.lang.String getRegistrationNo() {
		return _accademicRecord.getRegistrationNo();
	}

	/**
	* Sets the registration no of this accademic record.
	*
	* @param registrationNo the registration no of this accademic record
	*/
	public void setRegistrationNo(java.lang.String registrationNo) {
		_accademicRecord.setRegistrationNo(registrationNo);
	}

	public boolean isNew() {
		return _accademicRecord.isNew();
	}

	public void setNew(boolean n) {
		_accademicRecord.setNew(n);
	}

	public boolean isCachedModel() {
		return _accademicRecord.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_accademicRecord.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _accademicRecord.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _accademicRecord.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_accademicRecord.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _accademicRecord.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_accademicRecord.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AccademicRecordWrapper((AccademicRecord)_accademicRecord.clone());
	}

	public int compareTo(
		info.diit.portal.student.model.AccademicRecord accademicRecord) {
		return _accademicRecord.compareTo(accademicRecord);
	}

	@Override
	public int hashCode() {
		return _accademicRecord.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.student.model.AccademicRecord> toCacheModel() {
		return _accademicRecord.toCacheModel();
	}

	public info.diit.portal.student.model.AccademicRecord toEscapedModel() {
		return new AccademicRecordWrapper(_accademicRecord.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _accademicRecord.toString();
	}

	public java.lang.String toXmlString() {
		return _accademicRecord.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_accademicRecord.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public AccademicRecord getWrappedAccademicRecord() {
		return _accademicRecord;
	}

	public AccademicRecord getWrappedModel() {
		return _accademicRecord;
	}

	public void resetOriginalValues() {
		_accademicRecord.resetOriginalValues();
	}

	private AccademicRecord _accademicRecord;
}
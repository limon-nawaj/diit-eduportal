/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import info.diit.portal.student.model.AcademicRecordClp;
import info.diit.portal.student.model.BatchClp;
import info.diit.portal.student.model.ExperianceClp;
import info.diit.portal.student.model.StudentClp;
import info.diit.portal.student.model.StudentDocumentClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Brian Wing Shun Chan
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"StudentAddmmission-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"StudentAddmmission-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "StudentAddmmission-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(AcademicRecordClp.class.getName())) {
			return translateInputAcademicRecord(oldModel);
		}

		if (oldModelClassName.equals(BatchClp.class.getName())) {
			return translateInputBatch(oldModel);
		}

		if (oldModelClassName.equals(ExperianceClp.class.getName())) {
			return translateInputExperiance(oldModel);
		}

		if (oldModelClassName.equals(StudentClp.class.getName())) {
			return translateInputStudent(oldModel);
		}

		if (oldModelClassName.equals(StudentDocumentClp.class.getName())) {
			return translateInputStudentDocument(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputAcademicRecord(BaseModel<?> oldModel) {
		AcademicRecordClp oldClpModel = (AcademicRecordClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAcademicRecordRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputBatch(BaseModel<?> oldModel) {
		BatchClp oldClpModel = (BatchClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBatchRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputExperiance(BaseModel<?> oldModel) {
		ExperianceClp oldClpModel = (ExperianceClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getExperianceRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStudent(BaseModel<?> oldModel) {
		StudentClp oldClpModel = (StudentClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStudentRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStudentDocument(BaseModel<?> oldModel) {
		StudentDocumentClp oldClpModel = (StudentDocumentClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStudentDocumentRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(
					"info.diit.portal.student.model.impl.AcademicRecordImpl")) {
			return translateOutputAcademicRecord(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.student.model.impl.BatchImpl")) {
			return translateOutputBatch(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.student.model.impl.ExperianceImpl")) {
			return translateOutputExperiance(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.student.model.impl.StudentImpl")) {
			return translateOutputStudent(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.student.model.impl.StudentDocumentImpl")) {
			return translateOutputStudentDocument(oldModel);
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals(
					"info.diit.portal.student.NoSuchAcademicRecordException")) {
			return new info.diit.portal.student.NoSuchAcademicRecordException();
		}

		if (className.equals("info.diit.portal.student.NoSuchBatchException")) {
			return new info.diit.portal.student.NoSuchBatchException();
		}

		if (className.equals(
					"info.diit.portal.student.NoSuchExperianceException")) {
			return new info.diit.portal.student.NoSuchExperianceException();
		}

		if (className.equals("info.diit.portal.student.NoSuchStudentException")) {
			return new info.diit.portal.student.NoSuchStudentException();
		}

		if (className.equals(
					"info.diit.portal.student.NoSuchStudentDocumentException")) {
			return new info.diit.portal.student.NoSuchStudentDocumentException();
		}

		return throwable;
	}

	public static Object translateOutputAcademicRecord(BaseModel<?> oldModel) {
		AcademicRecordClp newModel = new AcademicRecordClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAcademicRecordRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputBatch(BaseModel<?> oldModel) {
		BatchClp newModel = new BatchClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBatchRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputExperiance(BaseModel<?> oldModel) {
		ExperianceClp newModel = new ExperianceClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setExperianceRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStudent(BaseModel<?> oldModel) {
		StudentClp newModel = new StudentClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStudentRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStudentDocument(BaseModel<?> oldModel) {
		StudentDocumentClp newModel = new StudentDocumentClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStudentDocumentRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}
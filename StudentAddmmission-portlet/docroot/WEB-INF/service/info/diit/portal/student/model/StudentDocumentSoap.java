/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model;

import java.io.Serializable;

import java.sql.Blob;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    nasimul
 * @generated
 */
public class StudentDocumentSoap implements Serializable {
	public static StudentDocumentSoap toSoapModel(StudentDocument model) {
		StudentDocumentSoap soapModel = new StudentDocumentSoap();

		soapModel.setDocumentId(model.getDocumentId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setType(model.getType());
		soapModel.setDocumentData(model.getDocumentData());
		soapModel.setRefferenceNo(model.getRefferenceNo());

		return soapModel;
	}

	public static StudentDocumentSoap[] toSoapModels(StudentDocument[] models) {
		StudentDocumentSoap[] soapModels = new StudentDocumentSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static StudentDocumentSoap[][] toSoapModels(
		StudentDocument[][] models) {
		StudentDocumentSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new StudentDocumentSoap[models.length][models[0].length];
		}
		else {
			soapModels = new StudentDocumentSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static StudentDocumentSoap[] toSoapModels(
		List<StudentDocument> models) {
		List<StudentDocumentSoap> soapModels = new ArrayList<StudentDocumentSoap>(models.size());

		for (StudentDocument model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new StudentDocumentSoap[soapModels.size()]);
	}

	public StudentDocumentSoap() {
	}

	public long getPrimaryKey() {
		return _documentId;
	}

	public void setPrimaryKey(long pk) {
		setDocumentId(pk);
	}

	public long getDocumentId() {
		return _documentId;
	}

	public void setDocumentId(long documentId) {
		_documentId = documentId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public String getType() {
		return _type;
	}

	public void setType(String type) {
		_type = type;
	}

	public Blob getDocumentData() {
		return _documentData;
	}

	public void setDocumentData(Blob documentData) {
		_documentData = documentData;
	}

	public long getRefferenceNo() {
		return _refferenceNo;
	}

	public void setRefferenceNo(long refferenceNo) {
		_refferenceNo = refferenceNo;
	}

	private long _documentId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private String _type;
	private Blob _documentData;
	private long _refferenceNo;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    nasimul
 * @generated
 */
public class BatchSoap implements Serializable {
	public static BatchSoap toSoapModel(Batch model) {
		BatchSoap soapModel = new BatchSoap();

		soapModel.setBatchId(model.getBatchId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setBatchSession(model.getBatchSession());
		soapModel.setStartDate(model.getStartDate());
		soapModel.setEndDate(model.getEndDate());
		soapModel.setCourseCodeId(model.getCourseCodeId());
		soapModel.setNote(model.getNote());
		soapModel.setStatus(model.getStatus());

		return soapModel;
	}

	public static BatchSoap[] toSoapModels(Batch[] models) {
		BatchSoap[] soapModels = new BatchSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BatchSoap[][] toSoapModels(Batch[][] models) {
		BatchSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BatchSoap[models.length][models[0].length];
		}
		else {
			soapModels = new BatchSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BatchSoap[] toSoapModels(List<Batch> models) {
		List<BatchSoap> soapModels = new ArrayList<BatchSoap>(models.size());

		for (Batch model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BatchSoap[soapModels.size()]);
	}

	public BatchSoap() {
	}

	public long getPrimaryKey() {
		return _batchId;
	}

	public void setPrimaryKey(long pk) {
		setBatchId(pk);
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public String getBatchSession() {
		return _batchSession;
	}

	public void setBatchSession(String batchSession) {
		_batchSession = batchSession;
	}

	public Date getStartDate() {
		return _startDate;
	}

	public void setStartDate(Date startDate) {
		_startDate = startDate;
	}

	public Date getEndDate() {
		return _endDate;
	}

	public void setEndDate(Date endDate) {
		_endDate = endDate;
	}

	public long getCourseCodeId() {
		return _courseCodeId;
	}

	public void setCourseCodeId(long courseCodeId) {
		_courseCodeId = courseCodeId;
	}

	public String getNote() {
		return _note;
	}

	public void setNote(String note) {
		_note = note;
	}

	public String getStatus() {
		return _status;
	}

	public void setStatus(String status) {
		_status = status;
	}

	private long _batchId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private String _batchSession;
	private Date _startDate;
	private Date _endDate;
	private long _courseCodeId;
	private String _note;
	private String _status;
}
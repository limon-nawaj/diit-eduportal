/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.student.model.StudentDocument;

/**
 * The persistence interface for the student document service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see StudentDocumentPersistenceImpl
 * @see StudentDocumentUtil
 * @generated
 */
public interface StudentDocumentPersistence extends BasePersistence<StudentDocument> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link StudentDocumentUtil} to access the student document persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the student document in the entity cache if it is enabled.
	*
	* @param studentDocument the student document
	*/
	public void cacheResult(
		info.diit.portal.student.model.StudentDocument studentDocument);

	/**
	* Caches the student documents in the entity cache if it is enabled.
	*
	* @param studentDocuments the student documents
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.student.model.StudentDocument> studentDocuments);

	/**
	* Creates a new student document with the primary key. Does not add the student document to the database.
	*
	* @param documentId the primary key for the new student document
	* @return the new student document
	*/
	public info.diit.portal.student.model.StudentDocument create(
		long documentId);

	/**
	* Removes the student document with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param documentId the primary key of the student document
	* @return the student document that was removed
	* @throws info.diit.portal.student.NoSuchStudentDocumentException if a student document with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.StudentDocument remove(
		long documentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchStudentDocumentException;

	public info.diit.portal.student.model.StudentDocument updateImpl(
		info.diit.portal.student.model.StudentDocument studentDocument,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student document with the primary key or throws a {@link info.diit.portal.student.NoSuchStudentDocumentException} if it could not be found.
	*
	* @param documentId the primary key of the student document
	* @return the student document
	* @throws info.diit.portal.student.NoSuchStudentDocumentException if a student document with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.StudentDocument findByPrimaryKey(
		long documentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchStudentDocumentException;

	/**
	* Returns the student document with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param documentId the primary key of the student document
	* @return the student document, or <code>null</code> if a student document with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.StudentDocument fetchByPrimaryKey(
		long documentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student document where documentId = &#63; or throws a {@link info.diit.portal.student.NoSuchStudentDocumentException} if it could not be found.
	*
	* @param documentId the document ID
	* @return the matching student document
	* @throws info.diit.portal.student.NoSuchStudentDocumentException if a matching student document could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.StudentDocument findBySTD_DOC(
		long documentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchStudentDocumentException;

	/**
	* Returns the student document where documentId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param documentId the document ID
	* @return the matching student document, or <code>null</code> if a matching student document could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.StudentDocument fetchBySTD_DOC(
		long documentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the student document where documentId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param documentId the document ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching student document, or <code>null</code> if a matching student document could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.StudentDocument fetchBySTD_DOC(
		long documentId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the student documents.
	*
	* @return the student documents
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.StudentDocument> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the student documents.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student documents
	* @param end the upper bound of the range of student documents (not inclusive)
	* @return the range of student documents
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.StudentDocument> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the student documents.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student documents
	* @param end the upper bound of the range of student documents (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of student documents
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.StudentDocument> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the student document where documentId = &#63; from the database.
	*
	* @param documentId the document ID
	* @return the student document that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.StudentDocument removeBySTD_DOC(
		long documentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchStudentDocumentException;

	/**
	* Removes all the student documents from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of student documents where documentId = &#63;.
	*
	* @param documentId the document ID
	* @return the number of matching student documents
	* @throws SystemException if a system exception occurred
	*/
	public int countBySTD_DOC(long documentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of student documents.
	*
	* @return the number of student documents
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    nasimul
 * @generated
 */
public class ExperianceSoap implements Serializable {
	public static ExperianceSoap toSoapModel(Experiance model) {
		ExperianceSoap soapModel = new ExperianceSoap();

		soapModel.setExperianceId(model.getExperianceId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setOrganization(model.getOrganization());
		soapModel.setDesignation(model.getDesignation());
		soapModel.setStartDate(model.getStartDate());
		soapModel.setEndDate(model.getEndDate());
		soapModel.setCurrentStatus(model.getCurrentStatus());

		return soapModel;
	}

	public static ExperianceSoap[] toSoapModels(Experiance[] models) {
		ExperianceSoap[] soapModels = new ExperianceSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ExperianceSoap[][] toSoapModels(Experiance[][] models) {
		ExperianceSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ExperianceSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ExperianceSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ExperianceSoap[] toSoapModels(List<Experiance> models) {
		List<ExperianceSoap> soapModels = new ArrayList<ExperianceSoap>(models.size());

		for (Experiance model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ExperianceSoap[soapModels.size()]);
	}

	public ExperianceSoap() {
	}

	public long getPrimaryKey() {
		return _experianceId;
	}

	public void setPrimaryKey(long pk) {
		setExperianceId(pk);
	}

	public long getExperianceId() {
		return _experianceId;
	}

	public void setExperianceId(long experianceId) {
		_experianceId = experianceId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public String getOrganization() {
		return _organization;
	}

	public void setOrganization(String organization) {
		_organization = organization;
	}

	public String getDesignation() {
		return _designation;
	}

	public void setDesignation(String designation) {
		_designation = designation;
	}

	public Date getStartDate() {
		return _startDate;
	}

	public void setStartDate(Date startDate) {
		_startDate = startDate;
	}

	public Date getEndDate() {
		return _endDate;
	}

	public void setEndDate(Date endDate) {
		_endDate = endDate;
	}

	public boolean getCurrentStatus() {
		return _currentStatus;
	}

	public boolean isCurrentStatus() {
		return _currentStatus;
	}

	public void setCurrentStatus(boolean currentStatus) {
		_currentStatus = currentStatus;
	}

	private long _experianceId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private String _organization;
	private String _designation;
	private Date _startDate;
	private Date _endDate;
	private boolean _currentStatus;
}
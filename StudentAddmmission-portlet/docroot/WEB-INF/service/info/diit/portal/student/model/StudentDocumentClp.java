/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.student.service.StudentDocumentLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Blob;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author nasimul
 */
public class StudentDocumentClp extends BaseModelImpl<StudentDocument>
	implements StudentDocument {
	public StudentDocumentClp() {
	}

	public Class<?> getModelClass() {
		return StudentDocument.class;
	}

	public String getModelClassName() {
		return StudentDocument.class.getName();
	}

	public long getPrimaryKey() {
		return _documentId;
	}

	public void setPrimaryKey(long primaryKey) {
		setDocumentId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_documentId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("documentId", getDocumentId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("type", getType());
		attributes.put("documentData", getDocumentData());
		attributes.put("refferenceNo", getRefferenceNo());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long documentId = (Long)attributes.get("documentId");

		if (documentId != null) {
			setDocumentId(documentId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		Blob documentData = (Blob)attributes.get("documentData");

		if (documentData != null) {
			setDocumentData(documentData);
		}

		Long refferenceNo = (Long)attributes.get("refferenceNo");

		if (refferenceNo != null) {
			setRefferenceNo(refferenceNo);
		}
	}

	public long getDocumentId() {
		return _documentId;
	}

	public void setDocumentId(long documentId) {
		_documentId = documentId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public String getType() {
		return _type;
	}

	public void setType(String type) {
		_type = type;
	}

	public Blob getDocumentData() {
		return _documentData;
	}

	public void setDocumentData(Blob documentData) {
		_documentData = documentData;
	}

	public long getRefferenceNo() {
		return _refferenceNo;
	}

	public void setRefferenceNo(long refferenceNo) {
		_refferenceNo = refferenceNo;
	}

	public BaseModel<?> getStudentDocumentRemoteModel() {
		return _studentDocumentRemoteModel;
	}

	public void setStudentDocumentRemoteModel(
		BaseModel<?> studentDocumentRemoteModel) {
		_studentDocumentRemoteModel = studentDocumentRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			StudentDocumentLocalServiceUtil.addStudentDocument(this);
		}
		else {
			StudentDocumentLocalServiceUtil.updateStudentDocument(this);
		}
	}

	@Override
	public StudentDocument toEscapedModel() {
		return (StudentDocument)Proxy.newProxyInstance(StudentDocument.class.getClassLoader(),
			new Class[] { StudentDocument.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		StudentDocumentClp clone = new StudentDocumentClp();

		clone.setDocumentId(getDocumentId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setOrganizationId(getOrganizationId());
		clone.setType(getType());
		clone.setDocumentData(getDocumentData());
		clone.setRefferenceNo(getRefferenceNo());

		return clone;
	}

	public int compareTo(StudentDocument studentDocument) {
		long primaryKey = studentDocument.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		StudentDocumentClp studentDocument = null;

		try {
			studentDocument = (StudentDocumentClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = studentDocument.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{documentId=");
		sb.append(getDocumentId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", type=");
		sb.append(getType());
		sb.append(", documentData=");
		sb.append(getDocumentData());
		sb.append(", refferenceNo=");
		sb.append(getRefferenceNo());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.student.model.StudentDocument");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>documentId</column-name><column-value><![CDATA[");
		sb.append(getDocumentId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>type</column-name><column-value><![CDATA[");
		sb.append(getType());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>documentData</column-name><column-value><![CDATA[");
		sb.append(getDocumentData());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>refferenceNo</column-name><column-value><![CDATA[");
		sb.append(getRefferenceNo());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _documentId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private String _type;
	private Blob _documentData;
	private long _refferenceNo;
	private BaseModel<?> _studentDocumentRemoteModel;
}
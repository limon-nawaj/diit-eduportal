/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.student.model.Batch;

import java.util.List;

/**
 * The persistence utility for the batch service. This utility wraps {@link BatchPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see BatchPersistence
 * @see BatchPersistenceImpl
 * @generated
 */
public class BatchUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Batch batch) {
		getPersistence().clearCache(batch);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Batch> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Batch> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Batch> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Batch update(Batch batch, boolean merge)
		throws SystemException {
		return getPersistence().update(batch, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Batch update(Batch batch, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(batch, merge, serviceContext);
	}

	/**
	* Caches the batch in the entity cache if it is enabled.
	*
	* @param batch the batch
	*/
	public static void cacheResult(info.diit.portal.student.model.Batch batch) {
		getPersistence().cacheResult(batch);
	}

	/**
	* Caches the batchs in the entity cache if it is enabled.
	*
	* @param batchs the batchs
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.student.model.Batch> batchs) {
		getPersistence().cacheResult(batchs);
	}

	/**
	* Creates a new batch with the primary key. Does not add the batch to the database.
	*
	* @param batchId the primary key for the new batch
	* @return the new batch
	*/
	public static info.diit.portal.student.model.Batch create(long batchId) {
		return getPersistence().create(batchId);
	}

	/**
	* Removes the batch with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchId the primary key of the batch
	* @return the batch that was removed
	* @throws info.diit.portal.student.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.model.Batch remove(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchBatchException {
		return getPersistence().remove(batchId);
	}

	public static info.diit.portal.student.model.Batch updateImpl(
		info.diit.portal.student.model.Batch batch, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(batch, merge);
	}

	/**
	* Returns the batch with the primary key or throws a {@link info.diit.portal.student.NoSuchBatchException} if it could not be found.
	*
	* @param batchId the primary key of the batch
	* @return the batch
	* @throws info.diit.portal.student.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.model.Batch findByPrimaryKey(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchBatchException {
		return getPersistence().findByPrimaryKey(batchId);
	}

	/**
	* Returns the batch with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param batchId the primary key of the batch
	* @return the batch, or <code>null</code> if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.student.model.Batch fetchByPrimaryKey(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(batchId);
	}

	/**
	* Returns all the batchs.
	*
	* @return the batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.model.Batch> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the batchs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @return the range of batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.model.Batch> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the batchs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.student.model.Batch> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the batchs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of batchs.
	*
	* @return the number of batchs
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static BatchPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (BatchPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.student.service.ClpSerializer.getServletContextName(),
					BatchPersistence.class.getName());

			ReferenceRegistry.registerReference(BatchUtil.class, "_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(BatchPersistence persistence) {
	}

	private static BatchPersistence _persistence;
}
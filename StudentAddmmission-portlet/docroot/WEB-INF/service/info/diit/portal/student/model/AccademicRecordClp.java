/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.student.service.AccademicRecordLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author nasimul
 */
public class AccademicRecordClp extends BaseModelImpl<AccademicRecord>
	implements AccademicRecord {
	public AccademicRecordClp() {
	}

	public Class<?> getModelClass() {
		return AccademicRecord.class;
	}

	public String getModelClassName() {
		return AccademicRecord.class.getName();
	}

	public long getPrimaryKey() {
		return _accademicRecordId;
	}

	public void setPrimaryKey(long primaryKey) {
		setAccademicRecordId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_accademicRecordId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("accademicRecordId", getAccademicRecordId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organisationId", getOrganisationId());
		attributes.put("degree", getDegree());
		attributes.put("board", getBoard());
		attributes.put("year", getYear());
		attributes.put("result", getResult());
		attributes.put("registrationNo", getRegistrationNo());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long accademicRecordId = (Long)attributes.get("accademicRecordId");

		if (accademicRecordId != null) {
			setAccademicRecordId(accademicRecordId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organisationId = (Long)attributes.get("organisationId");

		if (organisationId != null) {
			setOrganisationId(organisationId);
		}

		String degree = (String)attributes.get("degree");

		if (degree != null) {
			setDegree(degree);
		}

		String board = (String)attributes.get("board");

		if (board != null) {
			setBoard(board);
		}

		Integer year = (Integer)attributes.get("year");

		if (year != null) {
			setYear(year);
		}

		String result = (String)attributes.get("result");

		if (result != null) {
			setResult(result);
		}

		String registrationNo = (String)attributes.get("registrationNo");

		if (registrationNo != null) {
			setRegistrationNo(registrationNo);
		}
	}

	public long getAccademicRecordId() {
		return _accademicRecordId;
	}

	public void setAccademicRecordId(long accademicRecordId) {
		_accademicRecordId = accademicRecordId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganisationId() {
		return _organisationId;
	}

	public void setOrganisationId(long organisationId) {
		_organisationId = organisationId;
	}

	public String getDegree() {
		return _degree;
	}

	public void setDegree(String degree) {
		_degree = degree;
	}

	public String getBoard() {
		return _board;
	}

	public void setBoard(String board) {
		_board = board;
	}

	public int getYear() {
		return _year;
	}

	public void setYear(int year) {
		_year = year;
	}

	public String getResult() {
		return _result;
	}

	public void setResult(String result) {
		_result = result;
	}

	public String getRegistrationNo() {
		return _registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		_registrationNo = registrationNo;
	}

	public BaseModel<?> getAccademicRecordRemoteModel() {
		return _accademicRecordRemoteModel;
	}

	public void setAccademicRecordRemoteModel(
		BaseModel<?> accademicRecordRemoteModel) {
		_accademicRecordRemoteModel = accademicRecordRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			AccademicRecordLocalServiceUtil.addAccademicRecord(this);
		}
		else {
			AccademicRecordLocalServiceUtil.updateAccademicRecord(this);
		}
	}

	@Override
	public AccademicRecord toEscapedModel() {
		return (AccademicRecord)Proxy.newProxyInstance(AccademicRecord.class.getClassLoader(),
			new Class[] { AccademicRecord.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		AccademicRecordClp clone = new AccademicRecordClp();

		clone.setAccademicRecordId(getAccademicRecordId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setOrganisationId(getOrganisationId());
		clone.setDegree(getDegree());
		clone.setBoard(getBoard());
		clone.setYear(getYear());
		clone.setResult(getResult());
		clone.setRegistrationNo(getRegistrationNo());

		return clone;
	}

	public int compareTo(AccademicRecord accademicRecord) {
		long primaryKey = accademicRecord.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		AccademicRecordClp accademicRecord = null;

		try {
			accademicRecord = (AccademicRecordClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = accademicRecord.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{accademicRecordId=");
		sb.append(getAccademicRecordId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", organisationId=");
		sb.append(getOrganisationId());
		sb.append(", degree=");
		sb.append(getDegree());
		sb.append(", board=");
		sb.append(getBoard());
		sb.append(", year=");
		sb.append(getYear());
		sb.append(", result=");
		sb.append(getResult());
		sb.append(", registrationNo=");
		sb.append(getRegistrationNo());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(40);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.student.model.AccademicRecord");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>accademicRecordId</column-name><column-value><![CDATA[");
		sb.append(getAccademicRecordId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organisationId</column-name><column-value><![CDATA[");
		sb.append(getOrganisationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>degree</column-name><column-value><![CDATA[");
		sb.append(getDegree());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>board</column-name><column-value><![CDATA[");
		sb.append(getBoard());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>year</column-name><column-value><![CDATA[");
		sb.append(getYear());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>result</column-name><column-value><![CDATA[");
		sb.append(getResult());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>registrationNo</column-name><column-value><![CDATA[");
		sb.append(getRegistrationNo());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _accademicRecordId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organisationId;
	private String _degree;
	private String _board;
	private int _year;
	private String _result;
	private String _registrationNo;
	private BaseModel<?> _accademicRecordRemoteModel;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.student.model.Experiance;

/**
 * The persistence interface for the experiance service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see ExperiancePersistenceImpl
 * @see ExperianceUtil
 * @generated
 */
public interface ExperiancePersistence extends BasePersistence<Experiance> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ExperianceUtil} to access the experiance persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the experiance in the entity cache if it is enabled.
	*
	* @param experiance the experiance
	*/
	public void cacheResult(
		info.diit.portal.student.model.Experiance experiance);

	/**
	* Caches the experiances in the entity cache if it is enabled.
	*
	* @param experiances the experiances
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.student.model.Experiance> experiances);

	/**
	* Creates a new experiance with the primary key. Does not add the experiance to the database.
	*
	* @param experianceId the primary key for the new experiance
	* @return the new experiance
	*/
	public info.diit.portal.student.model.Experiance create(long experianceId);

	/**
	* Removes the experiance with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param experianceId the primary key of the experiance
	* @return the experiance that was removed
	* @throws info.diit.portal.student.NoSuchExperianceException if a experiance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.Experiance remove(long experianceId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchExperianceException;

	public info.diit.portal.student.model.Experiance updateImpl(
		info.diit.portal.student.model.Experiance experiance, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the experiance with the primary key or throws a {@link info.diit.portal.student.NoSuchExperianceException} if it could not be found.
	*
	* @param experianceId the primary key of the experiance
	* @return the experiance
	* @throws info.diit.portal.student.NoSuchExperianceException if a experiance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.Experiance findByPrimaryKey(
		long experianceId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchExperianceException;

	/**
	* Returns the experiance with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param experianceId the primary key of the experiance
	* @return the experiance, or <code>null</code> if a experiance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.Experiance fetchByPrimaryKey(
		long experianceId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the experiances.
	*
	* @return the experiances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Experiance> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the experiances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of experiances
	* @param end the upper bound of the range of experiances (not inclusive)
	* @return the range of experiances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Experiance> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the experiances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of experiances
	* @param end the upper bound of the range of experiances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of experiances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Experiance> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the experiances from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of experiances.
	*
	* @return the number of experiances
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
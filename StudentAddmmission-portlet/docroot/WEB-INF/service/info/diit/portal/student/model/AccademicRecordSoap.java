/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    nasimul
 * @generated
 */
public class AccademicRecordSoap implements Serializable {
	public static AccademicRecordSoap toSoapModel(AccademicRecord model) {
		AccademicRecordSoap soapModel = new AccademicRecordSoap();

		soapModel.setAccademicRecordId(model.getAccademicRecordId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setOrganisationId(model.getOrganisationId());
		soapModel.setDegree(model.getDegree());
		soapModel.setBoard(model.getBoard());
		soapModel.setYear(model.getYear());
		soapModel.setResult(model.getResult());
		soapModel.setRegistrationNo(model.getRegistrationNo());

		return soapModel;
	}

	public static AccademicRecordSoap[] toSoapModels(AccademicRecord[] models) {
		AccademicRecordSoap[] soapModels = new AccademicRecordSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AccademicRecordSoap[][] toSoapModels(
		AccademicRecord[][] models) {
		AccademicRecordSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AccademicRecordSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AccademicRecordSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AccademicRecordSoap[] toSoapModels(
		List<AccademicRecord> models) {
		List<AccademicRecordSoap> soapModels = new ArrayList<AccademicRecordSoap>(models.size());

		for (AccademicRecord model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AccademicRecordSoap[soapModels.size()]);
	}

	public AccademicRecordSoap() {
	}

	public long getPrimaryKey() {
		return _accademicRecordId;
	}

	public void setPrimaryKey(long pk) {
		setAccademicRecordId(pk);
	}

	public long getAccademicRecordId() {
		return _accademicRecordId;
	}

	public void setAccademicRecordId(long accademicRecordId) {
		_accademicRecordId = accademicRecordId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganisationId() {
		return _organisationId;
	}

	public void setOrganisationId(long organisationId) {
		_organisationId = organisationId;
	}

	public String getDegree() {
		return _degree;
	}

	public void setDegree(String degree) {
		_degree = degree;
	}

	public String getBoard() {
		return _board;
	}

	public void setBoard(String board) {
		_board = board;
	}

	public int getYear() {
		return _year;
	}

	public void setYear(int year) {
		_year = year;
	}

	public String getResult() {
		return _result;
	}

	public void setResult(String result) {
		_result = result;
	}

	public String getRegistrationNo() {
		return _registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		_registrationNo = registrationNo;
	}

	private long _accademicRecordId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organisationId;
	private String _degree;
	private String _board;
	private int _year;
	private String _result;
	private String _registrationNo;
}
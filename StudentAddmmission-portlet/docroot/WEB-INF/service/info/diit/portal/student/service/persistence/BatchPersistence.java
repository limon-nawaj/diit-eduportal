/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.student.model.Batch;

/**
 * The persistence interface for the batch service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see BatchPersistenceImpl
 * @see BatchUtil
 * @generated
 */
public interface BatchPersistence extends BasePersistence<Batch> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BatchUtil} to access the batch persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the batch in the entity cache if it is enabled.
	*
	* @param batch the batch
	*/
	public void cacheResult(info.diit.portal.student.model.Batch batch);

	/**
	* Caches the batchs in the entity cache if it is enabled.
	*
	* @param batchs the batchs
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.student.model.Batch> batchs);

	/**
	* Creates a new batch with the primary key. Does not add the batch to the database.
	*
	* @param batchId the primary key for the new batch
	* @return the new batch
	*/
	public info.diit.portal.student.model.Batch create(long batchId);

	/**
	* Removes the batch with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchId the primary key of the batch
	* @return the batch that was removed
	* @throws info.diit.portal.student.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.Batch remove(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchBatchException;

	public info.diit.portal.student.model.Batch updateImpl(
		info.diit.portal.student.model.Batch batch, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch with the primary key or throws a {@link info.diit.portal.student.NoSuchBatchException} if it could not be found.
	*
	* @param batchId the primary key of the batch
	* @return the batch
	* @throws info.diit.portal.student.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.Batch findByPrimaryKey(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.student.NoSuchBatchException;

	/**
	* Returns the batch with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param batchId the primary key of the batch
	* @return the batch, or <code>null</code> if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.student.model.Batch fetchByPrimaryKey(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the batchs.
	*
	* @return the batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Batch> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batchs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @return the range of batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Batch> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batchs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.student.model.Batch> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batchs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batchs.
	*
	* @return the number of batchs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
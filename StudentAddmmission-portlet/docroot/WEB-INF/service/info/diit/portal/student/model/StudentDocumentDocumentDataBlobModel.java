/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model;

import java.sql.Blob;

/**
 * The Blob model class for lazy loading the documentData column in StudentDocument.
 *
 * @author nasimul
 * @see StudentDocument
 * @generated
 */
public class StudentDocumentDocumentDataBlobModel {
	public StudentDocumentDocumentDataBlobModel() {
	}

	public StudentDocumentDocumentDataBlobModel(long documentId) {
		_documentId = documentId;
	}

	public StudentDocumentDocumentDataBlobModel(long documentId,
		Blob documentDataBlob) {
		_documentId = documentId;
		_documentDataBlob = documentDataBlob;
	}

	public long getDocumentId() {
		return _documentId;
	}

	public void setDocumentId(long documentId) {
		_documentId = documentId;
	}

	public Blob getDocumentDataBlob() {
		return _documentDataBlob;
	}

	public void setDocumentDataBlob(Blob documentDataBlob) {
		_documentDataBlob = documentDataBlob;
	}

	private long _documentId;
	private Blob _documentDataBlob;
}
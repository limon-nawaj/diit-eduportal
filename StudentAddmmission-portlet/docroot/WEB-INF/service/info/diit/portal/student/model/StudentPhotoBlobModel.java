/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model;

import java.sql.Blob;

/**
 * The Blob model class for lazy loading the photo column in Student.
 *
 * @author nasimul
 * @see Student
 * @generated
 */
public class StudentPhotoBlobModel {
	public StudentPhotoBlobModel() {
	}

	public StudentPhotoBlobModel(long studentId) {
		_studentId = studentId;
	}

	public StudentPhotoBlobModel(long studentId, Blob photoBlob) {
		_studentId = studentId;
		_photoBlob = photoBlob;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	public Blob getPhotoBlob() {
		return _photoBlob;
	}

	public void setPhotoBlob(Blob photoBlob) {
		_photoBlob = photoBlob;
	}

	private long _studentId;
	private Blob _photoBlob;
}
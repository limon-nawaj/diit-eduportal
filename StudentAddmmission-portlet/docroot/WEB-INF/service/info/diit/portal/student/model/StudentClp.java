/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.student.service.StudentLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.sql.Blob;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author nasimul
 */
public class StudentClp extends BaseModelImpl<Student> implements Student {
	public StudentClp() {
	}

	public Class<?> getModelClass() {
		return Student.class;
	}

	public String getModelClassName() {
		return Student.class.getName();
	}

	public long getPrimaryKey() {
		return _studentId;
	}

	public void setPrimaryKey(long primaryKey) {
		setStudentId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_studentId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("studentId", getStudentId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("name", getName());
		attributes.put("fatherName", getFatherName());
		attributes.put("motherName", getMotherName());
		attributes.put("presentAddress", getPresentAddress());
		attributes.put("permanentAddress", getPermanentAddress());
		attributes.put("homePhone", getHomePhone());
		attributes.put("fatherMobile", getFatherMobile());
		attributes.put("motherMobile", getMotherMobile());
		attributes.put("guardianMobile", getGuardianMobile());
		attributes.put("Mobile1", getMobile1());
		attributes.put("Mobile2", getMobile2());
		attributes.put("dateOfBirth", getDateOfBirth());
		attributes.put("email", getEmail());
		attributes.put("gender", getGender());
		attributes.put("religion", getReligion());
		attributes.put("nationality", getNationality());
		attributes.put("photo", getPhoto());
		attributes.put("status", getStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String fatherName = (String)attributes.get("fatherName");

		if (fatherName != null) {
			setFatherName(fatherName);
		}

		String motherName = (String)attributes.get("motherName");

		if (motherName != null) {
			setMotherName(motherName);
		}

		String presentAddress = (String)attributes.get("presentAddress");

		if (presentAddress != null) {
			setPresentAddress(presentAddress);
		}

		String permanentAddress = (String)attributes.get("permanentAddress");

		if (permanentAddress != null) {
			setPermanentAddress(permanentAddress);
		}

		String homePhone = (String)attributes.get("homePhone");

		if (homePhone != null) {
			setHomePhone(homePhone);
		}

		String fatherMobile = (String)attributes.get("fatherMobile");

		if (fatherMobile != null) {
			setFatherMobile(fatherMobile);
		}

		String motherMobile = (String)attributes.get("motherMobile");

		if (motherMobile != null) {
			setMotherMobile(motherMobile);
		}

		String guardianMobile = (String)attributes.get("guardianMobile");

		if (guardianMobile != null) {
			setGuardianMobile(guardianMobile);
		}

		String Mobile1 = (String)attributes.get("Mobile1");

		if (Mobile1 != null) {
			setMobile1(Mobile1);
		}

		String Mobile2 = (String)attributes.get("Mobile2");

		if (Mobile2 != null) {
			setMobile2(Mobile2);
		}

		Date dateOfBirth = (Date)attributes.get("dateOfBirth");

		if (dateOfBirth != null) {
			setDateOfBirth(dateOfBirth);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String gender = (String)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		String religion = (String)attributes.get("religion");

		if (religion != null) {
			setReligion(religion);
		}

		String nationality = (String)attributes.get("nationality");

		if (nationality != null) {
			setNationality(nationality);
		}

		Blob photo = (Blob)attributes.get("photo");

		if (photo != null) {
			setPhoto(photo);
		}

		String status = (String)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getFatherName() {
		return _fatherName;
	}

	public void setFatherName(String fatherName) {
		_fatherName = fatherName;
	}

	public String getMotherName() {
		return _motherName;
	}

	public void setMotherName(String motherName) {
		_motherName = motherName;
	}

	public String getPresentAddress() {
		return _presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		_presentAddress = presentAddress;
	}

	public String getPermanentAddress() {
		return _permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		_permanentAddress = permanentAddress;
	}

	public String getHomePhone() {
		return _homePhone;
	}

	public void setHomePhone(String homePhone) {
		_homePhone = homePhone;
	}

	public String getFatherMobile() {
		return _fatherMobile;
	}

	public void setFatherMobile(String fatherMobile) {
		_fatherMobile = fatherMobile;
	}

	public String getMotherMobile() {
		return _motherMobile;
	}

	public void setMotherMobile(String motherMobile) {
		_motherMobile = motherMobile;
	}

	public String getGuardianMobile() {
		return _guardianMobile;
	}

	public void setGuardianMobile(String guardianMobile) {
		_guardianMobile = guardianMobile;
	}

	public String getMobile1() {
		return _Mobile1;
	}

	public void setMobile1(String Mobile1) {
		_Mobile1 = Mobile1;
	}

	public String getMobile2() {
		return _Mobile2;
	}

	public void setMobile2(String Mobile2) {
		_Mobile2 = Mobile2;
	}

	public Date getDateOfBirth() {
		return _dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		_dateOfBirth = dateOfBirth;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getGender() {
		return _gender;
	}

	public void setGender(String gender) {
		_gender = gender;
	}

	public String getReligion() {
		return _religion;
	}

	public void setReligion(String religion) {
		_religion = religion;
	}

	public String getNationality() {
		return _nationality;
	}

	public void setNationality(String nationality) {
		_nationality = nationality;
	}

	public Blob getPhoto() {
		return _photo;
	}

	public void setPhoto(Blob photo) {
		_photo = photo;
	}

	public String getStatus() {
		return _status;
	}

	public void setStatus(String status) {
		_status = status;
	}

	public BaseModel<?> getStudentRemoteModel() {
		return _studentRemoteModel;
	}

	public void setStudentRemoteModel(BaseModel<?> studentRemoteModel) {
		_studentRemoteModel = studentRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			StudentLocalServiceUtil.addStudent(this);
		}
		else {
			StudentLocalServiceUtil.updateStudent(this);
		}
	}

	@Override
	public Student toEscapedModel() {
		return (Student)Proxy.newProxyInstance(Student.class.getClassLoader(),
			new Class[] { Student.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		StudentClp clone = new StudentClp();

		clone.setStudentId(getStudentId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setOrganizationId(getOrganizationId());
		clone.setName(getName());
		clone.setFatherName(getFatherName());
		clone.setMotherName(getMotherName());
		clone.setPresentAddress(getPresentAddress());
		clone.setPermanentAddress(getPermanentAddress());
		clone.setHomePhone(getHomePhone());
		clone.setFatherMobile(getFatherMobile());
		clone.setMotherMobile(getMotherMobile());
		clone.setGuardianMobile(getGuardianMobile());
		clone.setMobile1(getMobile1());
		clone.setMobile2(getMobile2());
		clone.setDateOfBirth(getDateOfBirth());
		clone.setEmail(getEmail());
		clone.setGender(getGender());
		clone.setReligion(getReligion());
		clone.setNationality(getNationality());
		clone.setPhoto(getPhoto());
		clone.setStatus(getStatus());

		return clone;
	}

	public int compareTo(Student student) {
		long primaryKey = student.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		StudentClp student = null;

		try {
			student = (StudentClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = student.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(51);

		sb.append("{studentId=");
		sb.append(getStudentId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", name=");
		sb.append(getName());
		sb.append(", fatherName=");
		sb.append(getFatherName());
		sb.append(", motherName=");
		sb.append(getMotherName());
		sb.append(", presentAddress=");
		sb.append(getPresentAddress());
		sb.append(", permanentAddress=");
		sb.append(getPermanentAddress());
		sb.append(", homePhone=");
		sb.append(getHomePhone());
		sb.append(", fatherMobile=");
		sb.append(getFatherMobile());
		sb.append(", motherMobile=");
		sb.append(getMotherMobile());
		sb.append(", guardianMobile=");
		sb.append(getGuardianMobile());
		sb.append(", Mobile1=");
		sb.append(getMobile1());
		sb.append(", Mobile2=");
		sb.append(getMobile2());
		sb.append(", dateOfBirth=");
		sb.append(getDateOfBirth());
		sb.append(", email=");
		sb.append(getEmail());
		sb.append(", gender=");
		sb.append(getGender());
		sb.append(", religion=");
		sb.append(getReligion());
		sb.append(", nationality=");
		sb.append(getNationality());
		sb.append(", photo=");
		sb.append(getPhoto());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(79);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.student.model.Student");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>studentId</column-name><column-value><![CDATA[");
		sb.append(getStudentId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fatherName</column-name><column-value><![CDATA[");
		sb.append(getFatherName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>motherName</column-name><column-value><![CDATA[");
		sb.append(getMotherName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>presentAddress</column-name><column-value><![CDATA[");
		sb.append(getPresentAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>permanentAddress</column-name><column-value><![CDATA[");
		sb.append(getPermanentAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>homePhone</column-name><column-value><![CDATA[");
		sb.append(getHomePhone());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fatherMobile</column-name><column-value><![CDATA[");
		sb.append(getFatherMobile());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>motherMobile</column-name><column-value><![CDATA[");
		sb.append(getMotherMobile());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>guardianMobile</column-name><column-value><![CDATA[");
		sb.append(getGuardianMobile());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>Mobile1</column-name><column-value><![CDATA[");
		sb.append(getMobile1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>Mobile2</column-name><column-value><![CDATA[");
		sb.append(getMobile2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dateOfBirth</column-name><column-value><![CDATA[");
		sb.append(getDateOfBirth());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>email</column-name><column-value><![CDATA[");
		sb.append(getEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>gender</column-name><column-value><![CDATA[");
		sb.append(getGender());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>religion</column-name><column-value><![CDATA[");
		sb.append(getReligion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nationality</column-name><column-value><![CDATA[");
		sb.append(getNationality());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>photo</column-name><column-value><![CDATA[");
		sb.append(getPhoto());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _studentId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private String _name;
	private String _fatherName;
	private String _motherName;
	private String _presentAddress;
	private String _permanentAddress;
	private String _homePhone;
	private String _fatherMobile;
	private String _motherMobile;
	private String _guardianMobile;
	private String _Mobile1;
	private String _Mobile2;
	private Date _dateOfBirth;
	private String _email;
	private String _gender;
	private String _religion;
	private String _nationality;
	private Blob _photo;
	private String _status;
	private BaseModel<?> _studentRemoteModel;
}
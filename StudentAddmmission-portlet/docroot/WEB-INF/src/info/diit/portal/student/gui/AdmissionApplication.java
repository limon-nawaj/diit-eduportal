package info.diit.portal.student.gui;

import info.diit.portal.student.dto.CustomSearchDto;
import info.diit.portal.student.model.Student;
import info.diit.portal.student.model.impl.StudentImpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;

import org.vaadin.tokenfield.TokenField;

import com.vaadin.Application;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.terminal.FileResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component.Event;
import com.vaadin.ui.Component.Listener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.StartedListener;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class AdmissionApplication extends Application implements Receiver,
		SucceededListener, FailedListener, StartedListener {
	
	private final static String COLUMN_COURSE 			= "courseName";
	private final static String COLUMN_BATCH_NAME 		= "batchName";
	private final static String COLUMN_STUDENT_ID 		= "studentId";
	private final static String COLUMN_STUDENT_NAME		= "studentName";
	
	BeanItemContainer<CustomSearchDto> customSearchContainer;
	
	public Window window;
	private File file;
	private Upload upload;
	Panel photoPanel;
	Window myPopWindow;
	private TabSheet tabSheet;
	
	private Table accademicRecordTable;
	private Table studentDocumentTable;
	private Table experienceTable;
	private Table batchTable;
	
	private Student student;

	public void init() {
		window = new Window();

		setMainWindow(window);
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.setSpacing(true);

		verticalLayout.addComponent(buildTabSheet());

		window.addComponent(verticalLayout);
	}
	
	public TabSheet buildTabSheet(){
		tabSheet = new TabSheet();
		tabSheet.addTab(initAdmissionForm(), "General Information");
		tabSheet.addTab(accademicRecordTable(), "Accademic Record");
		tabSheet.addTab(studentDocumentTable(), "Student Document");
		tabSheet.addTab(experienceTable(), "Experience");
		tabSheet.addTab(batchTable(), "Batch");
		tabSheet.addTab(easySearch(), "Easy Search");
		tabSheet.addTab(customSearch(), "Custom Search");
		return tabSheet;
	}
	
	private Button uploadButton;

	private GridLayout initAdmissionForm() {
		GridLayout admissionFormGridLayout = new GridLayout(11, 25);
		admissionFormGridLayout.setSpacing(true);
		admissionFormGridLayout.setWidth("100%");

		
		//left field
		final TextField studentNameField = new TextField("Student Name");
		final TextField fatherNameField = new TextField("Father's Name");
		final TextField motherNameField = new TextField("Mother's Name");
		final ComboBox genderOption = new ComboBox("Gender");
		final DateField dobField = new DateField("Date of Birth");
		final ComboBox batchComboBox = new ComboBox("Batch");
		
		genderOption.addItem("Male");
		genderOption.addItem("Female");
		batchComboBox.addItem("1");
		batchComboBox.addItem("2");
		
		
		//right token field		
		TokenField studentContactToken = new TokenField("Student Contact Number");
		TokenField fatherMobileToken = new TokenField("Father's Mobile");
		TokenField motherMobileToken = new TokenField("Mother's Mobile");
		TokenField emailToken = new TokenField("Email");
		TokenField homePhoneToken = new TokenField("Home Mobile");
		TokenField gardianContactToken = new TokenField("Gardian Contact Number");
		
		TextArea presentAdressArea = new TextArea("Present Adress");
		TextArea permanentAdressArea = new TextArea("Permanent Adress");
		
		VerticalLayout photoPanelLayout = new VerticalLayout();
		photoPanel = new Panel("Picture");
		photoPanel.setWidth("160px");
		photoPanel.setHeight("200px");
		photoPanelLayout.addComponent(photoPanel);
		
		uploadButton = new Button("Change");
		uploadButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				showPopupWindow("Stuent Addmission System - Photo Upload ");
			}
		});
		photoPanelLayout.addComponent(uploadButton);

		Button saveGeneralInfo = new Button("Save");
		saveGeneralInfo.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				if (student==null) {
					student = new StudentImpl();
				}
				
				student.setName(studentNameField.getValue().toString());
				student.setFatherName(fatherNameField.getValue().toString());
				student.setMotherName(motherNameField.getValue().toString());
				student.setGender(genderOption.getValue().toString());
				student.setDateOfBirth(new Date(dobField.getDateFormat()));
//				student.set
			}
		});
		
		admissionFormGridLayout.addComponent(studentNameField, 0, 1, 2, 1);
		admissionFormGridLayout.addComponent(studentContactToken, 3, 1, 5, 1);
		
		admissionFormGridLayout.addComponent(fatherNameField, 0, 2, 2, 2);
		admissionFormGridLayout.addComponent(fatherMobileToken, 3, 2, 5, 2);
		
		admissionFormGridLayout.addComponent(motherNameField, 0, 3, 2, 3);
		admissionFormGridLayout.addComponent(motherMobileToken, 3, 3, 5, 3);
		
		admissionFormGridLayout.addComponent(genderOption, 0, 4, 2, 4);
		admissionFormGridLayout.addComponent(emailToken, 3, 4, 5, 4);
		
		admissionFormGridLayout.addComponent(dobField, 0, 5, 2, 5);
		admissionFormGridLayout.addComponent(homePhoneToken, 3, 5, 5, 5);
		
		admissionFormGridLayout.addComponent(batchComboBox, 0, 6, 2, 6);
		admissionFormGridLayout.addComponent(gardianContactToken, 3, 6, 5, 6);
		
		admissionFormGridLayout.addComponent(presentAdressArea, 0, 7, 2, 7);
		admissionFormGridLayout.addComponent(permanentAdressArea, 3, 7, 5, 7);
		
		admissionFormGridLayout.addComponent(photoPanelLayout, 6, 1, 10, 4);
		
		admissionFormGridLayout.addComponent(saveGeneralInfo, 1, 8);
		
		return admissionFormGridLayout;
	}
	
	private VerticalLayout accademicRecordLayout;
	private Button moreAccademicRecordButton;
	private Button resetAccademicRecordButton;
	private Button saveAccademicRecord;
	
	private VerticalLayout accademicRecordTable(){
		accademicRecordLayout = new VerticalLayout();
		
		accademicRecordTable = new Table("Accademic Record");
		accademicRecordTable.setSelectable(true);
		accademicRecordTable.setEditable(true);
		accademicRecordTable.setWidth("100%");
		accademicRecordTable.setHeight("190px");
		accademicRecordTable.addContainerProperty("degree", String.class, null, "Degree", null, null );
		accademicRecordTable.addContainerProperty("board", String.class, null, "Board", null, null);
		accademicRecordTable.addContainerProperty("year", String.class, null, "Year", null, null);
		accademicRecordTable.addContainerProperty("result", String.class, null, "Result", null, null);
		accademicRecordTable.addContainerProperty("registrationNo", String.class, null, "Registration No", null, null);
		accademicRecordTable.addItem();
		accademicRecordLayout.addComponent(accademicRecordTable);
		
		HorizontalLayout accademicRecordButtonLayout = new HorizontalLayout();
		accademicRecordLayout.addComponent(accademicRecordButtonLayout);
		
		moreAccademicRecordButton = new Button("More");
		accademicRecordButtonLayout.addComponent(moreAccademicRecordButton);
		
		resetAccademicRecordButton = new Button("Reset");
		accademicRecordButtonLayout.addComponent(resetAccademicRecordButton);
		moreAccademicRecordButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				accademicRecordTable.addItem();
			}
		});
		
		resetAccademicRecordButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				accademicRecordTable.removeAllItems();
				accademicRecordTable.addItem();
			}
		});
		
		saveAccademicRecord = new Button("Save");
		accademicRecordButtonLayout.addComponent(saveAccademicRecord);
		
		return accademicRecordLayout;
	}
	
	private VerticalLayout studentDocumentLayout;
	private Button moreStudentDocumentButton;
	private Button resetStudentDocumentButton;
	private Button saveStudentDocument;
	
	private VerticalLayout studentDocumentTable(){
		studentDocumentLayout = new VerticalLayout();
		
		studentDocumentTable = new Table("Student Document");
		
		studentDocumentTable.setSelectable(true);
		studentDocumentTable.setEditable(true);
		studentDocumentTable.setWidth("100%");
		studentDocumentTable.setHeight("190px");
		studentDocumentTable.addContainerProperty("type", String.class, null, "Type", null, null );
		studentDocumentTable.addContainerProperty("documentData", String.class, null, "Document Data", null, null);
		studentDocumentTable.addContainerProperty("documentRefferenceNo", String.class, null, "Refference No", null, null);
		studentDocumentTable.addItem();
		studentDocumentLayout.addComponent(studentDocumentTable);
		
		HorizontalLayout studentDocumentButtonLayout = new HorizontalLayout();
		studentDocumentLayout.addComponent(studentDocumentButtonLayout);
		
		moreStudentDocumentButton = new Button("More");
		studentDocumentButtonLayout.addComponent(moreStudentDocumentButton);
		resetStudentDocumentButton = new Button("Reset");
		studentDocumentButtonLayout.addComponent(resetStudentDocumentButton);
		
		moreStudentDocumentButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				studentDocumentTable.addItem();
			}
		});
		
		resetStudentDocumentButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				studentDocumentTable.removeAllItems();
				studentDocumentTable.addItem();
			}
		});
		
		saveStudentDocument = new Button("Save");
		studentDocumentButtonLayout.addComponent(saveStudentDocument);
		
		return studentDocumentLayout;
	}
	
	private VerticalLayout experienceLayout;
	private Button moreExperienceButton;
	private Button resetExperienceButton;
	private Button saveExperience;
	
	private VerticalLayout experienceTable(){
		experienceLayout = new VerticalLayout();
		experienceTable = new Table("Experience");
		experienceTable.setSelectable(true);
		experienceTable.setEditable(true);
		experienceTable.setWidth("100%");
		experienceTable.setHeight("190px");
		experienceTable.addContainerProperty("organization", String.class, null, "Oraganization", null, null );
		experienceTable.addContainerProperty("designation", String.class, null, "Designation", null, null);
		experienceTable.addContainerProperty("experienceStartDate", Date.class, null, "Start Date", null, null);
		experienceTable.addContainerProperty("experienceEndDate", Date.class, null, "End Date", null, null);
		experienceTable.addContainerProperty("currentStatus", String.class, null, "Current Status", null, null);
		experienceTable.addItem();
		
		experienceLayout.addComponent(experienceTable);
		
		HorizontalLayout experienceButtonLayout = new HorizontalLayout();
		experienceLayout.addComponent(experienceButtonLayout);
		
		moreExperienceButton = new Button("More");
		experienceButtonLayout.addComponent(moreExperienceButton);
		resetExperienceButton = new Button("Reset");
		experienceButtonLayout.addComponent(resetExperienceButton);
		
		moreExperienceButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				experienceTable.addItem();
			}
		});
		
		resetExperienceButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				experienceTable.removeAllItems();
				experienceTable.addItem();
			}
		});
		
		saveExperience = new Button("Save");
		experienceButtonLayout.addComponent(saveExperience);
		
		return experienceLayout;
	}
	
	private VerticalLayout batchLayout;
	private Button moreBatchButton;
	private Button resetBatchButton;
	private Button saveBatch;
	
	private VerticalLayout batchTable(){
		batchLayout = new VerticalLayout();
		
		batchTable = new Table("Batch");
		batchLayout.addComponent(batchTable);
		batchTable.setSelectable(true);
		batchTable.setEditable(true);
		batchTable.setWidth("100%");
		batchTable.setHeight("190px");
		batchTable.addContainerProperty("batchSession", String.class, null, "Batch Session", null, null );
		batchTable.addContainerProperty("batchStartDate", Date.class, null, "Start Date", null, null);
		batchTable.addContainerProperty("batchEndDate", Date.class, null, "End Date", null, null);
		batchTable.addContainerProperty("courseCodeId", String.class, null, "Course Code Id", null, null);
		batchTable.addContainerProperty("note", String.class, null, "Note", null, null);
		batchTable.addContainerProperty("status", String.class, null, "Status", null, null);
		batchTable.addItem();
		
		HorizontalLayout batchButtonLayout = new HorizontalLayout();
		batchLayout.addComponent(batchButtonLayout);
		moreBatchButton = new Button("More");
		batchButtonLayout.addComponent(moreBatchButton);
		resetBatchButton = new Button("Reset");
		batchButtonLayout.addComponent(resetBatchButton);
		
		moreBatchButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				batchTable.addItem();
			}
		});
		
		resetBatchButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				batchTable.removeAllItems();
				batchTable.addItem();
			}
		});
		
		saveBatch = new Button("Save");
		batchButtonLayout.addComponent(saveBatch);
		
		return batchLayout;
	}
	
	
	// upload panel
	public void showPopupWindow(String title) {

		myPopWindow = new Window(title);
		myPopWindow.setWidth("370px");
		myPopWindow.setPositionX(300);
		myPopWindow.setPositionY(300);

		// Create the Upload component.
		upload = new Upload("Upload here", this);
		

		// Use a custom button caption instead of plain "Upload".
		upload.setButtonCaption("Upload Now");
		

		// myPopWindow.setVisible(true)
		upload.addListener((Upload.SucceededListener) this);
		upload.addListener((Upload.FailedListener) this);
		upload.addListener((Upload.StartedListener) this);
		myPopWindow.addComponent(upload);
		myPopWindow.addComponent(new Label("Click 'Browse' to select a file and then click 'Upload'."));
		window.addWindow(myPopWindow);

	}

	public void uploadStarted(StartedEvent event) {
		
	}

	public void uploadFailed(FailedEvent event) {
		myPopWindow.addComponent(new Label("Uploading " + event.getFilename()
				+ " of type '" + event.getMIMEType() + "' failed."));
	}

	public void uploadSucceeded(SucceededEvent event) {
		// Log the upload on screen.
		myPopWindow.addComponent(new Label("File " + event.getFilename()
				+ " of type '" + event.getMIMEType() + "' uploaded."));

		// Display the uploaded file in the image panel.
		final FileResource imageResource = new FileResource(file, this);
		photoPanel.removeAllComponents();
		Embedded image = new Embedded("", imageResource);
		image.setWidth("100%");
		image.setHeight("125px");
		photoPanel.addComponent(image);

	}

	public OutputStream receiveUpload(String filename, String mimeType) {
		// TODO Auto-generated method stub
		FileOutputStream fos = null; // Output stream to write to

		File tmpdir = new File("/tmp/uploads/");
		if (!tmpdir.exists()) {
			tmpdir.mkdirs();
			// System.out.println("Dir created ..");
		}

		// System.out.println("Exists created ..");
		file = new File(tmpdir, filename);

		try {
			// Open the file for writing.

			fos = new FileOutputStream(file);
		} catch (final java.io.FileNotFoundException e) {
			// Error while opening the file. Not reported here.
			e.printStackTrace();
			return null;
		}

		return fos; // Return the output stream to write to

	}
	
	public GridLayout easySearch(){
		GridLayout easySearchLayout = new GridLayout(5,5);
		easySearchLayout.setWidth("100%");
		easySearchLayout.setSpacing(true);
		TextField serchTextField = new TextField();
		serchTextField.setWidth("100%");
		
		Button searchButton = new Button("Search");
		searchButton.setWidth("100%");
		easySearchLayout.addComponent(searchButton,2,4);
		easySearchLayout.addComponent(serchTextField,1,3,3,3);
		return easySearchLayout;
	}
	
	private Table searchTable;
	
	public VerticalLayout customSearch(){
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setWidth("100%");

		final ComboBox courseCodeListBox = new ComboBox();		
        courseCodeListBox.setImmediate(true);
        courseCodeListBox.setCaption("By Course");
        courseCodeListBox.addItem("IFY");
        courseCodeListBox.addItem("IDCS");
        
		final ComboBox batchListBox = new ComboBox();
		batchListBox.setImmediate(true);
		batchListBox.setCaption("By Batch");	
		batchListBox.addItem("IFY30");
		batchListBox.addItem("IDCS50");
		
		final TextField nameField = new TextField("Student Name");
		nameField.setWidth("100%");
		nameField.setImmediate(true);
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setWidth("100%");
		horizontalLayout.setSpacing(true);
		horizontalLayout.addComponent(courseCodeListBox);
		horizontalLayout.addComponent(batchListBox);
		horizontalLayout.addComponent(nameField);
		verticalLayout.addComponent(horizontalLayout);
		
				
		searchTable = new Table("Search");
		customSearchContainer = new BeanItemContainer<CustomSearchDto>(CustomSearchDto.class);
		
		/*Dummy Data*/
		CustomSearchDto data1 = new CustomSearchDto();
		data1.setBatchName("IFY30");
		data1.setCourseName("IFY");
		data1.setStudentId(113278);
		data1.setStudentName("Mohammad Saeid");
		
		customSearchContainer.addBean(data1);
		
		CustomSearchDto data2 = new CustomSearchDto();
		data2.setBatchName("IDCS50");
		data2.setCourseName("IDCS");
		data2.setStudentId(113290);
		data2.setStudentName("Limon");
		
		customSearchContainer.addBean(data2);
		/*Dummy Data*/
		
		searchTable.setContainerDataSource(customSearchContainer);
		courseCodeListBox.addListener(new Listener() {
			SimpleStringFilter filter = null;
			
			public void componentEvent(Event event) {
				Filterable f = (Filterable)
						searchTable.getContainerDataSource();
				if (courseCodeListBox.getValue()!=null) {
					
			        if (filter != null)
			            f.removeContainerFilter(filter);
			        
			        // Set new filter for the "Name" column
			        filter = new SimpleStringFilter(COLUMN_COURSE,courseCodeListBox.getValue().toString(), true, false);
			        f.addContainerFilter(filter);
				}
				else{
					f.removeAllContainerFilters();
				}
			}
		});
		
		batchListBox.addListener(new Listener() {
			SimpleStringFilter filter = null;
			
			public void componentEvent(Event event) {
				Filterable f = (Filterable)
					searchTable.getContainerDataSource();
		        if (filter != null)
		            f.removeContainerFilter(filter);
		        
		        // Set new filter for the "Name" column
		        filter = new SimpleStringFilter(COLUMN_BATCH_NAME,batchListBox.getValue().toString(), true, false);
		        f.addContainerFilter(filter);				
			}
		});
		
		nameField.addListener(new Listener() {
			SimpleStringFilter filter = null;
			
			public void componentEvent(Event event) {
				Filterable f = (Filterable)
					searchTable.getContainerDataSource();
		        if (filter != null)
		            f.removeContainerFilter(filter);
		        
		        // Set new filter for the "Name" column
		        filter = new SimpleStringFilter(COLUMN_STUDENT_NAME,nameField.getValue().toString(), true, false);
		        f.addContainerFilter(filter);				
			}
		});
		
		searchTable.setColumnHeader(COLUMN_COURSE, "Course");
		searchTable.setColumnHeader(COLUMN_BATCH_NAME, "Batch");
		searchTable.setColumnHeader(COLUMN_STUDENT_ID, "Student ID");
		searchTable.setColumnHeader(COLUMN_STUDENT_NAME, "Student Name");
		
		searchTable.setVisibleColumns(new String[]{COLUMN_COURSE,COLUMN_BATCH_NAME,COLUMN_STUDENT_ID,COLUMN_STUDENT_NAME});
		
		searchTable.setColumnAlignment(COLUMN_COURSE,Table.ALIGN_RIGHT);
		searchTable.setColumnAlignment(COLUMN_BATCH_NAME,Table.ALIGN_RIGHT);
		searchTable.setColumnAlignment(COLUMN_STUDENT_ID,Table.ALIGN_RIGHT);
		searchTable.setColumnAlignment(COLUMN_STUDENT_NAME,Table.ALIGN_RIGHT);
		searchTable.setPageLength(15);
		searchTable.setWidth("100%");	

		Button editButton = new Button("Edit");
		verticalLayout.addComponent(editButton);
		verticalLayout.setComponentAlignment(editButton, Alignment.BOTTOM_RIGHT);		
			editButton.addListener(new ClickListener() {
				public void buttonClick(ClickEvent event) {
					
					window.showNotification("Please Select a Batch!");
				}
			});
		
		verticalLayout.addComponent(searchTable);
		
		return verticalLayout;
		
	}

}
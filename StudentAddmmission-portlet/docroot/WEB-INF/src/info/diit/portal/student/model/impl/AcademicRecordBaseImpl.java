/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model.impl;

import com.liferay.portal.kernel.exception.SystemException;

import info.diit.portal.student.model.AcademicRecord;
import info.diit.portal.student.service.AcademicRecordLocalServiceUtil;

/**
 * The extended model base implementation for the AcademicRecord service. Represents a row in the &quot;student_AcademicRecord&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link AcademicRecordImpl}.
 * </p>
 *
 * @author nasimul
 * @see AcademicRecordImpl
 * @see info.diit.portal.student.model.AcademicRecord
 * @generated
 */
public abstract class AcademicRecordBaseImpl extends AcademicRecordModelImpl
	implements AcademicRecord {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a academic record model instance should use the {@link AcademicRecord} interface instead.
	 */
	public void persist() throws SystemException {
		if (this.isNew()) {
			AcademicRecordLocalServiceUtil.addAcademicRecord(this);
		}
		else {
			AcademicRecordLocalServiceUtil.updateAcademicRecord(this);
		}
	}
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.student.NoSuchAcademicRecordException;
import info.diit.portal.student.model.AcademicRecord;
import info.diit.portal.student.model.impl.AcademicRecordImpl;
import info.diit.portal.student.model.impl.AcademicRecordModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the academic record service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see AcademicRecordPersistence
 * @see AcademicRecordUtil
 * @generated
 */
public class AcademicRecordPersistenceImpl extends BasePersistenceImpl<AcademicRecord>
	implements AcademicRecordPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AcademicRecordUtil} to access the academic record persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AcademicRecordImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordModelImpl.FINDER_CACHE_ENABLED,
			AcademicRecordImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordModelImpl.FINDER_CACHE_ENABLED,
			AcademicRecordImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the academic record in the entity cache if it is enabled.
	 *
	 * @param academicRecord the academic record
	 */
	public void cacheResult(AcademicRecord academicRecord) {
		EntityCacheUtil.putResult(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordImpl.class, academicRecord.getPrimaryKey(),
			academicRecord);

		academicRecord.resetOriginalValues();
	}

	/**
	 * Caches the academic records in the entity cache if it is enabled.
	 *
	 * @param academicRecords the academic records
	 */
	public void cacheResult(List<AcademicRecord> academicRecords) {
		for (AcademicRecord academicRecord : academicRecords) {
			if (EntityCacheUtil.getResult(
						AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
						AcademicRecordImpl.class, academicRecord.getPrimaryKey()) == null) {
				cacheResult(academicRecord);
			}
			else {
				academicRecord.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all academic records.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AcademicRecordImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AcademicRecordImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the academic record.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AcademicRecord academicRecord) {
		EntityCacheUtil.removeResult(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordImpl.class, academicRecord.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<AcademicRecord> academicRecords) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AcademicRecord academicRecord : academicRecords) {
			EntityCacheUtil.removeResult(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
				AcademicRecordImpl.class, academicRecord.getPrimaryKey());
		}
	}

	/**
	 * Creates a new academic record with the primary key. Does not add the academic record to the database.
	 *
	 * @param academicRecordId the primary key for the new academic record
	 * @return the new academic record
	 */
	public AcademicRecord create(long academicRecordId) {
		AcademicRecord academicRecord = new AcademicRecordImpl();

		academicRecord.setNew(true);
		academicRecord.setPrimaryKey(academicRecordId);

		return academicRecord;
	}

	/**
	 * Removes the academic record with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param academicRecordId the primary key of the academic record
	 * @return the academic record that was removed
	 * @throws info.diit.portal.student.NoSuchAcademicRecordException if a academic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AcademicRecord remove(long academicRecordId)
		throws NoSuchAcademicRecordException, SystemException {
		return remove(Long.valueOf(academicRecordId));
	}

	/**
	 * Removes the academic record with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the academic record
	 * @return the academic record that was removed
	 * @throws info.diit.portal.student.NoSuchAcademicRecordException if a academic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AcademicRecord remove(Serializable primaryKey)
		throws NoSuchAcademicRecordException, SystemException {
		Session session = null;

		try {
			session = openSession();

			AcademicRecord academicRecord = (AcademicRecord)session.get(AcademicRecordImpl.class,
					primaryKey);

			if (academicRecord == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAcademicRecordException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(academicRecord);
		}
		catch (NoSuchAcademicRecordException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AcademicRecord removeImpl(AcademicRecord academicRecord)
		throws SystemException {
		academicRecord = toUnwrappedModel(academicRecord);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, academicRecord);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(academicRecord);

		return academicRecord;
	}

	@Override
	public AcademicRecord updateImpl(
		info.diit.portal.student.model.AcademicRecord academicRecord,
		boolean merge) throws SystemException {
		academicRecord = toUnwrappedModel(academicRecord);

		boolean isNew = academicRecord.isNew();

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, academicRecord, merge);

			academicRecord.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AcademicRecordImpl.class, academicRecord.getPrimaryKey(),
			academicRecord);

		return academicRecord;
	}

	protected AcademicRecord toUnwrappedModel(AcademicRecord academicRecord) {
		if (academicRecord instanceof AcademicRecordImpl) {
			return academicRecord;
		}

		AcademicRecordImpl academicRecordImpl = new AcademicRecordImpl();

		academicRecordImpl.setNew(academicRecord.isNew());
		academicRecordImpl.setPrimaryKey(academicRecord.getPrimaryKey());

		academicRecordImpl.setAcademicRecordId(academicRecord.getAcademicRecordId());
		academicRecordImpl.setCompanyId(academicRecord.getCompanyId());
		academicRecordImpl.setUserId(academicRecord.getUserId());
		academicRecordImpl.setUserName(academicRecord.getUserName());
		academicRecordImpl.setCreateDate(academicRecord.getCreateDate());
		academicRecordImpl.setModifiedDate(academicRecord.getModifiedDate());
		academicRecordImpl.setOrganisationId(academicRecord.getOrganisationId());
		academicRecordImpl.setDegree(academicRecord.getDegree());
		academicRecordImpl.setBoard(academicRecord.getBoard());
		academicRecordImpl.setYear(academicRecord.getYear());
		academicRecordImpl.setResult(academicRecord.getResult());
		academicRecordImpl.setRegistrationNo(academicRecord.getRegistrationNo());

		return academicRecordImpl;
	}

	/**
	 * Returns the academic record with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the academic record
	 * @return the academic record
	 * @throws com.liferay.portal.NoSuchModelException if a academic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AcademicRecord findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the academic record with the primary key or throws a {@link info.diit.portal.student.NoSuchAcademicRecordException} if it could not be found.
	 *
	 * @param academicRecordId the primary key of the academic record
	 * @return the academic record
	 * @throws info.diit.portal.student.NoSuchAcademicRecordException if a academic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AcademicRecord findByPrimaryKey(long academicRecordId)
		throws NoSuchAcademicRecordException, SystemException {
		AcademicRecord academicRecord = fetchByPrimaryKey(academicRecordId);

		if (academicRecord == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + academicRecordId);
			}

			throw new NoSuchAcademicRecordException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				academicRecordId);
		}

		return academicRecord;
	}

	/**
	 * Returns the academic record with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the academic record
	 * @return the academic record, or <code>null</code> if a academic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AcademicRecord fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the academic record with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param academicRecordId the primary key of the academic record
	 * @return the academic record, or <code>null</code> if a academic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AcademicRecord fetchByPrimaryKey(long academicRecordId)
		throws SystemException {
		AcademicRecord academicRecord = (AcademicRecord)EntityCacheUtil.getResult(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
				AcademicRecordImpl.class, academicRecordId);

		if (academicRecord == _nullAcademicRecord) {
			return null;
		}

		if (academicRecord == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				academicRecord = (AcademicRecord)session.get(AcademicRecordImpl.class,
						Long.valueOf(academicRecordId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (academicRecord != null) {
					cacheResult(academicRecord);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
						AcademicRecordImpl.class, academicRecordId,
						_nullAcademicRecord);
				}

				closeSession(session);
			}
		}

		return academicRecord;
	}

	/**
	 * Returns all the academic records.
	 *
	 * @return the academic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AcademicRecord> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the academic records.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of academic records
	 * @param end the upper bound of the range of academic records (not inclusive)
	 * @return the range of academic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AcademicRecord> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the academic records.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of academic records
	 * @param end the upper bound of the range of academic records (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of academic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AcademicRecord> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<AcademicRecord> list = (List<AcademicRecord>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ACADEMICRECORD);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ACADEMICRECORD;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<AcademicRecord>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<AcademicRecord>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the academic records from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (AcademicRecord academicRecord : findAll()) {
			remove(academicRecord);
		}
	}

	/**
	 * Returns the number of academic records.
	 *
	 * @return the number of academic records
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ACADEMICRECORD);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the academic record persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.student.model.AcademicRecord")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<AcademicRecord>> listenersList = new ArrayList<ModelListener<AcademicRecord>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<AcademicRecord>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AcademicRecordImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_ACADEMICRECORD = "SELECT academicRecord FROM AcademicRecord academicRecord";
	private static final String _SQL_COUNT_ACADEMICRECORD = "SELECT COUNT(academicRecord) FROM AcademicRecord academicRecord";
	private static final String _ORDER_BY_ENTITY_ALIAS = "academicRecord.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AcademicRecord exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AcademicRecordPersistenceImpl.class);
	private static AcademicRecord _nullAcademicRecord = new AcademicRecordImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<AcademicRecord> toCacheModel() {
				return _nullAcademicRecordCacheModel;
			}
		};

	private static CacheModel<AcademicRecord> _nullAcademicRecordCacheModel = new CacheModel<AcademicRecord>() {
			public AcademicRecord toEntityModel() {
				return _nullAcademicRecord;
			}
		};
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.student.NoSuchAccademicRecordException;
import info.diit.portal.student.model.AccademicRecord;
import info.diit.portal.student.model.impl.AccademicRecordImpl;
import info.diit.portal.student.model.impl.AccademicRecordModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the accademic record service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see AccademicRecordPersistence
 * @see AccademicRecordUtil
 * @generated
 */
public class AccademicRecordPersistenceImpl extends BasePersistenceImpl<AccademicRecord>
	implements AccademicRecordPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AccademicRecordUtil} to access the accademic record persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AccademicRecordImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordModelImpl.FINDER_CACHE_ENABLED,
			AccademicRecordImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordModelImpl.FINDER_CACHE_ENABLED,
			AccademicRecordImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the accademic record in the entity cache if it is enabled.
	 *
	 * @param accademicRecord the accademic record
	 */
	public void cacheResult(AccademicRecord accademicRecord) {
		EntityCacheUtil.putResult(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordImpl.class, accademicRecord.getPrimaryKey(),
			accademicRecord);

		accademicRecord.resetOriginalValues();
	}

	/**
	 * Caches the accademic records in the entity cache if it is enabled.
	 *
	 * @param accademicRecords the accademic records
	 */
	public void cacheResult(List<AccademicRecord> accademicRecords) {
		for (AccademicRecord accademicRecord : accademicRecords) {
			if (EntityCacheUtil.getResult(
						AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
						AccademicRecordImpl.class,
						accademicRecord.getPrimaryKey()) == null) {
				cacheResult(accademicRecord);
			}
			else {
				accademicRecord.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all accademic records.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AccademicRecordImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AccademicRecordImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the accademic record.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AccademicRecord accademicRecord) {
		EntityCacheUtil.removeResult(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordImpl.class, accademicRecord.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<AccademicRecord> accademicRecords) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AccademicRecord accademicRecord : accademicRecords) {
			EntityCacheUtil.removeResult(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
				AccademicRecordImpl.class, accademicRecord.getPrimaryKey());
		}
	}

	/**
	 * Creates a new accademic record with the primary key. Does not add the accademic record to the database.
	 *
	 * @param accademicRecordId the primary key for the new accademic record
	 * @return the new accademic record
	 */
	public AccademicRecord create(long accademicRecordId) {
		AccademicRecord accademicRecord = new AccademicRecordImpl();

		accademicRecord.setNew(true);
		accademicRecord.setPrimaryKey(accademicRecordId);

		return accademicRecord;
	}

	/**
	 * Removes the accademic record with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param accademicRecordId the primary key of the accademic record
	 * @return the accademic record that was removed
	 * @throws info.diit.portal.student.NoSuchAccademicRecordException if a accademic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AccademicRecord remove(long accademicRecordId)
		throws NoSuchAccademicRecordException, SystemException {
		return remove(Long.valueOf(accademicRecordId));
	}

	/**
	 * Removes the accademic record with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the accademic record
	 * @return the accademic record that was removed
	 * @throws info.diit.portal.student.NoSuchAccademicRecordException if a accademic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AccademicRecord remove(Serializable primaryKey)
		throws NoSuchAccademicRecordException, SystemException {
		Session session = null;

		try {
			session = openSession();

			AccademicRecord accademicRecord = (AccademicRecord)session.get(AccademicRecordImpl.class,
					primaryKey);

			if (accademicRecord == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAccademicRecordException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(accademicRecord);
		}
		catch (NoSuchAccademicRecordException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AccademicRecord removeImpl(AccademicRecord accademicRecord)
		throws SystemException {
		accademicRecord = toUnwrappedModel(accademicRecord);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, accademicRecord);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(accademicRecord);

		return accademicRecord;
	}

	@Override
	public AccademicRecord updateImpl(
		info.diit.portal.student.model.AccademicRecord accademicRecord,
		boolean merge) throws SystemException {
		accademicRecord = toUnwrappedModel(accademicRecord);

		boolean isNew = accademicRecord.isNew();

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, accademicRecord, merge);

			accademicRecord.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			AccademicRecordImpl.class, accademicRecord.getPrimaryKey(),
			accademicRecord);

		return accademicRecord;
	}

	protected AccademicRecord toUnwrappedModel(AccademicRecord accademicRecord) {
		if (accademicRecord instanceof AccademicRecordImpl) {
			return accademicRecord;
		}

		AccademicRecordImpl accademicRecordImpl = new AccademicRecordImpl();

		accademicRecordImpl.setNew(accademicRecord.isNew());
		accademicRecordImpl.setPrimaryKey(accademicRecord.getPrimaryKey());

		accademicRecordImpl.setAccademicRecordId(accademicRecord.getAccademicRecordId());
		accademicRecordImpl.setCompanyId(accademicRecord.getCompanyId());
		accademicRecordImpl.setUserId(accademicRecord.getUserId());
		accademicRecordImpl.setUserName(accademicRecord.getUserName());
		accademicRecordImpl.setCreateDate(accademicRecord.getCreateDate());
		accademicRecordImpl.setModifiedDate(accademicRecord.getModifiedDate());
		accademicRecordImpl.setOrganisationId(accademicRecord.getOrganisationId());
		accademicRecordImpl.setDegree(accademicRecord.getDegree());
		accademicRecordImpl.setBoard(accademicRecord.getBoard());
		accademicRecordImpl.setYear(accademicRecord.getYear());
		accademicRecordImpl.setResult(accademicRecord.getResult());
		accademicRecordImpl.setRegistrationNo(accademicRecord.getRegistrationNo());

		return accademicRecordImpl;
	}

	/**
	 * Returns the accademic record with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the accademic record
	 * @return the accademic record
	 * @throws com.liferay.portal.NoSuchModelException if a accademic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AccademicRecord findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the accademic record with the primary key or throws a {@link info.diit.portal.student.NoSuchAccademicRecordException} if it could not be found.
	 *
	 * @param accademicRecordId the primary key of the accademic record
	 * @return the accademic record
	 * @throws info.diit.portal.student.NoSuchAccademicRecordException if a accademic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AccademicRecord findByPrimaryKey(long accademicRecordId)
		throws NoSuchAccademicRecordException, SystemException {
		AccademicRecord accademicRecord = fetchByPrimaryKey(accademicRecordId);

		if (accademicRecord == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + accademicRecordId);
			}

			throw new NoSuchAccademicRecordException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				accademicRecordId);
		}

		return accademicRecord;
	}

	/**
	 * Returns the accademic record with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the accademic record
	 * @return the accademic record, or <code>null</code> if a accademic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AccademicRecord fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the accademic record with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param accademicRecordId the primary key of the accademic record
	 * @return the accademic record, or <code>null</code> if a accademic record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public AccademicRecord fetchByPrimaryKey(long accademicRecordId)
		throws SystemException {
		AccademicRecord accademicRecord = (AccademicRecord)EntityCacheUtil.getResult(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
				AccademicRecordImpl.class, accademicRecordId);

		if (accademicRecord == _nullAccademicRecord) {
			return null;
		}

		if (accademicRecord == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				accademicRecord = (AccademicRecord)session.get(AccademicRecordImpl.class,
						Long.valueOf(accademicRecordId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (accademicRecord != null) {
					cacheResult(accademicRecord);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(AccademicRecordModelImpl.ENTITY_CACHE_ENABLED,
						AccademicRecordImpl.class, accademicRecordId,
						_nullAccademicRecord);
				}

				closeSession(session);
			}
		}

		return accademicRecord;
	}

	/**
	 * Returns all the accademic records.
	 *
	 * @return the accademic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AccademicRecord> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the accademic records.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of accademic records
	 * @param end the upper bound of the range of accademic records (not inclusive)
	 * @return the range of accademic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AccademicRecord> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the accademic records.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of accademic records
	 * @param end the upper bound of the range of accademic records (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of accademic records
	 * @throws SystemException if a system exception occurred
	 */
	public List<AccademicRecord> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<AccademicRecord> list = (List<AccademicRecord>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ACCADEMICRECORD);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ACCADEMICRECORD;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<AccademicRecord>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<AccademicRecord>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the accademic records from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (AccademicRecord accademicRecord : findAll()) {
			remove(accademicRecord);
		}
	}

	/**
	 * Returns the number of accademic records.
	 *
	 * @return the number of accademic records
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ACCADEMICRECORD);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the accademic record persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.student.model.AccademicRecord")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<AccademicRecord>> listenersList = new ArrayList<ModelListener<AccademicRecord>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<AccademicRecord>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AccademicRecordImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AccademicRecordPersistence.class)
	protected AccademicRecordPersistence accademicRecordPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_ACCADEMICRECORD = "SELECT accademicRecord FROM AccademicRecord accademicRecord";
	private static final String _SQL_COUNT_ACCADEMICRECORD = "SELECT COUNT(accademicRecord) FROM AccademicRecord accademicRecord";
	private static final String _ORDER_BY_ENTITY_ALIAS = "accademicRecord.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AccademicRecord exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AccademicRecordPersistenceImpl.class);
	private static AccademicRecord _nullAccademicRecord = new AccademicRecordImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<AccademicRecord> toCacheModel() {
				return _nullAccademicRecordCacheModel;
			}
		};

	private static CacheModel<AccademicRecord> _nullAccademicRecordCacheModel = new CacheModel<AccademicRecord>() {
			public AccademicRecord toEntityModel() {
				return _nullAccademicRecord;
			}
		};
}
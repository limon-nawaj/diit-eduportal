package info.diit.portal.student.dto;



import java.sql.Blob;
import java.util.Date;

public class StudentDto {
	private long studentId;
	private String name;
	private String fatherName;
	private String motherName;
	private String presentAddress;
	private String permanentAddress;
	private String homePhone;
	private String fatherMobile;
	private String motherMobile;
	private String guardianMobile;
	private String mobile1;
	private String mobile2;
	private Date   dateOfBirth;
	private String email;
	private String gender;
	private String religion;
	private String nationality;
	private String status;
	private Blob photo;
	
	
	
	public Blob getPhoto() {
		return photo;
	}


	public void setPhoto(Blob photo) {
		this.photo = photo;
	}


	public StudentDto() {
		
	}
	
	
	public StudentDto(long studentId, String name, String fatherName,
			String motherName, String presentAddress, String permanentAddress,
			String homePhone, String fatherMobile, String motherMobile,
			String guardianMobile, String mobile1, String mobile2,
			Date dateOfBirth, String email, String gender, String religion,
			String nationality, String status) {
		super();
		this.studentId = studentId;
		this.name = name;
		this.fatherName = fatherName;
		this.motherName = motherName;
		this.presentAddress = presentAddress;
		this.permanentAddress = permanentAddress;
		this.homePhone = homePhone;
		this.fatherMobile = fatherMobile;
		this.motherMobile = motherMobile;
		this.guardianMobile = guardianMobile;
		this.mobile1 = mobile1;
		this.mobile2 = mobile2;
		this.dateOfBirth = dateOfBirth;
		this.email = email;
		this.gender = gender;
		this.religion = religion;
		this.nationality = nationality;
		this.status = status;
	}
	
	
	public long getStudentId() {
		return studentId;
	}
	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getPresentAddress() {
		return presentAddress;
	}
	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}
	public String getPermanentAddress() {
		return permanentAddress;
	}
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public String getFatherMobile() {
		return fatherMobile;
	}
	public void setFatherMobile(String fatherMobile) {
		this.fatherMobile = fatherMobile;
	}
	public String getMotherMobile() {
		return motherMobile;
	}
	public void setMotherMobile(String motherMobile) {
		this.motherMobile = motherMobile;
	}
	public String getGuardianMobile() {
		return guardianMobile;
	}
	public void setGuardianMobile(String guardianMobile) {
		this.guardianMobile = guardianMobile;
	}
	public String getMobile1() {
		return mobile1;
	}
	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}
	public String getMobile2() {
		return mobile2;
	}
	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}

/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.student.model.Batch;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Batch in entity cache.
 *
 * @author nasimul
 * @see Batch
 * @generated
 */
public class BatchCacheModel implements CacheModel<Batch>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{batchId=");
		sb.append(batchId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", batchSession=");
		sb.append(batchSession);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", endDate=");
		sb.append(endDate);
		sb.append(", courseCodeId=");
		sb.append(courseCodeId);
		sb.append(", note=");
		sb.append(note);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	public Batch toEntityModel() {
		BatchImpl batchImpl = new BatchImpl();

		batchImpl.setBatchId(batchId);
		batchImpl.setCompanyId(companyId);
		batchImpl.setUserId(userId);

		if (userName == null) {
			batchImpl.setUserName(StringPool.BLANK);
		}
		else {
			batchImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			batchImpl.setCreateDate(null);
		}
		else {
			batchImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			batchImpl.setModifiedDate(null);
		}
		else {
			batchImpl.setModifiedDate(new Date(modifiedDate));
		}

		batchImpl.setOrganizationId(organizationId);

		if (batchSession == null) {
			batchImpl.setBatchSession(StringPool.BLANK);
		}
		else {
			batchImpl.setBatchSession(batchSession);
		}

		if (startDate == Long.MIN_VALUE) {
			batchImpl.setStartDate(null);
		}
		else {
			batchImpl.setStartDate(new Date(startDate));
		}

		if (endDate == Long.MIN_VALUE) {
			batchImpl.setEndDate(null);
		}
		else {
			batchImpl.setEndDate(new Date(endDate));
		}

		batchImpl.setCourseCodeId(courseCodeId);

		if (note == null) {
			batchImpl.setNote(StringPool.BLANK);
		}
		else {
			batchImpl.setNote(note);
		}

		if (status == null) {
			batchImpl.setStatus(StringPool.BLANK);
		}
		else {
			batchImpl.setStatus(status);
		}

		batchImpl.resetOriginalValues();

		return batchImpl;
	}

	public long batchId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long organizationId;
	public String batchSession;
	public long startDate;
	public long endDate;
	public long courseCodeId;
	public String note;
	public String status;
}
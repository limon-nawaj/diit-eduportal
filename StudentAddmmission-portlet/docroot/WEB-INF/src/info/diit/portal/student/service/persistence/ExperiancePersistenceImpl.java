/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.student.NoSuchExperianceException;
import info.diit.portal.student.model.Experiance;
import info.diit.portal.student.model.impl.ExperianceImpl;
import info.diit.portal.student.model.impl.ExperianceModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the experiance service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see ExperiancePersistence
 * @see ExperianceUtil
 * @generated
 */
public class ExperiancePersistenceImpl extends BasePersistenceImpl<Experiance>
	implements ExperiancePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ExperianceUtil} to access the experiance persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ExperianceImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceModelImpl.FINDER_CACHE_ENABLED, ExperianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceModelImpl.FINDER_CACHE_ENABLED, ExperianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the experiance in the entity cache if it is enabled.
	 *
	 * @param experiance the experiance
	 */
	public void cacheResult(Experiance experiance) {
		EntityCacheUtil.putResult(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceImpl.class, experiance.getPrimaryKey(), experiance);

		experiance.resetOriginalValues();
	}

	/**
	 * Caches the experiances in the entity cache if it is enabled.
	 *
	 * @param experiances the experiances
	 */
	public void cacheResult(List<Experiance> experiances) {
		for (Experiance experiance : experiances) {
			if (EntityCacheUtil.getResult(
						ExperianceModelImpl.ENTITY_CACHE_ENABLED,
						ExperianceImpl.class, experiance.getPrimaryKey()) == null) {
				cacheResult(experiance);
			}
			else {
				experiance.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all experiances.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ExperianceImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ExperianceImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the experiance.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Experiance experiance) {
		EntityCacheUtil.removeResult(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceImpl.class, experiance.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Experiance> experiances) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Experiance experiance : experiances) {
			EntityCacheUtil.removeResult(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
				ExperianceImpl.class, experiance.getPrimaryKey());
		}
	}

	/**
	 * Creates a new experiance with the primary key. Does not add the experiance to the database.
	 *
	 * @param experianceId the primary key for the new experiance
	 * @return the new experiance
	 */
	public Experiance create(long experianceId) {
		Experiance experiance = new ExperianceImpl();

		experiance.setNew(true);
		experiance.setPrimaryKey(experianceId);

		return experiance;
	}

	/**
	 * Removes the experiance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param experianceId the primary key of the experiance
	 * @return the experiance that was removed
	 * @throws info.diit.portal.student.NoSuchExperianceException if a experiance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Experiance remove(long experianceId)
		throws NoSuchExperianceException, SystemException {
		return remove(Long.valueOf(experianceId));
	}

	/**
	 * Removes the experiance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the experiance
	 * @return the experiance that was removed
	 * @throws info.diit.portal.student.NoSuchExperianceException if a experiance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Experiance remove(Serializable primaryKey)
		throws NoSuchExperianceException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Experiance experiance = (Experiance)session.get(ExperianceImpl.class,
					primaryKey);

			if (experiance == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchExperianceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(experiance);
		}
		catch (NoSuchExperianceException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Experiance removeImpl(Experiance experiance)
		throws SystemException {
		experiance = toUnwrappedModel(experiance);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, experiance);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(experiance);

		return experiance;
	}

	@Override
	public Experiance updateImpl(
		info.diit.portal.student.model.Experiance experiance, boolean merge)
		throws SystemException {
		experiance = toUnwrappedModel(experiance);

		boolean isNew = experiance.isNew();

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, experiance, merge);

			experiance.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			ExperianceImpl.class, experiance.getPrimaryKey(), experiance);

		return experiance;
	}

	protected Experiance toUnwrappedModel(Experiance experiance) {
		if (experiance instanceof ExperianceImpl) {
			return experiance;
		}

		ExperianceImpl experianceImpl = new ExperianceImpl();

		experianceImpl.setNew(experiance.isNew());
		experianceImpl.setPrimaryKey(experiance.getPrimaryKey());

		experianceImpl.setExperianceId(experiance.getExperianceId());
		experianceImpl.setCompanyId(experiance.getCompanyId());
		experianceImpl.setUserId(experiance.getUserId());
		experianceImpl.setUserName(experiance.getUserName());
		experianceImpl.setCreateDate(experiance.getCreateDate());
		experianceImpl.setModifiedDate(experiance.getModifiedDate());
		experianceImpl.setOrganizationId(experiance.getOrganizationId());
		experianceImpl.setOrganization(experiance.getOrganization());
		experianceImpl.setDesignation(experiance.getDesignation());
		experianceImpl.setStartDate(experiance.getStartDate());
		experianceImpl.setEndDate(experiance.getEndDate());
		experianceImpl.setCurrentStatus(experiance.isCurrentStatus());

		return experianceImpl;
	}

	/**
	 * Returns the experiance with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the experiance
	 * @return the experiance
	 * @throws com.liferay.portal.NoSuchModelException if a experiance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Experiance findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the experiance with the primary key or throws a {@link info.diit.portal.student.NoSuchExperianceException} if it could not be found.
	 *
	 * @param experianceId the primary key of the experiance
	 * @return the experiance
	 * @throws info.diit.portal.student.NoSuchExperianceException if a experiance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Experiance findByPrimaryKey(long experianceId)
		throws NoSuchExperianceException, SystemException {
		Experiance experiance = fetchByPrimaryKey(experianceId);

		if (experiance == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + experianceId);
			}

			throw new NoSuchExperianceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				experianceId);
		}

		return experiance;
	}

	/**
	 * Returns the experiance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the experiance
	 * @return the experiance, or <code>null</code> if a experiance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Experiance fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the experiance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param experianceId the primary key of the experiance
	 * @return the experiance, or <code>null</code> if a experiance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Experiance fetchByPrimaryKey(long experianceId)
		throws SystemException {
		Experiance experiance = (Experiance)EntityCacheUtil.getResult(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
				ExperianceImpl.class, experianceId);

		if (experiance == _nullExperiance) {
			return null;
		}

		if (experiance == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				experiance = (Experiance)session.get(ExperianceImpl.class,
						Long.valueOf(experianceId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (experiance != null) {
					cacheResult(experiance);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(ExperianceModelImpl.ENTITY_CACHE_ENABLED,
						ExperianceImpl.class, experianceId, _nullExperiance);
				}

				closeSession(session);
			}
		}

		return experiance;
	}

	/**
	 * Returns all the experiances.
	 *
	 * @return the experiances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Experiance> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the experiances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of experiances
	 * @param end the upper bound of the range of experiances (not inclusive)
	 * @return the range of experiances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Experiance> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the experiances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of experiances
	 * @param end the upper bound of the range of experiances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of experiances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Experiance> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Experiance> list = (List<Experiance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_EXPERIANCE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_EXPERIANCE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Experiance>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Experiance>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the experiances from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Experiance experiance : findAll()) {
			remove(experiance);
		}
	}

	/**
	 * Returns the number of experiances.
	 *
	 * @return the number of experiances
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_EXPERIANCE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the experiance persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.student.model.Experiance")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Experiance>> listenersList = new ArrayList<ModelListener<Experiance>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Experiance>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ExperianceImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_EXPERIANCE = "SELECT experiance FROM Experiance experiance";
	private static final String _SQL_COUNT_EXPERIANCE = "SELECT COUNT(experiance) FROM Experiance experiance";
	private static final String _ORDER_BY_ENTITY_ALIAS = "experiance.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Experiance exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ExperiancePersistenceImpl.class);
	private static Experiance _nullExperiance = new ExperianceImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Experiance> toCacheModel() {
				return _nullExperianceCacheModel;
			}
		};

	private static CacheModel<Experiance> _nullExperianceCacheModel = new CacheModel<Experiance>() {
			public Experiance toEntityModel() {
				return _nullExperiance;
			}
		};
}
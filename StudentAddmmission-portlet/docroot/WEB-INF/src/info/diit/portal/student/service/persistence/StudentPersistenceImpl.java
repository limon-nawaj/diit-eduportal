/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.jdbc.MappingSqlQuery;
import com.liferay.portal.kernel.dao.jdbc.MappingSqlQueryFactoryUtil;
import com.liferay.portal.kernel.dao.jdbc.RowMapper;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.student.NoSuchStudentException;
import info.diit.portal.student.model.Student;
import info.diit.portal.student.model.impl.StudentImpl;
import info.diit.portal.student.model.impl.StudentModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the student service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author nasimul
 * @see StudentPersistence
 * @see StudentUtil
 * @generated
 */
public class StudentPersistenceImpl extends BasePersistenceImpl<Student>
	implements StudentPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link StudentUtil} to access the student persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = StudentImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_FETCH_BY_S_PHOTO = new FinderPath(StudentModelImpl.ENTITY_CACHE_ENABLED,
			StudentModelImpl.FINDER_CACHE_ENABLED, StudentImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByS_PHOTO",
			new String[] { Long.class.getName() },
			StudentModelImpl.STUDENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_S_PHOTO = new FinderPath(StudentModelImpl.ENTITY_CACHE_ENABLED,
			StudentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByS_PHOTO",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(StudentModelImpl.ENTITY_CACHE_ENABLED,
			StudentModelImpl.FINDER_CACHE_ENABLED, StudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(StudentModelImpl.ENTITY_CACHE_ENABLED,
			StudentModelImpl.FINDER_CACHE_ENABLED, StudentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(StudentModelImpl.ENTITY_CACHE_ENABLED,
			StudentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the student in the entity cache if it is enabled.
	 *
	 * @param student the student
	 */
	public void cacheResult(Student student) {
		EntityCacheUtil.putResult(StudentModelImpl.ENTITY_CACHE_ENABLED,
			StudentImpl.class, student.getPrimaryKey(), student);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_S_PHOTO,
			new Object[] { Long.valueOf(student.getStudentId()) }, student);

		student.resetOriginalValues();
	}

	/**
	 * Caches the students in the entity cache if it is enabled.
	 *
	 * @param students the students
	 */
	public void cacheResult(List<Student> students) {
		for (Student student : students) {
			if (EntityCacheUtil.getResult(
						StudentModelImpl.ENTITY_CACHE_ENABLED,
						StudentImpl.class, student.getPrimaryKey()) == null) {
				cacheResult(student);
			}
			else {
				student.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all students.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(StudentImpl.class.getName());
		}

		EntityCacheUtil.clearCache(StudentImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the student.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Student student) {
		EntityCacheUtil.removeResult(StudentModelImpl.ENTITY_CACHE_ENABLED,
			StudentImpl.class, student.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(student);
	}

	@Override
	public void clearCache(List<Student> students) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Student student : students) {
			EntityCacheUtil.removeResult(StudentModelImpl.ENTITY_CACHE_ENABLED,
				StudentImpl.class, student.getPrimaryKey());

			clearUniqueFindersCache(student);
		}
	}

	protected void clearUniqueFindersCache(Student student) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_S_PHOTO,
			new Object[] { Long.valueOf(student.getStudentId()) });
	}

	/**
	 * Creates a new student with the primary key. Does not add the student to the database.
	 *
	 * @param studentId the primary key for the new student
	 * @return the new student
	 */
	public Student create(long studentId) {
		Student student = new StudentImpl();

		student.setNew(true);
		student.setPrimaryKey(studentId);

		return student;
	}

	/**
	 * Removes the student with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param studentId the primary key of the student
	 * @return the student that was removed
	 * @throws info.diit.portal.student.NoSuchStudentException if a student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Student remove(long studentId)
		throws NoSuchStudentException, SystemException {
		return remove(Long.valueOf(studentId));
	}

	/**
	 * Removes the student with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the student
	 * @return the student that was removed
	 * @throws info.diit.portal.student.NoSuchStudentException if a student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Student remove(Serializable primaryKey)
		throws NoSuchStudentException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Student student = (Student)session.get(StudentImpl.class, primaryKey);

			if (student == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchStudentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(student);
		}
		catch (NoSuchStudentException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Student removeImpl(Student student) throws SystemException {
		student = toUnwrappedModel(student);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, student);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(student);

		return student;
	}

	@Override
	public Student updateImpl(info.diit.portal.student.model.Student student,
		boolean merge) throws SystemException {
		student = toUnwrappedModel(student);

		boolean isNew = student.isNew();

		StudentModelImpl studentModelImpl = (StudentModelImpl)student;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, student, merge);

			student.setNew(false);

			session.flush();
			session.clear();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !StudentModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(StudentModelImpl.ENTITY_CACHE_ENABLED,
			StudentImpl.class, student.getPrimaryKey(), student);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_S_PHOTO,
				new Object[] { Long.valueOf(student.getStudentId()) }, student);
		}
		else {
			if ((studentModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_S_PHOTO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(studentModelImpl.getOriginalStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_S_PHOTO, args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_S_PHOTO, args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_S_PHOTO,
					new Object[] { Long.valueOf(student.getStudentId()) },
					student);
			}
		}

		student.resetOriginalValues();

		return student;
	}

	protected Student toUnwrappedModel(Student student) {
		if (student instanceof StudentImpl) {
			return student;
		}

		StudentImpl studentImpl = new StudentImpl();

		studentImpl.setNew(student.isNew());
		studentImpl.setPrimaryKey(student.getPrimaryKey());

		studentImpl.setStudentId(student.getStudentId());
		studentImpl.setCompanyId(student.getCompanyId());
		studentImpl.setUserId(student.getUserId());
		studentImpl.setUserName(student.getUserName());
		studentImpl.setCreateDate(student.getCreateDate());
		studentImpl.setModifiedDate(student.getModifiedDate());
		studentImpl.setOrganizationId(student.getOrganizationId());
		studentImpl.setName(student.getName());
		studentImpl.setFatherName(student.getFatherName());
		studentImpl.setMotherName(student.getMotherName());
		studentImpl.setPresentAddress(student.getPresentAddress());
		studentImpl.setPermanentAddress(student.getPermanentAddress());
		studentImpl.setHomePhone(student.getHomePhone());
		studentImpl.setFatherMobile(student.getFatherMobile());
		studentImpl.setMotherMobile(student.getMotherMobile());
		studentImpl.setGuardianMobile(student.getGuardianMobile());
		studentImpl.setMobile1(student.getMobile1());
		studentImpl.setMobile2(student.getMobile2());
		studentImpl.setDateOfBirth(student.getDateOfBirth());
		studentImpl.setEmail(student.getEmail());
		studentImpl.setGender(student.getGender());
		studentImpl.setReligion(student.getReligion());
		studentImpl.setNationality(student.getNationality());
		studentImpl.setPhoto(student.getPhoto());
		studentImpl.setStatus(student.getStatus());

		return studentImpl;
	}

	/**
	 * Returns the student with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the student
	 * @return the student
	 * @throws com.liferay.portal.NoSuchModelException if a student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Student findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the student with the primary key or throws a {@link info.diit.portal.student.NoSuchStudentException} if it could not be found.
	 *
	 * @param studentId the primary key of the student
	 * @return the student
	 * @throws info.diit.portal.student.NoSuchStudentException if a student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Student findByPrimaryKey(long studentId)
		throws NoSuchStudentException, SystemException {
		Student student = fetchByPrimaryKey(studentId);

		if (student == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + studentId);
			}

			throw new NoSuchStudentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				studentId);
		}

		return student;
	}

	/**
	 * Returns the student with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the student
	 * @return the student, or <code>null</code> if a student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Student fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the student with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param studentId the primary key of the student
	 * @return the student, or <code>null</code> if a student with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Student fetchByPrimaryKey(long studentId) throws SystemException {
		Student student = (Student)EntityCacheUtil.getResult(StudentModelImpl.ENTITY_CACHE_ENABLED,
				StudentImpl.class, studentId);

		if (student == _nullStudent) {
			return null;
		}

		if (student == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				student = (Student)session.get(StudentImpl.class,
						Long.valueOf(studentId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (student != null) {
					cacheResult(student);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(StudentModelImpl.ENTITY_CACHE_ENABLED,
						StudentImpl.class, studentId, _nullStudent);
				}

				closeSession(session);
			}
		}

		return student;
	}

	/**
	 * Returns the student where studentId = &#63; or throws a {@link info.diit.portal.student.NoSuchStudentException} if it could not be found.
	 *
	 * @param studentId the student ID
	 * @return the matching student
	 * @throws info.diit.portal.student.NoSuchStudentException if a matching student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Student findByS_PHOTO(long studentId)
		throws NoSuchStudentException, SystemException {
		Student student = fetchByS_PHOTO(studentId);

		if (student == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("studentId=");
			msg.append(studentId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchStudentException(msg.toString());
		}

		return student;
	}

	/**
	 * Returns the student where studentId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param studentId the student ID
	 * @return the matching student, or <code>null</code> if a matching student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Student fetchByS_PHOTO(long studentId) throws SystemException {
		return fetchByS_PHOTO(studentId, true);
	}

	/**
	 * Returns the student where studentId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param studentId the student ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching student, or <code>null</code> if a matching student could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Student fetchByS_PHOTO(long studentId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { studentId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_S_PHOTO,
					finderArgs, this);
		}

		if (result instanceof Student) {
			Student student = (Student)result;

			if ((studentId != student.getStudentId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_SELECT_STUDENT_WHERE);

			query.append(_FINDER_COLUMN_S_PHOTO_STUDENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				List<Student> list = q.list();

				result = list;

				Student student = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_S_PHOTO,
						finderArgs, list);
				}
				else {
					student = list.get(0);

					cacheResult(student);

					if ((student.getStudentId() != studentId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_S_PHOTO,
							finderArgs, student);
					}
				}

				return student;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_S_PHOTO,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (Student)result;
			}
		}
	}

	/**
	 * Returns all the students.
	 *
	 * @return the students
	 * @throws SystemException if a system exception occurred
	 */
	public List<Student> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of students
	 * @throws SystemException if a system exception occurred
	 */
	public List<Student> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the students.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of students
	 * @throws SystemException if a system exception occurred
	 */
	public List<Student> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Student> list = (List<Student>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_STUDENT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_STUDENT;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Student>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Student>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes the student where studentId = &#63; from the database.
	 *
	 * @param studentId the student ID
	 * @return the student that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public Student removeByS_PHOTO(long studentId)
		throws NoSuchStudentException, SystemException {
		Student student = findByS_PHOTO(studentId);

		return remove(student);
	}

	/**
	 * Removes all the students from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Student student : findAll()) {
			remove(student);
		}
	}

	/**
	 * Returns the number of students where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the number of matching students
	 * @throws SystemException if a system exception occurred
	 */
	public int countByS_PHOTO(long studentId) throws SystemException {
		Object[] finderArgs = new Object[] { studentId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_S_PHOTO,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_STUDENT_WHERE);

			query.append(_FINDER_COLUMN_S_PHOTO_STUDENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_S_PHOTO,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of students.
	 *
	 * @return the number of students
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_STUDENT);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns all the academic records associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return the academic records associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.student.model.AcademicRecord> getAcademicRecords(
		long pk) throws SystemException {
		return getAcademicRecords(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the academic records associated with the student.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the student
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of academic records associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.student.model.AcademicRecord> getAcademicRecords(
		long pk, int start, int end) throws SystemException {
		return getAcademicRecords(pk, start, end, null);
	}

	public static final FinderPath FINDER_PATH_GET_ACADEMICRECORDS = new FinderPath(info.diit.portal.student.model.impl.AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.student.model.impl.AcademicRecordModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.student.model.impl.AcademicRecordImpl.class,
			info.diit.portal.student.service.persistence.AcademicRecordPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getAcademicRecords",
			new String[] {
				Long.class.getName(), "java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});

	static {
		FINDER_PATH_GET_ACADEMICRECORDS.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns an ordered range of all the academic records associated with the student.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the student
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of academic records associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.student.model.AcademicRecord> getAcademicRecords(
		long pk, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, start, end, orderByComparator };

		List<info.diit.portal.student.model.AcademicRecord> list = (List<info.diit.portal.student.model.AcademicRecord>)FinderCacheUtil.getResult(FINDER_PATH_GET_ACADEMICRECORDS,
				finderArgs, this);

		if (list == null) {
			Session session = null;

			try {
				session = openSession();

				String sql = null;

				if (orderByComparator != null) {
					sql = _SQL_GETACADEMICRECORDS.concat(ORDER_BY_CLAUSE)
												 .concat(orderByComparator.getOrderBy());
				}
				else {
					sql = _SQL_GETACADEMICRECORDS;
				}

				SQLQuery q = session.createSQLQuery(sql);

				q.addEntity("student_AcademicRecord",
					info.diit.portal.student.model.impl.AcademicRecordImpl.class);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				list = (List<info.diit.portal.student.model.AcademicRecord>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_GET_ACADEMICRECORDS,
						finderArgs);
				}
				else {
					academicRecordPersistence.cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_GET_ACADEMICRECORDS,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	public static final FinderPath FINDER_PATH_GET_ACADEMICRECORDS_SIZE = new FinderPath(info.diit.portal.student.model.impl.AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.student.model.impl.AcademicRecordModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.student.model.impl.AcademicRecordImpl.class,
			info.diit.portal.student.service.persistence.AcademicRecordPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getAcademicRecordsSize", new String[] { Long.class.getName() });

	static {
		FINDER_PATH_GET_ACADEMICRECORDS_SIZE.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns the number of academic records associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return the number of academic records associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public int getAcademicRecordsSize(long pk) throws SystemException {
		Object[] finderArgs = new Object[] { pk };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_GET_ACADEMICRECORDS_SIZE,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				SQLQuery q = session.createSQLQuery(_SQL_GETACADEMICRECORDSSIZE);

				q.addScalar(COUNT_COLUMN_NAME,
					com.liferay.portal.kernel.dao.orm.Type.LONG);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_GET_ACADEMICRECORDS_SIZE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	public static final FinderPath FINDER_PATH_CONTAINS_ACADEMICRECORD = new FinderPath(info.diit.portal.student.model.impl.AcademicRecordModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.student.model.impl.AcademicRecordModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.student.model.impl.AcademicRecordImpl.class,
			info.diit.portal.student.service.persistence.AcademicRecordPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"containsAcademicRecord",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns <code>true</code> if the academic record is associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @param academicRecordPK the primary key of the academic record
	 * @return <code>true</code> if the academic record is associated with the student; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsAcademicRecord(long pk, long academicRecordPK)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, academicRecordPK };

		Boolean value = (Boolean)FinderCacheUtil.getResult(FINDER_PATH_CONTAINS_ACADEMICRECORD,
				finderArgs, this);

		if (value == null) {
			try {
				value = Boolean.valueOf(containsAcademicRecord.contains(pk,
							academicRecordPK));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (value == null) {
					value = Boolean.FALSE;
				}

				FinderCacheUtil.putResult(FINDER_PATH_CONTAINS_ACADEMICRECORD,
					finderArgs, value);
			}
		}

		return value.booleanValue();
	}

	/**
	 * Returns <code>true</code> if the student has any academic records associated with it.
	 *
	 * @param pk the primary key of the student to check for associations with academic records
	 * @return <code>true</code> if the student has any academic records associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsAcademicRecords(long pk) throws SystemException {
		if (getAcademicRecordsSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Returns all the student documents associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return the student documents associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.student.model.StudentDocument> getStudentDocuments(
		long pk) throws SystemException {
		return getStudentDocuments(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the student documents associated with the student.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the student
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of student documents associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.student.model.StudentDocument> getStudentDocuments(
		long pk, int start, int end) throws SystemException {
		return getStudentDocuments(pk, start, end, null);
	}

	public static final FinderPath FINDER_PATH_GET_STUDENTDOCUMENTS = new FinderPath(info.diit.portal.student.model.impl.StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.student.model.impl.StudentDocumentModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.student.model.impl.StudentDocumentImpl.class,
			info.diit.portal.student.service.persistence.StudentDocumentPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getStudentDocuments",
			new String[] {
				Long.class.getName(), "java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});

	static {
		FINDER_PATH_GET_STUDENTDOCUMENTS.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns an ordered range of all the student documents associated with the student.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the student
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of student documents associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.student.model.StudentDocument> getStudentDocuments(
		long pk, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, start, end, orderByComparator };

		List<info.diit.portal.student.model.StudentDocument> list = (List<info.diit.portal.student.model.StudentDocument>)FinderCacheUtil.getResult(FINDER_PATH_GET_STUDENTDOCUMENTS,
				finderArgs, this);

		if (list == null) {
			Session session = null;

			try {
				session = openSession();

				String sql = null;

				if (orderByComparator != null) {
					sql = _SQL_GETSTUDENTDOCUMENTS.concat(ORDER_BY_CLAUSE)
												  .concat(orderByComparator.getOrderBy());
				}
				else {
					sql = _SQL_GETSTUDENTDOCUMENTS;
				}

				SQLQuery q = session.createSQLQuery(sql);

				q.addEntity("student_StudentDocument",
					info.diit.portal.student.model.impl.StudentDocumentImpl.class);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				list = (List<info.diit.portal.student.model.StudentDocument>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_GET_STUDENTDOCUMENTS,
						finderArgs);
				}
				else {
					studentDocumentPersistence.cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_GET_STUDENTDOCUMENTS,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	public static final FinderPath FINDER_PATH_GET_STUDENTDOCUMENTS_SIZE = new FinderPath(info.diit.portal.student.model.impl.StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.student.model.impl.StudentDocumentModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.student.model.impl.StudentDocumentImpl.class,
			info.diit.portal.student.service.persistence.StudentDocumentPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getStudentDocumentsSize", new String[] { Long.class.getName() });

	static {
		FINDER_PATH_GET_STUDENTDOCUMENTS_SIZE.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns the number of student documents associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return the number of student documents associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public int getStudentDocumentsSize(long pk) throws SystemException {
		Object[] finderArgs = new Object[] { pk };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_GET_STUDENTDOCUMENTS_SIZE,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				SQLQuery q = session.createSQLQuery(_SQL_GETSTUDENTDOCUMENTSSIZE);

				q.addScalar(COUNT_COLUMN_NAME,
					com.liferay.portal.kernel.dao.orm.Type.LONG);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_GET_STUDENTDOCUMENTS_SIZE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	public static final FinderPath FINDER_PATH_CONTAINS_STUDENTDOCUMENT = new FinderPath(info.diit.portal.student.model.impl.StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.student.model.impl.StudentDocumentModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.student.model.impl.StudentDocumentImpl.class,
			info.diit.portal.student.service.persistence.StudentDocumentPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"containsStudentDocument",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns <code>true</code> if the student document is associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @param studentDocumentPK the primary key of the student document
	 * @return <code>true</code> if the student document is associated with the student; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsStudentDocument(long pk, long studentDocumentPK)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, studentDocumentPK };

		Boolean value = (Boolean)FinderCacheUtil.getResult(FINDER_PATH_CONTAINS_STUDENTDOCUMENT,
				finderArgs, this);

		if (value == null) {
			try {
				value = Boolean.valueOf(containsStudentDocument.contains(pk,
							studentDocumentPK));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (value == null) {
					value = Boolean.FALSE;
				}

				FinderCacheUtil.putResult(FINDER_PATH_CONTAINS_STUDENTDOCUMENT,
					finderArgs, value);
			}
		}

		return value.booleanValue();
	}

	/**
	 * Returns <code>true</code> if the student has any student documents associated with it.
	 *
	 * @param pk the primary key of the student to check for associations with student documents
	 * @return <code>true</code> if the student has any student documents associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsStudentDocuments(long pk) throws SystemException {
		if (getStudentDocumentsSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Returns all the experiances associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return the experiances associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.student.model.Experiance> getExperiances(
		long pk) throws SystemException {
		return getExperiances(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the experiances associated with the student.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the student
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of experiances associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.student.model.Experiance> getExperiances(
		long pk, int start, int end) throws SystemException {
		return getExperiances(pk, start, end, null);
	}

	public static final FinderPath FINDER_PATH_GET_EXPERIANCES = new FinderPath(info.diit.portal.student.model.impl.ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.student.model.impl.ExperianceModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.student.model.impl.ExperianceImpl.class,
			info.diit.portal.student.service.persistence.ExperiancePersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getExperiances",
			new String[] {
				Long.class.getName(), "java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});

	static {
		FINDER_PATH_GET_EXPERIANCES.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns an ordered range of all the experiances associated with the student.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the student
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of experiances associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.student.model.Experiance> getExperiances(
		long pk, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, start, end, orderByComparator };

		List<info.diit.portal.student.model.Experiance> list = (List<info.diit.portal.student.model.Experiance>)FinderCacheUtil.getResult(FINDER_PATH_GET_EXPERIANCES,
				finderArgs, this);

		if (list == null) {
			Session session = null;

			try {
				session = openSession();

				String sql = null;

				if (orderByComparator != null) {
					sql = _SQL_GETEXPERIANCES.concat(ORDER_BY_CLAUSE)
											 .concat(orderByComparator.getOrderBy());
				}
				else {
					sql = _SQL_GETEXPERIANCES;
				}

				SQLQuery q = session.createSQLQuery(sql);

				q.addEntity("student_Experiance",
					info.diit.portal.student.model.impl.ExperianceImpl.class);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				list = (List<info.diit.portal.student.model.Experiance>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_GET_EXPERIANCES,
						finderArgs);
				}
				else {
					experiancePersistence.cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_GET_EXPERIANCES,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	public static final FinderPath FINDER_PATH_GET_EXPERIANCES_SIZE = new FinderPath(info.diit.portal.student.model.impl.ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.student.model.impl.ExperianceModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.student.model.impl.ExperianceImpl.class,
			info.diit.portal.student.service.persistence.ExperiancePersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getExperiancesSize", new String[] { Long.class.getName() });

	static {
		FINDER_PATH_GET_EXPERIANCES_SIZE.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns the number of experiances associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return the number of experiances associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public int getExperiancesSize(long pk) throws SystemException {
		Object[] finderArgs = new Object[] { pk };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_GET_EXPERIANCES_SIZE,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				SQLQuery q = session.createSQLQuery(_SQL_GETEXPERIANCESSIZE);

				q.addScalar(COUNT_COLUMN_NAME,
					com.liferay.portal.kernel.dao.orm.Type.LONG);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_GET_EXPERIANCES_SIZE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	public static final FinderPath FINDER_PATH_CONTAINS_EXPERIANCE = new FinderPath(info.diit.portal.student.model.impl.ExperianceModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.student.model.impl.ExperianceModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.student.model.impl.ExperianceImpl.class,
			info.diit.portal.student.service.persistence.ExperiancePersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"containsExperiance",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns <code>true</code> if the experiance is associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @param experiancePK the primary key of the experiance
	 * @return <code>true</code> if the experiance is associated with the student; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsExperiance(long pk, long experiancePK)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, experiancePK };

		Boolean value = (Boolean)FinderCacheUtil.getResult(FINDER_PATH_CONTAINS_EXPERIANCE,
				finderArgs, this);

		if (value == null) {
			try {
				value = Boolean.valueOf(containsExperiance.contains(pk,
							experiancePK));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (value == null) {
					value = Boolean.FALSE;
				}

				FinderCacheUtil.putResult(FINDER_PATH_CONTAINS_EXPERIANCE,
					finderArgs, value);
			}
		}

		return value.booleanValue();
	}

	/**
	 * Returns <code>true</code> if the student has any experiances associated with it.
	 *
	 * @param pk the primary key of the student to check for associations with experiances
	 * @return <code>true</code> if the student has any experiances associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsExperiances(long pk) throws SystemException {
		if (getExperiancesSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Returns all the batchs associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return the batchs associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.student.model.Batch> getBatchs(long pk)
		throws SystemException {
		return getBatchs(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the batchs associated with the student.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the student
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @return the range of batchs associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.student.model.Batch> getBatchs(long pk,
		int start, int end) throws SystemException {
		return getBatchs(pk, start, end, null);
	}

	public static final FinderPath FINDER_PATH_GET_BATCHS = new FinderPath(info.diit.portal.student.model.impl.BatchModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.student.model.impl.BatchModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.student.model.impl.BatchImpl.class,
			info.diit.portal.student.service.persistence.BatchPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getBatchs",
			new String[] {
				Long.class.getName(), "java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});

	static {
		FINDER_PATH_GET_BATCHS.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns an ordered range of all the batchs associated with the student.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param pk the primary key of the student
	 * @param start the lower bound of the range of students
	 * @param end the upper bound of the range of students (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of batchs associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public List<info.diit.portal.student.model.Batch> getBatchs(long pk,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, start, end, orderByComparator };

		List<info.diit.portal.student.model.Batch> list = (List<info.diit.portal.student.model.Batch>)FinderCacheUtil.getResult(FINDER_PATH_GET_BATCHS,
				finderArgs, this);

		if (list == null) {
			Session session = null;

			try {
				session = openSession();

				String sql = null;

				if (orderByComparator != null) {
					sql = _SQL_GETBATCHS.concat(ORDER_BY_CLAUSE)
										.concat(orderByComparator.getOrderBy());
				}
				else {
					sql = _SQL_GETBATCHS;
				}

				SQLQuery q = session.createSQLQuery(sql);

				q.addEntity("student_Batch",
					info.diit.portal.student.model.impl.BatchImpl.class);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				list = (List<info.diit.portal.student.model.Batch>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_GET_BATCHS,
						finderArgs);
				}
				else {
					batchPersistence.cacheResult(list);

					FinderCacheUtil.putResult(FINDER_PATH_GET_BATCHS,
						finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	public static final FinderPath FINDER_PATH_GET_BATCHS_SIZE = new FinderPath(info.diit.portal.student.model.impl.BatchModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.student.model.impl.BatchModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.student.model.impl.BatchImpl.class,
			info.diit.portal.student.service.persistence.BatchPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"getBatchsSize", new String[] { Long.class.getName() });

	static {
		FINDER_PATH_GET_BATCHS_SIZE.setCacheKeyGeneratorCacheName(null);
	}

	/**
	 * Returns the number of batchs associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @return the number of batchs associated with the student
	 * @throws SystemException if a system exception occurred
	 */
	public int getBatchsSize(long pk) throws SystemException {
		Object[] finderArgs = new Object[] { pk };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_GET_BATCHS_SIZE,
				finderArgs, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				SQLQuery q = session.createSQLQuery(_SQL_GETBATCHSSIZE);

				q.addScalar(COUNT_COLUMN_NAME,
					com.liferay.portal.kernel.dao.orm.Type.LONG);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(pk);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_GET_BATCHS_SIZE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	public static final FinderPath FINDER_PATH_CONTAINS_BATCH = new FinderPath(info.diit.portal.student.model.impl.BatchModelImpl.ENTITY_CACHE_ENABLED,
			info.diit.portal.student.model.impl.BatchModelImpl.FINDER_CACHE_ENABLED,
			info.diit.portal.student.model.impl.BatchImpl.class,
			info.diit.portal.student.service.persistence.BatchPersistenceImpl.FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"containsBatch",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns <code>true</code> if the batch is associated with the student.
	 *
	 * @param pk the primary key of the student
	 * @param batchPK the primary key of the batch
	 * @return <code>true</code> if the batch is associated with the student; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsBatch(long pk, long batchPK)
		throws SystemException {
		Object[] finderArgs = new Object[] { pk, batchPK };

		Boolean value = (Boolean)FinderCacheUtil.getResult(FINDER_PATH_CONTAINS_BATCH,
				finderArgs, this);

		if (value == null) {
			try {
				value = Boolean.valueOf(containsBatch.contains(pk, batchPK));
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (value == null) {
					value = Boolean.FALSE;
				}

				FinderCacheUtil.putResult(FINDER_PATH_CONTAINS_BATCH,
					finderArgs, value);
			}
		}

		return value.booleanValue();
	}

	/**
	 * Returns <code>true</code> if the student has any batchs associated with it.
	 *
	 * @param pk the primary key of the student to check for associations with batchs
	 * @return <code>true</code> if the student has any batchs associated with it; <code>false</code> otherwise
	 * @throws SystemException if a system exception occurred
	 */
	public boolean containsBatchs(long pk) throws SystemException {
		if (getBatchsSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Initializes the student persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.student.model.Student")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Student>> listenersList = new ArrayList<ModelListener<Student>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Student>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}

		containsAcademicRecord = new ContainsAcademicRecord();

		containsStudentDocument = new ContainsStudentDocument();

		containsExperiance = new ContainsExperiance();

		containsBatch = new ContainsBatch();
	}

	public void destroy() {
		EntityCacheUtil.removeCache(StudentImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	protected ContainsAcademicRecord containsAcademicRecord;
	protected ContainsStudentDocument containsStudentDocument;
	protected ContainsExperiance containsExperiance;
	protected ContainsBatch containsBatch;

	protected class ContainsAcademicRecord {
		protected ContainsAcademicRecord() {
			_mappingSqlQuery = MappingSqlQueryFactoryUtil.getMappingSqlQuery(getDataSource(),
					_SQL_CONTAINSACADEMICRECORD,
					new int[] { java.sql.Types.BIGINT, java.sql.Types.BIGINT },
					RowMapper.COUNT);
		}

		protected boolean contains(long studentId, long academicRecordId) {
			List<Integer> results = _mappingSqlQuery.execute(new Object[] {
						new Long(studentId), new Long(academicRecordId)
					});

			if (results.size() > 0) {
				Integer count = results.get(0);

				if (count.intValue() > 0) {
					return true;
				}
			}

			return false;
		}

		private MappingSqlQuery<Integer> _mappingSqlQuery;
	}

	protected class ContainsStudentDocument {
		protected ContainsStudentDocument() {
			_mappingSqlQuery = MappingSqlQueryFactoryUtil.getMappingSqlQuery(getDataSource(),
					_SQL_CONTAINSSTUDENTDOCUMENT,
					new int[] { java.sql.Types.BIGINT, java.sql.Types.BIGINT },
					RowMapper.COUNT);
		}

		protected boolean contains(long studentId, long documentId) {
			List<Integer> results = _mappingSqlQuery.execute(new Object[] {
						new Long(studentId), new Long(documentId)
					});

			if (results.size() > 0) {
				Integer count = results.get(0);

				if (count.intValue() > 0) {
					return true;
				}
			}

			return false;
		}

		private MappingSqlQuery<Integer> _mappingSqlQuery;
	}

	protected class ContainsExperiance {
		protected ContainsExperiance() {
			_mappingSqlQuery = MappingSqlQueryFactoryUtil.getMappingSqlQuery(getDataSource(),
					_SQL_CONTAINSEXPERIANCE,
					new int[] { java.sql.Types.BIGINT, java.sql.Types.BIGINT },
					RowMapper.COUNT);
		}

		protected boolean contains(long studentId, long experianceId) {
			List<Integer> results = _mappingSqlQuery.execute(new Object[] {
						new Long(studentId), new Long(experianceId)
					});

			if (results.size() > 0) {
				Integer count = results.get(0);

				if (count.intValue() > 0) {
					return true;
				}
			}

			return false;
		}

		private MappingSqlQuery<Integer> _mappingSqlQuery;
	}

	protected class ContainsBatch {
		protected ContainsBatch() {
			_mappingSqlQuery = MappingSqlQueryFactoryUtil.getMappingSqlQuery(getDataSource(),
					_SQL_CONTAINSBATCH,
					new int[] { java.sql.Types.BIGINT, java.sql.Types.BIGINT },
					RowMapper.COUNT);
		}

		protected boolean contains(long studentId, long batchId) {
			List<Integer> results = _mappingSqlQuery.execute(new Object[] {
						new Long(studentId), new Long(batchId)
					});

			if (results.size() > 0) {
				Integer count = results.get(0);

				if (count.intValue() > 0) {
					return true;
				}
			}

			return false;
		}

		private MappingSqlQuery<Integer> _mappingSqlQuery;
	}

	private static final String _SQL_SELECT_STUDENT = "SELECT student FROM Student student";
	private static final String _SQL_SELECT_STUDENT_WHERE = "SELECT student FROM Student student WHERE ";
	private static final String _SQL_COUNT_STUDENT = "SELECT COUNT(student) FROM Student student";
	private static final String _SQL_COUNT_STUDENT_WHERE = "SELECT COUNT(student) FROM Student student WHERE ";
	private static final String _SQL_GETACADEMICRECORDS = "SELECT {student_AcademicRecord.*} FROM student_AcademicRecord INNER JOIN student_Student ON (student_Student.studentId = student_AcademicRecord.studentId) WHERE (student_Student.studentId = ?)";
	private static final String _SQL_GETACADEMICRECORDSSIZE = "SELECT COUNT(*) AS COUNT_VALUE FROM student_AcademicRecord WHERE studentId = ?";
	private static final String _SQL_CONTAINSACADEMICRECORD = "SELECT COUNT(*) AS COUNT_VALUE FROM student_AcademicRecord WHERE studentId = ? AND academicRecordId = ?";
	private static final String _SQL_GETSTUDENTDOCUMENTS = "SELECT {student_StudentDocument.*} FROM student_StudentDocument INNER JOIN student_Student ON (student_Student.studentId = student_StudentDocument.studentId) WHERE (student_Student.studentId = ?)";
	private static final String _SQL_GETSTUDENTDOCUMENTSSIZE = "SELECT COUNT(*) AS COUNT_VALUE FROM student_StudentDocument WHERE studentId = ?";
	private static final String _SQL_CONTAINSSTUDENTDOCUMENT = "SELECT COUNT(*) AS COUNT_VALUE FROM student_StudentDocument WHERE studentId = ? AND documentId = ?";
	private static final String _SQL_GETEXPERIANCES = "SELECT {student_Experiance.*} FROM student_Experiance INNER JOIN student_Student ON (student_Student.studentId = student_Experiance.studentId) WHERE (student_Student.studentId = ?)";
	private static final String _SQL_GETEXPERIANCESSIZE = "SELECT COUNT(*) AS COUNT_VALUE FROM student_Experiance WHERE studentId = ?";
	private static final String _SQL_CONTAINSEXPERIANCE = "SELECT COUNT(*) AS COUNT_VALUE FROM student_Experiance WHERE studentId = ? AND experianceId = ?";
	private static final String _SQL_GETBATCHS = "SELECT {student_Batch.*} FROM student_Batch INNER JOIN student_Student ON (student_Student.studentId = student_Batch.studentId) WHERE (student_Student.studentId = ?)";
	private static final String _SQL_GETBATCHSSIZE = "SELECT COUNT(*) AS COUNT_VALUE FROM student_Batch WHERE studentId = ?";
	private static final String _SQL_CONTAINSBATCH = "SELECT COUNT(*) AS COUNT_VALUE FROM student_Batch WHERE studentId = ? AND batchId = ?";
	private static final String _FINDER_COLUMN_S_PHOTO_STUDENTID_2 = "student.studentId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "student.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Student exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Student exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(StudentPersistenceImpl.class);
	private static Student _nullStudent = new StudentImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Student> toCacheModel() {
				return _nullStudentCacheModel;
			}
		};

	private static CacheModel<Student> _nullStudentCacheModel = new CacheModel<Student>() {
			public Student toEntityModel() {
				return _nullStudent;
			}
		};
}
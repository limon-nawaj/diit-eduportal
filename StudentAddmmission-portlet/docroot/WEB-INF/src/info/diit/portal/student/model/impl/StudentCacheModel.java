/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.student.model.Student;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Student in entity cache.
 *
 * @author nasimul
 * @see Student
 * @generated
 */
public class StudentCacheModel implements CacheModel<Student>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(49);

		sb.append("{studentId=");
		sb.append(studentId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", fatherName=");
		sb.append(fatherName);
		sb.append(", motherName=");
		sb.append(motherName);
		sb.append(", presentAddress=");
		sb.append(presentAddress);
		sb.append(", permanentAddress=");
		sb.append(permanentAddress);
		sb.append(", homePhone=");
		sb.append(homePhone);
		sb.append(", fatherMobile=");
		sb.append(fatherMobile);
		sb.append(", motherMobile=");
		sb.append(motherMobile);
		sb.append(", guardianMobile=");
		sb.append(guardianMobile);
		sb.append(", Mobile1=");
		sb.append(Mobile1);
		sb.append(", Mobile2=");
		sb.append(Mobile2);
		sb.append(", dateOfBirth=");
		sb.append(dateOfBirth);
		sb.append(", email=");
		sb.append(email);
		sb.append(", gender=");
		sb.append(gender);
		sb.append(", religion=");
		sb.append(religion);
		sb.append(", nationality=");
		sb.append(nationality);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	public Student toEntityModel() {
		StudentImpl studentImpl = new StudentImpl();

		studentImpl.setStudentId(studentId);
		studentImpl.setCompanyId(companyId);
		studentImpl.setUserId(userId);

		if (userName == null) {
			studentImpl.setUserName(StringPool.BLANK);
		}
		else {
			studentImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			studentImpl.setCreateDate(null);
		}
		else {
			studentImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			studentImpl.setModifiedDate(null);
		}
		else {
			studentImpl.setModifiedDate(new Date(modifiedDate));
		}

		studentImpl.setOrganizationId(organizationId);

		if (name == null) {
			studentImpl.setName(StringPool.BLANK);
		}
		else {
			studentImpl.setName(name);
		}

		if (fatherName == null) {
			studentImpl.setFatherName(StringPool.BLANK);
		}
		else {
			studentImpl.setFatherName(fatherName);
		}

		if (motherName == null) {
			studentImpl.setMotherName(StringPool.BLANK);
		}
		else {
			studentImpl.setMotherName(motherName);
		}

		if (presentAddress == null) {
			studentImpl.setPresentAddress(StringPool.BLANK);
		}
		else {
			studentImpl.setPresentAddress(presentAddress);
		}

		if (permanentAddress == null) {
			studentImpl.setPermanentAddress(StringPool.BLANK);
		}
		else {
			studentImpl.setPermanentAddress(permanentAddress);
		}

		if (homePhone == null) {
			studentImpl.setHomePhone(StringPool.BLANK);
		}
		else {
			studentImpl.setHomePhone(homePhone);
		}

		if (fatherMobile == null) {
			studentImpl.setFatherMobile(StringPool.BLANK);
		}
		else {
			studentImpl.setFatherMobile(fatherMobile);
		}

		if (motherMobile == null) {
			studentImpl.setMotherMobile(StringPool.BLANK);
		}
		else {
			studentImpl.setMotherMobile(motherMobile);
		}

		if (guardianMobile == null) {
			studentImpl.setGuardianMobile(StringPool.BLANK);
		}
		else {
			studentImpl.setGuardianMobile(guardianMobile);
		}

		if (Mobile1 == null) {
			studentImpl.setMobile1(StringPool.BLANK);
		}
		else {
			studentImpl.setMobile1(Mobile1);
		}

		if (Mobile2 == null) {
			studentImpl.setMobile2(StringPool.BLANK);
		}
		else {
			studentImpl.setMobile2(Mobile2);
		}

		if (dateOfBirth == Long.MIN_VALUE) {
			studentImpl.setDateOfBirth(null);
		}
		else {
			studentImpl.setDateOfBirth(new Date(dateOfBirth));
		}

		if (email == null) {
			studentImpl.setEmail(StringPool.BLANK);
		}
		else {
			studentImpl.setEmail(email);
		}

		if (gender == null) {
			studentImpl.setGender(StringPool.BLANK);
		}
		else {
			studentImpl.setGender(gender);
		}

		if (religion == null) {
			studentImpl.setReligion(StringPool.BLANK);
		}
		else {
			studentImpl.setReligion(religion);
		}

		if (nationality == null) {
			studentImpl.setNationality(StringPool.BLANK);
		}
		else {
			studentImpl.setNationality(nationality);
		}

		if (status == null) {
			studentImpl.setStatus(StringPool.BLANK);
		}
		else {
			studentImpl.setStatus(status);
		}

		studentImpl.resetOriginalValues();

		return studentImpl;
	}

	public long studentId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long organizationId;
	public String name;
	public String fatherName;
	public String motherName;
	public String presentAddress;
	public String permanentAddress;
	public String homePhone;
	public String fatherMobile;
	public String motherMobile;
	public String guardianMobile;
	public String Mobile1;
	public String Mobile2;
	public long dateOfBirth;
	public String email;
	public String gender;
	public String religion;
	public String nationality;
	public String status;
}
package info.diit.portal.student.dto;

import java.util.Date;

public class ExperianceDto {
	private long _experianceId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private String _organization;
	private String _designation;
	private Date _startDate;
	private Date _endDate;
	
	
	public long get_experianceId() {
		return _experianceId;
	}
	public void set_experianceId(long _experianceId) {
		this._experianceId = _experianceId;
	}
	public long get_companyId() {
		return _companyId;
	}
	public void set_companyId(long _companyId) {
		this._companyId = _companyId;
	}
	public long get_userId() {
		return _userId;
	}
	public void set_userId(long _userId) {
		this._userId = _userId;
	}
	public String get_userUuid() {
		return _userUuid;
	}
	public void set_userUuid(String _userUuid) {
		this._userUuid = _userUuid;
	}
	public String get_userName() {
		return _userName;
	}
	public void set_userName(String _userName) {
		this._userName = _userName;
	}
	public Date get_createDate() {
		return _createDate;
	}
	public void set_createDate(Date _createDate) {
		this._createDate = _createDate;
	}
	public Date get_modifiedDate() {
		return _modifiedDate;
	}
	public void set_modifiedDate(Date _modifiedDate) {
		this._modifiedDate = _modifiedDate;
	}
	public long get_organizationId() {
		return _organizationId;
	}
	public void set_organizationId(long _organizationId) {
		this._organizationId = _organizationId;
	}
	public String get_organization() {
		return _organization;
	}
	public void set_organization(String _organization) {
		this._organization = _organization;
	}
	public String get_designation() {
		return _designation;
	}
	public void set_designation(String _designation) {
		this._designation = _designation;
	}
	public Date get_startDate() {
		return _startDate;
	}
	public void set_startDate(Date _startDate) {
		this._startDate = _startDate;
	}
	public Date get_endDate() {
		return _endDate;
	}
	public void set_endDate(Date _endDate) {
		this._endDate = _endDate;
	}
	
	
}

package info.diit.portal.student.dto;

import java.util.Date;

public class AcademicRecordDto {
	private long _academicRecordId;
	private long _organisationId;
	private String _degree;
	private String _board;
	private int _year;
	private String _result;
	private String _registrationNo;
	public long get_academicRecordId() {
		return _academicRecordId;
	}
	public void set_academicRecordId(long _academicRecordId) {
		this._academicRecordId = _academicRecordId;
	}
	public long get_organisationId() {
		return _organisationId;
	}
	public void set_organisationId(long _organisationId) {
		this._organisationId = _organisationId;
	}
	public String get_degree() {
		return _degree;
	}
	public void set_degree(String _degree) {
		this._degree = _degree;
	}
	public String get_board() {
		return _board;
	}
	public void set_board(String _board) {
		this._board = _board;
	}
	public int get_year() {
		return _year;
	}
	public void set_year(int _year) {
		this._year = _year;
	}
	public String get_result() {
		return _result;
	}
	public void set_result(String _result) {
		this._result = _result;
	}
	public String get_registrationNo() {
		return _registrationNo;
	}
	public void set_registrationNo(String _registrationNo) {
		this._registrationNo = _registrationNo;
	}
	
	
}

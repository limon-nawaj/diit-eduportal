/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.student.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.student.model.AccademicRecord;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing AccademicRecord in entity cache.
 *
 * @author nasimul
 * @see AccademicRecord
 * @generated
 */
public class AccademicRecordCacheModel implements CacheModel<AccademicRecord>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{accademicRecordId=");
		sb.append(accademicRecordId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", organisationId=");
		sb.append(organisationId);
		sb.append(", degree=");
		sb.append(degree);
		sb.append(", board=");
		sb.append(board);
		sb.append(", year=");
		sb.append(year);
		sb.append(", result=");
		sb.append(result);
		sb.append(", registrationNo=");
		sb.append(registrationNo);
		sb.append("}");

		return sb.toString();
	}

	public AccademicRecord toEntityModel() {
		AccademicRecordImpl accademicRecordImpl = new AccademicRecordImpl();

		accademicRecordImpl.setAccademicRecordId(accademicRecordId);
		accademicRecordImpl.setCompanyId(companyId);
		accademicRecordImpl.setUserId(userId);

		if (userName == null) {
			accademicRecordImpl.setUserName(StringPool.BLANK);
		}
		else {
			accademicRecordImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			accademicRecordImpl.setCreateDate(null);
		}
		else {
			accademicRecordImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			accademicRecordImpl.setModifiedDate(null);
		}
		else {
			accademicRecordImpl.setModifiedDate(new Date(modifiedDate));
		}

		accademicRecordImpl.setOrganisationId(organisationId);

		if (degree == null) {
			accademicRecordImpl.setDegree(StringPool.BLANK);
		}
		else {
			accademicRecordImpl.setDegree(degree);
		}

		if (board == null) {
			accademicRecordImpl.setBoard(StringPool.BLANK);
		}
		else {
			accademicRecordImpl.setBoard(board);
		}

		accademicRecordImpl.setYear(year);

		if (result == null) {
			accademicRecordImpl.setResult(StringPool.BLANK);
		}
		else {
			accademicRecordImpl.setResult(result);
		}

		if (registrationNo == null) {
			accademicRecordImpl.setRegistrationNo(StringPool.BLANK);
		}
		else {
			accademicRecordImpl.setRegistrationNo(registrationNo);
		}

		accademicRecordImpl.resetOriginalValues();

		return accademicRecordImpl;
	}

	public long accademicRecordId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long organisationId;
	public String degree;
	public String board;
	public int year;
	public String result;
	public String registrationNo;
}
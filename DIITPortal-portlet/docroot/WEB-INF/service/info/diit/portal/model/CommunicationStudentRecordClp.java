/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import info.diit.portal.service.CommunicationStudentRecordLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class CommunicationStudentRecordClp extends BaseModelImpl<CommunicationStudentRecord>
	implements CommunicationStudentRecord {
	public CommunicationStudentRecordClp() {
	}

	public Class<?> getModelClass() {
		return CommunicationStudentRecord.class;
	}

	public String getModelClassName() {
		return CommunicationStudentRecord.class.getName();
	}

	public long getPrimaryKey() {
		return _CommunicationStudentRecorId;
	}

	public void setPrimaryKey(long primaryKey) {
		setCommunicationStudentRecorId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_CommunicationStudentRecorId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("CommunicationStudentRecorId",
			getCommunicationStudentRecorId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("communicationBy", getCommunicationBy());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("communicationRecordId", getCommunicationRecordId());
		attributes.put("studentId", getStudentId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long CommunicationStudentRecorId = (Long)attributes.get(
				"CommunicationStudentRecorId");

		if (CommunicationStudentRecorId != null) {
			setCommunicationStudentRecorId(CommunicationStudentRecorId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long communicationBy = (Long)attributes.get("communicationBy");

		if (communicationBy != null) {
			setCommunicationBy(communicationBy);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long communicationRecordId = (Long)attributes.get(
				"communicationRecordId");

		if (communicationRecordId != null) {
			setCommunicationRecordId(communicationRecordId);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}
	}

	public long getCommunicationStudentRecorId() {
		return _CommunicationStudentRecorId;
	}

	public void setCommunicationStudentRecorId(long CommunicationStudentRecorId) {
		_CommunicationStudentRecorId = CommunicationStudentRecorId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getCommunicationBy() {
		return _communicationBy;
	}

	public void setCommunicationBy(long communicationBy) {
		_communicationBy = communicationBy;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getCommunicationRecordId() {
		return _communicationRecordId;
	}

	public void setCommunicationRecordId(long communicationRecordId) {
		_communicationRecordId = communicationRecordId;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	public BaseModel<?> getCommunicationStudentRecordRemoteModel() {
		return _communicationStudentRecordRemoteModel;
	}

	public void setCommunicationStudentRecordRemoteModel(
		BaseModel<?> communicationStudentRecordRemoteModel) {
		_communicationStudentRecordRemoteModel = communicationStudentRecordRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			CommunicationStudentRecordLocalServiceUtil.addCommunicationStudentRecord(this);
		}
		else {
			CommunicationStudentRecordLocalServiceUtil.updateCommunicationStudentRecord(this);
		}
	}

	@Override
	public CommunicationStudentRecord toEscapedModel() {
		return (CommunicationStudentRecord)Proxy.newProxyInstance(CommunicationStudentRecord.class.getClassLoader(),
			new Class[] { CommunicationStudentRecord.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CommunicationStudentRecordClp clone = new CommunicationStudentRecordClp();

		clone.setCommunicationStudentRecorId(getCommunicationStudentRecorId());
		clone.setCompanyId(getCompanyId());
		clone.setOrganizationId(getOrganizationId());
		clone.setCommunicationBy(getCommunicationBy());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setCommunicationRecordId(getCommunicationRecordId());
		clone.setStudentId(getStudentId());

		return clone;
	}

	public int compareTo(CommunicationStudentRecord communicationStudentRecord) {
		long primaryKey = communicationStudentRecord.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		CommunicationStudentRecordClp communicationStudentRecord = null;

		try {
			communicationStudentRecord = (CommunicationStudentRecordClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = communicationStudentRecord.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{CommunicationStudentRecorId=");
		sb.append(getCommunicationStudentRecorId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", communicationBy=");
		sb.append(getCommunicationBy());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", communicationRecordId=");
		sb.append(getCommunicationRecordId());
		sb.append(", studentId=");
		sb.append(getStudentId());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(31);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.CommunicationStudentRecord");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>CommunicationStudentRecorId</column-name><column-value><![CDATA[");
		sb.append(getCommunicationStudentRecorId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>communicationBy</column-name><column-value><![CDATA[");
		sb.append(getCommunicationBy());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>communicationRecordId</column-name><column-value><![CDATA[");
		sb.append(getCommunicationRecordId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>studentId</column-name><column-value><![CDATA[");
		sb.append(getStudentId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _CommunicationStudentRecorId;
	private long _companyId;
	private long _organizationId;
	private long _communicationBy;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _communicationRecordId;
	private long _studentId;
	private BaseModel<?> _communicationStudentRecordRemoteModel;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.AuditedModel;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the DailyWork service. Represents a row in the &quot;EduPortal_DailyWork&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link info.diit.portal.model.impl.DailyWorkModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link info.diit.portal.model.impl.DailyWorkImpl}.
 * </p>
 *
 * @author mohammad
 * @see DailyWork
 * @see info.diit.portal.model.impl.DailyWorkImpl
 * @see info.diit.portal.model.impl.DailyWorkModelImpl
 * @generated
 */
public interface DailyWorkModel extends AuditedModel, BaseModel<DailyWork> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a daily work model instance should use the {@link DailyWork} interface instead.
	 */

	/**
	 * Returns the primary key of this daily work.
	 *
	 * @return the primary key of this daily work
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this daily work.
	 *
	 * @param primaryKey the primary key of this daily work
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the daily work ID of this daily work.
	 *
	 * @return the daily work ID of this daily work
	 */
	public long getDailyWorkId();

	/**
	 * Sets the daily work ID of this daily work.
	 *
	 * @param dailyWorkId the daily work ID of this daily work
	 */
	public void setDailyWorkId(long dailyWorkId);

	/**
	 * Returns the company ID of this daily work.
	 *
	 * @return the company ID of this daily work
	 */
	public long getCompanyId();

	/**
	 * Sets the company ID of this daily work.
	 *
	 * @param companyId the company ID of this daily work
	 */
	public void setCompanyId(long companyId);

	/**
	 * Returns the user ID of this daily work.
	 *
	 * @return the user ID of this daily work
	 */
	public long getUserId();

	/**
	 * Sets the user ID of this daily work.
	 *
	 * @param userId the user ID of this daily work
	 */
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this daily work.
	 *
	 * @return the user uuid of this daily work
	 * @throws SystemException if a system exception occurred
	 */
	public String getUserUuid() throws SystemException;

	/**
	 * Sets the user uuid of this daily work.
	 *
	 * @param userUuid the user uuid of this daily work
	 */
	public void setUserUuid(String userUuid);

	/**
	 * Returns the user name of this daily work.
	 *
	 * @return the user name of this daily work
	 */
	@AutoEscape
	public String getUserName();

	/**
	 * Sets the user name of this daily work.
	 *
	 * @param userName the user name of this daily work
	 */
	public void setUserName(String userName);

	/**
	 * Returns the create date of this daily work.
	 *
	 * @return the create date of this daily work
	 */
	public Date getCreateDate();

	/**
	 * Sets the create date of this daily work.
	 *
	 * @param createDate the create date of this daily work
	 */
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this daily work.
	 *
	 * @return the modified date of this daily work
	 */
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this daily work.
	 *
	 * @param modifiedDate the modified date of this daily work
	 */
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the employee ID of this daily work.
	 *
	 * @return the employee ID of this daily work
	 */
	public long getEmployeeId();

	/**
	 * Sets the employee ID of this daily work.
	 *
	 * @param employeeId the employee ID of this daily work
	 */
	public void setEmployeeId(long employeeId);

	/**
	 * Returns the wor king day of this daily work.
	 *
	 * @return the wor king day of this daily work
	 */
	public Date getWorKingDay();

	/**
	 * Sets the wor king day of this daily work.
	 *
	 * @param worKingDay the wor king day of this daily work
	 */
	public void setWorKingDay(Date worKingDay);

	/**
	 * Returns the task ID of this daily work.
	 *
	 * @return the task ID of this daily work
	 */
	public long getTaskId();

	/**
	 * Sets the task ID of this daily work.
	 *
	 * @param taskId the task ID of this daily work
	 */
	public void setTaskId(long taskId);

	/**
	 * Returns the time of this daily work.
	 *
	 * @return the time of this daily work
	 */
	public int getTime();

	/**
	 * Sets the time of this daily work.
	 *
	 * @param time the time of this daily work
	 */
	public void setTime(int time);

	/**
	 * Returns the note of this daily work.
	 *
	 * @return the note of this daily work
	 */
	@AutoEscape
	public String getNote();

	/**
	 * Sets the note of this daily work.
	 *
	 * @param note the note of this daily work
	 */
	public void setNote(String note);

	public boolean isNew();

	public void setNew(boolean n);

	public boolean isCachedModel();

	public void setCachedModel(boolean cachedModel);

	public boolean isEscapedModel();

	public Serializable getPrimaryKeyObj();

	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	public ExpandoBridge getExpandoBridge();

	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	public Object clone();

	public int compareTo(DailyWork dailyWork);

	public int hashCode();

	public CacheModel<DailyWork> toCacheModel();

	public DailyWork toEscapedModel();

	public String toString();

	public String toXmlString();
}
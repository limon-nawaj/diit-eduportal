/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class BatchPaymentScheduleSoap implements Serializable {
	public static BatchPaymentScheduleSoap toSoapModel(
		BatchPaymentSchedule model) {
		BatchPaymentScheduleSoap soapModel = new BatchPaymentScheduleSoap();

		soapModel.setBatchPaymentScheduleId(model.getBatchPaymentScheduleId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setBatchId(model.getBatchId());
		soapModel.setScheduleDate(model.getScheduleDate());
		soapModel.setFeeTypeId(model.getFeeTypeId());
		soapModel.setAmount(model.getAmount());

		return soapModel;
	}

	public static BatchPaymentScheduleSoap[] toSoapModels(
		BatchPaymentSchedule[] models) {
		BatchPaymentScheduleSoap[] soapModels = new BatchPaymentScheduleSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BatchPaymentScheduleSoap[][] toSoapModels(
		BatchPaymentSchedule[][] models) {
		BatchPaymentScheduleSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BatchPaymentScheduleSoap[models.length][models[0].length];
		}
		else {
			soapModels = new BatchPaymentScheduleSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BatchPaymentScheduleSoap[] toSoapModels(
		List<BatchPaymentSchedule> models) {
		List<BatchPaymentScheduleSoap> soapModels = new ArrayList<BatchPaymentScheduleSoap>(models.size());

		for (BatchPaymentSchedule model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BatchPaymentScheduleSoap[soapModels.size()]);
	}

	public BatchPaymentScheduleSoap() {
	}

	public long getPrimaryKey() {
		return _batchPaymentScheduleId;
	}

	public void setPrimaryKey(long pk) {
		setBatchPaymentScheduleId(pk);
	}

	public long getBatchPaymentScheduleId() {
		return _batchPaymentScheduleId;
	}

	public void setBatchPaymentScheduleId(long batchPaymentScheduleId) {
		_batchPaymentScheduleId = batchPaymentScheduleId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public Date getScheduleDate() {
		return _scheduleDate;
	}

	public void setScheduleDate(Date scheduleDate) {
		_scheduleDate = scheduleDate;
	}

	public long getFeeTypeId() {
		return _feeTypeId;
	}

	public void setFeeTypeId(long feeTypeId) {
		_feeTypeId = feeTypeId;
	}

	public double getAmount() {
		return _amount;
	}

	public void setAmount(double amount) {
		_amount = amount;
	}

	private long _batchPaymentScheduleId;
	private long _companyId;
	private long _userId;
	private Date _createDate;
	private Date _modifiedDate;
	private long _batchId;
	private Date _scheduleDate;
	private long _feeTypeId;
	private double _amount;
}
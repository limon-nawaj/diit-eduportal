/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class CourseOrganizationSoap implements Serializable {
	public static CourseOrganizationSoap toSoapModel(CourseOrganization model) {
		CourseOrganizationSoap soapModel = new CourseOrganizationSoap();

		soapModel.setCourseOrganizationId(model.getCourseOrganizationId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCourseId(model.getCourseId());

		return soapModel;
	}

	public static CourseOrganizationSoap[] toSoapModels(
		CourseOrganization[] models) {
		CourseOrganizationSoap[] soapModels = new CourseOrganizationSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CourseOrganizationSoap[][] toSoapModels(
		CourseOrganization[][] models) {
		CourseOrganizationSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CourseOrganizationSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CourseOrganizationSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CourseOrganizationSoap[] toSoapModels(
		List<CourseOrganization> models) {
		List<CourseOrganizationSoap> soapModels = new ArrayList<CourseOrganizationSoap>(models.size());

		for (CourseOrganization model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CourseOrganizationSoap[soapModels.size()]);
	}

	public CourseOrganizationSoap() {
	}

	public long getPrimaryKey() {
		return _courseOrganizationId;
	}

	public void setPrimaryKey(long pk) {
		setCourseOrganizationId(pk);
	}

	public long getCourseOrganizationId() {
		return _courseOrganizationId;
	}

	public void setCourseOrganizationId(long courseOrganizationId) {
		_courseOrganizationId = courseOrganizationId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getCourseId() {
		return _courseId;
	}

	public void setCourseId(long courseId) {
		_courseId = courseId;
	}

	private long _courseOrganizationId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _courseId;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CourseSubject}.
 * </p>
 *
 * @author    mohammad
 * @see       CourseSubject
 * @generated
 */
public class CourseSubjectWrapper implements CourseSubject,
	ModelWrapper<CourseSubject> {
	public CourseSubjectWrapper(CourseSubject courseSubject) {
		_courseSubject = courseSubject;
	}

	public Class<?> getModelClass() {
		return CourseSubject.class;
	}

	public String getModelClassName() {
		return CourseSubject.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("courseSubjectId", getCourseSubjectId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("courseId", getCourseId());
		attributes.put("subjectId", getSubjectId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long courseSubjectId = (Long)attributes.get("courseSubjectId");

		if (courseSubjectId != null) {
			setCourseSubjectId(courseSubjectId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long courseId = (Long)attributes.get("courseId");

		if (courseId != null) {
			setCourseId(courseId);
		}

		Long subjectId = (Long)attributes.get("subjectId");

		if (subjectId != null) {
			setSubjectId(subjectId);
		}
	}

	/**
	* Returns the primary key of this course subject.
	*
	* @return the primary key of this course subject
	*/
	public long getPrimaryKey() {
		return _courseSubject.getPrimaryKey();
	}

	/**
	* Sets the primary key of this course subject.
	*
	* @param primaryKey the primary key of this course subject
	*/
	public void setPrimaryKey(long primaryKey) {
		_courseSubject.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the course subject ID of this course subject.
	*
	* @return the course subject ID of this course subject
	*/
	public long getCourseSubjectId() {
		return _courseSubject.getCourseSubjectId();
	}

	/**
	* Sets the course subject ID of this course subject.
	*
	* @param courseSubjectId the course subject ID of this course subject
	*/
	public void setCourseSubjectId(long courseSubjectId) {
		_courseSubject.setCourseSubjectId(courseSubjectId);
	}

	/**
	* Returns the company ID of this course subject.
	*
	* @return the company ID of this course subject
	*/
	public long getCompanyId() {
		return _courseSubject.getCompanyId();
	}

	/**
	* Sets the company ID of this course subject.
	*
	* @param companyId the company ID of this course subject
	*/
	public void setCompanyId(long companyId) {
		_courseSubject.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this course subject.
	*
	* @return the organization ID of this course subject
	*/
	public long getOrganizationId() {
		return _courseSubject.getOrganizationId();
	}

	/**
	* Sets the organization ID of this course subject.
	*
	* @param organizationId the organization ID of this course subject
	*/
	public void setOrganizationId(long organizationId) {
		_courseSubject.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this course subject.
	*
	* @return the user ID of this course subject
	*/
	public long getUserId() {
		return _courseSubject.getUserId();
	}

	/**
	* Sets the user ID of this course subject.
	*
	* @param userId the user ID of this course subject
	*/
	public void setUserId(long userId) {
		_courseSubject.setUserId(userId);
	}

	/**
	* Returns the user uuid of this course subject.
	*
	* @return the user uuid of this course subject
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseSubject.getUserUuid();
	}

	/**
	* Sets the user uuid of this course subject.
	*
	* @param userUuid the user uuid of this course subject
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_courseSubject.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this course subject.
	*
	* @return the user name of this course subject
	*/
	public java.lang.String getUserName() {
		return _courseSubject.getUserName();
	}

	/**
	* Sets the user name of this course subject.
	*
	* @param userName the user name of this course subject
	*/
	public void setUserName(java.lang.String userName) {
		_courseSubject.setUserName(userName);
	}

	/**
	* Returns the create date of this course subject.
	*
	* @return the create date of this course subject
	*/
	public java.util.Date getCreateDate() {
		return _courseSubject.getCreateDate();
	}

	/**
	* Sets the create date of this course subject.
	*
	* @param createDate the create date of this course subject
	*/
	public void setCreateDate(java.util.Date createDate) {
		_courseSubject.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this course subject.
	*
	* @return the modified date of this course subject
	*/
	public java.util.Date getModifiedDate() {
		return _courseSubject.getModifiedDate();
	}

	/**
	* Sets the modified date of this course subject.
	*
	* @param modifiedDate the modified date of this course subject
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_courseSubject.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the course ID of this course subject.
	*
	* @return the course ID of this course subject
	*/
	public long getCourseId() {
		return _courseSubject.getCourseId();
	}

	/**
	* Sets the course ID of this course subject.
	*
	* @param courseId the course ID of this course subject
	*/
	public void setCourseId(long courseId) {
		_courseSubject.setCourseId(courseId);
	}

	/**
	* Returns the subject ID of this course subject.
	*
	* @return the subject ID of this course subject
	*/
	public long getSubjectId() {
		return _courseSubject.getSubjectId();
	}

	/**
	* Sets the subject ID of this course subject.
	*
	* @param subjectId the subject ID of this course subject
	*/
	public void setSubjectId(long subjectId) {
		_courseSubject.setSubjectId(subjectId);
	}

	public boolean isNew() {
		return _courseSubject.isNew();
	}

	public void setNew(boolean n) {
		_courseSubject.setNew(n);
	}

	public boolean isCachedModel() {
		return _courseSubject.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_courseSubject.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _courseSubject.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _courseSubject.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_courseSubject.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _courseSubject.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_courseSubject.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CourseSubjectWrapper((CourseSubject)_courseSubject.clone());
	}

	public int compareTo(info.diit.portal.model.CourseSubject courseSubject) {
		return _courseSubject.compareTo(courseSubject);
	}

	@Override
	public int hashCode() {
		return _courseSubject.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.CourseSubject> toCacheModel() {
		return _courseSubject.toCacheModel();
	}

	public info.diit.portal.model.CourseSubject toEscapedModel() {
		return new CourseSubjectWrapper(_courseSubject.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _courseSubject.toString();
	}

	public java.lang.String toXmlString() {
		return _courseSubject.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_courseSubject.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public CourseSubject getWrappedCourseSubject() {
		return _courseSubject;
	}

	public CourseSubject getWrappedModel() {
		return _courseSubject;
	}

	public void resetOriginalValues() {
		_courseSubject.resetOriginalValues();
	}

	private CourseSubject _courseSubject;
}
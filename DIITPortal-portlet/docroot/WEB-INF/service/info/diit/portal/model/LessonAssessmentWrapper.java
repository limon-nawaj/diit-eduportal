/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link LessonAssessment}.
 * </p>
 *
 * @author    mohammad
 * @see       LessonAssessment
 * @generated
 */
public class LessonAssessmentWrapper implements LessonAssessment,
	ModelWrapper<LessonAssessment> {
	public LessonAssessmentWrapper(LessonAssessment lessonAssessment) {
		_lessonAssessment = lessonAssessment;
	}

	public Class<?> getModelClass() {
		return LessonAssessment.class;
	}

	public String getModelClassName() {
		return LessonAssessment.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("lessonAssessmentId", getLessonAssessmentId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("lessonPlanId", getLessonPlanId());
		attributes.put("assessmentType", getAssessmentType());
		attributes.put("number", getNumber());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long lessonAssessmentId = (Long)attributes.get("lessonAssessmentId");

		if (lessonAssessmentId != null) {
			setLessonAssessmentId(lessonAssessmentId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long lessonPlanId = (Long)attributes.get("lessonPlanId");

		if (lessonPlanId != null) {
			setLessonPlanId(lessonPlanId);
		}

		Long assessmentType = (Long)attributes.get("assessmentType");

		if (assessmentType != null) {
			setAssessmentType(assessmentType);
		}

		Long number = (Long)attributes.get("number");

		if (number != null) {
			setNumber(number);
		}
	}

	/**
	* Returns the primary key of this lesson assessment.
	*
	* @return the primary key of this lesson assessment
	*/
	public long getPrimaryKey() {
		return _lessonAssessment.getPrimaryKey();
	}

	/**
	* Sets the primary key of this lesson assessment.
	*
	* @param primaryKey the primary key of this lesson assessment
	*/
	public void setPrimaryKey(long primaryKey) {
		_lessonAssessment.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the lesson assessment ID of this lesson assessment.
	*
	* @return the lesson assessment ID of this lesson assessment
	*/
	public long getLessonAssessmentId() {
		return _lessonAssessment.getLessonAssessmentId();
	}

	/**
	* Sets the lesson assessment ID of this lesson assessment.
	*
	* @param lessonAssessmentId the lesson assessment ID of this lesson assessment
	*/
	public void setLessonAssessmentId(long lessonAssessmentId) {
		_lessonAssessment.setLessonAssessmentId(lessonAssessmentId);
	}

	/**
	* Returns the company ID of this lesson assessment.
	*
	* @return the company ID of this lesson assessment
	*/
	public long getCompanyId() {
		return _lessonAssessment.getCompanyId();
	}

	/**
	* Sets the company ID of this lesson assessment.
	*
	* @param companyId the company ID of this lesson assessment
	*/
	public void setCompanyId(long companyId) {
		_lessonAssessment.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this lesson assessment.
	*
	* @return the user ID of this lesson assessment
	*/
	public long getUserId() {
		return _lessonAssessment.getUserId();
	}

	/**
	* Sets the user ID of this lesson assessment.
	*
	* @param userId the user ID of this lesson assessment
	*/
	public void setUserId(long userId) {
		_lessonAssessment.setUserId(userId);
	}

	/**
	* Returns the user uuid of this lesson assessment.
	*
	* @return the user uuid of this lesson assessment
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessment.getUserUuid();
	}

	/**
	* Sets the user uuid of this lesson assessment.
	*
	* @param userUuid the user uuid of this lesson assessment
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_lessonAssessment.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this lesson assessment.
	*
	* @return the user name of this lesson assessment
	*/
	public java.lang.String getUserName() {
		return _lessonAssessment.getUserName();
	}

	/**
	* Sets the user name of this lesson assessment.
	*
	* @param userName the user name of this lesson assessment
	*/
	public void setUserName(java.lang.String userName) {
		_lessonAssessment.setUserName(userName);
	}

	/**
	* Returns the create date of this lesson assessment.
	*
	* @return the create date of this lesson assessment
	*/
	public java.util.Date getCreateDate() {
		return _lessonAssessment.getCreateDate();
	}

	/**
	* Sets the create date of this lesson assessment.
	*
	* @param createDate the create date of this lesson assessment
	*/
	public void setCreateDate(java.util.Date createDate) {
		_lessonAssessment.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this lesson assessment.
	*
	* @return the modified date of this lesson assessment
	*/
	public java.util.Date getModifiedDate() {
		return _lessonAssessment.getModifiedDate();
	}

	/**
	* Sets the modified date of this lesson assessment.
	*
	* @param modifiedDate the modified date of this lesson assessment
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_lessonAssessment.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the lesson plan ID of this lesson assessment.
	*
	* @return the lesson plan ID of this lesson assessment
	*/
	public long getLessonPlanId() {
		return _lessonAssessment.getLessonPlanId();
	}

	/**
	* Sets the lesson plan ID of this lesson assessment.
	*
	* @param lessonPlanId the lesson plan ID of this lesson assessment
	*/
	public void setLessonPlanId(long lessonPlanId) {
		_lessonAssessment.setLessonPlanId(lessonPlanId);
	}

	/**
	* Returns the assessment type of this lesson assessment.
	*
	* @return the assessment type of this lesson assessment
	*/
	public long getAssessmentType() {
		return _lessonAssessment.getAssessmentType();
	}

	/**
	* Sets the assessment type of this lesson assessment.
	*
	* @param assessmentType the assessment type of this lesson assessment
	*/
	public void setAssessmentType(long assessmentType) {
		_lessonAssessment.setAssessmentType(assessmentType);
	}

	/**
	* Returns the number of this lesson assessment.
	*
	* @return the number of this lesson assessment
	*/
	public long getNumber() {
		return _lessonAssessment.getNumber();
	}

	/**
	* Sets the number of this lesson assessment.
	*
	* @param number the number of this lesson assessment
	*/
	public void setNumber(long number) {
		_lessonAssessment.setNumber(number);
	}

	public boolean isNew() {
		return _lessonAssessment.isNew();
	}

	public void setNew(boolean n) {
		_lessonAssessment.setNew(n);
	}

	public boolean isCachedModel() {
		return _lessonAssessment.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_lessonAssessment.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _lessonAssessment.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _lessonAssessment.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_lessonAssessment.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _lessonAssessment.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_lessonAssessment.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LessonAssessmentWrapper((LessonAssessment)_lessonAssessment.clone());
	}

	public int compareTo(
		info.diit.portal.model.LessonAssessment lessonAssessment) {
		return _lessonAssessment.compareTo(lessonAssessment);
	}

	@Override
	public int hashCode() {
		return _lessonAssessment.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.LessonAssessment> toCacheModel() {
		return _lessonAssessment.toCacheModel();
	}

	public info.diit.portal.model.LessonAssessment toEscapedModel() {
		return new LessonAssessmentWrapper(_lessonAssessment.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _lessonAssessment.toString();
	}

	public java.lang.String toXmlString() {
		return _lessonAssessment.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_lessonAssessment.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public LessonAssessment getWrappedLessonAssessment() {
		return _lessonAssessment;
	}

	public LessonAssessment getWrappedModel() {
		return _lessonAssessment;
	}

	public void resetOriginalValues() {
		_lessonAssessment.resetOriginalValues();
	}

	private LessonAssessment _lessonAssessment;
}
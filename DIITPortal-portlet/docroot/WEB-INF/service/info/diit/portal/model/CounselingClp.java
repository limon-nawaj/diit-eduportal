/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.service.CounselingLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class CounselingClp extends BaseModelImpl<Counseling>
	implements Counseling {
	public CounselingClp() {
	}

	public Class<?> getModelClass() {
		return Counseling.class;
	}

	public String getModelClassName() {
		return Counseling.class.getName();
	}

	public long getPrimaryKey() {
		return _counselingId;
	}

	public void setPrimaryKey(long primaryKey) {
		setCounselingId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_counselingId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("counselingId", getCounselingId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("counselingDate", getCounselingDate());
		attributes.put("medium", getMedium());
		attributes.put("name", getName());
		attributes.put("maximumQualifications", getMaximumQualifications());
		attributes.put("mobile", getMobile());
		attributes.put("email", getEmail());
		attributes.put("address", getAddress());
		attributes.put("note", getNote());
		attributes.put("source", getSource());
		attributes.put("sourceNote", getSourceNote());
		attributes.put("status", getStatus());
		attributes.put("statusNote", getStatusNote());
		attributes.put("majorInterestCourse", getMajorInterestCourse());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long counselingId = (Long)attributes.get("counselingId");

		if (counselingId != null) {
			setCounselingId(counselingId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date counselingDate = (Date)attributes.get("counselingDate");

		if (counselingDate != null) {
			setCounselingDate(counselingDate);
		}

		Integer medium = (Integer)attributes.get("medium");

		if (medium != null) {
			setMedium(medium);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String maximumQualifications = (String)attributes.get(
				"maximumQualifications");

		if (maximumQualifications != null) {
			setMaximumQualifications(maximumQualifications);
		}

		String mobile = (String)attributes.get("mobile");

		if (mobile != null) {
			setMobile(mobile);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String address = (String)attributes.get("address");

		if (address != null) {
			setAddress(address);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}

		Integer source = (Integer)attributes.get("source");

		if (source != null) {
			setSource(source);
		}

		String sourceNote = (String)attributes.get("sourceNote");

		if (sourceNote != null) {
			setSourceNote(sourceNote);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		String statusNote = (String)attributes.get("statusNote");

		if (statusNote != null) {
			setStatusNote(statusNote);
		}

		Long majorInterestCourse = (Long)attributes.get("majorInterestCourse");

		if (majorInterestCourse != null) {
			setMajorInterestCourse(majorInterestCourse);
		}
	}

	public long getCounselingId() {
		return _counselingId;
	}

	public void setCounselingId(long counselingId) {
		_counselingId = counselingId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getCounselingDate() {
		return _counselingDate;
	}

	public void setCounselingDate(Date counselingDate) {
		_counselingDate = counselingDate;
	}

	public int getMedium() {
		return _medium;
	}

	public void setMedium(int medium) {
		_medium = medium;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getMaximumQualifications() {
		return _maximumQualifications;
	}

	public void setMaximumQualifications(String maximumQualifications) {
		_maximumQualifications = maximumQualifications;
	}

	public String getMobile() {
		return _mobile;
	}

	public void setMobile(String mobile) {
		_mobile = mobile;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getAddress() {
		return _address;
	}

	public void setAddress(String address) {
		_address = address;
	}

	public String getNote() {
		return _note;
	}

	public void setNote(String note) {
		_note = note;
	}

	public int getSource() {
		return _source;
	}

	public void setSource(int source) {
		_source = source;
	}

	public String getSourceNote() {
		return _sourceNote;
	}

	public void setSourceNote(String sourceNote) {
		_sourceNote = sourceNote;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public String getStatusNote() {
		return _statusNote;
	}

	public void setStatusNote(String statusNote) {
		_statusNote = statusNote;
	}

	public long getMajorInterestCourse() {
		return _majorInterestCourse;
	}

	public void setMajorInterestCourse(long majorInterestCourse) {
		_majorInterestCourse = majorInterestCourse;
	}

	public BaseModel<?> getCounselingRemoteModel() {
		return _counselingRemoteModel;
	}

	public void setCounselingRemoteModel(BaseModel<?> counselingRemoteModel) {
		_counselingRemoteModel = counselingRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			CounselingLocalServiceUtil.addCounseling(this);
		}
		else {
			CounselingLocalServiceUtil.updateCounseling(this);
		}
	}

	@Override
	public Counseling toEscapedModel() {
		return (Counseling)Proxy.newProxyInstance(Counseling.class.getClassLoader(),
			new Class[] { Counseling.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CounselingClp clone = new CounselingClp();

		clone.setCounselingId(getCounselingId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setCounselingDate(getCounselingDate());
		clone.setMedium(getMedium());
		clone.setName(getName());
		clone.setMaximumQualifications(getMaximumQualifications());
		clone.setMobile(getMobile());
		clone.setEmail(getEmail());
		clone.setAddress(getAddress());
		clone.setNote(getNote());
		clone.setSource(getSource());
		clone.setSourceNote(getSourceNote());
		clone.setStatus(getStatus());
		clone.setStatusNote(getStatusNote());
		clone.setMajorInterestCourse(getMajorInterestCourse());

		return clone;
	}

	public int compareTo(Counseling counseling) {
		int value = 0;

		if (getCounselingId() < counseling.getCounselingId()) {
			value = -1;
		}
		else if (getCounselingId() > counseling.getCounselingId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		CounselingClp counseling = null;

		try {
			counseling = (CounselingClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = counseling.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(39);

		sb.append("{counselingId=");
		sb.append(getCounselingId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", counselingDate=");
		sb.append(getCounselingDate());
		sb.append(", medium=");
		sb.append(getMedium());
		sb.append(", name=");
		sb.append(getName());
		sb.append(", maximumQualifications=");
		sb.append(getMaximumQualifications());
		sb.append(", mobile=");
		sb.append(getMobile());
		sb.append(", email=");
		sb.append(getEmail());
		sb.append(", address=");
		sb.append(getAddress());
		sb.append(", note=");
		sb.append(getNote());
		sb.append(", source=");
		sb.append(getSource());
		sb.append(", sourceNote=");
		sb.append(getSourceNote());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append(", statusNote=");
		sb.append(getStatusNote());
		sb.append(", majorInterestCourse=");
		sb.append(getMajorInterestCourse());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(61);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.Counseling");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>counselingId</column-name><column-value><![CDATA[");
		sb.append(getCounselingId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>counselingDate</column-name><column-value><![CDATA[");
		sb.append(getCounselingDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>medium</column-name><column-value><![CDATA[");
		sb.append(getMedium());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>maximumQualifications</column-name><column-value><![CDATA[");
		sb.append(getMaximumQualifications());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mobile</column-name><column-value><![CDATA[");
		sb.append(getMobile());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>email</column-name><column-value><![CDATA[");
		sb.append(getEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>address</column-name><column-value><![CDATA[");
		sb.append(getAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>note</column-name><column-value><![CDATA[");
		sb.append(getNote());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>source</column-name><column-value><![CDATA[");
		sb.append(getSource());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sourceNote</column-name><column-value><![CDATA[");
		sb.append(getSourceNote());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statusNote</column-name><column-value><![CDATA[");
		sb.append(getStatusNote());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>majorInterestCourse</column-name><column-value><![CDATA[");
		sb.append(getMajorInterestCourse());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _counselingId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _counselingDate;
	private int _medium;
	private String _name;
	private String _maximumQualifications;
	private String _mobile;
	private String _email;
	private String _address;
	private String _note;
	private int _source;
	private String _sourceNote;
	private int _status;
	private String _statusNote;
	private long _majorInterestCourse;
	private BaseModel<?> _counselingRemoteModel;
}
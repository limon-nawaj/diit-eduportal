/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ClassRoutineEventBatch}.
 * </p>
 *
 * @author    mohammad
 * @see       ClassRoutineEventBatch
 * @generated
 */
public class ClassRoutineEventBatchWrapper implements ClassRoutineEventBatch,
	ModelWrapper<ClassRoutineEventBatch> {
	public ClassRoutineEventBatchWrapper(
		ClassRoutineEventBatch classRoutineEventBatch) {
		_classRoutineEventBatch = classRoutineEventBatch;
	}

	public Class<?> getModelClass() {
		return ClassRoutineEventBatch.class;
	}

	public String getModelClassName() {
		return ClassRoutineEventBatch.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("classRoutineEventBatchId", getClassRoutineEventBatchId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("classRoutineEventId", getClassRoutineEventId());
		attributes.put("batchId", getBatchId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long classRoutineEventBatchId = (Long)attributes.get(
				"classRoutineEventBatchId");

		if (classRoutineEventBatchId != null) {
			setClassRoutineEventBatchId(classRoutineEventBatchId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long classRoutineEventId = (Long)attributes.get("classRoutineEventId");

		if (classRoutineEventId != null) {
			setClassRoutineEventId(classRoutineEventId);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}
	}

	/**
	* Returns the primary key of this class routine event batch.
	*
	* @return the primary key of this class routine event batch
	*/
	public long getPrimaryKey() {
		return _classRoutineEventBatch.getPrimaryKey();
	}

	/**
	* Sets the primary key of this class routine event batch.
	*
	* @param primaryKey the primary key of this class routine event batch
	*/
	public void setPrimaryKey(long primaryKey) {
		_classRoutineEventBatch.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the class routine event batch ID of this class routine event batch.
	*
	* @return the class routine event batch ID of this class routine event batch
	*/
	public long getClassRoutineEventBatchId() {
		return _classRoutineEventBatch.getClassRoutineEventBatchId();
	}

	/**
	* Sets the class routine event batch ID of this class routine event batch.
	*
	* @param classRoutineEventBatchId the class routine event batch ID of this class routine event batch
	*/
	public void setClassRoutineEventBatchId(long classRoutineEventBatchId) {
		_classRoutineEventBatch.setClassRoutineEventBatchId(classRoutineEventBatchId);
	}

	/**
	* Returns the company ID of this class routine event batch.
	*
	* @return the company ID of this class routine event batch
	*/
	public long getCompanyId() {
		return _classRoutineEventBatch.getCompanyId();
	}

	/**
	* Sets the company ID of this class routine event batch.
	*
	* @param companyId the company ID of this class routine event batch
	*/
	public void setCompanyId(long companyId) {
		_classRoutineEventBatch.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this class routine event batch.
	*
	* @return the user ID of this class routine event batch
	*/
	public long getUserId() {
		return _classRoutineEventBatch.getUserId();
	}

	/**
	* Sets the user ID of this class routine event batch.
	*
	* @param userId the user ID of this class routine event batch
	*/
	public void setUserId(long userId) {
		_classRoutineEventBatch.setUserId(userId);
	}

	/**
	* Returns the user uuid of this class routine event batch.
	*
	* @return the user uuid of this class routine event batch
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatch.getUserUuid();
	}

	/**
	* Sets the user uuid of this class routine event batch.
	*
	* @param userUuid the user uuid of this class routine event batch
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_classRoutineEventBatch.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this class routine event batch.
	*
	* @return the user name of this class routine event batch
	*/
	public java.lang.String getUserName() {
		return _classRoutineEventBatch.getUserName();
	}

	/**
	* Sets the user name of this class routine event batch.
	*
	* @param userName the user name of this class routine event batch
	*/
	public void setUserName(java.lang.String userName) {
		_classRoutineEventBatch.setUserName(userName);
	}

	/**
	* Returns the create date of this class routine event batch.
	*
	* @return the create date of this class routine event batch
	*/
	public java.util.Date getCreateDate() {
		return _classRoutineEventBatch.getCreateDate();
	}

	/**
	* Sets the create date of this class routine event batch.
	*
	* @param createDate the create date of this class routine event batch
	*/
	public void setCreateDate(java.util.Date createDate) {
		_classRoutineEventBatch.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this class routine event batch.
	*
	* @return the modified date of this class routine event batch
	*/
	public java.util.Date getModifiedDate() {
		return _classRoutineEventBatch.getModifiedDate();
	}

	/**
	* Sets the modified date of this class routine event batch.
	*
	* @param modifiedDate the modified date of this class routine event batch
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_classRoutineEventBatch.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the organization ID of this class routine event batch.
	*
	* @return the organization ID of this class routine event batch
	*/
	public long getOrganizationId() {
		return _classRoutineEventBatch.getOrganizationId();
	}

	/**
	* Sets the organization ID of this class routine event batch.
	*
	* @param organizationId the organization ID of this class routine event batch
	*/
	public void setOrganizationId(long organizationId) {
		_classRoutineEventBatch.setOrganizationId(organizationId);
	}

	/**
	* Returns the class routine event ID of this class routine event batch.
	*
	* @return the class routine event ID of this class routine event batch
	*/
	public long getClassRoutineEventId() {
		return _classRoutineEventBatch.getClassRoutineEventId();
	}

	/**
	* Sets the class routine event ID of this class routine event batch.
	*
	* @param classRoutineEventId the class routine event ID of this class routine event batch
	*/
	public void setClassRoutineEventId(long classRoutineEventId) {
		_classRoutineEventBatch.setClassRoutineEventId(classRoutineEventId);
	}

	/**
	* Returns the batch ID of this class routine event batch.
	*
	* @return the batch ID of this class routine event batch
	*/
	public long getBatchId() {
		return _classRoutineEventBatch.getBatchId();
	}

	/**
	* Sets the batch ID of this class routine event batch.
	*
	* @param batchId the batch ID of this class routine event batch
	*/
	public void setBatchId(long batchId) {
		_classRoutineEventBatch.setBatchId(batchId);
	}

	public boolean isNew() {
		return _classRoutineEventBatch.isNew();
	}

	public void setNew(boolean n) {
		_classRoutineEventBatch.setNew(n);
	}

	public boolean isCachedModel() {
		return _classRoutineEventBatch.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_classRoutineEventBatch.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _classRoutineEventBatch.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _classRoutineEventBatch.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_classRoutineEventBatch.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _classRoutineEventBatch.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_classRoutineEventBatch.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ClassRoutineEventBatchWrapper((ClassRoutineEventBatch)_classRoutineEventBatch.clone());
	}

	public int compareTo(
		info.diit.portal.model.ClassRoutineEventBatch classRoutineEventBatch) {
		return _classRoutineEventBatch.compareTo(classRoutineEventBatch);
	}

	@Override
	public int hashCode() {
		return _classRoutineEventBatch.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.ClassRoutineEventBatch> toCacheModel() {
		return _classRoutineEventBatch.toCacheModel();
	}

	public info.diit.portal.model.ClassRoutineEventBatch toEscapedModel() {
		return new ClassRoutineEventBatchWrapper(_classRoutineEventBatch.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _classRoutineEventBatch.toString();
	}

	public java.lang.String toXmlString() {
		return _classRoutineEventBatch.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_classRoutineEventBatch.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public ClassRoutineEventBatch getWrappedClassRoutineEventBatch() {
		return _classRoutineEventBatch;
	}

	public ClassRoutineEventBatch getWrappedModel() {
		return _classRoutineEventBatch;
	}

	public void resetOriginalValues() {
		_classRoutineEventBatch.resetOriginalValues();
	}

	private ClassRoutineEventBatch _classRoutineEventBatch;
}
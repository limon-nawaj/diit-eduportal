/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Counseling}.
 * </p>
 *
 * @author    mohammad
 * @see       Counseling
 * @generated
 */
public class CounselingWrapper implements Counseling, ModelWrapper<Counseling> {
	public CounselingWrapper(Counseling counseling) {
		_counseling = counseling;
	}

	public Class<?> getModelClass() {
		return Counseling.class;
	}

	public String getModelClassName() {
		return Counseling.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("counselingId", getCounselingId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("counselingDate", getCounselingDate());
		attributes.put("medium", getMedium());
		attributes.put("name", getName());
		attributes.put("maximumQualifications", getMaximumQualifications());
		attributes.put("mobile", getMobile());
		attributes.put("email", getEmail());
		attributes.put("address", getAddress());
		attributes.put("note", getNote());
		attributes.put("source", getSource());
		attributes.put("sourceNote", getSourceNote());
		attributes.put("status", getStatus());
		attributes.put("statusNote", getStatusNote());
		attributes.put("majorInterestCourse", getMajorInterestCourse());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long counselingId = (Long)attributes.get("counselingId");

		if (counselingId != null) {
			setCounselingId(counselingId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date counselingDate = (Date)attributes.get("counselingDate");

		if (counselingDate != null) {
			setCounselingDate(counselingDate);
		}

		Integer medium = (Integer)attributes.get("medium");

		if (medium != null) {
			setMedium(medium);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String maximumQualifications = (String)attributes.get(
				"maximumQualifications");

		if (maximumQualifications != null) {
			setMaximumQualifications(maximumQualifications);
		}

		String mobile = (String)attributes.get("mobile");

		if (mobile != null) {
			setMobile(mobile);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String address = (String)attributes.get("address");

		if (address != null) {
			setAddress(address);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}

		Integer source = (Integer)attributes.get("source");

		if (source != null) {
			setSource(source);
		}

		String sourceNote = (String)attributes.get("sourceNote");

		if (sourceNote != null) {
			setSourceNote(sourceNote);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		String statusNote = (String)attributes.get("statusNote");

		if (statusNote != null) {
			setStatusNote(statusNote);
		}

		Long majorInterestCourse = (Long)attributes.get("majorInterestCourse");

		if (majorInterestCourse != null) {
			setMajorInterestCourse(majorInterestCourse);
		}
	}

	/**
	* Returns the primary key of this counseling.
	*
	* @return the primary key of this counseling
	*/
	public long getPrimaryKey() {
		return _counseling.getPrimaryKey();
	}

	/**
	* Sets the primary key of this counseling.
	*
	* @param primaryKey the primary key of this counseling
	*/
	public void setPrimaryKey(long primaryKey) {
		_counseling.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the counseling ID of this counseling.
	*
	* @return the counseling ID of this counseling
	*/
	public long getCounselingId() {
		return _counseling.getCounselingId();
	}

	/**
	* Sets the counseling ID of this counseling.
	*
	* @param counselingId the counseling ID of this counseling
	*/
	public void setCounselingId(long counselingId) {
		_counseling.setCounselingId(counselingId);
	}

	/**
	* Returns the company ID of this counseling.
	*
	* @return the company ID of this counseling
	*/
	public long getCompanyId() {
		return _counseling.getCompanyId();
	}

	/**
	* Sets the company ID of this counseling.
	*
	* @param companyId the company ID of this counseling
	*/
	public void setCompanyId(long companyId) {
		_counseling.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this counseling.
	*
	* @return the user ID of this counseling
	*/
	public long getUserId() {
		return _counseling.getUserId();
	}

	/**
	* Sets the user ID of this counseling.
	*
	* @param userId the user ID of this counseling
	*/
	public void setUserId(long userId) {
		_counseling.setUserId(userId);
	}

	/**
	* Returns the user uuid of this counseling.
	*
	* @return the user uuid of this counseling
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _counseling.getUserUuid();
	}

	/**
	* Sets the user uuid of this counseling.
	*
	* @param userUuid the user uuid of this counseling
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_counseling.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this counseling.
	*
	* @return the user name of this counseling
	*/
	public java.lang.String getUserName() {
		return _counseling.getUserName();
	}

	/**
	* Sets the user name of this counseling.
	*
	* @param userName the user name of this counseling
	*/
	public void setUserName(java.lang.String userName) {
		_counseling.setUserName(userName);
	}

	/**
	* Returns the create date of this counseling.
	*
	* @return the create date of this counseling
	*/
	public java.util.Date getCreateDate() {
		return _counseling.getCreateDate();
	}

	/**
	* Sets the create date of this counseling.
	*
	* @param createDate the create date of this counseling
	*/
	public void setCreateDate(java.util.Date createDate) {
		_counseling.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this counseling.
	*
	* @return the modified date of this counseling
	*/
	public java.util.Date getModifiedDate() {
		return _counseling.getModifiedDate();
	}

	/**
	* Sets the modified date of this counseling.
	*
	* @param modifiedDate the modified date of this counseling
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_counseling.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the counseling date of this counseling.
	*
	* @return the counseling date of this counseling
	*/
	public java.util.Date getCounselingDate() {
		return _counseling.getCounselingDate();
	}

	/**
	* Sets the counseling date of this counseling.
	*
	* @param counselingDate the counseling date of this counseling
	*/
	public void setCounselingDate(java.util.Date counselingDate) {
		_counseling.setCounselingDate(counselingDate);
	}

	/**
	* Returns the medium of this counseling.
	*
	* @return the medium of this counseling
	*/
	public int getMedium() {
		return _counseling.getMedium();
	}

	/**
	* Sets the medium of this counseling.
	*
	* @param medium the medium of this counseling
	*/
	public void setMedium(int medium) {
		_counseling.setMedium(medium);
	}

	/**
	* Returns the name of this counseling.
	*
	* @return the name of this counseling
	*/
	public java.lang.String getName() {
		return _counseling.getName();
	}

	/**
	* Sets the name of this counseling.
	*
	* @param name the name of this counseling
	*/
	public void setName(java.lang.String name) {
		_counseling.setName(name);
	}

	/**
	* Returns the maximum qualifications of this counseling.
	*
	* @return the maximum qualifications of this counseling
	*/
	public java.lang.String getMaximumQualifications() {
		return _counseling.getMaximumQualifications();
	}

	/**
	* Sets the maximum qualifications of this counseling.
	*
	* @param maximumQualifications the maximum qualifications of this counseling
	*/
	public void setMaximumQualifications(java.lang.String maximumQualifications) {
		_counseling.setMaximumQualifications(maximumQualifications);
	}

	/**
	* Returns the mobile of this counseling.
	*
	* @return the mobile of this counseling
	*/
	public java.lang.String getMobile() {
		return _counseling.getMobile();
	}

	/**
	* Sets the mobile of this counseling.
	*
	* @param mobile the mobile of this counseling
	*/
	public void setMobile(java.lang.String mobile) {
		_counseling.setMobile(mobile);
	}

	/**
	* Returns the email of this counseling.
	*
	* @return the email of this counseling
	*/
	public java.lang.String getEmail() {
		return _counseling.getEmail();
	}

	/**
	* Sets the email of this counseling.
	*
	* @param email the email of this counseling
	*/
	public void setEmail(java.lang.String email) {
		_counseling.setEmail(email);
	}

	/**
	* Returns the address of this counseling.
	*
	* @return the address of this counseling
	*/
	public java.lang.String getAddress() {
		return _counseling.getAddress();
	}

	/**
	* Sets the address of this counseling.
	*
	* @param address the address of this counseling
	*/
	public void setAddress(java.lang.String address) {
		_counseling.setAddress(address);
	}

	/**
	* Returns the note of this counseling.
	*
	* @return the note of this counseling
	*/
	public java.lang.String getNote() {
		return _counseling.getNote();
	}

	/**
	* Sets the note of this counseling.
	*
	* @param note the note of this counseling
	*/
	public void setNote(java.lang.String note) {
		_counseling.setNote(note);
	}

	/**
	* Returns the source of this counseling.
	*
	* @return the source of this counseling
	*/
	public int getSource() {
		return _counseling.getSource();
	}

	/**
	* Sets the source of this counseling.
	*
	* @param source the source of this counseling
	*/
	public void setSource(int source) {
		_counseling.setSource(source);
	}

	/**
	* Returns the source note of this counseling.
	*
	* @return the source note of this counseling
	*/
	public java.lang.String getSourceNote() {
		return _counseling.getSourceNote();
	}

	/**
	* Sets the source note of this counseling.
	*
	* @param sourceNote the source note of this counseling
	*/
	public void setSourceNote(java.lang.String sourceNote) {
		_counseling.setSourceNote(sourceNote);
	}

	/**
	* Returns the status of this counseling.
	*
	* @return the status of this counseling
	*/
	public int getStatus() {
		return _counseling.getStatus();
	}

	/**
	* Sets the status of this counseling.
	*
	* @param status the status of this counseling
	*/
	public void setStatus(int status) {
		_counseling.setStatus(status);
	}

	/**
	* Returns the status note of this counseling.
	*
	* @return the status note of this counseling
	*/
	public java.lang.String getStatusNote() {
		return _counseling.getStatusNote();
	}

	/**
	* Sets the status note of this counseling.
	*
	* @param statusNote the status note of this counseling
	*/
	public void setStatusNote(java.lang.String statusNote) {
		_counseling.setStatusNote(statusNote);
	}

	/**
	* Returns the major interest course of this counseling.
	*
	* @return the major interest course of this counseling
	*/
	public long getMajorInterestCourse() {
		return _counseling.getMajorInterestCourse();
	}

	/**
	* Sets the major interest course of this counseling.
	*
	* @param majorInterestCourse the major interest course of this counseling
	*/
	public void setMajorInterestCourse(long majorInterestCourse) {
		_counseling.setMajorInterestCourse(majorInterestCourse);
	}

	public boolean isNew() {
		return _counseling.isNew();
	}

	public void setNew(boolean n) {
		_counseling.setNew(n);
	}

	public boolean isCachedModel() {
		return _counseling.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_counseling.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _counseling.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _counseling.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_counseling.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _counseling.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_counseling.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CounselingWrapper((Counseling)_counseling.clone());
	}

	public int compareTo(info.diit.portal.model.Counseling counseling) {
		return _counseling.compareTo(counseling);
	}

	@Override
	public int hashCode() {
		return _counseling.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.Counseling> toCacheModel() {
		return _counseling.toCacheModel();
	}

	public info.diit.portal.model.Counseling toEscapedModel() {
		return new CounselingWrapper(_counseling.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _counseling.toString();
	}

	public java.lang.String toXmlString() {
		return _counseling.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_counseling.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Counseling getWrappedCounseling() {
		return _counseling;
	}

	public Counseling getWrappedModel() {
		return _counseling;
	}

	public void resetOriginalValues() {
		_counseling.resetOriginalValues();
	}

	private Counseling _counseling;
}
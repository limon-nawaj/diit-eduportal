/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class CounselingSoap implements Serializable {
	public static CounselingSoap toSoapModel(Counseling model) {
		CounselingSoap soapModel = new CounselingSoap();

		soapModel.setCounselingId(model.getCounselingId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCounselingDate(model.getCounselingDate());
		soapModel.setMedium(model.getMedium());
		soapModel.setName(model.getName());
		soapModel.setMaximumQualifications(model.getMaximumQualifications());
		soapModel.setMobile(model.getMobile());
		soapModel.setEmail(model.getEmail());
		soapModel.setAddress(model.getAddress());
		soapModel.setNote(model.getNote());
		soapModel.setSource(model.getSource());
		soapModel.setSourceNote(model.getSourceNote());
		soapModel.setStatus(model.getStatus());
		soapModel.setStatusNote(model.getStatusNote());
		soapModel.setMajorInterestCourse(model.getMajorInterestCourse());

		return soapModel;
	}

	public static CounselingSoap[] toSoapModels(Counseling[] models) {
		CounselingSoap[] soapModels = new CounselingSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CounselingSoap[][] toSoapModels(Counseling[][] models) {
		CounselingSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CounselingSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CounselingSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CounselingSoap[] toSoapModels(List<Counseling> models) {
		List<CounselingSoap> soapModels = new ArrayList<CounselingSoap>(models.size());

		for (Counseling model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CounselingSoap[soapModels.size()]);
	}

	public CounselingSoap() {
	}

	public long getPrimaryKey() {
		return _counselingId;
	}

	public void setPrimaryKey(long pk) {
		setCounselingId(pk);
	}

	public long getCounselingId() {
		return _counselingId;
	}

	public void setCounselingId(long counselingId) {
		_counselingId = counselingId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getCounselingDate() {
		return _counselingDate;
	}

	public void setCounselingDate(Date counselingDate) {
		_counselingDate = counselingDate;
	}

	public int getMedium() {
		return _medium;
	}

	public void setMedium(int medium) {
		_medium = medium;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getMaximumQualifications() {
		return _maximumQualifications;
	}

	public void setMaximumQualifications(String maximumQualifications) {
		_maximumQualifications = maximumQualifications;
	}

	public String getMobile() {
		return _mobile;
	}

	public void setMobile(String mobile) {
		_mobile = mobile;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getAddress() {
		return _address;
	}

	public void setAddress(String address) {
		_address = address;
	}

	public String getNote() {
		return _note;
	}

	public void setNote(String note) {
		_note = note;
	}

	public int getSource() {
		return _source;
	}

	public void setSource(int source) {
		_source = source;
	}

	public String getSourceNote() {
		return _sourceNote;
	}

	public void setSourceNote(String sourceNote) {
		_sourceNote = sourceNote;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public String getStatusNote() {
		return _statusNote;
	}

	public void setStatusNote(String statusNote) {
		_statusNote = statusNote;
	}

	public long getMajorInterestCourse() {
		return _majorInterestCourse;
	}

	public void setMajorInterestCourse(long majorInterestCourse) {
		_majorInterestCourse = majorInterestCourse;
	}

	private long _counselingId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _counselingDate;
	private int _medium;
	private String _name;
	private String _maximumQualifications;
	private String _mobile;
	private String _email;
	private String _address;
	private String _note;
	private int _source;
	private String _sourceNote;
	private int _status;
	private String _statusNote;
	private long _majorInterestCourse;
}
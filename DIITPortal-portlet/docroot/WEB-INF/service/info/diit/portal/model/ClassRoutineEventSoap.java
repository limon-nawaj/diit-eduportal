/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class ClassRoutineEventSoap implements Serializable {
	public static ClassRoutineEventSoap toSoapModel(ClassRoutineEvent model) {
		ClassRoutineEventSoap soapModel = new ClassRoutineEventSoap();

		soapModel.setClassRoutineEventId(model.getClassRoutineEventId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setSubjectId(model.getSubjectId());
		soapModel.setDay(model.getDay());
		soapModel.setStartTime(model.getStartTime());
		soapModel.setEndTime(model.getEndTime());
		soapModel.setRoomId(model.getRoomId());

		return soapModel;
	}

	public static ClassRoutineEventSoap[] toSoapModels(
		ClassRoutineEvent[] models) {
		ClassRoutineEventSoap[] soapModels = new ClassRoutineEventSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ClassRoutineEventSoap[][] toSoapModels(
		ClassRoutineEvent[][] models) {
		ClassRoutineEventSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ClassRoutineEventSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ClassRoutineEventSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ClassRoutineEventSoap[] toSoapModels(
		List<ClassRoutineEvent> models) {
		List<ClassRoutineEventSoap> soapModels = new ArrayList<ClassRoutineEventSoap>(models.size());

		for (ClassRoutineEvent model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ClassRoutineEventSoap[soapModels.size()]);
	}

	public ClassRoutineEventSoap() {
	}

	public long getPrimaryKey() {
		return _classRoutineEventId;
	}

	public void setPrimaryKey(long pk) {
		setClassRoutineEventId(pk);
	}

	public long getClassRoutineEventId() {
		return _classRoutineEventId;
	}

	public void setClassRoutineEventId(long classRoutineEventId) {
		_classRoutineEventId = classRoutineEventId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getSubjectId() {
		return _subjectId;
	}

	public void setSubjectId(long subjectId) {
		_subjectId = subjectId;
	}

	public int getDay() {
		return _day;
	}

	public void setDay(int day) {
		_day = day;
	}

	public Date getStartTime() {
		return _startTime;
	}

	public void setStartTime(Date startTime) {
		_startTime = startTime;
	}

	public Date getEndTime() {
		return _endTime;
	}

	public void setEndTime(Date endTime) {
		_endTime = endTime;
	}

	public long getRoomId() {
		return _roomId;
	}

	public void setRoomId(long roomId) {
		_roomId = roomId;
	}

	private long _classRoutineEventId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private long _subjectId;
	private int _day;
	private Date _startTime;
	private Date _endTime;
	private long _roomId;
}
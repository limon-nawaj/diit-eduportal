/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class CommunicationRecordSoap implements Serializable {
	public static CommunicationRecordSoap toSoapModel(CommunicationRecord model) {
		CommunicationRecordSoap soapModel = new CommunicationRecordSoap();

		soapModel.setCommunicationRecordId(model.getCommunicationRecordId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setCommunicationBy(model.getCommunicationBy());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setBatch(model.getBatch());
		soapModel.setDate(model.getDate());
		soapModel.setMedia(model.getMedia());
		soapModel.setSubject(model.getSubject());
		soapModel.setCommunicationWith(model.getCommunicationWith());
		soapModel.setDetails(model.getDetails());
		soapModel.setStatus(model.getStatus());

		return soapModel;
	}

	public static CommunicationRecordSoap[] toSoapModels(
		CommunicationRecord[] models) {
		CommunicationRecordSoap[] soapModels = new CommunicationRecordSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CommunicationRecordSoap[][] toSoapModels(
		CommunicationRecord[][] models) {
		CommunicationRecordSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CommunicationRecordSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CommunicationRecordSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CommunicationRecordSoap[] toSoapModels(
		List<CommunicationRecord> models) {
		List<CommunicationRecordSoap> soapModels = new ArrayList<CommunicationRecordSoap>(models.size());

		for (CommunicationRecord model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CommunicationRecordSoap[soapModels.size()]);
	}

	public CommunicationRecordSoap() {
	}

	public long getPrimaryKey() {
		return _communicationRecordId;
	}

	public void setPrimaryKey(long pk) {
		setCommunicationRecordId(pk);
	}

	public long getCommunicationRecordId() {
		return _communicationRecordId;
	}

	public void setCommunicationRecordId(long communicationRecordId) {
		_communicationRecordId = communicationRecordId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getCommunicationBy() {
		return _communicationBy;
	}

	public void setCommunicationBy(long communicationBy) {
		_communicationBy = communicationBy;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getBatch() {
		return _batch;
	}

	public void setBatch(long batch) {
		_batch = batch;
	}

	public Date getDate() {
		return _date;
	}

	public void setDate(Date date) {
		_date = date;
	}

	public long getMedia() {
		return _media;
	}

	public void setMedia(long media) {
		_media = media;
	}

	public long getSubject() {
		return _subject;
	}

	public void setSubject(long subject) {
		_subject = subject;
	}

	public long getCommunicationWith() {
		return _communicationWith;
	}

	public void setCommunicationWith(long communicationWith) {
		_communicationWith = communicationWith;
	}

	public String getDetails() {
		return _details;
	}

	public void setDetails(String details) {
		_details = details;
	}

	public long getStatus() {
		return _status;
	}

	public void setStatus(long status) {
		_status = status;
	}

	private long _communicationRecordId;
	private long _companyId;
	private long _organizationId;
	private long _communicationBy;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _batch;
	private Date _date;
	private long _media;
	private long _subject;
	private long _communicationWith;
	private String _details;
	private long _status;
}
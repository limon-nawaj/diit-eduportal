/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BatchTeacher}.
 * </p>
 *
 * @author    mohammad
 * @see       BatchTeacher
 * @generated
 */
public class BatchTeacherWrapper implements BatchTeacher,
	ModelWrapper<BatchTeacher> {
	public BatchTeacherWrapper(BatchTeacher batchTeacher) {
		_batchTeacher = batchTeacher;
	}

	public Class<?> getModelClass() {
		return BatchTeacher.class;
	}

	public String getModelClassName() {
		return BatchTeacher.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("batchTeacherId", getBatchTeacherId());
		attributes.put("teacherId", getTeacherId());
		attributes.put("startDate", getStartDate());
		attributes.put("endDate", getEndDate());
		attributes.put("batchId", getBatchId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long batchTeacherId = (Long)attributes.get("batchTeacherId");

		if (batchTeacherId != null) {
			setBatchTeacherId(batchTeacherId);
		}

		Long teacherId = (Long)attributes.get("teacherId");

		if (teacherId != null) {
			setTeacherId(teacherId);
		}

		Date startDate = (Date)attributes.get("startDate");

		if (startDate != null) {
			setStartDate(startDate);
		}

		Date endDate = (Date)attributes.get("endDate");

		if (endDate != null) {
			setEndDate(endDate);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}
	}

	/**
	* Returns the primary key of this batch teacher.
	*
	* @return the primary key of this batch teacher
	*/
	public long getPrimaryKey() {
		return _batchTeacher.getPrimaryKey();
	}

	/**
	* Sets the primary key of this batch teacher.
	*
	* @param primaryKey the primary key of this batch teacher
	*/
	public void setPrimaryKey(long primaryKey) {
		_batchTeacher.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the batch teacher ID of this batch teacher.
	*
	* @return the batch teacher ID of this batch teacher
	*/
	public long getBatchTeacherId() {
		return _batchTeacher.getBatchTeacherId();
	}

	/**
	* Sets the batch teacher ID of this batch teacher.
	*
	* @param batchTeacherId the batch teacher ID of this batch teacher
	*/
	public void setBatchTeacherId(long batchTeacherId) {
		_batchTeacher.setBatchTeacherId(batchTeacherId);
	}

	/**
	* Returns the teacher ID of this batch teacher.
	*
	* @return the teacher ID of this batch teacher
	*/
	public long getTeacherId() {
		return _batchTeacher.getTeacherId();
	}

	/**
	* Sets the teacher ID of this batch teacher.
	*
	* @param teacherId the teacher ID of this batch teacher
	*/
	public void setTeacherId(long teacherId) {
		_batchTeacher.setTeacherId(teacherId);
	}

	/**
	* Returns the start date of this batch teacher.
	*
	* @return the start date of this batch teacher
	*/
	public java.util.Date getStartDate() {
		return _batchTeacher.getStartDate();
	}

	/**
	* Sets the start date of this batch teacher.
	*
	* @param startDate the start date of this batch teacher
	*/
	public void setStartDate(java.util.Date startDate) {
		_batchTeacher.setStartDate(startDate);
	}

	/**
	* Returns the end date of this batch teacher.
	*
	* @return the end date of this batch teacher
	*/
	public java.util.Date getEndDate() {
		return _batchTeacher.getEndDate();
	}

	/**
	* Sets the end date of this batch teacher.
	*
	* @param endDate the end date of this batch teacher
	*/
	public void setEndDate(java.util.Date endDate) {
		_batchTeacher.setEndDate(endDate);
	}

	/**
	* Returns the batch ID of this batch teacher.
	*
	* @return the batch ID of this batch teacher
	*/
	public long getBatchId() {
		return _batchTeacher.getBatchId();
	}

	/**
	* Sets the batch ID of this batch teacher.
	*
	* @param batchId the batch ID of this batch teacher
	*/
	public void setBatchId(long batchId) {
		_batchTeacher.setBatchId(batchId);
	}

	public boolean isNew() {
		return _batchTeacher.isNew();
	}

	public void setNew(boolean n) {
		_batchTeacher.setNew(n);
	}

	public boolean isCachedModel() {
		return _batchTeacher.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_batchTeacher.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _batchTeacher.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _batchTeacher.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_batchTeacher.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _batchTeacher.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_batchTeacher.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new BatchTeacherWrapper((BatchTeacher)_batchTeacher.clone());
	}

	public int compareTo(info.diit.portal.model.BatchTeacher batchTeacher) {
		return _batchTeacher.compareTo(batchTeacher);
	}

	@Override
	public int hashCode() {
		return _batchTeacher.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.BatchTeacher> toCacheModel() {
		return _batchTeacher.toCacheModel();
	}

	public info.diit.portal.model.BatchTeacher toEscapedModel() {
		return new BatchTeacherWrapper(_batchTeacher.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _batchTeacher.toString();
	}

	public java.lang.String toXmlString() {
		return _batchTeacher.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_batchTeacher.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public BatchTeacher getWrappedBatchTeacher() {
		return _batchTeacher;
	}

	public BatchTeacher getWrappedModel() {
		return _batchTeacher;
	}

	public void resetOriginalValues() {
		_batchTeacher.resetOriginalValues();
	}

	private BatchTeacher _batchTeacher;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CourseOrganization}.
 * </p>
 *
 * @author    mohammad
 * @see       CourseOrganization
 * @generated
 */
public class CourseOrganizationWrapper implements CourseOrganization,
	ModelWrapper<CourseOrganization> {
	public CourseOrganizationWrapper(CourseOrganization courseOrganization) {
		_courseOrganization = courseOrganization;
	}

	public Class<?> getModelClass() {
		return CourseOrganization.class;
	}

	public String getModelClassName() {
		return CourseOrganization.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("courseOrganizationId", getCourseOrganizationId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("courseId", getCourseId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long courseOrganizationId = (Long)attributes.get("courseOrganizationId");

		if (courseOrganizationId != null) {
			setCourseOrganizationId(courseOrganizationId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long courseId = (Long)attributes.get("courseId");

		if (courseId != null) {
			setCourseId(courseId);
		}
	}

	/**
	* Returns the primary key of this course organization.
	*
	* @return the primary key of this course organization
	*/
	public long getPrimaryKey() {
		return _courseOrganization.getPrimaryKey();
	}

	/**
	* Sets the primary key of this course organization.
	*
	* @param primaryKey the primary key of this course organization
	*/
	public void setPrimaryKey(long primaryKey) {
		_courseOrganization.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the course organization ID of this course organization.
	*
	* @return the course organization ID of this course organization
	*/
	public long getCourseOrganizationId() {
		return _courseOrganization.getCourseOrganizationId();
	}

	/**
	* Sets the course organization ID of this course organization.
	*
	* @param courseOrganizationId the course organization ID of this course organization
	*/
	public void setCourseOrganizationId(long courseOrganizationId) {
		_courseOrganization.setCourseOrganizationId(courseOrganizationId);
	}

	/**
	* Returns the company ID of this course organization.
	*
	* @return the company ID of this course organization
	*/
	public long getCompanyId() {
		return _courseOrganization.getCompanyId();
	}

	/**
	* Sets the company ID of this course organization.
	*
	* @param companyId the company ID of this course organization
	*/
	public void setCompanyId(long companyId) {
		_courseOrganization.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this course organization.
	*
	* @return the organization ID of this course organization
	*/
	public long getOrganizationId() {
		return _courseOrganization.getOrganizationId();
	}

	/**
	* Sets the organization ID of this course organization.
	*
	* @param organizationId the organization ID of this course organization
	*/
	public void setOrganizationId(long organizationId) {
		_courseOrganization.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this course organization.
	*
	* @return the user ID of this course organization
	*/
	public long getUserId() {
		return _courseOrganization.getUserId();
	}

	/**
	* Sets the user ID of this course organization.
	*
	* @param userId the user ID of this course organization
	*/
	public void setUserId(long userId) {
		_courseOrganization.setUserId(userId);
	}

	/**
	* Returns the user uuid of this course organization.
	*
	* @return the user uuid of this course organization
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganization.getUserUuid();
	}

	/**
	* Sets the user uuid of this course organization.
	*
	* @param userUuid the user uuid of this course organization
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_courseOrganization.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this course organization.
	*
	* @return the user name of this course organization
	*/
	public java.lang.String getUserName() {
		return _courseOrganization.getUserName();
	}

	/**
	* Sets the user name of this course organization.
	*
	* @param userName the user name of this course organization
	*/
	public void setUserName(java.lang.String userName) {
		_courseOrganization.setUserName(userName);
	}

	/**
	* Returns the create date of this course organization.
	*
	* @return the create date of this course organization
	*/
	public java.util.Date getCreateDate() {
		return _courseOrganization.getCreateDate();
	}

	/**
	* Sets the create date of this course organization.
	*
	* @param createDate the create date of this course organization
	*/
	public void setCreateDate(java.util.Date createDate) {
		_courseOrganization.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this course organization.
	*
	* @return the modified date of this course organization
	*/
	public java.util.Date getModifiedDate() {
		return _courseOrganization.getModifiedDate();
	}

	/**
	* Sets the modified date of this course organization.
	*
	* @param modifiedDate the modified date of this course organization
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_courseOrganization.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the course ID of this course organization.
	*
	* @return the course ID of this course organization
	*/
	public long getCourseId() {
		return _courseOrganization.getCourseId();
	}

	/**
	* Sets the course ID of this course organization.
	*
	* @param courseId the course ID of this course organization
	*/
	public void setCourseId(long courseId) {
		_courseOrganization.setCourseId(courseId);
	}

	public boolean isNew() {
		return _courseOrganization.isNew();
	}

	public void setNew(boolean n) {
		_courseOrganization.setNew(n);
	}

	public boolean isCachedModel() {
		return _courseOrganization.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_courseOrganization.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _courseOrganization.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _courseOrganization.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_courseOrganization.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _courseOrganization.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_courseOrganization.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CourseOrganizationWrapper((CourseOrganization)_courseOrganization.clone());
	}

	public int compareTo(
		info.diit.portal.model.CourseOrganization courseOrganization) {
		return _courseOrganization.compareTo(courseOrganization);
	}

	@Override
	public int hashCode() {
		return _courseOrganization.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.CourseOrganization> toCacheModel() {
		return _courseOrganization.toCacheModel();
	}

	public info.diit.portal.model.CourseOrganization toEscapedModel() {
		return new CourseOrganizationWrapper(_courseOrganization.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _courseOrganization.toString();
	}

	public java.lang.String toXmlString() {
		return _courseOrganization.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_courseOrganization.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public CourseOrganization getWrappedCourseOrganization() {
		return _courseOrganization;
	}

	public CourseOrganization getWrappedModel() {
		return _courseOrganization;
	}

	public void resetOriginalValues() {
		_courseOrganization.resetOriginalValues();
	}

	private CourseOrganization _courseOrganization;
}
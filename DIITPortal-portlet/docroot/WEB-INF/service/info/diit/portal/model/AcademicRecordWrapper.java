/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AcademicRecord}.
 * </p>
 *
 * @author    mohammad
 * @see       AcademicRecord
 * @generated
 */
public class AcademicRecordWrapper implements AcademicRecord,
	ModelWrapper<AcademicRecord> {
	public AcademicRecordWrapper(AcademicRecord academicRecord) {
		_academicRecord = academicRecord;
	}

	public Class<?> getModelClass() {
		return AcademicRecord.class;
	}

	public String getModelClassName() {
		return AcademicRecord.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("academicRecordId", getAcademicRecordId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organisationId", getOrganisationId());
		attributes.put("degree", getDegree());
		attributes.put("board", getBoard());
		attributes.put("year", getYear());
		attributes.put("result", getResult());
		attributes.put("registrationNo", getRegistrationNo());
		attributes.put("studentId", getStudentId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long academicRecordId = (Long)attributes.get("academicRecordId");

		if (academicRecordId != null) {
			setAcademicRecordId(academicRecordId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organisationId = (Long)attributes.get("organisationId");

		if (organisationId != null) {
			setOrganisationId(organisationId);
		}

		String degree = (String)attributes.get("degree");

		if (degree != null) {
			setDegree(degree);
		}

		String board = (String)attributes.get("board");

		if (board != null) {
			setBoard(board);
		}

		Integer year = (Integer)attributes.get("year");

		if (year != null) {
			setYear(year);
		}

		String result = (String)attributes.get("result");

		if (result != null) {
			setResult(result);
		}

		String registrationNo = (String)attributes.get("registrationNo");

		if (registrationNo != null) {
			setRegistrationNo(registrationNo);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}
	}

	/**
	* Returns the primary key of this academic record.
	*
	* @return the primary key of this academic record
	*/
	public long getPrimaryKey() {
		return _academicRecord.getPrimaryKey();
	}

	/**
	* Sets the primary key of this academic record.
	*
	* @param primaryKey the primary key of this academic record
	*/
	public void setPrimaryKey(long primaryKey) {
		_academicRecord.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the academic record ID of this academic record.
	*
	* @return the academic record ID of this academic record
	*/
	public long getAcademicRecordId() {
		return _academicRecord.getAcademicRecordId();
	}

	/**
	* Sets the academic record ID of this academic record.
	*
	* @param academicRecordId the academic record ID of this academic record
	*/
	public void setAcademicRecordId(long academicRecordId) {
		_academicRecord.setAcademicRecordId(academicRecordId);
	}

	/**
	* Returns the company ID of this academic record.
	*
	* @return the company ID of this academic record
	*/
	public long getCompanyId() {
		return _academicRecord.getCompanyId();
	}

	/**
	* Sets the company ID of this academic record.
	*
	* @param companyId the company ID of this academic record
	*/
	public void setCompanyId(long companyId) {
		_academicRecord.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this academic record.
	*
	* @return the user ID of this academic record
	*/
	public long getUserId() {
		return _academicRecord.getUserId();
	}

	/**
	* Sets the user ID of this academic record.
	*
	* @param userId the user ID of this academic record
	*/
	public void setUserId(long userId) {
		_academicRecord.setUserId(userId);
	}

	/**
	* Returns the user uuid of this academic record.
	*
	* @return the user uuid of this academic record
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _academicRecord.getUserUuid();
	}

	/**
	* Sets the user uuid of this academic record.
	*
	* @param userUuid the user uuid of this academic record
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_academicRecord.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this academic record.
	*
	* @return the user name of this academic record
	*/
	public java.lang.String getUserName() {
		return _academicRecord.getUserName();
	}

	/**
	* Sets the user name of this academic record.
	*
	* @param userName the user name of this academic record
	*/
	public void setUserName(java.lang.String userName) {
		_academicRecord.setUserName(userName);
	}

	/**
	* Returns the create date of this academic record.
	*
	* @return the create date of this academic record
	*/
	public java.util.Date getCreateDate() {
		return _academicRecord.getCreateDate();
	}

	/**
	* Sets the create date of this academic record.
	*
	* @param createDate the create date of this academic record
	*/
	public void setCreateDate(java.util.Date createDate) {
		_academicRecord.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this academic record.
	*
	* @return the modified date of this academic record
	*/
	public java.util.Date getModifiedDate() {
		return _academicRecord.getModifiedDate();
	}

	/**
	* Sets the modified date of this academic record.
	*
	* @param modifiedDate the modified date of this academic record
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_academicRecord.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the organisation ID of this academic record.
	*
	* @return the organisation ID of this academic record
	*/
	public long getOrganisationId() {
		return _academicRecord.getOrganisationId();
	}

	/**
	* Sets the organisation ID of this academic record.
	*
	* @param organisationId the organisation ID of this academic record
	*/
	public void setOrganisationId(long organisationId) {
		_academicRecord.setOrganisationId(organisationId);
	}

	/**
	* Returns the degree of this academic record.
	*
	* @return the degree of this academic record
	*/
	public java.lang.String getDegree() {
		return _academicRecord.getDegree();
	}

	/**
	* Sets the degree of this academic record.
	*
	* @param degree the degree of this academic record
	*/
	public void setDegree(java.lang.String degree) {
		_academicRecord.setDegree(degree);
	}

	/**
	* Returns the board of this academic record.
	*
	* @return the board of this academic record
	*/
	public java.lang.String getBoard() {
		return _academicRecord.getBoard();
	}

	/**
	* Sets the board of this academic record.
	*
	* @param board the board of this academic record
	*/
	public void setBoard(java.lang.String board) {
		_academicRecord.setBoard(board);
	}

	/**
	* Returns the year of this academic record.
	*
	* @return the year of this academic record
	*/
	public int getYear() {
		return _academicRecord.getYear();
	}

	/**
	* Sets the year of this academic record.
	*
	* @param year the year of this academic record
	*/
	public void setYear(int year) {
		_academicRecord.setYear(year);
	}

	/**
	* Returns the result of this academic record.
	*
	* @return the result of this academic record
	*/
	public java.lang.String getResult() {
		return _academicRecord.getResult();
	}

	/**
	* Sets the result of this academic record.
	*
	* @param result the result of this academic record
	*/
	public void setResult(java.lang.String result) {
		_academicRecord.setResult(result);
	}

	/**
	* Returns the registration no of this academic record.
	*
	* @return the registration no of this academic record
	*/
	public java.lang.String getRegistrationNo() {
		return _academicRecord.getRegistrationNo();
	}

	/**
	* Sets the registration no of this academic record.
	*
	* @param registrationNo the registration no of this academic record
	*/
	public void setRegistrationNo(java.lang.String registrationNo) {
		_academicRecord.setRegistrationNo(registrationNo);
	}

	/**
	* Returns the student ID of this academic record.
	*
	* @return the student ID of this academic record
	*/
	public long getStudentId() {
		return _academicRecord.getStudentId();
	}

	/**
	* Sets the student ID of this academic record.
	*
	* @param studentId the student ID of this academic record
	*/
	public void setStudentId(long studentId) {
		_academicRecord.setStudentId(studentId);
	}

	public boolean isNew() {
		return _academicRecord.isNew();
	}

	public void setNew(boolean n) {
		_academicRecord.setNew(n);
	}

	public boolean isCachedModel() {
		return _academicRecord.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_academicRecord.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _academicRecord.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _academicRecord.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_academicRecord.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _academicRecord.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_academicRecord.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AcademicRecordWrapper((AcademicRecord)_academicRecord.clone());
	}

	public int compareTo(info.diit.portal.model.AcademicRecord academicRecord) {
		return _academicRecord.compareTo(academicRecord);
	}

	@Override
	public int hashCode() {
		return _academicRecord.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.AcademicRecord> toCacheModel() {
		return _academicRecord.toCacheModel();
	}

	public info.diit.portal.model.AcademicRecord toEscapedModel() {
		return new AcademicRecordWrapper(_academicRecord.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _academicRecord.toString();
	}

	public java.lang.String toXmlString() {
		return _academicRecord.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_academicRecord.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public AcademicRecord getWrappedAcademicRecord() {
		return _academicRecord;
	}

	public AcademicRecord getWrappedModel() {
		return _academicRecord;
	}

	public void resetOriginalValues() {
		_academicRecord.resetOriginalValues();
	}

	private AcademicRecord _academicRecord;
}
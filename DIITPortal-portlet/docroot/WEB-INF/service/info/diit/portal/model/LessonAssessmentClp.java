/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.service.LessonAssessmentLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class LessonAssessmentClp extends BaseModelImpl<LessonAssessment>
	implements LessonAssessment {
	public LessonAssessmentClp() {
	}

	public Class<?> getModelClass() {
		return LessonAssessment.class;
	}

	public String getModelClassName() {
		return LessonAssessment.class.getName();
	}

	public long getPrimaryKey() {
		return _lessonAssessmentId;
	}

	public void setPrimaryKey(long primaryKey) {
		setLessonAssessmentId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_lessonAssessmentId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("lessonAssessmentId", getLessonAssessmentId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("lessonPlanId", getLessonPlanId());
		attributes.put("assessmentType", getAssessmentType());
		attributes.put("number", getNumber());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long lessonAssessmentId = (Long)attributes.get("lessonAssessmentId");

		if (lessonAssessmentId != null) {
			setLessonAssessmentId(lessonAssessmentId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long lessonPlanId = (Long)attributes.get("lessonPlanId");

		if (lessonPlanId != null) {
			setLessonPlanId(lessonPlanId);
		}

		Long assessmentType = (Long)attributes.get("assessmentType");

		if (assessmentType != null) {
			setAssessmentType(assessmentType);
		}

		Long number = (Long)attributes.get("number");

		if (number != null) {
			setNumber(number);
		}
	}

	public long getLessonAssessmentId() {
		return _lessonAssessmentId;
	}

	public void setLessonAssessmentId(long lessonAssessmentId) {
		_lessonAssessmentId = lessonAssessmentId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getLessonPlanId() {
		return _lessonPlanId;
	}

	public void setLessonPlanId(long lessonPlanId) {
		_lessonPlanId = lessonPlanId;
	}

	public long getAssessmentType() {
		return _assessmentType;
	}

	public void setAssessmentType(long assessmentType) {
		_assessmentType = assessmentType;
	}

	public long getNumber() {
		return _number;
	}

	public void setNumber(long number) {
		_number = number;
	}

	public BaseModel<?> getLessonAssessmentRemoteModel() {
		return _lessonAssessmentRemoteModel;
	}

	public void setLessonAssessmentRemoteModel(
		BaseModel<?> lessonAssessmentRemoteModel) {
		_lessonAssessmentRemoteModel = lessonAssessmentRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			LessonAssessmentLocalServiceUtil.addLessonAssessment(this);
		}
		else {
			LessonAssessmentLocalServiceUtil.updateLessonAssessment(this);
		}
	}

	@Override
	public LessonAssessment toEscapedModel() {
		return (LessonAssessment)Proxy.newProxyInstance(LessonAssessment.class.getClassLoader(),
			new Class[] { LessonAssessment.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		LessonAssessmentClp clone = new LessonAssessmentClp();

		clone.setLessonAssessmentId(getLessonAssessmentId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setLessonPlanId(getLessonPlanId());
		clone.setAssessmentType(getAssessmentType());
		clone.setNumber(getNumber());

		return clone;
	}

	public int compareTo(LessonAssessment lessonAssessment) {
		long primaryKey = lessonAssessment.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		LessonAssessmentClp lessonAssessment = null;

		try {
			lessonAssessment = (LessonAssessmentClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = lessonAssessment.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{lessonAssessmentId=");
		sb.append(getLessonAssessmentId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", lessonPlanId=");
		sb.append(getLessonPlanId());
		sb.append(", assessmentType=");
		sb.append(getAssessmentType());
		sb.append(", number=");
		sb.append(getNumber());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(31);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.LessonAssessment");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>lessonAssessmentId</column-name><column-value><![CDATA[");
		sb.append(getLessonAssessmentId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lessonPlanId</column-name><column-value><![CDATA[");
		sb.append(getLessonPlanId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>assessmentType</column-name><column-value><![CDATA[");
		sb.append(getAssessmentType());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>number</column-name><column-value><![CDATA[");
		sb.append(getNumber());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _lessonAssessmentId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _lessonPlanId;
	private long _assessmentType;
	private long _number;
	private BaseModel<?> _lessonAssessmentRemoteModel;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Payment}.
 * </p>
 *
 * @author    mohammad
 * @see       Payment
 * @generated
 */
public class PaymentWrapper implements Payment, ModelWrapper<Payment> {
	public PaymentWrapper(Payment payment) {
		_payment = payment;
	}

	public Class<?> getModelClass() {
		return Payment.class;
	}

	public String getModelClassName() {
		return Payment.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("paymentId", getPaymentId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("studentId", getStudentId());
		attributes.put("batchId", getBatchId());
		attributes.put("feeTypeId", getFeeTypeId());
		attributes.put("amount", getAmount());
		attributes.put("paymentDate", getPaymentDate());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long paymentId = (Long)attributes.get("paymentId");

		if (paymentId != null) {
			setPaymentId(paymentId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Long feeTypeId = (Long)attributes.get("feeTypeId");

		if (feeTypeId != null) {
			setFeeTypeId(feeTypeId);
		}

		Double amount = (Double)attributes.get("amount");

		if (amount != null) {
			setAmount(amount);
		}

		Date paymentDate = (Date)attributes.get("paymentDate");

		if (paymentDate != null) {
			setPaymentDate(paymentDate);
		}
	}

	/**
	* Returns the primary key of this payment.
	*
	* @return the primary key of this payment
	*/
	public long getPrimaryKey() {
		return _payment.getPrimaryKey();
	}

	/**
	* Sets the primary key of this payment.
	*
	* @param primaryKey the primary key of this payment
	*/
	public void setPrimaryKey(long primaryKey) {
		_payment.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the payment ID of this payment.
	*
	* @return the payment ID of this payment
	*/
	public long getPaymentId() {
		return _payment.getPaymentId();
	}

	/**
	* Sets the payment ID of this payment.
	*
	* @param paymentId the payment ID of this payment
	*/
	public void setPaymentId(long paymentId) {
		_payment.setPaymentId(paymentId);
	}

	/**
	* Returns the company ID of this payment.
	*
	* @return the company ID of this payment
	*/
	public long getCompanyId() {
		return _payment.getCompanyId();
	}

	/**
	* Sets the company ID of this payment.
	*
	* @param companyId the company ID of this payment
	*/
	public void setCompanyId(long companyId) {
		_payment.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this payment.
	*
	* @return the user ID of this payment
	*/
	public long getUserId() {
		return _payment.getUserId();
	}

	/**
	* Sets the user ID of this payment.
	*
	* @param userId the user ID of this payment
	*/
	public void setUserId(long userId) {
		_payment.setUserId(userId);
	}

	/**
	* Returns the user uuid of this payment.
	*
	* @return the user uuid of this payment
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _payment.getUserUuid();
	}

	/**
	* Sets the user uuid of this payment.
	*
	* @param userUuid the user uuid of this payment
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_payment.setUserUuid(userUuid);
	}

	/**
	* Returns the create date of this payment.
	*
	* @return the create date of this payment
	*/
	public java.util.Date getCreateDate() {
		return _payment.getCreateDate();
	}

	/**
	* Sets the create date of this payment.
	*
	* @param createDate the create date of this payment
	*/
	public void setCreateDate(java.util.Date createDate) {
		_payment.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this payment.
	*
	* @return the modified date of this payment
	*/
	public java.util.Date getModifiedDate() {
		return _payment.getModifiedDate();
	}

	/**
	* Sets the modified date of this payment.
	*
	* @param modifiedDate the modified date of this payment
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_payment.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the student ID of this payment.
	*
	* @return the student ID of this payment
	*/
	public long getStudentId() {
		return _payment.getStudentId();
	}

	/**
	* Sets the student ID of this payment.
	*
	* @param studentId the student ID of this payment
	*/
	public void setStudentId(long studentId) {
		_payment.setStudentId(studentId);
	}

	/**
	* Returns the batch ID of this payment.
	*
	* @return the batch ID of this payment
	*/
	public long getBatchId() {
		return _payment.getBatchId();
	}

	/**
	* Sets the batch ID of this payment.
	*
	* @param batchId the batch ID of this payment
	*/
	public void setBatchId(long batchId) {
		_payment.setBatchId(batchId);
	}

	/**
	* Returns the fee type ID of this payment.
	*
	* @return the fee type ID of this payment
	*/
	public long getFeeTypeId() {
		return _payment.getFeeTypeId();
	}

	/**
	* Sets the fee type ID of this payment.
	*
	* @param feeTypeId the fee type ID of this payment
	*/
	public void setFeeTypeId(long feeTypeId) {
		_payment.setFeeTypeId(feeTypeId);
	}

	/**
	* Returns the amount of this payment.
	*
	* @return the amount of this payment
	*/
	public double getAmount() {
		return _payment.getAmount();
	}

	/**
	* Sets the amount of this payment.
	*
	* @param amount the amount of this payment
	*/
	public void setAmount(double amount) {
		_payment.setAmount(amount);
	}

	/**
	* Returns the payment date of this payment.
	*
	* @return the payment date of this payment
	*/
	public java.util.Date getPaymentDate() {
		return _payment.getPaymentDate();
	}

	/**
	* Sets the payment date of this payment.
	*
	* @param paymentDate the payment date of this payment
	*/
	public void setPaymentDate(java.util.Date paymentDate) {
		_payment.setPaymentDate(paymentDate);
	}

	public boolean isNew() {
		return _payment.isNew();
	}

	public void setNew(boolean n) {
		_payment.setNew(n);
	}

	public boolean isCachedModel() {
		return _payment.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_payment.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _payment.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _payment.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_payment.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _payment.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_payment.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new PaymentWrapper((Payment)_payment.clone());
	}

	public int compareTo(info.diit.portal.model.Payment payment) {
		return _payment.compareTo(payment);
	}

	@Override
	public int hashCode() {
		return _payment.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.Payment> toCacheModel() {
		return _payment.toCacheModel();
	}

	public info.diit.portal.model.Payment toEscapedModel() {
		return new PaymentWrapper(_payment.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _payment.toString();
	}

	public java.lang.String toXmlString() {
		return _payment.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_payment.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Payment getWrappedPayment() {
		return _payment;
	}

	public Payment getWrappedModel() {
		return _payment;
	}

	public void resetOriginalValues() {
		_payment.resetOriginalValues();
	}

	private Payment _payment;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Student}.
 * </p>
 *
 * @author    mohammad
 * @see       Student
 * @generated
 */
public class StudentWrapper implements Student, ModelWrapper<Student> {
	public StudentWrapper(Student student) {
		_student = student;
	}

	public Class<?> getModelClass() {
		return Student.class;
	}

	public String getModelClassName() {
		return Student.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("studentId", getStudentId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("name", getName());
		attributes.put("fatherName", getFatherName());
		attributes.put("motherName", getMotherName());
		attributes.put("presentAddress", getPresentAddress());
		attributes.put("permanentAddress", getPermanentAddress());
		attributes.put("homePhone", getHomePhone());
		attributes.put("dateOfBirth", getDateOfBirth());
		attributes.put("gender", getGender());
		attributes.put("religion", getReligion());
		attributes.put("nationality", getNationality());
		attributes.put("photo", getPhoto());
		attributes.put("status", getStatus());
		attributes.put("studentUserID", getStudentUserID());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String fatherName = (String)attributes.get("fatherName");

		if (fatherName != null) {
			setFatherName(fatherName);
		}

		String motherName = (String)attributes.get("motherName");

		if (motherName != null) {
			setMotherName(motherName);
		}

		String presentAddress = (String)attributes.get("presentAddress");

		if (presentAddress != null) {
			setPresentAddress(presentAddress);
		}

		String permanentAddress = (String)attributes.get("permanentAddress");

		if (permanentAddress != null) {
			setPermanentAddress(permanentAddress);
		}

		String homePhone = (String)attributes.get("homePhone");

		if (homePhone != null) {
			setHomePhone(homePhone);
		}

		Date dateOfBirth = (Date)attributes.get("dateOfBirth");

		if (dateOfBirth != null) {
			setDateOfBirth(dateOfBirth);
		}

		String gender = (String)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		String religion = (String)attributes.get("religion");

		if (religion != null) {
			setReligion(religion);
		}

		String nationality = (String)attributes.get("nationality");

		if (nationality != null) {
			setNationality(nationality);
		}

		Long photo = (Long)attributes.get("photo");

		if (photo != null) {
			setPhoto(photo);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long studentUserID = (Long)attributes.get("studentUserID");

		if (studentUserID != null) {
			setStudentUserID(studentUserID);
		}
	}

	/**
	* Returns the primary key of this student.
	*
	* @return the primary key of this student
	*/
	public long getPrimaryKey() {
		return _student.getPrimaryKey();
	}

	/**
	* Sets the primary key of this student.
	*
	* @param primaryKey the primary key of this student
	*/
	public void setPrimaryKey(long primaryKey) {
		_student.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the student ID of this student.
	*
	* @return the student ID of this student
	*/
	public long getStudentId() {
		return _student.getStudentId();
	}

	/**
	* Sets the student ID of this student.
	*
	* @param studentId the student ID of this student
	*/
	public void setStudentId(long studentId) {
		_student.setStudentId(studentId);
	}

	/**
	* Returns the company ID of this student.
	*
	* @return the company ID of this student
	*/
	public long getCompanyId() {
		return _student.getCompanyId();
	}

	/**
	* Sets the company ID of this student.
	*
	* @param companyId the company ID of this student
	*/
	public void setCompanyId(long companyId) {
		_student.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this student.
	*
	* @return the user ID of this student
	*/
	public long getUserId() {
		return _student.getUserId();
	}

	/**
	* Sets the user ID of this student.
	*
	* @param userId the user ID of this student
	*/
	public void setUserId(long userId) {
		_student.setUserId(userId);
	}

	/**
	* Returns the user uuid of this student.
	*
	* @return the user uuid of this student
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _student.getUserUuid();
	}

	/**
	* Sets the user uuid of this student.
	*
	* @param userUuid the user uuid of this student
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_student.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this student.
	*
	* @return the user name of this student
	*/
	public java.lang.String getUserName() {
		return _student.getUserName();
	}

	/**
	* Sets the user name of this student.
	*
	* @param userName the user name of this student
	*/
	public void setUserName(java.lang.String userName) {
		_student.setUserName(userName);
	}

	/**
	* Returns the create date of this student.
	*
	* @return the create date of this student
	*/
	public java.util.Date getCreateDate() {
		return _student.getCreateDate();
	}

	/**
	* Sets the create date of this student.
	*
	* @param createDate the create date of this student
	*/
	public void setCreateDate(java.util.Date createDate) {
		_student.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this student.
	*
	* @return the modified date of this student
	*/
	public java.util.Date getModifiedDate() {
		return _student.getModifiedDate();
	}

	/**
	* Sets the modified date of this student.
	*
	* @param modifiedDate the modified date of this student
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_student.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the organization ID of this student.
	*
	* @return the organization ID of this student
	*/
	public long getOrganizationId() {
		return _student.getOrganizationId();
	}

	/**
	* Sets the organization ID of this student.
	*
	* @param organizationId the organization ID of this student
	*/
	public void setOrganizationId(long organizationId) {
		_student.setOrganizationId(organizationId);
	}

	/**
	* Returns the name of this student.
	*
	* @return the name of this student
	*/
	public java.lang.String getName() {
		return _student.getName();
	}

	/**
	* Sets the name of this student.
	*
	* @param name the name of this student
	*/
	public void setName(java.lang.String name) {
		_student.setName(name);
	}

	/**
	* Returns the father name of this student.
	*
	* @return the father name of this student
	*/
	public java.lang.String getFatherName() {
		return _student.getFatherName();
	}

	/**
	* Sets the father name of this student.
	*
	* @param fatherName the father name of this student
	*/
	public void setFatherName(java.lang.String fatherName) {
		_student.setFatherName(fatherName);
	}

	/**
	* Returns the mother name of this student.
	*
	* @return the mother name of this student
	*/
	public java.lang.String getMotherName() {
		return _student.getMotherName();
	}

	/**
	* Sets the mother name of this student.
	*
	* @param motherName the mother name of this student
	*/
	public void setMotherName(java.lang.String motherName) {
		_student.setMotherName(motherName);
	}

	/**
	* Returns the present address of this student.
	*
	* @return the present address of this student
	*/
	public java.lang.String getPresentAddress() {
		return _student.getPresentAddress();
	}

	/**
	* Sets the present address of this student.
	*
	* @param presentAddress the present address of this student
	*/
	public void setPresentAddress(java.lang.String presentAddress) {
		_student.setPresentAddress(presentAddress);
	}

	/**
	* Returns the permanent address of this student.
	*
	* @return the permanent address of this student
	*/
	public java.lang.String getPermanentAddress() {
		return _student.getPermanentAddress();
	}

	/**
	* Sets the permanent address of this student.
	*
	* @param permanentAddress the permanent address of this student
	*/
	public void setPermanentAddress(java.lang.String permanentAddress) {
		_student.setPermanentAddress(permanentAddress);
	}

	/**
	* Returns the home phone of this student.
	*
	* @return the home phone of this student
	*/
	public java.lang.String getHomePhone() {
		return _student.getHomePhone();
	}

	/**
	* Sets the home phone of this student.
	*
	* @param homePhone the home phone of this student
	*/
	public void setHomePhone(java.lang.String homePhone) {
		_student.setHomePhone(homePhone);
	}

	/**
	* Returns the date of birth of this student.
	*
	* @return the date of birth of this student
	*/
	public java.util.Date getDateOfBirth() {
		return _student.getDateOfBirth();
	}

	/**
	* Sets the date of birth of this student.
	*
	* @param dateOfBirth the date of birth of this student
	*/
	public void setDateOfBirth(java.util.Date dateOfBirth) {
		_student.setDateOfBirth(dateOfBirth);
	}

	/**
	* Returns the gender of this student.
	*
	* @return the gender of this student
	*/
	public java.lang.String getGender() {
		return _student.getGender();
	}

	/**
	* Sets the gender of this student.
	*
	* @param gender the gender of this student
	*/
	public void setGender(java.lang.String gender) {
		_student.setGender(gender);
	}

	/**
	* Returns the religion of this student.
	*
	* @return the religion of this student
	*/
	public java.lang.String getReligion() {
		return _student.getReligion();
	}

	/**
	* Sets the religion of this student.
	*
	* @param religion the religion of this student
	*/
	public void setReligion(java.lang.String religion) {
		_student.setReligion(religion);
	}

	/**
	* Returns the nationality of this student.
	*
	* @return the nationality of this student
	*/
	public java.lang.String getNationality() {
		return _student.getNationality();
	}

	/**
	* Sets the nationality of this student.
	*
	* @param nationality the nationality of this student
	*/
	public void setNationality(java.lang.String nationality) {
		_student.setNationality(nationality);
	}

	/**
	* Returns the photo of this student.
	*
	* @return the photo of this student
	*/
	public long getPhoto() {
		return _student.getPhoto();
	}

	/**
	* Sets the photo of this student.
	*
	* @param photo the photo of this student
	*/
	public void setPhoto(long photo) {
		_student.setPhoto(photo);
	}

	/**
	* Returns the status of this student.
	*
	* @return the status of this student
	*/
	public int getStatus() {
		return _student.getStatus();
	}

	/**
	* Sets the status of this student.
	*
	* @param status the status of this student
	*/
	public void setStatus(int status) {
		_student.setStatus(status);
	}

	/**
	* Returns the student user i d of this student.
	*
	* @return the student user i d of this student
	*/
	public long getStudentUserID() {
		return _student.getStudentUserID();
	}

	/**
	* Sets the student user i d of this student.
	*
	* @param studentUserID the student user i d of this student
	*/
	public void setStudentUserID(long studentUserID) {
		_student.setStudentUserID(studentUserID);
	}

	public boolean isNew() {
		return _student.isNew();
	}

	public void setNew(boolean n) {
		_student.setNew(n);
	}

	public boolean isCachedModel() {
		return _student.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_student.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _student.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _student.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_student.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _student.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_student.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new StudentWrapper((Student)_student.clone());
	}

	public int compareTo(info.diit.portal.model.Student student) {
		return _student.compareTo(student);
	}

	@Override
	public int hashCode() {
		return _student.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.Student> toCacheModel() {
		return _student.toCacheModel();
	}

	public info.diit.portal.model.Student toEscapedModel() {
		return new StudentWrapper(_student.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _student.toString();
	}

	public java.lang.String toXmlString() {
		return _student.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_student.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Student getWrappedStudent() {
		return _student;
	}

	public Student getWrappedModel() {
		return _student;
	}

	public void resetOriginalValues() {
		_student.resetOriginalValues();
	}

	private Student _student;
}
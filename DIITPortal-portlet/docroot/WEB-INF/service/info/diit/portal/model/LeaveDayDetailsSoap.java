/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class LeaveDayDetailsSoap implements Serializable {
	public static LeaveDayDetailsSoap toSoapModel(LeaveDayDetails model) {
		LeaveDayDetailsSoap soapModel = new LeaveDayDetailsSoap();

		soapModel.setLeaveDayDetailsId(model.getLeaveDayDetailsId());
		soapModel.setLeaveId(model.getLeaveId());
		soapModel.setLeaveDate(model.getLeaveDate());
		soapModel.setDay(model.getDay());

		return soapModel;
	}

	public static LeaveDayDetailsSoap[] toSoapModels(LeaveDayDetails[] models) {
		LeaveDayDetailsSoap[] soapModels = new LeaveDayDetailsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LeaveDayDetailsSoap[][] toSoapModels(
		LeaveDayDetails[][] models) {
		LeaveDayDetailsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LeaveDayDetailsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LeaveDayDetailsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LeaveDayDetailsSoap[] toSoapModels(
		List<LeaveDayDetails> models) {
		List<LeaveDayDetailsSoap> soapModels = new ArrayList<LeaveDayDetailsSoap>(models.size());

		for (LeaveDayDetails model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LeaveDayDetailsSoap[soapModels.size()]);
	}

	public LeaveDayDetailsSoap() {
	}

	public long getPrimaryKey() {
		return _leaveDayDetailsId;
	}

	public void setPrimaryKey(long pk) {
		setLeaveDayDetailsId(pk);
	}

	public long getLeaveDayDetailsId() {
		return _leaveDayDetailsId;
	}

	public void setLeaveDayDetailsId(long leaveDayDetailsId) {
		_leaveDayDetailsId = leaveDayDetailsId;
	}

	public long getLeaveId() {
		return _leaveId;
	}

	public void setLeaveId(long leaveId) {
		_leaveId = leaveId;
	}

	public Date getLeaveDate() {
		return _leaveDate;
	}

	public void setLeaveDate(Date leaveDate) {
		_leaveDate = leaveDate;
	}

	public double getDay() {
		return _day;
	}

	public void setDay(double day) {
		_day = day;
	}

	private long _leaveDayDetailsId;
	private long _leaveId;
	private Date _leaveDate;
	private double _day;
}
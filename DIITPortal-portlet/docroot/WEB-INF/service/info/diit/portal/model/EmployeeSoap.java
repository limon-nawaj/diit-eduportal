/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class EmployeeSoap implements Serializable {
	public static EmployeeSoap toSoapModel(Employee model) {
		EmployeeSoap soapModel = new EmployeeSoap();

		soapModel.setEmployeeId(model.getEmployeeId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setEmployeeUserId(model.getEmployeeUserId());
		soapModel.setName(model.getName());
		soapModel.setGender(model.getGender());
		soapModel.setReligion(model.getReligion());
		soapModel.setBlood(model.getBlood());
		soapModel.setDob(model.getDob());
		soapModel.setDesignation(model.getDesignation());
		soapModel.setBranch(model.getBranch());
		soapModel.setStatus(model.getStatus());
		soapModel.setPresentAddress(model.getPresentAddress());
		soapModel.setPermanentAddress(model.getPermanentAddress());
		soapModel.setPhoto(model.getPhoto());
		soapModel.setSupervisor(model.getSupervisor());
		soapModel.setRole(model.getRole());

		return soapModel;
	}

	public static EmployeeSoap[] toSoapModels(Employee[] models) {
		EmployeeSoap[] soapModels = new EmployeeSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EmployeeSoap[][] toSoapModels(Employee[][] models) {
		EmployeeSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EmployeeSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EmployeeSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EmployeeSoap[] toSoapModels(List<Employee> models) {
		List<EmployeeSoap> soapModels = new ArrayList<EmployeeSoap>(models.size());

		for (Employee model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EmployeeSoap[soapModels.size()]);
	}

	public EmployeeSoap() {
	}

	public long getPrimaryKey() {
		return _employeeId;
	}

	public void setPrimaryKey(long pk) {
		setEmployeeId(pk);
	}

	public long getEmployeeId() {
		return _employeeId;
	}

	public void setEmployeeId(long employeeId) {
		_employeeId = employeeId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getEmployeeUserId() {
		return _employeeUserId;
	}

	public void setEmployeeUserId(long employeeUserId) {
		_employeeUserId = employeeUserId;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public long getGender() {
		return _gender;
	}

	public void setGender(long gender) {
		_gender = gender;
	}

	public long getReligion() {
		return _religion;
	}

	public void setReligion(long religion) {
		_religion = religion;
	}

	public long getBlood() {
		return _blood;
	}

	public void setBlood(long blood) {
		_blood = blood;
	}

	public Date getDob() {
		return _dob;
	}

	public void setDob(Date dob) {
		_dob = dob;
	}

	public long getDesignation() {
		return _designation;
	}

	public void setDesignation(long designation) {
		_designation = designation;
	}

	public long getBranch() {
		return _branch;
	}

	public void setBranch(long branch) {
		_branch = branch;
	}

	public long getStatus() {
		return _status;
	}

	public void setStatus(long status) {
		_status = status;
	}

	public String getPresentAddress() {
		return _presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		_presentAddress = presentAddress;
	}

	public String getPermanentAddress() {
		return _permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		_permanentAddress = permanentAddress;
	}

	public long getPhoto() {
		return _photo;
	}

	public void setPhoto(long photo) {
		_photo = photo;
	}

	public long getSupervisor() {
		return _supervisor;
	}

	public void setSupervisor(long supervisor) {
		_supervisor = supervisor;
	}

	public long getRole() {
		return _role;
	}

	public void setRole(long role) {
		_role = role;
	}

	private long _employeeId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _employeeUserId;
	private String _name;
	private long _gender;
	private long _religion;
	private long _blood;
	private Date _dob;
	private long _designation;
	private long _branch;
	private long _status;
	private String _presentAddress;
	private String _permanentAddress;
	private long _photo;
	private long _supervisor;
	private long _role;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the StudentFee service. Represents a row in the &quot;EduPortal_StudentFee&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link info.diit.portal.model.impl.StudentFeeModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link info.diit.portal.model.impl.StudentFeeImpl}.
 * </p>
 *
 * @author mohammad
 * @see StudentFee
 * @see info.diit.portal.model.impl.StudentFeeImpl
 * @see info.diit.portal.model.impl.StudentFeeModelImpl
 * @generated
 */
public interface StudentFeeModel extends BaseModel<StudentFee> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a student fee model instance should use the {@link StudentFee} interface instead.
	 */

	/**
	 * Returns the primary key of this student fee.
	 *
	 * @return the primary key of this student fee
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this student fee.
	 *
	 * @param primaryKey the primary key of this student fee
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the student fee ID of this student fee.
	 *
	 * @return the student fee ID of this student fee
	 */
	public long getStudentFeeId();

	/**
	 * Sets the student fee ID of this student fee.
	 *
	 * @param studentFeeId the student fee ID of this student fee
	 */
	public void setStudentFeeId(long studentFeeId);

	/**
	 * Returns the company ID of this student fee.
	 *
	 * @return the company ID of this student fee
	 */
	public long getCompanyId();

	/**
	 * Sets the company ID of this student fee.
	 *
	 * @param companyId the company ID of this student fee
	 */
	public void setCompanyId(long companyId);

	/**
	 * Returns the user ID of this student fee.
	 *
	 * @return the user ID of this student fee
	 */
	public long getUserId();

	/**
	 * Sets the user ID of this student fee.
	 *
	 * @param userId the user ID of this student fee
	 */
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this student fee.
	 *
	 * @return the user uuid of this student fee
	 * @throws SystemException if a system exception occurred
	 */
	public String getUserUuid() throws SystemException;

	/**
	 * Sets the user uuid of this student fee.
	 *
	 * @param userUuid the user uuid of this student fee
	 */
	public void setUserUuid(String userUuid);

	/**
	 * Returns the create date of this student fee.
	 *
	 * @return the create date of this student fee
	 */
	public Date getCreateDate();

	/**
	 * Sets the create date of this student fee.
	 *
	 * @param createDate the create date of this student fee
	 */
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this student fee.
	 *
	 * @return the modified date of this student fee
	 */
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this student fee.
	 *
	 * @param modifiedDate the modified date of this student fee
	 */
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the student ID of this student fee.
	 *
	 * @return the student ID of this student fee
	 */
	public long getStudentId();

	/**
	 * Sets the student ID of this student fee.
	 *
	 * @param studentId the student ID of this student fee
	 */
	public void setStudentId(long studentId);

	/**
	 * Returns the batch ID of this student fee.
	 *
	 * @return the batch ID of this student fee
	 */
	public long getBatchId();

	/**
	 * Sets the batch ID of this student fee.
	 *
	 * @param batchId the batch ID of this student fee
	 */
	public void setBatchId(long batchId);

	/**
	 * Returns the fee type ID of this student fee.
	 *
	 * @return the fee type ID of this student fee
	 */
	public long getFeeTypeId();

	/**
	 * Sets the fee type ID of this student fee.
	 *
	 * @param feeTypeId the fee type ID of this student fee
	 */
	public void setFeeTypeId(long feeTypeId);

	/**
	 * Returns the amount of this student fee.
	 *
	 * @return the amount of this student fee
	 */
	public double getAmount();

	/**
	 * Sets the amount of this student fee.
	 *
	 * @param amount the amount of this student fee
	 */
	public void setAmount(double amount);

	public boolean isNew();

	public void setNew(boolean n);

	public boolean isCachedModel();

	public void setCachedModel(boolean cachedModel);

	public boolean isEscapedModel();

	public Serializable getPrimaryKeyObj();

	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	public ExpandoBridge getExpandoBridge();

	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	public Object clone();

	public int compareTo(StudentFee studentFee);

	public int hashCode();

	public CacheModel<StudentFee> toCacheModel();

	public StudentFee toEscapedModel();

	public String toString();

	public String toXmlString();
}
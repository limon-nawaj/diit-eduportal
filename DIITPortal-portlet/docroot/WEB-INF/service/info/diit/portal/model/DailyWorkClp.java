/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.service.DailyWorkLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class DailyWorkClp extends BaseModelImpl<DailyWork> implements DailyWork {
	public DailyWorkClp() {
	}

	public Class<?> getModelClass() {
		return DailyWork.class;
	}

	public String getModelClassName() {
		return DailyWork.class.getName();
	}

	public long getPrimaryKey() {
		return _dailyWorkId;
	}

	public void setPrimaryKey(long primaryKey) {
		setDailyWorkId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_dailyWorkId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("dailyWorkId", getDailyWorkId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("employeeId", getEmployeeId());
		attributes.put("worKingDay", getWorKingDay());
		attributes.put("taskId", getTaskId());
		attributes.put("time", getTime());
		attributes.put("note", getNote());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long dailyWorkId = (Long)attributes.get("dailyWorkId");

		if (dailyWorkId != null) {
			setDailyWorkId(dailyWorkId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long employeeId = (Long)attributes.get("employeeId");

		if (employeeId != null) {
			setEmployeeId(employeeId);
		}

		Date worKingDay = (Date)attributes.get("worKingDay");

		if (worKingDay != null) {
			setWorKingDay(worKingDay);
		}

		Long taskId = (Long)attributes.get("taskId");

		if (taskId != null) {
			setTaskId(taskId);
		}

		Integer time = (Integer)attributes.get("time");

		if (time != null) {
			setTime(time);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}
	}

	public long getDailyWorkId() {
		return _dailyWorkId;
	}

	public void setDailyWorkId(long dailyWorkId) {
		_dailyWorkId = dailyWorkId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getEmployeeId() {
		return _employeeId;
	}

	public void setEmployeeId(long employeeId) {
		_employeeId = employeeId;
	}

	public Date getWorKingDay() {
		return _worKingDay;
	}

	public void setWorKingDay(Date worKingDay) {
		_worKingDay = worKingDay;
	}

	public long getTaskId() {
		return _taskId;
	}

	public void setTaskId(long taskId) {
		_taskId = taskId;
	}

	public int getTime() {
		return _time;
	}

	public void setTime(int time) {
		_time = time;
	}

	public String getNote() {
		return _note;
	}

	public void setNote(String note) {
		_note = note;
	}

	public BaseModel<?> getDailyWorkRemoteModel() {
		return _dailyWorkRemoteModel;
	}

	public void setDailyWorkRemoteModel(BaseModel<?> dailyWorkRemoteModel) {
		_dailyWorkRemoteModel = dailyWorkRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			DailyWorkLocalServiceUtil.addDailyWork(this);
		}
		else {
			DailyWorkLocalServiceUtil.updateDailyWork(this);
		}
	}

	@Override
	public DailyWork toEscapedModel() {
		return (DailyWork)Proxy.newProxyInstance(DailyWork.class.getClassLoader(),
			new Class[] { DailyWork.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		DailyWorkClp clone = new DailyWorkClp();

		clone.setDailyWorkId(getDailyWorkId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setEmployeeId(getEmployeeId());
		clone.setWorKingDay(getWorKingDay());
		clone.setTaskId(getTaskId());
		clone.setTime(getTime());
		clone.setNote(getNote());

		return clone;
	}

	public int compareTo(DailyWork dailyWork) {
		int value = 0;

		value = DateUtil.compareTo(getWorKingDay(), dailyWork.getWorKingDay());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		DailyWorkClp dailyWork = null;

		try {
			dailyWork = (DailyWorkClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = dailyWork.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{dailyWorkId=");
		sb.append(getDailyWorkId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", employeeId=");
		sb.append(getEmployeeId());
		sb.append(", worKingDay=");
		sb.append(getWorKingDay());
		sb.append(", taskId=");
		sb.append(getTaskId());
		sb.append(", time=");
		sb.append(getTime());
		sb.append(", note=");
		sb.append(getNote());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(37);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.DailyWork");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>dailyWorkId</column-name><column-value><![CDATA[");
		sb.append(getDailyWorkId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>employeeId</column-name><column-value><![CDATA[");
		sb.append(getEmployeeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>worKingDay</column-name><column-value><![CDATA[");
		sb.append(getWorKingDay());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>taskId</column-name><column-value><![CDATA[");
		sb.append(getTaskId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>time</column-name><column-value><![CDATA[");
		sb.append(getTime());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>note</column-name><column-value><![CDATA[");
		sb.append(getNote());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _dailyWorkId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _employeeId;
	private Date _worKingDay;
	private long _taskId;
	private int _time;
	private String _note;
	private BaseModel<?> _dailyWorkRemoteModel;
}
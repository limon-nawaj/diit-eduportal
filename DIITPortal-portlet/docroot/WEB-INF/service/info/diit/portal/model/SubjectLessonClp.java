/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.service.SubjectLessonLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class SubjectLessonClp extends BaseModelImpl<SubjectLesson>
	implements SubjectLesson {
	public SubjectLessonClp() {
	}

	public Class<?> getModelClass() {
		return SubjectLesson.class;
	}

	public String getModelClassName() {
		return SubjectLesson.class.getName();
	}

	public long getPrimaryKey() {
		return _subjectLessonId;
	}

	public void setPrimaryKey(long primaryKey) {
		setSubjectLessonId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_subjectLessonId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("subjectLessonId", getSubjectLessonId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("batchId", getBatchId());
		attributes.put("subjectId", getSubjectId());
		attributes.put("lessonPlanId", getLessonPlanId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long subjectLessonId = (Long)attributes.get("subjectLessonId");

		if (subjectLessonId != null) {
			setSubjectLessonId(subjectLessonId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Long subjectId = (Long)attributes.get("subjectId");

		if (subjectId != null) {
			setSubjectId(subjectId);
		}

		Long lessonPlanId = (Long)attributes.get("lessonPlanId");

		if (lessonPlanId != null) {
			setLessonPlanId(lessonPlanId);
		}
	}

	public long getSubjectLessonId() {
		return _subjectLessonId;
	}

	public void setSubjectLessonId(long subjectLessonId) {
		_subjectLessonId = subjectLessonId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public long getSubjectId() {
		return _subjectId;
	}

	public void setSubjectId(long subjectId) {
		_subjectId = subjectId;
	}

	public long getLessonPlanId() {
		return _lessonPlanId;
	}

	public void setLessonPlanId(long lessonPlanId) {
		_lessonPlanId = lessonPlanId;
	}

	public BaseModel<?> getSubjectLessonRemoteModel() {
		return _subjectLessonRemoteModel;
	}

	public void setSubjectLessonRemoteModel(
		BaseModel<?> subjectLessonRemoteModel) {
		_subjectLessonRemoteModel = subjectLessonRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			SubjectLessonLocalServiceUtil.addSubjectLesson(this);
		}
		else {
			SubjectLessonLocalServiceUtil.updateSubjectLesson(this);
		}
	}

	@Override
	public SubjectLesson toEscapedModel() {
		return (SubjectLesson)Proxy.newProxyInstance(SubjectLesson.class.getClassLoader(),
			new Class[] { SubjectLesson.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		SubjectLessonClp clone = new SubjectLessonClp();

		clone.setSubjectLessonId(getSubjectLessonId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setOrganizationId(getOrganizationId());
		clone.setBatchId(getBatchId());
		clone.setSubjectId(getSubjectId());
		clone.setLessonPlanId(getLessonPlanId());

		return clone;
	}

	public int compareTo(SubjectLesson subjectLesson) {
		long primaryKey = subjectLesson.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		SubjectLessonClp subjectLesson = null;

		try {
			subjectLesson = (SubjectLessonClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = subjectLesson.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{subjectLessonId=");
		sb.append(getSubjectLessonId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", batchId=");
		sb.append(getBatchId());
		sb.append(", subjectId=");
		sb.append(getSubjectId());
		sb.append(", lessonPlanId=");
		sb.append(getLessonPlanId());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.SubjectLesson");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>subjectLessonId</column-name><column-value><![CDATA[");
		sb.append(getSubjectLessonId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>batchId</column-name><column-value><![CDATA[");
		sb.append(getBatchId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>subjectId</column-name><column-value><![CDATA[");
		sb.append(getSubjectId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lessonPlanId</column-name><column-value><![CDATA[");
		sb.append(getLessonPlanId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _subjectLessonId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private long _batchId;
	private long _subjectId;
	private long _lessonPlanId;
	private BaseModel<?> _subjectLessonRemoteModel;
}
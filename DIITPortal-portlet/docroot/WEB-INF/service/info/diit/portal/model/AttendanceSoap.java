/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class AttendanceSoap implements Serializable {
	public static AttendanceSoap toSoapModel(Attendance model) {
		AttendanceSoap soapModel = new AttendanceSoap();

		soapModel.setAttendanceId(model.getAttendanceId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCampus(model.getCampus());
		soapModel.setAttendanceDate(model.getAttendanceDate());
		soapModel.setAttendanceRate(model.getAttendanceRate());
		soapModel.setBatch(model.getBatch());
		soapModel.setSubject(model.getSubject());
		soapModel.setRoutineIn(model.getRoutineIn());
		soapModel.setRoutineOut(model.getRoutineOut());
		soapModel.setActualIn(model.getActualIn());
		soapModel.setActualOut(model.getActualOut());
		soapModel.setRoutineDuration(model.getRoutineDuration());
		soapModel.setActualDuration(model.getActualDuration());
		soapModel.setLateEntry(model.getLateEntry());
		soapModel.setLagTime(model.getLagTime());
		soapModel.setNote(model.getNote());

		return soapModel;
	}

	public static AttendanceSoap[] toSoapModels(Attendance[] models) {
		AttendanceSoap[] soapModels = new AttendanceSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AttendanceSoap[][] toSoapModels(Attendance[][] models) {
		AttendanceSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AttendanceSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AttendanceSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AttendanceSoap[] toSoapModels(List<Attendance> models) {
		List<AttendanceSoap> soapModels = new ArrayList<AttendanceSoap>(models.size());

		for (Attendance model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AttendanceSoap[soapModels.size()]);
	}

	public AttendanceSoap() {
	}

	public long getPrimaryKey() {
		return _attendanceId;
	}

	public void setPrimaryKey(long pk) {
		setAttendanceId(pk);
	}

	public long getAttendanceId() {
		return _attendanceId;
	}

	public void setAttendanceId(long attendanceId) {
		_attendanceId = attendanceId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getCampus() {
		return _campus;
	}

	public void setCampus(long campus) {
		_campus = campus;
	}

	public Date getAttendanceDate() {
		return _attendanceDate;
	}

	public void setAttendanceDate(Date attendanceDate) {
		_attendanceDate = attendanceDate;
	}

	public long getAttendanceRate() {
		return _attendanceRate;
	}

	public void setAttendanceRate(long attendanceRate) {
		_attendanceRate = attendanceRate;
	}

	public long getBatch() {
		return _batch;
	}

	public void setBatch(long batch) {
		_batch = batch;
	}

	public long getSubject() {
		return _subject;
	}

	public void setSubject(long subject) {
		_subject = subject;
	}

	public Date getRoutineIn() {
		return _routineIn;
	}

	public void setRoutineIn(Date routineIn) {
		_routineIn = routineIn;
	}

	public Date getRoutineOut() {
		return _routineOut;
	}

	public void setRoutineOut(Date routineOut) {
		_routineOut = routineOut;
	}

	public Date getActualIn() {
		return _actualIn;
	}

	public void setActualIn(Date actualIn) {
		_actualIn = actualIn;
	}

	public Date getActualOut() {
		return _actualOut;
	}

	public void setActualOut(Date actualOut) {
		_actualOut = actualOut;
	}

	public long getRoutineDuration() {
		return _routineDuration;
	}

	public void setRoutineDuration(long routineDuration) {
		_routineDuration = routineDuration;
	}

	public long getActualDuration() {
		return _actualDuration;
	}

	public void setActualDuration(long actualDuration) {
		_actualDuration = actualDuration;
	}

	public long getLateEntry() {
		return _lateEntry;
	}

	public void setLateEntry(long lateEntry) {
		_lateEntry = lateEntry;
	}

	public long getLagTime() {
		return _lagTime;
	}

	public void setLagTime(long lagTime) {
		_lagTime = lagTime;
	}

	public String getNote() {
		return _note;
	}

	public void setNote(String note) {
		_note = note;
	}

	private long _attendanceId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _campus;
	private Date _attendanceDate;
	private long _attendanceRate;
	private long _batch;
	private long _subject;
	private Date _routineIn;
	private Date _routineOut;
	private Date _actualIn;
	private Date _actualOut;
	private long _routineDuration;
	private long _actualDuration;
	private long _lateEntry;
	private long _lagTime;
	private String _note;
}
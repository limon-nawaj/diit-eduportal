/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class LeaveSoap implements Serializable {
	public static LeaveSoap toSoapModel(Leave model) {
		LeaveSoap soapModel = new LeaveSoap();

		soapModel.setLeaveId(model.getLeaveId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setApplicationDate(model.getApplicationDate());
		soapModel.setEmployee(model.getEmployee());
		soapModel.setResponsibleEmployee(model.getResponsibleEmployee());
		soapModel.setStartDate(model.getStartDate());
		soapModel.setEndDate(model.getEndDate());
		soapModel.setNumberOfDay(model.getNumberOfDay());
		soapModel.setPhoneNumber(model.getPhoneNumber());
		soapModel.setCauseOfLeave(model.getCauseOfLeave());
		soapModel.setWhereEnjoy(model.getWhereEnjoy());
		soapModel.setFirstRecommendation(model.getFirstRecommendation());
		soapModel.setSecondRecommendation(model.getSecondRecommendation());
		soapModel.setLeaveCategory(model.getLeaveCategory());
		soapModel.setResponsibleEmployeeStatus(model.getResponsibleEmployeeStatus());
		soapModel.setApplicationStatus(model.getApplicationStatus());
		soapModel.setComments(model.getComments());
		soapModel.setComplementedBy(model.getComplementedBy());

		return soapModel;
	}

	public static LeaveSoap[] toSoapModels(Leave[] models) {
		LeaveSoap[] soapModels = new LeaveSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LeaveSoap[][] toSoapModels(Leave[][] models) {
		LeaveSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LeaveSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LeaveSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LeaveSoap[] toSoapModels(List<Leave> models) {
		List<LeaveSoap> soapModels = new ArrayList<LeaveSoap>(models.size());

		for (Leave model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LeaveSoap[soapModels.size()]);
	}

	public LeaveSoap() {
	}

	public long getPrimaryKey() {
		return _leaveId;
	}

	public void setPrimaryKey(long pk) {
		setLeaveId(pk);
	}

	public long getLeaveId() {
		return _leaveId;
	}

	public void setLeaveId(long leaveId) {
		_leaveId = leaveId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getApplicationDate() {
		return _applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		_applicationDate = applicationDate;
	}

	public long getEmployee() {
		return _employee;
	}

	public void setEmployee(long employee) {
		_employee = employee;
	}

	public long getResponsibleEmployee() {
		return _responsibleEmployee;
	}

	public void setResponsibleEmployee(long responsibleEmployee) {
		_responsibleEmployee = responsibleEmployee;
	}

	public Date getStartDate() {
		return _startDate;
	}

	public void setStartDate(Date startDate) {
		_startDate = startDate;
	}

	public Date getEndDate() {
		return _endDate;
	}

	public void setEndDate(Date endDate) {
		_endDate = endDate;
	}

	public double getNumberOfDay() {
		return _numberOfDay;
	}

	public void setNumberOfDay(double numberOfDay) {
		_numberOfDay = numberOfDay;
	}

	public String getPhoneNumber() {
		return _phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		_phoneNumber = phoneNumber;
	}

	public String getCauseOfLeave() {
		return _causeOfLeave;
	}

	public void setCauseOfLeave(String causeOfLeave) {
		_causeOfLeave = causeOfLeave;
	}

	public String getWhereEnjoy() {
		return _whereEnjoy;
	}

	public void setWhereEnjoy(String whereEnjoy) {
		_whereEnjoy = whereEnjoy;
	}

	public long getFirstRecommendation() {
		return _firstRecommendation;
	}

	public void setFirstRecommendation(long firstRecommendation) {
		_firstRecommendation = firstRecommendation;
	}

	public long getSecondRecommendation() {
		return _secondRecommendation;
	}

	public void setSecondRecommendation(long secondRecommendation) {
		_secondRecommendation = secondRecommendation;
	}

	public long getLeaveCategory() {
		return _leaveCategory;
	}

	public void setLeaveCategory(long leaveCategory) {
		_leaveCategory = leaveCategory;
	}

	public int getResponsibleEmployeeStatus() {
		return _responsibleEmployeeStatus;
	}

	public void setResponsibleEmployeeStatus(int responsibleEmployeeStatus) {
		_responsibleEmployeeStatus = responsibleEmployeeStatus;
	}

	public int getApplicationStatus() {
		return _applicationStatus;
	}

	public void setApplicationStatus(int applicationStatus) {
		_applicationStatus = applicationStatus;
	}

	public String getComments() {
		return _comments;
	}

	public void setComments(String comments) {
		_comments = comments;
	}

	public long getComplementedBy() {
		return _complementedBy;
	}

	public void setComplementedBy(long complementedBy) {
		_complementedBy = complementedBy;
	}

	private long _leaveId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _applicationDate;
	private long _employee;
	private long _responsibleEmployee;
	private Date _startDate;
	private Date _endDate;
	private double _numberOfDay;
	private String _phoneNumber;
	private String _causeOfLeave;
	private String _whereEnjoy;
	private long _firstRecommendation;
	private long _secondRecommendation;
	private long _leaveCategory;
	private int _responsibleEmployeeStatus;
	private int _applicationStatus;
	private String _comments;
	private long _complementedBy;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class SubjectLessonSoap implements Serializable {
	public static SubjectLessonSoap toSoapModel(SubjectLesson model) {
		SubjectLessonSoap soapModel = new SubjectLessonSoap();

		soapModel.setSubjectLessonId(model.getSubjectLessonId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setBatchId(model.getBatchId());
		soapModel.setSubjectId(model.getSubjectId());
		soapModel.setLessonPlanId(model.getLessonPlanId());

		return soapModel;
	}

	public static SubjectLessonSoap[] toSoapModels(SubjectLesson[] models) {
		SubjectLessonSoap[] soapModels = new SubjectLessonSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static SubjectLessonSoap[][] toSoapModels(SubjectLesson[][] models) {
		SubjectLessonSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new SubjectLessonSoap[models.length][models[0].length];
		}
		else {
			soapModels = new SubjectLessonSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static SubjectLessonSoap[] toSoapModels(List<SubjectLesson> models) {
		List<SubjectLessonSoap> soapModels = new ArrayList<SubjectLessonSoap>(models.size());

		for (SubjectLesson model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new SubjectLessonSoap[soapModels.size()]);
	}

	public SubjectLessonSoap() {
	}

	public long getPrimaryKey() {
		return _subjectLessonId;
	}

	public void setPrimaryKey(long pk) {
		setSubjectLessonId(pk);
	}

	public long getSubjectLessonId() {
		return _subjectLessonId;
	}

	public void setSubjectLessonId(long subjectLessonId) {
		_subjectLessonId = subjectLessonId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public long getSubjectId() {
		return _subjectId;
	}

	public void setSubjectId(long subjectId) {
		_subjectId = subjectId;
	}

	public long getLessonPlanId() {
		return _lessonPlanId;
	}

	public void setLessonPlanId(long lessonPlanId) {
		_lessonPlanId = lessonPlanId;
	}

	private long _subjectLessonId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private long _batchId;
	private long _subjectId;
	private long _lessonPlanId;
}
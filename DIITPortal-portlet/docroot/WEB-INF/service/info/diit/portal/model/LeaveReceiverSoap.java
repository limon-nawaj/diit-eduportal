/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class LeaveReceiverSoap implements Serializable {
	public static LeaveReceiverSoap toSoapModel(LeaveReceiver model) {
		LeaveReceiverSoap soapModel = new LeaveReceiverSoap();

		soapModel.setLeaveReceiverId(model.getLeaveReceiverId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setEmployeeId(model.getEmployeeId());

		return soapModel;
	}

	public static LeaveReceiverSoap[] toSoapModels(LeaveReceiver[] models) {
		LeaveReceiverSoap[] soapModels = new LeaveReceiverSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LeaveReceiverSoap[][] toSoapModels(LeaveReceiver[][] models) {
		LeaveReceiverSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LeaveReceiverSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LeaveReceiverSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LeaveReceiverSoap[] toSoapModels(List<LeaveReceiver> models) {
		List<LeaveReceiverSoap> soapModels = new ArrayList<LeaveReceiverSoap>(models.size());

		for (LeaveReceiver model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LeaveReceiverSoap[soapModels.size()]);
	}

	public LeaveReceiverSoap() {
	}

	public long getPrimaryKey() {
		return _leaveReceiverId;
	}

	public void setPrimaryKey(long pk) {
		setLeaveReceiverId(pk);
	}

	public long getLeaveReceiverId() {
		return _leaveReceiverId;
	}

	public void setLeaveReceiverId(long leaveReceiverId) {
		_leaveReceiverId = leaveReceiverId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getEmployeeId() {
		return _employeeId;
	}

	public void setEmployeeId(long employeeId) {
		_employeeId = employeeId;
	}

	private long _leaveReceiverId;
	private long _organizationId;
	private long _companyId;
	private long _employeeId;
}
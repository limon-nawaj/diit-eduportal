/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import info.diit.portal.service.CommunicationRecordLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class CommunicationRecordClp extends BaseModelImpl<CommunicationRecord>
	implements CommunicationRecord {
	public CommunicationRecordClp() {
	}

	public Class<?> getModelClass() {
		return CommunicationRecord.class;
	}

	public String getModelClassName() {
		return CommunicationRecord.class.getName();
	}

	public long getPrimaryKey() {
		return _communicationRecordId;
	}

	public void setPrimaryKey(long primaryKey) {
		setCommunicationRecordId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_communicationRecordId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("communicationRecordId", getCommunicationRecordId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("communicationBy", getCommunicationBy());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("batch", getBatch());
		attributes.put("date", getDate());
		attributes.put("media", getMedia());
		attributes.put("subject", getSubject());
		attributes.put("communicationWith", getCommunicationWith());
		attributes.put("details", getDetails());
		attributes.put("status", getStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long communicationRecordId = (Long)attributes.get(
				"communicationRecordId");

		if (communicationRecordId != null) {
			setCommunicationRecordId(communicationRecordId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long communicationBy = (Long)attributes.get("communicationBy");

		if (communicationBy != null) {
			setCommunicationBy(communicationBy);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long batch = (Long)attributes.get("batch");

		if (batch != null) {
			setBatch(batch);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		Long media = (Long)attributes.get("media");

		if (media != null) {
			setMedia(media);
		}

		Long subject = (Long)attributes.get("subject");

		if (subject != null) {
			setSubject(subject);
		}

		Long communicationWith = (Long)attributes.get("communicationWith");

		if (communicationWith != null) {
			setCommunicationWith(communicationWith);
		}

		String details = (String)attributes.get("details");

		if (details != null) {
			setDetails(details);
		}

		Long status = (Long)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	public long getCommunicationRecordId() {
		return _communicationRecordId;
	}

	public void setCommunicationRecordId(long communicationRecordId) {
		_communicationRecordId = communicationRecordId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getCommunicationBy() {
		return _communicationBy;
	}

	public void setCommunicationBy(long communicationBy) {
		_communicationBy = communicationBy;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getBatch() {
		return _batch;
	}

	public void setBatch(long batch) {
		_batch = batch;
	}

	public Date getDate() {
		return _date;
	}

	public void setDate(Date date) {
		_date = date;
	}

	public long getMedia() {
		return _media;
	}

	public void setMedia(long media) {
		_media = media;
	}

	public long getSubject() {
		return _subject;
	}

	public void setSubject(long subject) {
		_subject = subject;
	}

	public long getCommunicationWith() {
		return _communicationWith;
	}

	public void setCommunicationWith(long communicationWith) {
		_communicationWith = communicationWith;
	}

	public String getDetails() {
		return _details;
	}

	public void setDetails(String details) {
		_details = details;
	}

	public long getStatus() {
		return _status;
	}

	public void setStatus(long status) {
		_status = status;
	}

	public BaseModel<?> getCommunicationRecordRemoteModel() {
		return _communicationRecordRemoteModel;
	}

	public void setCommunicationRecordRemoteModel(
		BaseModel<?> communicationRecordRemoteModel) {
		_communicationRecordRemoteModel = communicationRecordRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			CommunicationRecordLocalServiceUtil.addCommunicationRecord(this);
		}
		else {
			CommunicationRecordLocalServiceUtil.updateCommunicationRecord(this);
		}
	}

	@Override
	public CommunicationRecord toEscapedModel() {
		return (CommunicationRecord)Proxy.newProxyInstance(CommunicationRecord.class.getClassLoader(),
			new Class[] { CommunicationRecord.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CommunicationRecordClp clone = new CommunicationRecordClp();

		clone.setCommunicationRecordId(getCommunicationRecordId());
		clone.setCompanyId(getCompanyId());
		clone.setOrganizationId(getOrganizationId());
		clone.setCommunicationBy(getCommunicationBy());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setBatch(getBatch());
		clone.setDate(getDate());
		clone.setMedia(getMedia());
		clone.setSubject(getSubject());
		clone.setCommunicationWith(getCommunicationWith());
		clone.setDetails(getDetails());
		clone.setStatus(getStatus());

		return clone;
	}

	public int compareTo(CommunicationRecord communicationRecord) {
		int value = 0;

		value = DateUtil.compareTo(getDate(), communicationRecord.getDate());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		CommunicationRecordClp communicationRecord = null;

		try {
			communicationRecord = (CommunicationRecordClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = communicationRecord.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{communicationRecordId=");
		sb.append(getCommunicationRecordId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", communicationBy=");
		sb.append(getCommunicationBy());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", batch=");
		sb.append(getBatch());
		sb.append(", date=");
		sb.append(getDate());
		sb.append(", media=");
		sb.append(getMedia());
		sb.append(", subject=");
		sb.append(getSubject());
		sb.append(", communicationWith=");
		sb.append(getCommunicationWith());
		sb.append(", details=");
		sb.append(getDetails());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(46);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.CommunicationRecord");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>communicationRecordId</column-name><column-value><![CDATA[");
		sb.append(getCommunicationRecordId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>communicationBy</column-name><column-value><![CDATA[");
		sb.append(getCommunicationBy());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>batch</column-name><column-value><![CDATA[");
		sb.append(getBatch());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>date</column-name><column-value><![CDATA[");
		sb.append(getDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>media</column-name><column-value><![CDATA[");
		sb.append(getMedia());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>subject</column-name><column-value><![CDATA[");
		sb.append(getSubject());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>communicationWith</column-name><column-value><![CDATA[");
		sb.append(getCommunicationWith());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>details</column-name><column-value><![CDATA[");
		sb.append(getDetails());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _communicationRecordId;
	private long _companyId;
	private long _organizationId;
	private long _communicationBy;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _batch;
	private Date _date;
	private long _media;
	private long _subject;
	private long _communicationWith;
	private String _details;
	private long _status;
	private BaseModel<?> _communicationRecordRemoteModel;
}
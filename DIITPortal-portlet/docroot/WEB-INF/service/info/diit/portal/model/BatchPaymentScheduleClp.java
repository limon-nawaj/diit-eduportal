/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.service.BatchPaymentScheduleLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class BatchPaymentScheduleClp extends BaseModelImpl<BatchPaymentSchedule>
	implements BatchPaymentSchedule {
	public BatchPaymentScheduleClp() {
	}

	public Class<?> getModelClass() {
		return BatchPaymentSchedule.class;
	}

	public String getModelClassName() {
		return BatchPaymentSchedule.class.getName();
	}

	public long getPrimaryKey() {
		return _batchPaymentScheduleId;
	}

	public void setPrimaryKey(long primaryKey) {
		setBatchPaymentScheduleId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_batchPaymentScheduleId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("batchPaymentScheduleId", getBatchPaymentScheduleId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("batchId", getBatchId());
		attributes.put("scheduleDate", getScheduleDate());
		attributes.put("feeTypeId", getFeeTypeId());
		attributes.put("amount", getAmount());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long batchPaymentScheduleId = (Long)attributes.get(
				"batchPaymentScheduleId");

		if (batchPaymentScheduleId != null) {
			setBatchPaymentScheduleId(batchPaymentScheduleId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Date scheduleDate = (Date)attributes.get("scheduleDate");

		if (scheduleDate != null) {
			setScheduleDate(scheduleDate);
		}

		Long feeTypeId = (Long)attributes.get("feeTypeId");

		if (feeTypeId != null) {
			setFeeTypeId(feeTypeId);
		}

		Double amount = (Double)attributes.get("amount");

		if (amount != null) {
			setAmount(amount);
		}
	}

	public long getBatchPaymentScheduleId() {
		return _batchPaymentScheduleId;
	}

	public void setBatchPaymentScheduleId(long batchPaymentScheduleId) {
		_batchPaymentScheduleId = batchPaymentScheduleId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public Date getScheduleDate() {
		return _scheduleDate;
	}

	public void setScheduleDate(Date scheduleDate) {
		_scheduleDate = scheduleDate;
	}

	public long getFeeTypeId() {
		return _feeTypeId;
	}

	public void setFeeTypeId(long feeTypeId) {
		_feeTypeId = feeTypeId;
	}

	public double getAmount() {
		return _amount;
	}

	public void setAmount(double amount) {
		_amount = amount;
	}

	public BaseModel<?> getBatchPaymentScheduleRemoteModel() {
		return _batchPaymentScheduleRemoteModel;
	}

	public void setBatchPaymentScheduleRemoteModel(
		BaseModel<?> batchPaymentScheduleRemoteModel) {
		_batchPaymentScheduleRemoteModel = batchPaymentScheduleRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			BatchPaymentScheduleLocalServiceUtil.addBatchPaymentSchedule(this);
		}
		else {
			BatchPaymentScheduleLocalServiceUtil.updateBatchPaymentSchedule(this);
		}
	}

	@Override
	public BatchPaymentSchedule toEscapedModel() {
		return (BatchPaymentSchedule)Proxy.newProxyInstance(BatchPaymentSchedule.class.getClassLoader(),
			new Class[] { BatchPaymentSchedule.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		BatchPaymentScheduleClp clone = new BatchPaymentScheduleClp();

		clone.setBatchPaymentScheduleId(getBatchPaymentScheduleId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setBatchId(getBatchId());
		clone.setScheduleDate(getScheduleDate());
		clone.setFeeTypeId(getFeeTypeId());
		clone.setAmount(getAmount());

		return clone;
	}

	public int compareTo(BatchPaymentSchedule batchPaymentSchedule) {
		long primaryKey = batchPaymentSchedule.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		BatchPaymentScheduleClp batchPaymentSchedule = null;

		try {
			batchPaymentSchedule = (BatchPaymentScheduleClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = batchPaymentSchedule.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{batchPaymentScheduleId=");
		sb.append(getBatchPaymentScheduleId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", batchId=");
		sb.append(getBatchId());
		sb.append(", scheduleDate=");
		sb.append(getScheduleDate());
		sb.append(", feeTypeId=");
		sb.append(getFeeTypeId());
		sb.append(", amount=");
		sb.append(getAmount());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(31);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.BatchPaymentSchedule");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>batchPaymentScheduleId</column-name><column-value><![CDATA[");
		sb.append(getBatchPaymentScheduleId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>batchId</column-name><column-value><![CDATA[");
		sb.append(getBatchId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>scheduleDate</column-name><column-value><![CDATA[");
		sb.append(getScheduleDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>feeTypeId</column-name><column-value><![CDATA[");
		sb.append(getFeeTypeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>amount</column-name><column-value><![CDATA[");
		sb.append(getAmount());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _batchPaymentScheduleId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private Date _createDate;
	private Date _modifiedDate;
	private long _batchId;
	private Date _scheduleDate;
	private long _feeTypeId;
	private double _amount;
	private BaseModel<?> _batchPaymentScheduleRemoteModel;
}
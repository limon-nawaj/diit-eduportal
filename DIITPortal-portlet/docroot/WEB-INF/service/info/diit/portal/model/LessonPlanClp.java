/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.service.LessonPlanLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class LessonPlanClp extends BaseModelImpl<LessonPlan>
	implements LessonPlan {
	public LessonPlanClp() {
	}

	public Class<?> getModelClass() {
		return LessonPlan.class;
	}

	public String getModelClassName() {
		return LessonPlan.class.getName();
	}

	public long getPrimaryKey() {
		return _lessonPlanId;
	}

	public void setPrimaryKey(long primaryKey) {
		setLessonPlanId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_lessonPlanId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("lessonPlanId", getLessonPlanId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("title", getTitle());
		attributes.put("organization", getOrganization());
		attributes.put("subject", getSubject());
		attributes.put("planDate", getPlanDate());
		attributes.put("totalClass", getTotalClass());
		attributes.put("totalDuration", getTotalDuration());
		attributes.put("objective", getObjective());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long lessonPlanId = (Long)attributes.get("lessonPlanId");

		if (lessonPlanId != null) {
			setLessonPlanId(lessonPlanId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		Long organization = (Long)attributes.get("organization");

		if (organization != null) {
			setOrganization(organization);
		}

		Long subject = (Long)attributes.get("subject");

		if (subject != null) {
			setSubject(subject);
		}

		Date planDate = (Date)attributes.get("planDate");

		if (planDate != null) {
			setPlanDate(planDate);
		}

		Long totalClass = (Long)attributes.get("totalClass");

		if (totalClass != null) {
			setTotalClass(totalClass);
		}

		Long totalDuration = (Long)attributes.get("totalDuration");

		if (totalDuration != null) {
			setTotalDuration(totalDuration);
		}

		String objective = (String)attributes.get("objective");

		if (objective != null) {
			setObjective(objective);
		}
	}

	public long getLessonPlanId() {
		return _lessonPlanId;
	}

	public void setLessonPlanId(long lessonPlanId) {
		_lessonPlanId = lessonPlanId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public long getOrganization() {
		return _organization;
	}

	public void setOrganization(long organization) {
		_organization = organization;
	}

	public long getSubject() {
		return _subject;
	}

	public void setSubject(long subject) {
		_subject = subject;
	}

	public Date getPlanDate() {
		return _planDate;
	}

	public void setPlanDate(Date planDate) {
		_planDate = planDate;
	}

	public long getTotalClass() {
		return _totalClass;
	}

	public void setTotalClass(long totalClass) {
		_totalClass = totalClass;
	}

	public long getTotalDuration() {
		return _totalDuration;
	}

	public void setTotalDuration(long totalDuration) {
		_totalDuration = totalDuration;
	}

	public String getObjective() {
		return _objective;
	}

	public void setObjective(String objective) {
		_objective = objective;
	}

	public BaseModel<?> getLessonPlanRemoteModel() {
		return _lessonPlanRemoteModel;
	}

	public void setLessonPlanRemoteModel(BaseModel<?> lessonPlanRemoteModel) {
		_lessonPlanRemoteModel = lessonPlanRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			LessonPlanLocalServiceUtil.addLessonPlan(this);
		}
		else {
			LessonPlanLocalServiceUtil.updateLessonPlan(this);
		}
	}

	@Override
	public LessonPlan toEscapedModel() {
		return (LessonPlan)Proxy.newProxyInstance(LessonPlan.class.getClassLoader(),
			new Class[] { LessonPlan.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		LessonPlanClp clone = new LessonPlanClp();

		clone.setLessonPlanId(getLessonPlanId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setTitle(getTitle());
		clone.setOrganization(getOrganization());
		clone.setSubject(getSubject());
		clone.setPlanDate(getPlanDate());
		clone.setTotalClass(getTotalClass());
		clone.setTotalDuration(getTotalDuration());
		clone.setObjective(getObjective());

		return clone;
	}

	public int compareTo(LessonPlan lessonPlan) {
		long primaryKey = lessonPlan.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		LessonPlanClp lessonPlan = null;

		try {
			lessonPlan = (LessonPlanClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = lessonPlan.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{lessonPlanId=");
		sb.append(getLessonPlanId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", title=");
		sb.append(getTitle());
		sb.append(", organization=");
		sb.append(getOrganization());
		sb.append(", subject=");
		sb.append(getSubject());
		sb.append(", planDate=");
		sb.append(getPlanDate());
		sb.append(", totalClass=");
		sb.append(getTotalClass());
		sb.append(", totalDuration=");
		sb.append(getTotalDuration());
		sb.append(", objective=");
		sb.append(getObjective());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(43);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.LessonPlan");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>lessonPlanId</column-name><column-value><![CDATA[");
		sb.append(getLessonPlanId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>title</column-name><column-value><![CDATA[");
		sb.append(getTitle());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organization</column-name><column-value><![CDATA[");
		sb.append(getOrganization());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>subject</column-name><column-value><![CDATA[");
		sb.append(getSubject());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>planDate</column-name><column-value><![CDATA[");
		sb.append(getPlanDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>totalClass</column-name><column-value><![CDATA[");
		sb.append(getTotalClass());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>totalDuration</column-name><column-value><![CDATA[");
		sb.append(getTotalDuration());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>objective</column-name><column-value><![CDATA[");
		sb.append(getObjective());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _lessonPlanId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _title;
	private long _organization;
	private long _subject;
	private Date _planDate;
	private long _totalClass;
	private long _totalDuration;
	private String _objective;
	private BaseModel<?> _lessonPlanRemoteModel;
}
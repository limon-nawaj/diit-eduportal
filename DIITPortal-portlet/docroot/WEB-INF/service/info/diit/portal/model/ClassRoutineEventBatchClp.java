/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.service.ClassRoutineEventBatchLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class ClassRoutineEventBatchClp extends BaseModelImpl<ClassRoutineEventBatch>
	implements ClassRoutineEventBatch {
	public ClassRoutineEventBatchClp() {
	}

	public Class<?> getModelClass() {
		return ClassRoutineEventBatch.class;
	}

	public String getModelClassName() {
		return ClassRoutineEventBatch.class.getName();
	}

	public long getPrimaryKey() {
		return _classRoutineEventBatchId;
	}

	public void setPrimaryKey(long primaryKey) {
		setClassRoutineEventBatchId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_classRoutineEventBatchId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("classRoutineEventBatchId", getClassRoutineEventBatchId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("classRoutineEventId", getClassRoutineEventId());
		attributes.put("batchId", getBatchId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long classRoutineEventBatchId = (Long)attributes.get(
				"classRoutineEventBatchId");

		if (classRoutineEventBatchId != null) {
			setClassRoutineEventBatchId(classRoutineEventBatchId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long classRoutineEventId = (Long)attributes.get("classRoutineEventId");

		if (classRoutineEventId != null) {
			setClassRoutineEventId(classRoutineEventId);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}
	}

	public long getClassRoutineEventBatchId() {
		return _classRoutineEventBatchId;
	}

	public void setClassRoutineEventBatchId(long classRoutineEventBatchId) {
		_classRoutineEventBatchId = classRoutineEventBatchId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getClassRoutineEventId() {
		return _classRoutineEventId;
	}

	public void setClassRoutineEventId(long classRoutineEventId) {
		_classRoutineEventId = classRoutineEventId;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public BaseModel<?> getClassRoutineEventBatchRemoteModel() {
		return _classRoutineEventBatchRemoteModel;
	}

	public void setClassRoutineEventBatchRemoteModel(
		BaseModel<?> classRoutineEventBatchRemoteModel) {
		_classRoutineEventBatchRemoteModel = classRoutineEventBatchRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			ClassRoutineEventBatchLocalServiceUtil.addClassRoutineEventBatch(this);
		}
		else {
			ClassRoutineEventBatchLocalServiceUtil.updateClassRoutineEventBatch(this);
		}
	}

	@Override
	public ClassRoutineEventBatch toEscapedModel() {
		return (ClassRoutineEventBatch)Proxy.newProxyInstance(ClassRoutineEventBatch.class.getClassLoader(),
			new Class[] { ClassRoutineEventBatch.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ClassRoutineEventBatchClp clone = new ClassRoutineEventBatchClp();

		clone.setClassRoutineEventBatchId(getClassRoutineEventBatchId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setOrganizationId(getOrganizationId());
		clone.setClassRoutineEventId(getClassRoutineEventId());
		clone.setBatchId(getBatchId());

		return clone;
	}

	public int compareTo(ClassRoutineEventBatch classRoutineEventBatch) {
		long primaryKey = classRoutineEventBatch.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		ClassRoutineEventBatchClp classRoutineEventBatch = null;

		try {
			classRoutineEventBatch = (ClassRoutineEventBatchClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = classRoutineEventBatch.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{classRoutineEventBatchId=");
		sb.append(getClassRoutineEventBatchId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", classRoutineEventId=");
		sb.append(getClassRoutineEventId());
		sb.append(", batchId=");
		sb.append(getBatchId());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(31);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.ClassRoutineEventBatch");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>classRoutineEventBatchId</column-name><column-value><![CDATA[");
		sb.append(getClassRoutineEventBatchId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>classRoutineEventId</column-name><column-value><![CDATA[");
		sb.append(getClassRoutineEventId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>batchId</column-name><column-value><![CDATA[");
		sb.append(getBatchId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _classRoutineEventBatchId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private long _classRoutineEventId;
	private long _batchId;
	private BaseModel<?> _classRoutineEventBatchRemoteModel;
}
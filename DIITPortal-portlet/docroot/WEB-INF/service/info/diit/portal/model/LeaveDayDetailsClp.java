/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import info.diit.portal.service.LeaveDayDetailsLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class LeaveDayDetailsClp extends BaseModelImpl<LeaveDayDetails>
	implements LeaveDayDetails {
	public LeaveDayDetailsClp() {
	}

	public Class<?> getModelClass() {
		return LeaveDayDetails.class;
	}

	public String getModelClassName() {
		return LeaveDayDetails.class.getName();
	}

	public long getPrimaryKey() {
		return _leaveDayDetailsId;
	}

	public void setPrimaryKey(long primaryKey) {
		setLeaveDayDetailsId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_leaveDayDetailsId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("leaveDayDetailsId", getLeaveDayDetailsId());
		attributes.put("leaveId", getLeaveId());
		attributes.put("leaveDate", getLeaveDate());
		attributes.put("day", getDay());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long leaveDayDetailsId = (Long)attributes.get("leaveDayDetailsId");

		if (leaveDayDetailsId != null) {
			setLeaveDayDetailsId(leaveDayDetailsId);
		}

		Long leaveId = (Long)attributes.get("leaveId");

		if (leaveId != null) {
			setLeaveId(leaveId);
		}

		Date leaveDate = (Date)attributes.get("leaveDate");

		if (leaveDate != null) {
			setLeaveDate(leaveDate);
		}

		Double day = (Double)attributes.get("day");

		if (day != null) {
			setDay(day);
		}
	}

	public long getLeaveDayDetailsId() {
		return _leaveDayDetailsId;
	}

	public void setLeaveDayDetailsId(long leaveDayDetailsId) {
		_leaveDayDetailsId = leaveDayDetailsId;
	}

	public long getLeaveId() {
		return _leaveId;
	}

	public void setLeaveId(long leaveId) {
		_leaveId = leaveId;
	}

	public Date getLeaveDate() {
		return _leaveDate;
	}

	public void setLeaveDate(Date leaveDate) {
		_leaveDate = leaveDate;
	}

	public double getDay() {
		return _day;
	}

	public void setDay(double day) {
		_day = day;
	}

	public BaseModel<?> getLeaveDayDetailsRemoteModel() {
		return _leaveDayDetailsRemoteModel;
	}

	public void setLeaveDayDetailsRemoteModel(
		BaseModel<?> leaveDayDetailsRemoteModel) {
		_leaveDayDetailsRemoteModel = leaveDayDetailsRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			LeaveDayDetailsLocalServiceUtil.addLeaveDayDetails(this);
		}
		else {
			LeaveDayDetailsLocalServiceUtil.updateLeaveDayDetails(this);
		}
	}

	@Override
	public LeaveDayDetails toEscapedModel() {
		return (LeaveDayDetails)Proxy.newProxyInstance(LeaveDayDetails.class.getClassLoader(),
			new Class[] { LeaveDayDetails.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		LeaveDayDetailsClp clone = new LeaveDayDetailsClp();

		clone.setLeaveDayDetailsId(getLeaveDayDetailsId());
		clone.setLeaveId(getLeaveId());
		clone.setLeaveDate(getLeaveDate());
		clone.setDay(getDay());

		return clone;
	}

	public int compareTo(LeaveDayDetails leaveDayDetails) {
		long primaryKey = leaveDayDetails.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		LeaveDayDetailsClp leaveDayDetails = null;

		try {
			leaveDayDetails = (LeaveDayDetailsClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = leaveDayDetails.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{leaveDayDetailsId=");
		sb.append(getLeaveDayDetailsId());
		sb.append(", leaveId=");
		sb.append(getLeaveId());
		sb.append(", leaveDate=");
		sb.append(getLeaveDate());
		sb.append(", day=");
		sb.append(getDay());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.LeaveDayDetails");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>leaveDayDetailsId</column-name><column-value><![CDATA[");
		sb.append(getLeaveDayDetailsId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>leaveId</column-name><column-value><![CDATA[");
		sb.append(getLeaveId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>leaveDate</column-name><column-value><![CDATA[");
		sb.append(getLeaveDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>day</column-name><column-value><![CDATA[");
		sb.append(getDay());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _leaveDayDetailsId;
	private long _leaveId;
	private Date _leaveDate;
	private double _day;
	private BaseModel<?> _leaveDayDetailsRemoteModel;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class AssessmentStudentSoap implements Serializable {
	public static AssessmentStudentSoap toSoapModel(AssessmentStudent model) {
		AssessmentStudentSoap soapModel = new AssessmentStudentSoap();

		soapModel.setAssessmentStudentId(model.getAssessmentStudentId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setObtainMark(model.getObtainMark());
		soapModel.setAssessmentId(model.getAssessmentId());
		soapModel.setStudentId(model.getStudentId());
		soapModel.setStatus(model.getStatus());

		return soapModel;
	}

	public static AssessmentStudentSoap[] toSoapModels(
		AssessmentStudent[] models) {
		AssessmentStudentSoap[] soapModels = new AssessmentStudentSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AssessmentStudentSoap[][] toSoapModels(
		AssessmentStudent[][] models) {
		AssessmentStudentSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AssessmentStudentSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AssessmentStudentSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AssessmentStudentSoap[] toSoapModels(
		List<AssessmentStudent> models) {
		List<AssessmentStudentSoap> soapModels = new ArrayList<AssessmentStudentSoap>(models.size());

		for (AssessmentStudent model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AssessmentStudentSoap[soapModels.size()]);
	}

	public AssessmentStudentSoap() {
	}

	public long getPrimaryKey() {
		return _assessmentStudentId;
	}

	public void setPrimaryKey(long pk) {
		setAssessmentStudentId(pk);
	}

	public long getAssessmentStudentId() {
		return _assessmentStudentId;
	}

	public void setAssessmentStudentId(long assessmentStudentId) {
		_assessmentStudentId = assessmentStudentId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public double getObtainMark() {
		return _obtainMark;
	}

	public void setObtainMark(double obtainMark) {
		_obtainMark = obtainMark;
	}

	public long getAssessmentId() {
		return _assessmentId;
	}

	public void setAssessmentId(long assessmentId) {
		_assessmentId = assessmentId;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	public long getStatus() {
		return _status;
	}

	public void setStatus(long status) {
		_status = status;
	}

	private long _assessmentStudentId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private double _obtainMark;
	private long _assessmentId;
	private long _studentId;
	private long _status;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link LeaveDayDetails}.
 * </p>
 *
 * @author    mohammad
 * @see       LeaveDayDetails
 * @generated
 */
public class LeaveDayDetailsWrapper implements LeaveDayDetails,
	ModelWrapper<LeaveDayDetails> {
	public LeaveDayDetailsWrapper(LeaveDayDetails leaveDayDetails) {
		_leaveDayDetails = leaveDayDetails;
	}

	public Class<?> getModelClass() {
		return LeaveDayDetails.class;
	}

	public String getModelClassName() {
		return LeaveDayDetails.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("leaveDayDetailsId", getLeaveDayDetailsId());
		attributes.put("leaveId", getLeaveId());
		attributes.put("leaveDate", getLeaveDate());
		attributes.put("day", getDay());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long leaveDayDetailsId = (Long)attributes.get("leaveDayDetailsId");

		if (leaveDayDetailsId != null) {
			setLeaveDayDetailsId(leaveDayDetailsId);
		}

		Long leaveId = (Long)attributes.get("leaveId");

		if (leaveId != null) {
			setLeaveId(leaveId);
		}

		Date leaveDate = (Date)attributes.get("leaveDate");

		if (leaveDate != null) {
			setLeaveDate(leaveDate);
		}

		Double day = (Double)attributes.get("day");

		if (day != null) {
			setDay(day);
		}
	}

	/**
	* Returns the primary key of this leave day details.
	*
	* @return the primary key of this leave day details
	*/
	public long getPrimaryKey() {
		return _leaveDayDetails.getPrimaryKey();
	}

	/**
	* Sets the primary key of this leave day details.
	*
	* @param primaryKey the primary key of this leave day details
	*/
	public void setPrimaryKey(long primaryKey) {
		_leaveDayDetails.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the leave day details ID of this leave day details.
	*
	* @return the leave day details ID of this leave day details
	*/
	public long getLeaveDayDetailsId() {
		return _leaveDayDetails.getLeaveDayDetailsId();
	}

	/**
	* Sets the leave day details ID of this leave day details.
	*
	* @param leaveDayDetailsId the leave day details ID of this leave day details
	*/
	public void setLeaveDayDetailsId(long leaveDayDetailsId) {
		_leaveDayDetails.setLeaveDayDetailsId(leaveDayDetailsId);
	}

	/**
	* Returns the leave ID of this leave day details.
	*
	* @return the leave ID of this leave day details
	*/
	public long getLeaveId() {
		return _leaveDayDetails.getLeaveId();
	}

	/**
	* Sets the leave ID of this leave day details.
	*
	* @param leaveId the leave ID of this leave day details
	*/
	public void setLeaveId(long leaveId) {
		_leaveDayDetails.setLeaveId(leaveId);
	}

	/**
	* Returns the leave date of this leave day details.
	*
	* @return the leave date of this leave day details
	*/
	public java.util.Date getLeaveDate() {
		return _leaveDayDetails.getLeaveDate();
	}

	/**
	* Sets the leave date of this leave day details.
	*
	* @param leaveDate the leave date of this leave day details
	*/
	public void setLeaveDate(java.util.Date leaveDate) {
		_leaveDayDetails.setLeaveDate(leaveDate);
	}

	/**
	* Returns the day of this leave day details.
	*
	* @return the day of this leave day details
	*/
	public double getDay() {
		return _leaveDayDetails.getDay();
	}

	/**
	* Sets the day of this leave day details.
	*
	* @param day the day of this leave day details
	*/
	public void setDay(double day) {
		_leaveDayDetails.setDay(day);
	}

	public boolean isNew() {
		return _leaveDayDetails.isNew();
	}

	public void setNew(boolean n) {
		_leaveDayDetails.setNew(n);
	}

	public boolean isCachedModel() {
		return _leaveDayDetails.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_leaveDayDetails.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _leaveDayDetails.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _leaveDayDetails.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_leaveDayDetails.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _leaveDayDetails.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_leaveDayDetails.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LeaveDayDetailsWrapper((LeaveDayDetails)_leaveDayDetails.clone());
	}

	public int compareTo(info.diit.portal.model.LeaveDayDetails leaveDayDetails) {
		return _leaveDayDetails.compareTo(leaveDayDetails);
	}

	@Override
	public int hashCode() {
		return _leaveDayDetails.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.LeaveDayDetails> toCacheModel() {
		return _leaveDayDetails.toCacheModel();
	}

	public info.diit.portal.model.LeaveDayDetails toEscapedModel() {
		return new LeaveDayDetailsWrapper(_leaveDayDetails.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _leaveDayDetails.toString();
	}

	public java.lang.String toXmlString() {
		return _leaveDayDetails.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_leaveDayDetails.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public LeaveDayDetails getWrappedLeaveDayDetails() {
		return _leaveDayDetails;
	}

	public LeaveDayDetails getWrappedModel() {
		return _leaveDayDetails;
	}

	public void resetOriginalValues() {
		_leaveDayDetails.resetOriginalValues();
	}

	private LeaveDayDetails _leaveDayDetails;
}
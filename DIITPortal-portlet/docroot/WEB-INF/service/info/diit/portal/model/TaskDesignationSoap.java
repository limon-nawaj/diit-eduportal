/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class TaskDesignationSoap implements Serializable {
	public static TaskDesignationSoap toSoapModel(TaskDesignation model) {
		TaskDesignationSoap soapModel = new TaskDesignationSoap();

		soapModel.setTaskDesignationId(model.getTaskDesignationId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setTaskId(model.getTaskId());
		soapModel.setDesignationId(model.getDesignationId());

		return soapModel;
	}

	public static TaskDesignationSoap[] toSoapModels(TaskDesignation[] models) {
		TaskDesignationSoap[] soapModels = new TaskDesignationSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TaskDesignationSoap[][] toSoapModels(
		TaskDesignation[][] models) {
		TaskDesignationSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TaskDesignationSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TaskDesignationSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TaskDesignationSoap[] toSoapModels(
		List<TaskDesignation> models) {
		List<TaskDesignationSoap> soapModels = new ArrayList<TaskDesignationSoap>(models.size());

		for (TaskDesignation model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TaskDesignationSoap[soapModels.size()]);
	}

	public TaskDesignationSoap() {
	}

	public long getPrimaryKey() {
		return _taskDesignationId;
	}

	public void setPrimaryKey(long pk) {
		setTaskDesignationId(pk);
	}

	public long getTaskDesignationId() {
		return _taskDesignationId;
	}

	public void setTaskDesignationId(long taskDesignationId) {
		_taskDesignationId = taskDesignationId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getTaskId() {
		return _taskId;
	}

	public void setTaskId(long taskId) {
		_taskId = taskId;
	}

	public long getDesignationId() {
		return _designationId;
	}

	public void setDesignationId(long designationId) {
		_designationId = designationId;
	}

	private long _taskDesignationId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _taskId;
	private long _designationId;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class CommunicationStudentRecordSoap implements Serializable {
	public static CommunicationStudentRecordSoap toSoapModel(
		CommunicationStudentRecord model) {
		CommunicationStudentRecordSoap soapModel = new CommunicationStudentRecordSoap();

		soapModel.setCommunicationStudentRecorId(model.getCommunicationStudentRecorId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setCommunicationBy(model.getCommunicationBy());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCommunicationRecordId(model.getCommunicationRecordId());
		soapModel.setStudentId(model.getStudentId());

		return soapModel;
	}

	public static CommunicationStudentRecordSoap[] toSoapModels(
		CommunicationStudentRecord[] models) {
		CommunicationStudentRecordSoap[] soapModels = new CommunicationStudentRecordSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CommunicationStudentRecordSoap[][] toSoapModels(
		CommunicationStudentRecord[][] models) {
		CommunicationStudentRecordSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CommunicationStudentRecordSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CommunicationStudentRecordSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CommunicationStudentRecordSoap[] toSoapModels(
		List<CommunicationStudentRecord> models) {
		List<CommunicationStudentRecordSoap> soapModels = new ArrayList<CommunicationStudentRecordSoap>(models.size());

		for (CommunicationStudentRecord model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CommunicationStudentRecordSoap[soapModels.size()]);
	}

	public CommunicationStudentRecordSoap() {
	}

	public long getPrimaryKey() {
		return _CommunicationStudentRecorId;
	}

	public void setPrimaryKey(long pk) {
		setCommunicationStudentRecorId(pk);
	}

	public long getCommunicationStudentRecorId() {
		return _CommunicationStudentRecorId;
	}

	public void setCommunicationStudentRecorId(long CommunicationStudentRecorId) {
		_CommunicationStudentRecorId = CommunicationStudentRecorId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getCommunicationBy() {
		return _communicationBy;
	}

	public void setCommunicationBy(long communicationBy) {
		_communicationBy = communicationBy;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getCommunicationRecordId() {
		return _communicationRecordId;
	}

	public void setCommunicationRecordId(long communicationRecordId) {
		_communicationRecordId = communicationRecordId;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	private long _CommunicationStudentRecorId;
	private long _companyId;
	private long _organizationId;
	private long _communicationBy;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _communicationRecordId;
	private long _studentId;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Designation}.
 * </p>
 *
 * @author    mohammad
 * @see       Designation
 * @generated
 */
public class DesignationWrapper implements Designation,
	ModelWrapper<Designation> {
	public DesignationWrapper(Designation designation) {
		_designation = designation;
	}

	public Class<?> getModelClass() {
		return Designation.class;
	}

	public String getModelClassName() {
		return Designation.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("designationId", getDesignationId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("designationTitle", getDesignationTitle());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long designationId = (Long)attributes.get("designationId");

		if (designationId != null) {
			setDesignationId(designationId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String designationTitle = (String)attributes.get("designationTitle");

		if (designationTitle != null) {
			setDesignationTitle(designationTitle);
		}
	}

	/**
	* Returns the primary key of this designation.
	*
	* @return the primary key of this designation
	*/
	public long getPrimaryKey() {
		return _designation.getPrimaryKey();
	}

	/**
	* Sets the primary key of this designation.
	*
	* @param primaryKey the primary key of this designation
	*/
	public void setPrimaryKey(long primaryKey) {
		_designation.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the designation ID of this designation.
	*
	* @return the designation ID of this designation
	*/
	public long getDesignationId() {
		return _designation.getDesignationId();
	}

	/**
	* Sets the designation ID of this designation.
	*
	* @param designationId the designation ID of this designation
	*/
	public void setDesignationId(long designationId) {
		_designation.setDesignationId(designationId);
	}

	/**
	* Returns the company ID of this designation.
	*
	* @return the company ID of this designation
	*/
	public long getCompanyId() {
		return _designation.getCompanyId();
	}

	/**
	* Sets the company ID of this designation.
	*
	* @param companyId the company ID of this designation
	*/
	public void setCompanyId(long companyId) {
		_designation.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this designation.
	*
	* @return the user ID of this designation
	*/
	public long getUserId() {
		return _designation.getUserId();
	}

	/**
	* Sets the user ID of this designation.
	*
	* @param userId the user ID of this designation
	*/
	public void setUserId(long userId) {
		_designation.setUserId(userId);
	}

	/**
	* Returns the user uuid of this designation.
	*
	* @return the user uuid of this designation
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _designation.getUserUuid();
	}

	/**
	* Sets the user uuid of this designation.
	*
	* @param userUuid the user uuid of this designation
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_designation.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this designation.
	*
	* @return the user name of this designation
	*/
	public java.lang.String getUserName() {
		return _designation.getUserName();
	}

	/**
	* Sets the user name of this designation.
	*
	* @param userName the user name of this designation
	*/
	public void setUserName(java.lang.String userName) {
		_designation.setUserName(userName);
	}

	/**
	* Returns the create date of this designation.
	*
	* @return the create date of this designation
	*/
	public java.util.Date getCreateDate() {
		return _designation.getCreateDate();
	}

	/**
	* Sets the create date of this designation.
	*
	* @param createDate the create date of this designation
	*/
	public void setCreateDate(java.util.Date createDate) {
		_designation.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this designation.
	*
	* @return the modified date of this designation
	*/
	public java.util.Date getModifiedDate() {
		return _designation.getModifiedDate();
	}

	/**
	* Sets the modified date of this designation.
	*
	* @param modifiedDate the modified date of this designation
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_designation.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the designation title of this designation.
	*
	* @return the designation title of this designation
	*/
	public java.lang.String getDesignationTitle() {
		return _designation.getDesignationTitle();
	}

	/**
	* Sets the designation title of this designation.
	*
	* @param designationTitle the designation title of this designation
	*/
	public void setDesignationTitle(java.lang.String designationTitle) {
		_designation.setDesignationTitle(designationTitle);
	}

	public boolean isNew() {
		return _designation.isNew();
	}

	public void setNew(boolean n) {
		_designation.setNew(n);
	}

	public boolean isCachedModel() {
		return _designation.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_designation.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _designation.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _designation.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_designation.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _designation.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_designation.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new DesignationWrapper((Designation)_designation.clone());
	}

	public int compareTo(info.diit.portal.model.Designation designation) {
		return _designation.compareTo(designation);
	}

	@Override
	public int hashCode() {
		return _designation.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.Designation> toCacheModel() {
		return _designation.toCacheModel();
	}

	public info.diit.portal.model.Designation toEscapedModel() {
		return new DesignationWrapper(_designation.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _designation.toString();
	}

	public java.lang.String toXmlString() {
		return _designation.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_designation.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Designation getWrappedDesignation() {
		return _designation;
	}

	public Designation getWrappedModel() {
		return _designation;
	}

	public void resetOriginalValues() {
		_designation.resetOriginalValues();
	}

	private Designation _designation;
}
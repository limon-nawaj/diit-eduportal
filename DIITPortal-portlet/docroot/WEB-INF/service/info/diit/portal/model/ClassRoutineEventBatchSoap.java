/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class ClassRoutineEventBatchSoap implements Serializable {
	public static ClassRoutineEventBatchSoap toSoapModel(
		ClassRoutineEventBatch model) {
		ClassRoutineEventBatchSoap soapModel = new ClassRoutineEventBatchSoap();

		soapModel.setClassRoutineEventBatchId(model.getClassRoutineEventBatchId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setClassRoutineEventId(model.getClassRoutineEventId());
		soapModel.setBatchId(model.getBatchId());

		return soapModel;
	}

	public static ClassRoutineEventBatchSoap[] toSoapModels(
		ClassRoutineEventBatch[] models) {
		ClassRoutineEventBatchSoap[] soapModels = new ClassRoutineEventBatchSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ClassRoutineEventBatchSoap[][] toSoapModels(
		ClassRoutineEventBatch[][] models) {
		ClassRoutineEventBatchSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ClassRoutineEventBatchSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ClassRoutineEventBatchSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ClassRoutineEventBatchSoap[] toSoapModels(
		List<ClassRoutineEventBatch> models) {
		List<ClassRoutineEventBatchSoap> soapModels = new ArrayList<ClassRoutineEventBatchSoap>(models.size());

		for (ClassRoutineEventBatch model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ClassRoutineEventBatchSoap[soapModels.size()]);
	}

	public ClassRoutineEventBatchSoap() {
	}

	public long getPrimaryKey() {
		return _classRoutineEventBatchId;
	}

	public void setPrimaryKey(long pk) {
		setClassRoutineEventBatchId(pk);
	}

	public long getClassRoutineEventBatchId() {
		return _classRoutineEventBatchId;
	}

	public void setClassRoutineEventBatchId(long classRoutineEventBatchId) {
		_classRoutineEventBatchId = classRoutineEventBatchId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getClassRoutineEventId() {
		return _classRoutineEventId;
	}

	public void setClassRoutineEventId(long classRoutineEventId) {
		_classRoutineEventId = classRoutineEventId;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	private long _classRoutineEventBatchId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private long _classRoutineEventId;
	private long _batchId;
}
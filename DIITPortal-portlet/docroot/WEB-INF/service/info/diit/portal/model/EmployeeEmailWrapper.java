/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link EmployeeEmail}.
 * </p>
 *
 * @author    mohammad
 * @see       EmployeeEmail
 * @generated
 */
public class EmployeeEmailWrapper implements EmployeeEmail,
	ModelWrapper<EmployeeEmail> {
	public EmployeeEmailWrapper(EmployeeEmail employeeEmail) {
		_employeeEmail = employeeEmail;
	}

	public Class<?> getModelClass() {
		return EmployeeEmail.class;
	}

	public String getModelClassName() {
		return EmployeeEmail.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("emailId", getEmailId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("employeeId", getEmployeeId());
		attributes.put("workEmail", getWorkEmail());
		attributes.put("personalEmail", getPersonalEmail());
		attributes.put("status", getStatus());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long emailId = (Long)attributes.get("emailId");

		if (emailId != null) {
			setEmailId(emailId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long employeeId = (Long)attributes.get("employeeId");

		if (employeeId != null) {
			setEmployeeId(employeeId);
		}

		String workEmail = (String)attributes.get("workEmail");

		if (workEmail != null) {
			setWorkEmail(workEmail);
		}

		String personalEmail = (String)attributes.get("personalEmail");

		if (personalEmail != null) {
			setPersonalEmail(personalEmail);
		}

		Long status = (Long)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	/**
	* Returns the primary key of this employee email.
	*
	* @return the primary key of this employee email
	*/
	public long getPrimaryKey() {
		return _employeeEmail.getPrimaryKey();
	}

	/**
	* Sets the primary key of this employee email.
	*
	* @param primaryKey the primary key of this employee email
	*/
	public void setPrimaryKey(long primaryKey) {
		_employeeEmail.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the email ID of this employee email.
	*
	* @return the email ID of this employee email
	*/
	public long getEmailId() {
		return _employeeEmail.getEmailId();
	}

	/**
	* Sets the email ID of this employee email.
	*
	* @param emailId the email ID of this employee email
	*/
	public void setEmailId(long emailId) {
		_employeeEmail.setEmailId(emailId);
	}

	/**
	* Returns the company ID of this employee email.
	*
	* @return the company ID of this employee email
	*/
	public long getCompanyId() {
		return _employeeEmail.getCompanyId();
	}

	/**
	* Sets the company ID of this employee email.
	*
	* @param companyId the company ID of this employee email
	*/
	public void setCompanyId(long companyId) {
		_employeeEmail.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this employee email.
	*
	* @return the user ID of this employee email
	*/
	public long getUserId() {
		return _employeeEmail.getUserId();
	}

	/**
	* Sets the user ID of this employee email.
	*
	* @param userId the user ID of this employee email
	*/
	public void setUserId(long userId) {
		_employeeEmail.setUserId(userId);
	}

	/**
	* Returns the user uuid of this employee email.
	*
	* @return the user uuid of this employee email
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeEmail.getUserUuid();
	}

	/**
	* Sets the user uuid of this employee email.
	*
	* @param userUuid the user uuid of this employee email
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_employeeEmail.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this employee email.
	*
	* @return the user name of this employee email
	*/
	public java.lang.String getUserName() {
		return _employeeEmail.getUserName();
	}

	/**
	* Sets the user name of this employee email.
	*
	* @param userName the user name of this employee email
	*/
	public void setUserName(java.lang.String userName) {
		_employeeEmail.setUserName(userName);
	}

	/**
	* Returns the create date of this employee email.
	*
	* @return the create date of this employee email
	*/
	public java.util.Date getCreateDate() {
		return _employeeEmail.getCreateDate();
	}

	/**
	* Sets the create date of this employee email.
	*
	* @param createDate the create date of this employee email
	*/
	public void setCreateDate(java.util.Date createDate) {
		_employeeEmail.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this employee email.
	*
	* @return the modified date of this employee email
	*/
	public java.util.Date getModifiedDate() {
		return _employeeEmail.getModifiedDate();
	}

	/**
	* Sets the modified date of this employee email.
	*
	* @param modifiedDate the modified date of this employee email
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_employeeEmail.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the employee ID of this employee email.
	*
	* @return the employee ID of this employee email
	*/
	public long getEmployeeId() {
		return _employeeEmail.getEmployeeId();
	}

	/**
	* Sets the employee ID of this employee email.
	*
	* @param employeeId the employee ID of this employee email
	*/
	public void setEmployeeId(long employeeId) {
		_employeeEmail.setEmployeeId(employeeId);
	}

	/**
	* Returns the work email of this employee email.
	*
	* @return the work email of this employee email
	*/
	public java.lang.String getWorkEmail() {
		return _employeeEmail.getWorkEmail();
	}

	/**
	* Sets the work email of this employee email.
	*
	* @param workEmail the work email of this employee email
	*/
	public void setWorkEmail(java.lang.String workEmail) {
		_employeeEmail.setWorkEmail(workEmail);
	}

	/**
	* Returns the personal email of this employee email.
	*
	* @return the personal email of this employee email
	*/
	public java.lang.String getPersonalEmail() {
		return _employeeEmail.getPersonalEmail();
	}

	/**
	* Sets the personal email of this employee email.
	*
	* @param personalEmail the personal email of this employee email
	*/
	public void setPersonalEmail(java.lang.String personalEmail) {
		_employeeEmail.setPersonalEmail(personalEmail);
	}

	/**
	* Returns the status of this employee email.
	*
	* @return the status of this employee email
	*/
	public long getStatus() {
		return _employeeEmail.getStatus();
	}

	/**
	* Sets the status of this employee email.
	*
	* @param status the status of this employee email
	*/
	public void setStatus(long status) {
		_employeeEmail.setStatus(status);
	}

	public boolean isNew() {
		return _employeeEmail.isNew();
	}

	public void setNew(boolean n) {
		_employeeEmail.setNew(n);
	}

	public boolean isCachedModel() {
		return _employeeEmail.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_employeeEmail.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _employeeEmail.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _employeeEmail.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_employeeEmail.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _employeeEmail.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_employeeEmail.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new EmployeeEmailWrapper((EmployeeEmail)_employeeEmail.clone());
	}

	public int compareTo(info.diit.portal.model.EmployeeEmail employeeEmail) {
		return _employeeEmail.compareTo(employeeEmail);
	}

	@Override
	public int hashCode() {
		return _employeeEmail.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.EmployeeEmail> toCacheModel() {
		return _employeeEmail.toCacheModel();
	}

	public info.diit.portal.model.EmployeeEmail toEscapedModel() {
		return new EmployeeEmailWrapper(_employeeEmail.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _employeeEmail.toString();
	}

	public java.lang.String toXmlString() {
		return _employeeEmail.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_employeeEmail.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public EmployeeEmail getWrappedEmployeeEmail() {
		return _employeeEmail;
	}

	public EmployeeEmail getWrappedModel() {
		return _employeeEmail;
	}

	public void resetOriginalValues() {
		_employeeEmail.resetOriginalValues();
	}

	private EmployeeEmail _employeeEmail;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link DailyWork}.
 * </p>
 *
 * @author    mohammad
 * @see       DailyWork
 * @generated
 */
public class DailyWorkWrapper implements DailyWork, ModelWrapper<DailyWork> {
	public DailyWorkWrapper(DailyWork dailyWork) {
		_dailyWork = dailyWork;
	}

	public Class<?> getModelClass() {
		return DailyWork.class;
	}

	public String getModelClassName() {
		return DailyWork.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("dailyWorkId", getDailyWorkId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("employeeId", getEmployeeId());
		attributes.put("worKingDay", getWorKingDay());
		attributes.put("taskId", getTaskId());
		attributes.put("time", getTime());
		attributes.put("note", getNote());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long dailyWorkId = (Long)attributes.get("dailyWorkId");

		if (dailyWorkId != null) {
			setDailyWorkId(dailyWorkId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long employeeId = (Long)attributes.get("employeeId");

		if (employeeId != null) {
			setEmployeeId(employeeId);
		}

		Date worKingDay = (Date)attributes.get("worKingDay");

		if (worKingDay != null) {
			setWorKingDay(worKingDay);
		}

		Long taskId = (Long)attributes.get("taskId");

		if (taskId != null) {
			setTaskId(taskId);
		}

		Integer time = (Integer)attributes.get("time");

		if (time != null) {
			setTime(time);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}
	}

	/**
	* Returns the primary key of this daily work.
	*
	* @return the primary key of this daily work
	*/
	public long getPrimaryKey() {
		return _dailyWork.getPrimaryKey();
	}

	/**
	* Sets the primary key of this daily work.
	*
	* @param primaryKey the primary key of this daily work
	*/
	public void setPrimaryKey(long primaryKey) {
		_dailyWork.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the daily work ID of this daily work.
	*
	* @return the daily work ID of this daily work
	*/
	public long getDailyWorkId() {
		return _dailyWork.getDailyWorkId();
	}

	/**
	* Sets the daily work ID of this daily work.
	*
	* @param dailyWorkId the daily work ID of this daily work
	*/
	public void setDailyWorkId(long dailyWorkId) {
		_dailyWork.setDailyWorkId(dailyWorkId);
	}

	/**
	* Returns the company ID of this daily work.
	*
	* @return the company ID of this daily work
	*/
	public long getCompanyId() {
		return _dailyWork.getCompanyId();
	}

	/**
	* Sets the company ID of this daily work.
	*
	* @param companyId the company ID of this daily work
	*/
	public void setCompanyId(long companyId) {
		_dailyWork.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this daily work.
	*
	* @return the user ID of this daily work
	*/
	public long getUserId() {
		return _dailyWork.getUserId();
	}

	/**
	* Sets the user ID of this daily work.
	*
	* @param userId the user ID of this daily work
	*/
	public void setUserId(long userId) {
		_dailyWork.setUserId(userId);
	}

	/**
	* Returns the user uuid of this daily work.
	*
	* @return the user uuid of this daily work
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _dailyWork.getUserUuid();
	}

	/**
	* Sets the user uuid of this daily work.
	*
	* @param userUuid the user uuid of this daily work
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_dailyWork.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this daily work.
	*
	* @return the user name of this daily work
	*/
	public java.lang.String getUserName() {
		return _dailyWork.getUserName();
	}

	/**
	* Sets the user name of this daily work.
	*
	* @param userName the user name of this daily work
	*/
	public void setUserName(java.lang.String userName) {
		_dailyWork.setUserName(userName);
	}

	/**
	* Returns the create date of this daily work.
	*
	* @return the create date of this daily work
	*/
	public java.util.Date getCreateDate() {
		return _dailyWork.getCreateDate();
	}

	/**
	* Sets the create date of this daily work.
	*
	* @param createDate the create date of this daily work
	*/
	public void setCreateDate(java.util.Date createDate) {
		_dailyWork.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this daily work.
	*
	* @return the modified date of this daily work
	*/
	public java.util.Date getModifiedDate() {
		return _dailyWork.getModifiedDate();
	}

	/**
	* Sets the modified date of this daily work.
	*
	* @param modifiedDate the modified date of this daily work
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_dailyWork.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the employee ID of this daily work.
	*
	* @return the employee ID of this daily work
	*/
	public long getEmployeeId() {
		return _dailyWork.getEmployeeId();
	}

	/**
	* Sets the employee ID of this daily work.
	*
	* @param employeeId the employee ID of this daily work
	*/
	public void setEmployeeId(long employeeId) {
		_dailyWork.setEmployeeId(employeeId);
	}

	/**
	* Returns the wor king day of this daily work.
	*
	* @return the wor king day of this daily work
	*/
	public java.util.Date getWorKingDay() {
		return _dailyWork.getWorKingDay();
	}

	/**
	* Sets the wor king day of this daily work.
	*
	* @param worKingDay the wor king day of this daily work
	*/
	public void setWorKingDay(java.util.Date worKingDay) {
		_dailyWork.setWorKingDay(worKingDay);
	}

	/**
	* Returns the task ID of this daily work.
	*
	* @return the task ID of this daily work
	*/
	public long getTaskId() {
		return _dailyWork.getTaskId();
	}

	/**
	* Sets the task ID of this daily work.
	*
	* @param taskId the task ID of this daily work
	*/
	public void setTaskId(long taskId) {
		_dailyWork.setTaskId(taskId);
	}

	/**
	* Returns the time of this daily work.
	*
	* @return the time of this daily work
	*/
	public int getTime() {
		return _dailyWork.getTime();
	}

	/**
	* Sets the time of this daily work.
	*
	* @param time the time of this daily work
	*/
	public void setTime(int time) {
		_dailyWork.setTime(time);
	}

	/**
	* Returns the note of this daily work.
	*
	* @return the note of this daily work
	*/
	public java.lang.String getNote() {
		return _dailyWork.getNote();
	}

	/**
	* Sets the note of this daily work.
	*
	* @param note the note of this daily work
	*/
	public void setNote(java.lang.String note) {
		_dailyWork.setNote(note);
	}

	public boolean isNew() {
		return _dailyWork.isNew();
	}

	public void setNew(boolean n) {
		_dailyWork.setNew(n);
	}

	public boolean isCachedModel() {
		return _dailyWork.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_dailyWork.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _dailyWork.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _dailyWork.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_dailyWork.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _dailyWork.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_dailyWork.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new DailyWorkWrapper((DailyWork)_dailyWork.clone());
	}

	public int compareTo(info.diit.portal.model.DailyWork dailyWork) {
		return _dailyWork.compareTo(dailyWork);
	}

	@Override
	public int hashCode() {
		return _dailyWork.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.DailyWork> toCacheModel() {
		return _dailyWork.toCacheModel();
	}

	public info.diit.portal.model.DailyWork toEscapedModel() {
		return new DailyWorkWrapper(_dailyWork.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _dailyWork.toString();
	}

	public java.lang.String toXmlString() {
		return _dailyWork.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_dailyWork.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public DailyWork getWrappedDailyWork() {
		return _dailyWork;
	}

	public DailyWork getWrappedModel() {
		return _dailyWork;
	}

	public void resetOriginalValues() {
		_dailyWork.resetOriginalValues();
	}

	private DailyWork _dailyWork;
}
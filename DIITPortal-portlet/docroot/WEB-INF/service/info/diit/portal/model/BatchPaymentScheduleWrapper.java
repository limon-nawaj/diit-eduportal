/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BatchPaymentSchedule}.
 * </p>
 *
 * @author    mohammad
 * @see       BatchPaymentSchedule
 * @generated
 */
public class BatchPaymentScheduleWrapper implements BatchPaymentSchedule,
	ModelWrapper<BatchPaymentSchedule> {
	public BatchPaymentScheduleWrapper(
		BatchPaymentSchedule batchPaymentSchedule) {
		_batchPaymentSchedule = batchPaymentSchedule;
	}

	public Class<?> getModelClass() {
		return BatchPaymentSchedule.class;
	}

	public String getModelClassName() {
		return BatchPaymentSchedule.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("batchPaymentScheduleId", getBatchPaymentScheduleId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("batchId", getBatchId());
		attributes.put("scheduleDate", getScheduleDate());
		attributes.put("feeTypeId", getFeeTypeId());
		attributes.put("amount", getAmount());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long batchPaymentScheduleId = (Long)attributes.get(
				"batchPaymentScheduleId");

		if (batchPaymentScheduleId != null) {
			setBatchPaymentScheduleId(batchPaymentScheduleId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Date scheduleDate = (Date)attributes.get("scheduleDate");

		if (scheduleDate != null) {
			setScheduleDate(scheduleDate);
		}

		Long feeTypeId = (Long)attributes.get("feeTypeId");

		if (feeTypeId != null) {
			setFeeTypeId(feeTypeId);
		}

		Double amount = (Double)attributes.get("amount");

		if (amount != null) {
			setAmount(amount);
		}
	}

	/**
	* Returns the primary key of this batch payment schedule.
	*
	* @return the primary key of this batch payment schedule
	*/
	public long getPrimaryKey() {
		return _batchPaymentSchedule.getPrimaryKey();
	}

	/**
	* Sets the primary key of this batch payment schedule.
	*
	* @param primaryKey the primary key of this batch payment schedule
	*/
	public void setPrimaryKey(long primaryKey) {
		_batchPaymentSchedule.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the batch payment schedule ID of this batch payment schedule.
	*
	* @return the batch payment schedule ID of this batch payment schedule
	*/
	public long getBatchPaymentScheduleId() {
		return _batchPaymentSchedule.getBatchPaymentScheduleId();
	}

	/**
	* Sets the batch payment schedule ID of this batch payment schedule.
	*
	* @param batchPaymentScheduleId the batch payment schedule ID of this batch payment schedule
	*/
	public void setBatchPaymentScheduleId(long batchPaymentScheduleId) {
		_batchPaymentSchedule.setBatchPaymentScheduleId(batchPaymentScheduleId);
	}

	/**
	* Returns the company ID of this batch payment schedule.
	*
	* @return the company ID of this batch payment schedule
	*/
	public long getCompanyId() {
		return _batchPaymentSchedule.getCompanyId();
	}

	/**
	* Sets the company ID of this batch payment schedule.
	*
	* @param companyId the company ID of this batch payment schedule
	*/
	public void setCompanyId(long companyId) {
		_batchPaymentSchedule.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this batch payment schedule.
	*
	* @return the user ID of this batch payment schedule
	*/
	public long getUserId() {
		return _batchPaymentSchedule.getUserId();
	}

	/**
	* Sets the user ID of this batch payment schedule.
	*
	* @param userId the user ID of this batch payment schedule
	*/
	public void setUserId(long userId) {
		_batchPaymentSchedule.setUserId(userId);
	}

	/**
	* Returns the user uuid of this batch payment schedule.
	*
	* @return the user uuid of this batch payment schedule
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchPaymentSchedule.getUserUuid();
	}

	/**
	* Sets the user uuid of this batch payment schedule.
	*
	* @param userUuid the user uuid of this batch payment schedule
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_batchPaymentSchedule.setUserUuid(userUuid);
	}

	/**
	* Returns the create date of this batch payment schedule.
	*
	* @return the create date of this batch payment schedule
	*/
	public java.util.Date getCreateDate() {
		return _batchPaymentSchedule.getCreateDate();
	}

	/**
	* Sets the create date of this batch payment schedule.
	*
	* @param createDate the create date of this batch payment schedule
	*/
	public void setCreateDate(java.util.Date createDate) {
		_batchPaymentSchedule.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this batch payment schedule.
	*
	* @return the modified date of this batch payment schedule
	*/
	public java.util.Date getModifiedDate() {
		return _batchPaymentSchedule.getModifiedDate();
	}

	/**
	* Sets the modified date of this batch payment schedule.
	*
	* @param modifiedDate the modified date of this batch payment schedule
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_batchPaymentSchedule.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the batch ID of this batch payment schedule.
	*
	* @return the batch ID of this batch payment schedule
	*/
	public long getBatchId() {
		return _batchPaymentSchedule.getBatchId();
	}

	/**
	* Sets the batch ID of this batch payment schedule.
	*
	* @param batchId the batch ID of this batch payment schedule
	*/
	public void setBatchId(long batchId) {
		_batchPaymentSchedule.setBatchId(batchId);
	}

	/**
	* Returns the schedule date of this batch payment schedule.
	*
	* @return the schedule date of this batch payment schedule
	*/
	public java.util.Date getScheduleDate() {
		return _batchPaymentSchedule.getScheduleDate();
	}

	/**
	* Sets the schedule date of this batch payment schedule.
	*
	* @param scheduleDate the schedule date of this batch payment schedule
	*/
	public void setScheduleDate(java.util.Date scheduleDate) {
		_batchPaymentSchedule.setScheduleDate(scheduleDate);
	}

	/**
	* Returns the fee type ID of this batch payment schedule.
	*
	* @return the fee type ID of this batch payment schedule
	*/
	public long getFeeTypeId() {
		return _batchPaymentSchedule.getFeeTypeId();
	}

	/**
	* Sets the fee type ID of this batch payment schedule.
	*
	* @param feeTypeId the fee type ID of this batch payment schedule
	*/
	public void setFeeTypeId(long feeTypeId) {
		_batchPaymentSchedule.setFeeTypeId(feeTypeId);
	}

	/**
	* Returns the amount of this batch payment schedule.
	*
	* @return the amount of this batch payment schedule
	*/
	public double getAmount() {
		return _batchPaymentSchedule.getAmount();
	}

	/**
	* Sets the amount of this batch payment schedule.
	*
	* @param amount the amount of this batch payment schedule
	*/
	public void setAmount(double amount) {
		_batchPaymentSchedule.setAmount(amount);
	}

	public boolean isNew() {
		return _batchPaymentSchedule.isNew();
	}

	public void setNew(boolean n) {
		_batchPaymentSchedule.setNew(n);
	}

	public boolean isCachedModel() {
		return _batchPaymentSchedule.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_batchPaymentSchedule.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _batchPaymentSchedule.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _batchPaymentSchedule.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_batchPaymentSchedule.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _batchPaymentSchedule.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_batchPaymentSchedule.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new BatchPaymentScheduleWrapper((BatchPaymentSchedule)_batchPaymentSchedule.clone());
	}

	public int compareTo(
		info.diit.portal.model.BatchPaymentSchedule batchPaymentSchedule) {
		return _batchPaymentSchedule.compareTo(batchPaymentSchedule);
	}

	@Override
	public int hashCode() {
		return _batchPaymentSchedule.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.BatchPaymentSchedule> toCacheModel() {
		return _batchPaymentSchedule.toCacheModel();
	}

	public info.diit.portal.model.BatchPaymentSchedule toEscapedModel() {
		return new BatchPaymentScheduleWrapper(_batchPaymentSchedule.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _batchPaymentSchedule.toString();
	}

	public java.lang.String toXmlString() {
		return _batchPaymentSchedule.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_batchPaymentSchedule.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public BatchPaymentSchedule getWrappedBatchPaymentSchedule() {
		return _batchPaymentSchedule;
	}

	public BatchPaymentSchedule getWrappedModel() {
		return _batchPaymentSchedule;
	}

	public void resetOriginalValues() {
		_batchPaymentSchedule.resetOriginalValues();
	}

	private BatchPaymentSchedule _batchPaymentSchedule;
}
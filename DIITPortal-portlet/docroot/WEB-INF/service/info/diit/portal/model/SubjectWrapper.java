/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Subject}.
 * </p>
 *
 * @author    mohammad
 * @see       Subject
 * @generated
 */
public class SubjectWrapper implements Subject, ModelWrapper<Subject> {
	public SubjectWrapper(Subject subject) {
		_subject = subject;
	}

	public Class<?> getModelClass() {
		return Subject.class;
	}

	public String getModelClassName() {
		return Subject.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("subjectId", getSubjectId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("subjectCode", getSubjectCode());
		attributes.put("subjectName", getSubjectName());
		attributes.put("description", getDescription());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long subjectId = (Long)attributes.get("subjectId");

		if (subjectId != null) {
			setSubjectId(subjectId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String subjectCode = (String)attributes.get("subjectCode");

		if (subjectCode != null) {
			setSubjectCode(subjectCode);
		}

		String subjectName = (String)attributes.get("subjectName");

		if (subjectName != null) {
			setSubjectName(subjectName);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}
	}

	/**
	* Returns the primary key of this subject.
	*
	* @return the primary key of this subject
	*/
	public long getPrimaryKey() {
		return _subject.getPrimaryKey();
	}

	/**
	* Sets the primary key of this subject.
	*
	* @param primaryKey the primary key of this subject
	*/
	public void setPrimaryKey(long primaryKey) {
		_subject.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the subject ID of this subject.
	*
	* @return the subject ID of this subject
	*/
	public long getSubjectId() {
		return _subject.getSubjectId();
	}

	/**
	* Sets the subject ID of this subject.
	*
	* @param subjectId the subject ID of this subject
	*/
	public void setSubjectId(long subjectId) {
		_subject.setSubjectId(subjectId);
	}

	/**
	* Returns the company ID of this subject.
	*
	* @return the company ID of this subject
	*/
	public long getCompanyId() {
		return _subject.getCompanyId();
	}

	/**
	* Sets the company ID of this subject.
	*
	* @param companyId the company ID of this subject
	*/
	public void setCompanyId(long companyId) {
		_subject.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this subject.
	*
	* @return the organization ID of this subject
	*/
	public long getOrganizationId() {
		return _subject.getOrganizationId();
	}

	/**
	* Sets the organization ID of this subject.
	*
	* @param organizationId the organization ID of this subject
	*/
	public void setOrganizationId(long organizationId) {
		_subject.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this subject.
	*
	* @return the user ID of this subject
	*/
	public long getUserId() {
		return _subject.getUserId();
	}

	/**
	* Sets the user ID of this subject.
	*
	* @param userId the user ID of this subject
	*/
	public void setUserId(long userId) {
		_subject.setUserId(userId);
	}

	/**
	* Returns the user uuid of this subject.
	*
	* @return the user uuid of this subject
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subject.getUserUuid();
	}

	/**
	* Sets the user uuid of this subject.
	*
	* @param userUuid the user uuid of this subject
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_subject.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this subject.
	*
	* @return the user name of this subject
	*/
	public java.lang.String getUserName() {
		return _subject.getUserName();
	}

	/**
	* Sets the user name of this subject.
	*
	* @param userName the user name of this subject
	*/
	public void setUserName(java.lang.String userName) {
		_subject.setUserName(userName);
	}

	/**
	* Returns the create date of this subject.
	*
	* @return the create date of this subject
	*/
	public java.util.Date getCreateDate() {
		return _subject.getCreateDate();
	}

	/**
	* Sets the create date of this subject.
	*
	* @param createDate the create date of this subject
	*/
	public void setCreateDate(java.util.Date createDate) {
		_subject.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this subject.
	*
	* @return the modified date of this subject
	*/
	public java.util.Date getModifiedDate() {
		return _subject.getModifiedDate();
	}

	/**
	* Sets the modified date of this subject.
	*
	* @param modifiedDate the modified date of this subject
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_subject.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the subject code of this subject.
	*
	* @return the subject code of this subject
	*/
	public java.lang.String getSubjectCode() {
		return _subject.getSubjectCode();
	}

	/**
	* Sets the subject code of this subject.
	*
	* @param subjectCode the subject code of this subject
	*/
	public void setSubjectCode(java.lang.String subjectCode) {
		_subject.setSubjectCode(subjectCode);
	}

	/**
	* Returns the subject name of this subject.
	*
	* @return the subject name of this subject
	*/
	public java.lang.String getSubjectName() {
		return _subject.getSubjectName();
	}

	/**
	* Sets the subject name of this subject.
	*
	* @param subjectName the subject name of this subject
	*/
	public void setSubjectName(java.lang.String subjectName) {
		_subject.setSubjectName(subjectName);
	}

	/**
	* Returns the description of this subject.
	*
	* @return the description of this subject
	*/
	public java.lang.String getDescription() {
		return _subject.getDescription();
	}

	/**
	* Sets the description of this subject.
	*
	* @param description the description of this subject
	*/
	public void setDescription(java.lang.String description) {
		_subject.setDescription(description);
	}

	public boolean isNew() {
		return _subject.isNew();
	}

	public void setNew(boolean n) {
		_subject.setNew(n);
	}

	public boolean isCachedModel() {
		return _subject.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_subject.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _subject.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _subject.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_subject.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _subject.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_subject.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new SubjectWrapper((Subject)_subject.clone());
	}

	public int compareTo(info.diit.portal.model.Subject subject) {
		return _subject.compareTo(subject);
	}

	@Override
	public int hashCode() {
		return _subject.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.Subject> toCacheModel() {
		return _subject.toCacheModel();
	}

	public info.diit.portal.model.Subject toEscapedModel() {
		return new SubjectWrapper(_subject.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _subject.toString();
	}

	public java.lang.String toXmlString() {
		return _subject.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_subject.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Subject getWrappedSubject() {
		return _subject;
	}

	public Subject getWrappedModel() {
		return _subject;
	}

	public void resetOriginalValues() {
		_subject.resetOriginalValues();
	}

	private Subject _subject;
}
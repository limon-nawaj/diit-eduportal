/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class BatchStudentSoap implements Serializable {
	public static BatchStudentSoap toSoapModel(BatchStudent model) {
		BatchStudentSoap soapModel = new BatchStudentSoap();

		soapModel.setBatchStudentId(model.getBatchStudentId());
		soapModel.setStudentId(model.getStudentId());
		soapModel.setBatchId(model.getBatchId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setStartDate(model.getStartDate());
		soapModel.setEndDate(model.getEndDate());
		soapModel.setNote(model.getNote());
		soapModel.setStatus(model.getStatus());

		return soapModel;
	}

	public static BatchStudentSoap[] toSoapModels(BatchStudent[] models) {
		BatchStudentSoap[] soapModels = new BatchStudentSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BatchStudentSoap[][] toSoapModels(BatchStudent[][] models) {
		BatchStudentSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BatchStudentSoap[models.length][models[0].length];
		}
		else {
			soapModels = new BatchStudentSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BatchStudentSoap[] toSoapModels(List<BatchStudent> models) {
		List<BatchStudentSoap> soapModels = new ArrayList<BatchStudentSoap>(models.size());

		for (BatchStudent model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BatchStudentSoap[soapModels.size()]);
	}

	public BatchStudentSoap() {
	}

	public long getPrimaryKey() {
		return _batchStudentId;
	}

	public void setPrimaryKey(long pk) {
		setBatchStudentId(pk);
	}

	public long getBatchStudentId() {
		return _batchStudentId;
	}

	public void setBatchStudentId(long batchStudentId) {
		_batchStudentId = batchStudentId;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public Date getStartDate() {
		return _startDate;
	}

	public void setStartDate(Date startDate) {
		_startDate = startDate;
	}

	public Date getEndDate() {
		return _endDate;
	}

	public void setEndDate(Date endDate) {
		_endDate = endDate;
	}

	public String getNote() {
		return _note;
	}

	public void setNote(String note) {
		_note = note;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	private long _batchStudentId;
	private long _studentId;
	private long _batchId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private Date _startDate;
	private Date _endDate;
	private String _note;
	private int _status;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link info.diit.portal.service.http.CommentsServiceSoap}.
 *
 * @author    mohammad
 * @see       info.diit.portal.service.http.CommentsServiceSoap
 * @generated
 */
public class CommentsSoap implements Serializable {
	public static CommentsSoap toSoapModel(Comments model) {
		CommentsSoap soapModel = new CommentsSoap();

		soapModel.setCommentId(model.getCommentId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setBatchId(model.getBatchId());
		soapModel.setStudentId(model.getStudentId());
		soapModel.setCommentDate(model.getCommentDate());
		soapModel.setComments(model.getComments());

		return soapModel;
	}

	public static CommentsSoap[] toSoapModels(Comments[] models) {
		CommentsSoap[] soapModels = new CommentsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CommentsSoap[][] toSoapModels(Comments[][] models) {
		CommentsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CommentsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CommentsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CommentsSoap[] toSoapModels(List<Comments> models) {
		List<CommentsSoap> soapModels = new ArrayList<CommentsSoap>(models.size());

		for (Comments model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CommentsSoap[soapModels.size()]);
	}

	public CommentsSoap() {
	}

	public long getPrimaryKey() {
		return _commentId;
	}

	public void setPrimaryKey(long pk) {
		setCommentId(pk);
	}

	public long getCommentId() {
		return _commentId;
	}

	public void setCommentId(long commentId) {
		_commentId = commentId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	public Date getCommentDate() {
		return _commentDate;
	}

	public void setCommentDate(Date commentDate) {
		_commentDate = commentDate;
	}

	public String getComments() {
		return _comments;
	}

	public void setComments(String comments) {
		_comments = comments;
	}

	private long _commentId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _batchId;
	private long _studentId;
	private Date _commentDate;
	private String _comments;
}
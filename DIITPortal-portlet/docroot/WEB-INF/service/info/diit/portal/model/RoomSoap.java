/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class RoomSoap implements Serializable {
	public static RoomSoap toSoapModel(Room model) {
		RoomSoap soapModel = new RoomSoap();

		soapModel.setRoomId(model.getRoomId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setLabel(model.getLabel());
		soapModel.setDescription(model.getDescription());
		soapModel.setRoomType(model.getRoomType());

		return soapModel;
	}

	public static RoomSoap[] toSoapModels(Room[] models) {
		RoomSoap[] soapModels = new RoomSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static RoomSoap[][] toSoapModels(Room[][] models) {
		RoomSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new RoomSoap[models.length][models[0].length];
		}
		else {
			soapModels = new RoomSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static RoomSoap[] toSoapModels(List<Room> models) {
		List<RoomSoap> soapModels = new ArrayList<RoomSoap>(models.size());

		for (Room model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new RoomSoap[soapModels.size()]);
	}

	public RoomSoap() {
	}

	public long getPrimaryKey() {
		return _roomId;
	}

	public void setPrimaryKey(long pk) {
		setRoomId(pk);
	}

	public long getRoomId() {
		return _roomId;
	}

	public void setRoomId(long roomId) {
		_roomId = roomId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getLabel() {
		return _label;
	}

	public void setLabel(String label) {
		_label = label;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public int getRoomType() {
		return _roomType;
	}

	public void setRoomType(int roomType) {
		_roomType = roomType;
	}

	private long _roomId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _label;
	private String _description;
	private int _roomType;
}
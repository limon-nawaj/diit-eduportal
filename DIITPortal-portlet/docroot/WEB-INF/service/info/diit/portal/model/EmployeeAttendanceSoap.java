/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class EmployeeAttendanceSoap implements Serializable {
	public static EmployeeAttendanceSoap toSoapModel(EmployeeAttendance model) {
		EmployeeAttendanceSoap soapModel = new EmployeeAttendanceSoap();

		soapModel.setEmployeeAttendanceId(model.getEmployeeAttendanceId());
		soapModel.setEmployeeId(model.getEmployeeId());
		soapModel.setExpectedStart(model.getExpectedStart());
		soapModel.setExpectedEnd(model.getExpectedEnd());
		soapModel.setRealTime(model.getRealTime());
		soapModel.setStatus(model.getStatus());

		return soapModel;
	}

	public static EmployeeAttendanceSoap[] toSoapModels(
		EmployeeAttendance[] models) {
		EmployeeAttendanceSoap[] soapModels = new EmployeeAttendanceSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EmployeeAttendanceSoap[][] toSoapModels(
		EmployeeAttendance[][] models) {
		EmployeeAttendanceSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EmployeeAttendanceSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EmployeeAttendanceSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EmployeeAttendanceSoap[] toSoapModels(
		List<EmployeeAttendance> models) {
		List<EmployeeAttendanceSoap> soapModels = new ArrayList<EmployeeAttendanceSoap>(models.size());

		for (EmployeeAttendance model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EmployeeAttendanceSoap[soapModels.size()]);
	}

	public EmployeeAttendanceSoap() {
	}

	public long getPrimaryKey() {
		return _employeeAttendanceId;
	}

	public void setPrimaryKey(long pk) {
		setEmployeeAttendanceId(pk);
	}

	public long getEmployeeAttendanceId() {
		return _employeeAttendanceId;
	}

	public void setEmployeeAttendanceId(long employeeAttendanceId) {
		_employeeAttendanceId = employeeAttendanceId;
	}

	public long getEmployeeId() {
		return _employeeId;
	}

	public void setEmployeeId(long employeeId) {
		_employeeId = employeeId;
	}

	public Date getExpectedStart() {
		return _expectedStart;
	}

	public void setExpectedStart(Date expectedStart) {
		_expectedStart = expectedStart;
	}

	public Date getExpectedEnd() {
		return _expectedEnd;
	}

	public void setExpectedEnd(Date expectedEnd) {
		_expectedEnd = expectedEnd;
	}

	public Date getRealTime() {
		return _realTime;
	}

	public void setRealTime(Date realTime) {
		_realTime = realTime;
	}

	public long getStatus() {
		return _status;
	}

	public void setStatus(long status) {
		_status = status;
	}

	private long _employeeAttendanceId;
	private long _employeeId;
	private Date _expectedStart;
	private Date _expectedEnd;
	private Date _realTime;
	private long _status;
}
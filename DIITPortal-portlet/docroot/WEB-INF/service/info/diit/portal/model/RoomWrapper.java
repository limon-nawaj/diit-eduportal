/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Room}.
 * </p>
 *
 * @author    mohammad
 * @see       Room
 * @generated
 */
public class RoomWrapper implements Room, ModelWrapper<Room> {
	public RoomWrapper(Room room) {
		_room = room;
	}

	public Class<?> getModelClass() {
		return Room.class;
	}

	public String getModelClassName() {
		return Room.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("roomId", getRoomId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("label", getLabel());
		attributes.put("description", getDescription());
		attributes.put("roomType", getRoomType());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long roomId = (Long)attributes.get("roomId");

		if (roomId != null) {
			setRoomId(roomId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String label = (String)attributes.get("label");

		if (label != null) {
			setLabel(label);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Integer roomType = (Integer)attributes.get("roomType");

		if (roomType != null) {
			setRoomType(roomType);
		}
	}

	/**
	* Returns the primary key of this room.
	*
	* @return the primary key of this room
	*/
	public long getPrimaryKey() {
		return _room.getPrimaryKey();
	}

	/**
	* Sets the primary key of this room.
	*
	* @param primaryKey the primary key of this room
	*/
	public void setPrimaryKey(long primaryKey) {
		_room.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the room ID of this room.
	*
	* @return the room ID of this room
	*/
	public long getRoomId() {
		return _room.getRoomId();
	}

	/**
	* Sets the room ID of this room.
	*
	* @param roomId the room ID of this room
	*/
	public void setRoomId(long roomId) {
		_room.setRoomId(roomId);
	}

	/**
	* Returns the company ID of this room.
	*
	* @return the company ID of this room
	*/
	public long getCompanyId() {
		return _room.getCompanyId();
	}

	/**
	* Sets the company ID of this room.
	*
	* @param companyId the company ID of this room
	*/
	public void setCompanyId(long companyId) {
		_room.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this room.
	*
	* @return the organization ID of this room
	*/
	public long getOrganizationId() {
		return _room.getOrganizationId();
	}

	/**
	* Sets the organization ID of this room.
	*
	* @param organizationId the organization ID of this room
	*/
	public void setOrganizationId(long organizationId) {
		_room.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this room.
	*
	* @return the user ID of this room
	*/
	public long getUserId() {
		return _room.getUserId();
	}

	/**
	* Sets the user ID of this room.
	*
	* @param userId the user ID of this room
	*/
	public void setUserId(long userId) {
		_room.setUserId(userId);
	}

	/**
	* Returns the user uuid of this room.
	*
	* @return the user uuid of this room
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _room.getUserUuid();
	}

	/**
	* Sets the user uuid of this room.
	*
	* @param userUuid the user uuid of this room
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_room.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this room.
	*
	* @return the user name of this room
	*/
	public java.lang.String getUserName() {
		return _room.getUserName();
	}

	/**
	* Sets the user name of this room.
	*
	* @param userName the user name of this room
	*/
	public void setUserName(java.lang.String userName) {
		_room.setUserName(userName);
	}

	/**
	* Returns the create date of this room.
	*
	* @return the create date of this room
	*/
	public java.util.Date getCreateDate() {
		return _room.getCreateDate();
	}

	/**
	* Sets the create date of this room.
	*
	* @param createDate the create date of this room
	*/
	public void setCreateDate(java.util.Date createDate) {
		_room.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this room.
	*
	* @return the modified date of this room
	*/
	public java.util.Date getModifiedDate() {
		return _room.getModifiedDate();
	}

	/**
	* Sets the modified date of this room.
	*
	* @param modifiedDate the modified date of this room
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_room.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the label of this room.
	*
	* @return the label of this room
	*/
	public java.lang.String getLabel() {
		return _room.getLabel();
	}

	/**
	* Sets the label of this room.
	*
	* @param label the label of this room
	*/
	public void setLabel(java.lang.String label) {
		_room.setLabel(label);
	}

	/**
	* Returns the description of this room.
	*
	* @return the description of this room
	*/
	public java.lang.String getDescription() {
		return _room.getDescription();
	}

	/**
	* Sets the description of this room.
	*
	* @param description the description of this room
	*/
	public void setDescription(java.lang.String description) {
		_room.setDescription(description);
	}

	/**
	* Returns the room type of this room.
	*
	* @return the room type of this room
	*/
	public int getRoomType() {
		return _room.getRoomType();
	}

	/**
	* Sets the room type of this room.
	*
	* @param roomType the room type of this room
	*/
	public void setRoomType(int roomType) {
		_room.setRoomType(roomType);
	}

	public boolean isNew() {
		return _room.isNew();
	}

	public void setNew(boolean n) {
		_room.setNew(n);
	}

	public boolean isCachedModel() {
		return _room.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_room.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _room.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _room.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_room.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _room.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_room.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new RoomWrapper((Room)_room.clone());
	}

	public int compareTo(info.diit.portal.model.Room room) {
		return _room.compareTo(room);
	}

	@Override
	public int hashCode() {
		return _room.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.Room> toCacheModel() {
		return _room.toCacheModel();
	}

	public info.diit.portal.model.Room toEscapedModel() {
		return new RoomWrapper(_room.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _room.toString();
	}

	public java.lang.String toXmlString() {
		return _room.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_room.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Room getWrappedRoom() {
		return _room;
	}

	public Room getWrappedModel() {
		return _room;
	}

	public void resetOriginalValues() {
		_room.resetOriginalValues();
	}

	private Room _room;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link info.diit.portal.service.http.CounselingCourseInterestServiceSoap}.
 *
 * @author    mohammad
 * @see       info.diit.portal.service.http.CounselingCourseInterestServiceSoap
 * @generated
 */
public class CounselingCourseInterestSoap implements Serializable {
	public static CounselingCourseInterestSoap toSoapModel(
		CounselingCourseInterest model) {
		CounselingCourseInterestSoap soapModel = new CounselingCourseInterestSoap();

		soapModel.setCounselingCourseInterestId(model.getCounselingCourseInterestId());
		soapModel.setCourseId(model.getCourseId());
		soapModel.setCounselingId(model.getCounselingId());

		return soapModel;
	}

	public static CounselingCourseInterestSoap[] toSoapModels(
		CounselingCourseInterest[] models) {
		CounselingCourseInterestSoap[] soapModels = new CounselingCourseInterestSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CounselingCourseInterestSoap[][] toSoapModels(
		CounselingCourseInterest[][] models) {
		CounselingCourseInterestSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CounselingCourseInterestSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CounselingCourseInterestSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CounselingCourseInterestSoap[] toSoapModels(
		List<CounselingCourseInterest> models) {
		List<CounselingCourseInterestSoap> soapModels = new ArrayList<CounselingCourseInterestSoap>(models.size());

		for (CounselingCourseInterest model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CounselingCourseInterestSoap[soapModels.size()]);
	}

	public CounselingCourseInterestSoap() {
	}

	public long getPrimaryKey() {
		return _CounselingCourseInterestId;
	}

	public void setPrimaryKey(long pk) {
		setCounselingCourseInterestId(pk);
	}

	public long getCounselingCourseInterestId() {
		return _CounselingCourseInterestId;
	}

	public void setCounselingCourseInterestId(long CounselingCourseInterestId) {
		_CounselingCourseInterestId = CounselingCourseInterestId;
	}

	public long getCourseId() {
		return _courseId;
	}

	public void setCourseId(long courseId) {
		_courseId = courseId;
	}

	public long getCounselingId() {
		return _counselingId;
	}

	public void setCounselingId(long counselingId) {
		_counselingId = counselingId;
	}

	private long _CounselingCourseInterestId;
	private long _courseId;
	private long _counselingId;
}
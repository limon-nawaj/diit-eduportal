/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import info.diit.portal.service.EmployeeRoleLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class EmployeeRoleClp extends BaseModelImpl<EmployeeRole>
	implements EmployeeRole {
	public EmployeeRoleClp() {
	}

	public Class<?> getModelClass() {
		return EmployeeRole.class;
	}

	public String getModelClassName() {
		return EmployeeRole.class.getName();
	}

	public long getPrimaryKey() {
		return _employeeRoleId;
	}

	public void setPrimaryKey(long primaryKey) {
		setEmployeeRoleId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_employeeRoleId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("employeeRoleId", getEmployeeRoleId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("companyId", getCompanyId());
		attributes.put("role", getRole());
		attributes.put("startTime", getStartTime());
		attributes.put("endTime", getEndTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long employeeRoleId = (Long)attributes.get("employeeRoleId");

		if (employeeRoleId != null) {
			setEmployeeRoleId(employeeRoleId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		String role = (String)attributes.get("role");

		if (role != null) {
			setRole(role);
		}

		Date startTime = (Date)attributes.get("startTime");

		if (startTime != null) {
			setStartTime(startTime);
		}

		Date endTime = (Date)attributes.get("endTime");

		if (endTime != null) {
			setEndTime(endTime);
		}
	}

	public long getEmployeeRoleId() {
		return _employeeRoleId;
	}

	public void setEmployeeRoleId(long employeeRoleId) {
		_employeeRoleId = employeeRoleId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public String getRole() {
		return _role;
	}

	public void setRole(String role) {
		_role = role;
	}

	public Date getStartTime() {
		return _startTime;
	}

	public void setStartTime(Date startTime) {
		_startTime = startTime;
	}

	public Date getEndTime() {
		return _endTime;
	}

	public void setEndTime(Date endTime) {
		_endTime = endTime;
	}

	public BaseModel<?> getEmployeeRoleRemoteModel() {
		return _employeeRoleRemoteModel;
	}

	public void setEmployeeRoleRemoteModel(BaseModel<?> employeeRoleRemoteModel) {
		_employeeRoleRemoteModel = employeeRoleRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			EmployeeRoleLocalServiceUtil.addEmployeeRole(this);
		}
		else {
			EmployeeRoleLocalServiceUtil.updateEmployeeRole(this);
		}
	}

	@Override
	public EmployeeRole toEscapedModel() {
		return (EmployeeRole)Proxy.newProxyInstance(EmployeeRole.class.getClassLoader(),
			new Class[] { EmployeeRole.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		EmployeeRoleClp clone = new EmployeeRoleClp();

		clone.setEmployeeRoleId(getEmployeeRoleId());
		clone.setOrganizationId(getOrganizationId());
		clone.setCompanyId(getCompanyId());
		clone.setRole(getRole());
		clone.setStartTime(getStartTime());
		clone.setEndTime(getEndTime());

		return clone;
	}

	public int compareTo(EmployeeRole employeeRole) {
		long primaryKey = employeeRole.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		EmployeeRoleClp employeeRole = null;

		try {
			employeeRole = (EmployeeRoleClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = employeeRole.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{employeeRoleId=");
		sb.append(getEmployeeRoleId());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", role=");
		sb.append(getRole());
		sb.append(", startTime=");
		sb.append(getStartTime());
		sb.append(", endTime=");
		sb.append(getEndTime());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.EmployeeRole");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>employeeRoleId</column-name><column-value><![CDATA[");
		sb.append(getEmployeeRoleId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>role</column-name><column-value><![CDATA[");
		sb.append(getRole());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>startTime</column-name><column-value><![CDATA[");
		sb.append(getStartTime());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>endTime</column-name><column-value><![CDATA[");
		sb.append(getEndTime());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _employeeRoleId;
	private long _organizationId;
	private long _companyId;
	private String _role;
	private Date _startTime;
	private Date _endTime;
	private BaseModel<?> _employeeRoleRemoteModel;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.AuditedModel;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the Experiance service. Represents a row in the &quot;EduPortal_Experiance&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link info.diit.portal.model.impl.ExperianceModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link info.diit.portal.model.impl.ExperianceImpl}.
 * </p>
 *
 * @author mohammad
 * @see Experiance
 * @see info.diit.portal.model.impl.ExperianceImpl
 * @see info.diit.portal.model.impl.ExperianceModelImpl
 * @generated
 */
public interface ExperianceModel extends AuditedModel, BaseModel<Experiance> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a experiance model instance should use the {@link Experiance} interface instead.
	 */

	/**
	 * Returns the primary key of this experiance.
	 *
	 * @return the primary key of this experiance
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this experiance.
	 *
	 * @param primaryKey the primary key of this experiance
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the experiance ID of this experiance.
	 *
	 * @return the experiance ID of this experiance
	 */
	public long getExperianceId();

	/**
	 * Sets the experiance ID of this experiance.
	 *
	 * @param experianceId the experiance ID of this experiance
	 */
	public void setExperianceId(long experianceId);

	/**
	 * Returns the company ID of this experiance.
	 *
	 * @return the company ID of this experiance
	 */
	public long getCompanyId();

	/**
	 * Sets the company ID of this experiance.
	 *
	 * @param companyId the company ID of this experiance
	 */
	public void setCompanyId(long companyId);

	/**
	 * Returns the user ID of this experiance.
	 *
	 * @return the user ID of this experiance
	 */
	public long getUserId();

	/**
	 * Sets the user ID of this experiance.
	 *
	 * @param userId the user ID of this experiance
	 */
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this experiance.
	 *
	 * @return the user uuid of this experiance
	 * @throws SystemException if a system exception occurred
	 */
	public String getUserUuid() throws SystemException;

	/**
	 * Sets the user uuid of this experiance.
	 *
	 * @param userUuid the user uuid of this experiance
	 */
	public void setUserUuid(String userUuid);

	/**
	 * Returns the user name of this experiance.
	 *
	 * @return the user name of this experiance
	 */
	@AutoEscape
	public String getUserName();

	/**
	 * Sets the user name of this experiance.
	 *
	 * @param userName the user name of this experiance
	 */
	public void setUserName(String userName);

	/**
	 * Returns the create date of this experiance.
	 *
	 * @return the create date of this experiance
	 */
	public Date getCreateDate();

	/**
	 * Sets the create date of this experiance.
	 *
	 * @param createDate the create date of this experiance
	 */
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this experiance.
	 *
	 * @return the modified date of this experiance
	 */
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this experiance.
	 *
	 * @param modifiedDate the modified date of this experiance
	 */
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the organization ID of this experiance.
	 *
	 * @return the organization ID of this experiance
	 */
	public long getOrganizationId();

	/**
	 * Sets the organization ID of this experiance.
	 *
	 * @param organizationId the organization ID of this experiance
	 */
	public void setOrganizationId(long organizationId);

	/**
	 * Returns the organization of this experiance.
	 *
	 * @return the organization of this experiance
	 */
	@AutoEscape
	public String getOrganization();

	/**
	 * Sets the organization of this experiance.
	 *
	 * @param organization the organization of this experiance
	 */
	public void setOrganization(String organization);

	/**
	 * Returns the designation of this experiance.
	 *
	 * @return the designation of this experiance
	 */
	@AutoEscape
	public String getDesignation();

	/**
	 * Sets the designation of this experiance.
	 *
	 * @param designation the designation of this experiance
	 */
	public void setDesignation(String designation);

	/**
	 * Returns the start date of this experiance.
	 *
	 * @return the start date of this experiance
	 */
	public Date getStartDate();

	/**
	 * Sets the start date of this experiance.
	 *
	 * @param startDate the start date of this experiance
	 */
	public void setStartDate(Date startDate);

	/**
	 * Returns the end date of this experiance.
	 *
	 * @return the end date of this experiance
	 */
	public Date getEndDate();

	/**
	 * Sets the end date of this experiance.
	 *
	 * @param endDate the end date of this experiance
	 */
	public void setEndDate(Date endDate);

	/**
	 * Returns the current status of this experiance.
	 *
	 * @return the current status of this experiance
	 */
	public int getCurrentStatus();

	/**
	 * Sets the current status of this experiance.
	 *
	 * @param currentStatus the current status of this experiance
	 */
	public void setCurrentStatus(int currentStatus);

	/**
	 * Returns the student ID of this experiance.
	 *
	 * @return the student ID of this experiance
	 */
	public long getStudentId();

	/**
	 * Sets the student ID of this experiance.
	 *
	 * @param studentId the student ID of this experiance
	 */
	public void setStudentId(long studentId);

	public boolean isNew();

	public void setNew(boolean n);

	public boolean isCachedModel();

	public void setCachedModel(boolean cachedModel);

	public boolean isEscapedModel();

	public Serializable getPrimaryKeyObj();

	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	public ExpandoBridge getExpandoBridge();

	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	public Object clone();

	public int compareTo(Experiance experiance);

	public int hashCode();

	public CacheModel<Experiance> toCacheModel();

	public Experiance toEscapedModel();

	public String toString();

	public String toXmlString();
}
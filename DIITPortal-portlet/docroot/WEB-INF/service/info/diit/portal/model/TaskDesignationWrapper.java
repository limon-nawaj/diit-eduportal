/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link TaskDesignation}.
 * </p>
 *
 * @author    mohammad
 * @see       TaskDesignation
 * @generated
 */
public class TaskDesignationWrapper implements TaskDesignation,
	ModelWrapper<TaskDesignation> {
	public TaskDesignationWrapper(TaskDesignation taskDesignation) {
		_taskDesignation = taskDesignation;
	}

	public Class<?> getModelClass() {
		return TaskDesignation.class;
	}

	public String getModelClassName() {
		return TaskDesignation.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("taskDesignationId", getTaskDesignationId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("taskId", getTaskId());
		attributes.put("designationId", getDesignationId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long taskDesignationId = (Long)attributes.get("taskDesignationId");

		if (taskDesignationId != null) {
			setTaskDesignationId(taskDesignationId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long taskId = (Long)attributes.get("taskId");

		if (taskId != null) {
			setTaskId(taskId);
		}

		Long designationId = (Long)attributes.get("designationId");

		if (designationId != null) {
			setDesignationId(designationId);
		}
	}

	/**
	* Returns the primary key of this task designation.
	*
	* @return the primary key of this task designation
	*/
	public long getPrimaryKey() {
		return _taskDesignation.getPrimaryKey();
	}

	/**
	* Sets the primary key of this task designation.
	*
	* @param primaryKey the primary key of this task designation
	*/
	public void setPrimaryKey(long primaryKey) {
		_taskDesignation.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the task designation ID of this task designation.
	*
	* @return the task designation ID of this task designation
	*/
	public long getTaskDesignationId() {
		return _taskDesignation.getTaskDesignationId();
	}

	/**
	* Sets the task designation ID of this task designation.
	*
	* @param taskDesignationId the task designation ID of this task designation
	*/
	public void setTaskDesignationId(long taskDesignationId) {
		_taskDesignation.setTaskDesignationId(taskDesignationId);
	}

	/**
	* Returns the company ID of this task designation.
	*
	* @return the company ID of this task designation
	*/
	public long getCompanyId() {
		return _taskDesignation.getCompanyId();
	}

	/**
	* Sets the company ID of this task designation.
	*
	* @param companyId the company ID of this task designation
	*/
	public void setCompanyId(long companyId) {
		_taskDesignation.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this task designation.
	*
	* @return the user ID of this task designation
	*/
	public long getUserId() {
		return _taskDesignation.getUserId();
	}

	/**
	* Sets the user ID of this task designation.
	*
	* @param userId the user ID of this task designation
	*/
	public void setUserId(long userId) {
		_taskDesignation.setUserId(userId);
	}

	/**
	* Returns the user uuid of this task designation.
	*
	* @return the user uuid of this task designation
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignation.getUserUuid();
	}

	/**
	* Sets the user uuid of this task designation.
	*
	* @param userUuid the user uuid of this task designation
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_taskDesignation.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this task designation.
	*
	* @return the user name of this task designation
	*/
	public java.lang.String getUserName() {
		return _taskDesignation.getUserName();
	}

	/**
	* Sets the user name of this task designation.
	*
	* @param userName the user name of this task designation
	*/
	public void setUserName(java.lang.String userName) {
		_taskDesignation.setUserName(userName);
	}

	/**
	* Returns the create date of this task designation.
	*
	* @return the create date of this task designation
	*/
	public java.util.Date getCreateDate() {
		return _taskDesignation.getCreateDate();
	}

	/**
	* Sets the create date of this task designation.
	*
	* @param createDate the create date of this task designation
	*/
	public void setCreateDate(java.util.Date createDate) {
		_taskDesignation.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this task designation.
	*
	* @return the modified date of this task designation
	*/
	public java.util.Date getModifiedDate() {
		return _taskDesignation.getModifiedDate();
	}

	/**
	* Sets the modified date of this task designation.
	*
	* @param modifiedDate the modified date of this task designation
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_taskDesignation.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the task ID of this task designation.
	*
	* @return the task ID of this task designation
	*/
	public long getTaskId() {
		return _taskDesignation.getTaskId();
	}

	/**
	* Sets the task ID of this task designation.
	*
	* @param taskId the task ID of this task designation
	*/
	public void setTaskId(long taskId) {
		_taskDesignation.setTaskId(taskId);
	}

	/**
	* Returns the designation ID of this task designation.
	*
	* @return the designation ID of this task designation
	*/
	public long getDesignationId() {
		return _taskDesignation.getDesignationId();
	}

	/**
	* Sets the designation ID of this task designation.
	*
	* @param designationId the designation ID of this task designation
	*/
	public void setDesignationId(long designationId) {
		_taskDesignation.setDesignationId(designationId);
	}

	public boolean isNew() {
		return _taskDesignation.isNew();
	}

	public void setNew(boolean n) {
		_taskDesignation.setNew(n);
	}

	public boolean isCachedModel() {
		return _taskDesignation.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_taskDesignation.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _taskDesignation.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _taskDesignation.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_taskDesignation.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _taskDesignation.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_taskDesignation.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new TaskDesignationWrapper((TaskDesignation)_taskDesignation.clone());
	}

	public int compareTo(info.diit.portal.model.TaskDesignation taskDesignation) {
		return _taskDesignation.compareTo(taskDesignation);
	}

	@Override
	public int hashCode() {
		return _taskDesignation.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.TaskDesignation> toCacheModel() {
		return _taskDesignation.toCacheModel();
	}

	public info.diit.portal.model.TaskDesignation toEscapedModel() {
		return new TaskDesignationWrapper(_taskDesignation.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _taskDesignation.toString();
	}

	public java.lang.String toXmlString() {
		return _taskDesignation.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_taskDesignation.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public TaskDesignation getWrappedTaskDesignation() {
		return _taskDesignation;
	}

	public TaskDesignation getWrappedModel() {
		return _taskDesignation;
	}

	public void resetOriginalValues() {
		_taskDesignation.resetOriginalValues();
	}

	private TaskDesignation _taskDesignation;
}
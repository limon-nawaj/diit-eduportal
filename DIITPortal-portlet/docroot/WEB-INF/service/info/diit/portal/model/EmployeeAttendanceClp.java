/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import info.diit.portal.service.EmployeeAttendanceLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class EmployeeAttendanceClp extends BaseModelImpl<EmployeeAttendance>
	implements EmployeeAttendance {
	public EmployeeAttendanceClp() {
	}

	public Class<?> getModelClass() {
		return EmployeeAttendance.class;
	}

	public String getModelClassName() {
		return EmployeeAttendance.class.getName();
	}

	public long getPrimaryKey() {
		return _employeeAttendanceId;
	}

	public void setPrimaryKey(long primaryKey) {
		setEmployeeAttendanceId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_employeeAttendanceId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("employeeAttendanceId", getEmployeeAttendanceId());
		attributes.put("employeeId", getEmployeeId());
		attributes.put("expectedStart", getExpectedStart());
		attributes.put("expectedEnd", getExpectedEnd());
		attributes.put("realTime", getRealTime());
		attributes.put("status", getStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long employeeAttendanceId = (Long)attributes.get("employeeAttendanceId");

		if (employeeAttendanceId != null) {
			setEmployeeAttendanceId(employeeAttendanceId);
		}

		Long employeeId = (Long)attributes.get("employeeId");

		if (employeeId != null) {
			setEmployeeId(employeeId);
		}

		Date expectedStart = (Date)attributes.get("expectedStart");

		if (expectedStart != null) {
			setExpectedStart(expectedStart);
		}

		Date expectedEnd = (Date)attributes.get("expectedEnd");

		if (expectedEnd != null) {
			setExpectedEnd(expectedEnd);
		}

		Date realTime = (Date)attributes.get("realTime");

		if (realTime != null) {
			setRealTime(realTime);
		}

		Long status = (Long)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	public long getEmployeeAttendanceId() {
		return _employeeAttendanceId;
	}

	public void setEmployeeAttendanceId(long employeeAttendanceId) {
		_employeeAttendanceId = employeeAttendanceId;
	}

	public long getEmployeeId() {
		return _employeeId;
	}

	public void setEmployeeId(long employeeId) {
		_employeeId = employeeId;
	}

	public Date getExpectedStart() {
		return _expectedStart;
	}

	public void setExpectedStart(Date expectedStart) {
		_expectedStart = expectedStart;
	}

	public Date getExpectedEnd() {
		return _expectedEnd;
	}

	public void setExpectedEnd(Date expectedEnd) {
		_expectedEnd = expectedEnd;
	}

	public Date getRealTime() {
		return _realTime;
	}

	public void setRealTime(Date realTime) {
		_realTime = realTime;
	}

	public long getStatus() {
		return _status;
	}

	public void setStatus(long status) {
		_status = status;
	}

	public BaseModel<?> getEmployeeAttendanceRemoteModel() {
		return _employeeAttendanceRemoteModel;
	}

	public void setEmployeeAttendanceRemoteModel(
		BaseModel<?> employeeAttendanceRemoteModel) {
		_employeeAttendanceRemoteModel = employeeAttendanceRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			EmployeeAttendanceLocalServiceUtil.addEmployeeAttendance(this);
		}
		else {
			EmployeeAttendanceLocalServiceUtil.updateEmployeeAttendance(this);
		}
	}

	@Override
	public EmployeeAttendance toEscapedModel() {
		return (EmployeeAttendance)Proxy.newProxyInstance(EmployeeAttendance.class.getClassLoader(),
			new Class[] { EmployeeAttendance.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		EmployeeAttendanceClp clone = new EmployeeAttendanceClp();

		clone.setEmployeeAttendanceId(getEmployeeAttendanceId());
		clone.setEmployeeId(getEmployeeId());
		clone.setExpectedStart(getExpectedStart());
		clone.setExpectedEnd(getExpectedEnd());
		clone.setRealTime(getRealTime());
		clone.setStatus(getStatus());

		return clone;
	}

	public int compareTo(EmployeeAttendance employeeAttendance) {
		long primaryKey = employeeAttendance.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		EmployeeAttendanceClp employeeAttendance = null;

		try {
			employeeAttendance = (EmployeeAttendanceClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = employeeAttendance.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{employeeAttendanceId=");
		sb.append(getEmployeeAttendanceId());
		sb.append(", employeeId=");
		sb.append(getEmployeeId());
		sb.append(", expectedStart=");
		sb.append(getExpectedStart());
		sb.append(", expectedEnd=");
		sb.append(getExpectedEnd());
		sb.append(", realTime=");
		sb.append(getRealTime());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.EmployeeAttendance");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>employeeAttendanceId</column-name><column-value><![CDATA[");
		sb.append(getEmployeeAttendanceId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>employeeId</column-name><column-value><![CDATA[");
		sb.append(getEmployeeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>expectedStart</column-name><column-value><![CDATA[");
		sb.append(getExpectedStart());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>expectedEnd</column-name><column-value><![CDATA[");
		sb.append(getExpectedEnd());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>realTime</column-name><column-value><![CDATA[");
		sb.append(getRealTime());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _employeeAttendanceId;
	private long _employeeId;
	private Date _expectedStart;
	private Date _expectedEnd;
	private Date _realTime;
	private long _status;
	private BaseModel<?> _employeeAttendanceRemoteModel;
}
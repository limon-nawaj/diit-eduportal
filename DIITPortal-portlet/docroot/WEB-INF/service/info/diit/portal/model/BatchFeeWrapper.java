/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BatchFee}.
 * </p>
 *
 * @author    mohammad
 * @see       BatchFee
 * @generated
 */
public class BatchFeeWrapper implements BatchFee, ModelWrapper<BatchFee> {
	public BatchFeeWrapper(BatchFee batchFee) {
		_batchFee = batchFee;
	}

	public Class<?> getModelClass() {
		return BatchFee.class;
	}

	public String getModelClassName() {
		return BatchFee.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("batchFeeId", getBatchFeeId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("batchId", getBatchId());
		attributes.put("feeTypeId", getFeeTypeId());
		attributes.put("amount", getAmount());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long batchFeeId = (Long)attributes.get("batchFeeId");

		if (batchFeeId != null) {
			setBatchFeeId(batchFeeId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Long feeTypeId = (Long)attributes.get("feeTypeId");

		if (feeTypeId != null) {
			setFeeTypeId(feeTypeId);
		}

		Double amount = (Double)attributes.get("amount");

		if (amount != null) {
			setAmount(amount);
		}
	}

	/**
	* Returns the primary key of this batch fee.
	*
	* @return the primary key of this batch fee
	*/
	public long getPrimaryKey() {
		return _batchFee.getPrimaryKey();
	}

	/**
	* Sets the primary key of this batch fee.
	*
	* @param primaryKey the primary key of this batch fee
	*/
	public void setPrimaryKey(long primaryKey) {
		_batchFee.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the batch fee ID of this batch fee.
	*
	* @return the batch fee ID of this batch fee
	*/
	public long getBatchFeeId() {
		return _batchFee.getBatchFeeId();
	}

	/**
	* Sets the batch fee ID of this batch fee.
	*
	* @param batchFeeId the batch fee ID of this batch fee
	*/
	public void setBatchFeeId(long batchFeeId) {
		_batchFee.setBatchFeeId(batchFeeId);
	}

	/**
	* Returns the company ID of this batch fee.
	*
	* @return the company ID of this batch fee
	*/
	public long getCompanyId() {
		return _batchFee.getCompanyId();
	}

	/**
	* Sets the company ID of this batch fee.
	*
	* @param companyId the company ID of this batch fee
	*/
	public void setCompanyId(long companyId) {
		_batchFee.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this batch fee.
	*
	* @return the user ID of this batch fee
	*/
	public long getUserId() {
		return _batchFee.getUserId();
	}

	/**
	* Sets the user ID of this batch fee.
	*
	* @param userId the user ID of this batch fee
	*/
	public void setUserId(long userId) {
		_batchFee.setUserId(userId);
	}

	/**
	* Returns the user uuid of this batch fee.
	*
	* @return the user uuid of this batch fee
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _batchFee.getUserUuid();
	}

	/**
	* Sets the user uuid of this batch fee.
	*
	* @param userUuid the user uuid of this batch fee
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_batchFee.setUserUuid(userUuid);
	}

	/**
	* Returns the create date of this batch fee.
	*
	* @return the create date of this batch fee
	*/
	public java.util.Date getCreateDate() {
		return _batchFee.getCreateDate();
	}

	/**
	* Sets the create date of this batch fee.
	*
	* @param createDate the create date of this batch fee
	*/
	public void setCreateDate(java.util.Date createDate) {
		_batchFee.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this batch fee.
	*
	* @return the modified date of this batch fee
	*/
	public java.util.Date getModifiedDate() {
		return _batchFee.getModifiedDate();
	}

	/**
	* Sets the modified date of this batch fee.
	*
	* @param modifiedDate the modified date of this batch fee
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_batchFee.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the batch ID of this batch fee.
	*
	* @return the batch ID of this batch fee
	*/
	public long getBatchId() {
		return _batchFee.getBatchId();
	}

	/**
	* Sets the batch ID of this batch fee.
	*
	* @param batchId the batch ID of this batch fee
	*/
	public void setBatchId(long batchId) {
		_batchFee.setBatchId(batchId);
	}

	/**
	* Returns the fee type ID of this batch fee.
	*
	* @return the fee type ID of this batch fee
	*/
	public long getFeeTypeId() {
		return _batchFee.getFeeTypeId();
	}

	/**
	* Sets the fee type ID of this batch fee.
	*
	* @param feeTypeId the fee type ID of this batch fee
	*/
	public void setFeeTypeId(long feeTypeId) {
		_batchFee.setFeeTypeId(feeTypeId);
	}

	/**
	* Returns the amount of this batch fee.
	*
	* @return the amount of this batch fee
	*/
	public double getAmount() {
		return _batchFee.getAmount();
	}

	/**
	* Sets the amount of this batch fee.
	*
	* @param amount the amount of this batch fee
	*/
	public void setAmount(double amount) {
		_batchFee.setAmount(amount);
	}

	public boolean isNew() {
		return _batchFee.isNew();
	}

	public void setNew(boolean n) {
		_batchFee.setNew(n);
	}

	public boolean isCachedModel() {
		return _batchFee.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_batchFee.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _batchFee.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _batchFee.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_batchFee.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _batchFee.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_batchFee.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new BatchFeeWrapper((BatchFee)_batchFee.clone());
	}

	public int compareTo(info.diit.portal.model.BatchFee batchFee) {
		return _batchFee.compareTo(batchFee);
	}

	@Override
	public int hashCode() {
		return _batchFee.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.BatchFee> toCacheModel() {
		return _batchFee.toCacheModel();
	}

	public info.diit.portal.model.BatchFee toEscapedModel() {
		return new BatchFeeWrapper(_batchFee.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _batchFee.toString();
	}

	public java.lang.String toXmlString() {
		return _batchFee.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_batchFee.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public BatchFee getWrappedBatchFee() {
		return _batchFee;
	}

	public BatchFee getWrappedModel() {
		return _batchFee;
	}

	public void resetOriginalValues() {
		_batchFee.resetOriginalValues();
	}

	private BatchFee _batchFee;
}
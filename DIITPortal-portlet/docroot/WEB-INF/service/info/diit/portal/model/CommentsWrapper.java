/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Comments}.
 * </p>
 *
 * @author    mohammad
 * @see       Comments
 * @generated
 */
public class CommentsWrapper implements Comments, ModelWrapper<Comments> {
	public CommentsWrapper(Comments comments) {
		_comments = comments;
	}

	public Class<?> getModelClass() {
		return Comments.class;
	}

	public String getModelClassName() {
		return Comments.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("commentId", getCommentId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("batchId", getBatchId());
		attributes.put("studentId", getStudentId());
		attributes.put("commentDate", getCommentDate());
		attributes.put("comments", getComments());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long commentId = (Long)attributes.get("commentId");

		if (commentId != null) {
			setCommentId(commentId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Date commentDate = (Date)attributes.get("commentDate");

		if (commentDate != null) {
			setCommentDate(commentDate);
		}

		String comments = (String)attributes.get("comments");

		if (comments != null) {
			setComments(comments);
		}
	}

	/**
	* Returns the primary key of this comments.
	*
	* @return the primary key of this comments
	*/
	public long getPrimaryKey() {
		return _comments.getPrimaryKey();
	}

	/**
	* Sets the primary key of this comments.
	*
	* @param primaryKey the primary key of this comments
	*/
	public void setPrimaryKey(long primaryKey) {
		_comments.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the comment ID of this comments.
	*
	* @return the comment ID of this comments
	*/
	public long getCommentId() {
		return _comments.getCommentId();
	}

	/**
	* Sets the comment ID of this comments.
	*
	* @param commentId the comment ID of this comments
	*/
	public void setCommentId(long commentId) {
		_comments.setCommentId(commentId);
	}

	/**
	* Returns the company ID of this comments.
	*
	* @return the company ID of this comments
	*/
	public long getCompanyId() {
		return _comments.getCompanyId();
	}

	/**
	* Sets the company ID of this comments.
	*
	* @param companyId the company ID of this comments
	*/
	public void setCompanyId(long companyId) {
		_comments.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this comments.
	*
	* @return the organization ID of this comments
	*/
	public long getOrganizationId() {
		return _comments.getOrganizationId();
	}

	/**
	* Sets the organization ID of this comments.
	*
	* @param organizationId the organization ID of this comments
	*/
	public void setOrganizationId(long organizationId) {
		_comments.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this comments.
	*
	* @return the user ID of this comments
	*/
	public long getUserId() {
		return _comments.getUserId();
	}

	/**
	* Sets the user ID of this comments.
	*
	* @param userId the user ID of this comments
	*/
	public void setUserId(long userId) {
		_comments.setUserId(userId);
	}

	/**
	* Returns the user uuid of this comments.
	*
	* @return the user uuid of this comments
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _comments.getUserUuid();
	}

	/**
	* Sets the user uuid of this comments.
	*
	* @param userUuid the user uuid of this comments
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_comments.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this comments.
	*
	* @return the user name of this comments
	*/
	public java.lang.String getUserName() {
		return _comments.getUserName();
	}

	/**
	* Sets the user name of this comments.
	*
	* @param userName the user name of this comments
	*/
	public void setUserName(java.lang.String userName) {
		_comments.setUserName(userName);
	}

	/**
	* Returns the create date of this comments.
	*
	* @return the create date of this comments
	*/
	public java.util.Date getCreateDate() {
		return _comments.getCreateDate();
	}

	/**
	* Sets the create date of this comments.
	*
	* @param createDate the create date of this comments
	*/
	public void setCreateDate(java.util.Date createDate) {
		_comments.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this comments.
	*
	* @return the modified date of this comments
	*/
	public java.util.Date getModifiedDate() {
		return _comments.getModifiedDate();
	}

	/**
	* Sets the modified date of this comments.
	*
	* @param modifiedDate the modified date of this comments
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_comments.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the batch ID of this comments.
	*
	* @return the batch ID of this comments
	*/
	public long getBatchId() {
		return _comments.getBatchId();
	}

	/**
	* Sets the batch ID of this comments.
	*
	* @param batchId the batch ID of this comments
	*/
	public void setBatchId(long batchId) {
		_comments.setBatchId(batchId);
	}

	/**
	* Returns the student ID of this comments.
	*
	* @return the student ID of this comments
	*/
	public long getStudentId() {
		return _comments.getStudentId();
	}

	/**
	* Sets the student ID of this comments.
	*
	* @param studentId the student ID of this comments
	*/
	public void setStudentId(long studentId) {
		_comments.setStudentId(studentId);
	}

	/**
	* Returns the comment date of this comments.
	*
	* @return the comment date of this comments
	*/
	public java.util.Date getCommentDate() {
		return _comments.getCommentDate();
	}

	/**
	* Sets the comment date of this comments.
	*
	* @param commentDate the comment date of this comments
	*/
	public void setCommentDate(java.util.Date commentDate) {
		_comments.setCommentDate(commentDate);
	}

	/**
	* Returns the comments of this comments.
	*
	* @return the comments of this comments
	*/
	public java.lang.String getComments() {
		return _comments.getComments();
	}

	/**
	* Sets the comments of this comments.
	*
	* @param comments the comments of this comments
	*/
	public void setComments(java.lang.String comments) {
		_comments.setComments(comments);
	}

	public boolean isNew() {
		return _comments.isNew();
	}

	public void setNew(boolean n) {
		_comments.setNew(n);
	}

	public boolean isCachedModel() {
		return _comments.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_comments.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _comments.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _comments.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_comments.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _comments.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_comments.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CommentsWrapper((Comments)_comments.clone());
	}

	public int compareTo(info.diit.portal.model.Comments comments) {
		return _comments.compareTo(comments);
	}

	@Override
	public int hashCode() {
		return _comments.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.Comments> toCacheModel() {
		return _comments.toCacheModel();
	}

	public info.diit.portal.model.Comments toEscapedModel() {
		return new CommentsWrapper(_comments.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _comments.toString();
	}

	public java.lang.String toXmlString() {
		return _comments.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_comments.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Comments getWrappedComments() {
		return _comments;
	}

	public Comments getWrappedModel() {
		return _comments;
	}

	public void resetOriginalValues() {
		_comments.resetOriginalValues();
	}

	private Comments _comments;
}
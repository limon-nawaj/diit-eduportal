/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.service.LeaveLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class LeaveClp extends BaseModelImpl<Leave> implements Leave {
	public LeaveClp() {
	}

	public Class<?> getModelClass() {
		return Leave.class;
	}

	public String getModelClassName() {
		return Leave.class.getName();
	}

	public long getPrimaryKey() {
		return _leaveId;
	}

	public void setPrimaryKey(long primaryKey) {
		setLeaveId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_leaveId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("leaveId", getLeaveId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("applicationDate", getApplicationDate());
		attributes.put("employee", getEmployee());
		attributes.put("responsibleEmployee", getResponsibleEmployee());
		attributes.put("startDate", getStartDate());
		attributes.put("endDate", getEndDate());
		attributes.put("numberOfDay", getNumberOfDay());
		attributes.put("phoneNumber", getPhoneNumber());
		attributes.put("causeOfLeave", getCauseOfLeave());
		attributes.put("whereEnjoy", getWhereEnjoy());
		attributes.put("firstRecommendation", getFirstRecommendation());
		attributes.put("secondRecommendation", getSecondRecommendation());
		attributes.put("leaveCategory", getLeaveCategory());
		attributes.put("responsibleEmployeeStatus",
			getResponsibleEmployeeStatus());
		attributes.put("applicationStatus", getApplicationStatus());
		attributes.put("comments", getComments());
		attributes.put("complementedBy", getComplementedBy());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long leaveId = (Long)attributes.get("leaveId");

		if (leaveId != null) {
			setLeaveId(leaveId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date applicationDate = (Date)attributes.get("applicationDate");

		if (applicationDate != null) {
			setApplicationDate(applicationDate);
		}

		Long employee = (Long)attributes.get("employee");

		if (employee != null) {
			setEmployee(employee);
		}

		Long responsibleEmployee = (Long)attributes.get("responsibleEmployee");

		if (responsibleEmployee != null) {
			setResponsibleEmployee(responsibleEmployee);
		}

		Date startDate = (Date)attributes.get("startDate");

		if (startDate != null) {
			setStartDate(startDate);
		}

		Date endDate = (Date)attributes.get("endDate");

		if (endDate != null) {
			setEndDate(endDate);
		}

		Double numberOfDay = (Double)attributes.get("numberOfDay");

		if (numberOfDay != null) {
			setNumberOfDay(numberOfDay);
		}

		String phoneNumber = (String)attributes.get("phoneNumber");

		if (phoneNumber != null) {
			setPhoneNumber(phoneNumber);
		}

		String causeOfLeave = (String)attributes.get("causeOfLeave");

		if (causeOfLeave != null) {
			setCauseOfLeave(causeOfLeave);
		}

		String whereEnjoy = (String)attributes.get("whereEnjoy");

		if (whereEnjoy != null) {
			setWhereEnjoy(whereEnjoy);
		}

		Long firstRecommendation = (Long)attributes.get("firstRecommendation");

		if (firstRecommendation != null) {
			setFirstRecommendation(firstRecommendation);
		}

		Long secondRecommendation = (Long)attributes.get("secondRecommendation");

		if (secondRecommendation != null) {
			setSecondRecommendation(secondRecommendation);
		}

		Long leaveCategory = (Long)attributes.get("leaveCategory");

		if (leaveCategory != null) {
			setLeaveCategory(leaveCategory);
		}

		Integer responsibleEmployeeStatus = (Integer)attributes.get(
				"responsibleEmployeeStatus");

		if (responsibleEmployeeStatus != null) {
			setResponsibleEmployeeStatus(responsibleEmployeeStatus);
		}

		Integer applicationStatus = (Integer)attributes.get("applicationStatus");

		if (applicationStatus != null) {
			setApplicationStatus(applicationStatus);
		}

		String comments = (String)attributes.get("comments");

		if (comments != null) {
			setComments(comments);
		}

		Long complementedBy = (Long)attributes.get("complementedBy");

		if (complementedBy != null) {
			setComplementedBy(complementedBy);
		}
	}

	public long getLeaveId() {
		return _leaveId;
	}

	public void setLeaveId(long leaveId) {
		_leaveId = leaveId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getApplicationDate() {
		return _applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		_applicationDate = applicationDate;
	}

	public long getEmployee() {
		return _employee;
	}

	public void setEmployee(long employee) {
		_employee = employee;
	}

	public long getResponsibleEmployee() {
		return _responsibleEmployee;
	}

	public void setResponsibleEmployee(long responsibleEmployee) {
		_responsibleEmployee = responsibleEmployee;
	}

	public Date getStartDate() {
		return _startDate;
	}

	public void setStartDate(Date startDate) {
		_startDate = startDate;
	}

	public Date getEndDate() {
		return _endDate;
	}

	public void setEndDate(Date endDate) {
		_endDate = endDate;
	}

	public double getNumberOfDay() {
		return _numberOfDay;
	}

	public void setNumberOfDay(double numberOfDay) {
		_numberOfDay = numberOfDay;
	}

	public String getPhoneNumber() {
		return _phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		_phoneNumber = phoneNumber;
	}

	public String getCauseOfLeave() {
		return _causeOfLeave;
	}

	public void setCauseOfLeave(String causeOfLeave) {
		_causeOfLeave = causeOfLeave;
	}

	public String getWhereEnjoy() {
		return _whereEnjoy;
	}

	public void setWhereEnjoy(String whereEnjoy) {
		_whereEnjoy = whereEnjoy;
	}

	public long getFirstRecommendation() {
		return _firstRecommendation;
	}

	public void setFirstRecommendation(long firstRecommendation) {
		_firstRecommendation = firstRecommendation;
	}

	public long getSecondRecommendation() {
		return _secondRecommendation;
	}

	public void setSecondRecommendation(long secondRecommendation) {
		_secondRecommendation = secondRecommendation;
	}

	public long getLeaveCategory() {
		return _leaveCategory;
	}

	public void setLeaveCategory(long leaveCategory) {
		_leaveCategory = leaveCategory;
	}

	public int getResponsibleEmployeeStatus() {
		return _responsibleEmployeeStatus;
	}

	public void setResponsibleEmployeeStatus(int responsibleEmployeeStatus) {
		_responsibleEmployeeStatus = responsibleEmployeeStatus;
	}

	public int getApplicationStatus() {
		return _applicationStatus;
	}

	public void setApplicationStatus(int applicationStatus) {
		_applicationStatus = applicationStatus;
	}

	public String getComments() {
		return _comments;
	}

	public void setComments(String comments) {
		_comments = comments;
	}

	public long getComplementedBy() {
		return _complementedBy;
	}

	public void setComplementedBy(long complementedBy) {
		_complementedBy = complementedBy;
	}

	public BaseModel<?> getLeaveRemoteModel() {
		return _leaveRemoteModel;
	}

	public void setLeaveRemoteModel(BaseModel<?> leaveRemoteModel) {
		_leaveRemoteModel = leaveRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			LeaveLocalServiceUtil.addLeave(this);
		}
		else {
			LeaveLocalServiceUtil.updateLeave(this);
		}
	}

	@Override
	public Leave toEscapedModel() {
		return (Leave)Proxy.newProxyInstance(Leave.class.getClassLoader(),
			new Class[] { Leave.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		LeaveClp clone = new LeaveClp();

		clone.setLeaveId(getLeaveId());
		clone.setCompanyId(getCompanyId());
		clone.setOrganizationId(getOrganizationId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setApplicationDate(getApplicationDate());
		clone.setEmployee(getEmployee());
		clone.setResponsibleEmployee(getResponsibleEmployee());
		clone.setStartDate(getStartDate());
		clone.setEndDate(getEndDate());
		clone.setNumberOfDay(getNumberOfDay());
		clone.setPhoneNumber(getPhoneNumber());
		clone.setCauseOfLeave(getCauseOfLeave());
		clone.setWhereEnjoy(getWhereEnjoy());
		clone.setFirstRecommendation(getFirstRecommendation());
		clone.setSecondRecommendation(getSecondRecommendation());
		clone.setLeaveCategory(getLeaveCategory());
		clone.setResponsibleEmployeeStatus(getResponsibleEmployeeStatus());
		clone.setApplicationStatus(getApplicationStatus());
		clone.setComments(getComments());
		clone.setComplementedBy(getComplementedBy());

		return clone;
	}

	public int compareTo(Leave leave) {
		long primaryKey = leave.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		LeaveClp leave = null;

		try {
			leave = (LeaveClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = leave.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(47);

		sb.append("{leaveId=");
		sb.append(getLeaveId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", applicationDate=");
		sb.append(getApplicationDate());
		sb.append(", employee=");
		sb.append(getEmployee());
		sb.append(", responsibleEmployee=");
		sb.append(getResponsibleEmployee());
		sb.append(", startDate=");
		sb.append(getStartDate());
		sb.append(", endDate=");
		sb.append(getEndDate());
		sb.append(", numberOfDay=");
		sb.append(getNumberOfDay());
		sb.append(", phoneNumber=");
		sb.append(getPhoneNumber());
		sb.append(", causeOfLeave=");
		sb.append(getCauseOfLeave());
		sb.append(", whereEnjoy=");
		sb.append(getWhereEnjoy());
		sb.append(", firstRecommendation=");
		sb.append(getFirstRecommendation());
		sb.append(", secondRecommendation=");
		sb.append(getSecondRecommendation());
		sb.append(", leaveCategory=");
		sb.append(getLeaveCategory());
		sb.append(", responsibleEmployeeStatus=");
		sb.append(getResponsibleEmployeeStatus());
		sb.append(", applicationStatus=");
		sb.append(getApplicationStatus());
		sb.append(", comments=");
		sb.append(getComments());
		sb.append(", complementedBy=");
		sb.append(getComplementedBy());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(73);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.Leave");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>leaveId</column-name><column-value><![CDATA[");
		sb.append(getLeaveId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>applicationDate</column-name><column-value><![CDATA[");
		sb.append(getApplicationDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>employee</column-name><column-value><![CDATA[");
		sb.append(getEmployee());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>responsibleEmployee</column-name><column-value><![CDATA[");
		sb.append(getResponsibleEmployee());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>startDate</column-name><column-value><![CDATA[");
		sb.append(getStartDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>endDate</column-name><column-value><![CDATA[");
		sb.append(getEndDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numberOfDay</column-name><column-value><![CDATA[");
		sb.append(getNumberOfDay());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>phoneNumber</column-name><column-value><![CDATA[");
		sb.append(getPhoneNumber());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>causeOfLeave</column-name><column-value><![CDATA[");
		sb.append(getCauseOfLeave());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>whereEnjoy</column-name><column-value><![CDATA[");
		sb.append(getWhereEnjoy());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>firstRecommendation</column-name><column-value><![CDATA[");
		sb.append(getFirstRecommendation());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>secondRecommendation</column-name><column-value><![CDATA[");
		sb.append(getSecondRecommendation());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>leaveCategory</column-name><column-value><![CDATA[");
		sb.append(getLeaveCategory());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>responsibleEmployeeStatus</column-name><column-value><![CDATA[");
		sb.append(getResponsibleEmployeeStatus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>applicationStatus</column-name><column-value><![CDATA[");
		sb.append(getApplicationStatus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>comments</column-name><column-value><![CDATA[");
		sb.append(getComments());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>complementedBy</column-name><column-value><![CDATA[");
		sb.append(getComplementedBy());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _leaveId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _applicationDate;
	private long _employee;
	private long _responsibleEmployee;
	private Date _startDate;
	private Date _endDate;
	private double _numberOfDay;
	private String _phoneNumber;
	private String _causeOfLeave;
	private String _whereEnjoy;
	private long _firstRecommendation;
	private long _secondRecommendation;
	private long _leaveCategory;
	private int _responsibleEmployeeStatus;
	private int _applicationStatus;
	private String _comments;
	private long _complementedBy;
	private BaseModel<?> _leaveRemoteModel;
}
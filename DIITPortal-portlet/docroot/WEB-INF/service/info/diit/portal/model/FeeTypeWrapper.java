/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link FeeType}.
 * </p>
 *
 * @author    mohammad
 * @see       FeeType
 * @generated
 */
public class FeeTypeWrapper implements FeeType, ModelWrapper<FeeType> {
	public FeeTypeWrapper(FeeType feeType) {
		_feeType = feeType;
	}

	public Class<?> getModelClass() {
		return FeeType.class;
	}

	public String getModelClassName() {
		return FeeType.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("feeTypeId", getFeeTypeId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("typeName", getTypeName());
		attributes.put("description", getDescription());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long feeTypeId = (Long)attributes.get("feeTypeId");

		if (feeTypeId != null) {
			setFeeTypeId(feeTypeId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String typeName = (String)attributes.get("typeName");

		if (typeName != null) {
			setTypeName(typeName);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}
	}

	/**
	* Returns the primary key of this fee type.
	*
	* @return the primary key of this fee type
	*/
	public long getPrimaryKey() {
		return _feeType.getPrimaryKey();
	}

	/**
	* Sets the primary key of this fee type.
	*
	* @param primaryKey the primary key of this fee type
	*/
	public void setPrimaryKey(long primaryKey) {
		_feeType.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the fee type ID of this fee type.
	*
	* @return the fee type ID of this fee type
	*/
	public long getFeeTypeId() {
		return _feeType.getFeeTypeId();
	}

	/**
	* Sets the fee type ID of this fee type.
	*
	* @param feeTypeId the fee type ID of this fee type
	*/
	public void setFeeTypeId(long feeTypeId) {
		_feeType.setFeeTypeId(feeTypeId);
	}

	/**
	* Returns the company ID of this fee type.
	*
	* @return the company ID of this fee type
	*/
	public long getCompanyId() {
		return _feeType.getCompanyId();
	}

	/**
	* Sets the company ID of this fee type.
	*
	* @param companyId the company ID of this fee type
	*/
	public void setCompanyId(long companyId) {
		_feeType.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this fee type.
	*
	* @return the user ID of this fee type
	*/
	public long getUserId() {
		return _feeType.getUserId();
	}

	/**
	* Sets the user ID of this fee type.
	*
	* @param userId the user ID of this fee type
	*/
	public void setUserId(long userId) {
		_feeType.setUserId(userId);
	}

	/**
	* Returns the user uuid of this fee type.
	*
	* @return the user uuid of this fee type
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _feeType.getUserUuid();
	}

	/**
	* Sets the user uuid of this fee type.
	*
	* @param userUuid the user uuid of this fee type
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_feeType.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this fee type.
	*
	* @return the user name of this fee type
	*/
	public java.lang.String getUserName() {
		return _feeType.getUserName();
	}

	/**
	* Sets the user name of this fee type.
	*
	* @param userName the user name of this fee type
	*/
	public void setUserName(java.lang.String userName) {
		_feeType.setUserName(userName);
	}

	/**
	* Returns the create date of this fee type.
	*
	* @return the create date of this fee type
	*/
	public java.util.Date getCreateDate() {
		return _feeType.getCreateDate();
	}

	/**
	* Sets the create date of this fee type.
	*
	* @param createDate the create date of this fee type
	*/
	public void setCreateDate(java.util.Date createDate) {
		_feeType.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this fee type.
	*
	* @return the modified date of this fee type
	*/
	public java.util.Date getModifiedDate() {
		return _feeType.getModifiedDate();
	}

	/**
	* Sets the modified date of this fee type.
	*
	* @param modifiedDate the modified date of this fee type
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_feeType.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the type name of this fee type.
	*
	* @return the type name of this fee type
	*/
	public java.lang.String getTypeName() {
		return _feeType.getTypeName();
	}

	/**
	* Sets the type name of this fee type.
	*
	* @param typeName the type name of this fee type
	*/
	public void setTypeName(java.lang.String typeName) {
		_feeType.setTypeName(typeName);
	}

	/**
	* Returns the description of this fee type.
	*
	* @return the description of this fee type
	*/
	public java.lang.String getDescription() {
		return _feeType.getDescription();
	}

	/**
	* Sets the description of this fee type.
	*
	* @param description the description of this fee type
	*/
	public void setDescription(java.lang.String description) {
		_feeType.setDescription(description);
	}

	public boolean isNew() {
		return _feeType.isNew();
	}

	public void setNew(boolean n) {
		_feeType.setNew(n);
	}

	public boolean isCachedModel() {
		return _feeType.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_feeType.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _feeType.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _feeType.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_feeType.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _feeType.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_feeType.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new FeeTypeWrapper((FeeType)_feeType.clone());
	}

	public int compareTo(info.diit.portal.model.FeeType feeType) {
		return _feeType.compareTo(feeType);
	}

	@Override
	public int hashCode() {
		return _feeType.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.FeeType> toCacheModel() {
		return _feeType.toCacheModel();
	}

	public info.diit.portal.model.FeeType toEscapedModel() {
		return new FeeTypeWrapper(_feeType.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _feeType.toString();
	}

	public java.lang.String toXmlString() {
		return _feeType.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_feeType.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public FeeType getWrappedFeeType() {
		return _feeType;
	}

	public FeeType getWrappedModel() {
		return _feeType;
	}

	public void resetOriginalValues() {
		_feeType.resetOriginalValues();
	}

	private FeeType _feeType;
}
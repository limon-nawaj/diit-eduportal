/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import info.diit.portal.service.CounselingCourseInterestLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class CounselingCourseInterestClp extends BaseModelImpl<CounselingCourseInterest>
	implements CounselingCourseInterest {
	public CounselingCourseInterestClp() {
	}

	public Class<?> getModelClass() {
		return CounselingCourseInterest.class;
	}

	public String getModelClassName() {
		return CounselingCourseInterest.class.getName();
	}

	public long getPrimaryKey() {
		return _CounselingCourseInterestId;
	}

	public void setPrimaryKey(long primaryKey) {
		setCounselingCourseInterestId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_CounselingCourseInterestId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("CounselingCourseInterestId",
			getCounselingCourseInterestId());
		attributes.put("courseId", getCourseId());
		attributes.put("counselingId", getCounselingId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long CounselingCourseInterestId = (Long)attributes.get(
				"CounselingCourseInterestId");

		if (CounselingCourseInterestId != null) {
			setCounselingCourseInterestId(CounselingCourseInterestId);
		}

		Long courseId = (Long)attributes.get("courseId");

		if (courseId != null) {
			setCourseId(courseId);
		}

		Long counselingId = (Long)attributes.get("counselingId");

		if (counselingId != null) {
			setCounselingId(counselingId);
		}
	}

	public long getCounselingCourseInterestId() {
		return _CounselingCourseInterestId;
	}

	public void setCounselingCourseInterestId(long CounselingCourseInterestId) {
		_CounselingCourseInterestId = CounselingCourseInterestId;
	}

	public long getCourseId() {
		return _courseId;
	}

	public void setCourseId(long courseId) {
		_courseId = courseId;
	}

	public long getCounselingId() {
		return _counselingId;
	}

	public void setCounselingId(long counselingId) {
		_counselingId = counselingId;
	}

	public BaseModel<?> getCounselingCourseInterestRemoteModel() {
		return _counselingCourseInterestRemoteModel;
	}

	public void setCounselingCourseInterestRemoteModel(
		BaseModel<?> counselingCourseInterestRemoteModel) {
		_counselingCourseInterestRemoteModel = counselingCourseInterestRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			CounselingCourseInterestLocalServiceUtil.addCounselingCourseInterest(this);
		}
		else {
			CounselingCourseInterestLocalServiceUtil.updateCounselingCourseInterest(this);
		}
	}

	@Override
	public CounselingCourseInterest toEscapedModel() {
		return (CounselingCourseInterest)Proxy.newProxyInstance(CounselingCourseInterest.class.getClassLoader(),
			new Class[] { CounselingCourseInterest.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CounselingCourseInterestClp clone = new CounselingCourseInterestClp();

		clone.setCounselingCourseInterestId(getCounselingCourseInterestId());
		clone.setCourseId(getCourseId());
		clone.setCounselingId(getCounselingId());

		return clone;
	}

	public int compareTo(CounselingCourseInterest counselingCourseInterest) {
		long primaryKey = counselingCourseInterest.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		CounselingCourseInterestClp counselingCourseInterest = null;

		try {
			counselingCourseInterest = (CounselingCourseInterestClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = counselingCourseInterest.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{CounselingCourseInterestId=");
		sb.append(getCounselingCourseInterestId());
		sb.append(", courseId=");
		sb.append(getCourseId());
		sb.append(", counselingId=");
		sb.append(getCounselingId());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.CounselingCourseInterest");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>CounselingCourseInterestId</column-name><column-value><![CDATA[");
		sb.append(getCounselingCourseInterestId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>courseId</column-name><column-value><![CDATA[");
		sb.append(getCourseId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>counselingId</column-name><column-value><![CDATA[");
		sb.append(getCounselingId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _CounselingCourseInterestId;
	private long _courseId;
	private long _counselingId;
	private BaseModel<?> _counselingCourseInterestRemoteModel;
}
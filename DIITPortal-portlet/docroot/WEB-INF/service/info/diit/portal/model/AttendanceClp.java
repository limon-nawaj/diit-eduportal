/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.service.AttendanceLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class AttendanceClp extends BaseModelImpl<Attendance>
	implements Attendance {
	public AttendanceClp() {
	}

	public Class<?> getModelClass() {
		return Attendance.class;
	}

	public String getModelClassName() {
		return Attendance.class.getName();
	}

	public long getPrimaryKey() {
		return _attendanceId;
	}

	public void setPrimaryKey(long primaryKey) {
		setAttendanceId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_attendanceId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("attendanceId", getAttendanceId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("campus", getCampus());
		attributes.put("attendanceDate", getAttendanceDate());
		attributes.put("attendanceRate", getAttendanceRate());
		attributes.put("batch", getBatch());
		attributes.put("subject", getSubject());
		attributes.put("routineIn", getRoutineIn());
		attributes.put("routineOut", getRoutineOut());
		attributes.put("actualIn", getActualIn());
		attributes.put("actualOut", getActualOut());
		attributes.put("routineDuration", getRoutineDuration());
		attributes.put("actualDuration", getActualDuration());
		attributes.put("lateEntry", getLateEntry());
		attributes.put("lagTime", getLagTime());
		attributes.put("note", getNote());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long attendanceId = (Long)attributes.get("attendanceId");

		if (attendanceId != null) {
			setAttendanceId(attendanceId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long campus = (Long)attributes.get("campus");

		if (campus != null) {
			setCampus(campus);
		}

		Date attendanceDate = (Date)attributes.get("attendanceDate");

		if (attendanceDate != null) {
			setAttendanceDate(attendanceDate);
		}

		Long attendanceRate = (Long)attributes.get("attendanceRate");

		if (attendanceRate != null) {
			setAttendanceRate(attendanceRate);
		}

		Long batch = (Long)attributes.get("batch");

		if (batch != null) {
			setBatch(batch);
		}

		Long subject = (Long)attributes.get("subject");

		if (subject != null) {
			setSubject(subject);
		}

		Date routineIn = (Date)attributes.get("routineIn");

		if (routineIn != null) {
			setRoutineIn(routineIn);
		}

		Date routineOut = (Date)attributes.get("routineOut");

		if (routineOut != null) {
			setRoutineOut(routineOut);
		}

		Date actualIn = (Date)attributes.get("actualIn");

		if (actualIn != null) {
			setActualIn(actualIn);
		}

		Date actualOut = (Date)attributes.get("actualOut");

		if (actualOut != null) {
			setActualOut(actualOut);
		}

		Long routineDuration = (Long)attributes.get("routineDuration");

		if (routineDuration != null) {
			setRoutineDuration(routineDuration);
		}

		Long actualDuration = (Long)attributes.get("actualDuration");

		if (actualDuration != null) {
			setActualDuration(actualDuration);
		}

		Long lateEntry = (Long)attributes.get("lateEntry");

		if (lateEntry != null) {
			setLateEntry(lateEntry);
		}

		Long lagTime = (Long)attributes.get("lagTime");

		if (lagTime != null) {
			setLagTime(lagTime);
		}

		String note = (String)attributes.get("note");

		if (note != null) {
			setNote(note);
		}
	}

	public long getAttendanceId() {
		return _attendanceId;
	}

	public void setAttendanceId(long attendanceId) {
		_attendanceId = attendanceId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getCampus() {
		return _campus;
	}

	public void setCampus(long campus) {
		_campus = campus;
	}

	public Date getAttendanceDate() {
		return _attendanceDate;
	}

	public void setAttendanceDate(Date attendanceDate) {
		_attendanceDate = attendanceDate;
	}

	public long getAttendanceRate() {
		return _attendanceRate;
	}

	public void setAttendanceRate(long attendanceRate) {
		_attendanceRate = attendanceRate;
	}

	public long getBatch() {
		return _batch;
	}

	public void setBatch(long batch) {
		_batch = batch;
	}

	public long getSubject() {
		return _subject;
	}

	public void setSubject(long subject) {
		_subject = subject;
	}

	public Date getRoutineIn() {
		return _routineIn;
	}

	public void setRoutineIn(Date routineIn) {
		_routineIn = routineIn;
	}

	public Date getRoutineOut() {
		return _routineOut;
	}

	public void setRoutineOut(Date routineOut) {
		_routineOut = routineOut;
	}

	public Date getActualIn() {
		return _actualIn;
	}

	public void setActualIn(Date actualIn) {
		_actualIn = actualIn;
	}

	public Date getActualOut() {
		return _actualOut;
	}

	public void setActualOut(Date actualOut) {
		_actualOut = actualOut;
	}

	public long getRoutineDuration() {
		return _routineDuration;
	}

	public void setRoutineDuration(long routineDuration) {
		_routineDuration = routineDuration;
	}

	public long getActualDuration() {
		return _actualDuration;
	}

	public void setActualDuration(long actualDuration) {
		_actualDuration = actualDuration;
	}

	public long getLateEntry() {
		return _lateEntry;
	}

	public void setLateEntry(long lateEntry) {
		_lateEntry = lateEntry;
	}

	public long getLagTime() {
		return _lagTime;
	}

	public void setLagTime(long lagTime) {
		_lagTime = lagTime;
	}

	public String getNote() {
		return _note;
	}

	public void setNote(String note) {
		_note = note;
	}

	public BaseModel<?> getAttendanceRemoteModel() {
		return _attendanceRemoteModel;
	}

	public void setAttendanceRemoteModel(BaseModel<?> attendanceRemoteModel) {
		_attendanceRemoteModel = attendanceRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			AttendanceLocalServiceUtil.addAttendance(this);
		}
		else {
			AttendanceLocalServiceUtil.updateAttendance(this);
		}
	}

	@Override
	public Attendance toEscapedModel() {
		return (Attendance)Proxy.newProxyInstance(Attendance.class.getClassLoader(),
			new Class[] { Attendance.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		AttendanceClp clone = new AttendanceClp();

		clone.setAttendanceId(getAttendanceId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setCampus(getCampus());
		clone.setAttendanceDate(getAttendanceDate());
		clone.setAttendanceRate(getAttendanceRate());
		clone.setBatch(getBatch());
		clone.setSubject(getSubject());
		clone.setRoutineIn(getRoutineIn());
		clone.setRoutineOut(getRoutineOut());
		clone.setActualIn(getActualIn());
		clone.setActualOut(getActualOut());
		clone.setRoutineDuration(getRoutineDuration());
		clone.setActualDuration(getActualDuration());
		clone.setLateEntry(getLateEntry());
		clone.setLagTime(getLagTime());
		clone.setNote(getNote());

		return clone;
	}

	public int compareTo(Attendance attendance) {
		long primaryKey = attendance.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		AttendanceClp attendance = null;

		try {
			attendance = (AttendanceClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = attendance.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(41);

		sb.append("{attendanceId=");
		sb.append(getAttendanceId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", campus=");
		sb.append(getCampus());
		sb.append(", attendanceDate=");
		sb.append(getAttendanceDate());
		sb.append(", attendanceRate=");
		sb.append(getAttendanceRate());
		sb.append(", batch=");
		sb.append(getBatch());
		sb.append(", subject=");
		sb.append(getSubject());
		sb.append(", routineIn=");
		sb.append(getRoutineIn());
		sb.append(", routineOut=");
		sb.append(getRoutineOut());
		sb.append(", actualIn=");
		sb.append(getActualIn());
		sb.append(", actualOut=");
		sb.append(getActualOut());
		sb.append(", routineDuration=");
		sb.append(getRoutineDuration());
		sb.append(", actualDuration=");
		sb.append(getActualDuration());
		sb.append(", lateEntry=");
		sb.append(getLateEntry());
		sb.append(", lagTime=");
		sb.append(getLagTime());
		sb.append(", note=");
		sb.append(getNote());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(64);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.Attendance");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>attendanceId</column-name><column-value><![CDATA[");
		sb.append(getAttendanceId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>campus</column-name><column-value><![CDATA[");
		sb.append(getCampus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>attendanceDate</column-name><column-value><![CDATA[");
		sb.append(getAttendanceDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>attendanceRate</column-name><column-value><![CDATA[");
		sb.append(getAttendanceRate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>batch</column-name><column-value><![CDATA[");
		sb.append(getBatch());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>subject</column-name><column-value><![CDATA[");
		sb.append(getSubject());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>routineIn</column-name><column-value><![CDATA[");
		sb.append(getRoutineIn());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>routineOut</column-name><column-value><![CDATA[");
		sb.append(getRoutineOut());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>actualIn</column-name><column-value><![CDATA[");
		sb.append(getActualIn());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>actualOut</column-name><column-value><![CDATA[");
		sb.append(getActualOut());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>routineDuration</column-name><column-value><![CDATA[");
		sb.append(getRoutineDuration());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>actualDuration</column-name><column-value><![CDATA[");
		sb.append(getActualDuration());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lateEntry</column-name><column-value><![CDATA[");
		sb.append(getLateEntry());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lagTime</column-name><column-value><![CDATA[");
		sb.append(getLagTime());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>note</column-name><column-value><![CDATA[");
		sb.append(getNote());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _attendanceId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _campus;
	private Date _attendanceDate;
	private long _attendanceRate;
	private long _batch;
	private long _subject;
	private Date _routineIn;
	private Date _routineOut;
	private Date _actualIn;
	private Date _actualOut;
	private long _routineDuration;
	private long _actualDuration;
	private long _lateEntry;
	private long _lagTime;
	private String _note;
	private BaseModel<?> _attendanceRemoteModel;
}
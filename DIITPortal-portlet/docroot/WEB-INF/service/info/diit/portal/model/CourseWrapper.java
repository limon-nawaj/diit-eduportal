/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Course}.
 * </p>
 *
 * @author    mohammad
 * @see       Course
 * @generated
 */
public class CourseWrapper implements Course, ModelWrapper<Course> {
	public CourseWrapper(Course course) {
		_course = course;
	}

	public Class<?> getModelClass() {
		return Course.class;
	}

	public String getModelClassName() {
		return Course.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("courseId", getCourseId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("courseCode", getCourseCode());
		attributes.put("courseName", getCourseName());
		attributes.put("description", getDescription());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long courseId = (Long)attributes.get("courseId");

		if (courseId != null) {
			setCourseId(courseId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String courseCode = (String)attributes.get("courseCode");

		if (courseCode != null) {
			setCourseCode(courseCode);
		}

		String courseName = (String)attributes.get("courseName");

		if (courseName != null) {
			setCourseName(courseName);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}
	}

	/**
	* Returns the primary key of this course.
	*
	* @return the primary key of this course
	*/
	public long getPrimaryKey() {
		return _course.getPrimaryKey();
	}

	/**
	* Sets the primary key of this course.
	*
	* @param primaryKey the primary key of this course
	*/
	public void setPrimaryKey(long primaryKey) {
		_course.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the course ID of this course.
	*
	* @return the course ID of this course
	*/
	public long getCourseId() {
		return _course.getCourseId();
	}

	/**
	* Sets the course ID of this course.
	*
	* @param courseId the course ID of this course
	*/
	public void setCourseId(long courseId) {
		_course.setCourseId(courseId);
	}

	/**
	* Returns the company ID of this course.
	*
	* @return the company ID of this course
	*/
	public long getCompanyId() {
		return _course.getCompanyId();
	}

	/**
	* Sets the company ID of this course.
	*
	* @param companyId the company ID of this course
	*/
	public void setCompanyId(long companyId) {
		_course.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this course.
	*
	* @return the organization ID of this course
	*/
	public long getOrganizationId() {
		return _course.getOrganizationId();
	}

	/**
	* Sets the organization ID of this course.
	*
	* @param organizationId the organization ID of this course
	*/
	public void setOrganizationId(long organizationId) {
		_course.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this course.
	*
	* @return the user ID of this course
	*/
	public long getUserId() {
		return _course.getUserId();
	}

	/**
	* Sets the user ID of this course.
	*
	* @param userId the user ID of this course
	*/
	public void setUserId(long userId) {
		_course.setUserId(userId);
	}

	/**
	* Returns the user uuid of this course.
	*
	* @return the user uuid of this course
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _course.getUserUuid();
	}

	/**
	* Sets the user uuid of this course.
	*
	* @param userUuid the user uuid of this course
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_course.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this course.
	*
	* @return the user name of this course
	*/
	public java.lang.String getUserName() {
		return _course.getUserName();
	}

	/**
	* Sets the user name of this course.
	*
	* @param userName the user name of this course
	*/
	public void setUserName(java.lang.String userName) {
		_course.setUserName(userName);
	}

	/**
	* Returns the create date of this course.
	*
	* @return the create date of this course
	*/
	public java.util.Date getCreateDate() {
		return _course.getCreateDate();
	}

	/**
	* Sets the create date of this course.
	*
	* @param createDate the create date of this course
	*/
	public void setCreateDate(java.util.Date createDate) {
		_course.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this course.
	*
	* @return the modified date of this course
	*/
	public java.util.Date getModifiedDate() {
		return _course.getModifiedDate();
	}

	/**
	* Sets the modified date of this course.
	*
	* @param modifiedDate the modified date of this course
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_course.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the course code of this course.
	*
	* @return the course code of this course
	*/
	public java.lang.String getCourseCode() {
		return _course.getCourseCode();
	}

	/**
	* Sets the course code of this course.
	*
	* @param courseCode the course code of this course
	*/
	public void setCourseCode(java.lang.String courseCode) {
		_course.setCourseCode(courseCode);
	}

	/**
	* Returns the course name of this course.
	*
	* @return the course name of this course
	*/
	public java.lang.String getCourseName() {
		return _course.getCourseName();
	}

	/**
	* Sets the course name of this course.
	*
	* @param courseName the course name of this course
	*/
	public void setCourseName(java.lang.String courseName) {
		_course.setCourseName(courseName);
	}

	/**
	* Returns the description of this course.
	*
	* @return the description of this course
	*/
	public java.lang.String getDescription() {
		return _course.getDescription();
	}

	/**
	* Sets the description of this course.
	*
	* @param description the description of this course
	*/
	public void setDescription(java.lang.String description) {
		_course.setDescription(description);
	}

	public boolean isNew() {
		return _course.isNew();
	}

	public void setNew(boolean n) {
		_course.setNew(n);
	}

	public boolean isCachedModel() {
		return _course.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_course.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _course.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _course.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_course.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _course.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_course.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CourseWrapper((Course)_course.clone());
	}

	public int compareTo(info.diit.portal.model.Course course) {
		return _course.compareTo(course);
	}

	@Override
	public int hashCode() {
		return _course.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.Course> toCacheModel() {
		return _course.toCacheModel();
	}

	public info.diit.portal.model.Course toEscapedModel() {
		return new CourseWrapper(_course.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _course.toString();
	}

	public java.lang.String toXmlString() {
		return _course.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_course.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Course getWrappedCourse() {
		return _course;
	}

	public Course getWrappedModel() {
		return _course;
	}

	public void resetOriginalValues() {
		_course.resetOriginalValues();
	}

	private Course _course;
}
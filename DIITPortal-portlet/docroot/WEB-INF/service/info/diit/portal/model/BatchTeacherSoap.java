/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class BatchTeacherSoap implements Serializable {
	public static BatchTeacherSoap toSoapModel(BatchTeacher model) {
		BatchTeacherSoap soapModel = new BatchTeacherSoap();

		soapModel.setBatchTeacherId(model.getBatchTeacherId());
		soapModel.setTeacherId(model.getTeacherId());
		soapModel.setStartDate(model.getStartDate());
		soapModel.setEndDate(model.getEndDate());
		soapModel.setBatchId(model.getBatchId());

		return soapModel;
	}

	public static BatchTeacherSoap[] toSoapModels(BatchTeacher[] models) {
		BatchTeacherSoap[] soapModels = new BatchTeacherSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BatchTeacherSoap[][] toSoapModels(BatchTeacher[][] models) {
		BatchTeacherSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BatchTeacherSoap[models.length][models[0].length];
		}
		else {
			soapModels = new BatchTeacherSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BatchTeacherSoap[] toSoapModels(List<BatchTeacher> models) {
		List<BatchTeacherSoap> soapModels = new ArrayList<BatchTeacherSoap>(models.size());

		for (BatchTeacher model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BatchTeacherSoap[soapModels.size()]);
	}

	public BatchTeacherSoap() {
	}

	public long getPrimaryKey() {
		return _batchTeacherId;
	}

	public void setPrimaryKey(long pk) {
		setBatchTeacherId(pk);
	}

	public long getBatchTeacherId() {
		return _batchTeacherId;
	}

	public void setBatchTeacherId(long batchTeacherId) {
		_batchTeacherId = batchTeacherId;
	}

	public long getTeacherId() {
		return _teacherId;
	}

	public void setTeacherId(long teacherId) {
		_teacherId = teacherId;
	}

	public Date getStartDate() {
		return _startDate;
	}

	public void setStartDate(Date startDate) {
		_startDate = startDate;
	}

	public Date getEndDate() {
		return _endDate;
	}

	public void setEndDate(Date endDate) {
		_endDate = endDate;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	private long _batchTeacherId;
	private long _teacherId;
	private Date _startDate;
	private Date _endDate;
	private long _batchId;
}
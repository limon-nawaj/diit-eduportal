/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link LessonTopic}.
 * </p>
 *
 * @author    mohammad
 * @see       LessonTopic
 * @generated
 */
public class LessonTopicWrapper implements LessonTopic,
	ModelWrapper<LessonTopic> {
	public LessonTopicWrapper(LessonTopic lessonTopic) {
		_lessonTopic = lessonTopic;
	}

	public Class<?> getModelClass() {
		return LessonTopic.class;
	}

	public String getModelClassName() {
		return LessonTopic.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("lessonTopicId", getLessonTopicId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("lessonPlanId", getLessonPlanId());
		attributes.put("topic", getTopic());
		attributes.put("classSequence", getClassSequence());
		attributes.put("resource", getResource());
		attributes.put("activities", getActivities());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long lessonTopicId = (Long)attributes.get("lessonTopicId");

		if (lessonTopicId != null) {
			setLessonTopicId(lessonTopicId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long lessonPlanId = (Long)attributes.get("lessonPlanId");

		if (lessonPlanId != null) {
			setLessonPlanId(lessonPlanId);
		}

		Long topic = (Long)attributes.get("topic");

		if (topic != null) {
			setTopic(topic);
		}

		Long classSequence = (Long)attributes.get("classSequence");

		if (classSequence != null) {
			setClassSequence(classSequence);
		}

		String resource = (String)attributes.get("resource");

		if (resource != null) {
			setResource(resource);
		}

		String activities = (String)attributes.get("activities");

		if (activities != null) {
			setActivities(activities);
		}
	}

	/**
	* Returns the primary key of this lesson topic.
	*
	* @return the primary key of this lesson topic
	*/
	public long getPrimaryKey() {
		return _lessonTopic.getPrimaryKey();
	}

	/**
	* Sets the primary key of this lesson topic.
	*
	* @param primaryKey the primary key of this lesson topic
	*/
	public void setPrimaryKey(long primaryKey) {
		_lessonTopic.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the lesson topic ID of this lesson topic.
	*
	* @return the lesson topic ID of this lesson topic
	*/
	public long getLessonTopicId() {
		return _lessonTopic.getLessonTopicId();
	}

	/**
	* Sets the lesson topic ID of this lesson topic.
	*
	* @param lessonTopicId the lesson topic ID of this lesson topic
	*/
	public void setLessonTopicId(long lessonTopicId) {
		_lessonTopic.setLessonTopicId(lessonTopicId);
	}

	/**
	* Returns the company ID of this lesson topic.
	*
	* @return the company ID of this lesson topic
	*/
	public long getCompanyId() {
		return _lessonTopic.getCompanyId();
	}

	/**
	* Sets the company ID of this lesson topic.
	*
	* @param companyId the company ID of this lesson topic
	*/
	public void setCompanyId(long companyId) {
		_lessonTopic.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this lesson topic.
	*
	* @return the user ID of this lesson topic
	*/
	public long getUserId() {
		return _lessonTopic.getUserId();
	}

	/**
	* Sets the user ID of this lesson topic.
	*
	* @param userId the user ID of this lesson topic
	*/
	public void setUserId(long userId) {
		_lessonTopic.setUserId(userId);
	}

	/**
	* Returns the user uuid of this lesson topic.
	*
	* @return the user uuid of this lesson topic
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopic.getUserUuid();
	}

	/**
	* Sets the user uuid of this lesson topic.
	*
	* @param userUuid the user uuid of this lesson topic
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_lessonTopic.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this lesson topic.
	*
	* @return the user name of this lesson topic
	*/
	public java.lang.String getUserName() {
		return _lessonTopic.getUserName();
	}

	/**
	* Sets the user name of this lesson topic.
	*
	* @param userName the user name of this lesson topic
	*/
	public void setUserName(java.lang.String userName) {
		_lessonTopic.setUserName(userName);
	}

	/**
	* Returns the create date of this lesson topic.
	*
	* @return the create date of this lesson topic
	*/
	public java.util.Date getCreateDate() {
		return _lessonTopic.getCreateDate();
	}

	/**
	* Sets the create date of this lesson topic.
	*
	* @param createDate the create date of this lesson topic
	*/
	public void setCreateDate(java.util.Date createDate) {
		_lessonTopic.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this lesson topic.
	*
	* @return the modified date of this lesson topic
	*/
	public java.util.Date getModifiedDate() {
		return _lessonTopic.getModifiedDate();
	}

	/**
	* Sets the modified date of this lesson topic.
	*
	* @param modifiedDate the modified date of this lesson topic
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_lessonTopic.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the lesson plan ID of this lesson topic.
	*
	* @return the lesson plan ID of this lesson topic
	*/
	public long getLessonPlanId() {
		return _lessonTopic.getLessonPlanId();
	}

	/**
	* Sets the lesson plan ID of this lesson topic.
	*
	* @param lessonPlanId the lesson plan ID of this lesson topic
	*/
	public void setLessonPlanId(long lessonPlanId) {
		_lessonTopic.setLessonPlanId(lessonPlanId);
	}

	/**
	* Returns the topic of this lesson topic.
	*
	* @return the topic of this lesson topic
	*/
	public long getTopic() {
		return _lessonTopic.getTopic();
	}

	/**
	* Sets the topic of this lesson topic.
	*
	* @param topic the topic of this lesson topic
	*/
	public void setTopic(long topic) {
		_lessonTopic.setTopic(topic);
	}

	/**
	* Returns the class sequence of this lesson topic.
	*
	* @return the class sequence of this lesson topic
	*/
	public long getClassSequence() {
		return _lessonTopic.getClassSequence();
	}

	/**
	* Sets the class sequence of this lesson topic.
	*
	* @param classSequence the class sequence of this lesson topic
	*/
	public void setClassSequence(long classSequence) {
		_lessonTopic.setClassSequence(classSequence);
	}

	/**
	* Returns the resource of this lesson topic.
	*
	* @return the resource of this lesson topic
	*/
	public java.lang.String getResource() {
		return _lessonTopic.getResource();
	}

	/**
	* Sets the resource of this lesson topic.
	*
	* @param resource the resource of this lesson topic
	*/
	public void setResource(java.lang.String resource) {
		_lessonTopic.setResource(resource);
	}

	/**
	* Returns the activities of this lesson topic.
	*
	* @return the activities of this lesson topic
	*/
	public java.lang.String getActivities() {
		return _lessonTopic.getActivities();
	}

	/**
	* Sets the activities of this lesson topic.
	*
	* @param activities the activities of this lesson topic
	*/
	public void setActivities(java.lang.String activities) {
		_lessonTopic.setActivities(activities);
	}

	public boolean isNew() {
		return _lessonTopic.isNew();
	}

	public void setNew(boolean n) {
		_lessonTopic.setNew(n);
	}

	public boolean isCachedModel() {
		return _lessonTopic.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_lessonTopic.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _lessonTopic.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _lessonTopic.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_lessonTopic.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _lessonTopic.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_lessonTopic.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LessonTopicWrapper((LessonTopic)_lessonTopic.clone());
	}

	public int compareTo(info.diit.portal.model.LessonTopic lessonTopic) {
		return _lessonTopic.compareTo(lessonTopic);
	}

	@Override
	public int hashCode() {
		return _lessonTopic.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.LessonTopic> toCacheModel() {
		return _lessonTopic.toCacheModel();
	}

	public info.diit.portal.model.LessonTopic toEscapedModel() {
		return new LessonTopicWrapper(_lessonTopic.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _lessonTopic.toString();
	}

	public java.lang.String toXmlString() {
		return _lessonTopic.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_lessonTopic.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public LessonTopic getWrappedLessonTopic() {
		return _lessonTopic;
	}

	public LessonTopic getWrappedModel() {
		return _lessonTopic;
	}

	public void resetOriginalValues() {
		_lessonTopic.resetOriginalValues();
	}

	private LessonTopic _lessonTopic;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AttendanceTopic}.
 * </p>
 *
 * @author    mohammad
 * @see       AttendanceTopic
 * @generated
 */
public class AttendanceTopicWrapper implements AttendanceTopic,
	ModelWrapper<AttendanceTopic> {
	public AttendanceTopicWrapper(AttendanceTopic attendanceTopic) {
		_attendanceTopic = attendanceTopic;
	}

	public Class<?> getModelClass() {
		return AttendanceTopic.class;
	}

	public String getModelClassName() {
		return AttendanceTopic.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("attendanceTopicId", getAttendanceTopicId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("attendanceId", getAttendanceId());
		attributes.put("topicId", getTopicId());
		attributes.put("description", getDescription());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long attendanceTopicId = (Long)attributes.get("attendanceTopicId");

		if (attendanceTopicId != null) {
			setAttendanceTopicId(attendanceTopicId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long attendanceId = (Long)attributes.get("attendanceId");

		if (attendanceId != null) {
			setAttendanceId(attendanceId);
		}

		Long topicId = (Long)attributes.get("topicId");

		if (topicId != null) {
			setTopicId(topicId);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}
	}

	/**
	* Returns the primary key of this attendance topic.
	*
	* @return the primary key of this attendance topic
	*/
	public long getPrimaryKey() {
		return _attendanceTopic.getPrimaryKey();
	}

	/**
	* Sets the primary key of this attendance topic.
	*
	* @param primaryKey the primary key of this attendance topic
	*/
	public void setPrimaryKey(long primaryKey) {
		_attendanceTopic.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the attendance topic ID of this attendance topic.
	*
	* @return the attendance topic ID of this attendance topic
	*/
	public long getAttendanceTopicId() {
		return _attendanceTopic.getAttendanceTopicId();
	}

	/**
	* Sets the attendance topic ID of this attendance topic.
	*
	* @param attendanceTopicId the attendance topic ID of this attendance topic
	*/
	public void setAttendanceTopicId(long attendanceTopicId) {
		_attendanceTopic.setAttendanceTopicId(attendanceTopicId);
	}

	/**
	* Returns the company ID of this attendance topic.
	*
	* @return the company ID of this attendance topic
	*/
	public long getCompanyId() {
		return _attendanceTopic.getCompanyId();
	}

	/**
	* Sets the company ID of this attendance topic.
	*
	* @param companyId the company ID of this attendance topic
	*/
	public void setCompanyId(long companyId) {
		_attendanceTopic.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this attendance topic.
	*
	* @return the user ID of this attendance topic
	*/
	public long getUserId() {
		return _attendanceTopic.getUserId();
	}

	/**
	* Sets the user ID of this attendance topic.
	*
	* @param userId the user ID of this attendance topic
	*/
	public void setUserId(long userId) {
		_attendanceTopic.setUserId(userId);
	}

	/**
	* Returns the user uuid of this attendance topic.
	*
	* @return the user uuid of this attendance topic
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _attendanceTopic.getUserUuid();
	}

	/**
	* Sets the user uuid of this attendance topic.
	*
	* @param userUuid the user uuid of this attendance topic
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_attendanceTopic.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this attendance topic.
	*
	* @return the user name of this attendance topic
	*/
	public java.lang.String getUserName() {
		return _attendanceTopic.getUserName();
	}

	/**
	* Sets the user name of this attendance topic.
	*
	* @param userName the user name of this attendance topic
	*/
	public void setUserName(java.lang.String userName) {
		_attendanceTopic.setUserName(userName);
	}

	/**
	* Returns the create date of this attendance topic.
	*
	* @return the create date of this attendance topic
	*/
	public java.util.Date getCreateDate() {
		return _attendanceTopic.getCreateDate();
	}

	/**
	* Sets the create date of this attendance topic.
	*
	* @param createDate the create date of this attendance topic
	*/
	public void setCreateDate(java.util.Date createDate) {
		_attendanceTopic.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this attendance topic.
	*
	* @return the modified date of this attendance topic
	*/
	public java.util.Date getModifiedDate() {
		return _attendanceTopic.getModifiedDate();
	}

	/**
	* Sets the modified date of this attendance topic.
	*
	* @param modifiedDate the modified date of this attendance topic
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_attendanceTopic.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the attendance ID of this attendance topic.
	*
	* @return the attendance ID of this attendance topic
	*/
	public long getAttendanceId() {
		return _attendanceTopic.getAttendanceId();
	}

	/**
	* Sets the attendance ID of this attendance topic.
	*
	* @param attendanceId the attendance ID of this attendance topic
	*/
	public void setAttendanceId(long attendanceId) {
		_attendanceTopic.setAttendanceId(attendanceId);
	}

	/**
	* Returns the topic ID of this attendance topic.
	*
	* @return the topic ID of this attendance topic
	*/
	public long getTopicId() {
		return _attendanceTopic.getTopicId();
	}

	/**
	* Sets the topic ID of this attendance topic.
	*
	* @param topicId the topic ID of this attendance topic
	*/
	public void setTopicId(long topicId) {
		_attendanceTopic.setTopicId(topicId);
	}

	/**
	* Returns the description of this attendance topic.
	*
	* @return the description of this attendance topic
	*/
	public java.lang.String getDescription() {
		return _attendanceTopic.getDescription();
	}

	/**
	* Sets the description of this attendance topic.
	*
	* @param description the description of this attendance topic
	*/
	public void setDescription(java.lang.String description) {
		_attendanceTopic.setDescription(description);
	}

	public boolean isNew() {
		return _attendanceTopic.isNew();
	}

	public void setNew(boolean n) {
		_attendanceTopic.setNew(n);
	}

	public boolean isCachedModel() {
		return _attendanceTopic.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_attendanceTopic.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _attendanceTopic.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _attendanceTopic.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_attendanceTopic.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _attendanceTopic.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_attendanceTopic.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AttendanceTopicWrapper((AttendanceTopic)_attendanceTopic.clone());
	}

	public int compareTo(info.diit.portal.model.AttendanceTopic attendanceTopic) {
		return _attendanceTopic.compareTo(attendanceTopic);
	}

	@Override
	public int hashCode() {
		return _attendanceTopic.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.AttendanceTopic> toCacheModel() {
		return _attendanceTopic.toCacheModel();
	}

	public info.diit.portal.model.AttendanceTopic toEscapedModel() {
		return new AttendanceTopicWrapper(_attendanceTopic.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _attendanceTopic.toString();
	}

	public java.lang.String toXmlString() {
		return _attendanceTopic.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_attendanceTopic.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public AttendanceTopic getWrappedAttendanceTopic() {
		return _attendanceTopic;
	}

	public AttendanceTopic getWrappedModel() {
		return _attendanceTopic;
	}

	public void resetOriginalValues() {
		_attendanceTopic.resetOriginalValues();
	}

	private AttendanceTopic _attendanceTopic;
}
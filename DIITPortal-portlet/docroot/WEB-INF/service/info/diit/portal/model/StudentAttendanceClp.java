/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.service.StudentAttendanceLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class StudentAttendanceClp extends BaseModelImpl<StudentAttendance>
	implements StudentAttendance {
	public StudentAttendanceClp() {
	}

	public Class<?> getModelClass() {
		return StudentAttendance.class;
	}

	public String getModelClassName() {
		return StudentAttendance.class.getName();
	}

	public long getPrimaryKey() {
		return _attendanceTopicId;
	}

	public void setPrimaryKey(long primaryKey) {
		setAttendanceTopicId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_attendanceTopicId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("attendanceTopicId", getAttendanceTopicId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("attendanceId", getAttendanceId());
		attributes.put("studentId", getStudentId());
		attributes.put("status", getStatus());
		attributes.put("attendanceDate", getAttendanceDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long attendanceTopicId = (Long)attributes.get("attendanceTopicId");

		if (attendanceTopicId != null) {
			setAttendanceTopicId(attendanceTopicId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long attendanceId = (Long)attributes.get("attendanceId");

		if (attendanceId != null) {
			setAttendanceId(attendanceId);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Date attendanceDate = (Date)attributes.get("attendanceDate");

		if (attendanceDate != null) {
			setAttendanceDate(attendanceDate);
		}
	}

	public long getAttendanceTopicId() {
		return _attendanceTopicId;
	}

	public void setAttendanceTopicId(long attendanceTopicId) {
		_attendanceTopicId = attendanceTopicId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getAttendanceId() {
		return _attendanceId;
	}

	public void setAttendanceId(long attendanceId) {
		_attendanceId = attendanceId;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public Date getAttendanceDate() {
		return _attendanceDate;
	}

	public void setAttendanceDate(Date attendanceDate) {
		_attendanceDate = attendanceDate;
	}

	public BaseModel<?> getStudentAttendanceRemoteModel() {
		return _studentAttendanceRemoteModel;
	}

	public void setStudentAttendanceRemoteModel(
		BaseModel<?> studentAttendanceRemoteModel) {
		_studentAttendanceRemoteModel = studentAttendanceRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			StudentAttendanceLocalServiceUtil.addStudentAttendance(this);
		}
		else {
			StudentAttendanceLocalServiceUtil.updateStudentAttendance(this);
		}
	}

	@Override
	public StudentAttendance toEscapedModel() {
		return (StudentAttendance)Proxy.newProxyInstance(StudentAttendance.class.getClassLoader(),
			new Class[] { StudentAttendance.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		StudentAttendanceClp clone = new StudentAttendanceClp();

		clone.setAttendanceTopicId(getAttendanceTopicId());
		clone.setCompanyId(getCompanyId());
		clone.setOrganizationId(getOrganizationId());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setAttendanceId(getAttendanceId());
		clone.setStudentId(getStudentId());
		clone.setStatus(getStatus());
		clone.setAttendanceDate(getAttendanceDate());

		return clone;
	}

	public int compareTo(StudentAttendance studentAttendance) {
		long primaryKey = studentAttendance.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		StudentAttendanceClp studentAttendance = null;

		try {
			studentAttendance = (StudentAttendanceClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = studentAttendance.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{attendanceTopicId=");
		sb.append(getAttendanceTopicId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", attendanceId=");
		sb.append(getAttendanceId());
		sb.append(", studentId=");
		sb.append(getStudentId());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append(", attendanceDate=");
		sb.append(getAttendanceDate());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(37);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.StudentAttendance");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>attendanceTopicId</column-name><column-value><![CDATA[");
		sb.append(getAttendanceTopicId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>attendanceId</column-name><column-value><![CDATA[");
		sb.append(getAttendanceId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>studentId</column-name><column-value><![CDATA[");
		sb.append(getStudentId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>attendanceDate</column-name><column-value><![CDATA[");
		sb.append(getAttendanceDate());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _attendanceTopicId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _attendanceId;
	private long _studentId;
	private int _status;
	private Date _attendanceDate;
	private BaseModel<?> _studentAttendanceRemoteModel;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link EmployeeRole}.
 * </p>
 *
 * @author    mohammad
 * @see       EmployeeRole
 * @generated
 */
public class EmployeeRoleWrapper implements EmployeeRole,
	ModelWrapper<EmployeeRole> {
	public EmployeeRoleWrapper(EmployeeRole employeeRole) {
		_employeeRole = employeeRole;
	}

	public Class<?> getModelClass() {
		return EmployeeRole.class;
	}

	public String getModelClassName() {
		return EmployeeRole.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("employeeRoleId", getEmployeeRoleId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("companyId", getCompanyId());
		attributes.put("role", getRole());
		attributes.put("startTime", getStartTime());
		attributes.put("endTime", getEndTime());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long employeeRoleId = (Long)attributes.get("employeeRoleId");

		if (employeeRoleId != null) {
			setEmployeeRoleId(employeeRoleId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		String role = (String)attributes.get("role");

		if (role != null) {
			setRole(role);
		}

		Date startTime = (Date)attributes.get("startTime");

		if (startTime != null) {
			setStartTime(startTime);
		}

		Date endTime = (Date)attributes.get("endTime");

		if (endTime != null) {
			setEndTime(endTime);
		}
	}

	/**
	* Returns the primary key of this employee role.
	*
	* @return the primary key of this employee role
	*/
	public long getPrimaryKey() {
		return _employeeRole.getPrimaryKey();
	}

	/**
	* Sets the primary key of this employee role.
	*
	* @param primaryKey the primary key of this employee role
	*/
	public void setPrimaryKey(long primaryKey) {
		_employeeRole.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the employee role ID of this employee role.
	*
	* @return the employee role ID of this employee role
	*/
	public long getEmployeeRoleId() {
		return _employeeRole.getEmployeeRoleId();
	}

	/**
	* Sets the employee role ID of this employee role.
	*
	* @param employeeRoleId the employee role ID of this employee role
	*/
	public void setEmployeeRoleId(long employeeRoleId) {
		_employeeRole.setEmployeeRoleId(employeeRoleId);
	}

	/**
	* Returns the organization ID of this employee role.
	*
	* @return the organization ID of this employee role
	*/
	public long getOrganizationId() {
		return _employeeRole.getOrganizationId();
	}

	/**
	* Sets the organization ID of this employee role.
	*
	* @param organizationId the organization ID of this employee role
	*/
	public void setOrganizationId(long organizationId) {
		_employeeRole.setOrganizationId(organizationId);
	}

	/**
	* Returns the company ID of this employee role.
	*
	* @return the company ID of this employee role
	*/
	public long getCompanyId() {
		return _employeeRole.getCompanyId();
	}

	/**
	* Sets the company ID of this employee role.
	*
	* @param companyId the company ID of this employee role
	*/
	public void setCompanyId(long companyId) {
		_employeeRole.setCompanyId(companyId);
	}

	/**
	* Returns the role of this employee role.
	*
	* @return the role of this employee role
	*/
	public java.lang.String getRole() {
		return _employeeRole.getRole();
	}

	/**
	* Sets the role of this employee role.
	*
	* @param role the role of this employee role
	*/
	public void setRole(java.lang.String role) {
		_employeeRole.setRole(role);
	}

	/**
	* Returns the start time of this employee role.
	*
	* @return the start time of this employee role
	*/
	public java.util.Date getStartTime() {
		return _employeeRole.getStartTime();
	}

	/**
	* Sets the start time of this employee role.
	*
	* @param startTime the start time of this employee role
	*/
	public void setStartTime(java.util.Date startTime) {
		_employeeRole.setStartTime(startTime);
	}

	/**
	* Returns the end time of this employee role.
	*
	* @return the end time of this employee role
	*/
	public java.util.Date getEndTime() {
		return _employeeRole.getEndTime();
	}

	/**
	* Sets the end time of this employee role.
	*
	* @param endTime the end time of this employee role
	*/
	public void setEndTime(java.util.Date endTime) {
		_employeeRole.setEndTime(endTime);
	}

	public boolean isNew() {
		return _employeeRole.isNew();
	}

	public void setNew(boolean n) {
		_employeeRole.setNew(n);
	}

	public boolean isCachedModel() {
		return _employeeRole.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_employeeRole.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _employeeRole.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _employeeRole.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_employeeRole.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _employeeRole.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_employeeRole.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new EmployeeRoleWrapper((EmployeeRole)_employeeRole.clone());
	}

	public int compareTo(info.diit.portal.model.EmployeeRole employeeRole) {
		return _employeeRole.compareTo(employeeRole);
	}

	@Override
	public int hashCode() {
		return _employeeRole.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.EmployeeRole> toCacheModel() {
		return _employeeRole.toCacheModel();
	}

	public info.diit.portal.model.EmployeeRole toEscapedModel() {
		return new EmployeeRoleWrapper(_employeeRole.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _employeeRole.toString();
	}

	public java.lang.String toXmlString() {
		return _employeeRole.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_employeeRole.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public EmployeeRole getWrappedEmployeeRole() {
		return _employeeRole;
	}

	public EmployeeRole getWrappedModel() {
		return _employeeRole;
	}

	public void resetOriginalValues() {
		_employeeRole.resetOriginalValues();
	}

	private EmployeeRole _employeeRole;
}
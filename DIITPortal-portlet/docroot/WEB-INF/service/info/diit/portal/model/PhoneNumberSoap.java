/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class PhoneNumberSoap implements Serializable {
	public static PhoneNumberSoap toSoapModel(PhoneNumber model) {
		PhoneNumberSoap soapModel = new PhoneNumberSoap();

		soapModel.setPhoneNumberId(model.getPhoneNumberId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setOwnerType(model.getOwnerType());
		soapModel.setPhoneNumber(model.getPhoneNumber());
		soapModel.setStudentId(model.getStudentId());

		return soapModel;
	}

	public static PhoneNumberSoap[] toSoapModels(PhoneNumber[] models) {
		PhoneNumberSoap[] soapModels = new PhoneNumberSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PhoneNumberSoap[][] toSoapModels(PhoneNumber[][] models) {
		PhoneNumberSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PhoneNumberSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PhoneNumberSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PhoneNumberSoap[] toSoapModels(List<PhoneNumber> models) {
		List<PhoneNumberSoap> soapModels = new ArrayList<PhoneNumberSoap>(models.size());

		for (PhoneNumber model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PhoneNumberSoap[soapModels.size()]);
	}

	public PhoneNumberSoap() {
	}

	public long getPrimaryKey() {
		return _phoneNumberId;
	}

	public void setPrimaryKey(long pk) {
		setPhoneNumberId(pk);
	}

	public long getPhoneNumberId() {
		return _phoneNumberId;
	}

	public void setPhoneNumberId(long phoneNumberId) {
		_phoneNumberId = phoneNumberId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public int getOwnerType() {
		return _ownerType;
	}

	public void setOwnerType(int ownerType) {
		_ownerType = ownerType;
	}

	public String getPhoneNumber() {
		return _phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		_phoneNumber = phoneNumber;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	private long _phoneNumberId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _organizationId;
	private int _ownerType;
	private String _phoneNumber;
	private long _studentId;
}
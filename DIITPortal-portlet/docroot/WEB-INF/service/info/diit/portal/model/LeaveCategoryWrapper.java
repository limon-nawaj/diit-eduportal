/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link LeaveCategory}.
 * </p>
 *
 * @author    mohammad
 * @see       LeaveCategory
 * @generated
 */
public class LeaveCategoryWrapper implements LeaveCategory,
	ModelWrapper<LeaveCategory> {
	public LeaveCategoryWrapper(LeaveCategory leaveCategory) {
		_leaveCategory = leaveCategory;
	}

	public Class<?> getModelClass() {
		return LeaveCategory.class;
	}

	public String getModelClassName() {
		return LeaveCategory.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("leaveCategoryId", getLeaveCategoryId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("title", getTitle());
		attributes.put("totalLeave", getTotalLeave());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long leaveCategoryId = (Long)attributes.get("leaveCategoryId");

		if (leaveCategoryId != null) {
			setLeaveCategoryId(leaveCategoryId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		Integer totalLeave = (Integer)attributes.get("totalLeave");

		if (totalLeave != null) {
			setTotalLeave(totalLeave);
		}
	}

	/**
	* Returns the primary key of this leave category.
	*
	* @return the primary key of this leave category
	*/
	public long getPrimaryKey() {
		return _leaveCategory.getPrimaryKey();
	}

	/**
	* Sets the primary key of this leave category.
	*
	* @param primaryKey the primary key of this leave category
	*/
	public void setPrimaryKey(long primaryKey) {
		_leaveCategory.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the leave category ID of this leave category.
	*
	* @return the leave category ID of this leave category
	*/
	public long getLeaveCategoryId() {
		return _leaveCategory.getLeaveCategoryId();
	}

	/**
	* Sets the leave category ID of this leave category.
	*
	* @param leaveCategoryId the leave category ID of this leave category
	*/
	public void setLeaveCategoryId(long leaveCategoryId) {
		_leaveCategory.setLeaveCategoryId(leaveCategoryId);
	}

	/**
	* Returns the company ID of this leave category.
	*
	* @return the company ID of this leave category
	*/
	public long getCompanyId() {
		return _leaveCategory.getCompanyId();
	}

	/**
	* Sets the company ID of this leave category.
	*
	* @param companyId the company ID of this leave category
	*/
	public void setCompanyId(long companyId) {
		_leaveCategory.setCompanyId(companyId);
	}

	/**
	* Returns the organization ID of this leave category.
	*
	* @return the organization ID of this leave category
	*/
	public long getOrganizationId() {
		return _leaveCategory.getOrganizationId();
	}

	/**
	* Sets the organization ID of this leave category.
	*
	* @param organizationId the organization ID of this leave category
	*/
	public void setOrganizationId(long organizationId) {
		_leaveCategory.setOrganizationId(organizationId);
	}

	/**
	* Returns the user ID of this leave category.
	*
	* @return the user ID of this leave category
	*/
	public long getUserId() {
		return _leaveCategory.getUserId();
	}

	/**
	* Sets the user ID of this leave category.
	*
	* @param userId the user ID of this leave category
	*/
	public void setUserId(long userId) {
		_leaveCategory.setUserId(userId);
	}

	/**
	* Returns the user uuid of this leave category.
	*
	* @return the user uuid of this leave category
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _leaveCategory.getUserUuid();
	}

	/**
	* Sets the user uuid of this leave category.
	*
	* @param userUuid the user uuid of this leave category
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_leaveCategory.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this leave category.
	*
	* @return the user name of this leave category
	*/
	public java.lang.String getUserName() {
		return _leaveCategory.getUserName();
	}

	/**
	* Sets the user name of this leave category.
	*
	* @param userName the user name of this leave category
	*/
	public void setUserName(java.lang.String userName) {
		_leaveCategory.setUserName(userName);
	}

	/**
	* Returns the create date of this leave category.
	*
	* @return the create date of this leave category
	*/
	public java.util.Date getCreateDate() {
		return _leaveCategory.getCreateDate();
	}

	/**
	* Sets the create date of this leave category.
	*
	* @param createDate the create date of this leave category
	*/
	public void setCreateDate(java.util.Date createDate) {
		_leaveCategory.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this leave category.
	*
	* @return the modified date of this leave category
	*/
	public java.util.Date getModifiedDate() {
		return _leaveCategory.getModifiedDate();
	}

	/**
	* Sets the modified date of this leave category.
	*
	* @param modifiedDate the modified date of this leave category
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_leaveCategory.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the title of this leave category.
	*
	* @return the title of this leave category
	*/
	public java.lang.String getTitle() {
		return _leaveCategory.getTitle();
	}

	/**
	* Sets the title of this leave category.
	*
	* @param title the title of this leave category
	*/
	public void setTitle(java.lang.String title) {
		_leaveCategory.setTitle(title);
	}

	/**
	* Returns the total leave of this leave category.
	*
	* @return the total leave of this leave category
	*/
	public int getTotalLeave() {
		return _leaveCategory.getTotalLeave();
	}

	/**
	* Sets the total leave of this leave category.
	*
	* @param totalLeave the total leave of this leave category
	*/
	public void setTotalLeave(int totalLeave) {
		_leaveCategory.setTotalLeave(totalLeave);
	}

	public boolean isNew() {
		return _leaveCategory.isNew();
	}

	public void setNew(boolean n) {
		_leaveCategory.setNew(n);
	}

	public boolean isCachedModel() {
		return _leaveCategory.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_leaveCategory.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _leaveCategory.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _leaveCategory.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_leaveCategory.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _leaveCategory.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_leaveCategory.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LeaveCategoryWrapper((LeaveCategory)_leaveCategory.clone());
	}

	public int compareTo(info.diit.portal.model.LeaveCategory leaveCategory) {
		return _leaveCategory.compareTo(leaveCategory);
	}

	@Override
	public int hashCode() {
		return _leaveCategory.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.LeaveCategory> toCacheModel() {
		return _leaveCategory.toCacheModel();
	}

	public info.diit.portal.model.LeaveCategory toEscapedModel() {
		return new LeaveCategoryWrapper(_leaveCategory.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _leaveCategory.toString();
	}

	public java.lang.String toXmlString() {
		return _leaveCategory.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_leaveCategory.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public LeaveCategory getWrappedLeaveCategory() {
		return _leaveCategory;
	}

	public LeaveCategory getWrappedModel() {
		return _leaveCategory;
	}

	public void resetOriginalValues() {
		_leaveCategory.resetOriginalValues();
	}

	private LeaveCategory _leaveCategory;
}
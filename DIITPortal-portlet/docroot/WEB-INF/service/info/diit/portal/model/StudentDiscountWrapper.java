/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link StudentDiscount}.
 * </p>
 *
 * @author    mohammad
 * @see       StudentDiscount
 * @generated
 */
public class StudentDiscountWrapper implements StudentDiscount,
	ModelWrapper<StudentDiscount> {
	public StudentDiscountWrapper(StudentDiscount studentDiscount) {
		_studentDiscount = studentDiscount;
	}

	public Class<?> getModelClass() {
		return StudentDiscount.class;
	}

	public String getModelClassName() {
		return StudentDiscount.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("studentDiscountId", getStudentDiscountId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("studentId", getStudentId());
		attributes.put("batchId", getBatchId());
		attributes.put("scholarships", getScholarships());
		attributes.put("discount", getDiscount());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long studentDiscountId = (Long)attributes.get("studentDiscountId");

		if (studentDiscountId != null) {
			setStudentDiscountId(studentDiscountId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Double scholarships = (Double)attributes.get("scholarships");

		if (scholarships != null) {
			setScholarships(scholarships);
		}

		Double discount = (Double)attributes.get("discount");

		if (discount != null) {
			setDiscount(discount);
		}
	}

	/**
	* Returns the primary key of this student discount.
	*
	* @return the primary key of this student discount
	*/
	public long getPrimaryKey() {
		return _studentDiscount.getPrimaryKey();
	}

	/**
	* Sets the primary key of this student discount.
	*
	* @param primaryKey the primary key of this student discount
	*/
	public void setPrimaryKey(long primaryKey) {
		_studentDiscount.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the student discount ID of this student discount.
	*
	* @return the student discount ID of this student discount
	*/
	public long getStudentDiscountId() {
		return _studentDiscount.getStudentDiscountId();
	}

	/**
	* Sets the student discount ID of this student discount.
	*
	* @param studentDiscountId the student discount ID of this student discount
	*/
	public void setStudentDiscountId(long studentDiscountId) {
		_studentDiscount.setStudentDiscountId(studentDiscountId);
	}

	/**
	* Returns the company ID of this student discount.
	*
	* @return the company ID of this student discount
	*/
	public long getCompanyId() {
		return _studentDiscount.getCompanyId();
	}

	/**
	* Sets the company ID of this student discount.
	*
	* @param companyId the company ID of this student discount
	*/
	public void setCompanyId(long companyId) {
		_studentDiscount.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this student discount.
	*
	* @return the user ID of this student discount
	*/
	public long getUserId() {
		return _studentDiscount.getUserId();
	}

	/**
	* Sets the user ID of this student discount.
	*
	* @param userId the user ID of this student discount
	*/
	public void setUserId(long userId) {
		_studentDiscount.setUserId(userId);
	}

	/**
	* Returns the user uuid of this student discount.
	*
	* @return the user uuid of this student discount
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscount.getUserUuid();
	}

	/**
	* Sets the user uuid of this student discount.
	*
	* @param userUuid the user uuid of this student discount
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_studentDiscount.setUserUuid(userUuid);
	}

	/**
	* Returns the create date of this student discount.
	*
	* @return the create date of this student discount
	*/
	public java.util.Date getCreateDate() {
		return _studentDiscount.getCreateDate();
	}

	/**
	* Sets the create date of this student discount.
	*
	* @param createDate the create date of this student discount
	*/
	public void setCreateDate(java.util.Date createDate) {
		_studentDiscount.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this student discount.
	*
	* @return the modified date of this student discount
	*/
	public java.util.Date getModifiedDate() {
		return _studentDiscount.getModifiedDate();
	}

	/**
	* Sets the modified date of this student discount.
	*
	* @param modifiedDate the modified date of this student discount
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_studentDiscount.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the student ID of this student discount.
	*
	* @return the student ID of this student discount
	*/
	public long getStudentId() {
		return _studentDiscount.getStudentId();
	}

	/**
	* Sets the student ID of this student discount.
	*
	* @param studentId the student ID of this student discount
	*/
	public void setStudentId(long studentId) {
		_studentDiscount.setStudentId(studentId);
	}

	/**
	* Returns the batch ID of this student discount.
	*
	* @return the batch ID of this student discount
	*/
	public long getBatchId() {
		return _studentDiscount.getBatchId();
	}

	/**
	* Sets the batch ID of this student discount.
	*
	* @param batchId the batch ID of this student discount
	*/
	public void setBatchId(long batchId) {
		_studentDiscount.setBatchId(batchId);
	}

	/**
	* Returns the scholarships of this student discount.
	*
	* @return the scholarships of this student discount
	*/
	public double getScholarships() {
		return _studentDiscount.getScholarships();
	}

	/**
	* Sets the scholarships of this student discount.
	*
	* @param scholarships the scholarships of this student discount
	*/
	public void setScholarships(double scholarships) {
		_studentDiscount.setScholarships(scholarships);
	}

	/**
	* Returns the discount of this student discount.
	*
	* @return the discount of this student discount
	*/
	public double getDiscount() {
		return _studentDiscount.getDiscount();
	}

	/**
	* Sets the discount of this student discount.
	*
	* @param discount the discount of this student discount
	*/
	public void setDiscount(double discount) {
		_studentDiscount.setDiscount(discount);
	}

	public boolean isNew() {
		return _studentDiscount.isNew();
	}

	public void setNew(boolean n) {
		_studentDiscount.setNew(n);
	}

	public boolean isCachedModel() {
		return _studentDiscount.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_studentDiscount.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _studentDiscount.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _studentDiscount.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_studentDiscount.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _studentDiscount.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_studentDiscount.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new StudentDiscountWrapper((StudentDiscount)_studentDiscount.clone());
	}

	public int compareTo(info.diit.portal.model.StudentDiscount studentDiscount) {
		return _studentDiscount.compareTo(studentDiscount);
	}

	@Override
	public int hashCode() {
		return _studentDiscount.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.StudentDiscount> toCacheModel() {
		return _studentDiscount.toCacheModel();
	}

	public info.diit.portal.model.StudentDiscount toEscapedModel() {
		return new StudentDiscountWrapper(_studentDiscount.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _studentDiscount.toString();
	}

	public java.lang.String toXmlString() {
		return _studentDiscount.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_studentDiscount.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public StudentDiscount getWrappedStudentDiscount() {
		return _studentDiscount;
	}

	public StudentDiscount getWrappedModel() {
		return _studentDiscount;
	}

	public void resetOriginalValues() {
		_studentDiscount.resetOriginalValues();
	}

	private StudentDiscount _studentDiscount;
}
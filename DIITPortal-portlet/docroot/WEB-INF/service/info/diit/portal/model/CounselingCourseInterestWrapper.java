/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CounselingCourseInterest}.
 * </p>
 *
 * @author    mohammad
 * @see       CounselingCourseInterest
 * @generated
 */
public class CounselingCourseInterestWrapper implements CounselingCourseInterest,
	ModelWrapper<CounselingCourseInterest> {
	public CounselingCourseInterestWrapper(
		CounselingCourseInterest counselingCourseInterest) {
		_counselingCourseInterest = counselingCourseInterest;
	}

	public Class<?> getModelClass() {
		return CounselingCourseInterest.class;
	}

	public String getModelClassName() {
		return CounselingCourseInterest.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("CounselingCourseInterestId",
			getCounselingCourseInterestId());
		attributes.put("courseId", getCourseId());
		attributes.put("counselingId", getCounselingId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long CounselingCourseInterestId = (Long)attributes.get(
				"CounselingCourseInterestId");

		if (CounselingCourseInterestId != null) {
			setCounselingCourseInterestId(CounselingCourseInterestId);
		}

		Long courseId = (Long)attributes.get("courseId");

		if (courseId != null) {
			setCourseId(courseId);
		}

		Long counselingId = (Long)attributes.get("counselingId");

		if (counselingId != null) {
			setCounselingId(counselingId);
		}
	}

	/**
	* Returns the primary key of this counseling course interest.
	*
	* @return the primary key of this counseling course interest
	*/
	public long getPrimaryKey() {
		return _counselingCourseInterest.getPrimaryKey();
	}

	/**
	* Sets the primary key of this counseling course interest.
	*
	* @param primaryKey the primary key of this counseling course interest
	*/
	public void setPrimaryKey(long primaryKey) {
		_counselingCourseInterest.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the counseling course interest ID of this counseling course interest.
	*
	* @return the counseling course interest ID of this counseling course interest
	*/
	public long getCounselingCourseInterestId() {
		return _counselingCourseInterest.getCounselingCourseInterestId();
	}

	/**
	* Sets the counseling course interest ID of this counseling course interest.
	*
	* @param CounselingCourseInterestId the counseling course interest ID of this counseling course interest
	*/
	public void setCounselingCourseInterestId(long CounselingCourseInterestId) {
		_counselingCourseInterest.setCounselingCourseInterestId(CounselingCourseInterestId);
	}

	/**
	* Returns the course ID of this counseling course interest.
	*
	* @return the course ID of this counseling course interest
	*/
	public long getCourseId() {
		return _counselingCourseInterest.getCourseId();
	}

	/**
	* Sets the course ID of this counseling course interest.
	*
	* @param courseId the course ID of this counseling course interest
	*/
	public void setCourseId(long courseId) {
		_counselingCourseInterest.setCourseId(courseId);
	}

	/**
	* Returns the counseling ID of this counseling course interest.
	*
	* @return the counseling ID of this counseling course interest
	*/
	public long getCounselingId() {
		return _counselingCourseInterest.getCounselingId();
	}

	/**
	* Sets the counseling ID of this counseling course interest.
	*
	* @param counselingId the counseling ID of this counseling course interest
	*/
	public void setCounselingId(long counselingId) {
		_counselingCourseInterest.setCounselingId(counselingId);
	}

	public boolean isNew() {
		return _counselingCourseInterest.isNew();
	}

	public void setNew(boolean n) {
		_counselingCourseInterest.setNew(n);
	}

	public boolean isCachedModel() {
		return _counselingCourseInterest.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_counselingCourseInterest.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _counselingCourseInterest.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _counselingCourseInterest.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_counselingCourseInterest.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _counselingCourseInterest.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_counselingCourseInterest.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CounselingCourseInterestWrapper((CounselingCourseInterest)_counselingCourseInterest.clone());
	}

	public int compareTo(
		info.diit.portal.model.CounselingCourseInterest counselingCourseInterest) {
		return _counselingCourseInterest.compareTo(counselingCourseInterest);
	}

	@Override
	public int hashCode() {
		return _counselingCourseInterest.hashCode();
	}

	public com.liferay.portal.model.CacheModel<info.diit.portal.model.CounselingCourseInterest> toCacheModel() {
		return _counselingCourseInterest.toCacheModel();
	}

	public info.diit.portal.model.CounselingCourseInterest toEscapedModel() {
		return new CounselingCourseInterestWrapper(_counselingCourseInterest.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _counselingCourseInterest.toString();
	}

	public java.lang.String toXmlString() {
		return _counselingCourseInterest.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_counselingCourseInterest.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public CounselingCourseInterest getWrappedCounselingCourseInterest() {
		return _counselingCourseInterest;
	}

	public CounselingCourseInterest getWrappedModel() {
		return _counselingCourseInterest;
	}

	public void resetOriginalValues() {
		_counselingCourseInterest.resetOriginalValues();
	}

	private CounselingCourseInterest _counselingCourseInterest;
}
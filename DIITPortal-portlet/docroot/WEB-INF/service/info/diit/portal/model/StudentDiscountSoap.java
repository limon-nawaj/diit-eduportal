/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class StudentDiscountSoap implements Serializable {
	public static StudentDiscountSoap toSoapModel(StudentDiscount model) {
		StudentDiscountSoap soapModel = new StudentDiscountSoap();

		soapModel.setStudentDiscountId(model.getStudentDiscountId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setStudentId(model.getStudentId());
		soapModel.setBatchId(model.getBatchId());
		soapModel.setScholarships(model.getScholarships());
		soapModel.setDiscount(model.getDiscount());

		return soapModel;
	}

	public static StudentDiscountSoap[] toSoapModels(StudentDiscount[] models) {
		StudentDiscountSoap[] soapModels = new StudentDiscountSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static StudentDiscountSoap[][] toSoapModels(
		StudentDiscount[][] models) {
		StudentDiscountSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new StudentDiscountSoap[models.length][models[0].length];
		}
		else {
			soapModels = new StudentDiscountSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static StudentDiscountSoap[] toSoapModels(
		List<StudentDiscount> models) {
		List<StudentDiscountSoap> soapModels = new ArrayList<StudentDiscountSoap>(models.size());

		for (StudentDiscount model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new StudentDiscountSoap[soapModels.size()]);
	}

	public StudentDiscountSoap() {
	}

	public long getPrimaryKey() {
		return _studentDiscountId;
	}

	public void setPrimaryKey(long pk) {
		setStudentDiscountId(pk);
	}

	public long getStudentDiscountId() {
		return _studentDiscountId;
	}

	public void setStudentDiscountId(long studentDiscountId) {
		_studentDiscountId = studentDiscountId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public double getScholarships() {
		return _scholarships;
	}

	public void setScholarships(double scholarships) {
		_scholarships = scholarships;
	}

	public double getDiscount() {
		return _discount;
	}

	public void setDiscount(double discount) {
		_discount = discount;
	}

	private long _studentDiscountId;
	private long _companyId;
	private long _userId;
	private Date _createDate;
	private Date _modifiedDate;
	private long _studentId;
	private long _batchId;
	private double _scholarships;
	private double _discount;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    mohammad
 * @generated
 */
public class StudentAttendanceSoap implements Serializable {
	public static StudentAttendanceSoap toSoapModel(StudentAttendance model) {
		StudentAttendanceSoap soapModel = new StudentAttendanceSoap();

		soapModel.setAttendanceTopicId(model.getAttendanceTopicId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setOrganizationId(model.getOrganizationId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setAttendanceId(model.getAttendanceId());
		soapModel.setStudentId(model.getStudentId());
		soapModel.setStatus(model.getStatus());
		soapModel.setAttendanceDate(model.getAttendanceDate());

		return soapModel;
	}

	public static StudentAttendanceSoap[] toSoapModels(
		StudentAttendance[] models) {
		StudentAttendanceSoap[] soapModels = new StudentAttendanceSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static StudentAttendanceSoap[][] toSoapModels(
		StudentAttendance[][] models) {
		StudentAttendanceSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new StudentAttendanceSoap[models.length][models[0].length];
		}
		else {
			soapModels = new StudentAttendanceSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static StudentAttendanceSoap[] toSoapModels(
		List<StudentAttendance> models) {
		List<StudentAttendanceSoap> soapModels = new ArrayList<StudentAttendanceSoap>(models.size());

		for (StudentAttendance model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new StudentAttendanceSoap[soapModels.size()]);
	}

	public StudentAttendanceSoap() {
	}

	public long getPrimaryKey() {
		return _attendanceTopicId;
	}

	public void setPrimaryKey(long pk) {
		setAttendanceTopicId(pk);
	}

	public long getAttendanceTopicId() {
		return _attendanceTopicId;
	}

	public void setAttendanceTopicId(long attendanceTopicId) {
		_attendanceTopicId = attendanceTopicId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_organizationId = organizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getAttendanceId() {
		return _attendanceId;
	}

	public void setAttendanceId(long attendanceId) {
		_attendanceId = attendanceId;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public Date getAttendanceDate() {
		return _attendanceDate;
	}

	public void setAttendanceDate(Date attendanceDate) {
		_attendanceDate = attendanceDate;
	}

	private long _attendanceTopicId;
	private long _companyId;
	private long _organizationId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _attendanceId;
	private long _studentId;
	private int _status;
	private Date _attendanceDate;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import info.diit.portal.service.StudentDiscountLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mohammad
 */
public class StudentDiscountClp extends BaseModelImpl<StudentDiscount>
	implements StudentDiscount {
	public StudentDiscountClp() {
	}

	public Class<?> getModelClass() {
		return StudentDiscount.class;
	}

	public String getModelClassName() {
		return StudentDiscount.class.getName();
	}

	public long getPrimaryKey() {
		return _studentDiscountId;
	}

	public void setPrimaryKey(long primaryKey) {
		setStudentDiscountId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_studentDiscountId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("studentDiscountId", getStudentDiscountId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("studentId", getStudentId());
		attributes.put("batchId", getBatchId());
		attributes.put("scholarships", getScholarships());
		attributes.put("discount", getDiscount());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long studentDiscountId = (Long)attributes.get("studentDiscountId");

		if (studentDiscountId != null) {
			setStudentDiscountId(studentDiscountId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long studentId = (Long)attributes.get("studentId");

		if (studentId != null) {
			setStudentId(studentId);
		}

		Long batchId = (Long)attributes.get("batchId");

		if (batchId != null) {
			setBatchId(batchId);
		}

		Double scholarships = (Double)attributes.get("scholarships");

		if (scholarships != null) {
			setScholarships(scholarships);
		}

		Double discount = (Double)attributes.get("discount");

		if (discount != null) {
			setDiscount(discount);
		}
	}

	public long getStudentDiscountId() {
		return _studentDiscountId;
	}

	public void setStudentDiscountId(long studentDiscountId) {
		_studentDiscountId = studentDiscountId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getStudentId() {
		return _studentId;
	}

	public void setStudentId(long studentId) {
		_studentId = studentId;
	}

	public long getBatchId() {
		return _batchId;
	}

	public void setBatchId(long batchId) {
		_batchId = batchId;
	}

	public double getScholarships() {
		return _scholarships;
	}

	public void setScholarships(double scholarships) {
		_scholarships = scholarships;
	}

	public double getDiscount() {
		return _discount;
	}

	public void setDiscount(double discount) {
		_discount = discount;
	}

	public BaseModel<?> getStudentDiscountRemoteModel() {
		return _studentDiscountRemoteModel;
	}

	public void setStudentDiscountRemoteModel(
		BaseModel<?> studentDiscountRemoteModel) {
		_studentDiscountRemoteModel = studentDiscountRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			StudentDiscountLocalServiceUtil.addStudentDiscount(this);
		}
		else {
			StudentDiscountLocalServiceUtil.updateStudentDiscount(this);
		}
	}

	@Override
	public StudentDiscount toEscapedModel() {
		return (StudentDiscount)Proxy.newProxyInstance(StudentDiscount.class.getClassLoader(),
			new Class[] { StudentDiscount.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		StudentDiscountClp clone = new StudentDiscountClp();

		clone.setStudentDiscountId(getStudentDiscountId());
		clone.setCompanyId(getCompanyId());
		clone.setUserId(getUserId());
		clone.setCreateDate(getCreateDate());
		clone.setModifiedDate(getModifiedDate());
		clone.setStudentId(getStudentId());
		clone.setBatchId(getBatchId());
		clone.setScholarships(getScholarships());
		clone.setDiscount(getDiscount());

		return clone;
	}

	public int compareTo(StudentDiscount studentDiscount) {
		long primaryKey = studentDiscount.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		StudentDiscountClp studentDiscount = null;

		try {
			studentDiscount = (StudentDiscountClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = studentDiscount.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{studentDiscountId=");
		sb.append(getStudentDiscountId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", studentId=");
		sb.append(getStudentId());
		sb.append(", batchId=");
		sb.append(getBatchId());
		sb.append(", scholarships=");
		sb.append(getScholarships());
		sb.append(", discount=");
		sb.append(getDiscount());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(31);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.StudentDiscount");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>studentDiscountId</column-name><column-value><![CDATA[");
		sb.append(getStudentDiscountId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>studentId</column-name><column-value><![CDATA[");
		sb.append(getStudentId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>batchId</column-name><column-value><![CDATA[");
		sb.append(getBatchId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>scholarships</column-name><column-value><![CDATA[");
		sb.append(getScholarships());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>discount</column-name><column-value><![CDATA[");
		sb.append(getDiscount());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _studentDiscountId;
	private long _companyId;
	private long _userId;
	private String _userUuid;
	private Date _createDate;
	private Date _modifiedDate;
	private long _studentId;
	private long _batchId;
	private double _scholarships;
	private double _discount;
	private BaseModel<?> _studentDiscountRemoteModel;
}
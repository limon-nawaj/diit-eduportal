/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.Designation;

import java.util.List;

/**
 * The persistence utility for the designation service. This utility wraps {@link DesignationPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see DesignationPersistence
 * @see DesignationPersistenceImpl
 * @generated
 */
public class DesignationUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Designation designation) {
		getPersistence().clearCache(designation);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Designation> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Designation> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Designation> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Designation update(Designation designation, boolean merge)
		throws SystemException {
		return getPersistence().update(designation, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Designation update(Designation designation, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(designation, merge, serviceContext);
	}

	/**
	* Caches the designation in the entity cache if it is enabled.
	*
	* @param designation the designation
	*/
	public static void cacheResult(
		info.diit.portal.model.Designation designation) {
		getPersistence().cacheResult(designation);
	}

	/**
	* Caches the designations in the entity cache if it is enabled.
	*
	* @param designations the designations
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.Designation> designations) {
		getPersistence().cacheResult(designations);
	}

	/**
	* Creates a new designation with the primary key. Does not add the designation to the database.
	*
	* @param designationId the primary key for the new designation
	* @return the new designation
	*/
	public static info.diit.portal.model.Designation create(long designationId) {
		return getPersistence().create(designationId);
	}

	/**
	* Removes the designation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param designationId the primary key of the designation
	* @return the designation that was removed
	* @throws info.diit.portal.NoSuchDesignationException if a designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Designation remove(long designationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDesignationException {
		return getPersistence().remove(designationId);
	}

	public static info.diit.portal.model.Designation updateImpl(
		info.diit.portal.model.Designation designation, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(designation, merge);
	}

	/**
	* Returns the designation with the primary key or throws a {@link info.diit.portal.NoSuchDesignationException} if it could not be found.
	*
	* @param designationId the primary key of the designation
	* @return the designation
	* @throws info.diit.portal.NoSuchDesignationException if a designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Designation findByPrimaryKey(
		long designationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDesignationException {
		return getPersistence().findByPrimaryKey(designationId);
	}

	/**
	* Returns the designation with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param designationId the primary key of the designation
	* @return the designation, or <code>null</code> if a designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Designation fetchByPrimaryKey(
		long designationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(designationId);
	}

	/**
	* Returns all the designations where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Designation> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId);
	}

	/**
	* Returns a range of all the designations where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of designations
	* @param end the upper bound of the range of designations (not inclusive)
	* @return the range of matching designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Designation> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the designations where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of designations
	* @param end the upper bound of the range of designations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Designation> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching designation
	* @throws info.diit.portal.NoSuchDesignationException if a matching designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Designation findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDesignationException {
		return getPersistence().findByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching designation, or <code>null</code> if a matching designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Designation fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching designation
	* @throws info.diit.portal.NoSuchDesignationException if a matching designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Designation findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDesignationException {
		return getPersistence().findByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching designation, or <code>null</code> if a matching designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Designation fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the designations before and after the current designation in the ordered set where companyId = &#63;.
	*
	* @param designationId the primary key of the current designation
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next designation
	* @throws info.diit.portal.NoSuchDesignationException if a designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Designation[] findByCompany_PrevAndNext(
		long designationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDesignationException {
		return getPersistence()
				   .findByCompany_PrevAndNext(designationId, companyId,
			orderByComparator);
	}

	/**
	* Returns all the designations.
	*
	* @return the designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Designation> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the designations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of designations
	* @param end the upper bound of the range of designations (not inclusive)
	* @return the range of designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Designation> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the designations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of designations
	* @param end the upper bound of the range of designations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of designations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Designation> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the designations where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCompany(companyId);
	}

	/**
	* Removes all the designations from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of designations where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching designations
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCompany(companyId);
	}

	/**
	* Returns the number of designations.
	*
	* @return the number of designations
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static DesignationPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (DesignationPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					DesignationPersistence.class.getName());

			ReferenceRegistry.registerReference(DesignationUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(DesignationPersistence persistence) {
	}

	private static DesignationPersistence _persistence;
}
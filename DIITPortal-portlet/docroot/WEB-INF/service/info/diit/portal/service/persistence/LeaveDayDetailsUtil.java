/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.LeaveDayDetails;

import java.util.List;

/**
 * The persistence utility for the leave day details service. This utility wraps {@link LeaveDayDetailsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see LeaveDayDetailsPersistence
 * @see LeaveDayDetailsPersistenceImpl
 * @generated
 */
public class LeaveDayDetailsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(LeaveDayDetails leaveDayDetails) {
		getPersistence().clearCache(leaveDayDetails);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<LeaveDayDetails> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<LeaveDayDetails> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<LeaveDayDetails> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static LeaveDayDetails update(LeaveDayDetails leaveDayDetails,
		boolean merge) throws SystemException {
		return getPersistence().update(leaveDayDetails, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static LeaveDayDetails update(LeaveDayDetails leaveDayDetails,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(leaveDayDetails, merge, serviceContext);
	}

	/**
	* Caches the leave day details in the entity cache if it is enabled.
	*
	* @param leaveDayDetails the leave day details
	*/
	public static void cacheResult(
		info.diit.portal.model.LeaveDayDetails leaveDayDetails) {
		getPersistence().cacheResult(leaveDayDetails);
	}

	/**
	* Caches the leave day detailses in the entity cache if it is enabled.
	*
	* @param leaveDayDetailses the leave day detailses
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.LeaveDayDetails> leaveDayDetailses) {
		getPersistence().cacheResult(leaveDayDetailses);
	}

	/**
	* Creates a new leave day details with the primary key. Does not add the leave day details to the database.
	*
	* @param leaveDayDetailsId the primary key for the new leave day details
	* @return the new leave day details
	*/
	public static info.diit.portal.model.LeaveDayDetails create(
		long leaveDayDetailsId) {
		return getPersistence().create(leaveDayDetailsId);
	}

	/**
	* Removes the leave day details with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param leaveDayDetailsId the primary key of the leave day details
	* @return the leave day details that was removed
	* @throws info.diit.portal.NoSuchLeaveDayDetailsException if a leave day details with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveDayDetails remove(
		long leaveDayDetailsId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveDayDetailsException {
		return getPersistence().remove(leaveDayDetailsId);
	}

	public static info.diit.portal.model.LeaveDayDetails updateImpl(
		info.diit.portal.model.LeaveDayDetails leaveDayDetails, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(leaveDayDetails, merge);
	}

	/**
	* Returns the leave day details with the primary key or throws a {@link info.diit.portal.NoSuchLeaveDayDetailsException} if it could not be found.
	*
	* @param leaveDayDetailsId the primary key of the leave day details
	* @return the leave day details
	* @throws info.diit.portal.NoSuchLeaveDayDetailsException if a leave day details with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveDayDetails findByPrimaryKey(
		long leaveDayDetailsId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveDayDetailsException {
		return getPersistence().findByPrimaryKey(leaveDayDetailsId);
	}

	/**
	* Returns the leave day details with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param leaveDayDetailsId the primary key of the leave day details
	* @return the leave day details, or <code>null</code> if a leave day details with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveDayDetails fetchByPrimaryKey(
		long leaveDayDetailsId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(leaveDayDetailsId);
	}

	/**
	* Returns all the leave day detailses where leaveId = &#63;.
	*
	* @param leaveId the leave ID
	* @return the matching leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveDayDetails> findByLeaveId(
		long leaveId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLeaveId(leaveId);
	}

	/**
	* Returns a range of all the leave day detailses where leaveId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param leaveId the leave ID
	* @param start the lower bound of the range of leave day detailses
	* @param end the upper bound of the range of leave day detailses (not inclusive)
	* @return the range of matching leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveDayDetails> findByLeaveId(
		long leaveId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLeaveId(leaveId, start, end);
	}

	/**
	* Returns an ordered range of all the leave day detailses where leaveId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param leaveId the leave ID
	* @param start the lower bound of the range of leave day detailses
	* @param end the upper bound of the range of leave day detailses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveDayDetails> findByLeaveId(
		long leaveId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLeaveId(leaveId, start, end, orderByComparator);
	}

	/**
	* Returns the first leave day details in the ordered set where leaveId = &#63;.
	*
	* @param leaveId the leave ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave day details
	* @throws info.diit.portal.NoSuchLeaveDayDetailsException if a matching leave day details could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveDayDetails findByLeaveId_First(
		long leaveId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveDayDetailsException {
		return getPersistence().findByLeaveId_First(leaveId, orderByComparator);
	}

	/**
	* Returns the first leave day details in the ordered set where leaveId = &#63;.
	*
	* @param leaveId the leave ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave day details, or <code>null</code> if a matching leave day details could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveDayDetails fetchByLeaveId_First(
		long leaveId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByLeaveId_First(leaveId, orderByComparator);
	}

	/**
	* Returns the last leave day details in the ordered set where leaveId = &#63;.
	*
	* @param leaveId the leave ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave day details
	* @throws info.diit.portal.NoSuchLeaveDayDetailsException if a matching leave day details could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveDayDetails findByLeaveId_Last(
		long leaveId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveDayDetailsException {
		return getPersistence().findByLeaveId_Last(leaveId, orderByComparator);
	}

	/**
	* Returns the last leave day details in the ordered set where leaveId = &#63;.
	*
	* @param leaveId the leave ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave day details, or <code>null</code> if a matching leave day details could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveDayDetails fetchByLeaveId_Last(
		long leaveId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByLeaveId_Last(leaveId, orderByComparator);
	}

	/**
	* Returns the leave day detailses before and after the current leave day details in the ordered set where leaveId = &#63;.
	*
	* @param leaveDayDetailsId the primary key of the current leave day details
	* @param leaveId the leave ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next leave day details
	* @throws info.diit.portal.NoSuchLeaveDayDetailsException if a leave day details with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveDayDetails[] findByLeaveId_PrevAndNext(
		long leaveDayDetailsId, long leaveId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveDayDetailsException {
		return getPersistence()
				   .findByLeaveId_PrevAndNext(leaveDayDetailsId, leaveId,
			orderByComparator);
	}

	/**
	* Returns all the leave day detailses.
	*
	* @return the leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveDayDetails> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the leave day detailses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leave day detailses
	* @param end the upper bound of the range of leave day detailses (not inclusive)
	* @return the range of leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveDayDetails> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the leave day detailses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leave day detailses
	* @param end the upper bound of the range of leave day detailses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveDayDetails> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the leave day detailses where leaveId = &#63; from the database.
	*
	* @param leaveId the leave ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByLeaveId(long leaveId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByLeaveId(leaveId);
	}

	/**
	* Removes all the leave day detailses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of leave day detailses where leaveId = &#63;.
	*
	* @param leaveId the leave ID
	* @return the number of matching leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByLeaveId(long leaveId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByLeaveId(leaveId);
	}

	/**
	* Returns the number of leave day detailses.
	*
	* @return the number of leave day detailses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static LeaveDayDetailsPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (LeaveDayDetailsPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					LeaveDayDetailsPersistence.class.getName());

			ReferenceRegistry.registerReference(LeaveDayDetailsUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(LeaveDayDetailsPersistence persistence) {
	}

	private static LeaveDayDetailsPersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.CourseOrganization;

import java.util.List;

/**
 * The persistence utility for the course organization service. This utility wraps {@link CourseOrganizationPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see CourseOrganizationPersistence
 * @see CourseOrganizationPersistenceImpl
 * @generated
 */
public class CourseOrganizationUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CourseOrganization courseOrganization) {
		getPersistence().clearCache(courseOrganization);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CourseOrganization> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CourseOrganization> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CourseOrganization> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static CourseOrganization update(
		CourseOrganization courseOrganization, boolean merge)
		throws SystemException {
		return getPersistence().update(courseOrganization, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static CourseOrganization update(
		CourseOrganization courseOrganization, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(courseOrganization, merge, serviceContext);
	}

	/**
	* Caches the course organization in the entity cache if it is enabled.
	*
	* @param courseOrganization the course organization
	*/
	public static void cacheResult(
		info.diit.portal.model.CourseOrganization courseOrganization) {
		getPersistence().cacheResult(courseOrganization);
	}

	/**
	* Caches the course organizations in the entity cache if it is enabled.
	*
	* @param courseOrganizations the course organizations
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.CourseOrganization> courseOrganizations) {
		getPersistence().cacheResult(courseOrganizations);
	}

	/**
	* Creates a new course organization with the primary key. Does not add the course organization to the database.
	*
	* @param courseOrganizationId the primary key for the new course organization
	* @return the new course organization
	*/
	public static info.diit.portal.model.CourseOrganization create(
		long courseOrganizationId) {
		return getPersistence().create(courseOrganizationId);
	}

	/**
	* Removes the course organization with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param courseOrganizationId the primary key of the course organization
	* @return the course organization that was removed
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization remove(
		long courseOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException {
		return getPersistence().remove(courseOrganizationId);
	}

	public static info.diit.portal.model.CourseOrganization updateImpl(
		info.diit.portal.model.CourseOrganization courseOrganization,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(courseOrganization, merge);
	}

	/**
	* Returns the course organization with the primary key or throws a {@link info.diit.portal.NoSuchCourseOrganizationException} if it could not be found.
	*
	* @param courseOrganizationId the primary key of the course organization
	* @return the course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization findByPrimaryKey(
		long courseOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException {
		return getPersistence().findByPrimaryKey(courseOrganizationId);
	}

	/**
	* Returns the course organization with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param courseOrganizationId the primary key of the course organization
	* @return the course organization, or <code>null</code> if a course organization with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization fetchByPrimaryKey(
		long courseOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(courseOrganizationId);
	}

	/**
	* Returns all the course organizations where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseOrganization> findBycourse(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycourse(courseId);
	}

	/**
	* Returns a range of all the course organizations where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @return the range of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseOrganization> findBycourse(
		long courseId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycourse(courseId, start, end);
	}

	/**
	* Returns an ordered range of all the course organizations where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseOrganization> findBycourse(
		long courseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycourse(courseId, start, end, orderByComparator);
	}

	/**
	* Returns the first course organization in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization findBycourse_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException {
		return getPersistence().findBycourse_First(courseId, orderByComparator);
	}

	/**
	* Returns the first course organization in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course organization, or <code>null</code> if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization fetchBycourse_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBycourse_First(courseId, orderByComparator);
	}

	/**
	* Returns the last course organization in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization findBycourse_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException {
		return getPersistence().findBycourse_Last(courseId, orderByComparator);
	}

	/**
	* Returns the last course organization in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course organization, or <code>null</code> if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization fetchBycourse_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBycourse_Last(courseId, orderByComparator);
	}

	/**
	* Returns the course organizations before and after the current course organization in the ordered set where courseId = &#63;.
	*
	* @param courseOrganizationId the primary key of the current course organization
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization[] findBycourse_PrevAndNext(
		long courseOrganizationId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException {
		return getPersistence()
				   .findBycourse_PrevAndNext(courseOrganizationId, courseId,
			orderByComparator);
	}

	/**
	* Returns all the course organizations where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseOrganization> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId);
	}

	/**
	* Returns a range of all the course organizations where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @return the range of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseOrganization> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the course organizations where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseOrganization> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByorganization(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first course organization in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException {
		return getPersistence()
				   .findByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first course organization in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course organization, or <code>null</code> if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the last course organization in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException {
		return getPersistence()
				   .findByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last course organization in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course organization, or <code>null</code> if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the course organizations before and after the current course organization in the ordered set where organizationId = &#63;.
	*
	* @param courseOrganizationId the primary key of the current course organization
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization[] findByorganization_PrevAndNext(
		long courseOrganizationId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException {
		return getPersistence()
				   .findByorganization_PrevAndNext(courseOrganizationId,
			organizationId, orderByComparator);
	}

	/**
	* Returns all the course organizations where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseOrganization> findBycompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycompany(companyId);
	}

	/**
	* Returns a range of all the course organizations where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @return the range of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseOrganization> findBycompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the course organizations where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseOrganization> findBycompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first course organization in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization findBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException {
		return getPersistence().findBycompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first course organization in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course organization, or <code>null</code> if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization fetchBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBycompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last course organization in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization findBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException {
		return getPersistence().findBycompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last course organization in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course organization, or <code>null</code> if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization fetchBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBycompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the course organizations before and after the current course organization in the ordered set where companyId = &#63;.
	*
	* @param courseOrganizationId the primary key of the current course organization
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseOrganization[] findBycompany_PrevAndNext(
		long courseOrganizationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException {
		return getPersistence()
				   .findBycompany_PrevAndNext(courseOrganizationId, companyId,
			orderByComparator);
	}

	/**
	* Returns all the course organizations.
	*
	* @return the course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseOrganization> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the course organizations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @return the range of course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseOrganization> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the course organizations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseOrganization> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the course organizations where courseId = &#63; from the database.
	*
	* @param courseId the course ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBycourse(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBycourse(courseId);
	}

	/**
	* Removes all the course organizations where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByorganization(organizationId);
	}

	/**
	* Removes all the course organizations where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBycompany(companyId);
	}

	/**
	* Removes all the course organizations from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of course organizations where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the number of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static int countBycourse(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBycourse(courseId);
	}

	/**
	* Returns the number of course organizations where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByorganization(organizationId);
	}

	/**
	* Returns the number of course organizations where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static int countBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBycompany(companyId);
	}

	/**
	* Returns the number of course organizations.
	*
	* @return the number of course organizations
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CourseOrganizationPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CourseOrganizationPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					CourseOrganizationPersistence.class.getName());

			ReferenceRegistry.registerReference(CourseOrganizationUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(CourseOrganizationPersistence persistence) {
	}

	private static CourseOrganizationPersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link ClassRoutineEventBatchLocalService}.
 * </p>
 *
 * @author    mohammad
 * @see       ClassRoutineEventBatchLocalService
 * @generated
 */
public class ClassRoutineEventBatchLocalServiceWrapper
	implements ClassRoutineEventBatchLocalService,
		ServiceWrapper<ClassRoutineEventBatchLocalService> {
	public ClassRoutineEventBatchLocalServiceWrapper(
		ClassRoutineEventBatchLocalService classRoutineEventBatchLocalService) {
		_classRoutineEventBatchLocalService = classRoutineEventBatchLocalService;
	}

	/**
	* Adds the class routine event batch to the database. Also notifies the appropriate model listeners.
	*
	* @param classRoutineEventBatch the class routine event batch
	* @return the class routine event batch that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch addClassRoutineEventBatch(
		info.diit.portal.model.ClassRoutineEventBatch classRoutineEventBatch)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.addClassRoutineEventBatch(classRoutineEventBatch);
	}

	/**
	* Creates a new class routine event batch with the primary key. Does not add the class routine event batch to the database.
	*
	* @param classRoutineEventBatchId the primary key for the new class routine event batch
	* @return the new class routine event batch
	*/
	public info.diit.portal.model.ClassRoutineEventBatch createClassRoutineEventBatch(
		long classRoutineEventBatchId) {
		return _classRoutineEventBatchLocalService.createClassRoutineEventBatch(classRoutineEventBatchId);
	}

	/**
	* Deletes the class routine event batch with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param classRoutineEventBatchId the primary key of the class routine event batch
	* @return the class routine event batch that was removed
	* @throws PortalException if a class routine event batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch deleteClassRoutineEventBatch(
		long classRoutineEventBatchId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.deleteClassRoutineEventBatch(classRoutineEventBatchId);
	}

	/**
	* Deletes the class routine event batch from the database. Also notifies the appropriate model listeners.
	*
	* @param classRoutineEventBatch the class routine event batch
	* @return the class routine event batch that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch deleteClassRoutineEventBatch(
		info.diit.portal.model.ClassRoutineEventBatch classRoutineEventBatch)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.deleteClassRoutineEventBatch(classRoutineEventBatch);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _classRoutineEventBatchLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.ClassRoutineEventBatch fetchClassRoutineEventBatch(
		long classRoutineEventBatchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.fetchClassRoutineEventBatch(classRoutineEventBatchId);
	}

	/**
	* Returns the class routine event batch with the primary key.
	*
	* @param classRoutineEventBatchId the primary key of the class routine event batch
	* @return the class routine event batch
	* @throws PortalException if a class routine event batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch getClassRoutineEventBatch(
		long classRoutineEventBatchId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.getClassRoutineEventBatch(classRoutineEventBatchId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the class routine event batchs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @return the range of class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> getClassRoutineEventBatchs(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.getClassRoutineEventBatchs(start,
			end);
	}

	/**
	* Returns the number of class routine event batchs.
	*
	* @return the number of class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public int getClassRoutineEventBatchsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.getClassRoutineEventBatchsCount();
	}

	/**
	* Updates the class routine event batch in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param classRoutineEventBatch the class routine event batch
	* @return the class routine event batch that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch updateClassRoutineEventBatch(
		info.diit.portal.model.ClassRoutineEventBatch classRoutineEventBatch)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.updateClassRoutineEventBatch(classRoutineEventBatch);
	}

	/**
	* Updates the class routine event batch in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param classRoutineEventBatch the class routine event batch
	* @param merge whether to merge the class routine event batch with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the class routine event batch that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch updateClassRoutineEventBatch(
		info.diit.portal.model.ClassRoutineEventBatch classRoutineEventBatch,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.updateClassRoutineEventBatch(classRoutineEventBatch,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _classRoutineEventBatchLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_classRoutineEventBatchLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _classRoutineEventBatchLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findEventBatchByBatchId(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.findEventBatchByBatchId(batchId);
	}

	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findEventBatchByEventId(
		long eventId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.findEventBatchByEventId(eventId);
	}

	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _classRoutineEventBatchLocalService.findByCompany(companyId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public ClassRoutineEventBatchLocalService getWrappedClassRoutineEventBatchLocalService() {
		return _classRoutineEventBatchLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedClassRoutineEventBatchLocalService(
		ClassRoutineEventBatchLocalService classRoutineEventBatchLocalService) {
		_classRoutineEventBatchLocalService = classRoutineEventBatchLocalService;
	}

	public ClassRoutineEventBatchLocalService getWrappedService() {
		return _classRoutineEventBatchLocalService;
	}

	public void setWrappedService(
		ClassRoutineEventBatchLocalService classRoutineEventBatchLocalService) {
		_classRoutineEventBatchLocalService = classRoutineEventBatchLocalService;
	}

	private ClassRoutineEventBatchLocalService _classRoutineEventBatchLocalService;
}
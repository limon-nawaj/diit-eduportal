/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the employee email local service. This utility wraps {@link info.diit.portal.service.impl.EmployeeEmailLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see EmployeeEmailLocalService
 * @see info.diit.portal.service.base.EmployeeEmailLocalServiceBaseImpl
 * @see info.diit.portal.service.impl.EmployeeEmailLocalServiceImpl
 * @generated
 */
public class EmployeeEmailLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.service.impl.EmployeeEmailLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the employee email to the database. Also notifies the appropriate model listeners.
	*
	* @param employeeEmail the employee email
	* @return the employee email that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeEmail addEmployeeEmail(
		info.diit.portal.model.EmployeeEmail employeeEmail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addEmployeeEmail(employeeEmail);
	}

	/**
	* Creates a new employee email with the primary key. Does not add the employee email to the database.
	*
	* @param emailId the primary key for the new employee email
	* @return the new employee email
	*/
	public static info.diit.portal.model.EmployeeEmail createEmployeeEmail(
		long emailId) {
		return getService().createEmployeeEmail(emailId);
	}

	/**
	* Deletes the employee email with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param emailId the primary key of the employee email
	* @return the employee email that was removed
	* @throws PortalException if a employee email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeEmail deleteEmployeeEmail(
		long emailId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteEmployeeEmail(emailId);
	}

	/**
	* Deletes the employee email from the database. Also notifies the appropriate model listeners.
	*
	* @param employeeEmail the employee email
	* @return the employee email that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeEmail deleteEmployeeEmail(
		info.diit.portal.model.EmployeeEmail employeeEmail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteEmployeeEmail(employeeEmail);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.model.EmployeeEmail fetchEmployeeEmail(
		long emailId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchEmployeeEmail(emailId);
	}

	/**
	* Returns the employee email with the primary key.
	*
	* @param emailId the primary key of the employee email
	* @return the employee email
	* @throws PortalException if a employee email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeEmail getEmployeeEmail(
		long emailId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getEmployeeEmail(emailId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the employee emails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee emails
	* @param end the upper bound of the range of employee emails (not inclusive)
	* @return the range of employee emails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeEmail> getEmployeeEmails(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getEmployeeEmails(start, end);
	}

	/**
	* Returns the number of employee emails.
	*
	* @return the number of employee emails
	* @throws SystemException if a system exception occurred
	*/
	public static int getEmployeeEmailsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getEmployeeEmailsCount();
	}

	/**
	* Updates the employee email in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param employeeEmail the employee email
	* @return the employee email that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeEmail updateEmployeeEmail(
		info.diit.portal.model.EmployeeEmail employeeEmail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateEmployeeEmail(employeeEmail);
	}

	/**
	* Updates the employee email in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param employeeEmail the employee email
	* @param merge whether to merge the employee email with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the employee email that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeEmail updateEmployeeEmail(
		info.diit.portal.model.EmployeeEmail employeeEmail, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateEmployeeEmail(employeeEmail, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<info.diit.portal.model.EmployeeEmail> findByEmployee(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByEmployee(employeeId);
	}

	public static void clearService() {
		_service = null;
	}

	public static EmployeeEmailLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					EmployeeEmailLocalService.class.getName());

			if (invokableLocalService instanceof EmployeeEmailLocalService) {
				_service = (EmployeeEmailLocalService)invokableLocalService;
			}
			else {
				_service = new EmployeeEmailLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(EmployeeEmailLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(EmployeeEmailLocalService service) {
	}

	private static EmployeeEmailLocalService _service;
}
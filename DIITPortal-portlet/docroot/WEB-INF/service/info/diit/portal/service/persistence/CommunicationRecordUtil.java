/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.CommunicationRecord;

import java.util.List;

/**
 * The persistence utility for the communication record service. This utility wraps {@link CommunicationRecordPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see CommunicationRecordPersistence
 * @see CommunicationRecordPersistenceImpl
 * @generated
 */
public class CommunicationRecordUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CommunicationRecord communicationRecord) {
		getPersistence().clearCache(communicationRecord);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CommunicationRecord> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CommunicationRecord> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CommunicationRecord> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static CommunicationRecord update(
		CommunicationRecord communicationRecord, boolean merge)
		throws SystemException {
		return getPersistence().update(communicationRecord, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static CommunicationRecord update(
		CommunicationRecord communicationRecord, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence()
				   .update(communicationRecord, merge, serviceContext);
	}

	/**
	* Caches the communication record in the entity cache if it is enabled.
	*
	* @param communicationRecord the communication record
	*/
	public static void cacheResult(
		info.diit.portal.model.CommunicationRecord communicationRecord) {
		getPersistence().cacheResult(communicationRecord);
	}

	/**
	* Caches the communication records in the entity cache if it is enabled.
	*
	* @param communicationRecords the communication records
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.CommunicationRecord> communicationRecords) {
		getPersistence().cacheResult(communicationRecords);
	}

	/**
	* Creates a new communication record with the primary key. Does not add the communication record to the database.
	*
	* @param communicationRecordId the primary key for the new communication record
	* @return the new communication record
	*/
	public static info.diit.portal.model.CommunicationRecord create(
		long communicationRecordId) {
		return getPersistence().create(communicationRecordId);
	}

	/**
	* Removes the communication record with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param communicationRecordId the primary key of the communication record
	* @return the communication record that was removed
	* @throws info.diit.portal.NoSuchCommunicationRecordException if a communication record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CommunicationRecord remove(
		long communicationRecordId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCommunicationRecordException {
		return getPersistence().remove(communicationRecordId);
	}

	public static info.diit.portal.model.CommunicationRecord updateImpl(
		info.diit.portal.model.CommunicationRecord communicationRecord,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(communicationRecord, merge);
	}

	/**
	* Returns the communication record with the primary key or throws a {@link info.diit.portal.NoSuchCommunicationRecordException} if it could not be found.
	*
	* @param communicationRecordId the primary key of the communication record
	* @return the communication record
	* @throws info.diit.portal.NoSuchCommunicationRecordException if a communication record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CommunicationRecord findByPrimaryKey(
		long communicationRecordId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCommunicationRecordException {
		return getPersistence().findByPrimaryKey(communicationRecordId);
	}

	/**
	* Returns the communication record with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param communicationRecordId the primary key of the communication record
	* @return the communication record, or <code>null</code> if a communication record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CommunicationRecord fetchByPrimaryKey(
		long communicationRecordId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(communicationRecordId);
	}

	/**
	* Returns all the communication records where communicationBy = &#63;.
	*
	* @param communicationBy the communication by
	* @return the matching communication records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CommunicationRecord> findByCommunicationBy(
		long communicationBy)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCommunicationBy(communicationBy);
	}

	/**
	* Returns a range of all the communication records where communicationBy = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param communicationBy the communication by
	* @param start the lower bound of the range of communication records
	* @param end the upper bound of the range of communication records (not inclusive)
	* @return the range of matching communication records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CommunicationRecord> findByCommunicationBy(
		long communicationBy, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCommunicationBy(communicationBy, start, end);
	}

	/**
	* Returns an ordered range of all the communication records where communicationBy = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param communicationBy the communication by
	* @param start the lower bound of the range of communication records
	* @param end the upper bound of the range of communication records (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching communication records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CommunicationRecord> findByCommunicationBy(
		long communicationBy, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCommunicationBy(communicationBy, start, end,
			orderByComparator);
	}

	/**
	* Returns the first communication record in the ordered set where communicationBy = &#63;.
	*
	* @param communicationBy the communication by
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching communication record
	* @throws info.diit.portal.NoSuchCommunicationRecordException if a matching communication record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CommunicationRecord findByCommunicationBy_First(
		long communicationBy,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCommunicationRecordException {
		return getPersistence()
				   .findByCommunicationBy_First(communicationBy,
			orderByComparator);
	}

	/**
	* Returns the first communication record in the ordered set where communicationBy = &#63;.
	*
	* @param communicationBy the communication by
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching communication record, or <code>null</code> if a matching communication record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CommunicationRecord fetchByCommunicationBy_First(
		long communicationBy,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCommunicationBy_First(communicationBy,
			orderByComparator);
	}

	/**
	* Returns the last communication record in the ordered set where communicationBy = &#63;.
	*
	* @param communicationBy the communication by
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching communication record
	* @throws info.diit.portal.NoSuchCommunicationRecordException if a matching communication record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CommunicationRecord findByCommunicationBy_Last(
		long communicationBy,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCommunicationRecordException {
		return getPersistence()
				   .findByCommunicationBy_Last(communicationBy,
			orderByComparator);
	}

	/**
	* Returns the last communication record in the ordered set where communicationBy = &#63;.
	*
	* @param communicationBy the communication by
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching communication record, or <code>null</code> if a matching communication record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CommunicationRecord fetchByCommunicationBy_Last(
		long communicationBy,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCommunicationBy_Last(communicationBy,
			orderByComparator);
	}

	/**
	* Returns the communication records before and after the current communication record in the ordered set where communicationBy = &#63;.
	*
	* @param communicationRecordId the primary key of the current communication record
	* @param communicationBy the communication by
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next communication record
	* @throws info.diit.portal.NoSuchCommunicationRecordException if a communication record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CommunicationRecord[] findByCommunicationBy_PrevAndNext(
		long communicationRecordId, long communicationBy,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCommunicationRecordException {
		return getPersistence()
				   .findByCommunicationBy_PrevAndNext(communicationRecordId,
			communicationBy, orderByComparator);
	}

	/**
	* Returns all the communication records.
	*
	* @return the communication records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CommunicationRecord> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the communication records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of communication records
	* @param end the upper bound of the range of communication records (not inclusive)
	* @return the range of communication records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CommunicationRecord> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the communication records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of communication records
	* @param end the upper bound of the range of communication records (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of communication records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CommunicationRecord> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the communication records where communicationBy = &#63; from the database.
	*
	* @param communicationBy the communication by
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCommunicationBy(long communicationBy)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCommunicationBy(communicationBy);
	}

	/**
	* Removes all the communication records from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of communication records where communicationBy = &#63;.
	*
	* @param communicationBy the communication by
	* @return the number of matching communication records
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCommunicationBy(long communicationBy)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCommunicationBy(communicationBy);
	}

	/**
	* Returns the number of communication records.
	*
	* @return the number of communication records
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CommunicationRecordPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CommunicationRecordPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					CommunicationRecordPersistence.class.getName());

			ReferenceRegistry.registerReference(CommunicationRecordUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(CommunicationRecordPersistence persistence) {
	}

	private static CommunicationRecordPersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link TaskDesignationLocalService}.
 * </p>
 *
 * @author    mohammad
 * @see       TaskDesignationLocalService
 * @generated
 */
public class TaskDesignationLocalServiceWrapper
	implements TaskDesignationLocalService,
		ServiceWrapper<TaskDesignationLocalService> {
	public TaskDesignationLocalServiceWrapper(
		TaskDesignationLocalService taskDesignationLocalService) {
		_taskDesignationLocalService = taskDesignationLocalService;
	}

	/**
	* Adds the task designation to the database. Also notifies the appropriate model listeners.
	*
	* @param taskDesignation the task designation
	* @return the task designation that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation addTaskDesignation(
		info.diit.portal.model.TaskDesignation taskDesignation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.addTaskDesignation(taskDesignation);
	}

	/**
	* Creates a new task designation with the primary key. Does not add the task designation to the database.
	*
	* @param taskDesignationId the primary key for the new task designation
	* @return the new task designation
	*/
	public info.diit.portal.model.TaskDesignation createTaskDesignation(
		long taskDesignationId) {
		return _taskDesignationLocalService.createTaskDesignation(taskDesignationId);
	}

	/**
	* Deletes the task designation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param taskDesignationId the primary key of the task designation
	* @return the task designation that was removed
	* @throws PortalException if a task designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation deleteTaskDesignation(
		long taskDesignationId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.deleteTaskDesignation(taskDesignationId);
	}

	/**
	* Deletes the task designation from the database. Also notifies the appropriate model listeners.
	*
	* @param taskDesignation the task designation
	* @return the task designation that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation deleteTaskDesignation(
		info.diit.portal.model.TaskDesignation taskDesignation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.deleteTaskDesignation(taskDesignation);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _taskDesignationLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.TaskDesignation fetchTaskDesignation(
		long taskDesignationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.fetchTaskDesignation(taskDesignationId);
	}

	/**
	* Returns the task designation with the primary key.
	*
	* @param taskDesignationId the primary key of the task designation
	* @return the task designation
	* @throws PortalException if a task designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation getTaskDesignation(
		long taskDesignationId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.getTaskDesignation(taskDesignationId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the task designations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @return the range of task designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.TaskDesignation> getTaskDesignations(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.getTaskDesignations(start, end);
	}

	/**
	* Returns the number of task designations.
	*
	* @return the number of task designations
	* @throws SystemException if a system exception occurred
	*/
	public int getTaskDesignationsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.getTaskDesignationsCount();
	}

	/**
	* Updates the task designation in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param taskDesignation the task designation
	* @return the task designation that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation updateTaskDesignation(
		info.diit.portal.model.TaskDesignation taskDesignation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.updateTaskDesignation(taskDesignation);
	}

	/**
	* Updates the task designation in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param taskDesignation the task designation
	* @param merge whether to merge the task designation with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the task designation that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation updateTaskDesignation(
		info.diit.portal.model.TaskDesignation taskDesignation, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.updateTaskDesignation(taskDesignation,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _taskDesignationLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_taskDesignationLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _taskDesignationLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.model.TaskDesignation> findByDesignation(
		long designationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taskDesignationLocalService.findByDesignation(designationId);
	}

	public info.diit.portal.model.TaskDesignation findByDesignationTask(
		long designationId, long taskId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchTaskDesignationException {
		return _taskDesignationLocalService.findByDesignationTask(designationId,
			taskId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public TaskDesignationLocalService getWrappedTaskDesignationLocalService() {
		return _taskDesignationLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedTaskDesignationLocalService(
		TaskDesignationLocalService taskDesignationLocalService) {
		_taskDesignationLocalService = taskDesignationLocalService;
	}

	public TaskDesignationLocalService getWrappedService() {
		return _taskDesignationLocalService;
	}

	public void setWrappedService(
		TaskDesignationLocalService taskDesignationLocalService) {
		_taskDesignationLocalService = taskDesignationLocalService;
	}

	private TaskDesignationLocalService _taskDesignationLocalService;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.Batch;

import java.util.List;

/**
 * The persistence utility for the batch service. This utility wraps {@link BatchPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see BatchPersistence
 * @see BatchPersistenceImpl
 * @generated
 */
public class BatchUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Batch batch) {
		getPersistence().clearCache(batch);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Batch> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Batch> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Batch> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Batch update(Batch batch, boolean merge)
		throws SystemException {
		return getPersistence().update(batch, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Batch update(Batch batch, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(batch, merge, serviceContext);
	}

	/**
	* Caches the batch in the entity cache if it is enabled.
	*
	* @param batch the batch
	*/
	public static void cacheResult(info.diit.portal.model.Batch batch) {
		getPersistence().cacheResult(batch);
	}

	/**
	* Caches the batchs in the entity cache if it is enabled.
	*
	* @param batchs the batchs
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.Batch> batchs) {
		getPersistence().cacheResult(batchs);
	}

	/**
	* Creates a new batch with the primary key. Does not add the batch to the database.
	*
	* @param batchId the primary key for the new batch
	* @return the new batch
	*/
	public static info.diit.portal.model.Batch create(long batchId) {
		return getPersistence().create(batchId);
	}

	/**
	* Removes the batch with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchId the primary key of the batch
	* @return the batch that was removed
	* @throws info.diit.portal.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch remove(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence().remove(batchId);
	}

	public static info.diit.portal.model.Batch updateImpl(
		info.diit.portal.model.Batch batch, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(batch, merge);
	}

	/**
	* Returns the batch with the primary key or throws a {@link info.diit.portal.NoSuchBatchException} if it could not be found.
	*
	* @param batchId the primary key of the batch
	* @return the batch
	* @throws info.diit.portal.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch findByPrimaryKey(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence().findByPrimaryKey(batchId);
	}

	/**
	* Returns the batch with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param batchId the primary key of the batch
	* @return the batch, or <code>null</code> if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch fetchByPrimaryKey(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(batchId);
	}

	/**
	* Returns all the batchs where organizationId = &#63; and companyId = &#63;.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @return the matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findByBatchesByComOrg(
		long organizationId, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBatchesByComOrg(organizationId, companyId);
	}

	/**
	* Returns a range of all the batchs where organizationId = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @return the range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findByBatchesByComOrg(
		long organizationId, long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatchesByComOrg(organizationId, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the batchs where organizationId = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findByBatchesByComOrg(
		long organizationId, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatchesByComOrg(organizationId, companyId, start,
			end, orderByComparator);
	}

	/**
	* Returns the first batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch
	* @throws info.diit.portal.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch findByBatchesByComOrg_First(
		long organizationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence()
				   .findByBatchesByComOrg_First(organizationId, companyId,
			orderByComparator);
	}

	/**
	* Returns the first batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch fetchByBatchesByComOrg_First(
		long organizationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBatchesByComOrg_First(organizationId, companyId,
			orderByComparator);
	}

	/**
	* Returns the last batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch
	* @throws info.diit.portal.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch findByBatchesByComOrg_Last(
		long organizationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence()
				   .findByBatchesByComOrg_Last(organizationId, companyId,
			orderByComparator);
	}

	/**
	* Returns the last batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch fetchByBatchesByComOrg_Last(
		long organizationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBatchesByComOrg_Last(organizationId, companyId,
			orderByComparator);
	}

	/**
	* Returns the batchs before and after the current batch in the ordered set where organizationId = &#63; and companyId = &#63;.
	*
	* @param batchId the primary key of the current batch
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch
	* @throws info.diit.portal.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch[] findByBatchesByComOrg_PrevAndNext(
		long batchId, long organizationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence()
				   .findByBatchesByComOrg_PrevAndNext(batchId, organizationId,
			companyId, orderByComparator);
	}

	/**
	* Returns all the batchs where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findByCompanyId(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompanyId(companyId);
	}

	/**
	* Returns a range of all the batchs where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @return the range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findByCompanyId(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompanyId(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the batchs where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findByCompanyId(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCompanyId(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch
	* @throws info.diit.portal.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch findByCompanyId_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence()
				   .findByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the first batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch fetchByCompanyId_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the last batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch
	* @throws info.diit.portal.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch findByCompanyId_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence()
				   .findByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch fetchByCompanyId_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the batchs before and after the current batch in the ordered set where companyId = &#63;.
	*
	* @param batchId the primary key of the current batch
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch
	* @throws info.diit.portal.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch[] findByCompanyId_PrevAndNext(
		long batchId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence()
				   .findByCompanyId_PrevAndNext(batchId, companyId,
			orderByComparator);
	}

	/**
	* Returns the batch where organizationId = &#63; and companyId = &#63; and batchName = &#63; or throws a {@link info.diit.portal.NoSuchBatchException} if it could not be found.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param batchName the batch name
	* @return the matching batch
	* @throws info.diit.portal.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch findByBatchByCompanyOrg(
		long organizationId, long companyId, java.lang.String batchName)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence()
				   .findByBatchByCompanyOrg(organizationId, companyId, batchName);
	}

	/**
	* Returns the batch where organizationId = &#63; and companyId = &#63; and batchName = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param batchName the batch name
	* @return the matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch fetchByBatchByCompanyOrg(
		long organizationId, long companyId, java.lang.String batchName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBatchByCompanyOrg(organizationId, companyId,
			batchName);
	}

	/**
	* Returns the batch where organizationId = &#63; and companyId = &#63; and batchName = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param batchName the batch name
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch fetchByBatchByCompanyOrg(
		long organizationId, long companyId, java.lang.String batchName,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBatchByCompanyOrg(organizationId, companyId,
			batchName, retrieveFromCache);
	}

	/**
	* Returns the batch where batchId = &#63; or throws a {@link info.diit.portal.NoSuchBatchException} if it could not be found.
	*
	* @param batchId the batch ID
	* @return the matching batch
	* @throws info.diit.portal.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch findByBatchId(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence().findByBatchId(batchId);
	}

	/**
	* Returns the batch where batchId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param batchId the batch ID
	* @return the matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch fetchByBatchId(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByBatchId(batchId);
	}

	/**
	* Returns the batch where batchId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param batchId the batch ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch fetchByBatchId(long batchId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByBatchId(batchId, retrieveFromCache);
	}

	/**
	* Returns all the batchs where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findByCourseId(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCourseId(courseId);
	}

	/**
	* Returns a range of all the batchs where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @return the range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findByCourseId(
		long courseId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCourseId(courseId, start, end);
	}

	/**
	* Returns an ordered range of all the batchs where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findByCourseId(
		long courseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCourseId(courseId, start, end, orderByComparator);
	}

	/**
	* Returns the first batch in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch
	* @throws info.diit.portal.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch findByCourseId_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence().findByCourseId_First(courseId, orderByComparator);
	}

	/**
	* Returns the first batch in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch fetchByCourseId_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCourseId_First(courseId, orderByComparator);
	}

	/**
	* Returns the last batch in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch
	* @throws info.diit.portal.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch findByCourseId_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence().findByCourseId_Last(courseId, orderByComparator);
	}

	/**
	* Returns the last batch in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch fetchByCourseId_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCourseId_Last(courseId, orderByComparator);
	}

	/**
	* Returns the batchs before and after the current batch in the ordered set where courseId = &#63;.
	*
	* @param batchId the primary key of the current batch
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch
	* @throws info.diit.portal.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch[] findByCourseId_PrevAndNext(
		long batchId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence()
				   .findByCourseId_PrevAndNext(batchId, courseId,
			orderByComparator);
	}

	/**
	* Returns all the batchs where organizationId = &#63; and courseId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @return the matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findByBatchesByOrgCourse(
		long organizationId, long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatchesByOrgCourse(organizationId, courseId);
	}

	/**
	* Returns a range of all the batchs where organizationId = &#63; and courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @return the range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findByBatchesByOrgCourse(
		long organizationId, long courseId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatchesByOrgCourse(organizationId, courseId, start,
			end);
	}

	/**
	* Returns an ordered range of all the batchs where organizationId = &#63; and courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findByBatchesByOrgCourse(
		long organizationId, long courseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatchesByOrgCourse(organizationId, courseId, start,
			end, orderByComparator);
	}

	/**
	* Returns the first batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch
	* @throws info.diit.portal.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch findByBatchesByOrgCourse_First(
		long organizationId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence()
				   .findByBatchesByOrgCourse_First(organizationId, courseId,
			orderByComparator);
	}

	/**
	* Returns the first batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch fetchByBatchesByOrgCourse_First(
		long organizationId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBatchesByOrgCourse_First(organizationId, courseId,
			orderByComparator);
	}

	/**
	* Returns the last batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch
	* @throws info.diit.portal.NoSuchBatchException if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch findByBatchesByOrgCourse_Last(
		long organizationId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence()
				   .findByBatchesByOrgCourse_Last(organizationId, courseId,
			orderByComparator);
	}

	/**
	* Returns the last batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch, or <code>null</code> if a matching batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch fetchByBatchesByOrgCourse_Last(
		long organizationId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBatchesByOrgCourse_Last(organizationId, courseId,
			orderByComparator);
	}

	/**
	* Returns the batchs before and after the current batch in the ordered set where organizationId = &#63; and courseId = &#63;.
	*
	* @param batchId the primary key of the current batch
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch
	* @throws info.diit.portal.NoSuchBatchException if a batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch[] findByBatchesByOrgCourse_PrevAndNext(
		long batchId, long organizationId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence()
				   .findByBatchesByOrgCourse_PrevAndNext(batchId,
			organizationId, courseId, orderByComparator);
	}

	/**
	* Returns all the batchs.
	*
	* @return the batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the batchs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @return the range of batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the batchs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batchs
	* @param end the upper bound of the range of batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Batch> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the batchs where organizationId = &#63; and companyId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBatchesByComOrg(long organizationId,
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBatchesByComOrg(organizationId, companyId);
	}

	/**
	* Removes all the batchs where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCompanyId(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCompanyId(companyId);
	}

	/**
	* Removes the batch where organizationId = &#63; and companyId = &#63; and batchName = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param batchName the batch name
	* @return the batch that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch removeByBatchByCompanyOrg(
		long organizationId, long companyId, java.lang.String batchName)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence()
				   .removeByBatchByCompanyOrg(organizationId, companyId,
			batchName);
	}

	/**
	* Removes the batch where batchId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @return the batch that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Batch removeByBatchId(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchException {
		return getPersistence().removeByBatchId(batchId);
	}

	/**
	* Removes all the batchs where courseId = &#63; from the database.
	*
	* @param courseId the course ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCourseId(courseId);
	}

	/**
	* Removes all the batchs where organizationId = &#63; and courseId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBatchesByOrgCourse(long organizationId,
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBatchesByOrgCourse(organizationId, courseId);
	}

	/**
	* Removes all the batchs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of batchs where organizationId = &#63; and companyId = &#63;.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @return the number of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBatchesByComOrg(long organizationId, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBatchesByComOrg(organizationId, companyId);
	}

	/**
	* Returns the number of batchs where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCompanyId(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCompanyId(companyId);
	}

	/**
	* Returns the number of batchs where organizationId = &#63; and companyId = &#63; and batchName = &#63;.
	*
	* @param organizationId the organization ID
	* @param companyId the company ID
	* @param batchName the batch name
	* @return the number of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBatchByCompanyOrg(long organizationId,
		long companyId, java.lang.String batchName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByBatchByCompanyOrg(organizationId, companyId,
			batchName);
	}

	/**
	* Returns the number of batchs where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the number of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBatchId(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBatchId(batchId);
	}

	/**
	* Returns the number of batchs where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the number of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCourseId(courseId);
	}

	/**
	* Returns the number of batchs where organizationId = &#63; and courseId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @return the number of matching batchs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBatchesByOrgCourse(long organizationId,
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByBatchesByOrgCourse(organizationId, courseId);
	}

	/**
	* Returns the number of batchs.
	*
	* @return the number of batchs
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static BatchPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (BatchPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					BatchPersistence.class.getName());

			ReferenceRegistry.registerReference(BatchUtil.class, "_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(BatchPersistence persistence) {
	}

	private static BatchPersistence _persistence;
}
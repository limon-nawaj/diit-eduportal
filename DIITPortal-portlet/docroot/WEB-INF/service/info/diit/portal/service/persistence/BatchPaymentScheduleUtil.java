/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.BatchPaymentSchedule;

import java.util.List;

/**
 * The persistence utility for the batch payment schedule service. This utility wraps {@link BatchPaymentSchedulePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see BatchPaymentSchedulePersistence
 * @see BatchPaymentSchedulePersistenceImpl
 * @generated
 */
public class BatchPaymentScheduleUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(BatchPaymentSchedule batchPaymentSchedule) {
		getPersistence().clearCache(batchPaymentSchedule);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<BatchPaymentSchedule> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<BatchPaymentSchedule> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<BatchPaymentSchedule> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static BatchPaymentSchedule update(
		BatchPaymentSchedule batchPaymentSchedule, boolean merge)
		throws SystemException {
		return getPersistence().update(batchPaymentSchedule, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static BatchPaymentSchedule update(
		BatchPaymentSchedule batchPaymentSchedule, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence()
				   .update(batchPaymentSchedule, merge, serviceContext);
	}

	/**
	* Caches the batch payment schedule in the entity cache if it is enabled.
	*
	* @param batchPaymentSchedule the batch payment schedule
	*/
	public static void cacheResult(
		info.diit.portal.model.BatchPaymentSchedule batchPaymentSchedule) {
		getPersistence().cacheResult(batchPaymentSchedule);
	}

	/**
	* Caches the batch payment schedules in the entity cache if it is enabled.
	*
	* @param batchPaymentSchedules the batch payment schedules
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.BatchPaymentSchedule> batchPaymentSchedules) {
		getPersistence().cacheResult(batchPaymentSchedules);
	}

	/**
	* Creates a new batch payment schedule with the primary key. Does not add the batch payment schedule to the database.
	*
	* @param batchPaymentScheduleId the primary key for the new batch payment schedule
	* @return the new batch payment schedule
	*/
	public static info.diit.portal.model.BatchPaymentSchedule create(
		long batchPaymentScheduleId) {
		return getPersistence().create(batchPaymentScheduleId);
	}

	/**
	* Removes the batch payment schedule with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchPaymentScheduleId the primary key of the batch payment schedule
	* @return the batch payment schedule that was removed
	* @throws info.diit.portal.NoSuchBatchPaymentScheduleException if a batch payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchPaymentSchedule remove(
		long batchPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchPaymentScheduleException {
		return getPersistence().remove(batchPaymentScheduleId);
	}

	public static info.diit.portal.model.BatchPaymentSchedule updateImpl(
		info.diit.portal.model.BatchPaymentSchedule batchPaymentSchedule,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(batchPaymentSchedule, merge);
	}

	/**
	* Returns the batch payment schedule with the primary key or throws a {@link info.diit.portal.NoSuchBatchPaymentScheduleException} if it could not be found.
	*
	* @param batchPaymentScheduleId the primary key of the batch payment schedule
	* @return the batch payment schedule
	* @throws info.diit.portal.NoSuchBatchPaymentScheduleException if a batch payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchPaymentSchedule findByPrimaryKey(
		long batchPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchPaymentScheduleException {
		return getPersistence().findByPrimaryKey(batchPaymentScheduleId);
	}

	/**
	* Returns the batch payment schedule with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param batchPaymentScheduleId the primary key of the batch payment schedule
	* @return the batch payment schedule, or <code>null</code> if a batch payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchPaymentSchedule fetchByPrimaryKey(
		long batchPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(batchPaymentScheduleId);
	}

	/**
	* Returns all the batch payment schedules where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the matching batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchPaymentSchedule> findByBatch(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBatch(batchId);
	}

	/**
	* Returns a range of all the batch payment schedules where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch payment schedules
	* @param end the upper bound of the range of batch payment schedules (not inclusive)
	* @return the range of matching batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchPaymentSchedule> findByBatch(
		long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBatch(batchId, start, end);
	}

	/**
	* Returns an ordered range of all the batch payment schedules where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch payment schedules
	* @param end the upper bound of the range of batch payment schedules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchPaymentSchedule> findByBatch(
		long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatch(batchId, start, end, orderByComparator);
	}

	/**
	* Returns the first batch payment schedule in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch payment schedule
	* @throws info.diit.portal.NoSuchBatchPaymentScheduleException if a matching batch payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchPaymentSchedule findByBatch_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchPaymentScheduleException {
		return getPersistence().findByBatch_First(batchId, orderByComparator);
	}

	/**
	* Returns the first batch payment schedule in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch payment schedule, or <code>null</code> if a matching batch payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchPaymentSchedule fetchByBatch_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByBatch_First(batchId, orderByComparator);
	}

	/**
	* Returns the last batch payment schedule in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch payment schedule
	* @throws info.diit.portal.NoSuchBatchPaymentScheduleException if a matching batch payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchPaymentSchedule findByBatch_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchPaymentScheduleException {
		return getPersistence().findByBatch_Last(batchId, orderByComparator);
	}

	/**
	* Returns the last batch payment schedule in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch payment schedule, or <code>null</code> if a matching batch payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchPaymentSchedule fetchByBatch_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByBatch_Last(batchId, orderByComparator);
	}

	/**
	* Returns the batch payment schedules before and after the current batch payment schedule in the ordered set where batchId = &#63;.
	*
	* @param batchPaymentScheduleId the primary key of the current batch payment schedule
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch payment schedule
	* @throws info.diit.portal.NoSuchBatchPaymentScheduleException if a batch payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchPaymentSchedule[] findByBatch_PrevAndNext(
		long batchPaymentScheduleId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchPaymentScheduleException {
		return getPersistence()
				   .findByBatch_PrevAndNext(batchPaymentScheduleId, batchId,
			orderByComparator);
	}

	/**
	* Returns all the batch payment schedules.
	*
	* @return the batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchPaymentSchedule> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the batch payment schedules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch payment schedules
	* @param end the upper bound of the range of batch payment schedules (not inclusive)
	* @return the range of batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchPaymentSchedule> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the batch payment schedules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch payment schedules
	* @param end the upper bound of the range of batch payment schedules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchPaymentSchedule> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the batch payment schedules where batchId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBatch(batchId);
	}

	/**
	* Removes all the batch payment schedules from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of batch payment schedules where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the number of matching batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBatch(batchId);
	}

	/**
	* Returns the number of batch payment schedules.
	*
	* @return the number of batch payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static BatchPaymentSchedulePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (BatchPaymentSchedulePersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					BatchPaymentSchedulePersistence.class.getName());

			ReferenceRegistry.registerReference(BatchPaymentScheduleUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(BatchPaymentSchedulePersistence persistence) {
	}

	private static BatchPaymentSchedulePersistence _persistence;
}
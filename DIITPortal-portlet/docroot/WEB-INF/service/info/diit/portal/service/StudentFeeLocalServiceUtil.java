/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the student fee local service. This utility wraps {@link info.diit.portal.service.impl.StudentFeeLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see StudentFeeLocalService
 * @see info.diit.portal.service.base.StudentFeeLocalServiceBaseImpl
 * @see info.diit.portal.service.impl.StudentFeeLocalServiceImpl
 * @generated
 */
public class StudentFeeLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.service.impl.StudentFeeLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the student fee to the database. Also notifies the appropriate model listeners.
	*
	* @param studentFee the student fee
	* @return the student fee that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StudentFee addStudentFee(
		info.diit.portal.model.StudentFee studentFee)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addStudentFee(studentFee);
	}

	/**
	* Creates a new student fee with the primary key. Does not add the student fee to the database.
	*
	* @param studentFeeId the primary key for the new student fee
	* @return the new student fee
	*/
	public static info.diit.portal.model.StudentFee createStudentFee(
		long studentFeeId) {
		return getService().createStudentFee(studentFeeId);
	}

	/**
	* Deletes the student fee with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param studentFeeId the primary key of the student fee
	* @return the student fee that was removed
	* @throws PortalException if a student fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StudentFee deleteStudentFee(
		long studentFeeId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteStudentFee(studentFeeId);
	}

	/**
	* Deletes the student fee from the database. Also notifies the appropriate model listeners.
	*
	* @param studentFee the student fee
	* @return the student fee that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StudentFee deleteStudentFee(
		info.diit.portal.model.StudentFee studentFee)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteStudentFee(studentFee);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.model.StudentFee fetchStudentFee(
		long studentFeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchStudentFee(studentFeeId);
	}

	/**
	* Returns the student fee with the primary key.
	*
	* @param studentFeeId the primary key of the student fee
	* @return the student fee
	* @throws PortalException if a student fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StudentFee getStudentFee(
		long studentFeeId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getStudentFee(studentFeeId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the student fees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student fees
	* @param end the upper bound of the range of student fees (not inclusive)
	* @return the range of student fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.StudentFee> getStudentFees(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getStudentFees(start, end);
	}

	/**
	* Returns the number of student fees.
	*
	* @return the number of student fees
	* @throws SystemException if a system exception occurred
	*/
	public static int getStudentFeesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getStudentFeesCount();
	}

	/**
	* Updates the student fee in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param studentFee the student fee
	* @return the student fee that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StudentFee updateStudentFee(
		info.diit.portal.model.StudentFee studentFee)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateStudentFee(studentFee);
	}

	/**
	* Updates the student fee in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param studentFee the student fee
	* @param merge whether to merge the student fee with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the student fee that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StudentFee updateStudentFee(
		info.diit.portal.model.StudentFee studentFee, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateStudentFee(studentFee, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static void clearService() {
		_service = null;
	}

	public static StudentFeeLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					StudentFeeLocalService.class.getName());

			if (invokableLocalService instanceof StudentFeeLocalService) {
				_service = (StudentFeeLocalService)invokableLocalService;
			}
			else {
				_service = new StudentFeeLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(StudentFeeLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(StudentFeeLocalService service) {
	}

	private static StudentFeeLocalService _service;
}
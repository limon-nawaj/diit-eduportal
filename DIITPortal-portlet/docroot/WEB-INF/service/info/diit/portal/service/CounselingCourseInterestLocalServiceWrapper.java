/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link CounselingCourseInterestLocalService}.
 * </p>
 *
 * @author    mohammad
 * @see       CounselingCourseInterestLocalService
 * @generated
 */
public class CounselingCourseInterestLocalServiceWrapper
	implements CounselingCourseInterestLocalService,
		ServiceWrapper<CounselingCourseInterestLocalService> {
	public CounselingCourseInterestLocalServiceWrapper(
		CounselingCourseInterestLocalService counselingCourseInterestLocalService) {
		_counselingCourseInterestLocalService = counselingCourseInterestLocalService;
	}

	/**
	* Adds the counseling course interest to the database. Also notifies the appropriate model listeners.
	*
	* @param counselingCourseInterest the counseling course interest
	* @return the counseling course interest that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CounselingCourseInterest addCounselingCourseInterest(
		info.diit.portal.model.CounselingCourseInterest counselingCourseInterest)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _counselingCourseInterestLocalService.addCounselingCourseInterest(counselingCourseInterest);
	}

	/**
	* Creates a new counseling course interest with the primary key. Does not add the counseling course interest to the database.
	*
	* @param CounselingCourseInterestId the primary key for the new counseling course interest
	* @return the new counseling course interest
	*/
	public info.diit.portal.model.CounselingCourseInterest createCounselingCourseInterest(
		long CounselingCourseInterestId) {
		return _counselingCourseInterestLocalService.createCounselingCourseInterest(CounselingCourseInterestId);
	}

	/**
	* Deletes the counseling course interest with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param CounselingCourseInterestId the primary key of the counseling course interest
	* @return the counseling course interest that was removed
	* @throws PortalException if a counseling course interest with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CounselingCourseInterest deleteCounselingCourseInterest(
		long CounselingCourseInterestId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _counselingCourseInterestLocalService.deleteCounselingCourseInterest(CounselingCourseInterestId);
	}

	/**
	* Deletes the counseling course interest from the database. Also notifies the appropriate model listeners.
	*
	* @param counselingCourseInterest the counseling course interest
	* @return the counseling course interest that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CounselingCourseInterest deleteCounselingCourseInterest(
		info.diit.portal.model.CounselingCourseInterest counselingCourseInterest)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _counselingCourseInterestLocalService.deleteCounselingCourseInterest(counselingCourseInterest);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _counselingCourseInterestLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _counselingCourseInterestLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _counselingCourseInterestLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _counselingCourseInterestLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _counselingCourseInterestLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.CounselingCourseInterest fetchCounselingCourseInterest(
		long CounselingCourseInterestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _counselingCourseInterestLocalService.fetchCounselingCourseInterest(CounselingCourseInterestId);
	}

	/**
	* Returns the counseling course interest with the primary key.
	*
	* @param CounselingCourseInterestId the primary key of the counseling course interest
	* @return the counseling course interest
	* @throws PortalException if a counseling course interest with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CounselingCourseInterest getCounselingCourseInterest(
		long CounselingCourseInterestId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _counselingCourseInterestLocalService.getCounselingCourseInterest(CounselingCourseInterestId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _counselingCourseInterestLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the counseling course interests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of counseling course interests
	* @param end the upper bound of the range of counseling course interests (not inclusive)
	* @return the range of counseling course interests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CounselingCourseInterest> getCounselingCourseInterests(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _counselingCourseInterestLocalService.getCounselingCourseInterests(start,
			end);
	}

	/**
	* Returns the number of counseling course interests.
	*
	* @return the number of counseling course interests
	* @throws SystemException if a system exception occurred
	*/
	public int getCounselingCourseInterestsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _counselingCourseInterestLocalService.getCounselingCourseInterestsCount();
	}

	/**
	* Updates the counseling course interest in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param counselingCourseInterest the counseling course interest
	* @return the counseling course interest that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CounselingCourseInterest updateCounselingCourseInterest(
		info.diit.portal.model.CounselingCourseInterest counselingCourseInterest)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _counselingCourseInterestLocalService.updateCounselingCourseInterest(counselingCourseInterest);
	}

	/**
	* Updates the counseling course interest in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param counselingCourseInterest the counseling course interest
	* @param merge whether to merge the counseling course interest with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the counseling course interest that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CounselingCourseInterest updateCounselingCourseInterest(
		info.diit.portal.model.CounselingCourseInterest counselingCourseInterest,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _counselingCourseInterestLocalService.updateCounselingCourseInterest(counselingCourseInterest,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _counselingCourseInterestLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_counselingCourseInterestLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _counselingCourseInterestLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public CounselingCourseInterestLocalService getWrappedCounselingCourseInterestLocalService() {
		return _counselingCourseInterestLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedCounselingCourseInterestLocalService(
		CounselingCourseInterestLocalService counselingCourseInterestLocalService) {
		_counselingCourseInterestLocalService = counselingCourseInterestLocalService;
	}

	public CounselingCourseInterestLocalService getWrappedService() {
		return _counselingCourseInterestLocalService;
	}

	public void setWrappedService(
		CounselingCourseInterestLocalService counselingCourseInterestLocalService) {
		_counselingCourseInterestLocalService = counselingCourseInterestLocalService;
	}

	private CounselingCourseInterestLocalService _counselingCourseInterestLocalService;
}
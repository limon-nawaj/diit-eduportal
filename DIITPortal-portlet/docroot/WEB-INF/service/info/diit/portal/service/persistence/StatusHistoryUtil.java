/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.StatusHistory;

import java.util.List;

/**
 * The persistence utility for the status history service. This utility wraps {@link StatusHistoryPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see StatusHistoryPersistence
 * @see StatusHistoryPersistenceImpl
 * @generated
 */
public class StatusHistoryUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(StatusHistory statusHistory) {
		getPersistence().clearCache(statusHistory);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<StatusHistory> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<StatusHistory> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<StatusHistory> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static StatusHistory update(StatusHistory statusHistory,
		boolean merge) throws SystemException {
		return getPersistence().update(statusHistory, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static StatusHistory update(StatusHistory statusHistory,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(statusHistory, merge, serviceContext);
	}

	/**
	* Caches the status history in the entity cache if it is enabled.
	*
	* @param statusHistory the status history
	*/
	public static void cacheResult(
		info.diit.portal.model.StatusHistory statusHistory) {
		getPersistence().cacheResult(statusHistory);
	}

	/**
	* Caches the status histories in the entity cache if it is enabled.
	*
	* @param statusHistories the status histories
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.StatusHistory> statusHistories) {
		getPersistence().cacheResult(statusHistories);
	}

	/**
	* Creates a new status history with the primary key. Does not add the status history to the database.
	*
	* @param statusHistoryId the primary key for the new status history
	* @return the new status history
	*/
	public static info.diit.portal.model.StatusHistory create(
		long statusHistoryId) {
		return getPersistence().create(statusHistoryId);
	}

	/**
	* Removes the status history with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param statusHistoryId the primary key of the status history
	* @return the status history that was removed
	* @throws info.diit.portal.NoSuchStatusHistoryException if a status history with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StatusHistory remove(
		long statusHistoryId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchStatusHistoryException {
		return getPersistence().remove(statusHistoryId);
	}

	public static info.diit.portal.model.StatusHistory updateImpl(
		info.diit.portal.model.StatusHistory statusHistory, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(statusHistory, merge);
	}

	/**
	* Returns the status history with the primary key or throws a {@link info.diit.portal.NoSuchStatusHistoryException} if it could not be found.
	*
	* @param statusHistoryId the primary key of the status history
	* @return the status history
	* @throws info.diit.portal.NoSuchStatusHistoryException if a status history with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StatusHistory findByPrimaryKey(
		long statusHistoryId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchStatusHistoryException {
		return getPersistence().findByPrimaryKey(statusHistoryId);
	}

	/**
	* Returns the status history with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param statusHistoryId the primary key of the status history
	* @return the status history, or <code>null</code> if a status history with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StatusHistory fetchByPrimaryKey(
		long statusHistoryId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(statusHistoryId);
	}

	/**
	* Returns all the status histories.
	*
	* @return the status histories
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.StatusHistory> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the status histories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of status histories
	* @param end the upper bound of the range of status histories (not inclusive)
	* @return the range of status histories
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.StatusHistory> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the status histories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of status histories
	* @param end the upper bound of the range of status histories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of status histories
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.StatusHistory> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the status histories from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of status histories.
	*
	* @return the number of status histories
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static StatusHistoryPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (StatusHistoryPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					StatusHistoryPersistence.class.getName());

			ReferenceRegistry.registerReference(StatusHistoryUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(StatusHistoryPersistence persistence) {
	}

	private static StatusHistoryPersistence _persistence;
}
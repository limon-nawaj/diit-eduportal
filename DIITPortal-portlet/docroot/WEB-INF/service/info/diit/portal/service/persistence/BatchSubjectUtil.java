/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.BatchSubject;

import java.util.List;

/**
 * The persistence utility for the batch subject service. This utility wraps {@link BatchSubjectPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see BatchSubjectPersistence
 * @see BatchSubjectPersistenceImpl
 * @generated
 */
public class BatchSubjectUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(BatchSubject batchSubject) {
		getPersistence().clearCache(batchSubject);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<BatchSubject> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<BatchSubject> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<BatchSubject> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static BatchSubject update(BatchSubject batchSubject, boolean merge)
		throws SystemException {
		return getPersistence().update(batchSubject, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static BatchSubject update(BatchSubject batchSubject, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(batchSubject, merge, serviceContext);
	}

	/**
	* Caches the batch subject in the entity cache if it is enabled.
	*
	* @param batchSubject the batch subject
	*/
	public static void cacheResult(
		info.diit.portal.model.BatchSubject batchSubject) {
		getPersistence().cacheResult(batchSubject);
	}

	/**
	* Caches the batch subjects in the entity cache if it is enabled.
	*
	* @param batchSubjects the batch subjects
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.BatchSubject> batchSubjects) {
		getPersistence().cacheResult(batchSubjects);
	}

	/**
	* Creates a new batch subject with the primary key. Does not add the batch subject to the database.
	*
	* @param batchSubjectId the primary key for the new batch subject
	* @return the new batch subject
	*/
	public static info.diit.portal.model.BatchSubject create(
		long batchSubjectId) {
		return getPersistence().create(batchSubjectId);
	}

	/**
	* Removes the batch subject with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchSubjectId the primary key of the batch subject
	* @return the batch subject that was removed
	* @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject remove(
		long batchSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence().remove(batchSubjectId);
	}

	public static info.diit.portal.model.BatchSubject updateImpl(
		info.diit.portal.model.BatchSubject batchSubject, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(batchSubject, merge);
	}

	/**
	* Returns the batch subject with the primary key or throws a {@link info.diit.portal.NoSuchBatchSubjectException} if it could not be found.
	*
	* @param batchSubjectId the primary key of the batch subject
	* @return the batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject findByPrimaryKey(
		long batchSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence().findByPrimaryKey(batchSubjectId);
	}

	/**
	* Returns the batch subject with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param batchSubjectId the primary key of the batch subject
	* @return the batch subject, or <code>null</code> if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject fetchByPrimaryKey(
		long batchSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(batchSubjectId);
	}

	/**
	* Returns all the batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @return the matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findByBatchSubjectsByOrgCourseBatch(
		long organizationId, long courseId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatchSubjectsByOrgCourseBatch(organizationId,
			courseId, batchId);
	}

	/**
	* Returns a range of all the batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @return the range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findByBatchSubjectsByOrgCourseBatch(
		long organizationId, long courseId, long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatchSubjectsByOrgCourseBatch(organizationId,
			courseId, batchId, start, end);
	}

	/**
	* Returns an ordered range of all the batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findByBatchSubjectsByOrgCourseBatch(
		long organizationId, long courseId, long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatchSubjectsByOrgCourseBatch(organizationId,
			courseId, batchId, start, end, orderByComparator);
	}

	/**
	* Returns the first batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject findByBatchSubjectsByOrgCourseBatch_First(
		long organizationId, long courseId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findByBatchSubjectsByOrgCourseBatch_First(organizationId,
			courseId, batchId, orderByComparator);
	}

	/**
	* Returns the first batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject fetchByBatchSubjectsByOrgCourseBatch_First(
		long organizationId, long courseId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBatchSubjectsByOrgCourseBatch_First(organizationId,
			courseId, batchId, orderByComparator);
	}

	/**
	* Returns the last batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject findByBatchSubjectsByOrgCourseBatch_Last(
		long organizationId, long courseId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findByBatchSubjectsByOrgCourseBatch_Last(organizationId,
			courseId, batchId, orderByComparator);
	}

	/**
	* Returns the last batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject fetchByBatchSubjectsByOrgCourseBatch_Last(
		long organizationId, long courseId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBatchSubjectsByOrgCourseBatch_Last(organizationId,
			courseId, batchId, orderByComparator);
	}

	/**
	* Returns the batch subjects before and after the current batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* @param batchSubjectId the primary key of the current batch subject
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject[] findByBatchSubjectsByOrgCourseBatch_PrevAndNext(
		long batchSubjectId, long organizationId, long courseId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findByBatchSubjectsByOrgCourseBatch_PrevAndNext(batchSubjectId,
			organizationId, courseId, batchId, orderByComparator);
	}

	/**
	* Returns all the batch subjects where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @return the matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findByteacherId(
		long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByteacherId(teacherId);
	}

	/**
	* Returns a range of all the batch subjects where teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param teacherId the teacher ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @return the range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findByteacherId(
		long teacherId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByteacherId(teacherId, start, end);
	}

	/**
	* Returns an ordered range of all the batch subjects where teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param teacherId the teacher ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findByteacherId(
		long teacherId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByteacherId(teacherId, start, end, orderByComparator);
	}

	/**
	* Returns the first batch subject in the ordered set where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject findByteacherId_First(
		long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findByteacherId_First(teacherId, orderByComparator);
	}

	/**
	* Returns the first batch subject in the ordered set where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject fetchByteacherId_First(
		long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByteacherId_First(teacherId, orderByComparator);
	}

	/**
	* Returns the last batch subject in the ordered set where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject findByteacherId_Last(
		long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findByteacherId_Last(teacherId, orderByComparator);
	}

	/**
	* Returns the last batch subject in the ordered set where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject fetchByteacherId_Last(
		long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByteacherId_Last(teacherId, orderByComparator);
	}

	/**
	* Returns the batch subjects before and after the current batch subject in the ordered set where teacherId = &#63;.
	*
	* @param batchSubjectId the primary key of the current batch subject
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject[] findByteacherId_PrevAndNext(
		long batchSubjectId, long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findByteacherId_PrevAndNext(batchSubjectId, teacherId,
			orderByComparator);
	}

	/**
	* Returns all the batch subjects where batchId = &#63; and teacherId = &#63;.
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @return the matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findBybatch(
		long batchId, long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybatch(batchId, teacherId);
	}

	/**
	* Returns a range of all the batch subjects where batchId = &#63; and teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @return the range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findBybatch(
		long batchId, long teacherId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybatch(batchId, teacherId, start, end);
	}

	/**
	* Returns an ordered range of all the batch subjects where batchId = &#63; and teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findBybatch(
		long batchId, long teacherId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBybatch(batchId, teacherId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject findBybatch_First(
		long batchId, long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findBybatch_First(batchId, teacherId, orderByComparator);
	}

	/**
	* Returns the first batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject fetchBybatch_First(
		long batchId, long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBybatch_First(batchId, teacherId, orderByComparator);
	}

	/**
	* Returns the last batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject findBybatch_Last(
		long batchId, long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findBybatch_Last(batchId, teacherId, orderByComparator);
	}

	/**
	* Returns the last batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject fetchBybatch_Last(
		long batchId, long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBybatch_Last(batchId, teacherId, orderByComparator);
	}

	/**
	* Returns the batch subjects before and after the current batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	*
	* @param batchSubjectId the primary key of the current batch subject
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject[] findBybatch_PrevAndNext(
		long batchSubjectId, long batchId, long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findBybatch_PrevAndNext(batchSubjectId, batchId, teacherId,
			orderByComparator);
	}

	/**
	* Returns all the batch subjects where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findBybatchSubject(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybatchSubject(batchId);
	}

	/**
	* Returns a range of all the batch subjects where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @return the range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findBybatchSubject(
		long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybatchSubject(batchId, start, end);
	}

	/**
	* Returns an ordered range of all the batch subjects where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findBybatchSubject(
		long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBybatchSubject(batchId, start, end, orderByComparator);
	}

	/**
	* Returns the first batch subject in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject findBybatchSubject_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findBybatchSubject_First(batchId, orderByComparator);
	}

	/**
	* Returns the first batch subject in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject fetchBybatchSubject_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBybatchSubject_First(batchId, orderByComparator);
	}

	/**
	* Returns the last batch subject in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject findBybatchSubject_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findBybatchSubject_Last(batchId, orderByComparator);
	}

	/**
	* Returns the last batch subject in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject fetchBybatchSubject_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBybatchSubject_Last(batchId, orderByComparator);
	}

	/**
	* Returns the batch subjects before and after the current batch subject in the ordered set where batchId = &#63;.
	*
	* @param batchSubjectId the primary key of the current batch subject
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject[] findBybatchSubject_PrevAndNext(
		long batchSubjectId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findBybatchSubject_PrevAndNext(batchSubjectId, batchId,
			orderByComparator);
	}

	/**
	* Returns the batch subject where subjectId = &#63; or throws a {@link info.diit.portal.NoSuchBatchSubjectException} if it could not be found.
	*
	* @param subjectId the subject ID
	* @return the matching batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject findBysubject(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence().findBysubject(subjectId);
	}

	/**
	* Returns the batch subject where subjectId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param subjectId the subject ID
	* @return the matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject fetchBysubject(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBysubject(subjectId);
	}

	/**
	* Returns the batch subject where subjectId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param subjectId the subject ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject fetchBysubject(
		long subjectId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBysubject(subjectId, retrieveFromCache);
	}

	/**
	* Returns all the batch subjects where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId);
	}

	/**
	* Returns a range of all the batch subjects where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @return the range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the batch subjects where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByorganization(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first batch subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first batch subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the last batch subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last batch subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the batch subjects before and after the current batch subject in the ordered set where organizationId = &#63;.
	*
	* @param batchSubjectId the primary key of the current batch subject
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch subject
	* @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject[] findByorganization_PrevAndNext(
		long batchSubjectId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence()
				   .findByorganization_PrevAndNext(batchSubjectId,
			organizationId, orderByComparator);
	}

	/**
	* Returns all the batch subjects.
	*
	* @return the batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the batch subjects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @return the range of batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the batch subjects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch subjects
	* @param end the upper bound of the range of batch subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchSubject> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBatchSubjectsByOrgCourseBatch(
		long organizationId, long courseId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByBatchSubjectsByOrgCourseBatch(organizationId, courseId,
			batchId);
	}

	/**
	* Removes all the batch subjects where teacherId = &#63; from the database.
	*
	* @param teacherId the teacher ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByteacherId(long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByteacherId(teacherId);
	}

	/**
	* Removes all the batch subjects where batchId = &#63; and teacherId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBybatch(long batchId, long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBybatch(batchId, teacherId);
	}

	/**
	* Removes all the batch subjects where batchId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBybatchSubject(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBybatchSubject(batchId);
	}

	/**
	* Removes the batch subject where subjectId = &#63; from the database.
	*
	* @param subjectId the subject ID
	* @return the batch subject that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchSubject removeBysubject(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchSubjectException {
		return getPersistence().removeBysubject(subjectId);
	}

	/**
	* Removes all the batch subjects where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByorganization(organizationId);
	}

	/**
	* Removes all the batch subjects from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param courseId the course ID
	* @param batchId the batch ID
	* @return the number of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBatchSubjectsByOrgCourseBatch(
		long organizationId, long courseId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByBatchSubjectsByOrgCourseBatch(organizationId,
			courseId, batchId);
	}

	/**
	* Returns the number of batch subjects where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @return the number of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countByteacherId(long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByteacherId(teacherId);
	}

	/**
	* Returns the number of batch subjects where batchId = &#63; and teacherId = &#63;.
	*
	* @param batchId the batch ID
	* @param teacherId the teacher ID
	* @return the number of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countBybatch(long batchId, long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBybatch(batchId, teacherId);
	}

	/**
	* Returns the number of batch subjects where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the number of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countBybatchSubject(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBybatchSubject(batchId);
	}

	/**
	* Returns the number of batch subjects where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @return the number of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countBysubject(long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBysubject(subjectId);
	}

	/**
	* Returns the number of batch subjects where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByorganization(organizationId);
	}

	/**
	* Returns the number of batch subjects.
	*
	* @return the number of batch subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static BatchSubjectPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (BatchSubjectPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					BatchSubjectPersistence.class.getName());

			ReferenceRegistry.registerReference(BatchSubjectUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(BatchSubjectPersistence persistence) {
	}

	private static BatchSubjectPersistence _persistence;
}
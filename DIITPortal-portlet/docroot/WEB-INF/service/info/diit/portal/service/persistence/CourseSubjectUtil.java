/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.CourseSubject;

import java.util.List;

/**
 * The persistence utility for the course subject service. This utility wraps {@link CourseSubjectPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see CourseSubjectPersistence
 * @see CourseSubjectPersistenceImpl
 * @generated
 */
public class CourseSubjectUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CourseSubject courseSubject) {
		getPersistence().clearCache(courseSubject);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CourseSubject> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CourseSubject> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CourseSubject> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static CourseSubject update(CourseSubject courseSubject,
		boolean merge) throws SystemException {
		return getPersistence().update(courseSubject, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static CourseSubject update(CourseSubject courseSubject,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(courseSubject, merge, serviceContext);
	}

	/**
	* Caches the course subject in the entity cache if it is enabled.
	*
	* @param courseSubject the course subject
	*/
	public static void cacheResult(
		info.diit.portal.model.CourseSubject courseSubject) {
		getPersistence().cacheResult(courseSubject);
	}

	/**
	* Caches the course subjects in the entity cache if it is enabled.
	*
	* @param courseSubjects the course subjects
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.CourseSubject> courseSubjects) {
		getPersistence().cacheResult(courseSubjects);
	}

	/**
	* Creates a new course subject with the primary key. Does not add the course subject to the database.
	*
	* @param courseSubjectId the primary key for the new course subject
	* @return the new course subject
	*/
	public static info.diit.portal.model.CourseSubject create(
		long courseSubjectId) {
		return getPersistence().create(courseSubjectId);
	}

	/**
	* Removes the course subject with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjectId the primary key of the course subject
	* @return the course subject that was removed
	* @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject remove(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException {
		return getPersistence().remove(courseSubjectId);
	}

	public static info.diit.portal.model.CourseSubject updateImpl(
		info.diit.portal.model.CourseSubject courseSubject, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(courseSubject, merge);
	}

	/**
	* Returns the course subject with the primary key or throws a {@link info.diit.portal.NoSuchCourseSubjectException} if it could not be found.
	*
	* @param courseSubjectId the primary key of the course subject
	* @return the course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject findByPrimaryKey(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException {
		return getPersistence().findByPrimaryKey(courseSubjectId);
	}

	/**
	* Returns the course subject with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param courseSubjectId the primary key of the course subject
	* @return the course subject, or <code>null</code> if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject fetchByPrimaryKey(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(courseSubjectId);
	}

	/**
	* Returns all the course subjects where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseSubject> findBycourseId(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycourseId(courseId);
	}

	/**
	* Returns a range of all the course subjects where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @return the range of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseSubject> findBycourseId(
		long courseId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycourseId(courseId, start, end);
	}

	/**
	* Returns an ordered range of all the course subjects where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseSubject> findBycourseId(
		long courseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycourseId(courseId, start, end, orderByComparator);
	}

	/**
	* Returns the first course subject in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject findBycourseId_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException {
		return getPersistence().findBycourseId_First(courseId, orderByComparator);
	}

	/**
	* Returns the first course subject in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject fetchBycourseId_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBycourseId_First(courseId, orderByComparator);
	}

	/**
	* Returns the last course subject in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject findBycourseId_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException {
		return getPersistence().findBycourseId_Last(courseId, orderByComparator);
	}

	/**
	* Returns the last course subject in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject fetchBycourseId_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBycourseId_Last(courseId, orderByComparator);
	}

	/**
	* Returns the course subjects before and after the current course subject in the ordered set where courseId = &#63;.
	*
	* @param courseSubjectId the primary key of the current course subject
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject[] findBycourseId_PrevAndNext(
		long courseSubjectId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException {
		return getPersistence()
				   .findBycourseId_PrevAndNext(courseSubjectId, courseId,
			orderByComparator);
	}

	/**
	* Returns the course subject where subjectId = &#63; or throws a {@link info.diit.portal.NoSuchCourseSubjectException} if it could not be found.
	*
	* @param subjectId the subject ID
	* @return the matching course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject findBysubjectId(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException {
		return getPersistence().findBysubjectId(subjectId);
	}

	/**
	* Returns the course subject where subjectId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param subjectId the subject ID
	* @return the matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject fetchBysubjectId(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBysubjectId(subjectId);
	}

	/**
	* Returns the course subject where subjectId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param subjectId the subject ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject fetchBysubjectId(
		long subjectId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBysubjectId(subjectId, retrieveFromCache);
	}

	/**
	* Returns all the course subjects where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseSubject> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId);
	}

	/**
	* Returns a range of all the course subjects where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @return the range of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseSubject> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the course subjects where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseSubject> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByorganization(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first course subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException {
		return getPersistence()
				   .findByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first course subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the last course subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException {
		return getPersistence()
				   .findByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last course subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the course subjects before and after the current course subject in the ordered set where organizationId = &#63;.
	*
	* @param courseSubjectId the primary key of the current course subject
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject[] findByorganization_PrevAndNext(
		long courseSubjectId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException {
		return getPersistence()
				   .findByorganization_PrevAndNext(courseSubjectId,
			organizationId, orderByComparator);
	}

	/**
	* Returns all the course subjects where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseSubject> findBycompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycompany(companyId);
	}

	/**
	* Returns a range of all the course subjects where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @return the range of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseSubject> findBycompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the course subjects where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseSubject> findBycompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first course subject in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject findBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException {
		return getPersistence().findBycompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first course subject in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject fetchBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBycompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last course subject in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject findBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException {
		return getPersistence().findBycompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last course subject in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject fetchBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBycompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the course subjects before and after the current course subject in the ordered set where companyId = &#63;.
	*
	* @param courseSubjectId the primary key of the current course subject
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject[] findBycompany_PrevAndNext(
		long courseSubjectId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException {
		return getPersistence()
				   .findBycompany_PrevAndNext(courseSubjectId, companyId,
			orderByComparator);
	}

	/**
	* Returns all the course subjects.
	*
	* @return the course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseSubject> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the course subjects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @return the range of course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseSubject> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the course subjects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseSubject> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the course subjects where courseId = &#63; from the database.
	*
	* @param courseId the course ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBycourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBycourseId(courseId);
	}

	/**
	* Removes the course subject where subjectId = &#63; from the database.
	*
	* @param subjectId the subject ID
	* @return the course subject that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseSubject removeBysubjectId(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException {
		return getPersistence().removeBysubjectId(subjectId);
	}

	/**
	* Removes all the course subjects where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByorganization(organizationId);
	}

	/**
	* Removes all the course subjects where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBycompany(companyId);
	}

	/**
	* Removes all the course subjects from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of course subjects where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the number of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countBycourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBycourseId(courseId);
	}

	/**
	* Returns the number of course subjects where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @return the number of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countBysubjectId(long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBysubjectId(subjectId);
	}

	/**
	* Returns the number of course subjects where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByorganization(organizationId);
	}

	/**
	* Returns the number of course subjects where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBycompany(companyId);
	}

	/**
	* Returns the number of course subjects.
	*
	* @return the number of course subjects
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CourseSubjectPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CourseSubjectPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					CourseSubjectPersistence.class.getName());

			ReferenceRegistry.registerReference(CourseSubjectUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(CourseSubjectPersistence persistence) {
	}

	private static CourseSubjectPersistence _persistence;
}
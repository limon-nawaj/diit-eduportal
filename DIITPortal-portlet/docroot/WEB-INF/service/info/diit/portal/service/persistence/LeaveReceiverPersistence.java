/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.LeaveReceiver;

/**
 * The persistence interface for the leave receiver service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see LeaveReceiverPersistenceImpl
 * @see LeaveReceiverUtil
 * @generated
 */
public interface LeaveReceiverPersistence extends BasePersistence<LeaveReceiver> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LeaveReceiverUtil} to access the leave receiver persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the leave receiver in the entity cache if it is enabled.
	*
	* @param leaveReceiver the leave receiver
	*/
	public void cacheResult(info.diit.portal.model.LeaveReceiver leaveReceiver);

	/**
	* Caches the leave receivers in the entity cache if it is enabled.
	*
	* @param leaveReceivers the leave receivers
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.LeaveReceiver> leaveReceivers);

	/**
	* Creates a new leave receiver with the primary key. Does not add the leave receiver to the database.
	*
	* @param leaveReceiverId the primary key for the new leave receiver
	* @return the new leave receiver
	*/
	public info.diit.portal.model.LeaveReceiver create(long leaveReceiverId);

	/**
	* Removes the leave receiver with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param leaveReceiverId the primary key of the leave receiver
	* @return the leave receiver that was removed
	* @throws info.diit.portal.NoSuchLeaveReceiverException if a leave receiver with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveReceiver remove(long leaveReceiverId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveReceiverException;

	public info.diit.portal.model.LeaveReceiver updateImpl(
		info.diit.portal.model.LeaveReceiver leaveReceiver, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the leave receiver with the primary key or throws a {@link info.diit.portal.NoSuchLeaveReceiverException} if it could not be found.
	*
	* @param leaveReceiverId the primary key of the leave receiver
	* @return the leave receiver
	* @throws info.diit.portal.NoSuchLeaveReceiverException if a leave receiver with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveReceiver findByPrimaryKey(
		long leaveReceiverId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveReceiverException;

	/**
	* Returns the leave receiver with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param leaveReceiverId the primary key of the leave receiver
	* @return the leave receiver, or <code>null</code> if a leave receiver with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveReceiver fetchByPrimaryKey(
		long leaveReceiverId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the leave receivers where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveReceiver> findByOrganizationId(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the leave receivers where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of leave receivers
	* @param end the upper bound of the range of leave receivers (not inclusive)
	* @return the range of matching leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveReceiver> findByOrganizationId(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the leave receivers where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of leave receivers
	* @param end the upper bound of the range of leave receivers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveReceiver> findByOrganizationId(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first leave receiver in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave receiver
	* @throws info.diit.portal.NoSuchLeaveReceiverException if a matching leave receiver could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveReceiver findByOrganizationId_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveReceiverException;

	/**
	* Returns the first leave receiver in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave receiver, or <code>null</code> if a matching leave receiver could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveReceiver fetchByOrganizationId_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last leave receiver in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave receiver
	* @throws info.diit.portal.NoSuchLeaveReceiverException if a matching leave receiver could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveReceiver findByOrganizationId_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveReceiverException;

	/**
	* Returns the last leave receiver in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave receiver, or <code>null</code> if a matching leave receiver could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveReceiver fetchByOrganizationId_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the leave receivers before and after the current leave receiver in the ordered set where organizationId = &#63;.
	*
	* @param leaveReceiverId the primary key of the current leave receiver
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next leave receiver
	* @throws info.diit.portal.NoSuchLeaveReceiverException if a leave receiver with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveReceiver[] findByOrganizationId_PrevAndNext(
		long leaveReceiverId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveReceiverException;

	/**
	* Returns the leave receiver where employeeId = &#63; or throws a {@link info.diit.portal.NoSuchLeaveReceiverException} if it could not be found.
	*
	* @param employeeId the employee ID
	* @return the matching leave receiver
	* @throws info.diit.portal.NoSuchLeaveReceiverException if a matching leave receiver could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveReceiver findByEmployeeId(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveReceiverException;

	/**
	* Returns the leave receiver where employeeId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param employeeId the employee ID
	* @return the matching leave receiver, or <code>null</code> if a matching leave receiver could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveReceiver fetchByEmployeeId(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the leave receiver where employeeId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param employeeId the employee ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching leave receiver, or <code>null</code> if a matching leave receiver could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveReceiver fetchByEmployeeId(
		long employeeId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the leave receivers.
	*
	* @return the leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveReceiver> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the leave receivers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leave receivers
	* @param end the upper bound of the range of leave receivers (not inclusive)
	* @return the range of leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveReceiver> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the leave receivers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leave receivers
	* @param end the upper bound of the range of leave receivers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LeaveReceiver> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the leave receivers where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByOrganizationId(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the leave receiver where employeeId = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @return the leave receiver that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LeaveReceiver removeByEmployeeId(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveReceiverException;

	/**
	* Removes all the leave receivers from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of leave receivers where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public int countByOrganizationId(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of leave receivers where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the number of matching leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public int countByEmployeeId(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of leave receivers.
	*
	* @return the number of leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
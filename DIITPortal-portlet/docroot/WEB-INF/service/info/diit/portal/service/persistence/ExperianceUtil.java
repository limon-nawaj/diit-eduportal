/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.Experiance;

import java.util.List;

/**
 * The persistence utility for the experiance service. This utility wraps {@link ExperiancePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see ExperiancePersistence
 * @see ExperiancePersistenceImpl
 * @generated
 */
public class ExperianceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Experiance experiance) {
		getPersistence().clearCache(experiance);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Experiance> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Experiance> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Experiance> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Experiance update(Experiance experiance, boolean merge)
		throws SystemException {
		return getPersistence().update(experiance, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Experiance update(Experiance experiance, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(experiance, merge, serviceContext);
	}

	/**
	* Caches the experiance in the entity cache if it is enabled.
	*
	* @param experiance the experiance
	*/
	public static void cacheResult(info.diit.portal.model.Experiance experiance) {
		getPersistence().cacheResult(experiance);
	}

	/**
	* Caches the experiances in the entity cache if it is enabled.
	*
	* @param experiances the experiances
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.Experiance> experiances) {
		getPersistence().cacheResult(experiances);
	}

	/**
	* Creates a new experiance with the primary key. Does not add the experiance to the database.
	*
	* @param experianceId the primary key for the new experiance
	* @return the new experiance
	*/
	public static info.diit.portal.model.Experiance create(long experianceId) {
		return getPersistence().create(experianceId);
	}

	/**
	* Removes the experiance with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param experianceId the primary key of the experiance
	* @return the experiance that was removed
	* @throws info.diit.portal.NoSuchExperianceException if a experiance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Experiance remove(long experianceId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchExperianceException {
		return getPersistence().remove(experianceId);
	}

	public static info.diit.portal.model.Experiance updateImpl(
		info.diit.portal.model.Experiance experiance, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(experiance, merge);
	}

	/**
	* Returns the experiance with the primary key or throws a {@link info.diit.portal.NoSuchExperianceException} if it could not be found.
	*
	* @param experianceId the primary key of the experiance
	* @return the experiance
	* @throws info.diit.portal.NoSuchExperianceException if a experiance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Experiance findByPrimaryKey(
		long experianceId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchExperianceException {
		return getPersistence().findByPrimaryKey(experianceId);
	}

	/**
	* Returns the experiance with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param experianceId the primary key of the experiance
	* @return the experiance, or <code>null</code> if a experiance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Experiance fetchByPrimaryKey(
		long experianceId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(experianceId);
	}

	/**
	* Returns all the experiances where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the matching experiances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Experiance> findByStudentExperianceList(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStudentExperianceList(studentId);
	}

	/**
	* Returns a range of all the experiances where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of experiances
	* @param end the upper bound of the range of experiances (not inclusive)
	* @return the range of matching experiances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Experiance> findByStudentExperianceList(
		long studentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentExperianceList(studentId, start, end);
	}

	/**
	* Returns an ordered range of all the experiances where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of experiances
	* @param end the upper bound of the range of experiances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching experiances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Experiance> findByStudentExperianceList(
		long studentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentExperianceList(studentId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first experiance in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching experiance
	* @throws info.diit.portal.NoSuchExperianceException if a matching experiance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Experiance findByStudentExperianceList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchExperianceException {
		return getPersistence()
				   .findByStudentExperianceList_First(studentId,
			orderByComparator);
	}

	/**
	* Returns the first experiance in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching experiance, or <code>null</code> if a matching experiance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Experiance fetchByStudentExperianceList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentExperianceList_First(studentId,
			orderByComparator);
	}

	/**
	* Returns the last experiance in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching experiance
	* @throws info.diit.portal.NoSuchExperianceException if a matching experiance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Experiance findByStudentExperianceList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchExperianceException {
		return getPersistence()
				   .findByStudentExperianceList_Last(studentId,
			orderByComparator);
	}

	/**
	* Returns the last experiance in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching experiance, or <code>null</code> if a matching experiance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Experiance fetchByStudentExperianceList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentExperianceList_Last(studentId,
			orderByComparator);
	}

	/**
	* Returns the experiances before and after the current experiance in the ordered set where studentId = &#63;.
	*
	* @param experianceId the primary key of the current experiance
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next experiance
	* @throws info.diit.portal.NoSuchExperianceException if a experiance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Experiance[] findByStudentExperianceList_PrevAndNext(
		long experianceId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchExperianceException {
		return getPersistence()
				   .findByStudentExperianceList_PrevAndNext(experianceId,
			studentId, orderByComparator);
	}

	/**
	* Returns all the experiances.
	*
	* @return the experiances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Experiance> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the experiances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of experiances
	* @param end the upper bound of the range of experiances (not inclusive)
	* @return the range of experiances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Experiance> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the experiances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of experiances
	* @param end the upper bound of the range of experiances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of experiances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Experiance> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the experiances where studentId = &#63; from the database.
	*
	* @param studentId the student ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByStudentExperianceList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByStudentExperianceList(studentId);
	}

	/**
	* Removes all the experiances from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of experiances where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the number of matching experiances
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStudentExperianceList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStudentExperianceList(studentId);
	}

	/**
	* Returns the number of experiances.
	*
	* @return the number of experiances
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ExperiancePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ExperiancePersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					ExperiancePersistence.class.getName());

			ReferenceRegistry.registerReference(ExperianceUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(ExperiancePersistence persistence) {
	}

	private static ExperiancePersistence _persistence;
}
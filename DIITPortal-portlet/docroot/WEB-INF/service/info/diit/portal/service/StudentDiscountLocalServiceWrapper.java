/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link StudentDiscountLocalService}.
 * </p>
 *
 * @author    mohammad
 * @see       StudentDiscountLocalService
 * @generated
 */
public class StudentDiscountLocalServiceWrapper
	implements StudentDiscountLocalService,
		ServiceWrapper<StudentDiscountLocalService> {
	public StudentDiscountLocalServiceWrapper(
		StudentDiscountLocalService studentDiscountLocalService) {
		_studentDiscountLocalService = studentDiscountLocalService;
	}

	/**
	* Adds the student discount to the database. Also notifies the appropriate model listeners.
	*
	* @param studentDiscount the student discount
	* @return the student discount that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.StudentDiscount addStudentDiscount(
		info.diit.portal.model.StudentDiscount studentDiscount)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscountLocalService.addStudentDiscount(studentDiscount);
	}

	/**
	* Creates a new student discount with the primary key. Does not add the student discount to the database.
	*
	* @param studentDiscountId the primary key for the new student discount
	* @return the new student discount
	*/
	public info.diit.portal.model.StudentDiscount createStudentDiscount(
		long studentDiscountId) {
		return _studentDiscountLocalService.createStudentDiscount(studentDiscountId);
	}

	/**
	* Deletes the student discount with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param studentDiscountId the primary key of the student discount
	* @return the student discount that was removed
	* @throws PortalException if a student discount with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.StudentDiscount deleteStudentDiscount(
		long studentDiscountId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscountLocalService.deleteStudentDiscount(studentDiscountId);
	}

	/**
	* Deletes the student discount from the database. Also notifies the appropriate model listeners.
	*
	* @param studentDiscount the student discount
	* @return the student discount that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.StudentDiscount deleteStudentDiscount(
		info.diit.portal.model.StudentDiscount studentDiscount)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscountLocalService.deleteStudentDiscount(studentDiscount);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _studentDiscountLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscountLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscountLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscountLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscountLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.StudentDiscount fetchStudentDiscount(
		long studentDiscountId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscountLocalService.fetchStudentDiscount(studentDiscountId);
	}

	/**
	* Returns the student discount with the primary key.
	*
	* @param studentDiscountId the primary key of the student discount
	* @return the student discount
	* @throws PortalException if a student discount with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.StudentDiscount getStudentDiscount(
		long studentDiscountId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscountLocalService.getStudentDiscount(studentDiscountId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscountLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the student discounts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student discounts
	* @param end the upper bound of the range of student discounts (not inclusive)
	* @return the range of student discounts
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.StudentDiscount> getStudentDiscounts(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscountLocalService.getStudentDiscounts(start, end);
	}

	/**
	* Returns the number of student discounts.
	*
	* @return the number of student discounts
	* @throws SystemException if a system exception occurred
	*/
	public int getStudentDiscountsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscountLocalService.getStudentDiscountsCount();
	}

	/**
	* Updates the student discount in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param studentDiscount the student discount
	* @return the student discount that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.StudentDiscount updateStudentDiscount(
		info.diit.portal.model.StudentDiscount studentDiscount)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscountLocalService.updateStudentDiscount(studentDiscount);
	}

	/**
	* Updates the student discount in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param studentDiscount the student discount
	* @param merge whether to merge the student discount with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the student discount that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.StudentDiscount updateStudentDiscount(
		info.diit.portal.model.StudentDiscount studentDiscount, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentDiscountLocalService.updateStudentDiscount(studentDiscount,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _studentDiscountLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_studentDiscountLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _studentDiscountLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public StudentDiscountLocalService getWrappedStudentDiscountLocalService() {
		return _studentDiscountLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedStudentDiscountLocalService(
		StudentDiscountLocalService studentDiscountLocalService) {
		_studentDiscountLocalService = studentDiscountLocalService;
	}

	public StudentDiscountLocalService getWrappedService() {
		return _studentDiscountLocalService;
	}

	public void setWrappedService(
		StudentDiscountLocalService studentDiscountLocalService) {
		_studentDiscountLocalService = studentDiscountLocalService;
	}

	private StudentDiscountLocalService _studentDiscountLocalService;
}
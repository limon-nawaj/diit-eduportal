/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.PhoneNumber;

/**
 * The persistence interface for the phone number service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see PhoneNumberPersistenceImpl
 * @see PhoneNumberUtil
 * @generated
 */
public interface PhoneNumberPersistence extends BasePersistence<PhoneNumber> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link PhoneNumberUtil} to access the phone number persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the phone number in the entity cache if it is enabled.
	*
	* @param phoneNumber the phone number
	*/
	public void cacheResult(info.diit.portal.model.PhoneNumber phoneNumber);

	/**
	* Caches the phone numbers in the entity cache if it is enabled.
	*
	* @param phoneNumbers the phone numbers
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.PhoneNumber> phoneNumbers);

	/**
	* Creates a new phone number with the primary key. Does not add the phone number to the database.
	*
	* @param phoneNumberId the primary key for the new phone number
	* @return the new phone number
	*/
	public info.diit.portal.model.PhoneNumber create(long phoneNumberId);

	/**
	* Removes the phone number with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param phoneNumberId the primary key of the phone number
	* @return the phone number that was removed
	* @throws info.diit.portal.NoSuchPhoneNumberException if a phone number with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.PhoneNumber remove(long phoneNumberId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPhoneNumberException;

	public info.diit.portal.model.PhoneNumber updateImpl(
		info.diit.portal.model.PhoneNumber phoneNumber, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the phone number with the primary key or throws a {@link info.diit.portal.NoSuchPhoneNumberException} if it could not be found.
	*
	* @param phoneNumberId the primary key of the phone number
	* @return the phone number
	* @throws info.diit.portal.NoSuchPhoneNumberException if a phone number with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.PhoneNumber findByPrimaryKey(
		long phoneNumberId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPhoneNumberException;

	/**
	* Returns the phone number with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param phoneNumberId the primary key of the phone number
	* @return the phone number, or <code>null</code> if a phone number with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.PhoneNumber fetchByPrimaryKey(
		long phoneNumberId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the phone numbers where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the matching phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.PhoneNumber> findByStudentPhoneNumberList(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the phone numbers where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of phone numbers
	* @param end the upper bound of the range of phone numbers (not inclusive)
	* @return the range of matching phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.PhoneNumber> findByStudentPhoneNumberList(
		long studentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the phone numbers where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of phone numbers
	* @param end the upper bound of the range of phone numbers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.PhoneNumber> findByStudentPhoneNumberList(
		long studentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first phone number in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching phone number
	* @throws info.diit.portal.NoSuchPhoneNumberException if a matching phone number could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.PhoneNumber findByStudentPhoneNumberList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPhoneNumberException;

	/**
	* Returns the first phone number in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching phone number, or <code>null</code> if a matching phone number could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.PhoneNumber fetchByStudentPhoneNumberList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last phone number in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching phone number
	* @throws info.diit.portal.NoSuchPhoneNumberException if a matching phone number could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.PhoneNumber findByStudentPhoneNumberList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPhoneNumberException;

	/**
	* Returns the last phone number in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching phone number, or <code>null</code> if a matching phone number could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.PhoneNumber fetchByStudentPhoneNumberList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the phone numbers before and after the current phone number in the ordered set where studentId = &#63;.
	*
	* @param phoneNumberId the primary key of the current phone number
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next phone number
	* @throws info.diit.portal.NoSuchPhoneNumberException if a phone number with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.PhoneNumber[] findByStudentPhoneNumberList_PrevAndNext(
		long phoneNumberId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPhoneNumberException;

	/**
	* Returns all the phone numbers.
	*
	* @return the phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.PhoneNumber> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the phone numbers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of phone numbers
	* @param end the upper bound of the range of phone numbers (not inclusive)
	* @return the range of phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.PhoneNumber> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the phone numbers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of phone numbers
	* @param end the upper bound of the range of phone numbers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.PhoneNumber> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the phone numbers where studentId = &#63; from the database.
	*
	* @param studentId the student ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByStudentPhoneNumberList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the phone numbers from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of phone numbers where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the number of matching phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public int countByStudentPhoneNumberList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of phone numbers.
	*
	* @return the number of phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.Payment;

import java.util.List;

/**
 * The persistence utility for the payment service. This utility wraps {@link PaymentPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see PaymentPersistence
 * @see PaymentPersistenceImpl
 * @generated
 */
public class PaymentUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Payment payment) {
		getPersistence().clearCache(payment);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Payment> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Payment> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Payment> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Payment update(Payment payment, boolean merge)
		throws SystemException {
		return getPersistence().update(payment, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Payment update(Payment payment, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(payment, merge, serviceContext);
	}

	/**
	* Caches the payment in the entity cache if it is enabled.
	*
	* @param payment the payment
	*/
	public static void cacheResult(info.diit.portal.model.Payment payment) {
		getPersistence().cacheResult(payment);
	}

	/**
	* Caches the payments in the entity cache if it is enabled.
	*
	* @param payments the payments
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.Payment> payments) {
		getPersistence().cacheResult(payments);
	}

	/**
	* Creates a new payment with the primary key. Does not add the payment to the database.
	*
	* @param paymentId the primary key for the new payment
	* @return the new payment
	*/
	public static info.diit.portal.model.Payment create(long paymentId) {
		return getPersistence().create(paymentId);
	}

	/**
	* Removes the payment with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param paymentId the primary key of the payment
	* @return the payment that was removed
	* @throws info.diit.portal.NoSuchPaymentException if a payment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Payment remove(long paymentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPaymentException {
		return getPersistence().remove(paymentId);
	}

	public static info.diit.portal.model.Payment updateImpl(
		info.diit.portal.model.Payment payment, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(payment, merge);
	}

	/**
	* Returns the payment with the primary key or throws a {@link info.diit.portal.NoSuchPaymentException} if it could not be found.
	*
	* @param paymentId the primary key of the payment
	* @return the payment
	* @throws info.diit.portal.NoSuchPaymentException if a payment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Payment findByPrimaryKey(
		long paymentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPaymentException {
		return getPersistence().findByPrimaryKey(paymentId);
	}

	/**
	* Returns the payment with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param paymentId the primary key of the payment
	* @return the payment, or <code>null</code> if a payment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Payment fetchByPrimaryKey(
		long paymentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(paymentId);
	}

	/**
	* Returns all the payments where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @return the matching payments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Payment> findByStudentBatch(
		long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStudentBatch(studentId, batchId);
	}

	/**
	* Returns a range of all the payments where studentId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of payments
	* @param end the upper bound of the range of payments (not inclusive)
	* @return the range of matching payments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Payment> findByStudentBatch(
		long studentId, long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentBatch(studentId, batchId, start, end);
	}

	/**
	* Returns an ordered range of all the payments where studentId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of payments
	* @param end the upper bound of the range of payments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching payments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Payment> findByStudentBatch(
		long studentId, long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentBatch(studentId, batchId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first payment in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching payment
	* @throws info.diit.portal.NoSuchPaymentException if a matching payment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Payment findByStudentBatch_First(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPaymentException {
		return getPersistence()
				   .findByStudentBatch_First(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the first payment in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching payment, or <code>null</code> if a matching payment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Payment fetchByStudentBatch_First(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentBatch_First(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the last payment in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching payment
	* @throws info.diit.portal.NoSuchPaymentException if a matching payment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Payment findByStudentBatch_Last(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPaymentException {
		return getPersistence()
				   .findByStudentBatch_Last(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the last payment in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching payment, or <code>null</code> if a matching payment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Payment fetchByStudentBatch_Last(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentBatch_Last(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the payments before and after the current payment in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param paymentId the primary key of the current payment
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next payment
	* @throws info.diit.portal.NoSuchPaymentException if a payment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Payment[] findByStudentBatch_PrevAndNext(
		long paymentId, long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPaymentException {
		return getPersistence()
				   .findByStudentBatch_PrevAndNext(paymentId, studentId,
			batchId, orderByComparator);
	}

	/**
	* Returns all the payments.
	*
	* @return the payments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Payment> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the payments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of payments
	* @param end the upper bound of the range of payments (not inclusive)
	* @return the range of payments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Payment> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the payments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of payments
	* @param end the upper bound of the range of payments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of payments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Payment> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the payments where studentId = &#63; and batchId = &#63; from the database.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByStudentBatch(long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByStudentBatch(studentId, batchId);
	}

	/**
	* Removes all the payments from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of payments where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @return the number of matching payments
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStudentBatch(long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStudentBatch(studentId, batchId);
	}

	/**
	* Returns the number of payments.
	*
	* @return the number of payments
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static PaymentPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (PaymentPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					PaymentPersistence.class.getName());

			ReferenceRegistry.registerReference(PaymentUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(PaymentPersistence persistence) {
	}

	private static PaymentPersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.Attendance;

/**
 * The persistence interface for the attendance service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see AttendancePersistenceImpl
 * @see AttendanceUtil
 * @generated
 */
public interface AttendancePersistence extends BasePersistence<Attendance> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AttendanceUtil} to access the attendance persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the attendance in the entity cache if it is enabled.
	*
	* @param attendance the attendance
	*/
	public void cacheResult(info.diit.portal.model.Attendance attendance);

	/**
	* Caches the attendances in the entity cache if it is enabled.
	*
	* @param attendances the attendances
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.Attendance> attendances);

	/**
	* Creates a new attendance with the primary key. Does not add the attendance to the database.
	*
	* @param attendanceId the primary key for the new attendance
	* @return the new attendance
	*/
	public info.diit.portal.model.Attendance create(long attendanceId);

	/**
	* Removes the attendance with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceId the primary key of the attendance
	* @return the attendance that was removed
	* @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance remove(long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	public info.diit.portal.model.Attendance updateImpl(
		info.diit.portal.model.Attendance attendance, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the attendance with the primary key or throws a {@link info.diit.portal.NoSuchAttendanceException} if it could not be found.
	*
	* @param attendanceId the primary key of the attendance
	* @return the attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance findByPrimaryKey(long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Returns the attendance with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param attendanceId the primary key of the attendance
	* @return the attendance, or <code>null</code> if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance fetchByPrimaryKey(
		long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the attendances where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the attendances where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @return the range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the attendances where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first attendance in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Returns the first attendance in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last attendance in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Returns the last attendance in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the attendances before and after the current attendance in the ordered set where companyId = &#63;.
	*
	* @param attendanceId the primary key of the current attendance
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance[] findByCompany_PrevAndNext(
		long attendanceId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Returns all the attendances where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @return the matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findByUserBatchSubject(
		long userId, long batch, long subject)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the attendances where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @return the range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findByUserBatchSubject(
		long userId, long batch, long subject, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the attendances where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findByUserBatchSubject(
		long userId, long batch, long subject, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance findByUserBatchSubject_First(
		long userId, long batch, long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Returns the first attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance fetchByUserBatchSubject_First(
		long userId, long batch, long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance findByUserBatchSubject_Last(
		long userId, long batch, long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Returns the last attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance fetchByUserBatchSubject_Last(
		long userId, long batch, long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the attendances before and after the current attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* @param attendanceId the primary key of the current attendance
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance[] findByUserBatchSubject_PrevAndNext(
		long attendanceId, long userId, long batch, long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Returns all the attendances where batch = &#63;.
	*
	* @param batch the batch
	* @return the matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findByBatch(
		long batch) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the attendances where batch = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batch the batch
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @return the range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findByBatch(
		long batch, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the attendances where batch = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batch the batch
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findByBatch(
		long batch, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first attendance in the ordered set where batch = &#63;.
	*
	* @param batch the batch
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance findByBatch_First(long batch,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Returns the first attendance in the ordered set where batch = &#63;.
	*
	* @param batch the batch
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance fetchByBatch_First(long batch,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last attendance in the ordered set where batch = &#63;.
	*
	* @param batch the batch
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance findByBatch_Last(long batch,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Returns the last attendance in the ordered set where batch = &#63;.
	*
	* @param batch the batch
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance fetchByBatch_Last(long batch,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the attendances before and after the current attendance in the ordered set where batch = &#63;.
	*
	* @param attendanceId the primary key of the current attendance
	* @param batch the batch
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance[] findByBatch_PrevAndNext(
		long attendanceId, long batch,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Returns the attendance where subject = &#63; and attendanceDate = &#63; or throws a {@link info.diit.portal.NoSuchAttendanceException} if it could not be found.
	*
	* @param subject the subject
	* @param attendanceDate the attendance date
	* @return the matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance findBySubjectDate(long subject,
		java.util.Date attendanceDate)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Returns the attendance where subject = &#63; and attendanceDate = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param subject the subject
	* @param attendanceDate the attendance date
	* @return the matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance fetchBySubjectDate(long subject,
		java.util.Date attendanceDate)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the attendance where subject = &#63; and attendanceDate = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param subject the subject
	* @param attendanceDate the attendance date
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance fetchBySubjectDate(long subject,
		java.util.Date attendanceDate, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the attendances where batch = &#63; and attendanceDate = &#63;.
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @return the matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findByBatchDate(
		long batch, java.util.Date attendanceDate)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the attendances where batch = &#63; and attendanceDate = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @return the range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findByBatchDate(
		long batch, java.util.Date attendanceDate, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the attendances where batch = &#63; and attendanceDate = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findByBatchDate(
		long batch, java.util.Date attendanceDate, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance findByBatchDate_First(long batch,
		java.util.Date attendanceDate,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Returns the first attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance fetchByBatchDate_First(
		long batch, java.util.Date attendanceDate,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance findByBatchDate_Last(long batch,
		java.util.Date attendanceDate,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Returns the last attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance fetchByBatchDate_Last(long batch,
		java.util.Date attendanceDate,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the attendances before and after the current attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	*
	* @param attendanceId the primary key of the current attendance
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance[] findByBatchDate_PrevAndNext(
		long attendanceId, long batch, java.util.Date attendanceDate,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Returns all the attendances.
	*
	* @return the attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the attendances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @return the range of attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the attendances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Attendance> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the attendances where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the attendances where userId = &#63; and batch = &#63; and subject = &#63; from the database.
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUserBatchSubject(long userId, long batch, long subject)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the attendances where batch = &#63; from the database.
	*
	* @param batch the batch
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBatch(long batch)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the attendance where subject = &#63; and attendanceDate = &#63; from the database.
	*
	* @param subject the subject
	* @param attendanceDate the attendance date
	* @return the attendance that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Attendance removeBySubjectDate(long subject,
		java.util.Date attendanceDate)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException;

	/**
	* Removes all the attendances where batch = &#63; and attendanceDate = &#63; from the database.
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBatchDate(long batch, java.util.Date attendanceDate)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the attendances from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of attendances where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of attendances where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @return the number of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserBatchSubject(long userId, long batch, long subject)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of attendances where batch = &#63;.
	*
	* @param batch the batch
	* @return the number of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public int countByBatch(long batch)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of attendances where subject = &#63; and attendanceDate = &#63;.
	*
	* @param subject the subject
	* @param attendanceDate the attendance date
	* @return the number of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public int countBySubjectDate(long subject, java.util.Date attendanceDate)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of attendances where batch = &#63; and attendanceDate = &#63;.
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @return the number of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public int countByBatchDate(long batch, java.util.Date attendanceDate)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of attendances.
	*
	* @return the number of attendances
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
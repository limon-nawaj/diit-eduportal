/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.EmployeeEmail;

/**
 * The persistence interface for the employee email service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see EmployeeEmailPersistenceImpl
 * @see EmployeeEmailUtil
 * @generated
 */
public interface EmployeeEmailPersistence extends BasePersistence<EmployeeEmail> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EmployeeEmailUtil} to access the employee email persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the employee email in the entity cache if it is enabled.
	*
	* @param employeeEmail the employee email
	*/
	public void cacheResult(info.diit.portal.model.EmployeeEmail employeeEmail);

	/**
	* Caches the employee emails in the entity cache if it is enabled.
	*
	* @param employeeEmails the employee emails
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.EmployeeEmail> employeeEmails);

	/**
	* Creates a new employee email with the primary key. Does not add the employee email to the database.
	*
	* @param emailId the primary key for the new employee email
	* @return the new employee email
	*/
	public info.diit.portal.model.EmployeeEmail create(long emailId);

	/**
	* Removes the employee email with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param emailId the primary key of the employee email
	* @return the employee email that was removed
	* @throws info.diit.portal.NoSuchEmployeeEmailException if a employee email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeEmail remove(long emailId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeEmailException;

	public info.diit.portal.model.EmployeeEmail updateImpl(
		info.diit.portal.model.EmployeeEmail employeeEmail, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the employee email with the primary key or throws a {@link info.diit.portal.NoSuchEmployeeEmailException} if it could not be found.
	*
	* @param emailId the primary key of the employee email
	* @return the employee email
	* @throws info.diit.portal.NoSuchEmployeeEmailException if a employee email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeEmail findByPrimaryKey(long emailId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeEmailException;

	/**
	* Returns the employee email with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param emailId the primary key of the employee email
	* @return the employee email, or <code>null</code> if a employee email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeEmail fetchByPrimaryKey(long emailId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the employee emails where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the matching employee emails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeEmail> findByEmployee(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the employee emails where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of employee emails
	* @param end the upper bound of the range of employee emails (not inclusive)
	* @return the range of matching employee emails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeEmail> findByEmployee(
		long employeeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the employee emails where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of employee emails
	* @param end the upper bound of the range of employee emails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching employee emails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeEmail> findByEmployee(
		long employeeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first employee email in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee email
	* @throws info.diit.portal.NoSuchEmployeeEmailException if a matching employee email could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeEmail findByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeEmailException;

	/**
	* Returns the first employee email in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee email, or <code>null</code> if a matching employee email could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeEmail fetchByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last employee email in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee email
	* @throws info.diit.portal.NoSuchEmployeeEmailException if a matching employee email could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeEmail findByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeEmailException;

	/**
	* Returns the last employee email in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee email, or <code>null</code> if a matching employee email could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeEmail fetchByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the employee emails before and after the current employee email in the ordered set where employeeId = &#63;.
	*
	* @param emailId the primary key of the current employee email
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next employee email
	* @throws info.diit.portal.NoSuchEmployeeEmailException if a employee email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeEmail[] findByEmployee_PrevAndNext(
		long emailId, long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeEmailException;

	/**
	* Returns all the employee emails.
	*
	* @return the employee emails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeEmail> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the employee emails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee emails
	* @param end the upper bound of the range of employee emails (not inclusive)
	* @return the range of employee emails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeEmail> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the employee emails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee emails
	* @param end the upper bound of the range of employee emails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of employee emails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeEmail> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the employee emails where employeeId = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the employee emails from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of employee emails where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the number of matching employee emails
	* @throws SystemException if a system exception occurred
	*/
	public int countByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of employee emails.
	*
	* @return the number of employee emails
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
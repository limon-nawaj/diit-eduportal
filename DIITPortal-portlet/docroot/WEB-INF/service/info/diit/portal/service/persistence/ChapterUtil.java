/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.Chapter;

import java.util.List;

/**
 * The persistence utility for the chapter service. This utility wraps {@link ChapterPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see ChapterPersistence
 * @see ChapterPersistenceImpl
 * @generated
 */
public class ChapterUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Chapter chapter) {
		getPersistence().clearCache(chapter);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Chapter> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Chapter> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Chapter> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Chapter update(Chapter chapter, boolean merge)
		throws SystemException {
		return getPersistence().update(chapter, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Chapter update(Chapter chapter, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(chapter, merge, serviceContext);
	}

	/**
	* Caches the chapter in the entity cache if it is enabled.
	*
	* @param chapter the chapter
	*/
	public static void cacheResult(info.diit.portal.model.Chapter chapter) {
		getPersistence().cacheResult(chapter);
	}

	/**
	* Caches the chapters in the entity cache if it is enabled.
	*
	* @param chapters the chapters
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.Chapter> chapters) {
		getPersistence().cacheResult(chapters);
	}

	/**
	* Creates a new chapter with the primary key. Does not add the chapter to the database.
	*
	* @param chapterId the primary key for the new chapter
	* @return the new chapter
	*/
	public static info.diit.portal.model.Chapter create(long chapterId) {
		return getPersistence().create(chapterId);
	}

	/**
	* Removes the chapter with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param chapterId the primary key of the chapter
	* @return the chapter that was removed
	* @throws info.diit.portal.NoSuchChapterException if a chapter with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter remove(long chapterId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchChapterException {
		return getPersistence().remove(chapterId);
	}

	public static info.diit.portal.model.Chapter updateImpl(
		info.diit.portal.model.Chapter chapter, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(chapter, merge);
	}

	/**
	* Returns the chapter with the primary key or throws a {@link info.diit.portal.NoSuchChapterException} if it could not be found.
	*
	* @param chapterId the primary key of the chapter
	* @return the chapter
	* @throws info.diit.portal.NoSuchChapterException if a chapter with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter findByPrimaryKey(
		long chapterId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchChapterException {
		return getPersistence().findByPrimaryKey(chapterId);
	}

	/**
	* Returns the chapter with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param chapterId the primary key of the chapter
	* @return the chapter, or <code>null</code> if a chapter with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter fetchByPrimaryKey(
		long chapterId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(chapterId);
	}

	/**
	* Returns all the chapters where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Chapter> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId);
	}

	/**
	* Returns a range of all the chapters where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of chapters
	* @param end the upper bound of the range of chapters (not inclusive)
	* @return the range of matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Chapter> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the chapters where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of chapters
	* @param end the upper bound of the range of chapters (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Chapter> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first chapter in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching chapter
	* @throws info.diit.portal.NoSuchChapterException if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchChapterException {
		return getPersistence().findByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first chapter in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching chapter, or <code>null</code> if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last chapter in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching chapter
	* @throws info.diit.portal.NoSuchChapterException if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchChapterException {
		return getPersistence().findByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last chapter in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching chapter, or <code>null</code> if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the chapters before and after the current chapter in the ordered set where companyId = &#63;.
	*
	* @param chapterId the primary key of the current chapter
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next chapter
	* @throws info.diit.portal.NoSuchChapterException if a chapter with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter[] findByCompany_PrevAndNext(
		long chapterId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchChapterException {
		return getPersistence()
				   .findByCompany_PrevAndNext(chapterId, companyId,
			orderByComparator);
	}

	/**
	* Returns all the chapters where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @return the matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Chapter> findBySubject(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBySubject(subjectId);
	}

	/**
	* Returns a range of all the chapters where subjectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject ID
	* @param start the lower bound of the range of chapters
	* @param end the upper bound of the range of chapters (not inclusive)
	* @return the range of matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Chapter> findBySubject(
		long subjectId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBySubject(subjectId, start, end);
	}

	/**
	* Returns an ordered range of all the chapters where subjectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject ID
	* @param start the lower bound of the range of chapters
	* @param end the upper bound of the range of chapters (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Chapter> findBySubject(
		long subjectId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBySubject(subjectId, start, end, orderByComparator);
	}

	/**
	* Returns the first chapter in the ordered set where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching chapter
	* @throws info.diit.portal.NoSuchChapterException if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter findBySubject_First(
		long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchChapterException {
		return getPersistence().findBySubject_First(subjectId, orderByComparator);
	}

	/**
	* Returns the first chapter in the ordered set where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching chapter, or <code>null</code> if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter fetchBySubject_First(
		long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBySubject_First(subjectId, orderByComparator);
	}

	/**
	* Returns the last chapter in the ordered set where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching chapter
	* @throws info.diit.portal.NoSuchChapterException if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter findBySubject_Last(
		long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchChapterException {
		return getPersistence().findBySubject_Last(subjectId, orderByComparator);
	}

	/**
	* Returns the last chapter in the ordered set where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching chapter, or <code>null</code> if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter fetchBySubject_Last(
		long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBySubject_Last(subjectId, orderByComparator);
	}

	/**
	* Returns the chapters before and after the current chapter in the ordered set where subjectId = &#63;.
	*
	* @param chapterId the primary key of the current chapter
	* @param subjectId the subject ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next chapter
	* @throws info.diit.portal.NoSuchChapterException if a chapter with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter[] findBySubject_PrevAndNext(
		long chapterId, long subjectId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchChapterException {
		return getPersistence()
				   .findBySubject_PrevAndNext(chapterId, subjectId,
			orderByComparator);
	}

	/**
	* Returns the chapter where chapterId = &#63; and sequence = &#63; or throws a {@link info.diit.portal.NoSuchChapterException} if it could not be found.
	*
	* @param chapterId the chapter ID
	* @param sequence the sequence
	* @return the matching chapter
	* @throws info.diit.portal.NoSuchChapterException if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter findByChapterSequence(
		long chapterId, long sequence)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchChapterException {
		return getPersistence().findByChapterSequence(chapterId, sequence);
	}

	/**
	* Returns the chapter where chapterId = &#63; and sequence = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param chapterId the chapter ID
	* @param sequence the sequence
	* @return the matching chapter, or <code>null</code> if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter fetchByChapterSequence(
		long chapterId, long sequence)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByChapterSequence(chapterId, sequence);
	}

	/**
	* Returns the chapter where chapterId = &#63; and sequence = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param chapterId the chapter ID
	* @param sequence the sequence
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching chapter, or <code>null</code> if a matching chapter could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter fetchByChapterSequence(
		long chapterId, long sequence, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByChapterSequence(chapterId, sequence,
			retrieveFromCache);
	}

	/**
	* Returns all the chapters.
	*
	* @return the chapters
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Chapter> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the chapters.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of chapters
	* @param end the upper bound of the range of chapters (not inclusive)
	* @return the range of chapters
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Chapter> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the chapters.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of chapters
	* @param end the upper bound of the range of chapters (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of chapters
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Chapter> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the chapters where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCompany(companyId);
	}

	/**
	* Removes all the chapters where subjectId = &#63; from the database.
	*
	* @param subjectId the subject ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBySubject(long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBySubject(subjectId);
	}

	/**
	* Removes the chapter where chapterId = &#63; and sequence = &#63; from the database.
	*
	* @param chapterId the chapter ID
	* @param sequence the sequence
	* @return the chapter that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Chapter removeByChapterSequence(
		long chapterId, long sequence)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchChapterException {
		return getPersistence().removeByChapterSequence(chapterId, sequence);
	}

	/**
	* Removes all the chapters from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of chapters where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCompany(companyId);
	}

	/**
	* Returns the number of chapters where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @return the number of matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public static int countBySubject(long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBySubject(subjectId);
	}

	/**
	* Returns the number of chapters where chapterId = &#63; and sequence = &#63;.
	*
	* @param chapterId the chapter ID
	* @param sequence the sequence
	* @return the number of matching chapters
	* @throws SystemException if a system exception occurred
	*/
	public static int countByChapterSequence(long chapterId, long sequence)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByChapterSequence(chapterId, sequence);
	}

	/**
	* Returns the number of chapters.
	*
	* @return the number of chapters
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ChapterPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ChapterPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					ChapterPersistence.class.getName());

			ReferenceRegistry.registerReference(ChapterUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(ChapterPersistence persistence) {
	}

	private static ChapterPersistence _persistence;
}
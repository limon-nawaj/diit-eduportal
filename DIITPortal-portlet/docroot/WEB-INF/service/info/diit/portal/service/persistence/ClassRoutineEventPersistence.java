/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.ClassRoutineEvent;

/**
 * The persistence interface for the class routine event service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see ClassRoutineEventPersistenceImpl
 * @see ClassRoutineEventUtil
 * @generated
 */
public interface ClassRoutineEventPersistence extends BasePersistence<ClassRoutineEvent> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ClassRoutineEventUtil} to access the class routine event persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the class routine event in the entity cache if it is enabled.
	*
	* @param classRoutineEvent the class routine event
	*/
	public void cacheResult(
		info.diit.portal.model.ClassRoutineEvent classRoutineEvent);

	/**
	* Caches the class routine events in the entity cache if it is enabled.
	*
	* @param classRoutineEvents the class routine events
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.ClassRoutineEvent> classRoutineEvents);

	/**
	* Creates a new class routine event with the primary key. Does not add the class routine event to the database.
	*
	* @param classRoutineEventId the primary key for the new class routine event
	* @return the new class routine event
	*/
	public info.diit.portal.model.ClassRoutineEvent create(
		long classRoutineEventId);

	/**
	* Removes the class routine event with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param classRoutineEventId the primary key of the class routine event
	* @return the class routine event that was removed
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent remove(
		long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException;

	public info.diit.portal.model.ClassRoutineEvent updateImpl(
		info.diit.portal.model.ClassRoutineEvent classRoutineEvent,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the class routine event with the primary key or throws a {@link info.diit.portal.NoSuchClassRoutineEventException} if it could not be found.
	*
	* @param classRoutineEventId the primary key of the class routine event
	* @return the class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent findByPrimaryKey(
		long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException;

	/**
	* Returns the class routine event with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param classRoutineEventId the primary key of the class routine event
	* @return the class routine event, or <code>null</code> if a class routine event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent fetchByPrimaryKey(
		long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the class routine event where subjectId = &#63; and classRoutineEventId = &#63; or throws a {@link info.diit.portal.NoSuchClassRoutineEventException} if it could not be found.
	*
	* @param subjectId the subject ID
	* @param classRoutineEventId the class routine event ID
	* @return the matching class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent findBySubjectId(
		long subjectId, long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException;

	/**
	* Returns the class routine event where subjectId = &#63; and classRoutineEventId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param subjectId the subject ID
	* @param classRoutineEventId the class routine event ID
	* @return the matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent fetchBySubjectId(
		long subjectId, long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the class routine event where subjectId = &#63; and classRoutineEventId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param subjectId the subject ID
	* @param classRoutineEventId the class routine event ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent fetchBySubjectId(
		long subjectId, long classRoutineEventId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the class routine events where roomId = &#63;.
	*
	* @param roomId the room ID
	* @return the matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEvent> findByRoom(
		long roomId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the class routine events where roomId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param roomId the room ID
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @return the range of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEvent> findByRoom(
		long roomId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the class routine events where roomId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param roomId the room ID
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEvent> findByRoom(
		long roomId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first class routine event in the ordered set where roomId = &#63;.
	*
	* @param roomId the room ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent findByRoom_First(
		long roomId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException;

	/**
	* Returns the first class routine event in the ordered set where roomId = &#63;.
	*
	* @param roomId the room ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent fetchByRoom_First(
		long roomId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last class routine event in the ordered set where roomId = &#63;.
	*
	* @param roomId the room ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent findByRoom_Last(
		long roomId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException;

	/**
	* Returns the last class routine event in the ordered set where roomId = &#63;.
	*
	* @param roomId the room ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent fetchByRoom_Last(
		long roomId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the class routine events before and after the current class routine event in the ordered set where roomId = &#63;.
	*
	* @param classRoutineEventId the primary key of the current class routine event
	* @param roomId the room ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent[] findByRoom_PrevAndNext(
		long classRoutineEventId, long roomId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException;

	/**
	* Returns all the class routine events where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEvent> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the class routine events where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @return the range of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEvent> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the class routine events where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEvent> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first class routine event in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException;

	/**
	* Returns the first class routine event in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last class routine event in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException;

	/**
	* Returns the last class routine event in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the class routine events before and after the current class routine event in the ordered set where companyId = &#63;.
	*
	* @param classRoutineEventId the primary key of the current class routine event
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent[] findByCompany_PrevAndNext(
		long classRoutineEventId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException;

	/**
	* Returns all the class routine events where subjectId = &#63; and day = &#63;.
	*
	* @param subjectId the subject ID
	* @param day the day
	* @return the matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEvent> findBySubjectDate(
		long subjectId, int day)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the class routine events where subjectId = &#63; and day = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject ID
	* @param day the day
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @return the range of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEvent> findBySubjectDate(
		long subjectId, int day, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the class routine events where subjectId = &#63; and day = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject ID
	* @param day the day
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEvent> findBySubjectDate(
		long subjectId, int day, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	*
	* @param subjectId the subject ID
	* @param day the day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent findBySubjectDate_First(
		long subjectId, int day,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException;

	/**
	* Returns the first class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	*
	* @param subjectId the subject ID
	* @param day the day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent fetchBySubjectDate_First(
		long subjectId, int day,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	*
	* @param subjectId the subject ID
	* @param day the day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent findBySubjectDate_Last(
		long subjectId, int day,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException;

	/**
	* Returns the last class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	*
	* @param subjectId the subject ID
	* @param day the day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent fetchBySubjectDate_Last(
		long subjectId, int day,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the class routine events before and after the current class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	*
	* @param classRoutineEventId the primary key of the current class routine event
	* @param subjectId the subject ID
	* @param day the day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent[] findBySubjectDate_PrevAndNext(
		long classRoutineEventId, long subjectId, int day,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException;

	/**
	* Returns all the class routine events.
	*
	* @return the class routine events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEvent> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the class routine events.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @return the range of class routine events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEvent> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the class routine events.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of class routine events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEvent> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the class routine event where subjectId = &#63; and classRoutineEventId = &#63; from the database.
	*
	* @param subjectId the subject ID
	* @param classRoutineEventId the class routine event ID
	* @return the class routine event that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEvent removeBySubjectId(
		long subjectId, long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException;

	/**
	* Removes all the class routine events where roomId = &#63; from the database.
	*
	* @param roomId the room ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByRoom(long roomId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the class routine events where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the class routine events where subjectId = &#63; and day = &#63; from the database.
	*
	* @param subjectId the subject ID
	* @param day the day
	* @throws SystemException if a system exception occurred
	*/
	public void removeBySubjectDate(long subjectId, int day)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the class routine events from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of class routine events where subjectId = &#63; and classRoutineEventId = &#63;.
	*
	* @param subjectId the subject ID
	* @param classRoutineEventId the class routine event ID
	* @return the number of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public int countBySubjectId(long subjectId, long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of class routine events where roomId = &#63;.
	*
	* @param roomId the room ID
	* @return the number of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public int countByRoom(long roomId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of class routine events where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of class routine events where subjectId = &#63; and day = &#63;.
	*
	* @param subjectId the subject ID
	* @param day the day
	* @return the number of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public int countBySubjectDate(long subjectId, int day)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of class routine events.
	*
	* @return the number of class routine events
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the communication record local service. This utility wraps {@link info.diit.portal.service.impl.CommunicationRecordLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see CommunicationRecordLocalService
 * @see info.diit.portal.service.base.CommunicationRecordLocalServiceBaseImpl
 * @see info.diit.portal.service.impl.CommunicationRecordLocalServiceImpl
 * @generated
 */
public class CommunicationRecordLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.service.impl.CommunicationRecordLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the communication record to the database. Also notifies the appropriate model listeners.
	*
	* @param communicationRecord the communication record
	* @return the communication record that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CommunicationRecord addCommunicationRecord(
		info.diit.portal.model.CommunicationRecord communicationRecord)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addCommunicationRecord(communicationRecord);
	}

	/**
	* Creates a new communication record with the primary key. Does not add the communication record to the database.
	*
	* @param communicationRecordId the primary key for the new communication record
	* @return the new communication record
	*/
	public static info.diit.portal.model.CommunicationRecord createCommunicationRecord(
		long communicationRecordId) {
		return getService().createCommunicationRecord(communicationRecordId);
	}

	/**
	* Deletes the communication record with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param communicationRecordId the primary key of the communication record
	* @return the communication record that was removed
	* @throws PortalException if a communication record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CommunicationRecord deleteCommunicationRecord(
		long communicationRecordId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCommunicationRecord(communicationRecordId);
	}

	/**
	* Deletes the communication record from the database. Also notifies the appropriate model listeners.
	*
	* @param communicationRecord the communication record
	* @return the communication record that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CommunicationRecord deleteCommunicationRecord(
		info.diit.portal.model.CommunicationRecord communicationRecord)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCommunicationRecord(communicationRecord);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.model.CommunicationRecord fetchCommunicationRecord(
		long communicationRecordId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchCommunicationRecord(communicationRecordId);
	}

	/**
	* Returns the communication record with the primary key.
	*
	* @param communicationRecordId the primary key of the communication record
	* @return the communication record
	* @throws PortalException if a communication record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CommunicationRecord getCommunicationRecord(
		long communicationRecordId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getCommunicationRecord(communicationRecordId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the communication records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of communication records
	* @param end the upper bound of the range of communication records (not inclusive)
	* @return the range of communication records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CommunicationRecord> getCommunicationRecords(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCommunicationRecords(start, end);
	}

	/**
	* Returns the number of communication records.
	*
	* @return the number of communication records
	* @throws SystemException if a system exception occurred
	*/
	public static int getCommunicationRecordsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCommunicationRecordsCount();
	}

	/**
	* Updates the communication record in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param communicationRecord the communication record
	* @return the communication record that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CommunicationRecord updateCommunicationRecord(
		info.diit.portal.model.CommunicationRecord communicationRecord)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateCommunicationRecord(communicationRecord);
	}

	/**
	* Updates the communication record in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param communicationRecord the communication record
	* @param merge whether to merge the communication record with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the communication record that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CommunicationRecord updateCommunicationRecord(
		info.diit.portal.model.CommunicationRecord communicationRecord,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateCommunicationRecord(communicationRecord, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<info.diit.portal.model.CommunicationRecord> findByCommunicationByUser(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByCommunicationByUser(userId);
	}

	public static void clearService() {
		_service = null;
	}

	public static CommunicationRecordLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					CommunicationRecordLocalService.class.getName());

			if (invokableLocalService instanceof CommunicationRecordLocalService) {
				_service = (CommunicationRecordLocalService)invokableLocalService;
			}
			else {
				_service = new CommunicationRecordLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(CommunicationRecordLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(CommunicationRecordLocalService service) {
	}

	private static CommunicationRecordLocalService _service;
}
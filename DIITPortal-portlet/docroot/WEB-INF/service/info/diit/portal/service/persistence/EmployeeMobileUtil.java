/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.EmployeeMobile;

import java.util.List;

/**
 * The persistence utility for the employee mobile service. This utility wraps {@link EmployeeMobilePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see EmployeeMobilePersistence
 * @see EmployeeMobilePersistenceImpl
 * @generated
 */
public class EmployeeMobileUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(EmployeeMobile employeeMobile) {
		getPersistence().clearCache(employeeMobile);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<EmployeeMobile> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<EmployeeMobile> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<EmployeeMobile> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static EmployeeMobile update(EmployeeMobile employeeMobile,
		boolean merge) throws SystemException {
		return getPersistence().update(employeeMobile, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static EmployeeMobile update(EmployeeMobile employeeMobile,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(employeeMobile, merge, serviceContext);
	}

	/**
	* Caches the employee mobile in the entity cache if it is enabled.
	*
	* @param employeeMobile the employee mobile
	*/
	public static void cacheResult(
		info.diit.portal.model.EmployeeMobile employeeMobile) {
		getPersistence().cacheResult(employeeMobile);
	}

	/**
	* Caches the employee mobiles in the entity cache if it is enabled.
	*
	* @param employeeMobiles the employee mobiles
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.EmployeeMobile> employeeMobiles) {
		getPersistence().cacheResult(employeeMobiles);
	}

	/**
	* Creates a new employee mobile with the primary key. Does not add the employee mobile to the database.
	*
	* @param mobileId the primary key for the new employee mobile
	* @return the new employee mobile
	*/
	public static info.diit.portal.model.EmployeeMobile create(long mobileId) {
		return getPersistence().create(mobileId);
	}

	/**
	* Removes the employee mobile with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param mobileId the primary key of the employee mobile
	* @return the employee mobile that was removed
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a employee mobile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile remove(long mobileId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException {
		return getPersistence().remove(mobileId);
	}

	public static info.diit.portal.model.EmployeeMobile updateImpl(
		info.diit.portal.model.EmployeeMobile employeeMobile, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(employeeMobile, merge);
	}

	/**
	* Returns the employee mobile with the primary key or throws a {@link info.diit.portal.NoSuchEmployeeMobileException} if it could not be found.
	*
	* @param mobileId the primary key of the employee mobile
	* @return the employee mobile
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a employee mobile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile findByPrimaryKey(
		long mobileId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException {
		return getPersistence().findByPrimaryKey(mobileId);
	}

	/**
	* Returns the employee mobile with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param mobileId the primary key of the employee mobile
	* @return the employee mobile, or <code>null</code> if a employee mobile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile fetchByPrimaryKey(
		long mobileId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(mobileId);
	}

	/**
	* Returns all the employee mobiles where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployee(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByEmployee(employeeId);
	}

	/**
	* Returns a range of all the employee mobiles where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of employee mobiles
	* @param end the upper bound of the range of employee mobiles (not inclusive)
	* @return the range of matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployee(
		long employeeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByEmployee(employeeId, start, end);
	}

	/**
	* Returns an ordered range of all the employee mobiles where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of employee mobiles
	* @param end the upper bound of the range of employee mobiles (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployee(
		long employeeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByEmployee(employeeId, start, end, orderByComparator);
	}

	/**
	* Returns the first employee mobile in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee mobile
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile findByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException {
		return getPersistence()
				   .findByEmployee_First(employeeId, orderByComparator);
	}

	/**
	* Returns the first employee mobile in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee mobile, or <code>null</code> if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile fetchByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployee_First(employeeId, orderByComparator);
	}

	/**
	* Returns the last employee mobile in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee mobile
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile findByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException {
		return getPersistence()
				   .findByEmployee_Last(employeeId, orderByComparator);
	}

	/**
	* Returns the last employee mobile in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee mobile, or <code>null</code> if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile fetchByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployee_Last(employeeId, orderByComparator);
	}

	/**
	* Returns the employee mobiles before and after the current employee mobile in the ordered set where employeeId = &#63;.
	*
	* @param mobileId the primary key of the current employee mobile
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next employee mobile
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a employee mobile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile[] findByEmployee_PrevAndNext(
		long mobileId, long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException {
		return getPersistence()
				   .findByEmployee_PrevAndNext(mobileId, employeeId,
			orderByComparator);
	}

	/**
	* Returns all the employee mobiles where employeeId = &#63; and status = &#63;.
	*
	* @param employeeId the employee ID
	* @param status the status
	* @return the matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployeeStatus(
		long employeeId, long status)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByEmployeeStatus(employeeId, status);
	}

	/**
	* Returns a range of all the employee mobiles where employeeId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param status the status
	* @param start the lower bound of the range of employee mobiles
	* @param end the upper bound of the range of employee mobiles (not inclusive)
	* @return the range of matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployeeStatus(
		long employeeId, long status, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByEmployeeStatus(employeeId, status, start, end);
	}

	/**
	* Returns an ordered range of all the employee mobiles where employeeId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param status the status
	* @param start the lower bound of the range of employee mobiles
	* @param end the upper bound of the range of employee mobiles (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeMobile> findByEmployeeStatus(
		long employeeId, long status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByEmployeeStatus(employeeId, status, start, end,
			orderByComparator);
	}

	/**
	* Returns the first employee mobile in the ordered set where employeeId = &#63; and status = &#63;.
	*
	* @param employeeId the employee ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee mobile
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile findByEmployeeStatus_First(
		long employeeId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException {
		return getPersistence()
				   .findByEmployeeStatus_First(employeeId, status,
			orderByComparator);
	}

	/**
	* Returns the first employee mobile in the ordered set where employeeId = &#63; and status = &#63;.
	*
	* @param employeeId the employee ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee mobile, or <code>null</code> if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile fetchByEmployeeStatus_First(
		long employeeId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployeeStatus_First(employeeId, status,
			orderByComparator);
	}

	/**
	* Returns the last employee mobile in the ordered set where employeeId = &#63; and status = &#63;.
	*
	* @param employeeId the employee ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee mobile
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile findByEmployeeStatus_Last(
		long employeeId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException {
		return getPersistence()
				   .findByEmployeeStatus_Last(employeeId, status,
			orderByComparator);
	}

	/**
	* Returns the last employee mobile in the ordered set where employeeId = &#63; and status = &#63;.
	*
	* @param employeeId the employee ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee mobile, or <code>null</code> if a matching employee mobile could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile fetchByEmployeeStatus_Last(
		long employeeId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployeeStatus_Last(employeeId, status,
			orderByComparator);
	}

	/**
	* Returns the employee mobiles before and after the current employee mobile in the ordered set where employeeId = &#63; and status = &#63;.
	*
	* @param mobileId the primary key of the current employee mobile
	* @param employeeId the employee ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next employee mobile
	* @throws info.diit.portal.NoSuchEmployeeMobileException if a employee mobile with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeMobile[] findByEmployeeStatus_PrevAndNext(
		long mobileId, long employeeId, long status,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeMobileException {
		return getPersistence()
				   .findByEmployeeStatus_PrevAndNext(mobileId, employeeId,
			status, orderByComparator);
	}

	/**
	* Returns all the employee mobiles.
	*
	* @return the employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeMobile> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the employee mobiles.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee mobiles
	* @param end the upper bound of the range of employee mobiles (not inclusive)
	* @return the range of employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeMobile> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the employee mobiles.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee mobiles
	* @param end the upper bound of the range of employee mobiles (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeMobile> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the employee mobiles where employeeId = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByEmployee(employeeId);
	}

	/**
	* Removes all the employee mobiles where employeeId = &#63; and status = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @param status the status
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByEmployeeStatus(long employeeId, long status)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByEmployeeStatus(employeeId, status);
	}

	/**
	* Removes all the employee mobiles from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of employee mobiles where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the number of matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public static int countByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByEmployee(employeeId);
	}

	/**
	* Returns the number of employee mobiles where employeeId = &#63; and status = &#63;.
	*
	* @param employeeId the employee ID
	* @param status the status
	* @return the number of matching employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public static int countByEmployeeStatus(long employeeId, long status)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByEmployeeStatus(employeeId, status);
	}

	/**
	* Returns the number of employee mobiles.
	*
	* @return the number of employee mobiles
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static EmployeeMobilePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (EmployeeMobilePersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					EmployeeMobilePersistence.class.getName());

			ReferenceRegistry.registerReference(EmployeeMobileUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(EmployeeMobilePersistence persistence) {
	}

	private static EmployeeMobilePersistence _persistence;
}
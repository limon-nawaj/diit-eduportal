/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.DailyWork;

import java.util.List;

/**
 * The persistence utility for the daily work service. This utility wraps {@link DailyWorkPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see DailyWorkPersistence
 * @see DailyWorkPersistenceImpl
 * @generated
 */
public class DailyWorkUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(DailyWork dailyWork) {
		getPersistence().clearCache(dailyWork);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<DailyWork> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<DailyWork> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<DailyWork> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static DailyWork update(DailyWork dailyWork, boolean merge)
		throws SystemException {
		return getPersistence().update(dailyWork, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static DailyWork update(DailyWork dailyWork, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(dailyWork, merge, serviceContext);
	}

	/**
	* Caches the daily work in the entity cache if it is enabled.
	*
	* @param dailyWork the daily work
	*/
	public static void cacheResult(info.diit.portal.model.DailyWork dailyWork) {
		getPersistence().cacheResult(dailyWork);
	}

	/**
	* Caches the daily works in the entity cache if it is enabled.
	*
	* @param dailyWorks the daily works
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.DailyWork> dailyWorks) {
		getPersistence().cacheResult(dailyWorks);
	}

	/**
	* Creates a new daily work with the primary key. Does not add the daily work to the database.
	*
	* @param dailyWorkId the primary key for the new daily work
	* @return the new daily work
	*/
	public static info.diit.portal.model.DailyWork create(long dailyWorkId) {
		return getPersistence().create(dailyWorkId);
	}

	/**
	* Removes the daily work with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dailyWorkId the primary key of the daily work
	* @return the daily work that was removed
	* @throws info.diit.portal.NoSuchDailyWorkException if a daily work with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork remove(long dailyWorkId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDailyWorkException {
		return getPersistence().remove(dailyWorkId);
	}

	public static info.diit.portal.model.DailyWork updateImpl(
		info.diit.portal.model.DailyWork dailyWork, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(dailyWork, merge);
	}

	/**
	* Returns the daily work with the primary key or throws a {@link info.diit.portal.NoSuchDailyWorkException} if it could not be found.
	*
	* @param dailyWorkId the primary key of the daily work
	* @return the daily work
	* @throws info.diit.portal.NoSuchDailyWorkException if a daily work with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork findByPrimaryKey(
		long dailyWorkId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDailyWorkException {
		return getPersistence().findByPrimaryKey(dailyWorkId);
	}

	/**
	* Returns the daily work with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param dailyWorkId the primary key of the daily work
	* @return the daily work, or <code>null</code> if a daily work with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork fetchByPrimaryKey(
		long dailyWorkId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(dailyWorkId);
	}

	/**
	* Returns all the daily works where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the matching daily works
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DailyWork> findByEmployee(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByEmployee(employeeId);
	}

	/**
	* Returns a range of all the daily works where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of daily works
	* @param end the upper bound of the range of daily works (not inclusive)
	* @return the range of matching daily works
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DailyWork> findByEmployee(
		long employeeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByEmployee(employeeId, start, end);
	}

	/**
	* Returns an ordered range of all the daily works where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of daily works
	* @param end the upper bound of the range of daily works (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching daily works
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DailyWork> findByEmployee(
		long employeeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByEmployee(employeeId, start, end, orderByComparator);
	}

	/**
	* Returns the first daily work in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching daily work
	* @throws info.diit.portal.NoSuchDailyWorkException if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork findByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDailyWorkException {
		return getPersistence()
				   .findByEmployee_First(employeeId, orderByComparator);
	}

	/**
	* Returns the first daily work in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching daily work, or <code>null</code> if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork fetchByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployee_First(employeeId, orderByComparator);
	}

	/**
	* Returns the last daily work in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching daily work
	* @throws info.diit.portal.NoSuchDailyWorkException if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork findByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDailyWorkException {
		return getPersistence()
				   .findByEmployee_Last(employeeId, orderByComparator);
	}

	/**
	* Returns the last daily work in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching daily work, or <code>null</code> if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork fetchByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployee_Last(employeeId, orderByComparator);
	}

	/**
	* Returns the daily works before and after the current daily work in the ordered set where employeeId = &#63;.
	*
	* @param dailyWorkId the primary key of the current daily work
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next daily work
	* @throws info.diit.portal.NoSuchDailyWorkException if a daily work with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork[] findByEmployee_PrevAndNext(
		long dailyWorkId, long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDailyWorkException {
		return getPersistence()
				   .findByEmployee_PrevAndNext(dailyWorkId, employeeId,
			orderByComparator);
	}

	/**
	* Returns all the daily works where employeeId = &#63; and worKingDay = &#63;.
	*
	* @param employeeId the employee ID
	* @param worKingDay the wor king day
	* @return the matching daily works
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DailyWork> findByEmployeeWorkingDay(
		long employeeId, java.util.Date worKingDay)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByEmployeeWorkingDay(employeeId, worKingDay);
	}

	/**
	* Returns a range of all the daily works where employeeId = &#63; and worKingDay = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param worKingDay the wor king day
	* @param start the lower bound of the range of daily works
	* @param end the upper bound of the range of daily works (not inclusive)
	* @return the range of matching daily works
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DailyWork> findByEmployeeWorkingDay(
		long employeeId, java.util.Date worKingDay, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByEmployeeWorkingDay(employeeId, worKingDay, start, end);
	}

	/**
	* Returns an ordered range of all the daily works where employeeId = &#63; and worKingDay = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param worKingDay the wor king day
	* @param start the lower bound of the range of daily works
	* @param end the upper bound of the range of daily works (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching daily works
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DailyWork> findByEmployeeWorkingDay(
		long employeeId, java.util.Date worKingDay, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByEmployeeWorkingDay(employeeId, worKingDay, start,
			end, orderByComparator);
	}

	/**
	* Returns the first daily work in the ordered set where employeeId = &#63; and worKingDay = &#63;.
	*
	* @param employeeId the employee ID
	* @param worKingDay the wor king day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching daily work
	* @throws info.diit.portal.NoSuchDailyWorkException if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork findByEmployeeWorkingDay_First(
		long employeeId, java.util.Date worKingDay,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDailyWorkException {
		return getPersistence()
				   .findByEmployeeWorkingDay_First(employeeId, worKingDay,
			orderByComparator);
	}

	/**
	* Returns the first daily work in the ordered set where employeeId = &#63; and worKingDay = &#63;.
	*
	* @param employeeId the employee ID
	* @param worKingDay the wor king day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching daily work, or <code>null</code> if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork fetchByEmployeeWorkingDay_First(
		long employeeId, java.util.Date worKingDay,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployeeWorkingDay_First(employeeId, worKingDay,
			orderByComparator);
	}

	/**
	* Returns the last daily work in the ordered set where employeeId = &#63; and worKingDay = &#63;.
	*
	* @param employeeId the employee ID
	* @param worKingDay the wor king day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching daily work
	* @throws info.diit.portal.NoSuchDailyWorkException if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork findByEmployeeWorkingDay_Last(
		long employeeId, java.util.Date worKingDay,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDailyWorkException {
		return getPersistence()
				   .findByEmployeeWorkingDay_Last(employeeId, worKingDay,
			orderByComparator);
	}

	/**
	* Returns the last daily work in the ordered set where employeeId = &#63; and worKingDay = &#63;.
	*
	* @param employeeId the employee ID
	* @param worKingDay the wor king day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching daily work, or <code>null</code> if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork fetchByEmployeeWorkingDay_Last(
		long employeeId, java.util.Date worKingDay,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployeeWorkingDay_Last(employeeId, worKingDay,
			orderByComparator);
	}

	/**
	* Returns the daily works before and after the current daily work in the ordered set where employeeId = &#63; and worKingDay = &#63;.
	*
	* @param dailyWorkId the primary key of the current daily work
	* @param employeeId the employee ID
	* @param worKingDay the wor king day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next daily work
	* @throws info.diit.portal.NoSuchDailyWorkException if a daily work with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork[] findByEmployeeWorkingDay_PrevAndNext(
		long dailyWorkId, long employeeId, java.util.Date worKingDay,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDailyWorkException {
		return getPersistence()
				   .findByEmployeeWorkingDay_PrevAndNext(dailyWorkId,
			employeeId, worKingDay, orderByComparator);
	}

	/**
	* Returns the daily work where employeeId = &#63; and taskId = &#63; and worKingDay = &#63; or throws a {@link info.diit.portal.NoSuchDailyWorkException} if it could not be found.
	*
	* @param employeeId the employee ID
	* @param taskId the task ID
	* @param worKingDay the wor king day
	* @return the matching daily work
	* @throws info.diit.portal.NoSuchDailyWorkException if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork findByEmployeeTaskWorkingDay(
		long employeeId, long taskId, java.util.Date worKingDay)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDailyWorkException {
		return getPersistence()
				   .findByEmployeeTaskWorkingDay(employeeId, taskId, worKingDay);
	}

	/**
	* Returns the daily work where employeeId = &#63; and taskId = &#63; and worKingDay = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param employeeId the employee ID
	* @param taskId the task ID
	* @param worKingDay the wor king day
	* @return the matching daily work, or <code>null</code> if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork fetchByEmployeeTaskWorkingDay(
		long employeeId, long taskId, java.util.Date worKingDay)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployeeTaskWorkingDay(employeeId, taskId, worKingDay);
	}

	/**
	* Returns the daily work where employeeId = &#63; and taskId = &#63; and worKingDay = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param employeeId the employee ID
	* @param taskId the task ID
	* @param worKingDay the wor king day
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching daily work, or <code>null</code> if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork fetchByEmployeeTaskWorkingDay(
		long employeeId, long taskId, java.util.Date worKingDay,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployeeTaskWorkingDay(employeeId, taskId,
			worKingDay, retrieveFromCache);
	}

	/**
	* Returns all the daily works where taskId = &#63;.
	*
	* @param taskId the task ID
	* @return the matching daily works
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DailyWork> findByTask(
		long taskId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByTask(taskId);
	}

	/**
	* Returns a range of all the daily works where taskId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param taskId the task ID
	* @param start the lower bound of the range of daily works
	* @param end the upper bound of the range of daily works (not inclusive)
	* @return the range of matching daily works
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DailyWork> findByTask(
		long taskId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByTask(taskId, start, end);
	}

	/**
	* Returns an ordered range of all the daily works where taskId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param taskId the task ID
	* @param start the lower bound of the range of daily works
	* @param end the upper bound of the range of daily works (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching daily works
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DailyWork> findByTask(
		long taskId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByTask(taskId, start, end, orderByComparator);
	}

	/**
	* Returns the first daily work in the ordered set where taskId = &#63;.
	*
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching daily work
	* @throws info.diit.portal.NoSuchDailyWorkException if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork findByTask_First(
		long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDailyWorkException {
		return getPersistence().findByTask_First(taskId, orderByComparator);
	}

	/**
	* Returns the first daily work in the ordered set where taskId = &#63;.
	*
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching daily work, or <code>null</code> if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork fetchByTask_First(
		long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByTask_First(taskId, orderByComparator);
	}

	/**
	* Returns the last daily work in the ordered set where taskId = &#63;.
	*
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching daily work
	* @throws info.diit.portal.NoSuchDailyWorkException if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork findByTask_Last(
		long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDailyWorkException {
		return getPersistence().findByTask_Last(taskId, orderByComparator);
	}

	/**
	* Returns the last daily work in the ordered set where taskId = &#63;.
	*
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching daily work, or <code>null</code> if a matching daily work could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork fetchByTask_Last(
		long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByTask_Last(taskId, orderByComparator);
	}

	/**
	* Returns the daily works before and after the current daily work in the ordered set where taskId = &#63;.
	*
	* @param dailyWorkId the primary key of the current daily work
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next daily work
	* @throws info.diit.portal.NoSuchDailyWorkException if a daily work with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork[] findByTask_PrevAndNext(
		long dailyWorkId, long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDailyWorkException {
		return getPersistence()
				   .findByTask_PrevAndNext(dailyWorkId, taskId,
			orderByComparator);
	}

	/**
	* Returns all the daily works.
	*
	* @return the daily works
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DailyWork> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the daily works.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of daily works
	* @param end the upper bound of the range of daily works (not inclusive)
	* @return the range of daily works
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DailyWork> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the daily works.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of daily works
	* @param end the upper bound of the range of daily works (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of daily works
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.DailyWork> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the daily works where employeeId = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByEmployee(employeeId);
	}

	/**
	* Removes all the daily works where employeeId = &#63; and worKingDay = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @param worKingDay the wor king day
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByEmployeeWorkingDay(long employeeId,
		java.util.Date worKingDay)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByEmployeeWorkingDay(employeeId, worKingDay);
	}

	/**
	* Removes the daily work where employeeId = &#63; and taskId = &#63; and worKingDay = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @param taskId the task ID
	* @param worKingDay the wor king day
	* @return the daily work that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.DailyWork removeByEmployeeTaskWorkingDay(
		long employeeId, long taskId, java.util.Date worKingDay)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchDailyWorkException {
		return getPersistence()
				   .removeByEmployeeTaskWorkingDay(employeeId, taskId,
			worKingDay);
	}

	/**
	* Removes all the daily works where taskId = &#63; from the database.
	*
	* @param taskId the task ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByTask(long taskId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByTask(taskId);
	}

	/**
	* Removes all the daily works from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of daily works where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the number of matching daily works
	* @throws SystemException if a system exception occurred
	*/
	public static int countByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByEmployee(employeeId);
	}

	/**
	* Returns the number of daily works where employeeId = &#63; and worKingDay = &#63;.
	*
	* @param employeeId the employee ID
	* @param worKingDay the wor king day
	* @return the number of matching daily works
	* @throws SystemException if a system exception occurred
	*/
	public static int countByEmployeeWorkingDay(long employeeId,
		java.util.Date worKingDay)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByEmployeeWorkingDay(employeeId, worKingDay);
	}

	/**
	* Returns the number of daily works where employeeId = &#63; and taskId = &#63; and worKingDay = &#63;.
	*
	* @param employeeId the employee ID
	* @param taskId the task ID
	* @param worKingDay the wor king day
	* @return the number of matching daily works
	* @throws SystemException if a system exception occurred
	*/
	public static int countByEmployeeTaskWorkingDay(long employeeId,
		long taskId, java.util.Date worKingDay)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByEmployeeTaskWorkingDay(employeeId, taskId, worKingDay);
	}

	/**
	* Returns the number of daily works where taskId = &#63;.
	*
	* @param taskId the task ID
	* @return the number of matching daily works
	* @throws SystemException if a system exception occurred
	*/
	public static int countByTask(long taskId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByTask(taskId);
	}

	/**
	* Returns the number of daily works.
	*
	* @return the number of daily works
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static DailyWorkPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (DailyWorkPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					DailyWorkPersistence.class.getName());

			ReferenceRegistry.registerReference(DailyWorkUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(DailyWorkPersistence persistence) {
	}

	private static DailyWorkPersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.LessonPlan;

import java.util.List;

/**
 * The persistence utility for the lesson plan service. This utility wraps {@link LessonPlanPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see LessonPlanPersistence
 * @see LessonPlanPersistenceImpl
 * @generated
 */
public class LessonPlanUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(LessonPlan lessonPlan) {
		getPersistence().clearCache(lessonPlan);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<LessonPlan> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<LessonPlan> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<LessonPlan> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static LessonPlan update(LessonPlan lessonPlan, boolean merge)
		throws SystemException {
		return getPersistence().update(lessonPlan, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static LessonPlan update(LessonPlan lessonPlan, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(lessonPlan, merge, serviceContext);
	}

	/**
	* Caches the lesson plan in the entity cache if it is enabled.
	*
	* @param lessonPlan the lesson plan
	*/
	public static void cacheResult(info.diit.portal.model.LessonPlan lessonPlan) {
		getPersistence().cacheResult(lessonPlan);
	}

	/**
	* Caches the lesson plans in the entity cache if it is enabled.
	*
	* @param lessonPlans the lesson plans
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.LessonPlan> lessonPlans) {
		getPersistence().cacheResult(lessonPlans);
	}

	/**
	* Creates a new lesson plan with the primary key. Does not add the lesson plan to the database.
	*
	* @param lessonPlanId the primary key for the new lesson plan
	* @return the new lesson plan
	*/
	public static info.diit.portal.model.LessonPlan create(long lessonPlanId) {
		return getPersistence().create(lessonPlanId);
	}

	/**
	* Removes the lesson plan with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonPlanId the primary key of the lesson plan
	* @return the lesson plan that was removed
	* @throws info.diit.portal.NoSuchLessonPlanException if a lesson plan with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonPlan remove(long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException {
		return getPersistence().remove(lessonPlanId);
	}

	public static info.diit.portal.model.LessonPlan updateImpl(
		info.diit.portal.model.LessonPlan lessonPlan, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(lessonPlan, merge);
	}

	/**
	* Returns the lesson plan with the primary key or throws a {@link info.diit.portal.NoSuchLessonPlanException} if it could not be found.
	*
	* @param lessonPlanId the primary key of the lesson plan
	* @return the lesson plan
	* @throws info.diit.portal.NoSuchLessonPlanException if a lesson plan with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonPlan findByPrimaryKey(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException {
		return getPersistence().findByPrimaryKey(lessonPlanId);
	}

	/**
	* Returns the lesson plan with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param lessonPlanId the primary key of the lesson plan
	* @return the lesson plan, or <code>null</code> if a lesson plan with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonPlan fetchByPrimaryKey(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(lessonPlanId);
	}

	/**
	* Returns all the lesson plans where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonPlan> findByOrganizationUser(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganizationUser(userId);
	}

	/**
	* Returns a range of all the lesson plans where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of lesson plans
	* @param end the upper bound of the range of lesson plans (not inclusive)
	* @return the range of matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonPlan> findByOrganizationUser(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganizationUser(userId, start, end);
	}

	/**
	* Returns an ordered range of all the lesson plans where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of lesson plans
	* @param end the upper bound of the range of lesson plans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonPlan> findByOrganizationUser(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByOrganizationUser(userId, start, end, orderByComparator);
	}

	/**
	* Returns the first lesson plan in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson plan
	* @throws info.diit.portal.NoSuchLessonPlanException if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonPlan findByOrganizationUser_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException {
		return getPersistence()
				   .findByOrganizationUser_First(userId, orderByComparator);
	}

	/**
	* Returns the first lesson plan in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson plan, or <code>null</code> if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonPlan fetchByOrganizationUser_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganizationUser_First(userId, orderByComparator);
	}

	/**
	* Returns the last lesson plan in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson plan
	* @throws info.diit.portal.NoSuchLessonPlanException if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonPlan findByOrganizationUser_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException {
		return getPersistence()
				   .findByOrganizationUser_Last(userId, orderByComparator);
	}

	/**
	* Returns the last lesson plan in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson plan, or <code>null</code> if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonPlan fetchByOrganizationUser_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganizationUser_Last(userId, orderByComparator);
	}

	/**
	* Returns the lesson plans before and after the current lesson plan in the ordered set where userId = &#63;.
	*
	* @param lessonPlanId the primary key of the current lesson plan
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lesson plan
	* @throws info.diit.portal.NoSuchLessonPlanException if a lesson plan with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonPlan[] findByOrganizationUser_PrevAndNext(
		long lessonPlanId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException {
		return getPersistence()
				   .findByOrganizationUser_PrevAndNext(lessonPlanId, userId,
			orderByComparator);
	}

	/**
	* Returns all the lesson plans where subject = &#63;.
	*
	* @param subject the subject
	* @return the matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonPlan> findBySubject(
		long subject)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBySubject(subject);
	}

	/**
	* Returns a range of all the lesson plans where subject = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subject the subject
	* @param start the lower bound of the range of lesson plans
	* @param end the upper bound of the range of lesson plans (not inclusive)
	* @return the range of matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonPlan> findBySubject(
		long subject, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBySubject(subject, start, end);
	}

	/**
	* Returns an ordered range of all the lesson plans where subject = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subject the subject
	* @param start the lower bound of the range of lesson plans
	* @param end the upper bound of the range of lesson plans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonPlan> findBySubject(
		long subject, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBySubject(subject, start, end, orderByComparator);
	}

	/**
	* Returns the first lesson plan in the ordered set where subject = &#63;.
	*
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson plan
	* @throws info.diit.portal.NoSuchLessonPlanException if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonPlan findBySubject_First(
		long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException {
		return getPersistence().findBySubject_First(subject, orderByComparator);
	}

	/**
	* Returns the first lesson plan in the ordered set where subject = &#63;.
	*
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson plan, or <code>null</code> if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonPlan fetchBySubject_First(
		long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBySubject_First(subject, orderByComparator);
	}

	/**
	* Returns the last lesson plan in the ordered set where subject = &#63;.
	*
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson plan
	* @throws info.diit.portal.NoSuchLessonPlanException if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonPlan findBySubject_Last(
		long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException {
		return getPersistence().findBySubject_Last(subject, orderByComparator);
	}

	/**
	* Returns the last lesson plan in the ordered set where subject = &#63;.
	*
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson plan, or <code>null</code> if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonPlan fetchBySubject_Last(
		long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBySubject_Last(subject, orderByComparator);
	}

	/**
	* Returns the lesson plans before and after the current lesson plan in the ordered set where subject = &#63;.
	*
	* @param lessonPlanId the primary key of the current lesson plan
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lesson plan
	* @throws info.diit.portal.NoSuchLessonPlanException if a lesson plan with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonPlan[] findBySubject_PrevAndNext(
		long lessonPlanId, long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException {
		return getPersistence()
				   .findBySubject_PrevAndNext(lessonPlanId, subject,
			orderByComparator);
	}

	/**
	* Returns all the lesson plans.
	*
	* @return the lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonPlan> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the lesson plans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson plans
	* @param end the upper bound of the range of lesson plans (not inclusive)
	* @return the range of lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonPlan> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the lesson plans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson plans
	* @param end the upper bound of the range of lesson plans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonPlan> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the lesson plans where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByOrganizationUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByOrganizationUser(userId);
	}

	/**
	* Removes all the lesson plans where subject = &#63; from the database.
	*
	* @param subject the subject
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBySubject(long subject)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBySubject(subject);
	}

	/**
	* Removes all the lesson plans from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of lesson plans where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public static int countByOrganizationUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByOrganizationUser(userId);
	}

	/**
	* Returns the number of lesson plans where subject = &#63;.
	*
	* @param subject the subject
	* @return the number of matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public static int countBySubject(long subject)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBySubject(subject);
	}

	/**
	* Returns the number of lesson plans.
	*
	* @return the number of lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static LessonPlanPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (LessonPlanPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					LessonPlanPersistence.class.getName());

			ReferenceRegistry.registerReference(LessonPlanUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(LessonPlanPersistence persistence) {
	}

	private static LessonPlanPersistence _persistence;
}
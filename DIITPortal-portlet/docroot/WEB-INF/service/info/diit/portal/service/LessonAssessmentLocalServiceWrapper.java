/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link LessonAssessmentLocalService}.
 * </p>
 *
 * @author    mohammad
 * @see       LessonAssessmentLocalService
 * @generated
 */
public class LessonAssessmentLocalServiceWrapper
	implements LessonAssessmentLocalService,
		ServiceWrapper<LessonAssessmentLocalService> {
	public LessonAssessmentLocalServiceWrapper(
		LessonAssessmentLocalService lessonAssessmentLocalService) {
		_lessonAssessmentLocalService = lessonAssessmentLocalService;
	}

	/**
	* Adds the lesson assessment to the database. Also notifies the appropriate model listeners.
	*
	* @param lessonAssessment the lesson assessment
	* @return the lesson assessment that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonAssessment addLessonAssessment(
		info.diit.portal.model.LessonAssessment lessonAssessment)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.addLessonAssessment(lessonAssessment);
	}

	/**
	* Creates a new lesson assessment with the primary key. Does not add the lesson assessment to the database.
	*
	* @param lessonAssessmentId the primary key for the new lesson assessment
	* @return the new lesson assessment
	*/
	public info.diit.portal.model.LessonAssessment createLessonAssessment(
		long lessonAssessmentId) {
		return _lessonAssessmentLocalService.createLessonAssessment(lessonAssessmentId);
	}

	/**
	* Deletes the lesson assessment with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonAssessmentId the primary key of the lesson assessment
	* @return the lesson assessment that was removed
	* @throws PortalException if a lesson assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonAssessment deleteLessonAssessment(
		long lessonAssessmentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.deleteLessonAssessment(lessonAssessmentId);
	}

	/**
	* Deletes the lesson assessment from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonAssessment the lesson assessment
	* @return the lesson assessment that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonAssessment deleteLessonAssessment(
		info.diit.portal.model.LessonAssessment lessonAssessment)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.deleteLessonAssessment(lessonAssessment);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _lessonAssessmentLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.LessonAssessment fetchLessonAssessment(
		long lessonAssessmentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.fetchLessonAssessment(lessonAssessmentId);
	}

	/**
	* Returns the lesson assessment with the primary key.
	*
	* @param lessonAssessmentId the primary key of the lesson assessment
	* @return the lesson assessment
	* @throws PortalException if a lesson assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonAssessment getLessonAssessment(
		long lessonAssessmentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.getLessonAssessment(lessonAssessmentId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the lesson assessments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson assessments
	* @param end the upper bound of the range of lesson assessments (not inclusive)
	* @return the range of lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonAssessment> getLessonAssessments(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.getLessonAssessments(start, end);
	}

	/**
	* Returns the number of lesson assessments.
	*
	* @return the number of lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public int getLessonAssessmentsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.getLessonAssessmentsCount();
	}

	/**
	* Updates the lesson assessment in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lessonAssessment the lesson assessment
	* @return the lesson assessment that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonAssessment updateLessonAssessment(
		info.diit.portal.model.LessonAssessment lessonAssessment)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.updateLessonAssessment(lessonAssessment);
	}

	/**
	* Updates the lesson assessment in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lessonAssessment the lesson assessment
	* @param merge whether to merge the lesson assessment with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the lesson assessment that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonAssessment updateLessonAssessment(
		info.diit.portal.model.LessonAssessment lessonAssessment, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.updateLessonAssessment(lessonAssessment,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _lessonAssessmentLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_lessonAssessmentLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _lessonAssessmentLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.model.LessonAssessment> findByLessonPlan(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonAssessmentLocalService.findByLessonPlan(lessonPlanId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public LessonAssessmentLocalService getWrappedLessonAssessmentLocalService() {
		return _lessonAssessmentLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedLessonAssessmentLocalService(
		LessonAssessmentLocalService lessonAssessmentLocalService) {
		_lessonAssessmentLocalService = lessonAssessmentLocalService;
	}

	public LessonAssessmentLocalService getWrappedService() {
		return _lessonAssessmentLocalService;
	}

	public void setWrappedService(
		LessonAssessmentLocalService lessonAssessmentLocalService) {
		_lessonAssessmentLocalService = lessonAssessmentLocalService;
	}

	private LessonAssessmentLocalService _lessonAssessmentLocalService;
}
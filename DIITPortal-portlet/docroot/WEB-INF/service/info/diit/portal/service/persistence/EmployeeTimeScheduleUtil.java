/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.EmployeeTimeSchedule;

import java.util.List;

/**
 * The persistence utility for the employee time schedule service. This utility wraps {@link EmployeeTimeSchedulePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see EmployeeTimeSchedulePersistence
 * @see EmployeeTimeSchedulePersistenceImpl
 * @generated
 */
public class EmployeeTimeScheduleUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(EmployeeTimeSchedule employeeTimeSchedule) {
		getPersistence().clearCache(employeeTimeSchedule);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<EmployeeTimeSchedule> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<EmployeeTimeSchedule> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<EmployeeTimeSchedule> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static EmployeeTimeSchedule update(
		EmployeeTimeSchedule employeeTimeSchedule, boolean merge)
		throws SystemException {
		return getPersistence().update(employeeTimeSchedule, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static EmployeeTimeSchedule update(
		EmployeeTimeSchedule employeeTimeSchedule, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence()
				   .update(employeeTimeSchedule, merge, serviceContext);
	}

	/**
	* Caches the employee time schedule in the entity cache if it is enabled.
	*
	* @param employeeTimeSchedule the employee time schedule
	*/
	public static void cacheResult(
		info.diit.portal.model.EmployeeTimeSchedule employeeTimeSchedule) {
		getPersistence().cacheResult(employeeTimeSchedule);
	}

	/**
	* Caches the employee time schedules in the entity cache if it is enabled.
	*
	* @param employeeTimeSchedules the employee time schedules
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.EmployeeTimeSchedule> employeeTimeSchedules) {
		getPersistence().cacheResult(employeeTimeSchedules);
	}

	/**
	* Creates a new employee time schedule with the primary key. Does not add the employee time schedule to the database.
	*
	* @param employeeTimeScheduleId the primary key for the new employee time schedule
	* @return the new employee time schedule
	*/
	public static info.diit.portal.model.EmployeeTimeSchedule create(
		long employeeTimeScheduleId) {
		return getPersistence().create(employeeTimeScheduleId);
	}

	/**
	* Removes the employee time schedule with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param employeeTimeScheduleId the primary key of the employee time schedule
	* @return the employee time schedule that was removed
	* @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a employee time schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeTimeSchedule remove(
		long employeeTimeScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException {
		return getPersistence().remove(employeeTimeScheduleId);
	}

	public static info.diit.portal.model.EmployeeTimeSchedule updateImpl(
		info.diit.portal.model.EmployeeTimeSchedule employeeTimeSchedule,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(employeeTimeSchedule, merge);
	}

	/**
	* Returns the employee time schedule with the primary key or throws a {@link info.diit.portal.NoSuchEmployeeTimeScheduleException} if it could not be found.
	*
	* @param employeeTimeScheduleId the primary key of the employee time schedule
	* @return the employee time schedule
	* @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a employee time schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeTimeSchedule findByPrimaryKey(
		long employeeTimeScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException {
		return getPersistence().findByPrimaryKey(employeeTimeScheduleId);
	}

	/**
	* Returns the employee time schedule with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param employeeTimeScheduleId the primary key of the employee time schedule
	* @return the employee time schedule, or <code>null</code> if a employee time schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeTimeSchedule fetchByPrimaryKey(
		long employeeTimeScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(employeeTimeScheduleId);
	}

	/**
	* Returns all the employee time schedules where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the matching employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeTimeSchedule> findByEmployee(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByEmployee(employeeId);
	}

	/**
	* Returns a range of all the employee time schedules where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of employee time schedules
	* @param end the upper bound of the range of employee time schedules (not inclusive)
	* @return the range of matching employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeTimeSchedule> findByEmployee(
		long employeeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByEmployee(employeeId, start, end);
	}

	/**
	* Returns an ordered range of all the employee time schedules where employeeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param employeeId the employee ID
	* @param start the lower bound of the range of employee time schedules
	* @param end the upper bound of the range of employee time schedules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeTimeSchedule> findByEmployee(
		long employeeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByEmployee(employeeId, start, end, orderByComparator);
	}

	/**
	* Returns the first employee time schedule in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee time schedule
	* @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a matching employee time schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeTimeSchedule findByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException {
		return getPersistence()
				   .findByEmployee_First(employeeId, orderByComparator);
	}

	/**
	* Returns the first employee time schedule in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching employee time schedule, or <code>null</code> if a matching employee time schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeTimeSchedule fetchByEmployee_First(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployee_First(employeeId, orderByComparator);
	}

	/**
	* Returns the last employee time schedule in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee time schedule
	* @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a matching employee time schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeTimeSchedule findByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException {
		return getPersistence()
				   .findByEmployee_Last(employeeId, orderByComparator);
	}

	/**
	* Returns the last employee time schedule in the ordered set where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching employee time schedule, or <code>null</code> if a matching employee time schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeTimeSchedule fetchByEmployee_Last(
		long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployee_Last(employeeId, orderByComparator);
	}

	/**
	* Returns the employee time schedules before and after the current employee time schedule in the ordered set where employeeId = &#63;.
	*
	* @param employeeTimeScheduleId the primary key of the current employee time schedule
	* @param employeeId the employee ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next employee time schedule
	* @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a employee time schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeTimeSchedule[] findByEmployee_PrevAndNext(
		long employeeTimeScheduleId, long employeeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException {
		return getPersistence()
				   .findByEmployee_PrevAndNext(employeeTimeScheduleId,
			employeeId, orderByComparator);
	}

	/**
	* Returns the employee time schedule where employeeId = &#63; and day = &#63; or throws a {@link info.diit.portal.NoSuchEmployeeTimeScheduleException} if it could not be found.
	*
	* @param employeeId the employee ID
	* @param day the day
	* @return the matching employee time schedule
	* @throws info.diit.portal.NoSuchEmployeeTimeScheduleException if a matching employee time schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeTimeSchedule findByEmployeeDay(
		long employeeId, int day)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException {
		return getPersistence().findByEmployeeDay(employeeId, day);
	}

	/**
	* Returns the employee time schedule where employeeId = &#63; and day = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param employeeId the employee ID
	* @param day the day
	* @return the matching employee time schedule, or <code>null</code> if a matching employee time schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeTimeSchedule fetchByEmployeeDay(
		long employeeId, int day)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByEmployeeDay(employeeId, day);
	}

	/**
	* Returns the employee time schedule where employeeId = &#63; and day = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param employeeId the employee ID
	* @param day the day
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching employee time schedule, or <code>null</code> if a matching employee time schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeTimeSchedule fetchByEmployeeDay(
		long employeeId, int day, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByEmployeeDay(employeeId, day, retrieveFromCache);
	}

	/**
	* Returns all the employee time schedules.
	*
	* @return the employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeTimeSchedule> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the employee time schedules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee time schedules
	* @param end the upper bound of the range of employee time schedules (not inclusive)
	* @return the range of employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeTimeSchedule> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the employee time schedules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee time schedules
	* @param end the upper bound of the range of employee time schedules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.EmployeeTimeSchedule> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the employee time schedules where employeeId = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByEmployee(employeeId);
	}

	/**
	* Removes the employee time schedule where employeeId = &#63; and day = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @param day the day
	* @return the employee time schedule that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.EmployeeTimeSchedule removeByEmployeeDay(
		long employeeId, int day)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException {
		return getPersistence().removeByEmployeeDay(employeeId, day);
	}

	/**
	* Removes all the employee time schedules from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of employee time schedules where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the number of matching employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByEmployee(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByEmployee(employeeId);
	}

	/**
	* Returns the number of employee time schedules where employeeId = &#63; and day = &#63;.
	*
	* @param employeeId the employee ID
	* @param day the day
	* @return the number of matching employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByEmployeeDay(long employeeId, int day)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByEmployeeDay(employeeId, day);
	}

	/**
	* Returns the number of employee time schedules.
	*
	* @return the number of employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static EmployeeTimeSchedulePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (EmployeeTimeSchedulePersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					EmployeeTimeSchedulePersistence.class.getName());

			ReferenceRegistry.registerReference(EmployeeTimeScheduleUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(EmployeeTimeSchedulePersistence persistence) {
	}

	private static EmployeeTimeSchedulePersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.BatchFee;

import java.util.List;

/**
 * The persistence utility for the batch fee service. This utility wraps {@link BatchFeePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see BatchFeePersistence
 * @see BatchFeePersistenceImpl
 * @generated
 */
public class BatchFeeUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(BatchFee batchFee) {
		getPersistence().clearCache(batchFee);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<BatchFee> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<BatchFee> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<BatchFee> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static BatchFee update(BatchFee batchFee, boolean merge)
		throws SystemException {
		return getPersistence().update(batchFee, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static BatchFee update(BatchFee batchFee, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(batchFee, merge, serviceContext);
	}

	/**
	* Caches the batch fee in the entity cache if it is enabled.
	*
	* @param batchFee the batch fee
	*/
	public static void cacheResult(info.diit.portal.model.BatchFee batchFee) {
		getPersistence().cacheResult(batchFee);
	}

	/**
	* Caches the batch fees in the entity cache if it is enabled.
	*
	* @param batchFees the batch fees
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.BatchFee> batchFees) {
		getPersistence().cacheResult(batchFees);
	}

	/**
	* Creates a new batch fee with the primary key. Does not add the batch fee to the database.
	*
	* @param batchFeeId the primary key for the new batch fee
	* @return the new batch fee
	*/
	public static info.diit.portal.model.BatchFee create(long batchFeeId) {
		return getPersistence().create(batchFeeId);
	}

	/**
	* Removes the batch fee with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchFeeId the primary key of the batch fee
	* @return the batch fee that was removed
	* @throws info.diit.portal.NoSuchBatchFeeException if a batch fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchFee remove(long batchFeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchFeeException {
		return getPersistence().remove(batchFeeId);
	}

	public static info.diit.portal.model.BatchFee updateImpl(
		info.diit.portal.model.BatchFee batchFee, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(batchFee, merge);
	}

	/**
	* Returns the batch fee with the primary key or throws a {@link info.diit.portal.NoSuchBatchFeeException} if it could not be found.
	*
	* @param batchFeeId the primary key of the batch fee
	* @return the batch fee
	* @throws info.diit.portal.NoSuchBatchFeeException if a batch fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchFee findByPrimaryKey(
		long batchFeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchFeeException {
		return getPersistence().findByPrimaryKey(batchFeeId);
	}

	/**
	* Returns the batch fee with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param batchFeeId the primary key of the batch fee
	* @return the batch fee, or <code>null</code> if a batch fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchFee fetchByPrimaryKey(
		long batchFeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(batchFeeId);
	}

	/**
	* Returns all the batch fees where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the matching batch fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchFee> findByBatch(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBatch(batchId);
	}

	/**
	* Returns a range of all the batch fees where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch fees
	* @param end the upper bound of the range of batch fees (not inclusive)
	* @return the range of matching batch fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchFee> findByBatch(
		long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBatch(batchId, start, end);
	}

	/**
	* Returns an ordered range of all the batch fees where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch fees
	* @param end the upper bound of the range of batch fees (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchFee> findByBatch(
		long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatch(batchId, start, end, orderByComparator);
	}

	/**
	* Returns the first batch fee in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch fee
	* @throws info.diit.portal.NoSuchBatchFeeException if a matching batch fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchFee findByBatch_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchFeeException {
		return getPersistence().findByBatch_First(batchId, orderByComparator);
	}

	/**
	* Returns the first batch fee in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch fee, or <code>null</code> if a matching batch fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchFee fetchByBatch_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByBatch_First(batchId, orderByComparator);
	}

	/**
	* Returns the last batch fee in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch fee
	* @throws info.diit.portal.NoSuchBatchFeeException if a matching batch fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchFee findByBatch_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchFeeException {
		return getPersistence().findByBatch_Last(batchId, orderByComparator);
	}

	/**
	* Returns the last batch fee in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch fee, or <code>null</code> if a matching batch fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchFee fetchByBatch_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByBatch_Last(batchId, orderByComparator);
	}

	/**
	* Returns the batch fees before and after the current batch fee in the ordered set where batchId = &#63;.
	*
	* @param batchFeeId the primary key of the current batch fee
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch fee
	* @throws info.diit.portal.NoSuchBatchFeeException if a batch fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.BatchFee[] findByBatch_PrevAndNext(
		long batchFeeId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchFeeException {
		return getPersistence()
				   .findByBatch_PrevAndNext(batchFeeId, batchId,
			orderByComparator);
	}

	/**
	* Returns all the batch fees.
	*
	* @return the batch fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchFee> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the batch fees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch fees
	* @param end the upper bound of the range of batch fees (not inclusive)
	* @return the range of batch fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchFee> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the batch fees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch fees
	* @param end the upper bound of the range of batch fees (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of batch fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.BatchFee> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the batch fees where batchId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBatch(batchId);
	}

	/**
	* Removes all the batch fees from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of batch fees where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the number of matching batch fees
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBatch(batchId);
	}

	/**
	* Returns the number of batch fees.
	*
	* @return the number of batch fees
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static BatchFeePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (BatchFeePersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					BatchFeePersistence.class.getName());

			ReferenceRegistry.registerReference(BatchFeeUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(BatchFeePersistence persistence) {
	}

	private static BatchFeePersistence _persistence;
}
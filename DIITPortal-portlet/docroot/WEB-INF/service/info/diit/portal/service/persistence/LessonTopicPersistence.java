/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.LessonTopic;

/**
 * The persistence interface for the lesson topic service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see LessonTopicPersistenceImpl
 * @see LessonTopicUtil
 * @generated
 */
public interface LessonTopicPersistence extends BasePersistence<LessonTopic> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LessonTopicUtil} to access the lesson topic persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the lesson topic in the entity cache if it is enabled.
	*
	* @param lessonTopic the lesson topic
	*/
	public void cacheResult(info.diit.portal.model.LessonTopic lessonTopic);

	/**
	* Caches the lesson topics in the entity cache if it is enabled.
	*
	* @param lessonTopics the lesson topics
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.LessonTopic> lessonTopics);

	/**
	* Creates a new lesson topic with the primary key. Does not add the lesson topic to the database.
	*
	* @param lessonTopicId the primary key for the new lesson topic
	* @return the new lesson topic
	*/
	public info.diit.portal.model.LessonTopic create(long lessonTopicId);

	/**
	* Removes the lesson topic with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonTopicId the primary key of the lesson topic
	* @return the lesson topic that was removed
	* @throws info.diit.portal.NoSuchLessonTopicException if a lesson topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic remove(long lessonTopicId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonTopicException;

	public info.diit.portal.model.LessonTopic updateImpl(
		info.diit.portal.model.LessonTopic lessonTopic, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lesson topic with the primary key or throws a {@link info.diit.portal.NoSuchLessonTopicException} if it could not be found.
	*
	* @param lessonTopicId the primary key of the lesson topic
	* @return the lesson topic
	* @throws info.diit.portal.NoSuchLessonTopicException if a lesson topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic findByPrimaryKey(
		long lessonTopicId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonTopicException;

	/**
	* Returns the lesson topic with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param lessonTopicId the primary key of the lesson topic
	* @return the lesson topic, or <code>null</code> if a lesson topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic fetchByPrimaryKey(
		long lessonTopicId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the lesson topics where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @return the matching lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonTopic> findByLessonPlan(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lesson topics where lessonPlanId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param lessonPlanId the lesson plan ID
	* @param start the lower bound of the range of lesson topics
	* @param end the upper bound of the range of lesson topics (not inclusive)
	* @return the range of matching lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonTopic> findByLessonPlan(
		long lessonPlanId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lesson topics where lessonPlanId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param lessonPlanId the lesson plan ID
	* @param start the lower bound of the range of lesson topics
	* @param end the upper bound of the range of lesson topics (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonTopic> findByLessonPlan(
		long lessonPlanId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first lesson topic in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson topic
	* @throws info.diit.portal.NoSuchLessonTopicException if a matching lesson topic could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic findByLessonPlan_First(
		long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonTopicException;

	/**
	* Returns the first lesson topic in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson topic, or <code>null</code> if a matching lesson topic could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic fetchByLessonPlan_First(
		long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last lesson topic in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson topic
	* @throws info.diit.portal.NoSuchLessonTopicException if a matching lesson topic could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic findByLessonPlan_Last(
		long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonTopicException;

	/**
	* Returns the last lesson topic in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson topic, or <code>null</code> if a matching lesson topic could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic fetchByLessonPlan_Last(
		long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lesson topics before and after the current lesson topic in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonTopicId the primary key of the current lesson topic
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lesson topic
	* @throws info.diit.portal.NoSuchLessonTopicException if a lesson topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic[] findByLessonPlan_PrevAndNext(
		long lessonTopicId, long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonTopicException;

	/**
	* Returns all the lesson topics where lessonPlanId = &#63; and topic = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param topic the topic
	* @return the matching lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonTopic> findByLessonTopic(
		long lessonPlanId, long topic)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lesson topics where lessonPlanId = &#63; and topic = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param lessonPlanId the lesson plan ID
	* @param topic the topic
	* @param start the lower bound of the range of lesson topics
	* @param end the upper bound of the range of lesson topics (not inclusive)
	* @return the range of matching lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonTopic> findByLessonTopic(
		long lessonPlanId, long topic, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lesson topics where lessonPlanId = &#63; and topic = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param lessonPlanId the lesson plan ID
	* @param topic the topic
	* @param start the lower bound of the range of lesson topics
	* @param end the upper bound of the range of lesson topics (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonTopic> findByLessonTopic(
		long lessonPlanId, long topic, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first lesson topic in the ordered set where lessonPlanId = &#63; and topic = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param topic the topic
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson topic
	* @throws info.diit.portal.NoSuchLessonTopicException if a matching lesson topic could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic findByLessonTopic_First(
		long lessonPlanId, long topic,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonTopicException;

	/**
	* Returns the first lesson topic in the ordered set where lessonPlanId = &#63; and topic = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param topic the topic
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson topic, or <code>null</code> if a matching lesson topic could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic fetchByLessonTopic_First(
		long lessonPlanId, long topic,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last lesson topic in the ordered set where lessonPlanId = &#63; and topic = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param topic the topic
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson topic
	* @throws info.diit.portal.NoSuchLessonTopicException if a matching lesson topic could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic findByLessonTopic_Last(
		long lessonPlanId, long topic,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonTopicException;

	/**
	* Returns the last lesson topic in the ordered set where lessonPlanId = &#63; and topic = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param topic the topic
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson topic, or <code>null</code> if a matching lesson topic could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic fetchByLessonTopic_Last(
		long lessonPlanId, long topic,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lesson topics before and after the current lesson topic in the ordered set where lessonPlanId = &#63; and topic = &#63;.
	*
	* @param lessonTopicId the primary key of the current lesson topic
	* @param lessonPlanId the lesson plan ID
	* @param topic the topic
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lesson topic
	* @throws info.diit.portal.NoSuchLessonTopicException if a lesson topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic[] findByLessonTopic_PrevAndNext(
		long lessonTopicId, long lessonPlanId, long topic,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonTopicException;

	/**
	* Returns all the lesson topics.
	*
	* @return the lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonTopic> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lesson topics.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson topics
	* @param end the upper bound of the range of lesson topics (not inclusive)
	* @return the range of lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonTopic> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lesson topics.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson topics
	* @param end the upper bound of the range of lesson topics (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonTopic> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the lesson topics where lessonPlanId = &#63; from the database.
	*
	* @param lessonPlanId the lesson plan ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByLessonPlan(long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the lesson topics where lessonPlanId = &#63; and topic = &#63; from the database.
	*
	* @param lessonPlanId the lesson plan ID
	* @param topic the topic
	* @throws SystemException if a system exception occurred
	*/
	public void removeByLessonTopic(long lessonPlanId, long topic)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the lesson topics from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lesson topics where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @return the number of matching lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public int countByLessonPlan(long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lesson topics where lessonPlanId = &#63; and topic = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param topic the topic
	* @return the number of matching lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public int countByLessonTopic(long lessonPlanId, long topic)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lesson topics.
	*
	* @return the number of lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
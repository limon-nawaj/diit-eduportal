/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.CounselingCourseInterest;

/**
 * The persistence interface for the counseling course interest service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see CounselingCourseInterestPersistenceImpl
 * @see CounselingCourseInterestUtil
 * @generated
 */
public interface CounselingCourseInterestPersistence extends BasePersistence<CounselingCourseInterest> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CounselingCourseInterestUtil} to access the counseling course interest persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the counseling course interest in the entity cache if it is enabled.
	*
	* @param counselingCourseInterest the counseling course interest
	*/
	public void cacheResult(
		info.diit.portal.model.CounselingCourseInterest counselingCourseInterest);

	/**
	* Caches the counseling course interests in the entity cache if it is enabled.
	*
	* @param counselingCourseInterests the counseling course interests
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.CounselingCourseInterest> counselingCourseInterests);

	/**
	* Creates a new counseling course interest with the primary key. Does not add the counseling course interest to the database.
	*
	* @param CounselingCourseInterestId the primary key for the new counseling course interest
	* @return the new counseling course interest
	*/
	public info.diit.portal.model.CounselingCourseInterest create(
		long CounselingCourseInterestId);

	/**
	* Removes the counseling course interest with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param CounselingCourseInterestId the primary key of the counseling course interest
	* @return the counseling course interest that was removed
	* @throws info.diit.portal.NoSuchCounselingCourseInterestException if a counseling course interest with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CounselingCourseInterest remove(
		long CounselingCourseInterestId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCounselingCourseInterestException;

	public info.diit.portal.model.CounselingCourseInterest updateImpl(
		info.diit.portal.model.CounselingCourseInterest counselingCourseInterest,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the counseling course interest with the primary key or throws a {@link info.diit.portal.NoSuchCounselingCourseInterestException} if it could not be found.
	*
	* @param CounselingCourseInterestId the primary key of the counseling course interest
	* @return the counseling course interest
	* @throws info.diit.portal.NoSuchCounselingCourseInterestException if a counseling course interest with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CounselingCourseInterest findByPrimaryKey(
		long CounselingCourseInterestId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCounselingCourseInterestException;

	/**
	* Returns the counseling course interest with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param CounselingCourseInterestId the primary key of the counseling course interest
	* @return the counseling course interest, or <code>null</code> if a counseling course interest with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CounselingCourseInterest fetchByPrimaryKey(
		long CounselingCourseInterestId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the counseling course interests.
	*
	* @return the counseling course interests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CounselingCourseInterest> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the counseling course interests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of counseling course interests
	* @param end the upper bound of the range of counseling course interests (not inclusive)
	* @return the range of counseling course interests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CounselingCourseInterest> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the counseling course interests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of counseling course interests
	* @param end the upper bound of the range of counseling course interests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of counseling course interests
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CounselingCourseInterest> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the counseling course interests from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of counseling course interests.
	*
	* @return the number of counseling course interests
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
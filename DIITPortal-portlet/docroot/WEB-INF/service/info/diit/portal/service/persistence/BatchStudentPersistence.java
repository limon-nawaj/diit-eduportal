/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.BatchStudent;

/**
 * The persistence interface for the batch student service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see BatchStudentPersistenceImpl
 * @see BatchStudentUtil
 * @generated
 */
public interface BatchStudentPersistence extends BasePersistence<BatchStudent> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BatchStudentUtil} to access the batch student persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the batch student in the entity cache if it is enabled.
	*
	* @param batchStudent the batch student
	*/
	public void cacheResult(info.diit.portal.model.BatchStudent batchStudent);

	/**
	* Caches the batch students in the entity cache if it is enabled.
	*
	* @param batchStudents the batch students
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.BatchStudent> batchStudents);

	/**
	* Creates a new batch student with the primary key. Does not add the batch student to the database.
	*
	* @param batchStudentId the primary key for the new batch student
	* @return the new batch student
	*/
	public info.diit.portal.model.BatchStudent create(long batchStudentId);

	/**
	* Removes the batch student with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchStudentId the primary key of the batch student
	* @return the batch student that was removed
	* @throws info.diit.portal.NoSuchBatchStudentException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent remove(long batchStudentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	public info.diit.portal.model.BatchStudent updateImpl(
		info.diit.portal.model.BatchStudent batchStudent, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch student with the primary key or throws a {@link info.diit.portal.NoSuchBatchStudentException} if it could not be found.
	*
	* @param batchStudentId the primary key of the batch student
	* @return the batch student
	* @throws info.diit.portal.NoSuchBatchStudentException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent findByPrimaryKey(
		long batchStudentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Returns the batch student with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param batchStudentId the primary key of the batch student
	* @return the batch student, or <code>null</code> if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent fetchByPrimaryKey(
		long batchStudentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the batch students where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findByBatch(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch students where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @return the range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findByBatch(
		long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch students where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findByBatch(
		long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch student in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student
	* @throws info.diit.portal.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent findByBatch_First(long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Returns the first batch student in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent fetchByBatch_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch student in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student
	* @throws info.diit.portal.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent findByBatch_Last(long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Returns the last batch student in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent fetchByBatch_Last(long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch students before and after the current batch student in the ordered set where batchId = &#63;.
	*
	* @param batchStudentId the primary key of the current batch student
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch student
	* @throws info.diit.portal.NoSuchBatchStudentException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent[] findByBatch_PrevAndNext(
		long batchStudentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Returns all the batch students where companyId = &#63; and organizationId = &#63;.
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @return the matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findByCompanyOrgnization(
		long companyId, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch students where companyId = &#63; and organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @return the range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findByCompanyOrgnization(
		long companyId, long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch students where companyId = &#63; and organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findByCompanyOrgnization(
		long companyId, long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student
	* @throws info.diit.portal.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent findByCompanyOrgnization_First(
		long companyId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Returns the first batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent fetchByCompanyOrgnization_First(
		long companyId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student
	* @throws info.diit.portal.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent findByCompanyOrgnization_Last(
		long companyId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Returns the last batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent fetchByCompanyOrgnization_Last(
		long companyId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch students before and after the current batch student in the ordered set where companyId = &#63; and organizationId = &#63;.
	*
	* @param batchStudentId the primary key of the current batch student
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch student
	* @throws info.diit.portal.NoSuchBatchStudentException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent[] findByCompanyOrgnization_PrevAndNext(
		long batchStudentId, long companyId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Returns all the batch students where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findByStudentBatchStudentList(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch students where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @return the range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findByStudentBatchStudentList(
		long studentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch students where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findByStudentBatchStudentList(
		long studentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch student in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student
	* @throws info.diit.portal.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent findByStudentBatchStudentList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Returns the first batch student in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent fetchByStudentBatchStudentList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch student in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student
	* @throws info.diit.portal.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent findByStudentBatchStudentList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Returns the last batch student in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent fetchByStudentBatchStudentList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch students before and after the current batch student in the ordered set where studentId = &#63;.
	*
	* @param batchStudentId the primary key of the current batch student
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch student
	* @throws info.diit.portal.NoSuchBatchStudentException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent[] findByStudentBatchStudentList_PrevAndNext(
		long batchStudentId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Returns all the batch students where organizationId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @return the matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findByAllStudentsByBatchId(
		long organizationId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch students where organizationId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @return the range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findByAllStudentsByBatchId(
		long organizationId, long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch students where organizationId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findByAllStudentsByBatchId(
		long organizationId, long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student
	* @throws info.diit.portal.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent findByAllStudentsByBatchId_First(
		long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Returns the first batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent fetchByAllStudentsByBatchId_First(
		long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student
	* @throws info.diit.portal.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent findByAllStudentsByBatchId_Last(
		long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Returns the last batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent fetchByAllStudentsByBatchId_Last(
		long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch students before and after the current batch student in the ordered set where organizationId = &#63; and batchId = &#63;.
	*
	* @param batchStudentId the primary key of the current batch student
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch student
	* @throws info.diit.portal.NoSuchBatchStudentException if a batch student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent[] findByAllStudentsByBatchId_PrevAndNext(
		long batchStudentId, long organizationId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Returns the batch student where organizationId = &#63; and batchId = &#63; and studentId = &#63; or throws a {@link info.diit.portal.NoSuchBatchStudentException} if it could not be found.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param studentId the student ID
	* @return the matching batch student
	* @throws info.diit.portal.NoSuchBatchStudentException if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent findByOrgBatchStudent(
		long organizationId, long batchId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Returns the batch student where organizationId = &#63; and batchId = &#63; and studentId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param studentId the student ID
	* @return the matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent fetchByOrgBatchStudent(
		long organizationId, long batchId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch student where organizationId = &#63; and batchId = &#63; and studentId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param studentId the student ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching batch student, or <code>null</code> if a matching batch student could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent fetchByOrgBatchStudent(
		long organizationId, long batchId, long studentId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the batch students.
	*
	* @return the batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch students.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @return the range of batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch students.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch students
	* @param end the upper bound of the range of batch students (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of batch students
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchStudent> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch students where batchId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch students where companyId = &#63; and organizationId = &#63; from the database.
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCompanyOrgnization(long companyId, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch students where studentId = &#63; from the database.
	*
	* @param studentId the student ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByStudentBatchStudentList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch students where organizationId = &#63; and batchId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByAllStudentsByBatchId(long organizationId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the batch student where organizationId = &#63; and batchId = &#63; and studentId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param studentId the student ID
	* @return the batch student that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchStudent removeByOrgBatchStudent(
		long organizationId, long batchId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchStudentException;

	/**
	* Removes all the batch students from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch students where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the number of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public int countByBatch(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch students where companyId = &#63; and organizationId = &#63;.
	*
	* @param companyId the company ID
	* @param organizationId the organization ID
	* @return the number of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public int countByCompanyOrgnization(long companyId, long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch students where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the number of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public int countByStudentBatchStudentList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch students where organizationId = &#63; and batchId = &#63;.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @return the number of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public int countByAllStudentsByBatchId(long organizationId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch students where organizationId = &#63; and batchId = &#63; and studentId = &#63;.
	*
	* @param organizationId the organization ID
	* @param batchId the batch ID
	* @param studentId the student ID
	* @return the number of matching batch students
	* @throws SystemException if a system exception occurred
	*/
	public int countByOrgBatchStudent(long organizationId, long batchId,
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch students.
	*
	* @return the number of batch students
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
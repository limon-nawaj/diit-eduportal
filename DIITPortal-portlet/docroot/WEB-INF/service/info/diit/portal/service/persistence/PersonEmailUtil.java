/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.PersonEmail;

import java.util.List;

/**
 * The persistence utility for the person email service. This utility wraps {@link PersonEmailPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see PersonEmailPersistence
 * @see PersonEmailPersistenceImpl
 * @generated
 */
public class PersonEmailUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(PersonEmail personEmail) {
		getPersistence().clearCache(personEmail);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<PersonEmail> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<PersonEmail> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<PersonEmail> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static PersonEmail update(PersonEmail personEmail, boolean merge)
		throws SystemException {
		return getPersistence().update(personEmail, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static PersonEmail update(PersonEmail personEmail, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(personEmail, merge, serviceContext);
	}

	/**
	* Caches the person email in the entity cache if it is enabled.
	*
	* @param personEmail the person email
	*/
	public static void cacheResult(
		info.diit.portal.model.PersonEmail personEmail) {
		getPersistence().cacheResult(personEmail);
	}

	/**
	* Caches the person emails in the entity cache if it is enabled.
	*
	* @param personEmails the person emails
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.PersonEmail> personEmails) {
		getPersistence().cacheResult(personEmails);
	}

	/**
	* Creates a new person email with the primary key. Does not add the person email to the database.
	*
	* @param personEmailId the primary key for the new person email
	* @return the new person email
	*/
	public static info.diit.portal.model.PersonEmail create(long personEmailId) {
		return getPersistence().create(personEmailId);
	}

	/**
	* Removes the person email with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param personEmailId the primary key of the person email
	* @return the person email that was removed
	* @throws info.diit.portal.NoSuchPersonEmailException if a person email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PersonEmail remove(long personEmailId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPersonEmailException {
		return getPersistence().remove(personEmailId);
	}

	public static info.diit.portal.model.PersonEmail updateImpl(
		info.diit.portal.model.PersonEmail personEmail, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(personEmail, merge);
	}

	/**
	* Returns the person email with the primary key or throws a {@link info.diit.portal.NoSuchPersonEmailException} if it could not be found.
	*
	* @param personEmailId the primary key of the person email
	* @return the person email
	* @throws info.diit.portal.NoSuchPersonEmailException if a person email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PersonEmail findByPrimaryKey(
		long personEmailId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPersonEmailException {
		return getPersistence().findByPrimaryKey(personEmailId);
	}

	/**
	* Returns the person email with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param personEmailId the primary key of the person email
	* @return the person email, or <code>null</code> if a person email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PersonEmail fetchByPrimaryKey(
		long personEmailId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(personEmailId);
	}

	/**
	* Returns all the person emails where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the matching person emails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.PersonEmail> findByStudentPersonEmailList(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStudentPersonEmailList(studentId);
	}

	/**
	* Returns a range of all the person emails where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of person emails
	* @param end the upper bound of the range of person emails (not inclusive)
	* @return the range of matching person emails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.PersonEmail> findByStudentPersonEmailList(
		long studentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentPersonEmailList(studentId, start, end);
	}

	/**
	* Returns an ordered range of all the person emails where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of person emails
	* @param end the upper bound of the range of person emails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching person emails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.PersonEmail> findByStudentPersonEmailList(
		long studentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentPersonEmailList(studentId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first person email in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching person email
	* @throws info.diit.portal.NoSuchPersonEmailException if a matching person email could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PersonEmail findByStudentPersonEmailList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPersonEmailException {
		return getPersistence()
				   .findByStudentPersonEmailList_First(studentId,
			orderByComparator);
	}

	/**
	* Returns the first person email in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching person email, or <code>null</code> if a matching person email could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PersonEmail fetchByStudentPersonEmailList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentPersonEmailList_First(studentId,
			orderByComparator);
	}

	/**
	* Returns the last person email in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching person email
	* @throws info.diit.portal.NoSuchPersonEmailException if a matching person email could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PersonEmail findByStudentPersonEmailList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPersonEmailException {
		return getPersistence()
				   .findByStudentPersonEmailList_Last(studentId,
			orderByComparator);
	}

	/**
	* Returns the last person email in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching person email, or <code>null</code> if a matching person email could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PersonEmail fetchByStudentPersonEmailList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentPersonEmailList_Last(studentId,
			orderByComparator);
	}

	/**
	* Returns the person emails before and after the current person email in the ordered set where studentId = &#63;.
	*
	* @param personEmailId the primary key of the current person email
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next person email
	* @throws info.diit.portal.NoSuchPersonEmailException if a person email with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PersonEmail[] findByStudentPersonEmailList_PrevAndNext(
		long personEmailId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPersonEmailException {
		return getPersistence()
				   .findByStudentPersonEmailList_PrevAndNext(personEmailId,
			studentId, orderByComparator);
	}

	/**
	* Returns all the person emails.
	*
	* @return the person emails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.PersonEmail> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the person emails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of person emails
	* @param end the upper bound of the range of person emails (not inclusive)
	* @return the range of person emails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.PersonEmail> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the person emails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of person emails
	* @param end the upper bound of the range of person emails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of person emails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.PersonEmail> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the person emails where studentId = &#63; from the database.
	*
	* @param studentId the student ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByStudentPersonEmailList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByStudentPersonEmailList(studentId);
	}

	/**
	* Removes all the person emails from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of person emails where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the number of matching person emails
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStudentPersonEmailList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStudentPersonEmailList(studentId);
	}

	/**
	* Returns the number of person emails.
	*
	* @return the number of person emails
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static PersonEmailPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (PersonEmailPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					PersonEmailPersistence.class.getName());

			ReferenceRegistry.registerReference(PersonEmailUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(PersonEmailPersistence persistence) {
	}

	private static PersonEmailPersistence _persistence;
}
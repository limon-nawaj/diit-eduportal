/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.messaging;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

import info.diit.portal.service.AcademicRecordLocalServiceUtil;
import info.diit.portal.service.AssessmentLocalServiceUtil;
import info.diit.portal.service.AssessmentStudentLocalServiceUtil;
import info.diit.portal.service.AssessmentTypeLocalServiceUtil;
import info.diit.portal.service.AttendanceLocalServiceUtil;
import info.diit.portal.service.AttendanceTopicLocalServiceUtil;
import info.diit.portal.service.BatchFeeLocalServiceUtil;
import info.diit.portal.service.BatchLocalServiceUtil;
import info.diit.portal.service.BatchPaymentScheduleLocalServiceUtil;
import info.diit.portal.service.BatchStudentLocalServiceUtil;
import info.diit.portal.service.BatchSubjectLocalServiceUtil;
import info.diit.portal.service.BatchSubjectServiceUtil;
import info.diit.portal.service.BatchTeacherLocalServiceUtil;
import info.diit.portal.service.ChapterLocalServiceUtil;
import info.diit.portal.service.ClassRoutineEventBatchLocalServiceUtil;
import info.diit.portal.service.ClassRoutineEventLocalServiceUtil;
import info.diit.portal.service.ClpSerializer;
import info.diit.portal.service.CommentsLocalServiceUtil;
import info.diit.portal.service.CommentsServiceUtil;
import info.diit.portal.service.CommunicationRecordLocalServiceUtil;
import info.diit.portal.service.CommunicationStudentRecordLocalServiceUtil;
import info.diit.portal.service.CounselingCourseInterestLocalServiceUtil;
import info.diit.portal.service.CounselingCourseInterestServiceUtil;
import info.diit.portal.service.CounselingLocalServiceUtil;
import info.diit.portal.service.CourseFeeLocalServiceUtil;
import info.diit.portal.service.CourseLocalServiceUtil;
import info.diit.portal.service.CourseOrganizationLocalServiceUtil;
import info.diit.portal.service.CourseSessionLocalServiceUtil;
import info.diit.portal.service.CourseSubjectLocalServiceUtil;
import info.diit.portal.service.DailyWorkLocalServiceUtil;
import info.diit.portal.service.DesignationLocalServiceUtil;
import info.diit.portal.service.EmployeeAttendanceLocalServiceUtil;
import info.diit.portal.service.EmployeeEmailLocalServiceUtil;
import info.diit.portal.service.EmployeeLocalServiceUtil;
import info.diit.portal.service.EmployeeMobileLocalServiceUtil;
import info.diit.portal.service.EmployeeRoleLocalServiceUtil;
import info.diit.portal.service.EmployeeTimeScheduleLocalServiceUtil;
import info.diit.portal.service.ExperianceLocalServiceUtil;
import info.diit.portal.service.FeeTypeLocalServiceUtil;
import info.diit.portal.service.LeaveCategoryLocalServiceUtil;
import info.diit.portal.service.LeaveDayDetailsLocalServiceUtil;
import info.diit.portal.service.LeaveLocalServiceUtil;
import info.diit.portal.service.LeaveReceiverLocalServiceUtil;
import info.diit.portal.service.LessonAssessmentLocalServiceUtil;
import info.diit.portal.service.LessonPlanLocalServiceUtil;
import info.diit.portal.service.LessonTopicLocalServiceUtil;
import info.diit.portal.service.PaymentLocalServiceUtil;
import info.diit.portal.service.PersonEmailLocalServiceUtil;
import info.diit.portal.service.PhoneNumberLocalServiceUtil;
import info.diit.portal.service.RoomLocalServiceUtil;
import info.diit.portal.service.StatusHistoryLocalServiceUtil;
import info.diit.portal.service.StudentAttendanceLocalServiceUtil;
import info.diit.portal.service.StudentDiscountLocalServiceUtil;
import info.diit.portal.service.StudentDocumentLocalServiceUtil;
import info.diit.portal.service.StudentFeeLocalServiceUtil;
import info.diit.portal.service.StudentLocalServiceUtil;
import info.diit.portal.service.StudentPaymentScheduleLocalServiceUtil;
import info.diit.portal.service.SubjectLessonLocalServiceUtil;
import info.diit.portal.service.SubjectLocalServiceUtil;
import info.diit.portal.service.TaskDesignationLocalServiceUtil;
import info.diit.portal.service.TaskLocalServiceUtil;
import info.diit.portal.service.TopicLocalServiceUtil;

/**
 * @author Brian Wing Shun Chan
 */
public class ClpMessageListener extends BaseMessageListener {
	public static String getServletContextName() {
		return ClpSerializer.getServletContextName();
	}

	@Override
	protected void doReceive(Message message) throws Exception {
		String command = message.getString("command");
		String servletContextName = message.getString("servletContextName");

		if (command.equals("undeploy") &&
				servletContextName.equals(getServletContextName())) {
			AcademicRecordLocalServiceUtil.clearService();

			AssessmentLocalServiceUtil.clearService();

			AssessmentStudentLocalServiceUtil.clearService();

			AssessmentTypeLocalServiceUtil.clearService();

			AttendanceLocalServiceUtil.clearService();

			AttendanceTopicLocalServiceUtil.clearService();

			BatchLocalServiceUtil.clearService();

			BatchFeeLocalServiceUtil.clearService();

			BatchPaymentScheduleLocalServiceUtil.clearService();

			BatchStudentLocalServiceUtil.clearService();

			BatchSubjectLocalServiceUtil.clearService();

			BatchSubjectServiceUtil.clearService();
			BatchTeacherLocalServiceUtil.clearService();

			ChapterLocalServiceUtil.clearService();

			ClassRoutineEventLocalServiceUtil.clearService();

			ClassRoutineEventBatchLocalServiceUtil.clearService();

			CommentsLocalServiceUtil.clearService();

			CommentsServiceUtil.clearService();
			CommunicationRecordLocalServiceUtil.clearService();

			CommunicationStudentRecordLocalServiceUtil.clearService();

			CounselingLocalServiceUtil.clearService();

			CounselingCourseInterestLocalServiceUtil.clearService();

			CounselingCourseInterestServiceUtil.clearService();
			CourseLocalServiceUtil.clearService();

			CourseFeeLocalServiceUtil.clearService();

			CourseOrganizationLocalServiceUtil.clearService();

			CourseSessionLocalServiceUtil.clearService();

			CourseSubjectLocalServiceUtil.clearService();

			DailyWorkLocalServiceUtil.clearService();

			DesignationLocalServiceUtil.clearService();

			EmployeeLocalServiceUtil.clearService();

			EmployeeAttendanceLocalServiceUtil.clearService();

			EmployeeEmailLocalServiceUtil.clearService();

			EmployeeMobileLocalServiceUtil.clearService();

			EmployeeRoleLocalServiceUtil.clearService();

			EmployeeTimeScheduleLocalServiceUtil.clearService();

			ExperianceLocalServiceUtil.clearService();

			FeeTypeLocalServiceUtil.clearService();

			LeaveLocalServiceUtil.clearService();

			LeaveCategoryLocalServiceUtil.clearService();

			LeaveDayDetailsLocalServiceUtil.clearService();

			LeaveReceiverLocalServiceUtil.clearService();

			LessonAssessmentLocalServiceUtil.clearService();

			LessonPlanLocalServiceUtil.clearService();

			LessonTopicLocalServiceUtil.clearService();

			PaymentLocalServiceUtil.clearService();

			PersonEmailLocalServiceUtil.clearService();

			PhoneNumberLocalServiceUtil.clearService();

			RoomLocalServiceUtil.clearService();

			StatusHistoryLocalServiceUtil.clearService();

			StudentLocalServiceUtil.clearService();

			StudentAttendanceLocalServiceUtil.clearService();

			StudentDiscountLocalServiceUtil.clearService();

			StudentDocumentLocalServiceUtil.clearService();

			StudentFeeLocalServiceUtil.clearService();

			StudentPaymentScheduleLocalServiceUtil.clearService();

			SubjectLocalServiceUtil.clearService();

			SubjectLessonLocalServiceUtil.clearService();

			TaskLocalServiceUtil.clearService();

			TaskDesignationLocalServiceUtil.clearService();

			TopicLocalServiceUtil.clearService();
		}
	}
}
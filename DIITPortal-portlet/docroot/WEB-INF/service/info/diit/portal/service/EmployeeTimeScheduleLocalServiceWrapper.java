/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link EmployeeTimeScheduleLocalService}.
 * </p>
 *
 * @author    mohammad
 * @see       EmployeeTimeScheduleLocalService
 * @generated
 */
public class EmployeeTimeScheduleLocalServiceWrapper
	implements EmployeeTimeScheduleLocalService,
		ServiceWrapper<EmployeeTimeScheduleLocalService> {
	public EmployeeTimeScheduleLocalServiceWrapper(
		EmployeeTimeScheduleLocalService employeeTimeScheduleLocalService) {
		_employeeTimeScheduleLocalService = employeeTimeScheduleLocalService;
	}

	/**
	* Adds the employee time schedule to the database. Also notifies the appropriate model listeners.
	*
	* @param employeeTimeSchedule the employee time schedule
	* @return the employee time schedule that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule addEmployeeTimeSchedule(
		info.diit.portal.model.EmployeeTimeSchedule employeeTimeSchedule)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.addEmployeeTimeSchedule(employeeTimeSchedule);
	}

	/**
	* Creates a new employee time schedule with the primary key. Does not add the employee time schedule to the database.
	*
	* @param employeeTimeScheduleId the primary key for the new employee time schedule
	* @return the new employee time schedule
	*/
	public info.diit.portal.model.EmployeeTimeSchedule createEmployeeTimeSchedule(
		long employeeTimeScheduleId) {
		return _employeeTimeScheduleLocalService.createEmployeeTimeSchedule(employeeTimeScheduleId);
	}

	/**
	* Deletes the employee time schedule with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param employeeTimeScheduleId the primary key of the employee time schedule
	* @return the employee time schedule that was removed
	* @throws PortalException if a employee time schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule deleteEmployeeTimeSchedule(
		long employeeTimeScheduleId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.deleteEmployeeTimeSchedule(employeeTimeScheduleId);
	}

	/**
	* Deletes the employee time schedule from the database. Also notifies the appropriate model listeners.
	*
	* @param employeeTimeSchedule the employee time schedule
	* @return the employee time schedule that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule deleteEmployeeTimeSchedule(
		info.diit.portal.model.EmployeeTimeSchedule employeeTimeSchedule)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.deleteEmployeeTimeSchedule(employeeTimeSchedule);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _employeeTimeScheduleLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.EmployeeTimeSchedule fetchEmployeeTimeSchedule(
		long employeeTimeScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.fetchEmployeeTimeSchedule(employeeTimeScheduleId);
	}

	/**
	* Returns the employee time schedule with the primary key.
	*
	* @param employeeTimeScheduleId the primary key of the employee time schedule
	* @return the employee time schedule
	* @throws PortalException if a employee time schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule getEmployeeTimeSchedule(
		long employeeTimeScheduleId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.getEmployeeTimeSchedule(employeeTimeScheduleId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the employee time schedules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee time schedules
	* @param end the upper bound of the range of employee time schedules (not inclusive)
	* @return the range of employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeTimeSchedule> getEmployeeTimeSchedules(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.getEmployeeTimeSchedules(start,
			end);
	}

	/**
	* Returns the number of employee time schedules.
	*
	* @return the number of employee time schedules
	* @throws SystemException if a system exception occurred
	*/
	public int getEmployeeTimeSchedulesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.getEmployeeTimeSchedulesCount();
	}

	/**
	* Updates the employee time schedule in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param employeeTimeSchedule the employee time schedule
	* @return the employee time schedule that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule updateEmployeeTimeSchedule(
		info.diit.portal.model.EmployeeTimeSchedule employeeTimeSchedule)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.updateEmployeeTimeSchedule(employeeTimeSchedule);
	}

	/**
	* Updates the employee time schedule in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param employeeTimeSchedule the employee time schedule
	* @param merge whether to merge the employee time schedule with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the employee time schedule that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeTimeSchedule updateEmployeeTimeSchedule(
		info.diit.portal.model.EmployeeTimeSchedule employeeTimeSchedule,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.updateEmployeeTimeSchedule(employeeTimeSchedule,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _employeeTimeScheduleLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_employeeTimeScheduleLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _employeeTimeScheduleLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	public java.util.List<info.diit.portal.model.EmployeeTimeSchedule> findByEmployee(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeTimeScheduleLocalService.findByEmployee(employeeId);
	}

	public info.diit.portal.model.EmployeeTimeSchedule findByEmployeeDay(
		long employeeId, int day)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchEmployeeTimeScheduleException {
		return _employeeTimeScheduleLocalService.findByEmployeeDay(employeeId,
			day);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public EmployeeTimeScheduleLocalService getWrappedEmployeeTimeScheduleLocalService() {
		return _employeeTimeScheduleLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedEmployeeTimeScheduleLocalService(
		EmployeeTimeScheduleLocalService employeeTimeScheduleLocalService) {
		_employeeTimeScheduleLocalService = employeeTimeScheduleLocalService;
	}

	public EmployeeTimeScheduleLocalService getWrappedService() {
		return _employeeTimeScheduleLocalService;
	}

	public void setWrappedService(
		EmployeeTimeScheduleLocalService employeeTimeScheduleLocalService) {
		_employeeTimeScheduleLocalService = employeeTimeScheduleLocalService;
	}

	private EmployeeTimeScheduleLocalService _employeeTimeScheduleLocalService;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the lesson topic local service. This utility wraps {@link info.diit.portal.service.impl.LessonTopicLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see LessonTopicLocalService
 * @see info.diit.portal.service.base.LessonTopicLocalServiceBaseImpl
 * @see info.diit.portal.service.impl.LessonTopicLocalServiceImpl
 * @generated
 */
public class LessonTopicLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.service.impl.LessonTopicLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the lesson topic to the database. Also notifies the appropriate model listeners.
	*
	* @param lessonTopic the lesson topic
	* @return the lesson topic that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonTopic addLessonTopic(
		info.diit.portal.model.LessonTopic lessonTopic)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addLessonTopic(lessonTopic);
	}

	/**
	* Creates a new lesson topic with the primary key. Does not add the lesson topic to the database.
	*
	* @param lessonTopicId the primary key for the new lesson topic
	* @return the new lesson topic
	*/
	public static info.diit.portal.model.LessonTopic createLessonTopic(
		long lessonTopicId) {
		return getService().createLessonTopic(lessonTopicId);
	}

	/**
	* Deletes the lesson topic with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonTopicId the primary key of the lesson topic
	* @return the lesson topic that was removed
	* @throws PortalException if a lesson topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonTopic deleteLessonTopic(
		long lessonTopicId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteLessonTopic(lessonTopicId);
	}

	/**
	* Deletes the lesson topic from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonTopic the lesson topic
	* @return the lesson topic that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonTopic deleteLessonTopic(
		info.diit.portal.model.LessonTopic lessonTopic)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteLessonTopic(lessonTopic);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.model.LessonTopic fetchLessonTopic(
		long lessonTopicId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchLessonTopic(lessonTopicId);
	}

	/**
	* Returns the lesson topic with the primary key.
	*
	* @param lessonTopicId the primary key of the lesson topic
	* @return the lesson topic
	* @throws PortalException if a lesson topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonTopic getLessonTopic(
		long lessonTopicId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getLessonTopic(lessonTopicId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the lesson topics.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson topics
	* @param end the upper bound of the range of lesson topics (not inclusive)
	* @return the range of lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonTopic> getLessonTopics(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLessonTopics(start, end);
	}

	/**
	* Returns the number of lesson topics.
	*
	* @return the number of lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public static int getLessonTopicsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getLessonTopicsCount();
	}

	/**
	* Updates the lesson topic in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lessonTopic the lesson topic
	* @return the lesson topic that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonTopic updateLessonTopic(
		info.diit.portal.model.LessonTopic lessonTopic)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateLessonTopic(lessonTopic);
	}

	/**
	* Updates the lesson topic in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lessonTopic the lesson topic
	* @param merge whether to merge the lesson topic with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the lesson topic that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonTopic updateLessonTopic(
		info.diit.portal.model.LessonTopic lessonTopic, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateLessonTopic(lessonTopic, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<info.diit.portal.model.LessonTopic> findByLessonPlan(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByLessonPlan(lessonPlanId);
	}

	public static java.util.List<info.diit.portal.model.LessonTopic> findByLessonTopic(
		long lessonPlanId, long topicId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByLessonTopic(lessonPlanId, topicId);
	}

	public static void clearService() {
		_service = null;
	}

	public static LessonTopicLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					LessonTopicLocalService.class.getName());

			if (invokableLocalService instanceof LessonTopicLocalService) {
				_service = (LessonTopicLocalService)invokableLocalService;
			}
			else {
				_service = new LessonTopicLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(LessonTopicLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(LessonTopicLocalService service) {
	}

	private static LessonTopicLocalService _service;
}
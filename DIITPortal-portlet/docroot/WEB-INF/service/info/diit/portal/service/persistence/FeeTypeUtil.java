/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.FeeType;

import java.util.List;

/**
 * The persistence utility for the fee type service. This utility wraps {@link FeeTypePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see FeeTypePersistence
 * @see FeeTypePersistenceImpl
 * @generated
 */
public class FeeTypeUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(FeeType feeType) {
		getPersistence().clearCache(feeType);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<FeeType> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<FeeType> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<FeeType> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static FeeType update(FeeType feeType, boolean merge)
		throws SystemException {
		return getPersistence().update(feeType, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static FeeType update(FeeType feeType, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(feeType, merge, serviceContext);
	}

	/**
	* Caches the fee type in the entity cache if it is enabled.
	*
	* @param feeType the fee type
	*/
	public static void cacheResult(info.diit.portal.model.FeeType feeType) {
		getPersistence().cacheResult(feeType);
	}

	/**
	* Caches the fee types in the entity cache if it is enabled.
	*
	* @param feeTypes the fee types
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.FeeType> feeTypes) {
		getPersistence().cacheResult(feeTypes);
	}

	/**
	* Creates a new fee type with the primary key. Does not add the fee type to the database.
	*
	* @param feeTypeId the primary key for the new fee type
	* @return the new fee type
	*/
	public static info.diit.portal.model.FeeType create(long feeTypeId) {
		return getPersistence().create(feeTypeId);
	}

	/**
	* Removes the fee type with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param feeTypeId the primary key of the fee type
	* @return the fee type that was removed
	* @throws info.diit.portal.NoSuchFeeTypeException if a fee type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.FeeType remove(long feeTypeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchFeeTypeException {
		return getPersistence().remove(feeTypeId);
	}

	public static info.diit.portal.model.FeeType updateImpl(
		info.diit.portal.model.FeeType feeType, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(feeType, merge);
	}

	/**
	* Returns the fee type with the primary key or throws a {@link info.diit.portal.NoSuchFeeTypeException} if it could not be found.
	*
	* @param feeTypeId the primary key of the fee type
	* @return the fee type
	* @throws info.diit.portal.NoSuchFeeTypeException if a fee type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.FeeType findByPrimaryKey(
		long feeTypeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchFeeTypeException {
		return getPersistence().findByPrimaryKey(feeTypeId);
	}

	/**
	* Returns the fee type with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param feeTypeId the primary key of the fee type
	* @return the fee type, or <code>null</code> if a fee type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.FeeType fetchByPrimaryKey(
		long feeTypeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(feeTypeId);
	}

	/**
	* Returns all the fee types where typeName = &#63;.
	*
	* @param typeName the type name
	* @return the matching fee types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.FeeType> findByTypeName(
		java.lang.String typeName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByTypeName(typeName);
	}

	/**
	* Returns a range of all the fee types where typeName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param typeName the type name
	* @param start the lower bound of the range of fee types
	* @param end the upper bound of the range of fee types (not inclusive)
	* @return the range of matching fee types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.FeeType> findByTypeName(
		java.lang.String typeName, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByTypeName(typeName, start, end);
	}

	/**
	* Returns an ordered range of all the fee types where typeName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param typeName the type name
	* @param start the lower bound of the range of fee types
	* @param end the upper bound of the range of fee types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching fee types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.FeeType> findByTypeName(
		java.lang.String typeName, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByTypeName(typeName, start, end, orderByComparator);
	}

	/**
	* Returns the first fee type in the ordered set where typeName = &#63;.
	*
	* @param typeName the type name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching fee type
	* @throws info.diit.portal.NoSuchFeeTypeException if a matching fee type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.FeeType findByTypeName_First(
		java.lang.String typeName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchFeeTypeException {
		return getPersistence().findByTypeName_First(typeName, orderByComparator);
	}

	/**
	* Returns the first fee type in the ordered set where typeName = &#63;.
	*
	* @param typeName the type name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching fee type, or <code>null</code> if a matching fee type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.FeeType fetchByTypeName_First(
		java.lang.String typeName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByTypeName_First(typeName, orderByComparator);
	}

	/**
	* Returns the last fee type in the ordered set where typeName = &#63;.
	*
	* @param typeName the type name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching fee type
	* @throws info.diit.portal.NoSuchFeeTypeException if a matching fee type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.FeeType findByTypeName_Last(
		java.lang.String typeName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchFeeTypeException {
		return getPersistence().findByTypeName_Last(typeName, orderByComparator);
	}

	/**
	* Returns the last fee type in the ordered set where typeName = &#63;.
	*
	* @param typeName the type name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching fee type, or <code>null</code> if a matching fee type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.FeeType fetchByTypeName_Last(
		java.lang.String typeName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByTypeName_Last(typeName, orderByComparator);
	}

	/**
	* Returns the fee types before and after the current fee type in the ordered set where typeName = &#63;.
	*
	* @param feeTypeId the primary key of the current fee type
	* @param typeName the type name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next fee type
	* @throws info.diit.portal.NoSuchFeeTypeException if a fee type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.FeeType[] findByTypeName_PrevAndNext(
		long feeTypeId, java.lang.String typeName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchFeeTypeException {
		return getPersistence()
				   .findByTypeName_PrevAndNext(feeTypeId, typeName,
			orderByComparator);
	}

	/**
	* Returns all the fee types.
	*
	* @return the fee types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.FeeType> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the fee types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of fee types
	* @param end the upper bound of the range of fee types (not inclusive)
	* @return the range of fee types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.FeeType> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the fee types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of fee types
	* @param end the upper bound of the range of fee types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of fee types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.FeeType> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the fee types where typeName = &#63; from the database.
	*
	* @param typeName the type name
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByTypeName(java.lang.String typeName)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByTypeName(typeName);
	}

	/**
	* Removes all the fee types from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of fee types where typeName = &#63;.
	*
	* @param typeName the type name
	* @return the number of matching fee types
	* @throws SystemException if a system exception occurred
	*/
	public static int countByTypeName(java.lang.String typeName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByTypeName(typeName);
	}

	/**
	* Returns the number of fee types.
	*
	* @return the number of fee types
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static FeeTypePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (FeeTypePersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					FeeTypePersistence.class.getName());

			ReferenceRegistry.registerReference(FeeTypeUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(FeeTypePersistence persistence) {
	}

	private static FeeTypePersistence _persistence;
}
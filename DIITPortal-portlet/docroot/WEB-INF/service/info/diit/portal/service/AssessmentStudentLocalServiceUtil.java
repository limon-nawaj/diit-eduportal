/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the assessment student local service. This utility wraps {@link info.diit.portal.service.impl.AssessmentStudentLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see AssessmentStudentLocalService
 * @see info.diit.portal.service.base.AssessmentStudentLocalServiceBaseImpl
 * @see info.diit.portal.service.impl.AssessmentStudentLocalServiceImpl
 * @generated
 */
public class AssessmentStudentLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link info.diit.portal.service.impl.AssessmentStudentLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the assessment student to the database. Also notifies the appropriate model listeners.
	*
	* @param assessmentStudent the assessment student
	* @return the assessment student that was added
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentStudent addAssessmentStudent(
		info.diit.portal.model.AssessmentStudent assessmentStudent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addAssessmentStudent(assessmentStudent);
	}

	/**
	* Creates a new assessment student with the primary key. Does not add the assessment student to the database.
	*
	* @param assessmentStudentId the primary key for the new assessment student
	* @return the new assessment student
	*/
	public static info.diit.portal.model.AssessmentStudent createAssessmentStudent(
		long assessmentStudentId) {
		return getService().createAssessmentStudent(assessmentStudentId);
	}

	/**
	* Deletes the assessment student with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param assessmentStudentId the primary key of the assessment student
	* @return the assessment student that was removed
	* @throws PortalException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentStudent deleteAssessmentStudent(
		long assessmentStudentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteAssessmentStudent(assessmentStudentId);
	}

	/**
	* Deletes the assessment student from the database. Also notifies the appropriate model listeners.
	*
	* @param assessmentStudent the assessment student
	* @return the assessment student that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentStudent deleteAssessmentStudent(
		info.diit.portal.model.AssessmentStudent assessmentStudent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteAssessmentStudent(assessmentStudent);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static info.diit.portal.model.AssessmentStudent fetchAssessmentStudent(
		long assessmentStudentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchAssessmentStudent(assessmentStudentId);
	}

	/**
	* Returns the assessment student with the primary key.
	*
	* @param assessmentStudentId the primary key of the assessment student
	* @return the assessment student
	* @throws PortalException if a assessment student with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentStudent getAssessmentStudent(
		long assessmentStudentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getAssessmentStudent(assessmentStudentId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the assessment students.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of assessment students
	* @param end the upper bound of the range of assessment students (not inclusive)
	* @return the range of assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AssessmentStudent> getAssessmentStudents(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getAssessmentStudents(start, end);
	}

	/**
	* Returns the number of assessment students.
	*
	* @return the number of assessment students
	* @throws SystemException if a system exception occurred
	*/
	public static int getAssessmentStudentsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getAssessmentStudentsCount();
	}

	/**
	* Updates the assessment student in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param assessmentStudent the assessment student
	* @return the assessment student that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentStudent updateAssessmentStudent(
		info.diit.portal.model.AssessmentStudent assessmentStudent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateAssessmentStudent(assessmentStudent);
	}

	/**
	* Updates the assessment student in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param assessmentStudent the assessment student
	* @param merge whether to merge the assessment student with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the assessment student that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentStudent updateAssessmentStudent(
		info.diit.portal.model.AssessmentStudent assessmentStudent,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateAssessmentStudent(assessmentStudent, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<info.diit.portal.model.AssessmentStudent> findByAssessmentId(
		long assessmentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByAssessmentId(assessmentId);
	}

	public static java.util.List<info.diit.portal.model.AssessmentStudent> findByUser(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByUser(userId);
	}

	public static info.diit.portal.model.AssessmentStudent findByAssessmentStudent(
		long assessmentId, long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByAssessmentStudent(assessmentId, studentId);
	}

	public static void clearService() {
		_service = null;
	}

	public static AssessmentStudentLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					AssessmentStudentLocalService.class.getName());

			if (invokableLocalService instanceof AssessmentStudentLocalService) {
				_service = (AssessmentStudentLocalService)invokableLocalService;
			}
			else {
				_service = new AssessmentStudentLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(AssessmentStudentLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(AssessmentStudentLocalService service) {
	}

	private static AssessmentStudentLocalService _service;
}
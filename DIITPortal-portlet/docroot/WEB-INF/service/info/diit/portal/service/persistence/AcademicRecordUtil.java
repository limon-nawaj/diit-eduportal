/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.AcademicRecord;

import java.util.List;

/**
 * The persistence utility for the academic record service. This utility wraps {@link AcademicRecordPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see AcademicRecordPersistence
 * @see AcademicRecordPersistenceImpl
 * @generated
 */
public class AcademicRecordUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(AcademicRecord academicRecord) {
		getPersistence().clearCache(academicRecord);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<AcademicRecord> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<AcademicRecord> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<AcademicRecord> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static AcademicRecord update(AcademicRecord academicRecord,
		boolean merge) throws SystemException {
		return getPersistence().update(academicRecord, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static AcademicRecord update(AcademicRecord academicRecord,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(academicRecord, merge, serviceContext);
	}

	/**
	* Caches the academic record in the entity cache if it is enabled.
	*
	* @param academicRecord the academic record
	*/
	public static void cacheResult(
		info.diit.portal.model.AcademicRecord academicRecord) {
		getPersistence().cacheResult(academicRecord);
	}

	/**
	* Caches the academic records in the entity cache if it is enabled.
	*
	* @param academicRecords the academic records
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.AcademicRecord> academicRecords) {
		getPersistence().cacheResult(academicRecords);
	}

	/**
	* Creates a new academic record with the primary key. Does not add the academic record to the database.
	*
	* @param academicRecordId the primary key for the new academic record
	* @return the new academic record
	*/
	public static info.diit.portal.model.AcademicRecord create(
		long academicRecordId) {
		return getPersistence().create(academicRecordId);
	}

	/**
	* Removes the academic record with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param academicRecordId the primary key of the academic record
	* @return the academic record that was removed
	* @throws info.diit.portal.NoSuchAcademicRecordException if a academic record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AcademicRecord remove(
		long academicRecordId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAcademicRecordException {
		return getPersistence().remove(academicRecordId);
	}

	public static info.diit.portal.model.AcademicRecord updateImpl(
		info.diit.portal.model.AcademicRecord academicRecord, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(academicRecord, merge);
	}

	/**
	* Returns the academic record with the primary key or throws a {@link info.diit.portal.NoSuchAcademicRecordException} if it could not be found.
	*
	* @param academicRecordId the primary key of the academic record
	* @return the academic record
	* @throws info.diit.portal.NoSuchAcademicRecordException if a academic record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AcademicRecord findByPrimaryKey(
		long academicRecordId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAcademicRecordException {
		return getPersistence().findByPrimaryKey(academicRecordId);
	}

	/**
	* Returns the academic record with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param academicRecordId the primary key of the academic record
	* @return the academic record, or <code>null</code> if a academic record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AcademicRecord fetchByPrimaryKey(
		long academicRecordId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(academicRecordId);
	}

	/**
	* Returns all the academic records where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the matching academic records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AcademicRecord> findByStudentAcademicRecordList(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStudentAcademicRecordList(studentId);
	}

	/**
	* Returns a range of all the academic records where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of academic records
	* @param end the upper bound of the range of academic records (not inclusive)
	* @return the range of matching academic records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AcademicRecord> findByStudentAcademicRecordList(
		long studentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentAcademicRecordList(studentId, start, end);
	}

	/**
	* Returns an ordered range of all the academic records where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of academic records
	* @param end the upper bound of the range of academic records (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching academic records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AcademicRecord> findByStudentAcademicRecordList(
		long studentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentAcademicRecordList(studentId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first academic record in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching academic record
	* @throws info.diit.portal.NoSuchAcademicRecordException if a matching academic record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AcademicRecord findByStudentAcademicRecordList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAcademicRecordException {
		return getPersistence()
				   .findByStudentAcademicRecordList_First(studentId,
			orderByComparator);
	}

	/**
	* Returns the first academic record in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching academic record, or <code>null</code> if a matching academic record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AcademicRecord fetchByStudentAcademicRecordList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentAcademicRecordList_First(studentId,
			orderByComparator);
	}

	/**
	* Returns the last academic record in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching academic record
	* @throws info.diit.portal.NoSuchAcademicRecordException if a matching academic record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AcademicRecord findByStudentAcademicRecordList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAcademicRecordException {
		return getPersistence()
				   .findByStudentAcademicRecordList_Last(studentId,
			orderByComparator);
	}

	/**
	* Returns the last academic record in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching academic record, or <code>null</code> if a matching academic record could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AcademicRecord fetchByStudentAcademicRecordList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentAcademicRecordList_Last(studentId,
			orderByComparator);
	}

	/**
	* Returns the academic records before and after the current academic record in the ordered set where studentId = &#63;.
	*
	* @param academicRecordId the primary key of the current academic record
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next academic record
	* @throws info.diit.portal.NoSuchAcademicRecordException if a academic record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AcademicRecord[] findByStudentAcademicRecordList_PrevAndNext(
		long academicRecordId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAcademicRecordException {
		return getPersistence()
				   .findByStudentAcademicRecordList_PrevAndNext(academicRecordId,
			studentId, orderByComparator);
	}

	/**
	* Returns all the academic records.
	*
	* @return the academic records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AcademicRecord> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the academic records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of academic records
	* @param end the upper bound of the range of academic records (not inclusive)
	* @return the range of academic records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AcademicRecord> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the academic records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of academic records
	* @param end the upper bound of the range of academic records (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of academic records
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AcademicRecord> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the academic records where studentId = &#63; from the database.
	*
	* @param studentId the student ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByStudentAcademicRecordList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByStudentAcademicRecordList(studentId);
	}

	/**
	* Removes all the academic records from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of academic records where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the number of matching academic records
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStudentAcademicRecordList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStudentAcademicRecordList(studentId);
	}

	/**
	* Returns the number of academic records.
	*
	* @return the number of academic records
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static AcademicRecordPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (AcademicRecordPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					AcademicRecordPersistence.class.getName());

			ReferenceRegistry.registerReference(AcademicRecordUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(AcademicRecordPersistence persistence) {
	}

	private static AcademicRecordPersistence _persistence;
}
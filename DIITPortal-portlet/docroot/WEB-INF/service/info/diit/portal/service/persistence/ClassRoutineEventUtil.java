/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.ClassRoutineEvent;

import java.util.List;

/**
 * The persistence utility for the class routine event service. This utility wraps {@link ClassRoutineEventPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see ClassRoutineEventPersistence
 * @see ClassRoutineEventPersistenceImpl
 * @generated
 */
public class ClassRoutineEventUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(ClassRoutineEvent classRoutineEvent) {
		getPersistence().clearCache(classRoutineEvent);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ClassRoutineEvent> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ClassRoutineEvent> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ClassRoutineEvent> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static ClassRoutineEvent update(
		ClassRoutineEvent classRoutineEvent, boolean merge)
		throws SystemException {
		return getPersistence().update(classRoutineEvent, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static ClassRoutineEvent update(
		ClassRoutineEvent classRoutineEvent, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(classRoutineEvent, merge, serviceContext);
	}

	/**
	* Caches the class routine event in the entity cache if it is enabled.
	*
	* @param classRoutineEvent the class routine event
	*/
	public static void cacheResult(
		info.diit.portal.model.ClassRoutineEvent classRoutineEvent) {
		getPersistence().cacheResult(classRoutineEvent);
	}

	/**
	* Caches the class routine events in the entity cache if it is enabled.
	*
	* @param classRoutineEvents the class routine events
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.ClassRoutineEvent> classRoutineEvents) {
		getPersistence().cacheResult(classRoutineEvents);
	}

	/**
	* Creates a new class routine event with the primary key. Does not add the class routine event to the database.
	*
	* @param classRoutineEventId the primary key for the new class routine event
	* @return the new class routine event
	*/
	public static info.diit.portal.model.ClassRoutineEvent create(
		long classRoutineEventId) {
		return getPersistence().create(classRoutineEventId);
	}

	/**
	* Removes the class routine event with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param classRoutineEventId the primary key of the class routine event
	* @return the class routine event that was removed
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent remove(
		long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException {
		return getPersistence().remove(classRoutineEventId);
	}

	public static info.diit.portal.model.ClassRoutineEvent updateImpl(
		info.diit.portal.model.ClassRoutineEvent classRoutineEvent,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(classRoutineEvent, merge);
	}

	/**
	* Returns the class routine event with the primary key or throws a {@link info.diit.portal.NoSuchClassRoutineEventException} if it could not be found.
	*
	* @param classRoutineEventId the primary key of the class routine event
	* @return the class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent findByPrimaryKey(
		long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException {
		return getPersistence().findByPrimaryKey(classRoutineEventId);
	}

	/**
	* Returns the class routine event with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param classRoutineEventId the primary key of the class routine event
	* @return the class routine event, or <code>null</code> if a class routine event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent fetchByPrimaryKey(
		long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(classRoutineEventId);
	}

	/**
	* Returns the class routine event where subjectId = &#63; and classRoutineEventId = &#63; or throws a {@link info.diit.portal.NoSuchClassRoutineEventException} if it could not be found.
	*
	* @param subjectId the subject ID
	* @param classRoutineEventId the class routine event ID
	* @return the matching class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent findBySubjectId(
		long subjectId, long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException {
		return getPersistence().findBySubjectId(subjectId, classRoutineEventId);
	}

	/**
	* Returns the class routine event where subjectId = &#63; and classRoutineEventId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param subjectId the subject ID
	* @param classRoutineEventId the class routine event ID
	* @return the matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent fetchBySubjectId(
		long subjectId, long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBySubjectId(subjectId, classRoutineEventId);
	}

	/**
	* Returns the class routine event where subjectId = &#63; and classRoutineEventId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param subjectId the subject ID
	* @param classRoutineEventId the class routine event ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent fetchBySubjectId(
		long subjectId, long classRoutineEventId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBySubjectId(subjectId, classRoutineEventId,
			retrieveFromCache);
	}

	/**
	* Returns all the class routine events where roomId = &#63;.
	*
	* @param roomId the room ID
	* @return the matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEvent> findByRoom(
		long roomId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByRoom(roomId);
	}

	/**
	* Returns a range of all the class routine events where roomId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param roomId the room ID
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @return the range of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEvent> findByRoom(
		long roomId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByRoom(roomId, start, end);
	}

	/**
	* Returns an ordered range of all the class routine events where roomId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param roomId the room ID
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEvent> findByRoom(
		long roomId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByRoom(roomId, start, end, orderByComparator);
	}

	/**
	* Returns the first class routine event in the ordered set where roomId = &#63;.
	*
	* @param roomId the room ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent findByRoom_First(
		long roomId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException {
		return getPersistence().findByRoom_First(roomId, orderByComparator);
	}

	/**
	* Returns the first class routine event in the ordered set where roomId = &#63;.
	*
	* @param roomId the room ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent fetchByRoom_First(
		long roomId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByRoom_First(roomId, orderByComparator);
	}

	/**
	* Returns the last class routine event in the ordered set where roomId = &#63;.
	*
	* @param roomId the room ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent findByRoom_Last(
		long roomId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException {
		return getPersistence().findByRoom_Last(roomId, orderByComparator);
	}

	/**
	* Returns the last class routine event in the ordered set where roomId = &#63;.
	*
	* @param roomId the room ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent fetchByRoom_Last(
		long roomId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByRoom_Last(roomId, orderByComparator);
	}

	/**
	* Returns the class routine events before and after the current class routine event in the ordered set where roomId = &#63;.
	*
	* @param classRoutineEventId the primary key of the current class routine event
	* @param roomId the room ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent[] findByRoom_PrevAndNext(
		long classRoutineEventId, long roomId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException {
		return getPersistence()
				   .findByRoom_PrevAndNext(classRoutineEventId, roomId,
			orderByComparator);
	}

	/**
	* Returns all the class routine events where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEvent> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId);
	}

	/**
	* Returns a range of all the class routine events where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @return the range of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEvent> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the class routine events where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEvent> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first class routine event in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException {
		return getPersistence().findByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first class routine event in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last class routine event in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException {
		return getPersistence().findByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last class routine event in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the class routine events before and after the current class routine event in the ordered set where companyId = &#63;.
	*
	* @param classRoutineEventId the primary key of the current class routine event
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent[] findByCompany_PrevAndNext(
		long classRoutineEventId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException {
		return getPersistence()
				   .findByCompany_PrevAndNext(classRoutineEventId, companyId,
			orderByComparator);
	}

	/**
	* Returns all the class routine events where subjectId = &#63; and day = &#63;.
	*
	* @param subjectId the subject ID
	* @param day the day
	* @return the matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEvent> findBySubjectDate(
		long subjectId, int day)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBySubjectDate(subjectId, day);
	}

	/**
	* Returns a range of all the class routine events where subjectId = &#63; and day = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject ID
	* @param day the day
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @return the range of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEvent> findBySubjectDate(
		long subjectId, int day, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBySubjectDate(subjectId, day, start, end);
	}

	/**
	* Returns an ordered range of all the class routine events where subjectId = &#63; and day = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subjectId the subject ID
	* @param day the day
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEvent> findBySubjectDate(
		long subjectId, int day, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBySubjectDate(subjectId, day, start, end,
			orderByComparator);
	}

	/**
	* Returns the first class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	*
	* @param subjectId the subject ID
	* @param day the day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent findBySubjectDate_First(
		long subjectId, int day,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException {
		return getPersistence()
				   .findBySubjectDate_First(subjectId, day, orderByComparator);
	}

	/**
	* Returns the first class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	*
	* @param subjectId the subject ID
	* @param day the day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent fetchBySubjectDate_First(
		long subjectId, int day,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBySubjectDate_First(subjectId, day, orderByComparator);
	}

	/**
	* Returns the last class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	*
	* @param subjectId the subject ID
	* @param day the day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent findBySubjectDate_Last(
		long subjectId, int day,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException {
		return getPersistence()
				   .findBySubjectDate_Last(subjectId, day, orderByComparator);
	}

	/**
	* Returns the last class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	*
	* @param subjectId the subject ID
	* @param day the day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event, or <code>null</code> if a matching class routine event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent fetchBySubjectDate_Last(
		long subjectId, int day,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBySubjectDate_Last(subjectId, day, orderByComparator);
	}

	/**
	* Returns the class routine events before and after the current class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	*
	* @param classRoutineEventId the primary key of the current class routine event
	* @param subjectId the subject ID
	* @param day the day
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next class routine event
	* @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent[] findBySubjectDate_PrevAndNext(
		long classRoutineEventId, long subjectId, int day,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException {
		return getPersistence()
				   .findBySubjectDate_PrevAndNext(classRoutineEventId,
			subjectId, day, orderByComparator);
	}

	/**
	* Returns all the class routine events.
	*
	* @return the class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEvent> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the class routine events.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @return the range of class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEvent> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the class routine events.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of class routine events
	* @param end the upper bound of the range of class routine events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEvent> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes the class routine event where subjectId = &#63; and classRoutineEventId = &#63; from the database.
	*
	* @param subjectId the subject ID
	* @param classRoutineEventId the class routine event ID
	* @return the class routine event that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEvent removeBySubjectId(
		long subjectId, long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventException {
		return getPersistence().removeBySubjectId(subjectId, classRoutineEventId);
	}

	/**
	* Removes all the class routine events where roomId = &#63; from the database.
	*
	* @param roomId the room ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByRoom(long roomId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByRoom(roomId);
	}

	/**
	* Removes all the class routine events where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCompany(companyId);
	}

	/**
	* Removes all the class routine events where subjectId = &#63; and day = &#63; from the database.
	*
	* @param subjectId the subject ID
	* @param day the day
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBySubjectDate(long subjectId, int day)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBySubjectDate(subjectId, day);
	}

	/**
	* Removes all the class routine events from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of class routine events where subjectId = &#63; and classRoutineEventId = &#63;.
	*
	* @param subjectId the subject ID
	* @param classRoutineEventId the class routine event ID
	* @return the number of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static int countBySubjectId(long subjectId, long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBySubjectId(subjectId, classRoutineEventId);
	}

	/**
	* Returns the number of class routine events where roomId = &#63;.
	*
	* @param roomId the room ID
	* @return the number of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static int countByRoom(long roomId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByRoom(roomId);
	}

	/**
	* Returns the number of class routine events where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCompany(companyId);
	}

	/**
	* Returns the number of class routine events where subjectId = &#63; and day = &#63;.
	*
	* @param subjectId the subject ID
	* @param day the day
	* @return the number of matching class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static int countBySubjectDate(long subjectId, int day)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBySubjectDate(subjectId, day);
	}

	/**
	* Returns the number of class routine events.
	*
	* @return the number of class routine events
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ClassRoutineEventPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ClassRoutineEventPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					ClassRoutineEventPersistence.class.getName());

			ReferenceRegistry.registerReference(ClassRoutineEventUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(ClassRoutineEventPersistence persistence) {
	}

	private static ClassRoutineEventPersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link EmployeeRoleLocalService}.
 * </p>
 *
 * @author    mohammad
 * @see       EmployeeRoleLocalService
 * @generated
 */
public class EmployeeRoleLocalServiceWrapper implements EmployeeRoleLocalService,
	ServiceWrapper<EmployeeRoleLocalService> {
	public EmployeeRoleLocalServiceWrapper(
		EmployeeRoleLocalService employeeRoleLocalService) {
		_employeeRoleLocalService = employeeRoleLocalService;
	}

	/**
	* Adds the employee role to the database. Also notifies the appropriate model listeners.
	*
	* @param employeeRole the employee role
	* @return the employee role that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeRole addEmployeeRole(
		info.diit.portal.model.EmployeeRole employeeRole)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.addEmployeeRole(employeeRole);
	}

	/**
	* Creates a new employee role with the primary key. Does not add the employee role to the database.
	*
	* @param employeeRoleId the primary key for the new employee role
	* @return the new employee role
	*/
	public info.diit.portal.model.EmployeeRole createEmployeeRole(
		long employeeRoleId) {
		return _employeeRoleLocalService.createEmployeeRole(employeeRoleId);
	}

	/**
	* Deletes the employee role with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param employeeRoleId the primary key of the employee role
	* @return the employee role that was removed
	* @throws PortalException if a employee role with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeRole deleteEmployeeRole(
		long employeeRoleId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.deleteEmployeeRole(employeeRoleId);
	}

	/**
	* Deletes the employee role from the database. Also notifies the appropriate model listeners.
	*
	* @param employeeRole the employee role
	* @return the employee role that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeRole deleteEmployeeRole(
		info.diit.portal.model.EmployeeRole employeeRole)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.deleteEmployeeRole(employeeRole);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _employeeRoleLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.EmployeeRole fetchEmployeeRole(
		long employeeRoleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.fetchEmployeeRole(employeeRoleId);
	}

	/**
	* Returns the employee role with the primary key.
	*
	* @param employeeRoleId the primary key of the employee role
	* @return the employee role
	* @throws PortalException if a employee role with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeRole getEmployeeRole(
		long employeeRoleId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.getEmployeeRole(employeeRoleId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the employee roles.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of employee roles
	* @param end the upper bound of the range of employee roles (not inclusive)
	* @return the range of employee roles
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.EmployeeRole> getEmployeeRoles(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.getEmployeeRoles(start, end);
	}

	/**
	* Returns the number of employee roles.
	*
	* @return the number of employee roles
	* @throws SystemException if a system exception occurred
	*/
	public int getEmployeeRolesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.getEmployeeRolesCount();
	}

	/**
	* Updates the employee role in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param employeeRole the employee role
	* @return the employee role that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeRole updateEmployeeRole(
		info.diit.portal.model.EmployeeRole employeeRole)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.updateEmployeeRole(employeeRole);
	}

	/**
	* Updates the employee role in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param employeeRole the employee role
	* @param merge whether to merge the employee role with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the employee role that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.EmployeeRole updateEmployeeRole(
		info.diit.portal.model.EmployeeRole employeeRole, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.updateEmployeeRole(employeeRole, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _employeeRoleLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_employeeRoleLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _employeeRoleLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.model.EmployeeRole> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.findByOrganization(organizationId);
	}

	public java.util.List<info.diit.portal.model.EmployeeRole> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _employeeRoleLocalService.findByCompany(companyId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public EmployeeRoleLocalService getWrappedEmployeeRoleLocalService() {
		return _employeeRoleLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedEmployeeRoleLocalService(
		EmployeeRoleLocalService employeeRoleLocalService) {
		_employeeRoleLocalService = employeeRoleLocalService;
	}

	public EmployeeRoleLocalService getWrappedService() {
		return _employeeRoleLocalService;
	}

	public void setWrappedService(
		EmployeeRoleLocalService employeeRoleLocalService) {
		_employeeRoleLocalService = employeeRoleLocalService;
	}

	private EmployeeRoleLocalService _employeeRoleLocalService;
}
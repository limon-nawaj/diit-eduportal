/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link SubjectLessonLocalService}.
 * </p>
 *
 * @author    mohammad
 * @see       SubjectLessonLocalService
 * @generated
 */
public class SubjectLessonLocalServiceWrapper
	implements SubjectLessonLocalService,
		ServiceWrapper<SubjectLessonLocalService> {
	public SubjectLessonLocalServiceWrapper(
		SubjectLessonLocalService subjectLessonLocalService) {
		_subjectLessonLocalService = subjectLessonLocalService;
	}

	/**
	* Adds the subject lesson to the database. Also notifies the appropriate model listeners.
	*
	* @param subjectLesson the subject lesson
	* @return the subject lesson that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.SubjectLesson addSubjectLesson(
		info.diit.portal.model.SubjectLesson subjectLesson)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.addSubjectLesson(subjectLesson);
	}

	/**
	* Creates a new subject lesson with the primary key. Does not add the subject lesson to the database.
	*
	* @param subjectLessonId the primary key for the new subject lesson
	* @return the new subject lesson
	*/
	public info.diit.portal.model.SubjectLesson createSubjectLesson(
		long subjectLessonId) {
		return _subjectLessonLocalService.createSubjectLesson(subjectLessonId);
	}

	/**
	* Deletes the subject lesson with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param subjectLessonId the primary key of the subject lesson
	* @return the subject lesson that was removed
	* @throws PortalException if a subject lesson with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.SubjectLesson deleteSubjectLesson(
		long subjectLessonId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.deleteSubjectLesson(subjectLessonId);
	}

	/**
	* Deletes the subject lesson from the database. Also notifies the appropriate model listeners.
	*
	* @param subjectLesson the subject lesson
	* @return the subject lesson that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.SubjectLesson deleteSubjectLesson(
		info.diit.portal.model.SubjectLesson subjectLesson)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.deleteSubjectLesson(subjectLesson);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _subjectLessonLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.SubjectLesson fetchSubjectLesson(
		long subjectLessonId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.fetchSubjectLesson(subjectLessonId);
	}

	/**
	* Returns the subject lesson with the primary key.
	*
	* @param subjectLessonId the primary key of the subject lesson
	* @return the subject lesson
	* @throws PortalException if a subject lesson with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.SubjectLesson getSubjectLesson(
		long subjectLessonId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.getSubjectLesson(subjectLessonId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the subject lessons.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of subject lessons
	* @param end the upper bound of the range of subject lessons (not inclusive)
	* @return the range of subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.SubjectLesson> getSubjectLessons(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.getSubjectLessons(start, end);
	}

	/**
	* Returns the number of subject lessons.
	*
	* @return the number of subject lessons
	* @throws SystemException if a system exception occurred
	*/
	public int getSubjectLessonsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.getSubjectLessonsCount();
	}

	/**
	* Updates the subject lesson in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param subjectLesson the subject lesson
	* @return the subject lesson that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.SubjectLesson updateSubjectLesson(
		info.diit.portal.model.SubjectLesson subjectLesson)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.updateSubjectLesson(subjectLesson);
	}

	/**
	* Updates the subject lesson in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param subjectLesson the subject lesson
	* @param merge whether to merge the subject lesson with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the subject lesson that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.SubjectLesson updateSubjectLesson(
		info.diit.portal.model.SubjectLesson subjectLesson, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.updateSubjectLesson(subjectLesson,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _subjectLessonLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_subjectLessonLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _subjectLessonLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.model.SubjectLesson> findByUserOrganization(
		long organizationId, long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.findByUserOrganization(organizationId,
			userId);
	}

	public java.util.List<info.diit.portal.model.SubjectLesson> findByBatchSubject(
		long batch, long subject)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _subjectLessonLocalService.findByBatchSubject(batch, subject);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public SubjectLessonLocalService getWrappedSubjectLessonLocalService() {
		return _subjectLessonLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedSubjectLessonLocalService(
		SubjectLessonLocalService subjectLessonLocalService) {
		_subjectLessonLocalService = subjectLessonLocalService;
	}

	public SubjectLessonLocalService getWrappedService() {
		return _subjectLessonLocalService;
	}

	public void setWrappedService(
		SubjectLessonLocalService subjectLessonLocalService) {
		_subjectLessonLocalService = subjectLessonLocalService;
	}

	private SubjectLessonLocalService _subjectLessonLocalService;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.LeaveReceiver;

import java.util.List;

/**
 * The persistence utility for the leave receiver service. This utility wraps {@link LeaveReceiverPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see LeaveReceiverPersistence
 * @see LeaveReceiverPersistenceImpl
 * @generated
 */
public class LeaveReceiverUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(LeaveReceiver leaveReceiver) {
		getPersistence().clearCache(leaveReceiver);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<LeaveReceiver> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<LeaveReceiver> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<LeaveReceiver> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static LeaveReceiver update(LeaveReceiver leaveReceiver,
		boolean merge) throws SystemException {
		return getPersistence().update(leaveReceiver, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static LeaveReceiver update(LeaveReceiver leaveReceiver,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(leaveReceiver, merge, serviceContext);
	}

	/**
	* Caches the leave receiver in the entity cache if it is enabled.
	*
	* @param leaveReceiver the leave receiver
	*/
	public static void cacheResult(
		info.diit.portal.model.LeaveReceiver leaveReceiver) {
		getPersistence().cacheResult(leaveReceiver);
	}

	/**
	* Caches the leave receivers in the entity cache if it is enabled.
	*
	* @param leaveReceivers the leave receivers
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.LeaveReceiver> leaveReceivers) {
		getPersistence().cacheResult(leaveReceivers);
	}

	/**
	* Creates a new leave receiver with the primary key. Does not add the leave receiver to the database.
	*
	* @param leaveReceiverId the primary key for the new leave receiver
	* @return the new leave receiver
	*/
	public static info.diit.portal.model.LeaveReceiver create(
		long leaveReceiverId) {
		return getPersistence().create(leaveReceiverId);
	}

	/**
	* Removes the leave receiver with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param leaveReceiverId the primary key of the leave receiver
	* @return the leave receiver that was removed
	* @throws info.diit.portal.NoSuchLeaveReceiverException if a leave receiver with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveReceiver remove(
		long leaveReceiverId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveReceiverException {
		return getPersistence().remove(leaveReceiverId);
	}

	public static info.diit.portal.model.LeaveReceiver updateImpl(
		info.diit.portal.model.LeaveReceiver leaveReceiver, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(leaveReceiver, merge);
	}

	/**
	* Returns the leave receiver with the primary key or throws a {@link info.diit.portal.NoSuchLeaveReceiverException} if it could not be found.
	*
	* @param leaveReceiverId the primary key of the leave receiver
	* @return the leave receiver
	* @throws info.diit.portal.NoSuchLeaveReceiverException if a leave receiver with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveReceiver findByPrimaryKey(
		long leaveReceiverId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveReceiverException {
		return getPersistence().findByPrimaryKey(leaveReceiverId);
	}

	/**
	* Returns the leave receiver with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param leaveReceiverId the primary key of the leave receiver
	* @return the leave receiver, or <code>null</code> if a leave receiver with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveReceiver fetchByPrimaryKey(
		long leaveReceiverId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(leaveReceiverId);
	}

	/**
	* Returns all the leave receivers where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveReceiver> findByOrganizationId(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganizationId(organizationId);
	}

	/**
	* Returns a range of all the leave receivers where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of leave receivers
	* @param end the upper bound of the range of leave receivers (not inclusive)
	* @return the range of matching leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveReceiver> findByOrganizationId(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByOrganizationId(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the leave receivers where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of leave receivers
	* @param end the upper bound of the range of leave receivers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveReceiver> findByOrganizationId(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByOrganizationId(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first leave receiver in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave receiver
	* @throws info.diit.portal.NoSuchLeaveReceiverException if a matching leave receiver could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveReceiver findByOrganizationId_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveReceiverException {
		return getPersistence()
				   .findByOrganizationId_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first leave receiver in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching leave receiver, or <code>null</code> if a matching leave receiver could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveReceiver fetchByOrganizationId_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganizationId_First(organizationId,
			orderByComparator);
	}

	/**
	* Returns the last leave receiver in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave receiver
	* @throws info.diit.portal.NoSuchLeaveReceiverException if a matching leave receiver could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveReceiver findByOrganizationId_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveReceiverException {
		return getPersistence()
				   .findByOrganizationId_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last leave receiver in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching leave receiver, or <code>null</code> if a matching leave receiver could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveReceiver fetchByOrganizationId_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByOrganizationId_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the leave receivers before and after the current leave receiver in the ordered set where organizationId = &#63;.
	*
	* @param leaveReceiverId the primary key of the current leave receiver
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next leave receiver
	* @throws info.diit.portal.NoSuchLeaveReceiverException if a leave receiver with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveReceiver[] findByOrganizationId_PrevAndNext(
		long leaveReceiverId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveReceiverException {
		return getPersistence()
				   .findByOrganizationId_PrevAndNext(leaveReceiverId,
			organizationId, orderByComparator);
	}

	/**
	* Returns the leave receiver where employeeId = &#63; or throws a {@link info.diit.portal.NoSuchLeaveReceiverException} if it could not be found.
	*
	* @param employeeId the employee ID
	* @return the matching leave receiver
	* @throws info.diit.portal.NoSuchLeaveReceiverException if a matching leave receiver could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveReceiver findByEmployeeId(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveReceiverException {
		return getPersistence().findByEmployeeId(employeeId);
	}

	/**
	* Returns the leave receiver where employeeId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param employeeId the employee ID
	* @return the matching leave receiver, or <code>null</code> if a matching leave receiver could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveReceiver fetchByEmployeeId(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByEmployeeId(employeeId);
	}

	/**
	* Returns the leave receiver where employeeId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param employeeId the employee ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching leave receiver, or <code>null</code> if a matching leave receiver could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveReceiver fetchByEmployeeId(
		long employeeId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByEmployeeId(employeeId, retrieveFromCache);
	}

	/**
	* Returns all the leave receivers.
	*
	* @return the leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveReceiver> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the leave receivers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leave receivers
	* @param end the upper bound of the range of leave receivers (not inclusive)
	* @return the range of leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveReceiver> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the leave receivers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of leave receivers
	* @param end the upper bound of the range of leave receivers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LeaveReceiver> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the leave receivers where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByOrganizationId(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByOrganizationId(organizationId);
	}

	/**
	* Removes the leave receiver where employeeId = &#63; from the database.
	*
	* @param employeeId the employee ID
	* @return the leave receiver that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LeaveReceiver removeByEmployeeId(
		long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLeaveReceiverException {
		return getPersistence().removeByEmployeeId(employeeId);
	}

	/**
	* Removes all the leave receivers from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of leave receivers where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public static int countByOrganizationId(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByOrganizationId(organizationId);
	}

	/**
	* Returns the number of leave receivers where employeeId = &#63;.
	*
	* @param employeeId the employee ID
	* @return the number of matching leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public static int countByEmployeeId(long employeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByEmployeeId(employeeId);
	}

	/**
	* Returns the number of leave receivers.
	*
	* @return the number of leave receivers
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static LeaveReceiverPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (LeaveReceiverPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					LeaveReceiverPersistence.class.getName());

			ReferenceRegistry.registerReference(LeaveReceiverUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(LeaveReceiverPersistence persistence) {
	}

	private static LeaveReceiverPersistence _persistence;
}
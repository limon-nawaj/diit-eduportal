/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.CourseOrganization;

/**
 * The persistence interface for the course organization service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see CourseOrganizationPersistenceImpl
 * @see CourseOrganizationUtil
 * @generated
 */
public interface CourseOrganizationPersistence extends BasePersistence<CourseOrganization> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CourseOrganizationUtil} to access the course organization persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the course organization in the entity cache if it is enabled.
	*
	* @param courseOrganization the course organization
	*/
	public void cacheResult(
		info.diit.portal.model.CourseOrganization courseOrganization);

	/**
	* Caches the course organizations in the entity cache if it is enabled.
	*
	* @param courseOrganizations the course organizations
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.CourseOrganization> courseOrganizations);

	/**
	* Creates a new course organization with the primary key. Does not add the course organization to the database.
	*
	* @param courseOrganizationId the primary key for the new course organization
	* @return the new course organization
	*/
	public info.diit.portal.model.CourseOrganization create(
		long courseOrganizationId);

	/**
	* Removes the course organization with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param courseOrganizationId the primary key of the course organization
	* @return the course organization that was removed
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization remove(
		long courseOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException;

	public info.diit.portal.model.CourseOrganization updateImpl(
		info.diit.portal.model.CourseOrganization courseOrganization,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course organization with the primary key or throws a {@link info.diit.portal.NoSuchCourseOrganizationException} if it could not be found.
	*
	* @param courseOrganizationId the primary key of the course organization
	* @return the course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization findByPrimaryKey(
		long courseOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException;

	/**
	* Returns the course organization with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param courseOrganizationId the primary key of the course organization
	* @return the course organization, or <code>null</code> if a course organization with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization fetchByPrimaryKey(
		long courseOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the course organizations where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseOrganization> findBycourse(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course organizations where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @return the range of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseOrganization> findBycourse(
		long courseId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course organizations where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseOrganization> findBycourse(
		long courseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course organization in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization findBycourse_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException;

	/**
	* Returns the first course organization in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course organization, or <code>null</code> if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization fetchBycourse_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course organization in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization findBycourse_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException;

	/**
	* Returns the last course organization in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course organization, or <code>null</code> if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization fetchBycourse_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course organizations before and after the current course organization in the ordered set where courseId = &#63;.
	*
	* @param courseOrganizationId the primary key of the current course organization
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization[] findBycourse_PrevAndNext(
		long courseOrganizationId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException;

	/**
	* Returns all the course organizations where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseOrganization> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course organizations where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @return the range of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseOrganization> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course organizations where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseOrganization> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course organization in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException;

	/**
	* Returns the first course organization in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course organization, or <code>null</code> if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course organization in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException;

	/**
	* Returns the last course organization in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course organization, or <code>null</code> if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course organizations before and after the current course organization in the ordered set where organizationId = &#63;.
	*
	* @param courseOrganizationId the primary key of the current course organization
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization[] findByorganization_PrevAndNext(
		long courseOrganizationId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException;

	/**
	* Returns all the course organizations where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseOrganization> findBycompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course organizations where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @return the range of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseOrganization> findBycompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course organizations where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseOrganization> findBycompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course organization in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization findBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException;

	/**
	* Returns the first course organization in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course organization, or <code>null</code> if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization fetchBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course organization in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization findBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException;

	/**
	* Returns the last course organization in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course organization, or <code>null</code> if a matching course organization could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization fetchBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course organizations before and after the current course organization in the ordered set where companyId = &#63;.
	*
	* @param courseOrganizationId the primary key of the current course organization
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course organization
	* @throws info.diit.portal.NoSuchCourseOrganizationException if a course organization with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization[] findBycompany_PrevAndNext(
		long courseOrganizationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseOrganizationException;

	/**
	* Returns all the course organizations.
	*
	* @return the course organizations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseOrganization> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course organizations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @return the range of course organizations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseOrganization> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course organizations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of course organizations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseOrganization> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course organizations where courseId = &#63; from the database.
	*
	* @param courseId the course ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycourse(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course organizations where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course organizations where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course organizations from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course organizations where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the number of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public int countBycourse(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course organizations where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course organizations where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching course organizations
	* @throws SystemException if a system exception occurred
	*/
	public int countBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course organizations.
	*
	* @return the number of course organizations
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
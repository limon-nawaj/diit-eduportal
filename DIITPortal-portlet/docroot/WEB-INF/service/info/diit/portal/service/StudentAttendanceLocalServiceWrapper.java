/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link StudentAttendanceLocalService}.
 * </p>
 *
 * @author    mohammad
 * @see       StudentAttendanceLocalService
 * @generated
 */
public class StudentAttendanceLocalServiceWrapper
	implements StudentAttendanceLocalService,
		ServiceWrapper<StudentAttendanceLocalService> {
	public StudentAttendanceLocalServiceWrapper(
		StudentAttendanceLocalService studentAttendanceLocalService) {
		_studentAttendanceLocalService = studentAttendanceLocalService;
	}

	/**
	* Adds the student attendance to the database. Also notifies the appropriate model listeners.
	*
	* @param studentAttendance the student attendance
	* @return the student attendance that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.StudentAttendance addStudentAttendance(
		info.diit.portal.model.StudentAttendance studentAttendance)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.addStudentAttendance(studentAttendance);
	}

	/**
	* Creates a new student attendance with the primary key. Does not add the student attendance to the database.
	*
	* @param attendanceTopicId the primary key for the new student attendance
	* @return the new student attendance
	*/
	public info.diit.portal.model.StudentAttendance createStudentAttendance(
		long attendanceTopicId) {
		return _studentAttendanceLocalService.createStudentAttendance(attendanceTopicId);
	}

	/**
	* Deletes the student attendance with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceTopicId the primary key of the student attendance
	* @return the student attendance that was removed
	* @throws PortalException if a student attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.StudentAttendance deleteStudentAttendance(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.deleteStudentAttendance(attendanceTopicId);
	}

	/**
	* Deletes the student attendance from the database. Also notifies the appropriate model listeners.
	*
	* @param studentAttendance the student attendance
	* @return the student attendance that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.StudentAttendance deleteStudentAttendance(
		info.diit.portal.model.StudentAttendance studentAttendance)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.deleteStudentAttendance(studentAttendance);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _studentAttendanceLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.StudentAttendance fetchStudentAttendance(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.fetchStudentAttendance(attendanceTopicId);
	}

	/**
	* Returns the student attendance with the primary key.
	*
	* @param attendanceTopicId the primary key of the student attendance
	* @return the student attendance
	* @throws PortalException if a student attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.StudentAttendance getStudentAttendance(
		long attendanceTopicId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.getStudentAttendance(attendanceTopicId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the student attendances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student attendances
	* @param end the upper bound of the range of student attendances (not inclusive)
	* @return the range of student attendances
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.StudentAttendance> getStudentAttendances(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.getStudentAttendances(start, end);
	}

	/**
	* Returns the number of student attendances.
	*
	* @return the number of student attendances
	* @throws SystemException if a system exception occurred
	*/
	public int getStudentAttendancesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.getStudentAttendancesCount();
	}

	/**
	* Updates the student attendance in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param studentAttendance the student attendance
	* @return the student attendance that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.StudentAttendance updateStudentAttendance(
		info.diit.portal.model.StudentAttendance studentAttendance)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.updateStudentAttendance(studentAttendance);
	}

	/**
	* Updates the student attendance in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param studentAttendance the student attendance
	* @param merge whether to merge the student attendance with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the student attendance that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.StudentAttendance updateStudentAttendance(
		info.diit.portal.model.StudentAttendance studentAttendance,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.updateStudentAttendance(studentAttendance,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _studentAttendanceLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_studentAttendanceLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _studentAttendanceLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	public java.util.List<info.diit.portal.model.StudentAttendance> findByAttendance(
		long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.findByAttendance(attendanceId);
	}

	public java.util.List<info.diit.portal.model.StudentAttendance> findByStudent(
		long student)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _studentAttendanceLocalService.findByStudent(student);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public StudentAttendanceLocalService getWrappedStudentAttendanceLocalService() {
		return _studentAttendanceLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedStudentAttendanceLocalService(
		StudentAttendanceLocalService studentAttendanceLocalService) {
		_studentAttendanceLocalService = studentAttendanceLocalService;
	}

	public StudentAttendanceLocalService getWrappedService() {
		return _studentAttendanceLocalService;
	}

	public void setWrappedService(
		StudentAttendanceLocalService studentAttendanceLocalService) {
		_studentAttendanceLocalService = studentAttendanceLocalService;
	}

	private StudentAttendanceLocalService _studentAttendanceLocalService;
}
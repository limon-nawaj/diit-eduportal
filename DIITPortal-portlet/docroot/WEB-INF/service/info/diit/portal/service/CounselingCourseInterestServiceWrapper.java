/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link CounselingCourseInterestService}.
 * </p>
 *
 * @author    mohammad
 * @see       CounselingCourseInterestService
 * @generated
 */
public class CounselingCourseInterestServiceWrapper
	implements CounselingCourseInterestService,
		ServiceWrapper<CounselingCourseInterestService> {
	public CounselingCourseInterestServiceWrapper(
		CounselingCourseInterestService counselingCourseInterestService) {
		_counselingCourseInterestService = counselingCourseInterestService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _counselingCourseInterestService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_counselingCourseInterestService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _counselingCourseInterestService.invokeMethod(name,
			parameterTypes, arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public CounselingCourseInterestService getWrappedCounselingCourseInterestService() {
		return _counselingCourseInterestService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedCounselingCourseInterestService(
		CounselingCourseInterestService counselingCourseInterestService) {
		_counselingCourseInterestService = counselingCourseInterestService;
	}

	public CounselingCourseInterestService getWrappedService() {
		return _counselingCourseInterestService;
	}

	public void setWrappedService(
		CounselingCourseInterestService counselingCourseInterestService) {
		_counselingCourseInterestService = counselingCourseInterestService;
	}

	private CounselingCourseInterestService _counselingCourseInterestService;
}
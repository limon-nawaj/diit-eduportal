/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.ClassRoutineEventBatch;

/**
 * The persistence interface for the class routine event batch service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see ClassRoutineEventBatchPersistenceImpl
 * @see ClassRoutineEventBatchUtil
 * @generated
 */
public interface ClassRoutineEventBatchPersistence extends BasePersistence<ClassRoutineEventBatch> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ClassRoutineEventBatchUtil} to access the class routine event batch persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the class routine event batch in the entity cache if it is enabled.
	*
	* @param classRoutineEventBatch the class routine event batch
	*/
	public void cacheResult(
		info.diit.portal.model.ClassRoutineEventBatch classRoutineEventBatch);

	/**
	* Caches the class routine event batchs in the entity cache if it is enabled.
	*
	* @param classRoutineEventBatchs the class routine event batchs
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.ClassRoutineEventBatch> classRoutineEventBatchs);

	/**
	* Creates a new class routine event batch with the primary key. Does not add the class routine event batch to the database.
	*
	* @param classRoutineEventBatchId the primary key for the new class routine event batch
	* @return the new class routine event batch
	*/
	public info.diit.portal.model.ClassRoutineEventBatch create(
		long classRoutineEventBatchId);

	/**
	* Removes the class routine event batch with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param classRoutineEventBatchId the primary key of the class routine event batch
	* @return the class routine event batch that was removed
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch remove(
		long classRoutineEventBatchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException;

	public info.diit.portal.model.ClassRoutineEventBatch updateImpl(
		info.diit.portal.model.ClassRoutineEventBatch classRoutineEventBatch,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the class routine event batch with the primary key or throws a {@link info.diit.portal.NoSuchClassRoutineEventBatchException} if it could not be found.
	*
	* @param classRoutineEventBatchId the primary key of the class routine event batch
	* @return the class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch findByPrimaryKey(
		long classRoutineEventBatchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException;

	/**
	* Returns the class routine event batch with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param classRoutineEventBatchId the primary key of the class routine event batch
	* @return the class routine event batch, or <code>null</code> if a class routine event batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch fetchByPrimaryKey(
		long classRoutineEventBatchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the class routine event batchs where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findByCompanyId(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the class routine event batchs where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @return the range of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findByCompanyId(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the class routine event batchs where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findByCompanyId(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first class routine event batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch findByCompanyId_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException;

	/**
	* Returns the first class routine event batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch fetchByCompanyId_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last class routine event batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch findByCompanyId_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException;

	/**
	* Returns the last class routine event batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch fetchByCompanyId_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the class routine event batchs before and after the current class routine event batch in the ordered set where companyId = &#63;.
	*
	* @param classRoutineEventBatchId the primary key of the current class routine event batch
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch[] findByCompanyId_PrevAndNext(
		long classRoutineEventBatchId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException;

	/**
	* Returns all the class routine event batchs where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findBybatchId(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the class routine event batchs where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @return the range of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findBybatchId(
		long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the class routine event batchs where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findBybatchId(
		long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first class routine event batch in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch findBybatchId_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException;

	/**
	* Returns the first class routine event batch in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch fetchBybatchId_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last class routine event batch in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch findBybatchId_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException;

	/**
	* Returns the last class routine event batch in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch fetchBybatchId_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the class routine event batchs before and after the current class routine event batch in the ordered set where batchId = &#63;.
	*
	* @param classRoutineEventBatchId the primary key of the current class routine event batch
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch[] findBybatchId_PrevAndNext(
		long classRoutineEventBatchId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException;

	/**
	* Returns all the class routine event batchs where classRoutineEventId = &#63;.
	*
	* @param classRoutineEventId the class routine event ID
	* @return the matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findByClassRoutineEventId(
		long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the class routine event batchs where classRoutineEventId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param classRoutineEventId the class routine event ID
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @return the range of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findByClassRoutineEventId(
		long classRoutineEventId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the class routine event batchs where classRoutineEventId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param classRoutineEventId the class routine event ID
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findByClassRoutineEventId(
		long classRoutineEventId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first class routine event batch in the ordered set where classRoutineEventId = &#63;.
	*
	* @param classRoutineEventId the class routine event ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch findByClassRoutineEventId_First(
		long classRoutineEventId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException;

	/**
	* Returns the first class routine event batch in the ordered set where classRoutineEventId = &#63;.
	*
	* @param classRoutineEventId the class routine event ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch fetchByClassRoutineEventId_First(
		long classRoutineEventId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last class routine event batch in the ordered set where classRoutineEventId = &#63;.
	*
	* @param classRoutineEventId the class routine event ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch findByClassRoutineEventId_Last(
		long classRoutineEventId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException;

	/**
	* Returns the last class routine event batch in the ordered set where classRoutineEventId = &#63;.
	*
	* @param classRoutineEventId the class routine event ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch fetchByClassRoutineEventId_Last(
		long classRoutineEventId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the class routine event batchs before and after the current class routine event batch in the ordered set where classRoutineEventId = &#63;.
	*
	* @param classRoutineEventBatchId the primary key of the current class routine event batch
	* @param classRoutineEventId the class routine event ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.ClassRoutineEventBatch[] findByClassRoutineEventId_PrevAndNext(
		long classRoutineEventBatchId, long classRoutineEventId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException;

	/**
	* Returns all the class routine event batchs.
	*
	* @return the class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the class routine event batchs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @return the range of class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the class routine event batchs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the class routine event batchs where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCompanyId(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the class routine event batchs where batchId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBybatchId(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the class routine event batchs where classRoutineEventId = &#63; from the database.
	*
	* @param classRoutineEventId the class routine event ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByClassRoutineEventId(long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the class routine event batchs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of class routine event batchs where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public int countByCompanyId(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of class routine event batchs where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the number of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public int countBybatchId(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of class routine event batchs where classRoutineEventId = &#63;.
	*
	* @param classRoutineEventId the class routine event ID
	* @return the number of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public int countByClassRoutineEventId(long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of class routine event batchs.
	*
	* @return the number of class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
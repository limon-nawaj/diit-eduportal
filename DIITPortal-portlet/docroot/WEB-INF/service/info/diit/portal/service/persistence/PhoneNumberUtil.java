/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.PhoneNumber;

import java.util.List;

/**
 * The persistence utility for the phone number service. This utility wraps {@link PhoneNumberPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see PhoneNumberPersistence
 * @see PhoneNumberPersistenceImpl
 * @generated
 */
public class PhoneNumberUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(PhoneNumber phoneNumber) {
		getPersistence().clearCache(phoneNumber);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<PhoneNumber> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<PhoneNumber> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<PhoneNumber> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static PhoneNumber update(PhoneNumber phoneNumber, boolean merge)
		throws SystemException {
		return getPersistence().update(phoneNumber, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static PhoneNumber update(PhoneNumber phoneNumber, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(phoneNumber, merge, serviceContext);
	}

	/**
	* Caches the phone number in the entity cache if it is enabled.
	*
	* @param phoneNumber the phone number
	*/
	public static void cacheResult(
		info.diit.portal.model.PhoneNumber phoneNumber) {
		getPersistence().cacheResult(phoneNumber);
	}

	/**
	* Caches the phone numbers in the entity cache if it is enabled.
	*
	* @param phoneNumbers the phone numbers
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.PhoneNumber> phoneNumbers) {
		getPersistence().cacheResult(phoneNumbers);
	}

	/**
	* Creates a new phone number with the primary key. Does not add the phone number to the database.
	*
	* @param phoneNumberId the primary key for the new phone number
	* @return the new phone number
	*/
	public static info.diit.portal.model.PhoneNumber create(long phoneNumberId) {
		return getPersistence().create(phoneNumberId);
	}

	/**
	* Removes the phone number with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param phoneNumberId the primary key of the phone number
	* @return the phone number that was removed
	* @throws info.diit.portal.NoSuchPhoneNumberException if a phone number with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PhoneNumber remove(long phoneNumberId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPhoneNumberException {
		return getPersistence().remove(phoneNumberId);
	}

	public static info.diit.portal.model.PhoneNumber updateImpl(
		info.diit.portal.model.PhoneNumber phoneNumber, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(phoneNumber, merge);
	}

	/**
	* Returns the phone number with the primary key or throws a {@link info.diit.portal.NoSuchPhoneNumberException} if it could not be found.
	*
	* @param phoneNumberId the primary key of the phone number
	* @return the phone number
	* @throws info.diit.portal.NoSuchPhoneNumberException if a phone number with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PhoneNumber findByPrimaryKey(
		long phoneNumberId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPhoneNumberException {
		return getPersistence().findByPrimaryKey(phoneNumberId);
	}

	/**
	* Returns the phone number with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param phoneNumberId the primary key of the phone number
	* @return the phone number, or <code>null</code> if a phone number with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PhoneNumber fetchByPrimaryKey(
		long phoneNumberId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(phoneNumberId);
	}

	/**
	* Returns all the phone numbers where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the matching phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.PhoneNumber> findByStudentPhoneNumberList(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStudentPhoneNumberList(studentId);
	}

	/**
	* Returns a range of all the phone numbers where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of phone numbers
	* @param end the upper bound of the range of phone numbers (not inclusive)
	* @return the range of matching phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.PhoneNumber> findByStudentPhoneNumberList(
		long studentId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentPhoneNumberList(studentId, start, end);
	}

	/**
	* Returns an ordered range of all the phone numbers where studentId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param start the lower bound of the range of phone numbers
	* @param end the upper bound of the range of phone numbers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.PhoneNumber> findByStudentPhoneNumberList(
		long studentId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentPhoneNumberList(studentId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first phone number in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching phone number
	* @throws info.diit.portal.NoSuchPhoneNumberException if a matching phone number could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PhoneNumber findByStudentPhoneNumberList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPhoneNumberException {
		return getPersistence()
				   .findByStudentPhoneNumberList_First(studentId,
			orderByComparator);
	}

	/**
	* Returns the first phone number in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching phone number, or <code>null</code> if a matching phone number could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PhoneNumber fetchByStudentPhoneNumberList_First(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentPhoneNumberList_First(studentId,
			orderByComparator);
	}

	/**
	* Returns the last phone number in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching phone number
	* @throws info.diit.portal.NoSuchPhoneNumberException if a matching phone number could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PhoneNumber findByStudentPhoneNumberList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPhoneNumberException {
		return getPersistence()
				   .findByStudentPhoneNumberList_Last(studentId,
			orderByComparator);
	}

	/**
	* Returns the last phone number in the ordered set where studentId = &#63;.
	*
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching phone number, or <code>null</code> if a matching phone number could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PhoneNumber fetchByStudentPhoneNumberList_Last(
		long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentPhoneNumberList_Last(studentId,
			orderByComparator);
	}

	/**
	* Returns the phone numbers before and after the current phone number in the ordered set where studentId = &#63;.
	*
	* @param phoneNumberId the primary key of the current phone number
	* @param studentId the student ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next phone number
	* @throws info.diit.portal.NoSuchPhoneNumberException if a phone number with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.PhoneNumber[] findByStudentPhoneNumberList_PrevAndNext(
		long phoneNumberId, long studentId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchPhoneNumberException {
		return getPersistence()
				   .findByStudentPhoneNumberList_PrevAndNext(phoneNumberId,
			studentId, orderByComparator);
	}

	/**
	* Returns all the phone numbers.
	*
	* @return the phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.PhoneNumber> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the phone numbers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of phone numbers
	* @param end the upper bound of the range of phone numbers (not inclusive)
	* @return the range of phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.PhoneNumber> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the phone numbers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of phone numbers
	* @param end the upper bound of the range of phone numbers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.PhoneNumber> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the phone numbers where studentId = &#63; from the database.
	*
	* @param studentId the student ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByStudentPhoneNumberList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByStudentPhoneNumberList(studentId);
	}

	/**
	* Removes all the phone numbers from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of phone numbers where studentId = &#63;.
	*
	* @param studentId the student ID
	* @return the number of matching phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStudentPhoneNumberList(long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStudentPhoneNumberList(studentId);
	}

	/**
	* Returns the number of phone numbers.
	*
	* @return the number of phone numbers
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static PhoneNumberPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (PhoneNumberPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					PhoneNumberPersistence.class.getName());

			ReferenceRegistry.registerReference(PhoneNumberUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(PhoneNumberPersistence persistence) {
	}

	private static PhoneNumberPersistence _persistence;
}
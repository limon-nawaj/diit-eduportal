/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.Attendance;

import java.util.List;

/**
 * The persistence utility for the attendance service. This utility wraps {@link AttendancePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see AttendancePersistence
 * @see AttendancePersistenceImpl
 * @generated
 */
public class AttendanceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Attendance attendance) {
		getPersistence().clearCache(attendance);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Attendance> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Attendance> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Attendance> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Attendance update(Attendance attendance, boolean merge)
		throws SystemException {
		return getPersistence().update(attendance, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Attendance update(Attendance attendance, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(attendance, merge, serviceContext);
	}

	/**
	* Caches the attendance in the entity cache if it is enabled.
	*
	* @param attendance the attendance
	*/
	public static void cacheResult(info.diit.portal.model.Attendance attendance) {
		getPersistence().cacheResult(attendance);
	}

	/**
	* Caches the attendances in the entity cache if it is enabled.
	*
	* @param attendances the attendances
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.Attendance> attendances) {
		getPersistence().cacheResult(attendances);
	}

	/**
	* Creates a new attendance with the primary key. Does not add the attendance to the database.
	*
	* @param attendanceId the primary key for the new attendance
	* @return the new attendance
	*/
	public static info.diit.portal.model.Attendance create(long attendanceId) {
		return getPersistence().create(attendanceId);
	}

	/**
	* Removes the attendance with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param attendanceId the primary key of the attendance
	* @return the attendance that was removed
	* @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance remove(long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence().remove(attendanceId);
	}

	public static info.diit.portal.model.Attendance updateImpl(
		info.diit.portal.model.Attendance attendance, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(attendance, merge);
	}

	/**
	* Returns the attendance with the primary key or throws a {@link info.diit.portal.NoSuchAttendanceException} if it could not be found.
	*
	* @param attendanceId the primary key of the attendance
	* @return the attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance findByPrimaryKey(
		long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence().findByPrimaryKey(attendanceId);
	}

	/**
	* Returns the attendance with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param attendanceId the primary key of the attendance
	* @return the attendance, or <code>null</code> if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance fetchByPrimaryKey(
		long attendanceId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(attendanceId);
	}

	/**
	* Returns all the attendances where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId);
	}

	/**
	* Returns a range of all the attendances where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @return the range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the attendances where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first attendance in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence().findByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first attendance in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last attendance in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence().findByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last attendance in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the attendances before and after the current attendance in the ordered set where companyId = &#63;.
	*
	* @param attendanceId the primary key of the current attendance
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance[] findByCompany_PrevAndNext(
		long attendanceId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence()
				   .findByCompany_PrevAndNext(attendanceId, companyId,
			orderByComparator);
	}

	/**
	* Returns all the attendances where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @return the matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findByUserBatchSubject(
		long userId, long batch, long subject)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserBatchSubject(userId, batch, subject);
	}

	/**
	* Returns a range of all the attendances where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @return the range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findByUserBatchSubject(
		long userId, long batch, long subject, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserBatchSubject(userId, batch, subject, start, end);
	}

	/**
	* Returns an ordered range of all the attendances where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findByUserBatchSubject(
		long userId, long batch, long subject, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserBatchSubject(userId, batch, subject, start, end,
			orderByComparator);
	}

	/**
	* Returns the first attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance findByUserBatchSubject_First(
		long userId, long batch, long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence()
				   .findByUserBatchSubject_First(userId, batch, subject,
			orderByComparator);
	}

	/**
	* Returns the first attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance fetchByUserBatchSubject_First(
		long userId, long batch, long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUserBatchSubject_First(userId, batch, subject,
			orderByComparator);
	}

	/**
	* Returns the last attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance findByUserBatchSubject_Last(
		long userId, long batch, long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence()
				   .findByUserBatchSubject_Last(userId, batch, subject,
			orderByComparator);
	}

	/**
	* Returns the last attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance fetchByUserBatchSubject_Last(
		long userId, long batch, long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUserBatchSubject_Last(userId, batch, subject,
			orderByComparator);
	}

	/**
	* Returns the attendances before and after the current attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* @param attendanceId the primary key of the current attendance
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance[] findByUserBatchSubject_PrevAndNext(
		long attendanceId, long userId, long batch, long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence()
				   .findByUserBatchSubject_PrevAndNext(attendanceId, userId,
			batch, subject, orderByComparator);
	}

	/**
	* Returns all the attendances where batch = &#63;.
	*
	* @param batch the batch
	* @return the matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findByBatch(
		long batch) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBatch(batch);
	}

	/**
	* Returns a range of all the attendances where batch = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batch the batch
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @return the range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findByBatch(
		long batch, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBatch(batch, start, end);
	}

	/**
	* Returns an ordered range of all the attendances where batch = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batch the batch
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findByBatch(
		long batch, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBatch(batch, start, end, orderByComparator);
	}

	/**
	* Returns the first attendance in the ordered set where batch = &#63;.
	*
	* @param batch the batch
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance findByBatch_First(
		long batch,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence().findByBatch_First(batch, orderByComparator);
	}

	/**
	* Returns the first attendance in the ordered set where batch = &#63;.
	*
	* @param batch the batch
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance fetchByBatch_First(
		long batch,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByBatch_First(batch, orderByComparator);
	}

	/**
	* Returns the last attendance in the ordered set where batch = &#63;.
	*
	* @param batch the batch
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance findByBatch_Last(
		long batch,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence().findByBatch_Last(batch, orderByComparator);
	}

	/**
	* Returns the last attendance in the ordered set where batch = &#63;.
	*
	* @param batch the batch
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance fetchByBatch_Last(
		long batch,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByBatch_Last(batch, orderByComparator);
	}

	/**
	* Returns the attendances before and after the current attendance in the ordered set where batch = &#63;.
	*
	* @param attendanceId the primary key of the current attendance
	* @param batch the batch
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance[] findByBatch_PrevAndNext(
		long attendanceId, long batch,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence()
				   .findByBatch_PrevAndNext(attendanceId, batch,
			orderByComparator);
	}

	/**
	* Returns the attendance where subject = &#63; and attendanceDate = &#63; or throws a {@link info.diit.portal.NoSuchAttendanceException} if it could not be found.
	*
	* @param subject the subject
	* @param attendanceDate the attendance date
	* @return the matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance findBySubjectDate(
		long subject, java.util.Date attendanceDate)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence().findBySubjectDate(subject, attendanceDate);
	}

	/**
	* Returns the attendance where subject = &#63; and attendanceDate = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param subject the subject
	* @param attendanceDate the attendance date
	* @return the matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance fetchBySubjectDate(
		long subject, java.util.Date attendanceDate)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBySubjectDate(subject, attendanceDate);
	}

	/**
	* Returns the attendance where subject = &#63; and attendanceDate = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param subject the subject
	* @param attendanceDate the attendance date
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance fetchBySubjectDate(
		long subject, java.util.Date attendanceDate, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBySubjectDate(subject, attendanceDate,
			retrieveFromCache);
	}

	/**
	* Returns all the attendances where batch = &#63; and attendanceDate = &#63;.
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @return the matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findByBatchDate(
		long batch, java.util.Date attendanceDate)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByBatchDate(batch, attendanceDate);
	}

	/**
	* Returns a range of all the attendances where batch = &#63; and attendanceDate = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @return the range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findByBatchDate(
		long batch, java.util.Date attendanceDate, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatchDate(batch, attendanceDate, start, end);
	}

	/**
	* Returns an ordered range of all the attendances where batch = &#63; and attendanceDate = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findByBatchDate(
		long batch, java.util.Date attendanceDate, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByBatchDate(batch, attendanceDate, start, end,
			orderByComparator);
	}

	/**
	* Returns the first attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance findByBatchDate_First(
		long batch, java.util.Date attendanceDate,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence()
				   .findByBatchDate_First(batch, attendanceDate,
			orderByComparator);
	}

	/**
	* Returns the first attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance fetchByBatchDate_First(
		long batch, java.util.Date attendanceDate,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBatchDate_First(batch, attendanceDate,
			orderByComparator);
	}

	/**
	* Returns the last attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance findByBatchDate_Last(
		long batch, java.util.Date attendanceDate,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence()
				   .findByBatchDate_Last(batch, attendanceDate,
			orderByComparator);
	}

	/**
	* Returns the last attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching attendance, or <code>null</code> if a matching attendance could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance fetchByBatchDate_Last(
		long batch, java.util.Date attendanceDate,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByBatchDate_Last(batch, attendanceDate,
			orderByComparator);
	}

	/**
	* Returns the attendances before and after the current attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	*
	* @param attendanceId the primary key of the current attendance
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next attendance
	* @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance[] findByBatchDate_PrevAndNext(
		long attendanceId, long batch, java.util.Date attendanceDate,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence()
				   .findByBatchDate_PrevAndNext(attendanceId, batch,
			attendanceDate, orderByComparator);
	}

	/**
	* Returns all the attendances.
	*
	* @return the attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the attendances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @return the range of attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the attendances.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of attendances
	* @param end the upper bound of the range of attendances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of attendances
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.Attendance> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the attendances where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCompany(companyId);
	}

	/**
	* Removes all the attendances where userId = &#63; and batch = &#63; and subject = &#63; from the database.
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUserBatchSubject(long userId, long batch,
		long subject)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUserBatchSubject(userId, batch, subject);
	}

	/**
	* Removes all the attendances where batch = &#63; from the database.
	*
	* @param batch the batch
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBatch(long batch)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBatch(batch);
	}

	/**
	* Removes the attendance where subject = &#63; and attendanceDate = &#63; from the database.
	*
	* @param subject the subject
	* @param attendanceDate the attendance date
	* @return the attendance that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.Attendance removeBySubjectDate(
		long subject, java.util.Date attendanceDate)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAttendanceException {
		return getPersistence().removeBySubjectDate(subject, attendanceDate);
	}

	/**
	* Removes all the attendances where batch = &#63; and attendanceDate = &#63; from the database.
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByBatchDate(long batch,
		java.util.Date attendanceDate)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByBatchDate(batch, attendanceDate);
	}

	/**
	* Removes all the attendances from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of attendances where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCompany(companyId);
	}

	/**
	* Returns the number of attendances where userId = &#63; and batch = &#63; and subject = &#63;.
	*
	* @param userId the user ID
	* @param batch the batch
	* @param subject the subject
	* @return the number of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUserBatchSubject(long userId, long batch,
		long subject)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUserBatchSubject(userId, batch, subject);
	}

	/**
	* Returns the number of attendances where batch = &#63;.
	*
	* @param batch the batch
	* @return the number of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBatch(long batch)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBatch(batch);
	}

	/**
	* Returns the number of attendances where subject = &#63; and attendanceDate = &#63;.
	*
	* @param subject the subject
	* @param attendanceDate the attendance date
	* @return the number of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static int countBySubjectDate(long subject,
		java.util.Date attendanceDate)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBySubjectDate(subject, attendanceDate);
	}

	/**
	* Returns the number of attendances where batch = &#63; and attendanceDate = &#63;.
	*
	* @param batch the batch
	* @param attendanceDate the attendance date
	* @return the number of matching attendances
	* @throws SystemException if a system exception occurred
	*/
	public static int countByBatchDate(long batch, java.util.Date attendanceDate)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByBatchDate(batch, attendanceDate);
	}

	/**
	* Returns the number of attendances.
	*
	* @return the number of attendances
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static AttendancePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (AttendancePersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					AttendancePersistence.class.getName());

			ReferenceRegistry.registerReference(AttendanceUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(AttendancePersistence persistence) {
	}

	private static AttendancePersistence _persistence;
}
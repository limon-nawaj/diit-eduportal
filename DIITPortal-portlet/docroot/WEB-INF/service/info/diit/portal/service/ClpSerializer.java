/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import info.diit.portal.model.AcademicRecordClp;
import info.diit.portal.model.AssessmentClp;
import info.diit.portal.model.AssessmentStudentClp;
import info.diit.portal.model.AssessmentTypeClp;
import info.diit.portal.model.AttendanceClp;
import info.diit.portal.model.AttendanceTopicClp;
import info.diit.portal.model.BatchClp;
import info.diit.portal.model.BatchFeeClp;
import info.diit.portal.model.BatchPaymentScheduleClp;
import info.diit.portal.model.BatchStudentClp;
import info.diit.portal.model.BatchSubjectClp;
import info.diit.portal.model.BatchTeacherClp;
import info.diit.portal.model.ChapterClp;
import info.diit.portal.model.ClassRoutineEventBatchClp;
import info.diit.portal.model.ClassRoutineEventClp;
import info.diit.portal.model.CommentsClp;
import info.diit.portal.model.CommunicationRecordClp;
import info.diit.portal.model.CommunicationStudentRecordClp;
import info.diit.portal.model.CounselingClp;
import info.diit.portal.model.CounselingCourseInterestClp;
import info.diit.portal.model.CourseClp;
import info.diit.portal.model.CourseFeeClp;
import info.diit.portal.model.CourseOrganizationClp;
import info.diit.portal.model.CourseSessionClp;
import info.diit.portal.model.CourseSubjectClp;
import info.diit.portal.model.DailyWorkClp;
import info.diit.portal.model.DesignationClp;
import info.diit.portal.model.EmployeeAttendanceClp;
import info.diit.portal.model.EmployeeClp;
import info.diit.portal.model.EmployeeEmailClp;
import info.diit.portal.model.EmployeeMobileClp;
import info.diit.portal.model.EmployeeRoleClp;
import info.diit.portal.model.EmployeeTimeScheduleClp;
import info.diit.portal.model.ExperianceClp;
import info.diit.portal.model.FeeTypeClp;
import info.diit.portal.model.LeaveCategoryClp;
import info.diit.portal.model.LeaveClp;
import info.diit.portal.model.LeaveDayDetailsClp;
import info.diit.portal.model.LeaveReceiverClp;
import info.diit.portal.model.LessonAssessmentClp;
import info.diit.portal.model.LessonPlanClp;
import info.diit.portal.model.LessonTopicClp;
import info.diit.portal.model.PaymentClp;
import info.diit.portal.model.PersonEmailClp;
import info.diit.portal.model.PhoneNumberClp;
import info.diit.portal.model.RoomClp;
import info.diit.portal.model.StatusHistoryClp;
import info.diit.portal.model.StudentAttendanceClp;
import info.diit.portal.model.StudentClp;
import info.diit.portal.model.StudentDiscountClp;
import info.diit.portal.model.StudentDocumentClp;
import info.diit.portal.model.StudentFeeClp;
import info.diit.portal.model.StudentPaymentScheduleClp;
import info.diit.portal.model.SubjectClp;
import info.diit.portal.model.SubjectLessonClp;
import info.diit.portal.model.TaskClp;
import info.diit.portal.model.TaskDesignationClp;
import info.diit.portal.model.TopicClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Brian Wing Shun Chan
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"DIITPortal-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"DIITPortal-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "DIITPortal-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(AcademicRecordClp.class.getName())) {
			return translateInputAcademicRecord(oldModel);
		}

		if (oldModelClassName.equals(AssessmentClp.class.getName())) {
			return translateInputAssessment(oldModel);
		}

		if (oldModelClassName.equals(AssessmentStudentClp.class.getName())) {
			return translateInputAssessmentStudent(oldModel);
		}

		if (oldModelClassName.equals(AssessmentTypeClp.class.getName())) {
			return translateInputAssessmentType(oldModel);
		}

		if (oldModelClassName.equals(AttendanceClp.class.getName())) {
			return translateInputAttendance(oldModel);
		}

		if (oldModelClassName.equals(AttendanceTopicClp.class.getName())) {
			return translateInputAttendanceTopic(oldModel);
		}

		if (oldModelClassName.equals(BatchClp.class.getName())) {
			return translateInputBatch(oldModel);
		}

		if (oldModelClassName.equals(BatchFeeClp.class.getName())) {
			return translateInputBatchFee(oldModel);
		}

		if (oldModelClassName.equals(BatchPaymentScheduleClp.class.getName())) {
			return translateInputBatchPaymentSchedule(oldModel);
		}

		if (oldModelClassName.equals(BatchStudentClp.class.getName())) {
			return translateInputBatchStudent(oldModel);
		}

		if (oldModelClassName.equals(BatchSubjectClp.class.getName())) {
			return translateInputBatchSubject(oldModel);
		}

		if (oldModelClassName.equals(BatchTeacherClp.class.getName())) {
			return translateInputBatchTeacher(oldModel);
		}

		if (oldModelClassName.equals(ChapterClp.class.getName())) {
			return translateInputChapter(oldModel);
		}

		if (oldModelClassName.equals(ClassRoutineEventClp.class.getName())) {
			return translateInputClassRoutineEvent(oldModel);
		}

		if (oldModelClassName.equals(ClassRoutineEventBatchClp.class.getName())) {
			return translateInputClassRoutineEventBatch(oldModel);
		}

		if (oldModelClassName.equals(CommentsClp.class.getName())) {
			return translateInputComments(oldModel);
		}

		if (oldModelClassName.equals(CommunicationRecordClp.class.getName())) {
			return translateInputCommunicationRecord(oldModel);
		}

		if (oldModelClassName.equals(
					CommunicationStudentRecordClp.class.getName())) {
			return translateInputCommunicationStudentRecord(oldModel);
		}

		if (oldModelClassName.equals(CounselingClp.class.getName())) {
			return translateInputCounseling(oldModel);
		}

		if (oldModelClassName.equals(
					CounselingCourseInterestClp.class.getName())) {
			return translateInputCounselingCourseInterest(oldModel);
		}

		if (oldModelClassName.equals(CourseClp.class.getName())) {
			return translateInputCourse(oldModel);
		}

		if (oldModelClassName.equals(CourseFeeClp.class.getName())) {
			return translateInputCourseFee(oldModel);
		}

		if (oldModelClassName.equals(CourseOrganizationClp.class.getName())) {
			return translateInputCourseOrganization(oldModel);
		}

		if (oldModelClassName.equals(CourseSessionClp.class.getName())) {
			return translateInputCourseSession(oldModel);
		}

		if (oldModelClassName.equals(CourseSubjectClp.class.getName())) {
			return translateInputCourseSubject(oldModel);
		}

		if (oldModelClassName.equals(DailyWorkClp.class.getName())) {
			return translateInputDailyWork(oldModel);
		}

		if (oldModelClassName.equals(DesignationClp.class.getName())) {
			return translateInputDesignation(oldModel);
		}

		if (oldModelClassName.equals(EmployeeClp.class.getName())) {
			return translateInputEmployee(oldModel);
		}

		if (oldModelClassName.equals(EmployeeAttendanceClp.class.getName())) {
			return translateInputEmployeeAttendance(oldModel);
		}

		if (oldModelClassName.equals(EmployeeEmailClp.class.getName())) {
			return translateInputEmployeeEmail(oldModel);
		}

		if (oldModelClassName.equals(EmployeeMobileClp.class.getName())) {
			return translateInputEmployeeMobile(oldModel);
		}

		if (oldModelClassName.equals(EmployeeRoleClp.class.getName())) {
			return translateInputEmployeeRole(oldModel);
		}

		if (oldModelClassName.equals(EmployeeTimeScheduleClp.class.getName())) {
			return translateInputEmployeeTimeSchedule(oldModel);
		}

		if (oldModelClassName.equals(ExperianceClp.class.getName())) {
			return translateInputExperiance(oldModel);
		}

		if (oldModelClassName.equals(FeeTypeClp.class.getName())) {
			return translateInputFeeType(oldModel);
		}

		if (oldModelClassName.equals(LeaveClp.class.getName())) {
			return translateInputLeave(oldModel);
		}

		if (oldModelClassName.equals(LeaveCategoryClp.class.getName())) {
			return translateInputLeaveCategory(oldModel);
		}

		if (oldModelClassName.equals(LeaveDayDetailsClp.class.getName())) {
			return translateInputLeaveDayDetails(oldModel);
		}

		if (oldModelClassName.equals(LeaveReceiverClp.class.getName())) {
			return translateInputLeaveReceiver(oldModel);
		}

		if (oldModelClassName.equals(LessonAssessmentClp.class.getName())) {
			return translateInputLessonAssessment(oldModel);
		}

		if (oldModelClassName.equals(LessonPlanClp.class.getName())) {
			return translateInputLessonPlan(oldModel);
		}

		if (oldModelClassName.equals(LessonTopicClp.class.getName())) {
			return translateInputLessonTopic(oldModel);
		}

		if (oldModelClassName.equals(PaymentClp.class.getName())) {
			return translateInputPayment(oldModel);
		}

		if (oldModelClassName.equals(PersonEmailClp.class.getName())) {
			return translateInputPersonEmail(oldModel);
		}

		if (oldModelClassName.equals(PhoneNumberClp.class.getName())) {
			return translateInputPhoneNumber(oldModel);
		}

		if (oldModelClassName.equals(RoomClp.class.getName())) {
			return translateInputRoom(oldModel);
		}

		if (oldModelClassName.equals(StatusHistoryClp.class.getName())) {
			return translateInputStatusHistory(oldModel);
		}

		if (oldModelClassName.equals(StudentClp.class.getName())) {
			return translateInputStudent(oldModel);
		}

		if (oldModelClassName.equals(StudentAttendanceClp.class.getName())) {
			return translateInputStudentAttendance(oldModel);
		}

		if (oldModelClassName.equals(StudentDiscountClp.class.getName())) {
			return translateInputStudentDiscount(oldModel);
		}

		if (oldModelClassName.equals(StudentDocumentClp.class.getName())) {
			return translateInputStudentDocument(oldModel);
		}

		if (oldModelClassName.equals(StudentFeeClp.class.getName())) {
			return translateInputStudentFee(oldModel);
		}

		if (oldModelClassName.equals(StudentPaymentScheduleClp.class.getName())) {
			return translateInputStudentPaymentSchedule(oldModel);
		}

		if (oldModelClassName.equals(SubjectClp.class.getName())) {
			return translateInputSubject(oldModel);
		}

		if (oldModelClassName.equals(SubjectLessonClp.class.getName())) {
			return translateInputSubjectLesson(oldModel);
		}

		if (oldModelClassName.equals(TaskClp.class.getName())) {
			return translateInputTask(oldModel);
		}

		if (oldModelClassName.equals(TaskDesignationClp.class.getName())) {
			return translateInputTaskDesignation(oldModel);
		}

		if (oldModelClassName.equals(TopicClp.class.getName())) {
			return translateInputTopic(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputAcademicRecord(BaseModel<?> oldModel) {
		AcademicRecordClp oldClpModel = (AcademicRecordClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAcademicRecordRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputAssessment(BaseModel<?> oldModel) {
		AssessmentClp oldClpModel = (AssessmentClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAssessmentRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputAssessmentStudent(BaseModel<?> oldModel) {
		AssessmentStudentClp oldClpModel = (AssessmentStudentClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAssessmentStudentRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputAssessmentType(BaseModel<?> oldModel) {
		AssessmentTypeClp oldClpModel = (AssessmentTypeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAssessmentTypeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputAttendance(BaseModel<?> oldModel) {
		AttendanceClp oldClpModel = (AttendanceClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAttendanceRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputAttendanceTopic(BaseModel<?> oldModel) {
		AttendanceTopicClp oldClpModel = (AttendanceTopicClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAttendanceTopicRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputBatch(BaseModel<?> oldModel) {
		BatchClp oldClpModel = (BatchClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBatchRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputBatchFee(BaseModel<?> oldModel) {
		BatchFeeClp oldClpModel = (BatchFeeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBatchFeeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputBatchPaymentSchedule(
		BaseModel<?> oldModel) {
		BatchPaymentScheduleClp oldClpModel = (BatchPaymentScheduleClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBatchPaymentScheduleRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputBatchStudent(BaseModel<?> oldModel) {
		BatchStudentClp oldClpModel = (BatchStudentClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBatchStudentRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputBatchSubject(BaseModel<?> oldModel) {
		BatchSubjectClp oldClpModel = (BatchSubjectClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBatchSubjectRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputBatchTeacher(BaseModel<?> oldModel) {
		BatchTeacherClp oldClpModel = (BatchTeacherClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBatchTeacherRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputChapter(BaseModel<?> oldModel) {
		ChapterClp oldClpModel = (ChapterClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getChapterRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputClassRoutineEvent(BaseModel<?> oldModel) {
		ClassRoutineEventClp oldClpModel = (ClassRoutineEventClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getClassRoutineEventRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputClassRoutineEventBatch(
		BaseModel<?> oldModel) {
		ClassRoutineEventBatchClp oldClpModel = (ClassRoutineEventBatchClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getClassRoutineEventBatchRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputComments(BaseModel<?> oldModel) {
		CommentsClp oldClpModel = (CommentsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCommentsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCommunicationRecord(
		BaseModel<?> oldModel) {
		CommunicationRecordClp oldClpModel = (CommunicationRecordClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCommunicationRecordRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCommunicationStudentRecord(
		BaseModel<?> oldModel) {
		CommunicationStudentRecordClp oldClpModel = (CommunicationStudentRecordClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCommunicationStudentRecordRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCounseling(BaseModel<?> oldModel) {
		CounselingClp oldClpModel = (CounselingClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCounselingRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCounselingCourseInterest(
		BaseModel<?> oldModel) {
		CounselingCourseInterestClp oldClpModel = (CounselingCourseInterestClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCounselingCourseInterestRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCourse(BaseModel<?> oldModel) {
		CourseClp oldClpModel = (CourseClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCourseRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCourseFee(BaseModel<?> oldModel) {
		CourseFeeClp oldClpModel = (CourseFeeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCourseFeeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCourseOrganization(BaseModel<?> oldModel) {
		CourseOrganizationClp oldClpModel = (CourseOrganizationClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCourseOrganizationRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCourseSession(BaseModel<?> oldModel) {
		CourseSessionClp oldClpModel = (CourseSessionClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCourseSessionRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCourseSubject(BaseModel<?> oldModel) {
		CourseSubjectClp oldClpModel = (CourseSubjectClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCourseSubjectRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputDailyWork(BaseModel<?> oldModel) {
		DailyWorkClp oldClpModel = (DailyWorkClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getDailyWorkRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputDesignation(BaseModel<?> oldModel) {
		DesignationClp oldClpModel = (DesignationClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getDesignationRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEmployee(BaseModel<?> oldModel) {
		EmployeeClp oldClpModel = (EmployeeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEmployeeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEmployeeAttendance(BaseModel<?> oldModel) {
		EmployeeAttendanceClp oldClpModel = (EmployeeAttendanceClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEmployeeAttendanceRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEmployeeEmail(BaseModel<?> oldModel) {
		EmployeeEmailClp oldClpModel = (EmployeeEmailClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEmployeeEmailRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEmployeeMobile(BaseModel<?> oldModel) {
		EmployeeMobileClp oldClpModel = (EmployeeMobileClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEmployeeMobileRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEmployeeRole(BaseModel<?> oldModel) {
		EmployeeRoleClp oldClpModel = (EmployeeRoleClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEmployeeRoleRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEmployeeTimeSchedule(
		BaseModel<?> oldModel) {
		EmployeeTimeScheduleClp oldClpModel = (EmployeeTimeScheduleClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEmployeeTimeScheduleRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputExperiance(BaseModel<?> oldModel) {
		ExperianceClp oldClpModel = (ExperianceClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getExperianceRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputFeeType(BaseModel<?> oldModel) {
		FeeTypeClp oldClpModel = (FeeTypeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getFeeTypeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLeave(BaseModel<?> oldModel) {
		LeaveClp oldClpModel = (LeaveClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLeaveRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLeaveCategory(BaseModel<?> oldModel) {
		LeaveCategoryClp oldClpModel = (LeaveCategoryClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLeaveCategoryRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLeaveDayDetails(BaseModel<?> oldModel) {
		LeaveDayDetailsClp oldClpModel = (LeaveDayDetailsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLeaveDayDetailsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLeaveReceiver(BaseModel<?> oldModel) {
		LeaveReceiverClp oldClpModel = (LeaveReceiverClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLeaveReceiverRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLessonAssessment(BaseModel<?> oldModel) {
		LessonAssessmentClp oldClpModel = (LessonAssessmentClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLessonAssessmentRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLessonPlan(BaseModel<?> oldModel) {
		LessonPlanClp oldClpModel = (LessonPlanClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLessonPlanRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputLessonTopic(BaseModel<?> oldModel) {
		LessonTopicClp oldClpModel = (LessonTopicClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLessonTopicRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputPayment(BaseModel<?> oldModel) {
		PaymentClp oldClpModel = (PaymentClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getPaymentRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputPersonEmail(BaseModel<?> oldModel) {
		PersonEmailClp oldClpModel = (PersonEmailClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getPersonEmailRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputPhoneNumber(BaseModel<?> oldModel) {
		PhoneNumberClp oldClpModel = (PhoneNumberClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getPhoneNumberRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputRoom(BaseModel<?> oldModel) {
		RoomClp oldClpModel = (RoomClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getRoomRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStatusHistory(BaseModel<?> oldModel) {
		StatusHistoryClp oldClpModel = (StatusHistoryClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStatusHistoryRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStudent(BaseModel<?> oldModel) {
		StudentClp oldClpModel = (StudentClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStudentRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStudentAttendance(BaseModel<?> oldModel) {
		StudentAttendanceClp oldClpModel = (StudentAttendanceClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStudentAttendanceRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStudentDiscount(BaseModel<?> oldModel) {
		StudentDiscountClp oldClpModel = (StudentDiscountClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStudentDiscountRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStudentDocument(BaseModel<?> oldModel) {
		StudentDocumentClp oldClpModel = (StudentDocumentClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStudentDocumentRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStudentFee(BaseModel<?> oldModel) {
		StudentFeeClp oldClpModel = (StudentFeeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStudentFeeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputStudentPaymentSchedule(
		BaseModel<?> oldModel) {
		StudentPaymentScheduleClp oldClpModel = (StudentPaymentScheduleClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getStudentPaymentScheduleRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputSubject(BaseModel<?> oldModel) {
		SubjectClp oldClpModel = (SubjectClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getSubjectRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputSubjectLesson(BaseModel<?> oldModel) {
		SubjectLessonClp oldClpModel = (SubjectLessonClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getSubjectLessonRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputTask(BaseModel<?> oldModel) {
		TaskClp oldClpModel = (TaskClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getTaskRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputTaskDesignation(BaseModel<?> oldModel) {
		TaskDesignationClp oldClpModel = (TaskDesignationClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getTaskDesignationRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputTopic(BaseModel<?> oldModel) {
		TopicClp oldClpModel = (TopicClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getTopicRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.AcademicRecordImpl")) {
			return translateOutputAcademicRecord(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.AssessmentImpl")) {
			return translateOutputAssessment(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.AssessmentStudentImpl")) {
			return translateOutputAssessmentStudent(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.AssessmentTypeImpl")) {
			return translateOutputAssessmentType(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.AttendanceImpl")) {
			return translateOutputAttendance(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.AttendanceTopicImpl")) {
			return translateOutputAttendanceTopic(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.BatchImpl")) {
			return translateOutputBatch(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.BatchFeeImpl")) {
			return translateOutputBatchFee(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.BatchPaymentScheduleImpl")) {
			return translateOutputBatchPaymentSchedule(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.BatchStudentImpl")) {
			return translateOutputBatchStudent(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.BatchSubjectImpl")) {
			return translateOutputBatchSubject(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.BatchTeacherImpl")) {
			return translateOutputBatchTeacher(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.ChapterImpl")) {
			return translateOutputChapter(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.ClassRoutineEventImpl")) {
			return translateOutputClassRoutineEvent(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.ClassRoutineEventBatchImpl")) {
			return translateOutputClassRoutineEventBatch(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.CommentsImpl")) {
			return translateOutputComments(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.CommunicationRecordImpl")) {
			return translateOutputCommunicationRecord(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.CommunicationStudentRecordImpl")) {
			return translateOutputCommunicationStudentRecord(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.CounselingImpl")) {
			return translateOutputCounseling(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.CounselingCourseInterestImpl")) {
			return translateOutputCounselingCourseInterest(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.CourseImpl")) {
			return translateOutputCourse(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.CourseFeeImpl")) {
			return translateOutputCourseFee(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.CourseOrganizationImpl")) {
			return translateOutputCourseOrganization(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.CourseSessionImpl")) {
			return translateOutputCourseSession(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.CourseSubjectImpl")) {
			return translateOutputCourseSubject(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.DailyWorkImpl")) {
			return translateOutputDailyWork(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.DesignationImpl")) {
			return translateOutputDesignation(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.EmployeeImpl")) {
			return translateOutputEmployee(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.EmployeeAttendanceImpl")) {
			return translateOutputEmployeeAttendance(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.EmployeeEmailImpl")) {
			return translateOutputEmployeeEmail(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.EmployeeMobileImpl")) {
			return translateOutputEmployeeMobile(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.EmployeeRoleImpl")) {
			return translateOutputEmployeeRole(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.EmployeeTimeScheduleImpl")) {
			return translateOutputEmployeeTimeSchedule(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.ExperianceImpl")) {
			return translateOutputExperiance(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.FeeTypeImpl")) {
			return translateOutputFeeType(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.LeaveImpl")) {
			return translateOutputLeave(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.LeaveCategoryImpl")) {
			return translateOutputLeaveCategory(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.LeaveDayDetailsImpl")) {
			return translateOutputLeaveDayDetails(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.LeaveReceiverImpl")) {
			return translateOutputLeaveReceiver(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.LessonAssessmentImpl")) {
			return translateOutputLessonAssessment(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.LessonPlanImpl")) {
			return translateOutputLessonPlan(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.LessonTopicImpl")) {
			return translateOutputLessonTopic(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.PaymentImpl")) {
			return translateOutputPayment(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.PersonEmailImpl")) {
			return translateOutputPersonEmail(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.PhoneNumberImpl")) {
			return translateOutputPhoneNumber(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.RoomImpl")) {
			return translateOutputRoom(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.StatusHistoryImpl")) {
			return translateOutputStatusHistory(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.StudentImpl")) {
			return translateOutputStudent(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.StudentAttendanceImpl")) {
			return translateOutputStudentAttendance(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.StudentDiscountImpl")) {
			return translateOutputStudentDiscount(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.StudentDocumentImpl")) {
			return translateOutputStudentDocument(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.StudentFeeImpl")) {
			return translateOutputStudentFee(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.StudentPaymentScheduleImpl")) {
			return translateOutputStudentPaymentSchedule(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.SubjectImpl")) {
			return translateOutputSubject(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.SubjectLessonImpl")) {
			return translateOutputSubjectLesson(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.TaskImpl")) {
			return translateOutputTask(oldModel);
		}

		if (oldModelClassName.equals(
					"info.diit.portal.model.impl.TaskDesignationImpl")) {
			return translateOutputTaskDesignation(oldModel);
		}

		if (oldModelClassName.equals("info.diit.portal.model.impl.TopicImpl")) {
			return translateOutputTopic(oldModel);
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals("info.diit.portal.NoSuchAcademicRecordException")) {
			return new info.diit.portal.NoSuchAcademicRecordException();
		}

		if (className.equals("info.diit.portal.NoSuchAssessmentException")) {
			return new info.diit.portal.NoSuchAssessmentException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchAssessmentStudentException")) {
			return new info.diit.portal.NoSuchAssessmentStudentException();
		}

		if (className.equals("info.diit.portal.NoSuchAssessmentTypeException")) {
			return new info.diit.portal.NoSuchAssessmentTypeException();
		}

		if (className.equals("info.diit.portal.NoSuchAttendanceException")) {
			return new info.diit.portal.NoSuchAttendanceException();
		}

		if (className.equals("info.diit.portal.NoSuchAttendanceTopicException")) {
			return new info.diit.portal.NoSuchAttendanceTopicException();
		}

		if (className.equals("info.diit.portal.NoSuchBatchException")) {
			return new info.diit.portal.NoSuchBatchException();
		}

		if (className.equals("info.diit.portal.NoSuchBatchFeeException")) {
			return new info.diit.portal.NoSuchBatchFeeException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchBatchPaymentScheduleException")) {
			return new info.diit.portal.NoSuchBatchPaymentScheduleException();
		}

		if (className.equals("info.diit.portal.NoSuchBatchStudentException")) {
			return new info.diit.portal.NoSuchBatchStudentException();
		}

		if (className.equals("info.diit.portal.NoSuchBatchSubjectException")) {
			return new info.diit.portal.NoSuchBatchSubjectException();
		}

		if (className.equals("info.diit.portal.NoSuchBatchTeacherException")) {
			return new info.diit.portal.NoSuchBatchTeacherException();
		}

		if (className.equals("info.diit.portal.NoSuchChapterException")) {
			return new info.diit.portal.NoSuchChapterException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchClassRoutineEventException")) {
			return new info.diit.portal.NoSuchClassRoutineEventException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchClassRoutineEventBatchException")) {
			return new info.diit.portal.NoSuchClassRoutineEventBatchException();
		}

		if (className.equals("info.diit.portal.NoSuchCommentsException")) {
			return new info.diit.portal.NoSuchCommentsException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchCommunicationRecordException")) {
			return new info.diit.portal.NoSuchCommunicationRecordException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchCommunicationStudentRecordException")) {
			return new info.diit.portal.NoSuchCommunicationStudentRecordException();
		}

		if (className.equals("info.diit.portal.NoSuchCounselingException")) {
			return new info.diit.portal.NoSuchCounselingException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchCounselingCourseInterestException")) {
			return new info.diit.portal.NoSuchCounselingCourseInterestException();
		}

		if (className.equals("info.diit.portal.NoSuchCourseException")) {
			return new info.diit.portal.NoSuchCourseException();
		}

		if (className.equals("info.diit.portal.NoSuchCourseFeeException")) {
			return new info.diit.portal.NoSuchCourseFeeException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchCourseOrganizationException")) {
			return new info.diit.portal.NoSuchCourseOrganizationException();
		}

		if (className.equals("info.diit.portal.NoSuchCourseSessionException")) {
			return new info.diit.portal.NoSuchCourseSessionException();
		}

		if (className.equals("info.diit.portal.NoSuchCourseSubjectException")) {
			return new info.diit.portal.NoSuchCourseSubjectException();
		}

		if (className.equals("info.diit.portal.NoSuchDailyWorkException")) {
			return new info.diit.portal.NoSuchDailyWorkException();
		}

		if (className.equals("info.diit.portal.NoSuchDesignationException")) {
			return new info.diit.portal.NoSuchDesignationException();
		}

		if (className.equals("info.diit.portal.NoSuchEmployeeException")) {
			return new info.diit.portal.NoSuchEmployeeException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchEmployeeAttendanceException")) {
			return new info.diit.portal.NoSuchEmployeeAttendanceException();
		}

		if (className.equals("info.diit.portal.NoSuchEmployeeEmailException")) {
			return new info.diit.portal.NoSuchEmployeeEmailException();
		}

		if (className.equals("info.diit.portal.NoSuchEmployeeMobileException")) {
			return new info.diit.portal.NoSuchEmployeeMobileException();
		}

		if (className.equals("info.diit.portal.NoSuchEmployeeRoleException")) {
			return new info.diit.portal.NoSuchEmployeeRoleException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchEmployeeTimeScheduleException")) {
			return new info.diit.portal.NoSuchEmployeeTimeScheduleException();
		}

		if (className.equals("info.diit.portal.NoSuchExperianceException")) {
			return new info.diit.portal.NoSuchExperianceException();
		}

		if (className.equals("info.diit.portal.NoSuchFeeTypeException")) {
			return new info.diit.portal.NoSuchFeeTypeException();
		}

		if (className.equals("info.diit.portal.NoSuchLeaveException")) {
			return new info.diit.portal.NoSuchLeaveException();
		}

		if (className.equals("info.diit.portal.NoSuchLeaveCategoryException")) {
			return new info.diit.portal.NoSuchLeaveCategoryException();
		}

		if (className.equals("info.diit.portal.NoSuchLeaveDayDetailsException")) {
			return new info.diit.portal.NoSuchLeaveDayDetailsException();
		}

		if (className.equals("info.diit.portal.NoSuchLeaveReceiverException")) {
			return new info.diit.portal.NoSuchLeaveReceiverException();
		}

		if (className.equals("info.diit.portal.NoSuchLessonAssessmentException")) {
			return new info.diit.portal.NoSuchLessonAssessmentException();
		}

		if (className.equals("info.diit.portal.NoSuchLessonPlanException")) {
			return new info.diit.portal.NoSuchLessonPlanException();
		}

		if (className.equals("info.diit.portal.NoSuchLessonTopicException")) {
			return new info.diit.portal.NoSuchLessonTopicException();
		}

		if (className.equals("info.diit.portal.NoSuchPaymentException")) {
			return new info.diit.portal.NoSuchPaymentException();
		}

		if (className.equals("info.diit.portal.NoSuchPersonEmailException")) {
			return new info.diit.portal.NoSuchPersonEmailException();
		}

		if (className.equals("info.diit.portal.NoSuchPhoneNumberException")) {
			return new info.diit.portal.NoSuchPhoneNumberException();
		}

		if (className.equals("info.diit.portal.NoSuchRoomException")) {
			return new info.diit.portal.NoSuchRoomException();
		}

		if (className.equals("info.diit.portal.NoSuchStatusHistoryException")) {
			return new info.diit.portal.NoSuchStatusHistoryException();
		}

		if (className.equals("info.diit.portal.NoSuchStudentException")) {
			return new info.diit.portal.NoSuchStudentException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchStudentAttendanceException")) {
			return new info.diit.portal.NoSuchStudentAttendanceException();
		}

		if (className.equals("info.diit.portal.NoSuchStudentDiscountException")) {
			return new info.diit.portal.NoSuchStudentDiscountException();
		}

		if (className.equals("info.diit.portal.NoSuchStudentDocumentException")) {
			return new info.diit.portal.NoSuchStudentDocumentException();
		}

		if (className.equals("info.diit.portal.NoSuchStudentFeeException")) {
			return new info.diit.portal.NoSuchStudentFeeException();
		}

		if (className.equals(
					"info.diit.portal.NoSuchStudentPaymentScheduleException")) {
			return new info.diit.portal.NoSuchStudentPaymentScheduleException();
		}

		if (className.equals("info.diit.portal.NoSuchSubjectException")) {
			return new info.diit.portal.NoSuchSubjectException();
		}

		if (className.equals("info.diit.portal.NoSuchSubjectLessonException")) {
			return new info.diit.portal.NoSuchSubjectLessonException();
		}

		if (className.equals("info.diit.portal.NoSuchTaskException")) {
			return new info.diit.portal.NoSuchTaskException();
		}

		if (className.equals("info.diit.portal.NoSuchTaskDesignationException")) {
			return new info.diit.portal.NoSuchTaskDesignationException();
		}

		if (className.equals("info.diit.portal.NoSuchTopicException")) {
			return new info.diit.portal.NoSuchTopicException();
		}

		return throwable;
	}

	public static Object translateOutputAcademicRecord(BaseModel<?> oldModel) {
		AcademicRecordClp newModel = new AcademicRecordClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAcademicRecordRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputAssessment(BaseModel<?> oldModel) {
		AssessmentClp newModel = new AssessmentClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAssessmentRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputAssessmentStudent(BaseModel<?> oldModel) {
		AssessmentStudentClp newModel = new AssessmentStudentClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAssessmentStudentRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputAssessmentType(BaseModel<?> oldModel) {
		AssessmentTypeClp newModel = new AssessmentTypeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAssessmentTypeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputAttendance(BaseModel<?> oldModel) {
		AttendanceClp newModel = new AttendanceClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAttendanceRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputAttendanceTopic(BaseModel<?> oldModel) {
		AttendanceTopicClp newModel = new AttendanceTopicClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAttendanceTopicRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputBatch(BaseModel<?> oldModel) {
		BatchClp newModel = new BatchClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBatchRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputBatchFee(BaseModel<?> oldModel) {
		BatchFeeClp newModel = new BatchFeeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBatchFeeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputBatchPaymentSchedule(
		BaseModel<?> oldModel) {
		BatchPaymentScheduleClp newModel = new BatchPaymentScheduleClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBatchPaymentScheduleRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputBatchStudent(BaseModel<?> oldModel) {
		BatchStudentClp newModel = new BatchStudentClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBatchStudentRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputBatchSubject(BaseModel<?> oldModel) {
		BatchSubjectClp newModel = new BatchSubjectClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBatchSubjectRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputBatchTeacher(BaseModel<?> oldModel) {
		BatchTeacherClp newModel = new BatchTeacherClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBatchTeacherRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputChapter(BaseModel<?> oldModel) {
		ChapterClp newModel = new ChapterClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setChapterRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputClassRoutineEvent(BaseModel<?> oldModel) {
		ClassRoutineEventClp newModel = new ClassRoutineEventClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setClassRoutineEventRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputClassRoutineEventBatch(
		BaseModel<?> oldModel) {
		ClassRoutineEventBatchClp newModel = new ClassRoutineEventBatchClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setClassRoutineEventBatchRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputComments(BaseModel<?> oldModel) {
		CommentsClp newModel = new CommentsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCommentsRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCommunicationRecord(
		BaseModel<?> oldModel) {
		CommunicationRecordClp newModel = new CommunicationRecordClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCommunicationRecordRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCommunicationStudentRecord(
		BaseModel<?> oldModel) {
		CommunicationStudentRecordClp newModel = new CommunicationStudentRecordClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCommunicationStudentRecordRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCounseling(BaseModel<?> oldModel) {
		CounselingClp newModel = new CounselingClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCounselingRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCounselingCourseInterest(
		BaseModel<?> oldModel) {
		CounselingCourseInterestClp newModel = new CounselingCourseInterestClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCounselingCourseInterestRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCourse(BaseModel<?> oldModel) {
		CourseClp newModel = new CourseClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCourseRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCourseFee(BaseModel<?> oldModel) {
		CourseFeeClp newModel = new CourseFeeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCourseFeeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCourseOrganization(
		BaseModel<?> oldModel) {
		CourseOrganizationClp newModel = new CourseOrganizationClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCourseOrganizationRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCourseSession(BaseModel<?> oldModel) {
		CourseSessionClp newModel = new CourseSessionClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCourseSessionRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCourseSubject(BaseModel<?> oldModel) {
		CourseSubjectClp newModel = new CourseSubjectClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCourseSubjectRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputDailyWork(BaseModel<?> oldModel) {
		DailyWorkClp newModel = new DailyWorkClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setDailyWorkRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputDesignation(BaseModel<?> oldModel) {
		DesignationClp newModel = new DesignationClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setDesignationRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEmployee(BaseModel<?> oldModel) {
		EmployeeClp newModel = new EmployeeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEmployeeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEmployeeAttendance(
		BaseModel<?> oldModel) {
		EmployeeAttendanceClp newModel = new EmployeeAttendanceClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEmployeeAttendanceRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEmployeeEmail(BaseModel<?> oldModel) {
		EmployeeEmailClp newModel = new EmployeeEmailClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEmployeeEmailRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEmployeeMobile(BaseModel<?> oldModel) {
		EmployeeMobileClp newModel = new EmployeeMobileClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEmployeeMobileRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEmployeeRole(BaseModel<?> oldModel) {
		EmployeeRoleClp newModel = new EmployeeRoleClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEmployeeRoleRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEmployeeTimeSchedule(
		BaseModel<?> oldModel) {
		EmployeeTimeScheduleClp newModel = new EmployeeTimeScheduleClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEmployeeTimeScheduleRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputExperiance(BaseModel<?> oldModel) {
		ExperianceClp newModel = new ExperianceClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setExperianceRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputFeeType(BaseModel<?> oldModel) {
		FeeTypeClp newModel = new FeeTypeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setFeeTypeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLeave(BaseModel<?> oldModel) {
		LeaveClp newModel = new LeaveClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLeaveRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLeaveCategory(BaseModel<?> oldModel) {
		LeaveCategoryClp newModel = new LeaveCategoryClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLeaveCategoryRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLeaveDayDetails(BaseModel<?> oldModel) {
		LeaveDayDetailsClp newModel = new LeaveDayDetailsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLeaveDayDetailsRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLeaveReceiver(BaseModel<?> oldModel) {
		LeaveReceiverClp newModel = new LeaveReceiverClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLeaveReceiverRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLessonAssessment(BaseModel<?> oldModel) {
		LessonAssessmentClp newModel = new LessonAssessmentClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLessonAssessmentRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLessonPlan(BaseModel<?> oldModel) {
		LessonPlanClp newModel = new LessonPlanClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLessonPlanRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputLessonTopic(BaseModel<?> oldModel) {
		LessonTopicClp newModel = new LessonTopicClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLessonTopicRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputPayment(BaseModel<?> oldModel) {
		PaymentClp newModel = new PaymentClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setPaymentRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputPersonEmail(BaseModel<?> oldModel) {
		PersonEmailClp newModel = new PersonEmailClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setPersonEmailRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputPhoneNumber(BaseModel<?> oldModel) {
		PhoneNumberClp newModel = new PhoneNumberClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setPhoneNumberRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputRoom(BaseModel<?> oldModel) {
		RoomClp newModel = new RoomClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setRoomRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStatusHistory(BaseModel<?> oldModel) {
		StatusHistoryClp newModel = new StatusHistoryClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStatusHistoryRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStudent(BaseModel<?> oldModel) {
		StudentClp newModel = new StudentClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStudentRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStudentAttendance(BaseModel<?> oldModel) {
		StudentAttendanceClp newModel = new StudentAttendanceClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStudentAttendanceRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStudentDiscount(BaseModel<?> oldModel) {
		StudentDiscountClp newModel = new StudentDiscountClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStudentDiscountRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStudentDocument(BaseModel<?> oldModel) {
		StudentDocumentClp newModel = new StudentDocumentClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStudentDocumentRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStudentFee(BaseModel<?> oldModel) {
		StudentFeeClp newModel = new StudentFeeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStudentFeeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputStudentPaymentSchedule(
		BaseModel<?> oldModel) {
		StudentPaymentScheduleClp newModel = new StudentPaymentScheduleClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setStudentPaymentScheduleRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputSubject(BaseModel<?> oldModel) {
		SubjectClp newModel = new SubjectClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setSubjectRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputSubjectLesson(BaseModel<?> oldModel) {
		SubjectLessonClp newModel = new SubjectLessonClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setSubjectLessonRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputTask(BaseModel<?> oldModel) {
		TaskClp newModel = new TaskClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setTaskRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputTaskDesignation(BaseModel<?> oldModel) {
		TaskDesignationClp newModel = new TaskDesignationClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setTaskDesignationRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputTopic(BaseModel<?> oldModel) {
		TopicClp newModel = new TopicClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setTopicRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}
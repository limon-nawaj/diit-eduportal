/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.CourseFee;

import java.util.List;

/**
 * The persistence utility for the course fee service. This utility wraps {@link CourseFeePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see CourseFeePersistence
 * @see CourseFeePersistenceImpl
 * @generated
 */
public class CourseFeeUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CourseFee courseFee) {
		getPersistence().clearCache(courseFee);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CourseFee> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CourseFee> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CourseFee> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static CourseFee update(CourseFee courseFee, boolean merge)
		throws SystemException {
		return getPersistence().update(courseFee, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static CourseFee update(CourseFee courseFee, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(courseFee, merge, serviceContext);
	}

	/**
	* Caches the course fee in the entity cache if it is enabled.
	*
	* @param courseFee the course fee
	*/
	public static void cacheResult(info.diit.portal.model.CourseFee courseFee) {
		getPersistence().cacheResult(courseFee);
	}

	/**
	* Caches the course fees in the entity cache if it is enabled.
	*
	* @param courseFees the course fees
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.CourseFee> courseFees) {
		getPersistence().cacheResult(courseFees);
	}

	/**
	* Creates a new course fee with the primary key. Does not add the course fee to the database.
	*
	* @param courseFeeId the primary key for the new course fee
	* @return the new course fee
	*/
	public static info.diit.portal.model.CourseFee create(long courseFeeId) {
		return getPersistence().create(courseFeeId);
	}

	/**
	* Removes the course fee with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param courseFeeId the primary key of the course fee
	* @return the course fee that was removed
	* @throws info.diit.portal.NoSuchCourseFeeException if a course fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseFee remove(long courseFeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseFeeException {
		return getPersistence().remove(courseFeeId);
	}

	public static info.diit.portal.model.CourseFee updateImpl(
		info.diit.portal.model.CourseFee courseFee, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(courseFee, merge);
	}

	/**
	* Returns the course fee with the primary key or throws a {@link info.diit.portal.NoSuchCourseFeeException} if it could not be found.
	*
	* @param courseFeeId the primary key of the course fee
	* @return the course fee
	* @throws info.diit.portal.NoSuchCourseFeeException if a course fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseFee findByPrimaryKey(
		long courseFeeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseFeeException {
		return getPersistence().findByPrimaryKey(courseFeeId);
	}

	/**
	* Returns the course fee with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param courseFeeId the primary key of the course fee
	* @return the course fee, or <code>null</code> if a course fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseFee fetchByPrimaryKey(
		long courseFeeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(courseFeeId);
	}

	/**
	* Returns all the course fees where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the matching course fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseFee> findByCourse(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCourse(courseId);
	}

	/**
	* Returns a range of all the course fees where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course fees
	* @param end the upper bound of the range of course fees (not inclusive)
	* @return the range of matching course fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseFee> findByCourse(
		long courseId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCourse(courseId, start, end);
	}

	/**
	* Returns an ordered range of all the course fees where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course fees
	* @param end the upper bound of the range of course fees (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseFee> findByCourse(
		long courseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCourse(courseId, start, end, orderByComparator);
	}

	/**
	* Returns the first course fee in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course fee
	* @throws info.diit.portal.NoSuchCourseFeeException if a matching course fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseFee findByCourse_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseFeeException {
		return getPersistence().findByCourse_First(courseId, orderByComparator);
	}

	/**
	* Returns the first course fee in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course fee, or <code>null</code> if a matching course fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseFee fetchByCourse_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCourse_First(courseId, orderByComparator);
	}

	/**
	* Returns the last course fee in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course fee
	* @throws info.diit.portal.NoSuchCourseFeeException if a matching course fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseFee findByCourse_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseFeeException {
		return getPersistence().findByCourse_Last(courseId, orderByComparator);
	}

	/**
	* Returns the last course fee in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course fee, or <code>null</code> if a matching course fee could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseFee fetchByCourse_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByCourse_Last(courseId, orderByComparator);
	}

	/**
	* Returns the course fees before and after the current course fee in the ordered set where courseId = &#63;.
	*
	* @param courseFeeId the primary key of the current course fee
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course fee
	* @throws info.diit.portal.NoSuchCourseFeeException if a course fee with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CourseFee[] findByCourse_PrevAndNext(
		long courseFeeId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseFeeException {
		return getPersistence()
				   .findByCourse_PrevAndNext(courseFeeId, courseId,
			orderByComparator);
	}

	/**
	* Returns all the course fees.
	*
	* @return the course fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseFee> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the course fees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course fees
	* @param end the upper bound of the range of course fees (not inclusive)
	* @return the range of course fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseFee> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the course fees.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course fees
	* @param end the upper bound of the range of course fees (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of course fees
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CourseFee> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the course fees where courseId = &#63; from the database.
	*
	* @param courseId the course ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCourse(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCourse(courseId);
	}

	/**
	* Removes all the course fees from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of course fees where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the number of matching course fees
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCourse(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCourse(courseId);
	}

	/**
	* Returns the number of course fees.
	*
	* @return the number of course fees
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CourseFeePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CourseFeePersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					CourseFeePersistence.class.getName());

			ReferenceRegistry.registerReference(CourseFeeUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(CourseFeePersistence persistence) {
	}

	private static CourseFeePersistence _persistence;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.LessonAssessment;

import java.util.List;

/**
 * The persistence utility for the lesson assessment service. This utility wraps {@link LessonAssessmentPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see LessonAssessmentPersistence
 * @see LessonAssessmentPersistenceImpl
 * @generated
 */
public class LessonAssessmentUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(LessonAssessment lessonAssessment) {
		getPersistence().clearCache(lessonAssessment);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<LessonAssessment> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<LessonAssessment> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<LessonAssessment> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static LessonAssessment update(LessonAssessment lessonAssessment,
		boolean merge) throws SystemException {
		return getPersistence().update(lessonAssessment, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static LessonAssessment update(LessonAssessment lessonAssessment,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(lessonAssessment, merge, serviceContext);
	}

	/**
	* Caches the lesson assessment in the entity cache if it is enabled.
	*
	* @param lessonAssessment the lesson assessment
	*/
	public static void cacheResult(
		info.diit.portal.model.LessonAssessment lessonAssessment) {
		getPersistence().cacheResult(lessonAssessment);
	}

	/**
	* Caches the lesson assessments in the entity cache if it is enabled.
	*
	* @param lessonAssessments the lesson assessments
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.LessonAssessment> lessonAssessments) {
		getPersistence().cacheResult(lessonAssessments);
	}

	/**
	* Creates a new lesson assessment with the primary key. Does not add the lesson assessment to the database.
	*
	* @param lessonAssessmentId the primary key for the new lesson assessment
	* @return the new lesson assessment
	*/
	public static info.diit.portal.model.LessonAssessment create(
		long lessonAssessmentId) {
		return getPersistence().create(lessonAssessmentId);
	}

	/**
	* Removes the lesson assessment with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonAssessmentId the primary key of the lesson assessment
	* @return the lesson assessment that was removed
	* @throws info.diit.portal.NoSuchLessonAssessmentException if a lesson assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonAssessment remove(
		long lessonAssessmentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonAssessmentException {
		return getPersistence().remove(lessonAssessmentId);
	}

	public static info.diit.portal.model.LessonAssessment updateImpl(
		info.diit.portal.model.LessonAssessment lessonAssessment, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(lessonAssessment, merge);
	}

	/**
	* Returns the lesson assessment with the primary key or throws a {@link info.diit.portal.NoSuchLessonAssessmentException} if it could not be found.
	*
	* @param lessonAssessmentId the primary key of the lesson assessment
	* @return the lesson assessment
	* @throws info.diit.portal.NoSuchLessonAssessmentException if a lesson assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonAssessment findByPrimaryKey(
		long lessonAssessmentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonAssessmentException {
		return getPersistence().findByPrimaryKey(lessonAssessmentId);
	}

	/**
	* Returns the lesson assessment with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param lessonAssessmentId the primary key of the lesson assessment
	* @return the lesson assessment, or <code>null</code> if a lesson assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonAssessment fetchByPrimaryKey(
		long lessonAssessmentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(lessonAssessmentId);
	}

	/**
	* Returns all the lesson assessments where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @return the matching lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonAssessment> findByLessonPlan(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLessonPlan(lessonPlanId);
	}

	/**
	* Returns a range of all the lesson assessments where lessonPlanId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param lessonPlanId the lesson plan ID
	* @param start the lower bound of the range of lesson assessments
	* @param end the upper bound of the range of lesson assessments (not inclusive)
	* @return the range of matching lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonAssessment> findByLessonPlan(
		long lessonPlanId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByLessonPlan(lessonPlanId, start, end);
	}

	/**
	* Returns an ordered range of all the lesson assessments where lessonPlanId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param lessonPlanId the lesson plan ID
	* @param start the lower bound of the range of lesson assessments
	* @param end the upper bound of the range of lesson assessments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonAssessment> findByLessonPlan(
		long lessonPlanId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByLessonPlan(lessonPlanId, start, end, orderByComparator);
	}

	/**
	* Returns the first lesson assessment in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson assessment
	* @throws info.diit.portal.NoSuchLessonAssessmentException if a matching lesson assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonAssessment findByLessonPlan_First(
		long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonAssessmentException {
		return getPersistence()
				   .findByLessonPlan_First(lessonPlanId, orderByComparator);
	}

	/**
	* Returns the first lesson assessment in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson assessment, or <code>null</code> if a matching lesson assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonAssessment fetchByLessonPlan_First(
		long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLessonPlan_First(lessonPlanId, orderByComparator);
	}

	/**
	* Returns the last lesson assessment in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson assessment
	* @throws info.diit.portal.NoSuchLessonAssessmentException if a matching lesson assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonAssessment findByLessonPlan_Last(
		long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonAssessmentException {
		return getPersistence()
				   .findByLessonPlan_Last(lessonPlanId, orderByComparator);
	}

	/**
	* Returns the last lesson assessment in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson assessment, or <code>null</code> if a matching lesson assessment could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonAssessment fetchByLessonPlan_Last(
		long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByLessonPlan_Last(lessonPlanId, orderByComparator);
	}

	/**
	* Returns the lesson assessments before and after the current lesson assessment in the ordered set where lessonPlanId = &#63;.
	*
	* @param lessonAssessmentId the primary key of the current lesson assessment
	* @param lessonPlanId the lesson plan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lesson assessment
	* @throws info.diit.portal.NoSuchLessonAssessmentException if a lesson assessment with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.LessonAssessment[] findByLessonPlan_PrevAndNext(
		long lessonAssessmentId, long lessonPlanId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonAssessmentException {
		return getPersistence()
				   .findByLessonPlan_PrevAndNext(lessonAssessmentId,
			lessonPlanId, orderByComparator);
	}

	/**
	* Returns all the lesson assessments.
	*
	* @return the lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonAssessment> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the lesson assessments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson assessments
	* @param end the upper bound of the range of lesson assessments (not inclusive)
	* @return the range of lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonAssessment> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the lesson assessments.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson assessments
	* @param end the upper bound of the range of lesson assessments (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.LessonAssessment> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the lesson assessments where lessonPlanId = &#63; from the database.
	*
	* @param lessonPlanId the lesson plan ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByLessonPlan(long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByLessonPlan(lessonPlanId);
	}

	/**
	* Removes all the lesson assessments from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of lesson assessments where lessonPlanId = &#63;.
	*
	* @param lessonPlanId the lesson plan ID
	* @return the number of matching lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public static int countByLessonPlan(long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByLessonPlan(lessonPlanId);
	}

	/**
	* Returns the number of lesson assessments.
	*
	* @return the number of lesson assessments
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static LessonAssessmentPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (LessonAssessmentPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					LessonAssessmentPersistence.class.getName());

			ReferenceRegistry.registerReference(LessonAssessmentUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(LessonAssessmentPersistence persistence) {
	}

	private static LessonAssessmentPersistence _persistence;
}
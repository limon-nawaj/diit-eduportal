/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.BatchTeacher;

/**
 * The persistence interface for the batch teacher service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see BatchTeacherPersistenceImpl
 * @see BatchTeacherUtil
 * @generated
 */
public interface BatchTeacherPersistence extends BasePersistence<BatchTeacher> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BatchTeacherUtil} to access the batch teacher persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the batch teacher in the entity cache if it is enabled.
	*
	* @param batchTeacher the batch teacher
	*/
	public void cacheResult(info.diit.portal.model.BatchTeacher batchTeacher);

	/**
	* Caches the batch teachers in the entity cache if it is enabled.
	*
	* @param batchTeachers the batch teachers
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.BatchTeacher> batchTeachers);

	/**
	* Creates a new batch teacher with the primary key. Does not add the batch teacher to the database.
	*
	* @param batchTeacherId the primary key for the new batch teacher
	* @return the new batch teacher
	*/
	public info.diit.portal.model.BatchTeacher create(long batchTeacherId);

	/**
	* Removes the batch teacher with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param batchTeacherId the primary key of the batch teacher
	* @return the batch teacher that was removed
	* @throws info.diit.portal.NoSuchBatchTeacherException if a batch teacher with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchTeacher remove(long batchTeacherId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchTeacherException;

	public info.diit.portal.model.BatchTeacher updateImpl(
		info.diit.portal.model.BatchTeacher batchTeacher, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch teacher with the primary key or throws a {@link info.diit.portal.NoSuchBatchTeacherException} if it could not be found.
	*
	* @param batchTeacherId the primary key of the batch teacher
	* @return the batch teacher
	* @throws info.diit.portal.NoSuchBatchTeacherException if a batch teacher with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchTeacher findByPrimaryKey(
		long batchTeacherId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchTeacherException;

	/**
	* Returns the batch teacher with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param batchTeacherId the primary key of the batch teacher
	* @return the batch teacher, or <code>null</code> if a batch teacher with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchTeacher fetchByPrimaryKey(
		long batchTeacherId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the batch teachers where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the matching batch teachers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchTeacher> findByTeacherByBatchId(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch teachers where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch teachers
	* @param end the upper bound of the range of batch teachers (not inclusive)
	* @return the range of matching batch teachers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchTeacher> findByTeacherByBatchId(
		long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch teachers where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of batch teachers
	* @param end the upper bound of the range of batch teachers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch teachers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchTeacher> findByTeacherByBatchId(
		long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch teacher in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch teacher
	* @throws info.diit.portal.NoSuchBatchTeacherException if a matching batch teacher could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchTeacher findByTeacherByBatchId_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchTeacherException;

	/**
	* Returns the first batch teacher in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch teacher, or <code>null</code> if a matching batch teacher could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchTeacher fetchByTeacherByBatchId_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch teacher in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch teacher
	* @throws info.diit.portal.NoSuchBatchTeacherException if a matching batch teacher could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchTeacher findByTeacherByBatchId_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchTeacherException;

	/**
	* Returns the last batch teacher in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch teacher, or <code>null</code> if a matching batch teacher could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchTeacher fetchByTeacherByBatchId_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch teachers before and after the current batch teacher in the ordered set where batchId = &#63;.
	*
	* @param batchTeacherId the primary key of the current batch teacher
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch teacher
	* @throws info.diit.portal.NoSuchBatchTeacherException if a batch teacher with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchTeacher[] findByTeacherByBatchId_PrevAndNext(
		long batchTeacherId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchTeacherException;

	/**
	* Returns all the batch teachers where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @return the matching batch teachers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchTeacher> findByBatchesByTeacherId(
		long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch teachers where teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param teacherId the teacher ID
	* @param start the lower bound of the range of batch teachers
	* @param end the upper bound of the range of batch teachers (not inclusive)
	* @return the range of matching batch teachers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchTeacher> findByBatchesByTeacherId(
		long teacherId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch teachers where teacherId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param teacherId the teacher ID
	* @param start the lower bound of the range of batch teachers
	* @param end the upper bound of the range of batch teachers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching batch teachers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchTeacher> findByBatchesByTeacherId(
		long teacherId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first batch teacher in the ordered set where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch teacher
	* @throws info.diit.portal.NoSuchBatchTeacherException if a matching batch teacher could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchTeacher findByBatchesByTeacherId_First(
		long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchTeacherException;

	/**
	* Returns the first batch teacher in the ordered set where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching batch teacher, or <code>null</code> if a matching batch teacher could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchTeacher fetchByBatchesByTeacherId_First(
		long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last batch teacher in the ordered set where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch teacher
	* @throws info.diit.portal.NoSuchBatchTeacherException if a matching batch teacher could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchTeacher findByBatchesByTeacherId_Last(
		long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchTeacherException;

	/**
	* Returns the last batch teacher in the ordered set where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching batch teacher, or <code>null</code> if a matching batch teacher could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchTeacher fetchByBatchesByTeacherId_Last(
		long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the batch teachers before and after the current batch teacher in the ordered set where teacherId = &#63;.
	*
	* @param batchTeacherId the primary key of the current batch teacher
	* @param teacherId the teacher ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next batch teacher
	* @throws info.diit.portal.NoSuchBatchTeacherException if a batch teacher with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.BatchTeacher[] findByBatchesByTeacherId_PrevAndNext(
		long batchTeacherId, long teacherId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchBatchTeacherException;

	/**
	* Returns all the batch teachers.
	*
	* @return the batch teachers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchTeacher> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the batch teachers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch teachers
	* @param end the upper bound of the range of batch teachers (not inclusive)
	* @return the range of batch teachers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchTeacher> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the batch teachers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of batch teachers
	* @param end the upper bound of the range of batch teachers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of batch teachers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.BatchTeacher> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch teachers where batchId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByTeacherByBatchId(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch teachers where teacherId = &#63; from the database.
	*
	* @param teacherId the teacher ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByBatchesByTeacherId(long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the batch teachers from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch teachers where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the number of matching batch teachers
	* @throws SystemException if a system exception occurred
	*/
	public int countByTeacherByBatchId(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch teachers where teacherId = &#63;.
	*
	* @param teacherId the teacher ID
	* @return the number of matching batch teachers
	* @throws SystemException if a system exception occurred
	*/
	public int countByBatchesByTeacherId(long teacherId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of batch teachers.
	*
	* @return the number of batch teachers
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
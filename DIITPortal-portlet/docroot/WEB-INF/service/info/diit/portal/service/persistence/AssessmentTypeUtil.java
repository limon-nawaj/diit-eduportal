/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.AssessmentType;

import java.util.List;

/**
 * The persistence utility for the assessment type service. This utility wraps {@link AssessmentTypePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see AssessmentTypePersistence
 * @see AssessmentTypePersistenceImpl
 * @generated
 */
public class AssessmentTypeUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(AssessmentType assessmentType) {
		getPersistence().clearCache(assessmentType);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<AssessmentType> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<AssessmentType> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<AssessmentType> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static AssessmentType update(AssessmentType assessmentType,
		boolean merge) throws SystemException {
		return getPersistence().update(assessmentType, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static AssessmentType update(AssessmentType assessmentType,
		boolean merge, ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(assessmentType, merge, serviceContext);
	}

	/**
	* Caches the assessment type in the entity cache if it is enabled.
	*
	* @param assessmentType the assessment type
	*/
	public static void cacheResult(
		info.diit.portal.model.AssessmentType assessmentType) {
		getPersistence().cacheResult(assessmentType);
	}

	/**
	* Caches the assessment types in the entity cache if it is enabled.
	*
	* @param assessmentTypes the assessment types
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.AssessmentType> assessmentTypes) {
		getPersistence().cacheResult(assessmentTypes);
	}

	/**
	* Creates a new assessment type with the primary key. Does not add the assessment type to the database.
	*
	* @param assessmentTypeId the primary key for the new assessment type
	* @return the new assessment type
	*/
	public static info.diit.portal.model.AssessmentType create(
		long assessmentTypeId) {
		return getPersistence().create(assessmentTypeId);
	}

	/**
	* Removes the assessment type with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param assessmentTypeId the primary key of the assessment type
	* @return the assessment type that was removed
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a assessment type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentType remove(
		long assessmentTypeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException {
		return getPersistence().remove(assessmentTypeId);
	}

	public static info.diit.portal.model.AssessmentType updateImpl(
		info.diit.portal.model.AssessmentType assessmentType, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(assessmentType, merge);
	}

	/**
	* Returns the assessment type with the primary key or throws a {@link info.diit.portal.NoSuchAssessmentTypeException} if it could not be found.
	*
	* @param assessmentTypeId the primary key of the assessment type
	* @return the assessment type
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a assessment type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentType findByPrimaryKey(
		long assessmentTypeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException {
		return getPersistence().findByPrimaryKey(assessmentTypeId);
	}

	/**
	* Returns the assessment type with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param assessmentTypeId the primary key of the assessment type
	* @return the assessment type, or <code>null</code> if a assessment type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentType fetchByPrimaryKey(
		long assessmentTypeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(assessmentTypeId);
	}

	/**
	* Returns all the assessment types where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AssessmentType> findBycompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycompany(companyId);
	}

	/**
	* Returns a range of all the assessment types where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of assessment types
	* @param end the upper bound of the range of assessment types (not inclusive)
	* @return the range of matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AssessmentType> findBycompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycompany(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the assessment types where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of assessment types
	* @param end the upper bound of the range of assessment types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AssessmentType> findBycompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycompany(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first assessment type in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment type
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentType findBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException {
		return getPersistence().findBycompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the first assessment type in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment type, or <code>null</code> if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentType fetchBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBycompany_First(companyId, orderByComparator);
	}

	/**
	* Returns the last assessment type in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment type
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentType findBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException {
		return getPersistence().findBycompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last assessment type in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment type, or <code>null</code> if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentType fetchBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBycompany_Last(companyId, orderByComparator);
	}

	/**
	* Returns the assessment types before and after the current assessment type in the ordered set where companyId = &#63;.
	*
	* @param assessmentTypeId the primary key of the current assessment type
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment type
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a assessment type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentType[] findBycompany_PrevAndNext(
		long assessmentTypeId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException {
		return getPersistence()
				   .findBycompany_PrevAndNext(assessmentTypeId, companyId,
			orderByComparator);
	}

	/**
	* Returns all the assessment types where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AssessmentType> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId);
	}

	/**
	* Returns a range of all the assessment types where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of assessment types
	* @param end the upper bound of the range of assessment types (not inclusive)
	* @return the range of matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AssessmentType> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByorganization(organizationId, start, end);
	}

	/**
	* Returns an ordered range of all the assessment types where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of assessment types
	* @param end the upper bound of the range of assessment types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AssessmentType> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByorganization(organizationId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first assessment type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment type
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentType findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException {
		return getPersistence()
				   .findByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the first assessment type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment type, or <code>null</code> if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentType fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_First(organizationId, orderByComparator);
	}

	/**
	* Returns the last assessment type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment type
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentType findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException {
		return getPersistence()
				   .findByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the last assessment type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment type, or <code>null</code> if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentType fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByorganization_Last(organizationId, orderByComparator);
	}

	/**
	* Returns the assessment types before and after the current assessment type in the ordered set where organizationId = &#63;.
	*
	* @param assessmentTypeId the primary key of the current assessment type
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment type
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a assessment type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.AssessmentType[] findByorganization_PrevAndNext(
		long assessmentTypeId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException {
		return getPersistence()
				   .findByorganization_PrevAndNext(assessmentTypeId,
			organizationId, orderByComparator);
	}

	/**
	* Returns all the assessment types.
	*
	* @return the assessment types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AssessmentType> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the assessment types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of assessment types
	* @param end the upper bound of the range of assessment types (not inclusive)
	* @return the range of assessment types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AssessmentType> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the assessment types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of assessment types
	* @param end the upper bound of the range of assessment types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of assessment types
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.AssessmentType> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the assessment types where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBycompany(companyId);
	}

	/**
	* Removes all the assessment types where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByorganization(organizationId);
	}

	/**
	* Removes all the assessment types from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of assessment types where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public static int countBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBycompany(companyId);
	}

	/**
	* Returns the number of assessment types where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public static int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByorganization(organizationId);
	}

	/**
	* Returns the number of assessment types.
	*
	* @return the number of assessment types
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static AssessmentTypePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (AssessmentTypePersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					AssessmentTypePersistence.class.getName());

			ReferenceRegistry.registerReference(AssessmentTypeUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(AssessmentTypePersistence persistence) {
	}

	private static AssessmentTypePersistence _persistence;
}
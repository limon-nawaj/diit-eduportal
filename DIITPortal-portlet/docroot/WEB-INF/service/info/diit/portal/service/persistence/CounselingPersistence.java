/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.Counseling;

/**
 * The persistence interface for the counseling service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see CounselingPersistenceImpl
 * @see CounselingUtil
 * @generated
 */
public interface CounselingPersistence extends BasePersistence<Counseling> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CounselingUtil} to access the counseling persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the counseling in the entity cache if it is enabled.
	*
	* @param counseling the counseling
	*/
	public void cacheResult(info.diit.portal.model.Counseling counseling);

	/**
	* Caches the counselings in the entity cache if it is enabled.
	*
	* @param counselings the counselings
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.Counseling> counselings);

	/**
	* Creates a new counseling with the primary key. Does not add the counseling to the database.
	*
	* @param counselingId the primary key for the new counseling
	* @return the new counseling
	*/
	public info.diit.portal.model.Counseling create(long counselingId);

	/**
	* Removes the counseling with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param counselingId the primary key of the counseling
	* @return the counseling that was removed
	* @throws info.diit.portal.NoSuchCounselingException if a counseling with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Counseling remove(long counselingId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCounselingException;

	public info.diit.portal.model.Counseling updateImpl(
		info.diit.portal.model.Counseling counseling, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the counseling with the primary key or throws a {@link info.diit.portal.NoSuchCounselingException} if it could not be found.
	*
	* @param counselingId the primary key of the counseling
	* @return the counseling
	* @throws info.diit.portal.NoSuchCounselingException if a counseling with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Counseling findByPrimaryKey(long counselingId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCounselingException;

	/**
	* Returns the counseling with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param counselingId the primary key of the counseling
	* @return the counseling, or <code>null</code> if a counseling with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Counseling fetchByPrimaryKey(
		long counselingId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the counseling where counselingId = &#63; or throws a {@link info.diit.portal.NoSuchCounselingException} if it could not be found.
	*
	* @param counselingId the counseling ID
	* @return the matching counseling
	* @throws info.diit.portal.NoSuchCounselingException if a matching counseling could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Counseling findByCounseling(long counselingId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCounselingException;

	/**
	* Returns the counseling where counselingId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param counselingId the counseling ID
	* @return the matching counseling, or <code>null</code> if a matching counseling could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Counseling fetchByCounseling(
		long counselingId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the counseling where counselingId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param counselingId the counseling ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching counseling, or <code>null</code> if a matching counseling could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Counseling fetchByCounseling(
		long counselingId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the counselings where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching counselings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Counseling> findByUser(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the counselings where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of counselings
	* @param end the upper bound of the range of counselings (not inclusive)
	* @return the range of matching counselings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Counseling> findByUser(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the counselings where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of counselings
	* @param end the upper bound of the range of counselings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching counselings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Counseling> findByUser(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first counseling in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching counseling
	* @throws info.diit.portal.NoSuchCounselingException if a matching counseling could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Counseling findByUser_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCounselingException;

	/**
	* Returns the first counseling in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching counseling, or <code>null</code> if a matching counseling could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Counseling fetchByUser_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last counseling in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching counseling
	* @throws info.diit.portal.NoSuchCounselingException if a matching counseling could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Counseling findByUser_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCounselingException;

	/**
	* Returns the last counseling in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching counseling, or <code>null</code> if a matching counseling could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Counseling fetchByUser_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the counselings before and after the current counseling in the ordered set where userId = &#63;.
	*
	* @param counselingId the primary key of the current counseling
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next counseling
	* @throws info.diit.portal.NoSuchCounselingException if a counseling with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Counseling[] findByUser_PrevAndNext(
		long counselingId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCounselingException;

	/**
	* Returns all the counselings.
	*
	* @return the counselings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Counseling> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the counselings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of counselings
	* @param end the upper bound of the range of counselings (not inclusive)
	* @return the range of counselings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Counseling> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the counselings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of counselings
	* @param end the upper bound of the range of counselings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of counselings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.Counseling> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the counseling where counselingId = &#63; from the database.
	*
	* @param counselingId the counseling ID
	* @return the counseling that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.Counseling removeByCounseling(
		long counselingId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCounselingException;

	/**
	* Removes all the counselings where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the counselings from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of counselings where counselingId = &#63;.
	*
	* @param counselingId the counseling ID
	* @return the number of matching counselings
	* @throws SystemException if a system exception occurred
	*/
	public int countByCounseling(long counselingId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of counselings where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching counselings
	* @throws SystemException if a system exception occurred
	*/
	public int countByUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of counselings.
	*
	* @return the number of counselings
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the counseling course interests associated with the counseling.
	*
	* @param pk the primary key of the counseling
	* @return the counseling course interests associated with the counseling
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CounselingCourseInterest> getCounselingCourseInterests(
		long pk) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the counseling course interests associated with the counseling.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the counseling
	* @param start the lower bound of the range of counselings
	* @param end the upper bound of the range of counselings (not inclusive)
	* @return the range of counseling course interests associated with the counseling
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CounselingCourseInterest> getCounselingCourseInterests(
		long pk, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the counseling course interests associated with the counseling.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param pk the primary key of the counseling
	* @param start the lower bound of the range of counselings
	* @param end the upper bound of the range of counselings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of counseling course interests associated with the counseling
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CounselingCourseInterest> getCounselingCourseInterests(
		long pk, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of counseling course interests associated with the counseling.
	*
	* @param pk the primary key of the counseling
	* @return the number of counseling course interests associated with the counseling
	* @throws SystemException if a system exception occurred
	*/
	public int getCounselingCourseInterestsSize(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the counseling course interest is associated with the counseling.
	*
	* @param pk the primary key of the counseling
	* @param counselingCourseInterestPK the primary key of the counseling course interest
	* @return <code>true</code> if the counseling course interest is associated with the counseling; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsCounselingCourseInterest(long pk,
		long counselingCourseInterestPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns <code>true</code> if the counseling has any counseling course interests associated with it.
	*
	* @param pk the primary key of the counseling to check for associations with counseling course interests
	* @return <code>true</code> if the counseling has any counseling course interests associated with it; <code>false</code> otherwise
	* @throws SystemException if a system exception occurred
	*/
	public boolean containsCounselingCourseInterests(long pk)
		throws com.liferay.portal.kernel.exception.SystemException;
}
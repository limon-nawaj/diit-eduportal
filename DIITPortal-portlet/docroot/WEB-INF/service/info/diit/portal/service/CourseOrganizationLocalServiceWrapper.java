/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link CourseOrganizationLocalService}.
 * </p>
 *
 * @author    mohammad
 * @see       CourseOrganizationLocalService
 * @generated
 */
public class CourseOrganizationLocalServiceWrapper
	implements CourseOrganizationLocalService,
		ServiceWrapper<CourseOrganizationLocalService> {
	public CourseOrganizationLocalServiceWrapper(
		CourseOrganizationLocalService courseOrganizationLocalService) {
		_courseOrganizationLocalService = courseOrganizationLocalService;
	}

	/**
	* Adds the course organization to the database. Also notifies the appropriate model listeners.
	*
	* @param courseOrganization the course organization
	* @return the course organization that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization addCourseOrganization(
		info.diit.portal.model.CourseOrganization courseOrganization)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.addCourseOrganization(courseOrganization);
	}

	/**
	* Creates a new course organization with the primary key. Does not add the course organization to the database.
	*
	* @param courseOrganizationId the primary key for the new course organization
	* @return the new course organization
	*/
	public info.diit.portal.model.CourseOrganization createCourseOrganization(
		long courseOrganizationId) {
		return _courseOrganizationLocalService.createCourseOrganization(courseOrganizationId);
	}

	/**
	* Deletes the course organization with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param courseOrganizationId the primary key of the course organization
	* @return the course organization that was removed
	* @throws PortalException if a course organization with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization deleteCourseOrganization(
		long courseOrganizationId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.deleteCourseOrganization(courseOrganizationId);
	}

	/**
	* Deletes the course organization from the database. Also notifies the appropriate model listeners.
	*
	* @param courseOrganization the course organization
	* @return the course organization that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization deleteCourseOrganization(
		info.diit.portal.model.CourseOrganization courseOrganization)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.deleteCourseOrganization(courseOrganization);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _courseOrganizationLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.CourseOrganization fetchCourseOrganization(
		long courseOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.fetchCourseOrganization(courseOrganizationId);
	}

	/**
	* Returns the course organization with the primary key.
	*
	* @param courseOrganizationId the primary key of the course organization
	* @return the course organization
	* @throws PortalException if a course organization with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization getCourseOrganization(
		long courseOrganizationId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.getCourseOrganization(courseOrganizationId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the course organizations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course organizations
	* @param end the upper bound of the range of course organizations (not inclusive)
	* @return the range of course organizations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseOrganization> getCourseOrganizations(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.getCourseOrganizations(start, end);
	}

	/**
	* Returns the number of course organizations.
	*
	* @return the number of course organizations
	* @throws SystemException if a system exception occurred
	*/
	public int getCourseOrganizationsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.getCourseOrganizationsCount();
	}

	/**
	* Updates the course organization in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param courseOrganization the course organization
	* @return the course organization that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization updateCourseOrganization(
		info.diit.portal.model.CourseOrganization courseOrganization)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.updateCourseOrganization(courseOrganization);
	}

	/**
	* Updates the course organization in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param courseOrganization the course organization
	* @param merge whether to merge the course organization with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the course organization that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseOrganization updateCourseOrganization(
		info.diit.portal.model.CourseOrganization courseOrganization,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.updateCourseOrganization(courseOrganization,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _courseOrganizationLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_courseOrganizationLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _courseOrganizationLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	public java.util.List<info.diit.portal.model.CourseOrganization> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.findByCompany(companyId);
	}

	public java.util.List<info.diit.portal.model.CourseOrganization> findByOrganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _courseOrganizationLocalService.findByOrganization(organizationId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public CourseOrganizationLocalService getWrappedCourseOrganizationLocalService() {
		return _courseOrganizationLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedCourseOrganizationLocalService(
		CourseOrganizationLocalService courseOrganizationLocalService) {
		_courseOrganizationLocalService = courseOrganizationLocalService;
	}

	public CourseOrganizationLocalService getWrappedService() {
		return _courseOrganizationLocalService;
	}

	public void setWrappedService(
		CourseOrganizationLocalService courseOrganizationLocalService) {
		_courseOrganizationLocalService = courseOrganizationLocalService;
	}

	private CourseOrganizationLocalService _courseOrganizationLocalService;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.StudentPaymentSchedule;

import java.util.List;

/**
 * The persistence utility for the student payment schedule service. This utility wraps {@link StudentPaymentSchedulePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see StudentPaymentSchedulePersistence
 * @see StudentPaymentSchedulePersistenceImpl
 * @generated
 */
public class StudentPaymentScheduleUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(StudentPaymentSchedule studentPaymentSchedule) {
		getPersistence().clearCache(studentPaymentSchedule);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<StudentPaymentSchedule> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<StudentPaymentSchedule> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<StudentPaymentSchedule> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static StudentPaymentSchedule update(
		StudentPaymentSchedule studentPaymentSchedule, boolean merge)
		throws SystemException {
		return getPersistence().update(studentPaymentSchedule, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static StudentPaymentSchedule update(
		StudentPaymentSchedule studentPaymentSchedule, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence()
				   .update(studentPaymentSchedule, merge, serviceContext);
	}

	/**
	* Caches the student payment schedule in the entity cache if it is enabled.
	*
	* @param studentPaymentSchedule the student payment schedule
	*/
	public static void cacheResult(
		info.diit.portal.model.StudentPaymentSchedule studentPaymentSchedule) {
		getPersistence().cacheResult(studentPaymentSchedule);
	}

	/**
	* Caches the student payment schedules in the entity cache if it is enabled.
	*
	* @param studentPaymentSchedules the student payment schedules
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.StudentPaymentSchedule> studentPaymentSchedules) {
		getPersistence().cacheResult(studentPaymentSchedules);
	}

	/**
	* Creates a new student payment schedule with the primary key. Does not add the student payment schedule to the database.
	*
	* @param studentPaymentScheduleId the primary key for the new student payment schedule
	* @return the new student payment schedule
	*/
	public static info.diit.portal.model.StudentPaymentSchedule create(
		long studentPaymentScheduleId) {
		return getPersistence().create(studentPaymentScheduleId);
	}

	/**
	* Removes the student payment schedule with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param studentPaymentScheduleId the primary key of the student payment schedule
	* @return the student payment schedule that was removed
	* @throws info.diit.portal.NoSuchStudentPaymentScheduleException if a student payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StudentPaymentSchedule remove(
		long studentPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchStudentPaymentScheduleException {
		return getPersistence().remove(studentPaymentScheduleId);
	}

	public static info.diit.portal.model.StudentPaymentSchedule updateImpl(
		info.diit.portal.model.StudentPaymentSchedule studentPaymentSchedule,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(studentPaymentSchedule, merge);
	}

	/**
	* Returns the student payment schedule with the primary key or throws a {@link info.diit.portal.NoSuchStudentPaymentScheduleException} if it could not be found.
	*
	* @param studentPaymentScheduleId the primary key of the student payment schedule
	* @return the student payment schedule
	* @throws info.diit.portal.NoSuchStudentPaymentScheduleException if a student payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StudentPaymentSchedule findByPrimaryKey(
		long studentPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchStudentPaymentScheduleException {
		return getPersistence().findByPrimaryKey(studentPaymentScheduleId);
	}

	/**
	* Returns the student payment schedule with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param studentPaymentScheduleId the primary key of the student payment schedule
	* @return the student payment schedule, or <code>null</code> if a student payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StudentPaymentSchedule fetchByPrimaryKey(
		long studentPaymentScheduleId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(studentPaymentScheduleId);
	}

	/**
	* Returns all the student payment schedules where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @return the matching student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.StudentPaymentSchedule> findByStudentBatch(
		long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByStudentBatch(studentId, batchId);
	}

	/**
	* Returns a range of all the student payment schedules where studentId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of student payment schedules
	* @param end the upper bound of the range of student payment schedules (not inclusive)
	* @return the range of matching student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.StudentPaymentSchedule> findByStudentBatch(
		long studentId, long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentBatch(studentId, batchId, start, end);
	}

	/**
	* Returns an ordered range of all the student payment schedules where studentId = &#63; and batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param start the lower bound of the range of student payment schedules
	* @param end the upper bound of the range of student payment schedules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.StudentPaymentSchedule> findByStudentBatch(
		long studentId, long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByStudentBatch(studentId, batchId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student payment schedule
	* @throws info.diit.portal.NoSuchStudentPaymentScheduleException if a matching student payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StudentPaymentSchedule findByStudentBatch_First(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchStudentPaymentScheduleException {
		return getPersistence()
				   .findByStudentBatch_First(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the first student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching student payment schedule, or <code>null</code> if a matching student payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StudentPaymentSchedule fetchByStudentBatch_First(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentBatch_First(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the last student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student payment schedule
	* @throws info.diit.portal.NoSuchStudentPaymentScheduleException if a matching student payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StudentPaymentSchedule findByStudentBatch_Last(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchStudentPaymentScheduleException {
		return getPersistence()
				   .findByStudentBatch_Last(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the last student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching student payment schedule, or <code>null</code> if a matching student payment schedule could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StudentPaymentSchedule fetchByStudentBatch_Last(
		long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByStudentBatch_Last(studentId, batchId,
			orderByComparator);
	}

	/**
	* Returns the student payment schedules before and after the current student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	*
	* @param studentPaymentScheduleId the primary key of the current student payment schedule
	* @param studentId the student ID
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next student payment schedule
	* @throws info.diit.portal.NoSuchStudentPaymentScheduleException if a student payment schedule with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.StudentPaymentSchedule[] findByStudentBatch_PrevAndNext(
		long studentPaymentScheduleId, long studentId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchStudentPaymentScheduleException {
		return getPersistence()
				   .findByStudentBatch_PrevAndNext(studentPaymentScheduleId,
			studentId, batchId, orderByComparator);
	}

	/**
	* Returns all the student payment schedules.
	*
	* @return the student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.StudentPaymentSchedule> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the student payment schedules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student payment schedules
	* @param end the upper bound of the range of student payment schedules (not inclusive)
	* @return the range of student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.StudentPaymentSchedule> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the student payment schedules.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of student payment schedules
	* @param end the upper bound of the range of student payment schedules (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.StudentPaymentSchedule> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the student payment schedules where studentId = &#63; and batchId = &#63; from the database.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByStudentBatch(long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByStudentBatch(studentId, batchId);
	}

	/**
	* Removes all the student payment schedules from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of student payment schedules where studentId = &#63; and batchId = &#63;.
	*
	* @param studentId the student ID
	* @param batchId the batch ID
	* @return the number of matching student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static int countByStudentBatch(long studentId, long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByStudentBatch(studentId, batchId);
	}

	/**
	* Returns the number of student payment schedules.
	*
	* @return the number of student payment schedules
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static StudentPaymentSchedulePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (StudentPaymentSchedulePersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					StudentPaymentSchedulePersistence.class.getName());

			ReferenceRegistry.registerReference(StudentPaymentScheduleUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(StudentPaymentSchedulePersistence persistence) {
	}

	private static StudentPaymentSchedulePersistence _persistence;
}
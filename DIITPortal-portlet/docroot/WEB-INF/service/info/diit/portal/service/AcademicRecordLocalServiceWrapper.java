/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link AcademicRecordLocalService}.
 * </p>
 *
 * @author    mohammad
 * @see       AcademicRecordLocalService
 * @generated
 */
public class AcademicRecordLocalServiceWrapper
	implements AcademicRecordLocalService,
		ServiceWrapper<AcademicRecordLocalService> {
	public AcademicRecordLocalServiceWrapper(
		AcademicRecordLocalService academicRecordLocalService) {
		_academicRecordLocalService = academicRecordLocalService;
	}

	/**
	* Adds the academic record to the database. Also notifies the appropriate model listeners.
	*
	* @param academicRecord the academic record
	* @return the academic record that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AcademicRecord addAcademicRecord(
		info.diit.portal.model.AcademicRecord academicRecord)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.addAcademicRecord(academicRecord);
	}

	/**
	* Creates a new academic record with the primary key. Does not add the academic record to the database.
	*
	* @param academicRecordId the primary key for the new academic record
	* @return the new academic record
	*/
	public info.diit.portal.model.AcademicRecord createAcademicRecord(
		long academicRecordId) {
		return _academicRecordLocalService.createAcademicRecord(academicRecordId);
	}

	/**
	* Deletes the academic record with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param academicRecordId the primary key of the academic record
	* @return the academic record that was removed
	* @throws PortalException if a academic record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AcademicRecord deleteAcademicRecord(
		long academicRecordId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.deleteAcademicRecord(academicRecordId);
	}

	/**
	* Deletes the academic record from the database. Also notifies the appropriate model listeners.
	*
	* @param academicRecord the academic record
	* @return the academic record that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AcademicRecord deleteAcademicRecord(
		info.diit.portal.model.AcademicRecord academicRecord)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.deleteAcademicRecord(academicRecord);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _academicRecordLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.AcademicRecord fetchAcademicRecord(
		long academicRecordId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.fetchAcademicRecord(academicRecordId);
	}

	/**
	* Returns the academic record with the primary key.
	*
	* @param academicRecordId the primary key of the academic record
	* @return the academic record
	* @throws PortalException if a academic record with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AcademicRecord getAcademicRecord(
		long academicRecordId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.getAcademicRecord(academicRecordId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the academic records.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of academic records
	* @param end the upper bound of the range of academic records (not inclusive)
	* @return the range of academic records
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AcademicRecord> getAcademicRecords(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.getAcademicRecords(start, end);
	}

	/**
	* Returns the number of academic records.
	*
	* @return the number of academic records
	* @throws SystemException if a system exception occurred
	*/
	public int getAcademicRecordsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.getAcademicRecordsCount();
	}

	/**
	* Updates the academic record in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param academicRecord the academic record
	* @return the academic record that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AcademicRecord updateAcademicRecord(
		info.diit.portal.model.AcademicRecord academicRecord)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.updateAcademicRecord(academicRecord);
	}

	/**
	* Updates the academic record in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param academicRecord the academic record
	* @param merge whether to merge the academic record with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the academic record that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AcademicRecord updateAcademicRecord(
		info.diit.portal.model.AcademicRecord academicRecord, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.updateAcademicRecord(academicRecord,
			merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _academicRecordLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_academicRecordLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _academicRecordLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.model.AcademicRecord> findByAcademicRecordList(
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _academicRecordLocalService.findByAcademicRecordList(studentId);
	}

	public void saveStudentAcademicRecord(
		java.util.List<info.diit.portal.model.AcademicRecord> academicRecords,
		long studentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		_academicRecordLocalService.saveStudentAcademicRecord(academicRecords,
			studentId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public AcademicRecordLocalService getWrappedAcademicRecordLocalService() {
		return _academicRecordLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedAcademicRecordLocalService(
		AcademicRecordLocalService academicRecordLocalService) {
		_academicRecordLocalService = academicRecordLocalService;
	}

	public AcademicRecordLocalService getWrappedService() {
		return _academicRecordLocalService;
	}

	public void setWrappedService(
		AcademicRecordLocalService academicRecordLocalService) {
		_academicRecordLocalService = academicRecordLocalService;
	}

	private AcademicRecordLocalService _academicRecordLocalService;
}
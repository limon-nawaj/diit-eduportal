/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.TaskDesignation;

/**
 * The persistence interface for the task designation service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see TaskDesignationPersistenceImpl
 * @see TaskDesignationUtil
 * @generated
 */
public interface TaskDesignationPersistence extends BasePersistence<TaskDesignation> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TaskDesignationUtil} to access the task designation persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the task designation in the entity cache if it is enabled.
	*
	* @param taskDesignation the task designation
	*/
	public void cacheResult(
		info.diit.portal.model.TaskDesignation taskDesignation);

	/**
	* Caches the task designations in the entity cache if it is enabled.
	*
	* @param taskDesignations the task designations
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.TaskDesignation> taskDesignations);

	/**
	* Creates a new task designation with the primary key. Does not add the task designation to the database.
	*
	* @param taskDesignationId the primary key for the new task designation
	* @return the new task designation
	*/
	public info.diit.portal.model.TaskDesignation create(long taskDesignationId);

	/**
	* Removes the task designation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param taskDesignationId the primary key of the task designation
	* @return the task designation that was removed
	* @throws info.diit.portal.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation remove(long taskDesignationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchTaskDesignationException;

	public info.diit.portal.model.TaskDesignation updateImpl(
		info.diit.portal.model.TaskDesignation taskDesignation, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the task designation with the primary key or throws a {@link info.diit.portal.NoSuchTaskDesignationException} if it could not be found.
	*
	* @param taskDesignationId the primary key of the task designation
	* @return the task designation
	* @throws info.diit.portal.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation findByPrimaryKey(
		long taskDesignationId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchTaskDesignationException;

	/**
	* Returns the task designation with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param taskDesignationId the primary key of the task designation
	* @return the task designation, or <code>null</code> if a task designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation fetchByPrimaryKey(
		long taskDesignationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the task designations where designationId = &#63;.
	*
	* @param designationId the designation ID
	* @return the matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.TaskDesignation> findByDesignation(
		long designationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the task designations where designationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param designationId the designation ID
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @return the range of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.TaskDesignation> findByDesignation(
		long designationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the task designations where designationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param designationId the designation ID
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.TaskDesignation> findByDesignation(
		long designationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first task designation in the ordered set where designationId = &#63;.
	*
	* @param designationId the designation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching task designation
	* @throws info.diit.portal.NoSuchTaskDesignationException if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation findByDesignation_First(
		long designationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchTaskDesignationException;

	/**
	* Returns the first task designation in the ordered set where designationId = &#63;.
	*
	* @param designationId the designation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation fetchByDesignation_First(
		long designationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last task designation in the ordered set where designationId = &#63;.
	*
	* @param designationId the designation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching task designation
	* @throws info.diit.portal.NoSuchTaskDesignationException if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation findByDesignation_Last(
		long designationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchTaskDesignationException;

	/**
	* Returns the last task designation in the ordered set where designationId = &#63;.
	*
	* @param designationId the designation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation fetchByDesignation_Last(
		long designationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the task designations before and after the current task designation in the ordered set where designationId = &#63;.
	*
	* @param taskDesignationId the primary key of the current task designation
	* @param designationId the designation ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next task designation
	* @throws info.diit.portal.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation[] findByDesignation_PrevAndNext(
		long taskDesignationId, long designationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchTaskDesignationException;

	/**
	* Returns all the task designations where taskId = &#63;.
	*
	* @param taskId the task ID
	* @return the matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.TaskDesignation> findByTask(
		long taskId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the task designations where taskId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param taskId the task ID
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @return the range of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.TaskDesignation> findByTask(
		long taskId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the task designations where taskId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param taskId the task ID
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.TaskDesignation> findByTask(
		long taskId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first task designation in the ordered set where taskId = &#63;.
	*
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching task designation
	* @throws info.diit.portal.NoSuchTaskDesignationException if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation findByTask_First(
		long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchTaskDesignationException;

	/**
	* Returns the first task designation in the ordered set where taskId = &#63;.
	*
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation fetchByTask_First(
		long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last task designation in the ordered set where taskId = &#63;.
	*
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching task designation
	* @throws info.diit.portal.NoSuchTaskDesignationException if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation findByTask_Last(long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchTaskDesignationException;

	/**
	* Returns the last task designation in the ordered set where taskId = &#63;.
	*
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation fetchByTask_Last(
		long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the task designations before and after the current task designation in the ordered set where taskId = &#63;.
	*
	* @param taskDesignationId the primary key of the current task designation
	* @param taskId the task ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next task designation
	* @throws info.diit.portal.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation[] findByTask_PrevAndNext(
		long taskDesignationId, long taskId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchTaskDesignationException;

	/**
	* Returns the task designation where designationId = &#63; and taskId = &#63; or throws a {@link info.diit.portal.NoSuchTaskDesignationException} if it could not be found.
	*
	* @param designationId the designation ID
	* @param taskId the task ID
	* @return the matching task designation
	* @throws info.diit.portal.NoSuchTaskDesignationException if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation findByDesignationTask(
		long designationId, long taskId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchTaskDesignationException;

	/**
	* Returns the task designation where designationId = &#63; and taskId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param designationId the designation ID
	* @param taskId the task ID
	* @return the matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation fetchByDesignationTask(
		long designationId, long taskId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the task designation where designationId = &#63; and taskId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param designationId the designation ID
	* @param taskId the task ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation fetchByDesignationTask(
		long designationId, long taskId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the task designations where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.TaskDesignation> findByCompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the task designations where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @return the range of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.TaskDesignation> findByCompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the task designations where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.TaskDesignation> findByCompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first task designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching task designation
	* @throws info.diit.portal.NoSuchTaskDesignationException if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation findByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchTaskDesignationException;

	/**
	* Returns the first task designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation fetchByCompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last task designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching task designation
	* @throws info.diit.portal.NoSuchTaskDesignationException if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation findByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchTaskDesignationException;

	/**
	* Returns the last task designation in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching task designation, or <code>null</code> if a matching task designation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation fetchByCompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the task designations before and after the current task designation in the ordered set where companyId = &#63;.
	*
	* @param taskDesignationId the primary key of the current task designation
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next task designation
	* @throws info.diit.portal.NoSuchTaskDesignationException if a task designation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation[] findByCompany_PrevAndNext(
		long taskDesignationId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchTaskDesignationException;

	/**
	* Returns all the task designations.
	*
	* @return the task designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.TaskDesignation> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the task designations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @return the range of task designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.TaskDesignation> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the task designations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of task designations
	* @param end the upper bound of the range of task designations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of task designations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.TaskDesignation> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the task designations where designationId = &#63; from the database.
	*
	* @param designationId the designation ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByDesignation(long designationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the task designations where taskId = &#63; from the database.
	*
	* @param taskId the task ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByTask(long taskId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the task designation where designationId = &#63; and taskId = &#63; from the database.
	*
	* @param designationId the designation ID
	* @param taskId the task ID
	* @return the task designation that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.TaskDesignation removeByDesignationTask(
		long designationId, long taskId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchTaskDesignationException;

	/**
	* Removes all the task designations where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the task designations from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of task designations where designationId = &#63;.
	*
	* @param designationId the designation ID
	* @return the number of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public int countByDesignation(long designationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of task designations where taskId = &#63;.
	*
	* @param taskId the task ID
	* @return the number of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public int countByTask(long taskId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of task designations where designationId = &#63; and taskId = &#63;.
	*
	* @param designationId the designation ID
	* @param taskId the task ID
	* @return the number of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public int countByDesignationTask(long designationId, long taskId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of task designations where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching task designations
	* @throws SystemException if a system exception occurred
	*/
	public int countByCompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of task designations.
	*
	* @return the number of task designations
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.AssessmentType;

/**
 * The persistence interface for the assessment type service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see AssessmentTypePersistenceImpl
 * @see AssessmentTypeUtil
 * @generated
 */
public interface AssessmentTypePersistence extends BasePersistence<AssessmentType> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AssessmentTypeUtil} to access the assessment type persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the assessment type in the entity cache if it is enabled.
	*
	* @param assessmentType the assessment type
	*/
	public void cacheResult(
		info.diit.portal.model.AssessmentType assessmentType);

	/**
	* Caches the assessment types in the entity cache if it is enabled.
	*
	* @param assessmentTypes the assessment types
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.AssessmentType> assessmentTypes);

	/**
	* Creates a new assessment type with the primary key. Does not add the assessment type to the database.
	*
	* @param assessmentTypeId the primary key for the new assessment type
	* @return the new assessment type
	*/
	public info.diit.portal.model.AssessmentType create(long assessmentTypeId);

	/**
	* Removes the assessment type with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param assessmentTypeId the primary key of the assessment type
	* @return the assessment type that was removed
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a assessment type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AssessmentType remove(long assessmentTypeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException;

	public info.diit.portal.model.AssessmentType updateImpl(
		info.diit.portal.model.AssessmentType assessmentType, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the assessment type with the primary key or throws a {@link info.diit.portal.NoSuchAssessmentTypeException} if it could not be found.
	*
	* @param assessmentTypeId the primary key of the assessment type
	* @return the assessment type
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a assessment type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AssessmentType findByPrimaryKey(
		long assessmentTypeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException;

	/**
	* Returns the assessment type with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param assessmentTypeId the primary key of the assessment type
	* @return the assessment type, or <code>null</code> if a assessment type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AssessmentType fetchByPrimaryKey(
		long assessmentTypeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the assessment types where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AssessmentType> findBycompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the assessment types where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of assessment types
	* @param end the upper bound of the range of assessment types (not inclusive)
	* @return the range of matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AssessmentType> findBycompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the assessment types where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of assessment types
	* @param end the upper bound of the range of assessment types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AssessmentType> findBycompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first assessment type in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment type
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AssessmentType findBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException;

	/**
	* Returns the first assessment type in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment type, or <code>null</code> if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AssessmentType fetchBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last assessment type in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment type
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AssessmentType findBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException;

	/**
	* Returns the last assessment type in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment type, or <code>null</code> if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AssessmentType fetchBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the assessment types before and after the current assessment type in the ordered set where companyId = &#63;.
	*
	* @param assessmentTypeId the primary key of the current assessment type
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment type
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a assessment type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AssessmentType[] findBycompany_PrevAndNext(
		long assessmentTypeId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException;

	/**
	* Returns all the assessment types where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AssessmentType> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the assessment types where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of assessment types
	* @param end the upper bound of the range of assessment types (not inclusive)
	* @return the range of matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AssessmentType> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the assessment types where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of assessment types
	* @param end the upper bound of the range of assessment types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AssessmentType> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first assessment type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment type
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AssessmentType findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException;

	/**
	* Returns the first assessment type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assessment type, or <code>null</code> if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AssessmentType fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last assessment type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment type
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AssessmentType findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException;

	/**
	* Returns the last assessment type in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assessment type, or <code>null</code> if a matching assessment type could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AssessmentType fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the assessment types before and after the current assessment type in the ordered set where organizationId = &#63;.
	*
	* @param assessmentTypeId the primary key of the current assessment type
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assessment type
	* @throws info.diit.portal.NoSuchAssessmentTypeException if a assessment type with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.AssessmentType[] findByorganization_PrevAndNext(
		long assessmentTypeId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchAssessmentTypeException;

	/**
	* Returns all the assessment types.
	*
	* @return the assessment types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AssessmentType> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the assessment types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of assessment types
	* @param end the upper bound of the range of assessment types (not inclusive)
	* @return the range of assessment types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AssessmentType> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the assessment types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of assessment types
	* @param end the upper bound of the range of assessment types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of assessment types
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.AssessmentType> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the assessment types where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the assessment types where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the assessment types from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of assessment types where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public int countBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of assessment types where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching assessment types
	* @throws SystemException if a system exception occurred
	*/
	public int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of assessment types.
	*
	* @return the number of assessment types
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
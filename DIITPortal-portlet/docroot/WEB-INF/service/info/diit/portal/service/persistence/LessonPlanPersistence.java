/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.LessonPlan;

/**
 * The persistence interface for the lesson plan service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see LessonPlanPersistenceImpl
 * @see LessonPlanUtil
 * @generated
 */
public interface LessonPlanPersistence extends BasePersistence<LessonPlan> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LessonPlanUtil} to access the lesson plan persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the lesson plan in the entity cache if it is enabled.
	*
	* @param lessonPlan the lesson plan
	*/
	public void cacheResult(info.diit.portal.model.LessonPlan lessonPlan);

	/**
	* Caches the lesson plans in the entity cache if it is enabled.
	*
	* @param lessonPlans the lesson plans
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.LessonPlan> lessonPlans);

	/**
	* Creates a new lesson plan with the primary key. Does not add the lesson plan to the database.
	*
	* @param lessonPlanId the primary key for the new lesson plan
	* @return the new lesson plan
	*/
	public info.diit.portal.model.LessonPlan create(long lessonPlanId);

	/**
	* Removes the lesson plan with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonPlanId the primary key of the lesson plan
	* @return the lesson plan that was removed
	* @throws info.diit.portal.NoSuchLessonPlanException if a lesson plan with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonPlan remove(long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException;

	public info.diit.portal.model.LessonPlan updateImpl(
		info.diit.portal.model.LessonPlan lessonPlan, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lesson plan with the primary key or throws a {@link info.diit.portal.NoSuchLessonPlanException} if it could not be found.
	*
	* @param lessonPlanId the primary key of the lesson plan
	* @return the lesson plan
	* @throws info.diit.portal.NoSuchLessonPlanException if a lesson plan with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonPlan findByPrimaryKey(long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException;

	/**
	* Returns the lesson plan with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param lessonPlanId the primary key of the lesson plan
	* @return the lesson plan, or <code>null</code> if a lesson plan with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonPlan fetchByPrimaryKey(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the lesson plans where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonPlan> findByOrganizationUser(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lesson plans where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of lesson plans
	* @param end the upper bound of the range of lesson plans (not inclusive)
	* @return the range of matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonPlan> findByOrganizationUser(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lesson plans where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of lesson plans
	* @param end the upper bound of the range of lesson plans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonPlan> findByOrganizationUser(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first lesson plan in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson plan
	* @throws info.diit.portal.NoSuchLessonPlanException if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonPlan findByOrganizationUser_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException;

	/**
	* Returns the first lesson plan in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson plan, or <code>null</code> if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonPlan fetchByOrganizationUser_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last lesson plan in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson plan
	* @throws info.diit.portal.NoSuchLessonPlanException if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonPlan findByOrganizationUser_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException;

	/**
	* Returns the last lesson plan in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson plan, or <code>null</code> if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonPlan fetchByOrganizationUser_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lesson plans before and after the current lesson plan in the ordered set where userId = &#63;.
	*
	* @param lessonPlanId the primary key of the current lesson plan
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lesson plan
	* @throws info.diit.portal.NoSuchLessonPlanException if a lesson plan with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonPlan[] findByOrganizationUser_PrevAndNext(
		long lessonPlanId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException;

	/**
	* Returns all the lesson plans where subject = &#63;.
	*
	* @param subject the subject
	* @return the matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonPlan> findBySubject(
		long subject)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lesson plans where subject = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subject the subject
	* @param start the lower bound of the range of lesson plans
	* @param end the upper bound of the range of lesson plans (not inclusive)
	* @return the range of matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonPlan> findBySubject(
		long subject, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lesson plans where subject = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param subject the subject
	* @param start the lower bound of the range of lesson plans
	* @param end the upper bound of the range of lesson plans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonPlan> findBySubject(
		long subject, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first lesson plan in the ordered set where subject = &#63;.
	*
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson plan
	* @throws info.diit.portal.NoSuchLessonPlanException if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonPlan findBySubject_First(long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException;

	/**
	* Returns the first lesson plan in the ordered set where subject = &#63;.
	*
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching lesson plan, or <code>null</code> if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonPlan fetchBySubject_First(
		long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last lesson plan in the ordered set where subject = &#63;.
	*
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson plan
	* @throws info.diit.portal.NoSuchLessonPlanException if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonPlan findBySubject_Last(long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException;

	/**
	* Returns the last lesson plan in the ordered set where subject = &#63;.
	*
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching lesson plan, or <code>null</code> if a matching lesson plan could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonPlan fetchBySubject_Last(long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the lesson plans before and after the current lesson plan in the ordered set where subject = &#63;.
	*
	* @param lessonPlanId the primary key of the current lesson plan
	* @param subject the subject
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next lesson plan
	* @throws info.diit.portal.NoSuchLessonPlanException if a lesson plan with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonPlan[] findBySubject_PrevAndNext(
		long lessonPlanId, long subject,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchLessonPlanException;

	/**
	* Returns all the lesson plans.
	*
	* @return the lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonPlan> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the lesson plans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson plans
	* @param end the upper bound of the range of lesson plans (not inclusive)
	* @return the range of lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonPlan> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the lesson plans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson plans
	* @param end the upper bound of the range of lesson plans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonPlan> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the lesson plans where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByOrganizationUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the lesson plans where subject = &#63; from the database.
	*
	* @param subject the subject
	* @throws SystemException if a system exception occurred
	*/
	public void removeBySubject(long subject)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the lesson plans from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lesson plans where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public int countByOrganizationUser(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lesson plans where subject = &#63;.
	*
	* @param subject the subject
	* @return the number of matching lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public int countBySubject(long subject)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of lesson plans.
	*
	* @return the number of lesson plans
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
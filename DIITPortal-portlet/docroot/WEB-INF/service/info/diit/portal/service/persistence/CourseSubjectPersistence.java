/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import info.diit.portal.model.CourseSubject;

/**
 * The persistence interface for the course subject service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see CourseSubjectPersistenceImpl
 * @see CourseSubjectUtil
 * @generated
 */
public interface CourseSubjectPersistence extends BasePersistence<CourseSubject> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CourseSubjectUtil} to access the course subject persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the course subject in the entity cache if it is enabled.
	*
	* @param courseSubject the course subject
	*/
	public void cacheResult(info.diit.portal.model.CourseSubject courseSubject);

	/**
	* Caches the course subjects in the entity cache if it is enabled.
	*
	* @param courseSubjects the course subjects
	*/
	public void cacheResult(
		java.util.List<info.diit.portal.model.CourseSubject> courseSubjects);

	/**
	* Creates a new course subject with the primary key. Does not add the course subject to the database.
	*
	* @param courseSubjectId the primary key for the new course subject
	* @return the new course subject
	*/
	public info.diit.portal.model.CourseSubject create(long courseSubjectId);

	/**
	* Removes the course subject with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param courseSubjectId the primary key of the course subject
	* @return the course subject that was removed
	* @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject remove(long courseSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException;

	public info.diit.portal.model.CourseSubject updateImpl(
		info.diit.portal.model.CourseSubject courseSubject, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course subject with the primary key or throws a {@link info.diit.portal.NoSuchCourseSubjectException} if it could not be found.
	*
	* @param courseSubjectId the primary key of the course subject
	* @return the course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject findByPrimaryKey(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException;

	/**
	* Returns the course subject with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param courseSubjectId the primary key of the course subject
	* @return the course subject, or <code>null</code> if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject fetchByPrimaryKey(
		long courseSubjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the course subjects where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseSubject> findBycourseId(
		long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course subjects where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @return the range of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseSubject> findBycourseId(
		long courseId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course subjects where courseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param courseId the course ID
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseSubject> findBycourseId(
		long courseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course subject in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject findBycourseId_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException;

	/**
	* Returns the first course subject in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject fetchBycourseId_First(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course subject in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject findBycourseId_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException;

	/**
	* Returns the last course subject in the ordered set where courseId = &#63;.
	*
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject fetchBycourseId_Last(
		long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course subjects before and after the current course subject in the ordered set where courseId = &#63;.
	*
	* @param courseSubjectId the primary key of the current course subject
	* @param courseId the course ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject[] findBycourseId_PrevAndNext(
		long courseSubjectId, long courseId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException;

	/**
	* Returns the course subject where subjectId = &#63; or throws a {@link info.diit.portal.NoSuchCourseSubjectException} if it could not be found.
	*
	* @param subjectId the subject ID
	* @return the matching course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject findBysubjectId(long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException;

	/**
	* Returns the course subject where subjectId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param subjectId the subject ID
	* @return the matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject fetchBysubjectId(long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course subject where subjectId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param subjectId the subject ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject fetchBysubjectId(
		long subjectId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the course subjects where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseSubject> findByorganization(
		long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course subjects where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @return the range of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseSubject> findByorganization(
		long organizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course subjects where organizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param organizationId the organization ID
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseSubject> findByorganization(
		long organizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject findByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException;

	/**
	* Returns the first course subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject fetchByorganization_First(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject findByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException;

	/**
	* Returns the last course subject in the ordered set where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject fetchByorganization_Last(
		long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course subjects before and after the current course subject in the ordered set where organizationId = &#63;.
	*
	* @param courseSubjectId the primary key of the current course subject
	* @param organizationId the organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject[] findByorganization_PrevAndNext(
		long courseSubjectId, long organizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException;

	/**
	* Returns all the course subjects where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseSubject> findBycompany(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course subjects where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @return the range of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseSubject> findBycompany(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course subjects where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseSubject> findBycompany(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first course subject in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject findBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException;

	/**
	* Returns the first course subject in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject fetchBycompany_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last course subject in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject findBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException;

	/**
	* Returns the last course subject in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching course subject, or <code>null</code> if a matching course subject could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject fetchBycompany_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the course subjects before and after the current course subject in the ordered set where companyId = &#63;.
	*
	* @param courseSubjectId the primary key of the current course subject
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next course subject
	* @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject[] findBycompany_PrevAndNext(
		long courseSubjectId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException;

	/**
	* Returns all the course subjects.
	*
	* @return the course subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseSubject> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the course subjects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @return the range of course subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseSubject> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the course subjects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of course subjects
	* @param end the upper bound of the range of course subjects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of course subjects
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.CourseSubject> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course subjects where courseId = &#63; from the database.
	*
	* @param courseId the course ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the course subject where subjectId = &#63; from the database.
	*
	* @param subjectId the subject ID
	* @return the course subject that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.CourseSubject removeBysubjectId(
		long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCourseSubjectException;

	/**
	* Removes all the course subjects where organizationId = &#63; from the database.
	*
	* @param organizationId the organization ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course subjects where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the course subjects from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course subjects where courseId = &#63;.
	*
	* @param courseId the course ID
	* @return the number of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public int countBycourseId(long courseId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course subjects where subjectId = &#63;.
	*
	* @param subjectId the subject ID
	* @return the number of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public int countBysubjectId(long subjectId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course subjects where organizationId = &#63;.
	*
	* @param organizationId the organization ID
	* @return the number of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public int countByorganization(long organizationId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course subjects where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching course subjects
	* @throws SystemException if a system exception occurred
	*/
	public int countBycompany(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of course subjects.
	*
	* @return the number of course subjects
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
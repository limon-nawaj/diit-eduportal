/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.ClassRoutineEventBatch;

import java.util.List;

/**
 * The persistence utility for the class routine event batch service. This utility wraps {@link ClassRoutineEventBatchPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see ClassRoutineEventBatchPersistence
 * @see ClassRoutineEventBatchPersistenceImpl
 * @generated
 */
public class ClassRoutineEventBatchUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(ClassRoutineEventBatch classRoutineEventBatch) {
		getPersistence().clearCache(classRoutineEventBatch);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ClassRoutineEventBatch> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ClassRoutineEventBatch> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ClassRoutineEventBatch> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static ClassRoutineEventBatch update(
		ClassRoutineEventBatch classRoutineEventBatch, boolean merge)
		throws SystemException {
		return getPersistence().update(classRoutineEventBatch, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static ClassRoutineEventBatch update(
		ClassRoutineEventBatch classRoutineEventBatch, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence()
				   .update(classRoutineEventBatch, merge, serviceContext);
	}

	/**
	* Caches the class routine event batch in the entity cache if it is enabled.
	*
	* @param classRoutineEventBatch the class routine event batch
	*/
	public static void cacheResult(
		info.diit.portal.model.ClassRoutineEventBatch classRoutineEventBatch) {
		getPersistence().cacheResult(classRoutineEventBatch);
	}

	/**
	* Caches the class routine event batchs in the entity cache if it is enabled.
	*
	* @param classRoutineEventBatchs the class routine event batchs
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.ClassRoutineEventBatch> classRoutineEventBatchs) {
		getPersistence().cacheResult(classRoutineEventBatchs);
	}

	/**
	* Creates a new class routine event batch with the primary key. Does not add the class routine event batch to the database.
	*
	* @param classRoutineEventBatchId the primary key for the new class routine event batch
	* @return the new class routine event batch
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch create(
		long classRoutineEventBatchId) {
		return getPersistence().create(classRoutineEventBatchId);
	}

	/**
	* Removes the class routine event batch with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param classRoutineEventBatchId the primary key of the class routine event batch
	* @return the class routine event batch that was removed
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch remove(
		long classRoutineEventBatchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException {
		return getPersistence().remove(classRoutineEventBatchId);
	}

	public static info.diit.portal.model.ClassRoutineEventBatch updateImpl(
		info.diit.portal.model.ClassRoutineEventBatch classRoutineEventBatch,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(classRoutineEventBatch, merge);
	}

	/**
	* Returns the class routine event batch with the primary key or throws a {@link info.diit.portal.NoSuchClassRoutineEventBatchException} if it could not be found.
	*
	* @param classRoutineEventBatchId the primary key of the class routine event batch
	* @return the class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch findByPrimaryKey(
		long classRoutineEventBatchId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException {
		return getPersistence().findByPrimaryKey(classRoutineEventBatchId);
	}

	/**
	* Returns the class routine event batch with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param classRoutineEventBatchId the primary key of the class routine event batch
	* @return the class routine event batch, or <code>null</code> if a class routine event batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch fetchByPrimaryKey(
		long classRoutineEventBatchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(classRoutineEventBatchId);
	}

	/**
	* Returns all the class routine event batchs where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findByCompanyId(
		long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompanyId(companyId);
	}

	/**
	* Returns a range of all the class routine event batchs where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @return the range of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findByCompanyId(
		long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCompanyId(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the class routine event batchs where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findByCompanyId(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCompanyId(companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first class routine event batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch findByCompanyId_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException {
		return getPersistence()
				   .findByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the first class routine event batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch fetchByCompanyId_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the last class routine event batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch findByCompanyId_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException {
		return getPersistence()
				   .findByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last class routine event batch in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch fetchByCompanyId_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the class routine event batchs before and after the current class routine event batch in the ordered set where companyId = &#63;.
	*
	* @param classRoutineEventBatchId the primary key of the current class routine event batch
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch[] findByCompanyId_PrevAndNext(
		long classRoutineEventBatchId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException {
		return getPersistence()
				   .findByCompanyId_PrevAndNext(classRoutineEventBatchId,
			companyId, orderByComparator);
	}

	/**
	* Returns all the class routine event batchs where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findBybatchId(
		long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybatchId(batchId);
	}

	/**
	* Returns a range of all the class routine event batchs where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @return the range of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findBybatchId(
		long batchId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybatchId(batchId, start, end);
	}

	/**
	* Returns an ordered range of all the class routine event batchs where batchId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param batchId the batch ID
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findBybatchId(
		long batchId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBybatchId(batchId, start, end, orderByComparator);
	}

	/**
	* Returns the first class routine event batch in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch findBybatchId_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException {
		return getPersistence().findBybatchId_First(batchId, orderByComparator);
	}

	/**
	* Returns the first class routine event batch in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch fetchBybatchId_First(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBybatchId_First(batchId, orderByComparator);
	}

	/**
	* Returns the last class routine event batch in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch findBybatchId_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException {
		return getPersistence().findBybatchId_Last(batchId, orderByComparator);
	}

	/**
	* Returns the last class routine event batch in the ordered set where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch fetchBybatchId_Last(
		long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBybatchId_Last(batchId, orderByComparator);
	}

	/**
	* Returns the class routine event batchs before and after the current class routine event batch in the ordered set where batchId = &#63;.
	*
	* @param classRoutineEventBatchId the primary key of the current class routine event batch
	* @param batchId the batch ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch[] findBybatchId_PrevAndNext(
		long classRoutineEventBatchId, long batchId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException {
		return getPersistence()
				   .findBybatchId_PrevAndNext(classRoutineEventBatchId,
			batchId, orderByComparator);
	}

	/**
	* Returns all the class routine event batchs where classRoutineEventId = &#63;.
	*
	* @param classRoutineEventId the class routine event ID
	* @return the matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findByClassRoutineEventId(
		long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByClassRoutineEventId(classRoutineEventId);
	}

	/**
	* Returns a range of all the class routine event batchs where classRoutineEventId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param classRoutineEventId the class routine event ID
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @return the range of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findByClassRoutineEventId(
		long classRoutineEventId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByClassRoutineEventId(classRoutineEventId, start, end);
	}

	/**
	* Returns an ordered range of all the class routine event batchs where classRoutineEventId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param classRoutineEventId the class routine event ID
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findByClassRoutineEventId(
		long classRoutineEventId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByClassRoutineEventId(classRoutineEventId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first class routine event batch in the ordered set where classRoutineEventId = &#63;.
	*
	* @param classRoutineEventId the class routine event ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch findByClassRoutineEventId_First(
		long classRoutineEventId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException {
		return getPersistence()
				   .findByClassRoutineEventId_First(classRoutineEventId,
			orderByComparator);
	}

	/**
	* Returns the first class routine event batch in the ordered set where classRoutineEventId = &#63;.
	*
	* @param classRoutineEventId the class routine event ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch fetchByClassRoutineEventId_First(
		long classRoutineEventId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByClassRoutineEventId_First(classRoutineEventId,
			orderByComparator);
	}

	/**
	* Returns the last class routine event batch in the ordered set where classRoutineEventId = &#63;.
	*
	* @param classRoutineEventId the class routine event ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch findByClassRoutineEventId_Last(
		long classRoutineEventId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException {
		return getPersistence()
				   .findByClassRoutineEventId_Last(classRoutineEventId,
			orderByComparator);
	}

	/**
	* Returns the last class routine event batch in the ordered set where classRoutineEventId = &#63;.
	*
	* @param classRoutineEventId the class routine event ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching class routine event batch, or <code>null</code> if a matching class routine event batch could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch fetchByClassRoutineEventId_Last(
		long classRoutineEventId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByClassRoutineEventId_Last(classRoutineEventId,
			orderByComparator);
	}

	/**
	* Returns the class routine event batchs before and after the current class routine event batch in the ordered set where classRoutineEventId = &#63;.
	*
	* @param classRoutineEventBatchId the primary key of the current class routine event batch
	* @param classRoutineEventId the class routine event ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next class routine event batch
	* @throws info.diit.portal.NoSuchClassRoutineEventBatchException if a class routine event batch with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.ClassRoutineEventBatch[] findByClassRoutineEventId_PrevAndNext(
		long classRoutineEventBatchId, long classRoutineEventId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchClassRoutineEventBatchException {
		return getPersistence()
				   .findByClassRoutineEventId_PrevAndNext(classRoutineEventBatchId,
			classRoutineEventId, orderByComparator);
	}

	/**
	* Returns all the class routine event batchs.
	*
	* @return the class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the class routine event batchs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @return the range of class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the class routine event batchs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of class routine event batchs
	* @param end the upper bound of the range of class routine event batchs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.ClassRoutineEventBatch> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the class routine event batchs where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCompanyId(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCompanyId(companyId);
	}

	/**
	* Removes all the class routine event batchs where batchId = &#63; from the database.
	*
	* @param batchId the batch ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBybatchId(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBybatchId(batchId);
	}

	/**
	* Removes all the class routine event batchs where classRoutineEventId = &#63; from the database.
	*
	* @param classRoutineEventId the class routine event ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByClassRoutineEventId(long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByClassRoutineEventId(classRoutineEventId);
	}

	/**
	* Removes all the class routine event batchs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of class routine event batchs where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCompanyId(long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCompanyId(companyId);
	}

	/**
	* Returns the number of class routine event batchs where batchId = &#63;.
	*
	* @param batchId the batch ID
	* @return the number of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static int countBybatchId(long batchId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBybatchId(batchId);
	}

	/**
	* Returns the number of class routine event batchs where classRoutineEventId = &#63;.
	*
	* @param classRoutineEventId the class routine event ID
	* @return the number of matching class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByClassRoutineEventId(long classRoutineEventId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByClassRoutineEventId(classRoutineEventId);
	}

	/**
	* Returns the number of class routine event batchs.
	*
	* @return the number of class routine event batchs
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ClassRoutineEventBatchPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ClassRoutineEventBatchPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					ClassRoutineEventBatchPersistence.class.getName());

			ReferenceRegistry.registerReference(ClassRoutineEventBatchUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(ClassRoutineEventBatchPersistence persistence) {
	}

	private static ClassRoutineEventBatchPersistence _persistence;
}
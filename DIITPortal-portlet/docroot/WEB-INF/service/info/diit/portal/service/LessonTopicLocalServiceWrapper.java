/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link LessonTopicLocalService}.
 * </p>
 *
 * @author    mohammad
 * @see       LessonTopicLocalService
 * @generated
 */
public class LessonTopicLocalServiceWrapper implements LessonTopicLocalService,
	ServiceWrapper<LessonTopicLocalService> {
	public LessonTopicLocalServiceWrapper(
		LessonTopicLocalService lessonTopicLocalService) {
		_lessonTopicLocalService = lessonTopicLocalService;
	}

	/**
	* Adds the lesson topic to the database. Also notifies the appropriate model listeners.
	*
	* @param lessonTopic the lesson topic
	* @return the lesson topic that was added
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic addLessonTopic(
		info.diit.portal.model.LessonTopic lessonTopic)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.addLessonTopic(lessonTopic);
	}

	/**
	* Creates a new lesson topic with the primary key. Does not add the lesson topic to the database.
	*
	* @param lessonTopicId the primary key for the new lesson topic
	* @return the new lesson topic
	*/
	public info.diit.portal.model.LessonTopic createLessonTopic(
		long lessonTopicId) {
		return _lessonTopicLocalService.createLessonTopic(lessonTopicId);
	}

	/**
	* Deletes the lesson topic with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonTopicId the primary key of the lesson topic
	* @return the lesson topic that was removed
	* @throws PortalException if a lesson topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic deleteLessonTopic(
		long lessonTopicId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.deleteLessonTopic(lessonTopicId);
	}

	/**
	* Deletes the lesson topic from the database. Also notifies the appropriate model listeners.
	*
	* @param lessonTopic the lesson topic
	* @return the lesson topic that was removed
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic deleteLessonTopic(
		info.diit.portal.model.LessonTopic lessonTopic)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.deleteLessonTopic(lessonTopic);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _lessonTopicLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.dynamicQueryCount(dynamicQuery);
	}

	public info.diit.portal.model.LessonTopic fetchLessonTopic(
		long lessonTopicId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.fetchLessonTopic(lessonTopicId);
	}

	/**
	* Returns the lesson topic with the primary key.
	*
	* @param lessonTopicId the primary key of the lesson topic
	* @return the lesson topic
	* @throws PortalException if a lesson topic with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic getLessonTopic(long lessonTopicId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.getLessonTopic(lessonTopicId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the lesson topics.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of lesson topics
	* @param end the upper bound of the range of lesson topics (not inclusive)
	* @return the range of lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<info.diit.portal.model.LessonTopic> getLessonTopics(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.getLessonTopics(start, end);
	}

	/**
	* Returns the number of lesson topics.
	*
	* @return the number of lesson topics
	* @throws SystemException if a system exception occurred
	*/
	public int getLessonTopicsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.getLessonTopicsCount();
	}

	/**
	* Updates the lesson topic in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lessonTopic the lesson topic
	* @return the lesson topic that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic updateLessonTopic(
		info.diit.portal.model.LessonTopic lessonTopic)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.updateLessonTopic(lessonTopic);
	}

	/**
	* Updates the lesson topic in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param lessonTopic the lesson topic
	* @param merge whether to merge the lesson topic with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the lesson topic that was updated
	* @throws SystemException if a system exception occurred
	*/
	public info.diit.portal.model.LessonTopic updateLessonTopic(
		info.diit.portal.model.LessonTopic lessonTopic, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.updateLessonTopic(lessonTopic, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _lessonTopicLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_lessonTopicLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _lessonTopicLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	public java.util.List<info.diit.portal.model.LessonTopic> findByLessonPlan(
		long lessonPlanId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.findByLessonPlan(lessonPlanId);
	}

	public java.util.List<info.diit.portal.model.LessonTopic> findByLessonTopic(
		long lessonPlanId, long topicId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _lessonTopicLocalService.findByLessonTopic(lessonPlanId, topicId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public LessonTopicLocalService getWrappedLessonTopicLocalService() {
		return _lessonTopicLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedLessonTopicLocalService(
		LessonTopicLocalService lessonTopicLocalService) {
		_lessonTopicLocalService = lessonTopicLocalService;
	}

	public LessonTopicLocalService getWrappedService() {
		return _lessonTopicLocalService;
	}

	public void setWrappedService(
		LessonTopicLocalService lessonTopicLocalService) {
		_lessonTopicLocalService = lessonTopicLocalService;
	}

	private LessonTopicLocalService _lessonTopicLocalService;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import info.diit.portal.model.CounselingCourseInterest;

import java.util.List;

/**
 * The persistence utility for the counseling course interest service. This utility wraps {@link CounselingCourseInterestPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see CounselingCourseInterestPersistence
 * @see CounselingCourseInterestPersistenceImpl
 * @generated
 */
public class CounselingCourseInterestUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(
		CounselingCourseInterest counselingCourseInterest) {
		getPersistence().clearCache(counselingCourseInterest);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CounselingCourseInterest> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CounselingCourseInterest> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CounselingCourseInterest> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static CounselingCourseInterest update(
		CounselingCourseInterest counselingCourseInterest, boolean merge)
		throws SystemException {
		return getPersistence().update(counselingCourseInterest, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static CounselingCourseInterest update(
		CounselingCourseInterest counselingCourseInterest, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence()
				   .update(counselingCourseInterest, merge, serviceContext);
	}

	/**
	* Caches the counseling course interest in the entity cache if it is enabled.
	*
	* @param counselingCourseInterest the counseling course interest
	*/
	public static void cacheResult(
		info.diit.portal.model.CounselingCourseInterest counselingCourseInterest) {
		getPersistence().cacheResult(counselingCourseInterest);
	}

	/**
	* Caches the counseling course interests in the entity cache if it is enabled.
	*
	* @param counselingCourseInterests the counseling course interests
	*/
	public static void cacheResult(
		java.util.List<info.diit.portal.model.CounselingCourseInterest> counselingCourseInterests) {
		getPersistence().cacheResult(counselingCourseInterests);
	}

	/**
	* Creates a new counseling course interest with the primary key. Does not add the counseling course interest to the database.
	*
	* @param CounselingCourseInterestId the primary key for the new counseling course interest
	* @return the new counseling course interest
	*/
	public static info.diit.portal.model.CounselingCourseInterest create(
		long CounselingCourseInterestId) {
		return getPersistence().create(CounselingCourseInterestId);
	}

	/**
	* Removes the counseling course interest with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param CounselingCourseInterestId the primary key of the counseling course interest
	* @return the counseling course interest that was removed
	* @throws info.diit.portal.NoSuchCounselingCourseInterestException if a counseling course interest with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CounselingCourseInterest remove(
		long CounselingCourseInterestId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCounselingCourseInterestException {
		return getPersistence().remove(CounselingCourseInterestId);
	}

	public static info.diit.portal.model.CounselingCourseInterest updateImpl(
		info.diit.portal.model.CounselingCourseInterest counselingCourseInterest,
		boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(counselingCourseInterest, merge);
	}

	/**
	* Returns the counseling course interest with the primary key or throws a {@link info.diit.portal.NoSuchCounselingCourseInterestException} if it could not be found.
	*
	* @param CounselingCourseInterestId the primary key of the counseling course interest
	* @return the counseling course interest
	* @throws info.diit.portal.NoSuchCounselingCourseInterestException if a counseling course interest with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CounselingCourseInterest findByPrimaryKey(
		long CounselingCourseInterestId)
		throws com.liferay.portal.kernel.exception.SystemException,
			info.diit.portal.NoSuchCounselingCourseInterestException {
		return getPersistence().findByPrimaryKey(CounselingCourseInterestId);
	}

	/**
	* Returns the counseling course interest with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param CounselingCourseInterestId the primary key of the counseling course interest
	* @return the counseling course interest, or <code>null</code> if a counseling course interest with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static info.diit.portal.model.CounselingCourseInterest fetchByPrimaryKey(
		long CounselingCourseInterestId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(CounselingCourseInterestId);
	}

	/**
	* Returns all the counseling course interests.
	*
	* @return the counseling course interests
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CounselingCourseInterest> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the counseling course interests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of counseling course interests
	* @param end the upper bound of the range of counseling course interests (not inclusive)
	* @return the range of counseling course interests
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CounselingCourseInterest> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the counseling course interests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of counseling course interests
	* @param end the upper bound of the range of counseling course interests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of counseling course interests
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<info.diit.portal.model.CounselingCourseInterest> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the counseling course interests from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of counseling course interests.
	*
	* @return the number of counseling course interests
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CounselingCourseInterestPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CounselingCourseInterestPersistence)PortletBeanLocatorUtil.locate(info.diit.portal.service.ClpSerializer.getServletContextName(),
					CounselingCourseInterestPersistence.class.getName());

			ReferenceRegistry.registerReference(CounselingCourseInterestUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(CounselingCourseInterestPersistence persistence) {
	}

	private static CounselingCourseInterestPersistence _persistence;
}
package info.diit.portal.dailywork;

import info.diit.portal.NoSuchEmployeeException;
import info.diit.portal.dto.TaskListDto;
import info.diit.portal.dto.WorkReportDto;
import info.diit.portal.model.DailyWork;
import info.diit.portal.model.Designation;
import info.diit.portal.model.Employee;
import info.diit.portal.model.EmployeeMobile;
import info.diit.portal.model.Task;
import info.diit.portal.model.TaskDesignation;
import info.diit.portal.model.impl.DailyWorkImpl;
import info.diit.portal.service.DailyWorkLocalServiceUtil;
import info.diit.portal.service.DesignationLocalServiceUtil;
import info.diit.portal.service.EmployeeLocalServiceUtil;
import info.diit.portal.service.EmployeeMobileLocalServiceUtil;
import info.diit.portal.service.TaskDesignationLocalServiceUtil;
import info.diit.portal.service.TaskLocalServiceUtil;
import com.vaadin.terminal.Resource;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import net.atontech.vaadin.ui.numericfield.NumericField;
import net.atontech.vaadin.ui.numericfield.widgetset.shared.NumericFieldType;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.theme.ThemeDisplay;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.terminal.StreamResource;
import com.vaadin.terminal.StreamResource.StreamSource;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

public class DailyWorkApp extends Application implements PortletRequestListener{

	private static ThemeDisplay themeDisplay;
	private static Window window;
	
	private final static String DATE_FORMAT = "dd/MM/yyyy";
	SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
	
	private BeanItemContainer<TaskListDto> taskContainer;
	
	private final static String TASK = "task";
	private final static String TIME = "time";
	private final static String NOTE = "note";
	
	private DailyWork dailyWork;
	
	private final static String ITEM_TODAY = "Today";
	private final static String ITEM_YESTERDAY = "Yesterday";
	private final static String ITEM_THIS_WEEK = "This Week";
	private final static String ITEM_LAST_WEEK = "Last Week";
	private final static String ITEM_THIS_MONTH = "This Month";
	private final static String ITEM_LAST_MONTH = "Last Month";
	
	private final static String COLUMN_TASK = "task";
	private final static String COLUMN_TIME = "time";
	private final static String COLUMN_AVERAGE = "average";
	
	private Table reportTable;
	
	DecimalFormat twoDecimalFormat = new DecimalFormat("0.00");
	
	private static BeanItemContainer<WorkReportDto> reportContainer;
	
	public void init() {
		window = new Window();
		setMainWindow(window);
		window.addComponent(tabSheet());
		loadTask();
		loadReport();
	}
	
	private TabSheet tabSheet(){
		TabSheet tabSheet = new TabSheet();
		tabSheet.addTab(mainLayout(), "Daily Work");
		tabSheet.addTab(report(), "Work Report");
		return tabSheet;
	}
	
	private DateField workingDate;
	private TextField nameField;
	private TextField spentTimeField;
	private TextField designationField;
	private Table taskListTable;
	private Button saveButton;
	
	private GridLayout mainLayout(){
		GridLayout mainGridLayout = new GridLayout(8, 8);
		mainGridLayout.setWidth("100%");
		mainGridLayout.setHeight("100%");
		mainGridLayout.setSpacing(true);
		
		workingDate = new DateField("Date");
		workingDate.setWidth("70%");
		workingDate.setResolution(DateField.RESOLUTION_DAY);
		workingDate.setDateFormat(DATE_FORMAT);
		workingDate.setValue(new Date());
		workingDate.setImmediate(true);
				
		nameField = new TextField("Name");
		nameField.setWidth("70%");
		
		spentTimeField = new TextField("Total Time Spent");
		spentTimeField.setWidth("70%");
		spentTimeField.setReadOnly(true);
		designationField = new TextField("Designation");
		designationField.setWidth("70%");
		
		try {
			Employee emp = EmployeeLocalServiceUtil.findByUserId(themeDisplay.getUser().getUserId());
			
			String name = emp.getName();
			if (emp!=null) {
				nameField.setValue(name);
				Designation designation = DesignationLocalServiceUtil.fetchDesignation(emp.getDesignation());
				if (designation!=null) {
					designationField.setValue(designation.getDesignationTitle());
				}
			}
		} catch (NoSuchEmployeeException e1) {
			e1.printStackTrace();
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
		
		nameField.setReadOnly(true);
		designationField.setReadOnly(true);
		
		taskContainer = new BeanItemContainer<TaskListDto>(TaskListDto.class);
		taskListTable = new Table("Task Checklist", taskContainer);
		taskListTable.setWidth("100%");
		taskListTable.setEditable(true);
		taskListTable.setSelectable(true);
		taskListTable.setColumnHeader(TASK, "Tasks");
		taskListTable.setColumnHeader(TIME, "Time(minute)");
		taskListTable.setColumnHeader(NOTE, "Note");
		
//		taskListTable.setFooterVisible(true);
		
		taskListTable.setVisibleColumns(new String[]{TASK, TIME, NOTE});
		taskListTable.setColumnExpandRatio(TASK, 4);
		taskListTable.setColumnExpandRatio(TIME, 1);
		taskListTable.setColumnExpandRatio(NOTE, 4);
		
		
		
		taskListTable.setTableFieldFactory(new DefaultFieldFactory(){
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				if (propertyId.equals(TASK)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				if (propertyId.equals(TIME)) {
					NumericField field = new NumericField();
					field.setNullSettingAllowed(true);
					field.setNumberType(NumericFieldType.INTEGER);
					field.setWidth("100%");
					field.setNullRepresentation("");
					
					field.addListener(new BlurListener() {
						
						@Override
						public void blur(BlurEvent event) {
							calculateSpentTime();
						}
					});
					
					field.setReadOnly(false);
					return field;
				}
				if (propertyId.equals(NOTE)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setNullRepresentation("");
					field.setReadOnly(false);
					return field;
				}
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		
		
		Label spacer = new Label();
		saveButton = new Button("Save");
		saveButton.setEnabled(true);
		saveButton.setImmediate(true);
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (dailyWork==null) {
					dailyWork = new DailyWorkImpl();
				}
				
				dailyWork.setCompanyId(themeDisplay.getCompanyId());
				dailyWork.setUserId(themeDisplay.getUser().getUserId());
				dailyWork.setUserName(themeDisplay.getUser().getScreenName());
				
				/*try {
					dailyWork.setOrganizationId(themeDisplay.getLayout().getGroup().getOrganizationId());
				} catch (PortalException e1) {
					e1.printStackTrace();
				} catch (SystemException e1) {
					e1.printStackTrace();
				}*/
								
				Date date = (Date) workingDate.getValue();
				date.setHours(0);
				date.setMinutes(0);
				date.setSeconds(0);
				dailyWork.setWorKingDay(date);
				dailyWork.setCreateDate(new Date());
				try {
					Employee employee = EmployeeLocalServiceUtil.findByUserId(themeDisplay.getUser().getUserId());
					dailyWork.setEmployeeId(employee.getEmployeeId());
					
					List<DailyWork> dailyWorks = DailyWorkLocalServiceUtil.findByEmployeeWorkingDay(employee.getEmployeeId(), date);
					if (dailyWorks!=null) {
						for (DailyWork day : dailyWorks) {
							DailyWorkLocalServiceUtil.deleteDailyWork(day);
						}
					}
					
					for (int i = 0; i < taskContainer.size(); i++) {
						TaskListDto taskListDto = taskContainer.getIdByIndex(i);
						if (taskListDto.getTime()!=null) {
							dailyWork.setTaskId(taskListDto.getId());
							dailyWork.setTime(taskListDto.getTime());
							dailyWork.setNote(taskListDto.getNote());
							DailyWorkLocalServiceUtil.addDailyWork(dailyWork);
						}
					}
					loadReport();
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (NoSuchEmployeeException e) {
					e.printStackTrace();
				}
				window.showNotification("Daily work added successfully");
			}
		});
		
		workingDate.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				loadTask();
//				loadDailyWorkFotter();
				
				
				Calendar calendar = Calendar.getInstance();
				calendar.add(Calendar.DATE, -1);
				
				Date dateField = (Date) workingDate.getValue();
				dateField.setHours(0);
				dateField.setMinutes(0);
				dateField.setSeconds(0);
				String workDate = dateFormat.format(dateField);
				
				Date newDate = new Date();
				newDate.setHours(0);
				newDate.setMinutes(0);
				newDate.setSeconds(0);
				
				String today = dateFormat.format(newDate);
				
				Date lastDay = calendar.getTime();
				lastDay.setHours(0);
				lastDay.setMinutes(0);
				lastDay.setSeconds(0);
				String yesterday = dateFormat.format(lastDay);
//				window.showNotification("Yesterday:"+lastDay, Window.Notification.TYPE_ERROR_MESSAGE);
				
				if (today.equals(workDate) || yesterday.equals(workDate)) {
					taskListTable.setEditable(true);
					saveButton.setEnabled(true);
				}else{
					taskListTable.setEditable(false);
					saveButton.setEnabled(false);
				}
				
			}
		});
		
		HorizontalLayout rowLayout = new HorizontalLayout();
		rowLayout.setSpacing(true);
		rowLayout.setWidth("100%");
		rowLayout.addComponent(spacer);
		rowLayout.addComponent(saveButton);
		
		/*Button testButton = new Button("Test");
		testButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
//				loadDailyWorkFotter();
			}
		});*/
//		rowLayout.addComponent(testButton);
		rowLayout.setExpandRatio(spacer, 1);
		
		mainGridLayout.addComponent(workingDate, 0, 0, 3, 0);
		mainGridLayout.addComponent(spentTimeField, 0, 1, 3, 1);
		mainGridLayout.addComponent(nameField, 4, 0, 7, 0);
		mainGridLayout.addComponent(designationField, 4, 1, 7, 1);
		
		mainGridLayout.addComponent(taskListTable, 0, 2, 7, 6);
		mainGridLayout.addComponent(rowLayout, 0, 7, 7, 7);
		
		return mainGridLayout;
	}
	
	private void calculateSpentTime(){
		if (taskContainer!=null) {
			int spentTime = 0;
			for (int i = 0; i < taskContainer.size(); i++) {
				TaskListDto taskListDto = taskContainer.getIdByIndex(i);
				if (taskListDto.getTime()!=null) {
					spentTime = spentTime+taskListDto.getTime();
				}
			}
			
			int quotient = 0;
			int reminder = 0;
			if (spentTime>0) {
				quotient = spentTime/60;
				reminder = spentTime%60;
			}
			spentTimeField.setReadOnly(false);
			if (reminder<=9) {
				spentTimeField.setValue(quotient+":"+"0"+reminder);
			}else{
				spentTimeField.setValue(quotient+":"+reminder);
			}
			
			spentTimeField.setReadOnly(true);
		}
	}
	
	private void loadTask(){
		if (taskContainer!=null) {
			taskContainer.removeAllItems();
			try {
				Employee employee = EmployeeLocalServiceUtil.findByUserId(themeDisplay.getUser().getUserId());
				List<TaskDesignation> taskList = TaskDesignationLocalServiceUtil.findByDesignation(employee.getDesignation());
				Date date = (Date) workingDate.getValue();
				date.setHours(0);
				date.setMinutes(0);
				date.setSeconds(0);
				for (TaskDesignation taskDesignation : taskList) {
					TaskListDto taskListDto = new TaskListDto();
					taskListDto.setId(taskDesignation.getTaskId());
					Task task = TaskLocalServiceUtil.fetchTask(taskDesignation.getTaskId());
					taskListDto.setTask(task.getTitle());
					List<DailyWork> dailyWorks = DailyWorkLocalServiceUtil.findByEmployeeWorkingDay(employee.getEmployeeId(), date);
					for (DailyWork dayWork : dailyWorks) {
						if (dayWork.getTaskId()==taskDesignation.getTaskId()) {
							taskListDto.setTime(dayWork.getTime());
							taskListDto.setNote(dayWork.getNote());
							break;
						}else{
							taskListDto.setTime(null);
							taskListDto.setNote(null);
						}
					}
					
					taskContainer.addBean(taskListDto);
				}
				calculateSpentTime();
				taskContainer.sort(new Object[]{TASK}, new boolean[]{true});
				/*taskListTable.setSortContainerPropertyId(TASK);
				taskListTable.setSortAscending(true);*/
			} catch (SystemException e) {
				e.printStackTrace();
			} catch (NoSuchEmployeeException e) {
				e.printStackTrace();
			} 
		}
	}
	
	private ComboBox durationBox;
	private static DateField startDate;
	private static DateField endDate;
	
	private GridLayout report(){
		GridLayout reportLayout = new GridLayout(9, 8);
		reportLayout.setSpacing(true);
		reportLayout.setWidth("100%");
		durationBox = new ComboBox("Duration");
		durationBox.setImmediate(true);
		durationBox.addItem(ITEM_TODAY);
		durationBox.addItem(ITEM_YESTERDAY);
		durationBox.addItem(ITEM_THIS_WEEK);
		durationBox.addItem(ITEM_LAST_WEEK);
		durationBox.addItem(ITEM_THIS_MONTH);
		durationBox.addItem(ITEM_LAST_MONTH);
		durationBox.setValue(ITEM_THIS_MONTH);
		
		startDate = new DateField("Start Date");
		startDate.setResolution(DateField.RESOLUTION_DAY);
		startDate.setDateFormat(DATE_FORMAT);
		startDate.setImmediate(true);
		endDate = new DateField("End Date");
		endDate.setResolution(DateField.RESOLUTION_DAY);
		endDate.setDateFormat(DATE_FORMAT);
		endDate.setImmediate(true);
		
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());  
        calendar.set(Calendar.DAY_OF_MONTH, 1);  
        Date firstDayOfMonth = calendar.getTime();
        firstDayOfMonth.setHours(23);
        firstDayOfMonth.setMinutes(59);
        firstDayOfMonth.setSeconds(59);
        startDate.setValue(firstDayOfMonth);
        endDate.setValue(new Date());
		
		durationBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Object duration = durationBox.getValue();
				
				if (duration!=null) {
					if (duration.equals(ITEM_TODAY)) {
						startDate.setValue(new Date());
						endDate.setValue(new Date());
					}else if (duration.equals(ITEM_YESTERDAY)) {
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.DATE, -1);
						Date lastDay = cal.getTime();
						lastDay.setHours(0);
						lastDay.setMinutes(0);
						lastDay.setSeconds(0);
						startDate.setValue(lastDay);
						endDate.setValue(lastDay);
					}else if (duration.equals(ITEM_THIS_WEEK)) {
						Calendar cal = Calendar.getInstance();
						
						cal.setFirstDayOfWeek(Calendar.SATURDAY);
						int firstDayOfWeek = cal.getFirstDayOfWeek();
						
						Calendar startDay = Calendar.getInstance();
						startDay.setTime(cal.getTime());
						
						int days = (startDay.get(Calendar.DAY_OF_WEEK)+7-firstDayOfWeek)%7;
						startDay.add(Calendar.DATE, -days);
						
						Date startDateOfWeek = startDay.getTime();
						startDateOfWeek.setHours(0);
						startDateOfWeek.setMinutes(0);
						startDateOfWeek.setSeconds(0);
						startDate.setValue(startDateOfWeek);
						endDate.setValue(new Date());
						
//						window.showNotification("Start date:"+startDayOfWeek.getTime());
						
					}else if (duration.equals(ITEM_LAST_WEEK)) {
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.DATE, -14);
						cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
						Date startDateOfWeek = cal.getTime();
						startDateOfWeek.setHours(0);
						startDateOfWeek.setMinutes(0);
						startDateOfWeek.setSeconds(0);
						startDate.setValue(startDateOfWeek);
						
						cal.add(Calendar.DATE, 6);
						Date endDateOfWeek = cal.getTime();
						endDateOfWeek.setHours(0);
						endDateOfWeek.setMinutes(0);
						endDateOfWeek.setSeconds(0);
						endDate.setValue(endDateOfWeek);
						
						/*cal.setFirstDayOfWeek(Calendar.SATURDAY);
						int firstDayOfWeek = cal.getFirstDayOfWeek();
						
						Calendar startDayOfWeek = Calendar.getInstance();
						startDayOfWeek.setTime(cal.getTime());
						
						int days = (startDayOfWeek.get(Calendar.DAY_OF_WEEK)+14-firstDayOfWeek)%14;
						startDayOfWeek.add(Calendar.DATE, -days);
						
						Date startDateOfWeek = startDayOfWeek.getTime();
						startDateOfWeek.setHours(0);
						startDateOfWeek.setMinutes(0);
						startDateOfWeek.setSeconds(0);
						
						window.showNotification("Start date:"+startDateOfWeek);*/
						
					}else if (duration.equals(ITEM_THIS_MONTH)) {
				        calendar.setTime(new Date());  
				        calendar.set(Calendar.DAY_OF_MONTH, 1);  
				        Date firstDayOfMonth = calendar.getTime();
				        firstDayOfMonth.setHours(0);
				        firstDayOfMonth.setMinutes(0);
				        firstDayOfMonth.setSeconds(0);
				        startDate.setValue(firstDayOfMonth);
				        endDate.setValue(new Date());
					}else if (duration.equals(ITEM_LAST_MONTH)) {
				        int monthdays = calendar.get(Calendar.DAY_OF_MONTH);
//				        window.showNotification("Month days:"+monthdays);
				        calendar.add(Calendar.DAY_OF_MONTH,-monthdays);        
				        Date lastdayofmonth = calendar.getTime();
				        lastdayofmonth.setHours(0);
				        lastdayofmonth.setMinutes(0);
				        lastdayofmonth.setSeconds(0);
				        int monthdays2 = calendar.get(Calendar.DAY_OF_MONTH);
				        int anotherday = monthdays2 - 1;
				        calendar.add(Calendar.DAY_OF_MONTH, - anotherday);
				        Date firstdayofmonth = calendar.getTime();
				        firstdayofmonth.setHours(0);
				        firstdayofmonth.setMinutes(0);
				        firstdayofmonth.setSeconds(0);
				        startDate.setValue(firstdayofmonth);
				        endDate.setValue(lastdayofmonth);
					}
					
				}
//				loadReport();
			}
		});
		
		startDate.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Date start = (Date) startDate.getValue();
				Date end = (Date) endDate.getValue();
				end.setHours(23);
				end.setMinutes(59);
				end.setSeconds(59);
				if (start!=null) {
					if (start.before(end)) {
						loadReport();
					}
				}
			}
		});
		
		
		endDate.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				Date start = (Date) startDate.getValue();
				Date end = (Date) endDate.getValue();
				end.setHours(23);
				end.setMinutes(59);
				end.setSeconds(59);
				if (end!=null) {
					if (end.before(start)) {
//						endDate.setValue(null);
						window.showNotification("Please select valid end date", Window.Notification.TYPE_ERROR_MESSAGE);
						return;
					}else{
						loadReport();
					}
				}
			}
		});
		
		reportContainer = new BeanItemContainer<WorkReportDto>(WorkReportDto.class);
//		reportContainer.sort(new Object[]{COLUMN_TASK}, new boolean[]{true});
		
		reportTable = new Table("Task Report", reportContainer);
		reportTable.setImmediate(true);
		reportTable.setWidth("100%");
		reportTable.setFooterVisible(true);
		
		reportTable.setColumnHeader(COLUMN_TASK, "Task");
		reportTable.setColumnHeader(COLUMN_TIME, "Time");
		reportTable.setColumnHeader(COLUMN_AVERAGE, "Average");
		reportTable.setVisibleColumns(new String[]{COLUMN_TASK, COLUMN_TIME, COLUMN_AVERAGE});
		
		reportTable.setColumnExpandRatio(COLUMN_TASK, 5);
		reportTable.setColumnExpandRatio(COLUMN_TIME, 2);
		reportTable.setColumnExpandRatio(COLUMN_AVERAGE, 2);
		
		
		/*Button test = new Button("Test");
		test.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(DailyWork.class);
				Date startDay = (Date) startDate.getValue();
				startDay.setHours(0);
				startDay.setMinutes(0);
				startDay.setSeconds(0);
				Date endDay = (Date) endDate.getValue();
				endDay.setHours(0);
				endDay.setMinutes(0);
				endDay.setSeconds(0);
				Criterion criterion = RestrictionsFactoryUtil.between("worKingDay", startDay, endDay);
				dynamicQuery.add(criterion);
				try {
					List<DailyWork> taskDesignations = DailyWorkLocalServiceUtil.dynamicQuery(dynamicQuery);
					window.showNotification("Size"+taskDesignations.size());
					
				} catch (SystemException e) {
					e.printStackTrace();
				}
				
			}
		});*/
		
		Label spacer = new Label();
		
		final Embedded pdfContents = new Embedded();
        pdfContents.setSizeFull();
        pdfContents.setType(Embedded.TYPE_BROWSER);
        
		Button printButton = new Button("Print");
		printButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				displayPopup();
			}
		});
		HorizontalLayout rowLayout = new HorizontalLayout();
		rowLayout.setWidth("100%");
		rowLayout.addComponent(spacer);
		rowLayout.addComponent(printButton);
		rowLayout.setExpandRatio(spacer, 1);
		
		reportLayout.addComponent(durationBox, 0, 0, 2, 0);
		reportLayout.addComponent(startDate, 3, 0, 5, 0);
		reportLayout.addComponent(endDate, 6, 0, 8, 0);
		reportLayout.addComponent(reportTable, 0, 1, 8, 1);
		reportLayout.addComponent(rowLayout, 0, 2, 8, 2);
		return reportLayout;
	}
	
	private Set<Long> taskIds;
	private static Set<Date> dateList;
	
	private void loadReport(){
		Date startDay = (Date) startDate.getValue();
		startDay.setHours(0);
		startDay.setMinutes(0);
		startDay.setSeconds(0);
		Date endDay = (Date) endDate.getValue();
		endDay.setHours(0);
		endDay.setMinutes(0);
		endDay.setSeconds(0);
		
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(DailyWork.class);
		Criterion criterion = RestrictionsFactoryUtil.between("worKingDay", startDay, endDay);
		
		
		
		try {
			Employee employee = EmployeeLocalServiceUtil.findByUserId(themeDisplay.getUser().getUserId());
			dynamicQuery.add(criterion).add(PropertyFactoryUtil.forName("employeeId").eq(employee.getEmployeeId()));
			
			List<DailyWork> dailyWorks = DailyWorkLocalServiceUtil.dynamicQuery(dynamicQuery);
			
			if (taskIds!=null) {
				taskIds.clear();
			}else{
				taskIds = new HashSet<Long>();
			}
			
			dateList = new HashSet<Date>();
			
			if (dailyWorks!=null) {
				for (DailyWork dailyWork : dailyWorks) {
					taskIds.add(dailyWork.getTaskId());
					dateList.add(dailyWork.getWorKingDay());
				}
			}
			
			if (reportContainer!=null) {
				reportContainer.removeAllItems();
			}
			
			int totalSpentTime = 0;
//			int rowCount = 0;
			
			
			
			for (Long id : taskIds) {
				DynamicQuery timQeuery = DynamicQueryFactoryUtil.forClass(DailyWork.class);
				timQeuery.add(criterion).add(PropertyFactoryUtil.forName("employeeId").eq(employee.getEmployeeId()))
						.add(PropertyFactoryUtil.forName("taskId").eq(id)).setProjection(ProjectionFactoryUtil.sum("time"));
				
				List timeList = DailyWorkLocalServiceUtil.dynamicQuery(timQeuery);
				int time = 0;
				if(timeList != null)
				{
					time = ((Long)timeList.get(0)).intValue();
				}
				
				totalSpentTime = totalSpentTime + time;
				
				int quotient = 0;
				int reminder = 0;
				
				DynamicQuery avgTimQeuery = DynamicQueryFactoryUtil.forClass(DailyWork.class);
				avgTimQeuery.add(criterion).add(PropertyFactoryUtil.forName("employeeId").eq(employee.getEmployeeId()))
						.add(PropertyFactoryUtil.forName("taskId").eq(id)).setProjection(ProjectionFactoryUtil.rowCount());
				
				List avgTimeList = DailyWorkLocalServiceUtil.dynamicQuery(avgTimQeuery);
				
				int rows = 0;
				if (avgTimeList!=null) {
					rows = ((Long)avgTimeList.get(0)).intValue();
				}
				
//				rowCount = rows + rowCount;
				
				int avgQuotient = 0;
				int avgReminder = 0;
				
				if (time>0) {
					quotient = time/60;
					reminder = time%60;
					
					avgQuotient = time/rows;
					avgReminder = time/rows;
				}
				
				if (avgQuotient>0) {
					avgQuotient = avgQuotient/60;
				}
				
				if (avgReminder>0) {
					avgReminder = avgReminder%60;
				}
				
				for (DailyWork dailyWork : dailyWorks) {
					
					if (dailyWork.getTaskId()==id) {
						Task task = TaskLocalServiceUtil.fetchTask(id);
//						window.showNotification("Task:"+task.getTitle());
						WorkReportDto workReportDto = new WorkReportDto();
						workReportDto.setId(dailyWork.getDailyWorkId());
						workReportDto.setTaskId(task.getTaskId());
						workReportDto.setTask(task.getTitle());
						
						
						
						if (reminder<=9) {
							workReportDto.setTime(quotient+":0"+reminder);
						}else{
							workReportDto.setTime(quotient+":"+reminder);
						}
						
						if (avgReminder<=9) {
							workReportDto.setAverage(avgQuotient+":0"+avgReminder);
						}else{
							workReportDto.setAverage(avgQuotient+":"+avgReminder);
						}
						
						reportContainer.addBean(workReportDto);
						
						break;
					}
				}
				
			}
//			window.showNotification("Total Spent time:"+totalSpentTime);
			
//			window.showNotification("Total Date: "+dateList.size());
			
			reportTable.setColumnFooter(COLUMN_TASK, "TOTAL");
			
			int footerQuotient = 0;
			int footerReminder = 0;
			
			if (totalSpentTime>0) {
				footerQuotient = totalSpentTime/60;
				footerReminder = totalSpentTime%60;
			}
			
			if (footerReminder<=9) {
				reportTable.setColumnFooter(COLUMN_TIME, footerQuotient+":0"+footerReminder);
			}else{
				reportTable.setColumnFooter(COLUMN_TIME, footerQuotient+":"+footerReminder);
			}
			
			int perDayQuotient = 0;
			int perDayReminder = 0;
			double perDayAvg = 0;
			if (totalSpentTime>0) {
				perDayAvg = totalSpentTime/dateList.size();
				
				perDayQuotient = (int) (perDayAvg/60);
				perDayReminder = (int) (perDayAvg%60);
			}
			
			
			
			if (perDayReminder<=9) {
				reportTable.setColumnFooter(COLUMN_AVERAGE, perDayQuotient+":0"+perDayReminder+" (per day)");
			}else{
				reportTable.setColumnFooter(COLUMN_AVERAGE, perDayQuotient+":"+perDayReminder+" (per day)");
			}
			
			reportContainer.sort(new Object[]{COLUMN_TASK}, new boolean[]{true});
			
			
		} catch (NoSuchEmployeeException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
//	PDF
	private Resource createPdf() {
        StreamResource resource = new StreamResource(new Pdf(), "test.pdf?" + System.currentTimeMillis(), this);
        resource.setMIMEType("application/pdf");
        return resource;
    }

    private void displayPopup() {
        Window win = new Window();
        win.getContent().setSizeFull();
        win.setResizable(true);
        win.setWidth("800");
        win.setHeight("600");
        win.center();
        Embedded e = new Embedded();
        e.setSizeFull();
        e.setType(Embedded.TYPE_BROWSER);

        StreamResource resource = new StreamResource(new Pdf(), "test.pdf?" + System.currentTimeMillis(), this);
        resource.setMIMEType("application/pdf");

        e.setSource(resource);
        win.addComponent(e);
        getMainWindow().addWindow(win);
    }
    
    public static class Pdf implements StreamSource {
        private final ByteArrayOutputStream os = new ByteArrayOutputStream();

        public Pdf() {
            Document document = null;

            try {
                document = new Document(PageSize.A4, 50, 50, 50, 50);
                PdfWriter.getInstance(document, os);
                document.setMarginMirroring(true);
                document.open();
                
                Paragraph companyTitleParagraph = new Paragraph();
                Font companyTitleFont = new Font();
                companyTitleFont.setSize(16);
                companyTitleParagraph.setFont(companyTitleFont);
                companyTitleParagraph.add(themeDisplay.getCompany().getName());
                companyTitleParagraph.setAlignment(Element.ALIGN_CENTER);
                companyTitleParagraph.setSpacingAfter(10);
                document.add(companyTitleParagraph);
                
                Paragraph titleParagraph = new Paragraph();
                Font titleFont = new Font();
                titleFont.setSize(14);
                titleParagraph.setFont(titleFont);
                titleParagraph.add("Work Report");
                titleParagraph.setAlignment(Element.ALIGN_CENTER);
                titleParagraph.setSpacingAfter(10);
                document.add(titleParagraph);
                
                Paragraph dateParagraph = new Paragraph();
                Font dateFont = new Font();
                dateFont.setSize(14);
                dateParagraph.setFont(dateFont);
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
                Date start = (Date) startDate.getValue();
                Date end = (Date) endDate.getValue();
//                Employee employee = EmployeeLocalServiceUtil.findByUserId(themeDisplay.getUser().getUserId());
                dateParagraph.add(dateFormat.format(start)+" to "+dateFormat.format(end));
                dateParagraph.setAlignment(Element.ALIGN_CENTER);
                dateParagraph.setSpacingAfter(10);
                document.add(dateParagraph);
                
                PdfPTable detailTable = new PdfPTable(2);
                detailTable.setSpacingAfter(30);
                detailTable.setHorizontalAlignment(Element.ALIGN_CENTER);
                PdfPCell detailCell;
                
                Employee employee = EmployeeLocalServiceUtil.findByUserId(themeDisplay.getUser().getUserId());
                
                detailCell = new PdfPCell(new Phrase("Employee Id: "+employee.getEmployeeId()));
                detailCell.setBorder(PdfPCell.NO_BORDER);
                detailTable.addCell(detailCell);
                
                
                Designation designation = DesignationLocalServiceUtil.fetchDesignation(employee.getDesignation());
                
                detailCell = new PdfPCell(new Phrase("Designation: "+designation.getDesignationTitle()));
                detailCell.setBorder(PdfPCell.NO_BORDER);
                detailTable.addCell(detailCell);
                
                detailCell = new PdfPCell(new Phrase("Name: "+employee.getName())); 
                detailCell.setBorder(PdfPCell.NO_BORDER);
                detailTable.addCell(detailCell);
                
                List<EmployeeMobile> mobiles = EmployeeMobileLocalServiceUtil.findByEmployeeStatus(employee.getEmployeeId(), 1);
                Phrase mobile = null;
                if (mobiles.size()>0) {
                	for (int i = 0; i < 1; i++) {
    					EmployeeMobile employeeMobile = mobiles.get(i);
    					detailCell = new PdfPCell(new Phrase("Mobile: "+employeeMobile.getMobile()));
    				}
				}else{
					detailCell = new PdfPCell(new Phrase("Mobile: "));
				}
                
				detailCell.setBorder(PdfPCell.NO_BORDER);
                detailTable.addCell(detailCell);
                document.add(detailTable);
                
                float[] colsWidth = {3f, 1.5f, 1.5f};
                
                PdfPTable table = new PdfPTable(colsWidth);
                PdfPCell cell;
                
                cell = new PdfPCell(new Phrase("Task"));
                cell.setBackgroundColor(Color.LIGHT_GRAY);
                cell.setPadding(5);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("Time Spent"));
                cell.setBackgroundColor(Color.LIGHT_GRAY);
                cell.setPadding(5);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("Average"));
                cell.setBackgroundColor(Color.LIGHT_GRAY);
                cell.setPadding(5);
                table.addCell(cell);
                
                if (reportContainer!=null) {
					for (int i = 0; i < reportContainer.size(); i++) {
						WorkReportDto workReport = reportContainer.getIdByIndex(i);
						cell = new PdfPCell(new Phrase(workReport.getTask()));
						cell.setPadding(5);
						table.addCell(cell);
						cell = new PdfPCell(new Phrase(workReport.getTime()));
						cell.setPadding(5);
						table.addCell(cell);
						cell = new PdfPCell(new Phrase(workReport.getAverage()));
						cell.setPadding(5);
						table.addCell(cell);
					}
					
					DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(DailyWork.class);
					Date startDay = (Date) startDate.getValue();
					startDay.setHours(0);
					startDay.setMinutes(0);
					startDay.setSeconds(0);
					Date endDay = (Date) endDate.getValue();
					endDay.setHours(0);
					endDay.setMinutes(0);
					endDay.setSeconds(0);
					Criterion criterion = RestrictionsFactoryUtil.between("worKingDay", startDay, endDay);
					
					Employee emp = EmployeeLocalServiceUtil.findByUserId(themeDisplay.getUser().getUserId());
					dynamicQuery.add(criterion).add(PropertyFactoryUtil.forName("employeeId").eq(emp.getEmployeeId()));
					
					List<DailyWork> dailyWorks = DailyWorkLocalServiceUtil.dynamicQuery(dynamicQuery);
					
					Set<Long> taskIdList = new HashSet<Long>();
 					
					for (DailyWork dailyWork : dailyWorks) {
						taskIdList.add(dailyWork.getTaskId());
					}
					
					int totalSpentTime = 0;
//					int rowCount = 0;
					
					for (Long id : taskIdList) {
						DynamicQuery timQeuery = DynamicQueryFactoryUtil.forClass(DailyWork.class);
						timQeuery.add(criterion).add(PropertyFactoryUtil.forName("employeeId").eq(emp.getEmployeeId()))
								.add(PropertyFactoryUtil.forName("taskId").eq(id)).setProjection(ProjectionFactoryUtil.sum("time"));
						
						List timeList = DailyWorkLocalServiceUtil.dynamicQuery(timQeuery);
						int time = 0;
						if(timeList != null)
						{
							time = ((Long)timeList.get(0)).intValue();
						}
						
						totalSpentTime = totalSpentTime + time;
						
						
						
						DynamicQuery avgTimQeuery = DynamicQueryFactoryUtil.forClass(DailyWork.class);
						avgTimQeuery.add(criterion).add(PropertyFactoryUtil.forName("employeeId").eq(emp.getEmployeeId()))
								.add(PropertyFactoryUtil.forName("taskId").eq(id)).setProjection(ProjectionFactoryUtil.rowCount());
						
						List totalRows = DailyWorkLocalServiceUtil.dynamicQuery(avgTimQeuery);
						
						int rows = 0;
						if (totalRows!=null) {
							rows = ((Long)totalRows.get(0)).intValue();
						}
						
//						rowCount = rows + rowCount;
					}
					
					cell = new PdfPCell(new Phrase("Total"));
	                cell.setBackgroundColor(Color.LIGHT_GRAY);
	                cell.setPadding(5);
	                table.addCell(cell);
	                
	                int quotient = 0;
	                int reminder = 0;
	                
	                
	                if (totalSpentTime>0) {
						quotient = totalSpentTime/60;
						reminder = totalSpentTime%60;
					}
	                
	                if (reminder<=9) {
	                	cell = new PdfPCell(new Phrase(quotient+":0"+reminder));
					}else{
						cell = new PdfPCell(new Phrase(quotient+":"+reminder));
					}
	                cell.setBackgroundColor(Color.LIGHT_GRAY);
	                cell.setPadding(5);
	                table.addCell(cell);
	                
	                int perDayQuotient = 0;
	    			int perDayReminder = 0;
	    			double perDayAvg = 0;
	    			if (totalSpentTime>0) {
	    				perDayAvg = totalSpentTime/dateList.size();
	    				
	    				perDayQuotient = (int) (perDayAvg/60);
	    				perDayReminder = (int) (perDayAvg%60);
	    			}
	                
	                if (perDayReminder<=9) {
	                	cell = new PdfPCell(new Phrase(perDayQuotient+":0"+perDayReminder+" (per day)"));
					}else{
						cell = new PdfPCell(new Phrase(perDayQuotient+":"+perDayReminder+" (per day)"));
					}
	                cell.setBackgroundColor(Color.LIGHT_GRAY);
	                cell.setPadding(5);
	                table.addCell(cell);
	                
					document.add(table);
				}
                
                
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (document != null) {
                    document.close();
                }
            }
        }
        
        public InputStream getStream() {
            // Here we return the pdf contents as a byte-array
            return new ByteArrayInputStream(os.toByteArray());
        }
    }

	public void onRequestStart(PortletRequest request, PortletResponse response) {
		
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

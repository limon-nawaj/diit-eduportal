package info.diit.portal.dailywork;

import info.diit.portal.dto.DesignationDto;
import info.diit.portal.dto.TaskDesignationDto;
import info.diit.portal.model.Designation;
import info.diit.portal.model.Task;
import info.diit.portal.model.TaskDesignation;
import info.diit.portal.model.impl.TaskDesignationImpl;
import info.diit.portal.service.DesignationLocalServiceUtil;
import info.diit.portal.service.TaskDesignationLocalServiceUtil;
import info.diit.portal.service.TaskLocalServiceUtil;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

public class DailyWorkManageApp extends Application implements PortletRequestListener {

	private ThemeDisplay themeDisplay;
	private Window window;
	
	private final static String COLUMN_CHECK = "checkBox";
	private final static String COLUMN_TASK = "task";
	
	private BeanItemContainer<TaskDesignationDto> taskContainer;
	private List<DesignationDto> designationList;
	
	private TaskDesignation taskDesignation;
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        loadDesignation();
        window.addComponent(mainLayout());
        loadTaskList();
    }
    
    private ComboBox designationBox;
	private Table taskListTable;
	
	private GridLayout mainLayout(){
		GridLayout mainLayout = new GridLayout(2,8);
		mainLayout.setWidth("100%");
		mainLayout.setSpacing(true);
		designationBox = new ComboBox("Designation");
		designationBox.setWidth("100%");
		designationBox.setImmediate(true);
		if (designationList!=null) {
			for (DesignationDto designationDto : designationList) {
				designationBox.addItem(designationDto);
			}
		}
		
		designationBox.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				loadTaskList();
			}
		});
		
		taskContainer = new BeanItemContainer<TaskDesignationDto>(TaskDesignationDto.class);
		taskListTable = new Table("Task List", taskContainer);
		taskListTable.setWidth("70%");
		taskListTable.setEditable(true);
		taskListTable.setSelectable(true);
		taskListTable.setColumnHeader(COLUMN_CHECK, "Check");
		taskListTable.setColumnHeader(COLUMN_TASK, "Tasks");
		
		taskListTable.setVisibleColumns(new String[]{COLUMN_CHECK, COLUMN_TASK});
		
		taskListTable.setColumnExpandRatio(COLUMN_TASK, 1);
		
		taskListTable.setTableFieldFactory(new DefaultFieldFactory(){
			
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				if (propertyId.equals(COLUMN_CHECK)) {
					CheckBox field = new CheckBox();
					field.setWidth("100%");
					field.setReadOnly(false);
					return field;
				}
				if (propertyId.equals(COLUMN_TASK)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		Label spacer = new Label();
		Button saveButton = new Button("Save");
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (taskDesignation==null) {
					taskDesignation = new TaskDesignationImpl();
				}
				taskDesignation.setCompanyId(themeDisplay.getCompanyId());
				taskDesignation.setUserId(themeDisplay.getUser().getUserId());
				taskDesignation.setUserName(themeDisplay.getUser().getScreenName());
				
				DesignationDto designation = (DesignationDto) designationBox.getValue();
				if (designation!=null) {
					taskDesignation.setDesignationId(designation.getId());
				}
				try {
					List<TaskDesignation> taskDesignations = TaskDesignationLocalServiceUtil.findByDesignation(designation.getId());
					if (taskDesignations!=null) {
						for (TaskDesignation taskDesignation : taskDesignations) {
							TaskDesignationLocalServiceUtil.deleteTaskDesignation(taskDesignation);
						}
					}
					if (taskContainer!=null) {
						for (int i = 0; i < taskContainer.size(); i++) {
							TaskDesignationDto task = taskContainer.getIdByIndex(i);
							taskDesignation.setTaskId(task.getId());
							if (task.getCheckBox()==true) {
								TaskDesignationLocalServiceUtil.addTaskDesignation(taskDesignation);
							}
						}
					}
					window.showNotification("Task assign successfully");
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
		
		HorizontalLayout rowLayout = new HorizontalLayout();
		rowLayout.setWidth("70%");
		rowLayout.setSpacing(true);
		rowLayout.addComponent(spacer);
		rowLayout.addComponent(saveButton);
		rowLayout.setExpandRatio(spacer, 1);
		
		mainLayout.addComponent(designationBox, 0, 0, 0, 0);
		mainLayout.addComponent(taskListTable, 0, 1, 1, 6);
		mainLayout.addComponent(rowLayout, 0, 7, 1, 7);
		return mainLayout;
	}
	
	private List<DesignationDto> loadDesignation(){
		try {
			if (designationList==null) {
				designationList = new ArrayList<DesignationDto>();
			}else{
				designationList.clear();
			}
			List<Designation> designations = DesignationLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
			for (Designation designation : designations) {
				DesignationDto designationDto = new DesignationDto();
				designationDto.setId(designation.getDesignationId());
				designationDto.setTitle(designation.getDesignationTitle());
				designationList.add(designationDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return designationList;
	}
	
	private void loadTaskList(){
		if (taskContainer!=null) {
			taskContainer.removeAllItems();
			try {
				DesignationDto designation = (DesignationDto) designationBox.getValue();
				List<Task> taskList = TaskLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
				if (taskList!=null) {
					for (Task task : taskList) {
						TaskDesignationDto taskDto = new TaskDesignationDto();
						taskDto.setId(task.getTaskId());
						taskDto.setTask(task.getTitle());
						if (designation!=null) {
							List<TaskDesignation> taskDesignations = TaskDesignationLocalServiceUtil.findByDesignation(designation.getId());
							for (TaskDesignation taskDesignation : taskDesignations) {
								if (task.getTaskId()==taskDesignation.getTaskId()) {
									taskDto.setCheckBox(true);
									break;
								}else{
									taskDto.setCheckBox(false);
								}
							}
						}
						taskContainer.sort(new Object[]{"task"}, new boolean[]{true});
						taskContainer.addBean(taskDto);
					}
				}
				
			} catch (SystemException e) {
				e.printStackTrace();
			} 
		}
	}

	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

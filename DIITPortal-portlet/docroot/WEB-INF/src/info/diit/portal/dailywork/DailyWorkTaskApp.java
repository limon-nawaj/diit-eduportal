package info.diit.portal.dailywork;

import info.diit.portal.dto.TaskDto;
import info.diit.portal.model.Task;
import info.diit.portal.model.impl.TaskImpl;
import info.diit.portal.service.TaskLocalServiceUtil;

import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class DailyWorkTaskApp extends Application implements PortletRequestListener {

	private ThemeDisplay themeDisplay;
	private Window window;
	
	private BeanItemContainer<TaskDto> taskContainer;
	
	private final static String COLUMN_TASK = "title";
	
	private Table taskTable;
	private Task task;
	
	private TextField taskField;
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        window.addComponent(mainLayout());
        loadTaskList();
    }
    
    private GridLayout mainLayout(){
		GridLayout mainGridLayout = new GridLayout(8, 4);
		mainGridLayout.setWidth("85%");
		mainGridLayout.setHeight("100%");
		mainGridLayout.setSpacing(true);
		
		VerticalLayout formLayout = new VerticalLayout();
		formLayout.setSpacing(true);
		formLayout.setWidth("100%");
		
		taskField = new TextField("Task Title");
		taskField.setWidth("100%");
		formLayout.addComponent(taskField);
		
		/*designationComboBox = new ComboBox("Designation");
		designationComboBox.setImmediate(true);
		if (designationList!=null) {
			for (DesignationDto designationDto : designationList) {
				designationComboBox.addItem(designationDto);
			}
		}*/
		
		Button saveButton = new Button("Save");
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
				if (task==null) {
					task = new TaskImpl();
					task.setNew(true);
				}
				task.setCompanyId(themeDisplay.getCompanyId());
				task.setUserId(themeDisplay.getUser().getUserId());
				task.setUserName(themeDisplay.getUser().getScreenName());
				
				String title = taskField.getValue().toString();
				if (title!=null) {
					task.setTitle(title);
				}
				
				try {
					if (task.isNew()) {
						task.setCreateDate(new Date());
						TaskLocalServiceUtil.addTask(task);
						window.showNotification("Task saved successfully");
					}else{
						task.setModifiedDate(new Date());
						TaskLocalServiceUtil.updateTask(task);
						window.showNotification("Task updated successfully");
					}
					loadTaskList();
					clear();
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
		formLayout.addComponent(saveButton);
		
		taskContainer = new BeanItemContainer<TaskDto>(TaskDto.class);
		taskTable = new Table("Task List", taskContainer);
		taskTable.setWidth("100%");
		taskTable.setHeight("100%");
		taskTable.setSelectable(true);
		
		taskTable.setColumnHeader(COLUMN_TASK, "Tasks");
		
		taskTable.setVisibleColumns(new String[]{COLUMN_TASK});
		
		HorizontalLayout rowLayout = new HorizontalLayout();
		rowLayout.setSpacing(true);
		rowLayout.setWidth("100%");
		Label spacer = new Label();
		Button editButton = new Button("Edit");
		editButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
				TaskDto taskDto = (TaskDto) taskTable.getValue();
				if (taskDto!=null) {
					taskDto.getTask().setNew(false);
					editTask(taskDto.getId());
				}
			}
		});
		
		Button deleteButton = new Button("Delete");
		deleteButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
				TaskDto taskDto = (TaskDto) taskTable.getValue();
				if (taskDto!=null) {
					try {
						Task task = TaskLocalServiceUtil.fetchTask(taskDto.getId());
						TaskLocalServiceUtil.deleteTask(task);
						loadTaskList();
						window.showNotification("Task deleted successfully");
						clear();
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}
			}
		});
		
		rowLayout.addComponent(spacer);
		rowLayout.addComponent(editButton);
		rowLayout.addComponent(deleteButton);
		rowLayout.setExpandRatio(spacer, 1);
		
		mainGridLayout.addComponent(formLayout, 0, 0, 4, 0);
		mainGridLayout.addComponent(taskTable, 0, 1, 4, 1);
		mainGridLayout.addComponent(rowLayout, 0, 2, 4, 2);
		
		return mainGridLayout;
	}
	
	private void loadTaskList(){
		if (taskContainer!=null) {
			taskContainer.removeAllItems();
			try {
				List<Task> taskList = TaskLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
				for (Task task : taskList) {
					TaskDto taskDto = new TaskDto();
					taskDto.setId(task.getTaskId());
					taskDto.setTitle(task.getTitle());
					taskContainer.addBean(taskDto);
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void editTask(long taskId){
		try {
			task = TaskLocalServiceUtil.fetchTask(taskId);
			taskField.setValue(task.getTitle());
//			designationComboBox.setValue(getDesignation(task.getDesignationId()));
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	/*private DesignationDto getDesignation(long designation){
		if (designationList!=null) {
			for (DesignationDto designationDto : designationList) {
				if (designationDto.getId()==designation) {
					return designationDto;
				}
			}
		}
		return null;
	}*/
	
	public void clear(){
		task = null;
		taskField.setValue("");
	}

	public void onRequestStart(PortletRequest request, PortletResponse response) {
		
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {

	}

}

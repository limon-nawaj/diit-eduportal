package info.diit.portal.counseling;

import info.diit.portal.dto.CounselingPerformanceDto;
import info.diit.portal.dto.CounselorDto;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class CounselingPerformanceApp extends Application implements PortletRequestListener{

	//Constants
	public final static String DATE_FORMAT = "dd/MM/yyyy";
	
	//columns for Performance table
	public final static String COLUMN_COURSE 		    = "courseCode";
	public final static String COLUMN_ADMISSION 		= "admission";
	public final static String COLUMN_REFERENCE 		= "reference";
	public final static String COLUMN_PHYSICAL_ENQUIRY 	= "physicalCounseling";
	public final static String COLUMN_PHONE_ENQUIRY 	= "phoneCounseling";
	public final static String COLUMN_EMAIL_ENQUIRY 	= "emailCounseling";
	public final static String COLUMN_SUCCESS_RATE 		= "successRate";
	
	
	//Performance Form Components 
	private DateField startDateField;
	private DateField endDateField;
	private ComboBox counselorComboBox;
	private Table performanceTable;

	
	
	private ThemeDisplay themeDisplay;
	private Window window;
	
	private static DecimalFormat twoDecimalFormat = new DecimalFormat("0.00");

	
	private BeanItemContainer<CounselingPerformanceDto> performanceBeanItemContainer;
	
    public void init() {
    	window = new Window();
		window.setSizeFull();
		setMainWindow(window);
		
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.setSpacing(true);

		verticalLayout.addComponent(initPerformanceLayout());
		
		
		window.addComponent(verticalLayout);
    }

    public VerticalLayout initPerformanceLayout()
	{
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setWidth("100%");
		
		
		performanceBeanItemContainer = new BeanItemContainer<CounselingPerformanceDto>(CounselingPerformanceDto.class);
		
		
		performanceTable = new Table("",performanceBeanItemContainer)
		{
			@Override
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				
				if(property.getType()==Double.class)
				{
					return twoDecimalFormat.format(property.getValue());
				}
				
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		performanceTable.setSelectable(true);
		performanceTable.setImmediate(true);
		
		performanceTable.setColumnHeader(COLUMN_COURSE, "Course");
		performanceTable.setColumnHeader(COLUMN_ADMISSION, "Admission");
		performanceTable.setColumnHeader(COLUMN_REFERENCE, "Reference");
		performanceTable.setColumnHeader(COLUMN_PHYSICAL_ENQUIRY, "Physical Enquiry");
		performanceTable.setColumnHeader(COLUMN_PHONE_ENQUIRY, "Phone Enquiry");
		performanceTable.setColumnHeader(COLUMN_EMAIL_ENQUIRY, "Email Enquiry");
		performanceTable.setColumnHeader(COLUMN_SUCCESS_RATE, "Success Rate");
		
		performanceTable.setVisibleColumns(new String[]{COLUMN_COURSE,COLUMN_ADMISSION,COLUMN_REFERENCE,COLUMN_PHYSICAL_ENQUIRY,COLUMN_PHONE_ENQUIRY,COLUMN_EMAIL_ENQUIRY,COLUMN_SUCCESS_RATE});
		
		performanceTable.setColumnAlignment(COLUMN_ADMISSION,Table.ALIGN_RIGHT);
		performanceTable.setColumnAlignment(COLUMN_REFERENCE,Table.ALIGN_RIGHT);
		performanceTable.setColumnAlignment(COLUMN_PHYSICAL_ENQUIRY,Table.ALIGN_RIGHT);
		performanceTable.setColumnAlignment(COLUMN_PHONE_ENQUIRY,Table.ALIGN_RIGHT);
		performanceTable.setColumnAlignment(COLUMN_EMAIL_ENQUIRY,Table.ALIGN_RIGHT);
		performanceTable.setColumnAlignment(COLUMN_SUCCESS_RATE,Table.ALIGN_RIGHT);
		
		performanceTable.setFooterVisible(true);
		
		performanceTable.setWidth("100%");
		//performanceTable.setHeight("150px");


		Date today = new Date();
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MONTH,-1);
		Date oneMonthBefore = now.getTime();

		startDateField = new DateField("Start");
		startDateField.setDateFormat("dd/MM/yyyy");
		startDateField.setResolution(DateField.RESOLUTION_DAY);
		startDateField.setValue(oneMonthBefore);
		startDateField.setImmediate(true);
		
		endDateField = new DateField("End");
		endDateField.setDateFormat("dd/MM/yyyy");
		endDateField.setResolution(DateField.RESOLUTION_DAY);
		endDateField.setValue(today);
		endDateField.setImmediate(true);
		
		startDateField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				showPerformance();
				
			}
		});
		
		endDateField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				showPerformance();
				
			}
		});
		
		
		counselorComboBox = new ComboBox("Select Counselor");
		counselorComboBox.setVisible(!isCounselor());
		counselorComboBox.setImmediate(true);
		counselorComboBox.setNewItemsAllowed(false);
		
		try
		{
			List<CounselorDto> counselors = CounselingDao.getCounselorList(themeDisplay.getCompanyId());
			
			for(CounselorDto counselor:counselors)
			{
				counselorComboBox.addItem(counselor);
			}
			
		} catch (PortalException e) {
		
			e.printStackTrace();
		} catch (SystemException e) {
		
			e.printStackTrace();
		}

		
		counselorComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {

				showPerformance();
				
			}
		});
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		horizontalLayout.addComponent(startDateField);
		horizontalLayout.addComponent(endDateField);
		horizontalLayout.addComponent(counselorComboBox);
		
		verticalLayout.addComponent(horizontalLayout);
		verticalLayout.addComponent(performanceTable);
		
		showPerformance();
		
		return verticalLayout;
		
	}
	
	public void loadPerformanceTableData(long userId, long companyId,Date startDate,Date endDate,boolean company)
	{
		
		int totalAdmission = 0;
		int totalReference = 0;
		int totalPhysicalEnquiry = 0;
		int totalPhoneEnquiry = 0;
		int totalEmailEnquiry = 0;
		
		
		if(performanceBeanItemContainer!=null)
		{
			performanceBeanItemContainer.removeAllItems();
			
			try {
				List<CounselingPerformanceDto> performanceDtos;
				
				if(!company)
				{
					performanceDtos = CounselingDao.getPerformanceListByUser(userId, companyId,startDate, endDate);
				}
				else
				{
					performanceDtos = CounselingDao.getPerformanceListByCompany(companyId,startDate, endDate);
				}
				
				for(CounselingPerformanceDto performanceDto:performanceDtos)
				{
					totalAdmission += performanceDto.getAdmission();
					totalReference += performanceDto.getReference();
					totalPhysicalEnquiry += performanceDto.getPhysicalCounseling();
					totalPhoneEnquiry += performanceDto.getPhoneCounseling();
					totalEmailEnquiry += performanceDto.getEmailCounseling();
						
					performanceBeanItemContainer.addBean(performanceDto);
				}
				performanceBeanItemContainer.sort(new String[]{COLUMN_COURSE}, new boolean[]{true});
				
				int totalEnquiry = totalPhysicalEnquiry+totalPhoneEnquiry+totalEmailEnquiry;
				
				double successRate = 0;
				
				if(totalEnquiry != 0)
				{
					successRate = (double)totalAdmission/totalEnquiry*100;
				}
				
				performanceTable.setColumnFooter(COLUMN_COURSE, "Total");
				performanceTable.setColumnFooter(COLUMN_ADMISSION, String.valueOf(totalAdmission));
				performanceTable.setColumnFooter(COLUMN_REFERENCE, String.valueOf(totalReference));
				performanceTable.setColumnFooter(COLUMN_PHYSICAL_ENQUIRY, String.valueOf(totalPhysicalEnquiry));
				performanceTable.setColumnFooter(COLUMN_PHONE_ENQUIRY, String.valueOf(totalPhoneEnquiry));
				performanceTable.setColumnFooter(COLUMN_EMAIL_ENQUIRY, String.valueOf(totalEmailEnquiry));
				performanceTable.setColumnFooter(COLUMN_SUCCESS_RATE, twoDecimalFormat.format(successRate));
				performanceTable.refreshRowCache();
				
				performanceTable.setPageLength(performanceBeanItemContainer.size());
				
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}

		
	}
	
	private void showPerformance()
	{
		Date startDate = (Date) startDateField.getValue();
		Date endDate = (Date) endDateField.getValue();
		
		if(startDate != null && endDate!=null)
		{
			startDate.setHours(0);
			startDate.setMinutes(0);
			startDate.setSeconds(0);
			
			endDate.setHours(23);
			endDate.setMinutes(59);
			endDate.setSeconds(59);
			
			CounselorDto counselor = (CounselorDto) counselorComboBox.getValue();
			
			long userId = 0;
			long companyId = themeDisplay.getCompanyId();
			
			if(counselor!=null)
			{
				userId = counselor.getUserId();
				loadPerformanceTableData(userId, companyId,startDate, endDate,false);
			}
			else
			{
				loadPerformanceTableData(userId, companyId,startDate, endDate,true);
			}
		}
		
	}
	
	private boolean isCounselor()
	{
		long userId = themeDisplay.getUserId();
		
		try
		{
			List<CounselorDto> counselors = CounselingDao.getCounselorList(themeDisplay.getCompanyId());
			
			for(CounselorDto counselor:counselors)
			{
				if(counselor.getUserId()==userId)
				{
					return true;
				}
			}
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

package info.diit.portal.employee;

import info.diit.portal.classroutine.CustomTimeField;
import info.diit.portal.dto.EmployeeDto;
import info.diit.portal.dto.EmployeeRoleDto;
import info.diit.portal.model.EmployeeRole;
import info.diit.portal.model.impl.EmployeeRoleImpl;
import info.diit.portal.service.EmployeeRoleLocalService;
import info.diit.portal.service.EmployeeRoleLocalServiceUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.vaadin.addon.customfield.CustomField;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class EmployeeRoleApplication extends Application implements PortletRequestListener {
	
	private ThemeDisplay themeDisplay;
	private Window window;
	
	private long organizationId;
	
	

    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        
        try {
			organizationId = themeDisplay.getLayout().getGroup().getOrganizationId();
			
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
        
        window.addComponent(mainLayout());
        if (organizationId>0) {
			loadRole();
		}
    }
    
    TextField roleField;
    CustomTimeField startTimeField;
    CustomTimeField endTimeField;
    Button saveButton;
    
    BeanItemContainer<EmployeeRoleDto> employeeRoleContainer;
    Table employeeRoleTable;
    
    private static final String ROLE = "role";
    private static final String START_TIME = "startTime";
    private static final String END_TIME = "endTime";
    
    private EmployeeRole employeeRole;
    
    SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
    
    private HorizontalLayout mainLayout(){
    	HorizontalLayout mainLayout = new HorizontalLayout();
    	mainLayout.setWidth("100%");
    	mainLayout.setSpacing(true);
    	
    	roleField = new TextField("Role");
    	roleField.setRequired(true);
    	
    	startTimeField = new CustomTimeField("Start Time");
    	startTimeField.setRequired(true);
    	startTimeField.setLocale(Locale.ROOT);
    	startTimeField.setTabIndex(2);
    	
    	endTimeField = new CustomTimeField("End Time");
    	endTimeField.setRequired(true);
    	endTimeField.setLocale(Locale.ROOT);
    	endTimeField.setTabIndex(3);
    	
    	saveButton = new Button("Save");
    	
    	saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				if (employeeRole==null) {
					employeeRole = new EmployeeRoleImpl();
					employeeRole.setNew(true);
				}
				
				employeeRole.setCompanyId(themeDisplay.getCompanyId());
				employeeRole.setOrganizationId(organizationId);
				
				String role = (String) roleField.getValue();
				if (role!=null && !role.equals(" ")) {
					employeeRole.setRole(role);
				}else{
					window.showNotification("Role can't be null", Window.Notification.TYPE_WARNING_MESSAGE);
					return;
				}
				
				Date startTime = (Date) startTimeField.getValue();
				if (startTime!=null) {
					employeeRole.setStartTime(startTime);
				}else{
					window.showNotification("Start time can't be null", Window.Notification.TYPE_WARNING_MESSAGE);
					return;
				}
				
				Date endTime = (Date) endTimeField.getValue();
				if (endTime!=null) {
					employeeRole.setEndTime(endTime);
				}else{
					window.showNotification("Start time can't be null", Window.Notification.TYPE_WARNING_MESSAGE);
					return;
				}
				
				try {
					if (employeeRole.isNew()) {
						EmployeeRoleLocalServiceUtil.addEmployeeRole(employeeRole);
						loadRole();
						window.showNotification("Role saved successfully");
					}else{
						EmployeeRoleLocalServiceUtil.updateEmployeeRole(employeeRole);
						loadRole();
						window.showNotification("Role updated successfully");
					}
					clear();
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
    	
    	Button clearButton = new Button("Clear");
    	clearButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				clear();
			}
		});
    	
    	HorizontalLayout buttonLayout = new HorizontalLayout();
    	buttonLayout.setSpacing(true);
    	buttonLayout.addComponent(saveButton);
    	buttonLayout.addComponent(clearButton);
    	
    	VerticalLayout formLayout = new VerticalLayout();
    	formLayout.setWidth("100%");
    	formLayout.setSpacing(true);
    	formLayout.addComponent(roleField);
    	formLayout.addComponent(startTimeField);
    	formLayout.addComponent(endTimeField);
    	formLayout.addComponent(buttonLayout);
    	
    	
    	
    	employeeRoleContainer = new BeanItemContainer<EmployeeRoleDto>(EmployeeRoleDto.class);
    	employeeRoleTable = new Table("", employeeRoleContainer);
    	employeeRoleTable.setWidth("100%");
    	
    	employeeRoleTable.setColumnHeader(ROLE, "Role");
    	employeeRoleTable.setColumnHeader(START_TIME, "Start Time");
    	employeeRoleTable.setColumnHeader(END_TIME, "End Time");
    	
    	employeeRoleTable.setEditable(true);
    	employeeRoleTable.setSelectable(true);
    	
    	employeeRoleTable.setTableFieldFactory(new DefaultFieldFactory(){
    		@Override
    		public Field createField(Container container, Object itemId,
    				Object propertyId, Component uiContext) {
    		
    			if (propertyId.equals(ROLE)) {
					TextField field = new TextField();
					field.setReadOnly(true);
					return field;
				}
    			if (propertyId.equals(START_TIME)) {
					DateField field = new DateField();
					field.setDateFormat("hh:mm a");
					field.setReadOnly(true);
					return field;
				}
    			
    			if (propertyId.equals(END_TIME)) {
    				DateField field = new DateField();
					field.setDateFormat("hh:mm a");
					field.setReadOnly(true);
					return field;
				}
    			// TODO Auto-generated method stub
    			return super.createField(container, itemId, propertyId, uiContext);
    		}
    	});
    	
    	employeeRoleTable.setVisibleColumns(new String[]{ROLE, START_TIME, END_TIME});
    	
    	Button editButton = new Button("Edit");
    	Button deleteButton = new Button("Delete");
    	
    	HorizontalLayout tableButtonLayout = new HorizontalLayout();
    	tableButtonLayout.addComponent(editButton);
    	tableButtonLayout.addComponent(deleteButton);
    	
    	editButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				EmployeeRoleDto roleDto = (EmployeeRoleDto) employeeRoleTable.getValue();
				if (roleDto!=null) {
					editRole(roleDto.getId());
				}
			}
		});
    	
    	deleteButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				EmployeeRoleDto roleDto = (EmployeeRoleDto) employeeRoleTable.getValue();
				if (roleDto!=null) {
					try {
						EmployeeRole role = EmployeeRoleLocalServiceUtil.fetchEmployeeRole(roleDto.getId());
						EmployeeRoleLocalServiceUtil.deleteEmployeeRole(role);
						loadRole();
						window.showNotification("Role daleted successfully");
					} catch (SystemException e) {
						e.printStackTrace();
					}
					
				}
			}
		});
    	
    	VerticalLayout tableLayout = new VerticalLayout();
    	tableLayout.setSpacing(true);
    	tableLayout.addComponent(employeeRoleTable);
    	tableLayout.addComponent(tableButtonLayout);
    	
    	mainLayout.addComponent(formLayout);
    	mainLayout.addComponent(tableLayout);
    	
    	return mainLayout;
    }
    
    private void editRole(long employeeRoleId){
    	try {
			employeeRole = EmployeeRoleLocalServiceUtil.fetchEmployeeRole(employeeRoleId);
			
			roleField.setValue(employeeRole.getRole());
			startTimeField.setValue(employeeRole.getStartTime());
			endTimeField.setValue(employeeRole.getEndTime());
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
    private void loadRole(){
    	employeeRoleContainer.removeAllItems();
    	try {
			List<EmployeeRole> roleList = EmployeeRoleLocalServiceUtil.findByOrganization(organizationId);
			if (roleList!=null) {
				for (EmployeeRole employeeRole : roleList) {
					EmployeeRoleDto roleDto = new EmployeeRoleDto();
					roleDto.setId(employeeRole.getEmployeeRoleId());
					roleDto.setRole(employeeRole.getRole());
					roleDto.setStartTime(employeeRole.getStartTime());
					roleDto.setEndTime(employeeRole.getEndTime());
					employeeRoleContainer.addBean(roleDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
    private void clear(){
    	employeeRole = null;
    	roleField.setValue("");
    	startTimeField.setValue(null);
    	endTimeField.setValue(null);
    }

	@Override
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

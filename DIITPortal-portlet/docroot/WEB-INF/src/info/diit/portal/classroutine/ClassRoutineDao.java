package info.diit.portal.classroutine;

import info.diit.portal.dto.BatchDto;
import info.diit.portal.dto.CourseDto;
import info.diit.portal.model.Batch;
import info.diit.portal.model.Course;
import info.diit.portal.model.CourseOrganization;
import info.diit.portal.service.BatchLocalServiceUtil;
import info.diit.portal.service.CourseLocalServiceUtil;
import info.diit.portal.service.CourseOrganizationLocalServiceUtil;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

public class ClassRoutineDao {
	public static ArrayList<CourseDto> getCourseListByCompany(long companyId) throws SystemException
	{
		List<Course> courses = CourseLocalServiceUtil.findByCompany(companyId);
		ArrayList<CourseDto> courseList = new ArrayList<CourseDto>(courses.size());
		
		for(Course course:courses)
		{
			CourseDto courseDto = new CourseDto();
			courseDto.setCourseCode(course.getCourseCode());
			courseDto.setCourseId(course.getCourseId());
			courseDto.setCourseName(course.getCourseName());
			
			courseList.add(courseDto);
		}
		
		return courseList;
	}
	
	public static ArrayList<CourseDto> getCourseListByOrganization(long organizationId) throws SystemException
	{
		List<CourseOrganization> courseOrganizations = CourseOrganizationLocalServiceUtil.findByOrganization(organizationId);
		
		ArrayList<CourseDto> courseList = new ArrayList<CourseDto>(courseOrganizations.size());
		
		for(CourseOrganization courseOrganization:courseOrganizations)
		{
			long courseId = courseOrganization.getCourseId();
			Course course = CourseLocalServiceUtil.fetchCourse(courseId);
			
			if(course!=null)
			{
				CourseDto courseDto = new CourseDto();
				courseDto.setCourseId(course.getCourseId());
				courseDto.setCourseCode(course.getCourseCode());
				courseDto.setCourseName(course.getCourseName());
				
				courseList.add(courseDto);
			}
			
		}
		
		return courseList;
		
	}
	
	public static ArrayList<BatchDto> getBatchListByCourse(long organizationId,long courseId) throws SystemException
	{
		List<Batch> batchList = BatchLocalServiceUtil.findBatchesByOrgCourseId(organizationId, courseId);
		ArrayList<BatchDto> batchDtoList = new ArrayList<BatchDto>(batchList.size());
		
		for(Batch batch:batchList)
		{
			BatchDto batchDto = new BatchDto();
			batchDto.setBatchId(batch.getBatchId());
			batchDto.setBatchName(batch.getBatchName());
			
			batchDtoList.add(batchDto);
		}
		
		return batchDtoList;
	}

}

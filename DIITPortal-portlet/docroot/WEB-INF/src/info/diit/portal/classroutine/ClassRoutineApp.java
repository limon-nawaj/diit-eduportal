package info.diit.portal.classroutine;

import info.diit.portal.NoSuchClassRoutineEventException;
import info.diit.portal.dto.BatchDto;
import info.diit.portal.dto.BatchSubjectDto;
import info.diit.portal.dto.CourseDto;
import info.diit.portal.dto.EventDto;
import info.diit.portal.dto.OrganizationDto;
import info.diit.portal.dto.RoomDto;
import info.diit.portal.dto.RoutineDto;
import info.diit.portal.dto.SubjectDto;
import info.diit.portal.dto.TeacherDto;
import info.diit.portal.model.Batch;
import info.diit.portal.model.BatchSubject;
import info.diit.portal.model.ClassRoutineEvent;
import info.diit.portal.model.ClassRoutineEventBatch;
import info.diit.portal.model.Course;
import info.diit.portal.model.CourseOrganization;
import info.diit.portal.model.Room;
import info.diit.portal.model.Subject;
import info.diit.portal.model.impl.ClassRoutineEventBatchImpl;
import info.diit.portal.model.impl.ClassRoutineEventImpl;
import info.diit.portal.service.BatchLocalServiceUtil;
import info.diit.portal.service.BatchSubjectLocalServiceUtil;
import info.diit.portal.service.ClassRoutineEventBatchLocalServiceUtil;
import info.diit.portal.service.ClassRoutineEventLocalServiceUtil;
import info.diit.portal.service.CourseLocalServiceUtil;
import info.diit.portal.service.CourseOrganizationLocalServiceUtil;
import info.diit.portal.service.RoomLocalServiceUtil;
import info.diit.portal.service.SubjectLocalServiceUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.vaadin.tokenfield.TokenField;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.OrganizationConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.addon.calendar.ui.CalendarComponentEvents;
import com.vaadin.addon.calendar.ui.CalendarComponentEvents.BackwardHandler;
import com.vaadin.addon.calendar.ui.CalendarComponentEvents.ForwardHandler;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Component.Event;
import com.vaadin.ui.Component.Listener;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.BaseTheme;

public class ClassRoutineApp extends Application implements PortletRequestListener{

	private Window						window;
	private Application					app;
	private VerticalLayout				verticalLayout;
	private ThemeDisplay				themeDisplay;
	private long						companyId;
	
	private GridLayout mainLayout;
	private final static String TEACHER_GROUP_NAME		= "Teacher";

	public void init() {
		window 			= new Window();
        app				=this;
		setMainWindow(window);
		
		verticalLayout 	= new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.setSpacing(true);
		
		companyId = themeDisplay.getCompanyId();
		loadBatch();
		loadTeacher();
		loadRoom();
		getOrganizationsByCompany();
		
		verticalLayout.addComponent(mainLayout());
		loadsideBar();
		window.addComponent(verticalLayout);
	}
	
	//components for batch fee setup
	private ComboBox routineCampusComboBox;
	private ComboBox routineCourseComboBox;
	private ComboBox routineBatchCombobox;
	private com.vaadin.addon.calendar.ui.Calendar routine;
	
	private OrganizationDto selectedCampus;
	private BeanItemContainer<EventDto> routineContainer;
	
	private Accordion accordion;
	Set<Long> allBatchId;
	Set<Long> teacherList;
	Set<Long> roomIdList;
	
	
	private GridLayout mainLayout(){
				
		routineCampusComboBox = new ComboBox("Campus");
		routineCampusComboBox.setImmediate(true);
		routineCampusComboBox.setWidth("100%");
		routineCampusComboBox.setNewItemsAllowed(false);
		
		if(organizationDtoList!=null)
		{
			for(OrganizationDto campus:organizationDtoList)
			{
				routineCampusComboBox.addItem(campus);
			}
		}
		
		if(organizationDtoList!=null && organizationDtoList.size()<=1)
		{
			routineCampusComboBox.setVisible(false);
		}

		routineCourseComboBox = new ComboBox("Course");
		routineCourseComboBox.setImmediate(true);
		routineCourseComboBox.setWidth("100%");
		
		if(selectedCampus!=null)
		{
			try {
				List<CourseDto> courseList = ClassRoutineDao.getCourseListByOrganization(selectedCampus.getOrganizationId());
				for(CourseDto courseDto:courseList)
				{
					routineCourseComboBox.addItem(courseDto);
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		
		routineBatchCombobox = new ComboBox("Batch");
		routineBatchCombobox.setImmediate(true);
		routineBatchCombobox.setWidth("100%");
		
		routineCampusComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				routineCourseComboBox.removeAllItems();
				OrganizationDto serchorg = (OrganizationDto)routineCampusComboBox.getValue();
				if(serchorg!=null){
					searchOrganizationId = serchorg.getOrganizationId();
					getCourseDtoByCompanyId();
					for (CourseDto course : courseDtoList) {
						routineCourseComboBox.addItem(course);
					}
				}
			}
		});
		
		routineCourseComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				routineBatchCombobox.removeAllItems();
				CourseDto course = (CourseDto) routineCourseComboBox.getValue();
				if(course!=null){
					courseId =course.getCourseId();
					getBatchDtoByCourseId(courseId);
					for(BatchDto batchDto:batchDtoList){
						routineBatchCombobox.addItem(batchDto);
					}
				}
			}
		});

		routineBatchCombobox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				BatchDto batch = (BatchDto)routineBatchCombobox.getValue();
				
				if(batch!=null)
				{
					loadEventByBatch(batch.getBatchId());
				}else {
					routineContainer.removeAllItems();
				}
			}
		});
		
		Button createEventButton = new Button("Create New Event");
		createEventButton.addListener(new Listener() {
			
			@Override
			public void componentEvent(Event event) {
				VerticalLayout layout = new VerticalLayout();
				layout.setMargin(true);
				layout.setSpacing(true);
				final Window popUpWindow = new Window(null, layout);
				
				layout.addComponent(classroutineLayout());
				popUpWindow.setWidth("70%");
				popUpWindow.center();
				popUpWindow.setModal(true);
				getMainWindow().addWindow(popUpWindow);
				loadEventComboBox();
			}
		});
		
		accordion = new Accordion();
		accordion.setSizeFull();
		
		VerticalLayout rightLayout = new VerticalLayout();
		rightLayout.setSizeFull();
		rightLayout.setSpacing(true);
		rightLayout.addComponent(createEventButton);
		rightLayout.addComponent(accordion);
		
		routineContainer = new BeanItemContainer<EventDto>(EventDto.class);
		
		routine = new com.vaadin.addon.calendar.ui.Calendar();
		routine.setSizeFull();
		routine.setImmediate(true);
		routine.setReadOnly(false);
		routine.setReadOnly(false);
		routine.setHandler((ForwardHandler)null);
		routine.setHandler((BackwardHandler)null);
//		routine.setDropHandler((DragAndDropEvent)null);
		
		routine.setWeeklyCaptionFormat("dd/MM/yyyy");
		
		showCurrentWeek();
		
		routine.setFirstVisibleHourOfDay(8);
		routine.setLastVisibleHourOfDay(22);
		
		routine.setContainerDataSource(routineContainer);
		
		routineContainer.sort(new Object[]{"start"}, new boolean[]{true});

		routine.setContainerDataSource(routineContainer, "caption","description", "start", "end", "styleName");
				
		routine.setHandler(new CalendarComponentEvents.EventClickHandler() {
            public void eventClick(CalendarComponentEvents.EventClick event) {
            	EventDto e = (EventDto) event.getCalendarEvent();
//            	getMainWindow().showNotification("Event clicked: " + e.getEventId());

            	VerticalLayout layout = new VerticalLayout();
				layout.setMargin(true);
				layout.setSpacing(true);
				final Window w = new Window(null, layout);
				
				layout.addComponent(classroutineLayout());
				w.setWidth("70%");
				w.center();
				w.setModal(true);
				getMainWindow().addWindow(w);  
				populateEvent(e.getEventId());
            }
        });
		mainLayout = new GridLayout(8, 8);
		mainLayout.setWidth("100%");
		mainLayout.setSpacing(true);
		mainLayout.addComponent(routineCampusComboBox, 0, 0, 1, 0);
		mainLayout.addComponent(routineCourseComboBox, 3, 0, 4, 0);
		mainLayout.addComponent(routineBatchCombobox, 6, 0, 7, 0);
		mainLayout.addComponent(routine, 0, 1, 5, 1);
		mainLayout.addComponent(rightLayout, 6, 1, 7, 1);
		
		return mainLayout;
	}
	
	private void loadsideBar(){
		accordion.removeAllComponents();
		VerticalLayout teacherVLayout = new VerticalLayout();
		if (teacherList!=null) {
			try {
				Iterator<Long> iterator = teacherList.iterator();
				while (iterator.hasNext()) {
					Long id = iterator.next();
					final User user = UserLocalServiceUtil.fetchUser(id);
					
					Button t = new Button(""+user.getFullName());
		    		t.setStyleName(BaseTheme.BUTTON_LINK);
		    		
		    		t.addListener(new ClickListener() {
						
						@Override
						public void buttonClick(ClickEvent event) {
							loadByBatchTeacherRoom(0, 0, user.getUserId());
//							window.showNotification("Id: "+user.getUserId()+" Name: "+user.getFullName());
						}
					});
		    		teacherVLayout.addComponent(t);
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
				
		VerticalLayout batchLayout = new VerticalLayout();
		if (allBatchId!=null) {
			try {
				Iterator<Long> iterator = allBatchId.iterator();
				while (iterator.hasNext()) {
					final Long batchId = iterator.next();
					final Batch batch = BatchLocalServiceUtil.fetchBatch(batchId);
					
					Button batchButton = new Button(""+batch.getBatchName());
					batchButton.setStyleName(BaseTheme.BUTTON_LINK);
					batchButton.addListener(new ClickListener() {
						
						@Override
						public void buttonClick(ClickEvent event) {
							loadByBatchTeacherRoom(batch.getBatchId(), 0, 0);
						}
					});
		    		batchLayout.addComponent(batchButton);
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		
		VerticalLayout roomLayout = new VerticalLayout();
		if (roomIdList!=null) {
			try {
				Iterator<Long> iterator = roomIdList.iterator();
				while (iterator.hasNext()) {
					Long id = iterator.next();
					final Room room = RoomLocalServiceUtil.fetchRoom(id);
					Button roomButton = new Button(""+room.getLabel());
					roomButton.setStyle(BaseTheme.BUTTON_LINK);
					roomButton.addListener(new ClickListener() {
						
						@Override
						public void buttonClick(ClickEvent event) {
							loadByBatchTeacherRoom(0, room.getRoomId(), 0);
						}
					});
					roomLayout.addComponent(roomButton);
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
			
		
		accordion.addTab(batchLayout, "Batch");
		accordion.addTab(teacherVLayout, "Teacher");
		accordion.addTab(roomLayout, "Room");
	}
	
	private void loadRoom(){
		try{
			roomIdList = new HashSet<Long>();
			/*List<ClassRoutineEvent> eventList = ClassRoutineEventLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
			if (eventList!=null) {
				for (ClassRoutineEvent classRoutineEvent : eventList) {
					roomIdList.add(classRoutineEvent.getRoomId());
				}
			}*/
			
			List<Room> roomList = RoomLocalServiceUtil.findByCompanyId(themeDisplay.getCompanyId());
			if (roomList!=null) {
				for (Room room : roomList) {
					roomIdList.add(room.getRoomId());
				}
			}
		} catch (SystemException e){
			e.printStackTrace();
		}
	}
	
	private void loadBatch(){
		try {
			allBatchId = new HashSet<Long>();
			/*List<ClassRoutineEventBatch> eventBatchList = ClassRoutineEventBatchLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
			if (eventBatchList!=null) {
				for (ClassRoutineEventBatch classRoutineEventBatch : eventBatchList) {
					allBatchId.add(classRoutineEventBatch.getBatchId());
					
				}
			}*/
			List<Batch> batchList = BatchLocalServiceUtil.findAllBatchByCompanyId(themeDisplay.getCompanyId());
			if (batchList!=null) {
				for (Batch batch : batchList) {
					if (batch.getStatus()==1) {
						allBatchId.add(batch.getBatchId());
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void loadTeacher(){
		/*if (allBatchId!=null) {
			teacherList = new HashSet<Long>();
			try {
				for (Long id : allBatchId) {
					List<BatchSubject> batchSubjectList = BatchSubjectLocalServiceUtil.findSubjectByBatch(id);
					List<ClassRoutineEventBatch> eventBatchList = ClassRoutineEventBatchLocalServiceUtil.findEventBatchByBatchId(id);
					if (eventBatchList!=null) {
						for (ClassRoutineEventBatch classRoutineEventBatch : eventBatchList) {
							ClassRoutineEvent event = ClassRoutineEventLocalServiceUtil.fetchClassRoutineEvent(classRoutineEventBatch.getClassRoutineEventId());
							for (BatchSubject batchSubject : batchSubjectList) {
								if (event.getSubjectId()==batchSubject.getSubjectId()) {
									teacherList.add(batchSubject.getTeacherId());
								}
							}
						}
					}
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}*/
		try {
			teacherList = new HashSet<Long>();
			
			UserGroup teacherGroup = UserGroupLocalServiceUtil.getUserGroup(companyId, TEACHER_GROUP_NAME);
			List<User> teachers = UserLocalServiceUtil.getUserGroupUsers(teacherGroup.getUserGroupId());
			if (teachers!=null) {
				for (User user : teachers) {
					teacherList.add(user.getUserId());
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}
	
//	load event from data
	private void loadByBatchTeacherRoom(long batchId, long roomId, long teacherId){
		routineContainer.removeAllItems();
		try {
			List<ClassRoutineEventBatch> eventBatchList = ClassRoutineEventBatchLocalServiceUtil.findEventBatchByBatchId(batchId);
			List<ClassRoutineEvent> eventRoomList = ClassRoutineEventLocalServiceUtil.findByRoom(roomId);
			List<BatchSubject> batchSubjects = BatchSubjectLocalServiceUtil.findByBatchSubjectTeacher(teacherId);
			if (eventBatchList!=null) {
				for (ClassRoutineEventBatch classRoutineEventBatch : eventBatchList) {
					loadEventByBatch(classRoutineEventBatch.getBatchId());
				}
			}
			if (eventRoomList!=null) {
				for (ClassRoutineEvent classRoutineEvent : eventRoomList) {
					setEvent(classRoutineEvent);
				}
			}
			if (batchSubjects!=null) {
				for (BatchSubject batchSubject : batchSubjects) {
					List<ClassRoutineEventBatch> eventBatchs = ClassRoutineEventBatchLocalServiceUtil.findEventBatchByBatchId(batchSubject.getBatchId());
					if (eventBatchs!=null) {
						for (ClassRoutineEventBatch classRoutineEventBatch : eventBatchs) {
							ClassRoutineEvent event = ClassRoutineEventLocalServiceUtil.fetchClassRoutineEvent(classRoutineEventBatch.getClassRoutineEventId());
							if (event!=null) {
								if (event.getSubjectId()==batchSubject.getSubjectId()) {
									setEvent(event);
								}
							}
						}
					}
				}
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	public void loadEventByBatch(long batchId){
		routineContainer.removeAllItems();
		try {
			if(batchId>0) {
				List<ClassRoutineEventBatch> batchList = ClassRoutineEventBatchLocalServiceUtil.findEventBatchByBatchId(batchId);
				if (batchList!=null && batchList.size()>0) {
					for (ClassRoutineEventBatch classRoutineBatch : batchList) {
						ClassRoutineEvent classRoutineDetails = ClassRoutineEventLocalServiceUtil.fetchClassRoutineEvent(classRoutineBatch.getClassRoutineEventId());
						if (classRoutineDetails!=null) {
							setEvent(classRoutineDetails);
						}
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void setEvent(ClassRoutineEvent classRoutineDetails){
		try {
			EventDto event = new EventDto();
			event.setEventId(classRoutineDetails.getClassRoutineEventId());
			
			Date s = new Date();
			s.setHours(classRoutineDetails.getStartTime().getHours());
			s.setMinutes(classRoutineDetails.getStartTime().getMinutes());
			
			Date e = new Date();
			e.setHours(classRoutineDetails.getEndTime().getHours());
			e.setMinutes(classRoutineDetails.getEndTime().getMinutes());
			
			java.util.Calendar calStart = java.util.Calendar.getInstance();
			calStart.setFirstDayOfWeek(java.util.Calendar.SATURDAY);
			calStart.setTime(s);
			calStart.set(java.util.Calendar.DAY_OF_WEEK, classRoutineDetails.getDay());
			
			java.util.Calendar calEnd = java.util.Calendar.getInstance();
			calEnd.setFirstDayOfWeek(java.util.Calendar.SATURDAY);
			calEnd.setTime(e);
			calEnd.set(java.util.Calendar.DAY_OF_WEEK, classRoutineDetails.getDay());
											
			event.setStart(calStart.getTime());
			event.setEnd(calEnd.getTime());
			
			List<BatchSubject> batchSubjectList = null;
			List<ClassRoutineEventBatch> eventBatchList = ClassRoutineEventBatchLocalServiceUtil.findEventBatchByEventId(classRoutineDetails.getClassRoutineEventId());
			if (eventBatchList!=null) {
				for (ClassRoutineEventBatch classRoutineEventBatch : eventBatchList) {
					batchSubjectList = getByBatch(classRoutineEventBatch.getBatchId());
				}
			}
			
			if (batchSubjectList!=null) {
				for (BatchSubject batchSubject : batchSubjectList) {
					if (classRoutineDetails.getSubjectId()==batchSubject.getSubjectId()) {
						User user = UserLocalServiceUtil.fetchUser(batchSubject.getTeacherId());
						TeacherDto teacher = new TeacherDto();
						teacher.setTeacherId(user.getUserId());
						teacher.setTeacherName(user.getFullName());
						Subject subject = SubjectLocalServiceUtil.fetchSubject(batchSubject.getSubjectId());
						SubjectDto subjectDto = new SubjectDto();
						subjectDto.setSubjectId(subject.getSubjectId());
						subjectDto.setSubjcetName(subject.getSubjectName());
						event.setTeacher(teacher);
						event.setSubject(subjectDto);
					}
				}
			}
			
			Room room = RoomLocalServiceUtil.fetchRoom(classRoutineDetails.getRoomId());
			RoomDto roomDto = new RoomDto();
			roomDto.setId(room.getRoomId());
			roomDto.setLabel(room.getLabel());
			
			event.setRoom(roomDto);
			
			event.setCaption("The new event");
//			event.setDescription("New event");
			
			event.setStyleName("attending");
																			
			SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a");
			
			event.setCaption(formatter.format(event.getEnd())+"\n"+event.getTeacher()+"\n"+event.getSubject()+"\n"+event.getRoom());
			event.setDescription(formatter.format(event.getStart())+"-"+formatter.format(event.getEnd())+", "+"\n"+event.getTeacher()+", "+"\n"+event.getSubject()+", "+"\n"+event.getRoom());
			
			routineContainer.addBean(event);
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
	}
	
	private List<BatchSubject> getByBatch(long batchId){
		try {
			List<BatchSubject> batchSubjectList = BatchSubjectLocalServiceUtil.findSubjectByBatch(batchId);
			if (batchSubjectList!=null) {
				return batchSubjectList;
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return null;
	}
//	load event from data
	
	private void loadRoutineComboBox(){
		OrganizationDto campusDto = (OrganizationDto) campusComboBox.getValue();
		CourseDto courseDTO = (CourseDto) courseComboBox.getValue();
		BatchDto batchDto = (BatchDto) batchComboBox.getValue();
		
		if (campusDto!=null && courseDTO!=null && batchDto!=null) {
			routineCampusComboBox.setValue(getOrganization(campusDto.getOrganizationId()));
			routineCourseComboBox.setValue(getCourse(courseDTO.getCourseId()));
			routineBatchCombobox.setValue(getBatch(batchDto.getBatchId()));
		}
	}
	
	private void loadEventComboBox(){
		OrganizationDto campusDto = (OrganizationDto) routineCampusComboBox.getValue();
		CourseDto courseDTO = (CourseDto) routineCourseComboBox.getValue();
		BatchDto batchDto = (BatchDto) routineBatchCombobox.getValue();
		
		if (campusDto!=null && courseDTO!=null && batchDto!=null) {
			campusComboBox.setValue(getOrganization(campusDto.getOrganizationId()));
			courseComboBox.setValue(getCourse(courseDTO.getCourseId()));
			batchComboBox.setValue(getBatch(batchDto.getBatchId()));
		}
	}
	
	private void showCurrentWeek()
	{
		//get the current week and set to routine calendar
		java.util.Calendar calendar = java.util.Calendar.getInstance();
		calendar.setFirstDayOfWeek(java.util.Calendar.SATURDAY);
		
		int currentWeek = calendar.get(java.util.Calendar.WEEK_OF_YEAR);
		int currentYear = calendar.get(java.util.Calendar.YEAR);
		int currentHour = calendar.get(java.util.Calendar.HOUR);
		int currentMinute = calendar.get(java.util.Calendar.MINUTE);
		
		calendar.clear();
		calendar.setFirstDayOfWeek(java.util.Calendar.SATURDAY);
		calendar.set(java.util.Calendar.WEEK_OF_YEAR, currentWeek);
		calendar.set(java.util.Calendar.YEAR, currentYear);
		calendar.set(java.util.Calendar.HOUR, currentHour);
		calendar.set(java.util.Calendar.MINUTE, currentMinute);
		
		Date weekFirstDate = calendar.getTime();
		
		calendar.add(java.util.Calendar.DAY_OF_MONTH, 6);
		
		Date weekLastDate = calendar.getTime();
		
		routine.setStartDate(weekFirstDate);
		routine.setEndDate(weekLastDate);
		
	}
	
	private GridLayout					classroutineLayout;
	private ComboBox					campusComboBox;
	private ComboBox					courseComboBox;
	private ComboBox					batchComboBox;
	private ComboBox					subjectComboBox;
	private Table						routineTable;
	
	private Button						saveButton;
	private Button						clearButton;
	private Button						deleteButton;
	
	private long						searchOrganizationId;
	
	private final static String ROUTINE_DAY		 		= "dayName";
	private final static String ROUTINE_START		 	= "start";
	private final static String ROUTINE_END	 			= "end";
	private final static String ROUTINE_ROOM	 		= "room";
	private final static String ROUTINE_BATCH	 		= "batch";
	
	private BeanItemContainer<RoutineDto>	routineBeanContainer;
	
	private long						courseId;
	private long						batchId;
	private long 						selectedSubjectId;
	private BatchDto					selectedBatch;
	
	private List<ClassRoutineEventBatch> classRoutineEventBatchList;
	
	private final static String DATE_FORMAT 			= "hh.mm";
	
	private GridLayout classroutineLayout(){
		classroutineLayout = new GridLayout(4,7);
		classroutineLayout.setWidth("100%");
		classroutineLayout.setSpacing(true);
		
		Label	campuslabel		 			= new Label("Campus :");
		Label	courselabel					= new Label("Course :");	
		Label	batchlabel					= new Label("Batch :");
		Label	subjectlaLabel				= new Label("Subject :");
		
		
		campusComboBox 	= new ComboBox();
		courseComboBox 	= new ComboBox();
		batchComboBox	= new ComboBox();	
		subjectComboBox	= new ComboBox();	
		
		campusComboBox.setNullSelectionAllowed(false);
		courseComboBox.setNullSelectionAllowed(false);
		batchComboBox.setNullSelectionAllowed(false);
		subjectComboBox.setNullSelectionAllowed(false);
		
		campusComboBox.setWidth("100%");
		courseComboBox.setWidth("100%");
		batchComboBox.setWidth("100%");
		subjectComboBox.setWidth("100%");
		
		campusComboBox.setImmediate(true);
		courseComboBox.setImmediate(true);
		batchComboBox.setImmediate(true);
		subjectComboBox.setImmediate(true);
		
		for (OrganizationDto orgDto : organizationDtoList) {
			campusComboBox.addItem(orgDto);
		}
		campusComboBox.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				courseComboBox.removeAllItems();
				OrganizationDto serchorg = (OrganizationDto)campusComboBox.getValue();
				if(serchorg!=null){
					searchOrganizationId = serchorg.getOrganizationId();
					getCourseDtoByCompanyId();
					getRoomDtoByCampusId();
					for (CourseDto course : courseDtoList) {
						courseComboBox.addItem(course);
					}
				}				
			}
		});	
		
		courseComboBox.addListener(new Listener() {
			public void componentEvent(Event event) {
				batchComboBox.removeAllItems();
				CourseDto course = (CourseDto) courseComboBox.getValue();
				if(course!=null){
					courseId =course.getCourseId();
					getBatchDtoByCourseId(courseId);
					for(BatchDto batchDto:batchDtoList){
						batchComboBox.addItem(batchDto);
					}
				}
			}
		});
		
		batchComboBox.addListener(new Listener() {
			public void componentEvent(Event event) {
				subjectComboBox.removeAllItems();
				BatchDto batch = (BatchDto) batchComboBox.getValue();
				if (batch!=null) {
					selectedBatch= batch;
					batchId = batch.getBatchId();
					getBatchClassRoutineEvent(batchId);
					getBatchAssignedSubject(batchId, courseId, searchOrganizationId);
					for (BatchSubjectDto batchSubjectDto : batchSubjectDtoList) {
						subjectComboBox.addItem(batchSubjectDto);
					}
				}
			}

			
		});
		
		subjectComboBox.addListener(new Listener() {
			public void componentEvent(Event event) {
				
				BatchSubjectDto batchSubjectDto = (BatchSubjectDto) subjectComboBox.getValue();
				if(batchSubjectDto!=null){
					selectedSubjectId = batchSubjectDto.getSubjectId();
					getBatchClassRoutineEvent(batchId);
					loadRoutineDays();
				}			
			}
		});
		
		classroutineLayout.addComponent(campuslabel,0,0,0,0);				classroutineLayout.addComponent(campusComboBox,1,0,1,0);
		classroutineLayout.addComponent(batchlabel,0,1,0,1);				classroutineLayout.addComponent(batchComboBox,1,1,1,1);
		
		classroutineLayout.addComponent(courselabel,2,0,2,0);				classroutineLayout.addComponent(courseComboBox,3,0,3,0);
		classroutineLayout.addComponent(subjectlaLabel,2,1,2,1);			classroutineLayout.addComponent(subjectComboBox,3,1,3,1);
		
		
		routineBeanContainer 	= new BeanItemContainer<RoutineDto>(RoutineDto.class);
		
		
		routineTable 			= new Table("",routineBeanContainer);
		routineTable.setImmediate(true);
		routineTable.setEditable(true);
		routineTable.setSelectable(true);
		routineTable.setWidth("100%");
		//routineTable.setHeight("100%");
		
		routineTable.setColumnHeader(ROUTINE_DAY, "Day");
		routineTable.setColumnHeader(ROUTINE_START, "Start");	
		routineTable.setColumnHeader(ROUTINE_END, "End");
		routineTable.setColumnHeader(ROUTINE_ROOM, "Room");	
		routineTable.setColumnHeader(ROUTINE_BATCH, "Batch");
		
		routineTable.setVisibleColumns(new String[]{ROUTINE_DAY,ROUTINE_START,ROUTINE_END,ROUTINE_ROOM,ROUTINE_BATCH});	
		
		routineTable.setColumnExpandRatio(ROUTINE_DAY,1);
		routineTable.setColumnExpandRatio(ROUTINE_START, 1);
		routineTable.setColumnExpandRatio(ROUTINE_END, 1);
		routineTable.setColumnExpandRatio(ROUTINE_ROOM, 1);
		routineTable.setColumnExpandRatio(ROUTINE_BATCH, 1);
		
		routineTable.setPageLength(6);
		routineTable.setNullSelectionAllowed(false);
//		routineTable.
		
		routineTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				if (propertyId.equals(ROUTINE_ROOM)) {
					ComboBox roomComboBox = new ComboBox();
					roomComboBox.setNullSelectionAllowed(true);
					roomComboBox.setWidth("100%");
					roomComboBox.setImmediate(true);
					roomComboBox.setReadOnly(false);
					
					for (RoomDto roomDto : roomDtoList) {
						roomComboBox.addItem(roomDto);
						if(room!=null){
							roomComboBox.select(room);
						}
					}
					
					return roomComboBox;
				}
				
				if (propertyId.equals(ROUTINE_BATCH)) {
					TokenField batchToken = new TokenField(){
						protected void onTokenInput(Object tokenId) {
	                        Set<Object> tokenset = (Set<Object>) getValue();
	                        	                        
	                        if (tokenset != null && tokenset.contains(tokenId)) {
	                            // duplicate
	                            window.showNotification(
	                                    getTokenCaption(tokenId)
	                                            + " is already added");
	                        } else {
	                            if (!cb.containsId(tokenId)) {
	                                // don't add directly,
	                                // show custom "add to address book" dialog
	                                window.showNotification("Don't exists");
	                            } else {
	                                // it's in the 'address book', just add
	                            	if(selectedBatch.getBatchName().equals(tokenId.toString())){
	                            		window.showNotification(
	    	                                    getTokenCaption(tokenId)
	    	                                            + " is already added");
	                            	}else{
	                            		addToken(tokenId);
	                            	}	                                
	                            }
	                        }
	                    }
					};
					
					batchToken.setWidth("100%");
					batchToken.setImmediate(true);
					batchToken.setReadOnly(false);
					batchToken.setContainerDataSource(batchContainer);
					batchToken.setRememberNewTokens(false);
					batchToken.setFilteringMode(ComboBox.FILTERINGMODE_CONTAINS);
					
					return batchToken;
				}
				
				if (propertyId.equals(ROUTINE_DAY)) {
					TextField roomComboBox = new TextField();
					roomComboBox.setWidth("100%");
					roomComboBox.setImmediate(true);
					roomComboBox.setReadOnly(true);
					
					return roomComboBox;
				}
				if (propertyId.equals(ROUTINE_START)) {
					CustomTimeField startTimefield = new CustomTimeField();
					startTimefield.setLocale(Locale.ROOT);
					startTimefield.setWidth("100%");
					startTimefield.setTabIndex(3);
					
					return startTimefield;
				}
				
				if (propertyId.equals(ROUTINE_END)) {
					CustomTimeField endTimefield = new CustomTimeField();
					endTimefield.setLocale(Locale.ROOT);
					endTimefield.setWidth("100%");
					endTimefield.setTabIndex(3);
					
					return endTimefield;
				}
				
			return super.createField(container, itemId, propertyId, uiContext);
			}
		});
				
		saveButton 				= new Button("Save");
		clearButton 			= new Button("Clear");
		deleteButton			= new Button("Delete");
				
		saveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				
				if(selectedSubjectId!=0){
					int count = 0;
					
					for (int i = 0; i < routineBeanContainer.size(); i++) {
						
						ClassRoutineEvent classRoutineEvent = new ClassRoutineEventImpl();
						RoutineDto routineDto = routineBeanContainer.getIdByIndex(i);
						
						Set<BatchDto> additionalBatches = routineDto.getBatch();
						//window.showNotification("additional batch size" + additionalBatches.size());
						
						List<ClassRoutineEventBatch> classRoutineEventBatchs = new ArrayList<ClassRoutineEventBatch>();;
						ClassRoutineEventBatch mainroutineEventBatch = new ClassRoutineEventBatchImpl();
						mainroutineEventBatch.setBatchId(batchId);
						mainroutineEventBatch.setCompanyId(companyId);
						mainroutineEventBatch.setOrganizationId(searchOrganizationId);
						classRoutineEventBatchs.add(mainroutineEventBatch);
						if(additionalBatches!=null){
							
							for (BatchDto batchDto : additionalBatches) {
								ClassRoutineEventBatch routineEventBatch = new ClassRoutineEventBatchImpl();
								routineEventBatch.setBatchId(batchDto.getBatchId());
								routineEventBatch.setCompanyId(companyId);
								routineEventBatch.setOrganizationId(searchOrganizationId);
								classRoutineEventBatchs.add(routineEventBatch);
							}
						}
						
						if(routineDto.getStart()!=null && routineDto.getEnd()!=null && routineDto.getRoom()!=null){
							
							classRoutineEvent.setCompanyId(companyId);
							classRoutineEvent.setOrganizationId(searchOrganizationId);
							classRoutineEvent.setRoomId(routineDto.getRoom().getId());
							classRoutineEvent.setDay(routineDto.getDay());
							classRoutineEvent.setStartTime(routineDto.getStart());
							classRoutineEvent.setEndTime(routineDto.getEnd());
							classRoutineEvent.setSubjectId(selectedSubjectId);	
							
							if(cRoutineDetailList.size()==0){
								try {
									if(ClassRoutineEventLocalServiceUtil.saveClassEventWithBatches(classRoutineEvent, classRoutineEventBatchs)){
										count ++;
									}
								} catch (SystemException e) {
									e.printStackTrace();
								}
							}else{
								try {
									if(routineDto.getId()!=0){
										ClassRoutineEventLocalServiceUtil.deleteEvent(routineDto.getId());
										if(ClassRoutineEventLocalServiceUtil.saveClassEventWithBatches(classRoutineEvent, classRoutineEventBatchs)){
											count ++;
										}
									}else{
										if(ClassRoutineEventLocalServiceUtil.saveClassEventWithBatches(classRoutineEvent, classRoutineEventBatchs)){
											count ++;
										}
									}									
									
								} catch (NoSuchClassRoutineEventException e) {
									e.printStackTrace();
								} catch (SystemException e) {
									e.printStackTrace();
								}
							}							
						}							
					}
					window.showNotification(count+ " Days Event have saved");
					CourseDto course = (CourseDto) routineCourseComboBox.getValue();
					if(course!=null){
						courseId =course.getCourseId();
						getBatchDtoByCourseId(courseId);
						for(BatchDto batchDto:batchDtoList){
							routineBatchCombobox.addItem(batchDto);
						}
					}
					loadBatch();
					loadTeacher();
					loadRoom();
					loadsideBar();
					loadRoutineComboBox();
				}else{
					window.showNotification("Please Select Subject");
				}
			}
				
		});
		
		deleteButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				RoutineDto routineDto = (RoutineDto) routineTable.getValue();
				if (routineDto!=null) {
					try {
						ClassRoutineEventLocalServiceUtil.deleteClassRoutineEvent(routineDto.getId());
						loadRoutineDays();
						window.showNotification("Event Deleted Successfully");
					} catch (PortalException e) {
						e.printStackTrace();
					} catch (SystemException e) {
						e.printStackTrace();
					}
				} else{
					window.showNotification("Select an Event to delete", Window.Notification.TYPE_ERROR_MESSAGE);
				}
				
			}
		});
		
		clearButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				classRoutineEventBatchList = null;
				campusComboBox.select(null);
				courseComboBox.select(null);
				batchComboBox.select(null);
				subjectComboBox.select(null);
				routineBeanContainer.removeAllItems();
				routineTable.removeAllItems();
			}
		});
		
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setSpacing(true);
		
		buttonLayout.addComponent(saveButton);
		buttonLayout.addComponent(clearButton);
		buttonLayout.addComponent(deleteButton);
		
		classroutineLayout.addComponent(routineTable,0,2,3,5);
		classroutineLayout.addComponent(buttonLayout,0,6,3,6);
		classroutineLayout.setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);
		
		return classroutineLayout;
		
	}
	
	private void getBatchClassRoutineEvent(long batchId) {
		try {
			classRoutineEventBatchList = ClassRoutineEventBatchLocalServiceUtil.findEventBatchByBatchId(batchId);
			
			/*if(classRoutineEventBatchList!=null){
				window.showNotification("classRoutineBatch size" + classRoutineEventBatchList.size());
			}*/
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	ClassRoutineEvent classRoutineForDetail;
	private List<ClassRoutineEvent> 	cRoutineDetailList;
	RoomDto room;
	private void loadRoutineDays() {
		
		if(classRoutineEventBatchList!=null){
			cRoutineDetailList = new ArrayList<ClassRoutineEvent>();
			for (ClassRoutineEventBatch cRoutineEventBatch : classRoutineEventBatchList) {
				try {
					classRoutineForDetail = ClassRoutineEventLocalServiceUtil.findEventsBySubjectId(selectedSubjectId, cRoutineEventBatch.getClassRoutineEventId());
					if(classRoutineForDetail!=null){
						cRoutineDetailList.add(classRoutineForDetail);
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		}
		routineBeanContainer.removeAllItems();
		routineTable.refreshRowCache();
		
		RoutineDto saturdayroutineDto = new RoutineDto();
		saturdayroutineDto.setDay(Calendar.SATURDAY);
		saturdayroutineDto.setDayName("SATURDAY");
		
		
		RoutineDto sundayroutineDto = new RoutineDto();
		sundayroutineDto.setDay(Calendar.SUNDAY);
		sundayroutineDto.setDayName("SUNDAY");
		
		
		RoutineDto mondayroutineDto = new RoutineDto();
		mondayroutineDto.setDay(Calendar.MONDAY);
		mondayroutineDto.setDayName("MONDAY");
		
		
		RoutineDto tuesdayroutineDto = new RoutineDto();
		tuesdayroutineDto.setDay(Calendar.TUESDAY);
		tuesdayroutineDto.setDayName("TUESDAY");
		
		
		RoutineDto wednesdayroutineDto = new RoutineDto();
		wednesdayroutineDto.setDay(Calendar.WEDNESDAY);
		wednesdayroutineDto.setDayName("WEDNESDAY");
		
		
		RoutineDto thrusdayroutineDto = new RoutineDto();
		thrusdayroutineDto.setDay(Calendar.THURSDAY);
		thrusdayroutineDto.setDayName("THURSDAY");
		
		
		RoutineDto fridayroutineDto = new RoutineDto();
		fridayroutineDto.setDay(Calendar.FRIDAY);
		fridayroutineDto.setDayName("FRIDAY");
		
		if(cRoutineDetailList!=null){
			for (ClassRoutineEvent cRoutineEvent : cRoutineDetailList) {
				Date startTime = cRoutineEvent.getStartTime();
				Date endTime = cRoutineEvent.getEndTime();
				List<ClassRoutineEventBatch>		additionalBatches = new ArrayList<ClassRoutineEventBatch>();
				try {
					additionalBatches = ClassRoutineEventBatchLocalServiceUtil.findEventBatchByEventId(cRoutineEvent.getClassRoutineEventId());
					//window.showNotification("additional batehs" + additionalBatches.size());
				} catch (SystemException e) {
					e.printStackTrace();
				}
				Set<BatchDto> 				additionalBathcDtoSet = new HashSet<BatchDto>();
				for (ClassRoutineEventBatch classRoutineEventBatch : additionalBatches) {
					for (BatchDto batchDto : batchDtoList) {
						if(classRoutineEventBatch.getBatchId()==batchDto.getBatchId() && classRoutineEventBatch.getBatchId()!=batchId ){
							additionalBathcDtoSet.add(batchDto);
						}
					}
				}
				room  = new RoomDto();
				for (RoomDto roomDto : roomDtoList) {
					if(roomDto.getId()==cRoutineEvent.getRoomId()){
						room= roomDto;
					}
				}
				long id = cRoutineEvent.getClassRoutineEventId();
				switch (cRoutineEvent.getDay()) {
				case Calendar.SATURDAY:
					saturdayroutineDto.setId(id);
					saturdayroutineDto.setStart(startTime);
					saturdayroutineDto.setEnd(endTime);
					saturdayroutineDto.setBatch(additionalBathcDtoSet);
					saturdayroutineDto.setRoom(room);
					break;
				case Calendar.SUNDAY:
					sundayroutineDto.setId(id);
					sundayroutineDto.setStart(startTime);
					sundayroutineDto.setEnd(endTime);
					sundayroutineDto.setBatch(additionalBathcDtoSet);
					sundayroutineDto.setRoom(room);
					break;
				case Calendar.MONDAY:
					mondayroutineDto.setId(id);
					mondayroutineDto.setStart(startTime);
					mondayroutineDto.setEnd(endTime);
					mondayroutineDto.setBatch(additionalBathcDtoSet);
					mondayroutineDto.setRoom(room);
					break;
				case Calendar.TUESDAY:
					tuesdayroutineDto.setId(id);
					tuesdayroutineDto.setStart(startTime);
					tuesdayroutineDto.setEnd(endTime);
					tuesdayroutineDto.setBatch(additionalBathcDtoSet);
					tuesdayroutineDto.setRoom(room);
					break;
				case Calendar.WEDNESDAY:
					wednesdayroutineDto.setId(id);
					wednesdayroutineDto.setStart(startTime);
					wednesdayroutineDto.setEnd(endTime);
					wednesdayroutineDto.setBatch(additionalBathcDtoSet);
					wednesdayroutineDto.setRoom(room);
					break;
				case Calendar.THURSDAY:
					thrusdayroutineDto.setId(id);
					thrusdayroutineDto.setStart(startTime);
					thrusdayroutineDto.setEnd(endTime);
					thrusdayroutineDto.setBatch(additionalBathcDtoSet);
					thrusdayroutineDto.setRoom(room);
					break;
				case Calendar.FRIDAY:
					fridayroutineDto.setId(id);
					fridayroutineDto.setStart(startTime);
					fridayroutineDto.setEnd(endTime);
					fridayroutineDto.setBatch(additionalBathcDtoSet);
					fridayroutineDto.setRoom(room);
					break;
				}
			}
		}
		
		routineBeanContainer.addBean(saturdayroutineDto);
		routineBeanContainer.addBean(sundayroutineDto);
		routineBeanContainer.addBean(mondayroutineDto);
		routineBeanContainer.addBean(tuesdayroutineDto);
		routineBeanContainer.addBean(wednesdayroutineDto);
		routineBeanContainer.addBean(thrusdayroutineDto);
		routineBeanContainer.addBean(fridayroutineDto);
	}
	

	private List<OrganizationDto>			organizationDtoList;
	
	private void getOrganizationsByCompany(){
		
		List<Organization> organization = null;
		try {
			organization = OrganizationLocalServiceUtil.getOrganizations(companyId, OrganizationConstants.ANY_PARENT_ORGANIZATION_ID);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		organizationDtoList = new ArrayList<OrganizationDto>();
		for(Organization org : organization){
			OrganizationDto orgDto = new OrganizationDto();
			orgDto.setOrganizationId(org.getOrganizationId());
			orgDto.setOrganizationName(org.getName());				
			organizationDtoList.add(orgDto);
		}
	}
	
	private List<CourseDto> 		courseDtoList;
	
	private void getCourseDtoByCompanyId(){
		try {
			List<CourseOrganization> courseOrganizations = CourseOrganizationLocalServiceUtil.findByOrganization(searchOrganizationId);
			//System.out.println("couse size by organization"+courseOrganizations.size());
			courseDtoList = new ArrayList<CourseDto>();
			for(CourseOrganization courseOrg : courseOrganizations){
				CourseDto courseDto = new CourseDto();
				long courseid = courseOrg.getCourseId();
				Course course = CourseLocalServiceUtil.fetchCourse(courseid);
				courseDto.setCourseId(courseid);
				courseDto.setCourseName(course.getCourseName());				
				courseDtoList.add(courseDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private List<BatchDto> 								batchDtoList;
	private BeanItemContainer<BatchDto> 				batchContainer;
	
	private void getBatchDtoByCourseId(long courseId){
		try {
			List<Batch> batchList = BatchLocalServiceUtil.findBatchesByOrgCourseId(searchOrganizationId, courseId);			
//			courseSubjectList = CourseSubjectLocalServiceUtil.findByCourseId(courseId);
			
			batchDtoList = new ArrayList<BatchDto>();
			batchContainer = new BeanItemContainer<BatchDto>(BatchDto.class);
			for(Batch batch : batchList){
				BatchDto batchDto = new BatchDto();
				batchDto.setBatchId(batch.getBatchId());
				batchDto.setBatchName(batch.getBatchName());
				batchDtoList.add(batchDto);
				batchContainer.addBean(batchDto);
			}
						
		} catch (SystemException e) {
			e.printStackTrace();
		}		
	}
	
	private List<BatchSubjectDto> 						batchSubjectDtoList;

	private void getBatchAssignedSubject(long batchId, long courseId, long organizationId){
		
		try {
			List<BatchSubject> batchSubjectList = BatchSubjectLocalServiceUtil.getBatchSubjectByOrgCourseBatch(organizationId, courseId, batchId);
			batchSubjectDtoList = new ArrayList<BatchSubjectDto>();
			if (batchSubjectList!=null) {
				for(BatchSubject batchSubject:batchSubjectList){
					BatchSubjectDto batchSubjectDto = new BatchSubjectDto();
					long subjectId = batchSubject.getSubjectId();
					batchSubjectDto.setSubjectId(subjectId);
					batchSubjectDto.setSubjcetName(getSubjectNameById(subjectId));
					
					batchSubjectDtoList.add(batchSubjectDto);
				}
			}		
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	List<RoomDto> 										roomDtoList;
	public void getRoomDtoByCampusId(){
		try {
			List<Room> roomList = RoomLocalServiceUtil.findByOrganizationId(searchOrganizationId);
			roomDtoList = new ArrayList<RoomDto>();
			if(roomList!=null){
				for (Room room : roomList) {
					RoomDto roomDto = new RoomDto();
					roomDto.setId(room.getRoomId());
					roomDto.setLabel(room.getLabel());
					roomDtoList.add(roomDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	}
	
	private String getSubjectNameById(long subjectId){
		String name = "";
		try {
			Subject subject = SubjectLocalServiceUtil.getSubject(subjectId);
			name = subject.getSubjectName();
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return name;
	}
	
	
	private void populateEvent(long eventId){
		try {
			ClassRoutineEvent classRoutineEvent = ClassRoutineEventLocalServiceUtil.fetchClassRoutineEvent(eventId);
			campusComboBox.setValue(getOrganization(classRoutineEvent.getOrganizationId()));
			
			List<ClassRoutineEventBatch> eventBatchList = ClassRoutineEventBatchLocalServiceUtil.findEventBatchByEventId(eventId);
			ClassRoutineEventBatch eventBatch = eventBatchList.get(0);

			List<BatchSubject> batchSubjects = BatchSubjectLocalServiceUtil.findSubjectByBatch(eventBatch.getBatchId());
			for (BatchSubject batchSubject : batchSubjects) {
				if (classRoutineEvent.getSubjectId()==batchSubject.getSubjectId()) {
					courseComboBox.setValue(getCourse(batchSubject.getCourseId()));
				}
			}
			
			batchComboBox.setValue(getBatch(eventBatch.getBatchId()));
			subjectComboBox.setValue(getSubject(classRoutineEvent.getSubjectId()));
			loadRoutineDays();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private OrganizationDto getOrganization(long organizationId){
		if (organizationDtoList!=null) {
			for (OrganizationDto org : organizationDtoList) {
				if (org.getOrganizationId()==organizationId) {
					return org;
				}
			}
		}
		return null;
	}
	
	private CourseDto getCourse(long courseId){
		if (courseDtoList!=null) {
			for (CourseDto course : courseDtoList) {
				if (course.getCourseId()==courseId) {
					return course;
				}
			}
		}
		return null;
	}
	
	private BatchDto getBatch(long batchId){
		if (batchDtoList!=null) {
			for (BatchDto batchDto : batchDtoList) {
				if (batchDto.getBatchId()==batchId) {
					return batchDto;
				}
			}
		}
		return null;
	}
	
	private BatchSubjectDto getSubject(long subjectId){
		if (batchSubjectDtoList!=null) {
			for (BatchSubjectDto subject : batchSubjectDtoList) {
				if (subject.getSubjectId()==subjectId) {
					return subject;
				}
			}
		}
		return null;
	}

	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

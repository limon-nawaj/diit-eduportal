package info.diit.portal.lesson;

import info.diit.portal.dto.BatchDto;
import info.diit.portal.dto.CourseDto;
import info.diit.portal.dto.LessonAnalyticDto;
import info.diit.portal.dto.OrganizationDto;
import info.diit.portal.dto.SubjectDto;
import info.diit.portal.model.Attendance;
import info.diit.portal.model.AttendanceTopic;
import info.diit.portal.model.Batch;
import info.diit.portal.model.Course;
import info.diit.portal.model.CourseOrganization;
import info.diit.portal.model.LessonTopic;
import info.diit.portal.model.Subject;
import info.diit.portal.model.SubjectLesson;
import info.diit.portal.service.AttendanceLocalServiceUtil;
import info.diit.portal.service.AttendanceTopicLocalServiceUtil;
import info.diit.portal.service.BatchLocalServiceUtil;
import info.diit.portal.service.CourseLocalServiceUtil;
import info.diit.portal.service.CourseOrganizationLocalServiceUtil;
import info.diit.portal.service.LessonTopicLocalServiceUtil;
import info.diit.portal.service.SubjectLessonLocalServiceUtil;
import info.diit.portal.service.SubjectLocalServiceUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;

public class LessonAnalyticApp extends Application implements PortletRequestListener{

	private Window window;
	private ThemeDisplay themeDisplay;
	private List<Organization> userOrganizationList;
	
	private final static String COLUMN_SUBJECT = "subject";
	private final static String COLUMN_TOTAL_TOPIC = "totalTopic";
	private final static String COLUMN_COMPLETED_TOPIC = "completedTopic";
	private final static String COLUMN_COMPLETED_PER = "completedPercentage";
	private final static String COLUMN_REMAINING_TOPIC = "remainingTopic";
	private final static String COLUMN_REMAINING_PER = "remainingPercentage";
	
	private List<OrganizationDto> campusList;
	private List<BatchDto> batchList;
	private List<CourseDto> courseList;
	
	DecimalFormat twoDecimalFormat = new DecimalFormat("0.00");
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        
        try {
			userOrganizationList = themeDisplay.getUser().getOrganizations();
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}
        loadCampus();
        window.addComponent(mainLayout());
        
        if (userOrganizationList.size()==1) {
        	loadCourse();
        	courseComboBox();
		}
    }
    ComboBox campusComboBox;
	ComboBox courseComboBox;
	ComboBox batchComboBox;
	
	BeanItemContainer<LessonAnalyticDto> analyticContainer;
	Table analyticTable;
	
	private GridLayout mainLayout(){
		GridLayout mainLayout = new GridLayout(8, 8);
		mainLayout.setWidth("100%");
		
		campusComboBox = new ComboBox("Campus");
		courseComboBox = new ComboBox("Course");
		batchComboBox  = new ComboBox("Batch");
		
		campusComboBox.setImmediate(true);
		courseComboBox.setImmediate(true);
		batchComboBox.setImmediate(true);
		
		campusComboBox.setWidth("100%");
		courseComboBox.setWidth("100%");
		batchComboBox.setWidth("100%");
		
		if (userOrganizationList.size()>1) {
			if (campusList!=null) {
				for (OrganizationDto campus : campusList) {
					campusComboBox.addItem(campus);
				}
			}
		}
		
		campusComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				courseComboBox.removeAllItems();
				loadCourse();
	        	courseComboBox();
			}
		});
		
		courseComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				batchComboBox.removeAllItems();
				loadBatch();
				batchComboBox();
			}
		});
		
		batchComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				loadTopic();
			}
		});
		
		analyticContainer = new BeanItemContainer<LessonAnalyticDto>(LessonAnalyticDto.class);
		analyticTable = new Table("Lesson Analytics", analyticContainer){
			@Override
    		protected String formatPropertyValue(Object rowId, Object colId,
    				Property property) {
    			if (property.getType()==Double.class) {
					return twoDecimalFormat.format(property.getValue());
				}
    			return super.formatPropertyValue(rowId, colId, property);
    		}
		};
		analyticTable.setWidth("100%");
		analyticTable.setImmediate(true);
		
		analyticTable.setColumnHeader(COLUMN_SUBJECT, "Subject");
		analyticTable.setColumnHeader(COLUMN_TOTAL_TOPIC, "Total Topic");
		analyticTable.setColumnHeader(COLUMN_COMPLETED_TOPIC, "Completed Topic");
		analyticTable.setColumnHeader(COLUMN_COMPLETED_PER, "% Completed");
		analyticTable.setColumnHeader(COLUMN_REMAINING_TOPIC, "Remaining Topic");
		analyticTable.setColumnHeader(COLUMN_REMAINING_PER, "Remaining");
		
		analyticTable.setVisibleColumns(new String[]{COLUMN_SUBJECT, COLUMN_TOTAL_TOPIC, COLUMN_COMPLETED_TOPIC, COLUMN_COMPLETED_PER, COLUMN_REMAINING_TOPIC, COLUMN_REMAINING_PER});
		
		if (userOrganizationList.size()>1) {
			mainLayout.addComponent(campusComboBox, 0, 0, 1, 0);
			mainLayout.addComponent(courseComboBox, 3, 0, 4, 0);
			mainLayout.addComponent(batchComboBox, 6, 0, 7, 0);
			mainLayout.addComponent(analyticTable, 0, 1, 7, 1);
		}else if (userOrganizationList.size()==1) {
			mainLayout.addComponent(courseComboBox, 0, 0, 1, 0);
			mainLayout.addComponent(batchComboBox, 3, 0, 4, 0);
			mainLayout.addComponent(analyticTable, 0, 1, 7, 1);
		}
		
		return mainLayout;
	}
	
	private void batchComboBox(){
		if (batchList!=null) {
			for (BatchDto batchDto : batchList) {
				batchComboBox.addItem(batchDto);
			}
		}
	}
	
	private void courseComboBox(){
		if (courseList!=null) {
			for (CourseDto courseDto : courseList) {
				courseComboBox.addItem(courseDto);
			}
		}
	}
	
	private void loadCampus(){
		campusList = new ArrayList<OrganizationDto>();
		for (Organization organization : userOrganizationList) {
			OrganizationDto campus = new OrganizationDto();
			campus.setOrganizationId(organization.getOrganizationId());
			campus.setOrganizationName(organization.getName());
			campusList.add(campus);
		}
	}
	
	private void loadCourse(){
		courseList = new ArrayList<CourseDto>();
		try {
			List<CourseOrganization> courseOrganizations = null;
			if (userOrganizationList.size()==1) {
				Organization org = userOrganizationList.get(0);
				courseOrganizations = CourseOrganizationLocalServiceUtil.findByOrganization(org.getOrganizationId());
				
			}else if (userOrganizationList.size()>1) {
				OrganizationDto campusDto = (OrganizationDto) campusComboBox.getValue();
				if (campusDto!=null) {
					courseOrganizations = CourseOrganizationLocalServiceUtil.findByOrganization(campusDto.getOrganizationId());
				}
			}
			
			if (courseOrganizations!=null) {
				for (CourseOrganization courseOrganization : courseOrganizations) {
					Course course = CourseLocalServiceUtil.fetchCourse(courseOrganization.getCourseId());
					CourseDto courseDto = new CourseDto();
					courseDto.setCourseId(course.getCourseId());
					courseDto.setCourseName(course.getCourseName());
					courseList.add(courseDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	}
	
	private void loadBatch(){
		batchList = new ArrayList<BatchDto>();
		try {
			CourseDto courseDto = (CourseDto) courseComboBox.getValue();
			List<Batch> batchs = null;
			if (userOrganizationList.size()==1) {
				if (courseDto!=null) {
					batchs = BatchLocalServiceUtil.findBatchesByOrgCourseId(themeDisplay.getLayout().getGroup().getOrganizationId(), courseDto.getCourseId());
				}
			} else if(userOrganizationList.size()>1){
				OrganizationDto campusDto = (OrganizationDto) campusComboBox.getValue();
				if (courseDto!=null && campusDto!=null) {
					batchs = BatchLocalServiceUtil.findBatchesByOrgCourseId(campusDto.getOrganizationId(), courseDto.getCourseId());
				}
			}
			
			if (batchs!=null) {
				for (Batch batch : batchs) {
					BatchDto batchDto = new BatchDto();
					batchDto.setBatchId(batch.getBatchId());
					batchDto.setBatchName(batch.getBatchName());
					batchList.add(batchDto);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
		
	}
	
	private void loadTopic(){
		analyticContainer.removeAllItems();
		BatchDto batchDto = (BatchDto) batchComboBox.getValue();
		try {
			if (batchDto!=null) {
				List<Attendance> attendences = AttendanceLocalServiceUtil.findByBatch(batchDto.getBatchId());
//				List<Attendance> attendenceList = new ArrayList<Attendance>();
//				if (attendences!=null) {
//					for (Attendance attendence : attendences) {
//						attendenceList.add(attendence);
//					}
//				}
				
				if (attendences!=null) {
					Set<Long> subjectIds = new HashSet<Long>();
					for (Attendance attendence : attendences) {
						subjectIds.add(attendence.getSubject());
					}
					
					
					
					if (subjectIds!=null) {
						for (Long id : subjectIds) {
							LessonAnalyticDto analyticDto = new LessonAnalyticDto(); 
							Subject subject = SubjectLocalServiceUtil.fetchSubject(id);
							SubjectDto subjectDto = new SubjectDto();
							subjectDto.setSubjectId(subject.getSubjectId());
							subjectDto.setSubjcetName(subject.getSubjectName());
							
							analyticDto.setSubject(subjectDto);
							
							double completed = 0;
							for (Attendance attendence : attendences) {
								if (attendence.getSubject()==id) {
									DynamicQuery completedQuery = DynamicQueryFactoryUtil.forClass(AttendanceTopic.class);
									completedQuery.add(PropertyFactoryUtil.forName("attendanceTopicId").eq(attendence.getAttendanceId())).setProjection(ProjectionFactoryUtil.rowCount());
									List completedList = AttendanceTopicLocalServiceUtil.dynamicQuery(completedQuery);
									if (completedList!=null) {
										completed = ((Long)completedList.get(0)).intValue();
									}
								}
							}
							
							List<SubjectLesson> subjectLessons = SubjectLessonLocalServiceUtil.findByBatchSubject(batchDto.getBatchId(), id);
							Set<Long> topics = new HashSet<Long>();
							if (subjectLessons!=null) {
								for (SubjectLesson subjectLesson : subjectLessons) {
									List<LessonTopic> lessonTopicList = LessonTopicLocalServiceUtil.findByLessonPlan(subjectLesson.getLessonPlanId());
									if (lessonTopicList!=null) {
										for (LessonTopic lessonTopic : lessonTopicList) {
											topics.add(lessonTopic.getTopic());
										}
									}
									
								}
							}
							
							double completedPercentage = 0;
							double remainingPercentage = 0;
							
							if (topics!=null) {
								analyticDto.setTotalTopic(topics.size());
							}
							analyticDto.setCompletedTopic((int) completed);
							analyticDto.setRemainingTopic((int) (topics.size()-completed));
							
							if (topics.size()>0) {
								completedPercentage = completed*100/topics.size();
								remainingPercentage = (topics.size()-completed)*100/topics.size();
								analyticDto.setCompletedPercentage(completedPercentage);
								analyticDto.setRemainingPercentage(remainingPercentage);
							} else{
								analyticDto.setCompletedPercentage(0);
								analyticDto.setRemainingPercentage(0);
							}
							
							analyticContainer.addBean(analyticDto);
						}
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

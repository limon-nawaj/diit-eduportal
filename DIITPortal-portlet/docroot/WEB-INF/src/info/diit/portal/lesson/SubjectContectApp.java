package info.diit.portal.lesson;

import info.diit.portal.constant.ContentType;
import info.diit.portal.dto.ContentDto;
import info.diit.portal.dto.CourseDto;
import info.diit.portal.dto.SubjectDto;
import info.diit.portal.dto.TopicDto;
import info.diit.portal.model.Chapter;
import info.diit.portal.model.Course;
import info.diit.portal.model.CourseSubject;
import info.diit.portal.model.Subject;
import info.diit.portal.model.Topic;
import info.diit.portal.model.impl.ChapterImpl;
import info.diit.portal.model.impl.TopicImpl;
import info.diit.portal.service.ChapterLocalServiceUtil;
import info.diit.portal.service.CourseLocalServiceUtil;
import info.diit.portal.service.CourseSubjectLocalServiceUtil;
import info.diit.portal.service.SubjectLocalServiceUtil;
import info.diit.portal.service.TopicLocalServiceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import net.atontech.vaadin.ui.numericfield.NumericField;
import net.atontech.vaadin.ui.numericfield.widgetset.shared.NumericFieldType;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ContainerHierarchicalWrapper;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.SourceIs;
import com.vaadin.terminal.gwt.client.ui.dd.VerticalDropLocation;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.AbstractSelect.AbstractSelectTargetDetails;
import com.vaadin.ui.AbstractSelect.AcceptItem;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

public class SubjectContectApp extends Application implements PortletRequestListener{

	private Window window;
	private ThemeDisplay themeDisplay;

	private Chapter chapter;
	private Topic topic;
	
//	private List<ChapterDto> chapterList;
//	private List<TopicDto> topicList;
	private List<TopicDto> parentTopics;
	
	private List<CourseDto> courseList;
	private List<SubjectDto> subjectList;
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        window.setSizeFull();
//        loadTopic();
        loadCourse();
        window.addComponent(mainLayout());
//        loadChapterBox();
        loadSubject();
        loadSubjectComboBox();
    }

    BeanItemContainer<ContentDto> contentContainer;
	ContainerHierarchicalWrapper contentWrapperContainer;
	private TreeTable treeTable;
	
	/*private final static String COLUMN_CONTENT = "content";
	private final static String COLUMN_TIME = "time";*/
	
	private final static Action ADD_CHAPTER = new Action("Add Chapter");
	private final static Action ADD_TOPIC = new Action("Add Sub Topic");
//	private final static Action EDIT = new Action("Edit");
	private final static Action REMOVE = new Action("Remove");
	private final static Action READ_MODE = new Action("Read Mode");
	private final static Action WRITE_MODE = new Action("Write Mode");
	
	private ComboBox courseComboBox;
	private ComboBox subjectComboBox;
	private ComboBox contentTypeComboBox;
	private TextField chapterField;
	private TextField topicField;
	
//	private ComboBox chapterComboBox;
	private TextField parentField;
	private Button addChapterButton;
	
	
    private final static String CONTENT = "content";
    private final static String TIME = "time";
    private final static String NOTE = "note";
    
    private long pos = 0;
    private ContentDto contentTopic;
    private ContentDto chapterContent;
    
    private ContentDto nextContent;
    
    HashMap<Integer,TextField> valueFields = new HashMap<Integer,TextField>();
    
    private GridLayout mainLayout(){
    	GridLayout mainLayout = new GridLayout(8, 12);
    	mainLayout.setSizeFull();
    	mainLayout.setSpacing(true);
    	mainLayout.setHeight("200%");
    	
    	courseComboBox = new ComboBox("Course");
    	courseComboBox.setWidth("100%");
    	courseComboBox.setImmediate(true);
    	
    	if (courseList!=null) {
			for (CourseDto course : courseList) {
				courseComboBox.addItem(course);
			}
		}
    	
    	subjectComboBox = new ComboBox("Subject");
    	subjectComboBox.setWidth("100%");
    	subjectComboBox.setImmediate(true);
    	subjectComboBox.setRequired(true);
    	subjectComboBox.setNullSelectionAllowed(false);
    	
    	courseComboBox.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				subjectComboBox.removeAllItems();
				loadSubject();
				loadSubjectComboBox();
			}
		});
    	
    	contentContainer = new BeanItemContainer<ContentDto>(ContentDto.class);
        contentWrapperContainer = new ContainerHierarchicalWrapper(contentContainer);

        subjectComboBox.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				container();
			}
		});	

        treeTable = new TreeTable("", contentWrapperContainer);
//        treeTable.setSelectable(true);
        treeTable.setSizeFull();
//        treeTable.setColumnHeaderMode(TreeTable.COLUMN_HEADER_MODE_HIDDEN);
        treeTable.setStyleName(Reindeer.TABLE_BORDERLESS);
        treeTable.setColumnReorderingAllowed(true);
        treeTable.setImmediate(true);
        treeTable.setHeight("200%");
//        treeTable.setEditable(true);
        
        treeTable.setColumnHeader(CONTENT, "Subject Content");
        treeTable.setColumnHeader(TIME, "Time(minutes)");
        treeTable.setColumnHeader(NOTE, "Note");
        treeTable.setVisibleColumns(new String[]{CONTENT, TIME, NOTE});
        
        treeTable.setColumnExpandRatio(CONTENT, 3);
        treeTable.setColumnExpandRatio(TIME, 1);
        treeTable.setColumnExpandRatio(NOTE, 2);
        
        treeTable.setCaption("Table of Content");
        
        contentTypeComboBox = new ComboBox("Content Type");
        contentTypeComboBox.setImmediate(true);
        
        for (ContentType type : ContentType.values()) {
			contentTypeComboBox.addItem(type);
		}
                
        treeTable.addActionHandler(new Handler() {
			public void handleAction(Action action, Object sender, Object target) {
				
				ContentDto contentDto = (ContentDto) target;
				if (action==ADD_CHAPTER) {
					saveChapter(chapter);
				}else if (action==ADD_TOPIC) {
					saveTopic(topic, contentDto);
				}else if (action==REMOVE) {
					if (contentDto!=null) {
						try {
				    		if (contentDto.getParent()==0) {
								Chapter chp = ChapterLocalServiceUtil.fetchChapter(contentDto.getId());
								
								List<Topic> topics = TopicLocalServiceUtil.findByChapter(chp.getChapterId());
								for (Topic topic : topics) {
									TopicLocalServiceUtil.deleteTopic(topic);
								}
								ChapterLocalServiceUtil.deleteChapter(chp);
								contentWrapperContainer.removeItemRecursively(contentDto);
				    		}else{
				    			Topic parentTopic = TopicLocalServiceUtil.fetchTopic(contentDto.getId());
				    			deleteContent(parentTopic);
				    			TopicLocalServiceUtil.deleteTopic(parentTopic);
				    			contentWrapperContainer.removeItemRecursively(contentDto);
				    		}
				    	} catch (SystemException e) {
							e.printStackTrace();
						}
					}
				}else if (action==READ_MODE) {
					treeTable.setSelectable(false);
					treeTable.setEditable(false);
				}else if(action==WRITE_MODE){
					treeTable.setSelectable(true);
					treeTable.setEditable(true);
				}
			}
			
			public Action[] getActions(Object target, Object sender) {
				
				if (treeTable.isSelectable() && treeTable.isEditable()) {
					return new Action[]{READ_MODE, ADD_CHAPTER, ADD_TOPIC, REMOVE };
				}else{
					return new Action[]{ADD_CHAPTER, READ_MODE, WRITE_MODE};
				}
			}
		});
        
        treeTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			public Field createField(Container container, final Object itemId,
					Object propertyId, Component uiContext) {
				
				
				if (propertyId.equals(CONTENT)) {
					final TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(false);
					field.setImmediate(true);
					ContentDto contentId = (ContentDto) itemId;
					field.setData(contentId);
					
					field.addListener(new FocusListener() {
						public void focus(FocusEvent event) {
							contentTopic = null;
							chapterContent = null;
							nextContent = null;
							treeTable.select(itemId);
						}
					});
					
					field.addListener(new BlurListener() {
						public void blur(BlurEvent event) {
							ContentDto content = (ContentDto) treeTable.getValue();
							if (content!=null) {
								editContent(content);
							}
							
						}
					});
					
					field.addShortcutListener(new ShortcutListener("", ShortcutAction.KeyCode.ENTER, null) {
						public void handleAction(Object sender, Object target) {
							ContentDto content = (ContentDto) treeTable.getValue();
							editContent(content);
							if (content.getParent()==0) {
								saveChapter(chapter);
							}else{
								topicEnterEvent(topic, content);
							}
						}
					});
					
					final ContentDto contentDto = (ContentDto) itemId;
					valueFields.put((int) contentDto.getId(), field);
					if (contentTopic!=null) {
						if ((int)contentDto.getId()==(int)contentTopic.getId()) {
							field.focus();
						}
					}
					
					if (chapterContent!=null) {
						if ((int)contentDto.getId()==(int)chapterContent.getId()) {
							field.focus();
						}
					}
					
					field.addShortcutListener(new ShortcutListener("", ShortcutAction.KeyCode.ARROW_DOWN, null) {
						public void handleAction(Object sender, Object target) {
							if (target instanceof TextField) {
								ContentDto content = (ContentDto) treeTable.getValue();
								editContent(content);
								nextContent = (ContentDto) treeTable.nextItemId(content);
								treeTable.select(nextContent);
								ContentDto selected = (ContentDto) treeTable.getValue();
								if ((int)nextContent.getId()==(int)selected.getId()) {
									treeTable.setCurrentPageFirstItemId(nextContent.getId());
								}
							}
						}
					});
					
					field.addShortcutListener(new ShortcutListener("", ShortcutAction.KeyCode.ARROW_UP, null) {
						public void handleAction(Object sender, Object target) {
							if (target instanceof TextField) {
								contentTopic = null;
								chapterContent = null;
								ContentDto content = (ContentDto) treeTable.getValue();
								editContent(content);
								nextContent = (ContentDto) treeTable.prevItemId(content);
								treeTable.select(nextContent);
								treeTable.setCurrentPageFirstItemId(nextContent.getId());
								ContentDto selected = (ContentDto) treeTable.getValue();
								if (nextContent.getId()==selected.getId()) {
									treeTable.setCurrentPageFirstItemId(nextContent.getId());
								}
							}
						}
					});
					
					if (nextContent!=null) {
						ContentDto selected = (ContentDto) itemId;
						if ((int)selected.getId()==(int)nextContent.getId()) {
							treeTable.getCurrentPageFirstItemId();
							field.focus();
						}
					}
										
					return field;
				}
				if (propertyId.equals(TIME)) {
					final NumericField field = new NumericField();
					field.setNullSettingAllowed(true);
					field.setNumberType(NumericFieldType.INTEGER);
					field.setWidth("100%");
					field.setNullRepresentation("");
					field.setReadOnly(false);
					
					field.addListener(new FocusListener() {
						public void focus(FocusEvent event) {
							treeTable.select(itemId);
						}
					});
					
					field.addListener(new BlurListener() {
						public void blur(BlurEvent event) {
							ContentDto content = (ContentDto) treeTable.getValue();
							if (content!=null) {
								editContent(content);
							}
						}
					});
					
					field.addShortcutListener(new ShortcutListener("", ShortcutAction.KeyCode.ENTER, null) {
						public void handleAction(Object sender, Object target) {
							ContentDto content = (ContentDto) treeTable.getValue();
							editContent(content);
							if (content.getParent()==0) {
								saveChapter(chapter);
							}else{
								topicEnterEvent(topic, content);
							}
						}
					});
					
					return field;
				}
				if (propertyId.equals(NOTE)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(false);
					field.setNullRepresentation("");
					field.addListener(new FocusListener() {
						public void focus(FocusEvent event) {
							treeTable.select(itemId);
						}
					});
					
					field.addListener(new BlurListener() {
						public void blur(BlurEvent event) {
							ContentDto content = (ContentDto) treeTable.getValue();
							if (content!=null) {
								editContent(content);
							}
							
						}
					});
					
					field.addShortcutListener(new ShortcutListener("", ShortcutAction.KeyCode.ENTER, null) {
						public void handleAction(Object sender, Object target) {
							ContentDto content = (ContentDto) treeTable.getValue();
							editContent(content);
							if (content.getParent()==0) {
								saveChapter(chapter);
							}else{
								topicEnterEvent(topic, content);
							}
							
						}
					});
					return field;
				}
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
        
        treeTable.setDragMode(TableDragMode.ROW);
        
        treeTable.setDropHandler(new DropHandler() {
			public AcceptCriterion getAcceptCriterion() {
				return new And(new SourceIs(treeTable), AcceptItem.ALL);
			}
			public void drop(DragAndDropEvent event) {
				DataBoundTransferable selectContent = (DataBoundTransferable) event.getTransferable();
				AbstractSelectTargetDetails targetContent = (AbstractSelectTargetDetails) event.getTargetDetails();
				
				ContentDto sourceItemId = (ContentDto) selectContent.getItemId();
				ContentDto targetItemId = (ContentDto) targetContent.getItemIdOver();
				
				VerticalDropLocation location = targetContent.getDropLocation();
				
				//Chapter drag and drop
				if (sourceItemId.getParent()==0) {
					
					SubjectDto subjectDto = (SubjectDto) subjectComboBox.getValue();
					try {
						List<Chapter> allChapters = ChapterLocalServiceUtil.findBySubject(subjectDto.getSubjectId());
						
						long sequence = 0;
						long maxNumber = 0;
						if (targetItemId.getParent()==0) {
							sequence = targetItemId.getSequence();
							
						}else{
							Topic topicId = TopicLocalServiceUtil.fetchTopic(targetItemId.getId());
							Chapter chp = ChapterLocalServiceUtil.fetchChapter(topicId.getChapterId());
							sequence = chp.getSequence();
						}
						
						if (location==VerticalDropLocation.TOP) {
							Chapter chapter = ChapterLocalServiceUtil.fetchChapter(sourceItemId.getId());
							chapter.setCompanyId(themeDisplay.getCompanyId());
							chapter.setUserId(themeDisplay.getUser().getUserId());
							chapter.setUserName(themeDisplay.getUser().getFullName());
							chapter.setName(chapter.getName());
							chapter.setSubjectId(subjectDto.getSubjectId());
							chapter.setSequence(sequence);
							
							ChapterLocalServiceUtil.updateChapter(chapter);
							
							Chapter targetChapter = ChapterLocalServiceUtil.fetchChapter(targetItemId.getId());
							targetChapter.setCompanyId(themeDisplay.getCompanyId());
							targetChapter.setUserId(themeDisplay.getUser().getUserId());
							targetChapter.setUserName(themeDisplay.getUser().getFullName());
							targetChapter.setName(targetChapter.getName());
							targetChapter.setSubjectId(subjectDto.getSubjectId());
							targetChapter.setSequence(sequence+1);
							
							ChapterLocalServiceUtil.updateChapter(targetChapter);
							
							List<Chapter> chapterList = ChapterLocalServiceUtil.findBySubject(subjectDto.getSubjectId());
							if (chapterList.size()>0) {
								for (Chapter cp : chapterList) {
									int number = (int) cp.getSequence();
									if (number>maxNumber) {
										maxNumber = number;
									}
								}
							}
							
							DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Chapter.class);
							long i = sequence+2;
							Criterion criterion = RestrictionsFactoryUtil.between("sequence", sequence+1, maxNumber);
							dynamicQuery.add(criterion).add(PropertyFactoryUtil.forName("subjectId").eq(subjectDto.getSubjectId())).addOrder(OrderFactoryUtil.asc("sequence"));
							List<Chapter> dynamicChapters = ChapterLocalServiceUtil.dynamicQuery(dynamicQuery);
							for (Chapter chapter2 : dynamicChapters) {
								if (targetChapter.getChapterId()!=chapter2.getChapterId()) {
									chapter2.setCompanyId(chapter2.getCompanyId());
									chapter2.setUserId(chapter2.getUserId());
									chapter2.setUserName(chapter2.getUserName());
									chapter2.setName(chapter2.getName());
									chapter2.setSubjectId(chapter2.getSubjectId());
									chapter2.setSequence(i);
									ChapterLocalServiceUtil.updateChapter(chapter2);
									i++;
								}
							}
							container();
						}else if (location==VerticalDropLocation.BOTTOM) {
							Chapter chapter = ChapterLocalServiceUtil.fetchChapter(sourceItemId.getId());
							chapter.setCompanyId(themeDisplay.getCompanyId());
							chapter.setUserId(themeDisplay.getUser().getUserId());
							chapter.setUserName(themeDisplay.getUser().getFullName());
							chapter.setName(chapter.getName());
							chapter.setSubjectId(subjectDto.getSubjectId());
							chapter.setSequence(sequence+1);
							
							ChapterLocalServiceUtil.updateChapter(chapter);
							
							List<Chapter> chapterList = ChapterLocalServiceUtil.findBySubject(subjectDto.getSubjectId());
							if (chapterList.size()>0) {
								for (Chapter cp : chapterList) {
									int number = (int) cp.getSequence();
									if (number>maxNumber) {
										maxNumber = number;
									}
								}
							}
							
							DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Chapter.class);
							
							Criterion criterion = RestrictionsFactoryUtil.between("sequence", sequence+1, maxNumber);
							dynamicQuery.add(criterion).add(PropertyFactoryUtil.forName("subjectId").eq(subjectDto.getSubjectId())).addOrder(OrderFactoryUtil.asc("sequence"));
							List<Chapter> dynamicChapters = ChapterLocalServiceUtil.dynamicQuery(dynamicQuery);
							
							
							long i = sequence+2;
							
							for (Chapter chapter2 : dynamicChapters) {
								if (chapter.getChapterId()!=chapter2.getChapterId()) {
									chapter2.setCompanyId(chapter2.getCompanyId());
									chapter2.setUserId(chapter2.getUserId());
									chapter2.setUserName(chapter2.getUserName());
									chapter2.setName(chapter2.getName());
									chapter2.setSubjectId(chapter2.getSubjectId());
									chapter2.setSequence(i);
									ChapterLocalServiceUtil.updateChapter(chapter2);
									i++;
								}
							}
							container();
						}
						
						
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}
				//end Chapter drag and drop
				
				//Topic drag and drop
				treeTable.setImmediate(true);
				
				for (ContentDto content = targetItemId; content!=null; content=(ContentDto) treeTable.getParent(content)) {
					if (content==sourceItemId) {
						return;
					}
				}
				
				ContentDto preParent = (ContentDto) contentWrapperContainer.getParent(sourceItemId);
				if (contentWrapperContainer.hasChildren(preParent)==false) {
					contentWrapperContainer.setChildrenAllowed(preParent, false);
				}
				
				if (contentWrapperContainer.hasChildren(targetItemId)==false) {
					contentWrapperContainer.setChildrenAllowed(targetItemId, true);
				}
				
				if (sourceItemId.getParent()!=0) {
					if (location==VerticalDropLocation.MIDDLE) {
						contentWrapperContainer.addItem(sourceItemId);
						treeTable.setCollapsed(targetItemId, true);
						contentWrapperContainer.setParent(sourceItemId, targetItemId);
						treeTable.setCollapsed(targetItemId, false);
						updateDropRow(sourceItemId, targetItemId);
												
					}else if (location==VerticalDropLocation.TOP) {
						ContentDto parent = (ContentDto) contentWrapperContainer.getParent(targetItemId);
						if (contentWrapperContainer.setParent(sourceItemId, parent)) {
							contentWrapperContainer.addItem(sourceItemId);
							treeTable.setCollapsed(parent, true);
							contentWrapperContainer.setParent(sourceItemId, targetItemId);
							treeTable.setCollapsed(parent, false);
							updateDropRow(sourceItemId, targetItemId);
						}
					}else if (location==VerticalDropLocation.BOTTOM) {
						ContentDto parent = (ContentDto) contentWrapperContainer.getParent(targetItemId);
						if (contentWrapperContainer.setParent(sourceItemId, parent)) {
							contentWrapperContainer.addItem(sourceItemId);
							treeTable.setCollapsed(parent, true);
							contentWrapperContainer.setParent(sourceItemId, targetItemId);
							treeTable.setCollapsed(parent, false);
							updateDropRow(sourceItemId, targetItemId);
						}
					}
				}
				//end Topic drag and drop
				treeTable.refreshRowCache();
			}
		});
        
        Label label = new Label("all activities on the page are autosaving");
        
        mainLayout.addComponent(courseComboBox, 0, 0, 2, 0);
        mainLayout.addComponent(subjectComboBox, 5, 0, 7, 0);
        
        mainLayout.addComponent(treeTable, 0, 1, 7, 1);
        mainLayout.addComponent(label, 0, 2, 7, 2);
        
    	return mainLayout;
    }    
    
    private void saveChapter(Chapter chapter){
    	if (chapter==null) {
			chapter = new ChapterImpl();
			chapter.setNew(true);
		}
		
		chapter.setCompanyId(themeDisplay.getCompanyId());
		chapter.setUserId(themeDisplay.getUser().getUserId());
		chapter.setUserName(themeDisplay.getUser().getFullName());
		
		SubjectDto subject = (SubjectDto) subjectComboBox.getValue();
		int maxNumber = 0;
		if (subject!=null) {
			chapter.setSubjectId(subject.getSubjectId());
			try {
				List<Chapter> chapterList = ChapterLocalServiceUtil.findBySubject(subject.getSubjectId());
				if (chapterList.size()>0) {
					for (Chapter cp : chapterList) {
						int number = (int) cp.getSequence();
						if (number>maxNumber) {
							maxNumber = number;
						}
					}
				}
				
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}else{
			window.showNotification("Please select a subject", Window.Notification.TYPE_ERROR_MESSAGE);
			return;
		}
		
		chapter.setName("New Chapter");
		chapter.setSequence(maxNumber+1);
		try {
			if (chapter.isNew()) {
				ChapterLocalServiceUtil.addChapter(chapter);
				chapterContent = new ContentDto();
				chapterContent.setId(chapter.getChapterId());
				chapterContent.setContent(chapter.getName());
				chapterContent.setParent(0);
	            
	            contentWrapperContainer.addItem(chapterContent);
	            contentWrapperContainer.setChildrenAllowed(chapterContent, false);
	            
	            treeTable.select(chapterContent);
	            treeTable.setCurrentPageFirstItemId(chapterContent.getId());
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
    private void saveTopic(Topic topic, ContentDto target){
    	if (topic==null) {
			topic = new TopicImpl();
			topic.setNew(true);
		}
		
		topic.setCompanyId(themeDisplay.getCompany().getCompanyId());
		topic.setUserId(themeDisplay.getUser().getUserId());
		topic.setUserName(themeDisplay.getUser().getFullName());
		
		topic.setTopic("New Topic");
		
		
		ContentDto content = (ContentDto) treeTable.getValue();
		if (content!=null) {
			if (content.getParent()==0) {
				topic.setChapterId(content.getId());
				topic.setParentTopic(0);
			}else{
				try {
					Topic tp = TopicLocalServiceUtil.fetchTopic(content.getId());
					topic.setChapterId(tp.getChapterId());
					topic.setParentTopic(tp.getTopicId());
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		}else{
			window.showNotification("Please select a chapter or topic", Window.Notification.TYPE_ERROR_MESSAGE);
			return;
		}
		
		try {
			if (topic.isNew()) {
				TopicLocalServiceUtil.addTopic(topic);
				treeTable.setChildrenAllowed(target, true);
				if (target!=null) {
					if (content.getParent()==0) {
						ContentDto contentTopic = new ContentDto();
						contentTopic.setId(topic.getTopicId());
						contentTopic.setContent(topic.getTopic());
						contentTopic.setParent(target.getId());
						
						treeTable.select(contentTopic);
						
						treeTable.setChildrenAllowed(target, true);
						treeTable.setImmediate(true);
						treeTable.refreshRowCache();
						treeTable.addItem(contentTopic);
						treeTable.setCollapsed(target, true);
						treeTable.setParent(contentTopic, target);
						treeTable.setCollapsed(target, false);
						treeTable.setChildrenAllowed(contentTopic, false);
						
					}else{
						ContentDto contentTopic = new ContentDto();
						contentTopic.setId(topic.getTopicId());
						contentTopic.setContent(topic.getTopic());
						contentTopic.setParent(target.getId());
						
						treeTable.setChildrenAllowed(target, true);
						treeTable.setImmediate(true);
						treeTable.refreshRowCache();
						treeTable.addItem(contentTopic);
						treeTable.setCollapsed(target, true);
						treeTable.setParent(contentTopic, target);
						treeTable.setCollapsed(target, false);
						treeTable.setChildrenAllowed(contentTopic, false);
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
        
    public void topicEnterEvent(Topic topic, ContentDto content){
    	if (topic==null) {
			topic = new TopicImpl();
			topic.setNew(true);
		}
    	
		
		topic.setCompanyId(themeDisplay.getCompany().getCompanyId());
		topic.setUserId(themeDisplay.getUser().getUserId());
		topic.setUserName(themeDisplay.getUser().getFullName());
		
		topic.setTopic("New Topic");
		
		ContentDto parentContent = (ContentDto) treeTable.getParent(content);
		
		if (content!=null) {
			if (parentContent.getParent()==0) {
				topic.setChapterId(parentContent.getId());
				topic.setParentTopic(0);
			}else{
				try {
					Topic tp = TopicLocalServiceUtil.fetchTopic(content.getId());
					topic.setChapterId(tp.getChapterId());
					topic.setParentTopic(parentContent.getId());
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		}
		
		try {
			TopicLocalServiceUtil.addTopic(topic);
//			treeTable.setChildrenAllowed(target, true);
				
			contentTopic = new ContentDto();
			contentTopic.setId(topic.getTopicId());
			contentTopic.setContent(topic.getTopic());
			contentTopic.setParent(content.getParent());
			
			treeTable.setChildrenAllowed(parentContent, true);
			treeTable.setImmediate(true);
			treeTable.refreshRowCache();
			treeTable.addItem(contentTopic);
			treeTable.setCollapsed(parentContent, true);
			treeTable.setParent(contentTopic, parentContent);
			treeTable.setCollapsed(parentContent, false);
			treeTable.setChildrenAllowed(contentTopic, false);
			
			treeTable.setCurrentPageFirstItemId(contentTopic.getId());
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
        
    private void editContent(ContentDto content){
    	try {
	    	if (content.getParent()==0) {
				Chapter chapter = ChapterLocalServiceUtil.fetchChapter(content.getId());
				chapter.setCompanyId(themeDisplay.getCompanyId());
				chapter.setUserId(themeDisplay.getUser().getUserId());
				chapter.setUserName(themeDisplay.getUser().getFullName());
				
				SubjectDto subject = (SubjectDto) subjectComboBox.getValue();
				if (subject!=null) {
					chapter.setSubjectId(subject.getSubjectId());
				}
				
				chapter.setName(content.getContent());
				chapter.setSequence(chapter.getSequence());
				chapter.setNote(content.getNote());
				ChapterLocalServiceUtil.updateChapter(chapter);
			}else{
				Topic topic = TopicLocalServiceUtil.fetchTopic(content.getId());
				topic.setCompanyId(topic.getCompanyId());
				topic.setUserId(topic.getUserId());
				topic.setUserName(topic.getUserName());
				
				topic.setTopic(content.getContent());
				if (content.getTime()!=null) {
					topic.setTime(content.getTime());
				}
				topic.setChapterId(topic.getChapterId());
				topic.setParentTopic(topic.getParentTopic());
				topic.setNote(content.getNote());
				TopicLocalServiceUtil.updateTopic(topic);
			}
    	} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
    private void deleteContent(Topic content){
    	try {
			List<Topic> childTopics = TopicLocalServiceUtil.findByParentTopic(content.getTopicId());
			if (content!=null) {
				if (childTopics.size()>0) {
					for (Topic tp : childTopics) {
						TopicLocalServiceUtil.deleteTopic(tp);
						deleteContent(tp);
					}
				}
			}
    	} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
    private void updateDropRow(ContentDto sourceItemId, ContentDto targetItemId){
    	try {
			Topic topic = TopicLocalServiceUtil.fetchTopic(sourceItemId.getId());
			Topic parentTopic = TopicLocalServiceUtil.fetchTopic(targetItemId.getId());
			
			topic.setCompanyId(topic.getCompanyId());
			topic.setUserId(topic.getUserId());
			topic.setUserName(topic.getUserName());
			topic.setTopic(topic.getTopic());
			
			if (targetItemId.getParent()==0) {
				topic.setChapterId(targetItemId.getId());
				topic.setParentTopic(0);
			}else{
				topic.setChapterId(parentTopic.getChapterId());
				topic.setParentTopic(targetItemId.getId());
			}
			TopicLocalServiceUtil.updateTopic(topic);
			updateDropChildRow(topic);
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
    private void updateDropChildRow(Topic parentTopic){
    	try {
			
			List<Topic> childTopics = TopicLocalServiceUtil.findByParentTopic(parentTopic.getTopicId());
			if (childTopics!=null) {
				for (Topic child : childTopics) {
					Topic topic = TopicLocalServiceUtil.fetchTopic(child.getTopicId());
					
					topic.setCompanyId(child.getCompanyId());
					topic.setUserId(child.getUserId());
					topic.setUserName(child.getUserName());
					topic.setTopic(child.getTopic());
					topic.setChapterId(parentTopic.getChapterId());
					topic.setParentTopic(parentTopic.getTopicId());
					TopicLocalServiceUtil.updateTopic(topic);
					updateDropChildRow(topic);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
    private void container() {
    	treeTable.removeAllItems();
        try {
        	SubjectDto subject = (SubjectDto) subjectComboBox.getValue();
        	if (subject!=null) {
        		List<Chapter> chapters = ChapterLocalServiceUtil.findBySubject(subject.getSubjectId());
	        	
    			for (Chapter chp : chapters) {
    	            ContentDto chapterContent = new ContentDto();
    	            
    	            chapterContent.setId(chp.getChapterId());
    	            chapterContent.setContent(chp.getName());
    	            chapterContent.setSequence(chp.getSequence());
    	            chapterContent.setParent(0);
    	            
    	            contentWrapperContainer.addItem(chapterContent);
    	            
    	            List<Topic> topics = TopicLocalServiceUtil.findByChapter(chp.getChapterId());
    	            
    	            if (topics.size()==0) {
    	            	contentWrapperContainer.setChildrenAllowed(chapterContent, false);
					}else{
						contentWrapperContainer.setChildrenAllowed(chapterContent, true);
					}
    	            
    	            if (topics!=null) {
    	            	
    					for (Topic topic : topics) {
    						ContentDto parentTopic = new ContentDto();
    						parentTopic.setId(topic.getTopicId());
    						parentTopic.setContent(topic.getTopic());
    						parentTopic.setParent(chp.getChapterId());
    						parentTopic.setTime(topic.getTime());
    						
    						if (topic.getParentTopic()==0) {
    							contentWrapperContainer.addItem(parentTopic);
    							contentWrapperContainer.setParent(parentTopic, chapterContent);
    							
    							List<Topic> childList = TopicLocalServiceUtil.findByParentTopic(topic.getTopicId());
    							if (childList.size()==0) {
    								contentWrapperContainer.setChildrenAllowed(parentTopic, false);
    							}else{
    								contentWrapperContainer.setChildrenAllowed(parentTopic, true);
    							}
    							
    							recursiveTopic(parentTopic);
    							
    						}
    					}
    				}
    	        }
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
    private void recursiveTopic(ContentDto topic){
    	try {
			List<Topic> allTopics = TopicLocalServiceUtil.findByParentTopic(topic.getId());
			if (allTopics!=null) {
				
				long topicTotal = 0;
				for (Topic topic2 : allTopics) {
					ContentDto childTopic = new ContentDto();
					childTopic.setId(topic2.getTopicId());
					childTopic.setContent(topic2.getTopic());
					childTopic.setParent(topic.getId());
					childTopic.setTime(topic2.getTime());
					
					//int totalTime = 0;
					contentWrapperContainer.addItem(childTopic);
					contentWrapperContainer.setParent(childTopic, topic);
					
					/*if(!contentWrapperContainer.hasChildren(childTopic))
					{
						topicTotal += childTopic.getTime();
					}*/
					
					List<Topic> childList = TopicLocalServiceUtil.findByParentTopic(topic2.getTopicId());
					
					if (childList!=null) {
						for (Topic topic3 : childList) {
							if(!topicTime(topic3)){
								topicTotal +=topic3.getTime();
							}
						}
					}
					
					if (childList.size()==0) {
						topicTotal += childTopic.getTime();
						contentWrapperContainer.setChildrenAllowed(childTopic, false);
					}else{
						contentWrapperContainer.setChildrenAllowed(childTopic, true);
					}
					
					recursiveTopic(childTopic);
					
				}
				
				if(allTopics.size()!=0) {
					topic.setTime(topicTotal);
				}
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
    private boolean topicTime(Topic topic){
    	boolean status=true;
		try {
			List<Topic> childs = TopicLocalServiceUtil.findByParentTopic(topic.getTopicId());
			if (childs.size()==0) {
				status=false;
			}else{
				for (Topic topic2 : childs) {
					topicTime(topic2);
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return status;
    }
    
    private void loadCourse(){
    	try {
    		courseList = new ArrayList<CourseDto>();
			List<Course> courses = CourseLocalServiceUtil.findByCompany(themeDisplay.getCompany().getCompanyId());
			for (Course course : courses) {
				CourseDto courseDto = new CourseDto();
				courseDto.setCourseId(course.getCourseId());
				courseDto.setCourseName(course.getCourseName());
				courseList.add(courseDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
    private void loadSubject(){
    	if (subjectList!=null) {
			subjectList.clear();
		}
    	subjectList = new ArrayList<SubjectDto>();
    	try {
	    	CourseDto course = (CourseDto) courseComboBox.getValue();
	    	if (course==null) {
	    		List<Subject> subjects = SubjectLocalServiceUtil.findByCompany(themeDisplay.getCompany().getCompanyId());
				for (Subject subject : subjects) {
					SubjectDto subjectDto = new SubjectDto();
					subjectDto.setSubjectId(subject.getSubjectId());
					subjectDto.setSubjcetName(subject.getSubjectName());
					subjectList.add(subjectDto);
				}
			}else{
				List<CourseSubject> courseSubjects = CourseSubjectLocalServiceUtil.findByCourseId(course.getCourseId());
				for (CourseSubject courseSubject : courseSubjects) {
					Subject subject = SubjectLocalServiceUtil.fetchSubject(courseSubject.getSubjectId());
					SubjectDto subjectDto = new SubjectDto();
					subjectDto.setSubjectId(subject.getSubjectId());
					subjectDto.setSubjcetName(subject.getSubjectName());
					subjectList.add(subjectDto);
				}
			}
    	} catch (SystemException e) {
			e.printStackTrace();
		}
    }
    
    private void loadSubjectComboBox(){
    	if (subjectList!=null) {
			for (SubjectDto subject : subjectList) {
				subjectComboBox.addItem(subject);
			}
		}
    }
    
    private void clearChapter(){
    	chapter = null;
    	chapterField.setValue("");
    }
    
    private void clearTopic(){
    	topic = null;
    	topicField.setValue("");
    }
    
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

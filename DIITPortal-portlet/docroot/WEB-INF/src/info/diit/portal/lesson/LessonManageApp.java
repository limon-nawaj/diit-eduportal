package info.diit.portal.lesson;

import info.diit.portal.dto.BatchDto;
import info.diit.portal.dto.LessonDto;
import info.diit.portal.dto.LessonManagementDto;
import info.diit.portal.dto.OrganizationDto;
import info.diit.portal.dto.SubjectDto;
import info.diit.portal.model.Batch;
import info.diit.portal.model.BatchSubject;
import info.diit.portal.model.LessonPlan;
import info.diit.portal.model.Subject;
import info.diit.portal.model.SubjectLesson;
import info.diit.portal.model.impl.SubjectLessonImpl;
import info.diit.portal.service.BatchLocalServiceUtil;
import info.diit.portal.service.BatchSubjectLocalServiceUtil;
import info.diit.portal.service.LessonPlanLocalServiceUtil;
import info.diit.portal.service.SubjectLessonLocalServiceUtil;
import info.diit.portal.service.SubjectLocalServiceUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action.Listener;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component.Event;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;

public class LessonManageApp extends Application implements PortletRequestListener{

	private ThemeDisplay themeDisplay;
	private Window window;
	
	private List<Organization> userOrganizationList;
	private List<OrganizationDto> campusList;
	private List<BatchDto> batchList;
	private List<SubjectDto> subjectList;
	private List<LessonDto> lessonList;
	
	private SubjectLesson subjectLesson;
	
	private final static String COLUMN_CAMPUS = "campus";
	private final static String COLUMN_BATCH = "batch";
	private final static String COLUMN_SUBJECT = "subject";
	private final static String COLUMN_LESSON = "lesson";
	
    public void init() {
        window = new Window("Vaadin Portlet Application");
        setMainWindow(window);
        
        try {
			userOrganizationList = themeDisplay.getUser().getOrganizations();
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
        
        loadCampus();
        window.addComponent(mainLayout());
        loadLessonManagement();
    }

    private ComboBox campusComboBox;
	private ComboBox batchComboBox;
	private ComboBox subjectComboBox;
	private ComboBox lessonComboBox;
	
	private Button saveButton;
	
	private BeanItemContainer<LessonManagementDto> container;
	private Table table;
	
	private GridLayout mainLayout(){
		GridLayout mainLayout = new GridLayout(8, 8);
		mainLayout.setWidth("100%");
		mainLayout.setSpacing(true);
		
		campusComboBox = new ComboBox("Campus");
		batchComboBox = new ComboBox("Batch");
		subjectComboBox = new ComboBox("Subject");
		lessonComboBox = new ComboBox("Lesson Plan");
		
		campusComboBox.setImmediate(true);
		batchComboBox.setImmediate(true);
		subjectComboBox.setImmediate(true);
		lessonComboBox.setImmediate(true);
		
		campusComboBox.setWidth("100%");
		batchComboBox.setWidth("100%");
		batchComboBox.setWidth("100%");
		subjectComboBox.setWidth("100%");
		lessonComboBox.setWidth("100%");
		
		batchComboBox.setRequired(true);
		subjectComboBox.setRequired(true);
		lessonComboBox.setRequired(true);
		
		if (campusList!=null) {
			for (OrganizationDto campus : campusList) {
				campusComboBox.addItem(campus);
			}
		}
		
		loadBatch();
		loadBatchCobBox();
		
		if (userOrganizationList.size()>1) {
			campusComboBox.addListener(new ValueChangeListener() {
				
				@Override
				public void valueChange(ValueChangeEvent event) {
					OrganizationDto campus = (OrganizationDto) campusComboBox.getValue();
					if (campus!=null) {
						loadBatch();
						loadBatchCobBox();
					}
				}
			});
		}
		
		batchComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				loadSubject();
				if (subjectList!=null) {
					for (SubjectDto subject : subjectList) {
						subjectComboBox.addItem(subject);
					}
				}
			}
		});
		
		subjectComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				loadLesson();
				if (lessonList!=null) {
					for (LessonDto lesson : lessonList) {
						lessonComboBox.addItem(lesson);
					}
				}
			}
		});
		
		saveButton = new Button("Save");
		saveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				if (subjectLesson==null) {
					subjectLesson = new SubjectLessonImpl();
					subjectLesson.setNew(true);
				}
				
				subjectLesson.setCompanyId(themeDisplay.getCompanyId());
				subjectLesson.setUserId(themeDisplay.getUserId());
				subjectLesson.setUserName(themeDisplay.getUser().getFullName());
				
				if (userOrganizationList.size()>1) {
					OrganizationDto campusDto = (OrganizationDto) campusComboBox.getValue();
					if (campusDto!=null) {
						subjectLesson.setOrganizationId(campusDto.getOrganizationId());
					}
				}else if(userOrganizationList.size()==1){
					try {
						subjectLesson.setOrganizationId(themeDisplay.getLayout().getGroup().getOrganizationId());
					} catch (PortalException e) {
						e.printStackTrace();
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}
				
				
				BatchDto batchDto = (BatchDto) batchComboBox.getValue();
				if (batchDto!=null) {
					subjectLesson.setBatchId(batchDto.getBatchId());
				} else {
					window.showNotification("Please select a batch", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				SubjectDto subjectDto = (SubjectDto) subjectComboBox.getValue();
				if (subjectDto!=null) {
					subjectLesson.setSubjectId(subjectDto.getSubjectId());
				} else {
					window.showNotification("Please select a subject", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				LessonDto lessonDto = (LessonDto) lessonComboBox.getValue();
				if (lessonDto!=null) {
					subjectLesson.setLessonPlanId(lessonDto.getId());
				} else {
					window.showNotification("Please select a lesson", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				try {
					if (!checkBatchSubject(batchDto, subjectDto)) {
						if (subjectLesson.isNew()) {
							
							subjectLesson.setCreateDate(new Date());
							SubjectLessonLocalServiceUtil.addSubjectLesson(subjectLesson);
							window.showNotification("Lesson management save successfully");
						}else{
							subjectLesson.setModifiedDate(new Date());
							SubjectLessonLocalServiceUtil.updateSubjectLesson(subjectLesson);
							window.showNotification("Lesson management update successfully");
						}
						loadLessonManagement();
						clear();
					} else{
						window.showNotification("Subject already assign to the batch", Window.Notification.TYPE_ERROR_MESSAGE);
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
		
		
		Button resetButton = new Button("Reset");
		resetButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				clear();
			}
		});
		
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setSpacing(true);
		buttonLayout.addComponent(saveButton);
		buttonLayout.addComponent(resetButton);
		
		container = new BeanItemContainer<LessonManagementDto>(LessonManagementDto.class);
		table = new Table("", container);
		table.setWidth("100%");
		table.setSelectable(true);
		table.setColumnHeader(COLUMN_CAMPUS, "Campus");
		table.setColumnHeader(COLUMN_BATCH, "Batch");
		table.setColumnHeader(COLUMN_SUBJECT, "Subject");
		table.setColumnHeader(COLUMN_LESSON, "Lesson");
		
		if (userOrganizationList.size()>1) {
			table.setVisibleColumns(new String[]{COLUMN_CAMPUS, COLUMN_BATCH, COLUMN_SUBJECT, COLUMN_LESSON});
		}else{
			table.setVisibleColumns(new String[]{COLUMN_BATCH, COLUMN_SUBJECT, COLUMN_LESSON});
		}
		
		Button editButton = new Button("Edit");
		editButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				LessonManagementDto managementDto = (LessonManagementDto) table.getValue();
				if (managementDto!=null) {
					edit(managementDto.getId());
				}
			}
		});
		
		Button deleteButton = new Button("Delete");
		deleteButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				LessonManagementDto managementDto = (LessonManagementDto) table.getValue();
				if (managementDto!=null) {
					try {
						subjectLesson = SubjectLessonLocalServiceUtil.fetchSubjectLesson(managementDto.getId());
						SubjectLessonLocalServiceUtil.deleteSubjectLesson(subjectLesson);
						loadLessonManagement();
						clear();
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}
			}
		});
		
		Label spacer = new Label();
		
		HorizontalLayout rowLayout = new HorizontalLayout();
		rowLayout.setSpacing(true);
		rowLayout.setWidth("100%");
		rowLayout.addComponent(spacer);
		rowLayout.addComponent(editButton);
		rowLayout.addComponent(deleteButton);
		rowLayout.setExpandRatio(spacer, 1);
		
		
		if (userOrganizationList.size()==1) {
			mainLayout.addComponent(batchComboBox, 0, 0, 1, 0);
			mainLayout.addComponent(subjectComboBox, 0, 1, 1, 1);
			mainLayout.addComponent(lessonComboBox, 0, 2, 1, 2);
			mainLayout.addComponent(buttonLayout, 0, 3, 1, 3);
			mainLayout.addComponent(table, 3, 0, 7, 6);
			mainLayout.addComponent(rowLayout, 3, 7, 7, 7);
		}else{
			mainLayout.addComponent(campusComboBox, 0, 0, 1, 0);
			mainLayout.addComponent(batchComboBox, 0, 1, 1, 1);
			mainLayout.addComponent(subjectComboBox, 0, 2, 1, 2);
			mainLayout.addComponent(lessonComboBox, 0, 3, 1, 3);
			mainLayout.addComponent(buttonLayout, 0, 4, 1, 4);
			mainLayout.addComponent(table, 3, 0, 7, 6);
			mainLayout.addComponent(rowLayout, 3, 7, 7, 7);
		}
		
		return mainLayout;
	}
	
	private boolean checkBatchSubject(BatchDto batchDto, SubjectDto subjectDto){
		boolean status = false;
		try {
			if (batchDto!=null && subjectDto!=null) {
				List<SubjectLesson> subjectLessons = SubjectLessonLocalServiceUtil.findByBatchSubject(batchDto.getBatchId(), subjectDto.getSubjectId());
				if (subjectLessons!=null) {
					for (SubjectLesson sl : subjectLessons) {
						if (subjectLesson.getSubjectLessonId()!=sl.getSubjectLessonId()) {
							status = true;
						}else{
							status = false;
						}
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return status;
	}
	
	private void loadBatchCobBox(){
		batchComboBox.removeAllItems();
		if (batchList!=null) {
			for (BatchDto batch : batchList) {
				batchComboBox.addItem(batch);
			}
		}
	}
	
	private void edit(long managementId){
		try {
			subjectLesson = SubjectLessonLocalServiceUtil.fetchSubjectLesson(managementId);
			if (userOrganizationList.size()>1) {
				campusComboBox.setValue(getCampus(subjectLesson.getOrganizationId()));
			}
			batchComboBox.setValue(getBatch(subjectLesson.getBatchId()));
			subjectComboBox.setValue(getSubject(subjectLesson.getSubjectId()));
			lessonComboBox.setValue(getLesson(subjectLesson.getLessonPlanId()));
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private OrganizationDto getCampus(long campusId){
		if (campusList!=null) {
			for (OrganizationDto campus : campusList) {
				if (campus.getOrganizationId()==campusId) {
					return campus;
				}
			}
		}
		return null;
	}
	
	private BatchDto getBatch(long batchId){
		if (batchList!=null) {
			for (BatchDto batch : batchList) {
				if (batch.getBatchId()==batchId) {
					return batch;
				}
			}
		}
		return null;
	}
	
	private SubjectDto getSubject(long subjectId){
		if (subjectList!=null) {
			for (SubjectDto subject : subjectList) {
				if (subject.getSubjectId()==subjectId) {
					return subject;
				}
			}
		}
		return null;
	}
	
	private LessonDto getLesson(long lessonId){
		if (lessonList!=null) {
			for (LessonDto lesson : lessonList) {
				if (lesson.getId()==lessonId) {
					return lesson;
				}
			}
		}
		return null;
	}
	
	private void loadCampus(){
		campusList = new ArrayList<OrganizationDto>();
		for (Organization organization : userOrganizationList) {
			OrganizationDto campus = new OrganizationDto();
			campus.setOrganizationId(organization.getOrganizationId());
			campus.setOrganizationName(organization.getName());
			campusList.add(campus);
		}
	}
	
	private void loadBatch(){
		try {
			batchList = new ArrayList<BatchDto>();

			List<BatchSubject> batchSubjectList = null;
			
			if (userOrganizationList.size()==1) {
				Organization org = userOrganizationList.get(0);
				batchSubjectList = BatchSubjectLocalServiceUtil.findBatchesByOrg(org.getOrganizationId()); 
			}else {
				OrganizationDto campus = (OrganizationDto) campusComboBox.getValue();
				if (campus!=null) {
					batchSubjectList = BatchSubjectLocalServiceUtil.findBatchesByOrg(campus.getOrganizationId());
				}
			}
			
			if (batchSubjectList!=null) {
				Set<Long> ids = new HashSet<Long>();
				for (BatchSubject batchSubject : batchSubjectList) {
					ids.add(batchSubject.getSubjectId());
				}
				
				if (ids!=null) {
					/*for (Long id : ids) {
						BatchDto batchDto = new BatchDto();
						Batch batch = BatchLocalServiceUtil.fetchBatch(id);
						if (batch!=null) {
							batchDto.setId(batch.getBatchId());
							batchDto.setName(batch.getBatchName());
							batchList.add(batchDto);
						}
					}*/
					Iterator<Long> batchIds = ids.iterator();
					while (batchIds.hasNext()) {
						Long id = (Long) batchIds.next();
						BatchDto batchDto = new BatchDto();
						Batch batch = BatchLocalServiceUtil.fetchBatch(id);
						if (batch!=null) {
							batchDto.setBatchId(batch.getBatchId());
							batchDto.setBatchName(batch.getBatchName());
							batchList.add(batchDto);
						}
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void loadSubject(){
		BatchDto batch = (BatchDto) batchComboBox.getValue();
		subjectComboBox.removeAllItems();
		try {
			subjectList = new ArrayList<SubjectDto>();
			if (batch!=null) {
				List<BatchSubject> batchSubjectList = BatchSubjectLocalServiceUtil.findSubjectByBatch(batch.getBatchId());
				if (batchSubjectList!=null) {
					for (BatchSubject batchSubject : batchSubjectList) {
						Subject subject = SubjectLocalServiceUtil.fetchSubject(batchSubject.getSubjectId());
						SubjectDto subjectDto = new SubjectDto();
						
						subjectDto.setSubjectId(subject.getSubjectId());
						subjectDto.setSubjcetName(subject.getSubjectName());
						subjectList.add(subjectDto);
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void loadLesson(){
		SubjectDto subject = (SubjectDto) subjectComboBox.getValue();
		lessonComboBox.removeAllItems();
		try {
			lessonList = new ArrayList<LessonDto>();
			if (subject!=null) {
				List<LessonPlan> lessonPlanList = LessonPlanLocalServiceUtil.findBySubject(subject.getSubjectId());
				if (lessonPlanList!=null) {
					for (LessonPlan lessonPlan : lessonPlanList) {
						LessonDto lesson = new LessonDto();
						lesson.setId(lessonPlan.getLessonPlanId());
						lesson.setLesson(lessonPlan.getTitle());
						lessonList.add(lesson);
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

	private void loadLessonManagement(){
		container.removeAllItems();
		try {
			if (campusList!=null) {
				for (OrganizationDto campus : campusList) {
					List<SubjectLesson> subjectLessonList = SubjectLessonLocalServiceUtil.findByUserOrganization(campus.getOrganizationId(), themeDisplay.getUserId());
					for (SubjectLesson subjectLesson : subjectLessonList) {
						LessonManagementDto lessonManagement = new LessonManagementDto();
						lessonManagement.setId(subjectLesson.getSubjectLessonId());
						
						OrganizationDto campusDto = new OrganizationDto();
						BatchDto batchDto = new BatchDto();
						SubjectDto subjectDto = new SubjectDto();
						LessonDto lessonDto = new LessonDto();
						
						Organization organization = OrganizationLocalServiceUtil.fetchOrganization(subjectLesson.getOrganizationId());
						Batch batch = BatchLocalServiceUtil.fetchBatch(subjectLesson.getBatchId());
						Subject subject = SubjectLocalServiceUtil.fetchSubject(subjectLesson.getSubjectId());
						LessonPlan lessonPlan = LessonPlanLocalServiceUtil.fetchLessonPlan(subjectLesson.getLessonPlanId());
						
						campusDto.setOrganizationId(organization.getOrganizationId());
						campusDto.setOrganizationName(organization.getName());
						
						batchDto.setBatchId(batch.getBatchId()); 
						batchDto.setBatchName(batch.getBatchName());
						
						subjectDto.setSubjectId(subject.getSubjectId());
						subjectDto.setSubjcetName(subject.getSubjectName());
						
						lessonDto.setId(lessonPlan.getLessonPlanId());
						lessonDto.setLesson(lessonPlan.getTitle());
						
						lessonManagement.setCampus(campusDto);
						lessonManagement.setBatch(batchDto);
						lessonManagement.setSubject(subjectDto);
						lessonManagement.setLesson(lessonDto);
						
						container.addBean(lessonManagement);
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void clear(){
		subjectLesson = null;
		campusComboBox.setValue(null);
		batchComboBox.setValue(null);
		subjectComboBox.setValue(null);
		lessonComboBox.setValue(null);
	}
    
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

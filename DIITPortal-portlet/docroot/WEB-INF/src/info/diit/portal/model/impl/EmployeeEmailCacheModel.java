/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.EmployeeEmail;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing EmployeeEmail in entity cache.
 *
 * @author mohammad
 * @see EmployeeEmail
 * @generated
 */
public class EmployeeEmailCacheModel implements CacheModel<EmployeeEmail>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{emailId=");
		sb.append(emailId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", employeeId=");
		sb.append(employeeId);
		sb.append(", workEmail=");
		sb.append(workEmail);
		sb.append(", personalEmail=");
		sb.append(personalEmail);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	public EmployeeEmail toEntityModel() {
		EmployeeEmailImpl employeeEmailImpl = new EmployeeEmailImpl();

		employeeEmailImpl.setEmailId(emailId);
		employeeEmailImpl.setCompanyId(companyId);
		employeeEmailImpl.setUserId(userId);

		if (userName == null) {
			employeeEmailImpl.setUserName(StringPool.BLANK);
		}
		else {
			employeeEmailImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			employeeEmailImpl.setCreateDate(null);
		}
		else {
			employeeEmailImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			employeeEmailImpl.setModifiedDate(null);
		}
		else {
			employeeEmailImpl.setModifiedDate(new Date(modifiedDate));
		}

		employeeEmailImpl.setEmployeeId(employeeId);

		if (workEmail == null) {
			employeeEmailImpl.setWorkEmail(StringPool.BLANK);
		}
		else {
			employeeEmailImpl.setWorkEmail(workEmail);
		}

		if (personalEmail == null) {
			employeeEmailImpl.setPersonalEmail(StringPool.BLANK);
		}
		else {
			employeeEmailImpl.setPersonalEmail(personalEmail);
		}

		employeeEmailImpl.setStatus(status);

		employeeEmailImpl.resetOriginalValues();

		return employeeEmailImpl;
	}

	public long emailId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long employeeId;
	public String workEmail;
	public String personalEmail;
	public long status;
}
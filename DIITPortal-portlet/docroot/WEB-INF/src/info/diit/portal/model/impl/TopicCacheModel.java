/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.Topic;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Topic in entity cache.
 *
 * @author mohammad
 * @see Topic
 * @generated
 */
public class TopicCacheModel implements CacheModel<Topic>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{topicId=");
		sb.append(topicId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", chapterId=");
		sb.append(chapterId);
		sb.append(", topic=");
		sb.append(topic);
		sb.append(", parentTopic=");
		sb.append(parentTopic);
		sb.append(", time=");
		sb.append(time);
		sb.append(", note=");
		sb.append(note);
		sb.append("}");

		return sb.toString();
	}

	public Topic toEntityModel() {
		TopicImpl topicImpl = new TopicImpl();

		topicImpl.setTopicId(topicId);
		topicImpl.setCompanyId(companyId);
		topicImpl.setOrganizationId(organizationId);
		topicImpl.setUserId(userId);

		if (userName == null) {
			topicImpl.setUserName(StringPool.BLANK);
		}
		else {
			topicImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			topicImpl.setCreateDate(null);
		}
		else {
			topicImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			topicImpl.setModifiedDate(null);
		}
		else {
			topicImpl.setModifiedDate(new Date(modifiedDate));
		}

		topicImpl.setChapterId(chapterId);

		if (topic == null) {
			topicImpl.setTopic(StringPool.BLANK);
		}
		else {
			topicImpl.setTopic(topic);
		}

		topicImpl.setParentTopic(parentTopic);
		topicImpl.setTime(time);

		if (note == null) {
			topicImpl.setNote(StringPool.BLANK);
		}
		else {
			topicImpl.setNote(note);
		}

		topicImpl.resetOriginalValues();

		return topicImpl;
	}

	public long topicId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long chapterId;
	public String topic;
	public long parentTopic;
	public long time;
	public String note;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.TaskDesignation;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing TaskDesignation in entity cache.
 *
 * @author mohammad
 * @see TaskDesignation
 * @generated
 */
public class TaskDesignationCacheModel implements CacheModel<TaskDesignation>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{taskDesignationId=");
		sb.append(taskDesignationId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", taskId=");
		sb.append(taskId);
		sb.append(", designationId=");
		sb.append(designationId);
		sb.append("}");

		return sb.toString();
	}

	public TaskDesignation toEntityModel() {
		TaskDesignationImpl taskDesignationImpl = new TaskDesignationImpl();

		taskDesignationImpl.setTaskDesignationId(taskDesignationId);
		taskDesignationImpl.setCompanyId(companyId);
		taskDesignationImpl.setUserId(userId);

		if (userName == null) {
			taskDesignationImpl.setUserName(StringPool.BLANK);
		}
		else {
			taskDesignationImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			taskDesignationImpl.setCreateDate(null);
		}
		else {
			taskDesignationImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			taskDesignationImpl.setModifiedDate(null);
		}
		else {
			taskDesignationImpl.setModifiedDate(new Date(modifiedDate));
		}

		taskDesignationImpl.setTaskId(taskId);
		taskDesignationImpl.setDesignationId(designationId);

		taskDesignationImpl.resetOriginalValues();

		return taskDesignationImpl;
	}

	public long taskDesignationId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long taskId;
	public long designationId;
}
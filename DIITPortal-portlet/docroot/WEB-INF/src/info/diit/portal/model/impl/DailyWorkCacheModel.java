/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.DailyWork;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing DailyWork in entity cache.
 *
 * @author mohammad
 * @see DailyWork
 * @generated
 */
public class DailyWorkCacheModel implements CacheModel<DailyWork>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{dailyWorkId=");
		sb.append(dailyWorkId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", employeeId=");
		sb.append(employeeId);
		sb.append(", worKingDay=");
		sb.append(worKingDay);
		sb.append(", taskId=");
		sb.append(taskId);
		sb.append(", time=");
		sb.append(time);
		sb.append(", note=");
		sb.append(note);
		sb.append("}");

		return sb.toString();
	}

	public DailyWork toEntityModel() {
		DailyWorkImpl dailyWorkImpl = new DailyWorkImpl();

		dailyWorkImpl.setDailyWorkId(dailyWorkId);
		dailyWorkImpl.setCompanyId(companyId);
		dailyWorkImpl.setUserId(userId);

		if (userName == null) {
			dailyWorkImpl.setUserName(StringPool.BLANK);
		}
		else {
			dailyWorkImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			dailyWorkImpl.setCreateDate(null);
		}
		else {
			dailyWorkImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			dailyWorkImpl.setModifiedDate(null);
		}
		else {
			dailyWorkImpl.setModifiedDate(new Date(modifiedDate));
		}

		dailyWorkImpl.setEmployeeId(employeeId);

		if (worKingDay == Long.MIN_VALUE) {
			dailyWorkImpl.setWorKingDay(null);
		}
		else {
			dailyWorkImpl.setWorKingDay(new Date(worKingDay));
		}

		dailyWorkImpl.setTaskId(taskId);
		dailyWorkImpl.setTime(time);

		if (note == null) {
			dailyWorkImpl.setNote(StringPool.BLANK);
		}
		else {
			dailyWorkImpl.setNote(note);
		}

		dailyWorkImpl.resetOriginalValues();

		return dailyWorkImpl;
	}

	public long dailyWorkId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long employeeId;
	public long worKingDay;
	public long taskId;
	public int time;
	public String note;
}
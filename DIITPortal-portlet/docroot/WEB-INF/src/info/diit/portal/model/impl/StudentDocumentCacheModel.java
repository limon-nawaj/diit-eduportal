/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.StudentDocument;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing StudentDocument in entity cache.
 *
 * @author mohammad
 * @see StudentDocument
 * @generated
 */
public class StudentDocumentCacheModel implements CacheModel<StudentDocument>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{documentId=");
		sb.append(documentId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", type=");
		sb.append(type);
		sb.append(", documentData=");
		sb.append(documentData);
		sb.append(", description=");
		sb.append(description);
		sb.append(", studentId=");
		sb.append(studentId);
		sb.append(", fileEntryId=");
		sb.append(fileEntryId);
		sb.append("}");

		return sb.toString();
	}

	public StudentDocument toEntityModel() {
		StudentDocumentImpl studentDocumentImpl = new StudentDocumentImpl();

		studentDocumentImpl.setDocumentId(documentId);
		studentDocumentImpl.setCompanyId(companyId);
		studentDocumentImpl.setUserId(userId);

		if (userName == null) {
			studentDocumentImpl.setUserName(StringPool.BLANK);
		}
		else {
			studentDocumentImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			studentDocumentImpl.setCreateDate(null);
		}
		else {
			studentDocumentImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			studentDocumentImpl.setModifiedDate(null);
		}
		else {
			studentDocumentImpl.setModifiedDate(new Date(modifiedDate));
		}

		studentDocumentImpl.setOrganizationId(organizationId);

		if (type == null) {
			studentDocumentImpl.setType(StringPool.BLANK);
		}
		else {
			studentDocumentImpl.setType(type);
		}

		studentDocumentImpl.setDocumentData(documentData);

		if (description == null) {
			studentDocumentImpl.setDescription(StringPool.BLANK);
		}
		else {
			studentDocumentImpl.setDescription(description);
		}

		studentDocumentImpl.setStudentId(studentId);
		studentDocumentImpl.setFileEntryId(fileEntryId);

		studentDocumentImpl.resetOriginalValues();

		return studentDocumentImpl;
	}

	public long documentId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long organizationId;
	public String type;
	public long documentData;
	public String description;
	public long studentId;
	public long fileEntryId;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.BatchSubject;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing BatchSubject in entity cache.
 *
 * @author mohammad
 * @see BatchSubject
 * @generated
 */
public class BatchSubjectCacheModel implements CacheModel<BatchSubject>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{batchSubjectId=");
		sb.append(batchSubjectId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", courseId=");
		sb.append(courseId);
		sb.append(", batchId=");
		sb.append(batchId);
		sb.append(", subjectId=");
		sb.append(subjectId);
		sb.append(", teacherId=");
		sb.append(teacherId);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", hours=");
		sb.append(hours);
		sb.append("}");

		return sb.toString();
	}

	public BatchSubject toEntityModel() {
		BatchSubjectImpl batchSubjectImpl = new BatchSubjectImpl();

		batchSubjectImpl.setBatchSubjectId(batchSubjectId);
		batchSubjectImpl.setCompanyId(companyId);
		batchSubjectImpl.setOrganizationId(organizationId);
		batchSubjectImpl.setUserId(userId);

		if (userName == null) {
			batchSubjectImpl.setUserName(StringPool.BLANK);
		}
		else {
			batchSubjectImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			batchSubjectImpl.setCreateDate(null);
		}
		else {
			batchSubjectImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			batchSubjectImpl.setModifiedDate(null);
		}
		else {
			batchSubjectImpl.setModifiedDate(new Date(modifiedDate));
		}

		batchSubjectImpl.setCourseId(courseId);
		batchSubjectImpl.setBatchId(batchId);
		batchSubjectImpl.setSubjectId(subjectId);
		batchSubjectImpl.setTeacherId(teacherId);

		if (startDate == Long.MIN_VALUE) {
			batchSubjectImpl.setStartDate(null);
		}
		else {
			batchSubjectImpl.setStartDate(new Date(startDate));
		}

		batchSubjectImpl.setHours(hours);

		batchSubjectImpl.resetOriginalValues();

		return batchSubjectImpl;
	}

	public long batchSubjectId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long courseId;
	public long batchId;
	public long subjectId;
	public long teacherId;
	public long startDate;
	public int hours;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.ClassRoutineEventBatch;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing ClassRoutineEventBatch in entity cache.
 *
 * @author mohammad
 * @see ClassRoutineEventBatch
 * @generated
 */
public class ClassRoutineEventBatchCacheModel implements CacheModel<ClassRoutineEventBatch>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{classRoutineEventBatchId=");
		sb.append(classRoutineEventBatchId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", classRoutineEventId=");
		sb.append(classRoutineEventId);
		sb.append(", batchId=");
		sb.append(batchId);
		sb.append("}");

		return sb.toString();
	}

	public ClassRoutineEventBatch toEntityModel() {
		ClassRoutineEventBatchImpl classRoutineEventBatchImpl = new ClassRoutineEventBatchImpl();

		classRoutineEventBatchImpl.setClassRoutineEventBatchId(classRoutineEventBatchId);
		classRoutineEventBatchImpl.setCompanyId(companyId);
		classRoutineEventBatchImpl.setUserId(userId);

		if (userName == null) {
			classRoutineEventBatchImpl.setUserName(StringPool.BLANK);
		}
		else {
			classRoutineEventBatchImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			classRoutineEventBatchImpl.setCreateDate(null);
		}
		else {
			classRoutineEventBatchImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			classRoutineEventBatchImpl.setModifiedDate(null);
		}
		else {
			classRoutineEventBatchImpl.setModifiedDate(new Date(modifiedDate));
		}

		classRoutineEventBatchImpl.setOrganizationId(organizationId);
		classRoutineEventBatchImpl.setClassRoutineEventId(classRoutineEventId);
		classRoutineEventBatchImpl.setBatchId(batchId);

		classRoutineEventBatchImpl.resetOriginalValues();

		return classRoutineEventBatchImpl;
	}

	public long classRoutineEventBatchId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long organizationId;
	public long classRoutineEventId;
	public long batchId;
}
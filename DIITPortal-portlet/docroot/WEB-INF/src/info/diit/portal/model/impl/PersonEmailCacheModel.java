/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.PersonEmail;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing PersonEmail in entity cache.
 *
 * @author mohammad
 * @see PersonEmail
 * @generated
 */
public class PersonEmailCacheModel implements CacheModel<PersonEmail>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{personEmailId=");
		sb.append(personEmailId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", ownerType=");
		sb.append(ownerType);
		sb.append(", personEmail=");
		sb.append(personEmail);
		sb.append(", studentId=");
		sb.append(studentId);
		sb.append("}");

		return sb.toString();
	}

	public PersonEmail toEntityModel() {
		PersonEmailImpl personEmailImpl = new PersonEmailImpl();

		personEmailImpl.setPersonEmailId(personEmailId);
		personEmailImpl.setCompanyId(companyId);
		personEmailImpl.setUserId(userId);

		if (userName == null) {
			personEmailImpl.setUserName(StringPool.BLANK);
		}
		else {
			personEmailImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			personEmailImpl.setCreateDate(null);
		}
		else {
			personEmailImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			personEmailImpl.setModifiedDate(null);
		}
		else {
			personEmailImpl.setModifiedDate(new Date(modifiedDate));
		}

		personEmailImpl.setOrganizationId(organizationId);
		personEmailImpl.setOwnerType(ownerType);

		if (personEmail == null) {
			personEmailImpl.setPersonEmail(StringPool.BLANK);
		}
		else {
			personEmailImpl.setPersonEmail(personEmail);
		}

		personEmailImpl.setStudentId(studentId);

		personEmailImpl.resetOriginalValues();

		return personEmailImpl;
	}

	public long personEmailId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long organizationId;
	public int ownerType;
	public String personEmail;
	public long studentId;
}
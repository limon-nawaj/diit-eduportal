/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.EmployeeMobile;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing EmployeeMobile in entity cache.
 *
 * @author mohammad
 * @see EmployeeMobile
 * @generated
 */
public class EmployeeMobileCacheModel implements CacheModel<EmployeeMobile>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{mobileId=");
		sb.append(mobileId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", employeeId=");
		sb.append(employeeId);
		sb.append(", mobile=");
		sb.append(mobile);
		sb.append(", homePhone=");
		sb.append(homePhone);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	public EmployeeMobile toEntityModel() {
		EmployeeMobileImpl employeeMobileImpl = new EmployeeMobileImpl();

		employeeMobileImpl.setMobileId(mobileId);
		employeeMobileImpl.setCompanyId(companyId);
		employeeMobileImpl.setUserId(userId);

		if (userName == null) {
			employeeMobileImpl.setUserName(StringPool.BLANK);
		}
		else {
			employeeMobileImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			employeeMobileImpl.setCreateDate(null);
		}
		else {
			employeeMobileImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			employeeMobileImpl.setModifiedDate(null);
		}
		else {
			employeeMobileImpl.setModifiedDate(new Date(modifiedDate));
		}

		employeeMobileImpl.setEmployeeId(employeeId);

		if (mobile == null) {
			employeeMobileImpl.setMobile(StringPool.BLANK);
		}
		else {
			employeeMobileImpl.setMobile(mobile);
		}

		if (homePhone == null) {
			employeeMobileImpl.setHomePhone(StringPool.BLANK);
		}
		else {
			employeeMobileImpl.setHomePhone(homePhone);
		}

		employeeMobileImpl.setStatus(status);

		employeeMobileImpl.resetOriginalValues();

		return employeeMobileImpl;
	}

	public long mobileId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long employeeId;
	public String mobile;
	public String homePhone;
	public long status;
}
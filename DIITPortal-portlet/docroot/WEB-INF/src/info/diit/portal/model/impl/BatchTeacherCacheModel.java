/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.BatchTeacher;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing BatchTeacher in entity cache.
 *
 * @author mohammad
 * @see BatchTeacher
 * @generated
 */
public class BatchTeacherCacheModel implements CacheModel<BatchTeacher>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{batchTeacherId=");
		sb.append(batchTeacherId);
		sb.append(", teacherId=");
		sb.append(teacherId);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", endDate=");
		sb.append(endDate);
		sb.append(", batchId=");
		sb.append(batchId);
		sb.append("}");

		return sb.toString();
	}

	public BatchTeacher toEntityModel() {
		BatchTeacherImpl batchTeacherImpl = new BatchTeacherImpl();

		batchTeacherImpl.setBatchTeacherId(batchTeacherId);
		batchTeacherImpl.setTeacherId(teacherId);

		if (startDate == Long.MIN_VALUE) {
			batchTeacherImpl.setStartDate(null);
		}
		else {
			batchTeacherImpl.setStartDate(new Date(startDate));
		}

		if (endDate == Long.MIN_VALUE) {
			batchTeacherImpl.setEndDate(null);
		}
		else {
			batchTeacherImpl.setEndDate(new Date(endDate));
		}

		batchTeacherImpl.setBatchId(batchId);

		batchTeacherImpl.resetOriginalValues();

		return batchTeacherImpl;
	}

	public long batchTeacherId;
	public long teacherId;
	public long startDate;
	public long endDate;
	public long batchId;
}
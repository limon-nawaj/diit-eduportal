/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.EmployeeAttendance;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing EmployeeAttendance in entity cache.
 *
 * @author mohammad
 * @see EmployeeAttendance
 * @generated
 */
public class EmployeeAttendanceCacheModel implements CacheModel<EmployeeAttendance>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{employeeAttendanceId=");
		sb.append(employeeAttendanceId);
		sb.append(", employeeId=");
		sb.append(employeeId);
		sb.append(", expectedStart=");
		sb.append(expectedStart);
		sb.append(", expectedEnd=");
		sb.append(expectedEnd);
		sb.append(", realTime=");
		sb.append(realTime);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	public EmployeeAttendance toEntityModel() {
		EmployeeAttendanceImpl employeeAttendanceImpl = new EmployeeAttendanceImpl();

		employeeAttendanceImpl.setEmployeeAttendanceId(employeeAttendanceId);
		employeeAttendanceImpl.setEmployeeId(employeeId);

		if (expectedStart == Long.MIN_VALUE) {
			employeeAttendanceImpl.setExpectedStart(null);
		}
		else {
			employeeAttendanceImpl.setExpectedStart(new Date(expectedStart));
		}

		if (expectedEnd == Long.MIN_VALUE) {
			employeeAttendanceImpl.setExpectedEnd(null);
		}
		else {
			employeeAttendanceImpl.setExpectedEnd(new Date(expectedEnd));
		}

		if (realTime == Long.MIN_VALUE) {
			employeeAttendanceImpl.setRealTime(null);
		}
		else {
			employeeAttendanceImpl.setRealTime(new Date(realTime));
		}

		employeeAttendanceImpl.setStatus(status);

		employeeAttendanceImpl.resetOriginalValues();

		return employeeAttendanceImpl;
	}

	public long employeeAttendanceId;
	public long employeeId;
	public long expectedStart;
	public long expectedEnd;
	public long realTime;
	public long status;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.CounselingCourseInterest;

import java.io.Serializable;

/**
 * The cache model class for representing CounselingCourseInterest in entity cache.
 *
 * @author mohammad
 * @see CounselingCourseInterest
 * @generated
 */
public class CounselingCourseInterestCacheModel implements CacheModel<CounselingCourseInterest>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{CounselingCourseInterestId=");
		sb.append(CounselingCourseInterestId);
		sb.append(", courseId=");
		sb.append(courseId);
		sb.append(", counselingId=");
		sb.append(counselingId);
		sb.append("}");

		return sb.toString();
	}

	public CounselingCourseInterest toEntityModel() {
		CounselingCourseInterestImpl counselingCourseInterestImpl = new CounselingCourseInterestImpl();

		counselingCourseInterestImpl.setCounselingCourseInterestId(CounselingCourseInterestId);
		counselingCourseInterestImpl.setCourseId(courseId);
		counselingCourseInterestImpl.setCounselingId(counselingId);

		counselingCourseInterestImpl.resetOriginalValues();

		return counselingCourseInterestImpl;
	}

	public long CounselingCourseInterestId;
	public long courseId;
	public long counselingId;
}
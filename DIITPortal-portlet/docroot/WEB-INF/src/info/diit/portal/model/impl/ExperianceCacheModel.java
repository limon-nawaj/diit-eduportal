/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.Experiance;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Experiance in entity cache.
 *
 * @author mohammad
 * @see Experiance
 * @generated
 */
public class ExperianceCacheModel implements CacheModel<Experiance>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{experianceId=");
		sb.append(experianceId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", organization=");
		sb.append(organization);
		sb.append(", designation=");
		sb.append(designation);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", endDate=");
		sb.append(endDate);
		sb.append(", currentStatus=");
		sb.append(currentStatus);
		sb.append(", studentId=");
		sb.append(studentId);
		sb.append("}");

		return sb.toString();
	}

	public Experiance toEntityModel() {
		ExperianceImpl experianceImpl = new ExperianceImpl();

		experianceImpl.setExperianceId(experianceId);
		experianceImpl.setCompanyId(companyId);
		experianceImpl.setUserId(userId);

		if (userName == null) {
			experianceImpl.setUserName(StringPool.BLANK);
		}
		else {
			experianceImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			experianceImpl.setCreateDate(null);
		}
		else {
			experianceImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			experianceImpl.setModifiedDate(null);
		}
		else {
			experianceImpl.setModifiedDate(new Date(modifiedDate));
		}

		experianceImpl.setOrganizationId(organizationId);

		if (organization == null) {
			experianceImpl.setOrganization(StringPool.BLANK);
		}
		else {
			experianceImpl.setOrganization(organization);
		}

		if (designation == null) {
			experianceImpl.setDesignation(StringPool.BLANK);
		}
		else {
			experianceImpl.setDesignation(designation);
		}

		if (startDate == Long.MIN_VALUE) {
			experianceImpl.setStartDate(null);
		}
		else {
			experianceImpl.setStartDate(new Date(startDate));
		}

		if (endDate == Long.MIN_VALUE) {
			experianceImpl.setEndDate(null);
		}
		else {
			experianceImpl.setEndDate(new Date(endDate));
		}

		experianceImpl.setCurrentStatus(currentStatus);
		experianceImpl.setStudentId(studentId);

		experianceImpl.resetOriginalValues();

		return experianceImpl;
	}

	public long experianceId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long organizationId;
	public String organization;
	public String designation;
	public long startDate;
	public long endDate;
	public int currentStatus;
	public long studentId;
}
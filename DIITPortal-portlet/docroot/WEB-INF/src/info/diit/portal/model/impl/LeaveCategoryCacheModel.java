/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.LeaveCategory;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing LeaveCategory in entity cache.
 *
 * @author mohammad
 * @see LeaveCategory
 * @generated
 */
public class LeaveCategoryCacheModel implements CacheModel<LeaveCategory>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{leaveCategoryId=");
		sb.append(leaveCategoryId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", title=");
		sb.append(title);
		sb.append(", totalLeave=");
		sb.append(totalLeave);
		sb.append("}");

		return sb.toString();
	}

	public LeaveCategory toEntityModel() {
		LeaveCategoryImpl leaveCategoryImpl = new LeaveCategoryImpl();

		leaveCategoryImpl.setLeaveCategoryId(leaveCategoryId);
		leaveCategoryImpl.setCompanyId(companyId);
		leaveCategoryImpl.setOrganizationId(organizationId);
		leaveCategoryImpl.setUserId(userId);

		if (userName == null) {
			leaveCategoryImpl.setUserName(StringPool.BLANK);
		}
		else {
			leaveCategoryImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			leaveCategoryImpl.setCreateDate(null);
		}
		else {
			leaveCategoryImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			leaveCategoryImpl.setModifiedDate(null);
		}
		else {
			leaveCategoryImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (title == null) {
			leaveCategoryImpl.setTitle(StringPool.BLANK);
		}
		else {
			leaveCategoryImpl.setTitle(title);
		}

		leaveCategoryImpl.setTotalLeave(totalLeave);

		leaveCategoryImpl.resetOriginalValues();

		return leaveCategoryImpl;
	}

	public long leaveCategoryId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String title;
	public int totalLeave;
}
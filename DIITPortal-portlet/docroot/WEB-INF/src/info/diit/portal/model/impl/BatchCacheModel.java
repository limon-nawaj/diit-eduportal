/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.Batch;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Batch in entity cache.
 *
 * @author mohammad
 * @see Batch
 * @generated
 */
public class BatchCacheModel implements CacheModel<Batch>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{batchId=");
		sb.append(batchId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", batchName=");
		sb.append(batchName);
		sb.append(", courseId=");
		sb.append(courseId);
		sb.append(", sessionId=");
		sb.append(sessionId);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", endDate=");
		sb.append(endDate);
		sb.append(", batchTeacherId=");
		sb.append(batchTeacherId);
		sb.append(", status=");
		sb.append(status);
		sb.append(", note=");
		sb.append(note);
		sb.append("}");

		return sb.toString();
	}

	public Batch toEntityModel() {
		BatchImpl batchImpl = new BatchImpl();

		batchImpl.setBatchId(batchId);
		batchImpl.setCompanyId(companyId);
		batchImpl.setOrganizationId(organizationId);
		batchImpl.setUserId(userId);

		if (userName == null) {
			batchImpl.setUserName(StringPool.BLANK);
		}
		else {
			batchImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			batchImpl.setCreateDate(null);
		}
		else {
			batchImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			batchImpl.setModifiedDate(null);
		}
		else {
			batchImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (batchName == null) {
			batchImpl.setBatchName(StringPool.BLANK);
		}
		else {
			batchImpl.setBatchName(batchName);
		}

		batchImpl.setCourseId(courseId);
		batchImpl.setSessionId(sessionId);

		if (startDate == Long.MIN_VALUE) {
			batchImpl.setStartDate(null);
		}
		else {
			batchImpl.setStartDate(new Date(startDate));
		}

		if (endDate == Long.MIN_VALUE) {
			batchImpl.setEndDate(null);
		}
		else {
			batchImpl.setEndDate(new Date(endDate));
		}

		batchImpl.setBatchTeacherId(batchTeacherId);
		batchImpl.setStatus(status);

		if (note == null) {
			batchImpl.setNote(StringPool.BLANK);
		}
		else {
			batchImpl.setNote(note);
		}

		batchImpl.resetOriginalValues();

		return batchImpl;
	}

	public long batchId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String batchName;
	public long courseId;
	public long sessionId;
	public long startDate;
	public long endDate;
	public long batchTeacherId;
	public int status;
	public String note;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.Assessment;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Assessment in entity cache.
 *
 * @author mohammad
 * @see Assessment
 * @generated
 */
public class AssessmentCacheModel implements CacheModel<Assessment>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{assessmentId=");
		sb.append(assessmentId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", assessmentDate=");
		sb.append(assessmentDate);
		sb.append(", resultDate=");
		sb.append(resultDate);
		sb.append(", totalMark=");
		sb.append(totalMark);
		sb.append(", assessmentTypeId=");
		sb.append(assessmentTypeId);
		sb.append(", batchId=");
		sb.append(batchId);
		sb.append(", subjectId=");
		sb.append(subjectId);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	public Assessment toEntityModel() {
		AssessmentImpl assessmentImpl = new AssessmentImpl();

		assessmentImpl.setAssessmentId(assessmentId);
		assessmentImpl.setCompanyId(companyId);
		assessmentImpl.setOrganizationId(organizationId);
		assessmentImpl.setUserId(userId);

		if (userName == null) {
			assessmentImpl.setUserName(StringPool.BLANK);
		}
		else {
			assessmentImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			assessmentImpl.setCreateDate(null);
		}
		else {
			assessmentImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			assessmentImpl.setModifiedDate(null);
		}
		else {
			assessmentImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (assessmentDate == Long.MIN_VALUE) {
			assessmentImpl.setAssessmentDate(null);
		}
		else {
			assessmentImpl.setAssessmentDate(new Date(assessmentDate));
		}

		if (resultDate == Long.MIN_VALUE) {
			assessmentImpl.setResultDate(null);
		}
		else {
			assessmentImpl.setResultDate(new Date(resultDate));
		}

		assessmentImpl.setTotalMark(totalMark);
		assessmentImpl.setAssessmentTypeId(assessmentTypeId);
		assessmentImpl.setBatchId(batchId);
		assessmentImpl.setSubjectId(subjectId);
		assessmentImpl.setStatus(status);

		assessmentImpl.resetOriginalValues();

		return assessmentImpl;
	}

	public long assessmentId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long assessmentDate;
	public long resultDate;
	public double totalMark;
	public long assessmentTypeId;
	public long batchId;
	public long subjectId;
	public long status;
}
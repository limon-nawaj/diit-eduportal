/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.CourseSession;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing CourseSession in entity cache.
 *
 * @author mohammad
 * @see CourseSession
 * @generated
 */
public class CourseSessionCacheModel implements CacheModel<CourseSession>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{sessionId=");
		sb.append(sessionId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", sessionName=");
		sb.append(sessionName);
		sb.append(", courseId=");
		sb.append(courseId);
		sb.append("}");

		return sb.toString();
	}

	public CourseSession toEntityModel() {
		CourseSessionImpl courseSessionImpl = new CourseSessionImpl();

		courseSessionImpl.setSessionId(sessionId);
		courseSessionImpl.setCompanyId(companyId);
		courseSessionImpl.setOrganizationId(organizationId);
		courseSessionImpl.setUserId(userId);

		if (userName == null) {
			courseSessionImpl.setUserName(StringPool.BLANK);
		}
		else {
			courseSessionImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			courseSessionImpl.setCreateDate(null);
		}
		else {
			courseSessionImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			courseSessionImpl.setModifiedDate(null);
		}
		else {
			courseSessionImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (sessionName == null) {
			courseSessionImpl.setSessionName(StringPool.BLANK);
		}
		else {
			courseSessionImpl.setSessionName(sessionName);
		}

		courseSessionImpl.setCourseId(courseId);

		courseSessionImpl.resetOriginalValues();

		return courseSessionImpl;
	}

	public long sessionId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String sessionName;
	public long courseId;
}
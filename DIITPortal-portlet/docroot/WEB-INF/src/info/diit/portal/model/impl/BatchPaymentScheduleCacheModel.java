/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.BatchPaymentSchedule;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing BatchPaymentSchedule in entity cache.
 *
 * @author mohammad
 * @see BatchPaymentSchedule
 * @generated
 */
public class BatchPaymentScheduleCacheModel implements CacheModel<BatchPaymentSchedule>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{batchPaymentScheduleId=");
		sb.append(batchPaymentScheduleId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", batchId=");
		sb.append(batchId);
		sb.append(", scheduleDate=");
		sb.append(scheduleDate);
		sb.append(", feeTypeId=");
		sb.append(feeTypeId);
		sb.append(", amount=");
		sb.append(amount);
		sb.append("}");

		return sb.toString();
	}

	public BatchPaymentSchedule toEntityModel() {
		BatchPaymentScheduleImpl batchPaymentScheduleImpl = new BatchPaymentScheduleImpl();

		batchPaymentScheduleImpl.setBatchPaymentScheduleId(batchPaymentScheduleId);
		batchPaymentScheduleImpl.setCompanyId(companyId);
		batchPaymentScheduleImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			batchPaymentScheduleImpl.setCreateDate(null);
		}
		else {
			batchPaymentScheduleImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			batchPaymentScheduleImpl.setModifiedDate(null);
		}
		else {
			batchPaymentScheduleImpl.setModifiedDate(new Date(modifiedDate));
		}

		batchPaymentScheduleImpl.setBatchId(batchId);

		if (scheduleDate == Long.MIN_VALUE) {
			batchPaymentScheduleImpl.setScheduleDate(null);
		}
		else {
			batchPaymentScheduleImpl.setScheduleDate(new Date(scheduleDate));
		}

		batchPaymentScheduleImpl.setFeeTypeId(feeTypeId);
		batchPaymentScheduleImpl.setAmount(amount);

		batchPaymentScheduleImpl.resetOriginalValues();

		return batchPaymentScheduleImpl;
	}

	public long batchPaymentScheduleId;
	public long companyId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public long batchId;
	public long scheduleDate;
	public long feeTypeId;
	public double amount;
}
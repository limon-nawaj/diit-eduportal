/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.LeaveDayDetails;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing LeaveDayDetails in entity cache.
 *
 * @author mohammad
 * @see LeaveDayDetails
 * @generated
 */
public class LeaveDayDetailsCacheModel implements CacheModel<LeaveDayDetails>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{leaveDayDetailsId=");
		sb.append(leaveDayDetailsId);
		sb.append(", leaveId=");
		sb.append(leaveId);
		sb.append(", leaveDate=");
		sb.append(leaveDate);
		sb.append(", day=");
		sb.append(day);
		sb.append("}");

		return sb.toString();
	}

	public LeaveDayDetails toEntityModel() {
		LeaveDayDetailsImpl leaveDayDetailsImpl = new LeaveDayDetailsImpl();

		leaveDayDetailsImpl.setLeaveDayDetailsId(leaveDayDetailsId);
		leaveDayDetailsImpl.setLeaveId(leaveId);

		if (leaveDate == Long.MIN_VALUE) {
			leaveDayDetailsImpl.setLeaveDate(null);
		}
		else {
			leaveDayDetailsImpl.setLeaveDate(new Date(leaveDate));
		}

		leaveDayDetailsImpl.setDay(day);

		leaveDayDetailsImpl.resetOriginalValues();

		return leaveDayDetailsImpl;
	}

	public long leaveDayDetailsId;
	public long leaveId;
	public long leaveDate;
	public double day;
}
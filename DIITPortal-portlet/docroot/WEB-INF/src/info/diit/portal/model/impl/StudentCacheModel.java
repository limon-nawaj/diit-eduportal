/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.Student;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Student in entity cache.
 *
 * @author mohammad
 * @see Student
 * @generated
 */
public class StudentCacheModel implements CacheModel<Student>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(41);

		sb.append("{studentId=");
		sb.append(studentId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", fatherName=");
		sb.append(fatherName);
		sb.append(", motherName=");
		sb.append(motherName);
		sb.append(", presentAddress=");
		sb.append(presentAddress);
		sb.append(", permanentAddress=");
		sb.append(permanentAddress);
		sb.append(", homePhone=");
		sb.append(homePhone);
		sb.append(", dateOfBirth=");
		sb.append(dateOfBirth);
		sb.append(", gender=");
		sb.append(gender);
		sb.append(", religion=");
		sb.append(religion);
		sb.append(", nationality=");
		sb.append(nationality);
		sb.append(", photo=");
		sb.append(photo);
		sb.append(", status=");
		sb.append(status);
		sb.append(", studentUserID=");
		sb.append(studentUserID);
		sb.append("}");

		return sb.toString();
	}

	public Student toEntityModel() {
		StudentImpl studentImpl = new StudentImpl();

		studentImpl.setStudentId(studentId);
		studentImpl.setCompanyId(companyId);
		studentImpl.setUserId(userId);

		if (userName == null) {
			studentImpl.setUserName(StringPool.BLANK);
		}
		else {
			studentImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			studentImpl.setCreateDate(null);
		}
		else {
			studentImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			studentImpl.setModifiedDate(null);
		}
		else {
			studentImpl.setModifiedDate(new Date(modifiedDate));
		}

		studentImpl.setOrganizationId(organizationId);

		if (name == null) {
			studentImpl.setName(StringPool.BLANK);
		}
		else {
			studentImpl.setName(name);
		}

		if (fatherName == null) {
			studentImpl.setFatherName(StringPool.BLANK);
		}
		else {
			studentImpl.setFatherName(fatherName);
		}

		if (motherName == null) {
			studentImpl.setMotherName(StringPool.BLANK);
		}
		else {
			studentImpl.setMotherName(motherName);
		}

		if (presentAddress == null) {
			studentImpl.setPresentAddress(StringPool.BLANK);
		}
		else {
			studentImpl.setPresentAddress(presentAddress);
		}

		if (permanentAddress == null) {
			studentImpl.setPermanentAddress(StringPool.BLANK);
		}
		else {
			studentImpl.setPermanentAddress(permanentAddress);
		}

		if (homePhone == null) {
			studentImpl.setHomePhone(StringPool.BLANK);
		}
		else {
			studentImpl.setHomePhone(homePhone);
		}

		if (dateOfBirth == Long.MIN_VALUE) {
			studentImpl.setDateOfBirth(null);
		}
		else {
			studentImpl.setDateOfBirth(new Date(dateOfBirth));
		}

		if (gender == null) {
			studentImpl.setGender(StringPool.BLANK);
		}
		else {
			studentImpl.setGender(gender);
		}

		if (religion == null) {
			studentImpl.setReligion(StringPool.BLANK);
		}
		else {
			studentImpl.setReligion(religion);
		}

		if (nationality == null) {
			studentImpl.setNationality(StringPool.BLANK);
		}
		else {
			studentImpl.setNationality(nationality);
		}

		studentImpl.setPhoto(photo);
		studentImpl.setStatus(status);
		studentImpl.setStudentUserID(studentUserID);

		studentImpl.resetOriginalValues();

		return studentImpl;
	}

	public long studentId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long organizationId;
	public String name;
	public String fatherName;
	public String motherName;
	public String presentAddress;
	public String permanentAddress;
	public String homePhone;
	public long dateOfBirth;
	public String gender;
	public String religion;
	public String nationality;
	public long photo;
	public int status;
	public long studentUserID;
}
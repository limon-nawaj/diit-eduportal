/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.ClassRoutineEvent;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing ClassRoutineEvent in entity cache.
 *
 * @author mohammad
 * @see ClassRoutineEvent
 * @generated
 */
public class ClassRoutineEventCacheModel implements CacheModel<ClassRoutineEvent>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{classRoutineEventId=");
		sb.append(classRoutineEventId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", subjectId=");
		sb.append(subjectId);
		sb.append(", day=");
		sb.append(day);
		sb.append(", startTime=");
		sb.append(startTime);
		sb.append(", endTime=");
		sb.append(endTime);
		sb.append(", roomId=");
		sb.append(roomId);
		sb.append("}");

		return sb.toString();
	}

	public ClassRoutineEvent toEntityModel() {
		ClassRoutineEventImpl classRoutineEventImpl = new ClassRoutineEventImpl();

		classRoutineEventImpl.setClassRoutineEventId(classRoutineEventId);
		classRoutineEventImpl.setCompanyId(companyId);
		classRoutineEventImpl.setUserId(userId);

		if (userName == null) {
			classRoutineEventImpl.setUserName(StringPool.BLANK);
		}
		else {
			classRoutineEventImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			classRoutineEventImpl.setCreateDate(null);
		}
		else {
			classRoutineEventImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			classRoutineEventImpl.setModifiedDate(null);
		}
		else {
			classRoutineEventImpl.setModifiedDate(new Date(modifiedDate));
		}

		classRoutineEventImpl.setOrganizationId(organizationId);
		classRoutineEventImpl.setSubjectId(subjectId);
		classRoutineEventImpl.setDay(day);

		if (startTime == Long.MIN_VALUE) {
			classRoutineEventImpl.setStartTime(null);
		}
		else {
			classRoutineEventImpl.setStartTime(new Date(startTime));
		}

		if (endTime == Long.MIN_VALUE) {
			classRoutineEventImpl.setEndTime(null);
		}
		else {
			classRoutineEventImpl.setEndTime(new Date(endTime));
		}

		classRoutineEventImpl.setRoomId(roomId);

		classRoutineEventImpl.resetOriginalValues();

		return classRoutineEventImpl;
	}

	public long classRoutineEventId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long organizationId;
	public long subjectId;
	public int day;
	public long startTime;
	public long endTime;
	public long roomId;
}
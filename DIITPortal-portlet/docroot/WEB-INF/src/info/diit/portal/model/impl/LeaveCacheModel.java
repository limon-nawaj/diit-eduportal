/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.Leave;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Leave in entity cache.
 *
 * @author mohammad
 * @see Leave
 * @generated
 */
public class LeaveCacheModel implements CacheModel<Leave>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(47);

		sb.append("{leaveId=");
		sb.append(leaveId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", applicationDate=");
		sb.append(applicationDate);
		sb.append(", employee=");
		sb.append(employee);
		sb.append(", responsibleEmployee=");
		sb.append(responsibleEmployee);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", endDate=");
		sb.append(endDate);
		sb.append(", numberOfDay=");
		sb.append(numberOfDay);
		sb.append(", phoneNumber=");
		sb.append(phoneNumber);
		sb.append(", causeOfLeave=");
		sb.append(causeOfLeave);
		sb.append(", whereEnjoy=");
		sb.append(whereEnjoy);
		sb.append(", firstRecommendation=");
		sb.append(firstRecommendation);
		sb.append(", secondRecommendation=");
		sb.append(secondRecommendation);
		sb.append(", leaveCategory=");
		sb.append(leaveCategory);
		sb.append(", responsibleEmployeeStatus=");
		sb.append(responsibleEmployeeStatus);
		sb.append(", applicationStatus=");
		sb.append(applicationStatus);
		sb.append(", comments=");
		sb.append(comments);
		sb.append(", complementedBy=");
		sb.append(complementedBy);
		sb.append("}");

		return sb.toString();
	}

	public Leave toEntityModel() {
		LeaveImpl leaveImpl = new LeaveImpl();

		leaveImpl.setLeaveId(leaveId);
		leaveImpl.setCompanyId(companyId);
		leaveImpl.setOrganizationId(organizationId);
		leaveImpl.setUserId(userId);

		if (userName == null) {
			leaveImpl.setUserName(StringPool.BLANK);
		}
		else {
			leaveImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			leaveImpl.setCreateDate(null);
		}
		else {
			leaveImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			leaveImpl.setModifiedDate(null);
		}
		else {
			leaveImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (applicationDate == Long.MIN_VALUE) {
			leaveImpl.setApplicationDate(null);
		}
		else {
			leaveImpl.setApplicationDate(new Date(applicationDate));
		}

		leaveImpl.setEmployee(employee);
		leaveImpl.setResponsibleEmployee(responsibleEmployee);

		if (startDate == Long.MIN_VALUE) {
			leaveImpl.setStartDate(null);
		}
		else {
			leaveImpl.setStartDate(new Date(startDate));
		}

		if (endDate == Long.MIN_VALUE) {
			leaveImpl.setEndDate(null);
		}
		else {
			leaveImpl.setEndDate(new Date(endDate));
		}

		leaveImpl.setNumberOfDay(numberOfDay);

		if (phoneNumber == null) {
			leaveImpl.setPhoneNumber(StringPool.BLANK);
		}
		else {
			leaveImpl.setPhoneNumber(phoneNumber);
		}

		if (causeOfLeave == null) {
			leaveImpl.setCauseOfLeave(StringPool.BLANK);
		}
		else {
			leaveImpl.setCauseOfLeave(causeOfLeave);
		}

		if (whereEnjoy == null) {
			leaveImpl.setWhereEnjoy(StringPool.BLANK);
		}
		else {
			leaveImpl.setWhereEnjoy(whereEnjoy);
		}

		leaveImpl.setFirstRecommendation(firstRecommendation);
		leaveImpl.setSecondRecommendation(secondRecommendation);
		leaveImpl.setLeaveCategory(leaveCategory);
		leaveImpl.setResponsibleEmployeeStatus(responsibleEmployeeStatus);
		leaveImpl.setApplicationStatus(applicationStatus);

		if (comments == null) {
			leaveImpl.setComments(StringPool.BLANK);
		}
		else {
			leaveImpl.setComments(comments);
		}

		leaveImpl.setComplementedBy(complementedBy);

		leaveImpl.resetOriginalValues();

		return leaveImpl;
	}

	public long leaveId;
	public long companyId;
	public long organizationId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long applicationDate;
	public long employee;
	public long responsibleEmployee;
	public long startDate;
	public long endDate;
	public double numberOfDay;
	public String phoneNumber;
	public String causeOfLeave;
	public String whereEnjoy;
	public long firstRecommendation;
	public long secondRecommendation;
	public long leaveCategory;
	public int responsibleEmployeeStatus;
	public int applicationStatus;
	public String comments;
	public long complementedBy;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.AcademicRecord;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing AcademicRecord in entity cache.
 *
 * @author mohammad
 * @see AcademicRecord
 * @generated
 */
public class AcademicRecordCacheModel implements CacheModel<AcademicRecord>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{academicRecordId=");
		sb.append(academicRecordId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", organisationId=");
		sb.append(organisationId);
		sb.append(", degree=");
		sb.append(degree);
		sb.append(", board=");
		sb.append(board);
		sb.append(", year=");
		sb.append(year);
		sb.append(", result=");
		sb.append(result);
		sb.append(", registrationNo=");
		sb.append(registrationNo);
		sb.append(", studentId=");
		sb.append(studentId);
		sb.append("}");

		return sb.toString();
	}

	public AcademicRecord toEntityModel() {
		AcademicRecordImpl academicRecordImpl = new AcademicRecordImpl();

		academicRecordImpl.setAcademicRecordId(academicRecordId);
		academicRecordImpl.setCompanyId(companyId);
		academicRecordImpl.setUserId(userId);

		if (userName == null) {
			academicRecordImpl.setUserName(StringPool.BLANK);
		}
		else {
			academicRecordImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			academicRecordImpl.setCreateDate(null);
		}
		else {
			academicRecordImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			academicRecordImpl.setModifiedDate(null);
		}
		else {
			academicRecordImpl.setModifiedDate(new Date(modifiedDate));
		}

		academicRecordImpl.setOrganisationId(organisationId);

		if (degree == null) {
			academicRecordImpl.setDegree(StringPool.BLANK);
		}
		else {
			academicRecordImpl.setDegree(degree);
		}

		if (board == null) {
			academicRecordImpl.setBoard(StringPool.BLANK);
		}
		else {
			academicRecordImpl.setBoard(board);
		}

		academicRecordImpl.setYear(year);

		if (result == null) {
			academicRecordImpl.setResult(StringPool.BLANK);
		}
		else {
			academicRecordImpl.setResult(result);
		}

		if (registrationNo == null) {
			academicRecordImpl.setRegistrationNo(StringPool.BLANK);
		}
		else {
			academicRecordImpl.setRegistrationNo(registrationNo);
		}

		academicRecordImpl.setStudentId(studentId);

		academicRecordImpl.resetOriginalValues();

		return academicRecordImpl;
	}

	public long academicRecordId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long organisationId;
	public String degree;
	public String board;
	public int year;
	public String result;
	public String registrationNo;
	public long studentId;
}
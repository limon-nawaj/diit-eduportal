/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.util.PortalUtil;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import info.diit.portal.model.CourseSession;
import info.diit.portal.model.CourseSessionModel;

import java.io.Serializable;

import java.sql.Types;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the CourseSession service. Represents a row in the &quot;EduPortal_CourseSession&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link info.diit.portal.model.CourseSessionModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link CourseSessionImpl}.
 * </p>
 *
 * @author mohammad
 * @see CourseSessionImpl
 * @see info.diit.portal.model.CourseSession
 * @see info.diit.portal.model.CourseSessionModel
 * @generated
 */
public class CourseSessionModelImpl extends BaseModelImpl<CourseSession>
	implements CourseSessionModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a course session model instance should use the {@link info.diit.portal.model.CourseSession} interface instead.
	 */
	public static final String TABLE_NAME = "EduPortal_CourseSession";
	public static final Object[][] TABLE_COLUMNS = {
			{ "sessionId", Types.BIGINT },
			{ "companyId", Types.BIGINT },
			{ "organizationId", Types.BIGINT },
			{ "userId", Types.BIGINT },
			{ "userName", Types.VARCHAR },
			{ "createDate", Types.TIMESTAMP },
			{ "modifiedDate", Types.TIMESTAMP },
			{ "sessionName", Types.VARCHAR },
			{ "courseId", Types.BIGINT }
		};
	public static final String TABLE_SQL_CREATE = "create table EduPortal_CourseSession (sessionId LONG not null primary key IDENTITY,companyId LONG,organizationId LONG,userId LONG,userName VARCHAR(75) null,createDate DATE null,modifiedDate DATE null,sessionName VARCHAR(75) null,courseId LONG)";
	public static final String TABLE_SQL_DROP = "drop table EduPortal_CourseSession";
	public static final String ORDER_BY_JPQL = " ORDER BY courseSession.sessionId ASC";
	public static final String ORDER_BY_SQL = " ORDER BY EduPortal_CourseSession.sessionId ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.info.diit.portal.model.CourseSession"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.info.diit.portal.model.CourseSession"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.column.bitmask.enabled.info.diit.portal.model.CourseSession"),
			true);
	public static long COMPANYID_COLUMN_BITMASK = 1L;
	public static long COURSEID_COLUMN_BITMASK = 2L;
	public static long ORGANIZATIONID_COLUMN_BITMASK = 4L;
	public static long SESSIONNAME_COLUMN_BITMASK = 8L;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.info.diit.portal.model.CourseSession"));

	public CourseSessionModelImpl() {
	}

	public long getPrimaryKey() {
		return _sessionId;
	}

	public void setPrimaryKey(long primaryKey) {
		setSessionId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_sessionId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	public Class<?> getModelClass() {
		return CourseSession.class;
	}

	public String getModelClassName() {
		return CourseSession.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("sessionId", getSessionId());
		attributes.put("companyId", getCompanyId());
		attributes.put("organizationId", getOrganizationId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("sessionName", getSessionName());
		attributes.put("courseId", getCourseId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long sessionId = (Long)attributes.get("sessionId");

		if (sessionId != null) {
			setSessionId(sessionId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long organizationId = (Long)attributes.get("organizationId");

		if (organizationId != null) {
			setOrganizationId(organizationId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String sessionName = (String)attributes.get("sessionName");

		if (sessionName != null) {
			setSessionName(sessionName);
		}

		Long courseId = (Long)attributes.get("courseId");

		if (courseId != null) {
			setCourseId(courseId);
		}
	}

	public long getSessionId() {
		return _sessionId;
	}

	public void setSessionId(long sessionId) {
		_columnBitmask = -1L;

		_sessionId = sessionId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_columnBitmask |= COMPANYID_COLUMN_BITMASK;

		if (!_setOriginalCompanyId) {
			_setOriginalCompanyId = true;

			_originalCompanyId = _companyId;
		}

		_companyId = companyId;
	}

	public long getOriginalCompanyId() {
		return _originalCompanyId;
	}

	public long getOrganizationId() {
		return _organizationId;
	}

	public void setOrganizationId(long organizationId) {
		_columnBitmask |= ORGANIZATIONID_COLUMN_BITMASK;

		if (!_setOriginalOrganizationId) {
			_setOriginalOrganizationId = true;

			_originalOrganizationId = _organizationId;
		}

		_organizationId = organizationId;
	}

	public long getOriginalOrganizationId() {
		return _originalOrganizationId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public String getUserName() {
		if (_userName == null) {
			return StringPool.BLANK;
		}
		else {
			return _userName;
		}
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getSessionName() {
		if (_sessionName == null) {
			return StringPool.BLANK;
		}
		else {
			return _sessionName;
		}
	}

	public void setSessionName(String sessionName) {
		_columnBitmask |= SESSIONNAME_COLUMN_BITMASK;

		if (_originalSessionName == null) {
			_originalSessionName = _sessionName;
		}

		_sessionName = sessionName;
	}

	public String getOriginalSessionName() {
		return GetterUtil.getString(_originalSessionName);
	}

	public long getCourseId() {
		return _courseId;
	}

	public void setCourseId(long courseId) {
		_columnBitmask |= COURSEID_COLUMN_BITMASK;

		if (!_setOriginalCourseId) {
			_setOriginalCourseId = true;

			_originalCourseId = _courseId;
		}

		_courseId = courseId;
	}

	public long getOriginalCourseId() {
		return _originalCourseId;
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(getCompanyId(),
			CourseSession.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public CourseSession toEscapedModel() {
		if (_escapedModelProxy == null) {
			_escapedModelProxy = (CourseSession)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelProxyInterfaces,
					new AutoEscapeBeanHandler(this));
		}

		return _escapedModelProxy;
	}

	@Override
	public Object clone() {
		CourseSessionImpl courseSessionImpl = new CourseSessionImpl();

		courseSessionImpl.setSessionId(getSessionId());
		courseSessionImpl.setCompanyId(getCompanyId());
		courseSessionImpl.setOrganizationId(getOrganizationId());
		courseSessionImpl.setUserId(getUserId());
		courseSessionImpl.setUserName(getUserName());
		courseSessionImpl.setCreateDate(getCreateDate());
		courseSessionImpl.setModifiedDate(getModifiedDate());
		courseSessionImpl.setSessionName(getSessionName());
		courseSessionImpl.setCourseId(getCourseId());

		courseSessionImpl.resetOriginalValues();

		return courseSessionImpl;
	}

	public int compareTo(CourseSession courseSession) {
		int value = 0;

		if (getSessionId() < courseSession.getSessionId()) {
			value = -1;
		}
		else if (getSessionId() > courseSession.getSessionId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		CourseSession courseSession = null;

		try {
			courseSession = (CourseSession)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = courseSession.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public void resetOriginalValues() {
		CourseSessionModelImpl courseSessionModelImpl = this;

		courseSessionModelImpl._originalCompanyId = courseSessionModelImpl._companyId;

		courseSessionModelImpl._setOriginalCompanyId = false;

		courseSessionModelImpl._originalOrganizationId = courseSessionModelImpl._organizationId;

		courseSessionModelImpl._setOriginalOrganizationId = false;

		courseSessionModelImpl._originalSessionName = courseSessionModelImpl._sessionName;

		courseSessionModelImpl._originalCourseId = courseSessionModelImpl._courseId;

		courseSessionModelImpl._setOriginalCourseId = false;

		courseSessionModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<CourseSession> toCacheModel() {
		CourseSessionCacheModel courseSessionCacheModel = new CourseSessionCacheModel();

		courseSessionCacheModel.sessionId = getSessionId();

		courseSessionCacheModel.companyId = getCompanyId();

		courseSessionCacheModel.organizationId = getOrganizationId();

		courseSessionCacheModel.userId = getUserId();

		courseSessionCacheModel.userName = getUserName();

		String userName = courseSessionCacheModel.userName;

		if ((userName != null) && (userName.length() == 0)) {
			courseSessionCacheModel.userName = null;
		}

		Date createDate = getCreateDate();

		if (createDate != null) {
			courseSessionCacheModel.createDate = createDate.getTime();
		}
		else {
			courseSessionCacheModel.createDate = Long.MIN_VALUE;
		}

		Date modifiedDate = getModifiedDate();

		if (modifiedDate != null) {
			courseSessionCacheModel.modifiedDate = modifiedDate.getTime();
		}
		else {
			courseSessionCacheModel.modifiedDate = Long.MIN_VALUE;
		}

		courseSessionCacheModel.sessionName = getSessionName();

		String sessionName = courseSessionCacheModel.sessionName;

		if ((sessionName != null) && (sessionName.length() == 0)) {
			courseSessionCacheModel.sessionName = null;
		}

		courseSessionCacheModel.courseId = getCourseId();

		return courseSessionCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{sessionId=");
		sb.append(getSessionId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", organizationId=");
		sb.append(getOrganizationId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", modifiedDate=");
		sb.append(getModifiedDate());
		sb.append(", sessionName=");
		sb.append(getSessionName());
		sb.append(", courseId=");
		sb.append(getCourseId());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(31);

		sb.append("<model><model-name>");
		sb.append("info.diit.portal.model.CourseSession");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>sessionId</column-name><column-value><![CDATA[");
		sb.append(getSessionId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>organizationId</column-name><column-value><![CDATA[");
		sb.append(getOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>modifiedDate</column-name><column-value><![CDATA[");
		sb.append(getModifiedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>sessionName</column-name><column-value><![CDATA[");
		sb.append(getSessionName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>courseId</column-name><column-value><![CDATA[");
		sb.append(getCourseId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = CourseSession.class.getClassLoader();
	private static Class<?>[] _escapedModelProxyInterfaces = new Class[] {
			CourseSession.class
		};
	private long _sessionId;
	private long _companyId;
	private long _originalCompanyId;
	private boolean _setOriginalCompanyId;
	private long _organizationId;
	private long _originalOrganizationId;
	private boolean _setOriginalOrganizationId;
	private long _userId;
	private String _userUuid;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _sessionName;
	private String _originalSessionName;
	private long _courseId;
	private long _originalCourseId;
	private boolean _setOriginalCourseId;
	private long _columnBitmask;
	private CourseSession _escapedModelProxy;
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.CommunicationRecord;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing CommunicationRecord in entity cache.
 *
 * @author mohammad
 * @see CommunicationRecord
 * @generated
 */
public class CommunicationRecordCacheModel implements CacheModel<CommunicationRecord>,
	Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{communicationRecordId=");
		sb.append(communicationRecordId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", communicationBy=");
		sb.append(communicationBy);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", batch=");
		sb.append(batch);
		sb.append(", date=");
		sb.append(date);
		sb.append(", media=");
		sb.append(media);
		sb.append(", subject=");
		sb.append(subject);
		sb.append(", communicationWith=");
		sb.append(communicationWith);
		sb.append(", details=");
		sb.append(details);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	public CommunicationRecord toEntityModel() {
		CommunicationRecordImpl communicationRecordImpl = new CommunicationRecordImpl();

		communicationRecordImpl.setCommunicationRecordId(communicationRecordId);
		communicationRecordImpl.setCompanyId(companyId);
		communicationRecordImpl.setOrganizationId(organizationId);
		communicationRecordImpl.setCommunicationBy(communicationBy);

		if (userName == null) {
			communicationRecordImpl.setUserName(StringPool.BLANK);
		}
		else {
			communicationRecordImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			communicationRecordImpl.setCreateDate(null);
		}
		else {
			communicationRecordImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			communicationRecordImpl.setModifiedDate(null);
		}
		else {
			communicationRecordImpl.setModifiedDate(new Date(modifiedDate));
		}

		communicationRecordImpl.setBatch(batch);

		if (date == Long.MIN_VALUE) {
			communicationRecordImpl.setDate(null);
		}
		else {
			communicationRecordImpl.setDate(new Date(date));
		}

		communicationRecordImpl.setMedia(media);
		communicationRecordImpl.setSubject(subject);
		communicationRecordImpl.setCommunicationWith(communicationWith);

		if (details == null) {
			communicationRecordImpl.setDetails(StringPool.BLANK);
		}
		else {
			communicationRecordImpl.setDetails(details);
		}

		communicationRecordImpl.setStatus(status);

		communicationRecordImpl.resetOriginalValues();

		return communicationRecordImpl;
	}

	public long communicationRecordId;
	public long companyId;
	public long organizationId;
	public long communicationBy;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long batch;
	public long date;
	public long media;
	public long subject;
	public long communicationWith;
	public String details;
	public long status;
}
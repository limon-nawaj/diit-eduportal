/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import info.diit.portal.model.CourseFee;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing CourseFee in entity cache.
 *
 * @author mohammad
 * @see CourseFee
 * @generated
 */
public class CourseFeeCacheModel implements CacheModel<CourseFee>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{courseFeeId=");
		sb.append(courseFeeId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", courseId=");
		sb.append(courseId);
		sb.append(", feeTypeId=");
		sb.append(feeTypeId);
		sb.append(", amount=");
		sb.append(amount);
		sb.append("}");

		return sb.toString();
	}

	public CourseFee toEntityModel() {
		CourseFeeImpl courseFeeImpl = new CourseFeeImpl();

		courseFeeImpl.setCourseFeeId(courseFeeId);
		courseFeeImpl.setCompanyId(companyId);
		courseFeeImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			courseFeeImpl.setCreateDate(null);
		}
		else {
			courseFeeImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			courseFeeImpl.setModifiedDate(null);
		}
		else {
			courseFeeImpl.setModifiedDate(new Date(modifiedDate));
		}

		courseFeeImpl.setCourseId(courseId);
		courseFeeImpl.setFeeTypeId(feeTypeId);
		courseFeeImpl.setAmount(amount);

		courseFeeImpl.resetOriginalValues();

		return courseFeeImpl;
	}

	public long courseFeeId;
	public long companyId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public long courseId;
	public long feeTypeId;
	public double amount;
}
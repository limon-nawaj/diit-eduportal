package info.diit.portal.constant;


public enum CounselingStatus {
	ADMITTED(1,"Admitted"),
	FORM_SOLD(2,"Form Sold"),
	REFERRED(3,"Referred");
	
	private int key;
	private String value;
	
	private CounselingStatus(int key,String value)
	{
		this.key = key;
		this.value = value;
	}
	
	public int getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}
	
	@Override
	public String toString()
	{
		return value;
	}
	
	public static CounselingStatus getStatus(int key)
	{
		
		CounselingStatus statuses[]=CounselingStatus.values();
		
		for(CounselingStatus status:statuses)
		{
			if(status.key==key)
			{
				return status;
			}
		}
		
		return null;
	}
}

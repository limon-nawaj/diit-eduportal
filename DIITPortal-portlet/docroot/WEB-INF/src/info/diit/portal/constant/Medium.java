package info.diit.portal.constant;

public enum Medium {

	PHYSICAL(1,"Physical"),
	PHONE(2,"Phone"),
	EMAIL(3,"Email");
	
	private int key;
	private String value;
	
	private Medium(int key,String value)
	{
		this.key=key;
		this.value=value;
	}
	
	public int getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}

	@Override
	public String toString()
	{
		return value;
	}
	
	public static Medium getMedium(int key)
	{
		
		Medium media[]=Medium.values();
		
		for(Medium medium:media)
		{
			if(medium.key==key)
			{
				return medium;
			}
		}
		
		return null;
	}

}

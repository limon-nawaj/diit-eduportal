package info.diit.portal.constant;


public enum CommunicationWith {

	STUDENT(1, "Student"),
	FATHER(2, "Father"),
	MOTHER(3, "Mother"),
	ELDER_BROTHER(4, "Elder Brother"),
	ELDER_SISTER(5, "Elder Sister"),
	LOCAL_GUARDIAN(6, "Local Guardian"),
	OTHER(7, "Other");
	private int key;
	private String value;
	
	private CommunicationWith(int key, String value){
		this.key = key;
		this.value = value;
	}
	
	public int getKey() {
		return key;
	}
	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return value;
	}
	
	public static CommunicationWith getCommunicationWith(int key){
		CommunicationWith communicationWiths[] = CommunicationWith.values();
		for (CommunicationWith communicationWith : communicationWiths) {
			if (communicationWith.key==key) {
				return communicationWith;
			}
		}
		return null;
	}
}

package info.diit.portal.constant;

import java.io.Serializable;

public enum BatchStatus implements Serializable {


	RUNNING(1,"Running"),	
	PENDING(2,"Pending"),
	COMPLETED(3,"Completed"),
	STOPPED(4,"Stopped"),
	TRANSFERED(5,"Transfered"),
	DROPPEDOUT(6,"Dropped Out");
	
	private int key;
	private String value;
	
	private BatchStatus(int key,String value)
	{
		this.key = key;
		this.value = value;
	}
	
	@Override
	public String toString()
	{
		return value;
	}
	
	public int getKey(){
		return key;
	}
	
	public String getValue(){
		return value;
	}


	public static BatchStatus getStatus(int key)
	{		
		BatchStatus statuses[]=BatchStatus.values();
		
		for(BatchStatus status:statuses)
		{
			if(status.key==key)
			{
				return status;
			}
		}		
		return null;
	}
}
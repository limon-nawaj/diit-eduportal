package info.diit.portal.constant;

public class StudentConstant {
	public final static String COLUMN_COURSE 						= "courseName";
	public final static String COLUMN_BATCH_NAME 					= "batchName";
	public final static String COLUMN_STUDENT_ID 					= "studentId";
	public final static String COLUMN_STUDENT_NAME					= "studentName";
	public final static int    COLUMN_STUDENT_CONTACT 				= 0;
	public final static int    COLUMN_FATHER_CONTACT	    		= 1;
	public final static int    COLUMN_MOTHER_CONTACT	    		= 2;
	public final static int    COLUMN_GARDIAN_CONTACT	    		= 3;
	public final static int    COLUMN_HOME_CONTACT	    			= 4;
	public final static String DATE_FORMAT 							= "dd/MM/yyyy";
	
	public final static String RECORD_DEGREE 						= "degree";
	public final static String RECORD_BOARD 						= "board";
	public final static String RECORD_YEAR 							= "year";
	public final static String RECORD_RESULT 						= "result";
	public final static String RECORD_REGISTRATION_NO 				= "registrationNo";
	
	//Experiance 
	
	//public final static String EXPERIANCE_RECORD_EXPERIANCEID = "experianceId";
	public final static String EXPERIANCE_RECORD_ORGANIZATION 		= "organization";
	public final static String EXPERIANCE_RECORD_DESINATION 		= "designation";
	public final static String EXPERIANCE_RECORD_STARTDATE 			= "startDate";
	public final static String EXPERIANCE_RECORD_ENDDATE 			= "endDate";
	public final static String EXPERIANCE_RECORD_CURRENTSTATUS 		= "currentStatus";
	
	public final static int  BATCH_STATUS_RUNNING					=1;
	public final static int  BATCH_STATUS_PENDING					=2;
	public final static int  BATCH_STATUS_COMPLETED					=3;
	public final static int  BATCH_STATUS_STOPPED					=4;
	
	public final static String BATCH_RCORD_BATCHSESSION				="batchSession";
	public final static String BATCH_RCORD_BATCHSTARTDATE			="batchStartDate";
	public final static String BATCH_RCORD_BATCHENDDATE				="batchEndDate";
	public final static String BATCH_RCORD_COURSECODEID				="courseCodeId";
	public final static String BATCH_RCORD_NOTE						="note";
	public final static String BATCH_RCORD_STATUS					="status";
	
	
	public final static String STUDENT_DOCUMENT_TYPE 				="type";
	public final static String STUDENT_DOCUMENT_DESCRIPTION			="description";
	public final static String STUDENT_DOCUMENT_NAME				="name";

	public final static String BATCH_RCORD_BATCH 					= "batchName";
	public final static String TEACHER_GROUP_NAME					= "Teacher";
	public final static String COLUMN_SIBJECT_ID 					= "subjectId";
	public final static String COLUMN_SUBJECT_NAME 					= "subjcetName";
	public final static String COLUMN_TEACHER_NAME 					= "teacherName";
	public final static String COLUMN_START_DATE 					= "startDate";
	public final static String COLUMN_HOURS 						= "hours";
	
	public final static String COLUMN_ORGANIZATION 					= "organizationName";
	public final static String COLUMN_SESSION 						= "session";
	public final static String COLUMN_BATCH_START_DATE 				= "batchStartDate";
	public final static String COLUMN_END_DATE 						= "batchEndDate";
	public final static String COLUMN_CLASS_TEACHER 				= "batchClassTeacher";
	public final static String COLUMN_STATUS 						= "status";	
	
}

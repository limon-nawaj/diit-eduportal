package info.diit.portal.constant;

public enum ContentType {

	CHAPTER(1, "Chapter"),
	TOPIC(2, "Topic");
	private int key;
	private String value;
	
	private ContentType(int key, String value) {
		this.key = key;
		this.value = value;
	}

	public int getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	public String toString(){
		return value;
	}
	
	public static ContentType getContentType(int key){
		ContentType types[] = ContentType.values();
		for (ContentType type : types) {
			if (type.key==key) {
				return type;
			}
		}
		return null;
	}
}

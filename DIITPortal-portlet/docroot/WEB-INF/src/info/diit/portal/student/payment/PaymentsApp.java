package info.diit.portal.student.payment;

import info.diit.portal.dto.FeesDto;
import info.diit.portal.dto.ScheduleDto;
import info.diit.portal.model.FeeType;
import info.diit.portal.service.FeeTypeLocalServiceUtil;

import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import net.atontech.vaadin.ui.numericfield.NumericField;
import net.atontech.vaadin.ui.numericfield.widgetset.shared.NumericFieldType;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class PaymentsApp extends Application implements PortletRequestListener{

	private ThemeDisplay themeDisplay;
	private Window window;
	
	private static final String COLUMN_FEE_TYPE = "feeType";
	private static final String COLUMN_DESCRIPTION = "description";
	private static final String COLUMN_AMOUNT = "amount";
	private static final String COLUMN_SCHEDULE_DATE = "scheduleDate";
	
	private NumericField studentIdField;
	private TextField nameField;
	private ComboBox studentBatchComboBox;
	private NumericField totalPaidField;
	private NumericField totalDueField;
	private DateField dateField;
	private Table paymentTable;
	private BeanItemContainer<FeesDto> feeBeanItemContainer;
	private Table studentPaymentHistoryTable;
	private BeanItemContainer<ScheduleDto> studentPaymentHistoryBeanItemContainer;
	
	public final static String DATE_FORMAT = "dd/MM/yyyy";
	
    public void init() {
    	
    	
    	window = new Window();
		window.setSizeFull();
		setMainWindow(window);
		
		loadFeeTypesContainer();
		
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.setSpacing(true);

		verticalLayout.addComponent(initPaymentsPanel());
		
		window.addComponent(verticalLayout);
    }
    
    private GridLayout initPaymentsPanel()
	{
		studentIdField = new NumericField("Student ID");
		studentIdField.setAllowNegative(false);
		studentIdField.setImmediate(true);
		studentIdField.setNullRepresentation("");
		studentIdField.setNumberType(NumericFieldType.INTEGER);
		studentIdField.setWidth("100%");
		
		nameField = new TextField("Student Name");
		nameField.setReadOnly(true);
		nameField.setWidth("100%");
		
		studentBatchComboBox = new ComboBox("Batch");
		studentBatchComboBox.setImmediate(true);
		studentBatchComboBox.setWidth("100%");
		
		totalPaidField = new NumericField("Total Paid");
		totalPaidField.setWidth("100%");
		totalPaidField.setNullRepresentation("");
		totalPaidField.setNumberType(NumericFieldType.INTEGER);
		totalPaidField.setReadOnly(true);

		totalDueField = new NumericField("Total Due");
		totalDueField.setWidth("100%");
		totalDueField.setNullRepresentation("");
		totalDueField.setNumberType(NumericFieldType.INTEGER);
		totalDueField.setReadOnly(true);
		
		dateField = new DateField("Date");
		dateField.setResolution(DateField.RESOLUTION_MONTH);
		dateField.setValue(new Date());
		dateField.setDateFormat(DATE_FORMAT);
		dateField.setWidth("100%");
		
		
		
		paymentTable = new Table("New Payment",feeBeanItemContainer);
		paymentTable.setImmediate(true);
		paymentTable.setWidth("100%");
		paymentTable.setPageLength(5);
		
		paymentTable.setColumnHeader(COLUMN_FEE_TYPE, "Fee Type");
		paymentTable.setColumnHeader(COLUMN_AMOUNT, "Amount");

		paymentTable.setVisibleColumns(new String[]{COLUMN_FEE_TYPE,COLUMN_AMOUNT});
		
		paymentTable.setFooterVisible(true);
		
		paymentTable.setColumnFooter(COLUMN_FEE_TYPE, "Total");
		paymentTable.setColumnFooter(COLUMN_AMOUNT, "0");
		
		paymentTable.setColumnAlignment(COLUMN_AMOUNT, Table.ALIGN_RIGHT);
		
		paymentTable.setEditable(true);
		
		paymentTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {

				
				if(propertyId.equals(COLUMN_FEE_TYPE))
				{
					TextField field = new TextField();
					field.setWidth("100%");
					field.addStyleName("align-right");
					field.setReadOnly(true);
					
					
					return field;
				}
				
				if(propertyId.equals(COLUMN_AMOUNT))
				{
					NumericField field = new NumericField();
					field.setWidth("100%");
					field.setAllowNegative(false);
					field.setNullSettingAllowed(true);
					field.setNullRepresentation("");
					field.setNumberType(NumericFieldType.INTEGER);
					field.setImmediate(true);
					
					field.addListener(new ValueChangeListener() {
						
						@Override
						public void valueChange(ValueChangeEvent event) {
							
							long total = getNewPaymentTotal();
							paymentTable.setColumnFooter(COLUMN_AMOUNT, String.valueOf(total));
						}
					});
					return field;
				}
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});

		
		studentPaymentHistoryBeanItemContainer = new BeanItemContainer<ScheduleDto>(ScheduleDto.class);
		studentPaymentHistoryTable = new Table("Payment History",studentPaymentHistoryBeanItemContainer);
		studentPaymentHistoryTable.setWidth("100%");
		studentPaymentHistoryTable.setPageLength(17);
		
		studentPaymentHistoryTable.setColumnHeader(COLUMN_SCHEDULE_DATE, "Date");
		studentPaymentHistoryTable.setColumnHeader(COLUMN_FEE_TYPE, "Fee Type");
		studentPaymentHistoryTable.setColumnHeader(COLUMN_AMOUNT, "Amount");
		
		studentPaymentHistoryTable.setColumnExpandRatio(COLUMN_SCHEDULE_DATE, 1);
		studentPaymentHistoryTable.setColumnExpandRatio(COLUMN_FEE_TYPE, 1);
		studentPaymentHistoryTable.setColumnExpandRatio(COLUMN_AMOUNT, 1);
		
		studentPaymentHistoryTable.setVisibleColumns(new String[]{COLUMN_SCHEDULE_DATE,COLUMN_FEE_TYPE,COLUMN_AMOUNT});
		
		Button saveButton = new Button("Save");
		
		Label spacer = new Label();
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		horizontalLayout.setWidth("100%");
		
		horizontalLayout.addComponent(spacer);
		horizontalLayout.addComponent(saveButton);
		
		horizontalLayout.setExpandRatio(spacer, 1);

		GridLayout gridLayout = new GridLayout(5,6);
		gridLayout.setWidth("100%");
		gridLayout.setSpacing(true);
		
		gridLayout.addComponent(studentIdField,0,0);
		gridLayout.addComponent(studentBatchComboBox,1,0);
		gridLayout.addComponent(nameField,0,1,1,1);
		gridLayout.addComponent(totalPaidField,0,2);
		gridLayout.addComponent(totalDueField,1,2);
		gridLayout.addComponent(dateField,0,3);
		gridLayout.addComponent(paymentTable,0,4,1,4);
		gridLayout.addComponent(studentPaymentHistoryTable,2,0,4,4);
		gridLayout.addComponent(horizontalLayout,0,5,1,5);
		
		return gridLayout;
	}

	private void loadFeeTypesContainer() 
	{
		try
		{
			feeBeanItemContainer = new BeanItemContainer<FeesDto>(FeesDto.class);
			List<FeeType> feeTypesList = FeeTypeLocalServiceUtil.getFeeTypes(0, FeeTypeLocalServiceUtil.getFeeTypesCount());
			
			for(FeeType feeType:feeTypesList)
			{
				FeesDto accountsDto = new FeesDto();
				accountsDto.setId(feeType.getFeeTypeId());
				accountsDto.setFeeType(feeType.getTypeName());
				accountsDto.setDescription(feeType.getDescription());
				
				feeBeanItemContainer.addBean(accountsDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

	
	public long getNewPaymentTotal()
	{
		long total = 0;
		
		for(int i=0;i<feeBeanItemContainer.size();i++)
		{
			FeesDto feesDto = feeBeanItemContainer.getIdByIndex(i);
			
			if(feesDto.getAmount()!=null)
			{
				total += feesDto.getAmount();
			}
		}
		
		return total;
	}

	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

package info.diit.portal.student;

import info.diit.portal.NoSuchStudentException;
import info.diit.portal.constant.BatchStatus;
import info.diit.portal.dto.BatchDto;
import info.diit.portal.dto.BatchStudentDto;
import info.diit.portal.dto.CourseDto;
import info.diit.portal.dto.OrganizationDto;
import info.diit.portal.model.Batch;
import info.diit.portal.model.BatchStudent;
import info.diit.portal.model.Course;
import info.diit.portal.model.CourseOrganization;
import info.diit.portal.model.Student;
import info.diit.portal.service.BatchLocalServiceUtil;
import info.diit.portal.service.BatchStudentLocalServiceUtil;
import info.diit.portal.service.CourseLocalServiceUtil;
import info.diit.portal.service.CourseOrganizationLocalServiceUtil;
import info.diit.portal.service.StudentLocalServiceUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import com.liferay.portal.kernel.dao.jdbc.DataAccess;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.OrganizationConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.terminal.StreamResource;
import com.vaadin.terminal.StreamResource.StreamSource;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Component.Event;
import com.vaadin.ui.Component.Listener;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

public class StudentStatusReportApp extends Application implements 
PortletRequestListener{
	private static final long serialVersionUID = 1L;
	
	public Window 										window;
	public Window										myPopWindow;
	private Application 								app;
	private ThemeDisplay 								themeDisplay;
	private User 										user;
	private String 										userName;
	private long 										companyId;
	private List<CourseDto> 							courseDtoList;
	private List<BatchDto> 								batchesDtoList;
	private List<OrganizationDto>						organizationDtoList;
	private List<BatchStudentDto> 						studentDtoList;
	
	private BeanItemContainer<BatchStudentDto> 			batchStudentBeanItemContainer;
	private Table										batchStudentsTable;

	private long										searchOrganizationId;
	
	public final static String COLUMN_STUDENT_ID 		= "studentId";
	public final static String COLUMN_STUDENT_NAME		= "name";
	public final static String COLUMN_STUDENT_BATCH 	= "batchName";
	public final static String COLUMN_STUDENT_STATUS	= "status";
	
	private ComboBox 									batchComboBox;
	private ComboBox 									courseComboBox;
	private ComboBox									campusComboBox;
	private ComboBox									batchStudentStatusBox;
	private Button										showReport;
	private BatchStatus 								status;

    public void init() {
    	window = new Window();
        app=this;
		setMainWindow(window);
		
		try {
			connection = DataAccess.getConnection();
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
		user		= themeDisplay.getUser();
		companyId 	= themeDisplay.getCompanyId();
		
		getCampusesByCompany();
		window.addComponent(customSearch());
    }
    
    public VerticalLayout customSearch() {
		VerticalLayout verticalLayout 					= new VerticalLayout();
		verticalLayout									.setSizeFull();
		//verticalLayout.setWidth("100%");
		HorizontalLayout horizontalLayout 				= new HorizontalLayout();
		horizontalLayout								.setWidth("100%");
		horizontalLayout								.setSpacing(true);
		
		campusComboBox 									= new ComboBox("Campus");
		campusComboBox									.setImmediate(true);
		
		courseComboBox 									= new ComboBox();
		courseComboBox									.setImmediate(true);
		courseComboBox									.setCaption("Course");
		
		for (OrganizationDto orgDto : organizationDtoList) {
			campusComboBox.addItem(orgDto);
		}
		campusComboBox.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				courseComboBox							.removeAllItems();
				OrganizationDto serchorg 				= (OrganizationDto)campusComboBox.getValue();
				searchOrganizationId 					= serchorg.getOrganizationId();
				getCourseDtoByCompanyId();
				
				for (CourseDto course : courseDtoList) {
					courseComboBox.addItem(course);
				}
			}
		});	
		
		batchComboBox 									= new ComboBox("Batch");
		batchComboBox									.setImmediate(true);		
		
		courseComboBox.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				CourseDto courseDTO = (CourseDto)courseComboBox.getValue(); 
				batchComboBox.removeAllItems();
				getCourseBatches(courseDTO.getCourseId());
				getBatchStudents();
				for (BatchDto batchDto : batchesDtoList) {
					batchComboBox.addItem(batchDto);
				}
			}
		});	
		
		batchComboBox.addListener(new Listener() {
			BatchDto batch = (BatchDto) batchComboBox.getValue();
			SimpleStringFilter filter = null;
			
			public void componentEvent(Event event) {
				Filterable f = (Filterable)
						batchStudentsTable.getContainerDataSource();
				if (batch!=null) {
					
			        if (filter != null)
			            f.removeContainerFilter(filter);
			        
			        // Set new filter for the "Name" column
			        filter = new SimpleStringFilter(COLUMN_STUDENT_BATCH,batch.getBatchName(), true, false);
			        f.addContainerFilter(filter);
				}else{
					f.removeAllContainerFilters();
				}
			}
			
		});
		
		batchStudentStatusBox = new ComboBox("Status");
		for(BatchStatus status:BatchStatus.values())
		{
			batchStudentStatusBox.addItem(status.getValue());
		}
		
		batchStudentStatusBox.addListener(new Listener() {
			String status = (String) batchStudentStatusBox.getValue();
			SimpleStringFilter filter = null;
			
			public void componentEvent(Event event) {
				Filterable f = (Filterable)
						batchStudentsTable.getContainerDataSource();
				if (!status.equals("")) {
					
			        if (filter != null)
			            f.removeContainerFilter(filter);
			        
			        // Set new filter for the "Name" column
			        filter = new SimpleStringFilter(COLUMN_STUDENT_STATUS,status, true, false);
			        f.addContainerFilter(filter);
				}else{
					f.removeAllContainerFilters();
				}
			}
		});
		
		batchStudentBeanItemContainer = new BeanItemContainer<BatchStudentDto>(BatchStudentDto.class);
		
		batchStudentsTable = new Table("Students",batchStudentBeanItemContainer);
		batchStudentsTable.setPageLength(15);
		batchStudentsTable.setWidth("100%");
		batchStudentsTable.setSelectable(true);
		batchStudentsTable.setEditable(true);
		
		batchStudentsTable.setColumnHeader(COLUMN_STUDENT_BATCH, "Batch");
		batchStudentsTable.setColumnHeader(COLUMN_STUDENT_ID, "Student ID");
		batchStudentsTable.setColumnHeader(COLUMN_STUDENT_NAME, "Student Name");
		batchStudentsTable.setColumnHeader(COLUMN_STUDENT_STATUS, "Status");
		
		batchStudentsTable.setVisibleColumns(new String[] { COLUMN_STUDENT_BATCH, COLUMN_STUDENT_ID, COLUMN_STUDENT_NAME,COLUMN_STUDENT_STATUS});
		
		batchStudentsTable.setColumnExpandRatio(COLUMN_STUDENT_BATCH, 1);
		batchStudentsTable.setColumnExpandRatio(COLUMN_STUDENT_ID, 1);
		batchStudentsTable.setColumnExpandRatio(COLUMN_STUDENT_NAME, 3);
		batchStudentsTable.setColumnExpandRatio(COLUMN_STUDENT_STATUS, 1);
		
		batchStudentsTable.setTableFieldFactory(new DefaultFieldFactory(){
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				if (propertyId.equals(COLUMN_STUDENT_BATCH)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				
				if (propertyId.equals(COLUMN_STUDENT_ID)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				
				if (propertyId.equals(COLUMN_STUDENT_NAME)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				
				if (propertyId.equals(COLUMN_STUDENT_STATUS)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		showReport = new Button("Show Report");
		
		showReport.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				BatchStudentDto studentDto = (BatchStudentDto) batchStudentsTable.getValue();
				if(studentDto!=null){
					reportWindow(studentDto.getStudentId());
				}else{
					window.showNotification("Select a student", Window.Notification.TYPE_WARNING_MESSAGE);
				}
			}
		});
		
		horizontalLayout.addComponent(campusComboBox);
		horizontalLayout.addComponent(courseComboBox);
		horizontalLayout.addComponent(batchComboBox);
		horizontalLayout.addComponent(batchStudentStatusBox);
		
		verticalLayout.addComponent(horizontalLayout);
		verticalLayout.addComponent(batchStudentsTable);
		verticalLayout.addComponent(showReport);
		
		return verticalLayout;
	}
    
    private void getCampusesByCompany(){
		
		List<Organization> organization = null;
		try {
			organization = OrganizationLocalServiceUtil.getOrganizations(companyId, OrganizationConstants.ANY_PARENT_ORGANIZATION_ID);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		organizationDtoList = new ArrayList<OrganizationDto>();
		for(Organization org : organization){
			OrganizationDto orgDto = new OrganizationDto();
			orgDto.setOrganizationId(org.getOrganizationId());
			orgDto.setOrganizationName(org.getName());				
			organizationDtoList.add(orgDto);
		}
    }
    
    private void getCourseDtoByCompanyId(){
		try {
			List<CourseOrganization> courseOrganizations = CourseOrganizationLocalServiceUtil.findByOrganization(searchOrganizationId);
			//System.out.println("couse size by organization"+courseOrganizations.size());
			courseDtoList = new ArrayList<CourseDto>();
			for(CourseOrganization courseOrg : courseOrganizations){
				CourseDto courseDto = new CourseDto();
				long courseid = courseOrg.getCourseId();
				Course course = CourseLocalServiceUtil.fetchCourse(courseid);
				courseDto.setCourseId(courseid);
				courseDto.setCourseName(course.getCourseName());				
				courseDtoList.add(courseDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
    
    private void getCourseBatches(long courseId) {
		try {
			List<Batch> batches = BatchLocalServiceUtil.findBatchByCourseId(courseId);
			batchesDtoList = new ArrayList<BatchDto>();
			if(batches!=null){
				for (Batch batch : batches) {
					BatchDto batchDto = new BatchDto();
					batchDto.setBatchId(batch.getBatchId());
					batchDto.setBatchName(batch.getBatchName());
					batchesDtoList.add(batchDto);
				}
			}			
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
    
    public void getBatchStudents(){
    	batchStudentBeanItemContainer.removeAllItems();
		try {
			studentDtoList = new ArrayList<BatchStudentDto>();
			for (BatchDto batchDto : batchesDtoList) {
				List<BatchStudent> batchStudnetList = BatchStudentLocalServiceUtil.findAllStudentByOrgBatchId(searchOrganizationId, batchDto.getBatchId());
				for(BatchStudent batchStudent :batchStudnetList){
					try {
						Student student = StudentLocalServiceUtil.findStudentByOrgStudentId(searchOrganizationId, batchStudent.getStudentId());
						BatchStudentDto studentDto = new BatchStudentDto(student,batchStudent);
						studentDto.setBatchName(batchDto.getBatchName());
						BatchStatus status = BatchStatus.getStatus(batchStudent.getStatus());
						studentDto.setStatusStr(status.getValue());
						studentDtoList.add(studentDto);
					} catch (NoSuchStudentException e) {
						e.printStackTrace();
					}
				}
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
		if(studentDtoList!=null){
			for(BatchStudentDto studentDto:studentDtoList){
				batchStudentBeanItemContainer.addBean(studentDto);
			}
		}

		batchStudentsTable.refreshRowCache();
    }
    
    // report 
    SimpleDateFormat dateformat 	= new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
	private final String timeStamp 	= dateformat.format(new Date());
	JasperPrint 					jasperPrint;
	Connection 						connection;
	
	public void reportWindow(long studentId) {
		
		myPopWindow = new Window("report");
		myPopWindow.setWidth("60%");
		myPopWindow.setHeight("800px");
		myPopWindow.setPositionX(300);
		myPopWindow.setPositionY(300);
		
		final Map reportParameter = new HashMap();
		reportParameter.put("studentId", studentId);
		/*long batchid = 25;
		imagelocation.put("batchId",batchid );*/
		
		StreamSource s = new StreamResource.StreamSource() {
			public InputStream getStream() {
				try {		
					try {
						jasperPrint = JasperFillManager.fillReport(PropsUtil.get("dl.hook.file.system.root.dir.report") + "/" 
					+"Student_Profile.jasper", reportParameter,connection);
						
					} catch (JRException e2) {
						e2.printStackTrace();
					}
					
					JasperExportManager.exportReportToPdfFile(jasperPrint,PropsUtil.get("dl.hook.file.system.root.dir.pdf")
							+"/"+user.getUserId()+"_"+timeStamp+ ".pdf");
					
					System.out.println("location ----------------------------"+PropsUtil.get("dl.hook.file.system.root.dir.pdf")
							+"/"+user.getUserId()+"_"+timeStamp+ ".pdf");
					
					File file = new File(PropsUtil.get("dl.hook.file.system.root.dir.pdf")
							+"/"+user.getUserId()+"_"+timeStamp+ ".pdf");
					
					//System.out.println("Absolute pateh --------------"+file.getAbsolutePath());
					FileInputStream fis = new FileInputStream(file);
					return fis;
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}
			}
		};
		
		StreamResource r = new StreamResource(s, "repy.pdf", this);
		
		Embedded e = new Embedded();
		e.setSizeFull();
		e.setHeight("700px");
		e.setType(Embedded.TYPE_BROWSER);
		r.setMIMEType("application/pdf");
		e.setSource(r);
		
		myPopWindow.addComponent(e);	

		window.addWindow(myPopWindow);
		
		myPopWindow.addListener(new CloseListener() {
			public void windowClose(CloseEvent e) {
				File file = new File(PropsUtil.get("dl.hook.file.system.root.dir.pdf")
						+"/"+user.getUserId()+"_"+timeStamp+ ".pdf");
				file.delete();
			}
		});
	}
    // end report

	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

package info.diit.portal.student;

import info.diit.portal.NoSuchStudentException;
import info.diit.portal.constant.BatchStatus;
import info.diit.portal.dto.BatchDto;
import info.diit.portal.dto.BatchStudentDto;
import info.diit.portal.dto.CourseDto;
import info.diit.portal.dto.OrganizationDto;
import info.diit.portal.model.Batch;
import info.diit.portal.model.BatchStudent;
import info.diit.portal.model.Course;
import info.diit.portal.model.CourseOrganization;
import info.diit.portal.model.StatusHistory;
import info.diit.portal.model.Student;
import info.diit.portal.model.impl.BatchStudentImpl;
import info.diit.portal.model.impl.StatusHistoryImpl;
import info.diit.portal.service.BatchLocalServiceUtil;
import info.diit.portal.service.BatchStudentLocalServiceUtil;
import info.diit.portal.service.CourseLocalServiceUtil;
import info.diit.portal.service.CourseOrganizationLocalServiceUtil;
import info.diit.portal.service.StatusHistoryLocalServiceUtil;
import info.diit.portal.service.StudentLocalServiceUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.OrganizationConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class StudentStatusApp extends Application implements 
PortletRequestListener {
	public Window 										window;
	private Application 								app;
	private ThemeDisplay 								themeDisplay;
	private User 										user;
	private String 										userName;
	private long 										companyId;
	private List<CourseDto> 							courseDtoList;
	private List<BatchDto> 								batchesDtoList;
	private List<OrganizationDto>						organizationDtoList;
	
	private BeanItemContainer<BatchStudentDto> 			batchStudentBeanItemContainer;
	private Table										batchStudentsTable;

	private long										searchOrganizationId;
	public final static String COLUMN_STUDENT_ID 		= "studentId";
	public final static String COLUMN_STUDENT_NAME		= "name";
	public final static String COLUMN_CHECK				= "check";
	
    public void init() {
    	window = new Window();
        app=this;
		setMainWindow(window);
		
		companyId = themeDisplay.getCompanyId();
		
		getCampusesByCompany();
		window.addComponent(customSearch());
    }
    
    public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
	}
	private ComboBox 					batchComboBox;
	private ComboBox 					courseComboBox;
	private ComboBox					campusComboBox;
	private ComboBox					batchStudentStatusBox;
	private Button						saveButton;
	private BatchStatus 				status;
	
	public HorizontalLayout customSearch() {
		VerticalLayout verticalLayout = new VerticalLayout();
		//verticalLayout.setWidth("100%");
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setWidth("100%");
		horizontalLayout.setSpacing(true);
		
		campusComboBox = new ComboBox("Campus");
		campusComboBox.setImmediate(true);
		
		courseComboBox = new ComboBox();
		courseComboBox.setImmediate(true);
		courseComboBox.setCaption("Course");
		
		for (OrganizationDto orgDto : organizationDtoList) {
			campusComboBox.addItem(orgDto);
		}
		campusComboBox.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				courseComboBox.removeAllItems();
				OrganizationDto serchorg = (OrganizationDto)campusComboBox.getValue();
				searchOrganizationId = serchorg.getOrganizationId();
				getCourseDtoByCompanyId();
				for (CourseDto course : courseDtoList) {
					courseComboBox.addItem(course);
				}
			}
		});	
		
		batchComboBox = new ComboBox("Batch");
		batchComboBox.setImmediate(true);		
		
		courseComboBox.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				CourseDto courseDTO = (CourseDto)courseComboBox.getValue(); 
				getCourseBatches(courseDTO.getCourseId());
				batchComboBox.removeAllItems();
				for (BatchDto batchDto : batchesDtoList) {
					batchComboBox.addItem(batchDto);
				}
			}
		});	
		
		batchComboBox.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				BatchDto batch = (BatchDto) batchComboBox.getValue();
				getStudentByBatchId(batch.getBatchId());
			}
		});
		
		batchStudentStatusBox = new ComboBox("Change Status");
		for(BatchStatus status:BatchStatus.values())
		{
			batchStudentStatusBox.addItem(status);
		}
		
		batchStudentBeanItemContainer = new BeanItemContainer<BatchStudentDto>(BatchStudentDto.class);
		
		batchStudentsTable = new Table("Students",batchStudentBeanItemContainer);
		batchStudentsTable.setPageLength(15);
		batchStudentsTable.setWidth("100%");
		batchStudentsTable.setSelectable(true);
		batchStudentsTable.setEditable(true);
		
		batchStudentsTable.setColumnHeader(COLUMN_STUDENT_ID, "Student ID");
		batchStudentsTable.setColumnHeader(COLUMN_STUDENT_NAME, "Student Name");
		batchStudentsTable.setColumnHeader(COLUMN_CHECK, "Check");
		
		batchStudentsTable.setVisibleColumns(new String[] { COLUMN_CHECK , COLUMN_STUDENT_ID, COLUMN_STUDENT_NAME});
		
		batchStudentsTable.setColumnExpandRatio(COLUMN_CHECK, 1);
		batchStudentsTable.setColumnExpandRatio(COLUMN_STUDENT_ID, 1);
		batchStudentsTable.setColumnExpandRatio(COLUMN_STUDENT_NAME, 3);
		
		batchStudentsTable.setTableFieldFactory(new DefaultFieldFactory(){
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				if (propertyId.equals(COLUMN_STUDENT_ID)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				
				if (propertyId.equals(COLUMN_STUDENT_NAME)) {
					TextField field = new TextField();
					field.setWidth("100%");
					field.setReadOnly(true);
					return field;
				}
				
				if (propertyId.equals(COLUMN_CHECK)) {
					CheckBox field = new CheckBox();
					field.setWidth("100%");
					field.setReadOnly(false);
					return field;
				}
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		saveButton = new Button("Save");
		
		saveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				if(batchStudentBeanItemContainer.size()>0){
					status = (BatchStatus) batchStudentStatusBox.getValue();
					for (int i = 0; i < batchStudentBeanItemContainer.size(); i++) {
						BatchStudentDto studentDto = batchStudentBeanItemContainer.getIdByIndex(i);
						if(studentDto.isCheck()){
							if(studentDto.getStatus()!=status.getKey()){
								StatusHistory statusHistory = new StatusHistoryImpl();
								statusHistory.setModifiedDate(new Date());
								statusHistory.setBatchStudentId(studentDto.getBatchStudentId());
								statusHistory.setFromStatus(studentDto.getStatus());
								statusHistory.setToStatus(status.getKey());
								
								BatchStudent batchStudent = new BatchStudentImpl();
								batchStudent.setNew(false);
								batchStudent.setBatchStudentId(studentDto.getBatchStudentId());
								batchStudent.setStatus(status.getKey());
								
								try {
									BatchStudentLocalServiceUtil.updateBatchStudent(batchStudent);
									StatusHistoryLocalServiceUtil.addStatusHistory(statusHistory);
								} catch (SystemException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}else{
					window.showNotification("No Student, to chnage status");
				}
				
			}
		});
		
		verticalLayout.addComponent(campusComboBox);
		verticalLayout.addComponent(courseComboBox);
		verticalLayout.addComponent(batchComboBox);
		verticalLayout.addComponent(batchStudentStatusBox);
		verticalLayout.addComponent(saveButton);
		
		horizontalLayout.addComponent(verticalLayout);
		horizontalLayout.addComponent(batchStudentsTable);
		
		return horizontalLayout;
	}
	
	private List<BatchStudentDto> studentDtoList;
	private void getStudentByBatchId(long batchId) {
		batchStudentBeanItemContainer.removeAllItems();
		try {
			studentDtoList = new ArrayList<BatchStudentDto>();
			List<BatchStudent> batchStudnetList = BatchStudentLocalServiceUtil.findAllStudentByOrgBatchId(searchOrganizationId, batchId);
			for(BatchStudent batchStudent :batchStudnetList){
				try {
					Student student = StudentLocalServiceUtil.findStudentByOrgStudentId(searchOrganizationId, batchStudent.getStudentId());
					BatchStudentDto studentDto = new BatchStudentDto(student,batchStudent);
					studentDtoList.add(studentDto);
				} catch (NoSuchStudentException e) {
					e.printStackTrace();
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		if(studentDtoList!=null){
			for(BatchStudentDto studentDto:studentDtoList){
				batchStudentBeanItemContainer.addBean(studentDto);
			}
		}

		batchStudentsTable.refreshRowCache();
	}
	
	private void getCampusesByCompany(){
		
			List<Organization> organization = null;
			try {
				organization = OrganizationLocalServiceUtil.getOrganizations(companyId, OrganizationConstants.ANY_PARENT_ORGANIZATION_ID);
			} catch (SystemException e) {
				e.printStackTrace();
			}
			organizationDtoList = new ArrayList<OrganizationDto>();
			for(Organization org : organization){
				OrganizationDto orgDto = new OrganizationDto();
				orgDto.setOrganizationId(org.getOrganizationId());
				orgDto.setOrganizationName(org.getName());				
				organizationDtoList.add(orgDto);
			}
	}
	
	private void getCourseDtoByCompanyId(){
		try {
			List<CourseOrganization> courseOrganizations = CourseOrganizationLocalServiceUtil.findByOrganization(searchOrganizationId);
			//System.out.println("couse size by organization"+courseOrganizations.size());
			courseDtoList = new ArrayList<CourseDto>();
			for(CourseOrganization courseOrg : courseOrganizations){
				CourseDto courseDto = new CourseDto();
				long courseid = courseOrg.getCourseId();
				Course course = CourseLocalServiceUtil.fetchCourse(courseid);
				courseDto.setCourseId(courseid);
				courseDto.setCourseName(course.getCourseName());				
				courseDtoList.add(courseDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	
	
	private void getCourseBatches(long courseId) {
		try {
			List<Batch> batches = BatchLocalServiceUtil.findBatchByCourseId(courseId);
			batchesDtoList = new ArrayList<BatchDto>();
			for (Batch batch : batches) {
				BatchDto batchDto = new BatchDto();
				batchDto.setBatchId(batch.getBatchId());
				batchDto.setBatchName(batch.getBatchName());
				batchesDtoList.add(batchDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

}

package info.diit.portal.course.subject;

import info.diit.portal.model.Course;
import info.diit.portal.model.CourseOrganization;
import info.diit.portal.model.CourseSession;
import info.diit.portal.model.CourseSubject;
import info.diit.portal.model.Subject;
import info.diit.portal.model.impl.CourseImpl;
import info.diit.portal.model.impl.CourseOrganizationImpl;
import info.diit.portal.model.impl.CourseSessionImpl;
import info.diit.portal.model.impl.CourseSubjectImpl;
import info.diit.portal.model.impl.SubjectImpl;
import info.diit.portal.service.CourseLocalServiceUtil;
import info.diit.portal.service.CourseOrganizationLocalServiceUtil;
import info.diit.portal.service.CourseSessionLocalServiceUtil;
import info.diit.portal.service.CourseSubjectLocalServiceUtil;
import info.diit.portal.service.SubjectLocalServiceUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.tokenfield.TokenField;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.OrganizationConstants;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class CourseSubjectManageApp extends Application implements PortletRequestListener{

	private final static String COURSE_CODE 			= "courseCode";
	private final static String COURSE_NAME 			= "courseName";
//	private final static String COURSE_DESCRIPTION = "description";
	
	private final static String SUBJECT_CODE 			= "subjectCode";
	private final static String SUBJECT_NAME 			= "subjectName";
//	private final static String SUBJECT_DESCRIPTION = "description";
	
	private Window 										window;
	
	private ThemeDisplay 								themeDisplay;
	
//	private TabSheet tabSheet;
	private VerticalLayout 								mainVerticalLayout;
	
	// Layout	
	private GridLayout 									allComponentGridLayout;
	private VerticalLayout 								addCourseVerticalLayout;
	private VerticalLayout 								addSubjectVerticalLayout;
	private VerticalLayout 								addSessionVerticalLayout;
//	private GridLayout allTableGridLayout;
	private GridLayout 									courseTabelGridLayout;
	private GridLayout 									subjectTableGridLayout;
	
	//course management	
	private Course 										course = null;
	private TextField 									courseCodeField;
	private TextField 									courseNameField;
	private TextArea 									courseDescriptionArea;
	
	private BeanItemContainer<Course> 					courseDataSource;
	private Object 										previouseCourseCode;
	private Table 										courseTable;
	private Object 										courseId;
	private Label 										courseCodeMessageLabel;
	
	//Subject Management
	private Subject 									subject = null;
	private BeanItemContainer<Subject> 					subjectDataSource;
	private TextField 									subjectCodeField;
	private TextField 									subjectNameField;
	private TextArea 									subjectDescriptionArea;
	private Label 										subjectCodeMessageLabel;
	private Table 										subjectTable;
	
	private Object 										previousSubjectCode;
	
	//Course container
	private VerticalLayout 								courseSubjectContainerVerticalLayout;
	private VerticalLayout 								courseOrganizationContainerVerticalLayout;
	
	// Course Session
	private CourseSession 								courseSession;
	private TokenField 									sessionToken;
	private Set<String> 								tokens; 
	
	public void init() {
		window = new Window();

		setMainWindow(window);
		
		Label label = new Label("Hello SubjectCourseManagement!");
		
		courseDataSource = new BeanItemContainer<Course>(Course.class);
		subjectDataSource = new BeanItemContainer<Subject>(Subject.class);
		
		mainVerticalLayout = new VerticalLayout();
		mainVerticalLayout.setSizeFull();
		mainVerticalLayout.setSpacing(true);
		
		mainVerticalLayout.addComponent(allComponentGrid());
		loadCourseTable();
		loadSubjectTable();

		window.addComponent(mainVerticalLayout);
	}

	private GridLayout allComponentGrid(){
		allComponentGridLayout = new GridLayout(8, 3);
		allComponentGridLayout.setSpacing(true);
		allComponentGridLayout.setWidth("100%");
		allComponentGridLayout.setSpacing(true);
		
		Label separatorLabel = new Label("<hr />",Label.CONTENT_XHTML);
		separatorLabel.setHeight("40px");
		separatorLabel.setWidth("100%");
		
		allComponentGridLayout.addComponent(addCourseForm(), 0, 0, 1, 0);
		allComponentGridLayout.addComponent(courseTable(), 3,0,6,0);
		allComponentGridLayout.addComponent(separatorLabel, 0, 1 , 7, 1);
		allComponentGridLayout.addComponent(addSubjectForm(), 0, 2, 1, 2);
		allComponentGridLayout.addComponent(subjectTable(), 3, 2,6,2);
		return allComponentGridLayout;
	}
	
	private VerticalLayout addCourseForm(){
		addCourseVerticalLayout = new VerticalLayout();
		addCourseVerticalLayout.setImmediate(false);
		addCourseVerticalLayout.setSpacing(true);
		addCourseVerticalLayout.setWidth("100%");
		addCourseVerticalLayout.setHeight("-1px");
		addCourseVerticalLayout.setMargin(false);
		addCourseVerticalLayout.setSpacing(true);
		
		courseCodeMessageLabel = new Label();
		addCourseVerticalLayout.addComponent(courseCodeMessageLabel);
					
		// courseCodeField
		courseCodeField = new TextField("Course Code");
		courseCodeField.setImmediate(false);
		courseCodeField.setWidth("100%");
		courseCodeField.setHeight("-1px");
		courseCodeField.setRequired(true);
		addCourseVerticalLayout.addComponent(courseCodeField);
		
		courseNameField = new TextField("Course Title");
		courseNameField.setImmediate(false);
		courseNameField.setWidth("100%");
		courseNameField.setHeight("-1px");
		courseNameField.setRequired(true);
		addCourseVerticalLayout.addComponent(courseNameField);
		
		courseDescriptionArea = new TextArea("Description");
		courseDescriptionArea.setImmediate(false);
		courseDescriptionArea.setWidth("100%");
		courseDescriptionArea.setHeight("-1px");
		addCourseVerticalLayout.addComponent(courseDescriptionArea);
		
		
		sessionToken = new TokenField("Session");
		sessionToken.setImmediate(false);
		sessionToken.setWidth("100%");
		
		tokens = new HashSet<String>();
		sessionToken.addListener(new ValueChangeListener() {                    
		  
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				Set nameToken = (Set) event.getProperty().getValue();
				tokens.clear();
				if(nameToken!=null)
				{
					tokens.addAll(nameToken);
				}
			}
		});
				
		addCourseVerticalLayout.addComponent(sessionToken);
		
		HorizontalLayout courseFormButtonLayout = new HorizontalLayout();
		courseFormButtonLayout.setSpacing(true);
		
		Button saveCourse = new Button();
		saveCourse.setCaption("Save");
		saveCourse.setImmediate(false);
		saveCourse.setWidth("-1px");
		saveCourse.setHeight("-1px");
		courseFormButtonLayout.addComponent(saveCourse);
		
		Button courseReset = new Button();
		courseReset.setCaption("Reset");
		courseReset.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				course = null;
				clearCourse();
			}
		});
		courseFormButtonLayout.addComponent(courseReset);
		
		addCourseVerticalLayout.addComponent(courseFormButtonLayout);
				
		saveCourse.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				
				if(course==null){
					course = new CourseImpl();
					course.setNew(true);
				}
				
				if (courseSession==null) {
					courseSession = new CourseSessionImpl();
				}
				
				course.setCompanyId(themeDisplay.getCompanyId());
				
				/*try {
					course.setOrganizationId(themeDisplay.getLayout().getGroup().getOrganizationId());
				} catch (PortalException e1) {
					e1.printStackTrace();
				} catch (SystemException e1) {
					e1.printStackTrace();
				}*/
				
				course.setUserId(themeDisplay.getUser().getUserId());
				course.setUserName(themeDisplay.getUser().getScreenName());
				
				String courseCode = courseCodeField.getValue().toString();
				if (!courseCode.equals(null) && !courseCode.equals("")) {
					course.setCourseCode(courseCode);
				}else{
					window.showNotification("Course code can't be empty", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				String courseName = courseNameField.getValue().toString();
				if (!courseName.equals(null) && !courseName.equals("")) {
					course.setCourseName(courseName);
				}else{
					window.showNotification("Course name can't be empty", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				String courseDescription = courseDescriptionArea.getValue().toString();
				if (courseDescription!=null) {
					course.setDescription(courseDescription);
				}
				
				courseSession.setCompanyId(themeDisplay.getCompanyId());
				/*try {
					courseSession.setOrganizationId(themeDisplay.getLayout().getGroup().getOrganizationId());
				} catch (PortalException e1) {
					e1.printStackTrace();
				} catch (SystemException e1) {
					e1.printStackTrace();
				}*/
				courseSession.setUserId(themeDisplay.getUser().getUserId());
				courseSession.setUserName(themeDisplay.getUser().getScreenName());
				courseSession.setCreateDate(new Date());
												
				try {
					String code = courseCodeField.getValue().toString();
					if(course.isNew()){						
						if(getCourseCompany(code)){
							/*courseCodeMessageLabel.setVisible(true);
							courseCodeMessageLabel.setCaption("Course code can't be same");*/
							window.showNotification("Course code can't be same", Window.Notification.TYPE_ERROR_MESSAGE);
						}else{
							course.setCreateDate(new Date());
							CourseLocalServiceUtil.addCourse(course);
							
							Iterator<String> iterator = tokens.iterator();
							while (iterator.hasNext()) {
								String session = iterator.next();
								courseSession.setSessionName(session);
								courseSession.setCourseId(course.getCourseId());
								CourseSessionLocalServiceUtil.addCourseSession(courseSession);
								
							}
							window.showNotification("Course saved successfully!");
							loadCourseTable();
							clearCourse();
						}
					}else{
						if (previouseCourseCode.equals(courseCodeField.getValue())) {
							course.setModifiedDate(new Date());
							courseUpdate();
						}else if(getCourseCompany(code)){
							/*courseCodeMessageLabel.setVisible(true);
							courseCodeMessageLabel.setCaption("Course code can't be same");*/
							window.showNotification("Course code can't be same", Window.Notification.TYPE_ERROR_MESSAGE);
						}else{
							course.setModifiedDate(new Date());
							courseUpdate();
						}
					}
					
				} catch (SystemException e) {
					e.printStackTrace();
				} 								
			}
		});
		return addCourseVerticalLayout;
	}
	
	/*private boolean getCourseOrganization(String code){
		boolean status = false;
		try {
			List<Course> courseOrg = CourseLocalServiceUtil.findByOrganization(themeDisplay.getLayout().getGroup().getOrganizationId());
			for (int i = 0; i < courseOrg.size(); i++) {
				Course org = courseOrg.get(i);
				if (org.getCourseCode().equals(code)) {
					status = true;
					break;
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
		return status;
	}*/
	
	private boolean getCourseCompany(String code){
		boolean status = false;
		try {
			List<Course> courseCompany = CourseLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
			for (int i = 0; i < courseCompany.size(); i++) {
				Course company = courseCompany.get(i);
				if (company.getCourseCode().trim().equalsIgnoreCase(code.trim())) {
					status = true;
					break;
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return status;
	}
	
	private void courseUpdate(){
		try {
			CourseLocalServiceUtil.updateCourse(course);
			
			List<CourseSession> csList = CourseSessionLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
			Object courseID = course.getCourseId();
			for (int i = 0; i < csList.size(); i++) {
				CourseSession cs = csList.get(i);
				if (courseID.equals(cs.getCourseId())) {
					CourseSessionLocalServiceUtil.deleteCourseSession(cs);
				}
			}
			
			Iterator<String> iterator = tokens.iterator();
			while (iterator.hasNext()) {
				String session = iterator.next();
				courseSession.setSessionName(session);
				courseSession.setCourseId(course.getCourseId());
				CourseSessionLocalServiceUtil.addCourseSession(courseSession);
				
			}
			window.showNotification("Course data updated!");
			loadCourseTable();
			clearCourse();
			courseCodeMessageLabel.setVisible(false);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private VerticalLayout addSubjectForm(){
		addSubjectVerticalLayout = new VerticalLayout();
		addSubjectVerticalLayout.setImmediate(false);
		addSubjectVerticalLayout.setSpacing(true);
		addSubjectVerticalLayout.setWidth("100%");
		addSubjectVerticalLayout.setHeight("-1px");
		addSubjectVerticalLayout.setMargin(false);
		addSubjectVerticalLayout.setSpacing(true);
		
		subjectCodeMessageLabel = new Label();
		addSubjectVerticalLayout.addComponent(subjectCodeMessageLabel);
				
//		HorizontalLayout subjectCodeHLayout = new HorizontalLayout();
		
		subjectCodeField = new TextField("Subject Code");
		subjectCodeField.setImmediate(false);
		subjectCodeField.setWidth("100%");
		subjectCodeField.setHeight("-1px");
		subjectCodeField.setRequired(true);
//		subjectCodeHLayout.addComponent(subjectCodeField);
				
				
		addSubjectVerticalLayout.addComponent(subjectCodeField);
		
		subjectNameField = new TextField("Subject Name");
		subjectNameField.setImmediate(false);
		subjectNameField.setWidth("100%");
		subjectNameField.setHeight("-1px");
		subjectNameField.setRequired(true);
		addSubjectVerticalLayout.addComponent(subjectNameField);
		
		subjectDescriptionArea = new TextArea("Description");
		subjectDescriptionArea.setImmediate(false);
		subjectDescriptionArea.setWidth("100%");
		subjectDescriptionArea.setHeight("-1px");
		addSubjectVerticalLayout.addComponent(subjectDescriptionArea);
		
		HorizontalLayout subjectFormButton = new HorizontalLayout();
		subjectFormButton.setSpacing(true);
		
		Button saveButton = new Button();
		saveButton.setCaption("Save");
		saveButton.setImmediate(false);
		subjectFormButton.addComponent(saveButton);
		
		saveButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				
				if (subject==null) {
					subject = new SubjectImpl();
					subject.setNew(true);
				}
				subject.setCompanyId(themeDisplay.getCompanyId());
				/*try {
					subject.setOrganizationId(themeDisplay.getLayout().getGroup().getOrganizationId());
				} catch (PortalException e1) {
					e1.printStackTrace();
				} catch (SystemException e1) {
					e1.printStackTrace();
				}*/
				subject.setUserId(themeDisplay.getUserId());
				subject.setUserName(themeDisplay.getUser().getScreenName());
				
				String subjectCode = subjectCodeField.getValue().toString();
				if (!subjectCode.equals(null) && !subjectCode.equals("")) {
					subject.setSubjectCode(subjectCode);
				}else{
					window.showNotification("Subject code can't be empty", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				String subjectName = subjectNameField.getValue().toString();
				if (!subjectName.equals(null) && !subjectName.equals("")) {
					subject.setSubjectName(subjectName);
				}else{
					window.showNotification("Subject name can't be empty", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				subject.setDescription(subjectDescriptionArea.getValue().toString());
				
				try {
					String code = subjectCodeField.getValue().toString();
					if (subject.isNew()) {
						if (getSubjectCompany(code)) {
							/*subjectCodeMessageLabel.setVisible(true);
							subjectCodeMessageLabel.setCaption("Subject code can't be duplicate");*/
							window.showNotification("Subject code can't be duplicate", Window.Notification.TYPE_ERROR_MESSAGE);
						}else{
							subject.setCreateDate(new Date());
							SubjectLocalServiceUtil.addSubject(subject);
							window.showNotification("Subject code successfully saved!");
							loadSubjectTable();
							clearSubject();
						}
					}else{
						if (previousSubjectCode.equals(subjectCodeField.getValue())) {
							subject.setModifiedDate(new Date());
							subjectUpdate();
						}else if(getSubjectCompany(code)){
							subjectCodeMessageLabel.setVisible(true);
							subjectCodeMessageLabel.setCaption("Subject code can't be duplicate");
						}else{
							subjectUpdate();
						}
					}
					
				} catch (SystemException e1) {
					e1.printStackTrace();
				}
				
			}
		});
		
		Button resetButton = new Button();
		resetButton.setCaption("Reset");
		resetButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				subject = null;
				clearSubject();
			}
		});
		subjectFormButton.addComponent(resetButton);
		
		addSubjectVerticalLayout.addComponent(subjectFormButton);
		return addSubjectVerticalLayout;
	}
	
	/*private boolean getSubjectOrganization(String code){
		boolean status = false;
		try {
			List<Subject> subjectOrg = SubjectLocalServiceUtil.findByOrganization(themeDisplay.getLayout().getGroup().getOrganizationId());
			for (int i = 0; i < subjectOrg.size(); i++) {
				Subject org = subjectOrg.get(i);
				if (org.getSubjectCode().equals(code)) {
					status = true;
					break;
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
		return status;
	}*/
	
	private boolean getSubjectCompany(String code){
		boolean status = false;
		try {
			List<Subject> companyList = SubjectLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
			for (int i = 0; i < companyList.size(); i++) {
				Subject company = companyList.get(i);
				if (company.getSubjectCode().trim().equalsIgnoreCase(code.trim())) {
					status = true;
					break;
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return status;
	}
		
	private void subjectUpdate(){
		try {
			SubjectLocalServiceUtil.updateSubject(subject);
			window.showNotification("Subject updated successfully");
			loadSubjectTable();
			clearSubject();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
		
		
	public GridLayout courseTable(){
		courseTabelGridLayout = new GridLayout(3, 2);
		courseTabelGridLayout.setImmediate(false);
		courseTabelGridLayout.setSpacing(true);
		courseTabelGridLayout.setWidth("100%");
		courseTabelGridLayout.setMargin(false);
		courseTabelGridLayout.setSpacing(true);
		
		courseTable = new Table("Course Table", courseDataSource);
		courseTable.setImmediate(false);
		courseTable.setSelectable(true);
		courseTable.setWidth("100.0%");
		courseTabelGridLayout.addComponent(courseTable, 0, 0, 2, 0);
		
		courseTable.setColumnHeader(COURSE_CODE, "Course Code");
		courseTable.setColumnHeader(COURSE_NAME, "Course Title");
//		courseTable.setColumnHeader(COURSE_DESCRIPTION, "Description");
		
		courseTable.setVisibleColumns(new String[]{COURSE_CODE, COURSE_NAME});
		
		HorizontalLayout courseButtonRowLayout = new HorizontalLayout();
		courseButtonRowLayout.setSpacing(true);
		courseButtonRowLayout.setWidth("100%");
		
		Button courseEditButton = new Button("Edit");
		
		courseEditButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				course = (Course) courseTable.getValue();
				if (course!=null) {
					course.setNew(false);
					showEditCourse(course);
				}else {
					window.showNotification("Please select a course!", Window.Notification.TYPE_ERROR_MESSAGE);
				}
			}
		});
		
		Button courseDeleteButton = new Button("Delete");
		courseDeleteButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				
				ConfirmDialog.show(window, "Are you sure you want to delete this record?", new ConfirmDialog.Listener() {
					
					@Override
					public void onClose(ConfirmDialog arg0) {
						course = (Course) courseTable.getValue();
						if (course!=null) {
							try {
								Object courseId = course.getCourseId();
								
								List<CourseSubject> courseSubList = CourseSubjectLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
								
								for (int i = 0; i < courseSubList.size(); i++) {
									CourseSubject courseSub = courseSubList.get(i);
									Object id = courseSub.getCourseId();
									if (courseId.equals(id)) {
										CourseSubjectLocalServiceUtil.deleteCourseSubject(courseSub);
									}
								}
								List<CourseSession> courseSessionList = CourseSessionLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
								for (int i = 0; i < courseSessionList.size(); i++) {
									CourseSession courseSub = courseSessionList.get(i);
									if (courseId.equals(courseSub.getCourseId())) {
										CourseSessionLocalServiceUtil.deleteCourseSession(courseSub);
									}
								}
								
								List<CourseOrganization> courseOrganizationList = CourseOrganizationLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
								for (int i = 0; i < courseOrganizationList.size(); i++) {
									CourseOrganization courseOrganization = courseOrganizationList.get(i);
									if (courseId.equals(courseOrganization.getCourseId())) {
										CourseOrganizationLocalServiceUtil.deleteCourseOrganization(courseOrganization);
									}
								}
								
								CourseLocalServiceUtil.deleteCourse(course);
								window.showNotification("Course deleted successfully!");
								loadCourseTable();
							} catch (SystemException e) {
								e.printStackTrace();
							}
						}else {
							window.showNotification("Please select a course!", Window.Notification.TYPE_ERROR_MESSAGE);
						}
						
						clearCourse();
					}
				});
				
				
			}
		});
		
		Button assignSubjectButton = new Button("Add Subject");
		assignSubjectButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				course = (Course) courseTable.getValue();
				if (course!=null) {
					allComponentGridLayout.setVisible(false);
					mainVerticalLayout.addComponent(courseSubjectContainer());
					course.setNew(false);
					showCourseSubject(course);
				} else{
					window.showNotification("Please select a course!", Window.Notification.TYPE_ERROR_MESSAGE);
				}
			}
		});
		
		Button assignOrganizationButton = new Button("Associate campus");
		assignOrganizationButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				course = (Course) courseTable.getValue();
				if (course!=null) {
					allComponentGridLayout.setVisible(false);
					mainVerticalLayout.addComponent(courseOrganizationContainer());
					course.setNew(false);
					showCourseOrganization(course);
				}else {
					window.showNotification("Please select a course!", Window.Notification.TYPE_ERROR_MESSAGE);
				}
			}
		});
		
		Label spacer = new Label();
		
		courseButtonRowLayout.addComponent(spacer);
		courseButtonRowLayout.addComponent(courseEditButton);
		courseButtonRowLayout.addComponent(courseDeleteButton);
		courseButtonRowLayout.addComponent(assignSubjectButton);
		courseButtonRowLayout.addComponent(assignOrganizationButton);
		
		courseButtonRowLayout.setExpandRatio(spacer, 1);
		
		courseTabelGridLayout.addComponent(courseButtonRowLayout, 0, 1, 2, 1);
		
		return courseTabelGridLayout;
	}
	
	public void showEditCourse(Course course){
		previouseCourseCode = course.getCourseCode();
		courseCodeField.setValue(course.getCourseCode());
		courseNameField.setValue(course.getCourseName());
		courseDescriptionArea.setValue(course.getDescription());
		
		try {
			sessionToken.setValue(null);
			List<CourseSession> courseSessionList = CourseSessionLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
			Object courseID = course.getCourseId();
			for (int i = 0; i < courseSessionList.size(); i++) {
				CourseSession cs = courseSessionList.get(i);
				if (courseID.equals(cs.getCourseId())) {
					sessionToken.addToken(cs.getSessionName());
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	public void loadCourseTable(){
		if (courseDataSource!=null) {
			courseDataSource.removeAllItems();
			try {
				List<Course> courses = CourseLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
				for (int i = 0; i < courses.size(); i++) {
					Course course = courses.get(i);
					course.getCourseCode();
					course.getCourseName();
					course.getDescription();
					courseDataSource.addBean(course);
				}
				
				courseTable.refreshRowCache();
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void clearCourse(){
		course = null;
		courseCodeMessageLabel.setCaption("");
		courseCodeMessageLabel.setVisible(false);
		courseCodeField.setValue("");
		courseNameField.setValue("");
		courseDescriptionArea.setValue("");
		
		courseSession = null;
		sessionToken.setValue(null);
		courseTable.refreshRowCache();
		courseTable.setValue(null);
	}
	
	private GridLayout subjectTable(){
		subjectTableGridLayout = new GridLayout(3, 2);
		subjectTableGridLayout.setImmediate(false);
		subjectTableGridLayout.setSpacing(true);
		subjectTableGridLayout.setWidth("100%");
		subjectTableGridLayout.setMargin(false);
		subjectTableGridLayout.setSpacing(true);
		
		// subjectTable
		subjectTable = new Table("Subject Table", subjectDataSource);
		subjectTable.setImmediate(false);
		subjectTable.setWidth("100.0%");
		subjectTable.setSelectable(true);
		subjectTableGridLayout.addComponent(subjectTable, 0, 0, 2, 0);
		
		subjectTable.setColumnHeader(SUBJECT_CODE, "Subject Code");
		subjectTable.setColumnHeader(SUBJECT_NAME, "Subject Name");
//		subjectTable.setColumnHeader(SUBJECT_DESCRIPTION, "Description");
		
		subjectTable.setVisibleColumns(new String[]{SUBJECT_CODE, SUBJECT_NAME});
		
		Button editSubjectButton = new Button("Edit");
		editSubjectButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				subject = (Subject) subjectTable.getValue();
				if (subject!=null) {
					subject.setNew(false);
					showEditSubject(subject);
				} else{
					window.showNotification("Please select a subject!", Window.Notification.TYPE_ERROR_MESSAGE);
				}
			}
		});
		
		Button deleteSubjectButton = new Button("Delete");
		deleteSubjectButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				
				ConfirmDialog.show(window, "Are you sure you want to delete this record?", new ConfirmDialog.Listener() {
					
					@Override
					public void onClose(ConfirmDialog arg0) {
						subject = (Subject) subjectTable.getValue();
						if (subject!=null) {
							try {
								Object subjectId = subject.getSubjectId();
								SubjectLocalServiceUtil.deleteSubject(subject);
								
								List<CourseSubject> courseSubList = CourseSubjectLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
								
								for (int i = 0; i < courseSubList.size(); i++) {
									CourseSubject cs = courseSubList.get(i);
									Object id = cs.getSubjectId();
									if (subjectId.equals(id)) {
										CourseSubjectLocalServiceUtil.deleteCourseSubject(cs);
									}
								}
								
								window.showNotification("Subject deleted successfully");
								loadSubjectTable();
								clearSubject();
							} catch (SystemException e) {
								e.printStackTrace();
							}
						}else {
							window.showNotification("Please select subject", Window.Notification.TYPE_ERROR_MESSAGE);
						}
					}
				});
				
				
			}
		});
		
		HorizontalLayout subjectButtonRowLayout = new HorizontalLayout();
		subjectButtonRowLayout.setSpacing(true);
		subjectButtonRowLayout.setWidth("100%");
		
		Label spacer = new Label();
		
		subjectButtonRowLayout.addComponent(spacer);
		subjectButtonRowLayout.addComponent(editSubjectButton);
		subjectButtonRowLayout.addComponent(deleteSubjectButton);
		
		subjectButtonRowLayout.setExpandRatio(spacer, 1);
		
		subjectTableGridLayout.addComponent(subjectButtonRowLayout, 0, 1, 2, 1);
		
		return subjectTableGridLayout;
	}
	
	public void loadSubjectTable(){
		if (subjectDataSource!=null) {
			subjectDataSource.removeAllItems();
			try {
				List<Subject> subjects = SubjectLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
				for (int i = 0; i < subjects.size(); i++) {
					Subject subject = subjects.get(i);
					subject.getSubjectCode();
					subject.getSubjectName();
					subject.getDescription();
					subjectDataSource.addItem(subject);
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void showEditSubject(Subject subject){
		previousSubjectCode = subject.getSubjectCode();
		subjectCodeField.setValue(subject.getSubjectCode());
		subjectNameField.setValue(subject.getSubjectName());
		subjectDescriptionArea.setValue(subject.getDescription());
	}
	
	public void clearSubject(){
		subject = null;
		subjectCodeMessageLabel.setCaption("");
		subjectCodeMessageLabel.setVisible(false);
		subjectCodeField.setValue("");
		subjectNameField.setValue("");
		subjectDescriptionArea.setValue("");
		
		subjectTable.refreshRowCache();
		subjectTable.setValue(null);
	}
	
	private Label courseTitleLabel;
	private TwinColSelect subjectColSelect;
	private Button addCourseSubjectButton;
	
	public VerticalLayout courseSubjectContainer(){
		courseSubjectContainerVerticalLayout = new VerticalLayout();
		courseSubjectContainerVerticalLayout.setSpacing(true);
		
		courseTitleLabel = new Label();
		
		courseSubjectContainerVerticalLayout.addComponent(courseTitleLabel);
		
		Button backButton = new Button("<< Back");
		backButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				allComponentGridLayout.setVisible(true);
				courseSubjectContainerVerticalLayout.setVisible(false);
				course = null;
			}
		});
		courseSubjectContainerVerticalLayout.addComponent(backButton);
		
		subjectColSelect = new TwinColSelect();
		subjectColSelect.setWidth("80%");
		
		
		subjectColSelect.setLeftColumnCaption("Available Subject");
		subjectColSelect.setRightColumnCaption("Selected Subject");
		
		courseSubjectContainerVerticalLayout.addComponent(subjectColSelect);
		
		addCourseSubjectButton = new Button("Assign Subject");
		
		courseSubjectContainerVerticalLayout.addComponent(addCourseSubjectButton);
		
		return courseSubjectContainerVerticalLayout;
	}
	
	private Label courseOrgTitleLabel;
	private TwinColSelect organizationColSelect;
	private Button addCourseOrganizationButton;
	
	public VerticalLayout courseOrganizationContainer(){
		courseOrganizationContainerVerticalLayout = new VerticalLayout();
		courseOrganizationContainerVerticalLayout.setSpacing(true);
		
		courseOrgTitleLabel = new Label();
		
		courseOrganizationContainerVerticalLayout.addComponent(courseOrgTitleLabel);
		
		Button backButton = new Button("<< Back");
		backButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				allComponentGridLayout.setVisible(true);
				courseOrganizationContainerVerticalLayout.setVisible(false);
				course = null;
			}
		});
		courseOrganizationContainerVerticalLayout.addComponent(backButton);
		
		organizationColSelect = new TwinColSelect();
		organizationColSelect.setWidth("80%");
		
		organizationColSelect.setLeftColumnCaption("Available Organization");
		organizationColSelect.setRightColumnCaption("Selected Organization");
		
		courseOrganizationContainerVerticalLayout.addComponent(organizationColSelect);
		
		addCourseOrganizationButton = new Button("Add Organization");
		
		courseOrganizationContainerVerticalLayout.addComponent(addCourseOrganizationButton);
		
		return courseOrganizationContainerVerticalLayout;
	}
	
	private CourseSubject courseSubject;
	private List preSubjectList;
	private List<Long> preSelectedList;
	private List<CourseSubject> filterCourseList;
	
	private void loadSubjectTwinCol(Course course){
		courseTitleLabel.setCaption(course.getCourseName());
		try {
			//available and selected subject
			List<CourseSubject> courseSubList = CourseSubjectLocalServiceUtil.getCourseSubjects(0, CourseSubjectLocalServiceUtil.getCourseSubjectsCount());
			filterCourseList = new ArrayList<CourseSubject>();
			preSubjectList = new ArrayList();
			for (int i = 0; i < courseSubList.size(); i++) {
				CourseSubject courseSubject = courseSubList.get(i);
				Object courseId = courseSubject.getCourseId();
				Object courseObj = course.getCourseId();
				if (courseObj.equals(courseId)) {
					filterCourseList.add(courseSubject);
					preSubjectList.add(courseSubject.getSubjectId());
				}
			}
									
			List<Subject> subList = SubjectLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
			
			preSelectedList = new ArrayList<Long>();
			
			for (int i = 0; i < subList.size(); i++) {
				Subject sub = subList.get(i);
				long id = sub.getSubjectId();
				if (checkSubject(id)==true) {
					preSelectedList.add(id);
				}
			}
			
			for (int i = 0; i < subList.size(); i++) {
				Subject sub = subList.get(i);
				subjectColSelect.addItem(sub.getSubjectId());
				subjectColSelect.setItemCaption(sub.getSubjectId(), sub.getSubjectName());
			}
			subjectColSelect.setValue(preSelectedList);
			
			
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
	}
	
	public void showCourseSubject(final Course course){
		loadSubjectTwinCol(course);
		addCourseSubjectButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
								
				List<Long> selectedList = new ArrayList<Long>();
				
				Collection<Long> selected = (Collection<Long>) subjectColSelect.getValue();
				
				for(Long subject:selected){
					selectedList.add(subject);
				}
				courseSubject = new CourseSubjectImpl();
				Set<Long> newList = new HashSet<Long>();
				
				courseSubject.setCompanyId(themeDisplay.getCompanyId());
				/*try {
					courseSubject.setOrganizationId(themeDisplay.getLayout().getGroup().getOrganizationId());
				} catch (PortalException e1) {
					e1.printStackTrace();
				} catch (SystemException e1) {
					e1.printStackTrace();
				}*/
				courseSubject.setUserId(themeDisplay.getUser().getUserId());
				courseSubject.setUserName(themeDisplay.getUser().getScreenName());
				courseSubject.setCreateDate(new Date());
				try {
					if (preSubjectList.size()==0) {
						for (int i = 0; i < selectedList.size(); i++) {
							Long id = selectedList.get(i);
							
							courseSubject.setSubjectId(id);
							courseSubject.setCourseId(course.getCourseId());
							
							CourseSubjectLocalServiceUtil.addCourseSubject(courseSubject);
							window.showNotification("Subject added successfully");
							
						}
					}else if(selected.size()==0){
						for (int i = 0; i < filterCourseList.size(); i++) {
							CourseSubject cs = filterCourseList.get(i);

							CourseSubjectLocalServiceUtil.deleteCourseSubject(cs);
							window.showNotification("All subject is removed!");
						}										
					}else{
						for (int i = 0; i <filterCourseList.size(); i++) {
							CourseSubject cs = filterCourseList.get(i);
							
							CourseSubjectLocalServiceUtil.deleteCourseSubject(cs);
						}
						
						for (int i = 0; i <selectedList.size(); i++) {
							Long id = selectedList.get(i);
							
							courseSubject.setSubjectId(id);
							courseSubject.setCourseId(course.getCourseId());
							CourseSubjectLocalServiceUtil.addCourseSubject(courseSubject);
							window.showNotification("Subject added successfully");
						}
					}
					loadSubjectTwinCol(course);
				}catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});					
	}
	
	public Boolean checkSubject(long id){
		
		boolean status = false;
		for (int i = 0; i < preSubjectList.size(); i++) {
			Object sub = preSubjectList.get(i);
			if (sub.equals(id)) {
				status = true;
				break;
			}else{
				status = false;
			}
		}
		return status;
	}
	
	private List preOrganizationList;
	private CourseOrganization courseOrganization;
	private List<CourseOrganization> filterCourseOrganization;
	
	private void loadOrganizationTwinCol(Course course){
		courseOrgTitleLabel.setCaption(course.getCourseName());
		try {
			List<CourseOrganization> courseOrganizationList = CourseOrganizationLocalServiceUtil.findByCompany(themeDisplay.getCompanyId());
			filterCourseOrganization = new ArrayList<CourseOrganization>();
			preOrganizationList = new ArrayList();
			for (int i = 0; i < courseOrganizationList.size(); i++) {
				CourseOrganization courseOrganization = courseOrganizationList.get(i);
				Object courseId = courseOrganization.getCourseId();
				if (courseId.equals(course.getCourseId())) {
					filterCourseOrganization.add(courseOrganization);
					preOrganizationList.add(courseOrganization.getOrganizationId());
				}
			}
			
			List<Organization> orgList = OrganizationLocalServiceUtil.getOrganizations(themeDisplay.getCompanyId(), OrganizationConstants.ANY_PARENT_ORGANIZATION_ID, 
					0, OrganizationLocalServiceUtil.getOrganizationsCount());
			
			List<Long> preSelected = new ArrayList<Long>();
			for (int i = 0; i < orgList.size(); i++) {
				Organization organization = orgList.get(i);
				long id = organization.getOrganizationId();
				if (checkOrganization(id)) {
					preSelected.add(id);
				}
			}

			for (int i = 0; i < orgList.size(); i++) {
				Organization organization = orgList.get(i);
				organizationColSelect.addItem(organization.getOrganizationId());
				organizationColSelect.setItemCaption(organization.getOrganizationId(), organization.getName());
			}
			organizationColSelect.setValue(preSelected);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	public void showCourseOrganization(final Course course){
		
		loadOrganizationTwinCol(course);
		
		addCourseOrganizationButton.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				
				List<Long> selectedList = new ArrayList<Long>();
				Collection<Long> selected = (Collection<Long>) organizationColSelect.getValue();
				
				for(Long organization:selected){
					selectedList.add(organization);
				}
				courseOrganization = new CourseOrganizationImpl();
				Set<Long> newList = new HashSet<Long>();
				
				courseOrganization.setCompanyId(themeDisplay.getCompanyId());
				courseOrganization.setUserId(themeDisplay.getUser().getUserId());
				courseOrganization.setUserName(themeDisplay.getUser().getScreenName());
				courseOrganization.setCreateDate(new Date());
				
				try{
					if (preOrganizationList.size()==0) {
						for (int i = 0; i < selectedList.size(); i++) {
							Long id = selectedList.get(i);
							courseOrganization.setCourseId(course.getCourseId());
							courseOrganization.setOrganizationId(id);
							CourseOrganizationLocalServiceUtil.addCourseOrganization(courseOrganization);
							window.showNotification("Organization added successfully");
						}
					}else if (selected.size()==0) {
						for (int i = 0; i < filterCourseOrganization.size(); i++) {
							CourseOrganization courseOrg = filterCourseOrganization.get(i);
							CourseOrganizationLocalServiceUtil.deleteCourseOrganization(courseOrg);
							window.showNotification("All organization is removed");
						}
					}else{
						for (int i = 0; i < filterCourseOrganization.size(); i++) {
							CourseOrganization courseOrg = filterCourseOrganization.get(i);
							CourseOrganizationLocalServiceUtil.deleteCourseOrganization(courseOrg);
						}
						for (int i = 0; i < selectedList.size(); i++) {
							Long id = selectedList.get(i);
							courseOrganization.setCourseId(course.getCourseId());
							courseOrganization.setOrganizationId(id);
							CourseOrganizationLocalServiceUtil.addCourseOrganization(courseOrganization);
							window.showNotification("Organization added successfully");
						}
					}
					loadOrganizationTwinCol(course);
				}catch (SystemException e) {
					e.printStackTrace();
				} 
			}
		});
		
	}
	
	public Boolean checkOrganization(long id){
		boolean status = false;
		for (int i = 0; i < preOrganizationList.size(); i++) {
			Object organization = preOrganizationList.get(i);
			if (organization.equals(id)) {
				status = true;
				break;
			}else{
				status = false;
			}
		}
		return status;
	}
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

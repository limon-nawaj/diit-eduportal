package info.diit.portal.studentAttendance;

import info.diit.portal.NoSuchAttendanceException;
import info.diit.portal.classroutine.CustomTimeField;
import info.diit.portal.dto.BatchDto;
import info.diit.portal.dto.OrganizationDto;
import info.diit.portal.dto.StudentDto;
import info.diit.portal.dto.SubjectDto;
import info.diit.portal.dto.TopicDto;
import info.diit.portal.model.Attendance;
import info.diit.portal.model.AttendanceTopic;
import info.diit.portal.model.Batch;
import info.diit.portal.model.BatchStudent;
import info.diit.portal.model.BatchSubject;
import info.diit.portal.model.ClassRoutineEvent;
import info.diit.portal.model.ClassRoutineEventBatch;
import info.diit.portal.model.LessonTopic;
import info.diit.portal.model.Student;
import info.diit.portal.model.StudentAttendance;
import info.diit.portal.model.Subject;
import info.diit.portal.model.SubjectLesson;
import info.diit.portal.model.Topic;
import info.diit.portal.model.impl.AttendanceImpl;
import info.diit.portal.model.impl.AttendanceTopicImpl;
import info.diit.portal.model.impl.StudentAttendanceImpl;
import info.diit.portal.service.AttendanceLocalServiceUtil;
import info.diit.portal.service.AttendanceTopicLocalServiceUtil;
import info.diit.portal.service.BatchLocalServiceUtil;
import info.diit.portal.service.BatchStudentLocalServiceUtil;
import info.diit.portal.service.BatchSubjectLocalServiceUtil;
import info.diit.portal.service.ClassRoutineEventBatchLocalServiceUtil;
import info.diit.portal.service.ClassRoutineEventLocalServiceUtil;
import info.diit.portal.service.LessonTopicLocalServiceUtil;
import info.diit.portal.service.StudentAttendanceLocalServiceUtil;
import info.diit.portal.service.StudentLocalServiceUtil;
import info.diit.portal.service.SubjectLessonLocalServiceUtil;
import info.diit.portal.service.SubjectLocalServiceUtil;
import info.diit.portal.service.TopicLocalServiceUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Organization;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class StudentAttedanceApp extends Application implements PortletRequestListener{

	private Window						window;
	private Application					app;
	private VerticalLayout				verticalLayout;
	
	private List<Organization> userOrganizationList;
	private ThemeDisplay themeDisplay;
	private List<OrganizationDto> campusList;
	private List<BatchDto> batchList;
	
	private Attendance attendence;
	private AttendanceTopic attendenceTopic;
	private StudentAttendance studentAttendance;
	
	private final static String hourFormate = "hh:mm a";

	public void init() {
		window 			= new Window();
        app				=this;
		setMainWindow(window);
		
		try {
			userOrganizationList = themeDisplay.getUser().getOrganizations();
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		/*verticalLayout 	= new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.setSpacing(true);*/
		
		if (userOrganizationList.size()>1) {
			loadCampus();
			window.addComponent(campusLayout());
			window.addComponent(attendenceLayout());
		}else if (userOrganizationList.size()==1) {
			window.addComponent(attendenceLayout());
			loadBatch();
			batchComboBox();
		}
//		verticalLayout.addComponent(attendenceLayout());
	}

	private GridLayout					attendenceLayout;
	private DateField					attendenceDate;
	private ComboBox					batchComboBox;
	private DateField					routineInField;
	private DateField					routineOutField;
	private CustomTimeField				actualInField;
	private CustomTimeField				actualOutField;
	private TextField					routineDurationField;
	private TextField					actualDurationField;
	private TextField					lateEntryField;
	private TextField					lagTimeField;
	private ComboBox					subjectComboBox;
	private TextField					attendenceRateField;
	private Table						studentTable;
	private Table						topicTable;
	private TextArea					noteField;
	private ComboBox 					campusComboBox;
	
	private Button						findButton;
	private Button						saveButton;
	private Button						updateButton;
	private Button						deleteButton;
	private Button						clearButton;
	
	private final static String COLUMN_STUDENT_PRESENT  = "present";
	private final static String COLUMN_STUDENT_ID 		= "studentId";
	private final static String COLUMN_STUDENT_NAME 	= "studentName";
	
	private final static String COLUMN_SELECTION 		= "selection";
	private final static String COLUMN_TOPIC 			= "topic";
	private final static String COLUMN_DESCRIPTION 		= "description";
	
	private BeanItemContainer<StudentDto>	studentBeanContainer;
	private BeanItemContainer<TopicDto>		topicBeanContainer;
	
	private GridLayout attendenceLayout() {
		attendenceLayout = new GridLayout(4,15);
		attendenceLayout.setWidth("100%");
		attendenceLayout.setSpacing(true);
		
		Label	campusLabel		 			= new Label("Attendance Date :");
		Label	attendenceDateLabel		 	= new Label("Attendence Date :");
		Label	batchComboBoxLabel			= new Label("Batch :");	
		Label	routineInLabel				= new Label("Routine In :");
		Label	routineOutLabel				= new Label("Routine Out :");
		Label	actualInLabel				= new Label("Actual In :");
		Label	actualOutLabel				= new Label("Actual Out :");
		Label	routineDurationLabel		= new Label("Routine Duration :");
		Label	actualDurationLabel			= new Label("Actual Duration :");
		Label	lateEntryLabel				= new Label("Late Entry :");
		Label	subjectLabel				= new Label("Subject :");
		Label	attendenceRateLabel			= new Label("Attendence Rate :");
		Label	lagTime						= new Label("Lag Time :");
		
		attendenceDate 			= new DateField();
		batchComboBox 			= new ComboBox();
		routineInField 			= new DateField();
		routineOutField 		= new DateField();
		actualInField 			= new CustomTimeField();
		actualOutField 			= new CustomTimeField();
		routineDurationField 	= new TextField();
		actualDurationField 	= new TextField();
		lateEntryField 			= new TextField();
		subjectComboBox 		= new ComboBox();
		attendenceRateField 	= new TextField();
		lagTimeField 			= new TextField();
		noteField				= new TextArea("Note");
		
		routineInField.setLocale(Locale.ROOT);
		routineOutField.setLocale(Locale.ROOT);
		actualInField.setLocale(Locale.ROOT);
		actualOutField.setLocale(Locale.ROOT);
		
		routineInField.setTabIndex(3);
		routineOutField.setTabIndex(3);
		actualInField.setTabIndex(3);
		actualOutField.setTabIndex(3);
		
		batchComboBox.setImmediate(true);
		subjectComboBox.setImmediate(true);
		routineInField.setImmediate(true);
		routineOutField.setImmediate(true);
		actualInField.setImmediate(true);
		actualOutField.setImmediate(true);
		attendenceDate.setImmediate(true);
		
		attendenceDate.setWidth("100%");
		batchComboBox.setWidth("100%");
		routineInField.setWidth("100%");
		routineOutField.setWidth("100%");
		actualInField.setWidth("100%");
		actualOutField.setWidth("100%");
		routineDurationField.setWidth("100%");
		actualDurationField.setWidth("100%");
		lateEntryField.setWidth("100%");
		subjectComboBox.setWidth("100%");
		attendenceRateField.setWidth("100%");
		lagTimeField.setWidth("100%");
		noteField.setWidth("100%");
		
		routineInField.setDateFormat(hourFormate);
		routineOutField.setDateFormat(hourFormate);
		
		routineInField.setReadOnly(true);
		routineOutField.setReadOnly(true);
		routineDurationField.setReadOnly(true);
		lateEntryField.setReadOnly(true);
		lagTimeField.setReadOnly(true);
		attendenceRateField.setReadOnly(true);
		
		subjectComboBox.setRequired(true);
		actualInField.setRequired(true);
		actualOutField.setRequired(true);
		
		attendenceDate.setDateFormat("dd/MM/yyyy");
		attendenceDate.setValue(new Date());
		
		if (userOrganizationList.size()>1) {
			campusComboBox.addListener(new ValueChangeListener() {
				
				@Override
				public void valueChange(ValueChangeEvent event) {
					batchComboBox.removeAllItems();
					loadBatch();
					batchComboBox();
				}
			});
		}
		
		attendenceDate.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				attendence = null;
				studentAttendance = null;
				attendenceTopic = null;
				loadStudent();
				loadForm();
			}
		});
		
		batchComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				attendence = null;
				studentAttendance = null;
				attendenceTopic = null;
				subjectComboBox.removeAllItems();
				BatchDto batch = (BatchDto) batchComboBox.getValue();
				try {
					if (batch!=null) {
						List<BatchSubject> batchSubjectList = BatchSubjectLocalServiceUtil.findSubjectByBatch(batch.getBatchId());
						Set<Long> subjectIds = new HashSet<Long>();
						if (batchSubjectList!=null) {
							for (BatchSubject batchSubject : batchSubjectList) {
								subjectIds.add(batchSubject.getSubjectId());
							}
						}
						
						if (subjectIds!=null) {
							for (Long id : subjectIds) {
								SubjectDto subjectDto = new SubjectDto();
								Subject subject = SubjectLocalServiceUtil.fetchSubject(id);
								subjectDto.setSubjectId(subject.getSubjectId());
								subjectDto.setSubjcetName(subject.getSubjectName());
								subjectComboBox.addItem(subjectDto);
							}
						}
					}
					loadStudent();
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
		
		subjectComboBox.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				attendence = null;
				studentAttendance = null;
				attendenceTopic = null;
				loadTopic();
				routineTime();
				loadForm();
				loadStudent();
				attendenceRateField.setReadOnly(false);
				attendenceRateField.setValue("");
				attendenceRateField.setReadOnly(true);
			}
		});
		
		actualOutField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				actualDurationField.setReadOnly(false);
				actualDuration();
			}
		});
		
		actualInField.addListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				actualDurationField.setReadOnly(false);
				actualDuration();
			}
		});
		
		attendenceLayout.addComponent(attendenceDateLabel,0,0,0,0);		attendenceLayout.addComponent(attendenceDate,1,0,1,0);
		attendenceLayout.addComponent(batchComboBoxLabel,0,1,0,1);		attendenceLayout.addComponent(batchComboBox,1,1,1,1);
		attendenceLayout.addComponent(routineInLabel,0,2,0,2);			attendenceLayout.addComponent(routineInField,1,2,1,2);
		attendenceLayout.addComponent(actualInLabel,0,3,0,3);			attendenceLayout.addComponent(actualInField,1,3,1,3);
		attendenceLayout.addComponent(routineDurationLabel,0,4,0,4);	attendenceLayout.addComponent(routineDurationField,1,4,1,4);
		attendenceLayout.addComponent(lateEntryLabel,0,5,0,5);			attendenceLayout.addComponent(lateEntryField,1,5,1,5);
		
		attendenceLayout.addComponent(attendenceRateLabel,2,0,2,0);		attendenceLayout.addComponent(attendenceRateField,3,0,3,0);
		attendenceLayout.addComponent(subjectLabel,2,1,2,1);			attendenceLayout.addComponent(subjectComboBox,3,1,3,1);
		attendenceLayout.addComponent(routineOutLabel,2,2,2,2);			attendenceLayout.addComponent(routineOutField,3,2,3,2);
		attendenceLayout.addComponent(actualOutLabel,2,3,2,3);			attendenceLayout.addComponent(actualOutField,3,3,3,3);
		attendenceLayout.addComponent(actualDurationLabel,2,4,2,4);		attendenceLayout.addComponent(actualDurationField,3,4,3,4);
		attendenceLayout.addComponent(lagTime,2,5,2,5);					attendenceLayout.addComponent(lagTimeField,3,5,3,5);
		
		
		studentBeanContainer 	= new BeanItemContainer<StudentDto>(StudentDto.class);
		topicBeanContainer		= new BeanItemContainer<TopicDto>(TopicDto.class);
		
		studentTable 			= new Table("",studentBeanContainer);
		topicTable 				= new Table("",topicBeanContainer);
		
		studentTable.setWidth("100%");
		studentTable.setImmediate(true);
		studentTable.setEditable(true);
		
		studentTable.setColumnHeader(COLUMN_STUDENT_PRESENT, "#");
		studentTable.setColumnHeader(COLUMN_STUDENT_ID, "Student Id");
		studentTable.setColumnHeader(COLUMN_STUDENT_NAME, "Student Name");		
		studentTable.setVisibleColumns(new String[]{COLUMN_STUDENT_PRESENT, COLUMN_STUDENT_ID,COLUMN_STUDENT_NAME});		
		
		topicTable.setWidth("100%");
		topicTable.setImmediate(true);
		topicTable.setEditable(true);
		
		topicTable.setColumnHeader(COLUMN_SELECTION, "#");
		topicTable.setColumnHeader(COLUMN_TOPIC, "Topic");
		topicTable.setColumnHeader(COLUMN_DESCRIPTION, "Description");		
		topicTable.setVisibleColumns(new String[]{COLUMN_SELECTION, COLUMN_TOPIC, COLUMN_DESCRIPTION});
		
		topicTable.setColumnExpandRatio(COLUMN_SELECTION, 1);
		topicTable.setColumnExpandRatio(COLUMN_TOPIC, 4);
		topicTable.setColumnExpandRatio(COLUMN_DESCRIPTION, 4);
		
		topicTable.setTableFieldFactory(new DefaultFieldFactory(){
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				if (propertyId.equals(COLUMN_SELECTION)) {
					CheckBox field = new CheckBox();
					field.setReadOnly(false);
					field.setWidth("100%");
					return field;
				}else if (propertyId.equals(COLUMN_TOPIC)) {
					TextField field = new TextField();
					field.setReadOnly(true);
					field.setWidth("100%");
					field.setNullRepresentation("");
					return field;
				}else if (propertyId.equals(COLUMN_DESCRIPTION)) {
					TextField field = new TextField();
					field.setReadOnly(false);
					field.setWidth("100%");
					field.setNullRepresentation("");
					return field;
				}
				
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
		
		studentTable.setTableFieldFactory(new DefaultFieldFactory(){
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				
				if (propertyId.equals(COLUMN_STUDENT_PRESENT)) {
					CheckBox field = new CheckBox();
					field.setReadOnly(false);
					field.setWidth("100%");
					field.setImmediate(true);
					field.addListener(new ValueChangeListener() {
						
						@Override
						public void valueChange(ValueChangeEvent event) {
							loadAttendanceRate();
						}
					});
					return field;
				}else if (propertyId.equals(COLUMN_STUDENT_ID)) {
					TextField field = new TextField();
					field.setReadOnly(true);
					field.setWidth("100%");
					field.setNullRepresentation("");
					return field;
				}else if (propertyId.equals(COLUMN_STUDENT_NAME)) {
					TextField field = new TextField();
					field.setReadOnly(true);
					field.setWidth("100%");
					field.setNullRepresentation("");
					return field;
				}
				return super.createField(container, itemId, propertyId, uiContext);
			}
			
		});
		
		findButton 				= new Button("Find");
		saveButton 				= new Button("Save");
		updateButton 			= new Button("Update");
		deleteButton 			= new Button("Delete");
		clearButton 			= new Button("Clear");
				
		saveButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (attendence==null) {
					attendence = new AttendanceImpl();
					attendence.setNew(true);
				}
				attendence.setCompanyId(themeDisplay.getCompanyId());
				attendence.setUserId(themeDisplay.getUserId());
				attendence.setUserName(themeDisplay.getUser().getFullName());
				
				if (userOrganizationList.size()>1) {
					OrganizationDto campus = (OrganizationDto) campusComboBox.getValue();
					if (campus!=null) {
						attendence.setCampus(campus.getOrganizationId());
					}
				} else {
					Organization org = userOrganizationList.get(0);
					attendence.setCampus(org.getOrganizationId());
				}
				
				Date attendDate = (Date) attendenceDate.getValue();
				if (attendDate!=null) {
					attendDate.setHours(0);
					attendDate.setMinutes(0);
					attendDate.setSeconds(0);
					attendence.setAttendanceDate(attendDate);
				}
				
				BatchDto batchDto = (BatchDto) batchComboBox.getValue();
				if (batchDto!=null) {
					attendence.setBatch(batchDto.getBatchId());
				}
				
				SubjectDto subjectDto = (SubjectDto) subjectComboBox.getValue();
				if (subjectDto!=null) {
					attendence.setSubject(subjectDto.getSubjectId());
					Date routineInDate = (Date) routineInField.getValue();
					if (routineInDate!=null) {
						attendence.setRoutineIn(routineInDate);
					}
					
					Date routineOutDate = (Date) routineOutField.getValue();
					if (routineOutDate!=null) {
						attendence.setRoutineOut(routineOutDate);
					}
					
					attendence.setRoutineDuration(routineMinutes);
				}else {
					window.showNotification("Please select a subject!", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				Date actualInDate = (Date) actualInField.getValue();
				Date actualOutDate = (Date) actualOutField.getValue();
				
				if (actualInDate!=null && actualOutDate!=null) {
					attendence.setActualIn(actualInDate);
					attendence.setActualOut(actualOutDate);
					
					attendence.setRoutineDuration(routineMinutes);
					attendence.setActualDuration(actualMinutes);
					attendence.setLateEntry(lateEntryMinutes);
					attendence.setLagTime(lagMinutes);
				}else {
					window.showNotification("Please select actual time!", Window.Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				attendence.setAttendanceRate((long) rate);
				
				try {
					if (attendence.isNew()) {
						attendence.setCreateDate(new Date());
						AttendanceLocalServiceUtil.addAttendance(attendence);
						saveStudent(attendence);
						saveTopic(attendence);
						window.showNotification("Attedence saved successfully");
					}else{
						
						attendDate.setHours(0);
						attendDate.setMinutes(0);
						attendDate.setSeconds(0);
						
						if (attendDate==attendence.getAttendanceDate() && batchDto.getBatchId()==attendence.getBatch() && subjectDto.getSubjectId()==attendence.getSubject()) {
							AttendanceLocalServiceUtil.updateAttendance(attendence);
							saveStudent(attendence);
							saveTopic(attendence);
							window.showNotification("Attedence updated successfully");
						}else{
							attendence.setCreateDate(new Date());
							AttendanceLocalServiceUtil.addAttendance(attendence);
							saveStudent(attendence);
							saveTopic(attendence);
							window.showNotification("Attedence saved successfully");
						}
						
						
					}
					
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
		});
		
		clearButton.addListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				clear();
			}
		});
		
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setSpacing(true);
		
		buttonLayout.addComponent(findButton);
		buttonLayout.addComponent(saveButton);
		buttonLayout.addComponent(updateButton);
		buttonLayout.addComponent(deleteButton);
		buttonLayout.addComponent(clearButton);
		
		attendenceLayout.addComponent(studentTable,0,6,1,10);
		attendenceLayout.addComponent(topicTable,2,6,3,10);
		attendenceLayout.addComponent(noteField,0,11,3,12);
		attendenceLayout.addComponent(buttonLayout,0,13,3,13);
		attendenceLayout.setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);
		
		return attendenceLayout;
	}
	
	private void loadForm(){
		Date attenDate = (Date) attendenceDate.getValue();
		SubjectDto subject = (SubjectDto) subjectComboBox.getValue();
		if (attenDate!=null && subject!=null) {
			attenDate.setHours(0);
			attenDate.setMinutes(0);
			attenDate.setSeconds(0);
			try {
				attendence = AttendanceLocalServiceUtil.findBySubjectDate(subject.getSubjectId(), attenDate);
			} catch (NoSuchAttendanceException e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		
		if (attendence!=null) {
			actualInField.setValue(attendence.getActualIn());
			actualOutField.setValue(attendence.getActualOut());
		}
	}
	
	/*1 for present and 0 for absence*/
	private void saveStudent(Attendance attendance){
		if (studentAttendance==null) {
			studentAttendance = new StudentAttendanceImpl();
			studentAttendance.setNew(true);
		}
		
		studentAttendance.setCompanyId(themeDisplay.getCompany().getCompanyId());
		studentAttendance.setUserId(themeDisplay.getUser().getUserId());
		studentAttendance.setUserName(themeDisplay.getUser().getFullName());
		studentAttendance.setCreateDate(new Date());
		
		Date attendDate = (Date) attendenceDate.getValue();
		if (attendDate!=null) {
			attendDate.setHours(0);
			attendDate.setMinutes(0);
			attendDate.setSeconds(0);
			studentAttendance.setAttendanceDate(attendDate);
		}
		
		
		studentAttendance.setAttendanceId(attendence.getAttendanceId());
		
		try {
			List<StudentAttendance> studentAttendances = StudentAttendanceLocalServiceUtil.findByAttendance(attendance.getAttendanceId());
			if (studentAttendances!=null) {
				for (StudentAttendance studentAttendance : studentAttendances) {
					
					
					StudentAttendanceLocalServiceUtil.deleteStudentAttendance(studentAttendance);
				}
			}
			
			if (studentBeanContainer!=null) {
				for (int i = 0; i < studentBeanContainer.size(); i++) {
					StudentDto student = studentBeanContainer.getIdByIndex(i);
					studentAttendance.setStudentId(student.getStudentId());
					if (student.isPresent()) {
						studentAttendance.setStatus(1);
					}else {
						studentAttendance.setStatus(0);
					}
					
					StudentAttendanceLocalServiceUtil.addStudentAttendance(studentAttendance);
				} 
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
	}
	
	private void saveTopic(Attendance attedence){
		if (attendenceTopic==null) {
			attendenceTopic = new AttendanceTopicImpl();
			attendenceTopic.setNew(true);
		}
		
		attendenceTopic.setCompanyId(themeDisplay.getCompany().getCompanyId());
		attendenceTopic.setUserId(themeDisplay.getUser().getUserId());
		attendenceTopic.setUserName(themeDisplay.getUser().getFullName());
		
		attendenceTopic.setAttendanceId(attendence.getAttendanceId());
		
		try {
			
			
			if (topicBeanContainer!=null) {
				for (int i = 0; i < topicBeanContainer.size(); i++) {
					List<AttendanceTopic> topicList = AttendanceTopicLocalServiceUtil.findByAttendence(attendence.getAttendanceId());
					if (topicList!=null) {
						for (AttendanceTopic attendenceTopic : topicList) {
							AttendanceTopicLocalServiceUtil.deleteAttendanceTopic(attendenceTopic);
						}
					}
				}
			
				for (int i = 0; i < topicBeanContainer.size(); i++) {
					TopicDto topicDto = topicBeanContainer.getIdByIndex(i);
					if (topicDto.isSelection()==true) {
						attendenceTopic.setTopicId(topicDto.getId());
						attendenceTopic.setDescription(topicDto.getDescription());
						attendenceTopic.setCreateDate(new Date());
						AttendanceTopicLocalServiceUtil.addAttendanceTopic(attendenceTopic);
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private GridLayout campusLayout(){
		GridLayout campusLayout = new GridLayout(4, 3);
		campusLayout.setSpacing(true);
		
		Label campusLabel = new Label("Campus");
		campusComboBox = new ComboBox();
		campusComboBox.setImmediate(true);
		
		if (campusList!=null) {
			for (OrganizationDto campus : campusList) {
				campusComboBox.addItem(campus);
			}
		}
		
		campusLayout.setWidth("100%");
		campusLabel.setWidth("100%");
		campusComboBox.setWidth("100%");
		
		campusLayout.addComponent(campusLabel, 0, 0, 0, 0);
		campusLayout.addComponent(campusComboBox, 1, 0, 1, 0);
		
		return campusLayout;
	}
	
	private void batchComboBox(){
		if (batchList!=null) {
			for (BatchDto batchDto : batchList) {
				batchComboBox.addItem(batchDto);
			}
		}
	}
	
	private void loadCampus(){
		campusList = new ArrayList<OrganizationDto>();
		for (Organization organization : userOrganizationList) {
			OrganizationDto campus = new OrganizationDto();
			campus.setOrganizationId(organization.getOrganizationId());
			campus.setOrganizationName(organization.getName());
			campusList.add(campus);
		}
	}
	
	private void loadBatch(){
		try {
			List<BatchSubject> batchSubjects = BatchSubjectLocalServiceUtil.findByBatchSubjectTeacher(themeDisplay.getUser().getUserId());
			if (batchSubjects!=null) {
				List<Batch> batchs = new ArrayList<Batch>();
				
				for (BatchSubject batchSubject : batchSubjects) {
					if (userOrganizationList.size()==1) {
						for (Organization organization : userOrganizationList) {
							if (organization.getOrganizationId()==batchSubject.getOrganizationId()) {
								Batch batch = BatchLocalServiceUtil.fetchBatch(batchSubject.getBatchId());
								batchs.add(batch);
							}
						}
					}else{
						OrganizationDto campus = (OrganizationDto) campusComboBox.getValue();
						if (campus!=null) {
							batchComboBox.removeAllItems();
							if (campus.getOrganizationId()==batchSubject.getOrganizationId()) {
								Batch batch = BatchLocalServiceUtil.fetchBatch(batchSubject.getBatchId());
								batchs.add(batch);
							}
						}else{
							batchComboBox.removeAllItems();
						}
					}
				}
				
				if (batchList!=null) {
					batchList.clear();
				}else{
					batchList = new ArrayList<BatchDto>();
				}
				
				if (batchs!=null) {
					for (Batch batch : batchs) {
						BatchDto batchDto = new BatchDto();
						batchDto.setBatchId(batch.getBatchId());
						batchDto.setBatchName(batch.getBatchName());
						batchList.add(batchDto);
					}
				}
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		} 
	}
	
	private void loadStudent(){
		studentTable.removeAllItems();
		BatchDto batch = (BatchDto) batchComboBox.getValue();
		try {
			if (batch!=null) {
				List<BatchStudent> batchStudentList = BatchStudentLocalServiceUtil.findByBatch(batch.getBatchId());
				if (batchStudentList!=null) {
					for (BatchStudent batchStudent : batchStudentList) {
						Student student = StudentLocalServiceUtil.fetchStudent(batchStudent.getStudentId());
						StudentDto studentDto = new StudentDto();
						studentDto.setStudentId(student.getStudentId());
						studentDto.setName(student.getName());
						if (compareStudent(student.getStudentId())) {
							studentDto.setPresent(true);
						}else {
							studentDto.setPresent(false);
						}
						if (batchStudent.getStatus()==1) {
							studentBeanContainer.addBean(studentDto);
						}
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private Boolean compareStudent(long studentId){
		boolean status = false;
		Date attenDate = (Date) attendenceDate.getValue();
//		BatchDto batch = (BatchDto) batchComboBox.getValue();
		SubjectDto subject = (SubjectDto) subjectComboBox.getValue();
		if (attendenceDate!=null && subject!=null) {
			attenDate.setHours(0);
			attenDate.setMinutes(0);
			attenDate.setSeconds(0);
			try {
				Attendance attend = AttendanceLocalServiceUtil.findBySubjectDate(subject.getSubjectId(), attenDate);
				if (attend!=null) {
					List<StudentAttendance> studentAttends = StudentAttendanceLocalServiceUtil.findByAttendance(attend.getAttendanceId());
					if (studentAttends!=null) {
						for (StudentAttendance studentAttendance : studentAttends) {
							if (studentAttendance.getStudentId()==studentId) {
								if (studentAttendance.getStatus()==1) {
									status = true;
									break;
								}
							}
							
						}
					}
				}
			} catch (SystemException e) {
				e.printStackTrace();
			} catch (NoSuchAttendanceException e) {
				e.printStackTrace();
			}
		}
		return status;
	}

	private void loadTopic(){
		topicTable.removeAllItems();
		SubjectDto subjectDto = (SubjectDto) subjectComboBox.getValue();
		BatchDto batchDto = (BatchDto) batchComboBox.getValue();
		try {
			if (subjectDto!=null && batchDto!=null) {
				
				List<SubjectLesson> subjectLessons = SubjectLessonLocalServiceUtil.findByBatchSubject(batchDto.getBatchId(), subjectDto.getSubjectId());
				
				Set<Long> topicIds = new HashSet<Long>();
				
				if (subjectLessons!=null) {
					for (SubjectLesson subjectLesson : subjectLessons) {
						List<LessonTopic> lessonTopicList = LessonTopicLocalServiceUtil.findByLessonPlan(subjectLesson.getLessonPlanId());
						if (lessonTopicList!=null) {
							for (LessonTopic lessonTopic : lessonTopicList) {
								if (compareTopic(lessonTopic.getTopic())==false) {
									topicIds.add(lessonTopic.getTopic());
								}
							}
						}
					}
				}
				
				if (topicIds.size()>0) {
					for (Long id : topicIds) {
						Topic topic = TopicLocalServiceUtil.fetchTopic(id);
						TopicDto topicDto = new TopicDto();
						topicDto.setId(topic.getTopicId());
						topicDto.setTopic(topic.getTopic());
						topicBeanContainer.addBean(topicDto);
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private boolean compareTopic(long topic){
		boolean status = false;
		try {
			SubjectDto subjectDto = (SubjectDto) subjectComboBox.getValue();
			BatchDto batchDto = (BatchDto) batchComboBox.getValue();
			if (subjectDto!=null && batchDto!=null) {
				List<Attendance> attendences = AttendanceLocalServiceUtil.findByUserBatchSubject(themeDisplay.getUserId(), batchDto.getBatchId(), subjectDto.getSubjectId());
				if (attendences.size()>0) {
					List<Long> attendenceIds = new ArrayList<Long>();
					if (attendences!=null) {
						for (Attendance attendence : attendences) {
							attendenceIds.add(attendence.getAttendanceId());
						}
					}
					
					if (attendenceIds!=null) {
						for (Long id : attendenceIds) {
							List<AttendanceTopic> attendenceTopicList = AttendanceTopicLocalServiceUtil.findByAttendence(id);
							
							if (attendenceTopicList!=null) {
								for (AttendanceTopic attendenceTopic : attendenceTopicList) {
									if (attendenceTopic.getTopicId()==topic) {
										status = true;
										break;
									}else{
										status = false;
									}
								}
							}
							
						}
					}
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return status;
	}
	
	int routineMinutes;
	
	private void routineTime(){
		routineInField.setReadOnly(false);
		routineOutField.setReadOnly(false);
		actualInField.setReadOnly(false);
		actualOutField.setReadOnly(false);
		routineDurationField.setReadOnly(false);
		
		routineInField.setValue(null);
		routineOutField.setValue(null);
		actualInField.setValue(null);
		actualOutField.setValue(null);
		routineDurationField.setValue("");
				
		Calendar cal = Calendar.getInstance();
		
		Date attenDate = (Date) attendenceDate.getValue();
		cal.setTime(attenDate);
		cal.setFirstDayOfWeek(Calendar.SUNDAY);
		
		if (attenDate!=null) {
			try {
				int day = cal.get(Calendar.DAY_OF_WEEK);
				SubjectDto subject = (SubjectDto) subjectComboBox.getValue();
				if (subject!=null) {
					List<ClassRoutineEvent> eventList = ClassRoutineEventLocalServiceUtil.findBySubujectDay(subject.getSubjectId(), day);
					if (eventList!=null) {
//						window.showNotification("Size: "+eventList.size());
						for (ClassRoutineEvent classRoutineEvent : eventList) {
							List<ClassRoutineEventBatch> eventBatchList = ClassRoutineEventBatchLocalServiceUtil.findEventBatchByEventId(classRoutineEvent.getClassRoutineEventId());
							BatchDto batch = (BatchDto) batchComboBox.getValue();
							if (eventBatchList!=null) {
								for (ClassRoutineEventBatch classRoutineEventBatch : eventBatchList) {
									if (classRoutineEventBatch.getBatchId()==batch.getBatchId()) {
										routineInField.setValue(classRoutineEvent.getStartTime());
										routineOutField.setValue(classRoutineEvent.getEndTime());
										
										/*actualInField.setValue(classRoutineEvent.getStartTime());
										actualOutField.setValue(classRoutineEvent.getEndTime());*/
										
										Date start = classRoutineEvent.getStartTime();
										long s = classRoutineEvent.getEndTime().getTime() - classRoutineEvent.getStartTime().getTime();
										routineMinutes = (int) (s/(1000*60));
										if (routineMinutes>=60) {
											int hours = routineMinutes/60;
											int min = routineMinutes%60;
											routineDurationField.setValue(hours+" hour(s) "+min+" min(s)");
										}else {
											routineDurationField.setValue(routineMinutes+" min(s)");
										}
									}
								}
							}
						}
						lagTime();
					}
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}else {
			window.showNotification("No date is selected");
		}
		
		routineInField.setReadOnly(true);
		routineOutField.setReadOnly(true);
		routineDurationField.setReadOnly(true);
	}
	
	int actualMinutes;
	
	private void actualDuration(){
		actualDurationField.setReadOnly(false);
		actualDurationField.setValue("");
		Date actualIn = (Date) actualInField.getValue();
		Date actualOut = (Date) actualOutField.getValue();
		if (actualIn!=null && actualOut!=null) {
			long outTime = actualOut.getTime();
			long inTime = actualIn.getTime();
			if (outTime>inTime) {
				long second = actualOut.getTime()-actualIn.getTime();
				actualMinutes = (int) (second/(1000*60));
				if (actualMinutes>=60) {
					int hours = actualMinutes/60;
					int min = actualMinutes%60;
					actualDurationField.setValue(hours+" hour(s) "+min+" min(s)");
				}else {
					actualDurationField.setValue(actualMinutes+" min(s)");
				}
				lagTime();
				lateEntry();
			}else {
				window.showNotification("Incorrect time", Window.Notification.TYPE_ERROR_MESSAGE);
			}
		}
		actualDurationField.setReadOnly(true);
	}
	
	int lagMinutes;
	
	private void lagTime(){
		lagTimeField.setReadOnly(false);
		lagTimeField.setValue("");
		if (routineMinutes>0 && actualMinutes>0) {
			lagMinutes = routineMinutes-actualMinutes;
			if (lagMinutes==0) {
				lagTimeField.setValue("On time");
			}else {
				if (lagMinutes>0) {
					if (lagMinutes>=60) {
						int hours = lagMinutes/60;
						int min = lagMinutes%60;
						lagTimeField.setValue(hours+" hour(s) "+min+" min(s) late");
					}else {
						lagTimeField.setValue(lagMinutes+" min(s) late");
					}
				}else {
					if (lagMinutes<=-60) {
						int hours = lagMinutes/60;
						int min = lagMinutes%60;
						lagTimeField.setValue(Math.abs(hours)+" hour(s) "+Math.abs(min)+" min(s) early");
					}else {
						lagTimeField.setValue(Math.abs(lagMinutes)+" min(s) early");
					}
				}
				
			}
		}
		lagTimeField.setReadOnly(true);
	}
	
	int lateEntryMinutes;
	
	private void lateEntry(){
		lateEntryField.setReadOnly(false);
		
		Date routineStart = (Date) routineInField.getValue();
		Date actualStart = (Date) actualInField.getValue();
		if (routineStart!=null && actualStart!=null) {
			long routineTime = routineStart.getTime();
			long actualTime = actualStart.getTime();
			if (actualTime>=routineTime) {
				long second = actualTime-routineTime;
				actualMinutes = (int) (second/(1000*60));
				if (actualMinutes>=60) {
					int hours = actualMinutes/60;
					int min = actualMinutes%60;
					lateEntryField.setValue(hours+" hour(s) "+min+" min(s)");
				}else {
					lateEntryField.setValue(actualMinutes+" min(s)");
				}
			}
		}
		
		lateEntryField.setReadOnly(true);
	}
	
	double rate = 0;
	
	private void loadAttendanceRate(){
		if (studentBeanContainer!=null) {
			
			int count = 0;
			for (int i = 0; i < studentBeanContainer.size(); i++) {
				StudentDto student = studentBeanContainer.getIdByIndex(i);
				if (student.isPresent()) {
					count++;
				}
			}
			
			if (count>0) {
				rate = (count*100)/studentBeanContainer.size();
			}
			attendenceRateField.setReadOnly(false);
			attendenceRateField.setValue(rate+"%");
			attendenceRateField.setReadOnly(true);
		}
	}
	
	private void clear(){
		attendence = null;
		studentAttendance = null;
		attendenceTopic = null;
	}
	
	public void onRequestStart(PortletRequest request, PortletResponse response) {
		
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}

}

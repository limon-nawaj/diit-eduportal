package info.diit.portal.batch;

import static info.diit.portal.constant.StudentConstant.COLUMN_BATCH_NAME;
import static info.diit.portal.constant.StudentConstant.COLUMN_BATCH_START_DATE;
import static info.diit.portal.constant.StudentConstant.COLUMN_CLASS_TEACHER;
import static info.diit.portal.constant.StudentConstant.COLUMN_COURSE;
import static info.diit.portal.constant.StudentConstant.COLUMN_END_DATE;
import static info.diit.portal.constant.StudentConstant.COLUMN_ORGANIZATION;
import static info.diit.portal.constant.StudentConstant.COLUMN_SESSION;
import static info.diit.portal.constant.StudentConstant.COLUMN_STATUS;
import static info.diit.portal.constant.StudentConstant.DATE_FORMAT;
import static info.diit.portal.constant.StudentConstant.TEACHER_GROUP_NAME;
import info.diit.portal.NoSuchStudentException;
import info.diit.portal.dto.BatchDto;
import info.diit.portal.dto.BatchStudentDto;
import info.diit.portal.dto.CourseDto;
import info.diit.portal.dto.SessionDto;
import info.diit.portal.dto.TeacherDto;
import info.diit.portal.dto.UserOrganizationsDto;
import info.diit.portal.model.Batch;
import info.diit.portal.model.BatchStudent;
import info.diit.portal.model.Course;
import info.diit.portal.model.CourseOrganization;
import info.diit.portal.model.CourseSession;
import info.diit.portal.model.Student;
import info.diit.portal.model.impl.BatchImpl;
import info.diit.portal.service.BatchLocalServiceUtil;
import info.diit.portal.service.BatchStudentLocalServiceUtil;
import info.diit.portal.service.CourseLocalServiceUtil;
import info.diit.portal.service.CourseOrganizationLocalServiceUtil;
import info.diit.portal.service.CourseSessionLocalServiceUtil;
import info.diit.portal.service.StudentLocalServiceUtil;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.vaadin.dialogs.ConfirmDialog;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component.Event;
import com.vaadin.ui.Component.Listener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class BatchManageApp extends Application implements PortletRequestListener{
	
	private TabSheet 									batchTabSheet;
	private GridLayout									initBatchLayout;	
	private Window 										window;
	
	private Batch 										batch;

	private Table										batchesSearchTable;
	private Table										batchesDetailTable;
	

	
	
	private ComboBox									organizationsComboBox;
	private ComboBox									courseComboBox;
	private DateField									batchStartDateField;
	private DateField									batchEndDateField;
	private TextField									batchNameField;
	private ComboBox									sessionComboBox;
	private ComboBox									classTeachersComboBox;
	private ComboBox									statusComboBox;
	private TextArea 									noteTextArea;
	private Button										saveButton;
	private Button										deleteButton;
	private Button										cancelButton;
	private Button										editButton;
	

	private User										user;
	private long										companyId;
	private long										organizationId;
	private ThemeDisplay								themeDisplay;
	private UserGroup 									teacherGroup;
	private List<User>									teacherList;
	
	private TeacherDto									changedClassteacherId;
	private TeacherDto									previusClassteacherId;
	private BatchDto 									selectedValue;
	
	private List<Organization> 							userOrganizationsList;
	private List<UserOrganizationsDto>					userOganizationsDtos;
	private List<CourseDto> 							courseDtoList;
	private List<SessionDto>							sessionDtoList;
	private List<TeacherDto> 							teacherDto;
	private List<Batch> 								batchesList;
	private static List<BatchDto> 						batchesDtoList;
	private Set<String> 								searchCoursesList;
	private Set<String> 								searchSessionList;
	

	BeanItemContainer<BatchDto> 						batchSearchBeanItemContainer;
	BeanItemContainer<BatchDto> 						batchDetailBeanItemContainer;

	public void init() {
		window = new Window();
		window.setSizeFull();

		setMainWindow(window);
		
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.setSpacing(true);

		
		user			=	themeDisplay.getUser();
		companyId		=	themeDisplay.getCompanyId();
		
		try {
			userOrganizationsList	=	user.getOrganizations();
			teacherGroup = UserGroupLocalServiceUtil.getUserGroup(companyId,TEACHER_GROUP_NAME);
			teacherList = UserLocalServiceUtil.getUserGroupUsers(teacherGroup.getUserGroupId());
			
		valueInitialization();
			
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		verticalLayout.addComponent(initBatchManageTabSheet());
		window.addComponent(verticalLayout);
	}
	
	private void valueInitialization(){
		batchesList = new ArrayList<Batch>();
		//searchCoursesList = new HashSet<String>();
		//searchSessionList = new HashSet<String>();
		try {
		for (Organization userOrg : userOrganizationsList){
			List<Batch> batches;
			
				batches = BatchLocalServiceUtil.findBatchesByComOrg(companyId, userOrg.getOrganizationId());
			
			//List<CourseOrganization> courseOrganizations = CourseOrganizationLocalServiceUtil.findByOrganization(userOrg.getOrganizationId());
			for(Batch batch : batches){
				batchesList.add(batch);
			}
			/*for(CourseOrganization courseOrganization : courseOrganizations){					
				long courseid = courseOrganization.getCourseId();
				
				List<CourseSession> courseSessions = CourseSessionLocalServiceUtil.findByCourseId(courseid);
				for(CourseSession courseSession :courseSessions ){
					searchSessionList.add(courseSession.getSessionName());
				}				
				String name  = getCourseNameById(courseid);
				searchCoursesList.add(name);
			}*/
		}
		} catch (SystemException e) {
			e.printStackTrace();
		}	
	}
	
	private TabSheet initBatchManageTabSheet()
	{
		batchTabSheet = new TabSheet();
		initBatchLayout = initBatchLayout();
		batchTabSheet.addTab(initBatchLayout,"Add");
		batchTabSheet.addTab(batchManageLayout(),"Manage Batches");
		//batchTabSheet.addTab(assignStudent(),"Assign Student");
		batchTabSheet.setWidth("100%");
		
		return batchTabSheet;
	}
	
	private GridLayout initBatchLayout() 
	{	
		//GridLayout (Column, Row)
		GridLayout gridLayout = new GridLayout(3,7);
		gridLayout.setSpacing(true);		
		gridLayout.setWidth("100%");
		
		organizationsComboBox = new ComboBox("Campuses");
		courseComboBox = new ComboBox("Course");
		sessionComboBox = new ComboBox("Session");
		classTeachersComboBox = new ComboBox("Batch Class Teacher");
		
		organizationsComboBox.setWidth("100%");
		courseComboBox.setWidth("100%");
		sessionComboBox.setWidth("100%");
		//classTeachersComboBox.setWidth("100%");
		
		//organizationsComboBox.setVisible(false);

		
		if (userOrganizationsList.size()>1) {
			//organizationsComboBox.setVisible(true);
			userOganizationsDtos = new ArrayList<UserOrganizationsDto>();
			for(Organization userOrg : userOrganizationsList){
				UserOrganizationsDto userOrgDto = new UserOrganizationsDto();
				userOrgDto.setOrgId(userOrg.getOrganizationId());
				userOrgDto.setOrgName(userOrg.getName());
				userOganizationsDtos.add(userOrgDto);
				organizationsComboBox.addItem(userOrgDto);
			}
			organizationsComboBox.setImmediate(true);			
		}
		else if(userOrganizationsList.size()==1){
			organizationsComboBox.setEnabled(false);
			
			userOganizationsDtos = new ArrayList<UserOrganizationsDto>();
			Organization userOrg = userOrganizationsList.get(0);
			organizationId  = userOrg.getOrganizationId();
			UserOrganizationsDto userOrgDto = new UserOrganizationsDto();
			userOrgDto.setOrgId(organizationId);
			userOrgDto.setOrgName(userOrg.getName());
			userOganizationsDtos.add(userOrgDto);
			getCourseDtoByOrgId(organizationId);
			loadTeacherComboBox();
			if (courseDtoList!=null) {	
				for(CourseDto courseDto: courseDtoList){					
					courseComboBox.addItem(courseDto);
				}
			}
		}
		else{
			organizationId = 0;
		}
		
		organizationsComboBox.addListener(new Listener() {			
			public void componentEvent(Event event) {
				// this method below will populate the courseCombobox according to the user belongs to org
				courseComboBox.removeAllItems();
				UserOrganizationsDto org = (UserOrganizationsDto)organizationsComboBox.getValue();				
				if(org!=null){
					organizationId = org.getOrgId();
					getCourseDtoByOrgId(organizationId);
					loadTeacherComboBox();
					if (courseDtoList!=null) {	
						for(CourseDto courseDto: courseDtoList){
							courseComboBox.addItem(courseDto);
						}
					}
				}
			}
		});				
		
		courseComboBox.setRequired(true);
		courseComboBox.setImmediate(true);		
		
		courseComboBox.addListener(new Listener() {
			public void componentEvent(Event event) {
				sessionComboBox.removeAllItems();
				CourseDto course = (CourseDto) courseComboBox.getValue();
				if(course!=null){
					getSessionDtoByCourseId(course.getCourseId());
					if (sessionDtoList!=null) {
						for(SessionDto sessionDto: sessionDtoList){
							sessionComboBox.addItem(sessionDto);
						}
					}
				}				
			}
		});
				
		
		sessionComboBox.setNewItemsAllowed(true);			
		
		batchNameField = new TextField("Batch Name");
		batchNameField.setRequired(true);
		batchNameField.setWidth("100%");
		
		
		batchStartDateField = new DateField("Start Date");
		//batchStartDateField.setWidth("100%");
		batchStartDateField.setRequired(true);
		batchStartDateField.setResolution(DateField.RESOLUTION_DAY);
		batchStartDateField.setDateFormat(DATE_FORMAT);
		batchStartDateField.setValue(new Date());
		
		
		batchEndDateField = new DateField("End Date");
		//batchEndDateField.setWidth("100%");
		//batchEndDateField.setRequired(true);
		batchEndDateField.setResolution(DateField.RESOLUTION_DAY);
		batchEndDateField.setDateFormat(DATE_FORMAT);
		batchEndDateField.setValue(new Date());
				
		
		classTeachersComboBox.setRequired(true);		
		classTeachersComboBox.setNewItemsAllowed(true);
					
		
		statusComboBox = new ComboBox("Status");
		//statusComboBox.setWidth("100%");
		for(Status status:Status.values())
		{
			statusComboBox.addItem(status);
		}
		statusComboBox.setTextInputAllowed(false);
		statusComboBox.setVisible(false);
						
		
		
		noteTextArea = new TextArea();
		noteTextArea.setCaption("Note");
		noteTextArea.setVisible(false);
		noteTextArea.setWidth("100%");
				
		//gridLayout.addComponent(searchBatchListLayout(),1,0,7,9);
		
		saveButton = new Button("Save");
		deleteButton = new Button("Delete");
		deleteButton.setVisible(false);
		cancelButton = new Button("Cancel");
		
		/*// Print TEST
		
		
		// The embedded will hold out pdf contents
        final Embedded pdfContents = new Embedded();
        pdfContents.setSizeFull();
        pdfContents.setType(Embedded.TYPE_BROWSER);

        CssLayout vl = new CssLayout();
        Button popup = new Button("PDF in popup");
        vl.addComponent(popup);
        popup.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				// Remove the embedded in the mainlayout when showing the popup.
                // The pdfreader always show on top of the other divs.
				//window.removeComponent(pdfContents);
                displayPopup();
			}
		});
		*/
		//
				
		HorizontalLayout buttonRow = new HorizontalLayout();
		//buttonRow.setWidth("100%");
		buttonRow.setSpacing(true);
		//Label expandLabel = new Label();
		
		//buttonRow.addComponent(expandLabel);
		buttonRow.addComponent(saveButton);
		buttonRow.addComponent(deleteButton);
		buttonRow.addComponent(cancelButton);
//		buttonRow.addComponent(popup);
		//buttonRow.setExpandRatio(expandLabel, 1);
		
		gridLayout.addComponent(organizationsComboBox,0,0,0,0);
		gridLayout.addComponent(courseComboBox,0,1,0,1);
		gridLayout.addComponent(sessionComboBox,0,2,0,2);
		gridLayout.addComponent(batchNameField,0,3,0,3);
		gridLayout.addComponent(batchStartDateField,1,0,1,0);
		gridLayout.addComponent(batchEndDateField,1,1,1,1);
		gridLayout.addComponent(classTeachersComboBox,1,2,1,2);
		gridLayout.addComponent(statusComboBox,1,3,1,3);
		gridLayout.addComponent(noteTextArea,0,4,1,5);
		gridLayout.addComponent(buttonRow,0,6,0,6);
		
		gridLayout.setComponentAlignment(buttonRow, Alignment.BOTTOM_LEFT);
		
		
		saveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				
				batch = getPopulatedFieldsValue();
				
				// validating the required fields
				if (checkValidation(batch)){
					if(batch.isNew()){
						if(!existForSave(batch)){
							
							batch.setStatus(Status.RUNNING.key);
							try {
								BatchLocalServiceUtil.addBatch(batch);
								window.showNotification("Batch Added Successfully");
								//loadTables();
								clearBatchForm();
							} catch (SystemException e) {
								e.printStackTrace();
							}
						}else{
							window.showNotification("Batch Already Exists!",Window.Notification.TYPE_WARNING_MESSAGE);
							batch=null;
						}						
					}else{
						if(existForUpdate(batch)){
							try {
								BatchLocalServiceUtil.updateBatch(batch);
								window.showNotification("Batch Updated Successfully");
								//loadTables();
								clearBatchForm();
							} catch (SystemException e) {
								e.printStackTrace();
							}
						}else{
							window.showNotification("Updated Batch Name Already Exists!",Window.Notification.TYPE_WARNING_MESSAGE);
							return;
						}
					}
				}else{						
					window.showNotification("Fill up the required Fields first",Window.Notification.TYPE_WARNING_MESSAGE);	
					batch=null;
				}
				batch=null;
			}			
			
		});
		
		cancelButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				clearBatchForm();
			}
		});
		
		deleteButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				batch = getPopulatedFieldsValue();
				if(batch!=null){
					ConfirmDialog.show(window, "Are you sure you want to delete this batch record?",
					        new ConfirmDialog.Listener() {
					            public void onClose(ConfirmDialog dialog) {
					                if (dialog.isConfirmed()) {
					                	try {
											BatchLocalServiceUtil.deleteBatch(batch);
											window.showNotification("Batch Deleted Successfully");
											//loadTables();
											clearBatchForm();
										} catch (SystemException e) {
											e.printStackTrace();
											window.showNotification("Error! Cannot delete the batch record.");
										}
					                } 
					            }
					        });
					                
					
				}else{
					window.showNotification("Something went wrong!", Window.Notification.TYPE_WARNING_MESSAGE);
				}
				
			}
		});
		return gridLayout;
	}
	
	private void loadTeacherComboBox(){
		if(teacherList!=null){
			classTeachersComboBox.removeAllItems();
			teacherDto = new ArrayList<TeacherDto>();
			for (User groupTeacher : teacherList){
				try {
					List<Organization> teacherOrgList = groupTeacher.getOrganizations();
					for(Organization org :teacherOrgList){
						if(organizationId == org.getOrganizationId()){
							TeacherDto teacher = new TeacherDto();
							teacher.setTeacherId(groupTeacher.getUserId());
							teacher.setTeacherName(groupTeacher.getFullName());
							teacherDto.add(teacher);
							classTeachersComboBox.addItem(teacher);
						}
					}
					
				} catch (PortalException e) {
					e.printStackTrace();
				} catch (SystemException e) {
					e.printStackTrace();
				}
				
			}
		}
	}
	
	private boolean checkValidation(Batch batch){
		boolean res = false;
		if(batch.getCourseId()!=0 && batch.getBatchName()!="" && batch.getBatchTeacherId()!=0){
			res = true;
		}
		
		return res;
	}
	
	private boolean existForSave(Batch batch){
		boolean res = false;
		for(Batch batchFromList : batchesList){			
			if(batchFromList.getCourseId()==batch.getCourseId() && batchFromList.getOrganizationId()== organizationId && batch.getBatchName().equalsIgnoreCase(batchFromList.getBatchName())){
				res = true;
			}			
		}
		return res;
	}
	
	private boolean existForUpdate(Batch batch){
		boolean res = true;
		for(Batch batchFromList : batchesList){
			if(batch.getBatchId()!=batchFromList.getBatchId()){
				if(batch.getOrganizationId() == batchFromList.getOrganizationId() && batch.getBatchName().equalsIgnoreCase(batchFromList.getBatchName())){
					res = false;
				}
			}
		}
		
		return res;
	}
	
	private ComboBox 			manageCampusComboBox;
	private ComboBox			manageCourseBox;
	private ComboBox			manageBatchBox;
	private long 				manageOrganizationId;
	
	public VerticalLayout batchManageLayout()
	{
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.setSpacing(true);
		
		HorizontalLayout optionsLayout = new HorizontalLayout();
		optionsLayout.setSpacing(true);
		
		manageCampusComboBox = new ComboBox("Campus");
		manageCampusComboBox.setImmediate(true);
		manageCourseBox = new ComboBox("Course");
		manageCourseBox.setImmediate(true);
		manageBatchBox = new ComboBox("Batches");
		manageBatchBox.setImmediate(true);
		
		if(userOganizationsDtos!=null){
			if(userOganizationsDtos.size()>1){
				manageCampusComboBox.setVisible(true);
				for(UserOrganizationsDto orgnization: userOganizationsDtos){
					manageCampusComboBox.addItem(orgnization);
				}
				optionsLayout.addComponent(manageCampusComboBox);
			}else if(userOganizationsDtos.size()==1){
				manageOrganizationId = userOrganizationsList.get(0).getOrganizationId();
				getManageCourseDtoByOrgId(manageOrganizationId);
				if (manageCourseDtoList!=null) {
					manageCourseBox.removeAllItems();
					for( CourseDto courseDto: manageCourseDtoList){
						manageCourseBox.addItem(courseDto);
					}
				}
			}
			else{
				manageOrganizationId = 0;
			}
		}
		
		manageCampusComboBox.addListener(new Listener() {			
			public void componentEvent(Event event) {
				UserOrganizationsDto organizationDto = (UserOrganizationsDto) manageCampusComboBox.getValue();
				if(organizationDto!=null){
					manageOrganizationId = organizationDto.getOrgId();
					getManageCourseDtoByOrgId(manageOrganizationId);					
					if (manageCourseDtoList!=null) {
						manageCourseBox.removeAllItems();
						for( CourseDto courseDto: manageCourseDtoList){
							manageCourseBox.addItem(courseDto);
						}
					}
				}else{
					manageCourseBox.removeAllItems();
					batchDetailBeanItemContainer.removeAllItems();
					batchesDetailTable.refreshRowCache();
				}				
			}
		});	
		
		manageCourseBox.addListener(new Listener() {
			public void componentEvent(Event event) {
				CourseDto courseDTO = (CourseDto) manageCourseBox.getValue();
				if(courseDTO!=null){
					getSessionDtoByCourseId(courseDTO.getCourseId());
					loadTables(courseDTO.getCourseId());
				}else{
					batchDetailBeanItemContainer.removeAllItems();
					batchesDetailTable.refreshRowCache();
				}
			}
		});	
		
		optionsLayout.addComponent(manageCourseBox);
		//optionsLayout.addComponent(manageBatchBox);
		
		verticalLayout.addComponent(optionsLayout);
		//HorizontalLayout options = new HorizontalLayout();
		
		batchesDetailTable = new Table()		
		{
			protected String formatPropertyValue(Object rowId, Object colId,
					Property property) {
				if(property.getType()==Date.class)
				{
					SimpleDateFormat dateFormat = new SimpleDateFormat("EEE dd/MM/yyyy");
					if(property.getValue()!=null){
						return dateFormat.format(property.getValue());
					}
				}
				/*if (colId.toString().equalsIgnoreCase(COLUMN_STATUS)) {
					Status status = getStatus((Integer) property.getValue());
					if (status!=null) {			
						return status.value;
					}
					else{
						return "";
					}
				}*/
				return super.formatPropertyValue(rowId, colId, property);
			}
		};
		
		batchesDetailTable.setWidth("100%");
		batchesDetailTable.setSelectable(true);
		
		batchDetailBeanItemContainer = new BeanItemContainer<BatchDto>(BatchDto.class);		
		
		if(batchDetailBeanItemContainer!=null){
			batchesDetailTable.setContainerDataSource(batchDetailBeanItemContainer);
		}
		batchesDetailTable.setColumnHeader(COLUMN_ORGANIZATION, "Campus");
		batchesDetailTable.setColumnHeader(COLUMN_COURSE, "Course");
		batchesDetailTable.setColumnHeader(COLUMN_SESSION, "Session");
		batchesDetailTable.setColumnHeader(COLUMN_BATCH_NAME, "Batch");
		batchesDetailTable.setColumnHeader(COLUMN_BATCH_START_DATE, "Start Date");
		batchesDetailTable.setColumnHeader(COLUMN_END_DATE, "End Date");
		batchesDetailTable.setColumnHeader(COLUMN_CLASS_TEACHER, "Class Teacher");
		batchesDetailTable.setColumnHeader(COLUMN_STATUS, "Status");
		
		batchesDetailTable.setVisibleColumns(new String[]{COLUMN_ORGANIZATION,COLUMN_COURSE,COLUMN_SESSION,COLUMN_BATCH_NAME,COLUMN_BATCH_START_DATE,COLUMN_END_DATE,COLUMN_CLASS_TEACHER,COLUMN_STATUS});
		
		batchesDetailTable.setPageLength(15);
		
		verticalLayout.addComponent(batchesDetailTable);
		
		editButton = new Button("Edit");
		verticalLayout.addComponent(editButton);
		verticalLayout.setComponentAlignment(editButton, Alignment.BOTTOM_RIGHT);		
			editButton.addListener(new ClickListener() {
				public void buttonClick(ClickEvent event) {
					BatchDto selectedValue = (BatchDto)batchesDetailTable.getValue();
					if(selectedValue!=null){
						populateEditForm(selectedValue);
						batchTabSheet.setSelectedTab(initBatchLayout);	
						batch = new BatchImpl();
						batch.setBatchId(selectedValue.getBatchId());
					}
					else{
						window.showNotification("Please Select a Batch!", Window.Notification.TYPE_WARNING_MESSAGE);
					}					
				}
			});
		
		return verticalLayout;
	}	
	
	private ComboBox 			campusComboBox;
	private ComboBox			courseBox;
	private ComboBox			fromBatchBox;
	private ComboBox			toBatchBox;
	private long 				searchOrganizationId;
	private Table				assignBatchTable;
	private ComboBox			assignActionBox;
	private Button 				assignSaveButton;
	private Button				assignCancelButton;
	private Button				assignDeleteButton;
	
	private BeanItemContainer<BatchStudentDto> studentBeanItemContainer;
	
	public final static String COLUMN_STUDENT_ID 		= "studentId";
	public final static String COLUMN_STUDENT_NAME		= "name";
	public final static String COLUMN_CHECK				= "check";
	
	

	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
	}
	
	//This section is to populate the DTO and the list form the other services
	private void getCourseDtoByOrgId(long organizationId){
		try {
			List<CourseOrganization> courseOrganizations = CourseOrganizationLocalServiceUtil.findByOrganization(organizationId);
			//System.out.println("couse size by organization"+courseOrganizations.size());
			courseDtoList = new ArrayList<CourseDto>();
			for(CourseOrganization courseOrg : courseOrganizations){
				CourseDto courseDto = new CourseDto();
				long courseid = courseOrg.getCourseId();
				Course course = CourseLocalServiceUtil.fetchCourse(courseid);
				courseDto.setCourseId(courseid);
				courseDto.setCourseName(course.getCourseName());				
				courseDtoList.add(courseDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	List<CourseDto> manageCourseDtoList;
	private void getManageCourseDtoByOrgId(long organizationId){
		try {
			List<CourseOrganization> courseOrganizations = CourseOrganizationLocalServiceUtil.findByOrganization(organizationId);
			//System.out.println("couse size by organization"+courseOrganizations.size());
			manageCourseDtoList = new ArrayList<CourseDto>();
			for(CourseOrganization courseOrg : courseOrganizations){
				CourseDto courseDto = new CourseDto();
				long courseid = courseOrg.getCourseId();
				Course course = CourseLocalServiceUtil.fetchCourse(courseid);
				courseDto.setCourseId(courseid);
				courseDto.setCourseName(course.getCourseName());				
				manageCourseDtoList.add(courseDto);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	
	private void getSessionDtoByCourseId(long courseId){
		try {
			sessionDtoList = new ArrayList<SessionDto>();
			List<CourseSession> courseSessions = CourseSessionLocalServiceUtil.findByCourseId(courseId);
			for (CourseSession sessionDto : courseSessions) {
				SessionDto session = new SessionDto();
				session.setSessionId(sessionDto.getSessionId());
				session.setSessionName(sessionDto.getSessionName());
				sessionDtoList.add(session);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private List<BatchStudentDto> studentDtoList;
	private void getStudentByBatchId(long batchId) {
		studentBeanItemContainer.removeAllItems();
		try {
			studentDtoList = new ArrayList<BatchStudentDto>();
			List<BatchStudent> batchStudnetList = BatchStudentLocalServiceUtil.findAllStudentByOrgBatchId(searchOrganizationId, batchId);
			for(BatchStudent batchStudent :batchStudnetList){
				try {
					Student student = StudentLocalServiceUtil.findStudentByOrgStudentId(searchOrganizationId, batchStudent.getStudentId());
					BatchStudentDto studentDto = new BatchStudentDto(student,batchStudent);
					studentDtoList.add(studentDto);
				} catch (NoSuchStudentException e) {
					e.printStackTrace();
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		if(studentDtoList!=null){
			for(BatchStudentDto studentDto:studentDtoList){
				studentBeanItemContainer.addBean(studentDto);
			}
		}

		assignBatchTable.refreshRowCache();
	}
	
	private String getCourseNameById(long courseId){
		String name = "";
		try {
			name = CourseLocalServiceUtil.getCourse(courseId).getCourseName();
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return name;
	}
	
	private String getSessionNameById(long sessionId){
		String name = "";
		try {
			name = CourseSessionLocalServiceUtil.getCourseSession(sessionId).getSessionName();
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return name;
	}
	
	private void loadTables(long courseId){
		
		if (batchDetailBeanItemContainer!=null){
			batchDetailBeanItemContainer.removeAllItems();
			batchesDtoList = new ArrayList<BatchDto>();
			List<Batch> batchList=null;
			try {
				batchList = BatchLocalServiceUtil.findBatchesByOrgCourseId(manageOrganizationId, courseId);
			} catch (SystemException e) {
				e.printStackTrace();
			}
			if(batchList!=null){
				for (int i = 0; i < batchList.size(); i++) {
					Batch batch = batchList.get(i);
					BatchDto batchDto = new BatchDto();
					batchDto.setBatchId(batch.getBatchId());
					for(Organization userOrg : userOrganizationsList){
						if(userOrg.getOrganizationId()==batch.getOrganizationId()){
							batchDto.setOrganizationName(userOrg.getName());
							batchDto.setOrganizationId(batch.getOrganizationId());
							break;
						}
					}
					
					batchDto.setBatchEndDate(batch.getEndDate());
					batchDto.setBatchStartDate(batch.getStartDate());
					batchDto.setBatchName(batch.getBatchName());
					batchDto.setNote(batch.getNote());
					
					long teacherId = batch.getBatchTeacherId();
					for (User user : teacherList){
						if (user.getUserId()==teacherId){
							batchDto.setBatchClassTeacher(user.getFullName());
							batchDto.setTeacherId(teacherId);
							break;
						}
					}										
					
					String courseName = getCourseNameById(batch.getCourseId());
					batchDto.setCourseId(batch.getCourseId());
					batchDto.setCourseCode(courseName);
					
					if(batch.getSessionId()!=0){
						String session = getSessionNameById(batch.getSessionId());
						batchDto.setSessionId(batch.getSessionId());
						batchDto.setSession(session);
					}
					
					Status status = getStatus(batch.getStatus());
					if (status!=null) {			
						batchDto.setStatus(status.value);
					}
					batchDto.setStatusKey(batch.getStatus());
					batchesDtoList.add(batchDto);
					batchDetailBeanItemContainer.addBean(batchDto);
				}
			}
			batchesDetailTable.refreshRowCache();
		}
		
	}
	
	private Batch getPopulatedFieldsValue(){
		//this id is taken to compare if the class teacher id is changed or not
		changedClassteacherId = (TeacherDto) classTeachersComboBox.getValue();
		if(batch==null){
			batch = new BatchImpl();
			batch.setNew(true);
			batch.setCreateDate(new Date());			
		}else{
			batch.setNew(false);
			batch.setModifiedDate(new Date());
		}
		
		Object batchname = batchNameField.getValue();
		if(batchname!=null){
			batch.setBatchName(batchname.toString().trim());
		}
		
		CourseDto course = (CourseDto) courseComboBox.getValue();
		if(course!=null){
			batch.setCourseId(course.getCourseId());
		}
		
		if(changedClassteacherId!=null){
			batch.setBatchTeacherId(changedClassteacherId.getTeacherId());	
		}	
		
		Date enddate = (Date) batchEndDateField.getValue();
		if(enddate!=null){
			batch.setEndDate(enddate);	
		}
		
		Date startdate = (Date) batchStartDateField.getValue();
		if(startdate!=null){
			batch.setStartDate(startdate);	
		}
		
		String note = (String) noteTextArea.getValue();
		if(note!=null){
			batch.setNote(note.trim());
		}
		
		SessionDto session = (SessionDto) sessionComboBox.getValue();
		if(session!=null){
			batch.setSessionId(session.getSessionId());
		}
		
		Status status = (Status)statusComboBox.getValue();
		if(status!=null){
			batch.setStatus(status.key);
		}
	
		batch.setUserId(user.getUserId());
		batch.setCompanyId(companyId);
		
		batch.setOrganizationId(organizationId);

		return batch;
	}
	public void populateEditForm(BatchDto batchesDetail)
	{
		if(userOganizationsDtos.size()>1){
			organizationsComboBox.setValue(getOrgDto(batchesDetail.getOrganizationId()));
		}		
		
		CourseDto course = getCourseDto(batchesDetail.getCourseId());
		courseComboBox.setValue(getCourseDto(batchesDetail.getCourseId()));
		if(batchesDetail.getSessionId()!=0){
			sessionComboBox.setValue(getSessionDto(batchesDetail.getSessionId()));
		}		
		batchNameField.setValue(batchesDetail.getBatchName());
		batchStartDateField.setValue(batchesDetail.getBatchStartDate());
		batchEndDateField.setValue(batchesDetail.getBatchEndDate());
		classTeachersComboBox.setValue(getClassteacherDto(batchesDetail.getTeacherId()));
		
		Status status = getStatus(batchesDetail.getStatusKey());		
		statusComboBox.setValue(status);
		
		noteTextArea.setValue(batchesDetail.getNote());
		
		noteTextArea.setVisible(true);
		statusComboBox.setVisible(true);
		deleteButton.setVisible(true);
		
		//selectedValue = null;
	}
	
	private UserOrganizationsDto getOrgDto(long orgId){
		for(UserOrganizationsDto userOrg : userOganizationsDtos){
			if(userOrg.getOrgId()==orgId){
				return userOrg;
			}			
		}
		return null;
	}
	
	private CourseDto getCourseDto(long courseId){
		for(CourseDto course : courseDtoList){
			if(course.getCourseId() == courseId){
				return course;
			}
		}
		return null;
	}
	private SessionDto getSessionDto(long sessionId){
		for(SessionDto session : sessionDtoList){
			if(session.getSessionId() == sessionId){
				return session;
			}
		}
		return null;
	}
	
	private TeacherDto getClassteacherDto(long teacherId){
		for(TeacherDto teacher : teacherDto){
			if(teacher.getTeacherId()== teacherId){
				return teacher;
			}
		}		
		return null;
	}
	
	private enum Status implements Serializable
	{
		RUNNING(1,"Running"),
		PENDING(2,"Pending"),
		COMPLETED(3,"Completed"),
		STOPPED(4,"Stopped");
		
		private int key;
		private String value;
		
		private Status(int key,String value)
		{
			this.key = key;
			this.value = value;
		}
		
		@Override
		public String toString()
		{
			return value;
		}
		
	}

	private Status getStatus(int key)
	{		
		Status statuses[]=Status.values();
		
		for(Status status:statuses)
		{
			if(status.key==key)
			{
				return status;
			}
		}		
		return null;
	}
	
	private void clearBatchForm(){		
		
		organizationsComboBox.setValue(null);
		courseComboBox.setValue(null);
		sessionComboBox.setValue(null);
		
		batchStartDateField.setValue(new Date());
		batchEndDateField.setValue(new Date());
		batchNameField.setValue("");
		
		classTeachersComboBox.setValue(null);
		statusComboBox.setValue(null);
		noteTextArea.setValue("");
		statusComboBox.setVisible(false);
		noteTextArea.setVisible(false);
		deleteButton.setVisible(false);
		batch = null;
		
	}
	
	// Testing PDF
	
	/*private Resource createPdf() {
        // Here we create a new StreamResource which downloads our StreamSource,
        // which is our pdf.
        StreamResource resource = new StreamResource(new Pdf(), "test.pdf?" + System.currentTimeMillis(), this);
        // Set the right mime type
        resource.setMIMEType("application/pdf");
        return resource;
    }

    private void displayPopup() {
        Window window = new Window();
        ((VerticalLayout) window.getContent()).setSizeFull();
        window.setResizable(true);
        window.setWidth("800");
        window.setHeight("600");
        window.center();
        Embedded e = new Embedded();
        e.setSizeFull();
        e.setType(Embedded.TYPE_BROWSER);

        // Here we create a new StreamResource which downloads our StreamSource,
        // which is our pdf.
        StreamResource resource = new StreamResource(new Pdf(), "test.pdf?" + System.currentTimeMillis(), this);
        // Set the right mime type
        resource.setMIMEType("application/pdf");

        e.setSource(resource);
        window.addComponent(e);
        getMainWindow().addWindow(window);
    }

    *//**
     * This class creates a PDF with the iText library. This class implements
     * the StreamSource interface which defines the getStream method.
     *//*
    public static class Pdf implements StreamSource {
        private final ByteArrayOutputStream os = new ByteArrayOutputStream();

        public Pdf() {
            Document document = null;

            try {
                document = new Document(PageSize.A4, 50, 50, 50, 50);
                PdfWriter.getInstance(document, os);
                document.open();
               
                PdfPTable table = new PdfPTable(3);
                PdfPCell cell;
                
                Set<String> campus = new HashSet<String>();
                Set<String> course = new HashSet<String>();
                
                if(batchesDtoList!=null){
                	for (BatchDto batch : batchesDtoList ){
                    	campus.add(batch.getOrganizationName());
                    	course.add(batch.getCourseCode());
                    }
                    
                    Iterator campusiterator = campus.iterator();                
                    while(campusiterator.hasNext()){
                    	String campusName = (String) campusiterator.next();
                    	cell = new PdfPCell(new Phrase(campusName));
                        cell.setColspan(3);
                        table.addCell(cell);
                        
                        Iterator courseiterator = course.iterator();                
                        while(courseiterator.hasNext()){
                        	String courseName = (String) courseiterator.next();
                        	cell = new PdfPCell(new Phrase(courseName));
                        	if(getBatesSizeForSpan(campusName,courseName)>0){
                        		cell.setRowspan(getBatesSizeForSpan(campusName,courseName));
                                table.addCell(cell);    
                                for(BatchDto batch : batchesDtoList){
                                	if(batch.getOrganizationName().equalsIgnoreCase(campusName)&& batch.getCourseCode().equalsIgnoreCase(courseName)){
                                		table.addCell(batch.getBatchName());
                                        table.addCell(batch.getStatus());
                                	}                        	
                                }
                        	}                        
                        }                	
                    }
                    document.add(table);
                }                              
                
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (document != null) {
                    document.close();
                }
            }
        }
        
        private int getBatesSizeForSpan(String campusName, String courseName){
        	int total=0;
        	for(BatchDto batch : batchesDtoList){
            	if(batch.getOrganizationName().equalsIgnoreCase(campusName)&& batch.getCourseCode().equalsIgnoreCase(courseName)){
            		total = total+1;
            	}                        	
            }
        	return total;
        }

        public InputStream getStream() {
            // Here we return the pdf contents as a byte-array
            return new ByteArrayInputStream(os.toByteArray());
        }
    }*/
	// Ending Test PDF
}

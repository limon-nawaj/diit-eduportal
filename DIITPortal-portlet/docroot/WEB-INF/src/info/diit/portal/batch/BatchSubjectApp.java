package info.diit.portal.batch;

import static info.diit.portal.constant.StudentConstant.TEACHER_GROUP_NAME;
import static info.diit.portal.constant.StudentConstant.COLUMN_SUBJECT_NAME;
import static info.diit.portal.constant.StudentConstant.COLUMN_TEACHER_NAME;
import static info.diit.portal.constant.StudentConstant.COLUMN_START_DATE;
import static info.diit.portal.constant.StudentConstant.COLUMN_HOURS;
import static info.diit.portal.constant.StudentConstant.DATE_FORMAT;


import info.diit.portal.dto.BatchDto;
import info.diit.portal.dto.BatchSubjectDto;
import info.diit.portal.dto.CourseDto;
import info.diit.portal.dto.SubjectDto;
import info.diit.portal.dto.TeacherDto;
import info.diit.portal.dto.UserOrganizationsDto;
import info.diit.portal.model.Batch;
import info.diit.portal.model.BatchSubject;
import info.diit.portal.model.Course;
import info.diit.portal.model.CourseOrganization;
import info.diit.portal.model.CourseSubject;
import info.diit.portal.model.Subject;
import info.diit.portal.model.impl.BatchSubjectImpl;
import info.diit.portal.service.BatchLocalServiceUtil;
import info.diit.portal.service.BatchSubjectLocalServiceUtil;
import info.diit.portal.service.CourseLocalServiceUtil;
import info.diit.portal.service.CourseOrganizationLocalServiceUtil;
import info.diit.portal.service.CourseSubjectLocalServiceUtil;
import info.diit.portal.service.SubjectLocalServiceUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import net.atontech.vaadin.ui.numericfield.NumericField;
import net.atontech.vaadin.ui.numericfield.widgetset.shared.NumericFieldType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.Application;
import com.vaadin.data.Container;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.terminal.gwt.server.PortletRequestListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Component.Event;
import com.vaadin.ui.Component.Listener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class BatchSubjectApp extends Application implements PortletRequestListener {

	private Window 										window;	
	private VerticalLayout 								batchSubjectManageLayout;
	private Table										courseSubjectTable;
	private Table 										batchSubjectManageTable;
	
	private ComboBox									organizationsComboBox;
	private ComboBox									courseComboBox;
	private ComboBox									batchComboBox;
	private Button										saveButton;

	
	
	private ThemeDisplay								themeDisplay;
	private User										user;
	private long										companyId;
	private long										organizationId;
	private UserGroup 									teacherGroup;
	private List<User>									teacherList;
	
	private long 										courseId;
	private long 										batchId;
	
	private List<Organization> 							userOrganizationsList;
	private List<UserOrganizationsDto>					userOganizationsDtos;
	private List<Batch> 								batchesList;
	private List<CourseDto> 							courseDtoList;
	private List<TeacherDto> 							teacherDto;
	private List<BatchDto> 								batchDtoList;
	private List<SubjectDto> 							subjectDtoList;
	private List<CourseSubject> 						courseSubjectList;
	private List<BatchSubjectDto> 						batchSubjectDtoList;
	
	private BeanItemContainer<SubjectDto> 				courseSubjectBeanItemContainer;
	private BeanItemContainer<BatchSubjectDto> 			batchSubjectDtoContainer;
	
	public void init() {
		window = new Window();
		window.setSizeFull();

		setMainWindow(window);
		
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.setSpacing(true);

		
		user			=	themeDisplay.getUser();
		companyId		=	themeDisplay.getCompanyId();
		
		try {
			userOrganizationsList	=	user.getOrganizations();
			teacherGroup = UserGroupLocalServiceUtil.getUserGroup(companyId,TEACHER_GROUP_NAME);
			teacherList = UserLocalServiceUtil.getUserGroupUsers(teacherGroup.getUserGroupId());
						
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		verticalLayout.addComponent(initBatchSubjectLayout());
		window.addComponent(verticalLayout);
	}
	
	private GridLayout initBatchSubjectLayout() 
	{	
		organizationsComboBox = new ComboBox("Campuses");
		courseComboBox = new ComboBox("Course");
		batchComboBox = new ComboBox("Batch");	
		
		organizationsComboBox.setWidth("100%");
		courseComboBox.setWidth("100%");
		batchComboBox.setWidth("100%");
		
		organizationsComboBox.setVisible(false);
		
		if (userOrganizationsList.size()>1) {
			organizationsComboBox.setVisible(true);
			userOganizationsDtos = new ArrayList<UserOrganizationsDto>();
			for(Organization userOrg : userOrganizationsList){
				UserOrganizationsDto userOrgDto = new UserOrganizationsDto();
				userOrgDto.setOrgId(userOrg.getOrganizationId());
				userOrgDto.setOrgName(userOrg.getName());
				userOganizationsDtos.add(userOrgDto);
				organizationsComboBox.addItem(userOrgDto);
			}
			organizationsComboBox.setImmediate(true);			
		}
		else if(userOrganizationsList.size()==1){
			//window.showNotification("working organization size checking");
			organizationId = userOrganizationsList.get(0).getOrganizationId();
			//window.showNotification("organization id found " +organizationId);
			getCourseDtoByOrgId(organizationId);
			if (courseDtoList!=null) {	
				for(CourseDto courseDto: courseDtoList){
					courseComboBox.addItem(courseDto);
				}
			}
		}
		else{
			organizationId = 0;
		}		
		
		organizationsComboBox.addListener(new Listener() {			
			public void componentEvent(Event event) {
				// this method below will populate the courseCombobox according to the user belongs to org
				courseComboBox.removeAllItems();
				UserOrganizationsDto org = (UserOrganizationsDto)organizationsComboBox.getValue();				
				if(org!=null){
					organizationId = org.getOrgId();
					getCourseDtoByOrgId(organizationId);
					if (courseDtoList!=null) {	
						for(CourseDto courseDto: courseDtoList){
							courseComboBox.addItem(courseDto);
						}
					}
				}
			}
		});	
		
		courseComboBox.setRequired(true);
		courseComboBox.setImmediate(true);		
		
		courseComboBox.addListener(new Listener() {
			public void componentEvent(Event event) {
				batchComboBox.removeAllItems();
				CourseDto course = (CourseDto) courseComboBox.getValue();
				if(course!=null){
					courseId =course.getCourseId();
					getBatchDtoByCourseId(courseId);
					loadCourseSubjectContainer();
					if(batchDtoList!=null){
						for(BatchDto batchDto:batchDtoList){
							batchComboBox.addItem(batchDto);
						}
						saveButton.setEnabled(true);
					}
				}	
				else {
					saveButton.setEnabled(false);
				}
			}
		});	
		
		batchComboBox.setImmediate(true);
		batchComboBox.setRequired(true);
		batchComboBox.addListener(new Listener() {
			public void componentEvent(Event event) {
				BatchDto batch = (BatchDto) batchComboBox.getValue();
				if (batch!=null) {
					batchId = batch.getBatchId();
					getBatchAssignedSubject(batchId, courseId, organizationId);
				}
			}
		});
		
		/**/
		courseSubjectBeanItemContainer = new BeanItemContainer<SubjectDto>(SubjectDto.class);
		courseSubjectTable = new Table("Course Subjects");
		courseSubjectTable.setWidth("100%");
		courseSubjectTable.setPageLength(8);
				
		if(courseSubjectBeanItemContainer!=null){
			courseSubjectTable.setContainerDataSource(courseSubjectBeanItemContainer);
		}		
		
		//courseSubjectTable.setColumnHeader(COLUMN_SIBJECT_ID, "Subject ID");
		courseSubjectTable.setColumnHeader(COLUMN_SUBJECT_NAME, "Subject Name");

		//courseSubjectTable.setVisibleColumns(new String[]{COLUMN_SIBJECT_ID,COLUMN_SUBJECT_NAME});
		courseSubjectTable.setVisibleColumns(new String[]{COLUMN_SUBJECT_NAME});
		
		//courseSubjectTable.setColumnAlignment(COLUMN_SIBJECT_ID, Table.ALIGN_RIGHT);
		courseSubjectTable.setColumnAlignment(COLUMN_SUBJECT_NAME, Table.ALIGN_CENTER);
		
		saveButton = new Button("Save");
		saveButton.setEnabled(false);
		GridLayout gridLayout = new GridLayout(11,5);
		gridLayout.setWidth("100%");
		gridLayout.setSpacing(true);
		
		gridLayout.addComponent(organizationsComboBox,0,0,2,0);
		gridLayout.addComponent(courseComboBox,0,1,2,1);
		gridLayout.addComponent(batchComboBox,0,2,2,2);
		gridLayout.addComponent(courseSubjectTable,0,3,2,3);
		gridLayout.addComponent(loadBatchSubjectTable(),3,0,10,3);
		gridLayout.addComponent(saveButton,0,4);
		
		saveButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				BatchDto batchDto = (BatchDto) batchComboBox.getValue();
				List<BatchSubject> batchSubjectList = new ArrayList<BatchSubject>();
				if (batchDto!=null) {	
					if(batchSubjectDtoContainer!=null){
						for (int i = 0; i < batchSubjectDtoContainer.size(); i++) {
							BatchSubjectDto batchSubjectDto = batchSubjectDtoContainer.getIdByIndex(i);
							long subjectId = 0;
							long teacherId = 0;
							if(batchSubjectDto.getSubjcetName()!=null){
								subjectId = Long.parseLong(stringSplitter(batchSubjectDto.getSubjcetName()));
							}
							if(batchSubjectDto.getTeacherName()!=null){
								teacherId = Long.parseLong(stringSplitter(batchSubjectDto.getTeacherName()));
							}
							
							Date startDate = batchSubjectDto.getStartDate();
							int hours = batchSubjectDto.getHours();
							if(subjectId!=0 && teacherId!=0 && startDate!=null && hours!=0){
								BatchSubject batchSubject = new BatchSubjectImpl();
								batchSubject.setOrganizationId(organizationId);
								batchSubject.setCompanyId(companyId);
								batchSubject.setCourseId(courseId);
								batchSubject.setBatchId(batchId);
								batchSubject.setSubjectId(subjectId);
								batchSubject.setTeacherId(teacherId);
								batchSubject.setStartDate(startDate);
								batchSubject.setHours(hours);
								batchSubjectList.add(batchSubject);
							}						
						}
					}
					
					try {
						if (batchSubjectList.size()>0){
							BatchSubjectLocalServiceUtil.updateBatchSubject(organizationId, courseId, batchId, batchSubjectList);
							window.showNotification("Batch Subject Assigned Successfully");
						}else{
							window.showNotification("Nothing to save!", Window.Notification.TYPE_WARNING_MESSAGE);
						}
						
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}
				else {
					window.showNotification("Please select a batch",Window.Notification.TYPE_ERROR_MESSAGE);
				}
			}
		});
				
		return gridLayout;		
		
	}
	
	private String stringSplitter (String string){
		String id="";
		
		if (string!="" && string!=null) {
			String[] result = string.split("-");
			id = result[0];
		}
		
		
		return id;
	}
	
	private VerticalLayout loadBatchSubjectTable(){
		batchSubjectManageLayout = new VerticalLayout();
		batchSubjectDtoContainer = new BeanItemContainer<BatchSubjectDto>(BatchSubjectDto.class);
		batchSubjectManageTable = new Table("Batch Subjects",batchSubjectDtoContainer);
		batchSubjectManageTable.setImmediate(true);
		batchSubjectManageTable.setEditable(true);
		batchSubjectManageTable.setSelectable(true);
		batchSubjectManageTable.setWidth("100%");
		batchSubjectManageTable.setHeight("100%");
		
		batchSubjectManageTable.setColumnHeader(COLUMN_SUBJECT_NAME, "Subject");
		batchSubjectManageTable.setColumnHeader(COLUMN_TEACHER_NAME, "Teacher");
		batchSubjectManageTable.setColumnHeader(COLUMN_START_DATE, "Start Date");
		batchSubjectManageTable.setColumnHeader(COLUMN_HOURS, "Hours");
		
		batchSubjectManageTable.setVisibleColumns(new String[]{COLUMN_SUBJECT_NAME, COLUMN_TEACHER_NAME, COLUMN_START_DATE, COLUMN_HOURS});
		
		batchSubjectManageTable.setColumnExpandRatio(COLUMN_SUBJECT_NAME, 3);
		batchSubjectManageTable.setColumnExpandRatio(COLUMN_TEACHER_NAME, 3);
		batchSubjectManageTable.setColumnExpandRatio(COLUMN_START_DATE, 2);
		batchSubjectManageTable.setColumnExpandRatio(COLUMN_HOURS, 1);
		
		batchSubjectManageTable.setPageLength(10);
		
		batchSubjectManageLayout.addComponent(batchSubjectManageTable);
		
		batchSubjectManageTable.setTableFieldFactory(new DefaultFieldFactory()
		{
			@Override
			public Field createField(Container container, Object itemId,
					Object propertyId, Component uiContext) {
				if (propertyId.equals(COLUMN_SUBJECT_NAME)) {
					ComboBox subjectComboBox = new ComboBox();
					subjectComboBox.setNullSelectionAllowed(true);
					subjectComboBox.setWidth("100%");
					subjectComboBox.setImmediate(true);
					subjectComboBox.setReadOnly(false);
					
					if (subjectDtoList!=null) {
						for (SubjectDto subjectDto : subjectDtoList) {
							subjectComboBox.addItem(subjectDto.getSubjectId()+"-"+subjectDto.getSubjcetName());
						}
					}
					return subjectComboBox;
				}
				
				if (propertyId.equals(COLUMN_TEACHER_NAME)) {
					ComboBox teacherComboBox = new ComboBox();
					teacherComboBox.setNullSelectionAllowed(true);
					teacherComboBox.setWidth("100%");
					teacherComboBox.setImmediate(true);
					teacherComboBox.setReadOnly(false);
					
					if(teacherList!=null){
						for (User groupTeacher : teacherList){
							try {
								List<Organization> teacherOrgList = groupTeacher.getOrganizations();
								for(Organization org :teacherOrgList){
									if(organizationId == org.getOrganizationId()){
										teacherComboBox.addItem(groupTeacher.getUserId()+"-"+groupTeacher.getFullName());
									}
								}
								
							} catch (PortalException e) {
								e.printStackTrace();
							} catch (SystemException e) {
								e.printStackTrace();
							}
							
						}
					}
					return teacherComboBox;
				}				
				
				if (propertyId.equals(COLUMN_START_DATE)) {
					DateField field = new DateField();
					field.setWidth("100%");
					field.setResolution(DateField.RESOLUTION_DAY);
					field.setDateFormat(DATE_FORMAT);
					field.setReadOnly(false);
					return field;
				}
				
				if (propertyId.equals(COLUMN_HOURS)) {
					NumericField field = new NumericField();
					field.setWidth("100%");
					field.setNullSettingAllowed(true);
					field.setNumberType(NumericFieldType.INTEGER);
					field.setNullRepresentation("");
					field.setReadOnly(false);
					return field;
				}
								
				return super.createField(container, itemId, propertyId, uiContext);
			}
		});
				
		return batchSubjectManageLayout;
	}
	
	private void loadCourseSubjectContainer(){
		if(courseSubjectBeanItemContainer!=null){
			courseSubjectBeanItemContainer.removeAllItems();
			subjectDtoList = new ArrayList<SubjectDto>();
			for(CourseSubject courseSubject: courseSubjectList){
				SubjectDto subjectDto = new SubjectDto();
				long subjectId = courseSubject.getSubjectId();
				subjectDto.setSubjectId(subjectId);
				subjectDto.setSubjcetName(getSubjectNameById(subjectId));
				subjectDtoList.add(subjectDto);
				courseSubjectBeanItemContainer.addBean(subjectDto);
			}				
			courseSubjectTable.refreshRowCache();
		}
	}
			
	private void getCourseDtoByOrgId(long organizationId){
		try {
			List<CourseOrganization> courseOrganizations = CourseOrganizationLocalServiceUtil.findByOrganization(organizationId);
			//System.out.println("couse size by organization"+courseOrganizations.size());
			courseDtoList = new ArrayList<CourseDto>();
			for(CourseOrganization courseOrg : courseOrganizations){
				CourseDto courseDto = new CourseDto();
				long courseid = courseOrg.getCourseId();
				Course course = CourseLocalServiceUtil.fetchCourse(courseid);
				courseDto.setCourseId(courseid);
				//System.out.println("course Name"+course.getCourseName());
				courseDto.setCourseName(course.getCourseName());				
				courseDtoList.add(courseDto);
				//System.out.println("Course Dto Size"+ courseDtoList.size());
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private void getBatchDtoByCourseId(long courseId){
		try {
			List<Batch> batchList = BatchLocalServiceUtil.findBatchesByOrgCourseId(organizationId, courseId);			
			courseSubjectList = CourseSubjectLocalServiceUtil.findByCourseId(courseId);
			
			batchDtoList = new ArrayList<BatchDto>();
			for(Batch batch : batchList){
				BatchDto batchDto = new BatchDto();
				batchDto.setBatchId(batch.getBatchId());
				batchDto.setBatchName(batch.getBatchName());
				batchDtoList.add(batchDto);
			}
						
		} catch (SystemException e) {
			e.printStackTrace();
		}		
	}
	
	private void getBatchAssignedSubject(long batchId, long courseId, long organizationId){
		
		try {
			List<BatchSubject> batchSubjectList = BatchSubjectLocalServiceUtil.getBatchSubjectByOrgCourseBatch(organizationId, courseId, batchId);
			batchSubjectDtoList = new ArrayList<BatchSubjectDto>();
			if (batchSubjectList!=null) {
				for(BatchSubject batchSubject:batchSubjectList){
					BatchSubjectDto batchSubjectDto = new BatchSubjectDto();
					long subjectId = batchSubject.getSubjectId();
					batchSubjectDto.setSubjectId(subjectId);
					batchSubjectDto.setSubjcetName(getSubjectNameById(subjectId));
					long teacherId = batchSubject.getTeacherId();
					batchSubjectDto.setTeacherId(teacherId);
					batchSubjectDto.setTeacherName(getTeacherName(teacherId));
					batchSubjectDto.setStartDate(batchSubject.getStartDate());
					batchSubjectDto.setHours(batchSubject.getHours());
					batchSubjectDtoList.add(batchSubjectDto);
				}
			}
			int courseSubjectSize = subjectDtoList.size();
			int assignedBatchSubjectSize = batchSubjectDtoList.size();
			int notAssignedSubjectSize = courseSubjectSize - assignedBatchSubjectSize;
				
			if(assignedBatchSubjectSize>0){
				if (batchSubjectDtoContainer!=null) {
					batchSubjectDtoContainer.removeAllItems();
					for (BatchSubjectDto batchSubjectDto: batchSubjectDtoList) {
						
						BatchSubjectDto subjectDto = new BatchSubjectDto();
						subjectDto.setSubjcetName(batchSubjectDto.getSubjectId()+"-"+batchSubjectDto.getSubjcetName());
						subjectDto.setTeacherName(batchSubjectDto.getTeacherId()+"-"+batchSubjectDto.getTeacherName());
						subjectDto.setStartDate(batchSubjectDto.getStartDate());
						subjectDto.setHours(batchSubjectDto.getHours());
						batchSubjectDtoContainer.addBean(subjectDto);
											
					}
					if(notAssignedSubjectSize>0){
						for (int i = 0; i < notAssignedSubjectSize ; i++) {
							BatchSubjectDto batchSubjectDto = new BatchSubjectDto();
							batchSubjectDtoContainer.addBean(batchSubjectDto);
						}
					}
					batchSubjectManageTable.refreshRowCache();
				}
			}else{
				loadCourseSubjectContainer();
				batchSubjectDtoContainer.removeAllItems();
				for (int i = 0; i < courseSubjectSize ; i++) {
					BatchSubjectDto batchSubjectDto = new BatchSubjectDto();
					batchSubjectDtoContainer.addBean(batchSubjectDto);
				}
				batchSubjectManageTable.refreshRowCache();
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	private String getSubjectNameById(long subjectId){
		String name = "";
		try {
			Subject subject = SubjectLocalServiceUtil.getSubject(subjectId);
			name = subject.getSubjectName();
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return name;
	}
	
	private String getTeacherName(long teacherId){
		String name = "";
		for (User groupTeacher : teacherList){
			if (groupTeacher.getUserId() == teacherId) {
				return groupTeacher.getFullName();
			}
		}
		return name;
	}

	public void onRequestStart(PortletRequest request, PortletResponse response) {
		themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
	}

	public void onRequestEnd(PortletRequest request, PortletResponse response) {
		
	}
}

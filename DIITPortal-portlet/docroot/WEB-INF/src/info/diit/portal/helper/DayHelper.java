package info.diit.portal.helper;

import java.util.Calendar;
import java.util.Date;

public class DayHelper {
	Calendar calendar;
	
//	First day of month
	public Date getFirstDateOfMonth(Date currentDate){
		calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		Date firstDateOfMonth = calendar.getTime();
		return firstDateOfMonth;
	}
	
//	Last day of month
	public Date getEndDateOfMonth(Date currentDate){
		calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.add(Calendar.MONTH, 1);  
		calendar.set(Calendar.DAY_OF_MONTH, 1);  
		calendar.add(Calendar.DATE, -1); 
		Date endDateOfMonth = calendar.getTime();
		return endDateOfMonth;
	}
}

/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchStudentDocumentException;
import info.diit.portal.model.StudentDocument;
import info.diit.portal.model.impl.StudentDocumentImpl;
import info.diit.portal.model.impl.StudentDocumentModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the student document service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see StudentDocumentPersistence
 * @see StudentDocumentUtil
 * @generated
 */
public class StudentDocumentPersistenceImpl extends BasePersistenceImpl<StudentDocument>
	implements StudentDocumentPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link StudentDocumentUtil} to access the student document persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = StudentDocumentImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_FETCH_BY_STUDENTDOCUMENT = new FinderPath(StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
			StudentDocumentModelImpl.FINDER_CACHE_ENABLED,
			StudentDocumentImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByStudentDocument", new String[] { Long.class.getName() },
			StudentDocumentModelImpl.DOCUMENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STUDENTDOCUMENT = new FinderPath(StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
			StudentDocumentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByStudentDocument", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTDOCUMENTLIST =
		new FinderPath(StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
			StudentDocumentModelImpl.FINDER_CACHE_ENABLED,
			StudentDocumentImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByStudentDocumentList",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTDOCUMENTLIST =
		new FinderPath(StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
			StudentDocumentModelImpl.FINDER_CACHE_ENABLED,
			StudentDocumentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByStudentDocumentList", new String[] { Long.class.getName() },
			StudentDocumentModelImpl.STUDENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STUDENTDOCUMENTLIST = new FinderPath(StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
			StudentDocumentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByStudentDocumentList", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
			StudentDocumentModelImpl.FINDER_CACHE_ENABLED,
			StudentDocumentImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
			StudentDocumentModelImpl.FINDER_CACHE_ENABLED,
			StudentDocumentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
			StudentDocumentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the student document in the entity cache if it is enabled.
	 *
	 * @param studentDocument the student document
	 */
	public void cacheResult(StudentDocument studentDocument) {
		EntityCacheUtil.putResult(StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
			StudentDocumentImpl.class, studentDocument.getPrimaryKey(),
			studentDocument);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_STUDENTDOCUMENT,
			new Object[] { Long.valueOf(studentDocument.getDocumentId()) },
			studentDocument);

		studentDocument.resetOriginalValues();
	}

	/**
	 * Caches the student documents in the entity cache if it is enabled.
	 *
	 * @param studentDocuments the student documents
	 */
	public void cacheResult(List<StudentDocument> studentDocuments) {
		for (StudentDocument studentDocument : studentDocuments) {
			if (EntityCacheUtil.getResult(
						StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
						StudentDocumentImpl.class,
						studentDocument.getPrimaryKey()) == null) {
				cacheResult(studentDocument);
			}
			else {
				studentDocument.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all student documents.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(StudentDocumentImpl.class.getName());
		}

		EntityCacheUtil.clearCache(StudentDocumentImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the student document.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(StudentDocument studentDocument) {
		EntityCacheUtil.removeResult(StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
			StudentDocumentImpl.class, studentDocument.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(studentDocument);
	}

	@Override
	public void clearCache(List<StudentDocument> studentDocuments) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (StudentDocument studentDocument : studentDocuments) {
			EntityCacheUtil.removeResult(StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
				StudentDocumentImpl.class, studentDocument.getPrimaryKey());

			clearUniqueFindersCache(studentDocument);
		}
	}

	protected void clearUniqueFindersCache(StudentDocument studentDocument) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_STUDENTDOCUMENT,
			new Object[] { Long.valueOf(studentDocument.getDocumentId()) });
	}

	/**
	 * Creates a new student document with the primary key. Does not add the student document to the database.
	 *
	 * @param documentId the primary key for the new student document
	 * @return the new student document
	 */
	public StudentDocument create(long documentId) {
		StudentDocument studentDocument = new StudentDocumentImpl();

		studentDocument.setNew(true);
		studentDocument.setPrimaryKey(documentId);

		return studentDocument;
	}

	/**
	 * Removes the student document with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param documentId the primary key of the student document
	 * @return the student document that was removed
	 * @throws info.diit.portal.NoSuchStudentDocumentException if a student document with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDocument remove(long documentId)
		throws NoSuchStudentDocumentException, SystemException {
		return remove(Long.valueOf(documentId));
	}

	/**
	 * Removes the student document with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the student document
	 * @return the student document that was removed
	 * @throws info.diit.portal.NoSuchStudentDocumentException if a student document with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentDocument remove(Serializable primaryKey)
		throws NoSuchStudentDocumentException, SystemException {
		Session session = null;

		try {
			session = openSession();

			StudentDocument studentDocument = (StudentDocument)session.get(StudentDocumentImpl.class,
					primaryKey);

			if (studentDocument == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchStudentDocumentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(studentDocument);
		}
		catch (NoSuchStudentDocumentException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected StudentDocument removeImpl(StudentDocument studentDocument)
		throws SystemException {
		studentDocument = toUnwrappedModel(studentDocument);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, studentDocument);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(studentDocument);

		return studentDocument;
	}

	@Override
	public StudentDocument updateImpl(
		info.diit.portal.model.StudentDocument studentDocument, boolean merge)
		throws SystemException {
		studentDocument = toUnwrappedModel(studentDocument);

		boolean isNew = studentDocument.isNew();

		StudentDocumentModelImpl studentDocumentModelImpl = (StudentDocumentModelImpl)studentDocument;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, studentDocument, merge);

			studentDocument.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !StudentDocumentModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((studentDocumentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTDOCUMENTLIST.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(studentDocumentModelImpl.getOriginalStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTDOCUMENTLIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTDOCUMENTLIST,
					args);

				args = new Object[] {
						Long.valueOf(studentDocumentModelImpl.getStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTDOCUMENTLIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTDOCUMENTLIST,
					args);
			}
		}

		EntityCacheUtil.putResult(StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
			StudentDocumentImpl.class, studentDocument.getPrimaryKey(),
			studentDocument);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_STUDENTDOCUMENT,
				new Object[] { Long.valueOf(studentDocument.getDocumentId()) },
				studentDocument);
		}
		else {
			if ((studentDocumentModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_STUDENTDOCUMENT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(studentDocumentModelImpl.getOriginalDocumentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTDOCUMENT,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_STUDENTDOCUMENT,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_STUDENTDOCUMENT,
					new Object[] { Long.valueOf(studentDocument.getDocumentId()) },
					studentDocument);
			}
		}

		return studentDocument;
	}

	protected StudentDocument toUnwrappedModel(StudentDocument studentDocument) {
		if (studentDocument instanceof StudentDocumentImpl) {
			return studentDocument;
		}

		StudentDocumentImpl studentDocumentImpl = new StudentDocumentImpl();

		studentDocumentImpl.setNew(studentDocument.isNew());
		studentDocumentImpl.setPrimaryKey(studentDocument.getPrimaryKey());

		studentDocumentImpl.setDocumentId(studentDocument.getDocumentId());
		studentDocumentImpl.setCompanyId(studentDocument.getCompanyId());
		studentDocumentImpl.setUserId(studentDocument.getUserId());
		studentDocumentImpl.setUserName(studentDocument.getUserName());
		studentDocumentImpl.setCreateDate(studentDocument.getCreateDate());
		studentDocumentImpl.setModifiedDate(studentDocument.getModifiedDate());
		studentDocumentImpl.setOrganizationId(studentDocument.getOrganizationId());
		studentDocumentImpl.setType(studentDocument.getType());
		studentDocumentImpl.setDocumentData(studentDocument.getDocumentData());
		studentDocumentImpl.setDescription(studentDocument.getDescription());
		studentDocumentImpl.setStudentId(studentDocument.getStudentId());
		studentDocumentImpl.setFileEntryId(studentDocument.getFileEntryId());

		return studentDocumentImpl;
	}

	/**
	 * Returns the student document with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the student document
	 * @return the student document
	 * @throws com.liferay.portal.NoSuchModelException if a student document with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentDocument findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the student document with the primary key or throws a {@link info.diit.portal.NoSuchStudentDocumentException} if it could not be found.
	 *
	 * @param documentId the primary key of the student document
	 * @return the student document
	 * @throws info.diit.portal.NoSuchStudentDocumentException if a student document with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDocument findByPrimaryKey(long documentId)
		throws NoSuchStudentDocumentException, SystemException {
		StudentDocument studentDocument = fetchByPrimaryKey(documentId);

		if (studentDocument == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + documentId);
			}

			throw new NoSuchStudentDocumentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				documentId);
		}

		return studentDocument;
	}

	/**
	 * Returns the student document with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the student document
	 * @return the student document, or <code>null</code> if a student document with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentDocument fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the student document with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param documentId the primary key of the student document
	 * @return the student document, or <code>null</code> if a student document with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDocument fetchByPrimaryKey(long documentId)
		throws SystemException {
		StudentDocument studentDocument = (StudentDocument)EntityCacheUtil.getResult(StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
				StudentDocumentImpl.class, documentId);

		if (studentDocument == _nullStudentDocument) {
			return null;
		}

		if (studentDocument == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				studentDocument = (StudentDocument)session.get(StudentDocumentImpl.class,
						Long.valueOf(documentId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (studentDocument != null) {
					cacheResult(studentDocument);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(StudentDocumentModelImpl.ENTITY_CACHE_ENABLED,
						StudentDocumentImpl.class, documentId,
						_nullStudentDocument);
				}

				closeSession(session);
			}
		}

		return studentDocument;
	}

	/**
	 * Returns the student document where documentId = &#63; or throws a {@link info.diit.portal.NoSuchStudentDocumentException} if it could not be found.
	 *
	 * @param documentId the document ID
	 * @return the matching student document
	 * @throws info.diit.portal.NoSuchStudentDocumentException if a matching student document could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDocument findByStudentDocument(long documentId)
		throws NoSuchStudentDocumentException, SystemException {
		StudentDocument studentDocument = fetchByStudentDocument(documentId);

		if (studentDocument == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("documentId=");
			msg.append(documentId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchStudentDocumentException(msg.toString());
		}

		return studentDocument;
	}

	/**
	 * Returns the student document where documentId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param documentId the document ID
	 * @return the matching student document, or <code>null</code> if a matching student document could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDocument fetchByStudentDocument(long documentId)
		throws SystemException {
		return fetchByStudentDocument(documentId, true);
	}

	/**
	 * Returns the student document where documentId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param documentId the document ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching student document, or <code>null</code> if a matching student document could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDocument fetchByStudentDocument(long documentId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { documentId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_STUDENTDOCUMENT,
					finderArgs, this);
		}

		if (result instanceof StudentDocument) {
			StudentDocument studentDocument = (StudentDocument)result;

			if ((documentId != studentDocument.getDocumentId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_SELECT_STUDENTDOCUMENT_WHERE);

			query.append(_FINDER_COLUMN_STUDENTDOCUMENT_DOCUMENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(documentId);

				List<StudentDocument> list = q.list();

				result = list;

				StudentDocument studentDocument = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_STUDENTDOCUMENT,
						finderArgs, list);
				}
				else {
					studentDocument = list.get(0);

					cacheResult(studentDocument);

					if ((studentDocument.getDocumentId() != documentId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_STUDENTDOCUMENT,
							finderArgs, studentDocument);
					}
				}

				return studentDocument;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_STUDENTDOCUMENT,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (StudentDocument)result;
			}
		}
	}

	/**
	 * Returns all the student documents where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the matching student documents
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentDocument> findByStudentDocumentList(long studentId)
		throws SystemException {
		return findByStudentDocumentList(studentId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the student documents where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of student documents
	 * @param end the upper bound of the range of student documents (not inclusive)
	 * @return the range of matching student documents
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentDocument> findByStudentDocumentList(long studentId,
		int start, int end) throws SystemException {
		return findByStudentDocumentList(studentId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the student documents where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of student documents
	 * @param end the upper bound of the range of student documents (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching student documents
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentDocument> findByStudentDocumentList(long studentId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTDOCUMENTLIST;
			finderArgs = new Object[] { studentId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTDOCUMENTLIST;
			finderArgs = new Object[] { studentId, start, end, orderByComparator };
		}

		List<StudentDocument> list = (List<StudentDocument>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (StudentDocument studentDocument : list) {
				if ((studentId != studentDocument.getStudentId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_STUDENTDOCUMENT_WHERE);

			query.append(_FINDER_COLUMN_STUDENTDOCUMENTLIST_STUDENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				list = (List<StudentDocument>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first student document in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student document
	 * @throws info.diit.portal.NoSuchStudentDocumentException if a matching student document could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDocument findByStudentDocumentList_First(long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchStudentDocumentException, SystemException {
		StudentDocument studentDocument = fetchByStudentDocumentList_First(studentId,
				orderByComparator);

		if (studentDocument != null) {
			return studentDocument;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchStudentDocumentException(msg.toString());
	}

	/**
	 * Returns the first student document in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student document, or <code>null</code> if a matching student document could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDocument fetchByStudentDocumentList_First(long studentId,
		OrderByComparator orderByComparator) throws SystemException {
		List<StudentDocument> list = findByStudentDocumentList(studentId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last student document in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student document
	 * @throws info.diit.portal.NoSuchStudentDocumentException if a matching student document could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDocument findByStudentDocumentList_Last(long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchStudentDocumentException, SystemException {
		StudentDocument studentDocument = fetchByStudentDocumentList_Last(studentId,
				orderByComparator);

		if (studentDocument != null) {
			return studentDocument;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchStudentDocumentException(msg.toString());
	}

	/**
	 * Returns the last student document in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student document, or <code>null</code> if a matching student document could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDocument fetchByStudentDocumentList_Last(long studentId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByStudentDocumentList(studentId);

		List<StudentDocument> list = findByStudentDocumentList(studentId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the student documents before and after the current student document in the ordered set where studentId = &#63;.
	 *
	 * @param documentId the primary key of the current student document
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next student document
	 * @throws info.diit.portal.NoSuchStudentDocumentException if a student document with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDocument[] findByStudentDocumentList_PrevAndNext(
		long documentId, long studentId, OrderByComparator orderByComparator)
		throws NoSuchStudentDocumentException, SystemException {
		StudentDocument studentDocument = findByPrimaryKey(documentId);

		Session session = null;

		try {
			session = openSession();

			StudentDocument[] array = new StudentDocumentImpl[3];

			array[0] = getByStudentDocumentList_PrevAndNext(session,
					studentDocument, studentId, orderByComparator, true);

			array[1] = studentDocument;

			array[2] = getByStudentDocumentList_PrevAndNext(session,
					studentDocument, studentId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected StudentDocument getByStudentDocumentList_PrevAndNext(
		Session session, StudentDocument studentDocument, long studentId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_STUDENTDOCUMENT_WHERE);

		query.append(_FINDER_COLUMN_STUDENTDOCUMENTLIST_STUDENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(studentId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(studentDocument);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<StudentDocument> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the student documents.
	 *
	 * @return the student documents
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentDocument> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the student documents.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of student documents
	 * @param end the upper bound of the range of student documents (not inclusive)
	 * @return the range of student documents
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentDocument> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the student documents.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of student documents
	 * @param end the upper bound of the range of student documents (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of student documents
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentDocument> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<StudentDocument> list = (List<StudentDocument>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_STUDENTDOCUMENT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_STUDENTDOCUMENT;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<StudentDocument>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<StudentDocument>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes the student document where documentId = &#63; from the database.
	 *
	 * @param documentId the document ID
	 * @return the student document that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDocument removeByStudentDocument(long documentId)
		throws NoSuchStudentDocumentException, SystemException {
		StudentDocument studentDocument = findByStudentDocument(documentId);

		return remove(studentDocument);
	}

	/**
	 * Removes all the student documents where studentId = &#63; from the database.
	 *
	 * @param studentId the student ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByStudentDocumentList(long studentId)
		throws SystemException {
		for (StudentDocument studentDocument : findByStudentDocumentList(
				studentId)) {
			remove(studentDocument);
		}
	}

	/**
	 * Removes all the student documents from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (StudentDocument studentDocument : findAll()) {
			remove(studentDocument);
		}
	}

	/**
	 * Returns the number of student documents where documentId = &#63;.
	 *
	 * @param documentId the document ID
	 * @return the number of matching student documents
	 * @throws SystemException if a system exception occurred
	 */
	public int countByStudentDocument(long documentId)
		throws SystemException {
		Object[] finderArgs = new Object[] { documentId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_STUDENTDOCUMENT,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_STUDENTDOCUMENT_WHERE);

			query.append(_FINDER_COLUMN_STUDENTDOCUMENT_DOCUMENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(documentId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_STUDENTDOCUMENT,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of student documents where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the number of matching student documents
	 * @throws SystemException if a system exception occurred
	 */
	public int countByStudentDocumentList(long studentId)
		throws SystemException {
		Object[] finderArgs = new Object[] { studentId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_STUDENTDOCUMENTLIST,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_STUDENTDOCUMENT_WHERE);

			query.append(_FINDER_COLUMN_STUDENTDOCUMENTLIST_STUDENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_STUDENTDOCUMENTLIST,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of student documents.
	 *
	 * @return the number of student documents
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_STUDENTDOCUMENT);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the student document persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.StudentDocument")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<StudentDocument>> listenersList = new ArrayList<ModelListener<StudentDocument>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<StudentDocument>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(StudentDocumentImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_STUDENTDOCUMENT = "SELECT studentDocument FROM StudentDocument studentDocument";
	private static final String _SQL_SELECT_STUDENTDOCUMENT_WHERE = "SELECT studentDocument FROM StudentDocument studentDocument WHERE ";
	private static final String _SQL_COUNT_STUDENTDOCUMENT = "SELECT COUNT(studentDocument) FROM StudentDocument studentDocument";
	private static final String _SQL_COUNT_STUDENTDOCUMENT_WHERE = "SELECT COUNT(studentDocument) FROM StudentDocument studentDocument WHERE ";
	private static final String _FINDER_COLUMN_STUDENTDOCUMENT_DOCUMENTID_2 = "studentDocument.documentId = ?";
	private static final String _FINDER_COLUMN_STUDENTDOCUMENTLIST_STUDENTID_2 = "studentDocument.studentId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "studentDocument.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No StudentDocument exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No StudentDocument exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(StudentDocumentPersistenceImpl.class);
	private static StudentDocument _nullStudentDocument = new StudentDocumentImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<StudentDocument> toCacheModel() {
				return _nullStudentDocumentCacheModel;
			}
		};

	private static CacheModel<StudentDocument> _nullStudentDocumentCacheModel = new CacheModel<StudentDocument>() {
			public StudentDocument toEntityModel() {
				return _nullStudentDocument;
			}
		};
}
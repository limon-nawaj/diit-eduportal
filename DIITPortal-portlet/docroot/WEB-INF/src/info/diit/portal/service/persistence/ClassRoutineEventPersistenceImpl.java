/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchClassRoutineEventException;
import info.diit.portal.model.ClassRoutineEvent;
import info.diit.portal.model.impl.ClassRoutineEventImpl;
import info.diit.portal.model.impl.ClassRoutineEventModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the class routine event service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see ClassRoutineEventPersistence
 * @see ClassRoutineEventUtil
 * @generated
 */
public class ClassRoutineEventPersistenceImpl extends BasePersistenceImpl<ClassRoutineEvent>
	implements ClassRoutineEventPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ClassRoutineEventUtil} to access the class routine event persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ClassRoutineEventImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_FETCH_BY_SUBJECTID = new FinderPath(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchBySubjectId",
			new String[] { Long.class.getName(), Long.class.getName() },
			ClassRoutineEventModelImpl.SUBJECTID_COLUMN_BITMASK |
			ClassRoutineEventModelImpl.CLASSROUTINEEVENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SUBJECTID = new FinderPath(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBySubjectId",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ROOM = new FinderPath(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByRoom",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROOM = new FinderPath(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByRoom",
			new String[] { Long.class.getName() },
			ClassRoutineEventModelImpl.ROOMID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ROOM = new FinderPath(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByRoom",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY = new FinderPath(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompany",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY =
		new FinderPath(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompany",
			new String[] { Long.class.getName() },
			ClassRoutineEventModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANY = new FinderPath(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompany",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SUBJECTDATE =
		new FinderPath(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBySubjectDate",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTDATE =
		new FinderPath(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBySubjectDate",
			new String[] { Long.class.getName(), Integer.class.getName() },
			ClassRoutineEventModelImpl.SUBJECTID_COLUMN_BITMASK |
			ClassRoutineEventModelImpl.DAY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SUBJECTDATE = new FinderPath(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBySubjectDate",
			new String[] { Long.class.getName(), Integer.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventModelImpl.FINDER_CACHE_ENABLED,
			ClassRoutineEventImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the class routine event in the entity cache if it is enabled.
	 *
	 * @param classRoutineEvent the class routine event
	 */
	public void cacheResult(ClassRoutineEvent classRoutineEvent) {
		EntityCacheUtil.putResult(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventImpl.class, classRoutineEvent.getPrimaryKey(),
			classRoutineEvent);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTID,
			new Object[] {
				Long.valueOf(classRoutineEvent.getSubjectId()),
				Long.valueOf(classRoutineEvent.getClassRoutineEventId())
			}, classRoutineEvent);

		classRoutineEvent.resetOriginalValues();
	}

	/**
	 * Caches the class routine events in the entity cache if it is enabled.
	 *
	 * @param classRoutineEvents the class routine events
	 */
	public void cacheResult(List<ClassRoutineEvent> classRoutineEvents) {
		for (ClassRoutineEvent classRoutineEvent : classRoutineEvents) {
			if (EntityCacheUtil.getResult(
						ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
						ClassRoutineEventImpl.class,
						classRoutineEvent.getPrimaryKey()) == null) {
				cacheResult(classRoutineEvent);
			}
			else {
				classRoutineEvent.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all class routine events.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ClassRoutineEventImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ClassRoutineEventImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the class routine event.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ClassRoutineEvent classRoutineEvent) {
		EntityCacheUtil.removeResult(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventImpl.class, classRoutineEvent.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(classRoutineEvent);
	}

	@Override
	public void clearCache(List<ClassRoutineEvent> classRoutineEvents) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ClassRoutineEvent classRoutineEvent : classRoutineEvents) {
			EntityCacheUtil.removeResult(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
				ClassRoutineEventImpl.class, classRoutineEvent.getPrimaryKey());

			clearUniqueFindersCache(classRoutineEvent);
		}
	}

	protected void clearUniqueFindersCache(ClassRoutineEvent classRoutineEvent) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SUBJECTID,
			new Object[] {
				Long.valueOf(classRoutineEvent.getSubjectId()),
				Long.valueOf(classRoutineEvent.getClassRoutineEventId())
			});
	}

	/**
	 * Creates a new class routine event with the primary key. Does not add the class routine event to the database.
	 *
	 * @param classRoutineEventId the primary key for the new class routine event
	 * @return the new class routine event
	 */
	public ClassRoutineEvent create(long classRoutineEventId) {
		ClassRoutineEvent classRoutineEvent = new ClassRoutineEventImpl();

		classRoutineEvent.setNew(true);
		classRoutineEvent.setPrimaryKey(classRoutineEventId);

		return classRoutineEvent;
	}

	/**
	 * Removes the class routine event with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param classRoutineEventId the primary key of the class routine event
	 * @return the class routine event that was removed
	 * @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent remove(long classRoutineEventId)
		throws NoSuchClassRoutineEventException, SystemException {
		return remove(Long.valueOf(classRoutineEventId));
	}

	/**
	 * Removes the class routine event with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the class routine event
	 * @return the class routine event that was removed
	 * @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClassRoutineEvent remove(Serializable primaryKey)
		throws NoSuchClassRoutineEventException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ClassRoutineEvent classRoutineEvent = (ClassRoutineEvent)session.get(ClassRoutineEventImpl.class,
					primaryKey);

			if (classRoutineEvent == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchClassRoutineEventException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(classRoutineEvent);
		}
		catch (NoSuchClassRoutineEventException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ClassRoutineEvent removeImpl(ClassRoutineEvent classRoutineEvent)
		throws SystemException {
		classRoutineEvent = toUnwrappedModel(classRoutineEvent);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, classRoutineEvent);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(classRoutineEvent);

		return classRoutineEvent;
	}

	@Override
	public ClassRoutineEvent updateImpl(
		info.diit.portal.model.ClassRoutineEvent classRoutineEvent,
		boolean merge) throws SystemException {
		classRoutineEvent = toUnwrappedModel(classRoutineEvent);

		boolean isNew = classRoutineEvent.isNew();

		ClassRoutineEventModelImpl classRoutineEventModelImpl = (ClassRoutineEventModelImpl)classRoutineEvent;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, classRoutineEvent, merge);

			classRoutineEvent.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ClassRoutineEventModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((classRoutineEventModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROOM.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(classRoutineEventModelImpl.getOriginalRoomId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ROOM, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROOM,
					args);

				args = new Object[] {
						Long.valueOf(classRoutineEventModelImpl.getRoomId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ROOM, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROOM,
					args);
			}

			if ((classRoutineEventModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(classRoutineEventModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);

				args = new Object[] {
						Long.valueOf(classRoutineEventModelImpl.getCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);
			}

			if ((classRoutineEventModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTDATE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(classRoutineEventModelImpl.getOriginalSubjectId()),
						Integer.valueOf(classRoutineEventModelImpl.getOriginalDay())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBJECTDATE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTDATE,
					args);

				args = new Object[] {
						Long.valueOf(classRoutineEventModelImpl.getSubjectId()),
						Integer.valueOf(classRoutineEventModelImpl.getDay())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBJECTDATE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTDATE,
					args);
			}
		}

		EntityCacheUtil.putResult(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
			ClassRoutineEventImpl.class, classRoutineEvent.getPrimaryKey(),
			classRoutineEvent);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTID,
				new Object[] {
					Long.valueOf(classRoutineEvent.getSubjectId()),
					Long.valueOf(classRoutineEvent.getClassRoutineEventId())
				}, classRoutineEvent);
		}
		else {
			if ((classRoutineEventModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_SUBJECTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(classRoutineEventModelImpl.getOriginalSubjectId()),
						Long.valueOf(classRoutineEventModelImpl.getOriginalClassRoutineEventId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBJECTID,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SUBJECTID,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTID,
					new Object[] {
						Long.valueOf(classRoutineEvent.getSubjectId()),
						Long.valueOf(classRoutineEvent.getClassRoutineEventId())
					}, classRoutineEvent);
			}
		}

		return classRoutineEvent;
	}

	protected ClassRoutineEvent toUnwrappedModel(
		ClassRoutineEvent classRoutineEvent) {
		if (classRoutineEvent instanceof ClassRoutineEventImpl) {
			return classRoutineEvent;
		}

		ClassRoutineEventImpl classRoutineEventImpl = new ClassRoutineEventImpl();

		classRoutineEventImpl.setNew(classRoutineEvent.isNew());
		classRoutineEventImpl.setPrimaryKey(classRoutineEvent.getPrimaryKey());

		classRoutineEventImpl.setClassRoutineEventId(classRoutineEvent.getClassRoutineEventId());
		classRoutineEventImpl.setCompanyId(classRoutineEvent.getCompanyId());
		classRoutineEventImpl.setUserId(classRoutineEvent.getUserId());
		classRoutineEventImpl.setUserName(classRoutineEvent.getUserName());
		classRoutineEventImpl.setCreateDate(classRoutineEvent.getCreateDate());
		classRoutineEventImpl.setModifiedDate(classRoutineEvent.getModifiedDate());
		classRoutineEventImpl.setOrganizationId(classRoutineEvent.getOrganizationId());
		classRoutineEventImpl.setSubjectId(classRoutineEvent.getSubjectId());
		classRoutineEventImpl.setDay(classRoutineEvent.getDay());
		classRoutineEventImpl.setStartTime(classRoutineEvent.getStartTime());
		classRoutineEventImpl.setEndTime(classRoutineEvent.getEndTime());
		classRoutineEventImpl.setRoomId(classRoutineEvent.getRoomId());

		return classRoutineEventImpl;
	}

	/**
	 * Returns the class routine event with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the class routine event
	 * @return the class routine event
	 * @throws com.liferay.portal.NoSuchModelException if a class routine event with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClassRoutineEvent findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the class routine event with the primary key or throws a {@link info.diit.portal.NoSuchClassRoutineEventException} if it could not be found.
	 *
	 * @param classRoutineEventId the primary key of the class routine event
	 * @return the class routine event
	 * @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent findByPrimaryKey(long classRoutineEventId)
		throws NoSuchClassRoutineEventException, SystemException {
		ClassRoutineEvent classRoutineEvent = fetchByPrimaryKey(classRoutineEventId);

		if (classRoutineEvent == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					classRoutineEventId);
			}

			throw new NoSuchClassRoutineEventException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				classRoutineEventId);
		}

		return classRoutineEvent;
	}

	/**
	 * Returns the class routine event with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the class routine event
	 * @return the class routine event, or <code>null</code> if a class routine event with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ClassRoutineEvent fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the class routine event with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param classRoutineEventId the primary key of the class routine event
	 * @return the class routine event, or <code>null</code> if a class routine event with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent fetchByPrimaryKey(long classRoutineEventId)
		throws SystemException {
		ClassRoutineEvent classRoutineEvent = (ClassRoutineEvent)EntityCacheUtil.getResult(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
				ClassRoutineEventImpl.class, classRoutineEventId);

		if (classRoutineEvent == _nullClassRoutineEvent) {
			return null;
		}

		if (classRoutineEvent == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				classRoutineEvent = (ClassRoutineEvent)session.get(ClassRoutineEventImpl.class,
						Long.valueOf(classRoutineEventId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (classRoutineEvent != null) {
					cacheResult(classRoutineEvent);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(ClassRoutineEventModelImpl.ENTITY_CACHE_ENABLED,
						ClassRoutineEventImpl.class, classRoutineEventId,
						_nullClassRoutineEvent);
				}

				closeSession(session);
			}
		}

		return classRoutineEvent;
	}

	/**
	 * Returns the class routine event where subjectId = &#63; and classRoutineEventId = &#63; or throws a {@link info.diit.portal.NoSuchClassRoutineEventException} if it could not be found.
	 *
	 * @param subjectId the subject ID
	 * @param classRoutineEventId the class routine event ID
	 * @return the matching class routine event
	 * @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent findBySubjectId(long subjectId,
		long classRoutineEventId)
		throws NoSuchClassRoutineEventException, SystemException {
		ClassRoutineEvent classRoutineEvent = fetchBySubjectId(subjectId,
				classRoutineEventId);

		if (classRoutineEvent == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("subjectId=");
			msg.append(subjectId);

			msg.append(", classRoutineEventId=");
			msg.append(classRoutineEventId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchClassRoutineEventException(msg.toString());
		}

		return classRoutineEvent;
	}

	/**
	 * Returns the class routine event where subjectId = &#63; and classRoutineEventId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param subjectId the subject ID
	 * @param classRoutineEventId the class routine event ID
	 * @return the matching class routine event, or <code>null</code> if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent fetchBySubjectId(long subjectId,
		long classRoutineEventId) throws SystemException {
		return fetchBySubjectId(subjectId, classRoutineEventId, true);
	}

	/**
	 * Returns the class routine event where subjectId = &#63; and classRoutineEventId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param subjectId the subject ID
	 * @param classRoutineEventId the class routine event ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching class routine event, or <code>null</code> if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent fetchBySubjectId(long subjectId,
		long classRoutineEventId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { subjectId, classRoutineEventId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_SUBJECTID,
					finderArgs, this);
		}

		if (result instanceof ClassRoutineEvent) {
			ClassRoutineEvent classRoutineEvent = (ClassRoutineEvent)result;

			if ((subjectId != classRoutineEvent.getSubjectId()) ||
					(classRoutineEventId != classRoutineEvent.getClassRoutineEventId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_CLASSROUTINEEVENT_WHERE);

			query.append(_FINDER_COLUMN_SUBJECTID_SUBJECTID_2);

			query.append(_FINDER_COLUMN_SUBJECTID_CLASSROUTINEEVENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				qPos.add(classRoutineEventId);

				List<ClassRoutineEvent> list = q.list();

				result = list;

				ClassRoutineEvent classRoutineEvent = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTID,
						finderArgs, list);
				}
				else {
					classRoutineEvent = list.get(0);

					cacheResult(classRoutineEvent);

					if ((classRoutineEvent.getSubjectId() != subjectId) ||
							(classRoutineEvent.getClassRoutineEventId() != classRoutineEventId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTID,
							finderArgs, classRoutineEvent);
					}
				}

				return classRoutineEvent;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SUBJECTID,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (ClassRoutineEvent)result;
			}
		}
	}

	/**
	 * Returns all the class routine events where roomId = &#63;.
	 *
	 * @param roomId the room ID
	 * @return the matching class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEvent> findByRoom(long roomId)
		throws SystemException {
		return findByRoom(roomId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the class routine events where roomId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param roomId the room ID
	 * @param start the lower bound of the range of class routine events
	 * @param end the upper bound of the range of class routine events (not inclusive)
	 * @return the range of matching class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEvent> findByRoom(long roomId, int start, int end)
		throws SystemException {
		return findByRoom(roomId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the class routine events where roomId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param roomId the room ID
	 * @param start the lower bound of the range of class routine events
	 * @param end the upper bound of the range of class routine events (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEvent> findByRoom(long roomId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROOM;
			finderArgs = new Object[] { roomId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ROOM;
			finderArgs = new Object[] { roomId, start, end, orderByComparator };
		}

		List<ClassRoutineEvent> list = (List<ClassRoutineEvent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ClassRoutineEvent classRoutineEvent : list) {
				if ((roomId != classRoutineEvent.getRoomId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_CLASSROUTINEEVENT_WHERE);

			query.append(_FINDER_COLUMN_ROOM_ROOMID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(roomId);

				list = (List<ClassRoutineEvent>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first class routine event in the ordered set where roomId = &#63;.
	 *
	 * @param roomId the room ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching class routine event
	 * @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent findByRoom_First(long roomId,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventException, SystemException {
		ClassRoutineEvent classRoutineEvent = fetchByRoom_First(roomId,
				orderByComparator);

		if (classRoutineEvent != null) {
			return classRoutineEvent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("roomId=");
		msg.append(roomId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchClassRoutineEventException(msg.toString());
	}

	/**
	 * Returns the first class routine event in the ordered set where roomId = &#63;.
	 *
	 * @param roomId the room ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching class routine event, or <code>null</code> if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent fetchByRoom_First(long roomId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ClassRoutineEvent> list = findByRoom(roomId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last class routine event in the ordered set where roomId = &#63;.
	 *
	 * @param roomId the room ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching class routine event
	 * @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent findByRoom_Last(long roomId,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventException, SystemException {
		ClassRoutineEvent classRoutineEvent = fetchByRoom_Last(roomId,
				orderByComparator);

		if (classRoutineEvent != null) {
			return classRoutineEvent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("roomId=");
		msg.append(roomId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchClassRoutineEventException(msg.toString());
	}

	/**
	 * Returns the last class routine event in the ordered set where roomId = &#63;.
	 *
	 * @param roomId the room ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching class routine event, or <code>null</code> if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent fetchByRoom_Last(long roomId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByRoom(roomId);

		List<ClassRoutineEvent> list = findByRoom(roomId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the class routine events before and after the current class routine event in the ordered set where roomId = &#63;.
	 *
	 * @param classRoutineEventId the primary key of the current class routine event
	 * @param roomId the room ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next class routine event
	 * @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent[] findByRoom_PrevAndNext(
		long classRoutineEventId, long roomId,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventException, SystemException {
		ClassRoutineEvent classRoutineEvent = findByPrimaryKey(classRoutineEventId);

		Session session = null;

		try {
			session = openSession();

			ClassRoutineEvent[] array = new ClassRoutineEventImpl[3];

			array[0] = getByRoom_PrevAndNext(session, classRoutineEvent,
					roomId, orderByComparator, true);

			array[1] = classRoutineEvent;

			array[2] = getByRoom_PrevAndNext(session, classRoutineEvent,
					roomId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ClassRoutineEvent getByRoom_PrevAndNext(Session session,
		ClassRoutineEvent classRoutineEvent, long roomId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLASSROUTINEEVENT_WHERE);

		query.append(_FINDER_COLUMN_ROOM_ROOMID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(roomId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(classRoutineEvent);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ClassRoutineEvent> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the class routine events where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEvent> findByCompany(long companyId)
		throws SystemException {
		return findByCompany(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the class routine events where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of class routine events
	 * @param end the upper bound of the range of class routine events (not inclusive)
	 * @return the range of matching class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEvent> findByCompany(long companyId, int start,
		int end) throws SystemException {
		return findByCompany(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the class routine events where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of class routine events
	 * @param end the upper bound of the range of class routine events (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEvent> findByCompany(long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<ClassRoutineEvent> list = (List<ClassRoutineEvent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ClassRoutineEvent classRoutineEvent : list) {
				if ((companyId != classRoutineEvent.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_CLASSROUTINEEVENT_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<ClassRoutineEvent>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first class routine event in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching class routine event
	 * @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent findByCompany_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventException, SystemException {
		ClassRoutineEvent classRoutineEvent = fetchByCompany_First(companyId,
				orderByComparator);

		if (classRoutineEvent != null) {
			return classRoutineEvent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchClassRoutineEventException(msg.toString());
	}

	/**
	 * Returns the first class routine event in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching class routine event, or <code>null</code> if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent fetchByCompany_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ClassRoutineEvent> list = findByCompany(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last class routine event in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching class routine event
	 * @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent findByCompany_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventException, SystemException {
		ClassRoutineEvent classRoutineEvent = fetchByCompany_Last(companyId,
				orderByComparator);

		if (classRoutineEvent != null) {
			return classRoutineEvent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchClassRoutineEventException(msg.toString());
	}

	/**
	 * Returns the last class routine event in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching class routine event, or <code>null</code> if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent fetchByCompany_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompany(companyId);

		List<ClassRoutineEvent> list = findByCompany(companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the class routine events before and after the current class routine event in the ordered set where companyId = &#63;.
	 *
	 * @param classRoutineEventId the primary key of the current class routine event
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next class routine event
	 * @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent[] findByCompany_PrevAndNext(
		long classRoutineEventId, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventException, SystemException {
		ClassRoutineEvent classRoutineEvent = findByPrimaryKey(classRoutineEventId);

		Session session = null;

		try {
			session = openSession();

			ClassRoutineEvent[] array = new ClassRoutineEventImpl[3];

			array[0] = getByCompany_PrevAndNext(session, classRoutineEvent,
					companyId, orderByComparator, true);

			array[1] = classRoutineEvent;

			array[2] = getByCompany_PrevAndNext(session, classRoutineEvent,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ClassRoutineEvent getByCompany_PrevAndNext(Session session,
		ClassRoutineEvent classRoutineEvent, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLASSROUTINEEVENT_WHERE);

		query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(classRoutineEvent);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ClassRoutineEvent> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the class routine events where subjectId = &#63; and day = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param day the day
	 * @return the matching class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEvent> findBySubjectDate(long subjectId, int day)
		throws SystemException {
		return findBySubjectDate(subjectId, day, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the class routine events where subjectId = &#63; and day = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param subjectId the subject ID
	 * @param day the day
	 * @param start the lower bound of the range of class routine events
	 * @param end the upper bound of the range of class routine events (not inclusive)
	 * @return the range of matching class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEvent> findBySubjectDate(long subjectId, int day,
		int start, int end) throws SystemException {
		return findBySubjectDate(subjectId, day, start, end, null);
	}

	/**
	 * Returns an ordered range of all the class routine events where subjectId = &#63; and day = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param subjectId the subject ID
	 * @param day the day
	 * @param start the lower bound of the range of class routine events
	 * @param end the upper bound of the range of class routine events (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEvent> findBySubjectDate(long subjectId, int day,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBJECTDATE;
			finderArgs = new Object[] { subjectId, day };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SUBJECTDATE;
			finderArgs = new Object[] {
					subjectId, day,
					
					start, end, orderByComparator
				};
		}

		List<ClassRoutineEvent> list = (List<ClassRoutineEvent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ClassRoutineEvent classRoutineEvent : list) {
				if ((subjectId != classRoutineEvent.getSubjectId()) ||
						(day != classRoutineEvent.getDay())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLASSROUTINEEVENT_WHERE);

			query.append(_FINDER_COLUMN_SUBJECTDATE_SUBJECTID_2);

			query.append(_FINDER_COLUMN_SUBJECTDATE_DAY_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				qPos.add(day);

				list = (List<ClassRoutineEvent>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param day the day
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching class routine event
	 * @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent findBySubjectDate_First(long subjectId, int day,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventException, SystemException {
		ClassRoutineEvent classRoutineEvent = fetchBySubjectDate_First(subjectId,
				day, orderByComparator);

		if (classRoutineEvent != null) {
			return classRoutineEvent;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("subjectId=");
		msg.append(subjectId);

		msg.append(", day=");
		msg.append(day);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchClassRoutineEventException(msg.toString());
	}

	/**
	 * Returns the first class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param day the day
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching class routine event, or <code>null</code> if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent fetchBySubjectDate_First(long subjectId, int day,
		OrderByComparator orderByComparator) throws SystemException {
		List<ClassRoutineEvent> list = findBySubjectDate(subjectId, day, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param day the day
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching class routine event
	 * @throws info.diit.portal.NoSuchClassRoutineEventException if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent findBySubjectDate_Last(long subjectId, int day,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventException, SystemException {
		ClassRoutineEvent classRoutineEvent = fetchBySubjectDate_Last(subjectId,
				day, orderByComparator);

		if (classRoutineEvent != null) {
			return classRoutineEvent;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("subjectId=");
		msg.append(subjectId);

		msg.append(", day=");
		msg.append(day);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchClassRoutineEventException(msg.toString());
	}

	/**
	 * Returns the last class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param day the day
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching class routine event, or <code>null</code> if a matching class routine event could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent fetchBySubjectDate_Last(long subjectId, int day,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBySubjectDate(subjectId, day);

		List<ClassRoutineEvent> list = findBySubjectDate(subjectId, day,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the class routine events before and after the current class routine event in the ordered set where subjectId = &#63; and day = &#63;.
	 *
	 * @param classRoutineEventId the primary key of the current class routine event
	 * @param subjectId the subject ID
	 * @param day the day
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next class routine event
	 * @throws info.diit.portal.NoSuchClassRoutineEventException if a class routine event with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent[] findBySubjectDate_PrevAndNext(
		long classRoutineEventId, long subjectId, int day,
		OrderByComparator orderByComparator)
		throws NoSuchClassRoutineEventException, SystemException {
		ClassRoutineEvent classRoutineEvent = findByPrimaryKey(classRoutineEventId);

		Session session = null;

		try {
			session = openSession();

			ClassRoutineEvent[] array = new ClassRoutineEventImpl[3];

			array[0] = getBySubjectDate_PrevAndNext(session, classRoutineEvent,
					subjectId, day, orderByComparator, true);

			array[1] = classRoutineEvent;

			array[2] = getBySubjectDate_PrevAndNext(session, classRoutineEvent,
					subjectId, day, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ClassRoutineEvent getBySubjectDate_PrevAndNext(Session session,
		ClassRoutineEvent classRoutineEvent, long subjectId, int day,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLASSROUTINEEVENT_WHERE);

		query.append(_FINDER_COLUMN_SUBJECTDATE_SUBJECTID_2);

		query.append(_FINDER_COLUMN_SUBJECTDATE_DAY_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(subjectId);

		qPos.add(day);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(classRoutineEvent);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ClassRoutineEvent> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the class routine events.
	 *
	 * @return the class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEvent> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the class routine events.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of class routine events
	 * @param end the upper bound of the range of class routine events (not inclusive)
	 * @return the range of class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEvent> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the class routine events.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of class routine events
	 * @param end the upper bound of the range of class routine events (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public List<ClassRoutineEvent> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ClassRoutineEvent> list = (List<ClassRoutineEvent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLASSROUTINEEVENT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLASSROUTINEEVENT;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<ClassRoutineEvent>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<ClassRoutineEvent>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes the class routine event where subjectId = &#63; and classRoutineEventId = &#63; from the database.
	 *
	 * @param subjectId the subject ID
	 * @param classRoutineEventId the class routine event ID
	 * @return the class routine event that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public ClassRoutineEvent removeBySubjectId(long subjectId,
		long classRoutineEventId)
		throws NoSuchClassRoutineEventException, SystemException {
		ClassRoutineEvent classRoutineEvent = findBySubjectId(subjectId,
				classRoutineEventId);

		return remove(classRoutineEvent);
	}

	/**
	 * Removes all the class routine events where roomId = &#63; from the database.
	 *
	 * @param roomId the room ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByRoom(long roomId) throws SystemException {
		for (ClassRoutineEvent classRoutineEvent : findByRoom(roomId)) {
			remove(classRoutineEvent);
		}
	}

	/**
	 * Removes all the class routine events where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCompany(long companyId) throws SystemException {
		for (ClassRoutineEvent classRoutineEvent : findByCompany(companyId)) {
			remove(classRoutineEvent);
		}
	}

	/**
	 * Removes all the class routine events where subjectId = &#63; and day = &#63; from the database.
	 *
	 * @param subjectId the subject ID
	 * @param day the day
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBySubjectDate(long subjectId, int day)
		throws SystemException {
		for (ClassRoutineEvent classRoutineEvent : findBySubjectDate(
				subjectId, day)) {
			remove(classRoutineEvent);
		}
	}

	/**
	 * Removes all the class routine events from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (ClassRoutineEvent classRoutineEvent : findAll()) {
			remove(classRoutineEvent);
		}
	}

	/**
	 * Returns the number of class routine events where subjectId = &#63; and classRoutineEventId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param classRoutineEventId the class routine event ID
	 * @return the number of matching class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public int countBySubjectId(long subjectId, long classRoutineEventId)
		throws SystemException {
		Object[] finderArgs = new Object[] { subjectId, classRoutineEventId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_SUBJECTID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLASSROUTINEEVENT_WHERE);

			query.append(_FINDER_COLUMN_SUBJECTID_SUBJECTID_2);

			query.append(_FINDER_COLUMN_SUBJECTID_CLASSROUTINEEVENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				qPos.add(classRoutineEventId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_SUBJECTID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of class routine events where roomId = &#63;.
	 *
	 * @param roomId the room ID
	 * @return the number of matching class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public int countByRoom(long roomId) throws SystemException {
		Object[] finderArgs = new Object[] { roomId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ROOM,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLASSROUTINEEVENT_WHERE);

			query.append(_FINDER_COLUMN_ROOM_ROOMID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(roomId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ROOM,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of class routine events where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCompany(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLASSROUTINEEVENT_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of class routine events where subjectId = &#63; and day = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @param day the day
	 * @return the number of matching class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public int countBySubjectDate(long subjectId, int day)
		throws SystemException {
		Object[] finderArgs = new Object[] { subjectId, day };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_SUBJECTDATE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLASSROUTINEEVENT_WHERE);

			query.append(_FINDER_COLUMN_SUBJECTDATE_SUBJECTID_2);

			query.append(_FINDER_COLUMN_SUBJECTDATE_DAY_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				qPos.add(day);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_SUBJECTDATE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of class routine events.
	 *
	 * @return the number of class routine events
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLASSROUTINEEVENT);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the class routine event persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.ClassRoutineEvent")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ClassRoutineEvent>> listenersList = new ArrayList<ModelListener<ClassRoutineEvent>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ClassRoutineEvent>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ClassRoutineEventImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_CLASSROUTINEEVENT = "SELECT classRoutineEvent FROM ClassRoutineEvent classRoutineEvent";
	private static final String _SQL_SELECT_CLASSROUTINEEVENT_WHERE = "SELECT classRoutineEvent FROM ClassRoutineEvent classRoutineEvent WHERE ";
	private static final String _SQL_COUNT_CLASSROUTINEEVENT = "SELECT COUNT(classRoutineEvent) FROM ClassRoutineEvent classRoutineEvent";
	private static final String _SQL_COUNT_CLASSROUTINEEVENT_WHERE = "SELECT COUNT(classRoutineEvent) FROM ClassRoutineEvent classRoutineEvent WHERE ";
	private static final String _FINDER_COLUMN_SUBJECTID_SUBJECTID_2 = "classRoutineEvent.subjectId = ? AND ";
	private static final String _FINDER_COLUMN_SUBJECTID_CLASSROUTINEEVENTID_2 = "classRoutineEvent.classRoutineEventId = ?";
	private static final String _FINDER_COLUMN_ROOM_ROOMID_2 = "classRoutineEvent.roomId = ?";
	private static final String _FINDER_COLUMN_COMPANY_COMPANYID_2 = "classRoutineEvent.companyId = ?";
	private static final String _FINDER_COLUMN_SUBJECTDATE_SUBJECTID_2 = "classRoutineEvent.subjectId = ? AND ";
	private static final String _FINDER_COLUMN_SUBJECTDATE_DAY_2 = "classRoutineEvent.day = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "classRoutineEvent.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ClassRoutineEvent exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ClassRoutineEvent exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ClassRoutineEventPersistenceImpl.class);
	private static ClassRoutineEvent _nullClassRoutineEvent = new ClassRoutineEventImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ClassRoutineEvent> toCacheModel() {
				return _nullClassRoutineEventCacheModel;
			}
		};

	private static CacheModel<ClassRoutineEvent> _nullClassRoutineEventCacheModel =
		new CacheModel<ClassRoutineEvent>() {
			public ClassRoutineEvent toEntityModel() {
				return _nullClassRoutineEvent;
			}
		};
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchSubjectLessonException;
import info.diit.portal.model.SubjectLesson;
import info.diit.portal.model.impl.SubjectLessonImpl;
import info.diit.portal.model.impl.SubjectLessonModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the subject lesson service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see SubjectLessonPersistence
 * @see SubjectLessonUtil
 * @generated
 */
public class SubjectLessonPersistenceImpl extends BasePersistenceImpl<SubjectLesson>
	implements SubjectLessonPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link SubjectLessonUtil} to access the subject lesson persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = SubjectLessonImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERORGANIZATION =
		new FinderPath(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
			SubjectLessonModelImpl.FINDER_CACHE_ENABLED,
			SubjectLessonImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUserOrganization",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATION =
		new FinderPath(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
			SubjectLessonModelImpl.FINDER_CACHE_ENABLED,
			SubjectLessonImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUserOrganization",
			new String[] { Long.class.getName(), Long.class.getName() },
			SubjectLessonModelImpl.ORGANIZATIONID_COLUMN_BITMASK |
			SubjectLessonModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERORGANIZATION = new FinderPath(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
			SubjectLessonModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUserOrganization",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHSUBJECT =
		new FinderPath(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
			SubjectLessonModelImpl.FINDER_CACHE_ENABLED,
			SubjectLessonImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByBatchSubject",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECT =
		new FinderPath(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
			SubjectLessonModelImpl.FINDER_CACHE_ENABLED,
			SubjectLessonImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBatchSubject",
			new String[] { Long.class.getName(), Long.class.getName() },
			SubjectLessonModelImpl.BATCHID_COLUMN_BITMASK |
			SubjectLessonModelImpl.SUBJECTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCHSUBJECT = new FinderPath(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
			SubjectLessonModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBatchSubject",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
			SubjectLessonModelImpl.FINDER_CACHE_ENABLED,
			SubjectLessonImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
			SubjectLessonModelImpl.FINDER_CACHE_ENABLED,
			SubjectLessonImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
			SubjectLessonModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the subject lesson in the entity cache if it is enabled.
	 *
	 * @param subjectLesson the subject lesson
	 */
	public void cacheResult(SubjectLesson subjectLesson) {
		EntityCacheUtil.putResult(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
			SubjectLessonImpl.class, subjectLesson.getPrimaryKey(),
			subjectLesson);

		subjectLesson.resetOriginalValues();
	}

	/**
	 * Caches the subject lessons in the entity cache if it is enabled.
	 *
	 * @param subjectLessons the subject lessons
	 */
	public void cacheResult(List<SubjectLesson> subjectLessons) {
		for (SubjectLesson subjectLesson : subjectLessons) {
			if (EntityCacheUtil.getResult(
						SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
						SubjectLessonImpl.class, subjectLesson.getPrimaryKey()) == null) {
				cacheResult(subjectLesson);
			}
			else {
				subjectLesson.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all subject lessons.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(SubjectLessonImpl.class.getName());
		}

		EntityCacheUtil.clearCache(SubjectLessonImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the subject lesson.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(SubjectLesson subjectLesson) {
		EntityCacheUtil.removeResult(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
			SubjectLessonImpl.class, subjectLesson.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<SubjectLesson> subjectLessons) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (SubjectLesson subjectLesson : subjectLessons) {
			EntityCacheUtil.removeResult(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
				SubjectLessonImpl.class, subjectLesson.getPrimaryKey());
		}
	}

	/**
	 * Creates a new subject lesson with the primary key. Does not add the subject lesson to the database.
	 *
	 * @param subjectLessonId the primary key for the new subject lesson
	 * @return the new subject lesson
	 */
	public SubjectLesson create(long subjectLessonId) {
		SubjectLesson subjectLesson = new SubjectLessonImpl();

		subjectLesson.setNew(true);
		subjectLesson.setPrimaryKey(subjectLessonId);

		return subjectLesson;
	}

	/**
	 * Removes the subject lesson with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param subjectLessonId the primary key of the subject lesson
	 * @return the subject lesson that was removed
	 * @throws info.diit.portal.NoSuchSubjectLessonException if a subject lesson with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public SubjectLesson remove(long subjectLessonId)
		throws NoSuchSubjectLessonException, SystemException {
		return remove(Long.valueOf(subjectLessonId));
	}

	/**
	 * Removes the subject lesson with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the subject lesson
	 * @return the subject lesson that was removed
	 * @throws info.diit.portal.NoSuchSubjectLessonException if a subject lesson with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SubjectLesson remove(Serializable primaryKey)
		throws NoSuchSubjectLessonException, SystemException {
		Session session = null;

		try {
			session = openSession();

			SubjectLesson subjectLesson = (SubjectLesson)session.get(SubjectLessonImpl.class,
					primaryKey);

			if (subjectLesson == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSubjectLessonException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(subjectLesson);
		}
		catch (NoSuchSubjectLessonException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected SubjectLesson removeImpl(SubjectLesson subjectLesson)
		throws SystemException {
		subjectLesson = toUnwrappedModel(subjectLesson);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, subjectLesson);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(subjectLesson);

		return subjectLesson;
	}

	@Override
	public SubjectLesson updateImpl(
		info.diit.portal.model.SubjectLesson subjectLesson, boolean merge)
		throws SystemException {
		subjectLesson = toUnwrappedModel(subjectLesson);

		boolean isNew = subjectLesson.isNew();

		SubjectLessonModelImpl subjectLessonModelImpl = (SubjectLessonModelImpl)subjectLesson;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, subjectLesson, merge);

			subjectLesson.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !SubjectLessonModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((subjectLessonModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(subjectLessonModelImpl.getOriginalOrganizationId()),
						Long.valueOf(subjectLessonModelImpl.getOriginalUserId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(subjectLessonModelImpl.getOrganizationId()),
						Long.valueOf(subjectLessonModelImpl.getUserId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATION,
					args);
			}

			if ((subjectLessonModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(subjectLessonModelImpl.getOriginalBatchId()),
						Long.valueOf(subjectLessonModelImpl.getOriginalSubjectId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHSUBJECT,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECT,
					args);

				args = new Object[] {
						Long.valueOf(subjectLessonModelImpl.getBatchId()),
						Long.valueOf(subjectLessonModelImpl.getSubjectId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHSUBJECT,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECT,
					args);
			}
		}

		EntityCacheUtil.putResult(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
			SubjectLessonImpl.class, subjectLesson.getPrimaryKey(),
			subjectLesson);

		return subjectLesson;
	}

	protected SubjectLesson toUnwrappedModel(SubjectLesson subjectLesson) {
		if (subjectLesson instanceof SubjectLessonImpl) {
			return subjectLesson;
		}

		SubjectLessonImpl subjectLessonImpl = new SubjectLessonImpl();

		subjectLessonImpl.setNew(subjectLesson.isNew());
		subjectLessonImpl.setPrimaryKey(subjectLesson.getPrimaryKey());

		subjectLessonImpl.setSubjectLessonId(subjectLesson.getSubjectLessonId());
		subjectLessonImpl.setCompanyId(subjectLesson.getCompanyId());
		subjectLessonImpl.setUserId(subjectLesson.getUserId());
		subjectLessonImpl.setUserName(subjectLesson.getUserName());
		subjectLessonImpl.setCreateDate(subjectLesson.getCreateDate());
		subjectLessonImpl.setModifiedDate(subjectLesson.getModifiedDate());
		subjectLessonImpl.setOrganizationId(subjectLesson.getOrganizationId());
		subjectLessonImpl.setBatchId(subjectLesson.getBatchId());
		subjectLessonImpl.setSubjectId(subjectLesson.getSubjectId());
		subjectLessonImpl.setLessonPlanId(subjectLesson.getLessonPlanId());

		return subjectLessonImpl;
	}

	/**
	 * Returns the subject lesson with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the subject lesson
	 * @return the subject lesson
	 * @throws com.liferay.portal.NoSuchModelException if a subject lesson with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SubjectLesson findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the subject lesson with the primary key or throws a {@link info.diit.portal.NoSuchSubjectLessonException} if it could not be found.
	 *
	 * @param subjectLessonId the primary key of the subject lesson
	 * @return the subject lesson
	 * @throws info.diit.portal.NoSuchSubjectLessonException if a subject lesson with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public SubjectLesson findByPrimaryKey(long subjectLessonId)
		throws NoSuchSubjectLessonException, SystemException {
		SubjectLesson subjectLesson = fetchByPrimaryKey(subjectLessonId);

		if (subjectLesson == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + subjectLessonId);
			}

			throw new NoSuchSubjectLessonException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				subjectLessonId);
		}

		return subjectLesson;
	}

	/**
	 * Returns the subject lesson with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the subject lesson
	 * @return the subject lesson, or <code>null</code> if a subject lesson with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SubjectLesson fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the subject lesson with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param subjectLessonId the primary key of the subject lesson
	 * @return the subject lesson, or <code>null</code> if a subject lesson with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public SubjectLesson fetchByPrimaryKey(long subjectLessonId)
		throws SystemException {
		SubjectLesson subjectLesson = (SubjectLesson)EntityCacheUtil.getResult(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
				SubjectLessonImpl.class, subjectLessonId);

		if (subjectLesson == _nullSubjectLesson) {
			return null;
		}

		if (subjectLesson == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				subjectLesson = (SubjectLesson)session.get(SubjectLessonImpl.class,
						Long.valueOf(subjectLessonId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (subjectLesson != null) {
					cacheResult(subjectLesson);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(SubjectLessonModelImpl.ENTITY_CACHE_ENABLED,
						SubjectLessonImpl.class, subjectLessonId,
						_nullSubjectLesson);
				}

				closeSession(session);
			}
		}

		return subjectLesson;
	}

	/**
	 * Returns all the subject lessons where organizationId = &#63; and userId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param userId the user ID
	 * @return the matching subject lessons
	 * @throws SystemException if a system exception occurred
	 */
	public List<SubjectLesson> findByUserOrganization(long organizationId,
		long userId) throws SystemException {
		return findByUserOrganization(organizationId, userId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the subject lessons where organizationId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of subject lessons
	 * @param end the upper bound of the range of subject lessons (not inclusive)
	 * @return the range of matching subject lessons
	 * @throws SystemException if a system exception occurred
	 */
	public List<SubjectLesson> findByUserOrganization(long organizationId,
		long userId, int start, int end) throws SystemException {
		return findByUserOrganization(organizationId, userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the subject lessons where organizationId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of subject lessons
	 * @param end the upper bound of the range of subject lessons (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching subject lessons
	 * @throws SystemException if a system exception occurred
	 */
	public List<SubjectLesson> findByUserOrganization(long organizationId,
		long userId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERORGANIZATION;
			finderArgs = new Object[] { organizationId, userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERORGANIZATION;
			finderArgs = new Object[] {
					organizationId, userId,
					
					start, end, orderByComparator
				};
		}

		List<SubjectLesson> list = (List<SubjectLesson>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (SubjectLesson subjectLesson : list) {
				if ((organizationId != subjectLesson.getOrganizationId()) ||
						(userId != subjectLesson.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_SUBJECTLESSON_WHERE);

			query.append(_FINDER_COLUMN_USERORGANIZATION_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_USERORGANIZATION_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(userId);

				list = (List<SubjectLesson>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching subject lesson
	 * @throws info.diit.portal.NoSuchSubjectLessonException if a matching subject lesson could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public SubjectLesson findByUserOrganization_First(long organizationId,
		long userId, OrderByComparator orderByComparator)
		throws NoSuchSubjectLessonException, SystemException {
		SubjectLesson subjectLesson = fetchByUserOrganization_First(organizationId,
				userId, orderByComparator);

		if (subjectLesson != null) {
			return subjectLesson;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(", userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSubjectLessonException(msg.toString());
	}

	/**
	 * Returns the first subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching subject lesson, or <code>null</code> if a matching subject lesson could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public SubjectLesson fetchByUserOrganization_First(long organizationId,
		long userId, OrderByComparator orderByComparator)
		throws SystemException {
		List<SubjectLesson> list = findByUserOrganization(organizationId,
				userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching subject lesson
	 * @throws info.diit.portal.NoSuchSubjectLessonException if a matching subject lesson could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public SubjectLesson findByUserOrganization_Last(long organizationId,
		long userId, OrderByComparator orderByComparator)
		throws NoSuchSubjectLessonException, SystemException {
		SubjectLesson subjectLesson = fetchByUserOrganization_Last(organizationId,
				userId, orderByComparator);

		if (subjectLesson != null) {
			return subjectLesson;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(", userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSubjectLessonException(msg.toString());
	}

	/**
	 * Returns the last subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching subject lesson, or <code>null</code> if a matching subject lesson could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public SubjectLesson fetchByUserOrganization_Last(long organizationId,
		long userId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByUserOrganization(organizationId, userId);

		List<SubjectLesson> list = findByUserOrganization(organizationId,
				userId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the subject lessons before and after the current subject lesson in the ordered set where organizationId = &#63; and userId = &#63;.
	 *
	 * @param subjectLessonId the primary key of the current subject lesson
	 * @param organizationId the organization ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next subject lesson
	 * @throws info.diit.portal.NoSuchSubjectLessonException if a subject lesson with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public SubjectLesson[] findByUserOrganization_PrevAndNext(
		long subjectLessonId, long organizationId, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchSubjectLessonException, SystemException {
		SubjectLesson subjectLesson = findByPrimaryKey(subjectLessonId);

		Session session = null;

		try {
			session = openSession();

			SubjectLesson[] array = new SubjectLessonImpl[3];

			array[0] = getByUserOrganization_PrevAndNext(session,
					subjectLesson, organizationId, userId, orderByComparator,
					true);

			array[1] = subjectLesson;

			array[2] = getByUserOrganization_PrevAndNext(session,
					subjectLesson, organizationId, userId, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected SubjectLesson getByUserOrganization_PrevAndNext(Session session,
		SubjectLesson subjectLesson, long organizationId, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SUBJECTLESSON_WHERE);

		query.append(_FINDER_COLUMN_USERORGANIZATION_ORGANIZATIONID_2);

		query.append(_FINDER_COLUMN_USERORGANIZATION_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(subjectLesson);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<SubjectLesson> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the subject lessons where batchId = &#63; and subjectId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param subjectId the subject ID
	 * @return the matching subject lessons
	 * @throws SystemException if a system exception occurred
	 */
	public List<SubjectLesson> findByBatchSubject(long batchId, long subjectId)
		throws SystemException {
		return findByBatchSubject(batchId, subjectId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the subject lessons where batchId = &#63; and subjectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param subjectId the subject ID
	 * @param start the lower bound of the range of subject lessons
	 * @param end the upper bound of the range of subject lessons (not inclusive)
	 * @return the range of matching subject lessons
	 * @throws SystemException if a system exception occurred
	 */
	public List<SubjectLesson> findByBatchSubject(long batchId, long subjectId,
		int start, int end) throws SystemException {
		return findByBatchSubject(batchId, subjectId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the subject lessons where batchId = &#63; and subjectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param subjectId the subject ID
	 * @param start the lower bound of the range of subject lessons
	 * @param end the upper bound of the range of subject lessons (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching subject lessons
	 * @throws SystemException if a system exception occurred
	 */
	public List<SubjectLesson> findByBatchSubject(long batchId, long subjectId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECT;
			finderArgs = new Object[] { batchId, subjectId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHSUBJECT;
			finderArgs = new Object[] {
					batchId, subjectId,
					
					start, end, orderByComparator
				};
		}

		List<SubjectLesson> list = (List<SubjectLesson>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (SubjectLesson subjectLesson : list) {
				if ((batchId != subjectLesson.getBatchId()) ||
						(subjectId != subjectLesson.getSubjectId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_SUBJECTLESSON_WHERE);

			query.append(_FINDER_COLUMN_BATCHSUBJECT_BATCHID_2);

			query.append(_FINDER_COLUMN_BATCHSUBJECT_SUBJECTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				qPos.add(subjectId);

				list = (List<SubjectLesson>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param subjectId the subject ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching subject lesson
	 * @throws info.diit.portal.NoSuchSubjectLessonException if a matching subject lesson could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public SubjectLesson findByBatchSubject_First(long batchId, long subjectId,
		OrderByComparator orderByComparator)
		throws NoSuchSubjectLessonException, SystemException {
		SubjectLesson subjectLesson = fetchByBatchSubject_First(batchId,
				subjectId, orderByComparator);

		if (subjectLesson != null) {
			return subjectLesson;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(", subjectId=");
		msg.append(subjectId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSubjectLessonException(msg.toString());
	}

	/**
	 * Returns the first subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param subjectId the subject ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching subject lesson, or <code>null</code> if a matching subject lesson could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public SubjectLesson fetchByBatchSubject_First(long batchId,
		long subjectId, OrderByComparator orderByComparator)
		throws SystemException {
		List<SubjectLesson> list = findByBatchSubject(batchId, subjectId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param subjectId the subject ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching subject lesson
	 * @throws info.diit.portal.NoSuchSubjectLessonException if a matching subject lesson could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public SubjectLesson findByBatchSubject_Last(long batchId, long subjectId,
		OrderByComparator orderByComparator)
		throws NoSuchSubjectLessonException, SystemException {
		SubjectLesson subjectLesson = fetchByBatchSubject_Last(batchId,
				subjectId, orderByComparator);

		if (subjectLesson != null) {
			return subjectLesson;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(", subjectId=");
		msg.append(subjectId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSubjectLessonException(msg.toString());
	}

	/**
	 * Returns the last subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param subjectId the subject ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching subject lesson, or <code>null</code> if a matching subject lesson could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public SubjectLesson fetchByBatchSubject_Last(long batchId, long subjectId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBatchSubject(batchId, subjectId);

		List<SubjectLesson> list = findByBatchSubject(batchId, subjectId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the subject lessons before and after the current subject lesson in the ordered set where batchId = &#63; and subjectId = &#63;.
	 *
	 * @param subjectLessonId the primary key of the current subject lesson
	 * @param batchId the batch ID
	 * @param subjectId the subject ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next subject lesson
	 * @throws info.diit.portal.NoSuchSubjectLessonException if a subject lesson with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public SubjectLesson[] findByBatchSubject_PrevAndNext(
		long subjectLessonId, long batchId, long subjectId,
		OrderByComparator orderByComparator)
		throws NoSuchSubjectLessonException, SystemException {
		SubjectLesson subjectLesson = findByPrimaryKey(subjectLessonId);

		Session session = null;

		try {
			session = openSession();

			SubjectLesson[] array = new SubjectLessonImpl[3];

			array[0] = getByBatchSubject_PrevAndNext(session, subjectLesson,
					batchId, subjectId, orderByComparator, true);

			array[1] = subjectLesson;

			array[2] = getByBatchSubject_PrevAndNext(session, subjectLesson,
					batchId, subjectId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected SubjectLesson getByBatchSubject_PrevAndNext(Session session,
		SubjectLesson subjectLesson, long batchId, long subjectId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SUBJECTLESSON_WHERE);

		query.append(_FINDER_COLUMN_BATCHSUBJECT_BATCHID_2);

		query.append(_FINDER_COLUMN_BATCHSUBJECT_SUBJECTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(batchId);

		qPos.add(subjectId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(subjectLesson);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<SubjectLesson> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the subject lessons.
	 *
	 * @return the subject lessons
	 * @throws SystemException if a system exception occurred
	 */
	public List<SubjectLesson> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the subject lessons.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of subject lessons
	 * @param end the upper bound of the range of subject lessons (not inclusive)
	 * @return the range of subject lessons
	 * @throws SystemException if a system exception occurred
	 */
	public List<SubjectLesson> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the subject lessons.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of subject lessons
	 * @param end the upper bound of the range of subject lessons (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of subject lessons
	 * @throws SystemException if a system exception occurred
	 */
	public List<SubjectLesson> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<SubjectLesson> list = (List<SubjectLesson>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_SUBJECTLESSON);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SUBJECTLESSON;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<SubjectLesson>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<SubjectLesson>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the subject lessons where organizationId = &#63; and userId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUserOrganization(long organizationId, long userId)
		throws SystemException {
		for (SubjectLesson subjectLesson : findByUserOrganization(
				organizationId, userId)) {
			remove(subjectLesson);
		}
	}

	/**
	 * Removes all the subject lessons where batchId = &#63; and subjectId = &#63; from the database.
	 *
	 * @param batchId the batch ID
	 * @param subjectId the subject ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBatchSubject(long batchId, long subjectId)
		throws SystemException {
		for (SubjectLesson subjectLesson : findByBatchSubject(batchId, subjectId)) {
			remove(subjectLesson);
		}
	}

	/**
	 * Removes all the subject lessons from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (SubjectLesson subjectLesson : findAll()) {
			remove(subjectLesson);
		}
	}

	/**
	 * Returns the number of subject lessons where organizationId = &#63; and userId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param userId the user ID
	 * @return the number of matching subject lessons
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUserOrganization(long organizationId, long userId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId, userId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_SUBJECTLESSON_WHERE);

			query.append(_FINDER_COLUMN_USERORGANIZATION_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_USERORGANIZATION_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(userId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of subject lessons where batchId = &#63; and subjectId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param subjectId the subject ID
	 * @return the number of matching subject lessons
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBatchSubject(long batchId, long subjectId)
		throws SystemException {
		Object[] finderArgs = new Object[] { batchId, subjectId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCHSUBJECT,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_SUBJECTLESSON_WHERE);

			query.append(_FINDER_COLUMN_BATCHSUBJECT_BATCHID_2);

			query.append(_FINDER_COLUMN_BATCHSUBJECT_SUBJECTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				qPos.add(subjectId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCHSUBJECT,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of subject lessons.
	 *
	 * @return the number of subject lessons
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SUBJECTLESSON);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the subject lesson persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.SubjectLesson")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<SubjectLesson>> listenersList = new ArrayList<ModelListener<SubjectLesson>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<SubjectLesson>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(SubjectLessonImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_SUBJECTLESSON = "SELECT subjectLesson FROM SubjectLesson subjectLesson";
	private static final String _SQL_SELECT_SUBJECTLESSON_WHERE = "SELECT subjectLesson FROM SubjectLesson subjectLesson WHERE ";
	private static final String _SQL_COUNT_SUBJECTLESSON = "SELECT COUNT(subjectLesson) FROM SubjectLesson subjectLesson";
	private static final String _SQL_COUNT_SUBJECTLESSON_WHERE = "SELECT COUNT(subjectLesson) FROM SubjectLesson subjectLesson WHERE ";
	private static final String _FINDER_COLUMN_USERORGANIZATION_ORGANIZATIONID_2 =
		"subjectLesson.organizationId = ? AND ";
	private static final String _FINDER_COLUMN_USERORGANIZATION_USERID_2 = "subjectLesson.userId = ?";
	private static final String _FINDER_COLUMN_BATCHSUBJECT_BATCHID_2 = "subjectLesson.batchId = ? AND ";
	private static final String _FINDER_COLUMN_BATCHSUBJECT_SUBJECTID_2 = "subjectLesson.subjectId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "subjectLesson.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No SubjectLesson exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No SubjectLesson exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(SubjectLessonPersistenceImpl.class);
	private static SubjectLesson _nullSubjectLesson = new SubjectLessonImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<SubjectLesson> toCacheModel() {
				return _nullSubjectLessonCacheModel;
			}
		};

	private static CacheModel<SubjectLesson> _nullSubjectLessonCacheModel = new CacheModel<SubjectLesson>() {
			public SubjectLesson toEntityModel() {
				return _nullSubjectLesson;
			}
		};
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchPhoneNumberException;
import info.diit.portal.model.PhoneNumber;
import info.diit.portal.model.impl.PhoneNumberImpl;
import info.diit.portal.model.impl.PhoneNumberModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the phone number service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see PhoneNumberPersistence
 * @see PhoneNumberUtil
 * @generated
 */
public class PhoneNumberPersistenceImpl extends BasePersistenceImpl<PhoneNumber>
	implements PhoneNumberPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PhoneNumberUtil} to access the phone number persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PhoneNumberImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTPHONENUMBERLIST =
		new FinderPath(PhoneNumberModelImpl.ENTITY_CACHE_ENABLED,
			PhoneNumberModelImpl.FINDER_CACHE_ENABLED, PhoneNumberImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByStudentPhoneNumberList",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTPHONENUMBERLIST =
		new FinderPath(PhoneNumberModelImpl.ENTITY_CACHE_ENABLED,
			PhoneNumberModelImpl.FINDER_CACHE_ENABLED, PhoneNumberImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByStudentPhoneNumberList",
			new String[] { Long.class.getName() },
			PhoneNumberModelImpl.STUDENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STUDENTPHONENUMBERLIST = new FinderPath(PhoneNumberModelImpl.ENTITY_CACHE_ENABLED,
			PhoneNumberModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByStudentPhoneNumberList",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PhoneNumberModelImpl.ENTITY_CACHE_ENABLED,
			PhoneNumberModelImpl.FINDER_CACHE_ENABLED, PhoneNumberImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PhoneNumberModelImpl.ENTITY_CACHE_ENABLED,
			PhoneNumberModelImpl.FINDER_CACHE_ENABLED, PhoneNumberImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PhoneNumberModelImpl.ENTITY_CACHE_ENABLED,
			PhoneNumberModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the phone number in the entity cache if it is enabled.
	 *
	 * @param phoneNumber the phone number
	 */
	public void cacheResult(PhoneNumber phoneNumber) {
		EntityCacheUtil.putResult(PhoneNumberModelImpl.ENTITY_CACHE_ENABLED,
			PhoneNumberImpl.class, phoneNumber.getPrimaryKey(), phoneNumber);

		phoneNumber.resetOriginalValues();
	}

	/**
	 * Caches the phone numbers in the entity cache if it is enabled.
	 *
	 * @param phoneNumbers the phone numbers
	 */
	public void cacheResult(List<PhoneNumber> phoneNumbers) {
		for (PhoneNumber phoneNumber : phoneNumbers) {
			if (EntityCacheUtil.getResult(
						PhoneNumberModelImpl.ENTITY_CACHE_ENABLED,
						PhoneNumberImpl.class, phoneNumber.getPrimaryKey()) == null) {
				cacheResult(phoneNumber);
			}
			else {
				phoneNumber.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all phone numbers.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(PhoneNumberImpl.class.getName());
		}

		EntityCacheUtil.clearCache(PhoneNumberImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the phone number.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(PhoneNumber phoneNumber) {
		EntityCacheUtil.removeResult(PhoneNumberModelImpl.ENTITY_CACHE_ENABLED,
			PhoneNumberImpl.class, phoneNumber.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<PhoneNumber> phoneNumbers) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (PhoneNumber phoneNumber : phoneNumbers) {
			EntityCacheUtil.removeResult(PhoneNumberModelImpl.ENTITY_CACHE_ENABLED,
				PhoneNumberImpl.class, phoneNumber.getPrimaryKey());
		}
	}

	/**
	 * Creates a new phone number with the primary key. Does not add the phone number to the database.
	 *
	 * @param phoneNumberId the primary key for the new phone number
	 * @return the new phone number
	 */
	public PhoneNumber create(long phoneNumberId) {
		PhoneNumber phoneNumber = new PhoneNumberImpl();

		phoneNumber.setNew(true);
		phoneNumber.setPrimaryKey(phoneNumberId);

		return phoneNumber;
	}

	/**
	 * Removes the phone number with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param phoneNumberId the primary key of the phone number
	 * @return the phone number that was removed
	 * @throws info.diit.portal.NoSuchPhoneNumberException if a phone number with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PhoneNumber remove(long phoneNumberId)
		throws NoSuchPhoneNumberException, SystemException {
		return remove(Long.valueOf(phoneNumberId));
	}

	/**
	 * Removes the phone number with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the phone number
	 * @return the phone number that was removed
	 * @throws info.diit.portal.NoSuchPhoneNumberException if a phone number with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PhoneNumber remove(Serializable primaryKey)
		throws NoSuchPhoneNumberException, SystemException {
		Session session = null;

		try {
			session = openSession();

			PhoneNumber phoneNumber = (PhoneNumber)session.get(PhoneNumberImpl.class,
					primaryKey);

			if (phoneNumber == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchPhoneNumberException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(phoneNumber);
		}
		catch (NoSuchPhoneNumberException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected PhoneNumber removeImpl(PhoneNumber phoneNumber)
		throws SystemException {
		phoneNumber = toUnwrappedModel(phoneNumber);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, phoneNumber);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(phoneNumber);

		return phoneNumber;
	}

	@Override
	public PhoneNumber updateImpl(
		info.diit.portal.model.PhoneNumber phoneNumber, boolean merge)
		throws SystemException {
		phoneNumber = toUnwrappedModel(phoneNumber);

		boolean isNew = phoneNumber.isNew();

		PhoneNumberModelImpl phoneNumberModelImpl = (PhoneNumberModelImpl)phoneNumber;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, phoneNumber, merge);

			phoneNumber.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !PhoneNumberModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((phoneNumberModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTPHONENUMBERLIST.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(phoneNumberModelImpl.getOriginalStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTPHONENUMBERLIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTPHONENUMBERLIST,
					args);

				args = new Object[] {
						Long.valueOf(phoneNumberModelImpl.getStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTPHONENUMBERLIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTPHONENUMBERLIST,
					args);
			}
		}

		EntityCacheUtil.putResult(PhoneNumberModelImpl.ENTITY_CACHE_ENABLED,
			PhoneNumberImpl.class, phoneNumber.getPrimaryKey(), phoneNumber);

		return phoneNumber;
	}

	protected PhoneNumber toUnwrappedModel(PhoneNumber phoneNumber) {
		if (phoneNumber instanceof PhoneNumberImpl) {
			return phoneNumber;
		}

		PhoneNumberImpl phoneNumberImpl = new PhoneNumberImpl();

		phoneNumberImpl.setNew(phoneNumber.isNew());
		phoneNumberImpl.setPrimaryKey(phoneNumber.getPrimaryKey());

		phoneNumberImpl.setPhoneNumberId(phoneNumber.getPhoneNumberId());
		phoneNumberImpl.setCompanyId(phoneNumber.getCompanyId());
		phoneNumberImpl.setUserId(phoneNumber.getUserId());
		phoneNumberImpl.setUserName(phoneNumber.getUserName());
		phoneNumberImpl.setCreateDate(phoneNumber.getCreateDate());
		phoneNumberImpl.setModifiedDate(phoneNumber.getModifiedDate());
		phoneNumberImpl.setOrganizationId(phoneNumber.getOrganizationId());
		phoneNumberImpl.setOwnerType(phoneNumber.getOwnerType());
		phoneNumberImpl.setPhoneNumber(phoneNumber.getPhoneNumber());
		phoneNumberImpl.setStudentId(phoneNumber.getStudentId());

		return phoneNumberImpl;
	}

	/**
	 * Returns the phone number with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the phone number
	 * @return the phone number
	 * @throws com.liferay.portal.NoSuchModelException if a phone number with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PhoneNumber findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the phone number with the primary key or throws a {@link info.diit.portal.NoSuchPhoneNumberException} if it could not be found.
	 *
	 * @param phoneNumberId the primary key of the phone number
	 * @return the phone number
	 * @throws info.diit.portal.NoSuchPhoneNumberException if a phone number with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PhoneNumber findByPrimaryKey(long phoneNumberId)
		throws NoSuchPhoneNumberException, SystemException {
		PhoneNumber phoneNumber = fetchByPrimaryKey(phoneNumberId);

		if (phoneNumber == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + phoneNumberId);
			}

			throw new NoSuchPhoneNumberException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				phoneNumberId);
		}

		return phoneNumber;
	}

	/**
	 * Returns the phone number with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the phone number
	 * @return the phone number, or <code>null</code> if a phone number with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PhoneNumber fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the phone number with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param phoneNumberId the primary key of the phone number
	 * @return the phone number, or <code>null</code> if a phone number with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PhoneNumber fetchByPrimaryKey(long phoneNumberId)
		throws SystemException {
		PhoneNumber phoneNumber = (PhoneNumber)EntityCacheUtil.getResult(PhoneNumberModelImpl.ENTITY_CACHE_ENABLED,
				PhoneNumberImpl.class, phoneNumberId);

		if (phoneNumber == _nullPhoneNumber) {
			return null;
		}

		if (phoneNumber == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				phoneNumber = (PhoneNumber)session.get(PhoneNumberImpl.class,
						Long.valueOf(phoneNumberId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (phoneNumber != null) {
					cacheResult(phoneNumber);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(PhoneNumberModelImpl.ENTITY_CACHE_ENABLED,
						PhoneNumberImpl.class, phoneNumberId, _nullPhoneNumber);
				}

				closeSession(session);
			}
		}

		return phoneNumber;
	}

	/**
	 * Returns all the phone numbers where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the matching phone numbers
	 * @throws SystemException if a system exception occurred
	 */
	public List<PhoneNumber> findByStudentPhoneNumberList(long studentId)
		throws SystemException {
		return findByStudentPhoneNumberList(studentId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the phone numbers where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of phone numbers
	 * @param end the upper bound of the range of phone numbers (not inclusive)
	 * @return the range of matching phone numbers
	 * @throws SystemException if a system exception occurred
	 */
	public List<PhoneNumber> findByStudentPhoneNumberList(long studentId,
		int start, int end) throws SystemException {
		return findByStudentPhoneNumberList(studentId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the phone numbers where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of phone numbers
	 * @param end the upper bound of the range of phone numbers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching phone numbers
	 * @throws SystemException if a system exception occurred
	 */
	public List<PhoneNumber> findByStudentPhoneNumberList(long studentId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTPHONENUMBERLIST;
			finderArgs = new Object[] { studentId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTPHONENUMBERLIST;
			finderArgs = new Object[] { studentId, start, end, orderByComparator };
		}

		List<PhoneNumber> list = (List<PhoneNumber>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (PhoneNumber phoneNumber : list) {
				if ((studentId != phoneNumber.getStudentId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_PHONENUMBER_WHERE);

			query.append(_FINDER_COLUMN_STUDENTPHONENUMBERLIST_STUDENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				list = (List<PhoneNumber>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first phone number in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching phone number
	 * @throws info.diit.portal.NoSuchPhoneNumberException if a matching phone number could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PhoneNumber findByStudentPhoneNumberList_First(long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchPhoneNumberException, SystemException {
		PhoneNumber phoneNumber = fetchByStudentPhoneNumberList_First(studentId,
				orderByComparator);

		if (phoneNumber != null) {
			return phoneNumber;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPhoneNumberException(msg.toString());
	}

	/**
	 * Returns the first phone number in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching phone number, or <code>null</code> if a matching phone number could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PhoneNumber fetchByStudentPhoneNumberList_First(long studentId,
		OrderByComparator orderByComparator) throws SystemException {
		List<PhoneNumber> list = findByStudentPhoneNumberList(studentId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last phone number in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching phone number
	 * @throws info.diit.portal.NoSuchPhoneNumberException if a matching phone number could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PhoneNumber findByStudentPhoneNumberList_Last(long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchPhoneNumberException, SystemException {
		PhoneNumber phoneNumber = fetchByStudentPhoneNumberList_Last(studentId,
				orderByComparator);

		if (phoneNumber != null) {
			return phoneNumber;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPhoneNumberException(msg.toString());
	}

	/**
	 * Returns the last phone number in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching phone number, or <code>null</code> if a matching phone number could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PhoneNumber fetchByStudentPhoneNumberList_Last(long studentId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByStudentPhoneNumberList(studentId);

		List<PhoneNumber> list = findByStudentPhoneNumberList(studentId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the phone numbers before and after the current phone number in the ordered set where studentId = &#63;.
	 *
	 * @param phoneNumberId the primary key of the current phone number
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next phone number
	 * @throws info.diit.portal.NoSuchPhoneNumberException if a phone number with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PhoneNumber[] findByStudentPhoneNumberList_PrevAndNext(
		long phoneNumberId, long studentId, OrderByComparator orderByComparator)
		throws NoSuchPhoneNumberException, SystemException {
		PhoneNumber phoneNumber = findByPrimaryKey(phoneNumberId);

		Session session = null;

		try {
			session = openSession();

			PhoneNumber[] array = new PhoneNumberImpl[3];

			array[0] = getByStudentPhoneNumberList_PrevAndNext(session,
					phoneNumber, studentId, orderByComparator, true);

			array[1] = phoneNumber;

			array[2] = getByStudentPhoneNumberList_PrevAndNext(session,
					phoneNumber, studentId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected PhoneNumber getByStudentPhoneNumberList_PrevAndNext(
		Session session, PhoneNumber phoneNumber, long studentId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PHONENUMBER_WHERE);

		query.append(_FINDER_COLUMN_STUDENTPHONENUMBERLIST_STUDENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(studentId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(phoneNumber);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<PhoneNumber> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the phone numbers.
	 *
	 * @return the phone numbers
	 * @throws SystemException if a system exception occurred
	 */
	public List<PhoneNumber> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the phone numbers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of phone numbers
	 * @param end the upper bound of the range of phone numbers (not inclusive)
	 * @return the range of phone numbers
	 * @throws SystemException if a system exception occurred
	 */
	public List<PhoneNumber> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the phone numbers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of phone numbers
	 * @param end the upper bound of the range of phone numbers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of phone numbers
	 * @throws SystemException if a system exception occurred
	 */
	public List<PhoneNumber> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<PhoneNumber> list = (List<PhoneNumber>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PHONENUMBER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PHONENUMBER;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<PhoneNumber>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<PhoneNumber>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the phone numbers where studentId = &#63; from the database.
	 *
	 * @param studentId the student ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByStudentPhoneNumberList(long studentId)
		throws SystemException {
		for (PhoneNumber phoneNumber : findByStudentPhoneNumberList(studentId)) {
			remove(phoneNumber);
		}
	}

	/**
	 * Removes all the phone numbers from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (PhoneNumber phoneNumber : findAll()) {
			remove(phoneNumber);
		}
	}

	/**
	 * Returns the number of phone numbers where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the number of matching phone numbers
	 * @throws SystemException if a system exception occurred
	 */
	public int countByStudentPhoneNumberList(long studentId)
		throws SystemException {
		Object[] finderArgs = new Object[] { studentId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_STUDENTPHONENUMBERLIST,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PHONENUMBER_WHERE);

			query.append(_FINDER_COLUMN_STUDENTPHONENUMBERLIST_STUDENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_STUDENTPHONENUMBERLIST,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of phone numbers.
	 *
	 * @return the number of phone numbers
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PHONENUMBER);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the phone number persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.PhoneNumber")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<PhoneNumber>> listenersList = new ArrayList<ModelListener<PhoneNumber>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<PhoneNumber>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(PhoneNumberImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_PHONENUMBER = "SELECT phoneNumber FROM PhoneNumber phoneNumber";
	private static final String _SQL_SELECT_PHONENUMBER_WHERE = "SELECT phoneNumber FROM PhoneNumber phoneNumber WHERE ";
	private static final String _SQL_COUNT_PHONENUMBER = "SELECT COUNT(phoneNumber) FROM PhoneNumber phoneNumber";
	private static final String _SQL_COUNT_PHONENUMBER_WHERE = "SELECT COUNT(phoneNumber) FROM PhoneNumber phoneNumber WHERE ";
	private static final String _FINDER_COLUMN_STUDENTPHONENUMBERLIST_STUDENTID_2 =
		"phoneNumber.studentId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "phoneNumber.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PhoneNumber exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No PhoneNumber exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(PhoneNumberPersistenceImpl.class);
	private static PhoneNumber _nullPhoneNumber = new PhoneNumberImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<PhoneNumber> toCacheModel() {
				return _nullPhoneNumberCacheModel;
			}
		};

	private static CacheModel<PhoneNumber> _nullPhoneNumberCacheModel = new CacheModel<PhoneNumber>() {
			public PhoneNumber toEntityModel() {
				return _nullPhoneNumber;
			}
		};
}
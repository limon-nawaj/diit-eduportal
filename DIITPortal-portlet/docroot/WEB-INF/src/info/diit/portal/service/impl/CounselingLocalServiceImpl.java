/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.impl;

import info.diit.portal.model.Counseling;
import info.diit.portal.model.CounselingCourseInterest;
import info.diit.portal.service.CounselingCourseInterestLocalServiceUtil;
import info.diit.portal.service.CounselingLocalServiceUtil;
import info.diit.portal.service.base.CounselingLocalServiceBaseImpl;
import info.diit.portal.service.persistence.CounselingUtil;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the counseling local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.service.CounselingLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see info.diit.portal.service.base.CounselingLocalServiceBaseImpl
 * @see info.diit.portal.service.CounselingLocalServiceUtil
 */
public class CounselingLocalServiceImpl extends CounselingLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.service.CounselingLocalServiceUtil} to access the counseling local service.
	 */
	public List<Counseling> findCounselingByUser(long userId) throws SystemException
	{
		return CounselingUtil.findByUser(userId);
	}
	
	public Set<CounselingCourseInterest> getCounselingCourseInterests(long counselingId) throws SystemException
	{
		Set<CounselingCourseInterest> courseInterests = new HashSet<CounselingCourseInterest>(CounselingUtil.getCounselingCourseInterests(counselingId));
		return courseInterests;
	}
	
	
	public Counseling addCounseling(Counseling counseling, Set<CounselingCourseInterest> courseInterests) throws SystemException
	{
		
		counseling = CounselingLocalServiceUtil.addCounseling(counseling);
		
		for(CounselingCourseInterest courseInterest:courseInterests)
		{
			courseInterest.setCounselingId(counseling.getCounselingId());
			CounselingCourseInterestLocalServiceUtil.addCounselingCourseInterest(courseInterest);
			
		}
		
		return counseling;
	}
	
	public Counseling updateCounseling(Counseling counseling, Set<CounselingCourseInterest> courseInterests) throws SystemException
	{
		counseling = CounselingLocalServiceUtil.updateCounseling(counseling);
		
		
		Set<CounselingCourseInterest> oldCourseInterests = this.getCounselingCourseInterests(counseling.getCounselingId());
		
		//match whether the old and new course interests are same or different
		boolean matched = true;
		
		if(courseInterests.size() == oldCourseInterests.size())
		{
			Iterator<CounselingCourseInterest> iterator = courseInterests.iterator();
			Iterator<CounselingCourseInterest> oldIterator = oldCourseInterests.iterator();
			while(iterator.hasNext())
			{
				CounselingCourseInterest courseInterest = iterator.next();
				CounselingCourseInterest oldCourseInterest = oldIterator.next();
				
				if(courseInterest.getCourseId() != oldCourseInterest.getCourseId())
				{
					matched = false;
					break;
				}
			}
		}
		else
		{
			matched = false;
		}
		
		//remove the old course interests if matched=false. matched=true means, the old and new course interests are same 
		//and no need to update
		
		if(matched == false)
		{
		
			for(CounselingCourseInterest oldCourseInterest:oldCourseInterests)
			{
				CounselingCourseInterestLocalServiceUtil.deleteCounselingCourseInterest(oldCourseInterest);
			}
			
			//add the new course interests
			
			for(CounselingCourseInterest courseInterest:courseInterests)
			{
				courseInterest.setCounselingId(counseling.getCounselingId());
				CounselingCourseInterestLocalServiceUtil.addCounselingCourseInterest(courseInterest);
			}
		}
		
		return counseling;

	}
	
	public Counseling deleteCounselingWithChild(Counseling counseling) throws SystemException
	{
		Set<CounselingCourseInterest> courseInterests = this.getCounselingCourseInterests(counseling.getCounselingId());
		
		for(CounselingCourseInterest oldCourseInterest:courseInterests)
		{
			CounselingCourseInterestLocalServiceUtil.deleteCounselingCourseInterest(oldCourseInterest);
		}
		
		counseling = CounselingLocalServiceUtil.deleteCounseling(counseling);
		
		return counseling;

	}
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchCourseSubjectException;
import info.diit.portal.model.CourseSubject;
import info.diit.portal.model.impl.CourseSubjectImpl;
import info.diit.portal.model.impl.CourseSubjectModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the course subject service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see CourseSubjectPersistence
 * @see CourseSubjectUtil
 * @generated
 */
public class CourseSubjectPersistenceImpl extends BasePersistenceImpl<CourseSubject>
	implements CourseSubjectPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CourseSubjectUtil} to access the course subject persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CourseSubjectImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COURSEID = new FinderPath(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectModelImpl.FINDER_CACHE_ENABLED,
			CourseSubjectImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBycourseId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID =
		new FinderPath(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectModelImpl.FINDER_CACHE_ENABLED,
			CourseSubjectImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBycourseId", new String[] { Long.class.getName() },
			CourseSubjectModelImpl.COURSEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COURSEID = new FinderPath(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBycourseId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FETCH_BY_SUBJECTID = new FinderPath(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectModelImpl.FINDER_CACHE_ENABLED,
			CourseSubjectImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchBysubjectId", new String[] { Long.class.getName() },
			CourseSubjectModelImpl.SUBJECTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SUBJECTID = new FinderPath(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBysubjectId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectModelImpl.FINDER_CACHE_ENABLED,
			CourseSubjectImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByorganization",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectModelImpl.FINDER_CACHE_ENABLED,
			CourseSubjectImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByorganization", new String[] { Long.class.getName() },
			CourseSubjectModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATION = new FinderPath(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByorganization",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY = new FinderPath(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectModelImpl.FINDER_CACHE_ENABLED,
			CourseSubjectImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBycompany",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY =
		new FinderPath(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectModelImpl.FINDER_CACHE_ENABLED,
			CourseSubjectImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBycompany", new String[] { Long.class.getName() },
			CourseSubjectModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANY = new FinderPath(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBycompany",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectModelImpl.FINDER_CACHE_ENABLED,
			CourseSubjectImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectModelImpl.FINDER_CACHE_ENABLED,
			CourseSubjectImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the course subject in the entity cache if it is enabled.
	 *
	 * @param courseSubject the course subject
	 */
	public void cacheResult(CourseSubject courseSubject) {
		EntityCacheUtil.putResult(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectImpl.class, courseSubject.getPrimaryKey(),
			courseSubject);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTID,
			new Object[] { Long.valueOf(courseSubject.getSubjectId()) },
			courseSubject);

		courseSubject.resetOriginalValues();
	}

	/**
	 * Caches the course subjects in the entity cache if it is enabled.
	 *
	 * @param courseSubjects the course subjects
	 */
	public void cacheResult(List<CourseSubject> courseSubjects) {
		for (CourseSubject courseSubject : courseSubjects) {
			if (EntityCacheUtil.getResult(
						CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
						CourseSubjectImpl.class, courseSubject.getPrimaryKey()) == null) {
				cacheResult(courseSubject);
			}
			else {
				courseSubject.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all course subjects.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CourseSubjectImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CourseSubjectImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the course subject.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CourseSubject courseSubject) {
		EntityCacheUtil.removeResult(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectImpl.class, courseSubject.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(courseSubject);
	}

	@Override
	public void clearCache(List<CourseSubject> courseSubjects) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CourseSubject courseSubject : courseSubjects) {
			EntityCacheUtil.removeResult(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
				CourseSubjectImpl.class, courseSubject.getPrimaryKey());

			clearUniqueFindersCache(courseSubject);
		}
	}

	protected void clearUniqueFindersCache(CourseSubject courseSubject) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SUBJECTID,
			new Object[] { Long.valueOf(courseSubject.getSubjectId()) });
	}

	/**
	 * Creates a new course subject with the primary key. Does not add the course subject to the database.
	 *
	 * @param courseSubjectId the primary key for the new course subject
	 * @return the new course subject
	 */
	public CourseSubject create(long courseSubjectId) {
		CourseSubject courseSubject = new CourseSubjectImpl();

		courseSubject.setNew(true);
		courseSubject.setPrimaryKey(courseSubjectId);

		return courseSubject;
	}

	/**
	 * Removes the course subject with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param courseSubjectId the primary key of the course subject
	 * @return the course subject that was removed
	 * @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject remove(long courseSubjectId)
		throws NoSuchCourseSubjectException, SystemException {
		return remove(Long.valueOf(courseSubjectId));
	}

	/**
	 * Removes the course subject with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the course subject
	 * @return the course subject that was removed
	 * @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CourseSubject remove(Serializable primaryKey)
		throws NoSuchCourseSubjectException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CourseSubject courseSubject = (CourseSubject)session.get(CourseSubjectImpl.class,
					primaryKey);

			if (courseSubject == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCourseSubjectException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(courseSubject);
		}
		catch (NoSuchCourseSubjectException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CourseSubject removeImpl(CourseSubject courseSubject)
		throws SystemException {
		courseSubject = toUnwrappedModel(courseSubject);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, courseSubject);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(courseSubject);

		return courseSubject;
	}

	@Override
	public CourseSubject updateImpl(
		info.diit.portal.model.CourseSubject courseSubject, boolean merge)
		throws SystemException {
		courseSubject = toUnwrappedModel(courseSubject);

		boolean isNew = courseSubject.isNew();

		CourseSubjectModelImpl courseSubjectModelImpl = (CourseSubjectModelImpl)courseSubject;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, courseSubject, merge);

			courseSubject.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CourseSubjectModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((courseSubjectModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(courseSubjectModelImpl.getOriginalCourseId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COURSEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID,
					args);

				args = new Object[] {
						Long.valueOf(courseSubjectModelImpl.getCourseId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COURSEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID,
					args);
			}

			if ((courseSubjectModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(courseSubjectModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(courseSubjectModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);
			}

			if ((courseSubjectModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(courseSubjectModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);

				args = new Object[] {
						Long.valueOf(courseSubjectModelImpl.getCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);
			}
		}

		EntityCacheUtil.putResult(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
			CourseSubjectImpl.class, courseSubject.getPrimaryKey(),
			courseSubject);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTID,
				new Object[] { Long.valueOf(courseSubject.getSubjectId()) },
				courseSubject);
		}
		else {
			if ((courseSubjectModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_SUBJECTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(courseSubjectModelImpl.getOriginalSubjectId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBJECTID,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SUBJECTID,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTID,
					new Object[] { Long.valueOf(courseSubject.getSubjectId()) },
					courseSubject);
			}
		}

		return courseSubject;
	}

	protected CourseSubject toUnwrappedModel(CourseSubject courseSubject) {
		if (courseSubject instanceof CourseSubjectImpl) {
			return courseSubject;
		}

		CourseSubjectImpl courseSubjectImpl = new CourseSubjectImpl();

		courseSubjectImpl.setNew(courseSubject.isNew());
		courseSubjectImpl.setPrimaryKey(courseSubject.getPrimaryKey());

		courseSubjectImpl.setCourseSubjectId(courseSubject.getCourseSubjectId());
		courseSubjectImpl.setCompanyId(courseSubject.getCompanyId());
		courseSubjectImpl.setOrganizationId(courseSubject.getOrganizationId());
		courseSubjectImpl.setUserId(courseSubject.getUserId());
		courseSubjectImpl.setUserName(courseSubject.getUserName());
		courseSubjectImpl.setCreateDate(courseSubject.getCreateDate());
		courseSubjectImpl.setModifiedDate(courseSubject.getModifiedDate());
		courseSubjectImpl.setCourseId(courseSubject.getCourseId());
		courseSubjectImpl.setSubjectId(courseSubject.getSubjectId());

		return courseSubjectImpl;
	}

	/**
	 * Returns the course subject with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the course subject
	 * @return the course subject
	 * @throws com.liferay.portal.NoSuchModelException if a course subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CourseSubject findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the course subject with the primary key or throws a {@link info.diit.portal.NoSuchCourseSubjectException} if it could not be found.
	 *
	 * @param courseSubjectId the primary key of the course subject
	 * @return the course subject
	 * @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject findByPrimaryKey(long courseSubjectId)
		throws NoSuchCourseSubjectException, SystemException {
		CourseSubject courseSubject = fetchByPrimaryKey(courseSubjectId);

		if (courseSubject == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + courseSubjectId);
			}

			throw new NoSuchCourseSubjectException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				courseSubjectId);
		}

		return courseSubject;
	}

	/**
	 * Returns the course subject with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the course subject
	 * @return the course subject, or <code>null</code> if a course subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CourseSubject fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the course subject with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param courseSubjectId the primary key of the course subject
	 * @return the course subject, or <code>null</code> if a course subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject fetchByPrimaryKey(long courseSubjectId)
		throws SystemException {
		CourseSubject courseSubject = (CourseSubject)EntityCacheUtil.getResult(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
				CourseSubjectImpl.class, courseSubjectId);

		if (courseSubject == _nullCourseSubject) {
			return null;
		}

		if (courseSubject == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				courseSubject = (CourseSubject)session.get(CourseSubjectImpl.class,
						Long.valueOf(courseSubjectId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (courseSubject != null) {
					cacheResult(courseSubject);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(CourseSubjectModelImpl.ENTITY_CACHE_ENABLED,
						CourseSubjectImpl.class, courseSubjectId,
						_nullCourseSubject);
				}

				closeSession(session);
			}
		}

		return courseSubject;
	}

	/**
	 * Returns all the course subjects where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @return the matching course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSubject> findBycourseId(long courseId)
		throws SystemException {
		return findBycourseId(courseId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the course subjects where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course ID
	 * @param start the lower bound of the range of course subjects
	 * @param end the upper bound of the range of course subjects (not inclusive)
	 * @return the range of matching course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSubject> findBycourseId(long courseId, int start, int end)
		throws SystemException {
		return findBycourseId(courseId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the course subjects where courseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param courseId the course ID
	 * @param start the lower bound of the range of course subjects
	 * @param end the upper bound of the range of course subjects (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSubject> findBycourseId(long courseId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COURSEID;
			finderArgs = new Object[] { courseId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COURSEID;
			finderArgs = new Object[] { courseId, start, end, orderByComparator };
		}

		List<CourseSubject> list = (List<CourseSubject>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CourseSubject courseSubject : list) {
				if ((courseId != courseSubject.getCourseId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COURSESUBJECT_WHERE);

			query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CourseSubjectModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				list = (List<CourseSubject>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first course subject in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course subject
	 * @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject findBycourseId_First(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSubjectException, SystemException {
		CourseSubject courseSubject = fetchBycourseId_First(courseId,
				orderByComparator);

		if (courseSubject != null) {
			return courseSubject;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("courseId=");
		msg.append(courseId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseSubjectException(msg.toString());
	}

	/**
	 * Returns the first course subject in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course subject, or <code>null</code> if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject fetchBycourseId_First(long courseId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CourseSubject> list = findBycourseId(courseId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last course subject in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course subject
	 * @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject findBycourseId_Last(long courseId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSubjectException, SystemException {
		CourseSubject courseSubject = fetchBycourseId_Last(courseId,
				orderByComparator);

		if (courseSubject != null) {
			return courseSubject;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("courseId=");
		msg.append(courseId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseSubjectException(msg.toString());
	}

	/**
	 * Returns the last course subject in the ordered set where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course subject, or <code>null</code> if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject fetchBycourseId_Last(long courseId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBycourseId(courseId);

		List<CourseSubject> list = findBycourseId(courseId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the course subjects before and after the current course subject in the ordered set where courseId = &#63;.
	 *
	 * @param courseSubjectId the primary key of the current course subject
	 * @param courseId the course ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course subject
	 * @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject[] findBycourseId_PrevAndNext(long courseSubjectId,
		long courseId, OrderByComparator orderByComparator)
		throws NoSuchCourseSubjectException, SystemException {
		CourseSubject courseSubject = findByPrimaryKey(courseSubjectId);

		Session session = null;

		try {
			session = openSession();

			CourseSubject[] array = new CourseSubjectImpl[3];

			array[0] = getBycourseId_PrevAndNext(session, courseSubject,
					courseId, orderByComparator, true);

			array[1] = courseSubject;

			array[2] = getBycourseId_PrevAndNext(session, courseSubject,
					courseId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourseSubject getBycourseId_PrevAndNext(Session session,
		CourseSubject courseSubject, long courseId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSESUBJECT_WHERE);

		query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CourseSubjectModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(courseId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(courseSubject);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CourseSubject> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns the course subject where subjectId = &#63; or throws a {@link info.diit.portal.NoSuchCourseSubjectException} if it could not be found.
	 *
	 * @param subjectId the subject ID
	 * @return the matching course subject
	 * @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject findBysubjectId(long subjectId)
		throws NoSuchCourseSubjectException, SystemException {
		CourseSubject courseSubject = fetchBysubjectId(subjectId);

		if (courseSubject == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("subjectId=");
			msg.append(subjectId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCourseSubjectException(msg.toString());
		}

		return courseSubject;
	}

	/**
	 * Returns the course subject where subjectId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param subjectId the subject ID
	 * @return the matching course subject, or <code>null</code> if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject fetchBysubjectId(long subjectId)
		throws SystemException {
		return fetchBysubjectId(subjectId, true);
	}

	/**
	 * Returns the course subject where subjectId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param subjectId the subject ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching course subject, or <code>null</code> if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject fetchBysubjectId(long subjectId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { subjectId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_SUBJECTID,
					finderArgs, this);
		}

		if (result instanceof CourseSubject) {
			CourseSubject courseSubject = (CourseSubject)result;

			if ((subjectId != courseSubject.getSubjectId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_COURSESUBJECT_WHERE);

			query.append(_FINDER_COLUMN_SUBJECTID_SUBJECTID_2);

			query.append(CourseSubjectModelImpl.ORDER_BY_JPQL);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				List<CourseSubject> list = q.list();

				result = list;

				CourseSubject courseSubject = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTID,
						finderArgs, list);
				}
				else {
					courseSubject = list.get(0);

					cacheResult(courseSubject);

					if ((courseSubject.getSubjectId() != subjectId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTID,
							finderArgs, courseSubject);
					}
				}

				return courseSubject;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SUBJECTID,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (CourseSubject)result;
			}
		}
	}

	/**
	 * Returns all the course subjects where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSubject> findByorganization(long organizationId)
		throws SystemException {
		return findByorganization(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the course subjects where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of course subjects
	 * @param end the upper bound of the range of course subjects (not inclusive)
	 * @return the range of matching course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSubject> findByorganization(long organizationId,
		int start, int end) throws SystemException {
		return findByorganization(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the course subjects where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of course subjects
	 * @param end the upper bound of the range of course subjects (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSubject> findByorganization(long organizationId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<CourseSubject> list = (List<CourseSubject>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CourseSubject courseSubject : list) {
				if ((organizationId != courseSubject.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COURSESUBJECT_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CourseSubjectModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				list = (List<CourseSubject>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first course subject in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course subject
	 * @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject findByorganization_First(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSubjectException, SystemException {
		CourseSubject courseSubject = fetchByorganization_First(organizationId,
				orderByComparator);

		if (courseSubject != null) {
			return courseSubject;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseSubjectException(msg.toString());
	}

	/**
	 * Returns the first course subject in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course subject, or <code>null</code> if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject fetchByorganization_First(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CourseSubject> list = findByorganization(organizationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last course subject in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course subject
	 * @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject findByorganization_Last(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSubjectException, SystemException {
		CourseSubject courseSubject = fetchByorganization_Last(organizationId,
				orderByComparator);

		if (courseSubject != null) {
			return courseSubject;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseSubjectException(msg.toString());
	}

	/**
	 * Returns the last course subject in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course subject, or <code>null</code> if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject fetchByorganization_Last(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByorganization(organizationId);

		List<CourseSubject> list = findByorganization(organizationId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the course subjects before and after the current course subject in the ordered set where organizationId = &#63;.
	 *
	 * @param courseSubjectId the primary key of the current course subject
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course subject
	 * @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject[] findByorganization_PrevAndNext(
		long courseSubjectId, long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSubjectException, SystemException {
		CourseSubject courseSubject = findByPrimaryKey(courseSubjectId);

		Session session = null;

		try {
			session = openSession();

			CourseSubject[] array = new CourseSubjectImpl[3];

			array[0] = getByorganization_PrevAndNext(session, courseSubject,
					organizationId, orderByComparator, true);

			array[1] = courseSubject;

			array[2] = getByorganization_PrevAndNext(session, courseSubject,
					organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourseSubject getByorganization_PrevAndNext(Session session,
		CourseSubject courseSubject, long organizationId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSESUBJECT_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CourseSubjectModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(courseSubject);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CourseSubject> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the course subjects where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSubject> findBycompany(long companyId)
		throws SystemException {
		return findBycompany(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the course subjects where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of course subjects
	 * @param end the upper bound of the range of course subjects (not inclusive)
	 * @return the range of matching course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSubject> findBycompany(long companyId, int start, int end)
		throws SystemException {
		return findBycompany(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the course subjects where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of course subjects
	 * @param end the upper bound of the range of course subjects (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSubject> findBycompany(long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<CourseSubject> list = (List<CourseSubject>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CourseSubject courseSubject : list) {
				if ((companyId != courseSubject.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COURSESUBJECT_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(CourseSubjectModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<CourseSubject>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first course subject in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course subject
	 * @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject findBycompany_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSubjectException, SystemException {
		CourseSubject courseSubject = fetchBycompany_First(companyId,
				orderByComparator);

		if (courseSubject != null) {
			return courseSubject;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseSubjectException(msg.toString());
	}

	/**
	 * Returns the first course subject in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course subject, or <code>null</code> if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject fetchBycompany_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CourseSubject> list = findBycompany(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last course subject in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course subject
	 * @throws info.diit.portal.NoSuchCourseSubjectException if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject findBycompany_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCourseSubjectException, SystemException {
		CourseSubject courseSubject = fetchBycompany_Last(companyId,
				orderByComparator);

		if (courseSubject != null) {
			return courseSubject;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCourseSubjectException(msg.toString());
	}

	/**
	 * Returns the last course subject in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course subject, or <code>null</code> if a matching course subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject fetchBycompany_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBycompany(companyId);

		List<CourseSubject> list = findBycompany(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the course subjects before and after the current course subject in the ordered set where companyId = &#63;.
	 *
	 * @param courseSubjectId the primary key of the current course subject
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course subject
	 * @throws info.diit.portal.NoSuchCourseSubjectException if a course subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject[] findBycompany_PrevAndNext(long courseSubjectId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchCourseSubjectException, SystemException {
		CourseSubject courseSubject = findByPrimaryKey(courseSubjectId);

		Session session = null;

		try {
			session = openSession();

			CourseSubject[] array = new CourseSubjectImpl[3];

			array[0] = getBycompany_PrevAndNext(session, courseSubject,
					companyId, orderByComparator, true);

			array[1] = courseSubject;

			array[2] = getBycompany_PrevAndNext(session, courseSubject,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourseSubject getBycompany_PrevAndNext(Session session,
		CourseSubject courseSubject, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COURSESUBJECT_WHERE);

		query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(CourseSubjectModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(courseSubject);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CourseSubject> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the course subjects.
	 *
	 * @return the course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSubject> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the course subjects.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of course subjects
	 * @param end the upper bound of the range of course subjects (not inclusive)
	 * @return the range of course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSubject> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the course subjects.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of course subjects
	 * @param end the upper bound of the range of course subjects (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<CourseSubject> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CourseSubject> list = (List<CourseSubject>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_COURSESUBJECT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COURSESUBJECT.concat(CourseSubjectModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<CourseSubject>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<CourseSubject>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the course subjects where courseId = &#63; from the database.
	 *
	 * @param courseId the course ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBycourseId(long courseId) throws SystemException {
		for (CourseSubject courseSubject : findBycourseId(courseId)) {
			remove(courseSubject);
		}
	}

	/**
	 * Removes the course subject where subjectId = &#63; from the database.
	 *
	 * @param subjectId the subject ID
	 * @return the course subject that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public CourseSubject removeBysubjectId(long subjectId)
		throws NoSuchCourseSubjectException, SystemException {
		CourseSubject courseSubject = findBysubjectId(subjectId);

		return remove(courseSubject);
	}

	/**
	 * Removes all the course subjects where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByorganization(long organizationId)
		throws SystemException {
		for (CourseSubject courseSubject : findByorganization(organizationId)) {
			remove(courseSubject);
		}
	}

	/**
	 * Removes all the course subjects where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBycompany(long companyId) throws SystemException {
		for (CourseSubject courseSubject : findBycompany(companyId)) {
			remove(courseSubject);
		}
	}

	/**
	 * Removes all the course subjects from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (CourseSubject courseSubject : findAll()) {
			remove(courseSubject);
		}
	}

	/**
	 * Returns the number of course subjects where courseId = &#63;.
	 *
	 * @param courseId the course ID
	 * @return the number of matching course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public int countBycourseId(long courseId) throws SystemException {
		Object[] finderArgs = new Object[] { courseId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COURSEID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSESUBJECT_WHERE);

			query.append(_FINDER_COLUMN_COURSEID_COURSEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(courseId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COURSEID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of course subjects where subjectId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @return the number of matching course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public int countBysubjectId(long subjectId) throws SystemException {
		Object[] finderArgs = new Object[] { subjectId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_SUBJECTID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSESUBJECT_WHERE);

			query.append(_FINDER_COLUMN_SUBJECTID_SUBJECTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_SUBJECTID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of course subjects where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public int countByorganization(long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSESUBJECT_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of course subjects where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public int countBycompany(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COURSESUBJECT_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of course subjects.
	 *
	 * @return the number of course subjects
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COURSESUBJECT);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the course subject persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.CourseSubject")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CourseSubject>> listenersList = new ArrayList<ModelListener<CourseSubject>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CourseSubject>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CourseSubjectImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_COURSESUBJECT = "SELECT courseSubject FROM CourseSubject courseSubject";
	private static final String _SQL_SELECT_COURSESUBJECT_WHERE = "SELECT courseSubject FROM CourseSubject courseSubject WHERE ";
	private static final String _SQL_COUNT_COURSESUBJECT = "SELECT COUNT(courseSubject) FROM CourseSubject courseSubject";
	private static final String _SQL_COUNT_COURSESUBJECT_WHERE = "SELECT COUNT(courseSubject) FROM CourseSubject courseSubject WHERE ";
	private static final String _FINDER_COLUMN_COURSEID_COURSEID_2 = "courseSubject.courseId = ?";
	private static final String _FINDER_COLUMN_SUBJECTID_SUBJECTID_2 = "courseSubject.subjectId = ?";
	private static final String _FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2 = "courseSubject.organizationId = ?";
	private static final String _FINDER_COLUMN_COMPANY_COMPANYID_2 = "courseSubject.companyId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "courseSubject.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CourseSubject exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CourseSubject exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CourseSubjectPersistenceImpl.class);
	private static CourseSubject _nullCourseSubject = new CourseSubjectImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CourseSubject> toCacheModel() {
				return _nullCourseSubjectCacheModel;
			}
		};

	private static CacheModel<CourseSubject> _nullCourseSubjectCacheModel = new CacheModel<CourseSubject>() {
			public CourseSubject toEntityModel() {
				return _nullCourseSubject;
			}
		};
}
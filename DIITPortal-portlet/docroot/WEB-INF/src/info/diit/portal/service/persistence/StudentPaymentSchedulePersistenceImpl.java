/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchStudentPaymentScheduleException;
import info.diit.portal.model.StudentPaymentSchedule;
import info.diit.portal.model.impl.StudentPaymentScheduleImpl;
import info.diit.portal.model.impl.StudentPaymentScheduleModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the student payment schedule service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see StudentPaymentSchedulePersistence
 * @see StudentPaymentScheduleUtil
 * @generated
 */
public class StudentPaymentSchedulePersistenceImpl extends BasePersistenceImpl<StudentPaymentSchedule>
	implements StudentPaymentSchedulePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link StudentPaymentScheduleUtil} to access the student payment schedule persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = StudentPaymentScheduleImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTBATCH =
		new FinderPath(StudentPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			StudentPaymentScheduleModelImpl.FINDER_CACHE_ENABLED,
			StudentPaymentScheduleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByStudentBatch",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH =
		new FinderPath(StudentPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			StudentPaymentScheduleModelImpl.FINDER_CACHE_ENABLED,
			StudentPaymentScheduleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStudentBatch",
			new String[] { Long.class.getName(), Long.class.getName() },
			StudentPaymentScheduleModelImpl.STUDENTID_COLUMN_BITMASK |
			StudentPaymentScheduleModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STUDENTBATCH = new FinderPath(StudentPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			StudentPaymentScheduleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByStudentBatch",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(StudentPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			StudentPaymentScheduleModelImpl.FINDER_CACHE_ENABLED,
			StudentPaymentScheduleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(StudentPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			StudentPaymentScheduleModelImpl.FINDER_CACHE_ENABLED,
			StudentPaymentScheduleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(StudentPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			StudentPaymentScheduleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the student payment schedule in the entity cache if it is enabled.
	 *
	 * @param studentPaymentSchedule the student payment schedule
	 */
	public void cacheResult(StudentPaymentSchedule studentPaymentSchedule) {
		EntityCacheUtil.putResult(StudentPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			StudentPaymentScheduleImpl.class,
			studentPaymentSchedule.getPrimaryKey(), studentPaymentSchedule);

		studentPaymentSchedule.resetOriginalValues();
	}

	/**
	 * Caches the student payment schedules in the entity cache if it is enabled.
	 *
	 * @param studentPaymentSchedules the student payment schedules
	 */
	public void cacheResult(
		List<StudentPaymentSchedule> studentPaymentSchedules) {
		for (StudentPaymentSchedule studentPaymentSchedule : studentPaymentSchedules) {
			if (EntityCacheUtil.getResult(
						StudentPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
						StudentPaymentScheduleImpl.class,
						studentPaymentSchedule.getPrimaryKey()) == null) {
				cacheResult(studentPaymentSchedule);
			}
			else {
				studentPaymentSchedule.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all student payment schedules.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(StudentPaymentScheduleImpl.class.getName());
		}

		EntityCacheUtil.clearCache(StudentPaymentScheduleImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the student payment schedule.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(StudentPaymentSchedule studentPaymentSchedule) {
		EntityCacheUtil.removeResult(StudentPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			StudentPaymentScheduleImpl.class,
			studentPaymentSchedule.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<StudentPaymentSchedule> studentPaymentSchedules) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (StudentPaymentSchedule studentPaymentSchedule : studentPaymentSchedules) {
			EntityCacheUtil.removeResult(StudentPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
				StudentPaymentScheduleImpl.class,
				studentPaymentSchedule.getPrimaryKey());
		}
	}

	/**
	 * Creates a new student payment schedule with the primary key. Does not add the student payment schedule to the database.
	 *
	 * @param studentPaymentScheduleId the primary key for the new student payment schedule
	 * @return the new student payment schedule
	 */
	public StudentPaymentSchedule create(long studentPaymentScheduleId) {
		StudentPaymentSchedule studentPaymentSchedule = new StudentPaymentScheduleImpl();

		studentPaymentSchedule.setNew(true);
		studentPaymentSchedule.setPrimaryKey(studentPaymentScheduleId);

		return studentPaymentSchedule;
	}

	/**
	 * Removes the student payment schedule with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param studentPaymentScheduleId the primary key of the student payment schedule
	 * @return the student payment schedule that was removed
	 * @throws info.diit.portal.NoSuchStudentPaymentScheduleException if a student payment schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentPaymentSchedule remove(long studentPaymentScheduleId)
		throws NoSuchStudentPaymentScheduleException, SystemException {
		return remove(Long.valueOf(studentPaymentScheduleId));
	}

	/**
	 * Removes the student payment schedule with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the student payment schedule
	 * @return the student payment schedule that was removed
	 * @throws info.diit.portal.NoSuchStudentPaymentScheduleException if a student payment schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentPaymentSchedule remove(Serializable primaryKey)
		throws NoSuchStudentPaymentScheduleException, SystemException {
		Session session = null;

		try {
			session = openSession();

			StudentPaymentSchedule studentPaymentSchedule = (StudentPaymentSchedule)session.get(StudentPaymentScheduleImpl.class,
					primaryKey);

			if (studentPaymentSchedule == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchStudentPaymentScheduleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(studentPaymentSchedule);
		}
		catch (NoSuchStudentPaymentScheduleException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected StudentPaymentSchedule removeImpl(
		StudentPaymentSchedule studentPaymentSchedule)
		throws SystemException {
		studentPaymentSchedule = toUnwrappedModel(studentPaymentSchedule);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, studentPaymentSchedule);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(studentPaymentSchedule);

		return studentPaymentSchedule;
	}

	@Override
	public StudentPaymentSchedule updateImpl(
		info.diit.portal.model.StudentPaymentSchedule studentPaymentSchedule,
		boolean merge) throws SystemException {
		studentPaymentSchedule = toUnwrappedModel(studentPaymentSchedule);

		boolean isNew = studentPaymentSchedule.isNew();

		StudentPaymentScheduleModelImpl studentPaymentScheduleModelImpl = (StudentPaymentScheduleModelImpl)studentPaymentSchedule;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, studentPaymentSchedule, merge);

			studentPaymentSchedule.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !StudentPaymentScheduleModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((studentPaymentScheduleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(studentPaymentScheduleModelImpl.getOriginalStudentId()),
						Long.valueOf(studentPaymentScheduleModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH,
					args);

				args = new Object[] {
						Long.valueOf(studentPaymentScheduleModelImpl.getStudentId()),
						Long.valueOf(studentPaymentScheduleModelImpl.getBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH,
					args);
			}
		}

		EntityCacheUtil.putResult(StudentPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			StudentPaymentScheduleImpl.class,
			studentPaymentSchedule.getPrimaryKey(), studentPaymentSchedule);

		return studentPaymentSchedule;
	}

	protected StudentPaymentSchedule toUnwrappedModel(
		StudentPaymentSchedule studentPaymentSchedule) {
		if (studentPaymentSchedule instanceof StudentPaymentScheduleImpl) {
			return studentPaymentSchedule;
		}

		StudentPaymentScheduleImpl studentPaymentScheduleImpl = new StudentPaymentScheduleImpl();

		studentPaymentScheduleImpl.setNew(studentPaymentSchedule.isNew());
		studentPaymentScheduleImpl.setPrimaryKey(studentPaymentSchedule.getPrimaryKey());

		studentPaymentScheduleImpl.setStudentPaymentScheduleId(studentPaymentSchedule.getStudentPaymentScheduleId());
		studentPaymentScheduleImpl.setCompanyId(studentPaymentSchedule.getCompanyId());
		studentPaymentScheduleImpl.setUserId(studentPaymentSchedule.getUserId());
		studentPaymentScheduleImpl.setCreateDate(studentPaymentSchedule.getCreateDate());
		studentPaymentScheduleImpl.setModifiedDate(studentPaymentSchedule.getModifiedDate());
		studentPaymentScheduleImpl.setStudentId(studentPaymentSchedule.getStudentId());
		studentPaymentScheduleImpl.setBatchId(studentPaymentSchedule.getBatchId());
		studentPaymentScheduleImpl.setScheduleDate(studentPaymentSchedule.getScheduleDate());
		studentPaymentScheduleImpl.setFeeTypeId(studentPaymentSchedule.getFeeTypeId());
		studentPaymentScheduleImpl.setAmount(studentPaymentSchedule.getAmount());

		return studentPaymentScheduleImpl;
	}

	/**
	 * Returns the student payment schedule with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the student payment schedule
	 * @return the student payment schedule
	 * @throws com.liferay.portal.NoSuchModelException if a student payment schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentPaymentSchedule findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the student payment schedule with the primary key or throws a {@link info.diit.portal.NoSuchStudentPaymentScheduleException} if it could not be found.
	 *
	 * @param studentPaymentScheduleId the primary key of the student payment schedule
	 * @return the student payment schedule
	 * @throws info.diit.portal.NoSuchStudentPaymentScheduleException if a student payment schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentPaymentSchedule findByPrimaryKey(
		long studentPaymentScheduleId)
		throws NoSuchStudentPaymentScheduleException, SystemException {
		StudentPaymentSchedule studentPaymentSchedule = fetchByPrimaryKey(studentPaymentScheduleId);

		if (studentPaymentSchedule == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					studentPaymentScheduleId);
			}

			throw new NoSuchStudentPaymentScheduleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				studentPaymentScheduleId);
		}

		return studentPaymentSchedule;
	}

	/**
	 * Returns the student payment schedule with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the student payment schedule
	 * @return the student payment schedule, or <code>null</code> if a student payment schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentPaymentSchedule fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the student payment schedule with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param studentPaymentScheduleId the primary key of the student payment schedule
	 * @return the student payment schedule, or <code>null</code> if a student payment schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentPaymentSchedule fetchByPrimaryKey(
		long studentPaymentScheduleId) throws SystemException {
		StudentPaymentSchedule studentPaymentSchedule = (StudentPaymentSchedule)EntityCacheUtil.getResult(StudentPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
				StudentPaymentScheduleImpl.class, studentPaymentScheduleId);

		if (studentPaymentSchedule == _nullStudentPaymentSchedule) {
			return null;
		}

		if (studentPaymentSchedule == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				studentPaymentSchedule = (StudentPaymentSchedule)session.get(StudentPaymentScheduleImpl.class,
						Long.valueOf(studentPaymentScheduleId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (studentPaymentSchedule != null) {
					cacheResult(studentPaymentSchedule);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(StudentPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
						StudentPaymentScheduleImpl.class,
						studentPaymentScheduleId, _nullStudentPaymentSchedule);
				}

				closeSession(session);
			}
		}

		return studentPaymentSchedule;
	}

	/**
	 * Returns all the student payment schedules where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @return the matching student payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentPaymentSchedule> findByStudentBatch(long studentId,
		long batchId) throws SystemException {
		return findByStudentBatch(studentId, batchId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the student payment schedules where studentId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of student payment schedules
	 * @param end the upper bound of the range of student payment schedules (not inclusive)
	 * @return the range of matching student payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentPaymentSchedule> findByStudentBatch(long studentId,
		long batchId, int start, int end) throws SystemException {
		return findByStudentBatch(studentId, batchId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the student payment schedules where studentId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of student payment schedules
	 * @param end the upper bound of the range of student payment schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching student payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentPaymentSchedule> findByStudentBatch(long studentId,
		long batchId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH;
			finderArgs = new Object[] { studentId, batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTBATCH;
			finderArgs = new Object[] {
					studentId, batchId,
					
					start, end, orderByComparator
				};
		}

		List<StudentPaymentSchedule> list = (List<StudentPaymentSchedule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (StudentPaymentSchedule studentPaymentSchedule : list) {
				if ((studentId != studentPaymentSchedule.getStudentId()) ||
						(batchId != studentPaymentSchedule.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_STUDENTPAYMENTSCHEDULE_WHERE);

			query.append(_FINDER_COLUMN_STUDENTBATCH_STUDENTID_2);

			query.append(_FINDER_COLUMN_STUDENTBATCH_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				qPos.add(batchId);

				list = (List<StudentPaymentSchedule>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student payment schedule
	 * @throws info.diit.portal.NoSuchStudentPaymentScheduleException if a matching student payment schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentPaymentSchedule findByStudentBatch_First(long studentId,
		long batchId, OrderByComparator orderByComparator)
		throws NoSuchStudentPaymentScheduleException, SystemException {
		StudentPaymentSchedule studentPaymentSchedule = fetchByStudentBatch_First(studentId,
				batchId, orderByComparator);

		if (studentPaymentSchedule != null) {
			return studentPaymentSchedule;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchStudentPaymentScheduleException(msg.toString());
	}

	/**
	 * Returns the first student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student payment schedule, or <code>null</code> if a matching student payment schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentPaymentSchedule fetchByStudentBatch_First(long studentId,
		long batchId, OrderByComparator orderByComparator)
		throws SystemException {
		List<StudentPaymentSchedule> list = findByStudentBatch(studentId,
				batchId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student payment schedule
	 * @throws info.diit.portal.NoSuchStudentPaymentScheduleException if a matching student payment schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentPaymentSchedule findByStudentBatch_Last(long studentId,
		long batchId, OrderByComparator orderByComparator)
		throws NoSuchStudentPaymentScheduleException, SystemException {
		StudentPaymentSchedule studentPaymentSchedule = fetchByStudentBatch_Last(studentId,
				batchId, orderByComparator);

		if (studentPaymentSchedule != null) {
			return studentPaymentSchedule;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchStudentPaymentScheduleException(msg.toString());
	}

	/**
	 * Returns the last student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student payment schedule, or <code>null</code> if a matching student payment schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentPaymentSchedule fetchByStudentBatch_Last(long studentId,
		long batchId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByStudentBatch(studentId, batchId);

		List<StudentPaymentSchedule> list = findByStudentBatch(studentId,
				batchId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the student payment schedules before and after the current student payment schedule in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentPaymentScheduleId the primary key of the current student payment schedule
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next student payment schedule
	 * @throws info.diit.portal.NoSuchStudentPaymentScheduleException if a student payment schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentPaymentSchedule[] findByStudentBatch_PrevAndNext(
		long studentPaymentScheduleId, long studentId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchStudentPaymentScheduleException, SystemException {
		StudentPaymentSchedule studentPaymentSchedule = findByPrimaryKey(studentPaymentScheduleId);

		Session session = null;

		try {
			session = openSession();

			StudentPaymentSchedule[] array = new StudentPaymentScheduleImpl[3];

			array[0] = getByStudentBatch_PrevAndNext(session,
					studentPaymentSchedule, studentId, batchId,
					orderByComparator, true);

			array[1] = studentPaymentSchedule;

			array[2] = getByStudentBatch_PrevAndNext(session,
					studentPaymentSchedule, studentId, batchId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected StudentPaymentSchedule getByStudentBatch_PrevAndNext(
		Session session, StudentPaymentSchedule studentPaymentSchedule,
		long studentId, long batchId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_STUDENTPAYMENTSCHEDULE_WHERE);

		query.append(_FINDER_COLUMN_STUDENTBATCH_STUDENTID_2);

		query.append(_FINDER_COLUMN_STUDENTBATCH_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(studentId);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(studentPaymentSchedule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<StudentPaymentSchedule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the student payment schedules.
	 *
	 * @return the student payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentPaymentSchedule> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the student payment schedules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of student payment schedules
	 * @param end the upper bound of the range of student payment schedules (not inclusive)
	 * @return the range of student payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentPaymentSchedule> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the student payment schedules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of student payment schedules
	 * @param end the upper bound of the range of student payment schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of student payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentPaymentSchedule> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<StudentPaymentSchedule> list = (List<StudentPaymentSchedule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_STUDENTPAYMENTSCHEDULE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_STUDENTPAYMENTSCHEDULE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<StudentPaymentSchedule>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<StudentPaymentSchedule>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the student payment schedules where studentId = &#63; and batchId = &#63; from the database.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByStudentBatch(long studentId, long batchId)
		throws SystemException {
		for (StudentPaymentSchedule studentPaymentSchedule : findByStudentBatch(
				studentId, batchId)) {
			remove(studentPaymentSchedule);
		}
	}

	/**
	 * Removes all the student payment schedules from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (StudentPaymentSchedule studentPaymentSchedule : findAll()) {
			remove(studentPaymentSchedule);
		}
	}

	/**
	 * Returns the number of student payment schedules where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @return the number of matching student payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public int countByStudentBatch(long studentId, long batchId)
		throws SystemException {
		Object[] finderArgs = new Object[] { studentId, batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_STUDENTPAYMENTSCHEDULE_WHERE);

			query.append(_FINDER_COLUMN_STUDENTBATCH_STUDENTID_2);

			query.append(_FINDER_COLUMN_STUDENTBATCH_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of student payment schedules.
	 *
	 * @return the number of student payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_STUDENTPAYMENTSCHEDULE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the student payment schedule persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.StudentPaymentSchedule")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<StudentPaymentSchedule>> listenersList = new ArrayList<ModelListener<StudentPaymentSchedule>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<StudentPaymentSchedule>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(StudentPaymentScheduleImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_STUDENTPAYMENTSCHEDULE = "SELECT studentPaymentSchedule FROM StudentPaymentSchedule studentPaymentSchedule";
	private static final String _SQL_SELECT_STUDENTPAYMENTSCHEDULE_WHERE = "SELECT studentPaymentSchedule FROM StudentPaymentSchedule studentPaymentSchedule WHERE ";
	private static final String _SQL_COUNT_STUDENTPAYMENTSCHEDULE = "SELECT COUNT(studentPaymentSchedule) FROM StudentPaymentSchedule studentPaymentSchedule";
	private static final String _SQL_COUNT_STUDENTPAYMENTSCHEDULE_WHERE = "SELECT COUNT(studentPaymentSchedule) FROM StudentPaymentSchedule studentPaymentSchedule WHERE ";
	private static final String _FINDER_COLUMN_STUDENTBATCH_STUDENTID_2 = "studentPaymentSchedule.studentId = ? AND ";
	private static final String _FINDER_COLUMN_STUDENTBATCH_BATCHID_2 = "studentPaymentSchedule.batchId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "studentPaymentSchedule.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No StudentPaymentSchedule exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No StudentPaymentSchedule exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(StudentPaymentSchedulePersistenceImpl.class);
	private static StudentPaymentSchedule _nullStudentPaymentSchedule = new StudentPaymentScheduleImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<StudentPaymentSchedule> toCacheModel() {
				return _nullStudentPaymentScheduleCacheModel;
			}
		};

	private static CacheModel<StudentPaymentSchedule> _nullStudentPaymentScheduleCacheModel =
		new CacheModel<StudentPaymentSchedule>() {
			public StudentPaymentSchedule toEntityModel() {
				return _nullStudentPaymentSchedule;
			}
		};
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchBatchPaymentScheduleException;
import info.diit.portal.model.BatchPaymentSchedule;
import info.diit.portal.model.impl.BatchPaymentScheduleImpl;
import info.diit.portal.model.impl.BatchPaymentScheduleModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the batch payment schedule service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see BatchPaymentSchedulePersistence
 * @see BatchPaymentScheduleUtil
 * @generated
 */
public class BatchPaymentSchedulePersistenceImpl extends BasePersistenceImpl<BatchPaymentSchedule>
	implements BatchPaymentSchedulePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link BatchPaymentScheduleUtil} to access the batch payment schedule persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = BatchPaymentScheduleImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCH = new FinderPath(BatchPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			BatchPaymentScheduleModelImpl.FINDER_CACHE_ENABLED,
			BatchPaymentScheduleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBatch",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH = new FinderPath(BatchPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			BatchPaymentScheduleModelImpl.FINDER_CACHE_ENABLED,
			BatchPaymentScheduleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBatch",
			new String[] { Long.class.getName() },
			BatchPaymentScheduleModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCH = new FinderPath(BatchPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			BatchPaymentScheduleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBatch",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BatchPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			BatchPaymentScheduleModelImpl.FINDER_CACHE_ENABLED,
			BatchPaymentScheduleImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BatchPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			BatchPaymentScheduleModelImpl.FINDER_CACHE_ENABLED,
			BatchPaymentScheduleImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BatchPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			BatchPaymentScheduleModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the batch payment schedule in the entity cache if it is enabled.
	 *
	 * @param batchPaymentSchedule the batch payment schedule
	 */
	public void cacheResult(BatchPaymentSchedule batchPaymentSchedule) {
		EntityCacheUtil.putResult(BatchPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			BatchPaymentScheduleImpl.class,
			batchPaymentSchedule.getPrimaryKey(), batchPaymentSchedule);

		batchPaymentSchedule.resetOriginalValues();
	}

	/**
	 * Caches the batch payment schedules in the entity cache if it is enabled.
	 *
	 * @param batchPaymentSchedules the batch payment schedules
	 */
	public void cacheResult(List<BatchPaymentSchedule> batchPaymentSchedules) {
		for (BatchPaymentSchedule batchPaymentSchedule : batchPaymentSchedules) {
			if (EntityCacheUtil.getResult(
						BatchPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
						BatchPaymentScheduleImpl.class,
						batchPaymentSchedule.getPrimaryKey()) == null) {
				cacheResult(batchPaymentSchedule);
			}
			else {
				batchPaymentSchedule.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all batch payment schedules.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(BatchPaymentScheduleImpl.class.getName());
		}

		EntityCacheUtil.clearCache(BatchPaymentScheduleImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the batch payment schedule.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(BatchPaymentSchedule batchPaymentSchedule) {
		EntityCacheUtil.removeResult(BatchPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			BatchPaymentScheduleImpl.class, batchPaymentSchedule.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<BatchPaymentSchedule> batchPaymentSchedules) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (BatchPaymentSchedule batchPaymentSchedule : batchPaymentSchedules) {
			EntityCacheUtil.removeResult(BatchPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
				BatchPaymentScheduleImpl.class,
				batchPaymentSchedule.getPrimaryKey());
		}
	}

	/**
	 * Creates a new batch payment schedule with the primary key. Does not add the batch payment schedule to the database.
	 *
	 * @param batchPaymentScheduleId the primary key for the new batch payment schedule
	 * @return the new batch payment schedule
	 */
	public BatchPaymentSchedule create(long batchPaymentScheduleId) {
		BatchPaymentSchedule batchPaymentSchedule = new BatchPaymentScheduleImpl();

		batchPaymentSchedule.setNew(true);
		batchPaymentSchedule.setPrimaryKey(batchPaymentScheduleId);

		return batchPaymentSchedule;
	}

	/**
	 * Removes the batch payment schedule with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param batchPaymentScheduleId the primary key of the batch payment schedule
	 * @return the batch payment schedule that was removed
	 * @throws info.diit.portal.NoSuchBatchPaymentScheduleException if a batch payment schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchPaymentSchedule remove(long batchPaymentScheduleId)
		throws NoSuchBatchPaymentScheduleException, SystemException {
		return remove(Long.valueOf(batchPaymentScheduleId));
	}

	/**
	 * Removes the batch payment schedule with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the batch payment schedule
	 * @return the batch payment schedule that was removed
	 * @throws info.diit.portal.NoSuchBatchPaymentScheduleException if a batch payment schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchPaymentSchedule remove(Serializable primaryKey)
		throws NoSuchBatchPaymentScheduleException, SystemException {
		Session session = null;

		try {
			session = openSession();

			BatchPaymentSchedule batchPaymentSchedule = (BatchPaymentSchedule)session.get(BatchPaymentScheduleImpl.class,
					primaryKey);

			if (batchPaymentSchedule == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchBatchPaymentScheduleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(batchPaymentSchedule);
		}
		catch (NoSuchBatchPaymentScheduleException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected BatchPaymentSchedule removeImpl(
		BatchPaymentSchedule batchPaymentSchedule) throws SystemException {
		batchPaymentSchedule = toUnwrappedModel(batchPaymentSchedule);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, batchPaymentSchedule);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(batchPaymentSchedule);

		return batchPaymentSchedule;
	}

	@Override
	public BatchPaymentSchedule updateImpl(
		info.diit.portal.model.BatchPaymentSchedule batchPaymentSchedule,
		boolean merge) throws SystemException {
		batchPaymentSchedule = toUnwrappedModel(batchPaymentSchedule);

		boolean isNew = batchPaymentSchedule.isNew();

		BatchPaymentScheduleModelImpl batchPaymentScheduleModelImpl = (BatchPaymentScheduleModelImpl)batchPaymentSchedule;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, batchPaymentSchedule, merge);

			batchPaymentSchedule.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !BatchPaymentScheduleModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((batchPaymentScheduleModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchPaymentScheduleModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCH, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH,
					args);

				args = new Object[] {
						Long.valueOf(batchPaymentScheduleModelImpl.getBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCH, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH,
					args);
			}
		}

		EntityCacheUtil.putResult(BatchPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
			BatchPaymentScheduleImpl.class,
			batchPaymentSchedule.getPrimaryKey(), batchPaymentSchedule);

		return batchPaymentSchedule;
	}

	protected BatchPaymentSchedule toUnwrappedModel(
		BatchPaymentSchedule batchPaymentSchedule) {
		if (batchPaymentSchedule instanceof BatchPaymentScheduleImpl) {
			return batchPaymentSchedule;
		}

		BatchPaymentScheduleImpl batchPaymentScheduleImpl = new BatchPaymentScheduleImpl();

		batchPaymentScheduleImpl.setNew(batchPaymentSchedule.isNew());
		batchPaymentScheduleImpl.setPrimaryKey(batchPaymentSchedule.getPrimaryKey());

		batchPaymentScheduleImpl.setBatchPaymentScheduleId(batchPaymentSchedule.getBatchPaymentScheduleId());
		batchPaymentScheduleImpl.setCompanyId(batchPaymentSchedule.getCompanyId());
		batchPaymentScheduleImpl.setUserId(batchPaymentSchedule.getUserId());
		batchPaymentScheduleImpl.setCreateDate(batchPaymentSchedule.getCreateDate());
		batchPaymentScheduleImpl.setModifiedDate(batchPaymentSchedule.getModifiedDate());
		batchPaymentScheduleImpl.setBatchId(batchPaymentSchedule.getBatchId());
		batchPaymentScheduleImpl.setScheduleDate(batchPaymentSchedule.getScheduleDate());
		batchPaymentScheduleImpl.setFeeTypeId(batchPaymentSchedule.getFeeTypeId());
		batchPaymentScheduleImpl.setAmount(batchPaymentSchedule.getAmount());

		return batchPaymentScheduleImpl;
	}

	/**
	 * Returns the batch payment schedule with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the batch payment schedule
	 * @return the batch payment schedule
	 * @throws com.liferay.portal.NoSuchModelException if a batch payment schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchPaymentSchedule findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the batch payment schedule with the primary key or throws a {@link info.diit.portal.NoSuchBatchPaymentScheduleException} if it could not be found.
	 *
	 * @param batchPaymentScheduleId the primary key of the batch payment schedule
	 * @return the batch payment schedule
	 * @throws info.diit.portal.NoSuchBatchPaymentScheduleException if a batch payment schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchPaymentSchedule findByPrimaryKey(long batchPaymentScheduleId)
		throws NoSuchBatchPaymentScheduleException, SystemException {
		BatchPaymentSchedule batchPaymentSchedule = fetchByPrimaryKey(batchPaymentScheduleId);

		if (batchPaymentSchedule == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					batchPaymentScheduleId);
			}

			throw new NoSuchBatchPaymentScheduleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				batchPaymentScheduleId);
		}

		return batchPaymentSchedule;
	}

	/**
	 * Returns the batch payment schedule with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the batch payment schedule
	 * @return the batch payment schedule, or <code>null</code> if a batch payment schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchPaymentSchedule fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the batch payment schedule with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param batchPaymentScheduleId the primary key of the batch payment schedule
	 * @return the batch payment schedule, or <code>null</code> if a batch payment schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchPaymentSchedule fetchByPrimaryKey(long batchPaymentScheduleId)
		throws SystemException {
		BatchPaymentSchedule batchPaymentSchedule = (BatchPaymentSchedule)EntityCacheUtil.getResult(BatchPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
				BatchPaymentScheduleImpl.class, batchPaymentScheduleId);

		if (batchPaymentSchedule == _nullBatchPaymentSchedule) {
			return null;
		}

		if (batchPaymentSchedule == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				batchPaymentSchedule = (BatchPaymentSchedule)session.get(BatchPaymentScheduleImpl.class,
						Long.valueOf(batchPaymentScheduleId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (batchPaymentSchedule != null) {
					cacheResult(batchPaymentSchedule);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(BatchPaymentScheduleModelImpl.ENTITY_CACHE_ENABLED,
						BatchPaymentScheduleImpl.class, batchPaymentScheduleId,
						_nullBatchPaymentSchedule);
				}

				closeSession(session);
			}
		}

		return batchPaymentSchedule;
	}

	/**
	 * Returns all the batch payment schedules where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the matching batch payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchPaymentSchedule> findByBatch(long batchId)
		throws SystemException {
		return findByBatch(batchId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch payment schedules where batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of batch payment schedules
	 * @param end the upper bound of the range of batch payment schedules (not inclusive)
	 * @return the range of matching batch payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchPaymentSchedule> findByBatch(long batchId, int start,
		int end) throws SystemException {
		return findByBatch(batchId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch payment schedules where batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of batch payment schedules
	 * @param end the upper bound of the range of batch payment schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batch payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchPaymentSchedule> findByBatch(long batchId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH;
			finderArgs = new Object[] { batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCH;
			finderArgs = new Object[] { batchId, start, end, orderByComparator };
		}

		List<BatchPaymentSchedule> list = (List<BatchPaymentSchedule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BatchPaymentSchedule batchPaymentSchedule : list) {
				if ((batchId != batchPaymentSchedule.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_BATCHPAYMENTSCHEDULE_WHERE);

			query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				list = (List<BatchPaymentSchedule>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch payment schedule in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch payment schedule
	 * @throws info.diit.portal.NoSuchBatchPaymentScheduleException if a matching batch payment schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchPaymentSchedule findByBatch_First(long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchPaymentScheduleException, SystemException {
		BatchPaymentSchedule batchPaymentSchedule = fetchByBatch_First(batchId,
				orderByComparator);

		if (batchPaymentSchedule != null) {
			return batchPaymentSchedule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchPaymentScheduleException(msg.toString());
	}

	/**
	 * Returns the first batch payment schedule in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch payment schedule, or <code>null</code> if a matching batch payment schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchPaymentSchedule fetchByBatch_First(long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BatchPaymentSchedule> list = findByBatch(batchId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch payment schedule in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch payment schedule
	 * @throws info.diit.portal.NoSuchBatchPaymentScheduleException if a matching batch payment schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchPaymentSchedule findByBatch_Last(long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchPaymentScheduleException, SystemException {
		BatchPaymentSchedule batchPaymentSchedule = fetchByBatch_Last(batchId,
				orderByComparator);

		if (batchPaymentSchedule != null) {
			return batchPaymentSchedule;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchPaymentScheduleException(msg.toString());
	}

	/**
	 * Returns the last batch payment schedule in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch payment schedule, or <code>null</code> if a matching batch payment schedule could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchPaymentSchedule fetchByBatch_Last(long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBatch(batchId);

		List<BatchPaymentSchedule> list = findByBatch(batchId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batch payment schedules before and after the current batch payment schedule in the ordered set where batchId = &#63;.
	 *
	 * @param batchPaymentScheduleId the primary key of the current batch payment schedule
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch payment schedule
	 * @throws info.diit.portal.NoSuchBatchPaymentScheduleException if a batch payment schedule with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchPaymentSchedule[] findByBatch_PrevAndNext(
		long batchPaymentScheduleId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchPaymentScheduleException, SystemException {
		BatchPaymentSchedule batchPaymentSchedule = findByPrimaryKey(batchPaymentScheduleId);

		Session session = null;

		try {
			session = openSession();

			BatchPaymentSchedule[] array = new BatchPaymentScheduleImpl[3];

			array[0] = getByBatch_PrevAndNext(session, batchPaymentSchedule,
					batchId, orderByComparator, true);

			array[1] = batchPaymentSchedule;

			array[2] = getByBatch_PrevAndNext(session, batchPaymentSchedule,
					batchId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BatchPaymentSchedule getByBatch_PrevAndNext(Session session,
		BatchPaymentSchedule batchPaymentSchedule, long batchId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCHPAYMENTSCHEDULE_WHERE);

		query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batchPaymentSchedule);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BatchPaymentSchedule> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the batch payment schedules.
	 *
	 * @return the batch payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchPaymentSchedule> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch payment schedules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of batch payment schedules
	 * @param end the upper bound of the range of batch payment schedules (not inclusive)
	 * @return the range of batch payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchPaymentSchedule> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch payment schedules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of batch payment schedules
	 * @param end the upper bound of the range of batch payment schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of batch payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchPaymentSchedule> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<BatchPaymentSchedule> list = (List<BatchPaymentSchedule>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_BATCHPAYMENTSCHEDULE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_BATCHPAYMENTSCHEDULE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<BatchPaymentSchedule>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<BatchPaymentSchedule>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the batch payment schedules where batchId = &#63; from the database.
	 *
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBatch(long batchId) throws SystemException {
		for (BatchPaymentSchedule batchPaymentSchedule : findByBatch(batchId)) {
			remove(batchPaymentSchedule);
		}
	}

	/**
	 * Removes all the batch payment schedules from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (BatchPaymentSchedule batchPaymentSchedule : findAll()) {
			remove(batchPaymentSchedule);
		}
	}

	/**
	 * Returns the number of batch payment schedules where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the number of matching batch payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBatch(long batchId) throws SystemException {
		Object[] finderArgs = new Object[] { batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCH,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BATCHPAYMENTSCHEDULE_WHERE);

			query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCH,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch payment schedules.
	 *
	 * @return the number of batch payment schedules
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_BATCHPAYMENTSCHEDULE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the batch payment schedule persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.BatchPaymentSchedule")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<BatchPaymentSchedule>> listenersList = new ArrayList<ModelListener<BatchPaymentSchedule>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<BatchPaymentSchedule>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(BatchPaymentScheduleImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_BATCHPAYMENTSCHEDULE = "SELECT batchPaymentSchedule FROM BatchPaymentSchedule batchPaymentSchedule";
	private static final String _SQL_SELECT_BATCHPAYMENTSCHEDULE_WHERE = "SELECT batchPaymentSchedule FROM BatchPaymentSchedule batchPaymentSchedule WHERE ";
	private static final String _SQL_COUNT_BATCHPAYMENTSCHEDULE = "SELECT COUNT(batchPaymentSchedule) FROM BatchPaymentSchedule batchPaymentSchedule";
	private static final String _SQL_COUNT_BATCHPAYMENTSCHEDULE_WHERE = "SELECT COUNT(batchPaymentSchedule) FROM BatchPaymentSchedule batchPaymentSchedule WHERE ";
	private static final String _FINDER_COLUMN_BATCH_BATCHID_2 = "batchPaymentSchedule.batchId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "batchPaymentSchedule.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BatchPaymentSchedule exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No BatchPaymentSchedule exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(BatchPaymentSchedulePersistenceImpl.class);
	private static BatchPaymentSchedule _nullBatchPaymentSchedule = new BatchPaymentScheduleImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<BatchPaymentSchedule> toCacheModel() {
				return _nullBatchPaymentScheduleCacheModel;
			}
		};

	private static CacheModel<BatchPaymentSchedule> _nullBatchPaymentScheduleCacheModel =
		new CacheModel<BatchPaymentSchedule>() {
			public BatchPaymentSchedule toEntityModel() {
				return _nullBatchPaymentSchedule;
			}
		};
}
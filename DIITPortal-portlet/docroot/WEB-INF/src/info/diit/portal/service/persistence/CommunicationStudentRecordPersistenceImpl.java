/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchCommunicationStudentRecordException;
import info.diit.portal.model.CommunicationStudentRecord;
import info.diit.portal.model.impl.CommunicationStudentRecordImpl;
import info.diit.portal.model.impl.CommunicationStudentRecordModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the communication student record service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see CommunicationStudentRecordPersistence
 * @see CommunicationStudentRecordUtil
 * @generated
 */
public class CommunicationStudentRecordPersistenceImpl
	extends BasePersistenceImpl<CommunicationStudentRecord>
	implements CommunicationStudentRecordPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CommunicationStudentRecordUtil} to access the communication student record persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CommunicationStudentRecordImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMMUNICATIONRECORD =
		new FinderPath(CommunicationStudentRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationStudentRecordModelImpl.FINDER_CACHE_ENABLED,
			CommunicationStudentRecordImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCommunicationRecord",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMMUNICATIONRECORD =
		new FinderPath(CommunicationStudentRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationStudentRecordModelImpl.FINDER_CACHE_ENABLED,
			CommunicationStudentRecordImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCommunicationRecord", new String[] { Long.class.getName() },
			CommunicationStudentRecordModelImpl.COMMUNICATIONRECORDID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMMUNICATIONRECORD = new FinderPath(CommunicationStudentRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationStudentRecordModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCommunicationRecord", new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CommunicationStudentRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationStudentRecordModelImpl.FINDER_CACHE_ENABLED,
			CommunicationStudentRecordImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CommunicationStudentRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationStudentRecordModelImpl.FINDER_CACHE_ENABLED,
			CommunicationStudentRecordImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CommunicationStudentRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationStudentRecordModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);

	/**
	 * Caches the communication student record in the entity cache if it is enabled.
	 *
	 * @param communicationStudentRecord the communication student record
	 */
	public void cacheResult(
		CommunicationStudentRecord communicationStudentRecord) {
		EntityCacheUtil.putResult(CommunicationStudentRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationStudentRecordImpl.class,
			communicationStudentRecord.getPrimaryKey(),
			communicationStudentRecord);

		communicationStudentRecord.resetOriginalValues();
	}

	/**
	 * Caches the communication student records in the entity cache if it is enabled.
	 *
	 * @param communicationStudentRecords the communication student records
	 */
	public void cacheResult(
		List<CommunicationStudentRecord> communicationStudentRecords) {
		for (CommunicationStudentRecord communicationStudentRecord : communicationStudentRecords) {
			if (EntityCacheUtil.getResult(
						CommunicationStudentRecordModelImpl.ENTITY_CACHE_ENABLED,
						CommunicationStudentRecordImpl.class,
						communicationStudentRecord.getPrimaryKey()) == null) {
				cacheResult(communicationStudentRecord);
			}
			else {
				communicationStudentRecord.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all communication student records.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CommunicationStudentRecordImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CommunicationStudentRecordImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the communication student record.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(
		CommunicationStudentRecord communicationStudentRecord) {
		EntityCacheUtil.removeResult(CommunicationStudentRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationStudentRecordImpl.class,
			communicationStudentRecord.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(
		List<CommunicationStudentRecord> communicationStudentRecords) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CommunicationStudentRecord communicationStudentRecord : communicationStudentRecords) {
			EntityCacheUtil.removeResult(CommunicationStudentRecordModelImpl.ENTITY_CACHE_ENABLED,
				CommunicationStudentRecordImpl.class,
				communicationStudentRecord.getPrimaryKey());
		}
	}

	/**
	 * Creates a new communication student record with the primary key. Does not add the communication student record to the database.
	 *
	 * @param CommunicationStudentRecorId the primary key for the new communication student record
	 * @return the new communication student record
	 */
	public CommunicationStudentRecord create(long CommunicationStudentRecorId) {
		CommunicationStudentRecord communicationStudentRecord = new CommunicationStudentRecordImpl();

		communicationStudentRecord.setNew(true);
		communicationStudentRecord.setPrimaryKey(CommunicationStudentRecorId);

		return communicationStudentRecord;
	}

	/**
	 * Removes the communication student record with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param CommunicationStudentRecorId the primary key of the communication student record
	 * @return the communication student record that was removed
	 * @throws info.diit.portal.NoSuchCommunicationStudentRecordException if a communication student record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationStudentRecord remove(long CommunicationStudentRecorId)
		throws NoSuchCommunicationStudentRecordException, SystemException {
		return remove(Long.valueOf(CommunicationStudentRecorId));
	}

	/**
	 * Removes the communication student record with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the communication student record
	 * @return the communication student record that was removed
	 * @throws info.diit.portal.NoSuchCommunicationStudentRecordException if a communication student record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CommunicationStudentRecord remove(Serializable primaryKey)
		throws NoSuchCommunicationStudentRecordException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CommunicationStudentRecord communicationStudentRecord = (CommunicationStudentRecord)session.get(CommunicationStudentRecordImpl.class,
					primaryKey);

			if (communicationStudentRecord == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCommunicationStudentRecordException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(communicationStudentRecord);
		}
		catch (NoSuchCommunicationStudentRecordException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CommunicationStudentRecord removeImpl(
		CommunicationStudentRecord communicationStudentRecord)
		throws SystemException {
		communicationStudentRecord = toUnwrappedModel(communicationStudentRecord);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, communicationStudentRecord);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(communicationStudentRecord);

		return communicationStudentRecord;
	}

	@Override
	public CommunicationStudentRecord updateImpl(
		info.diit.portal.model.CommunicationStudentRecord communicationStudentRecord,
		boolean merge) throws SystemException {
		communicationStudentRecord = toUnwrappedModel(communicationStudentRecord);

		boolean isNew = communicationStudentRecord.isNew();

		CommunicationStudentRecordModelImpl communicationStudentRecordModelImpl = (CommunicationStudentRecordModelImpl)communicationStudentRecord;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, communicationStudentRecord, merge);

			communicationStudentRecord.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew ||
				!CommunicationStudentRecordModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((communicationStudentRecordModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMMUNICATIONRECORD.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(communicationStudentRecordModelImpl.getOriginalCommunicationRecordId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMMUNICATIONRECORD,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMMUNICATIONRECORD,
					args);

				args = new Object[] {
						Long.valueOf(communicationStudentRecordModelImpl.getCommunicationRecordId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMMUNICATIONRECORD,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMMUNICATIONRECORD,
					args);
			}
		}

		EntityCacheUtil.putResult(CommunicationStudentRecordModelImpl.ENTITY_CACHE_ENABLED,
			CommunicationStudentRecordImpl.class,
			communicationStudentRecord.getPrimaryKey(),
			communicationStudentRecord);

		return communicationStudentRecord;
	}

	protected CommunicationStudentRecord toUnwrappedModel(
		CommunicationStudentRecord communicationStudentRecord) {
		if (communicationStudentRecord instanceof CommunicationStudentRecordImpl) {
			return communicationStudentRecord;
		}

		CommunicationStudentRecordImpl communicationStudentRecordImpl = new CommunicationStudentRecordImpl();

		communicationStudentRecordImpl.setNew(communicationStudentRecord.isNew());
		communicationStudentRecordImpl.setPrimaryKey(communicationStudentRecord.getPrimaryKey());

		communicationStudentRecordImpl.setCommunicationStudentRecorId(communicationStudentRecord.getCommunicationStudentRecorId());
		communicationStudentRecordImpl.setCompanyId(communicationStudentRecord.getCompanyId());
		communicationStudentRecordImpl.setOrganizationId(communicationStudentRecord.getOrganizationId());
		communicationStudentRecordImpl.setCommunicationBy(communicationStudentRecord.getCommunicationBy());
		communicationStudentRecordImpl.setUserName(communicationStudentRecord.getUserName());
		communicationStudentRecordImpl.setCreateDate(communicationStudentRecord.getCreateDate());
		communicationStudentRecordImpl.setModifiedDate(communicationStudentRecord.getModifiedDate());
		communicationStudentRecordImpl.setCommunicationRecordId(communicationStudentRecord.getCommunicationRecordId());
		communicationStudentRecordImpl.setStudentId(communicationStudentRecord.getStudentId());

		return communicationStudentRecordImpl;
	}

	/**
	 * Returns the communication student record with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the communication student record
	 * @return the communication student record
	 * @throws com.liferay.portal.NoSuchModelException if a communication student record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CommunicationStudentRecord findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the communication student record with the primary key or throws a {@link info.diit.portal.NoSuchCommunicationStudentRecordException} if it could not be found.
	 *
	 * @param CommunicationStudentRecorId the primary key of the communication student record
	 * @return the communication student record
	 * @throws info.diit.portal.NoSuchCommunicationStudentRecordException if a communication student record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationStudentRecord findByPrimaryKey(
		long CommunicationStudentRecorId)
		throws NoSuchCommunicationStudentRecordException, SystemException {
		CommunicationStudentRecord communicationStudentRecord = fetchByPrimaryKey(CommunicationStudentRecorId);

		if (communicationStudentRecord == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					CommunicationStudentRecorId);
			}

			throw new NoSuchCommunicationStudentRecordException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				CommunicationStudentRecorId);
		}

		return communicationStudentRecord;
	}

	/**
	 * Returns the communication student record with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the communication student record
	 * @return the communication student record, or <code>null</code> if a communication student record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CommunicationStudentRecord fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the communication student record with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param CommunicationStudentRecorId the primary key of the communication student record
	 * @return the communication student record, or <code>null</code> if a communication student record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationStudentRecord fetchByPrimaryKey(
		long CommunicationStudentRecorId) throws SystemException {
		CommunicationStudentRecord communicationStudentRecord = (CommunicationStudentRecord)EntityCacheUtil.getResult(CommunicationStudentRecordModelImpl.ENTITY_CACHE_ENABLED,
				CommunicationStudentRecordImpl.class,
				CommunicationStudentRecorId);

		if (communicationStudentRecord == _nullCommunicationStudentRecord) {
			return null;
		}

		if (communicationStudentRecord == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				communicationStudentRecord = (CommunicationStudentRecord)session.get(CommunicationStudentRecordImpl.class,
						Long.valueOf(CommunicationStudentRecorId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (communicationStudentRecord != null) {
					cacheResult(communicationStudentRecord);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(CommunicationStudentRecordModelImpl.ENTITY_CACHE_ENABLED,
						CommunicationStudentRecordImpl.class,
						CommunicationStudentRecorId,
						_nullCommunicationStudentRecord);
				}

				closeSession(session);
			}
		}

		return communicationStudentRecord;
	}

	/**
	 * Returns all the communication student records where communicationRecordId = &#63;.
	 *
	 * @param communicationRecordId the communication record ID
	 * @return the matching communication student records
	 * @throws SystemException if a system exception occurred
	 */
	public List<CommunicationStudentRecord> findByCommunicationRecord(
		long communicationRecordId) throws SystemException {
		return findByCommunicationRecord(communicationRecordId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the communication student records where communicationRecordId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param communicationRecordId the communication record ID
	 * @param start the lower bound of the range of communication student records
	 * @param end the upper bound of the range of communication student records (not inclusive)
	 * @return the range of matching communication student records
	 * @throws SystemException if a system exception occurred
	 */
	public List<CommunicationStudentRecord> findByCommunicationRecord(
		long communicationRecordId, int start, int end)
		throws SystemException {
		return findByCommunicationRecord(communicationRecordId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the communication student records where communicationRecordId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param communicationRecordId the communication record ID
	 * @param start the lower bound of the range of communication student records
	 * @param end the upper bound of the range of communication student records (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching communication student records
	 * @throws SystemException if a system exception occurred
	 */
	public List<CommunicationStudentRecord> findByCommunicationRecord(
		long communicationRecordId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMMUNICATIONRECORD;
			finderArgs = new Object[] { communicationRecordId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMMUNICATIONRECORD;
			finderArgs = new Object[] {
					communicationRecordId,
					
					start, end, orderByComparator
				};
		}

		List<CommunicationStudentRecord> list = (List<CommunicationStudentRecord>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CommunicationStudentRecord communicationStudentRecord : list) {
				if ((communicationRecordId != communicationStudentRecord.getCommunicationRecordId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_COMMUNICATIONSTUDENTRECORD_WHERE);

			query.append(_FINDER_COLUMN_COMMUNICATIONRECORD_COMMUNICATIONRECORDID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(communicationRecordId);

				list = (List<CommunicationStudentRecord>)QueryUtil.list(q,
						getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first communication student record in the ordered set where communicationRecordId = &#63;.
	 *
	 * @param communicationRecordId the communication record ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching communication student record
	 * @throws info.diit.portal.NoSuchCommunicationStudentRecordException if a matching communication student record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationStudentRecord findByCommunicationRecord_First(
		long communicationRecordId, OrderByComparator orderByComparator)
		throws NoSuchCommunicationStudentRecordException, SystemException {
		CommunicationStudentRecord communicationStudentRecord = fetchByCommunicationRecord_First(communicationRecordId,
				orderByComparator);

		if (communicationStudentRecord != null) {
			return communicationStudentRecord;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("communicationRecordId=");
		msg.append(communicationRecordId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCommunicationStudentRecordException(msg.toString());
	}

	/**
	 * Returns the first communication student record in the ordered set where communicationRecordId = &#63;.
	 *
	 * @param communicationRecordId the communication record ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching communication student record, or <code>null</code> if a matching communication student record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationStudentRecord fetchByCommunicationRecord_First(
		long communicationRecordId, OrderByComparator orderByComparator)
		throws SystemException {
		List<CommunicationStudentRecord> list = findByCommunicationRecord(communicationRecordId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last communication student record in the ordered set where communicationRecordId = &#63;.
	 *
	 * @param communicationRecordId the communication record ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching communication student record
	 * @throws info.diit.portal.NoSuchCommunicationStudentRecordException if a matching communication student record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationStudentRecord findByCommunicationRecord_Last(
		long communicationRecordId, OrderByComparator orderByComparator)
		throws NoSuchCommunicationStudentRecordException, SystemException {
		CommunicationStudentRecord communicationStudentRecord = fetchByCommunicationRecord_Last(communicationRecordId,
				orderByComparator);

		if (communicationStudentRecord != null) {
			return communicationStudentRecord;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("communicationRecordId=");
		msg.append(communicationRecordId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCommunicationStudentRecordException(msg.toString());
	}

	/**
	 * Returns the last communication student record in the ordered set where communicationRecordId = &#63;.
	 *
	 * @param communicationRecordId the communication record ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching communication student record, or <code>null</code> if a matching communication student record could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationStudentRecord fetchByCommunicationRecord_Last(
		long communicationRecordId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByCommunicationRecord(communicationRecordId);

		List<CommunicationStudentRecord> list = findByCommunicationRecord(communicationRecordId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the communication student records before and after the current communication student record in the ordered set where communicationRecordId = &#63;.
	 *
	 * @param CommunicationStudentRecorId the primary key of the current communication student record
	 * @param communicationRecordId the communication record ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next communication student record
	 * @throws info.diit.portal.NoSuchCommunicationStudentRecordException if a communication student record with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public CommunicationStudentRecord[] findByCommunicationRecord_PrevAndNext(
		long CommunicationStudentRecorId, long communicationRecordId,
		OrderByComparator orderByComparator)
		throws NoSuchCommunicationStudentRecordException, SystemException {
		CommunicationStudentRecord communicationStudentRecord = findByPrimaryKey(CommunicationStudentRecorId);

		Session session = null;

		try {
			session = openSession();

			CommunicationStudentRecord[] array = new CommunicationStudentRecordImpl[3];

			array[0] = getByCommunicationRecord_PrevAndNext(session,
					communicationStudentRecord, communicationRecordId,
					orderByComparator, true);

			array[1] = communicationStudentRecord;

			array[2] = getByCommunicationRecord_PrevAndNext(session,
					communicationStudentRecord, communicationRecordId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CommunicationStudentRecord getByCommunicationRecord_PrevAndNext(
		Session session, CommunicationStudentRecord communicationStudentRecord,
		long communicationRecordId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COMMUNICATIONSTUDENTRECORD_WHERE);

		query.append(_FINDER_COLUMN_COMMUNICATIONRECORD_COMMUNICATIONRECORDID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(communicationRecordId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(communicationStudentRecord);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CommunicationStudentRecord> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the communication student records.
	 *
	 * @return the communication student records
	 * @throws SystemException if a system exception occurred
	 */
	public List<CommunicationStudentRecord> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the communication student records.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of communication student records
	 * @param end the upper bound of the range of communication student records (not inclusive)
	 * @return the range of communication student records
	 * @throws SystemException if a system exception occurred
	 */
	public List<CommunicationStudentRecord> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the communication student records.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of communication student records
	 * @param end the upper bound of the range of communication student records (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of communication student records
	 * @throws SystemException if a system exception occurred
	 */
	public List<CommunicationStudentRecord> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CommunicationStudentRecord> list = (List<CommunicationStudentRecord>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_COMMUNICATIONSTUDENTRECORD);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COMMUNICATIONSTUDENTRECORD;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<CommunicationStudentRecord>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<CommunicationStudentRecord>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the communication student records where communicationRecordId = &#63; from the database.
	 *
	 * @param communicationRecordId the communication record ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCommunicationRecord(long communicationRecordId)
		throws SystemException {
		for (CommunicationStudentRecord communicationStudentRecord : findByCommunicationRecord(
				communicationRecordId)) {
			remove(communicationStudentRecord);
		}
	}

	/**
	 * Removes all the communication student records from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (CommunicationStudentRecord communicationStudentRecord : findAll()) {
			remove(communicationStudentRecord);
		}
	}

	/**
	 * Returns the number of communication student records where communicationRecordId = &#63;.
	 *
	 * @param communicationRecordId the communication record ID
	 * @return the number of matching communication student records
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCommunicationRecord(long communicationRecordId)
		throws SystemException {
		Object[] finderArgs = new Object[] { communicationRecordId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMMUNICATIONRECORD,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COMMUNICATIONSTUDENTRECORD_WHERE);

			query.append(_FINDER_COLUMN_COMMUNICATIONRECORD_COMMUNICATIONRECORDID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(communicationRecordId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMMUNICATIONRECORD,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of communication student records.
	 *
	 * @return the number of communication student records
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COMMUNICATIONSTUDENTRECORD);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the communication student record persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.CommunicationStudentRecord")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CommunicationStudentRecord>> listenersList = new ArrayList<ModelListener<CommunicationStudentRecord>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CommunicationStudentRecord>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CommunicationStudentRecordImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_COMMUNICATIONSTUDENTRECORD = "SELECT communicationStudentRecord FROM CommunicationStudentRecord communicationStudentRecord";
	private static final String _SQL_SELECT_COMMUNICATIONSTUDENTRECORD_WHERE = "SELECT communicationStudentRecord FROM CommunicationStudentRecord communicationStudentRecord WHERE ";
	private static final String _SQL_COUNT_COMMUNICATIONSTUDENTRECORD = "SELECT COUNT(communicationStudentRecord) FROM CommunicationStudentRecord communicationStudentRecord";
	private static final String _SQL_COUNT_COMMUNICATIONSTUDENTRECORD_WHERE = "SELECT COUNT(communicationStudentRecord) FROM CommunicationStudentRecord communicationStudentRecord WHERE ";
	private static final String _FINDER_COLUMN_COMMUNICATIONRECORD_COMMUNICATIONRECORDID_2 =
		"communicationStudentRecord.communicationRecordId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "communicationStudentRecord.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CommunicationStudentRecord exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CommunicationStudentRecord exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CommunicationStudentRecordPersistenceImpl.class);
	private static CommunicationStudentRecord _nullCommunicationStudentRecord = new CommunicationStudentRecordImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CommunicationStudentRecord> toCacheModel() {
				return _nullCommunicationStudentRecordCacheModel;
			}
		};

	private static CacheModel<CommunicationStudentRecord> _nullCommunicationStudentRecordCacheModel =
		new CacheModel<CommunicationStudentRecord>() {
			public CommunicationStudentRecord toEntityModel() {
				return _nullCommunicationStudentRecord;
			}
		};
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchStudentDiscountException;
import info.diit.portal.model.StudentDiscount;
import info.diit.portal.model.impl.StudentDiscountImpl;
import info.diit.portal.model.impl.StudentDiscountModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the student discount service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see StudentDiscountPersistence
 * @see StudentDiscountUtil
 * @generated
 */
public class StudentDiscountPersistenceImpl extends BasePersistenceImpl<StudentDiscount>
	implements StudentDiscountPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link StudentDiscountUtil} to access the student discount persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = StudentDiscountImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTBATCH =
		new FinderPath(StudentDiscountModelImpl.ENTITY_CACHE_ENABLED,
			StudentDiscountModelImpl.FINDER_CACHE_ENABLED,
			StudentDiscountImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByStudentBatch",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH =
		new FinderPath(StudentDiscountModelImpl.ENTITY_CACHE_ENABLED,
			StudentDiscountModelImpl.FINDER_CACHE_ENABLED,
			StudentDiscountImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStudentBatch",
			new String[] { Long.class.getName(), Long.class.getName() },
			StudentDiscountModelImpl.STUDENTID_COLUMN_BITMASK |
			StudentDiscountModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STUDENTBATCH = new FinderPath(StudentDiscountModelImpl.ENTITY_CACHE_ENABLED,
			StudentDiscountModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByStudentBatch",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(StudentDiscountModelImpl.ENTITY_CACHE_ENABLED,
			StudentDiscountModelImpl.FINDER_CACHE_ENABLED,
			StudentDiscountImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(StudentDiscountModelImpl.ENTITY_CACHE_ENABLED,
			StudentDiscountModelImpl.FINDER_CACHE_ENABLED,
			StudentDiscountImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(StudentDiscountModelImpl.ENTITY_CACHE_ENABLED,
			StudentDiscountModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the student discount in the entity cache if it is enabled.
	 *
	 * @param studentDiscount the student discount
	 */
	public void cacheResult(StudentDiscount studentDiscount) {
		EntityCacheUtil.putResult(StudentDiscountModelImpl.ENTITY_CACHE_ENABLED,
			StudentDiscountImpl.class, studentDiscount.getPrimaryKey(),
			studentDiscount);

		studentDiscount.resetOriginalValues();
	}

	/**
	 * Caches the student discounts in the entity cache if it is enabled.
	 *
	 * @param studentDiscounts the student discounts
	 */
	public void cacheResult(List<StudentDiscount> studentDiscounts) {
		for (StudentDiscount studentDiscount : studentDiscounts) {
			if (EntityCacheUtil.getResult(
						StudentDiscountModelImpl.ENTITY_CACHE_ENABLED,
						StudentDiscountImpl.class,
						studentDiscount.getPrimaryKey()) == null) {
				cacheResult(studentDiscount);
			}
			else {
				studentDiscount.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all student discounts.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(StudentDiscountImpl.class.getName());
		}

		EntityCacheUtil.clearCache(StudentDiscountImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the student discount.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(StudentDiscount studentDiscount) {
		EntityCacheUtil.removeResult(StudentDiscountModelImpl.ENTITY_CACHE_ENABLED,
			StudentDiscountImpl.class, studentDiscount.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<StudentDiscount> studentDiscounts) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (StudentDiscount studentDiscount : studentDiscounts) {
			EntityCacheUtil.removeResult(StudentDiscountModelImpl.ENTITY_CACHE_ENABLED,
				StudentDiscountImpl.class, studentDiscount.getPrimaryKey());
		}
	}

	/**
	 * Creates a new student discount with the primary key. Does not add the student discount to the database.
	 *
	 * @param studentDiscountId the primary key for the new student discount
	 * @return the new student discount
	 */
	public StudentDiscount create(long studentDiscountId) {
		StudentDiscount studentDiscount = new StudentDiscountImpl();

		studentDiscount.setNew(true);
		studentDiscount.setPrimaryKey(studentDiscountId);

		return studentDiscount;
	}

	/**
	 * Removes the student discount with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param studentDiscountId the primary key of the student discount
	 * @return the student discount that was removed
	 * @throws info.diit.portal.NoSuchStudentDiscountException if a student discount with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDiscount remove(long studentDiscountId)
		throws NoSuchStudentDiscountException, SystemException {
		return remove(Long.valueOf(studentDiscountId));
	}

	/**
	 * Removes the student discount with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the student discount
	 * @return the student discount that was removed
	 * @throws info.diit.portal.NoSuchStudentDiscountException if a student discount with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentDiscount remove(Serializable primaryKey)
		throws NoSuchStudentDiscountException, SystemException {
		Session session = null;

		try {
			session = openSession();

			StudentDiscount studentDiscount = (StudentDiscount)session.get(StudentDiscountImpl.class,
					primaryKey);

			if (studentDiscount == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchStudentDiscountException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(studentDiscount);
		}
		catch (NoSuchStudentDiscountException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected StudentDiscount removeImpl(StudentDiscount studentDiscount)
		throws SystemException {
		studentDiscount = toUnwrappedModel(studentDiscount);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, studentDiscount);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(studentDiscount);

		return studentDiscount;
	}

	@Override
	public StudentDiscount updateImpl(
		info.diit.portal.model.StudentDiscount studentDiscount, boolean merge)
		throws SystemException {
		studentDiscount = toUnwrappedModel(studentDiscount);

		boolean isNew = studentDiscount.isNew();

		StudentDiscountModelImpl studentDiscountModelImpl = (StudentDiscountModelImpl)studentDiscount;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, studentDiscount, merge);

			studentDiscount.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !StudentDiscountModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((studentDiscountModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(studentDiscountModelImpl.getOriginalStudentId()),
						Long.valueOf(studentDiscountModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH,
					args);

				args = new Object[] {
						Long.valueOf(studentDiscountModelImpl.getStudentId()),
						Long.valueOf(studentDiscountModelImpl.getBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH,
					args);
			}
		}

		EntityCacheUtil.putResult(StudentDiscountModelImpl.ENTITY_CACHE_ENABLED,
			StudentDiscountImpl.class, studentDiscount.getPrimaryKey(),
			studentDiscount);

		return studentDiscount;
	}

	protected StudentDiscount toUnwrappedModel(StudentDiscount studentDiscount) {
		if (studentDiscount instanceof StudentDiscountImpl) {
			return studentDiscount;
		}

		StudentDiscountImpl studentDiscountImpl = new StudentDiscountImpl();

		studentDiscountImpl.setNew(studentDiscount.isNew());
		studentDiscountImpl.setPrimaryKey(studentDiscount.getPrimaryKey());

		studentDiscountImpl.setStudentDiscountId(studentDiscount.getStudentDiscountId());
		studentDiscountImpl.setCompanyId(studentDiscount.getCompanyId());
		studentDiscountImpl.setUserId(studentDiscount.getUserId());
		studentDiscountImpl.setCreateDate(studentDiscount.getCreateDate());
		studentDiscountImpl.setModifiedDate(studentDiscount.getModifiedDate());
		studentDiscountImpl.setStudentId(studentDiscount.getStudentId());
		studentDiscountImpl.setBatchId(studentDiscount.getBatchId());
		studentDiscountImpl.setScholarships(studentDiscount.getScholarships());
		studentDiscountImpl.setDiscount(studentDiscount.getDiscount());

		return studentDiscountImpl;
	}

	/**
	 * Returns the student discount with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the student discount
	 * @return the student discount
	 * @throws com.liferay.portal.NoSuchModelException if a student discount with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentDiscount findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the student discount with the primary key or throws a {@link info.diit.portal.NoSuchStudentDiscountException} if it could not be found.
	 *
	 * @param studentDiscountId the primary key of the student discount
	 * @return the student discount
	 * @throws info.diit.portal.NoSuchStudentDiscountException if a student discount with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDiscount findByPrimaryKey(long studentDiscountId)
		throws NoSuchStudentDiscountException, SystemException {
		StudentDiscount studentDiscount = fetchByPrimaryKey(studentDiscountId);

		if (studentDiscount == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + studentDiscountId);
			}

			throw new NoSuchStudentDiscountException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				studentDiscountId);
		}

		return studentDiscount;
	}

	/**
	 * Returns the student discount with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the student discount
	 * @return the student discount, or <code>null</code> if a student discount with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StudentDiscount fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the student discount with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param studentDiscountId the primary key of the student discount
	 * @return the student discount, or <code>null</code> if a student discount with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDiscount fetchByPrimaryKey(long studentDiscountId)
		throws SystemException {
		StudentDiscount studentDiscount = (StudentDiscount)EntityCacheUtil.getResult(StudentDiscountModelImpl.ENTITY_CACHE_ENABLED,
				StudentDiscountImpl.class, studentDiscountId);

		if (studentDiscount == _nullStudentDiscount) {
			return null;
		}

		if (studentDiscount == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				studentDiscount = (StudentDiscount)session.get(StudentDiscountImpl.class,
						Long.valueOf(studentDiscountId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (studentDiscount != null) {
					cacheResult(studentDiscount);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(StudentDiscountModelImpl.ENTITY_CACHE_ENABLED,
						StudentDiscountImpl.class, studentDiscountId,
						_nullStudentDiscount);
				}

				closeSession(session);
			}
		}

		return studentDiscount;
	}

	/**
	 * Returns all the student discounts where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @return the matching student discounts
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentDiscount> findByStudentBatch(long studentId, long batchId)
		throws SystemException {
		return findByStudentBatch(studentId, batchId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the student discounts where studentId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of student discounts
	 * @param end the upper bound of the range of student discounts (not inclusive)
	 * @return the range of matching student discounts
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentDiscount> findByStudentBatch(long studentId,
		long batchId, int start, int end) throws SystemException {
		return findByStudentBatch(studentId, batchId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the student discounts where studentId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of student discounts
	 * @param end the upper bound of the range of student discounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching student discounts
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentDiscount> findByStudentBatch(long studentId,
		long batchId, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH;
			finderArgs = new Object[] { studentId, batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTBATCH;
			finderArgs = new Object[] {
					studentId, batchId,
					
					start, end, orderByComparator
				};
		}

		List<StudentDiscount> list = (List<StudentDiscount>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (StudentDiscount studentDiscount : list) {
				if ((studentId != studentDiscount.getStudentId()) ||
						(batchId != studentDiscount.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_STUDENTDISCOUNT_WHERE);

			query.append(_FINDER_COLUMN_STUDENTBATCH_STUDENTID_2);

			query.append(_FINDER_COLUMN_STUDENTBATCH_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				qPos.add(batchId);

				list = (List<StudentDiscount>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student discount
	 * @throws info.diit.portal.NoSuchStudentDiscountException if a matching student discount could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDiscount findByStudentBatch_First(long studentId,
		long batchId, OrderByComparator orderByComparator)
		throws NoSuchStudentDiscountException, SystemException {
		StudentDiscount studentDiscount = fetchByStudentBatch_First(studentId,
				batchId, orderByComparator);

		if (studentDiscount != null) {
			return studentDiscount;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchStudentDiscountException(msg.toString());
	}

	/**
	 * Returns the first student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching student discount, or <code>null</code> if a matching student discount could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDiscount fetchByStudentBatch_First(long studentId,
		long batchId, OrderByComparator orderByComparator)
		throws SystemException {
		List<StudentDiscount> list = findByStudentBatch(studentId, batchId, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student discount
	 * @throws info.diit.portal.NoSuchStudentDiscountException if a matching student discount could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDiscount findByStudentBatch_Last(long studentId,
		long batchId, OrderByComparator orderByComparator)
		throws NoSuchStudentDiscountException, SystemException {
		StudentDiscount studentDiscount = fetchByStudentBatch_Last(studentId,
				batchId, orderByComparator);

		if (studentDiscount != null) {
			return studentDiscount;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchStudentDiscountException(msg.toString());
	}

	/**
	 * Returns the last student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching student discount, or <code>null</code> if a matching student discount could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDiscount fetchByStudentBatch_Last(long studentId,
		long batchId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByStudentBatch(studentId, batchId);

		List<StudentDiscount> list = findByStudentBatch(studentId, batchId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the student discounts before and after the current student discount in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentDiscountId the primary key of the current student discount
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next student discount
	 * @throws info.diit.portal.NoSuchStudentDiscountException if a student discount with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StudentDiscount[] findByStudentBatch_PrevAndNext(
		long studentDiscountId, long studentId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchStudentDiscountException, SystemException {
		StudentDiscount studentDiscount = findByPrimaryKey(studentDiscountId);

		Session session = null;

		try {
			session = openSession();

			StudentDiscount[] array = new StudentDiscountImpl[3];

			array[0] = getByStudentBatch_PrevAndNext(session, studentDiscount,
					studentId, batchId, orderByComparator, true);

			array[1] = studentDiscount;

			array[2] = getByStudentBatch_PrevAndNext(session, studentDiscount,
					studentId, batchId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected StudentDiscount getByStudentBatch_PrevAndNext(Session session,
		StudentDiscount studentDiscount, long studentId, long batchId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_STUDENTDISCOUNT_WHERE);

		query.append(_FINDER_COLUMN_STUDENTBATCH_STUDENTID_2);

		query.append(_FINDER_COLUMN_STUDENTBATCH_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(studentId);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(studentDiscount);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<StudentDiscount> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the student discounts.
	 *
	 * @return the student discounts
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentDiscount> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the student discounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of student discounts
	 * @param end the upper bound of the range of student discounts (not inclusive)
	 * @return the range of student discounts
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentDiscount> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the student discounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of student discounts
	 * @param end the upper bound of the range of student discounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of student discounts
	 * @throws SystemException if a system exception occurred
	 */
	public List<StudentDiscount> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<StudentDiscount> list = (List<StudentDiscount>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_STUDENTDISCOUNT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_STUDENTDISCOUNT;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<StudentDiscount>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<StudentDiscount>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the student discounts where studentId = &#63; and batchId = &#63; from the database.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByStudentBatch(long studentId, long batchId)
		throws SystemException {
		for (StudentDiscount studentDiscount : findByStudentBatch(studentId,
				batchId)) {
			remove(studentDiscount);
		}
	}

	/**
	 * Removes all the student discounts from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (StudentDiscount studentDiscount : findAll()) {
			remove(studentDiscount);
		}
	}

	/**
	 * Returns the number of student discounts where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @return the number of matching student discounts
	 * @throws SystemException if a system exception occurred
	 */
	public int countByStudentBatch(long studentId, long batchId)
		throws SystemException {
		Object[] finderArgs = new Object[] { studentId, batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_STUDENTDISCOUNT_WHERE);

			query.append(_FINDER_COLUMN_STUDENTBATCH_STUDENTID_2);

			query.append(_FINDER_COLUMN_STUDENTBATCH_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of student discounts.
	 *
	 * @return the number of student discounts
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_STUDENTDISCOUNT);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the student discount persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.StudentDiscount")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<StudentDiscount>> listenersList = new ArrayList<ModelListener<StudentDiscount>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<StudentDiscount>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(StudentDiscountImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_STUDENTDISCOUNT = "SELECT studentDiscount FROM StudentDiscount studentDiscount";
	private static final String _SQL_SELECT_STUDENTDISCOUNT_WHERE = "SELECT studentDiscount FROM StudentDiscount studentDiscount WHERE ";
	private static final String _SQL_COUNT_STUDENTDISCOUNT = "SELECT COUNT(studentDiscount) FROM StudentDiscount studentDiscount";
	private static final String _SQL_COUNT_STUDENTDISCOUNT_WHERE = "SELECT COUNT(studentDiscount) FROM StudentDiscount studentDiscount WHERE ";
	private static final String _FINDER_COLUMN_STUDENTBATCH_STUDENTID_2 = "studentDiscount.studentId = ? AND ";
	private static final String _FINDER_COLUMN_STUDENTBATCH_BATCHID_2 = "studentDiscount.batchId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "studentDiscount.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No StudentDiscount exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No StudentDiscount exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(StudentDiscountPersistenceImpl.class);
	private static StudentDiscount _nullStudentDiscount = new StudentDiscountImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<StudentDiscount> toCacheModel() {
				return _nullStudentDiscountCacheModel;
			}
		};

	private static CacheModel<StudentDiscount> _nullStudentDiscountCacheModel = new CacheModel<StudentDiscount>() {
			public StudentDiscount toEntityModel() {
				return _nullStudentDiscount;
			}
		};
}
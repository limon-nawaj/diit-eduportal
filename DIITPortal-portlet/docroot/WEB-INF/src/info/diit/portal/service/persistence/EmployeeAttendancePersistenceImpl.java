/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchEmployeeAttendanceException;
import info.diit.portal.model.EmployeeAttendance;
import info.diit.portal.model.impl.EmployeeAttendanceImpl;
import info.diit.portal.model.impl.EmployeeAttendanceModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the employee attendance service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see EmployeeAttendancePersistence
 * @see EmployeeAttendanceUtil
 * @generated
 */
public class EmployeeAttendancePersistenceImpl extends BasePersistenceImpl<EmployeeAttendance>
	implements EmployeeAttendancePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EmployeeAttendanceUtil} to access the employee attendance persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EmployeeAttendanceImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceModelImpl.FINDER_CACHE_ENABLED,
			EmployeeAttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceModelImpl.FINDER_CACHE_ENABLED,
			EmployeeAttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the employee attendance in the entity cache if it is enabled.
	 *
	 * @param employeeAttendance the employee attendance
	 */
	public void cacheResult(EmployeeAttendance employeeAttendance) {
		EntityCacheUtil.putResult(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceImpl.class, employeeAttendance.getPrimaryKey(),
			employeeAttendance);

		employeeAttendance.resetOriginalValues();
	}

	/**
	 * Caches the employee attendances in the entity cache if it is enabled.
	 *
	 * @param employeeAttendances the employee attendances
	 */
	public void cacheResult(List<EmployeeAttendance> employeeAttendances) {
		for (EmployeeAttendance employeeAttendance : employeeAttendances) {
			if (EntityCacheUtil.getResult(
						EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
						EmployeeAttendanceImpl.class,
						employeeAttendance.getPrimaryKey()) == null) {
				cacheResult(employeeAttendance);
			}
			else {
				employeeAttendance.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all employee attendances.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EmployeeAttendanceImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EmployeeAttendanceImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the employee attendance.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EmployeeAttendance employeeAttendance) {
		EntityCacheUtil.removeResult(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceImpl.class, employeeAttendance.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<EmployeeAttendance> employeeAttendances) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EmployeeAttendance employeeAttendance : employeeAttendances) {
			EntityCacheUtil.removeResult(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
				EmployeeAttendanceImpl.class, employeeAttendance.getPrimaryKey());
		}
	}

	/**
	 * Creates a new employee attendance with the primary key. Does not add the employee attendance to the database.
	 *
	 * @param employeeAttendanceId the primary key for the new employee attendance
	 * @return the new employee attendance
	 */
	public EmployeeAttendance create(long employeeAttendanceId) {
		EmployeeAttendance employeeAttendance = new EmployeeAttendanceImpl();

		employeeAttendance.setNew(true);
		employeeAttendance.setPrimaryKey(employeeAttendanceId);

		return employeeAttendance;
	}

	/**
	 * Removes the employee attendance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param employeeAttendanceId the primary key of the employee attendance
	 * @return the employee attendance that was removed
	 * @throws info.diit.portal.NoSuchEmployeeAttendanceException if a employee attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance remove(long employeeAttendanceId)
		throws NoSuchEmployeeAttendanceException, SystemException {
		return remove(Long.valueOf(employeeAttendanceId));
	}

	/**
	 * Removes the employee attendance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the employee attendance
	 * @return the employee attendance that was removed
	 * @throws info.diit.portal.NoSuchEmployeeAttendanceException if a employee attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeAttendance remove(Serializable primaryKey)
		throws NoSuchEmployeeAttendanceException, SystemException {
		Session session = null;

		try {
			session = openSession();

			EmployeeAttendance employeeAttendance = (EmployeeAttendance)session.get(EmployeeAttendanceImpl.class,
					primaryKey);

			if (employeeAttendance == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEmployeeAttendanceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(employeeAttendance);
		}
		catch (NoSuchEmployeeAttendanceException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EmployeeAttendance removeImpl(
		EmployeeAttendance employeeAttendance) throws SystemException {
		employeeAttendance = toUnwrappedModel(employeeAttendance);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, employeeAttendance);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(employeeAttendance);

		return employeeAttendance;
	}

	@Override
	public EmployeeAttendance updateImpl(
		info.diit.portal.model.EmployeeAttendance employeeAttendance,
		boolean merge) throws SystemException {
		employeeAttendance = toUnwrappedModel(employeeAttendance);

		boolean isNew = employeeAttendance.isNew();

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, employeeAttendance, merge);

			employeeAttendance.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeAttendanceImpl.class, employeeAttendance.getPrimaryKey(),
			employeeAttendance);

		return employeeAttendance;
	}

	protected EmployeeAttendance toUnwrappedModel(
		EmployeeAttendance employeeAttendance) {
		if (employeeAttendance instanceof EmployeeAttendanceImpl) {
			return employeeAttendance;
		}

		EmployeeAttendanceImpl employeeAttendanceImpl = new EmployeeAttendanceImpl();

		employeeAttendanceImpl.setNew(employeeAttendance.isNew());
		employeeAttendanceImpl.setPrimaryKey(employeeAttendance.getPrimaryKey());

		employeeAttendanceImpl.setEmployeeAttendanceId(employeeAttendance.getEmployeeAttendanceId());
		employeeAttendanceImpl.setEmployeeId(employeeAttendance.getEmployeeId());
		employeeAttendanceImpl.setExpectedStart(employeeAttendance.getExpectedStart());
		employeeAttendanceImpl.setExpectedEnd(employeeAttendance.getExpectedEnd());
		employeeAttendanceImpl.setRealTime(employeeAttendance.getRealTime());
		employeeAttendanceImpl.setStatus(employeeAttendance.getStatus());

		return employeeAttendanceImpl;
	}

	/**
	 * Returns the employee attendance with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the employee attendance
	 * @return the employee attendance
	 * @throws com.liferay.portal.NoSuchModelException if a employee attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeAttendance findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the employee attendance with the primary key or throws a {@link info.diit.portal.NoSuchEmployeeAttendanceException} if it could not be found.
	 *
	 * @param employeeAttendanceId the primary key of the employee attendance
	 * @return the employee attendance
	 * @throws info.diit.portal.NoSuchEmployeeAttendanceException if a employee attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance findByPrimaryKey(long employeeAttendanceId)
		throws NoSuchEmployeeAttendanceException, SystemException {
		EmployeeAttendance employeeAttendance = fetchByPrimaryKey(employeeAttendanceId);

		if (employeeAttendance == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					employeeAttendanceId);
			}

			throw new NoSuchEmployeeAttendanceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				employeeAttendanceId);
		}

		return employeeAttendance;
	}

	/**
	 * Returns the employee attendance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the employee attendance
	 * @return the employee attendance, or <code>null</code> if a employee attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeAttendance fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the employee attendance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param employeeAttendanceId the primary key of the employee attendance
	 * @return the employee attendance, or <code>null</code> if a employee attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeAttendance fetchByPrimaryKey(long employeeAttendanceId)
		throws SystemException {
		EmployeeAttendance employeeAttendance = (EmployeeAttendance)EntityCacheUtil.getResult(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
				EmployeeAttendanceImpl.class, employeeAttendanceId);

		if (employeeAttendance == _nullEmployeeAttendance) {
			return null;
		}

		if (employeeAttendance == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				employeeAttendance = (EmployeeAttendance)session.get(EmployeeAttendanceImpl.class,
						Long.valueOf(employeeAttendanceId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (employeeAttendance != null) {
					cacheResult(employeeAttendance);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(EmployeeAttendanceModelImpl.ENTITY_CACHE_ENABLED,
						EmployeeAttendanceImpl.class, employeeAttendanceId,
						_nullEmployeeAttendance);
				}

				closeSession(session);
			}
		}

		return employeeAttendance;
	}

	/**
	 * Returns all the employee attendances.
	 *
	 * @return the employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeAttendance> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the employee attendances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of employee attendances
	 * @param end the upper bound of the range of employee attendances (not inclusive)
	 * @return the range of employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeAttendance> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the employee attendances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of employee attendances
	 * @param end the upper bound of the range of employee attendances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeAttendance> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EmployeeAttendance> list = (List<EmployeeAttendance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_EMPLOYEEATTENDANCE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_EMPLOYEEATTENDANCE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<EmployeeAttendance>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<EmployeeAttendance>)QueryUtil.list(q,
							getDialect(), start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the employee attendances from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (EmployeeAttendance employeeAttendance : findAll()) {
			remove(employeeAttendance);
		}
	}

	/**
	 * Returns the number of employee attendances.
	 *
	 * @return the number of employee attendances
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_EMPLOYEEATTENDANCE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the employee attendance persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.EmployeeAttendance")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<EmployeeAttendance>> listenersList = new ArrayList<ModelListener<EmployeeAttendance>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<EmployeeAttendance>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EmployeeAttendanceImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_EMPLOYEEATTENDANCE = "SELECT employeeAttendance FROM EmployeeAttendance employeeAttendance";
	private static final String _SQL_COUNT_EMPLOYEEATTENDANCE = "SELECT COUNT(employeeAttendance) FROM EmployeeAttendance employeeAttendance";
	private static final String _ORDER_BY_ENTITY_ALIAS = "employeeAttendance.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EmployeeAttendance exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EmployeeAttendancePersistenceImpl.class);
	private static EmployeeAttendance _nullEmployeeAttendance = new EmployeeAttendanceImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<EmployeeAttendance> toCacheModel() {
				return _nullEmployeeAttendanceCacheModel;
			}
		};

	private static CacheModel<EmployeeAttendance> _nullEmployeeAttendanceCacheModel =
		new CacheModel<EmployeeAttendance>() {
			public EmployeeAttendance toEntityModel() {
				return _nullEmployeeAttendance;
			}
		};
}
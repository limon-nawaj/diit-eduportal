/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import info.diit.portal.model.LeaveDayDetails;
import info.diit.portal.service.base.LeaveDayDetailsLocalServiceBaseImpl;
import info.diit.portal.service.persistence.LeaveDayDetailsUtil;

/**
 * The implementation of the leave day details local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.service.LeaveDayDetailsLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see info.diit.portal.service.base.LeaveDayDetailsLocalServiceBaseImpl
 * @see info.diit.portal.service.LeaveDayDetailsLocalServiceUtil
 */
public class LeaveDayDetailsLocalServiceImpl
	extends LeaveDayDetailsLocalServiceBaseImpl {
	
	public List<LeaveDayDetails> findByLeave(long leave) throws SystemException{
		return LeaveDayDetailsUtil.findByLeaveId(leave);
	}
}
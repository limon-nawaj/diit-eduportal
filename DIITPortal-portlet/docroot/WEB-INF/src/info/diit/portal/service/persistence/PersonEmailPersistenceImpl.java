/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchPersonEmailException;
import info.diit.portal.model.PersonEmail;
import info.diit.portal.model.impl.PersonEmailImpl;
import info.diit.portal.model.impl.PersonEmailModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the person email service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see PersonEmailPersistence
 * @see PersonEmailUtil
 * @generated
 */
public class PersonEmailPersistenceImpl extends BasePersistenceImpl<PersonEmail>
	implements PersonEmailPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PersonEmailUtil} to access the person email persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PersonEmailImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTPERSONEMAILLIST =
		new FinderPath(PersonEmailModelImpl.ENTITY_CACHE_ENABLED,
			PersonEmailModelImpl.FINDER_CACHE_ENABLED, PersonEmailImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByStudentPersonEmailList",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTPERSONEMAILLIST =
		new FinderPath(PersonEmailModelImpl.ENTITY_CACHE_ENABLED,
			PersonEmailModelImpl.FINDER_CACHE_ENABLED, PersonEmailImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByStudentPersonEmailList",
			new String[] { Long.class.getName() },
			PersonEmailModelImpl.STUDENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STUDENTPERSONEMAILLIST = new FinderPath(PersonEmailModelImpl.ENTITY_CACHE_ENABLED,
			PersonEmailModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByStudentPersonEmailList",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PersonEmailModelImpl.ENTITY_CACHE_ENABLED,
			PersonEmailModelImpl.FINDER_CACHE_ENABLED, PersonEmailImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PersonEmailModelImpl.ENTITY_CACHE_ENABLED,
			PersonEmailModelImpl.FINDER_CACHE_ENABLED, PersonEmailImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PersonEmailModelImpl.ENTITY_CACHE_ENABLED,
			PersonEmailModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the person email in the entity cache if it is enabled.
	 *
	 * @param personEmail the person email
	 */
	public void cacheResult(PersonEmail personEmail) {
		EntityCacheUtil.putResult(PersonEmailModelImpl.ENTITY_CACHE_ENABLED,
			PersonEmailImpl.class, personEmail.getPrimaryKey(), personEmail);

		personEmail.resetOriginalValues();
	}

	/**
	 * Caches the person emails in the entity cache if it is enabled.
	 *
	 * @param personEmails the person emails
	 */
	public void cacheResult(List<PersonEmail> personEmails) {
		for (PersonEmail personEmail : personEmails) {
			if (EntityCacheUtil.getResult(
						PersonEmailModelImpl.ENTITY_CACHE_ENABLED,
						PersonEmailImpl.class, personEmail.getPrimaryKey()) == null) {
				cacheResult(personEmail);
			}
			else {
				personEmail.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all person emails.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(PersonEmailImpl.class.getName());
		}

		EntityCacheUtil.clearCache(PersonEmailImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the person email.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(PersonEmail personEmail) {
		EntityCacheUtil.removeResult(PersonEmailModelImpl.ENTITY_CACHE_ENABLED,
			PersonEmailImpl.class, personEmail.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<PersonEmail> personEmails) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (PersonEmail personEmail : personEmails) {
			EntityCacheUtil.removeResult(PersonEmailModelImpl.ENTITY_CACHE_ENABLED,
				PersonEmailImpl.class, personEmail.getPrimaryKey());
		}
	}

	/**
	 * Creates a new person email with the primary key. Does not add the person email to the database.
	 *
	 * @param personEmailId the primary key for the new person email
	 * @return the new person email
	 */
	public PersonEmail create(long personEmailId) {
		PersonEmail personEmail = new PersonEmailImpl();

		personEmail.setNew(true);
		personEmail.setPrimaryKey(personEmailId);

		return personEmail;
	}

	/**
	 * Removes the person email with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param personEmailId the primary key of the person email
	 * @return the person email that was removed
	 * @throws info.diit.portal.NoSuchPersonEmailException if a person email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PersonEmail remove(long personEmailId)
		throws NoSuchPersonEmailException, SystemException {
		return remove(Long.valueOf(personEmailId));
	}

	/**
	 * Removes the person email with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the person email
	 * @return the person email that was removed
	 * @throws info.diit.portal.NoSuchPersonEmailException if a person email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PersonEmail remove(Serializable primaryKey)
		throws NoSuchPersonEmailException, SystemException {
		Session session = null;

		try {
			session = openSession();

			PersonEmail personEmail = (PersonEmail)session.get(PersonEmailImpl.class,
					primaryKey);

			if (personEmail == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchPersonEmailException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(personEmail);
		}
		catch (NoSuchPersonEmailException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected PersonEmail removeImpl(PersonEmail personEmail)
		throws SystemException {
		personEmail = toUnwrappedModel(personEmail);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, personEmail);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(personEmail);

		return personEmail;
	}

	@Override
	public PersonEmail updateImpl(
		info.diit.portal.model.PersonEmail personEmail, boolean merge)
		throws SystemException {
		personEmail = toUnwrappedModel(personEmail);

		boolean isNew = personEmail.isNew();

		PersonEmailModelImpl personEmailModelImpl = (PersonEmailModelImpl)personEmail;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, personEmail, merge);

			personEmail.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !PersonEmailModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((personEmailModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTPERSONEMAILLIST.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(personEmailModelImpl.getOriginalStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTPERSONEMAILLIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTPERSONEMAILLIST,
					args);

				args = new Object[] {
						Long.valueOf(personEmailModelImpl.getStudentId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTPERSONEMAILLIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTPERSONEMAILLIST,
					args);
			}
		}

		EntityCacheUtil.putResult(PersonEmailModelImpl.ENTITY_CACHE_ENABLED,
			PersonEmailImpl.class, personEmail.getPrimaryKey(), personEmail);

		return personEmail;
	}

	protected PersonEmail toUnwrappedModel(PersonEmail personEmail) {
		if (personEmail instanceof PersonEmailImpl) {
			return personEmail;
		}

		PersonEmailImpl personEmailImpl = new PersonEmailImpl();

		personEmailImpl.setNew(personEmail.isNew());
		personEmailImpl.setPrimaryKey(personEmail.getPrimaryKey());

		personEmailImpl.setPersonEmailId(personEmail.getPersonEmailId());
		personEmailImpl.setCompanyId(personEmail.getCompanyId());
		personEmailImpl.setUserId(personEmail.getUserId());
		personEmailImpl.setUserName(personEmail.getUserName());
		personEmailImpl.setCreateDate(personEmail.getCreateDate());
		personEmailImpl.setModifiedDate(personEmail.getModifiedDate());
		personEmailImpl.setOrganizationId(personEmail.getOrganizationId());
		personEmailImpl.setOwnerType(personEmail.getOwnerType());
		personEmailImpl.setPersonEmail(personEmail.getPersonEmail());
		personEmailImpl.setStudentId(personEmail.getStudentId());

		return personEmailImpl;
	}

	/**
	 * Returns the person email with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the person email
	 * @return the person email
	 * @throws com.liferay.portal.NoSuchModelException if a person email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PersonEmail findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the person email with the primary key or throws a {@link info.diit.portal.NoSuchPersonEmailException} if it could not be found.
	 *
	 * @param personEmailId the primary key of the person email
	 * @return the person email
	 * @throws info.diit.portal.NoSuchPersonEmailException if a person email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PersonEmail findByPrimaryKey(long personEmailId)
		throws NoSuchPersonEmailException, SystemException {
		PersonEmail personEmail = fetchByPrimaryKey(personEmailId);

		if (personEmail == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + personEmailId);
			}

			throw new NoSuchPersonEmailException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				personEmailId);
		}

		return personEmail;
	}

	/**
	 * Returns the person email with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the person email
	 * @return the person email, or <code>null</code> if a person email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PersonEmail fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the person email with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param personEmailId the primary key of the person email
	 * @return the person email, or <code>null</code> if a person email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PersonEmail fetchByPrimaryKey(long personEmailId)
		throws SystemException {
		PersonEmail personEmail = (PersonEmail)EntityCacheUtil.getResult(PersonEmailModelImpl.ENTITY_CACHE_ENABLED,
				PersonEmailImpl.class, personEmailId);

		if (personEmail == _nullPersonEmail) {
			return null;
		}

		if (personEmail == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				personEmail = (PersonEmail)session.get(PersonEmailImpl.class,
						Long.valueOf(personEmailId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (personEmail != null) {
					cacheResult(personEmail);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(PersonEmailModelImpl.ENTITY_CACHE_ENABLED,
						PersonEmailImpl.class, personEmailId, _nullPersonEmail);
				}

				closeSession(session);
			}
		}

		return personEmail;
	}

	/**
	 * Returns all the person emails where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the matching person emails
	 * @throws SystemException if a system exception occurred
	 */
	public List<PersonEmail> findByStudentPersonEmailList(long studentId)
		throws SystemException {
		return findByStudentPersonEmailList(studentId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the person emails where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of person emails
	 * @param end the upper bound of the range of person emails (not inclusive)
	 * @return the range of matching person emails
	 * @throws SystemException if a system exception occurred
	 */
	public List<PersonEmail> findByStudentPersonEmailList(long studentId,
		int start, int end) throws SystemException {
		return findByStudentPersonEmailList(studentId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the person emails where studentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param start the lower bound of the range of person emails
	 * @param end the upper bound of the range of person emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching person emails
	 * @throws SystemException if a system exception occurred
	 */
	public List<PersonEmail> findByStudentPersonEmailList(long studentId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTPERSONEMAILLIST;
			finderArgs = new Object[] { studentId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTPERSONEMAILLIST;
			finderArgs = new Object[] { studentId, start, end, orderByComparator };
		}

		List<PersonEmail> list = (List<PersonEmail>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (PersonEmail personEmail : list) {
				if ((studentId != personEmail.getStudentId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_PERSONEMAIL_WHERE);

			query.append(_FINDER_COLUMN_STUDENTPERSONEMAILLIST_STUDENTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				list = (List<PersonEmail>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first person email in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching person email
	 * @throws info.diit.portal.NoSuchPersonEmailException if a matching person email could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PersonEmail findByStudentPersonEmailList_First(long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchPersonEmailException, SystemException {
		PersonEmail personEmail = fetchByStudentPersonEmailList_First(studentId,
				orderByComparator);

		if (personEmail != null) {
			return personEmail;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPersonEmailException(msg.toString());
	}

	/**
	 * Returns the first person email in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching person email, or <code>null</code> if a matching person email could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PersonEmail fetchByStudentPersonEmailList_First(long studentId,
		OrderByComparator orderByComparator) throws SystemException {
		List<PersonEmail> list = findByStudentPersonEmailList(studentId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last person email in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching person email
	 * @throws info.diit.portal.NoSuchPersonEmailException if a matching person email could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PersonEmail findByStudentPersonEmailList_Last(long studentId,
		OrderByComparator orderByComparator)
		throws NoSuchPersonEmailException, SystemException {
		PersonEmail personEmail = fetchByStudentPersonEmailList_Last(studentId,
				orderByComparator);

		if (personEmail != null) {
			return personEmail;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPersonEmailException(msg.toString());
	}

	/**
	 * Returns the last person email in the ordered set where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching person email, or <code>null</code> if a matching person email could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PersonEmail fetchByStudentPersonEmailList_Last(long studentId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByStudentPersonEmailList(studentId);

		List<PersonEmail> list = findByStudentPersonEmailList(studentId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the person emails before and after the current person email in the ordered set where studentId = &#63;.
	 *
	 * @param personEmailId the primary key of the current person email
	 * @param studentId the student ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next person email
	 * @throws info.diit.portal.NoSuchPersonEmailException if a person email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PersonEmail[] findByStudentPersonEmailList_PrevAndNext(
		long personEmailId, long studentId, OrderByComparator orderByComparator)
		throws NoSuchPersonEmailException, SystemException {
		PersonEmail personEmail = findByPrimaryKey(personEmailId);

		Session session = null;

		try {
			session = openSession();

			PersonEmail[] array = new PersonEmailImpl[3];

			array[0] = getByStudentPersonEmailList_PrevAndNext(session,
					personEmail, studentId, orderByComparator, true);

			array[1] = personEmail;

			array[2] = getByStudentPersonEmailList_PrevAndNext(session,
					personEmail, studentId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected PersonEmail getByStudentPersonEmailList_PrevAndNext(
		Session session, PersonEmail personEmail, long studentId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PERSONEMAIL_WHERE);

		query.append(_FINDER_COLUMN_STUDENTPERSONEMAILLIST_STUDENTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(studentId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(personEmail);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<PersonEmail> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the person emails.
	 *
	 * @return the person emails
	 * @throws SystemException if a system exception occurred
	 */
	public List<PersonEmail> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the person emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of person emails
	 * @param end the upper bound of the range of person emails (not inclusive)
	 * @return the range of person emails
	 * @throws SystemException if a system exception occurred
	 */
	public List<PersonEmail> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the person emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of person emails
	 * @param end the upper bound of the range of person emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of person emails
	 * @throws SystemException if a system exception occurred
	 */
	public List<PersonEmail> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<PersonEmail> list = (List<PersonEmail>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PERSONEMAIL);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PERSONEMAIL;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<PersonEmail>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<PersonEmail>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the person emails where studentId = &#63; from the database.
	 *
	 * @param studentId the student ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByStudentPersonEmailList(long studentId)
		throws SystemException {
		for (PersonEmail personEmail : findByStudentPersonEmailList(studentId)) {
			remove(personEmail);
		}
	}

	/**
	 * Removes all the person emails from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (PersonEmail personEmail : findAll()) {
			remove(personEmail);
		}
	}

	/**
	 * Returns the number of person emails where studentId = &#63;.
	 *
	 * @param studentId the student ID
	 * @return the number of matching person emails
	 * @throws SystemException if a system exception occurred
	 */
	public int countByStudentPersonEmailList(long studentId)
		throws SystemException {
		Object[] finderArgs = new Object[] { studentId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_STUDENTPERSONEMAILLIST,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PERSONEMAIL_WHERE);

			query.append(_FINDER_COLUMN_STUDENTPERSONEMAILLIST_STUDENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_STUDENTPERSONEMAILLIST,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of person emails.
	 *
	 * @return the number of person emails
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PERSONEMAIL);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the person email persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.PersonEmail")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<PersonEmail>> listenersList = new ArrayList<ModelListener<PersonEmail>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<PersonEmail>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(PersonEmailImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_PERSONEMAIL = "SELECT personEmail FROM PersonEmail personEmail";
	private static final String _SQL_SELECT_PERSONEMAIL_WHERE = "SELECT personEmail FROM PersonEmail personEmail WHERE ";
	private static final String _SQL_COUNT_PERSONEMAIL = "SELECT COUNT(personEmail) FROM PersonEmail personEmail";
	private static final String _SQL_COUNT_PERSONEMAIL_WHERE = "SELECT COUNT(personEmail) FROM PersonEmail personEmail WHERE ";
	private static final String _FINDER_COLUMN_STUDENTPERSONEMAILLIST_STUDENTID_2 =
		"personEmail.studentId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "personEmail.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PersonEmail exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No PersonEmail exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(PersonEmailPersistenceImpl.class);
	private static PersonEmail _nullPersonEmail = new PersonEmailImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<PersonEmail> toCacheModel() {
				return _nullPersonEmailCacheModel;
			}
		};

	private static CacheModel<PersonEmail> _nullPersonEmailCacheModel = new CacheModel<PersonEmail>() {
			public PersonEmail toEntityModel() {
				return _nullPersonEmail;
			}
		};
}
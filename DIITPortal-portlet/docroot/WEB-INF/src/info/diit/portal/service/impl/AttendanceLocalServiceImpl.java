/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.impl;

import info.diit.portal.NoSuchAttendanceException;
import info.diit.portal.model.Attendance;
import info.diit.portal.service.base.AttendanceLocalServiceBaseImpl;
import info.diit.portal.service.persistence.AttendanceUtil;

import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the attendance local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.service.AttendanceLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see info.diit.portal.service.base.AttendanceLocalServiceBaseImpl
 * @see info.diit.portal.service.AttendanceLocalServiceUtil
 */
public class AttendanceLocalServiceImpl extends AttendanceLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.service.AttendanceLocalServiceUtil} to access the attendance local service.
	 */
	public List<Attendance> findByUserBatchSubject(long userId, long batchId, long subjectId) throws SystemException{
		return AttendanceUtil.findByUserBatchSubject(userId, batchId, subjectId);
	}
	
	public List<Attendance> findByBatch(long batch) throws SystemException{
		return AttendanceUtil.findByBatch(batch);
	}
	
	public Attendance findBySubjectDate(long subject, Date date) throws NoSuchAttendanceException, SystemException{
		return AttendanceUtil.findBySubjectDate(subject, date);
	}
	
	public List<Attendance> findByBatchDate(long batch, Date date) throws SystemException{
		return AttendanceUtil.findByBatchDate(batch, date);
	}
	
	public List<Attendance> findByCompany(long companyId) throws SystemException{
		return AttendanceUtil.findByCompany(companyId);
	}
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchPaymentException;
import info.diit.portal.model.Payment;
import info.diit.portal.model.impl.PaymentImpl;
import info.diit.portal.model.impl.PaymentModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the payment service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see PaymentPersistence
 * @see PaymentUtil
 * @generated
 */
public class PaymentPersistenceImpl extends BasePersistenceImpl<Payment>
	implements PaymentPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PaymentUtil} to access the payment persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PaymentImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTBATCH =
		new FinderPath(PaymentModelImpl.ENTITY_CACHE_ENABLED,
			PaymentModelImpl.FINDER_CACHE_ENABLED, PaymentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByStudentBatch",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH =
		new FinderPath(PaymentModelImpl.ENTITY_CACHE_ENABLED,
			PaymentModelImpl.FINDER_CACHE_ENABLED, PaymentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStudentBatch",
			new String[] { Long.class.getName(), Long.class.getName() },
			PaymentModelImpl.STUDENTID_COLUMN_BITMASK |
			PaymentModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STUDENTBATCH = new FinderPath(PaymentModelImpl.ENTITY_CACHE_ENABLED,
			PaymentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByStudentBatch",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PaymentModelImpl.ENTITY_CACHE_ENABLED,
			PaymentModelImpl.FINDER_CACHE_ENABLED, PaymentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PaymentModelImpl.ENTITY_CACHE_ENABLED,
			PaymentModelImpl.FINDER_CACHE_ENABLED, PaymentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PaymentModelImpl.ENTITY_CACHE_ENABLED,
			PaymentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the payment in the entity cache if it is enabled.
	 *
	 * @param payment the payment
	 */
	public void cacheResult(Payment payment) {
		EntityCacheUtil.putResult(PaymentModelImpl.ENTITY_CACHE_ENABLED,
			PaymentImpl.class, payment.getPrimaryKey(), payment);

		payment.resetOriginalValues();
	}

	/**
	 * Caches the payments in the entity cache if it is enabled.
	 *
	 * @param payments the payments
	 */
	public void cacheResult(List<Payment> payments) {
		for (Payment payment : payments) {
			if (EntityCacheUtil.getResult(
						PaymentModelImpl.ENTITY_CACHE_ENABLED,
						PaymentImpl.class, payment.getPrimaryKey()) == null) {
				cacheResult(payment);
			}
			else {
				payment.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all payments.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(PaymentImpl.class.getName());
		}

		EntityCacheUtil.clearCache(PaymentImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the payment.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Payment payment) {
		EntityCacheUtil.removeResult(PaymentModelImpl.ENTITY_CACHE_ENABLED,
			PaymentImpl.class, payment.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Payment> payments) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Payment payment : payments) {
			EntityCacheUtil.removeResult(PaymentModelImpl.ENTITY_CACHE_ENABLED,
				PaymentImpl.class, payment.getPrimaryKey());
		}
	}

	/**
	 * Creates a new payment with the primary key. Does not add the payment to the database.
	 *
	 * @param paymentId the primary key for the new payment
	 * @return the new payment
	 */
	public Payment create(long paymentId) {
		Payment payment = new PaymentImpl();

		payment.setNew(true);
		payment.setPrimaryKey(paymentId);

		return payment;
	}

	/**
	 * Removes the payment with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param paymentId the primary key of the payment
	 * @return the payment that was removed
	 * @throws info.diit.portal.NoSuchPaymentException if a payment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Payment remove(long paymentId)
		throws NoSuchPaymentException, SystemException {
		return remove(Long.valueOf(paymentId));
	}

	/**
	 * Removes the payment with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the payment
	 * @return the payment that was removed
	 * @throws info.diit.portal.NoSuchPaymentException if a payment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Payment remove(Serializable primaryKey)
		throws NoSuchPaymentException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Payment payment = (Payment)session.get(PaymentImpl.class, primaryKey);

			if (payment == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchPaymentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(payment);
		}
		catch (NoSuchPaymentException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Payment removeImpl(Payment payment) throws SystemException {
		payment = toUnwrappedModel(payment);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, payment);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(payment);

		return payment;
	}

	@Override
	public Payment updateImpl(info.diit.portal.model.Payment payment,
		boolean merge) throws SystemException {
		payment = toUnwrappedModel(payment);

		boolean isNew = payment.isNew();

		PaymentModelImpl paymentModelImpl = (PaymentModelImpl)payment;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, payment, merge);

			payment.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !PaymentModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((paymentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(paymentModelImpl.getOriginalStudentId()),
						Long.valueOf(paymentModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH,
					args);

				args = new Object[] {
						Long.valueOf(paymentModelImpl.getStudentId()),
						Long.valueOf(paymentModelImpl.getBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH,
					args);
			}
		}

		EntityCacheUtil.putResult(PaymentModelImpl.ENTITY_CACHE_ENABLED,
			PaymentImpl.class, payment.getPrimaryKey(), payment);

		return payment;
	}

	protected Payment toUnwrappedModel(Payment payment) {
		if (payment instanceof PaymentImpl) {
			return payment;
		}

		PaymentImpl paymentImpl = new PaymentImpl();

		paymentImpl.setNew(payment.isNew());
		paymentImpl.setPrimaryKey(payment.getPrimaryKey());

		paymentImpl.setPaymentId(payment.getPaymentId());
		paymentImpl.setCompanyId(payment.getCompanyId());
		paymentImpl.setUserId(payment.getUserId());
		paymentImpl.setCreateDate(payment.getCreateDate());
		paymentImpl.setModifiedDate(payment.getModifiedDate());
		paymentImpl.setStudentId(payment.getStudentId());
		paymentImpl.setBatchId(payment.getBatchId());
		paymentImpl.setFeeTypeId(payment.getFeeTypeId());
		paymentImpl.setAmount(payment.getAmount());
		paymentImpl.setPaymentDate(payment.getPaymentDate());

		return paymentImpl;
	}

	/**
	 * Returns the payment with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the payment
	 * @return the payment
	 * @throws com.liferay.portal.NoSuchModelException if a payment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Payment findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the payment with the primary key or throws a {@link info.diit.portal.NoSuchPaymentException} if it could not be found.
	 *
	 * @param paymentId the primary key of the payment
	 * @return the payment
	 * @throws info.diit.portal.NoSuchPaymentException if a payment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Payment findByPrimaryKey(long paymentId)
		throws NoSuchPaymentException, SystemException {
		Payment payment = fetchByPrimaryKey(paymentId);

		if (payment == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + paymentId);
			}

			throw new NoSuchPaymentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				paymentId);
		}

		return payment;
	}

	/**
	 * Returns the payment with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the payment
	 * @return the payment, or <code>null</code> if a payment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Payment fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the payment with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param paymentId the primary key of the payment
	 * @return the payment, or <code>null</code> if a payment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Payment fetchByPrimaryKey(long paymentId) throws SystemException {
		Payment payment = (Payment)EntityCacheUtil.getResult(PaymentModelImpl.ENTITY_CACHE_ENABLED,
				PaymentImpl.class, paymentId);

		if (payment == _nullPayment) {
			return null;
		}

		if (payment == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				payment = (Payment)session.get(PaymentImpl.class,
						Long.valueOf(paymentId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (payment != null) {
					cacheResult(payment);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(PaymentModelImpl.ENTITY_CACHE_ENABLED,
						PaymentImpl.class, paymentId, _nullPayment);
				}

				closeSession(session);
			}
		}

		return payment;
	}

	/**
	 * Returns all the payments where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @return the matching payments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Payment> findByStudentBatch(long studentId, long batchId)
		throws SystemException {
		return findByStudentBatch(studentId, batchId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the payments where studentId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of payments
	 * @param end the upper bound of the range of payments (not inclusive)
	 * @return the range of matching payments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Payment> findByStudentBatch(long studentId, long batchId,
		int start, int end) throws SystemException {
		return findByStudentBatch(studentId, batchId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the payments where studentId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of payments
	 * @param end the upper bound of the range of payments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching payments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Payment> findByStudentBatch(long studentId, long batchId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STUDENTBATCH;
			finderArgs = new Object[] { studentId, batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STUDENTBATCH;
			finderArgs = new Object[] {
					studentId, batchId,
					
					start, end, orderByComparator
				};
		}

		List<Payment> list = (List<Payment>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Payment payment : list) {
				if ((studentId != payment.getStudentId()) ||
						(batchId != payment.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PAYMENT_WHERE);

			query.append(_FINDER_COLUMN_STUDENTBATCH_STUDENTID_2);

			query.append(_FINDER_COLUMN_STUDENTBATCH_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(PaymentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				qPos.add(batchId);

				list = (List<Payment>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first payment in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching payment
	 * @throws info.diit.portal.NoSuchPaymentException if a matching payment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Payment findByStudentBatch_First(long studentId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchPaymentException, SystemException {
		Payment payment = fetchByStudentBatch_First(studentId, batchId,
				orderByComparator);

		if (payment != null) {
			return payment;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPaymentException(msg.toString());
	}

	/**
	 * Returns the first payment in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching payment, or <code>null</code> if a matching payment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Payment fetchByStudentBatch_First(long studentId, long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Payment> list = findByStudentBatch(studentId, batchId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last payment in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching payment
	 * @throws info.diit.portal.NoSuchPaymentException if a matching payment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Payment findByStudentBatch_Last(long studentId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchPaymentException, SystemException {
		Payment payment = fetchByStudentBatch_Last(studentId, batchId,
				orderByComparator);

		if (payment != null) {
			return payment;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("studentId=");
		msg.append(studentId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPaymentException(msg.toString());
	}

	/**
	 * Returns the last payment in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching payment, or <code>null</code> if a matching payment could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Payment fetchByStudentBatch_Last(long studentId, long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByStudentBatch(studentId, batchId);

		List<Payment> list = findByStudentBatch(studentId, batchId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the payments before and after the current payment in the ordered set where studentId = &#63; and batchId = &#63;.
	 *
	 * @param paymentId the primary key of the current payment
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next payment
	 * @throws info.diit.portal.NoSuchPaymentException if a payment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Payment[] findByStudentBatch_PrevAndNext(long paymentId,
		long studentId, long batchId, OrderByComparator orderByComparator)
		throws NoSuchPaymentException, SystemException {
		Payment payment = findByPrimaryKey(paymentId);

		Session session = null;

		try {
			session = openSession();

			Payment[] array = new PaymentImpl[3];

			array[0] = getByStudentBatch_PrevAndNext(session, payment,
					studentId, batchId, orderByComparator, true);

			array[1] = payment;

			array[2] = getByStudentBatch_PrevAndNext(session, payment,
					studentId, batchId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Payment getByStudentBatch_PrevAndNext(Session session,
		Payment payment, long studentId, long batchId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PAYMENT_WHERE);

		query.append(_FINDER_COLUMN_STUDENTBATCH_STUDENTID_2);

		query.append(_FINDER_COLUMN_STUDENTBATCH_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(PaymentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(studentId);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(payment);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Payment> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the payments.
	 *
	 * @return the payments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Payment> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the payments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of payments
	 * @param end the upper bound of the range of payments (not inclusive)
	 * @return the range of payments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Payment> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the payments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of payments
	 * @param end the upper bound of the range of payments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of payments
	 * @throws SystemException if a system exception occurred
	 */
	public List<Payment> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Payment> list = (List<Payment>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PAYMENT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PAYMENT.concat(PaymentModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Payment>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Payment>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the payments where studentId = &#63; and batchId = &#63; from the database.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByStudentBatch(long studentId, long batchId)
		throws SystemException {
		for (Payment payment : findByStudentBatch(studentId, batchId)) {
			remove(payment);
		}
	}

	/**
	 * Removes all the payments from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Payment payment : findAll()) {
			remove(payment);
		}
	}

	/**
	 * Returns the number of payments where studentId = &#63; and batchId = &#63;.
	 *
	 * @param studentId the student ID
	 * @param batchId the batch ID
	 * @return the number of matching payments
	 * @throws SystemException if a system exception occurred
	 */
	public int countByStudentBatch(long studentId, long batchId)
		throws SystemException {
		Object[] finderArgs = new Object[] { studentId, batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PAYMENT_WHERE);

			query.append(_FINDER_COLUMN_STUDENTBATCH_STUDENTID_2);

			query.append(_FINDER_COLUMN_STUDENTBATCH_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(studentId);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_STUDENTBATCH,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of payments.
	 *
	 * @return the number of payments
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PAYMENT);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the payment persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.Payment")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Payment>> listenersList = new ArrayList<ModelListener<Payment>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Payment>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(PaymentImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_PAYMENT = "SELECT payment FROM Payment payment";
	private static final String _SQL_SELECT_PAYMENT_WHERE = "SELECT payment FROM Payment payment WHERE ";
	private static final String _SQL_COUNT_PAYMENT = "SELECT COUNT(payment) FROM Payment payment";
	private static final String _SQL_COUNT_PAYMENT_WHERE = "SELECT COUNT(payment) FROM Payment payment WHERE ";
	private static final String _FINDER_COLUMN_STUDENTBATCH_STUDENTID_2 = "payment.studentId = ? AND ";
	private static final String _FINDER_COLUMN_STUDENTBATCH_BATCHID_2 = "payment.batchId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "payment.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Payment exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Payment exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(PaymentPersistenceImpl.class);
	private static Payment _nullPayment = new PaymentImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Payment> toCacheModel() {
				return _nullPaymentCacheModel;
			}
		};

	private static CacheModel<Payment> _nullPaymentCacheModel = new CacheModel<Payment>() {
			public Payment toEntityModel() {
				return _nullPayment;
			}
		};
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchLeaveReceiverException;
import info.diit.portal.model.LeaveReceiver;
import info.diit.portal.model.impl.LeaveReceiverImpl;
import info.diit.portal.model.impl.LeaveReceiverModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the leave receiver service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see LeaveReceiverPersistence
 * @see LeaveReceiverUtil
 * @generated
 */
public class LeaveReceiverPersistenceImpl extends BasePersistenceImpl<LeaveReceiver>
	implements LeaveReceiverPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LeaveReceiverUtil} to access the leave receiver persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LeaveReceiverImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATIONID =
		new FinderPath(LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
			LeaveReceiverModelImpl.FINDER_CACHE_ENABLED,
			LeaveReceiverImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByOrganizationId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID =
		new FinderPath(LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
			LeaveReceiverModelImpl.FINDER_CACHE_ENABLED,
			LeaveReceiverImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByOrganizationId", new String[] { Long.class.getName() },
			LeaveReceiverModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATIONID = new FinderPath(LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
			LeaveReceiverModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOrganizationId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FETCH_BY_EMPLOYEEID = new FinderPath(LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
			LeaveReceiverModelImpl.FINDER_CACHE_ENABLED,
			LeaveReceiverImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByEmployeeId", new String[] { Long.class.getName() },
			LeaveReceiverModelImpl.EMPLOYEEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMPLOYEEID = new FinderPath(LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
			LeaveReceiverModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEmployeeId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
			LeaveReceiverModelImpl.FINDER_CACHE_ENABLED,
			LeaveReceiverImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
			LeaveReceiverModelImpl.FINDER_CACHE_ENABLED,
			LeaveReceiverImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
			LeaveReceiverModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the leave receiver in the entity cache if it is enabled.
	 *
	 * @param leaveReceiver the leave receiver
	 */
	public void cacheResult(LeaveReceiver leaveReceiver) {
		EntityCacheUtil.putResult(LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
			LeaveReceiverImpl.class, leaveReceiver.getPrimaryKey(),
			leaveReceiver);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEEID,
			new Object[] { Long.valueOf(leaveReceiver.getEmployeeId()) },
			leaveReceiver);

		leaveReceiver.resetOriginalValues();
	}

	/**
	 * Caches the leave receivers in the entity cache if it is enabled.
	 *
	 * @param leaveReceivers the leave receivers
	 */
	public void cacheResult(List<LeaveReceiver> leaveReceivers) {
		for (LeaveReceiver leaveReceiver : leaveReceivers) {
			if (EntityCacheUtil.getResult(
						LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
						LeaveReceiverImpl.class, leaveReceiver.getPrimaryKey()) == null) {
				cacheResult(leaveReceiver);
			}
			else {
				leaveReceiver.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all leave receivers.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(LeaveReceiverImpl.class.getName());
		}

		EntityCacheUtil.clearCache(LeaveReceiverImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the leave receiver.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LeaveReceiver leaveReceiver) {
		EntityCacheUtil.removeResult(LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
			LeaveReceiverImpl.class, leaveReceiver.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(leaveReceiver);
	}

	@Override
	public void clearCache(List<LeaveReceiver> leaveReceivers) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LeaveReceiver leaveReceiver : leaveReceivers) {
			EntityCacheUtil.removeResult(LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
				LeaveReceiverImpl.class, leaveReceiver.getPrimaryKey());

			clearUniqueFindersCache(leaveReceiver);
		}
	}

	protected void clearUniqueFindersCache(LeaveReceiver leaveReceiver) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EMPLOYEEID,
			new Object[] { Long.valueOf(leaveReceiver.getEmployeeId()) });
	}

	/**
	 * Creates a new leave receiver with the primary key. Does not add the leave receiver to the database.
	 *
	 * @param leaveReceiverId the primary key for the new leave receiver
	 * @return the new leave receiver
	 */
	public LeaveReceiver create(long leaveReceiverId) {
		LeaveReceiver leaveReceiver = new LeaveReceiverImpl();

		leaveReceiver.setNew(true);
		leaveReceiver.setPrimaryKey(leaveReceiverId);

		return leaveReceiver;
	}

	/**
	 * Removes the leave receiver with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param leaveReceiverId the primary key of the leave receiver
	 * @return the leave receiver that was removed
	 * @throws info.diit.portal.NoSuchLeaveReceiverException if a leave receiver with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveReceiver remove(long leaveReceiverId)
		throws NoSuchLeaveReceiverException, SystemException {
		return remove(Long.valueOf(leaveReceiverId));
	}

	/**
	 * Removes the leave receiver with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the leave receiver
	 * @return the leave receiver that was removed
	 * @throws info.diit.portal.NoSuchLeaveReceiverException if a leave receiver with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LeaveReceiver remove(Serializable primaryKey)
		throws NoSuchLeaveReceiverException, SystemException {
		Session session = null;

		try {
			session = openSession();

			LeaveReceiver leaveReceiver = (LeaveReceiver)session.get(LeaveReceiverImpl.class,
					primaryKey);

			if (leaveReceiver == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLeaveReceiverException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(leaveReceiver);
		}
		catch (NoSuchLeaveReceiverException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LeaveReceiver removeImpl(LeaveReceiver leaveReceiver)
		throws SystemException {
		leaveReceiver = toUnwrappedModel(leaveReceiver);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, leaveReceiver);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(leaveReceiver);

		return leaveReceiver;
	}

	@Override
	public LeaveReceiver updateImpl(
		info.diit.portal.model.LeaveReceiver leaveReceiver, boolean merge)
		throws SystemException {
		leaveReceiver = toUnwrappedModel(leaveReceiver);

		boolean isNew = leaveReceiver.isNew();

		LeaveReceiverModelImpl leaveReceiverModelImpl = (LeaveReceiverModelImpl)leaveReceiver;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, leaveReceiver, merge);

			leaveReceiver.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !LeaveReceiverModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((leaveReceiverModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(leaveReceiverModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATIONID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID,
					args);

				args = new Object[] {
						Long.valueOf(leaveReceiverModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATIONID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID,
					args);
			}
		}

		EntityCacheUtil.putResult(LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
			LeaveReceiverImpl.class, leaveReceiver.getPrimaryKey(),
			leaveReceiver);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEEID,
				new Object[] { Long.valueOf(leaveReceiver.getEmployeeId()) },
				leaveReceiver);
		}
		else {
			if ((leaveReceiverModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_EMPLOYEEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(leaveReceiverModelImpl.getOriginalEmployeeId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEEID,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EMPLOYEEID,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEEID,
					new Object[] { Long.valueOf(leaveReceiver.getEmployeeId()) },
					leaveReceiver);
			}
		}

		return leaveReceiver;
	}

	protected LeaveReceiver toUnwrappedModel(LeaveReceiver leaveReceiver) {
		if (leaveReceiver instanceof LeaveReceiverImpl) {
			return leaveReceiver;
		}

		LeaveReceiverImpl leaveReceiverImpl = new LeaveReceiverImpl();

		leaveReceiverImpl.setNew(leaveReceiver.isNew());
		leaveReceiverImpl.setPrimaryKey(leaveReceiver.getPrimaryKey());

		leaveReceiverImpl.setLeaveReceiverId(leaveReceiver.getLeaveReceiverId());
		leaveReceiverImpl.setOrganizationId(leaveReceiver.getOrganizationId());
		leaveReceiverImpl.setCompanyId(leaveReceiver.getCompanyId());
		leaveReceiverImpl.setEmployeeId(leaveReceiver.getEmployeeId());

		return leaveReceiverImpl;
	}

	/**
	 * Returns the leave receiver with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the leave receiver
	 * @return the leave receiver
	 * @throws com.liferay.portal.NoSuchModelException if a leave receiver with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LeaveReceiver findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the leave receiver with the primary key or throws a {@link info.diit.portal.NoSuchLeaveReceiverException} if it could not be found.
	 *
	 * @param leaveReceiverId the primary key of the leave receiver
	 * @return the leave receiver
	 * @throws info.diit.portal.NoSuchLeaveReceiverException if a leave receiver with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveReceiver findByPrimaryKey(long leaveReceiverId)
		throws NoSuchLeaveReceiverException, SystemException {
		LeaveReceiver leaveReceiver = fetchByPrimaryKey(leaveReceiverId);

		if (leaveReceiver == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + leaveReceiverId);
			}

			throw new NoSuchLeaveReceiverException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				leaveReceiverId);
		}

		return leaveReceiver;
	}

	/**
	 * Returns the leave receiver with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the leave receiver
	 * @return the leave receiver, or <code>null</code> if a leave receiver with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LeaveReceiver fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the leave receiver with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param leaveReceiverId the primary key of the leave receiver
	 * @return the leave receiver, or <code>null</code> if a leave receiver with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveReceiver fetchByPrimaryKey(long leaveReceiverId)
		throws SystemException {
		LeaveReceiver leaveReceiver = (LeaveReceiver)EntityCacheUtil.getResult(LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
				LeaveReceiverImpl.class, leaveReceiverId);

		if (leaveReceiver == _nullLeaveReceiver) {
			return null;
		}

		if (leaveReceiver == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				leaveReceiver = (LeaveReceiver)session.get(LeaveReceiverImpl.class,
						Long.valueOf(leaveReceiverId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (leaveReceiver != null) {
					cacheResult(leaveReceiver);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(LeaveReceiverModelImpl.ENTITY_CACHE_ENABLED,
						LeaveReceiverImpl.class, leaveReceiverId,
						_nullLeaveReceiver);
				}

				closeSession(session);
			}
		}

		return leaveReceiver;
	}

	/**
	 * Returns all the leave receivers where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching leave receivers
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveReceiver> findByOrganizationId(long organizationId)
		throws SystemException {
		return findByOrganizationId(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the leave receivers where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of leave receivers
	 * @param end the upper bound of the range of leave receivers (not inclusive)
	 * @return the range of matching leave receivers
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveReceiver> findByOrganizationId(long organizationId,
		int start, int end) throws SystemException {
		return findByOrganizationId(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the leave receivers where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of leave receivers
	 * @param end the upper bound of the range of leave receivers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching leave receivers
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveReceiver> findByOrganizationId(long organizationId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATIONID;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATIONID;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<LeaveReceiver> list = (List<LeaveReceiver>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (LeaveReceiver leaveReceiver : list) {
				if ((organizationId != leaveReceiver.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_LEAVERECEIVER_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATIONID_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				list = (List<LeaveReceiver>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first leave receiver in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching leave receiver
	 * @throws info.diit.portal.NoSuchLeaveReceiverException if a matching leave receiver could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveReceiver findByOrganizationId_First(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveReceiverException, SystemException {
		LeaveReceiver leaveReceiver = fetchByOrganizationId_First(organizationId,
				orderByComparator);

		if (leaveReceiver != null) {
			return leaveReceiver;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLeaveReceiverException(msg.toString());
	}

	/**
	 * Returns the first leave receiver in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching leave receiver, or <code>null</code> if a matching leave receiver could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveReceiver fetchByOrganizationId_First(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<LeaveReceiver> list = findByOrganizationId(organizationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last leave receiver in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching leave receiver
	 * @throws info.diit.portal.NoSuchLeaveReceiverException if a matching leave receiver could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveReceiver findByOrganizationId_Last(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveReceiverException, SystemException {
		LeaveReceiver leaveReceiver = fetchByOrganizationId_Last(organizationId,
				orderByComparator);

		if (leaveReceiver != null) {
			return leaveReceiver;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLeaveReceiverException(msg.toString());
	}

	/**
	 * Returns the last leave receiver in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching leave receiver, or <code>null</code> if a matching leave receiver could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveReceiver fetchByOrganizationId_Last(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByOrganizationId(organizationId);

		List<LeaveReceiver> list = findByOrganizationId(organizationId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the leave receivers before and after the current leave receiver in the ordered set where organizationId = &#63;.
	 *
	 * @param leaveReceiverId the primary key of the current leave receiver
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next leave receiver
	 * @throws info.diit.portal.NoSuchLeaveReceiverException if a leave receiver with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveReceiver[] findByOrganizationId_PrevAndNext(
		long leaveReceiverId, long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchLeaveReceiverException, SystemException {
		LeaveReceiver leaveReceiver = findByPrimaryKey(leaveReceiverId);

		Session session = null;

		try {
			session = openSession();

			LeaveReceiver[] array = new LeaveReceiverImpl[3];

			array[0] = getByOrganizationId_PrevAndNext(session, leaveReceiver,
					organizationId, orderByComparator, true);

			array[1] = leaveReceiver;

			array[2] = getByOrganizationId_PrevAndNext(session, leaveReceiver,
					organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LeaveReceiver getByOrganizationId_PrevAndNext(Session session,
		LeaveReceiver leaveReceiver, long organizationId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LEAVERECEIVER_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATIONID_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(leaveReceiver);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LeaveReceiver> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns the leave receiver where employeeId = &#63; or throws a {@link info.diit.portal.NoSuchLeaveReceiverException} if it could not be found.
	 *
	 * @param employeeId the employee ID
	 * @return the matching leave receiver
	 * @throws info.diit.portal.NoSuchLeaveReceiverException if a matching leave receiver could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveReceiver findByEmployeeId(long employeeId)
		throws NoSuchLeaveReceiverException, SystemException {
		LeaveReceiver leaveReceiver = fetchByEmployeeId(employeeId);

		if (leaveReceiver == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("employeeId=");
			msg.append(employeeId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchLeaveReceiverException(msg.toString());
		}

		return leaveReceiver;
	}

	/**
	 * Returns the leave receiver where employeeId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param employeeId the employee ID
	 * @return the matching leave receiver, or <code>null</code> if a matching leave receiver could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveReceiver fetchByEmployeeId(long employeeId)
		throws SystemException {
		return fetchByEmployeeId(employeeId, true);
	}

	/**
	 * Returns the leave receiver where employeeId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param employeeId the employee ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching leave receiver, or <code>null</code> if a matching leave receiver could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveReceiver fetchByEmployeeId(long employeeId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { employeeId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_EMPLOYEEID,
					finderArgs, this);
		}

		if (result instanceof LeaveReceiver) {
			LeaveReceiver leaveReceiver = (LeaveReceiver)result;

			if ((employeeId != leaveReceiver.getEmployeeId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_SELECT_LEAVERECEIVER_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEEID_EMPLOYEEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				List<LeaveReceiver> list = q.list();

				result = list;

				LeaveReceiver leaveReceiver = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEEID,
						finderArgs, list);
				}
				else {
					leaveReceiver = list.get(0);

					cacheResult(leaveReceiver);

					if ((leaveReceiver.getEmployeeId() != employeeId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_EMPLOYEEID,
							finderArgs, leaveReceiver);
					}
				}

				return leaveReceiver;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_EMPLOYEEID,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (LeaveReceiver)result;
			}
		}
	}

	/**
	 * Returns all the leave receivers.
	 *
	 * @return the leave receivers
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveReceiver> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the leave receivers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of leave receivers
	 * @param end the upper bound of the range of leave receivers (not inclusive)
	 * @return the range of leave receivers
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveReceiver> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the leave receivers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of leave receivers
	 * @param end the upper bound of the range of leave receivers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of leave receivers
	 * @throws SystemException if a system exception occurred
	 */
	public List<LeaveReceiver> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LeaveReceiver> list = (List<LeaveReceiver>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LEAVERECEIVER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LEAVERECEIVER;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<LeaveReceiver>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<LeaveReceiver>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the leave receivers where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByOrganizationId(long organizationId)
		throws SystemException {
		for (LeaveReceiver leaveReceiver : findByOrganizationId(organizationId)) {
			remove(leaveReceiver);
		}
	}

	/**
	 * Removes the leave receiver where employeeId = &#63; from the database.
	 *
	 * @param employeeId the employee ID
	 * @return the leave receiver that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public LeaveReceiver removeByEmployeeId(long employeeId)
		throws NoSuchLeaveReceiverException, SystemException {
		LeaveReceiver leaveReceiver = findByEmployeeId(employeeId);

		return remove(leaveReceiver);
	}

	/**
	 * Removes all the leave receivers from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (LeaveReceiver leaveReceiver : findAll()) {
			remove(leaveReceiver);
		}
	}

	/**
	 * Returns the number of leave receivers where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching leave receivers
	 * @throws SystemException if a system exception occurred
	 */
	public int countByOrganizationId(long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATIONID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LEAVERECEIVER_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATIONID_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATIONID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of leave receivers where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @return the number of matching leave receivers
	 * @throws SystemException if a system exception occurred
	 */
	public int countByEmployeeId(long employeeId) throws SystemException {
		Object[] finderArgs = new Object[] { employeeId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_EMPLOYEEID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LEAVERECEIVER_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEEID_EMPLOYEEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EMPLOYEEID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of leave receivers.
	 *
	 * @return the number of leave receivers
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LEAVERECEIVER);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the leave receiver persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.LeaveReceiver")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<LeaveReceiver>> listenersList = new ArrayList<ModelListener<LeaveReceiver>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<LeaveReceiver>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(LeaveReceiverImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_LEAVERECEIVER = "SELECT leaveReceiver FROM LeaveReceiver leaveReceiver";
	private static final String _SQL_SELECT_LEAVERECEIVER_WHERE = "SELECT leaveReceiver FROM LeaveReceiver leaveReceiver WHERE ";
	private static final String _SQL_COUNT_LEAVERECEIVER = "SELECT COUNT(leaveReceiver) FROM LeaveReceiver leaveReceiver";
	private static final String _SQL_COUNT_LEAVERECEIVER_WHERE = "SELECT COUNT(leaveReceiver) FROM LeaveReceiver leaveReceiver WHERE ";
	private static final String _FINDER_COLUMN_ORGANIZATIONID_ORGANIZATIONID_2 = "leaveReceiver.organizationId = ?";
	private static final String _FINDER_COLUMN_EMPLOYEEID_EMPLOYEEID_2 = "leaveReceiver.employeeId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "leaveReceiver.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LeaveReceiver exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No LeaveReceiver exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(LeaveReceiverPersistenceImpl.class);
	private static LeaveReceiver _nullLeaveReceiver = new LeaveReceiverImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<LeaveReceiver> toCacheModel() {
				return _nullLeaveReceiverCacheModel;
			}
		};

	private static CacheModel<LeaveReceiver> _nullLeaveReceiverCacheModel = new CacheModel<LeaveReceiver>() {
			public LeaveReceiver toEntityModel() {
				return _nullLeaveReceiver;
			}
		};
}
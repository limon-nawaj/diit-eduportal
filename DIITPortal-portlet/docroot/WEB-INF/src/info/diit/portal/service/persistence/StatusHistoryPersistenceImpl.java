/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchStatusHistoryException;
import info.diit.portal.model.StatusHistory;
import info.diit.portal.model.impl.StatusHistoryImpl;
import info.diit.portal.model.impl.StatusHistoryModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the status history service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see StatusHistoryPersistence
 * @see StatusHistoryUtil
 * @generated
 */
public class StatusHistoryPersistenceImpl extends BasePersistenceImpl<StatusHistory>
	implements StatusHistoryPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link StatusHistoryUtil} to access the status history persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = StatusHistoryImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(StatusHistoryModelImpl.ENTITY_CACHE_ENABLED,
			StatusHistoryModelImpl.FINDER_CACHE_ENABLED,
			StatusHistoryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(StatusHistoryModelImpl.ENTITY_CACHE_ENABLED,
			StatusHistoryModelImpl.FINDER_CACHE_ENABLED,
			StatusHistoryImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(StatusHistoryModelImpl.ENTITY_CACHE_ENABLED,
			StatusHistoryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the status history in the entity cache if it is enabled.
	 *
	 * @param statusHistory the status history
	 */
	public void cacheResult(StatusHistory statusHistory) {
		EntityCacheUtil.putResult(StatusHistoryModelImpl.ENTITY_CACHE_ENABLED,
			StatusHistoryImpl.class, statusHistory.getPrimaryKey(),
			statusHistory);

		statusHistory.resetOriginalValues();
	}

	/**
	 * Caches the status histories in the entity cache if it is enabled.
	 *
	 * @param statusHistories the status histories
	 */
	public void cacheResult(List<StatusHistory> statusHistories) {
		for (StatusHistory statusHistory : statusHistories) {
			if (EntityCacheUtil.getResult(
						StatusHistoryModelImpl.ENTITY_CACHE_ENABLED,
						StatusHistoryImpl.class, statusHistory.getPrimaryKey()) == null) {
				cacheResult(statusHistory);
			}
			else {
				statusHistory.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all status histories.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(StatusHistoryImpl.class.getName());
		}

		EntityCacheUtil.clearCache(StatusHistoryImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the status history.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(StatusHistory statusHistory) {
		EntityCacheUtil.removeResult(StatusHistoryModelImpl.ENTITY_CACHE_ENABLED,
			StatusHistoryImpl.class, statusHistory.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<StatusHistory> statusHistories) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (StatusHistory statusHistory : statusHistories) {
			EntityCacheUtil.removeResult(StatusHistoryModelImpl.ENTITY_CACHE_ENABLED,
				StatusHistoryImpl.class, statusHistory.getPrimaryKey());
		}
	}

	/**
	 * Creates a new status history with the primary key. Does not add the status history to the database.
	 *
	 * @param statusHistoryId the primary key for the new status history
	 * @return the new status history
	 */
	public StatusHistory create(long statusHistoryId) {
		StatusHistory statusHistory = new StatusHistoryImpl();

		statusHistory.setNew(true);
		statusHistory.setPrimaryKey(statusHistoryId);

		return statusHistory;
	}

	/**
	 * Removes the status history with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param statusHistoryId the primary key of the status history
	 * @return the status history that was removed
	 * @throws info.diit.portal.NoSuchStatusHistoryException if a status history with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StatusHistory remove(long statusHistoryId)
		throws NoSuchStatusHistoryException, SystemException {
		return remove(Long.valueOf(statusHistoryId));
	}

	/**
	 * Removes the status history with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the status history
	 * @return the status history that was removed
	 * @throws info.diit.portal.NoSuchStatusHistoryException if a status history with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StatusHistory remove(Serializable primaryKey)
		throws NoSuchStatusHistoryException, SystemException {
		Session session = null;

		try {
			session = openSession();

			StatusHistory statusHistory = (StatusHistory)session.get(StatusHistoryImpl.class,
					primaryKey);

			if (statusHistory == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchStatusHistoryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(statusHistory);
		}
		catch (NoSuchStatusHistoryException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected StatusHistory removeImpl(StatusHistory statusHistory)
		throws SystemException {
		statusHistory = toUnwrappedModel(statusHistory);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, statusHistory);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(statusHistory);

		return statusHistory;
	}

	@Override
	public StatusHistory updateImpl(
		info.diit.portal.model.StatusHistory statusHistory, boolean merge)
		throws SystemException {
		statusHistory = toUnwrappedModel(statusHistory);

		boolean isNew = statusHistory.isNew();

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, statusHistory, merge);

			statusHistory.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(StatusHistoryModelImpl.ENTITY_CACHE_ENABLED,
			StatusHistoryImpl.class, statusHistory.getPrimaryKey(),
			statusHistory);

		return statusHistory;
	}

	protected StatusHistory toUnwrappedModel(StatusHistory statusHistory) {
		if (statusHistory instanceof StatusHistoryImpl) {
			return statusHistory;
		}

		StatusHistoryImpl statusHistoryImpl = new StatusHistoryImpl();

		statusHistoryImpl.setNew(statusHistory.isNew());
		statusHistoryImpl.setPrimaryKey(statusHistory.getPrimaryKey());

		statusHistoryImpl.setStatusHistoryId(statusHistory.getStatusHistoryId());
		statusHistoryImpl.setCompanyId(statusHistory.getCompanyId());
		statusHistoryImpl.setUserId(statusHistory.getUserId());
		statusHistoryImpl.setUserName(statusHistory.getUserName());
		statusHistoryImpl.setCreateDate(statusHistory.getCreateDate());
		statusHistoryImpl.setModifiedDate(statusHistory.getModifiedDate());
		statusHistoryImpl.setOrganizationId(statusHistory.getOrganizationId());
		statusHistoryImpl.setBatchStudentId(statusHistory.getBatchStudentId());
		statusHistoryImpl.setFromStatus(statusHistory.getFromStatus());
		statusHistoryImpl.setToStatus(statusHistory.getToStatus());

		return statusHistoryImpl;
	}

	/**
	 * Returns the status history with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the status history
	 * @return the status history
	 * @throws com.liferay.portal.NoSuchModelException if a status history with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StatusHistory findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the status history with the primary key or throws a {@link info.diit.portal.NoSuchStatusHistoryException} if it could not be found.
	 *
	 * @param statusHistoryId the primary key of the status history
	 * @return the status history
	 * @throws info.diit.portal.NoSuchStatusHistoryException if a status history with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StatusHistory findByPrimaryKey(long statusHistoryId)
		throws NoSuchStatusHistoryException, SystemException {
		StatusHistory statusHistory = fetchByPrimaryKey(statusHistoryId);

		if (statusHistory == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + statusHistoryId);
			}

			throw new NoSuchStatusHistoryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				statusHistoryId);
		}

		return statusHistory;
	}

	/**
	 * Returns the status history with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the status history
	 * @return the status history, or <code>null</code> if a status history with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public StatusHistory fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the status history with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param statusHistoryId the primary key of the status history
	 * @return the status history, or <code>null</code> if a status history with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public StatusHistory fetchByPrimaryKey(long statusHistoryId)
		throws SystemException {
		StatusHistory statusHistory = (StatusHistory)EntityCacheUtil.getResult(StatusHistoryModelImpl.ENTITY_CACHE_ENABLED,
				StatusHistoryImpl.class, statusHistoryId);

		if (statusHistory == _nullStatusHistory) {
			return null;
		}

		if (statusHistory == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				statusHistory = (StatusHistory)session.get(StatusHistoryImpl.class,
						Long.valueOf(statusHistoryId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (statusHistory != null) {
					cacheResult(statusHistory);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(StatusHistoryModelImpl.ENTITY_CACHE_ENABLED,
						StatusHistoryImpl.class, statusHistoryId,
						_nullStatusHistory);
				}

				closeSession(session);
			}
		}

		return statusHistory;
	}

	/**
	 * Returns all the status histories.
	 *
	 * @return the status histories
	 * @throws SystemException if a system exception occurred
	 */
	public List<StatusHistory> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the status histories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of status histories
	 * @param end the upper bound of the range of status histories (not inclusive)
	 * @return the range of status histories
	 * @throws SystemException if a system exception occurred
	 */
	public List<StatusHistory> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the status histories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of status histories
	 * @param end the upper bound of the range of status histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of status histories
	 * @throws SystemException if a system exception occurred
	 */
	public List<StatusHistory> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<StatusHistory> list = (List<StatusHistory>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_STATUSHISTORY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_STATUSHISTORY;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<StatusHistory>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<StatusHistory>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the status histories from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (StatusHistory statusHistory : findAll()) {
			remove(statusHistory);
		}
	}

	/**
	 * Returns the number of status histories.
	 *
	 * @return the number of status histories
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_STATUSHISTORY);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the status history persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.StatusHistory")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<StatusHistory>> listenersList = new ArrayList<ModelListener<StatusHistory>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<StatusHistory>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(StatusHistoryImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_STATUSHISTORY = "SELECT statusHistory FROM StatusHistory statusHistory";
	private static final String _SQL_COUNT_STATUSHISTORY = "SELECT COUNT(statusHistory) FROM StatusHistory statusHistory";
	private static final String _ORDER_BY_ENTITY_ALIAS = "statusHistory.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No StatusHistory exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(StatusHistoryPersistenceImpl.class);
	private static StatusHistory _nullStatusHistory = new StatusHistoryImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<StatusHistory> toCacheModel() {
				return _nullStatusHistoryCacheModel;
			}
		};

	private static CacheModel<StatusHistory> _nullStatusHistoryCacheModel = new CacheModel<StatusHistory>() {
			public StatusHistory toEntityModel() {
				return _nullStatusHistory;
			}
		};
}
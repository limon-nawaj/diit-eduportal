/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.impl;

import info.diit.portal.NoSuchChapterException;
import info.diit.portal.model.Chapter;
import info.diit.portal.service.base.ChapterLocalServiceBaseImpl;
import info.diit.portal.service.persistence.ChapterUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the chapter local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, 
 * rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.service.
 * ChapterLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on 
 * the propagated JAAS credentials because this service can only be accessed from within the 
 * same VM.
 * </p>
 *
 * @author mohammad
 * @see info.diit.portal.service.base.ChapterLocalServiceBaseImpl
 * @see info.diit.portal.service.ChapterLocalServiceUtil
 */
public class ChapterLocalServiceImpl extends ChapterLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.service.ChapterLocalServiceUtil} to access the chapter local service.
	 */
	public List<Chapter> findByCompany(long companyId) throws SystemException{
		return ChapterUtil.findByCompany(companyId);
	}
	
	public List<Chapter> findBySubject(long subjectId) throws SystemException{
		return ChapterUtil.findBySubject(subjectId);
	}
	
	public Chapter findByChapterSequence(long chapterId, long sequence) throws NoSuchChapterException, SystemException{
		return ChapterUtil.findByChapterSequence(chapterId, sequence);
	}
}
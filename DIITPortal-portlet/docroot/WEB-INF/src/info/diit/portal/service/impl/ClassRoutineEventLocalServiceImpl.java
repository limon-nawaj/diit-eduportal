/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.impl;

import info.diit.portal.NoSuchClassRoutineEventException;
import info.diit.portal.model.ClassRoutineEvent;
import info.diit.portal.model.ClassRoutineEventBatch;
import info.diit.portal.service.ClassRoutineEventBatchLocalServiceUtil;
import info.diit.portal.service.ClassRoutineEventLocalServiceUtil;
import info.diit.portal.service.base.ClassRoutineEventLocalServiceBaseImpl;
import info.diit.portal.service.persistence.ClassRoutineEventBatchUtil;
import info.diit.portal.service.persistence.ClassRoutineEventUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the class routine event local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.service.ClassRoutineEventLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see info.diit.portal.service.base.ClassRoutineEventLocalServiceBaseImpl
 * @see info.diit.portal.service.ClassRoutineEventLocalServiceUtil
 */
public class ClassRoutineEventLocalServiceImpl
	extends ClassRoutineEventLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.service.ClassRoutineEventLocalServiceUtil} to access the class routine event local service.
	 */
	public boolean saveClassEventWithBatches(ClassRoutineEvent classRoutineEvent, List<ClassRoutineEventBatch> eventBatchs) throws SystemException{
		boolean res = false;
		ClassRoutineEvent saveRoutineEvent = ClassRoutineEventLocalServiceUtil.addClassRoutineEvent(classRoutineEvent);
		if(saveRoutineEvent!=null){
			for (ClassRoutineEventBatch classRoutineEventBatch : eventBatchs) {
				classRoutineEventBatch.setClassRoutineEventId(saveRoutineEvent.getClassRoutineEventId());
				ClassRoutineEventBatchLocalServiceUtil.addClassRoutineEventBatch(classRoutineEventBatch);
				res = true;
			}
		}
		
		return res;
	}
	
	public ClassRoutineEvent findEventsBySubjectId (long subjectId, long eventId) throws SystemException{
		return ClassRoutineEventUtil.fetchBySubjectId(subjectId, eventId);
	}
	
	public void deleteEvent(long eventId) throws SystemException, NoSuchClassRoutineEventException{
		ClassRoutineEventBatchUtil.removeByClassRoutineEventId(eventId);
		ClassRoutineEventUtil.remove(eventId);
	}
	
	public List<ClassRoutineEvent> findByRoom(long roomId) throws SystemException{
		return ClassRoutineEventUtil.findByRoom(roomId);
	}
	
	public List<ClassRoutineEvent> findByCompany(long companyId) throws SystemException{
		return ClassRoutineEventUtil.findByCompany(companyId);
	}
	
	public List<ClassRoutineEvent> findBySubujectDay(long subjectId, int day) throws SystemException{
		return ClassRoutineEventUtil.findBySubjectDate(subjectId, day);
	}
}
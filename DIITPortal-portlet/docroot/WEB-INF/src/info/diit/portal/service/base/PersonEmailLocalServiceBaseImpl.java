/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.base;

import com.liferay.counter.service.CounterLocalService;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.bean.IdentifiableBean;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.model.PersistedModel;
import com.liferay.portal.service.BaseLocalServiceImpl;
import com.liferay.portal.service.PersistedModelLocalServiceRegistryUtil;
import com.liferay.portal.service.ResourceLocalService;
import com.liferay.portal.service.ResourceService;
import com.liferay.portal.service.UserLocalService;
import com.liferay.portal.service.UserService;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;

import info.diit.portal.model.PersonEmail;
import info.diit.portal.service.AcademicRecordLocalService;
import info.diit.portal.service.AssessmentLocalService;
import info.diit.portal.service.AssessmentStudentLocalService;
import info.diit.portal.service.AssessmentTypeLocalService;
import info.diit.portal.service.AttendanceLocalService;
import info.diit.portal.service.AttendanceTopicLocalService;
import info.diit.portal.service.BatchFeeLocalService;
import info.diit.portal.service.BatchLocalService;
import info.diit.portal.service.BatchPaymentScheduleLocalService;
import info.diit.portal.service.BatchStudentLocalService;
import info.diit.portal.service.BatchSubjectLocalService;
import info.diit.portal.service.BatchSubjectService;
import info.diit.portal.service.BatchTeacherLocalService;
import info.diit.portal.service.ChapterLocalService;
import info.diit.portal.service.ClassRoutineEventBatchLocalService;
import info.diit.portal.service.ClassRoutineEventLocalService;
import info.diit.portal.service.CommentsLocalService;
import info.diit.portal.service.CommentsService;
import info.diit.portal.service.CommunicationRecordLocalService;
import info.diit.portal.service.CommunicationStudentRecordLocalService;
import info.diit.portal.service.CounselingCourseInterestLocalService;
import info.diit.portal.service.CounselingCourseInterestService;
import info.diit.portal.service.CounselingLocalService;
import info.diit.portal.service.CourseFeeLocalService;
import info.diit.portal.service.CourseLocalService;
import info.diit.portal.service.CourseOrganizationLocalService;
import info.diit.portal.service.CourseSessionLocalService;
import info.diit.portal.service.CourseSubjectLocalService;
import info.diit.portal.service.DailyWorkLocalService;
import info.diit.portal.service.DesignationLocalService;
import info.diit.portal.service.EmployeeAttendanceLocalService;
import info.diit.portal.service.EmployeeEmailLocalService;
import info.diit.portal.service.EmployeeLocalService;
import info.diit.portal.service.EmployeeMobileLocalService;
import info.diit.portal.service.EmployeeRoleLocalService;
import info.diit.portal.service.EmployeeTimeScheduleLocalService;
import info.diit.portal.service.ExperianceLocalService;
import info.diit.portal.service.FeeTypeLocalService;
import info.diit.portal.service.LeaveCategoryLocalService;
import info.diit.portal.service.LeaveDayDetailsLocalService;
import info.diit.portal.service.LeaveLocalService;
import info.diit.portal.service.LeaveReceiverLocalService;
import info.diit.portal.service.LessonAssessmentLocalService;
import info.diit.portal.service.LessonPlanLocalService;
import info.diit.portal.service.LessonTopicLocalService;
import info.diit.portal.service.PaymentLocalService;
import info.diit.portal.service.PersonEmailLocalService;
import info.diit.portal.service.PhoneNumberLocalService;
import info.diit.portal.service.RoomLocalService;
import info.diit.portal.service.StatusHistoryLocalService;
import info.diit.portal.service.StudentAttendanceLocalService;
import info.diit.portal.service.StudentDiscountLocalService;
import info.diit.portal.service.StudentDocumentLocalService;
import info.diit.portal.service.StudentFeeLocalService;
import info.diit.portal.service.StudentLocalService;
import info.diit.portal.service.StudentPaymentScheduleLocalService;
import info.diit.portal.service.SubjectLessonLocalService;
import info.diit.portal.service.SubjectLocalService;
import info.diit.portal.service.TaskDesignationLocalService;
import info.diit.portal.service.TaskLocalService;
import info.diit.portal.service.TopicLocalService;
import info.diit.portal.service.persistence.AcademicRecordPersistence;
import info.diit.portal.service.persistence.AssessmentPersistence;
import info.diit.portal.service.persistence.AssessmentStudentPersistence;
import info.diit.portal.service.persistence.AssessmentTypePersistence;
import info.diit.portal.service.persistence.AttendancePersistence;
import info.diit.portal.service.persistence.AttendanceTopicPersistence;
import info.diit.portal.service.persistence.BatchFeePersistence;
import info.diit.portal.service.persistence.BatchPaymentSchedulePersistence;
import info.diit.portal.service.persistence.BatchPersistence;
import info.diit.portal.service.persistence.BatchStudentPersistence;
import info.diit.portal.service.persistence.BatchSubjectPersistence;
import info.diit.portal.service.persistence.BatchTeacherPersistence;
import info.diit.portal.service.persistence.ChapterPersistence;
import info.diit.portal.service.persistence.ClassRoutineEventBatchPersistence;
import info.diit.portal.service.persistence.ClassRoutineEventPersistence;
import info.diit.portal.service.persistence.CommentsPersistence;
import info.diit.portal.service.persistence.CommunicationRecordPersistence;
import info.diit.portal.service.persistence.CommunicationStudentRecordPersistence;
import info.diit.portal.service.persistence.CounselingCourseInterestPersistence;
import info.diit.portal.service.persistence.CounselingPersistence;
import info.diit.portal.service.persistence.CourseFeePersistence;
import info.diit.portal.service.persistence.CourseOrganizationPersistence;
import info.diit.portal.service.persistence.CoursePersistence;
import info.diit.portal.service.persistence.CourseSessionPersistence;
import info.diit.portal.service.persistence.CourseSubjectPersistence;
import info.diit.portal.service.persistence.DailyWorkPersistence;
import info.diit.portal.service.persistence.DesignationPersistence;
import info.diit.portal.service.persistence.EmployeeAttendancePersistence;
import info.diit.portal.service.persistence.EmployeeEmailPersistence;
import info.diit.portal.service.persistence.EmployeeMobilePersistence;
import info.diit.portal.service.persistence.EmployeePersistence;
import info.diit.portal.service.persistence.EmployeeRolePersistence;
import info.diit.portal.service.persistence.EmployeeTimeSchedulePersistence;
import info.diit.portal.service.persistence.ExperiancePersistence;
import info.diit.portal.service.persistence.FeeTypePersistence;
import info.diit.portal.service.persistence.LeaveCategoryPersistence;
import info.diit.portal.service.persistence.LeaveDayDetailsPersistence;
import info.diit.portal.service.persistence.LeavePersistence;
import info.diit.portal.service.persistence.LeaveReceiverPersistence;
import info.diit.portal.service.persistence.LessonAssessmentPersistence;
import info.diit.portal.service.persistence.LessonPlanPersistence;
import info.diit.portal.service.persistence.LessonTopicPersistence;
import info.diit.portal.service.persistence.PaymentPersistence;
import info.diit.portal.service.persistence.PersonEmailPersistence;
import info.diit.portal.service.persistence.PhoneNumberPersistence;
import info.diit.portal.service.persistence.RoomPersistence;
import info.diit.portal.service.persistence.StatusHistoryPersistence;
import info.diit.portal.service.persistence.StudentAttendancePersistence;
import info.diit.portal.service.persistence.StudentDiscountPersistence;
import info.diit.portal.service.persistence.StudentDocumentPersistence;
import info.diit.portal.service.persistence.StudentFeePersistence;
import info.diit.portal.service.persistence.StudentPaymentSchedulePersistence;
import info.diit.portal.service.persistence.StudentPersistence;
import info.diit.portal.service.persistence.SubjectLessonPersistence;
import info.diit.portal.service.persistence.SubjectPersistence;
import info.diit.portal.service.persistence.TaskDesignationPersistence;
import info.diit.portal.service.persistence.TaskPersistence;
import info.diit.portal.service.persistence.TopicPersistence;

import java.io.Serializable;

import java.util.List;

import javax.sql.DataSource;

/**
 * The base implementation of the person email local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link info.diit.portal.service.impl.PersonEmailLocalServiceImpl}.
 * </p>
 *
 * @author mohammad
 * @see info.diit.portal.service.impl.PersonEmailLocalServiceImpl
 * @see info.diit.portal.service.PersonEmailLocalServiceUtil
 * @generated
 */
public abstract class PersonEmailLocalServiceBaseImpl
	extends BaseLocalServiceImpl implements PersonEmailLocalService,
		IdentifiableBean {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link info.diit.portal.service.PersonEmailLocalServiceUtil} to access the person email local service.
	 */

	/**
	 * Adds the person email to the database. Also notifies the appropriate model listeners.
	 *
	 * @param personEmail the person email
	 * @return the person email that was added
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public PersonEmail addPersonEmail(PersonEmail personEmail)
		throws SystemException {
		personEmail.setNew(true);

		return personEmailPersistence.update(personEmail, false);
	}

	/**
	 * Creates a new person email with the primary key. Does not add the person email to the database.
	 *
	 * @param personEmailId the primary key for the new person email
	 * @return the new person email
	 */
	public PersonEmail createPersonEmail(long personEmailId) {
		return personEmailPersistence.create(personEmailId);
	}

	/**
	 * Deletes the person email with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param personEmailId the primary key of the person email
	 * @return the person email that was removed
	 * @throws PortalException if a person email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.DELETE)
	public PersonEmail deletePersonEmail(long personEmailId)
		throws PortalException, SystemException {
		return personEmailPersistence.remove(personEmailId);
	}

	/**
	 * Deletes the person email from the database. Also notifies the appropriate model listeners.
	 *
	 * @param personEmail the person email
	 * @return the person email that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.DELETE)
	public PersonEmail deletePersonEmail(PersonEmail personEmail)
		throws SystemException {
		return personEmailPersistence.remove(personEmail);
	}

	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(PersonEmail.class,
			clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return personEmailPersistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return personEmailPersistence.findWithDynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return personEmailPersistence.findWithDynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	public long dynamicQueryCount(DynamicQuery dynamicQuery)
		throws SystemException {
		return personEmailPersistence.countWithDynamicQuery(dynamicQuery);
	}

	public PersonEmail fetchPersonEmail(long personEmailId)
		throws SystemException {
		return personEmailPersistence.fetchByPrimaryKey(personEmailId);
	}

	/**
	 * Returns the person email with the primary key.
	 *
	 * @param personEmailId the primary key of the person email
	 * @return the person email
	 * @throws PortalException if a person email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public PersonEmail getPersonEmail(long personEmailId)
		throws PortalException, SystemException {
		return personEmailPersistence.findByPrimaryKey(personEmailId);
	}

	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException, SystemException {
		return personEmailPersistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns a range of all the person emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of person emails
	 * @param end the upper bound of the range of person emails (not inclusive)
	 * @return the range of person emails
	 * @throws SystemException if a system exception occurred
	 */
	public List<PersonEmail> getPersonEmails(int start, int end)
		throws SystemException {
		return personEmailPersistence.findAll(start, end);
	}

	/**
	 * Returns the number of person emails.
	 *
	 * @return the number of person emails
	 * @throws SystemException if a system exception occurred
	 */
	public int getPersonEmailsCount() throws SystemException {
		return personEmailPersistence.countAll();
	}

	/**
	 * Updates the person email in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param personEmail the person email
	 * @return the person email that was updated
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public PersonEmail updatePersonEmail(PersonEmail personEmail)
		throws SystemException {
		return updatePersonEmail(personEmail, true);
	}

	/**
	 * Updates the person email in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param personEmail the person email
	 * @param merge whether to merge the person email with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	 * @return the person email that was updated
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	public PersonEmail updatePersonEmail(PersonEmail personEmail, boolean merge)
		throws SystemException {
		personEmail.setNew(false);

		return personEmailPersistence.update(personEmail, merge);
	}

	/**
	 * Returns the academic record local service.
	 *
	 * @return the academic record local service
	 */
	public AcademicRecordLocalService getAcademicRecordLocalService() {
		return academicRecordLocalService;
	}

	/**
	 * Sets the academic record local service.
	 *
	 * @param academicRecordLocalService the academic record local service
	 */
	public void setAcademicRecordLocalService(
		AcademicRecordLocalService academicRecordLocalService) {
		this.academicRecordLocalService = academicRecordLocalService;
	}

	/**
	 * Returns the academic record persistence.
	 *
	 * @return the academic record persistence
	 */
	public AcademicRecordPersistence getAcademicRecordPersistence() {
		return academicRecordPersistence;
	}

	/**
	 * Sets the academic record persistence.
	 *
	 * @param academicRecordPersistence the academic record persistence
	 */
	public void setAcademicRecordPersistence(
		AcademicRecordPersistence academicRecordPersistence) {
		this.academicRecordPersistence = academicRecordPersistence;
	}

	/**
	 * Returns the assessment local service.
	 *
	 * @return the assessment local service
	 */
	public AssessmentLocalService getAssessmentLocalService() {
		return assessmentLocalService;
	}

	/**
	 * Sets the assessment local service.
	 *
	 * @param assessmentLocalService the assessment local service
	 */
	public void setAssessmentLocalService(
		AssessmentLocalService assessmentLocalService) {
		this.assessmentLocalService = assessmentLocalService;
	}

	/**
	 * Returns the assessment persistence.
	 *
	 * @return the assessment persistence
	 */
	public AssessmentPersistence getAssessmentPersistence() {
		return assessmentPersistence;
	}

	/**
	 * Sets the assessment persistence.
	 *
	 * @param assessmentPersistence the assessment persistence
	 */
	public void setAssessmentPersistence(
		AssessmentPersistence assessmentPersistence) {
		this.assessmentPersistence = assessmentPersistence;
	}

	/**
	 * Returns the assessment student local service.
	 *
	 * @return the assessment student local service
	 */
	public AssessmentStudentLocalService getAssessmentStudentLocalService() {
		return assessmentStudentLocalService;
	}

	/**
	 * Sets the assessment student local service.
	 *
	 * @param assessmentStudentLocalService the assessment student local service
	 */
	public void setAssessmentStudentLocalService(
		AssessmentStudentLocalService assessmentStudentLocalService) {
		this.assessmentStudentLocalService = assessmentStudentLocalService;
	}

	/**
	 * Returns the assessment student persistence.
	 *
	 * @return the assessment student persistence
	 */
	public AssessmentStudentPersistence getAssessmentStudentPersistence() {
		return assessmentStudentPersistence;
	}

	/**
	 * Sets the assessment student persistence.
	 *
	 * @param assessmentStudentPersistence the assessment student persistence
	 */
	public void setAssessmentStudentPersistence(
		AssessmentStudentPersistence assessmentStudentPersistence) {
		this.assessmentStudentPersistence = assessmentStudentPersistence;
	}

	/**
	 * Returns the assessment type local service.
	 *
	 * @return the assessment type local service
	 */
	public AssessmentTypeLocalService getAssessmentTypeLocalService() {
		return assessmentTypeLocalService;
	}

	/**
	 * Sets the assessment type local service.
	 *
	 * @param assessmentTypeLocalService the assessment type local service
	 */
	public void setAssessmentTypeLocalService(
		AssessmentTypeLocalService assessmentTypeLocalService) {
		this.assessmentTypeLocalService = assessmentTypeLocalService;
	}

	/**
	 * Returns the assessment type persistence.
	 *
	 * @return the assessment type persistence
	 */
	public AssessmentTypePersistence getAssessmentTypePersistence() {
		return assessmentTypePersistence;
	}

	/**
	 * Sets the assessment type persistence.
	 *
	 * @param assessmentTypePersistence the assessment type persistence
	 */
	public void setAssessmentTypePersistence(
		AssessmentTypePersistence assessmentTypePersistence) {
		this.assessmentTypePersistence = assessmentTypePersistence;
	}

	/**
	 * Returns the attendance local service.
	 *
	 * @return the attendance local service
	 */
	public AttendanceLocalService getAttendanceLocalService() {
		return attendanceLocalService;
	}

	/**
	 * Sets the attendance local service.
	 *
	 * @param attendanceLocalService the attendance local service
	 */
	public void setAttendanceLocalService(
		AttendanceLocalService attendanceLocalService) {
		this.attendanceLocalService = attendanceLocalService;
	}

	/**
	 * Returns the attendance persistence.
	 *
	 * @return the attendance persistence
	 */
	public AttendancePersistence getAttendancePersistence() {
		return attendancePersistence;
	}

	/**
	 * Sets the attendance persistence.
	 *
	 * @param attendancePersistence the attendance persistence
	 */
	public void setAttendancePersistence(
		AttendancePersistence attendancePersistence) {
		this.attendancePersistence = attendancePersistence;
	}

	/**
	 * Returns the attendance topic local service.
	 *
	 * @return the attendance topic local service
	 */
	public AttendanceTopicLocalService getAttendanceTopicLocalService() {
		return attendanceTopicLocalService;
	}

	/**
	 * Sets the attendance topic local service.
	 *
	 * @param attendanceTopicLocalService the attendance topic local service
	 */
	public void setAttendanceTopicLocalService(
		AttendanceTopicLocalService attendanceTopicLocalService) {
		this.attendanceTopicLocalService = attendanceTopicLocalService;
	}

	/**
	 * Returns the attendance topic persistence.
	 *
	 * @return the attendance topic persistence
	 */
	public AttendanceTopicPersistence getAttendanceTopicPersistence() {
		return attendanceTopicPersistence;
	}

	/**
	 * Sets the attendance topic persistence.
	 *
	 * @param attendanceTopicPersistence the attendance topic persistence
	 */
	public void setAttendanceTopicPersistence(
		AttendanceTopicPersistence attendanceTopicPersistence) {
		this.attendanceTopicPersistence = attendanceTopicPersistence;
	}

	/**
	 * Returns the batch local service.
	 *
	 * @return the batch local service
	 */
	public BatchLocalService getBatchLocalService() {
		return batchLocalService;
	}

	/**
	 * Sets the batch local service.
	 *
	 * @param batchLocalService the batch local service
	 */
	public void setBatchLocalService(BatchLocalService batchLocalService) {
		this.batchLocalService = batchLocalService;
	}

	/**
	 * Returns the batch persistence.
	 *
	 * @return the batch persistence
	 */
	public BatchPersistence getBatchPersistence() {
		return batchPersistence;
	}

	/**
	 * Sets the batch persistence.
	 *
	 * @param batchPersistence the batch persistence
	 */
	public void setBatchPersistence(BatchPersistence batchPersistence) {
		this.batchPersistence = batchPersistence;
	}

	/**
	 * Returns the batch fee local service.
	 *
	 * @return the batch fee local service
	 */
	public BatchFeeLocalService getBatchFeeLocalService() {
		return batchFeeLocalService;
	}

	/**
	 * Sets the batch fee local service.
	 *
	 * @param batchFeeLocalService the batch fee local service
	 */
	public void setBatchFeeLocalService(
		BatchFeeLocalService batchFeeLocalService) {
		this.batchFeeLocalService = batchFeeLocalService;
	}

	/**
	 * Returns the batch fee persistence.
	 *
	 * @return the batch fee persistence
	 */
	public BatchFeePersistence getBatchFeePersistence() {
		return batchFeePersistence;
	}

	/**
	 * Sets the batch fee persistence.
	 *
	 * @param batchFeePersistence the batch fee persistence
	 */
	public void setBatchFeePersistence(BatchFeePersistence batchFeePersistence) {
		this.batchFeePersistence = batchFeePersistence;
	}

	/**
	 * Returns the batch payment schedule local service.
	 *
	 * @return the batch payment schedule local service
	 */
	public BatchPaymentScheduleLocalService getBatchPaymentScheduleLocalService() {
		return batchPaymentScheduleLocalService;
	}

	/**
	 * Sets the batch payment schedule local service.
	 *
	 * @param batchPaymentScheduleLocalService the batch payment schedule local service
	 */
	public void setBatchPaymentScheduleLocalService(
		BatchPaymentScheduleLocalService batchPaymentScheduleLocalService) {
		this.batchPaymentScheduleLocalService = batchPaymentScheduleLocalService;
	}

	/**
	 * Returns the batch payment schedule persistence.
	 *
	 * @return the batch payment schedule persistence
	 */
	public BatchPaymentSchedulePersistence getBatchPaymentSchedulePersistence() {
		return batchPaymentSchedulePersistence;
	}

	/**
	 * Sets the batch payment schedule persistence.
	 *
	 * @param batchPaymentSchedulePersistence the batch payment schedule persistence
	 */
	public void setBatchPaymentSchedulePersistence(
		BatchPaymentSchedulePersistence batchPaymentSchedulePersistence) {
		this.batchPaymentSchedulePersistence = batchPaymentSchedulePersistence;
	}

	/**
	 * Returns the batch student local service.
	 *
	 * @return the batch student local service
	 */
	public BatchStudentLocalService getBatchStudentLocalService() {
		return batchStudentLocalService;
	}

	/**
	 * Sets the batch student local service.
	 *
	 * @param batchStudentLocalService the batch student local service
	 */
	public void setBatchStudentLocalService(
		BatchStudentLocalService batchStudentLocalService) {
		this.batchStudentLocalService = batchStudentLocalService;
	}

	/**
	 * Returns the batch student persistence.
	 *
	 * @return the batch student persistence
	 */
	public BatchStudentPersistence getBatchStudentPersistence() {
		return batchStudentPersistence;
	}

	/**
	 * Sets the batch student persistence.
	 *
	 * @param batchStudentPersistence the batch student persistence
	 */
	public void setBatchStudentPersistence(
		BatchStudentPersistence batchStudentPersistence) {
		this.batchStudentPersistence = batchStudentPersistence;
	}

	/**
	 * Returns the batch subject local service.
	 *
	 * @return the batch subject local service
	 */
	public BatchSubjectLocalService getBatchSubjectLocalService() {
		return batchSubjectLocalService;
	}

	/**
	 * Sets the batch subject local service.
	 *
	 * @param batchSubjectLocalService the batch subject local service
	 */
	public void setBatchSubjectLocalService(
		BatchSubjectLocalService batchSubjectLocalService) {
		this.batchSubjectLocalService = batchSubjectLocalService;
	}

	/**
	 * Returns the batch subject remote service.
	 *
	 * @return the batch subject remote service
	 */
	public BatchSubjectService getBatchSubjectService() {
		return batchSubjectService;
	}

	/**
	 * Sets the batch subject remote service.
	 *
	 * @param batchSubjectService the batch subject remote service
	 */
	public void setBatchSubjectService(BatchSubjectService batchSubjectService) {
		this.batchSubjectService = batchSubjectService;
	}

	/**
	 * Returns the batch subject persistence.
	 *
	 * @return the batch subject persistence
	 */
	public BatchSubjectPersistence getBatchSubjectPersistence() {
		return batchSubjectPersistence;
	}

	/**
	 * Sets the batch subject persistence.
	 *
	 * @param batchSubjectPersistence the batch subject persistence
	 */
	public void setBatchSubjectPersistence(
		BatchSubjectPersistence batchSubjectPersistence) {
		this.batchSubjectPersistence = batchSubjectPersistence;
	}

	/**
	 * Returns the batch teacher local service.
	 *
	 * @return the batch teacher local service
	 */
	public BatchTeacherLocalService getBatchTeacherLocalService() {
		return batchTeacherLocalService;
	}

	/**
	 * Sets the batch teacher local service.
	 *
	 * @param batchTeacherLocalService the batch teacher local service
	 */
	public void setBatchTeacherLocalService(
		BatchTeacherLocalService batchTeacherLocalService) {
		this.batchTeacherLocalService = batchTeacherLocalService;
	}

	/**
	 * Returns the batch teacher persistence.
	 *
	 * @return the batch teacher persistence
	 */
	public BatchTeacherPersistence getBatchTeacherPersistence() {
		return batchTeacherPersistence;
	}

	/**
	 * Sets the batch teacher persistence.
	 *
	 * @param batchTeacherPersistence the batch teacher persistence
	 */
	public void setBatchTeacherPersistence(
		BatchTeacherPersistence batchTeacherPersistence) {
		this.batchTeacherPersistence = batchTeacherPersistence;
	}

	/**
	 * Returns the chapter local service.
	 *
	 * @return the chapter local service
	 */
	public ChapterLocalService getChapterLocalService() {
		return chapterLocalService;
	}

	/**
	 * Sets the chapter local service.
	 *
	 * @param chapterLocalService the chapter local service
	 */
	public void setChapterLocalService(ChapterLocalService chapterLocalService) {
		this.chapterLocalService = chapterLocalService;
	}

	/**
	 * Returns the chapter persistence.
	 *
	 * @return the chapter persistence
	 */
	public ChapterPersistence getChapterPersistence() {
		return chapterPersistence;
	}

	/**
	 * Sets the chapter persistence.
	 *
	 * @param chapterPersistence the chapter persistence
	 */
	public void setChapterPersistence(ChapterPersistence chapterPersistence) {
		this.chapterPersistence = chapterPersistence;
	}

	/**
	 * Returns the class routine event local service.
	 *
	 * @return the class routine event local service
	 */
	public ClassRoutineEventLocalService getClassRoutineEventLocalService() {
		return classRoutineEventLocalService;
	}

	/**
	 * Sets the class routine event local service.
	 *
	 * @param classRoutineEventLocalService the class routine event local service
	 */
	public void setClassRoutineEventLocalService(
		ClassRoutineEventLocalService classRoutineEventLocalService) {
		this.classRoutineEventLocalService = classRoutineEventLocalService;
	}

	/**
	 * Returns the class routine event persistence.
	 *
	 * @return the class routine event persistence
	 */
	public ClassRoutineEventPersistence getClassRoutineEventPersistence() {
		return classRoutineEventPersistence;
	}

	/**
	 * Sets the class routine event persistence.
	 *
	 * @param classRoutineEventPersistence the class routine event persistence
	 */
	public void setClassRoutineEventPersistence(
		ClassRoutineEventPersistence classRoutineEventPersistence) {
		this.classRoutineEventPersistence = classRoutineEventPersistence;
	}

	/**
	 * Returns the class routine event batch local service.
	 *
	 * @return the class routine event batch local service
	 */
	public ClassRoutineEventBatchLocalService getClassRoutineEventBatchLocalService() {
		return classRoutineEventBatchLocalService;
	}

	/**
	 * Sets the class routine event batch local service.
	 *
	 * @param classRoutineEventBatchLocalService the class routine event batch local service
	 */
	public void setClassRoutineEventBatchLocalService(
		ClassRoutineEventBatchLocalService classRoutineEventBatchLocalService) {
		this.classRoutineEventBatchLocalService = classRoutineEventBatchLocalService;
	}

	/**
	 * Returns the class routine event batch persistence.
	 *
	 * @return the class routine event batch persistence
	 */
	public ClassRoutineEventBatchPersistence getClassRoutineEventBatchPersistence() {
		return classRoutineEventBatchPersistence;
	}

	/**
	 * Sets the class routine event batch persistence.
	 *
	 * @param classRoutineEventBatchPersistence the class routine event batch persistence
	 */
	public void setClassRoutineEventBatchPersistence(
		ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence) {
		this.classRoutineEventBatchPersistence = classRoutineEventBatchPersistence;
	}

	/**
	 * Returns the comments local service.
	 *
	 * @return the comments local service
	 */
	public CommentsLocalService getCommentsLocalService() {
		return commentsLocalService;
	}

	/**
	 * Sets the comments local service.
	 *
	 * @param commentsLocalService the comments local service
	 */
	public void setCommentsLocalService(
		CommentsLocalService commentsLocalService) {
		this.commentsLocalService = commentsLocalService;
	}

	/**
	 * Returns the comments remote service.
	 *
	 * @return the comments remote service
	 */
	public CommentsService getCommentsService() {
		return commentsService;
	}

	/**
	 * Sets the comments remote service.
	 *
	 * @param commentsService the comments remote service
	 */
	public void setCommentsService(CommentsService commentsService) {
		this.commentsService = commentsService;
	}

	/**
	 * Returns the comments persistence.
	 *
	 * @return the comments persistence
	 */
	public CommentsPersistence getCommentsPersistence() {
		return commentsPersistence;
	}

	/**
	 * Sets the comments persistence.
	 *
	 * @param commentsPersistence the comments persistence
	 */
	public void setCommentsPersistence(CommentsPersistence commentsPersistence) {
		this.commentsPersistence = commentsPersistence;
	}

	/**
	 * Returns the communication record local service.
	 *
	 * @return the communication record local service
	 */
	public CommunicationRecordLocalService getCommunicationRecordLocalService() {
		return communicationRecordLocalService;
	}

	/**
	 * Sets the communication record local service.
	 *
	 * @param communicationRecordLocalService the communication record local service
	 */
	public void setCommunicationRecordLocalService(
		CommunicationRecordLocalService communicationRecordLocalService) {
		this.communicationRecordLocalService = communicationRecordLocalService;
	}

	/**
	 * Returns the communication record persistence.
	 *
	 * @return the communication record persistence
	 */
	public CommunicationRecordPersistence getCommunicationRecordPersistence() {
		return communicationRecordPersistence;
	}

	/**
	 * Sets the communication record persistence.
	 *
	 * @param communicationRecordPersistence the communication record persistence
	 */
	public void setCommunicationRecordPersistence(
		CommunicationRecordPersistence communicationRecordPersistence) {
		this.communicationRecordPersistence = communicationRecordPersistence;
	}

	/**
	 * Returns the communication student record local service.
	 *
	 * @return the communication student record local service
	 */
	public CommunicationStudentRecordLocalService getCommunicationStudentRecordLocalService() {
		return communicationStudentRecordLocalService;
	}

	/**
	 * Sets the communication student record local service.
	 *
	 * @param communicationStudentRecordLocalService the communication student record local service
	 */
	public void setCommunicationStudentRecordLocalService(
		CommunicationStudentRecordLocalService communicationStudentRecordLocalService) {
		this.communicationStudentRecordLocalService = communicationStudentRecordLocalService;
	}

	/**
	 * Returns the communication student record persistence.
	 *
	 * @return the communication student record persistence
	 */
	public CommunicationStudentRecordPersistence getCommunicationStudentRecordPersistence() {
		return communicationStudentRecordPersistence;
	}

	/**
	 * Sets the communication student record persistence.
	 *
	 * @param communicationStudentRecordPersistence the communication student record persistence
	 */
	public void setCommunicationStudentRecordPersistence(
		CommunicationStudentRecordPersistence communicationStudentRecordPersistence) {
		this.communicationStudentRecordPersistence = communicationStudentRecordPersistence;
	}

	/**
	 * Returns the counseling local service.
	 *
	 * @return the counseling local service
	 */
	public CounselingLocalService getCounselingLocalService() {
		return counselingLocalService;
	}

	/**
	 * Sets the counseling local service.
	 *
	 * @param counselingLocalService the counseling local service
	 */
	public void setCounselingLocalService(
		CounselingLocalService counselingLocalService) {
		this.counselingLocalService = counselingLocalService;
	}

	/**
	 * Returns the counseling persistence.
	 *
	 * @return the counseling persistence
	 */
	public CounselingPersistence getCounselingPersistence() {
		return counselingPersistence;
	}

	/**
	 * Sets the counseling persistence.
	 *
	 * @param counselingPersistence the counseling persistence
	 */
	public void setCounselingPersistence(
		CounselingPersistence counselingPersistence) {
		this.counselingPersistence = counselingPersistence;
	}

	/**
	 * Returns the counseling course interest local service.
	 *
	 * @return the counseling course interest local service
	 */
	public CounselingCourseInterestLocalService getCounselingCourseInterestLocalService() {
		return counselingCourseInterestLocalService;
	}

	/**
	 * Sets the counseling course interest local service.
	 *
	 * @param counselingCourseInterestLocalService the counseling course interest local service
	 */
	public void setCounselingCourseInterestLocalService(
		CounselingCourseInterestLocalService counselingCourseInterestLocalService) {
		this.counselingCourseInterestLocalService = counselingCourseInterestLocalService;
	}

	/**
	 * Returns the counseling course interest remote service.
	 *
	 * @return the counseling course interest remote service
	 */
	public CounselingCourseInterestService getCounselingCourseInterestService() {
		return counselingCourseInterestService;
	}

	/**
	 * Sets the counseling course interest remote service.
	 *
	 * @param counselingCourseInterestService the counseling course interest remote service
	 */
	public void setCounselingCourseInterestService(
		CounselingCourseInterestService counselingCourseInterestService) {
		this.counselingCourseInterestService = counselingCourseInterestService;
	}

	/**
	 * Returns the counseling course interest persistence.
	 *
	 * @return the counseling course interest persistence
	 */
	public CounselingCourseInterestPersistence getCounselingCourseInterestPersistence() {
		return counselingCourseInterestPersistence;
	}

	/**
	 * Sets the counseling course interest persistence.
	 *
	 * @param counselingCourseInterestPersistence the counseling course interest persistence
	 */
	public void setCounselingCourseInterestPersistence(
		CounselingCourseInterestPersistence counselingCourseInterestPersistence) {
		this.counselingCourseInterestPersistence = counselingCourseInterestPersistence;
	}

	/**
	 * Returns the course local service.
	 *
	 * @return the course local service
	 */
	public CourseLocalService getCourseLocalService() {
		return courseLocalService;
	}

	/**
	 * Sets the course local service.
	 *
	 * @param courseLocalService the course local service
	 */
	public void setCourseLocalService(CourseLocalService courseLocalService) {
		this.courseLocalService = courseLocalService;
	}

	/**
	 * Returns the course persistence.
	 *
	 * @return the course persistence
	 */
	public CoursePersistence getCoursePersistence() {
		return coursePersistence;
	}

	/**
	 * Sets the course persistence.
	 *
	 * @param coursePersistence the course persistence
	 */
	public void setCoursePersistence(CoursePersistence coursePersistence) {
		this.coursePersistence = coursePersistence;
	}

	/**
	 * Returns the course fee local service.
	 *
	 * @return the course fee local service
	 */
	public CourseFeeLocalService getCourseFeeLocalService() {
		return courseFeeLocalService;
	}

	/**
	 * Sets the course fee local service.
	 *
	 * @param courseFeeLocalService the course fee local service
	 */
	public void setCourseFeeLocalService(
		CourseFeeLocalService courseFeeLocalService) {
		this.courseFeeLocalService = courseFeeLocalService;
	}

	/**
	 * Returns the course fee persistence.
	 *
	 * @return the course fee persistence
	 */
	public CourseFeePersistence getCourseFeePersistence() {
		return courseFeePersistence;
	}

	/**
	 * Sets the course fee persistence.
	 *
	 * @param courseFeePersistence the course fee persistence
	 */
	public void setCourseFeePersistence(
		CourseFeePersistence courseFeePersistence) {
		this.courseFeePersistence = courseFeePersistence;
	}

	/**
	 * Returns the course organization local service.
	 *
	 * @return the course organization local service
	 */
	public CourseOrganizationLocalService getCourseOrganizationLocalService() {
		return courseOrganizationLocalService;
	}

	/**
	 * Sets the course organization local service.
	 *
	 * @param courseOrganizationLocalService the course organization local service
	 */
	public void setCourseOrganizationLocalService(
		CourseOrganizationLocalService courseOrganizationLocalService) {
		this.courseOrganizationLocalService = courseOrganizationLocalService;
	}

	/**
	 * Returns the course organization persistence.
	 *
	 * @return the course organization persistence
	 */
	public CourseOrganizationPersistence getCourseOrganizationPersistence() {
		return courseOrganizationPersistence;
	}

	/**
	 * Sets the course organization persistence.
	 *
	 * @param courseOrganizationPersistence the course organization persistence
	 */
	public void setCourseOrganizationPersistence(
		CourseOrganizationPersistence courseOrganizationPersistence) {
		this.courseOrganizationPersistence = courseOrganizationPersistence;
	}

	/**
	 * Returns the course session local service.
	 *
	 * @return the course session local service
	 */
	public CourseSessionLocalService getCourseSessionLocalService() {
		return courseSessionLocalService;
	}

	/**
	 * Sets the course session local service.
	 *
	 * @param courseSessionLocalService the course session local service
	 */
	public void setCourseSessionLocalService(
		CourseSessionLocalService courseSessionLocalService) {
		this.courseSessionLocalService = courseSessionLocalService;
	}

	/**
	 * Returns the course session persistence.
	 *
	 * @return the course session persistence
	 */
	public CourseSessionPersistence getCourseSessionPersistence() {
		return courseSessionPersistence;
	}

	/**
	 * Sets the course session persistence.
	 *
	 * @param courseSessionPersistence the course session persistence
	 */
	public void setCourseSessionPersistence(
		CourseSessionPersistence courseSessionPersistence) {
		this.courseSessionPersistence = courseSessionPersistence;
	}

	/**
	 * Returns the course subject local service.
	 *
	 * @return the course subject local service
	 */
	public CourseSubjectLocalService getCourseSubjectLocalService() {
		return courseSubjectLocalService;
	}

	/**
	 * Sets the course subject local service.
	 *
	 * @param courseSubjectLocalService the course subject local service
	 */
	public void setCourseSubjectLocalService(
		CourseSubjectLocalService courseSubjectLocalService) {
		this.courseSubjectLocalService = courseSubjectLocalService;
	}

	/**
	 * Returns the course subject persistence.
	 *
	 * @return the course subject persistence
	 */
	public CourseSubjectPersistence getCourseSubjectPersistence() {
		return courseSubjectPersistence;
	}

	/**
	 * Sets the course subject persistence.
	 *
	 * @param courseSubjectPersistence the course subject persistence
	 */
	public void setCourseSubjectPersistence(
		CourseSubjectPersistence courseSubjectPersistence) {
		this.courseSubjectPersistence = courseSubjectPersistence;
	}

	/**
	 * Returns the daily work local service.
	 *
	 * @return the daily work local service
	 */
	public DailyWorkLocalService getDailyWorkLocalService() {
		return dailyWorkLocalService;
	}

	/**
	 * Sets the daily work local service.
	 *
	 * @param dailyWorkLocalService the daily work local service
	 */
	public void setDailyWorkLocalService(
		DailyWorkLocalService dailyWorkLocalService) {
		this.dailyWorkLocalService = dailyWorkLocalService;
	}

	/**
	 * Returns the daily work persistence.
	 *
	 * @return the daily work persistence
	 */
	public DailyWorkPersistence getDailyWorkPersistence() {
		return dailyWorkPersistence;
	}

	/**
	 * Sets the daily work persistence.
	 *
	 * @param dailyWorkPersistence the daily work persistence
	 */
	public void setDailyWorkPersistence(
		DailyWorkPersistence dailyWorkPersistence) {
		this.dailyWorkPersistence = dailyWorkPersistence;
	}

	/**
	 * Returns the designation local service.
	 *
	 * @return the designation local service
	 */
	public DesignationLocalService getDesignationLocalService() {
		return designationLocalService;
	}

	/**
	 * Sets the designation local service.
	 *
	 * @param designationLocalService the designation local service
	 */
	public void setDesignationLocalService(
		DesignationLocalService designationLocalService) {
		this.designationLocalService = designationLocalService;
	}

	/**
	 * Returns the designation persistence.
	 *
	 * @return the designation persistence
	 */
	public DesignationPersistence getDesignationPersistence() {
		return designationPersistence;
	}

	/**
	 * Sets the designation persistence.
	 *
	 * @param designationPersistence the designation persistence
	 */
	public void setDesignationPersistence(
		DesignationPersistence designationPersistence) {
		this.designationPersistence = designationPersistence;
	}

	/**
	 * Returns the employee local service.
	 *
	 * @return the employee local service
	 */
	public EmployeeLocalService getEmployeeLocalService() {
		return employeeLocalService;
	}

	/**
	 * Sets the employee local service.
	 *
	 * @param employeeLocalService the employee local service
	 */
	public void setEmployeeLocalService(
		EmployeeLocalService employeeLocalService) {
		this.employeeLocalService = employeeLocalService;
	}

	/**
	 * Returns the employee persistence.
	 *
	 * @return the employee persistence
	 */
	public EmployeePersistence getEmployeePersistence() {
		return employeePersistence;
	}

	/**
	 * Sets the employee persistence.
	 *
	 * @param employeePersistence the employee persistence
	 */
	public void setEmployeePersistence(EmployeePersistence employeePersistence) {
		this.employeePersistence = employeePersistence;
	}

	/**
	 * Returns the employee attendance local service.
	 *
	 * @return the employee attendance local service
	 */
	public EmployeeAttendanceLocalService getEmployeeAttendanceLocalService() {
		return employeeAttendanceLocalService;
	}

	/**
	 * Sets the employee attendance local service.
	 *
	 * @param employeeAttendanceLocalService the employee attendance local service
	 */
	public void setEmployeeAttendanceLocalService(
		EmployeeAttendanceLocalService employeeAttendanceLocalService) {
		this.employeeAttendanceLocalService = employeeAttendanceLocalService;
	}

	/**
	 * Returns the employee attendance persistence.
	 *
	 * @return the employee attendance persistence
	 */
	public EmployeeAttendancePersistence getEmployeeAttendancePersistence() {
		return employeeAttendancePersistence;
	}

	/**
	 * Sets the employee attendance persistence.
	 *
	 * @param employeeAttendancePersistence the employee attendance persistence
	 */
	public void setEmployeeAttendancePersistence(
		EmployeeAttendancePersistence employeeAttendancePersistence) {
		this.employeeAttendancePersistence = employeeAttendancePersistence;
	}

	/**
	 * Returns the employee email local service.
	 *
	 * @return the employee email local service
	 */
	public EmployeeEmailLocalService getEmployeeEmailLocalService() {
		return employeeEmailLocalService;
	}

	/**
	 * Sets the employee email local service.
	 *
	 * @param employeeEmailLocalService the employee email local service
	 */
	public void setEmployeeEmailLocalService(
		EmployeeEmailLocalService employeeEmailLocalService) {
		this.employeeEmailLocalService = employeeEmailLocalService;
	}

	/**
	 * Returns the employee email persistence.
	 *
	 * @return the employee email persistence
	 */
	public EmployeeEmailPersistence getEmployeeEmailPersistence() {
		return employeeEmailPersistence;
	}

	/**
	 * Sets the employee email persistence.
	 *
	 * @param employeeEmailPersistence the employee email persistence
	 */
	public void setEmployeeEmailPersistence(
		EmployeeEmailPersistence employeeEmailPersistence) {
		this.employeeEmailPersistence = employeeEmailPersistence;
	}

	/**
	 * Returns the employee mobile local service.
	 *
	 * @return the employee mobile local service
	 */
	public EmployeeMobileLocalService getEmployeeMobileLocalService() {
		return employeeMobileLocalService;
	}

	/**
	 * Sets the employee mobile local service.
	 *
	 * @param employeeMobileLocalService the employee mobile local service
	 */
	public void setEmployeeMobileLocalService(
		EmployeeMobileLocalService employeeMobileLocalService) {
		this.employeeMobileLocalService = employeeMobileLocalService;
	}

	/**
	 * Returns the employee mobile persistence.
	 *
	 * @return the employee mobile persistence
	 */
	public EmployeeMobilePersistence getEmployeeMobilePersistence() {
		return employeeMobilePersistence;
	}

	/**
	 * Sets the employee mobile persistence.
	 *
	 * @param employeeMobilePersistence the employee mobile persistence
	 */
	public void setEmployeeMobilePersistence(
		EmployeeMobilePersistence employeeMobilePersistence) {
		this.employeeMobilePersistence = employeeMobilePersistence;
	}

	/**
	 * Returns the employee role local service.
	 *
	 * @return the employee role local service
	 */
	public EmployeeRoleLocalService getEmployeeRoleLocalService() {
		return employeeRoleLocalService;
	}

	/**
	 * Sets the employee role local service.
	 *
	 * @param employeeRoleLocalService the employee role local service
	 */
	public void setEmployeeRoleLocalService(
		EmployeeRoleLocalService employeeRoleLocalService) {
		this.employeeRoleLocalService = employeeRoleLocalService;
	}

	/**
	 * Returns the employee role persistence.
	 *
	 * @return the employee role persistence
	 */
	public EmployeeRolePersistence getEmployeeRolePersistence() {
		return employeeRolePersistence;
	}

	/**
	 * Sets the employee role persistence.
	 *
	 * @param employeeRolePersistence the employee role persistence
	 */
	public void setEmployeeRolePersistence(
		EmployeeRolePersistence employeeRolePersistence) {
		this.employeeRolePersistence = employeeRolePersistence;
	}

	/**
	 * Returns the employee time schedule local service.
	 *
	 * @return the employee time schedule local service
	 */
	public EmployeeTimeScheduleLocalService getEmployeeTimeScheduleLocalService() {
		return employeeTimeScheduleLocalService;
	}

	/**
	 * Sets the employee time schedule local service.
	 *
	 * @param employeeTimeScheduleLocalService the employee time schedule local service
	 */
	public void setEmployeeTimeScheduleLocalService(
		EmployeeTimeScheduleLocalService employeeTimeScheduleLocalService) {
		this.employeeTimeScheduleLocalService = employeeTimeScheduleLocalService;
	}

	/**
	 * Returns the employee time schedule persistence.
	 *
	 * @return the employee time schedule persistence
	 */
	public EmployeeTimeSchedulePersistence getEmployeeTimeSchedulePersistence() {
		return employeeTimeSchedulePersistence;
	}

	/**
	 * Sets the employee time schedule persistence.
	 *
	 * @param employeeTimeSchedulePersistence the employee time schedule persistence
	 */
	public void setEmployeeTimeSchedulePersistence(
		EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence) {
		this.employeeTimeSchedulePersistence = employeeTimeSchedulePersistence;
	}

	/**
	 * Returns the experiance local service.
	 *
	 * @return the experiance local service
	 */
	public ExperianceLocalService getExperianceLocalService() {
		return experianceLocalService;
	}

	/**
	 * Sets the experiance local service.
	 *
	 * @param experianceLocalService the experiance local service
	 */
	public void setExperianceLocalService(
		ExperianceLocalService experianceLocalService) {
		this.experianceLocalService = experianceLocalService;
	}

	/**
	 * Returns the experiance persistence.
	 *
	 * @return the experiance persistence
	 */
	public ExperiancePersistence getExperiancePersistence() {
		return experiancePersistence;
	}

	/**
	 * Sets the experiance persistence.
	 *
	 * @param experiancePersistence the experiance persistence
	 */
	public void setExperiancePersistence(
		ExperiancePersistence experiancePersistence) {
		this.experiancePersistence = experiancePersistence;
	}

	/**
	 * Returns the fee type local service.
	 *
	 * @return the fee type local service
	 */
	public FeeTypeLocalService getFeeTypeLocalService() {
		return feeTypeLocalService;
	}

	/**
	 * Sets the fee type local service.
	 *
	 * @param feeTypeLocalService the fee type local service
	 */
	public void setFeeTypeLocalService(FeeTypeLocalService feeTypeLocalService) {
		this.feeTypeLocalService = feeTypeLocalService;
	}

	/**
	 * Returns the fee type persistence.
	 *
	 * @return the fee type persistence
	 */
	public FeeTypePersistence getFeeTypePersistence() {
		return feeTypePersistence;
	}

	/**
	 * Sets the fee type persistence.
	 *
	 * @param feeTypePersistence the fee type persistence
	 */
	public void setFeeTypePersistence(FeeTypePersistence feeTypePersistence) {
		this.feeTypePersistence = feeTypePersistence;
	}

	/**
	 * Returns the leave local service.
	 *
	 * @return the leave local service
	 */
	public LeaveLocalService getLeaveLocalService() {
		return leaveLocalService;
	}

	/**
	 * Sets the leave local service.
	 *
	 * @param leaveLocalService the leave local service
	 */
	public void setLeaveLocalService(LeaveLocalService leaveLocalService) {
		this.leaveLocalService = leaveLocalService;
	}

	/**
	 * Returns the leave persistence.
	 *
	 * @return the leave persistence
	 */
	public LeavePersistence getLeavePersistence() {
		return leavePersistence;
	}

	/**
	 * Sets the leave persistence.
	 *
	 * @param leavePersistence the leave persistence
	 */
	public void setLeavePersistence(LeavePersistence leavePersistence) {
		this.leavePersistence = leavePersistence;
	}

	/**
	 * Returns the leave category local service.
	 *
	 * @return the leave category local service
	 */
	public LeaveCategoryLocalService getLeaveCategoryLocalService() {
		return leaveCategoryLocalService;
	}

	/**
	 * Sets the leave category local service.
	 *
	 * @param leaveCategoryLocalService the leave category local service
	 */
	public void setLeaveCategoryLocalService(
		LeaveCategoryLocalService leaveCategoryLocalService) {
		this.leaveCategoryLocalService = leaveCategoryLocalService;
	}

	/**
	 * Returns the leave category persistence.
	 *
	 * @return the leave category persistence
	 */
	public LeaveCategoryPersistence getLeaveCategoryPersistence() {
		return leaveCategoryPersistence;
	}

	/**
	 * Sets the leave category persistence.
	 *
	 * @param leaveCategoryPersistence the leave category persistence
	 */
	public void setLeaveCategoryPersistence(
		LeaveCategoryPersistence leaveCategoryPersistence) {
		this.leaveCategoryPersistence = leaveCategoryPersistence;
	}

	/**
	 * Returns the leave day details local service.
	 *
	 * @return the leave day details local service
	 */
	public LeaveDayDetailsLocalService getLeaveDayDetailsLocalService() {
		return leaveDayDetailsLocalService;
	}

	/**
	 * Sets the leave day details local service.
	 *
	 * @param leaveDayDetailsLocalService the leave day details local service
	 */
	public void setLeaveDayDetailsLocalService(
		LeaveDayDetailsLocalService leaveDayDetailsLocalService) {
		this.leaveDayDetailsLocalService = leaveDayDetailsLocalService;
	}

	/**
	 * Returns the leave day details persistence.
	 *
	 * @return the leave day details persistence
	 */
	public LeaveDayDetailsPersistence getLeaveDayDetailsPersistence() {
		return leaveDayDetailsPersistence;
	}

	/**
	 * Sets the leave day details persistence.
	 *
	 * @param leaveDayDetailsPersistence the leave day details persistence
	 */
	public void setLeaveDayDetailsPersistence(
		LeaveDayDetailsPersistence leaveDayDetailsPersistence) {
		this.leaveDayDetailsPersistence = leaveDayDetailsPersistence;
	}

	/**
	 * Returns the leave receiver local service.
	 *
	 * @return the leave receiver local service
	 */
	public LeaveReceiverLocalService getLeaveReceiverLocalService() {
		return leaveReceiverLocalService;
	}

	/**
	 * Sets the leave receiver local service.
	 *
	 * @param leaveReceiverLocalService the leave receiver local service
	 */
	public void setLeaveReceiverLocalService(
		LeaveReceiverLocalService leaveReceiverLocalService) {
		this.leaveReceiverLocalService = leaveReceiverLocalService;
	}

	/**
	 * Returns the leave receiver persistence.
	 *
	 * @return the leave receiver persistence
	 */
	public LeaveReceiverPersistence getLeaveReceiverPersistence() {
		return leaveReceiverPersistence;
	}

	/**
	 * Sets the leave receiver persistence.
	 *
	 * @param leaveReceiverPersistence the leave receiver persistence
	 */
	public void setLeaveReceiverPersistence(
		LeaveReceiverPersistence leaveReceiverPersistence) {
		this.leaveReceiverPersistence = leaveReceiverPersistence;
	}

	/**
	 * Returns the lesson assessment local service.
	 *
	 * @return the lesson assessment local service
	 */
	public LessonAssessmentLocalService getLessonAssessmentLocalService() {
		return lessonAssessmentLocalService;
	}

	/**
	 * Sets the lesson assessment local service.
	 *
	 * @param lessonAssessmentLocalService the lesson assessment local service
	 */
	public void setLessonAssessmentLocalService(
		LessonAssessmentLocalService lessonAssessmentLocalService) {
		this.lessonAssessmentLocalService = lessonAssessmentLocalService;
	}

	/**
	 * Returns the lesson assessment persistence.
	 *
	 * @return the lesson assessment persistence
	 */
	public LessonAssessmentPersistence getLessonAssessmentPersistence() {
		return lessonAssessmentPersistence;
	}

	/**
	 * Sets the lesson assessment persistence.
	 *
	 * @param lessonAssessmentPersistence the lesson assessment persistence
	 */
	public void setLessonAssessmentPersistence(
		LessonAssessmentPersistence lessonAssessmentPersistence) {
		this.lessonAssessmentPersistence = lessonAssessmentPersistence;
	}

	/**
	 * Returns the lesson plan local service.
	 *
	 * @return the lesson plan local service
	 */
	public LessonPlanLocalService getLessonPlanLocalService() {
		return lessonPlanLocalService;
	}

	/**
	 * Sets the lesson plan local service.
	 *
	 * @param lessonPlanLocalService the lesson plan local service
	 */
	public void setLessonPlanLocalService(
		LessonPlanLocalService lessonPlanLocalService) {
		this.lessonPlanLocalService = lessonPlanLocalService;
	}

	/**
	 * Returns the lesson plan persistence.
	 *
	 * @return the lesson plan persistence
	 */
	public LessonPlanPersistence getLessonPlanPersistence() {
		return lessonPlanPersistence;
	}

	/**
	 * Sets the lesson plan persistence.
	 *
	 * @param lessonPlanPersistence the lesson plan persistence
	 */
	public void setLessonPlanPersistence(
		LessonPlanPersistence lessonPlanPersistence) {
		this.lessonPlanPersistence = lessonPlanPersistence;
	}

	/**
	 * Returns the lesson topic local service.
	 *
	 * @return the lesson topic local service
	 */
	public LessonTopicLocalService getLessonTopicLocalService() {
		return lessonTopicLocalService;
	}

	/**
	 * Sets the lesson topic local service.
	 *
	 * @param lessonTopicLocalService the lesson topic local service
	 */
	public void setLessonTopicLocalService(
		LessonTopicLocalService lessonTopicLocalService) {
		this.lessonTopicLocalService = lessonTopicLocalService;
	}

	/**
	 * Returns the lesson topic persistence.
	 *
	 * @return the lesson topic persistence
	 */
	public LessonTopicPersistence getLessonTopicPersistence() {
		return lessonTopicPersistence;
	}

	/**
	 * Sets the lesson topic persistence.
	 *
	 * @param lessonTopicPersistence the lesson topic persistence
	 */
	public void setLessonTopicPersistence(
		LessonTopicPersistence lessonTopicPersistence) {
		this.lessonTopicPersistence = lessonTopicPersistence;
	}

	/**
	 * Returns the payment local service.
	 *
	 * @return the payment local service
	 */
	public PaymentLocalService getPaymentLocalService() {
		return paymentLocalService;
	}

	/**
	 * Sets the payment local service.
	 *
	 * @param paymentLocalService the payment local service
	 */
	public void setPaymentLocalService(PaymentLocalService paymentLocalService) {
		this.paymentLocalService = paymentLocalService;
	}

	/**
	 * Returns the payment persistence.
	 *
	 * @return the payment persistence
	 */
	public PaymentPersistence getPaymentPersistence() {
		return paymentPersistence;
	}

	/**
	 * Sets the payment persistence.
	 *
	 * @param paymentPersistence the payment persistence
	 */
	public void setPaymentPersistence(PaymentPersistence paymentPersistence) {
		this.paymentPersistence = paymentPersistence;
	}

	/**
	 * Returns the person email local service.
	 *
	 * @return the person email local service
	 */
	public PersonEmailLocalService getPersonEmailLocalService() {
		return personEmailLocalService;
	}

	/**
	 * Sets the person email local service.
	 *
	 * @param personEmailLocalService the person email local service
	 */
	public void setPersonEmailLocalService(
		PersonEmailLocalService personEmailLocalService) {
		this.personEmailLocalService = personEmailLocalService;
	}

	/**
	 * Returns the person email persistence.
	 *
	 * @return the person email persistence
	 */
	public PersonEmailPersistence getPersonEmailPersistence() {
		return personEmailPersistence;
	}

	/**
	 * Sets the person email persistence.
	 *
	 * @param personEmailPersistence the person email persistence
	 */
	public void setPersonEmailPersistence(
		PersonEmailPersistence personEmailPersistence) {
		this.personEmailPersistence = personEmailPersistence;
	}

	/**
	 * Returns the phone number local service.
	 *
	 * @return the phone number local service
	 */
	public PhoneNumberLocalService getPhoneNumberLocalService() {
		return phoneNumberLocalService;
	}

	/**
	 * Sets the phone number local service.
	 *
	 * @param phoneNumberLocalService the phone number local service
	 */
	public void setPhoneNumberLocalService(
		PhoneNumberLocalService phoneNumberLocalService) {
		this.phoneNumberLocalService = phoneNumberLocalService;
	}

	/**
	 * Returns the phone number persistence.
	 *
	 * @return the phone number persistence
	 */
	public PhoneNumberPersistence getPhoneNumberPersistence() {
		return phoneNumberPersistence;
	}

	/**
	 * Sets the phone number persistence.
	 *
	 * @param phoneNumberPersistence the phone number persistence
	 */
	public void setPhoneNumberPersistence(
		PhoneNumberPersistence phoneNumberPersistence) {
		this.phoneNumberPersistence = phoneNumberPersistence;
	}

	/**
	 * Returns the room local service.
	 *
	 * @return the room local service
	 */
	public RoomLocalService getRoomLocalService() {
		return roomLocalService;
	}

	/**
	 * Sets the room local service.
	 *
	 * @param roomLocalService the room local service
	 */
	public void setRoomLocalService(RoomLocalService roomLocalService) {
		this.roomLocalService = roomLocalService;
	}

	/**
	 * Returns the room persistence.
	 *
	 * @return the room persistence
	 */
	public RoomPersistence getRoomPersistence() {
		return roomPersistence;
	}

	/**
	 * Sets the room persistence.
	 *
	 * @param roomPersistence the room persistence
	 */
	public void setRoomPersistence(RoomPersistence roomPersistence) {
		this.roomPersistence = roomPersistence;
	}

	/**
	 * Returns the status history local service.
	 *
	 * @return the status history local service
	 */
	public StatusHistoryLocalService getStatusHistoryLocalService() {
		return statusHistoryLocalService;
	}

	/**
	 * Sets the status history local service.
	 *
	 * @param statusHistoryLocalService the status history local service
	 */
	public void setStatusHistoryLocalService(
		StatusHistoryLocalService statusHistoryLocalService) {
		this.statusHistoryLocalService = statusHistoryLocalService;
	}

	/**
	 * Returns the status history persistence.
	 *
	 * @return the status history persistence
	 */
	public StatusHistoryPersistence getStatusHistoryPersistence() {
		return statusHistoryPersistence;
	}

	/**
	 * Sets the status history persistence.
	 *
	 * @param statusHistoryPersistence the status history persistence
	 */
	public void setStatusHistoryPersistence(
		StatusHistoryPersistence statusHistoryPersistence) {
		this.statusHistoryPersistence = statusHistoryPersistence;
	}

	/**
	 * Returns the student local service.
	 *
	 * @return the student local service
	 */
	public StudentLocalService getStudentLocalService() {
		return studentLocalService;
	}

	/**
	 * Sets the student local service.
	 *
	 * @param studentLocalService the student local service
	 */
	public void setStudentLocalService(StudentLocalService studentLocalService) {
		this.studentLocalService = studentLocalService;
	}

	/**
	 * Returns the student persistence.
	 *
	 * @return the student persistence
	 */
	public StudentPersistence getStudentPersistence() {
		return studentPersistence;
	}

	/**
	 * Sets the student persistence.
	 *
	 * @param studentPersistence the student persistence
	 */
	public void setStudentPersistence(StudentPersistence studentPersistence) {
		this.studentPersistence = studentPersistence;
	}

	/**
	 * Returns the student attendance local service.
	 *
	 * @return the student attendance local service
	 */
	public StudentAttendanceLocalService getStudentAttendanceLocalService() {
		return studentAttendanceLocalService;
	}

	/**
	 * Sets the student attendance local service.
	 *
	 * @param studentAttendanceLocalService the student attendance local service
	 */
	public void setStudentAttendanceLocalService(
		StudentAttendanceLocalService studentAttendanceLocalService) {
		this.studentAttendanceLocalService = studentAttendanceLocalService;
	}

	/**
	 * Returns the student attendance persistence.
	 *
	 * @return the student attendance persistence
	 */
	public StudentAttendancePersistence getStudentAttendancePersistence() {
		return studentAttendancePersistence;
	}

	/**
	 * Sets the student attendance persistence.
	 *
	 * @param studentAttendancePersistence the student attendance persistence
	 */
	public void setStudentAttendancePersistence(
		StudentAttendancePersistence studentAttendancePersistence) {
		this.studentAttendancePersistence = studentAttendancePersistence;
	}

	/**
	 * Returns the student discount local service.
	 *
	 * @return the student discount local service
	 */
	public StudentDiscountLocalService getStudentDiscountLocalService() {
		return studentDiscountLocalService;
	}

	/**
	 * Sets the student discount local service.
	 *
	 * @param studentDiscountLocalService the student discount local service
	 */
	public void setStudentDiscountLocalService(
		StudentDiscountLocalService studentDiscountLocalService) {
		this.studentDiscountLocalService = studentDiscountLocalService;
	}

	/**
	 * Returns the student discount persistence.
	 *
	 * @return the student discount persistence
	 */
	public StudentDiscountPersistence getStudentDiscountPersistence() {
		return studentDiscountPersistence;
	}

	/**
	 * Sets the student discount persistence.
	 *
	 * @param studentDiscountPersistence the student discount persistence
	 */
	public void setStudentDiscountPersistence(
		StudentDiscountPersistence studentDiscountPersistence) {
		this.studentDiscountPersistence = studentDiscountPersistence;
	}

	/**
	 * Returns the student document local service.
	 *
	 * @return the student document local service
	 */
	public StudentDocumentLocalService getStudentDocumentLocalService() {
		return studentDocumentLocalService;
	}

	/**
	 * Sets the student document local service.
	 *
	 * @param studentDocumentLocalService the student document local service
	 */
	public void setStudentDocumentLocalService(
		StudentDocumentLocalService studentDocumentLocalService) {
		this.studentDocumentLocalService = studentDocumentLocalService;
	}

	/**
	 * Returns the student document persistence.
	 *
	 * @return the student document persistence
	 */
	public StudentDocumentPersistence getStudentDocumentPersistence() {
		return studentDocumentPersistence;
	}

	/**
	 * Sets the student document persistence.
	 *
	 * @param studentDocumentPersistence the student document persistence
	 */
	public void setStudentDocumentPersistence(
		StudentDocumentPersistence studentDocumentPersistence) {
		this.studentDocumentPersistence = studentDocumentPersistence;
	}

	/**
	 * Returns the student fee local service.
	 *
	 * @return the student fee local service
	 */
	public StudentFeeLocalService getStudentFeeLocalService() {
		return studentFeeLocalService;
	}

	/**
	 * Sets the student fee local service.
	 *
	 * @param studentFeeLocalService the student fee local service
	 */
	public void setStudentFeeLocalService(
		StudentFeeLocalService studentFeeLocalService) {
		this.studentFeeLocalService = studentFeeLocalService;
	}

	/**
	 * Returns the student fee persistence.
	 *
	 * @return the student fee persistence
	 */
	public StudentFeePersistence getStudentFeePersistence() {
		return studentFeePersistence;
	}

	/**
	 * Sets the student fee persistence.
	 *
	 * @param studentFeePersistence the student fee persistence
	 */
	public void setStudentFeePersistence(
		StudentFeePersistence studentFeePersistence) {
		this.studentFeePersistence = studentFeePersistence;
	}

	/**
	 * Returns the student payment schedule local service.
	 *
	 * @return the student payment schedule local service
	 */
	public StudentPaymentScheduleLocalService getStudentPaymentScheduleLocalService() {
		return studentPaymentScheduleLocalService;
	}

	/**
	 * Sets the student payment schedule local service.
	 *
	 * @param studentPaymentScheduleLocalService the student payment schedule local service
	 */
	public void setStudentPaymentScheduleLocalService(
		StudentPaymentScheduleLocalService studentPaymentScheduleLocalService) {
		this.studentPaymentScheduleLocalService = studentPaymentScheduleLocalService;
	}

	/**
	 * Returns the student payment schedule persistence.
	 *
	 * @return the student payment schedule persistence
	 */
	public StudentPaymentSchedulePersistence getStudentPaymentSchedulePersistence() {
		return studentPaymentSchedulePersistence;
	}

	/**
	 * Sets the student payment schedule persistence.
	 *
	 * @param studentPaymentSchedulePersistence the student payment schedule persistence
	 */
	public void setStudentPaymentSchedulePersistence(
		StudentPaymentSchedulePersistence studentPaymentSchedulePersistence) {
		this.studentPaymentSchedulePersistence = studentPaymentSchedulePersistence;
	}

	/**
	 * Returns the subject local service.
	 *
	 * @return the subject local service
	 */
	public SubjectLocalService getSubjectLocalService() {
		return subjectLocalService;
	}

	/**
	 * Sets the subject local service.
	 *
	 * @param subjectLocalService the subject local service
	 */
	public void setSubjectLocalService(SubjectLocalService subjectLocalService) {
		this.subjectLocalService = subjectLocalService;
	}

	/**
	 * Returns the subject persistence.
	 *
	 * @return the subject persistence
	 */
	public SubjectPersistence getSubjectPersistence() {
		return subjectPersistence;
	}

	/**
	 * Sets the subject persistence.
	 *
	 * @param subjectPersistence the subject persistence
	 */
	public void setSubjectPersistence(SubjectPersistence subjectPersistence) {
		this.subjectPersistence = subjectPersistence;
	}

	/**
	 * Returns the subject lesson local service.
	 *
	 * @return the subject lesson local service
	 */
	public SubjectLessonLocalService getSubjectLessonLocalService() {
		return subjectLessonLocalService;
	}

	/**
	 * Sets the subject lesson local service.
	 *
	 * @param subjectLessonLocalService the subject lesson local service
	 */
	public void setSubjectLessonLocalService(
		SubjectLessonLocalService subjectLessonLocalService) {
		this.subjectLessonLocalService = subjectLessonLocalService;
	}

	/**
	 * Returns the subject lesson persistence.
	 *
	 * @return the subject lesson persistence
	 */
	public SubjectLessonPersistence getSubjectLessonPersistence() {
		return subjectLessonPersistence;
	}

	/**
	 * Sets the subject lesson persistence.
	 *
	 * @param subjectLessonPersistence the subject lesson persistence
	 */
	public void setSubjectLessonPersistence(
		SubjectLessonPersistence subjectLessonPersistence) {
		this.subjectLessonPersistence = subjectLessonPersistence;
	}

	/**
	 * Returns the task local service.
	 *
	 * @return the task local service
	 */
	public TaskLocalService getTaskLocalService() {
		return taskLocalService;
	}

	/**
	 * Sets the task local service.
	 *
	 * @param taskLocalService the task local service
	 */
	public void setTaskLocalService(TaskLocalService taskLocalService) {
		this.taskLocalService = taskLocalService;
	}

	/**
	 * Returns the task persistence.
	 *
	 * @return the task persistence
	 */
	public TaskPersistence getTaskPersistence() {
		return taskPersistence;
	}

	/**
	 * Sets the task persistence.
	 *
	 * @param taskPersistence the task persistence
	 */
	public void setTaskPersistence(TaskPersistence taskPersistence) {
		this.taskPersistence = taskPersistence;
	}

	/**
	 * Returns the task designation local service.
	 *
	 * @return the task designation local service
	 */
	public TaskDesignationLocalService getTaskDesignationLocalService() {
		return taskDesignationLocalService;
	}

	/**
	 * Sets the task designation local service.
	 *
	 * @param taskDesignationLocalService the task designation local service
	 */
	public void setTaskDesignationLocalService(
		TaskDesignationLocalService taskDesignationLocalService) {
		this.taskDesignationLocalService = taskDesignationLocalService;
	}

	/**
	 * Returns the task designation persistence.
	 *
	 * @return the task designation persistence
	 */
	public TaskDesignationPersistence getTaskDesignationPersistence() {
		return taskDesignationPersistence;
	}

	/**
	 * Sets the task designation persistence.
	 *
	 * @param taskDesignationPersistence the task designation persistence
	 */
	public void setTaskDesignationPersistence(
		TaskDesignationPersistence taskDesignationPersistence) {
		this.taskDesignationPersistence = taskDesignationPersistence;
	}

	/**
	 * Returns the topic local service.
	 *
	 * @return the topic local service
	 */
	public TopicLocalService getTopicLocalService() {
		return topicLocalService;
	}

	/**
	 * Sets the topic local service.
	 *
	 * @param topicLocalService the topic local service
	 */
	public void setTopicLocalService(TopicLocalService topicLocalService) {
		this.topicLocalService = topicLocalService;
	}

	/**
	 * Returns the topic persistence.
	 *
	 * @return the topic persistence
	 */
	public TopicPersistence getTopicPersistence() {
		return topicPersistence;
	}

	/**
	 * Sets the topic persistence.
	 *
	 * @param topicPersistence the topic persistence
	 */
	public void setTopicPersistence(TopicPersistence topicPersistence) {
		this.topicPersistence = topicPersistence;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public CounterLocalService getCounterLocalService() {
		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public ResourceLocalService getResourceLocalService() {
		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the resource remote service.
	 *
	 * @return the resource remote service
	 */
	public ResourceService getResourceService() {
		return resourceService;
	}

	/**
	 * Sets the resource remote service.
	 *
	 * @param resourceService the resource remote service
	 */
	public void setResourceService(ResourceService resourceService) {
		this.resourceService = resourceService;
	}

	/**
	 * Returns the resource persistence.
	 *
	 * @return the resource persistence
	 */
	public ResourcePersistence getResourcePersistence() {
		return resourcePersistence;
	}

	/**
	 * Sets the resource persistence.
	 *
	 * @param resourcePersistence the resource persistence
	 */
	public void setResourcePersistence(ResourcePersistence resourcePersistence) {
		this.resourcePersistence = resourcePersistence;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public UserLocalService getUserLocalService() {
		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user remote service.
	 *
	 * @return the user remote service
	 */
	public UserService getUserService() {
		return userService;
	}

	/**
	 * Sets the user remote service.
	 *
	 * @param userService the user remote service
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	public void afterPropertiesSet() {
		PersistedModelLocalServiceRegistryUtil.register("info.diit.portal.model.PersonEmail",
			personEmailLocalService);
	}

	public void destroy() {
		PersistedModelLocalServiceRegistryUtil.unregister(
			"info.diit.portal.model.PersonEmail");
	}

	/**
	 * Returns the Spring bean ID for this bean.
	 *
	 * @return the Spring bean ID for this bean
	 */
	public String getBeanIdentifier() {
		return _beanIdentifier;
	}

	/**
	 * Sets the Spring bean ID for this bean.
	 *
	 * @param beanIdentifier the Spring bean ID for this bean
	 */
	public void setBeanIdentifier(String beanIdentifier) {
		_beanIdentifier = beanIdentifier;
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		return _clpInvoker.invokeMethod(name, parameterTypes, arguments);
	}

	protected Class<?> getModelClass() {
		return PersonEmail.class;
	}

	protected String getModelClassName() {
		return PersonEmail.class.getName();
	}

	/**
	 * Performs an SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) throws SystemException {
		try {
			DataSource dataSource = personEmailPersistence.getDataSource();

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(dataSource,
					sql, new int[0]);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(type = AcademicRecordLocalService.class)
	protected AcademicRecordLocalService academicRecordLocalService;
	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentLocalService.class)
	protected AssessmentLocalService assessmentLocalService;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentLocalService.class)
	protected AssessmentStudentLocalService assessmentStudentLocalService;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypeLocalService.class)
	protected AssessmentTypeLocalService assessmentTypeLocalService;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendanceLocalService.class)
	protected AttendanceLocalService attendanceLocalService;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicLocalService.class)
	protected AttendanceTopicLocalService attendanceTopicLocalService;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchLocalService.class)
	protected BatchLocalService batchLocalService;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeeLocalService.class)
	protected BatchFeeLocalService batchFeeLocalService;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentScheduleLocalService.class)
	protected BatchPaymentScheduleLocalService batchPaymentScheduleLocalService;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentLocalService.class)
	protected BatchStudentLocalService batchStudentLocalService;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectLocalService.class)
	protected BatchSubjectLocalService batchSubjectLocalService;
	@BeanReference(type = BatchSubjectService.class)
	protected BatchSubjectService batchSubjectService;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherLocalService.class)
	protected BatchTeacherLocalService batchTeacherLocalService;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterLocalService.class)
	protected ChapterLocalService chapterLocalService;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventLocalService.class)
	protected ClassRoutineEventLocalService classRoutineEventLocalService;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchLocalService.class)
	protected ClassRoutineEventBatchLocalService classRoutineEventBatchLocalService;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsLocalService.class)
	protected CommentsLocalService commentsLocalService;
	@BeanReference(type = CommentsService.class)
	protected CommentsService commentsService;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordLocalService.class)
	protected CommunicationRecordLocalService communicationRecordLocalService;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordLocalService.class)
	protected CommunicationStudentRecordLocalService communicationStudentRecordLocalService;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingLocalService.class)
	protected CounselingLocalService counselingLocalService;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestLocalService.class)
	protected CounselingCourseInterestLocalService counselingCourseInterestLocalService;
	@BeanReference(type = CounselingCourseInterestService.class)
	protected CounselingCourseInterestService counselingCourseInterestService;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CourseLocalService.class)
	protected CourseLocalService courseLocalService;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeeLocalService.class)
	protected CourseFeeLocalService courseFeeLocalService;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationLocalService.class)
	protected CourseOrganizationLocalService courseOrganizationLocalService;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionLocalService.class)
	protected CourseSessionLocalService courseSessionLocalService;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectLocalService.class)
	protected CourseSubjectLocalService courseSubjectLocalService;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkLocalService.class)
	protected DailyWorkLocalService dailyWorkLocalService;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationLocalService.class)
	protected DesignationLocalService designationLocalService;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeeLocalService.class)
	protected EmployeeLocalService employeeLocalService;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendanceLocalService.class)
	protected EmployeeAttendanceLocalService employeeAttendanceLocalService;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailLocalService.class)
	protected EmployeeEmailLocalService employeeEmailLocalService;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobileLocalService.class)
	protected EmployeeMobileLocalService employeeMobileLocalService;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRoleLocalService.class)
	protected EmployeeRoleLocalService employeeRoleLocalService;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeScheduleLocalService.class)
	protected EmployeeTimeScheduleLocalService employeeTimeScheduleLocalService;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperianceLocalService.class)
	protected ExperianceLocalService experianceLocalService;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypeLocalService.class)
	protected FeeTypeLocalService feeTypeLocalService;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeaveLocalService.class)
	protected LeaveLocalService leaveLocalService;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryLocalService.class)
	protected LeaveCategoryLocalService leaveCategoryLocalService;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsLocalService.class)
	protected LeaveDayDetailsLocalService leaveDayDetailsLocalService;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverLocalService.class)
	protected LeaveReceiverLocalService leaveReceiverLocalService;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentLocalService.class)
	protected LessonAssessmentLocalService lessonAssessmentLocalService;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanLocalService.class)
	protected LessonPlanLocalService lessonPlanLocalService;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicLocalService.class)
	protected LessonTopicLocalService lessonTopicLocalService;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentLocalService.class)
	protected PaymentLocalService paymentLocalService;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailLocalService.class)
	protected PersonEmailLocalService personEmailLocalService;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberLocalService.class)
	protected PhoneNumberLocalService phoneNumberLocalService;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomLocalService.class)
	protected RoomLocalService roomLocalService;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryLocalService.class)
	protected StatusHistoryLocalService statusHistoryLocalService;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentLocalService.class)
	protected StudentLocalService studentLocalService;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendanceLocalService.class)
	protected StudentAttendanceLocalService studentAttendanceLocalService;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountLocalService.class)
	protected StudentDiscountLocalService studentDiscountLocalService;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentLocalService.class)
	protected StudentDocumentLocalService studentDocumentLocalService;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeeLocalService.class)
	protected StudentFeeLocalService studentFeeLocalService;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentScheduleLocalService.class)
	protected StudentPaymentScheduleLocalService studentPaymentScheduleLocalService;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectLocalService.class)
	protected SubjectLocalService subjectLocalService;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonLocalService.class)
	protected SubjectLessonLocalService subjectLessonLocalService;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskLocalService.class)
	protected TaskLocalService taskLocalService;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationLocalService.class)
	protected TaskDesignationLocalService taskDesignationLocalService;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicLocalService.class)
	protected TopicLocalService topicLocalService;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = CounterLocalService.class)
	protected CounterLocalService counterLocalService;
	@BeanReference(type = ResourceLocalService.class)
	protected ResourceLocalService resourceLocalService;
	@BeanReference(type = ResourceService.class)
	protected ResourceService resourceService;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserLocalService.class)
	protected UserLocalService userLocalService;
	@BeanReference(type = UserService.class)
	protected UserService userService;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private String _beanIdentifier;
	private PersonEmailLocalServiceClpInvoker _clpInvoker = new PersonEmailLocalServiceClpInvoker();
}
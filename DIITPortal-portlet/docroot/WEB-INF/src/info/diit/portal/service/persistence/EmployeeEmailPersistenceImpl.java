/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchEmployeeEmailException;
import info.diit.portal.model.EmployeeEmail;
import info.diit.portal.model.impl.EmployeeEmailImpl;
import info.diit.portal.model.impl.EmployeeEmailModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the employee email service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see EmployeeEmailPersistence
 * @see EmployeeEmailUtil
 * @generated
 */
public class EmployeeEmailPersistenceImpl extends BasePersistenceImpl<EmployeeEmail>
	implements EmployeeEmailPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EmployeeEmailUtil} to access the employee email persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EmployeeEmailImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEE = new FinderPath(EmployeeEmailModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeEmailModelImpl.FINDER_CACHE_ENABLED,
			EmployeeEmailImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByEmployee",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE =
		new FinderPath(EmployeeEmailModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeEmailModelImpl.FINDER_CACHE_ENABLED,
			EmployeeEmailImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByEmployee", new String[] { Long.class.getName() },
			EmployeeEmailModelImpl.EMPLOYEEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMPLOYEE = new FinderPath(EmployeeEmailModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeEmailModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEmployee",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EmployeeEmailModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeEmailModelImpl.FINDER_CACHE_ENABLED,
			EmployeeEmailImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EmployeeEmailModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeEmailModelImpl.FINDER_CACHE_ENABLED,
			EmployeeEmailImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EmployeeEmailModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeEmailModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the employee email in the entity cache if it is enabled.
	 *
	 * @param employeeEmail the employee email
	 */
	public void cacheResult(EmployeeEmail employeeEmail) {
		EntityCacheUtil.putResult(EmployeeEmailModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeEmailImpl.class, employeeEmail.getPrimaryKey(),
			employeeEmail);

		employeeEmail.resetOriginalValues();
	}

	/**
	 * Caches the employee emails in the entity cache if it is enabled.
	 *
	 * @param employeeEmails the employee emails
	 */
	public void cacheResult(List<EmployeeEmail> employeeEmails) {
		for (EmployeeEmail employeeEmail : employeeEmails) {
			if (EntityCacheUtil.getResult(
						EmployeeEmailModelImpl.ENTITY_CACHE_ENABLED,
						EmployeeEmailImpl.class, employeeEmail.getPrimaryKey()) == null) {
				cacheResult(employeeEmail);
			}
			else {
				employeeEmail.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all employee emails.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EmployeeEmailImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EmployeeEmailImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the employee email.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EmployeeEmail employeeEmail) {
		EntityCacheUtil.removeResult(EmployeeEmailModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeEmailImpl.class, employeeEmail.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<EmployeeEmail> employeeEmails) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EmployeeEmail employeeEmail : employeeEmails) {
			EntityCacheUtil.removeResult(EmployeeEmailModelImpl.ENTITY_CACHE_ENABLED,
				EmployeeEmailImpl.class, employeeEmail.getPrimaryKey());
		}
	}

	/**
	 * Creates a new employee email with the primary key. Does not add the employee email to the database.
	 *
	 * @param emailId the primary key for the new employee email
	 * @return the new employee email
	 */
	public EmployeeEmail create(long emailId) {
		EmployeeEmail employeeEmail = new EmployeeEmailImpl();

		employeeEmail.setNew(true);
		employeeEmail.setPrimaryKey(emailId);

		return employeeEmail;
	}

	/**
	 * Removes the employee email with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param emailId the primary key of the employee email
	 * @return the employee email that was removed
	 * @throws info.diit.portal.NoSuchEmployeeEmailException if a employee email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeEmail remove(long emailId)
		throws NoSuchEmployeeEmailException, SystemException {
		return remove(Long.valueOf(emailId));
	}

	/**
	 * Removes the employee email with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the employee email
	 * @return the employee email that was removed
	 * @throws info.diit.portal.NoSuchEmployeeEmailException if a employee email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeEmail remove(Serializable primaryKey)
		throws NoSuchEmployeeEmailException, SystemException {
		Session session = null;

		try {
			session = openSession();

			EmployeeEmail employeeEmail = (EmployeeEmail)session.get(EmployeeEmailImpl.class,
					primaryKey);

			if (employeeEmail == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEmployeeEmailException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(employeeEmail);
		}
		catch (NoSuchEmployeeEmailException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EmployeeEmail removeImpl(EmployeeEmail employeeEmail)
		throws SystemException {
		employeeEmail = toUnwrappedModel(employeeEmail);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, employeeEmail);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(employeeEmail);

		return employeeEmail;
	}

	@Override
	public EmployeeEmail updateImpl(
		info.diit.portal.model.EmployeeEmail employeeEmail, boolean merge)
		throws SystemException {
		employeeEmail = toUnwrappedModel(employeeEmail);

		boolean isNew = employeeEmail.isNew();

		EmployeeEmailModelImpl employeeEmailModelImpl = (EmployeeEmailModelImpl)employeeEmail;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, employeeEmail, merge);

			employeeEmail.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !EmployeeEmailModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((employeeEmailModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(employeeEmailModelImpl.getOriginalEmployeeId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE,
					args);

				args = new Object[] {
						Long.valueOf(employeeEmailModelImpl.getEmployeeId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMPLOYEE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE,
					args);
			}
		}

		EntityCacheUtil.putResult(EmployeeEmailModelImpl.ENTITY_CACHE_ENABLED,
			EmployeeEmailImpl.class, employeeEmail.getPrimaryKey(),
			employeeEmail);

		return employeeEmail;
	}

	protected EmployeeEmail toUnwrappedModel(EmployeeEmail employeeEmail) {
		if (employeeEmail instanceof EmployeeEmailImpl) {
			return employeeEmail;
		}

		EmployeeEmailImpl employeeEmailImpl = new EmployeeEmailImpl();

		employeeEmailImpl.setNew(employeeEmail.isNew());
		employeeEmailImpl.setPrimaryKey(employeeEmail.getPrimaryKey());

		employeeEmailImpl.setEmailId(employeeEmail.getEmailId());
		employeeEmailImpl.setCompanyId(employeeEmail.getCompanyId());
		employeeEmailImpl.setUserId(employeeEmail.getUserId());
		employeeEmailImpl.setUserName(employeeEmail.getUserName());
		employeeEmailImpl.setCreateDate(employeeEmail.getCreateDate());
		employeeEmailImpl.setModifiedDate(employeeEmail.getModifiedDate());
		employeeEmailImpl.setEmployeeId(employeeEmail.getEmployeeId());
		employeeEmailImpl.setWorkEmail(employeeEmail.getWorkEmail());
		employeeEmailImpl.setPersonalEmail(employeeEmail.getPersonalEmail());
		employeeEmailImpl.setStatus(employeeEmail.getStatus());

		return employeeEmailImpl;
	}

	/**
	 * Returns the employee email with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the employee email
	 * @return the employee email
	 * @throws com.liferay.portal.NoSuchModelException if a employee email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeEmail findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the employee email with the primary key or throws a {@link info.diit.portal.NoSuchEmployeeEmailException} if it could not be found.
	 *
	 * @param emailId the primary key of the employee email
	 * @return the employee email
	 * @throws info.diit.portal.NoSuchEmployeeEmailException if a employee email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeEmail findByPrimaryKey(long emailId)
		throws NoSuchEmployeeEmailException, SystemException {
		EmployeeEmail employeeEmail = fetchByPrimaryKey(emailId);

		if (employeeEmail == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + emailId);
			}

			throw new NoSuchEmployeeEmailException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				emailId);
		}

		return employeeEmail;
	}

	/**
	 * Returns the employee email with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the employee email
	 * @return the employee email, or <code>null</code> if a employee email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmployeeEmail fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the employee email with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param emailId the primary key of the employee email
	 * @return the employee email, or <code>null</code> if a employee email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeEmail fetchByPrimaryKey(long emailId)
		throws SystemException {
		EmployeeEmail employeeEmail = (EmployeeEmail)EntityCacheUtil.getResult(EmployeeEmailModelImpl.ENTITY_CACHE_ENABLED,
				EmployeeEmailImpl.class, emailId);

		if (employeeEmail == _nullEmployeeEmail) {
			return null;
		}

		if (employeeEmail == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				employeeEmail = (EmployeeEmail)session.get(EmployeeEmailImpl.class,
						Long.valueOf(emailId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (employeeEmail != null) {
					cacheResult(employeeEmail);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(EmployeeEmailModelImpl.ENTITY_CACHE_ENABLED,
						EmployeeEmailImpl.class, emailId, _nullEmployeeEmail);
				}

				closeSession(session);
			}
		}

		return employeeEmail;
	}

	/**
	 * Returns all the employee emails where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @return the matching employee emails
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeEmail> findByEmployee(long employeeId)
		throws SystemException {
		return findByEmployee(employeeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the employee emails where employeeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employeeId the employee ID
	 * @param start the lower bound of the range of employee emails
	 * @param end the upper bound of the range of employee emails (not inclusive)
	 * @return the range of matching employee emails
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeEmail> findByEmployee(long employeeId, int start,
		int end) throws SystemException {
		return findByEmployee(employeeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the employee emails where employeeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param employeeId the employee ID
	 * @param start the lower bound of the range of employee emails
	 * @param end the upper bound of the range of employee emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching employee emails
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeEmail> findByEmployee(long employeeId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMPLOYEE;
			finderArgs = new Object[] { employeeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_EMPLOYEE;
			finderArgs = new Object[] { employeeId, start, end, orderByComparator };
		}

		List<EmployeeEmail> list = (List<EmployeeEmail>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EmployeeEmail employeeEmail : list) {
				if ((employeeId != employeeEmail.getEmployeeId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_EMPLOYEEEMAIL_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				list = (List<EmployeeEmail>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first employee email in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching employee email
	 * @throws info.diit.portal.NoSuchEmployeeEmailException if a matching employee email could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeEmail findByEmployee_First(long employeeId,
		OrderByComparator orderByComparator)
		throws NoSuchEmployeeEmailException, SystemException {
		EmployeeEmail employeeEmail = fetchByEmployee_First(employeeId,
				orderByComparator);

		if (employeeEmail != null) {
			return employeeEmail;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employeeId=");
		msg.append(employeeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEmployeeEmailException(msg.toString());
	}

	/**
	 * Returns the first employee email in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching employee email, or <code>null</code> if a matching employee email could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeEmail fetchByEmployee_First(long employeeId,
		OrderByComparator orderByComparator) throws SystemException {
		List<EmployeeEmail> list = findByEmployee(employeeId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last employee email in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching employee email
	 * @throws info.diit.portal.NoSuchEmployeeEmailException if a matching employee email could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeEmail findByEmployee_Last(long employeeId,
		OrderByComparator orderByComparator)
		throws NoSuchEmployeeEmailException, SystemException {
		EmployeeEmail employeeEmail = fetchByEmployee_Last(employeeId,
				orderByComparator);

		if (employeeEmail != null) {
			return employeeEmail;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("employeeId=");
		msg.append(employeeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEmployeeEmailException(msg.toString());
	}

	/**
	 * Returns the last employee email in the ordered set where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching employee email, or <code>null</code> if a matching employee email could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeEmail fetchByEmployee_Last(long employeeId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByEmployee(employeeId);

		List<EmployeeEmail> list = findByEmployee(employeeId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the employee emails before and after the current employee email in the ordered set where employeeId = &#63;.
	 *
	 * @param emailId the primary key of the current employee email
	 * @param employeeId the employee ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next employee email
	 * @throws info.diit.portal.NoSuchEmployeeEmailException if a employee email with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public EmployeeEmail[] findByEmployee_PrevAndNext(long emailId,
		long employeeId, OrderByComparator orderByComparator)
		throws NoSuchEmployeeEmailException, SystemException {
		EmployeeEmail employeeEmail = findByPrimaryKey(emailId);

		Session session = null;

		try {
			session = openSession();

			EmployeeEmail[] array = new EmployeeEmailImpl[3];

			array[0] = getByEmployee_PrevAndNext(session, employeeEmail,
					employeeId, orderByComparator, true);

			array[1] = employeeEmail;

			array[2] = getByEmployee_PrevAndNext(session, employeeEmail,
					employeeId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EmployeeEmail getByEmployee_PrevAndNext(Session session,
		EmployeeEmail employeeEmail, long employeeId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EMPLOYEEEMAIL_WHERE);

		query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(employeeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(employeeEmail);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EmployeeEmail> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the employee emails.
	 *
	 * @return the employee emails
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeEmail> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the employee emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of employee emails
	 * @param end the upper bound of the range of employee emails (not inclusive)
	 * @return the range of employee emails
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeEmail> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the employee emails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of employee emails
	 * @param end the upper bound of the range of employee emails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of employee emails
	 * @throws SystemException if a system exception occurred
	 */
	public List<EmployeeEmail> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EmployeeEmail> list = (List<EmployeeEmail>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_EMPLOYEEEMAIL);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_EMPLOYEEEMAIL;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<EmployeeEmail>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<EmployeeEmail>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the employee emails where employeeId = &#63; from the database.
	 *
	 * @param employeeId the employee ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByEmployee(long employeeId) throws SystemException {
		for (EmployeeEmail employeeEmail : findByEmployee(employeeId)) {
			remove(employeeEmail);
		}
	}

	/**
	 * Removes all the employee emails from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (EmployeeEmail employeeEmail : findAll()) {
			remove(employeeEmail);
		}
	}

	/**
	 * Returns the number of employee emails where employeeId = &#63;.
	 *
	 * @param employeeId the employee ID
	 * @return the number of matching employee emails
	 * @throws SystemException if a system exception occurred
	 */
	public int countByEmployee(long employeeId) throws SystemException {
		Object[] finderArgs = new Object[] { employeeId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_EMPLOYEE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EMPLOYEEEMAIL_WHERE);

			query.append(_FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(employeeId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_EMPLOYEE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of employee emails.
	 *
	 * @return the number of employee emails
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_EMPLOYEEEMAIL);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the employee email persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.EmployeeEmail")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<EmployeeEmail>> listenersList = new ArrayList<ModelListener<EmployeeEmail>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<EmployeeEmail>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EmployeeEmailImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_EMPLOYEEEMAIL = "SELECT employeeEmail FROM EmployeeEmail employeeEmail";
	private static final String _SQL_SELECT_EMPLOYEEEMAIL_WHERE = "SELECT employeeEmail FROM EmployeeEmail employeeEmail WHERE ";
	private static final String _SQL_COUNT_EMPLOYEEEMAIL = "SELECT COUNT(employeeEmail) FROM EmployeeEmail employeeEmail";
	private static final String _SQL_COUNT_EMPLOYEEEMAIL_WHERE = "SELECT COUNT(employeeEmail) FROM EmployeeEmail employeeEmail WHERE ";
	private static final String _FINDER_COLUMN_EMPLOYEE_EMPLOYEEID_2 = "employeeEmail.employeeId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "employeeEmail.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EmployeeEmail exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No EmployeeEmail exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EmployeeEmailPersistenceImpl.class);
	private static EmployeeEmail _nullEmployeeEmail = new EmployeeEmailImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<EmployeeEmail> toCacheModel() {
				return _nullEmployeeEmailCacheModel;
			}
		};

	private static CacheModel<EmployeeEmail> _nullEmployeeEmailCacheModel = new CacheModel<EmployeeEmail>() {
			public EmployeeEmail toEntityModel() {
				return _nullEmployeeEmail;
			}
		};
}
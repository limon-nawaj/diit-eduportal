/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.impl;

import info.diit.portal.NoSuchDailyWorkException;
import info.diit.portal.model.DailyWork;
import info.diit.portal.service.base.DailyWorkLocalServiceBaseImpl;
import info.diit.portal.service.persistence.DailyWorkUtil;

import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the daily work local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.service.DailyWorkLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see info.diit.portal.service.base.DailyWorkLocalServiceBaseImpl
 * @see info.diit.portal.service.DailyWorkLocalServiceUtil
 */
public class DailyWorkLocalServiceImpl extends DailyWorkLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.service.DailyWorkLocalServiceUtil} to access the daily work local service.
	 */
	public List<DailyWork> findByEmployeeWorkingDay(long employeeId, Date date) throws SystemException{
		return DailyWorkUtil.findByEmployeeWorkingDay(employeeId, date);
	}
	
	public DailyWork findByEmployeeTaskWorkingDay(long employeeId, long taskId, Date date) throws SystemException, NoSuchDailyWorkException{
		return DailyWorkUtil.findByEmployeeTaskWorkingDay(employeeId, taskId, date);
	}
	
	public List<DailyWork> findByTask(long taskId) throws SystemException{
		return DailyWorkUtil.findByTask(taskId);
	}
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.impl;

import info.diit.portal.model.ClassRoutineEventBatch;
import info.diit.portal.service.base.ClassRoutineEventBatchLocalServiceBaseImpl;
import info.diit.portal.service.persistence.ClassRoutineEventBatchUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the class routine event batch local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.service.ClassRoutineEventBatchLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see info.diit.portal.service.base.ClassRoutineEventBatchLocalServiceBaseImpl
 * @see info.diit.portal.service.ClassRoutineEventBatchLocalServiceUtil
 */
public class ClassRoutineEventBatchLocalServiceImpl
	extends ClassRoutineEventBatchLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.service.ClassRoutineEventBatchLocalServiceUtil} to access the class routine event batch local service.
	 */
	public List<ClassRoutineEventBatch> findEventBatchByBatchId(long batchId) throws SystemException{
		return ClassRoutineEventBatchUtil.findBybatchId(batchId);
	}
	
	public List<ClassRoutineEventBatch> findEventBatchByEventId(long eventId) throws SystemException{
		return ClassRoutineEventBatchUtil.findByClassRoutineEventId(eventId);
	}
	
	public List<ClassRoutineEventBatch> findByCompany(long companyId) throws SystemException{
		return ClassRoutineEventBatchUtil.findByCompanyId(companyId);
	}
}
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.impl;

import info.diit.portal.NoSuchBatchStudentException;
import info.diit.portal.model.BatchStudent;
import info.diit.portal.service.base.BatchStudentLocalServiceBaseImpl;
import info.diit.portal.service.persistence.BatchStudentUtil;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the batch student local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.service.BatchStudentLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see info.diit.portal.service.base.BatchStudentLocalServiceBaseImpl
 * @see info.diit.portal.service.BatchStudentLocalServiceUtil
 */
public class BatchStudentLocalServiceImpl
	extends BatchStudentLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link info.diit.portal.service.BatchStudentLocalServiceUtil} to access the batch student local service.
	 */
	public List<BatchStudent> findByBatch(long batchId) throws SystemException{
		return BatchStudentUtil.findByBatch(batchId);
	}
	
	public List<BatchStudent> findByCompanyOrganization(long companyId, long organizationId) throws SystemException{
		return BatchStudentUtil.findByCompanyOrgnization(companyId, organizationId);
	}
	public List<BatchStudent> findBystduentBatchStudentList(long studentId) throws SystemException{
		return BatchStudentUtil.findByStudentBatchStudentList(studentId);
	}
	
	public List<BatchStudent> findAllStudentByOrgBatchId(long organizationId, long batchId) throws SystemException{
		return BatchStudentUtil.findByAllStudentsByBatchId(organizationId, batchId);
	}
	public BatchStudent findIsStudentExistInBatch(long organizationId, long batchId, long studentId) throws SystemException, NoSuchBatchStudentException{
		return BatchStudentUtil.findByOrgBatchStudent(organizationId, batchId, studentId);
	}
}
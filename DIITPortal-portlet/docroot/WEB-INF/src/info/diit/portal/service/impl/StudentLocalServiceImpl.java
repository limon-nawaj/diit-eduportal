/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.impl;

import info.diit.portal.NoSuchStudentException;
import info.diit.portal.model.Batch;
import info.diit.portal.model.BatchStudent;
import info.diit.portal.model.PersonEmail;
import info.diit.portal.model.PhoneNumber;
import info.diit.portal.model.Student;
import info.diit.portal.model.StudentDocument;
import info.diit.portal.model.impl.StudentDocumentImpl;
import info.diit.portal.service.BatchLocalServiceUtil;
import info.diit.portal.service.BatchStudentLocalServiceUtil;
import info.diit.portal.service.PersonEmailLocalServiceUtil;
import info.diit.portal.service.PhoneNumberLocalServiceUtil;
import info.diit.portal.service.StudentDocumentLocalServiceUtil;
import info.diit.portal.service.StudentLocalServiceUtil;
import info.diit.portal.service.base.StudentLocalServiceBaseImpl;
import info.diit.portal.service.persistence.BatchStudentUtil;
import info.diit.portal.service.persistence.PersonEmailUtil;
import info.diit.portal.service.persistence.PhoneNumberUtil;
import info.diit.portal.service.persistence.StudentUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.persistence.RoleUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.util.PwdGenerator;


/**
 * The implementation of the student local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link info.diit.portal.service.StudentLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author mohammad
 * @see info.diit.portal.service.base.StudentLocalServiceBaseImpl
 * @see info.diit.portal.service.StudentLocalServiceUtil
 */
public class StudentLocalServiceImpl extends StudentLocalServiceBaseImpl {
	private static final String STUDENT_GROUP_NAME = "Student";

	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * info.diit.portal.student.service.service.StudentLocalServiceUtil} to
	 * access the student local service.
	 */
	
	public Student findStudentByOrgIdStudentId (long studentId, List<Organization> organizations) throws SystemException{
		Student student = null;
		for(Organization organization: organizations){
			student = StudentUtil.fetchByOrganizationIdStudentId(organization.getOrganizationId(), studentId);
			if (student!=null) {
				return student;
			}			
		}
		return student;		
	}
	
	public Student deleteSearchedStudent(Student student) throws SystemException{		
		
		long studentId = student.getStudentId();
		//StudentDocumentLocalServiceUtil.deleteStudentDocument(documentId);
		
		DLFileEntry dlFileEntry=null;
		try {
			dlFileEntry = DLFileEntryLocalServiceUtil.getDLFileEntry(student.getPhoto());
		} catch (PortalException e1) {
			e1.printStackTrace();
		}
		if(dlFileEntry!=null){
			DLFileEntryLocalServiceUtil.deleteDLFileEntry(dlFileEntry);
		}	
		
		PhoneNumberUtil.removeByStudentPhoneNumberList(studentId);
		PersonEmailUtil.removeByStudentPersonEmailList(studentId);
		BatchStudentUtil.removeByStudentBatchStudentList(studentId);
		
		try {
			UserLocalServiceUtil.deleteUser(student.getStudentUserID());
		} catch (PortalException e) {
			e.printStackTrace();
		}
		return StudentLocalServiceUtil.deleteStudent(student);
		
	}
	
	
	/* save student with photo & creat user 
	 * if you want to send 
	 * */
	
	@SuppressWarnings("deprecation")
	public Student studentSave(Student student, List<PhoneNumber> phoneList,
			List<PersonEmail> emailList, File photo, User creatorUser, boolean action)
			throws SystemException, PortalException {

		
		PwdGenerator pwdGenerator = new PwdGenerator();

		long companyId = student.getCompanyId();
		String firstName = student.getName();
		String screenName = student.getName();
		String lastName = "";
		UserGroup userGroup = UserGroupLocalServiceUtil.getUserGroup(companyId, STUDENT_GROUP_NAME);
		String jobTitle = "Student";
		long[] groupIds = { userGroup.getUserGroupId() };
		System.out.println("organization Id "+student.getOrganizationId());
		long[] organizationIds = { student.getOrganizationId() };
		long creatorUserId = creatorUser.getUserId();
		List<Role> roleList= RoleUtil.findByName("User");
		long roleId=0;
		if(roleList.size()>0)
			 roleId=roleList.get(0).getRoleId();
		long[] roleIds = { roleId };
		boolean autoPassword = true;
		String password1 = pwdGenerator.getPassword();
		String password2 = password1;
		boolean autoScreenName = true;
		String emailAddress = emailList.get(0).getPersonEmail();
		String openId = StringPool.BLANK;
		Locale locale = Locale.US;
		String middleName = "";
		int prefixId = 0;
		int suffixId = 0;
		int birthdayMonth=0;
		int birthdayDay=0;
		int birthdayYear=0;
		if(student.getDateOfBirth()!=null){
		birthdayMonth = student.getDateOfBirth().getMonth();
		birthdayDay = student.getDateOfBirth().getDay();

		birthdayYear = student.getDateOfBirth().getYear();
		}
		long facebookId = 0;

		long[] userGroupIds = {userGroup.getPrimaryKey()};
		boolean sendEmail = false;
		boolean male = student.getGender().equals("Male");
		User user = null;
		
		//according to action true= save, false update
		if(action){
			user = UserLocalServiceUtil.addUser(creatorUserId, companyId,
					autoPassword, password1, password2, autoScreenName, screenName,
					emailAddress, facebookId, openId, locale, firstName,
					middleName, lastName, prefixId, suffixId, male, birthdayMonth,
					birthdayDay, birthdayYear, jobTitle, groupIds, organizationIds,
					roleIds, userGroupIds, sendEmail, null);
			
			student.setStudentUserID(user.getUserId());
			student = StudentLocalServiceUtil.addStudent(student);
		}else{	
			user = creatorUser;
			student = StudentLocalServiceUtil.updateStudent(student);
		}
		
	    if(!action){
	    	PersonEmailUtil.removeByStudentPersonEmailList(student.getStudentId());
	    }
		for (PersonEmail personEmail : emailList) {
			personEmail.setStudentId(student.getStudentId());
			personEmail.setUserId(user.getUserId());
			PersonEmailLocalServiceUtil.addPersonEmail(personEmail);						
		}
	  
	    if(!action){
	    	PhoneNumberUtil.removeByStudentPhoneNumberList(student.getStudentId());
	    }
		for (PhoneNumber phoneNumber : phoneList) {
			phoneNumber.setStudentId(student.getStudentId());
			phoneNumber.setUserId(user.getUserId());
			PhoneNumberLocalServiceUtil.addPhoneNumber(phoneNumber);
		}
	
		
		long folderId=0;
		long repositoryId=0; 
		
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
		
		String fileTitle="Student Photo";
		DLFileEntry fileENtry = null;
		if(photo!=null){
		 if(photo.exists()){
			try {
				String fileName=photo.getName();
				if(action){
					fileENtry = DLFileEntryLocalServiceUtil.addFileEntry(user.getUserId(), 
							userGroup.getGroup().getGroupId(),repositoryId, folderId, fileName, 
							FileUtil.getExtension(photo.getName()), "Id-"+student.getStudentId()+"_"+dateformat.format(new Date()), "Photo of "+student.getName(),
							fileTitle, 0, null, photo, new FileInputStream(photo), 
							photo.length(), new    ServiceContext());
				}else{
					if(student.getPhoto()!=0){
						fileENtry = DLFileEntryLocalServiceUtil.getDLFileEntry(student.getPhoto());
						DLFileEntryLocalServiceUtil.deleteDLFileEntry(fileENtry);
							
					}

					fileENtry = DLFileEntryLocalServiceUtil.addFileEntry(user.getUserId(), 
								userGroup.getGroup().getGroupId(),repositoryId, folderId, fileName, 
								FileUtil.getExtension(photo.getName()), "Id-"+student.getStudentId()+"_"+dateformat.format(new Date()), "Photo of "+student.getName(),
								fileTitle, 0, null, photo, new FileInputStream(photo), 
								photo.length(), new    ServiceContext());		
							
				}				 
				 
			} catch (FileNotFoundException e) {
				e.printStackTrace();
		 	} 			
		   }
		 }
		if(fileENtry!=null){
			student.setPhoto(fileENtry.getFileEntryId());
			//System.out.println("DLF entry genereted Id"+ fileENtry.getFileEntryId());
			StudentLocalServiceUtil.updateStudent(student);
		}
		
		
	    StudentDocument studentDocument=new StudentDocumentImpl();
	    studentDocument.setStudentId(student.getStudentId());
	    studentDocument.setUserId(user.getUserId());
	    studentDocument.setCompanyId(student.getCompanyId());
	    studentDocument.setOrganizationId(student.getOrganizationId());
	    
	    if(fileENtry!=null){
	    	 studentDocument.setFileEntryId(fileENtry.getFileEntryId()) ;
	 	    
	 	    if(action){
	 	    	StudentDocumentLocalServiceUtil.addStudentDocument(studentDocument);
	 	    }else{
	 	    	StudentDocumentLocalServiceUtil.updateStudentDocument(studentDocument);
	 	    }
	    }
	   
	    
	    
	  // DLFileEntry fileENtry= DLFileEntryLocalServiceUtil.addFileEntry(user.getUserId(), folderId,fileName, fileTitle, "description", "",inputStream, fileLength, new    ServiceContext());  
	  // DLFileEntryLocalServiceUtil.addFileEntry(userId, groupId, repositoryId, folderId, sourceFileName, mimeType, title, description, changeLog, fileEntryTypeId, fieldsMap, file, is, size, serviceContext)   
	  //	DFL
	  // DLFileEntryLocalServiceUtil.addFileEntryResources(dlFileEntry, addGroupPermissions, addGuestPermissions)
	    return student;
	}
	
	public String getDLFileAbsPath(DLFileEntry fileEntry) 
			throws PortalException, SystemException {
			  return PropsUtil.get("dl.hook.file.system.root.dir") + "/"
			    + fileEntry.getCompanyId() + "/"
			    + fileEntry.getDataRepositoryId() + "/"
			    + fileEntry.getName() + "/"
			    + fileEntry.getVersion();
			}
	
	public String getStudentPhotoLocation(long studentId){
		DLFileEntry dlFileEntry = null;		
			
		try {
			Student student = StudentLocalServiceUtil.getStudent(studentId);
			if(student.getPhoto()!=0)
			{
				dlFileEntry = DLFileEntryLocalServiceUtil.getDLFileEntry(student.getPhoto());
				
				if(dlFileEntry!=null)
				{
					return getDLFileAbsPath(dlFileEntry);
				}
			}
		} catch (PortalException e1) {
			e1.printStackTrace();
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
		
		return "";
	}
	
/*  get all student batch by student id 
 * 
 * 
 * */
	
	/*public List<Batch> getStudentBatch(long studentId) throws SystemException{
		
		ArrayList<Batch> batchList=new ArrayList<Batch>();
		ArrayList<BatchStudent> batchStudentList=(ArrayList<BatchStudent>) BatchStudentLocalServiceUtil.findBystduentBatchStudentList(studentId);
		if(batchStudentList!=null){
			for (BatchStudent batchStudent : batchStudentList) {
				
			Batch batch=	BatchLocalServiceUtil.fetchBatch(batchStudent.getBatchId());
			
		
				batchList.add(batch);
		}
		
		}
		return batchList;

	}*/
	
	public List<Student> findByOrganiztaion(long organizationId) throws SystemException{
		return StudentUtil.findByStudentOrganization(organizationId);
	}
	
	public Student findStudentByOrgStudentId(long organizationId, long studentId) throws NoSuchStudentException, SystemException{
		return StudentUtil.findByOrganizationIdStudentId(organizationId, studentId);
	}
}
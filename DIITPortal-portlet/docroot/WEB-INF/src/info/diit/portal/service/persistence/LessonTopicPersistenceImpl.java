/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchLessonTopicException;
import info.diit.portal.model.LessonTopic;
import info.diit.portal.model.impl.LessonTopicImpl;
import info.diit.portal.model.impl.LessonTopicModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the lesson topic service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see LessonTopicPersistence
 * @see LessonTopicUtil
 * @generated
 */
public class LessonTopicPersistenceImpl extends BasePersistenceImpl<LessonTopic>
	implements LessonTopicPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LessonTopicUtil} to access the lesson topic persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LessonTopicImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LESSONPLAN =
		new FinderPath(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
			LessonTopicModelImpl.FINDER_CACHE_ENABLED, LessonTopicImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLessonPlan",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONPLAN =
		new FinderPath(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
			LessonTopicModelImpl.FINDER_CACHE_ENABLED, LessonTopicImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLessonPlan",
			new String[] { Long.class.getName() },
			LessonTopicModelImpl.LESSONPLANID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LESSONPLAN = new FinderPath(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
			LessonTopicModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLessonPlan",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LESSONTOPIC =
		new FinderPath(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
			LessonTopicModelImpl.FINDER_CACHE_ENABLED, LessonTopicImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLessonTopic",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONTOPIC =
		new FinderPath(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
			LessonTopicModelImpl.FINDER_CACHE_ENABLED, LessonTopicImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLessonTopic",
			new String[] { Long.class.getName(), Long.class.getName() },
			LessonTopicModelImpl.LESSONPLANID_COLUMN_BITMASK |
			LessonTopicModelImpl.TOPIC_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LESSONTOPIC = new FinderPath(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
			LessonTopicModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLessonTopic",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
			LessonTopicModelImpl.FINDER_CACHE_ENABLED, LessonTopicImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
			LessonTopicModelImpl.FINDER_CACHE_ENABLED, LessonTopicImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
			LessonTopicModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the lesson topic in the entity cache if it is enabled.
	 *
	 * @param lessonTopic the lesson topic
	 */
	public void cacheResult(LessonTopic lessonTopic) {
		EntityCacheUtil.putResult(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
			LessonTopicImpl.class, lessonTopic.getPrimaryKey(), lessonTopic);

		lessonTopic.resetOriginalValues();
	}

	/**
	 * Caches the lesson topics in the entity cache if it is enabled.
	 *
	 * @param lessonTopics the lesson topics
	 */
	public void cacheResult(List<LessonTopic> lessonTopics) {
		for (LessonTopic lessonTopic : lessonTopics) {
			if (EntityCacheUtil.getResult(
						LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
						LessonTopicImpl.class, lessonTopic.getPrimaryKey()) == null) {
				cacheResult(lessonTopic);
			}
			else {
				lessonTopic.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all lesson topics.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(LessonTopicImpl.class.getName());
		}

		EntityCacheUtil.clearCache(LessonTopicImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the lesson topic.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LessonTopic lessonTopic) {
		EntityCacheUtil.removeResult(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
			LessonTopicImpl.class, lessonTopic.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<LessonTopic> lessonTopics) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LessonTopic lessonTopic : lessonTopics) {
			EntityCacheUtil.removeResult(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
				LessonTopicImpl.class, lessonTopic.getPrimaryKey());
		}
	}

	/**
	 * Creates a new lesson topic with the primary key. Does not add the lesson topic to the database.
	 *
	 * @param lessonTopicId the primary key for the new lesson topic
	 * @return the new lesson topic
	 */
	public LessonTopic create(long lessonTopicId) {
		LessonTopic lessonTopic = new LessonTopicImpl();

		lessonTopic.setNew(true);
		lessonTopic.setPrimaryKey(lessonTopicId);

		return lessonTopic;
	}

	/**
	 * Removes the lesson topic with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param lessonTopicId the primary key of the lesson topic
	 * @return the lesson topic that was removed
	 * @throws info.diit.portal.NoSuchLessonTopicException if a lesson topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonTopic remove(long lessonTopicId)
		throws NoSuchLessonTopicException, SystemException {
		return remove(Long.valueOf(lessonTopicId));
	}

	/**
	 * Removes the lesson topic with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the lesson topic
	 * @return the lesson topic that was removed
	 * @throws info.diit.portal.NoSuchLessonTopicException if a lesson topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LessonTopic remove(Serializable primaryKey)
		throws NoSuchLessonTopicException, SystemException {
		Session session = null;

		try {
			session = openSession();

			LessonTopic lessonTopic = (LessonTopic)session.get(LessonTopicImpl.class,
					primaryKey);

			if (lessonTopic == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLessonTopicException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(lessonTopic);
		}
		catch (NoSuchLessonTopicException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LessonTopic removeImpl(LessonTopic lessonTopic)
		throws SystemException {
		lessonTopic = toUnwrappedModel(lessonTopic);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, lessonTopic);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(lessonTopic);

		return lessonTopic;
	}

	@Override
	public LessonTopic updateImpl(
		info.diit.portal.model.LessonTopic lessonTopic, boolean merge)
		throws SystemException {
		lessonTopic = toUnwrappedModel(lessonTopic);

		boolean isNew = lessonTopic.isNew();

		LessonTopicModelImpl lessonTopicModelImpl = (LessonTopicModelImpl)lessonTopic;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, lessonTopic, merge);

			lessonTopic.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !LessonTopicModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((lessonTopicModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONPLAN.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(lessonTopicModelImpl.getOriginalLessonPlanId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LESSONPLAN,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONPLAN,
					args);

				args = new Object[] {
						Long.valueOf(lessonTopicModelImpl.getLessonPlanId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LESSONPLAN,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONPLAN,
					args);
			}

			if ((lessonTopicModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONTOPIC.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(lessonTopicModelImpl.getOriginalLessonPlanId()),
						Long.valueOf(lessonTopicModelImpl.getOriginalTopic())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LESSONTOPIC,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONTOPIC,
					args);

				args = new Object[] {
						Long.valueOf(lessonTopicModelImpl.getLessonPlanId()),
						Long.valueOf(lessonTopicModelImpl.getTopic())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LESSONTOPIC,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONTOPIC,
					args);
			}
		}

		EntityCacheUtil.putResult(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
			LessonTopicImpl.class, lessonTopic.getPrimaryKey(), lessonTopic);

		return lessonTopic;
	}

	protected LessonTopic toUnwrappedModel(LessonTopic lessonTopic) {
		if (lessonTopic instanceof LessonTopicImpl) {
			return lessonTopic;
		}

		LessonTopicImpl lessonTopicImpl = new LessonTopicImpl();

		lessonTopicImpl.setNew(lessonTopic.isNew());
		lessonTopicImpl.setPrimaryKey(lessonTopic.getPrimaryKey());

		lessonTopicImpl.setLessonTopicId(lessonTopic.getLessonTopicId());
		lessonTopicImpl.setCompanyId(lessonTopic.getCompanyId());
		lessonTopicImpl.setUserId(lessonTopic.getUserId());
		lessonTopicImpl.setUserName(lessonTopic.getUserName());
		lessonTopicImpl.setCreateDate(lessonTopic.getCreateDate());
		lessonTopicImpl.setModifiedDate(lessonTopic.getModifiedDate());
		lessonTopicImpl.setLessonPlanId(lessonTopic.getLessonPlanId());
		lessonTopicImpl.setTopic(lessonTopic.getTopic());
		lessonTopicImpl.setClassSequence(lessonTopic.getClassSequence());
		lessonTopicImpl.setResource(lessonTopic.getResource());
		lessonTopicImpl.setActivities(lessonTopic.getActivities());

		return lessonTopicImpl;
	}

	/**
	 * Returns the lesson topic with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the lesson topic
	 * @return the lesson topic
	 * @throws com.liferay.portal.NoSuchModelException if a lesson topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LessonTopic findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the lesson topic with the primary key or throws a {@link info.diit.portal.NoSuchLessonTopicException} if it could not be found.
	 *
	 * @param lessonTopicId the primary key of the lesson topic
	 * @return the lesson topic
	 * @throws info.diit.portal.NoSuchLessonTopicException if a lesson topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonTopic findByPrimaryKey(long lessonTopicId)
		throws NoSuchLessonTopicException, SystemException {
		LessonTopic lessonTopic = fetchByPrimaryKey(lessonTopicId);

		if (lessonTopic == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + lessonTopicId);
			}

			throw new NoSuchLessonTopicException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				lessonTopicId);
		}

		return lessonTopic;
	}

	/**
	 * Returns the lesson topic with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the lesson topic
	 * @return the lesson topic, or <code>null</code> if a lesson topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LessonTopic fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the lesson topic with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param lessonTopicId the primary key of the lesson topic
	 * @return the lesson topic, or <code>null</code> if a lesson topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonTopic fetchByPrimaryKey(long lessonTopicId)
		throws SystemException {
		LessonTopic lessonTopic = (LessonTopic)EntityCacheUtil.getResult(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
				LessonTopicImpl.class, lessonTopicId);

		if (lessonTopic == _nullLessonTopic) {
			return null;
		}

		if (lessonTopic == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				lessonTopic = (LessonTopic)session.get(LessonTopicImpl.class,
						Long.valueOf(lessonTopicId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (lessonTopic != null) {
					cacheResult(lessonTopic);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(LessonTopicModelImpl.ENTITY_CACHE_ENABLED,
						LessonTopicImpl.class, lessonTopicId, _nullLessonTopic);
				}

				closeSession(session);
			}
		}

		return lessonTopic;
	}

	/**
	 * Returns all the lesson topics where lessonPlanId = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @return the matching lesson topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonTopic> findByLessonPlan(long lessonPlanId)
		throws SystemException {
		return findByLessonPlan(lessonPlanId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lesson topics where lessonPlanId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param start the lower bound of the range of lesson topics
	 * @param end the upper bound of the range of lesson topics (not inclusive)
	 * @return the range of matching lesson topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonTopic> findByLessonPlan(long lessonPlanId, int start,
		int end) throws SystemException {
		return findByLessonPlan(lessonPlanId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the lesson topics where lessonPlanId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param start the lower bound of the range of lesson topics
	 * @param end the upper bound of the range of lesson topics (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching lesson topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonTopic> findByLessonPlan(long lessonPlanId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONPLAN;
			finderArgs = new Object[] { lessonPlanId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LESSONPLAN;
			finderArgs = new Object[] {
					lessonPlanId,
					
					start, end, orderByComparator
				};
		}

		List<LessonTopic> list = (List<LessonTopic>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (LessonTopic lessonTopic : list) {
				if ((lessonPlanId != lessonTopic.getLessonPlanId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_LESSONTOPIC_WHERE);

			query.append(_FINDER_COLUMN_LESSONPLAN_LESSONPLANID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(lessonPlanId);

				list = (List<LessonTopic>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first lesson topic in the ordered set where lessonPlanId = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lesson topic
	 * @throws info.diit.portal.NoSuchLessonTopicException if a matching lesson topic could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonTopic findByLessonPlan_First(long lessonPlanId,
		OrderByComparator orderByComparator)
		throws NoSuchLessonTopicException, SystemException {
		LessonTopic lessonTopic = fetchByLessonPlan_First(lessonPlanId,
				orderByComparator);

		if (lessonTopic != null) {
			return lessonTopic;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("lessonPlanId=");
		msg.append(lessonPlanId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLessonTopicException(msg.toString());
	}

	/**
	 * Returns the first lesson topic in the ordered set where lessonPlanId = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lesson topic, or <code>null</code> if a matching lesson topic could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonTopic fetchByLessonPlan_First(long lessonPlanId,
		OrderByComparator orderByComparator) throws SystemException {
		List<LessonTopic> list = findByLessonPlan(lessonPlanId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last lesson topic in the ordered set where lessonPlanId = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lesson topic
	 * @throws info.diit.portal.NoSuchLessonTopicException if a matching lesson topic could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonTopic findByLessonPlan_Last(long lessonPlanId,
		OrderByComparator orderByComparator)
		throws NoSuchLessonTopicException, SystemException {
		LessonTopic lessonTopic = fetchByLessonPlan_Last(lessonPlanId,
				orderByComparator);

		if (lessonTopic != null) {
			return lessonTopic;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("lessonPlanId=");
		msg.append(lessonPlanId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLessonTopicException(msg.toString());
	}

	/**
	 * Returns the last lesson topic in the ordered set where lessonPlanId = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lesson topic, or <code>null</code> if a matching lesson topic could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonTopic fetchByLessonPlan_Last(long lessonPlanId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLessonPlan(lessonPlanId);

		List<LessonTopic> list = findByLessonPlan(lessonPlanId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the lesson topics before and after the current lesson topic in the ordered set where lessonPlanId = &#63;.
	 *
	 * @param lessonTopicId the primary key of the current lesson topic
	 * @param lessonPlanId the lesson plan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next lesson topic
	 * @throws info.diit.portal.NoSuchLessonTopicException if a lesson topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonTopic[] findByLessonPlan_PrevAndNext(long lessonTopicId,
		long lessonPlanId, OrderByComparator orderByComparator)
		throws NoSuchLessonTopicException, SystemException {
		LessonTopic lessonTopic = findByPrimaryKey(lessonTopicId);

		Session session = null;

		try {
			session = openSession();

			LessonTopic[] array = new LessonTopicImpl[3];

			array[0] = getByLessonPlan_PrevAndNext(session, lessonTopic,
					lessonPlanId, orderByComparator, true);

			array[1] = lessonTopic;

			array[2] = getByLessonPlan_PrevAndNext(session, lessonTopic,
					lessonPlanId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LessonTopic getByLessonPlan_PrevAndNext(Session session,
		LessonTopic lessonTopic, long lessonPlanId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LESSONTOPIC_WHERE);

		query.append(_FINDER_COLUMN_LESSONPLAN_LESSONPLANID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(lessonPlanId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(lessonTopic);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LessonTopic> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the lesson topics where lessonPlanId = &#63; and topic = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param topic the topic
	 * @return the matching lesson topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonTopic> findByLessonTopic(long lessonPlanId, long topic)
		throws SystemException {
		return findByLessonTopic(lessonPlanId, topic, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lesson topics where lessonPlanId = &#63; and topic = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param topic the topic
	 * @param start the lower bound of the range of lesson topics
	 * @param end the upper bound of the range of lesson topics (not inclusive)
	 * @return the range of matching lesson topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonTopic> findByLessonTopic(long lessonPlanId, long topic,
		int start, int end) throws SystemException {
		return findByLessonTopic(lessonPlanId, topic, start, end, null);
	}

	/**
	 * Returns an ordered range of all the lesson topics where lessonPlanId = &#63; and topic = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param topic the topic
	 * @param start the lower bound of the range of lesson topics
	 * @param end the upper bound of the range of lesson topics (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching lesson topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonTopic> findByLessonTopic(long lessonPlanId, long topic,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LESSONTOPIC;
			finderArgs = new Object[] { lessonPlanId, topic };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LESSONTOPIC;
			finderArgs = new Object[] {
					lessonPlanId, topic,
					
					start, end, orderByComparator
				};
		}

		List<LessonTopic> list = (List<LessonTopic>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (LessonTopic lessonTopic : list) {
				if ((lessonPlanId != lessonTopic.getLessonPlanId()) ||
						(topic != lessonTopic.getTopic())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LESSONTOPIC_WHERE);

			query.append(_FINDER_COLUMN_LESSONTOPIC_LESSONPLANID_2);

			query.append(_FINDER_COLUMN_LESSONTOPIC_TOPIC_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(lessonPlanId);

				qPos.add(topic);

				list = (List<LessonTopic>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first lesson topic in the ordered set where lessonPlanId = &#63; and topic = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param topic the topic
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lesson topic
	 * @throws info.diit.portal.NoSuchLessonTopicException if a matching lesson topic could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonTopic findByLessonTopic_First(long lessonPlanId, long topic,
		OrderByComparator orderByComparator)
		throws NoSuchLessonTopicException, SystemException {
		LessonTopic lessonTopic = fetchByLessonTopic_First(lessonPlanId, topic,
				orderByComparator);

		if (lessonTopic != null) {
			return lessonTopic;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("lessonPlanId=");
		msg.append(lessonPlanId);

		msg.append(", topic=");
		msg.append(topic);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLessonTopicException(msg.toString());
	}

	/**
	 * Returns the first lesson topic in the ordered set where lessonPlanId = &#63; and topic = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param topic the topic
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching lesson topic, or <code>null</code> if a matching lesson topic could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonTopic fetchByLessonTopic_First(long lessonPlanId, long topic,
		OrderByComparator orderByComparator) throws SystemException {
		List<LessonTopic> list = findByLessonTopic(lessonPlanId, topic, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last lesson topic in the ordered set where lessonPlanId = &#63; and topic = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param topic the topic
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lesson topic
	 * @throws info.diit.portal.NoSuchLessonTopicException if a matching lesson topic could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonTopic findByLessonTopic_Last(long lessonPlanId, long topic,
		OrderByComparator orderByComparator)
		throws NoSuchLessonTopicException, SystemException {
		LessonTopic lessonTopic = fetchByLessonTopic_Last(lessonPlanId, topic,
				orderByComparator);

		if (lessonTopic != null) {
			return lessonTopic;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("lessonPlanId=");
		msg.append(lessonPlanId);

		msg.append(", topic=");
		msg.append(topic);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLessonTopicException(msg.toString());
	}

	/**
	 * Returns the last lesson topic in the ordered set where lessonPlanId = &#63; and topic = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param topic the topic
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching lesson topic, or <code>null</code> if a matching lesson topic could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonTopic fetchByLessonTopic_Last(long lessonPlanId, long topic,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByLessonTopic(lessonPlanId, topic);

		List<LessonTopic> list = findByLessonTopic(lessonPlanId, topic,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the lesson topics before and after the current lesson topic in the ordered set where lessonPlanId = &#63; and topic = &#63;.
	 *
	 * @param lessonTopicId the primary key of the current lesson topic
	 * @param lessonPlanId the lesson plan ID
	 * @param topic the topic
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next lesson topic
	 * @throws info.diit.portal.NoSuchLessonTopicException if a lesson topic with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LessonTopic[] findByLessonTopic_PrevAndNext(long lessonTopicId,
		long lessonPlanId, long topic, OrderByComparator orderByComparator)
		throws NoSuchLessonTopicException, SystemException {
		LessonTopic lessonTopic = findByPrimaryKey(lessonTopicId);

		Session session = null;

		try {
			session = openSession();

			LessonTopic[] array = new LessonTopicImpl[3];

			array[0] = getByLessonTopic_PrevAndNext(session, lessonTopic,
					lessonPlanId, topic, orderByComparator, true);

			array[1] = lessonTopic;

			array[2] = getByLessonTopic_PrevAndNext(session, lessonTopic,
					lessonPlanId, topic, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LessonTopic getByLessonTopic_PrevAndNext(Session session,
		LessonTopic lessonTopic, long lessonPlanId, long topic,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LESSONTOPIC_WHERE);

		query.append(_FINDER_COLUMN_LESSONTOPIC_LESSONPLANID_2);

		query.append(_FINDER_COLUMN_LESSONTOPIC_TOPIC_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(lessonPlanId);

		qPos.add(topic);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(lessonTopic);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LessonTopic> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the lesson topics.
	 *
	 * @return the lesson topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonTopic> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the lesson topics.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of lesson topics
	 * @param end the upper bound of the range of lesson topics (not inclusive)
	 * @return the range of lesson topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonTopic> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the lesson topics.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of lesson topics
	 * @param end the upper bound of the range of lesson topics (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of lesson topics
	 * @throws SystemException if a system exception occurred
	 */
	public List<LessonTopic> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LessonTopic> list = (List<LessonTopic>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_LESSONTOPIC);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LESSONTOPIC;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<LessonTopic>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<LessonTopic>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the lesson topics where lessonPlanId = &#63; from the database.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByLessonPlan(long lessonPlanId) throws SystemException {
		for (LessonTopic lessonTopic : findByLessonPlan(lessonPlanId)) {
			remove(lessonTopic);
		}
	}

	/**
	 * Removes all the lesson topics where lessonPlanId = &#63; and topic = &#63; from the database.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param topic the topic
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByLessonTopic(long lessonPlanId, long topic)
		throws SystemException {
		for (LessonTopic lessonTopic : findByLessonTopic(lessonPlanId, topic)) {
			remove(lessonTopic);
		}
	}

	/**
	 * Removes all the lesson topics from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (LessonTopic lessonTopic : findAll()) {
			remove(lessonTopic);
		}
	}

	/**
	 * Returns the number of lesson topics where lessonPlanId = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @return the number of matching lesson topics
	 * @throws SystemException if a system exception occurred
	 */
	public int countByLessonPlan(long lessonPlanId) throws SystemException {
		Object[] finderArgs = new Object[] { lessonPlanId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_LESSONPLAN,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LESSONTOPIC_WHERE);

			query.append(_FINDER_COLUMN_LESSONPLAN_LESSONPLANID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(lessonPlanId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LESSONPLAN,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of lesson topics where lessonPlanId = &#63; and topic = &#63;.
	 *
	 * @param lessonPlanId the lesson plan ID
	 * @param topic the topic
	 * @return the number of matching lesson topics
	 * @throws SystemException if a system exception occurred
	 */
	public int countByLessonTopic(long lessonPlanId, long topic)
		throws SystemException {
		Object[] finderArgs = new Object[] { lessonPlanId, topic };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_LESSONTOPIC,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_LESSONTOPIC_WHERE);

			query.append(_FINDER_COLUMN_LESSONTOPIC_LESSONPLANID_2);

			query.append(_FINDER_COLUMN_LESSONTOPIC_TOPIC_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(lessonPlanId);

				qPos.add(topic);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LESSONTOPIC,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of lesson topics.
	 *
	 * @return the number of lesson topics
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LESSONTOPIC);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the lesson topic persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.LessonTopic")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<LessonTopic>> listenersList = new ArrayList<ModelListener<LessonTopic>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<LessonTopic>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(LessonTopicImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_LESSONTOPIC = "SELECT lessonTopic FROM LessonTopic lessonTopic";
	private static final String _SQL_SELECT_LESSONTOPIC_WHERE = "SELECT lessonTopic FROM LessonTopic lessonTopic WHERE ";
	private static final String _SQL_COUNT_LESSONTOPIC = "SELECT COUNT(lessonTopic) FROM LessonTopic lessonTopic";
	private static final String _SQL_COUNT_LESSONTOPIC_WHERE = "SELECT COUNT(lessonTopic) FROM LessonTopic lessonTopic WHERE ";
	private static final String _FINDER_COLUMN_LESSONPLAN_LESSONPLANID_2 = "lessonTopic.lessonPlanId = ?";
	private static final String _FINDER_COLUMN_LESSONTOPIC_LESSONPLANID_2 = "lessonTopic.lessonPlanId = ? AND ";
	private static final String _FINDER_COLUMN_LESSONTOPIC_TOPIC_2 = "lessonTopic.topic = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "lessonTopic.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LessonTopic exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No LessonTopic exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(LessonTopicPersistenceImpl.class);
	private static LessonTopic _nullLessonTopic = new LessonTopicImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<LessonTopic> toCacheModel() {
				return _nullLessonTopicCacheModel;
			}
		};

	private static CacheModel<LessonTopic> _nullLessonTopicCacheModel = new CacheModel<LessonTopic>() {
			public LessonTopic toEntityModel() {
				return _nullLessonTopic;
			}
		};
}
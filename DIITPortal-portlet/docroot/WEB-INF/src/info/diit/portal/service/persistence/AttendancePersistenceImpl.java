/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchAttendanceException;
import info.diit.portal.model.Attendance;
import info.diit.portal.model.impl.AttendanceImpl;
import info.diit.portal.model.impl.AttendanceModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * The persistence implementation for the attendance service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see AttendancePersistence
 * @see AttendanceUtil
 * @generated
 */
public class AttendancePersistenceImpl extends BasePersistenceImpl<Attendance>
	implements AttendancePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AttendanceUtil} to access the attendance persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AttendanceImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY = new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, AttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompany",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY =
		new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, AttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompany",
			new String[] { Long.class.getName() },
			AttendanceModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANY = new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompany",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERBATCHSUBJECT =
		new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, AttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserBatchSubject",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERBATCHSUBJECT =
		new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, AttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUserBatchSubject",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			},
			AttendanceModelImpl.USERID_COLUMN_BITMASK |
			AttendanceModelImpl.BATCH_COLUMN_BITMASK |
			AttendanceModelImpl.SUBJECT_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERBATCHSUBJECT = new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUserBatchSubject",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCH = new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, AttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBatch",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH = new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, AttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBatch",
			new String[] { Long.class.getName() },
			AttendanceModelImpl.BATCH_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCH = new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBatch",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FETCH_BY_SUBJECTDATE = new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, AttendanceImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchBySubjectDate",
			new String[] { Long.class.getName(), Date.class.getName() },
			AttendanceModelImpl.SUBJECT_COLUMN_BITMASK |
			AttendanceModelImpl.ATTENDANCEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SUBJECTDATE = new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBySubjectDate",
			new String[] { Long.class.getName(), Date.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHDATE =
		new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, AttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBatchDate",
			new String[] {
				Long.class.getName(), Date.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHDATE =
		new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, AttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBatchDate",
			new String[] { Long.class.getName(), Date.class.getName() },
			AttendanceModelImpl.BATCH_COLUMN_BITMASK |
			AttendanceModelImpl.ATTENDANCEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCHDATE = new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBatchDate",
			new String[] { Long.class.getName(), Date.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, AttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, AttendanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the attendance in the entity cache if it is enabled.
	 *
	 * @param attendance the attendance
	 */
	public void cacheResult(Attendance attendance) {
		EntityCacheUtil.putResult(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceImpl.class, attendance.getPrimaryKey(), attendance);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTDATE,
			new Object[] {
				Long.valueOf(attendance.getSubject()),
				
			attendance.getAttendanceDate()
			}, attendance);

		attendance.resetOriginalValues();
	}

	/**
	 * Caches the attendances in the entity cache if it is enabled.
	 *
	 * @param attendances the attendances
	 */
	public void cacheResult(List<Attendance> attendances) {
		for (Attendance attendance : attendances) {
			if (EntityCacheUtil.getResult(
						AttendanceModelImpl.ENTITY_CACHE_ENABLED,
						AttendanceImpl.class, attendance.getPrimaryKey()) == null) {
				cacheResult(attendance);
			}
			else {
				attendance.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all attendances.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AttendanceImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AttendanceImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the attendance.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Attendance attendance) {
		EntityCacheUtil.removeResult(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceImpl.class, attendance.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(attendance);
	}

	@Override
	public void clearCache(List<Attendance> attendances) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Attendance attendance : attendances) {
			EntityCacheUtil.removeResult(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
				AttendanceImpl.class, attendance.getPrimaryKey());

			clearUniqueFindersCache(attendance);
		}
	}

	protected void clearUniqueFindersCache(Attendance attendance) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SUBJECTDATE,
			new Object[] {
				Long.valueOf(attendance.getSubject()),
				
			attendance.getAttendanceDate()
			});
	}

	/**
	 * Creates a new attendance with the primary key. Does not add the attendance to the database.
	 *
	 * @param attendanceId the primary key for the new attendance
	 * @return the new attendance
	 */
	public Attendance create(long attendanceId) {
		Attendance attendance = new AttendanceImpl();

		attendance.setNew(true);
		attendance.setPrimaryKey(attendanceId);

		return attendance;
	}

	/**
	 * Removes the attendance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param attendanceId the primary key of the attendance
	 * @return the attendance that was removed
	 * @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance remove(long attendanceId)
		throws NoSuchAttendanceException, SystemException {
		return remove(Long.valueOf(attendanceId));
	}

	/**
	 * Removes the attendance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the attendance
	 * @return the attendance that was removed
	 * @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Attendance remove(Serializable primaryKey)
		throws NoSuchAttendanceException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Attendance attendance = (Attendance)session.get(AttendanceImpl.class,
					primaryKey);

			if (attendance == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAttendanceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(attendance);
		}
		catch (NoSuchAttendanceException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Attendance removeImpl(Attendance attendance)
		throws SystemException {
		attendance = toUnwrappedModel(attendance);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, attendance);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(attendance);

		return attendance;
	}

	@Override
	public Attendance updateImpl(info.diit.portal.model.Attendance attendance,
		boolean merge) throws SystemException {
		attendance = toUnwrappedModel(attendance);

		boolean isNew = attendance.isNew();

		AttendanceModelImpl attendanceModelImpl = (AttendanceModelImpl)attendance;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, attendance, merge);

			attendance.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !AttendanceModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((attendanceModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(attendanceModelImpl.getOriginalCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);

				args = new Object[] {
						Long.valueOf(attendanceModelImpl.getCompanyId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY,
					args);
			}

			if ((attendanceModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERBATCHSUBJECT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(attendanceModelImpl.getOriginalUserId()),
						Long.valueOf(attendanceModelImpl.getOriginalBatch()),
						Long.valueOf(attendanceModelImpl.getOriginalSubject())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERBATCHSUBJECT,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERBATCHSUBJECT,
					args);

				args = new Object[] {
						Long.valueOf(attendanceModelImpl.getUserId()),
						Long.valueOf(attendanceModelImpl.getBatch()),
						Long.valueOf(attendanceModelImpl.getSubject())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERBATCHSUBJECT,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERBATCHSUBJECT,
					args);
			}

			if ((attendanceModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(attendanceModelImpl.getOriginalBatch())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCH, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH,
					args);

				args = new Object[] { Long.valueOf(attendanceModelImpl.getBatch()) };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCH, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH,
					args);
			}

			if ((attendanceModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHDATE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(attendanceModelImpl.getOriginalBatch()),
						
						attendanceModelImpl.getOriginalAttendanceDate()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHDATE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHDATE,
					args);

				args = new Object[] {
						Long.valueOf(attendanceModelImpl.getBatch()),
						
						attendanceModelImpl.getAttendanceDate()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHDATE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHDATE,
					args);
			}
		}

		EntityCacheUtil.putResult(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
			AttendanceImpl.class, attendance.getPrimaryKey(), attendance);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTDATE,
				new Object[] {
					Long.valueOf(attendance.getSubject()),
					
				attendance.getAttendanceDate()
				}, attendance);
		}
		else {
			if ((attendanceModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_SUBJECTDATE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(attendanceModelImpl.getOriginalSubject()),
						
						attendanceModelImpl.getOriginalAttendanceDate()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBJECTDATE,
					args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SUBJECTDATE,
					args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTDATE,
					new Object[] {
						Long.valueOf(attendance.getSubject()),
						
					attendance.getAttendanceDate()
					}, attendance);
			}
		}

		return attendance;
	}

	protected Attendance toUnwrappedModel(Attendance attendance) {
		if (attendance instanceof AttendanceImpl) {
			return attendance;
		}

		AttendanceImpl attendanceImpl = new AttendanceImpl();

		attendanceImpl.setNew(attendance.isNew());
		attendanceImpl.setPrimaryKey(attendance.getPrimaryKey());

		attendanceImpl.setAttendanceId(attendance.getAttendanceId());
		attendanceImpl.setCompanyId(attendance.getCompanyId());
		attendanceImpl.setUserId(attendance.getUserId());
		attendanceImpl.setUserName(attendance.getUserName());
		attendanceImpl.setCreateDate(attendance.getCreateDate());
		attendanceImpl.setModifiedDate(attendance.getModifiedDate());
		attendanceImpl.setCampus(attendance.getCampus());
		attendanceImpl.setAttendanceDate(attendance.getAttendanceDate());
		attendanceImpl.setAttendanceRate(attendance.getAttendanceRate());
		attendanceImpl.setBatch(attendance.getBatch());
		attendanceImpl.setSubject(attendance.getSubject());
		attendanceImpl.setRoutineIn(attendance.getRoutineIn());
		attendanceImpl.setRoutineOut(attendance.getRoutineOut());
		attendanceImpl.setActualIn(attendance.getActualIn());
		attendanceImpl.setActualOut(attendance.getActualOut());
		attendanceImpl.setRoutineDuration(attendance.getRoutineDuration());
		attendanceImpl.setActualDuration(attendance.getActualDuration());
		attendanceImpl.setLateEntry(attendance.getLateEntry());
		attendanceImpl.setLagTime(attendance.getLagTime());
		attendanceImpl.setNote(attendance.getNote());

		return attendanceImpl;
	}

	/**
	 * Returns the attendance with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the attendance
	 * @return the attendance
	 * @throws com.liferay.portal.NoSuchModelException if a attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Attendance findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the attendance with the primary key or throws a {@link info.diit.portal.NoSuchAttendanceException} if it could not be found.
	 *
	 * @param attendanceId the primary key of the attendance
	 * @return the attendance
	 * @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance findByPrimaryKey(long attendanceId)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = fetchByPrimaryKey(attendanceId);

		if (attendance == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + attendanceId);
			}

			throw new NoSuchAttendanceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				attendanceId);
		}

		return attendance;
	}

	/**
	 * Returns the attendance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the attendance
	 * @return the attendance, or <code>null</code> if a attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Attendance fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the attendance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param attendanceId the primary key of the attendance
	 * @return the attendance, or <code>null</code> if a attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance fetchByPrimaryKey(long attendanceId)
		throws SystemException {
		Attendance attendance = (Attendance)EntityCacheUtil.getResult(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
				AttendanceImpl.class, attendanceId);

		if (attendance == _nullAttendance) {
			return null;
		}

		if (attendance == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				attendance = (Attendance)session.get(AttendanceImpl.class,
						Long.valueOf(attendanceId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (attendance != null) {
					cacheResult(attendance);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(AttendanceModelImpl.ENTITY_CACHE_ENABLED,
						AttendanceImpl.class, attendanceId, _nullAttendance);
				}

				closeSession(session);
			}
		}

		return attendance;
	}

	/**
	 * Returns all the attendances where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findByCompany(long companyId)
		throws SystemException {
		return findByCompany(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the attendances where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of attendances
	 * @param end the upper bound of the range of attendances (not inclusive)
	 * @return the range of matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findByCompany(long companyId, int start, int end)
		throws SystemException {
		return findByCompany(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the attendances where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of attendances
	 * @param end the upper bound of the range of attendances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findByCompany(long companyId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANY;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<Attendance> list = (List<Attendance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Attendance attendance : list) {
				if ((companyId != attendance.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_ATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				list = (List<Attendance>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first attendance in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attendance
	 * @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance findByCompany_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = fetchByCompany_First(companyId,
				orderByComparator);

		if (attendance != null) {
			return attendance;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAttendanceException(msg.toString());
	}

	/**
	 * Returns the first attendance in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attendance, or <code>null</code> if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance fetchByCompany_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Attendance> list = findByCompany(companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last attendance in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attendance
	 * @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance findByCompany_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = fetchByCompany_Last(companyId, orderByComparator);

		if (attendance != null) {
			return attendance;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAttendanceException(msg.toString());
	}

	/**
	 * Returns the last attendance in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attendance, or <code>null</code> if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance fetchByCompany_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompany(companyId);

		List<Attendance> list = findByCompany(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the attendances before and after the current attendance in the ordered set where companyId = &#63;.
	 *
	 * @param attendanceId the primary key of the current attendance
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next attendance
	 * @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance[] findByCompany_PrevAndNext(long attendanceId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = findByPrimaryKey(attendanceId);

		Session session = null;

		try {
			session = openSession();

			Attendance[] array = new AttendanceImpl[3];

			array[0] = getByCompany_PrevAndNext(session, attendance, companyId,
					orderByComparator, true);

			array[1] = attendance;

			array[2] = getByCompany_PrevAndNext(session, attendance, companyId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Attendance getByCompany_PrevAndNext(Session session,
		Attendance attendance, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ATTENDANCE_WHERE);

		query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(attendance);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Attendance> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the attendances where userId = &#63; and batch = &#63; and subject = &#63;.
	 *
	 * @param userId the user ID
	 * @param batch the batch
	 * @param subject the subject
	 * @return the matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findByUserBatchSubject(long userId, long batch,
		long subject) throws SystemException {
		return findByUserBatchSubject(userId, batch, subject,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the attendances where userId = &#63; and batch = &#63; and subject = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param batch the batch
	 * @param subject the subject
	 * @param start the lower bound of the range of attendances
	 * @param end the upper bound of the range of attendances (not inclusive)
	 * @return the range of matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findByUserBatchSubject(long userId, long batch,
		long subject, int start, int end) throws SystemException {
		return findByUserBatchSubject(userId, batch, subject, start, end, null);
	}

	/**
	 * Returns an ordered range of all the attendances where userId = &#63; and batch = &#63; and subject = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param batch the batch
	 * @param subject the subject
	 * @param start the lower bound of the range of attendances
	 * @param end the upper bound of the range of attendances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findByUserBatchSubject(long userId, long batch,
		long subject, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERBATCHSUBJECT;
			finderArgs = new Object[] { userId, batch, subject };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERBATCHSUBJECT;
			finderArgs = new Object[] {
					userId, batch, subject,
					
					start, end, orderByComparator
				};
		}

		List<Attendance> list = (List<Attendance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Attendance attendance : list) {
				if ((userId != attendance.getUserId()) ||
						(batch != attendance.getBatch()) ||
						(subject != attendance.getSubject())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_USERBATCHSUBJECT_USERID_2);

			query.append(_FINDER_COLUMN_USERBATCHSUBJECT_BATCH_2);

			query.append(_FINDER_COLUMN_USERBATCHSUBJECT_SUBJECT_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(batch);

				qPos.add(subject);

				list = (List<Attendance>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	 *
	 * @param userId the user ID
	 * @param batch the batch
	 * @param subject the subject
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attendance
	 * @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance findByUserBatchSubject_First(long userId, long batch,
		long subject, OrderByComparator orderByComparator)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = fetchByUserBatchSubject_First(userId, batch,
				subject, orderByComparator);

		if (attendance != null) {
			return attendance;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", batch=");
		msg.append(batch);

		msg.append(", subject=");
		msg.append(subject);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAttendanceException(msg.toString());
	}

	/**
	 * Returns the first attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	 *
	 * @param userId the user ID
	 * @param batch the batch
	 * @param subject the subject
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attendance, or <code>null</code> if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance fetchByUserBatchSubject_First(long userId, long batch,
		long subject, OrderByComparator orderByComparator)
		throws SystemException {
		List<Attendance> list = findByUserBatchSubject(userId, batch, subject,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	 *
	 * @param userId the user ID
	 * @param batch the batch
	 * @param subject the subject
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attendance
	 * @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance findByUserBatchSubject_Last(long userId, long batch,
		long subject, OrderByComparator orderByComparator)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = fetchByUserBatchSubject_Last(userId, batch,
				subject, orderByComparator);

		if (attendance != null) {
			return attendance;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(", batch=");
		msg.append(batch);

		msg.append(", subject=");
		msg.append(subject);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAttendanceException(msg.toString());
	}

	/**
	 * Returns the last attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	 *
	 * @param userId the user ID
	 * @param batch the batch
	 * @param subject the subject
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attendance, or <code>null</code> if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance fetchByUserBatchSubject_Last(long userId, long batch,
		long subject, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByUserBatchSubject(userId, batch, subject);

		List<Attendance> list = findByUserBatchSubject(userId, batch, subject,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the attendances before and after the current attendance in the ordered set where userId = &#63; and batch = &#63; and subject = &#63;.
	 *
	 * @param attendanceId the primary key of the current attendance
	 * @param userId the user ID
	 * @param batch the batch
	 * @param subject the subject
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next attendance
	 * @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance[] findByUserBatchSubject_PrevAndNext(long attendanceId,
		long userId, long batch, long subject,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = findByPrimaryKey(attendanceId);

		Session session = null;

		try {
			session = openSession();

			Attendance[] array = new AttendanceImpl[3];

			array[0] = getByUserBatchSubject_PrevAndNext(session, attendance,
					userId, batch, subject, orderByComparator, true);

			array[1] = attendance;

			array[2] = getByUserBatchSubject_PrevAndNext(session, attendance,
					userId, batch, subject, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Attendance getByUserBatchSubject_PrevAndNext(Session session,
		Attendance attendance, long userId, long batch, long subject,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ATTENDANCE_WHERE);

		query.append(_FINDER_COLUMN_USERBATCHSUBJECT_USERID_2);

		query.append(_FINDER_COLUMN_USERBATCHSUBJECT_BATCH_2);

		query.append(_FINDER_COLUMN_USERBATCHSUBJECT_SUBJECT_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		qPos.add(batch);

		qPos.add(subject);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(attendance);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Attendance> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the attendances where batch = &#63;.
	 *
	 * @param batch the batch
	 * @return the matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findByBatch(long batch) throws SystemException {
		return findByBatch(batch, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the attendances where batch = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batch the batch
	 * @param start the lower bound of the range of attendances
	 * @param end the upper bound of the range of attendances (not inclusive)
	 * @return the range of matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findByBatch(long batch, int start, int end)
		throws SystemException {
		return findByBatch(batch, start, end, null);
	}

	/**
	 * Returns an ordered range of all the attendances where batch = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batch the batch
	 * @param start the lower bound of the range of attendances
	 * @param end the upper bound of the range of attendances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findByBatch(long batch, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH;
			finderArgs = new Object[] { batch };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCH;
			finderArgs = new Object[] { batch, start, end, orderByComparator };
		}

		List<Attendance> list = (List<Attendance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Attendance attendance : list) {
				if ((batch != attendance.getBatch())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_ATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_BATCH_BATCH_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batch);

				list = (List<Attendance>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first attendance in the ordered set where batch = &#63;.
	 *
	 * @param batch the batch
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attendance
	 * @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance findByBatch_First(long batch,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = fetchByBatch_First(batch, orderByComparator);

		if (attendance != null) {
			return attendance;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batch=");
		msg.append(batch);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAttendanceException(msg.toString());
	}

	/**
	 * Returns the first attendance in the ordered set where batch = &#63;.
	 *
	 * @param batch the batch
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attendance, or <code>null</code> if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance fetchByBatch_First(long batch,
		OrderByComparator orderByComparator) throws SystemException {
		List<Attendance> list = findByBatch(batch, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last attendance in the ordered set where batch = &#63;.
	 *
	 * @param batch the batch
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attendance
	 * @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance findByBatch_Last(long batch,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = fetchByBatch_Last(batch, orderByComparator);

		if (attendance != null) {
			return attendance;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batch=");
		msg.append(batch);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAttendanceException(msg.toString());
	}

	/**
	 * Returns the last attendance in the ordered set where batch = &#63;.
	 *
	 * @param batch the batch
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attendance, or <code>null</code> if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance fetchByBatch_Last(long batch,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBatch(batch);

		List<Attendance> list = findByBatch(batch, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the attendances before and after the current attendance in the ordered set where batch = &#63;.
	 *
	 * @param attendanceId the primary key of the current attendance
	 * @param batch the batch
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next attendance
	 * @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance[] findByBatch_PrevAndNext(long attendanceId, long batch,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = findByPrimaryKey(attendanceId);

		Session session = null;

		try {
			session = openSession();

			Attendance[] array = new AttendanceImpl[3];

			array[0] = getByBatch_PrevAndNext(session, attendance, batch,
					orderByComparator, true);

			array[1] = attendance;

			array[2] = getByBatch_PrevAndNext(session, attendance, batch,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Attendance getByBatch_PrevAndNext(Session session,
		Attendance attendance, long batch, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ATTENDANCE_WHERE);

		query.append(_FINDER_COLUMN_BATCH_BATCH_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(batch);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(attendance);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Attendance> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns the attendance where subject = &#63; and attendanceDate = &#63; or throws a {@link info.diit.portal.NoSuchAttendanceException} if it could not be found.
	 *
	 * @param subject the subject
	 * @param attendanceDate the attendance date
	 * @return the matching attendance
	 * @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance findBySubjectDate(long subject, Date attendanceDate)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = fetchBySubjectDate(subject, attendanceDate);

		if (attendance == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("subject=");
			msg.append(subject);

			msg.append(", attendanceDate=");
			msg.append(attendanceDate);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchAttendanceException(msg.toString());
		}

		return attendance;
	}

	/**
	 * Returns the attendance where subject = &#63; and attendanceDate = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param subject the subject
	 * @param attendanceDate the attendance date
	 * @return the matching attendance, or <code>null</code> if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance fetchBySubjectDate(long subject, Date attendanceDate)
		throws SystemException {
		return fetchBySubjectDate(subject, attendanceDate, true);
	}

	/**
	 * Returns the attendance where subject = &#63; and attendanceDate = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param subject the subject
	 * @param attendanceDate the attendance date
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching attendance, or <code>null</code> if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance fetchBySubjectDate(long subject, Date attendanceDate,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { subject, attendanceDate };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_SUBJECTDATE,
					finderArgs, this);
		}

		if (result instanceof Attendance) {
			Attendance attendance = (Attendance)result;

			if ((subject != attendance.getSubject()) ||
					!Validator.equals(attendanceDate,
						attendance.getAttendanceDate())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_ATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_SUBJECTDATE_SUBJECT_2);

			if (attendanceDate == null) {
				query.append(_FINDER_COLUMN_SUBJECTDATE_ATTENDANCEDATE_1);
			}
			else {
				query.append(_FINDER_COLUMN_SUBJECTDATE_ATTENDANCEDATE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subject);

				if (attendanceDate != null) {
					qPos.add(CalendarUtil.getTimestamp(attendanceDate));
				}

				List<Attendance> list = q.list();

				result = list;

				Attendance attendance = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTDATE,
						finderArgs, list);
				}
				else {
					attendance = list.get(0);

					cacheResult(attendance);

					if ((attendance.getSubject() != subject) ||
							(attendance.getAttendanceDate() == null) ||
							!attendance.getAttendanceDate()
										   .equals(attendanceDate)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECTDATE,
							finderArgs, attendance);
					}
				}

				return attendance;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SUBJECTDATE,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (Attendance)result;
			}
		}
	}

	/**
	 * Returns all the attendances where batch = &#63; and attendanceDate = &#63;.
	 *
	 * @param batch the batch
	 * @param attendanceDate the attendance date
	 * @return the matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findByBatchDate(long batch, Date attendanceDate)
		throws SystemException {
		return findByBatchDate(batch, attendanceDate, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the attendances where batch = &#63; and attendanceDate = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batch the batch
	 * @param attendanceDate the attendance date
	 * @param start the lower bound of the range of attendances
	 * @param end the upper bound of the range of attendances (not inclusive)
	 * @return the range of matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findByBatchDate(long batch, Date attendanceDate,
		int start, int end) throws SystemException {
		return findByBatchDate(batch, attendanceDate, start, end, null);
	}

	/**
	 * Returns an ordered range of all the attendances where batch = &#63; and attendanceDate = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batch the batch
	 * @param attendanceDate the attendance date
	 * @param start the lower bound of the range of attendances
	 * @param end the upper bound of the range of attendances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findByBatchDate(long batch, Date attendanceDate,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHDATE;
			finderArgs = new Object[] { batch, attendanceDate };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHDATE;
			finderArgs = new Object[] {
					batch, attendanceDate,
					
					start, end, orderByComparator
				};
		}

		List<Attendance> list = (List<Attendance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Attendance attendance : list) {
				if ((batch != attendance.getBatch()) ||
						!Validator.equals(attendanceDate,
							attendance.getAttendanceDate())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_BATCHDATE_BATCH_2);

			if (attendanceDate == null) {
				query.append(_FINDER_COLUMN_BATCHDATE_ATTENDANCEDATE_1);
			}
			else {
				query.append(_FINDER_COLUMN_BATCHDATE_ATTENDANCEDATE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batch);

				if (attendanceDate != null) {
					qPos.add(CalendarUtil.getTimestamp(attendanceDate));
				}

				list = (List<Attendance>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	 *
	 * @param batch the batch
	 * @param attendanceDate the attendance date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attendance
	 * @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance findByBatchDate_First(long batch, Date attendanceDate,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = fetchByBatchDate_First(batch, attendanceDate,
				orderByComparator);

		if (attendance != null) {
			return attendance;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batch=");
		msg.append(batch);

		msg.append(", attendanceDate=");
		msg.append(attendanceDate);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAttendanceException(msg.toString());
	}

	/**
	 * Returns the first attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	 *
	 * @param batch the batch
	 * @param attendanceDate the attendance date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attendance, or <code>null</code> if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance fetchByBatchDate_First(long batch, Date attendanceDate,
		OrderByComparator orderByComparator) throws SystemException {
		List<Attendance> list = findByBatchDate(batch, attendanceDate, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	 *
	 * @param batch the batch
	 * @param attendanceDate the attendance date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attendance
	 * @throws info.diit.portal.NoSuchAttendanceException if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance findByBatchDate_Last(long batch, Date attendanceDate,
		OrderByComparator orderByComparator)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = fetchByBatchDate_Last(batch, attendanceDate,
				orderByComparator);

		if (attendance != null) {
			return attendance;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batch=");
		msg.append(batch);

		msg.append(", attendanceDate=");
		msg.append(attendanceDate);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAttendanceException(msg.toString());
	}

	/**
	 * Returns the last attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	 *
	 * @param batch the batch
	 * @param attendanceDate the attendance date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attendance, or <code>null</code> if a matching attendance could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance fetchByBatchDate_Last(long batch, Date attendanceDate,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBatchDate(batch, attendanceDate);

		List<Attendance> list = findByBatchDate(batch, attendanceDate,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the attendances before and after the current attendance in the ordered set where batch = &#63; and attendanceDate = &#63;.
	 *
	 * @param attendanceId the primary key of the current attendance
	 * @param batch the batch
	 * @param attendanceDate the attendance date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next attendance
	 * @throws info.diit.portal.NoSuchAttendanceException if a attendance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance[] findByBatchDate_PrevAndNext(long attendanceId,
		long batch, Date attendanceDate, OrderByComparator orderByComparator)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = findByPrimaryKey(attendanceId);

		Session session = null;

		try {
			session = openSession();

			Attendance[] array = new AttendanceImpl[3];

			array[0] = getByBatchDate_PrevAndNext(session, attendance, batch,
					attendanceDate, orderByComparator, true);

			array[1] = attendance;

			array[2] = getByBatchDate_PrevAndNext(session, attendance, batch,
					attendanceDate, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Attendance getByBatchDate_PrevAndNext(Session session,
		Attendance attendance, long batch, Date attendanceDate,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ATTENDANCE_WHERE);

		query.append(_FINDER_COLUMN_BATCHDATE_BATCH_2);

		if (attendanceDate == null) {
			query.append(_FINDER_COLUMN_BATCHDATE_ATTENDANCEDATE_1);
		}
		else {
			query.append(_FINDER_COLUMN_BATCHDATE_ATTENDANCEDATE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(batch);

		if (attendanceDate != null) {
			qPos.add(CalendarUtil.getTimestamp(attendanceDate));
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(attendance);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Attendance> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the attendances.
	 *
	 * @return the attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the attendances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of attendances
	 * @param end the upper bound of the range of attendances (not inclusive)
	 * @return the range of attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the attendances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of attendances
	 * @param end the upper bound of the range of attendances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of attendances
	 * @throws SystemException if a system exception occurred
	 */
	public List<Attendance> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Attendance> list = (List<Attendance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ATTENDANCE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ATTENDANCE;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Attendance>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Attendance>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the attendances where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCompany(long companyId) throws SystemException {
		for (Attendance attendance : findByCompany(companyId)) {
			remove(attendance);
		}
	}

	/**
	 * Removes all the attendances where userId = &#63; and batch = &#63; and subject = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param batch the batch
	 * @param subject the subject
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByUserBatchSubject(long userId, long batch, long subject)
		throws SystemException {
		for (Attendance attendance : findByUserBatchSubject(userId, batch,
				subject)) {
			remove(attendance);
		}
	}

	/**
	 * Removes all the attendances where batch = &#63; from the database.
	 *
	 * @param batch the batch
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBatch(long batch) throws SystemException {
		for (Attendance attendance : findByBatch(batch)) {
			remove(attendance);
		}
	}

	/**
	 * Removes the attendance where subject = &#63; and attendanceDate = &#63; from the database.
	 *
	 * @param subject the subject
	 * @param attendanceDate the attendance date
	 * @return the attendance that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public Attendance removeBySubjectDate(long subject, Date attendanceDate)
		throws NoSuchAttendanceException, SystemException {
		Attendance attendance = findBySubjectDate(subject, attendanceDate);

		return remove(attendance);
	}

	/**
	 * Removes all the attendances where batch = &#63; and attendanceDate = &#63; from the database.
	 *
	 * @param batch the batch
	 * @param attendanceDate the attendance date
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBatchDate(long batch, Date attendanceDate)
		throws SystemException {
		for (Attendance attendance : findByBatchDate(batch, attendanceDate)) {
			remove(attendance);
		}
	}

	/**
	 * Removes all the attendances from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Attendance attendance : findAll()) {
			remove(attendance);
		}
	}

	/**
	 * Returns the number of attendances where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCompany(long companyId) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_COMPANY,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_COMPANY_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANY,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of attendances where userId = &#63; and batch = &#63; and subject = &#63;.
	 *
	 * @param userId the user ID
	 * @param batch the batch
	 * @param subject the subject
	 * @return the number of matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public int countByUserBatchSubject(long userId, long batch, long subject)
		throws SystemException {
		Object[] finderArgs = new Object[] { userId, batch, subject };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_USERBATCHSUBJECT,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_ATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_USERBATCHSUBJECT_USERID_2);

			query.append(_FINDER_COLUMN_USERBATCHSUBJECT_BATCH_2);

			query.append(_FINDER_COLUMN_USERBATCHSUBJECT_SUBJECT_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				qPos.add(batch);

				qPos.add(subject);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERBATCHSUBJECT,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of attendances where batch = &#63;.
	 *
	 * @param batch the batch
	 * @return the number of matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBatch(long batch) throws SystemException {
		Object[] finderArgs = new Object[] { batch };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCH,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_BATCH_BATCH_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batch);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCH,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of attendances where subject = &#63; and attendanceDate = &#63;.
	 *
	 * @param subject the subject
	 * @param attendanceDate the attendance date
	 * @return the number of matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public int countBySubjectDate(long subject, Date attendanceDate)
		throws SystemException {
		Object[] finderArgs = new Object[] { subject, attendanceDate };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_SUBJECTDATE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_SUBJECTDATE_SUBJECT_2);

			if (attendanceDate == null) {
				query.append(_FINDER_COLUMN_SUBJECTDATE_ATTENDANCEDATE_1);
			}
			else {
				query.append(_FINDER_COLUMN_SUBJECTDATE_ATTENDANCEDATE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subject);

				if (attendanceDate != null) {
					qPos.add(CalendarUtil.getTimestamp(attendanceDate));
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_SUBJECTDATE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of attendances where batch = &#63; and attendanceDate = &#63;.
	 *
	 * @param batch the batch
	 * @param attendanceDate the attendance date
	 * @return the number of matching attendances
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBatchDate(long batch, Date attendanceDate)
		throws SystemException {
		Object[] finderArgs = new Object[] { batch, attendanceDate };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCHDATE,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ATTENDANCE_WHERE);

			query.append(_FINDER_COLUMN_BATCHDATE_BATCH_2);

			if (attendanceDate == null) {
				query.append(_FINDER_COLUMN_BATCHDATE_ATTENDANCEDATE_1);
			}
			else {
				query.append(_FINDER_COLUMN_BATCHDATE_ATTENDANCEDATE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batch);

				if (attendanceDate != null) {
					qPos.add(CalendarUtil.getTimestamp(attendanceDate));
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCHDATE,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of attendances.
	 *
	 * @return the number of attendances
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ATTENDANCE);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the attendance persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.Attendance")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Attendance>> listenersList = new ArrayList<ModelListener<Attendance>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Attendance>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AttendanceImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_ATTENDANCE = "SELECT attendance FROM Attendance attendance";
	private static final String _SQL_SELECT_ATTENDANCE_WHERE = "SELECT attendance FROM Attendance attendance WHERE ";
	private static final String _SQL_COUNT_ATTENDANCE = "SELECT COUNT(attendance) FROM Attendance attendance";
	private static final String _SQL_COUNT_ATTENDANCE_WHERE = "SELECT COUNT(attendance) FROM Attendance attendance WHERE ";
	private static final String _FINDER_COLUMN_COMPANY_COMPANYID_2 = "attendance.companyId = ?";
	private static final String _FINDER_COLUMN_USERBATCHSUBJECT_USERID_2 = "attendance.userId = ? AND ";
	private static final String _FINDER_COLUMN_USERBATCHSUBJECT_BATCH_2 = "attendance.batch = ? AND ";
	private static final String _FINDER_COLUMN_USERBATCHSUBJECT_SUBJECT_2 = "attendance.subject = ?";
	private static final String _FINDER_COLUMN_BATCH_BATCH_2 = "attendance.batch = ?";
	private static final String _FINDER_COLUMN_SUBJECTDATE_SUBJECT_2 = "attendance.subject = ? AND ";
	private static final String _FINDER_COLUMN_SUBJECTDATE_ATTENDANCEDATE_1 = "attendance.attendanceDate IS NULL";
	private static final String _FINDER_COLUMN_SUBJECTDATE_ATTENDANCEDATE_2 = "attendance.attendanceDate = ?";
	private static final String _FINDER_COLUMN_BATCHDATE_BATCH_2 = "attendance.batch = ? AND ";
	private static final String _FINDER_COLUMN_BATCHDATE_ATTENDANCEDATE_1 = "attendance.attendanceDate IS NULL";
	private static final String _FINDER_COLUMN_BATCHDATE_ATTENDANCEDATE_2 = "attendance.attendanceDate = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "attendance.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Attendance exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Attendance exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AttendancePersistenceImpl.class);
	private static Attendance _nullAttendance = new AttendanceImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Attendance> toCacheModel() {
				return _nullAttendanceCacheModel;
			}
		};

	private static CacheModel<Attendance> _nullAttendanceCacheModel = new CacheModel<Attendance>() {
			public Attendance toEntityModel() {
				return _nullAttendance;
			}
		};
}
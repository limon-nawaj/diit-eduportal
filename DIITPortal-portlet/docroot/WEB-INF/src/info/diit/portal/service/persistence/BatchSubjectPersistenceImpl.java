/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package info.diit.portal.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import info.diit.portal.NoSuchBatchSubjectException;
import info.diit.portal.model.BatchSubject;
import info.diit.portal.model.impl.BatchSubjectImpl;
import info.diit.portal.model.impl.BatchSubjectModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the batch subject service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author mohammad
 * @see BatchSubjectPersistence
 * @see BatchSubjectUtil
 * @generated
 */
public class BatchSubjectPersistenceImpl extends BasePersistenceImpl<BatchSubject>
	implements BatchSubjectPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link BatchSubjectUtil} to access the batch subject persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = BatchSubjectImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHSUBJECTSBYORGCOURSEBATCH =
		new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, BatchSubjectImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByBatchSubjectsByOrgCourseBatch",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECTSBYORGCOURSEBATCH =
		new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, BatchSubjectImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByBatchSubjectsByOrgCourseBatch",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			},
			BatchSubjectModelImpl.ORGANIZATIONID_COLUMN_BITMASK |
			BatchSubjectModelImpl.COURSEID_COLUMN_BITMASK |
			BatchSubjectModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCHSUBJECTSBYORGCOURSEBATCH =
		new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByBatchSubjectsByOrgCourseBatch",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TEACHERID =
		new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, BatchSubjectImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByteacherId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TEACHERID =
		new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, BatchSubjectImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByteacherId",
			new String[] { Long.class.getName() },
			BatchSubjectModelImpl.TEACHERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TEACHERID = new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByteacherId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCH = new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, BatchSubjectImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBybatch",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH = new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, BatchSubjectImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBybatch",
			new String[] { Long.class.getName(), Long.class.getName() },
			BatchSubjectModelImpl.BATCHID_COLUMN_BITMASK |
			BatchSubjectModelImpl.TEACHERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCH = new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBybatch",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHSUBJECT =
		new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, BatchSubjectImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBybatchSubject",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECT =
		new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, BatchSubjectImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBybatchSubject",
			new String[] { Long.class.getName() },
			BatchSubjectModelImpl.BATCHID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BATCHSUBJECT = new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBybatchSubject",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_FETCH_BY_SUBJECT = new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, BatchSubjectImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchBysubject",
			new String[] { Long.class.getName() },
			BatchSubjectModelImpl.SUBJECTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SUBJECT = new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBysubject",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, BatchSubjectImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByorganization",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION =
		new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, BatchSubjectImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByorganization",
			new String[] { Long.class.getName() },
			BatchSubjectModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATION = new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByorganization",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, BatchSubjectImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, BatchSubjectImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the batch subject in the entity cache if it is enabled.
	 *
	 * @param batchSubject the batch subject
	 */
	public void cacheResult(BatchSubject batchSubject) {
		EntityCacheUtil.putResult(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectImpl.class, batchSubject.getPrimaryKey(), batchSubject);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECT,
			new Object[] { Long.valueOf(batchSubject.getSubjectId()) },
			batchSubject);

		batchSubject.resetOriginalValues();
	}

	/**
	 * Caches the batch subjects in the entity cache if it is enabled.
	 *
	 * @param batchSubjects the batch subjects
	 */
	public void cacheResult(List<BatchSubject> batchSubjects) {
		for (BatchSubject batchSubject : batchSubjects) {
			if (EntityCacheUtil.getResult(
						BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
						BatchSubjectImpl.class, batchSubject.getPrimaryKey()) == null) {
				cacheResult(batchSubject);
			}
			else {
				batchSubject.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all batch subjects.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(BatchSubjectImpl.class.getName());
		}

		EntityCacheUtil.clearCache(BatchSubjectImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the batch subject.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(BatchSubject batchSubject) {
		EntityCacheUtil.removeResult(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectImpl.class, batchSubject.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(batchSubject);
	}

	@Override
	public void clearCache(List<BatchSubject> batchSubjects) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (BatchSubject batchSubject : batchSubjects) {
			EntityCacheUtil.removeResult(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
				BatchSubjectImpl.class, batchSubject.getPrimaryKey());

			clearUniqueFindersCache(batchSubject);
		}
	}

	protected void clearUniqueFindersCache(BatchSubject batchSubject) {
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SUBJECT,
			new Object[] { Long.valueOf(batchSubject.getSubjectId()) });
	}

	/**
	 * Creates a new batch subject with the primary key. Does not add the batch subject to the database.
	 *
	 * @param batchSubjectId the primary key for the new batch subject
	 * @return the new batch subject
	 */
	public BatchSubject create(long batchSubjectId) {
		BatchSubject batchSubject = new BatchSubjectImpl();

		batchSubject.setNew(true);
		batchSubject.setPrimaryKey(batchSubjectId);

		return batchSubject;
	}

	/**
	 * Removes the batch subject with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param batchSubjectId the primary key of the batch subject
	 * @return the batch subject that was removed
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject remove(long batchSubjectId)
		throws NoSuchBatchSubjectException, SystemException {
		return remove(Long.valueOf(batchSubjectId));
	}

	/**
	 * Removes the batch subject with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the batch subject
	 * @return the batch subject that was removed
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchSubject remove(Serializable primaryKey)
		throws NoSuchBatchSubjectException, SystemException {
		Session session = null;

		try {
			session = openSession();

			BatchSubject batchSubject = (BatchSubject)session.get(BatchSubjectImpl.class,
					primaryKey);

			if (batchSubject == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchBatchSubjectException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(batchSubject);
		}
		catch (NoSuchBatchSubjectException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected BatchSubject removeImpl(BatchSubject batchSubject)
		throws SystemException {
		batchSubject = toUnwrappedModel(batchSubject);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, batchSubject);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(batchSubject);

		return batchSubject;
	}

	@Override
	public BatchSubject updateImpl(
		info.diit.portal.model.BatchSubject batchSubject, boolean merge)
		throws SystemException {
		batchSubject = toUnwrappedModel(batchSubject);

		boolean isNew = batchSubject.isNew();

		BatchSubjectModelImpl batchSubjectModelImpl = (BatchSubjectModelImpl)batchSubject;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, batchSubject, merge);

			batchSubject.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !BatchSubjectModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((batchSubjectModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECTSBYORGCOURSEBATCH.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchSubjectModelImpl.getOriginalOrganizationId()),
						Long.valueOf(batchSubjectModelImpl.getOriginalCourseId()),
						Long.valueOf(batchSubjectModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHSUBJECTSBYORGCOURSEBATCH,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECTSBYORGCOURSEBATCH,
					args);

				args = new Object[] {
						Long.valueOf(batchSubjectModelImpl.getOrganizationId()),
						Long.valueOf(batchSubjectModelImpl.getCourseId()),
						Long.valueOf(batchSubjectModelImpl.getBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHSUBJECTSBYORGCOURSEBATCH,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECTSBYORGCOURSEBATCH,
					args);
			}

			if ((batchSubjectModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TEACHERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchSubjectModelImpl.getOriginalTeacherId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TEACHERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TEACHERID,
					args);

				args = new Object[] {
						Long.valueOf(batchSubjectModelImpl.getTeacherId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TEACHERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TEACHERID,
					args);
			}

			if ((batchSubjectModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchSubjectModelImpl.getOriginalBatchId()),
						Long.valueOf(batchSubjectModelImpl.getOriginalTeacherId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCH, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH,
					args);

				args = new Object[] {
						Long.valueOf(batchSubjectModelImpl.getBatchId()),
						Long.valueOf(batchSubjectModelImpl.getTeacherId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCH, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH,
					args);
			}

			if ((batchSubjectModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchSubjectModelImpl.getOriginalBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHSUBJECT,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECT,
					args);

				args = new Object[] {
						Long.valueOf(batchSubjectModelImpl.getBatchId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BATCHSUBJECT,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECT,
					args);
			}

			if ((batchSubjectModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchSubjectModelImpl.getOriginalOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);

				args = new Object[] {
						Long.valueOf(batchSubjectModelImpl.getOrganizationId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION,
					args);
			}
		}

		EntityCacheUtil.putResult(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
			BatchSubjectImpl.class, batchSubject.getPrimaryKey(), batchSubject);

		if (isNew) {
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECT,
				new Object[] { Long.valueOf(batchSubject.getSubjectId()) },
				batchSubject);
		}
		else {
			if ((batchSubjectModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_SUBJECT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(batchSubjectModelImpl.getOriginalSubjectId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBJECT, args);

				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SUBJECT, args);

				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECT,
					new Object[] { Long.valueOf(batchSubject.getSubjectId()) },
					batchSubject);
			}
		}

		return batchSubject;
	}

	protected BatchSubject toUnwrappedModel(BatchSubject batchSubject) {
		if (batchSubject instanceof BatchSubjectImpl) {
			return batchSubject;
		}

		BatchSubjectImpl batchSubjectImpl = new BatchSubjectImpl();

		batchSubjectImpl.setNew(batchSubject.isNew());
		batchSubjectImpl.setPrimaryKey(batchSubject.getPrimaryKey());

		batchSubjectImpl.setBatchSubjectId(batchSubject.getBatchSubjectId());
		batchSubjectImpl.setCompanyId(batchSubject.getCompanyId());
		batchSubjectImpl.setOrganizationId(batchSubject.getOrganizationId());
		batchSubjectImpl.setUserId(batchSubject.getUserId());
		batchSubjectImpl.setUserName(batchSubject.getUserName());
		batchSubjectImpl.setCreateDate(batchSubject.getCreateDate());
		batchSubjectImpl.setModifiedDate(batchSubject.getModifiedDate());
		batchSubjectImpl.setCourseId(batchSubject.getCourseId());
		batchSubjectImpl.setBatchId(batchSubject.getBatchId());
		batchSubjectImpl.setSubjectId(batchSubject.getSubjectId());
		batchSubjectImpl.setTeacherId(batchSubject.getTeacherId());
		batchSubjectImpl.setStartDate(batchSubject.getStartDate());
		batchSubjectImpl.setHours(batchSubject.getHours());

		return batchSubjectImpl;
	}

	/**
	 * Returns the batch subject with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the batch subject
	 * @return the batch subject
	 * @throws com.liferay.portal.NoSuchModelException if a batch subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchSubject findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the batch subject with the primary key or throws a {@link info.diit.portal.NoSuchBatchSubjectException} if it could not be found.
	 *
	 * @param batchSubjectId the primary key of the batch subject
	 * @return the batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject findByPrimaryKey(long batchSubjectId)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = fetchByPrimaryKey(batchSubjectId);

		if (batchSubject == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + batchSubjectId);
			}

			throw new NoSuchBatchSubjectException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				batchSubjectId);
		}

		return batchSubject;
	}

	/**
	 * Returns the batch subject with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the batch subject
	 * @return the batch subject, or <code>null</code> if a batch subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BatchSubject fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the batch subject with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param batchSubjectId the primary key of the batch subject
	 * @return the batch subject, or <code>null</code> if a batch subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject fetchByPrimaryKey(long batchSubjectId)
		throws SystemException {
		BatchSubject batchSubject = (BatchSubject)EntityCacheUtil.getResult(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
				BatchSubjectImpl.class, batchSubjectId);

		if (batchSubject == _nullBatchSubject) {
			return null;
		}

		if (batchSubject == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				batchSubject = (BatchSubject)session.get(BatchSubjectImpl.class,
						Long.valueOf(batchSubjectId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (batchSubject != null) {
					cacheResult(batchSubject);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(BatchSubjectModelImpl.ENTITY_CACHE_ENABLED,
						BatchSubjectImpl.class, batchSubjectId,
						_nullBatchSubject);
				}

				closeSession(session);
			}
		}

		return batchSubject;
	}

	/**
	 * Returns all the batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param batchId the batch ID
	 * @return the matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findByBatchSubjectsByOrgCourseBatch(
		long organizationId, long courseId, long batchId)
		throws SystemException {
		return findByBatchSubjectsByOrgCourseBatch(organizationId, courseId,
			batchId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of batch subjects
	 * @param end the upper bound of the range of batch subjects (not inclusive)
	 * @return the range of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findByBatchSubjectsByOrgCourseBatch(
		long organizationId, long courseId, long batchId, int start, int end)
		throws SystemException {
		return findByBatchSubjectsByOrgCourseBatch(organizationId, courseId,
			batchId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of batch subjects
	 * @param end the upper bound of the range of batch subjects (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findByBatchSubjectsByOrgCourseBatch(
		long organizationId, long courseId, long batchId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECTSBYORGCOURSEBATCH;
			finderArgs = new Object[] { organizationId, courseId, batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHSUBJECTSBYORGCOURSEBATCH;
			finderArgs = new Object[] {
					organizationId, courseId, batchId,
					
					start, end, orderByComparator
				};
		}

		List<BatchSubject> list = (List<BatchSubject>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BatchSubject batchSubject : list) {
				if ((organizationId != batchSubject.getOrganizationId()) ||
						(courseId != batchSubject.getCourseId()) ||
						(batchId != batchSubject.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_BATCHSUBJECT_WHERE);

			query.append(_FINDER_COLUMN_BATCHSUBJECTSBYORGCOURSEBATCH_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_BATCHSUBJECTSBYORGCOURSEBATCH_COURSEID_2);

			query.append(_FINDER_COLUMN_BATCHSUBJECTSBYORGCOURSEBATCH_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(courseId);

				qPos.add(batchId);

				list = (List<BatchSubject>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject findByBatchSubjectsByOrgCourseBatch_First(
		long organizationId, long courseId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = fetchByBatchSubjectsByOrgCourseBatch_First(organizationId,
				courseId, batchId, orderByComparator);

		if (batchSubject != null) {
			return batchSubject;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(", courseId=");
		msg.append(courseId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchSubjectException(msg.toString());
	}

	/**
	 * Returns the first batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject fetchByBatchSubjectsByOrgCourseBatch_First(
		long organizationId, long courseId, long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BatchSubject> list = findByBatchSubjectsByOrgCourseBatch(organizationId,
				courseId, batchId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject findByBatchSubjectsByOrgCourseBatch_Last(
		long organizationId, long courseId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = fetchByBatchSubjectsByOrgCourseBatch_Last(organizationId,
				courseId, batchId, orderByComparator);

		if (batchSubject != null) {
			return batchSubject;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(", courseId=");
		msg.append(courseId);

		msg.append(", batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchSubjectException(msg.toString());
	}

	/**
	 * Returns the last batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject fetchByBatchSubjectsByOrgCourseBatch_Last(
		long organizationId, long courseId, long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByBatchSubjectsByOrgCourseBatch(organizationId,
				courseId, batchId);

		List<BatchSubject> list = findByBatchSubjectsByOrgCourseBatch(organizationId,
				courseId, batchId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batch subjects before and after the current batch subject in the ordered set where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	 *
	 * @param batchSubjectId the primary key of the current batch subject
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject[] findByBatchSubjectsByOrgCourseBatch_PrevAndNext(
		long batchSubjectId, long organizationId, long courseId, long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = findByPrimaryKey(batchSubjectId);

		Session session = null;

		try {
			session = openSession();

			BatchSubject[] array = new BatchSubjectImpl[3];

			array[0] = getByBatchSubjectsByOrgCourseBatch_PrevAndNext(session,
					batchSubject, organizationId, courseId, batchId,
					orderByComparator, true);

			array[1] = batchSubject;

			array[2] = getByBatchSubjectsByOrgCourseBatch_PrevAndNext(session,
					batchSubject, organizationId, courseId, batchId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BatchSubject getByBatchSubjectsByOrgCourseBatch_PrevAndNext(
		Session session, BatchSubject batchSubject, long organizationId,
		long courseId, long batchId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCHSUBJECT_WHERE);

		query.append(_FINDER_COLUMN_BATCHSUBJECTSBYORGCOURSEBATCH_ORGANIZATIONID_2);

		query.append(_FINDER_COLUMN_BATCHSUBJECTSBYORGCOURSEBATCH_COURSEID_2);

		query.append(_FINDER_COLUMN_BATCHSUBJECTSBYORGCOURSEBATCH_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		qPos.add(courseId);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batchSubject);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BatchSubject> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the batch subjects where teacherId = &#63;.
	 *
	 * @param teacherId the teacher ID
	 * @return the matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findByteacherId(long teacherId)
		throws SystemException {
		return findByteacherId(teacherId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the batch subjects where teacherId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param teacherId the teacher ID
	 * @param start the lower bound of the range of batch subjects
	 * @param end the upper bound of the range of batch subjects (not inclusive)
	 * @return the range of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findByteacherId(long teacherId, int start, int end)
		throws SystemException {
		return findByteacherId(teacherId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch subjects where teacherId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param teacherId the teacher ID
	 * @param start the lower bound of the range of batch subjects
	 * @param end the upper bound of the range of batch subjects (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findByteacherId(long teacherId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TEACHERID;
			finderArgs = new Object[] { teacherId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TEACHERID;
			finderArgs = new Object[] { teacherId, start, end, orderByComparator };
		}

		List<BatchSubject> list = (List<BatchSubject>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BatchSubject batchSubject : list) {
				if ((teacherId != batchSubject.getTeacherId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_BATCHSUBJECT_WHERE);

			query.append(_FINDER_COLUMN_TEACHERID_TEACHERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(teacherId);

				list = (List<BatchSubject>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch subject in the ordered set where teacherId = &#63;.
	 *
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject findByteacherId_First(long teacherId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = fetchByteacherId_First(teacherId,
				orderByComparator);

		if (batchSubject != null) {
			return batchSubject;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("teacherId=");
		msg.append(teacherId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchSubjectException(msg.toString());
	}

	/**
	 * Returns the first batch subject in the ordered set where teacherId = &#63;.
	 *
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject fetchByteacherId_First(long teacherId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BatchSubject> list = findByteacherId(teacherId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch subject in the ordered set where teacherId = &#63;.
	 *
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject findByteacherId_Last(long teacherId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = fetchByteacherId_Last(teacherId,
				orderByComparator);

		if (batchSubject != null) {
			return batchSubject;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("teacherId=");
		msg.append(teacherId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchSubjectException(msg.toString());
	}

	/**
	 * Returns the last batch subject in the ordered set where teacherId = &#63;.
	 *
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject fetchByteacherId_Last(long teacherId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByteacherId(teacherId);

		List<BatchSubject> list = findByteacherId(teacherId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batch subjects before and after the current batch subject in the ordered set where teacherId = &#63;.
	 *
	 * @param batchSubjectId the primary key of the current batch subject
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject[] findByteacherId_PrevAndNext(long batchSubjectId,
		long teacherId, OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = findByPrimaryKey(batchSubjectId);

		Session session = null;

		try {
			session = openSession();

			BatchSubject[] array = new BatchSubjectImpl[3];

			array[0] = getByteacherId_PrevAndNext(session, batchSubject,
					teacherId, orderByComparator, true);

			array[1] = batchSubject;

			array[2] = getByteacherId_PrevAndNext(session, batchSubject,
					teacherId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BatchSubject getByteacherId_PrevAndNext(Session session,
		BatchSubject batchSubject, long teacherId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCHSUBJECT_WHERE);

		query.append(_FINDER_COLUMN_TEACHERID_TEACHERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(teacherId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batchSubject);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BatchSubject> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the batch subjects where batchId = &#63; and teacherId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param teacherId the teacher ID
	 * @return the matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findBybatch(long batchId, long teacherId)
		throws SystemException {
		return findBybatch(batchId, teacherId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch subjects where batchId = &#63; and teacherId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param teacherId the teacher ID
	 * @param start the lower bound of the range of batch subjects
	 * @param end the upper bound of the range of batch subjects (not inclusive)
	 * @return the range of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findBybatch(long batchId, long teacherId,
		int start, int end) throws SystemException {
		return findBybatch(batchId, teacherId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch subjects where batchId = &#63; and teacherId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param teacherId the teacher ID
	 * @param start the lower bound of the range of batch subjects
	 * @param end the upper bound of the range of batch subjects (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findBybatch(long batchId, long teacherId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCH;
			finderArgs = new Object[] { batchId, teacherId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCH;
			finderArgs = new Object[] {
					batchId, teacherId,
					
					start, end, orderByComparator
				};
		}

		List<BatchSubject> list = (List<BatchSubject>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BatchSubject batchSubject : list) {
				if ((batchId != batchSubject.getBatchId()) ||
						(teacherId != batchSubject.getTeacherId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_BATCHSUBJECT_WHERE);

			query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

			query.append(_FINDER_COLUMN_BATCH_TEACHERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				qPos.add(teacherId);

				list = (List<BatchSubject>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject findBybatch_First(long batchId, long teacherId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = fetchBybatch_First(batchId, teacherId,
				orderByComparator);

		if (batchSubject != null) {
			return batchSubject;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(", teacherId=");
		msg.append(teacherId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchSubjectException(msg.toString());
	}

	/**
	 * Returns the first batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject fetchBybatch_First(long batchId, long teacherId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BatchSubject> list = findBybatch(batchId, teacherId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject findBybatch_Last(long batchId, long teacherId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = fetchBybatch_Last(batchId, teacherId,
				orderByComparator);

		if (batchSubject != null) {
			return batchSubject;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(", teacherId=");
		msg.append(teacherId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchSubjectException(msg.toString());
	}

	/**
	 * Returns the last batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject fetchBybatch_Last(long batchId, long teacherId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBybatch(batchId, teacherId);

		List<BatchSubject> list = findBybatch(batchId, teacherId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batch subjects before and after the current batch subject in the ordered set where batchId = &#63; and teacherId = &#63;.
	 *
	 * @param batchSubjectId the primary key of the current batch subject
	 * @param batchId the batch ID
	 * @param teacherId the teacher ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject[] findBybatch_PrevAndNext(long batchSubjectId,
		long batchId, long teacherId, OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = findByPrimaryKey(batchSubjectId);

		Session session = null;

		try {
			session = openSession();

			BatchSubject[] array = new BatchSubjectImpl[3];

			array[0] = getBybatch_PrevAndNext(session, batchSubject, batchId,
					teacherId, orderByComparator, true);

			array[1] = batchSubject;

			array[2] = getBybatch_PrevAndNext(session, batchSubject, batchId,
					teacherId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BatchSubject getBybatch_PrevAndNext(Session session,
		BatchSubject batchSubject, long batchId, long teacherId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCHSUBJECT_WHERE);

		query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

		query.append(_FINDER_COLUMN_BATCH_TEACHERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(batchId);

		qPos.add(teacherId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batchSubject);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BatchSubject> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the batch subjects where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findBybatchSubject(long batchId)
		throws SystemException {
		return findBybatchSubject(batchId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch subjects where batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of batch subjects
	 * @param end the upper bound of the range of batch subjects (not inclusive)
	 * @return the range of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findBybatchSubject(long batchId, int start,
		int end) throws SystemException {
		return findBybatchSubject(batchId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch subjects where batchId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param batchId the batch ID
	 * @param start the lower bound of the range of batch subjects
	 * @param end the upper bound of the range of batch subjects (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findBybatchSubject(long batchId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BATCHSUBJECT;
			finderArgs = new Object[] { batchId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BATCHSUBJECT;
			finderArgs = new Object[] { batchId, start, end, orderByComparator };
		}

		List<BatchSubject> list = (List<BatchSubject>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BatchSubject batchSubject : list) {
				if ((batchId != batchSubject.getBatchId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_BATCHSUBJECT_WHERE);

			query.append(_FINDER_COLUMN_BATCHSUBJECT_BATCHID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				list = (List<BatchSubject>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch subject in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject findBybatchSubject_First(long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = fetchBybatchSubject_First(batchId,
				orderByComparator);

		if (batchSubject != null) {
			return batchSubject;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchSubjectException(msg.toString());
	}

	/**
	 * Returns the first batch subject in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject fetchBybatchSubject_First(long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BatchSubject> list = findBybatchSubject(batchId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch subject in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject findBybatchSubject_Last(long batchId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = fetchBybatchSubject_Last(batchId,
				orderByComparator);

		if (batchSubject != null) {
			return batchSubject;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("batchId=");
		msg.append(batchId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchSubjectException(msg.toString());
	}

	/**
	 * Returns the last batch subject in the ordered set where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject fetchBybatchSubject_Last(long batchId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBybatchSubject(batchId);

		List<BatchSubject> list = findBybatchSubject(batchId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batch subjects before and after the current batch subject in the ordered set where batchId = &#63;.
	 *
	 * @param batchSubjectId the primary key of the current batch subject
	 * @param batchId the batch ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject[] findBybatchSubject_PrevAndNext(long batchSubjectId,
		long batchId, OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = findByPrimaryKey(batchSubjectId);

		Session session = null;

		try {
			session = openSession();

			BatchSubject[] array = new BatchSubjectImpl[3];

			array[0] = getBybatchSubject_PrevAndNext(session, batchSubject,
					batchId, orderByComparator, true);

			array[1] = batchSubject;

			array[2] = getBybatchSubject_PrevAndNext(session, batchSubject,
					batchId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BatchSubject getBybatchSubject_PrevAndNext(Session session,
		BatchSubject batchSubject, long batchId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCHSUBJECT_WHERE);

		query.append(_FINDER_COLUMN_BATCHSUBJECT_BATCHID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(batchId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batchSubject);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BatchSubject> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns the batch subject where subjectId = &#63; or throws a {@link info.diit.portal.NoSuchBatchSubjectException} if it could not be found.
	 *
	 * @param subjectId the subject ID
	 * @return the matching batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject findBysubject(long subjectId)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = fetchBysubject(subjectId);

		if (batchSubject == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("subjectId=");
			msg.append(subjectId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchBatchSubjectException(msg.toString());
		}

		return batchSubject;
	}

	/**
	 * Returns the batch subject where subjectId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param subjectId the subject ID
	 * @return the matching batch subject, or <code>null</code> if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject fetchBysubject(long subjectId)
		throws SystemException {
		return fetchBysubject(subjectId, true);
	}

	/**
	 * Returns the batch subject where subjectId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param subjectId the subject ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching batch subject, or <code>null</code> if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject fetchBysubject(long subjectId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { subjectId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_SUBJECT,
					finderArgs, this);
		}

		if (result instanceof BatchSubject) {
			BatchSubject batchSubject = (BatchSubject)result;

			if ((subjectId != batchSubject.getSubjectId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_SELECT_BATCHSUBJECT_WHERE);

			query.append(_FINDER_COLUMN_SUBJECT_SUBJECTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				List<BatchSubject> list = q.list();

				result = list;

				BatchSubject batchSubject = null;

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECT,
						finderArgs, list);
				}
				else {
					batchSubject = list.get(0);

					cacheResult(batchSubject);

					if ((batchSubject.getSubjectId() != subjectId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SUBJECT,
							finderArgs, batchSubject);
					}
				}

				return batchSubject;
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (result == null) {
					FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SUBJECT,
						finderArgs);
				}

				closeSession(session);
			}
		}
		else {
			if (result instanceof List<?>) {
				return null;
			}
			else {
				return (BatchSubject)result;
			}
		}
	}

	/**
	 * Returns all the batch subjects where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findByorganization(long organizationId)
		throws SystemException {
		return findByorganization(organizationId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch subjects where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of batch subjects
	 * @param end the upper bound of the range of batch subjects (not inclusive)
	 * @return the range of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findByorganization(long organizationId,
		int start, int end) throws SystemException {
		return findByorganization(organizationId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch subjects where organizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param organizationId the organization ID
	 * @param start the lower bound of the range of batch subjects
	 * @param end the upper bound of the range of batch subjects (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findByorganization(long organizationId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] { organizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORGANIZATION;
			finderArgs = new Object[] {
					organizationId,
					
					start, end, orderByComparator
				};
		}

		List<BatchSubject> list = (List<BatchSubject>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (BatchSubject batchSubject : list) {
				if ((organizationId != batchSubject.getOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_BATCHSUBJECT_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				list = (List<BatchSubject>)QueryUtil.list(q, getDialect(),
						start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first batch subject in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject findByorganization_First(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = fetchByorganization_First(organizationId,
				orderByComparator);

		if (batchSubject != null) {
			return batchSubject;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchSubjectException(msg.toString());
	}

	/**
	 * Returns the first batch subject in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching batch subject, or <code>null</code> if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject fetchByorganization_First(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		List<BatchSubject> list = findByorganization(organizationId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last batch subject in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject findByorganization_Last(long organizationId,
		OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = fetchByorganization_Last(organizationId,
				orderByComparator);

		if (batchSubject != null) {
			return batchSubject;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("organizationId=");
		msg.append(organizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchBatchSubjectException(msg.toString());
	}

	/**
	 * Returns the last batch subject in the ordered set where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching batch subject, or <code>null</code> if a matching batch subject could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject fetchByorganization_Last(long organizationId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByorganization(organizationId);

		List<BatchSubject> list = findByorganization(organizationId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the batch subjects before and after the current batch subject in the ordered set where organizationId = &#63;.
	 *
	 * @param batchSubjectId the primary key of the current batch subject
	 * @param organizationId the organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next batch subject
	 * @throws info.diit.portal.NoSuchBatchSubjectException if a batch subject with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject[] findByorganization_PrevAndNext(long batchSubjectId,
		long organizationId, OrderByComparator orderByComparator)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = findByPrimaryKey(batchSubjectId);

		Session session = null;

		try {
			session = openSession();

			BatchSubject[] array = new BatchSubjectImpl[3];

			array[0] = getByorganization_PrevAndNext(session, batchSubject,
					organizationId, orderByComparator, true);

			array[1] = batchSubject;

			array[2] = getByorganization_PrevAndNext(session, batchSubject,
					organizationId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected BatchSubject getByorganization_PrevAndNext(Session session,
		BatchSubject batchSubject, long organizationId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_BATCHSUBJECT_WHERE);

		query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(organizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(batchSubject);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<BatchSubject> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the batch subjects.
	 *
	 * @return the batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the batch subjects.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of batch subjects
	 * @param end the upper bound of the range of batch subjects (not inclusive)
	 * @return the range of batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the batch subjects.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of batch subjects
	 * @param end the upper bound of the range of batch subjects (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public List<BatchSubject> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<BatchSubject> list = (List<BatchSubject>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_BATCHSUBJECT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_BATCHSUBJECT;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<BatchSubject>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<BatchSubject>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByBatchSubjectsByOrgCourseBatch(long organizationId,
		long courseId, long batchId) throws SystemException {
		for (BatchSubject batchSubject : findByBatchSubjectsByOrgCourseBatch(
				organizationId, courseId, batchId)) {
			remove(batchSubject);
		}
	}

	/**
	 * Removes all the batch subjects where teacherId = &#63; from the database.
	 *
	 * @param teacherId the teacher ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByteacherId(long teacherId) throws SystemException {
		for (BatchSubject batchSubject : findByteacherId(teacherId)) {
			remove(batchSubject);
		}
	}

	/**
	 * Removes all the batch subjects where batchId = &#63; and teacherId = &#63; from the database.
	 *
	 * @param batchId the batch ID
	 * @param teacherId the teacher ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBybatch(long batchId, long teacherId)
		throws SystemException {
		for (BatchSubject batchSubject : findBybatch(batchId, teacherId)) {
			remove(batchSubject);
		}
	}

	/**
	 * Removes all the batch subjects where batchId = &#63; from the database.
	 *
	 * @param batchId the batch ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeBybatchSubject(long batchId) throws SystemException {
		for (BatchSubject batchSubject : findBybatchSubject(batchId)) {
			remove(batchSubject);
		}
	}

	/**
	 * Removes the batch subject where subjectId = &#63; from the database.
	 *
	 * @param subjectId the subject ID
	 * @return the batch subject that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public BatchSubject removeBysubject(long subjectId)
		throws NoSuchBatchSubjectException, SystemException {
		BatchSubject batchSubject = findBysubject(subjectId);

		return remove(batchSubject);
	}

	/**
	 * Removes all the batch subjects where organizationId = &#63; from the database.
	 *
	 * @param organizationId the organization ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByorganization(long organizationId)
		throws SystemException {
		for (BatchSubject batchSubject : findByorganization(organizationId)) {
			remove(batchSubject);
		}
	}

	/**
	 * Removes all the batch subjects from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (BatchSubject batchSubject : findAll()) {
			remove(batchSubject);
		}
	}

	/**
	 * Returns the number of batch subjects where organizationId = &#63; and courseId = &#63; and batchId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @param courseId the course ID
	 * @param batchId the batch ID
	 * @return the number of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public int countByBatchSubjectsByOrgCourseBatch(long organizationId,
		long courseId, long batchId) throws SystemException {
		Object[] finderArgs = new Object[] { organizationId, courseId, batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCHSUBJECTSBYORGCOURSEBATCH,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_BATCHSUBJECT_WHERE);

			query.append(_FINDER_COLUMN_BATCHSUBJECTSBYORGCOURSEBATCH_ORGANIZATIONID_2);

			query.append(_FINDER_COLUMN_BATCHSUBJECTSBYORGCOURSEBATCH_COURSEID_2);

			query.append(_FINDER_COLUMN_BATCHSUBJECTSBYORGCOURSEBATCH_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				qPos.add(courseId);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCHSUBJECTSBYORGCOURSEBATCH,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch subjects where teacherId = &#63;.
	 *
	 * @param teacherId the teacher ID
	 * @return the number of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public int countByteacherId(long teacherId) throws SystemException {
		Object[] finderArgs = new Object[] { teacherId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_TEACHERID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BATCHSUBJECT_WHERE);

			query.append(_FINDER_COLUMN_TEACHERID_TEACHERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(teacherId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TEACHERID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch subjects where batchId = &#63; and teacherId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @param teacherId the teacher ID
	 * @return the number of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public int countBybatch(long batchId, long teacherId)
		throws SystemException {
		Object[] finderArgs = new Object[] { batchId, teacherId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCH,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_BATCHSUBJECT_WHERE);

			query.append(_FINDER_COLUMN_BATCH_BATCHID_2);

			query.append(_FINDER_COLUMN_BATCH_TEACHERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				qPos.add(teacherId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCH,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch subjects where batchId = &#63;.
	 *
	 * @param batchId the batch ID
	 * @return the number of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public int countBybatchSubject(long batchId) throws SystemException {
		Object[] finderArgs = new Object[] { batchId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_BATCHSUBJECT,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BATCHSUBJECT_WHERE);

			query.append(_FINDER_COLUMN_BATCHSUBJECT_BATCHID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(batchId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BATCHSUBJECT,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch subjects where subjectId = &#63;.
	 *
	 * @param subjectId the subject ID
	 * @return the number of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public int countBysubject(long subjectId) throws SystemException {
		Object[] finderArgs = new Object[] { subjectId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_SUBJECT,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BATCHSUBJECT_WHERE);

			query.append(_FINDER_COLUMN_SUBJECT_SUBJECTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(subjectId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_SUBJECT,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch subjects where organizationId = &#63;.
	 *
	 * @param organizationId the organization ID
	 * @return the number of matching batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public int countByorganization(long organizationId)
		throws SystemException {
		Object[] finderArgs = new Object[] { organizationId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_BATCHSUBJECT_WHERE);

			query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(organizationId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of batch subjects.
	 *
	 * @return the number of batch subjects
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_BATCHSUBJECT);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the batch subject persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.info.diit.portal.model.BatchSubject")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<BatchSubject>> listenersList = new ArrayList<ModelListener<BatchSubject>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<BatchSubject>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(BatchSubjectImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = AcademicRecordPersistence.class)
	protected AcademicRecordPersistence academicRecordPersistence;
	@BeanReference(type = AssessmentPersistence.class)
	protected AssessmentPersistence assessmentPersistence;
	@BeanReference(type = AssessmentStudentPersistence.class)
	protected AssessmentStudentPersistence assessmentStudentPersistence;
	@BeanReference(type = AssessmentTypePersistence.class)
	protected AssessmentTypePersistence assessmentTypePersistence;
	@BeanReference(type = AttendancePersistence.class)
	protected AttendancePersistence attendancePersistence;
	@BeanReference(type = AttendanceTopicPersistence.class)
	protected AttendanceTopicPersistence attendanceTopicPersistence;
	@BeanReference(type = BatchPersistence.class)
	protected BatchPersistence batchPersistence;
	@BeanReference(type = BatchFeePersistence.class)
	protected BatchFeePersistence batchFeePersistence;
	@BeanReference(type = BatchPaymentSchedulePersistence.class)
	protected BatchPaymentSchedulePersistence batchPaymentSchedulePersistence;
	@BeanReference(type = BatchStudentPersistence.class)
	protected BatchStudentPersistence batchStudentPersistence;
	@BeanReference(type = BatchSubjectPersistence.class)
	protected BatchSubjectPersistence batchSubjectPersistence;
	@BeanReference(type = BatchTeacherPersistence.class)
	protected BatchTeacherPersistence batchTeacherPersistence;
	@BeanReference(type = ChapterPersistence.class)
	protected ChapterPersistence chapterPersistence;
	@BeanReference(type = ClassRoutineEventPersistence.class)
	protected ClassRoutineEventPersistence classRoutineEventPersistence;
	@BeanReference(type = ClassRoutineEventBatchPersistence.class)
	protected ClassRoutineEventBatchPersistence classRoutineEventBatchPersistence;
	@BeanReference(type = CommentsPersistence.class)
	protected CommentsPersistence commentsPersistence;
	@BeanReference(type = CommunicationRecordPersistence.class)
	protected CommunicationRecordPersistence communicationRecordPersistence;
	@BeanReference(type = CommunicationStudentRecordPersistence.class)
	protected CommunicationStudentRecordPersistence communicationStudentRecordPersistence;
	@BeanReference(type = CounselingPersistence.class)
	protected CounselingPersistence counselingPersistence;
	@BeanReference(type = CounselingCourseInterestPersistence.class)
	protected CounselingCourseInterestPersistence counselingCourseInterestPersistence;
	@BeanReference(type = CoursePersistence.class)
	protected CoursePersistence coursePersistence;
	@BeanReference(type = CourseFeePersistence.class)
	protected CourseFeePersistence courseFeePersistence;
	@BeanReference(type = CourseOrganizationPersistence.class)
	protected CourseOrganizationPersistence courseOrganizationPersistence;
	@BeanReference(type = CourseSessionPersistence.class)
	protected CourseSessionPersistence courseSessionPersistence;
	@BeanReference(type = CourseSubjectPersistence.class)
	protected CourseSubjectPersistence courseSubjectPersistence;
	@BeanReference(type = DailyWorkPersistence.class)
	protected DailyWorkPersistence dailyWorkPersistence;
	@BeanReference(type = DesignationPersistence.class)
	protected DesignationPersistence designationPersistence;
	@BeanReference(type = EmployeePersistence.class)
	protected EmployeePersistence employeePersistence;
	@BeanReference(type = EmployeeAttendancePersistence.class)
	protected EmployeeAttendancePersistence employeeAttendancePersistence;
	@BeanReference(type = EmployeeEmailPersistence.class)
	protected EmployeeEmailPersistence employeeEmailPersistence;
	@BeanReference(type = EmployeeMobilePersistence.class)
	protected EmployeeMobilePersistence employeeMobilePersistence;
	@BeanReference(type = EmployeeRolePersistence.class)
	protected EmployeeRolePersistence employeeRolePersistence;
	@BeanReference(type = EmployeeTimeSchedulePersistence.class)
	protected EmployeeTimeSchedulePersistence employeeTimeSchedulePersistence;
	@BeanReference(type = ExperiancePersistence.class)
	protected ExperiancePersistence experiancePersistence;
	@BeanReference(type = FeeTypePersistence.class)
	protected FeeTypePersistence feeTypePersistence;
	@BeanReference(type = LeavePersistence.class)
	protected LeavePersistence leavePersistence;
	@BeanReference(type = LeaveCategoryPersistence.class)
	protected LeaveCategoryPersistence leaveCategoryPersistence;
	@BeanReference(type = LeaveDayDetailsPersistence.class)
	protected LeaveDayDetailsPersistence leaveDayDetailsPersistence;
	@BeanReference(type = LeaveReceiverPersistence.class)
	protected LeaveReceiverPersistence leaveReceiverPersistence;
	@BeanReference(type = LessonAssessmentPersistence.class)
	protected LessonAssessmentPersistence lessonAssessmentPersistence;
	@BeanReference(type = LessonPlanPersistence.class)
	protected LessonPlanPersistence lessonPlanPersistence;
	@BeanReference(type = LessonTopicPersistence.class)
	protected LessonTopicPersistence lessonTopicPersistence;
	@BeanReference(type = PaymentPersistence.class)
	protected PaymentPersistence paymentPersistence;
	@BeanReference(type = PersonEmailPersistence.class)
	protected PersonEmailPersistence personEmailPersistence;
	@BeanReference(type = PhoneNumberPersistence.class)
	protected PhoneNumberPersistence phoneNumberPersistence;
	@BeanReference(type = RoomPersistence.class)
	protected RoomPersistence roomPersistence;
	@BeanReference(type = StatusHistoryPersistence.class)
	protected StatusHistoryPersistence statusHistoryPersistence;
	@BeanReference(type = StudentPersistence.class)
	protected StudentPersistence studentPersistence;
	@BeanReference(type = StudentAttendancePersistence.class)
	protected StudentAttendancePersistence studentAttendancePersistence;
	@BeanReference(type = StudentDiscountPersistence.class)
	protected StudentDiscountPersistence studentDiscountPersistence;
	@BeanReference(type = StudentDocumentPersistence.class)
	protected StudentDocumentPersistence studentDocumentPersistence;
	@BeanReference(type = StudentFeePersistence.class)
	protected StudentFeePersistence studentFeePersistence;
	@BeanReference(type = StudentPaymentSchedulePersistence.class)
	protected StudentPaymentSchedulePersistence studentPaymentSchedulePersistence;
	@BeanReference(type = SubjectPersistence.class)
	protected SubjectPersistence subjectPersistence;
	@BeanReference(type = SubjectLessonPersistence.class)
	protected SubjectLessonPersistence subjectLessonPersistence;
	@BeanReference(type = TaskPersistence.class)
	protected TaskPersistence taskPersistence;
	@BeanReference(type = TaskDesignationPersistence.class)
	protected TaskDesignationPersistence taskDesignationPersistence;
	@BeanReference(type = TopicPersistence.class)
	protected TopicPersistence topicPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_BATCHSUBJECT = "SELECT batchSubject FROM BatchSubject batchSubject";
	private static final String _SQL_SELECT_BATCHSUBJECT_WHERE = "SELECT batchSubject FROM BatchSubject batchSubject WHERE ";
	private static final String _SQL_COUNT_BATCHSUBJECT = "SELECT COUNT(batchSubject) FROM BatchSubject batchSubject";
	private static final String _SQL_COUNT_BATCHSUBJECT_WHERE = "SELECT COUNT(batchSubject) FROM BatchSubject batchSubject WHERE ";
	private static final String _FINDER_COLUMN_BATCHSUBJECTSBYORGCOURSEBATCH_ORGANIZATIONID_2 =
		"batchSubject.organizationId = ? AND ";
	private static final String _FINDER_COLUMN_BATCHSUBJECTSBYORGCOURSEBATCH_COURSEID_2 =
		"batchSubject.courseId = ? AND ";
	private static final String _FINDER_COLUMN_BATCHSUBJECTSBYORGCOURSEBATCH_BATCHID_2 =
		"batchSubject.batchId = ?";
	private static final String _FINDER_COLUMN_TEACHERID_TEACHERID_2 = "batchSubject.teacherId = ?";
	private static final String _FINDER_COLUMN_BATCH_BATCHID_2 = "batchSubject.batchId = ? AND ";
	private static final String _FINDER_COLUMN_BATCH_TEACHERID_2 = "batchSubject.teacherId = ?";
	private static final String _FINDER_COLUMN_BATCHSUBJECT_BATCHID_2 = "batchSubject.batchId = ?";
	private static final String _FINDER_COLUMN_SUBJECT_SUBJECTID_2 = "batchSubject.subjectId = ?";
	private static final String _FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2 = "batchSubject.organizationId = ?";
	private static final String _ORDER_BY_ENTITY_ALIAS = "batchSubject.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BatchSubject exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No BatchSubject exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(BatchSubjectPersistenceImpl.class);
	private static BatchSubject _nullBatchSubject = new BatchSubjectImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<BatchSubject> toCacheModel() {
				return _nullBatchSubjectCacheModel;
			}
		};

	private static CacheModel<BatchSubject> _nullBatchSubjectCacheModel = new CacheModel<BatchSubject>() {
			public BatchSubject toEntityModel() {
				return _nullBatchSubject;
			}
		};
}